@Echo Off
MOVE root.txt .env
MOVE backend.txt backend\.env
MOVE backup-trigger.txt services\backup-trigger\.env
MOVE frontend.txt frontend\.env
MOVE queue-processor.txt services\queue-processor\.env