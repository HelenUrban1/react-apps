const config = require('../config')();

module.exports = {
  dialect: 'postgres',
  host: config.accountDatabase.host,
  database: config.accountDatabase.database,
  username: config.accountDatabase.username,
  password: config.accountDatabase.password,
  logging: console.log,
};
