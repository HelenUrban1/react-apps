#!/usr/bin/env node

/**
 This script allows developer to search for and purge test data in IXC's back office systems

 If not search text is provided, it will use the value defined in ENV variable IXC_PERSONAL_TEST_DATA_LABEL (which can be added to backend/.env)
 This allows developers to purge data that contains their personal testing text pattern without having to pass search text 

 IXC_PERSONAL_TEST_DATA_LABEL=JDix
 
 To search for backoffice records:

   npm run backoffice:search                          // Will search for records that contain the string defined in IXC_PERSONAL_TEST_DATA_LABEL
   npm run backoffice:search -- -t "My Other Label"   // Will search for records that contain text "My Other Label"

 To purge for backoffice records:
 
   npm run backoffice:purge                           // Will DELETE records that contain the string defined in IXC_PERSONAL_TEST_DATA_LABEL
   npm run backoffice:purge -- -t "My Other Label"    // Will DELETE records that contain text "My Other Label"

 When run, both commands will list the CRM Companies, Contacts, and Deals, as well as the Billing System's Customer and 
 Subscriptions associated with CRM records that match the search text. 

 The purge command will also list the results of the delete request for each object.

 */
const config = require('../config')();
const { log } = require('../src/logger');

const { argv } = require('yargs/yargs')(process.argv.slice(2))
  .usage('Usage: $0 <command> [options]')
  .command('search', 'Lookup backoffice records that match the search text')
  .example('$0 search -t "Some Text"', 'This command will list the Companies, Contacts, and Deals in the CRM as well as the Billing Customer and Subscription')
  .option('t', {
    alias: 'text',
    describe: 'Text to search',
    type: 'string' /* array | boolean | string */,
    nargs: 1,
    demand: 'text is required',
    default: process.env.IXC_PERSONAL_TEST_DATA_LABEL,
  })
  .option('d', {
    alias: 'delete',
    describe: 'Whether to delete the records that are found',
    type: 'boolean' /* array | boolean | string */,
    //    nargs: 1,
    default: false,
  })
  .alias('h', 'help')
  .help('help')
  .epilog('NOTE: This CLI will not run against production systems.');

const makeBillingClient = require('../src/clients/billingClient').default;
const makeCrmClient = require('../src/clients/crmClient').default;

// NEVER run against anything other than test server
if (config.isNotDevEnv) throw Error(`This script can not be run against production systems.`);

// Make this initialization adapter agnostic by renaming config params
const billingClient = makeBillingClient({
  useAdapter: config.billing.useAdapter,
  siteName: config.billing.chargifySiteName,
  publicKey: config.billing.chargifyPublicKey,
  privateKey: config.billing.chargifyPrivateKey,
  apiKey: config.billing.chargifyApiKey,
  logger: log,
});
if (!billingClient.isTestSite()) throw Error(`This script can not be run against production Billing systems.`);

// Initialize subscription billing client
log.debug('ixBackoffice', config.crm.privateAccessToken);
const crmClient = makeCrmClient({
  useAdapter: config.crm.useAdapter,
  apiKey: config.crm.apiKey,
  privateAccessToken: config.crm.privateAccessToken,
  logger: log,
});
// TODO: There doesn't appear to be a way to detect test env in Hubspot API

// Searches CRM Records and deletes associated Chargify records
const searchCrmRecords = async (params) => {
  const { searchText, ixcAccountIDs = {}, bsCustomers = {}, bsSubscriptions = {}, crmContacts = {}, crmCompanies = {}, crmDeals = {} } = params;
  try {
    if (!searchText) throw new Error('Required param `searchText` not defined');
    log.info(`Starting search of Hubspot data for ${process.env.MIGRATION_ENV}`);

    // Lookup Contacts
    let results;
    results = await crmClient.searchContacts({ searchText });

    log.info(`Found ${results.total} CRM Contacts matching ${searchText}`);

    for (let c = 0; c < results.length; c++) {
      const contact = results[c];
      console.log('contact', contact);
      log.debug(`CONTACT ${contact.crmId}  ${contact.email}`);
      crmContacts[contact.crmId] = contact;
      if (contact.associatedcompanyid) crmCompanies[contact.associatedcompanyid] = contact.email;
    }
    // log.debug('Contact IDs', Object.keys(crmContacts));

    // Lookup Companies
    results = await crmClient.searchCompanies({ searchText });
    log.info(`Found ${results.total} CRM Companies matching ${searchText}`);
    for (let c = 0; c < results.length; c++) {
      const company = results[c];
      // log.debug(`COMPANY ${company.crmId} ${company.name}`);
      crmCompanies[company.crmId] = company;
      console.log('company', company);
      if (company.ixc_account_id) ixcAccountIDs[company.ixc_account_id] = company;
      if (company.ix_chargify_id) {
        bsCustomers[company.ix_chargify_id] = company;
        // bsSubscriptions[company.ix_chargify_id] = [];
      }
    }
    // log.debug('Company IDs', Object.keys(crmCompanies));

    // Lookup Deals
    results = await crmClient.searchDeals({ searchText });
    log.info(`Found ${results.total} CRM Deals matching ${searchText}`);
    for (let d = 0; d < results.length; d++) {
      const deal = results[d];
      // log.debug(`DEAL ${deal.crmId} ${deal.dealname}`);
      crmDeals[deal.crmId] = deal;
      if (deal.ix_chargify_id) {
        bsSubscriptions[deal.ix_chargify_id] = undefined;
      }
    }
    // log.debug('Deal IDs', Object.keys(crmDeals));

    // Search for contacts and deals associated with each company
    const crmCompanyIDs = Object.keys(crmCompanies);
    for (let c = 0; c < crmCompanyIDs.length; c++) {
      const companyId = crmCompanyIDs[c];
      const associatedContacts = await crmClient.getCompanysAssociatedContactIds({ companyId });
      log.info(`Company ${companyId} has ${associatedContacts.length} associated Contacts`);
      // log.debug(associatedContacts);
      for (let i = 0; i < associatedContacts.length; i++) {
        const contact = associatedContacts[i];
        crmContacts[contact] = `Associated with company ${companyId}`;
      }

      const associatedDeals = await crmClient.getCompanysAssociatedDealIds({ companyId });
      log.info(`Company ${companyId} has ${associatedDeals.length} associated Deals`);
      // log.debug(associatedDeals);
      for (let i = 0; i < associatedDeals.length; i++) {
        const deal = associatedDeals[i];
        crmDeals[deal] = `Associated with company ${companyId}`;
      }
    }

    // Lookup all of the Chargify records
    log.info(`Found ${Object.keys(bsCustomers).length} Chargify Customers associated with CRM records matching ${searchText}`);
    const bsCustomersIds = Object.keys(bsCustomers);
    for (let i = 0; i < bsCustomersIds.length; i++) {
      const ix_chargify_id = bsCustomersIds[i];

      // Get all subscriptions for Chargify customer
      const subscriptions = await billingClient.getSubscriptionsForCustomer(ix_chargify_id);
      for (let j = 0; j < subscriptions.length; j++) {
        const subscription = subscriptions[j];
        // console.log(`Chargify Test Subscriptions associated with Company ${company.crmId} with Chargify Customer.id ${ix_chargify_id}`);
        // Store each subscription ID with the customer ID for use in the delete calls
        bsSubscriptions[subscription.id] = ix_chargify_id;
      }
    }

    // Search for orphaned Subscription records
    const bsSubscriptionsIds = Object.keys(bsSubscriptions);
    for (let i = 0; i < bsSubscriptionsIds.length; i++) {
      const bsSubscriptionsId = bsSubscriptionsIds[i];
      const bsCustomerId = bsSubscriptions[bsSubscriptionsId];
      if (!bsCustomerId) {
        try {
          const subscription = await billingClient.getSubscriptionById(bsSubscriptionsId);
          bsSubscriptions[subscription.id] = subscription.customer.id;
          // log.debug(`Found Customer for orphan Subscription ${bsSubscriptionsId}`);
        } catch (error) {
          // log.debug(`Orphaned Subscription ${bsSubscriptionsId} removed from delete queue`);
          delete bsSubscriptions[bsSubscriptionsId];
        }
      }
    }

    log.info(`Found ${Object.keys(bsSubscriptions).length} Chargify Subscriptions associated with CRM records matching ${searchText}`);
    // log.debug('Subscription IDs', bsSubscriptions);

    return {
      crmCompanies,
      crmContacts,
      crmDeals,
      bsCustomers,
      bsSubscriptions,
      ixcAccountIDs,
    };
  } catch (error) {
    log.error(error, `searchCrmRecords failed`);
    throw error;
  }
};

// Searches Chargify and deletes records and associated CRM records
const searchChargifyRecords = async (params) => {
  const {
    ixcAccountIDs = {}, //
    bsCustomers = {},
    bsSubscriptions = {},
    crmContacts = {},
    crmCompanies = {},
    crmDeals = {},
  } = params;
  try {
    if (!params.searchText) throw new Error('Required param `searchText` not defined');
    log.info(`Starting search of Chargify data for ${process.env.MIGRATION_ENV}`);
    const searchText = params.searchText.toLowerCase();
    let reSearch;
    if (searchText === '.') {
      reSearch = new RegExp('^.$', 'i');
    } else {
      reSearch = new RegExp(searchText, 'i');
    }

    // Lookup Chargify Customers and Subscriptions
    const directions = ['asc', 'desc'];
    for (let d = 0; d < directions.length; d++) {
      const results = await billingClient.getSubscriptions({ per_page: 200, sort: 'created_at', direction: directions[d] });
      log.info(`Search ${directions[d]}`);
      for (let i = 0; i < results.length; i++) {
        const record = results[i];
        const { customer } = record.subscription;
        let match = false;
        if (customer.first_name && reSearch.test(customer.first_name)) match = true;
        if (customer.last_name && reSearch.test(customer.last_name)) match = true;
        if (customer.organization && reSearch.test(customer.organization)) match = true;
        log.debug(`Test of chargify customer "${customer.organization} ${customer.first_name} ${customer.last_name}" includes text "${searchText}" is ${match}`);
        if (match) {
          // log.info(`Chargify Customer "${customer.organization} ${customer.first_name} ${customer.last_name}" has a subscription`);
          // log.debug(record.subscription);
          bsSubscriptions[record.subscription.id] = record.subscription.customer.id;
          // Get CRM Company ID
          const customerMeta = await billingClient.getMetadataForCustomerWithId(record.subscription.customer.id);
          // log.debug(`Chargify mapped to CRM COMPANY ${customerMeta.hubspot_company_id}`);
          if (customerMeta.hubspot_company_id) {
            crmCompanies[customerMeta.hubspot_company_id.trim()] = {
              subscriptionId: record.subscription.id,
              customerId: record.subscription.customer.id,
            };
          }
        }
      }
    }

    // Search for contacts and deals associated with each company
    const crmCompanyIDs = Object.keys(crmCompanies);
    for (let c = 0; c < crmCompanyIDs.length; c++) {
      const companyId = crmCompanyIDs[c];
      const associatedContacts = await crmClient.getCompanysAssociatedContactIds({ companyId });
      // log.info(`Company ${companyId} has ${associatedContacts.length} associated Contacts`);
      // log.debug(associatedContacts);
      for (let i = 0; i < associatedContacts.length; i++) {
        const contact = associatedContacts[i];
        crmContacts[contact] = `Associated with company ${companyId}`;
      }

      const associatedDeals = await crmClient.getCompanysAssociatedDealIds({ companyId });
      // log.info(`Company ${companyId} has ${associatedDeals.length} associated Deals`);
      // log.debug(associatedDeals);
      for (let i = 0; i < associatedDeals.length; i++) {
        const deal = associatedDeals[i];
        crmDeals[deal] = `Associated with company ${companyId}`;
      }
    }

    return {
      crmCompanies,
      crmContacts,
      crmDeals,
      bsCustomers,
      bsSubscriptions,
      ixcAccountIDs,
    };
  } catch (error) {
    log.error(error, `searchChargifyRecords failed`);
    throw error;
  }
};

const purgeBackofficeRecords = async (params) => {
  const { crmCompanies, crmContacts, crmDeals, bsCustomers, bsSubscriptions, ixcAccountIDs } = params;
  try {
    let results = ['Deletes not committed'];
    const tasks = [];

    // Delete companies
    Object.keys(crmCompanies).forEach((id) => {
      tasks.push(crmClient.deleteCompany({ id }));
    });

    // Delete contacts
    Object.keys(crmContacts).forEach((id) => {
      tasks.push(crmClient.deleteContact({ id }));
    });

    // Delete deals
    Object.keys(crmDeals).forEach((id) => {
      tasks.push(crmClient.deleteDeal({ id }));
    });

    // Delete Chargify Customers and Subscriptions
    Object.entries(bsSubscriptions).forEach(([subscriptionId, customerId]) => {
      tasks.push(billingClient.purgeSubscription({ subscriptionId, customerId }));
    });

    // Perform all deletes in parallel
    results = await Promise.all(tasks);

    // TODO: Drop DBs in local test env
    // ixcAccountIDs[company.ixc_account_id] = company;

    return results;
  } catch (error) {
    log.error(error, `purgeBackofficeRecords failed`);
    throw error;
  }
};

// Main function
(async () => {
  log.debug('argv', argv);

  const searchText = argv.text;
  const commitDelete = argv.delete;

  let searchResults = {};

  // Search the Hubspot
  searchResults = await searchCrmRecords({ searchText });
  // log.info(`Results of search for CRM objects associated with any object matching search "${searchText}"`, searchResults);

  // Search the Chargify
  searchResults = await searchChargifyRecords({ searchText, ...searchResults });
  // log.info(`Results of search for Chargify objects associated with any object matching search "${searchText}"`, searchResults);
  log.info(`Results of search matching search "${searchText}"`, searchResults);

  if (commitDelete) {
    log.info(`Preparing to delete CRM objects associated with any object matching search "${searchText}"`);
    const deleteResults = await purgeBackofficeRecords(searchResults);
    log.info(`Results of request to delete backoffice objects associated with any object matching search "${searchText}"`, deleteResults);
  }
})()
  .then(() => {
    log.debug(`ixBackoffice returned without errors.`);
  })
  .catch((error) => {
    log.error(`Error occurred while running ixBackoffice: ${error.message}`);
    log.error(error?.stack);
  });

// TODO: Delete by ixAccount
// const getLocalAccountIds = (accountId) => {
//   try {
//     // Connect to the DB
//     const models = require('../src/database/models');
//     const AccountService = require('../src/services/accountService');

//     // FROM backend/storage/seeders/primary/20200101000000-ixc-account-users.js
//     const rootContext = {
//       currentUser: {
//         userId: 'e0a01779-c8ed-4090-bd44-9c1b2242fb75', // DEV_MONKEY_USER_ID
//         accountId: 'e0a01769-c8ed-4090-bd24-9c1b2241fb7', // ACME_CORP_ACCOUNT_ID
//         // siteId: '',
//       },
//       language: 'en',
//       // subscriptionWorker: undefined,
//     };

//     // Query the main DB
//     const accountService = new AccountService(rootContext);
//     const accountList = await accountService.findAndCountAll();
//     // console.log('accountList', accountList);
//     log.info(`Accounts Found: ${accountList.count}`);
//     // console.log('accountList', accountList.rows);

//     accountIds = [];
//     const accountList = Object.entries(accountList.rows).reduce((obj, account) => {
//       obj.push({
//         id: account.id,
//         id: account.name,
//       });
//       return obj;
//     }, accountIds);

//     return accountIds;
//   } catch (error) {
//     log.error(error, `getLocalAccountIds failed`);
//     throw error;
//   }
// };

// const purgeBackofficeRecordsForIxAccount = (accountId) => {
//   try {
//     // Delete by IX Account ID
//     // Get the subscription, crmDeal, and crmCompany records
//     const bsSubscription = await this.getSubscriptionForAccountId(accountId);
//     const bsCustomerMetadata = await this.billingClient.getMetadataForCustomerWithId(bsSubscription.customer.id);
//     const crmCompany = await this.crmClient.getCompanyById(bsCustomerMetadata.hubspot_company_id.trim());
//     const crmContact = await this.crmClient.getContactByEmail(email);
//     const accountMemberService = new AccountMemberService(context);
//     const accountMembersList = await accountMemberService.findAndCountAll();
//     // console.log('accountList', accountList);
//     log.info(`Accounts Members Found: ${accountMembersList.count}`);
//     // console.log('Account Members', accountMembersList.rows);

//     const subscriptionService = new SubscriptionService(context);
//     const subscriptionList = await subscriptionService.findAndCountAll({ withRelations: true });
//     console.log('accountList', accountList);
//     log.info(`Subscriptions Found: ${subscriptionList.count}`);
//     console.log('Account Members', subscriptionList.rows);
//     return { subscriptionList };
//   } catch (error) {
//     log.error(error, `purgeBackofficeRecordsForIxAccount failed`);
//     throw error;
//   }
// };
