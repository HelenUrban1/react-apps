-- Revoke privalages and drop development
-- REVOKE read/write role privlages on future objects
ALTER DEFAULT PRIVILEGES FOR ROLE "ixc_mono_role_readwrite" IN SCHEMA "public" REVOKE ALL PRIVILEGES ON TABLES FROM "ixc_mono_role_readwrite";
ALTER DEFAULT PRIVILEGES FOR ROLE "ixc_mono_role_readwrite" IN SCHEMA "public" REVOKE ALL PRIVILEGES ON SEQUENCES FROM "ixc_mono_role_readwrite";
ALTER DEFAULT PRIVILEGES FOR ROLE "ixc_mono_role_readwrite" IN SCHEMA "public" REVOKE ALL PRIVILEGES ON FUNCTIONS FROM "ixc_mono_role_readwrite";
-- REVOKE read/write role privlages on existing objects
REVOKE ALL PRIVILEGES ON ALL TABLES IN SCHEMA "public" FROM "ixc_mono_role_readwrite";
REVOKE ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA "public" FROM "ixc_mono_role_readwrite";
REVOKE ALL PRIVILEGES ON ALL FUNCTIONS IN SCHEMA "public" FROM "ixc_mono_role_readwrite";
-- REVOKE schema and connect privlages from ixc_mono_role_readwrite
REVOKE ALL PRIVILEGES ON SCHEMA "public" FROM "ixc_mono_role_readwrite";
REVOKE ALL PRIVILEGES ON DATABASE "ixc_mono_development" FROM "ixc_mono_role_readwrite";
-- Revoke Owner role privlages on future objects
ALTER DEFAULT PRIVILEGES FOR ROLE "ixc_mono_role_owner" IN SCHEMA "public" REVOKE ALL PRIVILEGES ON TABLES FROM "ixc_mono_role_owner";
ALTER DEFAULT PRIVILEGES FOR ROLE "ixc_mono_role_owner" IN SCHEMA "public" REVOKE ALL PRIVILEGES ON SEQUENCES FROM "ixc_mono_role_owner";
ALTER DEFAULT PRIVILEGES FOR ROLE "ixc_mono_role_owner" IN SCHEMA "public" REVOKE ALL PRIVILEGES ON FUNCTIONS FROM "ixc_mono_role_owner";
-- REVOKE schema and connect privlages from ixc_mono_role_readwrite
REVOKE ALL PRIVILEGES ON SCHEMA "public" FROM "ixc_mono_role_owner";
REVOKE ALL PRIVILEGES ON DATABASE "ixc_mono_development" FROM "ixc_mono_role_owner";
--- Drop database
DROP DATABASE IF EXISTS "ixc_mono_development";



-- Revoke privalages and drop test
-- REVOKE read/write role privlages on future objects
ALTER DEFAULT PRIVILEGES FOR ROLE "ixc_mono_role_readwrite" IN SCHEMA "public" REVOKE ALL PRIVILEGES ON TABLES FROM "ixc_mono_role_readwrite";
ALTER DEFAULT PRIVILEGES FOR ROLE "ixc_mono_role_readwrite" IN SCHEMA "public" REVOKE ALL PRIVILEGES ON SEQUENCES FROM "ixc_mono_role_readwrite";
ALTER DEFAULT PRIVILEGES FOR ROLE "ixc_mono_role_readwrite" IN SCHEMA "public" REVOKE ALL PRIVILEGES ON FUNCTIONS FROM "ixc_mono_role_readwrite";
-- REVOKE read/write role privlages on existing objects
REVOKE ALL PRIVILEGES ON ALL TABLES IN SCHEMA "public" FROM "ixc_mono_role_readwrite";
REVOKE ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA "public" FROM "ixc_mono_role_readwrite";
REVOKE ALL PRIVILEGES ON ALL FUNCTIONS IN SCHEMA "public" FROM "ixc_mono_role_readwrite";
-- REVOKE schema and connect privlages from ixc_mono_role_readwrite
REVOKE ALL PRIVILEGES ON SCHEMA "public" FROM "ixc_mono_role_readwrite";
REVOKE ALL PRIVILEGES ON DATABASE "ixc_mono_test" FROM "ixc_mono_role_readwrite";
-- Revoke Owner role privlages on future objects
ALTER DEFAULT PRIVILEGES FOR ROLE "ixc_mono_role_owner" IN SCHEMA "public" REVOKE ALL PRIVILEGES ON TABLES FROM "ixc_mono_role_owner";
ALTER DEFAULT PRIVILEGES FOR ROLE "ixc_mono_role_owner" IN SCHEMA "public" REVOKE ALL PRIVILEGES ON SEQUENCES FROM "ixc_mono_role_owner";
ALTER DEFAULT PRIVILEGES FOR ROLE "ixc_mono_role_owner" IN SCHEMA "public" REVOKE ALL PRIVILEGES ON FUNCTIONS FROM "ixc_mono_role_owner";
-- REVOKE schema and connect privlages from ixc_mono_role_readwrite
REVOKE ALL PRIVILEGES ON SCHEMA "public" FROM "ixc_mono_role_owner";
REVOKE ALL PRIVILEGES ON DATABASE "ixc_mono_test" FROM "ixc_mono_role_owner";
--- Drop database
DROP DATABASE IF EXISTS "ixc_mono_test";





-- Revoke privalages and drop production
-- REVOKE read/write role privlages on future objects
ALTER DEFAULT PRIVILEGES FOR ROLE "ixc_mono_role_readwrite" IN SCHEMA "public" REVOKE ALL PRIVILEGES ON TABLES FROM "ixc_mono_role_readwrite";
ALTER DEFAULT PRIVILEGES FOR ROLE "ixc_mono_role_readwrite" IN SCHEMA "public" REVOKE ALL PRIVILEGES ON SEQUENCES FROM "ixc_mono_role_readwrite";
ALTER DEFAULT PRIVILEGES FOR ROLE "ixc_mono_role_readwrite" IN SCHEMA "public" REVOKE ALL PRIVILEGES ON FUNCTIONS FROM "ixc_mono_role_readwrite";
-- REVOKE read/write role privlages on existing objects
REVOKE ALL PRIVILEGES ON ALL TABLES IN SCHEMA "public" FROM "ixc_mono_role_readwrite";
REVOKE ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA "public" FROM "ixc_mono_role_readwrite";
REVOKE ALL PRIVILEGES ON ALL FUNCTIONS IN SCHEMA "public" FROM "ixc_mono_role_readwrite";
-- REVOKE schema and connect privlages from ixc_mono_role_readwrite
REVOKE ALL PRIVILEGES ON SCHEMA "public" FROM "ixc_mono_role_readwrite";
REVOKE ALL PRIVILEGES ON DATABASE "ixc_mono_production" FROM "ixc_mono_role_readwrite";
-- Revoke Owner role privlages on future objects
ALTER DEFAULT PRIVILEGES FOR ROLE "ixc_mono_role_owner" IN SCHEMA "public" REVOKE ALL PRIVILEGES ON TABLES FROM "ixc_mono_role_owner";
ALTER DEFAULT PRIVILEGES FOR ROLE "ixc_mono_role_owner" IN SCHEMA "public" REVOKE ALL PRIVILEGES ON SEQUENCES FROM "ixc_mono_role_owner";
ALTER DEFAULT PRIVILEGES FOR ROLE "ixc_mono_role_owner" IN SCHEMA "public" REVOKE ALL PRIVILEGES ON FUNCTIONS FROM "ixc_mono_role_owner";
-- REVOKE schema and connect privlages from ixc_mono_role_readwrite
REVOKE ALL PRIVILEGES ON SCHEMA "public" FROM "ixc_mono_role_owner";
REVOKE ALL PRIVILEGES ON DATABASE "ixc_mono_production" FROM "ixc_mono_role_owner";
--- Drop database
DROP DATABASE IF EXISTS "ixc_mono_production";



-- Remove roles from users
REVOKE "ixc_mono_role_owner" FROM "ixc_mono_owner";
REVOKE "ixc_mono_role_readwrite" FROM "ixc_mono_app";

-- Drop roles
REASSIGN OWNED BY "ixc_mono_role_readwrite" TO "postgres";
DROP OWNED BY "ixc_mono_role_readwrite";
DROP ROLE IF EXISTS "ixc_mono_role_readwrite";

REASSIGN OWNED BY "ixc_mono_role_owner" TO "postgres";
DROP OWNED BY "ixc_mono_role_owner";
DROP ROLE IF EXISTS "ixc_mono_role_owner";

-- Drop users
REASSIGN OWNED BY "ixc_mono_app" TO "postgres";
DROP OWNED BY "ixc_mono_app";
DROP USER IF EXISTS "ixc_mono_app";

REASSIGN OWNED BY "ixc_mono_owner" TO "postgres";
DROP OWNED BY "ixc_mono_owner";
DROP USER IF EXISTS "ixc_mono_owner";
