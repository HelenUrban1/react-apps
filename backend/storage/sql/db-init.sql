
-- Create the Owner Role and Owner User which is used by developers and npm scripts to alter the DB schema
CREATE USER "ixc_mono_owner" WITH CREATEROLE CREATEDB PASSWORD 'Di882opK9Z2KofpjAEnoDGa6eZcq';
CREATE ROLE "ixc_mono_role_owner" WITH CREATEROLE CREATEDB;
GRANT "ixc_mono_role_owner" TO "ixc_mono_owner";

-- Create the App Role and App User which the applicaiton will use during operation
CREATE USER "ixc_mono_app" LOGIN PASSWORD 'pjAEnoDGa6eZcqDi882opK9Z2Kof';
CREATE ROLE "ixc_mono_role_readwrite";
GRANT "ixc_mono_role_readwrite" TO "ixc_mono_app";



-- Create development DB
CREATE DATABASE "ixc_mono_development" ENCODING 'UTF8' LC_COLLATE = 'en_US.utf8' LC_CTYPE = 'en_US.utf8';
-- Revokes the public role’s ability to connect to the database
REVOKE ALL ON DATABASE "ixc_mono_development" FROM PUBLIC;
-- Grant Owner role privlages on existing objects
GRANT ALL PRIVILEGES ON DATABASE "ixc_mono_development" TO "ixc_mono_role_owner";
-- Grant Owner role privlages on future objects
ALTER DEFAULT PRIVILEGES FOR ROLE "ixc_mono_role_owner" IN SCHEMA "public" GRANT ALL ON TABLES TO "ixc_mono_role_owner";
ALTER DEFAULT PRIVILEGES FOR ROLE "ixc_mono_role_owner" IN SCHEMA "public" GRANT USAGE ON SEQUENCES TO "ixc_mono_role_owner";
ALTER DEFAULT PRIVILEGES FOR ROLE "ixc_mono_role_owner" IN SCHEMA "public" GRANT EXECUTE ON FUNCTIONS TO "ixc_mono_role_owner";
-- Grant privlages to ixc_mono_role_readwrite
GRANT CONNECT ON DATABASE "ixc_mono_development" TO "ixc_mono_role_readwrite";
GRANT USAGE ON SCHEMA "public" TO "ixc_mono_role_readwrite";
-- Grant read/write role privlages on existing objects
GRANT SELECT, INSERT, UPDATE, DELETE ON ALL TABLES IN SCHEMA "public" TO "ixc_mono_role_readwrite";
GRANT USAGE ON ALL SEQUENCES IN SCHEMA "public" TO "ixc_mono_role_readwrite";
-- Grant read/write role privlages on future objects
ALTER DEFAULT PRIVILEGES FOR ROLE "ixc_mono_role_readwrite" IN SCHEMA "public" GRANT SELECT, INSERT, UPDATE, DELETE ON TABLES TO "ixc_mono_role_readwrite";
ALTER DEFAULT PRIVILEGES FOR ROLE "ixc_mono_role_readwrite" IN SCHEMA "public" GRANT USAGE ON SEQUENCES TO "ixc_mono_role_readwrite";



-- Create test DB
CREATE DATABASE "ixc_mono_test" ENCODING 'UTF8' LC_COLLATE = 'en_US.utf8' LC_CTYPE = 'en_US.utf8';
-- Revokes the public role’s ability to connect to the database
REVOKE ALL ON DATABASE "ixc_mono_test" FROM PUBLIC;
-- Grant Owner role privlages on existing objects
GRANT ALL PRIVILEGES ON DATABASE "ixc_mono_test" TO "ixc_mono_role_owner";
-- Grant Owner role privlages on future objects
ALTER DEFAULT PRIVILEGES FOR ROLE "ixc_mono_role_owner" IN SCHEMA "public" GRANT ALL ON TABLES TO "ixc_mono_role_owner";
ALTER DEFAULT PRIVILEGES FOR ROLE "ixc_mono_role_owner" IN SCHEMA "public" GRANT USAGE ON SEQUENCES TO "ixc_mono_role_owner";
ALTER DEFAULT PRIVILEGES FOR ROLE "ixc_mono_role_owner" IN SCHEMA "public" GRANT EXECUTE ON FUNCTIONS TO "ixc_mono_role_owner";
-- Grant privlages to ixc_mono_role_readwrite
GRANT CONNECT ON DATABASE "ixc_mono_test" TO "ixc_mono_role_readwrite";
GRANT USAGE ON SCHEMA "public" TO "ixc_mono_role_readwrite";
-- Grant read/write role privlages on existing objects
GRANT SELECT, INSERT, UPDATE, DELETE ON ALL TABLES IN SCHEMA "public" TO "ixc_mono_role_readwrite";
GRANT USAGE ON ALL SEQUENCES IN SCHEMA "public" TO "ixc_mono_role_readwrite";
-- Grant read/write role privlages on future objects
ALTER DEFAULT PRIVILEGES FOR ROLE "ixc_mono_role_readwrite" IN SCHEMA "public" GRANT SELECT, INSERT, UPDATE, DELETE ON TABLES TO "ixc_mono_role_readwrite";
ALTER DEFAULT PRIVILEGES FOR ROLE "ixc_mono_role_readwrite" IN SCHEMA "public" GRANT USAGE ON SEQUENCES TO "ixc_mono_role_readwrite";



-- Create production DB
CREATE DATABASE "ixc_mono_production" ENCODING 'UTF8' LC_COLLATE = 'en_US.utf8' LC_CTYPE = 'en_US.utf8';
-- Revokes the public role’s ability to connect to the database
REVOKE ALL ON DATABASE "ixc_mono_production" FROM PUBLIC;
-- Grant Owner role privlages on existing objects
GRANT ALL PRIVILEGES ON DATABASE "ixc_mono_production" TO "ixc_mono_role_owner";
-- Grant Owner role privlages on future objects
ALTER DEFAULT PRIVILEGES FOR ROLE "ixc_mono_role_owner" IN SCHEMA "public" GRANT ALL ON TABLES TO "ixc_mono_role_owner";
ALTER DEFAULT PRIVILEGES FOR ROLE "ixc_mono_role_owner" IN SCHEMA "public" GRANT USAGE ON SEQUENCES TO "ixc_mono_role_owner";
ALTER DEFAULT PRIVILEGES FOR ROLE "ixc_mono_role_owner" IN SCHEMA "public" GRANT EXECUTE ON FUNCTIONS TO "ixc_mono_role_owner";
-- Grant privlages to ixc_mono_role_readwrite
GRANT CONNECT ON DATABASE "ixc_mono_production" TO "ixc_mono_role_readwrite";
GRANT USAGE ON SCHEMA "public" TO "ixc_mono_role_readwrite";
-- Grant read/write role privlages on existing objects
GRANT SELECT, INSERT, UPDATE, DELETE ON ALL TABLES IN SCHEMA "public" TO "ixc_mono_role_readwrite";
GRANT USAGE ON ALL SEQUENCES IN SCHEMA "public" TO "ixc_mono_role_readwrite";
-- Grant read/write role privlages on future objects
ALTER DEFAULT PRIVILEGES FOR ROLE "ixc_mono_role_readwrite" IN SCHEMA "public" GRANT SELECT, INSERT, UPDATE, DELETE ON TABLES TO "ixc_mono_role_readwrite";
ALTER DEFAULT PRIVILEGES FOR ROLE "ixc_mono_role_readwrite" IN SCHEMA "public" GRANT USAGE ON SEQUENCES TO "ixc_mono_role_readwrite";

-- Grant Owner role privlages on future objects
ALTER DEFAULT PRIVILEGES FOR ROLE "ixc_mono_role_owner" IN SCHEMA "public" GRANT ALL ON TABLES TO "ixc_mono_role_owner";
ALTER DEFAULT PRIVILEGES FOR ROLE "ixc_mono_role_owner" IN SCHEMA "public" GRANT USAGE ON SEQUENCES TO "ixc_mono_role_owner";
ALTER DEFAULT PRIVILEGES FOR ROLE "ixc_mono_role_owner" IN SCHEMA "public" GRANT EXECUTE ON FUNCTIONS TO "ixc_mono_role_owner";
-- Grant privlages to ixc_mono_role_readwrite

GRANT USAGE ON SCHEMA "public" TO "ixc_mono_role_readwrite";

-- Grant read/write role privlages on existing objects
GRANT SELECT, INSERT, UPDATE, DELETE ON ALL TABLES IN SCHEMA "public" TO "ixc_mono_role_readwrite";
GRANT USAGE ON ALL SEQUENCES IN SCHEMA "public" TO "ixc_mono_role_readwrite";
-- Grant read/write role privlages on future objects
ALTER DEFAULT PRIVILEGES FOR ROLE "ixc_mono_role_readwrite" IN SCHEMA "public" GRANT SELECT, INSERT, UPDATE, DELETE ON TABLES TO "ixc_mono_role_readwrite";
ALTER DEFAULT PRIVILEGES FOR ROLE "ixc_mono_role_readwrite" IN SCHEMA "public" GRANT USAGE ON SEQUENCES TO "ixc_mono_role_readwrite";
