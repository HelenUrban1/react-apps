/**
 * This script is responsible for resetting the SQL database.
 * Run it via `npm run db:reset:<environment>`.
 */
const models = require('../src/database/models');
const { log } = require('../src/logger');

log.debug(`Resetting ${process.env.MIGRATION_ENV}...`);

models.sequelize
  .sync({ force: true })
  .then(() => {
    log.debug('OK');
    process.exit();
  })
  .catch((error) => {
    log.error(error);
    process.exit(1);
  });
