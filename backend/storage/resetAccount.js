/**
 * This script is responsible for resetting the SQL database for a test Account.
 * Run it via `npm run db:reset-account:<environment>`.
 *
 * The primary database should be reset and seeded with an account with an ID
 * matching config.testAccountId before running this, because it needs to be
 * able to look up the Account in the primary database to run.
 */
const models = require('../src/database/models');
const config = require('../config')();
const { log } = require('../src/logger');

log.debug(`Resetting Account Database on ${process.env.MIGRATION_ENV}...`);

const resetTables = async (accountId) => {
  await models.getTenant(accountId).then((tenant) => {
    tenant.sequelize
      .sync({ force: true })
      .then(() => {
        log.debug('OK');
        return true;
      })
      .catch((error) => {
        log.error(error);
        return false;
      });
  });
};

const createDatabase = async (accountDatabase, accountId) => {
  log.debug(`createDatabase ${accountDatabase.database}`);
  await models.sequelizeAdmin
    .query(`CREATE DATABASE "${accountDatabase.database}" OWNER "${accountDatabase.username}";`, {
      type: models.sequelize.QueryTypes.RAW,
    })
    .then(async () => {
      // Remove ixc_mono_owner's ability to access DBs after the reset (protection against theft of master PW)
      await models.sequelizeAdmin.query(`REVOKE "${accountDatabase.username}" FROM "ixc_mono_owner"`, { type: models.sequelize.QueryTypes.RAW });
    })
    .then(() => resetTables(accountId)) // resetTables runs with tenant user permissions (not admin)
    .catch((error) => {
      // If error code is 42P04, DB already exists, so we continue with the reset
      if (error.original.code === '42P04') {
        return resetTables(accountId);
      }
      log.error(error);
      return false;
    });
};

const createUser = async (accountDatabase, accountId) => {
  log.debug(`createUser ${accountDatabase.username}`);
  return models.sequelizeAdmin
    .query(
      `DO $$
    BEGIN
      CREATE USER "${accountDatabase.username}" WITH PASSWORD '${accountDatabase.password}';
      EXCEPTION WHEN DUPLICATE_OBJECT THEN RAISE NOTICE 'User already exists';
      GRANT "${accountDatabase.username}" TO "ixc_mono_owner";
    END
    $$;`,
      {
        type: models.sequelize.QueryTypes.RAW,
      }
    )
    .then(() => createDatabase(accountDatabase, accountId))
    .catch((error) => {
      log.error(error);
      process.exit(1);
    });
};

const createUsers = async () => {
  if (config.testAccountId) {
    await createUser(config.accountDatabase, config.testAccountId);
  } else {
    log.debug(`testAccountId not defined, skipping reset for Account Database on ${process.env.MIGRATION_ENV}...`);
    process.exit();
  }

  if (config.reviewAccountId) {
    await createUser(config.reviewAccountDatabase, config.reviewAccountId);
  } else {
    log.debug(`reviewAccountId not defined, skipping reset for Review Account Database on ${process.env.MIGRATION_ENV}...`);
    process.exit();
  }

  if (config.trialAccountId) {
    await createUser(config.trialAccountDatabase, config.trialAccountId);
  } else {
    log.debug(`trialAccountId not defined, skipping reset for Trial Account Database on ${process.env.MIGRATION_ENV}...`);
    process.exit();
  }

  if (config.trialExpiredAccountId) {
    await createUser(config.trialExpiredAccountDatabase, config.trialExpiredAccountId);
  } else {
    log.debug(`trialExpiredAccountId not defined, skipping reset for Trial Expired Account Database on ${process.env.MIGRATION_ENV}...`);
    process.exit();
  }

  if (config.freemiumAccountId) {
    await createUser(config.freemiumAccountDatabase, config.freemiumAccountId);
  } else {
    log.debug(`freemiumAccountId not defined, skipping reset for Freemium Account Database on ${process.env.MIGRATION_ENV}...`);
    process.exit();
  }
};

createUsers();
