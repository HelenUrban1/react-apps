// 'use strict';
const { ACME_CORP_ACCOUNT_ID, DEV_USER_ID } = require('../seedConfig');
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert(
      'subscriptions',
      [
        {
          id: '07d26541-9294-41a4-969e-eb5ec077a176',
          accountId: ACME_CORP_ACCOUNT_ID,
          billingId: DEV_USER_ID,
          companyId: '5788481594',
          activatedAt: '2020-05-04T13:44:32-04:00',
          providerProductId: 5233891,
          providerProductHandle: 'ix3_standard',
          providerFamilyHandle: 'inspectionxpert-cloud',
          automaticallyResumeAt: null,
          balanceInCents: 0, // Drawings
          billingPeriod: 'Annually',
          cancelAtEndOfPeriod: false,
          canceledAt: null,
          cancellationMessage: null,
          cancellationMethod: null,
          couponCode: null,
          // NOTE: Setting couponCodes to `[]` causes the Postgres error: "cannot determine type of empty array".
          //    There is a workaround in SubscriptionRepository that changes an empty array to `null`, but that
          //    does not affect seeding.
          // couponCodes: [],
          couponUseCount: null,
          couponUsesAllowed: null,
          createdAt: '2020-09-15T13:44:31-04:00',
          currentBillingAmountInCents: 0, // Drawings
          currentPeriodEndsAt: '2021-09-15T13:44:31-04:00',
          currentPeriodStartedAt: '2020-09-15T13:44:31-04:00',
          delayedCancelAt: null,
          expiresAt: '2021-09-15T13:44:31-04:00',
          netTerms: null,
          nextAssessmentAt: '2021-09-015T13:44:31-04:00',
          nextProductHandle: null,
          nextProductId: null,
          nextProductPricePointId: null,
          offerId: null,
          payerId: 37892060,
          paymentCollectionMethod: 'automatic',
          paymentSystemId: 37003269,
          paymentType: '25573100', // payment_profile_id
          previousStatus: 'active',
          productPriceInCents: 0, // Drawings
          paidUserSeatsUsed: 27,
          partsUsed: 0,
          measurementsUsed: 0,
          pendingComponents: '[]',
          drawingsUsed: 5, // Drawings
          components:
            '[{"component_id":1108426,"subscription_id":36961356,"component_handle":"drawings","allocated_quantity":5,"name":"Drawings","kind":"quantity_based_component","unit_name":"drawings","pricing_scheme":"stairstep","price_point_id":1067356,"price_point_handle":"standard"}]',
          productPricePointId: 914840,
          productVersionNumber: 1,
          reasonCode: null,
          receivesInvoiceEmails: null,
          referralCode: null,
          signupPaymentId: 421144154,
          signupRevenue: '0.00',
          snapDay: null,
          status: 'active',
          storedCredentialTransactionId: null,
          totalRevenueInCents: 0,
          trialEndedAt: null,
          trialStartedAt: null,
          createdById: DEV_USER_ID,
          updatedById: DEV_USER_ID,
          updatedAt: Sequelize.literal('NOW()'),
          cardId: 12345,
          maskedCardNumber: 'XXXX-XXXX-XXXX-3442',
          cardType: 'visa',
          expirationMonth: 9,
          expirationYear: 2021,
          customerId: '37892060',
          renewalDate: '2021-09-15T13:44:31-04:00',
          // pendingSeats: null,
          pendingTier: null,
          pendingBillingPeriod: null,
        },
      ],
      {}
    );
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('People', null, {});
    */
  },
};
