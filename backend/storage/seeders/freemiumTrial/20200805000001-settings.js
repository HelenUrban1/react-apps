// 'use strict';

// Settings in this file will be used before a user logs in.
// In most cases they will be overridden by Account, Site or User settings after log in.
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert(
      'settings',
      [
        {
          id: 'acafb538-6a9b-4419-afc7-181b33d1c650',
          name: 'theme',
          value: 'default',
          metadata: null,
          siteId: null,
          userId: null,
          createdAt: Sequelize.literal('NOW()'),
          updatedAt: Sequelize.literal('NOW()'),
        },
        {
          id: 'f7eca4c4-358d-4fef-bb9a-f699b84863ef',
          name: 'promptFont',
          value: 'false',
          metadata: null,
          siteId: null,
          userId: null,
          createdAt: Sequelize.literal('NOW()'),
          updatedAt: Sequelize.literal('NOW()'),
        },
      ],
      {}
    );
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('settings', null, {});
  },
};
