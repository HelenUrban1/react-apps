// 'use strict';

const { DefaultTolerances, CustomTolerances } = require('../../../src/staticData/defaults');

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert(
      'settings',
      [
        {
          id: 'd00738fe-bb02-41dc-b9d0-f1a506625610',
          name: 'theme',
          value: 'default',
          metadata: null,
          siteId: null,
          userId: null,
          createdAt: Sequelize.literal('NOW()'),
          updatedAt: Sequelize.literal('NOW()'),
        },
        {
          id: '8c60b355-9f7b-40ce-a03d-87fd4b0d6c86',
          name: 'promptFont',
          value: 'false',
          metadata: null,
          siteId: null,
          userId: null,
          createdAt: Sequelize.literal('NOW()'),
          updatedAt: Sequelize.literal('NOW()'),
        },
        {
          id: '9acfc012-9859-4364-a22b-5dd17637cca4',
          name: 'stylePresets',
          value: 'default',
          metadata:
            '{"Purple Diamond":{"customers":["480e0437-e26a-4a03-818d-8494b65a0bec"],"setting":{"1":{"style":"25750225-6d8b-4469-acad-a99c217fa8ba","assign":["Type","db687ac1-7bf8-4ada-be2b-979f8921e1a0","49d45a6c-1073-4c6e-805f-8f4105641bf1"]},"2":{"style":"3d3f1c44-d73e-456f-8f35-384a52cf7fb4","assign":["Classification","168eba1e-a8bf-41bb-a0d2-955e8a7b8d66"]},"default":{"style":"f12c5566-7612-48e9-9534-ef90b276ebaa","assign":["Default"]}}},"Fake Diamond":{"customers":["61d00076-0980-4f9e-b117-763fddc630d7"],"setting":{"1":{"style":"c623219f-4466-4be3-a95b-2bd8e9ffbea6","assign":["Type","db687ac1-7bf8-4ada-be2b-979f8921e1a0","4396f3c2-a915-4458-8cff-488b6d301ecd"]},"2":{"style":"5b48ed02-76cb-46c0-b017-da8060636817","assign":["Classification","168eba1e-a8bf-41bb-a0d2-955e8a7b8d66"]},"default":{"style":"2da1eab7-608a-4d8a-bfc3-fe874261b6f5","assign":["Default"]}}},"Default":{"customers":[],"setting":{"default":{"style":"f12c5566-7612-48e9-9534-ef90b276ebaa","assign":["Default"]}}}}',
          siteId: null,
          userId: null,
          createdAt: Sequelize.literal('NOW()'),
          updatedAt: Sequelize.literal('NOW()'),
        },
        {
          id: '6eeae1ce-6ac8-4408-ad48-df46f2f9a983',
          name: 'tolerancePresets',
          value: '',
          metadata: JSON.stringify({ Default: { customers: [], setting: DefaultTolerances }, Twos: { customers: ['480e0437-e26a-4a03-818d-8494b65a0bec'], setting: CustomTolerances } }),
          siteId: null,
          userId: null,
          createdAt: Sequelize.literal('NOW()'),
          updatedAt: Sequelize.literal('NOW()'),
        },
      ],
      {
        upsertKeys: ['id'],
        updateOnDuplicate: ['name', 'value', 'metadata', 'siteId', 'userId', 'createdAt', 'updatedAt'],
      }
    );
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('settings', null, {});
  },
};
