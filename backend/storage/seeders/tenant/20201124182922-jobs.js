// 'use strict';
const defaults = require('../../../src/staticData/defaults');

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert(
      'jobs',
      [
        {
          id: '82b7809c-ce80-46c5-9b9e-a82ee5eb2b67',
          accessControl: 'None',
          name: '867-5309',
          sampling: 'Timed',
          jobStatus: 'Active',
          interval: 30,
          samples: 5,
          passing: 2,
          scrapped: 1,
          createdAt: Sequelize.literal('NOW()'),
          updatedAt: Sequelize.literal('NOW()'),
        },
        {
          id: '219eb3ab-2e81-46a6-8fa3-74b67db3dfbe',
          accessControl: 'None',
          name: 'abcd-efg',
          sampling: 'Quantity',
          jobStatus: 'Inactive',
          interval: 30,
          samples: 99,
          passing: 80,
          scrapped: 10,
          createdAt: Sequelize.literal('NOW()'),
          updatedAt: Sequelize.literal('NOW()'),
        },
      ],
      {
        upsertKeys: ['id'],
        updateOnDuplicate: ['accessControl', 'name', 'sampling', 'jobStatus', 'interval', 'samples', 'passing', 'scrapped', 'createdAt', 'updatedAt'],
      }
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('jobs', null, {});
  },
};
