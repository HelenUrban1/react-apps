// 'use strict';
const { SERVICE_MONKEY_ACCOUNT_MEMBER_ID } = require('../seedConfig');
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert(
      'operators',
      [
        {
          id: '0ed5e900-9e38-4560-8af4-a0e3bc3a5fb0',
          fullName: 'Joe Guyman',
          accessControl: 'None',
          firstName: 'Joe',
          lastName: 'Guyman',
          email: null,
          status: 'Active',
          createdAt: Sequelize.literal('NOW()'),
          updatedAt: Sequelize.literal('NOW()'),
          createdById: SERVICE_MONKEY_ACCOUNT_MEMBER_ID,
          updatedById: SERVICE_MONKEY_ACCOUNT_MEMBER_ID,
        },
        {
          id: '24caf53a-43c1-4254-b8c4-69eb9e73fac3',
          fullName: 'Fred Friendfellow',
          accessControl: 'None',
          firstName: 'Fred',
          lastName: 'Friendfellow',
          email: null,
          status: 'Active',
          createdAt: Sequelize.literal('NOW()'),
          updatedAt: Sequelize.literal('NOW()'),
          createdById: SERVICE_MONKEY_ACCOUNT_MEMBER_ID,
          updatedById: SERVICE_MONKEY_ACCOUNT_MEMBER_ID,
        },
        {
          id: '0fa065d8-ce15-11eb-b8bc-0242ac130003',
          fullName: 'Alice Nicelady',
          accessControl: 'None',
          firstName: 'Alice',
          lastName: 'Nicelady',
          email: null,
          status: 'Active',
          createdAt: Sequelize.literal('NOW()'),
          updatedAt: Sequelize.literal('NOW()'),
          createdById: SERVICE_MONKEY_ACCOUNT_MEMBER_ID,
          updatedById: SERVICE_MONKEY_ACCOUNT_MEMBER_ID,
        },
      ],
      {
        upsertKeys: ['id'],
        updateOnDuplicate: ['fullName', 'firstName', 'lastName', 'accessControl', 'email', 'createdById', 'updatedById', 'createdAt', 'updatedAt'],
      }
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('operators', null, {});
  },
};
