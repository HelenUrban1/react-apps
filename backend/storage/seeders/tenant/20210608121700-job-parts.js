// 'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert(
      'jobParts',
      [
        {
          id: '1c4e651e-8cb3-45ec-9624-e833a4370542',
          jobId: '82b7809c-ce80-46c5-9b9e-a82ee5eb2b67',
          partId: 'c48e082a-ed09-4885-ae15-e45c4f58b628',
          createdAt: Sequelize.literal('NOW()'),
          updatedAt: Sequelize.literal('NOW()'),
        },
        {
          id: 'fd3a3cb1-4e8a-41e2-8d2b-2456dbc022fc',
          jobId: '82b7809c-ce80-46c5-9b9e-a82ee5eb2b67',
          partId: 'd6c5bc49-9fb2-4fbb-8a61-77bc8d28036f',
          createdAt: Sequelize.literal('NOW()'),
          updatedAt: Sequelize.literal('NOW()'),
        },
        {
          id: '72460a45-748b-406e-a291-3500e033223c',
          jobId: '219eb3ab-2e81-46a6-8fa3-74b67db3dfbe',
          partId: 'd6c5bc49-9fb2-4fbb-8a61-77bc8d28036f',
          createdAt: Sequelize.literal('NOW()'),
          updatedAt: Sequelize.literal('NOW()'),
        },
      ],
      {
        upsertKeys: ['id'],
        updateOnDuplicate: ['jobId', 'partId', 'createdAt', 'updatedAt'],
      }
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('jobParts', null, {});
  },
};
