// 'use strict';
const { SERVICE_MONKEY_ACCOUNT_MEMBER_ID, DEV_EMAIL, DEV_OWNER_FULL_NAME, DEV_ACCOUNT_FIRSTNAME, DEV_OWNER_LASTNAME } = require('../seedConfig');
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert(
      'operators',
      [
        {
          id: '52a45922-2b05-4a32-bd27-7ff319d30111',
          accessControl: 'None',
          email: DEV_EMAIL,
          fullName: DEV_OWNER_FULL_NAME,
          firstName: DEV_ACCOUNT_FIRSTNAME,
          lastName: DEV_OWNER_LASTNAME,
          createdAt: Sequelize.literal('NOW()'),
          updatedAt: Sequelize.literal('NOW()'),
          createdById: SERVICE_MONKEY_ACCOUNT_MEMBER_ID,
        },
      ],
      {
        upsertKeys: ['id'],
        updateOnDuplicate: ['accessControl', 'email', 'fullName', 'firstName', 'lastName', 'createdAt', 'updatedAt', 'createdById'],
      }
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('samples', null, {});
  },
};
