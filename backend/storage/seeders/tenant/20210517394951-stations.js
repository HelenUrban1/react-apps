// 'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert(
      'stations',
      [
        {
          id: '0ed5e900-9e38-4560-8af4-a0e3bc3a5fb0',
          name: 'Default',
          disabled: false,
          isMaster: true,
          createdAt: Sequelize.literal('NOW()'),
          updatedAt: Sequelize.literal('NOW()'),
        },
      ],
      {
        upsertKeys: ['id'],
        updateOnDuplicate: ['name', 'disabled', 'createdAt', 'updatedAt'],
      }
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('stations', null, {});
  },
};
