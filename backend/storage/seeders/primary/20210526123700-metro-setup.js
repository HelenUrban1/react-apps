const { Sequelize } = require('sequelize');
const { SERVICE_MONKEY_USER_ID, ACME_CORP_ACCOUNT_ID, SERVICE_MONKEY_ACCOUNT_MEMBER_ID, ACME_CORP_SITE_ID } = require('../seedConfig');

module.exports = {
  up: (queryInterface) => {
    return queryInterface.sequelize.query('SELECT * FROM "accounts"').then(async (result) => {
      if (result[0].length === 0) {
        return Promise.resolve();
      }
      await Promise.all(
        result[0].map((acct) => {
          const metro = true;
          const newSetting = JSON.stringify({ ...JSON.parse(acct.setting || '{}'), metro });
          return queryInterface.sequelize.query(`UPDATE accounts SET "settings" = '${newSetting}' WHERE "id" = '${acct.id}'`);
        })
      );

      await queryInterface.bulkInsert(
        'users',
        [
          {
            id: SERVICE_MONKEY_USER_ID,
            email: `service+${ACME_CORP_ACCOUNT_ID}@inspectionxpert.com`,
            emailVerified: 't',
            password: '$2b$12$rvwQlGp8yvE6ChdsmO1Vr.qkNFGEgdZRXsUAFOrFEzJGyVQn2A2SW', // 'test'
            disabled: 'f',
            authenticationUid: 'c7bf4eca-cd00-4d65-81a8-5d4da86ea824',
            activeAccountMemberId: SERVICE_MONKEY_ACCOUNT_MEMBER_ID,
            createdAt: Sequelize.literal('NOW()'),
            updatedAt: Sequelize.literal('NOW()'),
          },
        ],
        {
          upsertKeys: ['id'],
          updateOnDuplicate: [
            //
            'email',
            'emailVerified',
            'disabled',
            'authenticationUid',
            'activeAccountMemberId',
            'createdAt',
            'updatedAt',
          ],
        }
      );

      await queryInterface.bulkInsert(
        'accountMembers',
        [
          {
            id: SERVICE_MONKEY_ACCOUNT_MEMBER_ID,
            roles: Sequelize.literal(`ARRAY['Metro']::"enum_accountMembers_roles"[]`),
            settings: '{}',
            siteId: ACME_CORP_SITE_ID, // belongsTo
            status: 'Active',
            accessControl: false,
            userId: SERVICE_MONKEY_USER_ID,
            accountId: ACME_CORP_ACCOUNT_ID,
            createdById: SERVICE_MONKEY_USER_ID,
            updatedById: SERVICE_MONKEY_USER_ID,
            createdAt: Sequelize.literal('NOW()'),
            updatedAt: Sequelize.literal('NOW()'),
          },
        ],
        {
          upsertKeys: ['id'],
          updateOnDuplicate: [
            //
            'roles',
            'settings',
            'siteId',
            'status',
            'accessControl',
            'userId',
            'accountId',
            'createdById',
            'updatedById',
            'createdAt',
            'updatedAt',
          ],
        }
      );

      return Promise.resolve();
    });
  },

  down: (queryInterface) => {
    return Promise.resolve();
  },
};
