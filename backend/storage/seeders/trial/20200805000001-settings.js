// 'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert(
      'settings',
      [
        {
          id: 'd00738fe-bb02-41dc-b9d0-f1a506625610',
          name: 'theme',
          value: 'default',
          metadata: null,
          siteId: null,
          userId: null,
          createdAt: Sequelize.literal('NOW()'),
          updatedAt: Sequelize.literal('NOW()'),
        },
        {
          id: '8c60b355-9f7b-40ce-a03d-87fd4b0d6c86',
          name: 'promptFont',
          value: 'false',
          metadata: null,
          siteId: null,
          userId: null,
          createdAt: Sequelize.literal('NOW()'),
          updatedAt: Sequelize.literal('NOW()'),
        },
      ],
      {
        upsertKeys: ['id'],
        updateOnDuplicate: ['name', 'value', 'metadata', 'siteId', 'userId', 'createdAt', 'updatedAt'],
      }
    );
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('settings', null, {});
  },
};
