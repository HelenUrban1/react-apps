// 'use strict';

/*
const user = await User.findOne({
  where: {
    type: 'admin',
    email: 'sameermanek@hotmail.com'
  },
});
*/
const { REVIEWERS_USER_ID, REVIEWERS_TEST_USER_ID, OPS_REVIEWER_EMAIL } = require('../seedConfig');

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert(
      'userProfiles',
      [
        {
          id: '2ece10ae-1a1b-4399-b9c7-72f53bfac916',
          fullName: 'Review Owner',
          firstName: 'Review',
          lastName: 'Owner',
          phoneNumber: '123-456-7890',
          roles: Sequelize.literal(`ARRAY['Owner']::"enum_userProfiles_roles"[]`),
          userId: REVIEWERS_USER_ID,
          createdAt: Sequelize.literal('NOW()'),
          updatedAt: Sequelize.literal('NOW()'),
        },
        {
          id: 'ff617dbb-28d9-4ab2-bdb3-74b366705a7b',
          fullName: 'Reviewer Monkey',
          firstName: 'Review',
          lastName: 'Monkey',
          phoneNumber: '123-456-7890',
          roles: Sequelize.literal(`ARRAY['Reviewer']::"enum_userProfiles_roles"[]`),
          userId: REVIEWERS_TEST_USER_ID,
          createdAt: Sequelize.literal('NOW()'),
          updatedAt: Sequelize.literal('NOW()'),
        },
      ],
      {
        upsertKeys: ['id'],
        updateOnDuplicate: ['fullName', 'firstName', 'lastName', 'phoneNumber', 'roles', 'userId', 'createdById', 'updatedById', 'createdAt', 'updatedAt'],
      }
    );

    await queryInterface.bulkInsert(
      'sites',
      [
        {
          id: 'f57748fb-0e3f-4c69-9aeb-4408cb223340',
          status: 'Active',
          siteName: 'ReviewXpert',
          sitePhone: '987-654-3210',
          siteEmail: OPS_REVIEWER_EMAIL,
          siteStreet: '1 Glenwood Ave',
          siteStreet2: '',
          siteCity: 'Raleigh',
          siteRegion: 'North_Carolina',
          sitePostalcode: '27603',
          siteCountry: 'United_States',
          settings: '',
          createdById: REVIEWERS_USER_ID,
          updatedById: REVIEWERS_USER_ID,
          createdAt: Sequelize.literal('NOW()'),
          updatedAt: Sequelize.literal('NOW()'),
        },
      ],
      {
        upsertKeys: ['id'],
        updateOnDuplicate: ['status', 'siteName', 'sitePhone', 'siteEmail', 'siteStreet', 'siteStreet2', 'siteCity', 'siteRegion', 'sitePostalcode', 'siteCountry', 'settings', 'createdById', 'updatedById', 'createdAt', 'updatedAt'],
      }
    );

    await queryInterface.bulkInsert(
      'organizations',
      [
        {
          id: 'f6d518c9-4311-4adf-9a14-1f5fd4504f53',
          name: 'Reviewers Inc.',
          type: 'Account',
          settings: '',
          status: 'Active',
          createdAt: Sequelize.literal('NOW()'),
          updatedAt: Sequelize.literal('NOW()'),
        },
      ],
      {
        upsertKeys: ['id'],
        updateOnDuplicate: ['status', 'type', 'settings', 'name', 'createdAt', 'updatedAt'],
      }
    );
  },

  down: async (queryInterface, Sequelize) => {
    queryInterface.bulkDelete('sites', null, {});
    queryInterface.bulkDelete('organizations', null, {});
    return queryInterface.bulkDelete('userProfiles', null, {});
  },
};
