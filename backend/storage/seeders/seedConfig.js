// User ID's
exports.DEV_USER_ID = 'ad850692-26e6-11ee-be56-0242ac120002';
exports.DEV_ADMIN_USER_ID = 'df43f770-29fb-11ee-be56-0242ac120002';
exports.DEV_BILLING_USER_ID = '8e039102-2a02-11ee-be56-0242ac120002';
exports.DEV_COLLAB_USER_ID = 'e1551bcc-2a08-11ee-be56-0242ac120002';
exports.DEV_TRIAL_USER_ID = '6bbd860e-2a0a-11ee-be56-0242ac120002';
exports.DEV_TRIAL_EXPIRED_USER_ID = '72503d4c-2a0d-11ee-be56-0242ac120002';
exports.DEV_FREEMIUM_USER_ID = 'a5602fea-2a25-11ee-be56-0242ac120002';
exports.REVIEWERS_USER_ID = 'e78c1ff6-2ac4-11ee-be56-0242ac120002';
exports.REVIEWERS_TEST_USER_ID = '029c09bb-1f57-4adf-a1f3-bbc03491f065';
exports.SERVICE_MONKEY_USER_ID = '65529d3e-b4c0-4faa-9d1b-e226a74d2953';
exports.DEV_ACME_CORP_PENDING_USER_ID = '11bf70a2-2afc-11ee-be56-0242ac120002';
exports.DEV_ACME_CORP_INACTIVE_USER_ID = '11bf73c2-2afc-11ee-be56-0242ac120002';

// Account ID's
exports.ACME_CORP_ACCOUNT_ID = '27b06c74-306d-11ee-be56-0242ac120002';
exports.ACME_CORP_TRIAL_ACCOUNT_ID = '8bc54eee-2a0c-11ee-be56-0242ac120002';
exports.ACME_CORP_TRIAL_EXPIRED_ACCOUNT_ID = '72503ffe-2a0d-11ee-be56-0242ac120002';
exports.ACME_CORP_FREEMIUM_ACCOUNT_ID = '211cb734-2a35-11ee-be56-0242ac120002';
exports.ACME_CORP_SITE_ID = '31ef1d56-1b86-4ffa-b886-507ed8f021c5';
exports.REVIEWERS_ACCOUNT_ID = '31aa6662-2ac8-11ee-be56-0242ac120002';

// Account Member ID's
exports.DEV_ACME_CORP_TRIAL_ACCOUNT_MEMBER_ID = '49eac594-94d9-42b2-bbcf-cadcd7d3daef';
exports.DEV_ACME_CORP_ACCOUNT_MEMBER_ID = '007516cc-3168-40e4-8ad3-d42c92f66caa';
exports.DEV_ACME_CORP_ADMIN_ACCOUNT_MEMBER_ID = 'abc516cc-3168-40e4-8ad3-d42c92f66cba';
exports.DEV_ACME_CORP_BILLING_ACCOUNT_MEMBER_ID = '117516cc-3168-40e4-8ad3-d42c92f66cbb';
exports.DEV_ACME_CORP_COLLAB_ACCOUNT_MEMBER_ID = '227516cc-3168-40e4-8ad3-d42c92f66ccc';
exports.DEV_ACME_CORP_PENDING_ACCOUNT_MEMBER_ID = '447516cc-3168-40e4-8ad3-d42c92f66eee';
exports.DEV_ACME_CORP_INACTIVE_ACCOUNT_MEMBER_ID = '557516cc-3168-40e4-8ad3-d42c92f77123';
exports.DEV_ACME_CORP_TRIAL_EXPIRED_ACCOUNT_MEMBER_ID = 'aa3a2fc9-d243-443a-b549-c527636600d4';
exports.DEV_ACME_CORP_FREEMIUM_ACCOUNT_MEMBER_ID = 'ed21b270-3cb3-4195-9fe7-6bcec9a28ef9';
exports.REVIEWERS_ACCOUNT_MEMBER_ID = '227516cc-3a08-00e4-8ad3-d42c92f66ac1';
exports.REVIEWERS_TEST_ACCOUNT_MEMBER_ID = 'c3d93e90-a7b2-4432-a3a4-252f8461e63b';
exports.SERVICE_MONKEY_ACCOUNT_MEMBER_ID = '38b6e699-6483-4f27-b363-ff4d0ee32f61';

exports.PARTIAL_ID = '276aaba5-0ff9-4665-b545-8076e52819';

// Emails
exports.DEV_EMAIL = 'qualitycontrol.essentials@gmail.com';
exports.DEV_ADMIN_EMAIL = 'qualitycontrol.essentials+admin@gmail.com';
exports.DEV_BILLING_EMAIL = 'qualitycontrol.essentials+billing@gmail.com';
exports.DEV_TRIAL_EMAIL = 'qualitycontrol.essentials+trial@gmail.com';
exports.DEV_EXPIRED_EMAIL = 'qualitycontrol.essentials+expired@gmail.com';
exports.DEV_COLLAB_EMAIL = 'qualitycontrol.essentials+collab@gmail.com';
exports.DEV_PENDING_EMAIL = 'qualitycontrol.essentials+pending@gmail.com';
exports.DEV_INACTIVE_EMAIL = 'qualitycontrol.essentials+inactive@gmail.com';
exports.DEV_REVIEW_EMAIL = 'qualitycontrol.essentials+review@gmail.com';
exports.DEV_FREEMIUM_EMAIL = 'qualitycontrol.essentials+freemium@gmail.com';
exports.OPS_REVIEWER_EMAIL = 'ops+ixc.reviewers@inspectionxpert.com';

// Used to construct InspectionXpert testers names and email addresses
exports.QC_TESTERS_PREFIX = 'qualitycontroltesters+';
exports.QC_TESTERS_DOMAIN = '@gmail.com';

// InspectionXpert testers user ID's
exports.QC_TESTERS_USER_ID_1 = '2614adcc-b4cf-46bb-8ae5-5e4c834fdab4';
exports.QC_TESTERS_USER_ID_2 = '3d54df93-d33f-4edb-952b-9ce5f5120d44';
exports.QC_TESTERS_USER_ID_3 = 'f100e43e-cb09-464a-a5ce-539fb5531709';
exports.QC_TESTERS_USER_ID_4 = 'feb0c603-dcbb-451f-b9f3-6ec3317e0fcf';
exports.QC_TESTERS_USER_ID_5 = '236b877d-bc7d-4290-b7cd-6722fa5ae793';
exports.QC_TESTERS_USER_ID_6 = '34d140dc-294b-4c06-b44f-b1111bded2eb';
exports.QC_TESTERS_USER_ID_7 = 'bb951100-0990-42cc-9546-a93a9b5597ba';
exports.QC_TESTERS_USER_ID_8 = '12ca2916-8b96-4e43-8886-22291d45a57d';
exports.QC_TESTERS_USER_ID_9 = '76285e2f-4f9e-4760-9c42-a03c1d5dfb14';
exports.QC_TESTERS_USER_ID_10 = '5c2ea911-b673-45a8-8fac-9801d94155d3';
exports.QC_TESTERS_USER_ID_11 = 'e3f57db9-5d46-4bf4-8c64-52f2f2e3ed02';
exports.QC_TESTERS_USER_ID_12 = '8f9d3746-69d8-40a9-9ff4-ebc70d553e88';
exports.QC_TESTERS_USER_ID_13 = '04893d90-34f3-4a49-ace8-61142d3487a7';
exports.QC_TESTERS_USER_ID_14 = '518fc212-328a-4237-bc98-b672844a4a21';
exports.QC_TESTERS_USER_ID_15 = '5494dde9-20a8-4de1-b605-70614c9243c9';
exports.QC_TESTERS_USER_ID_16 = '7859766b-b58a-4dd9-8e15-1d950dab6ba5';
exports.QC_TESTERS_USER_ID_17 = '6db43218-e928-4fae-be05-f79fac05a9e8';
exports.QC_TESTERS_USER_ID_18 = '35cce32f-0518-4e07-af2a-61bfdf8965fe';
exports.QC_TESTERS_USER_ID_19 = 'beced13b-805f-4d7a-bb6a-bd1cee8036d9';
exports.QC_TESTERS_USER_ID_20 = 'c0091786-c8b0-41ee-8e74-d9fd84a35f30';
exports.QC_TESTERS_USER_ID_21 = '3046e6a9-fab1-4907-9515-486c5aa87d18';
exports.QC_TESTERS_USER_ID_22 = 'b20e6ce1-9d61-4467-ab70-91ed121e8ad8';
exports.QC_TESTERS_USER_ID_23 = 'af6afb38-c4a4-46ee-a5e4-9b261c4151e3';
exports.QC_TESTERS_USER_ID_24 = 'ec1925c1-6033-4612-adf0-8de6193b608c';
exports.QC_TESTERS_USER_ID_25 = '0784fa78-f755-470e-9b95-20c61948dee0';

exports.DEV_ACCOUNT_FIRSTNAME = 'Dev';

// Full Names
exports.DEV_OWNER_FULL_NAME = 'Dev Owner';
exports.DEV_COLLAB_FULL_NAME = 'Dev Collab';
exports.DEV_ADMIN_FULL_NAME = 'Dev Admin';
exports.DEV_PENDING_FULL_NAME = 'Dev Pending';
exports.DEV_BILLING_FULL_NAME = 'Dev Billing';

// Last Names
exports.DEV_OWNER_LASTNAME = 'Owner';
exports.DEV_COLLAB_LASTNAME = 'Collab';
exports.DEV_ADMIN_LASTNAME = 'Admin';
exports.DEV_PENDING_LASTNAME = 'Pending';
exports.DEV_BILLING_LASTNAME = 'Billing';
