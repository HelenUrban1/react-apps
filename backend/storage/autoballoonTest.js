#!/usr/bin/env node
/* eslint-disable import/no-extraneous-dependencies */

/**
 Used for testing part list page functions that show status and notifications for autoballooning.

 May expand to other functionality later to run autoballooning locally. For now, will simulate
 autoballoon service adding found capture counts as well as accepted, rejected and reviewed capture counts.
 
 The result while running with the part list showing is that the loading bars will fill up (without refreshing)
 and a notification will be created when it is complete.
 */
const axios = require('axios').default;
const { argv } = require('yargs/yargs')(process.argv.slice(2))
  .usage('Usage: $0 <command> [options]')
  .command('quick', 'Set foundCaptureCount, approvedCaptureCount and reviewedCaptureCount all to 10')
  .option('p', {
    alias: 'part',
    describe: 'partId to use',
    type: 'string',
    nargs: 1,
    demand: 'text is required',
    default: '0956a742-9466-48f6-b1f6-dfd72542fdef',
  })
  .option('q', {
    alias: 'quick',
    describe: 'Set foundCaptureCount, approvedCaptureCount and reviewedCaptureCount all to 10',
    type: 'boolean',
    default: false,
  })
  .option('r', {
    alias: 'review',
    describe: 'Add a new capture to the human review queue',
    type: 'boolean',
    default: false,
  })
  .alias('h', 'help')
  .help('help')
  .epilog('NOTE: This CLI will only run locally.');
const config = require('../config')();
const { log } = require('../src/logger');
const DrawingSheetService = require('../src/services/drawingSheetService');

axios.defaults.withCredentials = true;
const backendUrl = 'https://ixc-local.com:8080/api';

// THIS IS DANGEROUS, ONLY USED TO GET AROUND SELF-SIGNED CERT ERROR LOCALLY FOR TESTING
process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';

// NEVER run against anything other than local development server
if (config.isNotDevEnv) throw Error(`This script is only for local testing and cannot run against production environments.`);

const randomInteger = (min, max) => {
  return Math.floor(Math.random() * (max - min + 1)) + min;
};

const authUser = async (email, password) => {
  const headers = {
    'Content-Type': 'application/json; charset=utf-8',
  };

  // Log in as the admin user to get a bearer token
  const authRequestData = {
    query: 'mutation AUTH_SIGN_IN($email: String!, $password: String!) { authSignIn(email: $email, password: $password) }',
    variables: {
      email,
      password,
    },
  };
  const authResponse = await axios.post(backendUrl, JSON.stringify(authRequestData), { headers });
  return authResponse.data.data.authSignIn;
};

// Have to call the api endpoint so the event emitter sends websocket events to the frontend client.
const updateDrawingSheetCount = async (drawingSheet) => {
  try {
    const headers = {
      'Content-Type': 'application/json; charset=utf-8',
    };

    const authToken = await authUser('dev.mail.monkey@gmail.com', 'test');

    // Add bearer token to the header of the request
    headers.Authorization = `Bearer ${authToken}`;

    const drawingSheetInAccountInput = {
      pageIndex: drawingSheet.pageIndex,
      foundCaptureCount: drawingSheet.foundCaptureCount || 0,
      acceptedCaptureCount: drawingSheet.acceptedCaptureCount || 0,
      acceptedTimeoutCaptureCount: drawingSheet.acceptedTimeoutCaptureCount || 0,
      rejectedCaptureCount: drawingSheet.rejectedCaptureCount || 0,
      reviewedCaptureCount: drawingSheet.reviewedCaptureCount || 0,
      reviewedTimeoutCaptureCount: drawingSheet.reviewedTimeoutCaptureCount || 0,
      part: drawingSheet.part,
      drawing: drawingSheet.drawing,
      accountId: drawingSheet.accountId,
      siteId: drawingSheet.siteId,
    };

    const requestData = {
      query: 'mutation DRAWING_SHEET_INCREMENT_IN_ACCOUNT($data: DrawingSheetInAccountInput!) { drawingSheetIncrementInAccount(data: $data) { id } }',
      variables: { data: drawingSheetInAccountInput },
    };

    const response = await axios.post(backendUrl, JSON.stringify(requestData), { headers });
    return response.data.data.drawingSheetIncrementInAccount.id;
  } catch (error) {
    log.error(error);
    throw error;
  }
};

const addToQueue = async (review) => {
  try {
    const headers = {
      'Content-Type': 'application/json; charset=utf-8',
    };

    const authToken = await authUser('dev.mail.monkey@gmail.com', 'test');

    // Add bearer token to the header of the request
    headers.Authorization = `Bearer ${authToken}`;

    const ReviewTaskInput = [review];

    const requestData = {
      query: 'mutation REVIEW_TASK_ADD_TO_QUEUE($data: [ReviewTaskInput!]!) { reviewTaskAddToQueue(data: $data) }',
      variables: { data: ReviewTaskInput },
    };

    const response = await axios.post(backendUrl, JSON.stringify(requestData), { headers });
    return response.data;
  } catch (error) {
    log.error(error);
    throw error;
  }
};

// Main function
(async () => {
  log.info('starting');
  const { part, quick, review } = argv;

  const context = { currentUser: { accountId: config.testAccountId } };
  const drawingSheetService = new DrawingSheetService(context);

  const drawingSheets = await drawingSheetService.findAndCountAll({ filter: { part } });
  // log.info(drawingSheets);

  const drawingSheetUpdates = drawingSheets.rows.map((ds) => {
    return { ...ds, accountId: config.testAccountId, siteId: '31ef1d56-1b86-4ffa-b886-507ed8f021c5', part: ds.partId, drawing: ds.drawingId };
  });
  // log.info(drawingSheetUpdates);
  if (quick) {
    const drawingSheet = { ...drawingSheetUpdates[0], foundCaptureCount: 10, acceptedCaptureCount: 10, reviewedCaptureCount: 10 };
    await updateDrawingSheetCount(drawingSheet);
  } else if (review) {
    const review = {
      type: 'Capture',
      status: 'Waiting',
      metadata: JSON.stringify({ ocrText: '1.04±.02', correctedText: '', characteristicId: 'd880b839-7b1b-459f-aecd-97dbd0f59447' }),
      accountId: 'e0a01769-c8ed-4090-bd24-9c1b2241fb75',
      siteId: '31ef1d56-1b86-4ffa-b886-507ed8f021c5',
      partId: '0956a742-9466-48f6-b1f6-dfd72542fdef',
      drawingId: '113cde91-202b-4584-b0b4-1efa5e505790',
    };
    await addToQueue(review);
  } else {
    const balloonCounts = [];
    const incrementFoundCaptureCounts = [];
    for (let index = 0; index < drawingSheets.count; index++) {
      // each sheet will get a random number of captures between 10 and 20
      balloonCounts.push(randomInteger(10, 20));
      const drawingSheet = { ...drawingSheetUpdates[index], foundCaptureCount: balloonCounts[index], acceptedCaptureCount: 0, rejectedCaptureCount: 0, reviewedCaptureCount: 0 };
      incrementFoundCaptureCounts.push(updateDrawingSheetCount(drawingSheet));
    }
    await Promise.all(incrementFoundCaptureCounts);

    // Mark each capture as accepted or rejected
    // Keep track of how many are accepted so we can mark them as reviewed in next step
    const accepted = [];
    for (let index = 0; index < balloonCounts.length; index++) {
      accepted.push(0);
      for (let i = 0; i < balloonCounts[index]; i++) {
        const drawingSheet = { ...drawingSheetUpdates[index], foundCaptureCount: 0, acceptedCaptureCount: 0, rejectedCaptureCount: 0, reviewedCaptureCount: 0 };
        // randomly accept about 30% of captures
        if (randomInteger(1, 10) < 4) {
          drawingSheet.acceptedCaptureCount = 1;
          accepted[index] += 1;
        } else {
          drawingSheet.rejectedCaptureCount = 1;
        }
        // eslint-disable-next-line no-await-in-loop
        await updateDrawingSheetCount(drawingSheet);
      }
    }

    // Mark each accepted capture as reviewed
    for (let index = 0; index < balloonCounts.length; index++) {
      for (let i = 0; i < accepted[index]; i++) {
        const drawingSheet = { ...drawingSheetUpdates[index], foundCaptureCount: 0, acceptedCaptureCount: 0, rejectedCaptureCount: 0, reviewedCaptureCount: 1 };
        // eslint-disable-next-line no-await-in-loop
        await updateDrawingSheetCount(drawingSheet);
      }
    }
  }
})()
  .then(() => {
    log.debug(`Finished incrementing autoballoon counts, returned without errors.`);
  })
  .catch((error) => {
    log.error(`Error occurred while incrementing autoballoon counts: ${error.message}`);
    log.error(error?.stack);
  });
