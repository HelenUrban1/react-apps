const config = require('../config')();

module.exports = {
  dialect: 'postgres',
  host: config.reviewAccountDatabase.host,
  database: config.reviewAccountDatabase.database,
  username: config.reviewAccountDatabase.username,
  password: config.reviewAccountDatabase.password,
  logging: console.log,
};
