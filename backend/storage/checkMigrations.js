const fs = require('fs');
const path = require('path');
const { assumeRole } = require('aws-wrapper');
const { format, transports } = require('winston');
const Umzug = require('umzug');
const { Sequelize } = require('sequelize');
const AccountService = require('../src/services/accountService');
const { log } = require('../src/logger');
const config = require('../config')();

/**
 * Need to be in production mode, but want to use colors and non-json output for logging in console
 */
const configureLogging = () => {
  const loggingConfig = {
    type: 'console',
    level: 'debug',
    handleExceptions: true,
    format: format.colorize({
      all: true,
    }),
  };
  log.configure({
    transports: [new transports.Console(loggingConfig)],
  });
};

/**
 * If any migrations have not run, write them to the console
 */
const printDiff = (diff, accountId) => {
  if (diff.length > 0) {
    log.warn(`MISSING MIGRATIONS: ${accountId}`);
    log.warn(`${diff}\n`);
  } else {
    log.info(`Up to date: ${accountId}\n`);
  }
};

/**
 * Compare list of known migrations to the migrations that were run in a database
 */
const compareDifferences = (databaseMigrations, accountId, allMigrations) => {
  if (databaseMigrations) {
    const thisResult = databaseMigrations[0].map((r) => r.name);
    const diff = allMigrations.filter((n) => !thisResult.includes(n));
    printDiff(diff, accountId);
  } else {
    log.error(`Error getting migrations for ${accountId}\n`);
  }
};

/**
 * Returns a sequelize object for a database, username, password
 * @param {*} databaseName
 * @param {*} username
 * @param {*} password
 */
const getConnection = (databaseName, username, password) => {
  return new Sequelize(databaseName, username, password, {
    dialect: 'postgres',
    host: config.database.host,
    logging: log.trace.bind(log),
  });
};

/**
 * Get successful migrations from the tenant database
 * @param {*} accountId
 * @param {*} dbPassword
 */
const getTenantMigrations = async (accountId, databaseName, databasePassword) => {
  const tenant = getConnection(databaseName, `${config.accountDbPrefix}${process.env.NODE_ENV}_${accountId}`, databasePassword);
  return tenant.query('SELECT "name" FROM "SequelizeMeta";');
};

/**
 * Check input arguments for a tenant option
 * @param {*} args
 */
const parseTenantArgument = (args) => {
  let tenant = '';
  for (let index = 0; index < args.length; index++) {
    const arg = args[index];
    if (arg.startsWith('tenant=')) {
      tenant = arg.substr(7);
      break;
    } else if (arg === 'tenant') {
      tenant = 'all';
      break;
    }
  }
  return tenant;
};

/**
 * Extract's the migration to roll back to from the command line arguments.
 * @param {string[]} args The command line arguments
 */
const parseUndoArgument = (args) => {
  let undoTo = '';
  for (let index = 0; index < args.length; index++) {
    const arg = args[index];
    if (arg.startsWith('to=')) {
      undoTo = arg.split('=')[1];
      break;
    }
  }
  if (!undoTo) {
    throw new Error('No undo argument passed, add `to=` to roll back migrations');
  }
  return undoTo;
};

/**
 * Check input options for a MFA token value
 * @param {*} args
 */
const parseTokenArgument = (args) => {
  let token = '';
  for (let index = 0; index < args.length; index++) {
    const arg = args[index];
    if (arg.startsWith('token=')) {
      token = arg.substr(6);
    }
  }
  return token;
};

/**
 * Handles checking for migrations that have not run
 * @param {*} args
 */
const checkMigrations = async (args) => {
  try {
    log.debug('Checking Migrations...\n');

    const tenant = parseTenantArgument(args);

    const allMigrations = fs.readdirSync(path.resolve(__dirname, './migrations')).filter((m) => m.endsWith('.js'));

    if (args.includes('all') || args.includes('primary')) {
      try {
        const primary = getConnection(config.database.database, config.database.username, config.database.password);
        const primaryMigrations = await primary.query('SELECT "name" FROM "SequelizeMeta";');
        compareDifferences(primaryMigrations, 'PRIMARY', allMigrations);
      } catch (error) {
        log.error(`Error checking Primary`);
        log.error(`${error}\n`);
      }
    }

    if (args.includes('all') || tenant) {
      const accounts = await new AccountService({}).findAndCountAll();
      log.info(`Found ${accounts.rows.length} accounts\n`);
      const { username, sourceAccountId, targetAccountId, targetRole } = config.awsAssumeRole;
      const roleArn = `arn:aws:iam::${targetAccountId}:role/${targetRole}`;
      const tokenCode = parseTokenArgument(args);
      if (tokenCode || process.env.NODE_ENV === 'development') {
        const { secretsManager } = await assumeRole(username, sourceAccountId, roleArn, tokenCode);

        for (let index = 0; index < accounts.rows.length; index++) {
          const account = accounts.rows[index];
          if (args.includes('all') || tenant === 'all' || tenant === account.id) {
            try {
              // Resolving each promise in the loop since this is an admin tool and we want each success and failure
              // eslint-disable-next-line no-await-in-loop
              const accountDbPassword = await secretsManager.getDatabasePassword(account.id);
              // eslint-disable-next-line no-await-in-loop
              const accountMigrations = await getTenantMigrations(account.id, account.dbName, accountDbPassword);
              compareDifferences(accountMigrations, account.id, allMigrations);
            } catch (error) {
              log.error(`Error checking account ${account.id}`);
              log.error(`${error}\n`);
            }
          }
        }
      } else {
        log.error('Must add token argument to query tenant databases.');
      }
    }
  } catch (error) {
    log.error(error);
  }
};

/**
 * Outputs a list of migrations that ran
 * @param {*} migrations
 * @param {*} accountId
 * @param {*} up
 */
const printMigrations = (migrations, accountId, up) => {
  log.info(`Migrated ${up ? 'up' : 'down'} ${accountId}`);
  if (migrations.length === 0) {
    log.info(`No migrations to run`);
  } else {
    log.info(migrations.map((m) => m.file));
  }
};

/**
 * Creates a sequelize connection and runs migrations against it.
 * @param {*} databaseName
 * @param {*} username
 * @param {*} password
 * @param {*} accountId
 * @param {*} downAndUp If true, will first undo migrations to '20210406171301-partsUsed' and then run all migrations, otherwise only un-run migrations will be run
 */
const runMigrationsForDatabase = async (databaseName, username, password, accountId, downAndUp) => {
  log.debug(`Running on ${accountId} ${databaseName}`);
  const primary = getConnection(databaseName, username, password);
  const umzug = new Umzug({
    migrations: {
      path: path.join(__dirname, './migrations'),
      params: [primary.getQueryInterface()],
    },
    storage: 'sequelize',
    storageOptions: {
      sequelize: primary,
    },
  });

  if (downAndUp) {
    log.debug('Undoing Migrations...');
    printMigrations(await umzug.down({ to: '20210406171301-partsUsed' }), accountId, false);
  }

  log.debug('Running Migrations...');
  printMigrations(await umzug.up(), accountId, true);
};

/**
 * Rolls back migrations to a specific migration
 * @param {string} databaseName
 * @param {string} username
 * @param {string} password
 * @param {string} accountId
 * @param {string} undoTo The migration to undo to, e.g. 20230608111332-update-notes-to-use-na-in-enum
 */
const undoMigrationsForDatabase = async (databaseName, username, password, accountId, undoTo) => {
  log.debug(`Undoing migrations for ${accountId} ${databaseName} to ${undoTo}`);
  const primary = getConnection(databaseName, username, password);
  const umzug = new Umzug({
    migrations: {
      path: path.join(__dirname, './migrations'),
      params: [primary.getQueryInterface()],
    },
    storage: 'sequelize',
    storageOptions: {
      sequelize: primary,
    },
  });

  printMigrations(await umzug.down({ to: undoTo }), accountId, false);
};

/**
 * Attempts to run any un-run migrations
 * @param {*} args
 */
const runMigrations = async (args) => {
  try {
    log.debug('Running Migrations...\n');

    const tenant = parseTenantArgument(args);

    if (args.includes('all') || args.includes('primary')) {
      log.debug('Running on Primary');

      try {
        await runMigrationsForDatabase(config.database.database, config.database.username, config.database.password, 'Primary', false);
      } catch (error) {
        log.error(`Error migrating Primary`);
        log.error(`${error}\n`);
      }
    }

    if (args.includes('all') || tenant) {
      const accounts = await new AccountService({}).findAndCountAll();
      log.info(`Found ${accounts.rows.length} accounts\n`);

      const { username, sourceAccountId, targetAccountId, targetRole } = config.awsAssumeRole;
      const roleArn = `arn:aws:iam::${targetAccountId}:role/${targetRole}`;
      const tokenCode = parseTokenArgument(args);
      if (tokenCode || process.env.NODE_ENV === 'development') {
        const { secretsManager } = await assumeRole(username, sourceAccountId, roleArn, tokenCode);

        for (let index = 0; index < accounts.rows.length; index++) {
          const account = accounts.rows[index];
          if (args.includes('all') || tenant === 'all' || tenant === account.id) {
            try {
              // Resolving each promise in the loop since this is an admin tool and we want each success and failure
              // eslint-disable-next-line no-await-in-loop
              const accountDbPassword = await secretsManager.getDatabasePassword(account.id);

              // eslint-disable-next-line no-await-in-loop
              await runMigrationsForDatabase(account.dbName, `${config.accountDbPrefix}${process.env.NODE_ENV}_${account.id}`, accountDbPassword, account.id, false);
            } catch (error) {
              log.error(`Error migrating account ${account.id}`);
              log.error(`${error}\n`);
            }
          }
        }
      } else {
        log.error('Must add token argument to query tenant databases.');
      }
    }
  } catch (error) {
    log.error(error);
  }
};

/**
 * Attempts undo migrations to a specific migration.
 * @param {string[]} args
 */
const undoMigrations = async (args) => {
  const tenant = parseTenantArgument(args);
  const undoTo = parseUndoArgument(args);

  if (args.includes('all') || args.includes('primary')) {
    try {
      await undoMigrationsForDatabase(config.database.database, config.database.username, config.database.password, 'Primary', undoTo);
    } catch (error) {
      log.error(`Error migrating Primary: `, error);
      throw error;
    }
  }

  if (args.includes('all') || tenant) {
    const accounts = await new AccountService({}).findAndCountAll();
    log.info(`Found ${accounts.rows.length} accounts`);

    const { username, sourceAccountId, targetAccountId, targetRole } = config.awsAssumeRole;
    const roleArn = `arn:aws:iam::${targetAccountId}:role/${targetRole}`;
    const tokenCode = parseTokenArgument(args);
    if (tokenCode || process.env.NODE_ENV === 'development') {
      const { secretsManager } = await assumeRole(username, sourceAccountId, roleArn, tokenCode);

      for (let index = 0; index < accounts.rows.length; index++) {
        const account = accounts.rows[index];
        if (args.includes('all') || tenant === 'all' || tenant === account.id) {
          try {
            // Resolving each promise in the loop since this is an admin tool and we want each success and failure
            // eslint-disable-next-line no-await-in-loop
            const accountDbPassword = await secretsManager.getDatabasePassword(account.id);

            // eslint-disable-next-line no-await-in-loop
            await undoMigrationsForDatabase(account.dbName, `${config.accountDbPrefix}${process.env.NODE_ENV}_${account.id}`, accountDbPassword, account.id, undoTo);
          } catch (error) {
            log.error(`Error migrating account ${account.id}`, error);
          }
        }
      }
    } else {
      log.error('Must add token argument to query tenant databases.');
    }
  }
};

/**
 * Undoes migrations to 20210105104815-review-tasks and then runs them again
 * @param {*} args
 */
const testMigrations = async (args) => {
  try {
    if (process.env.NODE_ENV !== 'development') {
      log.error('Test is destructive and can only run against development.');
    } else if (args.includes('all')) {
      log.error('Test can only run against a single database, pass primary or tenant=[accountId] to test');
    } else {
      log.debug('Testing Migrations...\n');

      if (args.includes('primary')) {
        try {
          await runMigrationsForDatabase(config.database.database, config.database.username, config.database.password, 'Primary', true);
        } catch (error) {
          log.error(`Error migrating Primary`);
          log.error(`${error}\n`);
        }
      }

      const tenant = parseTenantArgument(args);
      if (tenant) {
        if (tenant === 'all') {
          log.error('Must enter parameter tenant=[accountId] to test migrations against a tenant.');
        } else {
          const accounts = await new AccountService({}).findAndCountAll();
          log.info(`Found ${accounts.rows.length} accounts\n`);
          const { username, sourceAccountId, targetAccountId, targetRole } = config.awsAssumeRole;
          const roleArn = `arn:aws:iam::${targetAccountId}:role/${targetRole}`;
          const tokenCode = parseTokenArgument(args);
          if (tokenCode || process.env.NODE_ENV === 'development') {
            const { secretsManager } = await assumeRole(username, sourceAccountId, roleArn, tokenCode);

            for (let index = 0; index < accounts.rows.length; index++) {
              const account = accounts.rows[index];
              if (tenant === account.id) {
                try {
                  // Resolving each promise in the loop since this is an admin tool and we want each success and failure
                  // eslint-disable-next-line no-await-in-loop
                  const accountDbPassword = await secretsManager.getDatabasePassword(account.id);

                  // eslint-disable-next-line no-await-in-loop
                  await runMigrationsForDatabase(account.dbName, `${config.accountDbPrefix}${process.env.NODE_ENV}_${account.id}`, accountDbPassword, account.id, true);
                } catch (error) {
                  log.error(`Error migrating account ${account.id}`);
                  log.error(`${error}\n`);
                }
              }
            }
          } else {
            log.error('Must add token argument to query tenant databases.');
          }
        }
      }
    }
  } catch (error) {
    log.error(error);
  }
};

const main = async () => {
  try {
    configureLogging();

    const args = process.argv.slice(2);
    if (!args || args.length === 0 || args.includes('help') || (!args.includes('check') && !args.includes('run') && !args.includes('undo') && !args.includes('test'))) {
      log.info('MAIN OPTIONS');
      log.info('"help"\t\tThis message.');
      log.info('"check"\t\tChecks databases and shows which migrations have not run.');
      log.info('"run"\t\tAttempts to run migrations against databases.');
      log.info('"undo"\t\tAttempts to undo migrations against databases.');
      log.info('"test"\t\tAttempts to test migrations against a database, can only run when environment is development.');
      log.info('MODIFIERS FOR "check" AND "run"');
      log.info('"all"\t\tPrimary and all tenant DBs.');
      log.info('"tenant"\t\tAll tenant DBs.');
      log.info('"token=[123456]"\tMFA token for getting tenant DB passwords, required if using all or tenant modifiers.');
      log.info('MODIFIERS FOR "check", "run" AND "test"');
      log.info('"primary"\t\tPrimary DB.');
      log.info('"tenant=[accountId]"\tSpecific tenant DB.');
    } else if (args.includes('check')) {
      await checkMigrations(args);
    } else if (args.includes('run')) {
      await runMigrations(args);
    } else if (args.includes('test')) {
      await testMigrations(args);
    } else if (args.includes('undo')) {
      await undoMigrations(args);
    }
  } catch (error) {
    log.error(error);
  } finally {
    process.exit();
  }
};

main();
