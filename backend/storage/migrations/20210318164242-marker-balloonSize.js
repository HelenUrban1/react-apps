const { Sequelize } = require('sequelize');

module.exports = {
  up: (queryInterface) => {
    return queryInterface.describeTable('markers').then((tableDefinition) => {
      if (tableDefinition.balloonSize) {
        return Promise.resolve();
      }
      return queryInterface.addColumn('markers', 'balloonSize', { type: Sequelize.DataTypes.INTEGER, allowNull: true, defaultValue: 20 }).then((results) => queryInterface.sequelize.query(`UPDATE markers SET "balloonSize" = 20 WHERE "balloonSize" IS NULL`));
    });
  },
  down: (queryInterface) => {
    return queryInterface.describeTable('markers').then((tableDefinition) => {
      if (!tableDefinition.balloonSize) {
        return Promise.resolve();
      }
      return queryInterface.removeColumn('markers', 'balloonSize');
    });
  },
};
