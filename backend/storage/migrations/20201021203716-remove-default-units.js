const { Sequelize } = require('sequelize');

module.exports = {
  up: (queryInterface) => {
    return queryInterface.describeTable('parts').then((tableDefinition) => {
      if (!tableDefinition.defaultLinearUnit) {
        return Promise.resolve();
      }
      return queryInterface.sequelize.transaction((t) => {
        return Promise.all([
          queryInterface.removeColumn('parts', 'defaultLinearUnit', { transaction: t }), //
          queryInterface.removeColumn('parts', 'defaultAngularUnit', { transaction: t }),
        ]);
      });
    });
  },

  down: (queryInterface) => {
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
        queryInterface.addColumn(
          'parts',
          'defaultLinearUnit',
          {
            type: Sequelize.DataTypes.TEXT,
            allowNull: true,
          },
          { transaction: t }
        ),
        queryInterface.addColumn(
          'parts',
          'defaultAngularUnit',
          {
            type: Sequelize.DataTypes.TEXT,
            allowNull: true,
          },
          { transaction: t }
        ),
      ]);
    });
  },
};
