const { Sequelize } = require('sequelize');

module.exports = {
  up: (queryInterface) => {
    return queryInterface.describeTable('drawingSheets').then((tableDefinition) => {
      if (tableDefinition.useGrid) {
        return Promise.resolve();
      }
      return queryInterface.addColumn('drawingSheets', 'useGrid', { type: Sequelize.DataTypes.BOOLEAN, allowNull: false, defaultValue: true });
    });
  },
  down: (queryInterface) => {
    return queryInterface.describeTable('drawingSheets').then((tableDefinition) => {
      if (!tableDefinition.useGrid) {
        return Promise.resolve();
      }
      return queryInterface.removeColumn('drawingSheets', 'useGrid');
    });
  },
};
