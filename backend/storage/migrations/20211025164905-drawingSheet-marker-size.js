const { Sequelize } = require('sequelize');

module.exports = {
  up: (queryInterface) => {
    return queryInterface.describeTable('drawingSheets').then((tableDefinition) => {
      if (tableDefinition.markerSize) {
        return Promise.resolve();
      }
      return queryInterface.addColumn('drawingSheets', 'markerSize', { type: Sequelize.DataTypes.INTEGER, allowNull: true, defaultValue: null });
    });
  },
  down: (queryInterface) => {
    return queryInterface.describeTable('drawingSheets').then((tableDefinition) => {
      if (!tableDefinition.markerSize) {
        return Promise.resolve();
      }
      return queryInterface.removeColumn('drawingSheets', 'markerSize');
    });
  },
};
