const { Sequelize } = require('sequelize');

module.exports = {
  up: (queryInterface) => {
    return queryInterface.describeTable('drawingSheets').then(async (tableDefinition) => {
      if (tableDefinition.foundCaptureCount) {
        return Promise.resolve();
      }
      // if error, then table doesn't exist so we create it
      const transaction = await queryInterface.sequelize.transaction();

      try {
        await Promise.all([
          queryInterface.addColumn('drawingSheets', 'foundCaptureCount', {
            type: Sequelize.DataTypes.INTEGER,
            allowNull: false,
            defaultValue: 0,
          }),
          queryInterface.addColumn('drawingSheets', 'acceptedCaptureCount', {
            type: Sequelize.DataTypes.INTEGER,
            allowNull: false,
            defaultValue: 0,
          }),
          queryInterface.addColumn('drawingSheets', 'rejectedCaptureCount', {
            type: Sequelize.DataTypes.INTEGER,
            allowNull: false,
            defaultValue: 0,
          }),
          queryInterface.addColumn('drawingSheets', 'acceptedTimeoutCaptureCount', {
            type: Sequelize.DataTypes.INTEGER,
            allowNull: false,
            defaultValue: 0,
          }),
          queryInterface.addColumn('drawingSheets', 'reviewedCaptureCount', {
            type: Sequelize.DataTypes.INTEGER,
            allowNull: false,
            defaultValue: 0,
          }),
          queryInterface.addColumn('drawingSheets', 'reviewedTimeoutCaptureCount', {
            type: Sequelize.DataTypes.INTEGER,
            allowNull: false,
            defaultValue: 0,
          }),
        ]);

        await transaction.commit();
      } catch (err) {
        await transaction.rollback();
        throw err;
      }

      return Promise.resolve();
    });
  },
  down: (queryInterface) => {
    return queryInterface.describeTable('stations').then(async (tableDefinition) => {
      if (!tableDefinition.foundCaptureCount) {
        return Promise.resolve();
      }
      return Promise.all([
        queryInterface.removeColumn('drawingSheets', 'foundCaptureCount'),
        queryInterface.removeColumn('drawingSheets', 'acceptedCaptureCount'),
        queryInterface.removeColumn('drawingSheets', 'rejectedCaptureCount'),
        queryInterface.removeColumn('drawingSheets', 'acceptedTimeoutCaptureCount'),
        queryInterface.removeColumn('drawingSheets', 'reviewedCaptureCount'),
        queryInterface.removeColumn('drawingSheets', 'reviewedTimeoutCaptureCount'),
      ]);
    });
  },
};
