const { Sequelize } = require('sequelize');

module.exports = {
  up: (queryInterface) => {
    return queryInterface.describeTable('operators').then(async (tableDefinition) => {
      if (tableDefinition.pin) {
        return Promise.resolve();
      }

      try {
        await queryInterface.addColumn('operators', 'pin', {
          type: Sequelize.DataTypes.STRING,
          allowNull: true,
        });
      } catch (err) {
        throw err;
      }
    });
  },
  down: (queryInterface) => {
    return queryInterface.describeTable('operators').then(async (tableDefinition) => {
      if (!tableDefinition.status) {
        return Promise.resolve();
      }
      return queryInterface.removeColumn('operators', 'pin');
    });
  },
};
