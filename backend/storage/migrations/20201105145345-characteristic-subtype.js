const { Sequelize } = require('sequelize');
const { defaultSubtypeIDs } = require('../../src/staticData/defaults');

module.exports = {
  up: (queryInterface) => {
    return queryInterface.describeTable('characteristics').then((tableDefinition) => {
      if (tableDefinition.notationSubtype.type === Sequelize.DataTypes.TEXT) {
        return Promise.resolve();
      }
      return queryInterface
        .addColumn('characteristics', 'tempSubtype', { type: Sequelize.DataTypes.TEXT, defaultValue: '805fd539-8063-4158-8361-509e10a5d371', allowNull: false })
        .then((results) => queryInterface.sequelize.query('UPDATE characteristics SET "tempSubtype" = "notationSubtype"'))
        .then((results) => queryInterface.removeColumn('characteristics', 'notationSubtype'))
        .then((results) => queryInterface.renameColumn('characteristics', 'tempSubtype', 'notationSubtype'))
        .then((results) =>
          Promise.all(
            Object.keys(defaultSubtypeIDs).map((subtype) => {
              return queryInterface.sequelize.query(`UPDATE characteristics SET "notationSubtype"='${defaultSubtypeIDs[subtype]}' WHERE "notationSubtype"='${subtype.replace(' ', '_')}'`);
            })
          )
        );
    });
  },
  down: (queryInterface) => {
    return queryInterface.describeTable('characteristics').then((tableDefinition) => {
      if (tableDefinition.notationSubtype.type === Sequelize.DataTypes.ENUM) {
        return Promise.resolve();
      }
      return queryInterface
        .addColumn('characteristics', 'tempSubtype', {
          type: Sequelize.DataTypes.ENUM,
          defaultValue: 'Note',
          allowNull: false,
          values: [
            'Length',
            'Angle',
            'Radius',
            'Diameter',
            'Curvilinear',
            'Chamfer_Size',
            'Chamfer_Angle',
            'Bend_Radius',
            'Counterbore_Depth',
            'Counterbore_Diameter',
            'Counterbore_Angle',
            'Countersink_Diameter',
            'Countersink_Angle',
            'Depth',
            'Edge_Radius',
            'Thickness',
            'Thread',
            'Edge_Burr_Size',
            'Edge_Undercut_Size',
            'Edge_Passing_Size',
            'Profile_of_a_Surface',
            'Position',
            'Flatness',
            'Perpendicularity',
            'Angularity',
            'Circularity',
            'Runout',
            'Total_Runout',
            'Profile_of_a_Line',
            'Cylindricity',
            'Concentricity',
            'Symmetry',
            'Parallelism',
            'Straightness',
            'Roughness',
            'Lay',
            'Waviness',
            'Structure',
            'Material_Removal_Allowance',
            'Size',
            'Convexity',
            'Reinforcement',
            'Additional_Characteristic',
            'Bead_Contour',
            'Depth_of_Bevel',
            'Penetration',
            'Location',
            'Pitch',
            'Root_Opening',
            'Note',
            'Flag_Note',
            'Temperature',
            'Time',
            'Torque',
            'Voltage',
            'Temper_Conductivity',
            'Temper_Verification',
            'Sound',
            'Frequency',
            'Capacitance',
            'Electric_Current',
            'Resistance',
            'Electric_Charge',
            'Inductance',
            'Apparent_Power',
            'Reactive_Power',
            'Electric_Power',
            'Conductance',
            'Admittance',
            'Electric_Flux',
            'Magnetic_Field',
            'Magnetic_Flux',
            'Area',
            'Resistivity',
            'Conductivity',
            'Electric_Field',
            'Datum_Target',
            'Production_Method',
            'Annotation_Table',
            'Table',
            'Cell',
            'Callout',
            'Datum',
            'Balloon',
          ],
        })
        .then((results) => queryInterface.sequelize.query('UPDATE characteristics SET "tempSubtype" = "notationSubtype"'))
        .then((results) => queryInterface.removeColumn('characteristics', 'notationSubtype'))
        .then((results) => queryInterface.renameColumn('characteristics', 'tempSubtype', 'notationSubtype'))
        .then((results) =>
          Promise.all(
            Object.keys(defaultSubtypeIDs).map((subtype) => {
              return queryInterface.sequelize.query(`UPDATE characteristics SET "notationSubtype"='${subtype.replace(' ', '_')}' WHERE "notationSubtype"='${defaultSubtypeIDs[subtype]}'`);
            })
          )
        );
    });
  },
};
