const { Sequelize } = require('sequelize');

module.exports = {
  up: (queryInterface) => {
    return queryInterface.describeTable('characteristics').then((tableDefinition) => {
      if (tableDefinition.connectionPointLocationX || tableDefinition.connectionPointLocationY) {
        return Promise.resolve();
      }
      return queryInterface.sequelize.transaction((t) => {
        return Promise.all([
          queryInterface.addColumn(
            'characteristics',
            'connectionPointLocationX',
            {
              type: Sequelize.DataTypes.DECIMAL,
              allowNull: false,
              defaultValue: 0,
            },
            { transaction: t }
          ),
          queryInterface.addColumn(
            'characteristics',
            'connectionPointLocationY',
            {
              type: Sequelize.DataTypes.DECIMAL,
              allowNull: false,
              defaultValue: 0,
            },
            { transaction: t }
          ),
          queryInterface.addColumn(
            'characteristics',
            'connectionPointIsFloating',
            {
              type: Sequelize.DataTypes.BOOLEAN,
            },
            { transaction: t }
          ),
          queryInterface.addColumn(
            'characteristics',
            'displayLeaderLine',
            {
              type: Sequelize.DataTypes.BOOLEAN,
              defaultValue: true,
            },
            { transaction: t }
          ),
        ]);
      });
    });
  },

  down: (queryInterface) => {
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([queryInterface.removeColumn('characteristics', 'connectionPointLocationX', { transaction: t }), queryInterface.removeColumn('characteristics', 'connectionPointLocationY', { transaction: t }), queryInterface.removeColumn('characteristics', 'connectionPointIsFloating', { transaction: t }), queryInterface.removeColumn('characteristics', 'displayLeaderLine', { transaction: t })]);
    });
  },
};
