const { Sequelize } = require('sequelize');

module.exports = {
  up: (queryInterface) => {
    return queryInterface.describeTable('characteristics').then((tableDefinition) => {
      if (tableDefinition.markerRotation || tableDefinition.leaderLineDistance) {
        return Promise.resolve();
      }
      return queryInterface.sequelize.transaction((t) => {
        return Promise.all([
          //
          queryInterface.addColumn('characteristics', 'leaderLineDistance', { type: Sequelize.DataTypes.DECIMAL, defaultValue: 0 }, { transaction: t }),
          queryInterface.addColumn('characteristics', 'markerRotation', { type: Sequelize.DataTypes.INTEGER, defaultValue: 0 }, { transaction: t }),
        ]);
      });
    });
  },

  down: (queryInterface) => {
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([queryInterface.removeColumn('characteristics', 'markerRotation', { transaction: t }), queryInterface.removeColumn('characteristics', 'leaderLineDistance', { transaction: t })]);
    });
  },
};
