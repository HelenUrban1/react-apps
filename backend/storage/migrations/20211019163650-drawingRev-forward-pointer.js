const { Sequelize, QueryTypes } = require('sequelize');

module.exports = {
  up: (queryInterface) => {
    return queryInterface.describeTable('drawings').then(async (tableDefinition) => {
      if (!tableDefinition.nextRevision) {
        await queryInterface.sequelize.transaction((t) => {
          return Promise.all([
            queryInterface.addColumn(
              'drawings',
              'nextRevision',
              {
                type: Sequelize.DataTypes.UUID,
                allowNull: true,
                references: {
                  model: 'drawings',
                  key: 'id',
                },
              },
              { transaction: t }
            ),
          ]);
        });
      }
      return queryInterface.sequelize
        .query('SELECT * FROM "drawings" WHERE "nextRevision" IS NULL', {
          type: QueryTypes.SELECT,
        })
        .then((result) => {
          if (result.length === 0) {
            return Promise.resolve();
          }
          return Promise.all(
            result.map((p) => {
              const { id } = p;
              return queryInterface.sequelize
                .query('SELECT * FROM "drawings" WHERE "previousRevision" = :id', {
                  type: QueryTypes.SELECT,
                  replacements: {
                    id,
                  },
                })
                .then((res) => {
                  if (res.length === 0) {
                    return Promise.resolve();
                  }
                  const nextRevId = res[0].id;
                  return queryInterface.sequelize.query('UPDATE "drawings" SET "nextRevision" = :nextRevId WHERE "id" = :id', {
                    replacements: {
                      nextRevId,
                      id,
                    },
                    type: QueryTypes.UPDATE,
                  });
                });
            })
          );
        });
    });
  },
  down: (queryInterface) => {
    return Promise.all([queryInterface.removeColumn('drawings', 'nextRevision')]);
  },
};
