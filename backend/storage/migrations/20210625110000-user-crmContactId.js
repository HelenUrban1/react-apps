const { Sequelize } = require('sequelize');

module.exports = {
  up: (queryInterface) => {
    return queryInterface.describeTable('users').then((tableDefinition) => {
      if (tableDefinition.crmContactId) {
        return Promise.resolve();
      }
      return queryInterface.addColumn('users', 'crmContactId', { type: Sequelize.DataTypes.TEXT, allowNull: true });
    });
  },
  down: (queryInterface) => {
    return queryInterface.describeTable('users').then((tableDefinition) => {
      if (!tableDefinition.crmContactId) {
        return Promise.resolve();
      }
      return queryInterface.removeColumn('users', 'crmContactId');
    });
  },
};
