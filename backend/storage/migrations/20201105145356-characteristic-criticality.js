const { Sequelize } = require('sequelize');
const { defaultClassificationIDs } = require('../../src/staticData/defaults');

module.exports = {
  up: (queryInterface) => {
    return queryInterface.describeTable('characteristics').then((tableDefinition) => {
      if (tableDefinition.criticality.type === Sequelize.DataTypes.TEXT) {
        return Promise.resolve();
      }
      return queryInterface
        .addColumn('characteristics', 'tempCrit', { type: Sequelize.DataTypes.TEXT, allowNull: true })
        .then((results) => queryInterface.sequelize.query('UPDATE characteristics SET "tempCrit" = "criticality"'))
        .then((results) => queryInterface.removeColumn('characteristics', 'criticality'))
        .then((results) => queryInterface.renameColumn('characteristics', 'tempCrit', 'criticality'))
        .then((results) =>
          Promise.all(
            Object.keys(defaultClassificationIDs).map((classification) => {
              return queryInterface.sequelize.query(`UPDATE characteristics SET "criticality"='${defaultClassificationIDs[classification]}' WHERE "criticality"='${classification.replace(' ', '_')}'`);
            })
          )
        );
    });
  },
  down: (queryInterface) => {
    return queryInterface.describeTable('characteristics').then((tableDefinition) => {
      if (tableDefinition.criticality.type === Sequelize.DataTypes.ENUM) {
        return Promise.resolve();
      }
      return queryInterface
        .addColumn('characteristics', 'tempCrit', { type: Sequelize.DataTypes.ENUM, allowNull: true, values: ['Incidental', 'Minor', 'Major', 'Key', 'Critical'] })
        .then((results) => queryInterface.sequelize.query('UPDATE characteristics SET "tempCrit" = "criticality"'))
        .then((results) => queryInterface.removeColumn('characteristics', 'criticality'))
        .then((results) => queryInterface.renameColumn('characteristics', 'tempCrit', 'criticality'))
        .then((results) =>
          Promise.all(
            Object.keys(defaultClassificationIDs).map((classification) => {
              return queryInterface.sequelize.query(`UPDATE characteristics SET "criticality"='${classification.replace(' ', '_')}' WHERE "criticality"='${defaultClassificationIDs[classification]}'`);
            })
          )
        );
    });
  },
};
