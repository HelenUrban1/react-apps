// These foreign keys were created in an earlier migration script, but due to the database design, this does not work as
// users are not stored in the tenant database - only the primary.

const { Sequelize } = require('sequelize');

module.exports = {
  up: (queryInterface) => {
    return queryInterface.describeTable('annotations').then(async () => {
      const transaction = await queryInterface.sequelize.transaction();
      try {
        await queryInterface.sequelize.query('ALTER TABLE "annotations" DROP CONSTRAINT IF EXISTS annotations_createdById_fkey;', { logging: console.log });
        await queryInterface.sequelize.query('ALTER TABLE "annotations" DROP CONSTRAINT IF EXISTS annotations_updatedById_fkey;', { logging: console.log });
        await transaction.commit();
      } catch (err) {
        await transaction.rollback();
        throw err;
      }
      return Promise.resolve();
    });
  },
  down: (queryInterface) => {
    return Promise.resolve();
  }
};
