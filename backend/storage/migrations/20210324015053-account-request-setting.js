const { Sequelize } = require('sequelize');
const config = require('../../config')();

module.exports = {
  up: (queryInterface) => {
    // Not loading list data into primary database, only tenants
    if (queryInterface.sequelize.config.database === config.database.database) {
      return Promise.resolve();
    }
    return queryInterface.sequelize.query('SELECT * FROM "settings" WHERE id = \'cb23aed2-ecc1-4525-9a10-7100a994f6ba\'').then((result) => {
      // If the list items already exist, skip trying to add them again
      if (result[0].length > 0) {
        return Promise.resolve();
      }
      return Promise.all([
        // add default Request setting
        queryInterface.bulkInsert(
          'settings',
          [
            {
              id: 'cb23aed2-ecc1-4525-9a10-7100a994f6ba',
              name: 'request',
              value: 'Open',
              metadata: null,
              siteId: null,
              userId: null,
              createdAt: Sequelize.literal('NOW()'),
              updatedAt: Sequelize.literal('NOW()'),
            },
          ],
          {}
        ),
      ]);
    });
  },

  down: (queryInterface) => {
    // Not loading list data into primary database, only tenants
    if (queryInterface.sequelize.config.database === config.database.database) {
      return Promise.resolve();
    }
    return queryInterface.sequelize.query('SELECT * FROM "settings" WHERE id = \'cb23aed2-ecc1-4525-9a10-7100a994f6ba\'').then((result) => {
      // If the list items already exist, skip trying to add them again
      if (result[0].length === 0) {
        return Promise.resolve();
      }
      return Promise.all([
        // Remove account Request setting
        queryInterface.bulkDelete(
          'settings',
          [
            {
              id: 'cb23aed2-ecc1-4525-9a10-7100a994f6ba',
            },
          ],
          {}
        ),
      ]);
    });
  },
};
