# Database Migrations

Migrations for the primary database are run on start up.

Migrations for tenant databases are run when they are first accessed.

Since both the primary and tenant databases use the same schema, the same migrations should run against each.

When a migration is run, a record is added to the SequelizeMeta table in the database to ensure it is not run again.

## Adding a Migration

Generate a new migration file with:

```
sequelize-cli migration:generate --name migration-name
```

This will generate the [migration skeleton](https://sequelize.org/master/manual/migrations.html#migration-skeleton). See the link for how to make changes using the queryInterface and how to build multiple changes into a transaction.

## Migration Tips

Don’t rename a table, create a new table with the new name, migrate the data and use insert and update triggers to maintain parity, start using new table.

When adding columns to existing tables, add without a default value, then set a default and backfill separately.

If changing a column type, create a new column of the new type, copy data in, then in a transaction rename both columns.

Use the concurrently option when creating or dropping indexes.

Whenever possible, keep changes backwards compatible, so older code could continue to work against newer schemas.

When creating a table the timestamp (createdAt, deletedAt, updatedAt) and audit (createdById, updatedById) columns need to be explicitly added. Using "timestamps: true" will not create the columns.

When adding a relation in a migration, create a column to hold an id reference, e.g. "userId" with a reference property that points to the model and key field on that model

If a column has allowNull as false, make sure to add a defaultValue

Make sure to return from inside the up and down functions

If adding a column with an ENUM type, the down function needs to remove the column first and then drop the type. Removing the column doesn't automatically drop the type (in case its used elsewhere)

If running Promise.all over all of the characteristics or other large tables split into batches (see featureRev-forward-pointer)
