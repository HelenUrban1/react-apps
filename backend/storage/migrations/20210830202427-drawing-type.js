const { Sequelize } = require('sequelize');
const config = require('../../config')();

module.exports = {
  up: (queryInterface) => {
    // Not adding columns to primary database, only tenants
    if (queryInterface.sequelize.config.database === config.database.database) {
      return Promise.resolve();
    }
    return queryInterface.describeTable('drawings').then((tableDefinition) => {
      if (tableDefinition.documentType) {
        return Promise.resolve();
      }
      return queryInterface.addColumn('drawings', 'documentType', {
        type: Sequelize.DataTypes.ENUM,
        values: ['Drawing', 'Materials', 'Specifications', 'Notes', 'Other'],
        defaultValue: 'Drawing',
      });
    });
  },
  down: (queryInterface) => {
    // Not adding columns to primary database, only tenants
    if (queryInterface.sequelize.config.database === config.database.database) {
      return Promise.resolve();
    }
    return queryInterface.describeTable('drawings').then((tableDefinition) => {
      if (!tableDefinition.documentType) {
        return Promise.resolve();
      }

      return queryInterface.removeColumn('drawings', 'documentType').then((result) => queryInterface.sequelize.query('DROP TYPE "enum_drawings_documentType";'));
    });
  },
};
