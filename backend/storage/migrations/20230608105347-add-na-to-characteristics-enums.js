'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface) {
    const transaction = await queryInterface.sequelize.transaction();
    try {
      await queryInterface.sequelize.query(`ALTER TYPE "enum_characteristics_toleranceSource" ADD VALUE 'N_A'`);
      await queryInterface.sequelize.query(`ALTER TYPE "enum_characteristics_notationClass" ADD VALUE 'N_A'`);
      await transaction.commit();
    } catch (error) {
      await transaction.rollback();
    }
  },

  async down(queryInterface) {
    await Promise.resolve();
  },
};
