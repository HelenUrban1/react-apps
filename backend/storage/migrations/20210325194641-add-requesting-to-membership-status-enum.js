const { Sequelize } = require('sequelize');
const config = require('../../config')();

module.exports = {
  up: (queryInterface) => {
    return queryInterface.sequelize
      .query(
        `
          SELECT enumlabel
          FROM pg_enum
          WHERE enumtypid = (
                        SELECT
                            oid
                        FROM
                            pg_type
                        WHERE
                            typname = 'enum_accountMembers_status'
                      )
          ORDER BY oid;
        `
      )
      .then((statusEnums) => {
        if (statusEnums[0].find((status) => status.enumlabel === 'Requesting')) {
          return Promise.resolve();
        }
        return queryInterface.sequelize.query(`ALTER TYPE "enum_accountMembers_status" ADD VALUE 'Requesting'`);
      });
  },
  down: (queryInterface) => {
    return queryInterface.sequelize
      .query(
        `
          SELECT enumlabel
          FROM pg_enum
          WHERE enumtypid = (
                        SELECT
                            oid
                        FROM
                            pg_type
                        WHERE
                            typname = 'enum_accountMembers_status'
                      )
          ORDER BY oid;
        `
      )
      .then((statusEnums) => {
        if (!statusEnums[0].find((status) => status.enumlabel === 'Requesting')) {
          return Promise.resolve();
        }
        return queryInterface.sequelize.query(`
          DELETE
          FROM
              pg_enum
          WHERE
              enumlabel = 'Requesting' AND
              enumtypid = (
                  SELECT
                      oid
                  FROM
                      pg_type
                  WHERE
                      typname = 'enum_accountMembers_status'
              )
        `);
      });
  },
};
