const { Sequelize, QueryTypes } = require('sequelize');

module.exports = {
  up: (queryInterface) => {
    return queryInterface.describeTable('drawingSheets').then(async (tableDefinition) => {
      if (!tableDefinition.nextRevision) {
        await queryInterface.sequelize.transaction((t) => {
          return Promise.all([
            queryInterface.addColumn(
              'drawingSheets',
              'nextRevision',
              {
                type: Sequelize.DataTypes.UUID,
                allowNull: true,
                references: {
                  model: 'drawingSheets',
                  key: 'id',
                },
              },
              { transaction: t }
            ),
          ]);
        });
      }
      return queryInterface.sequelize
        .query('SELECT * FROM "drawingSheets" WHERE "nextRevision" IS NULL', {
          type: QueryTypes.SELECT,
        })
        .then(async (result) => {
          if (result.length === 0) {
            return Promise.resolve();
          }
          const updateProcess = [];
          const batchSize = 5;
          for (let updateIndex = 0; updateIndex < result.length; updateIndex++) {
            // Want to handle in groups so we don't run out of memory
            if (updateProcess.length >= batchSize) {
              // eslint-disable-next-line no-await-in-loop
              await Promise.all(updateProcess);
              updateProcess.length = 0;
            }
            const { id } = result[updateIndex];
            updateProcess.push(
              queryInterface.sequelize
                .query('SELECT * FROM "drawingSheets" WHERE "previousRevision" = :id', {
                  type: QueryTypes.SELECT,
                  replacements: {
                    id,
                  },
                })
                .then((res) => {
                  if (res.length === 0) {
                    return Promise.resolve();
                  }
                  const nextRevId = res[0].id;
                  return queryInterface.sequelize.query('UPDATE "drawingSheets" SET "nextRevision" = :nextRevId WHERE "id" = :id', {
                    replacements: {
                      nextRevId,
                      id,
                    },
                    type: QueryTypes.UPDATE,
                  });
                })
            );
          }
          // clean up any remainders
          if (updateProcess.length > 0) {
            await Promise.all(updateProcess);
          }
          return Promise.resolve();
        });
    });
  },
  down: (queryInterface) => {
    return Promise.all([queryInterface.removeColumn('drawingSheets', 'nextRevision')]);
  },
};
