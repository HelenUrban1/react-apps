const { Sequelize } = require('sequelize');

module.exports = {
  up: (queryInterface) => {
    return queryInterface.describeTable('subscriptions').then((tableDefinition) => {
      if (tableDefinition.pendingBillingAmountInCents) {
        return Promise.resolve();
      }
      return queryInterface.addColumn('subscriptions', 'pendingBillingAmountInCents', { type: Sequelize.DataTypes.INTEGER, allowNull: false, defaultValue: 0 });
    });
  },
  down: (queryInterface) => {
    return queryInterface.describeTable('subscriptions').then((tableDefinition) => {
      if (!tableDefinition.pendingBillingAmountInCents) {
        return Promise.resolve();
      }
      return queryInterface.removeColumn('subscriptions', 'pendingBillingAmountInCents');
    });
  },
};
