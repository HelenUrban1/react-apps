const { Sequelize } = require('sequelize');

module.exports = {
  up: (queryInterface) => {
    return queryInterface.describeTable('accountMembers').then((tableDefinition) => {
      if (tableDefinition.inviteStatus) {
        return Promise.resolve();
      }
      return queryInterface.addColumn('accountMembers', 'inviteStatus', { type: Sequelize.DataTypes.TEXT, allowNull: true });
    });
  },
  down: (queryInterface) => {
    return queryInterface.describeTable('accountMembers').then((tableDefinition) => {
      if (!tableDefinition.inviteStatus) {
        return Promise.resolve();
      }
      return queryInterface.removeColumn('accountMembers', 'inviteStatus');
    });
  },
};
