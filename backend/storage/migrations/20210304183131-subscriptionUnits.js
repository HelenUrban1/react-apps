const { Sequelize } = require('sequelize');

module.exports = {
  up: async (queryInterface) => {
    return queryInterface.describeTable('subscriptions').then((tableDefinition) => {
      if (!tableDefinition.seatsPurchased && tableDefinition.measurementsUsed) {
        return Promise.resolve();
      }
      return queryInterface.sequelize.transaction((t) => {
        return Promise.all([
          queryInterface.removeColumn('subscriptions', 'drawingPrice', { transaction: t }),
          queryInterface.removeColumn('subscriptions', 'drawingsPurchased', { transaction: t }),
          queryInterface.removeColumn('subscriptions', 'pendingDrawings', { transaction: t }),
          queryInterface.removeColumn('subscriptions', 'seatsUsed', { transaction: t }),
          queryInterface.removeColumn('subscriptions', 'seatPrice', { transaction: t }),
          queryInterface.removeColumn('subscriptions', 'seatsPurchased', { transaction: t }),
          queryInterface.removeColumn('subscriptions', 'pendingSeats', { transaction: t }),
          queryInterface.addColumn(
            'subscriptions',
            'paidUserSeatsUsed',
            {
              type: Sequelize.DataTypes.INTEGER,
              allowNull: true,
            },
            { transaction: t }
          ),
          queryInterface.addColumn(
            'subscriptions',
            'measurementsUsed',
            {
              type: Sequelize.DataTypes.INTEGER,
              allowNull: true,
            },
            { transaction: t }
          ),
          queryInterface.addColumn(
            'subscriptions',
            'pendingComponents',
            {
              type: Sequelize.DataTypes.TEXT,
              allowNull: true,
            },
            { transaction: t }
          ),
          queryInterface.addColumn(
            'subscriptions',
            'providerFamilyHandle',
            {
              type: Sequelize.DataTypes.TEXT,
              allowNull: false,
              defaultValue: 'inspectionxpert-cloud',
            },
            { transaction: t }
          ),
        ]);
      });
    });
  },

  down: async (queryInterface) => {
    return queryInterface.describeTable('subscriptions').then((tableDefinition) => {
      if (tableDefinition.seatsPurchased && !tableDefinition.measurementsUsed) {
        return Promise.resolve();
      }
      return queryInterface.sequelize.transaction((t) => {
        return Promise.all([
          queryInterface.addColumn(
            'subscriptions',
            'drawingPrice',
            {
              type: Sequelize.DataTypes.INTEGER,
              allowNull: true,
            },
            { transaction: t }
          ),
          queryInterface.addColumn(
            'subscriptions',
            'drawingsPurchased',
            {
              type: Sequelize.DataTypes.INTEGER,
              allowNull: true,
            },
            { transaction: t }
          ),
          queryInterface.addColumn(
            'subscriptions',
            'pendingDrawings',
            {
              type: Sequelize.DataTypes.INTEGER,
              allowNull: true,
            },
            { transaction: t }
          ),
          queryInterface.addColumn(
            'subscriptions',
            'seatsUsed',
            {
              type: Sequelize.DataTypes.INTEGER,
              allowNull: true,
            },
            { transaction: t }
          ),
          queryInterface.addColumn(
            'subscriptions',
            'seatPrice',
            {
              type: Sequelize.DataTypes.INTEGER,
              allowNull: true,
            },
            { transaction: t }
          ),
          queryInterface.addColumn(
            'subscriptions',
            'seatsPurchased',
            {
              type: Sequelize.DataTypes.INTEGER,
              allowNull: true,
            },
            { transaction: t }
          ),
          queryInterface.addColumn(
            'subscriptions',
            'pendingSeats',
            {
              type: Sequelize.DataTypes.INTEGER,
              allowNull: true,
            },
            { transaction: t }
          ),
          queryInterface.removeColumn('subscriptions', 'paidUserSeatsUsed', { transaction: t }),
          queryInterface.removeColumn('subscriptions', 'measurementsUsed', { transaction: t }),
          queryInterface.removeColumn('subscriptions', 'pendingComponents', { transaction: t }),
          queryInterface.removeColumn('subscriptions', 'providerFamilyHandle', { transaction: t }),
        ]);
      });
    });
  },
};
