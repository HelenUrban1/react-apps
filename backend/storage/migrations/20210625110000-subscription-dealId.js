const { Sequelize } = require('sequelize');

module.exports = {
  up: (queryInterface) => {
    return queryInterface.describeTable('subscriptions').then((tableDefinition) => {
      if (tableDefinition.crmDealId) {
        return Promise.resolve();
      }
      return queryInterface.addColumn('subscriptions', 'crmDealId', { type: Sequelize.DataTypes.TEXT, allowNull: true });
    });
  },
  down: (queryInterface) => {
    return queryInterface.describeTable('subscriptions').then((tableDefinition) => {
      if (!tableDefinition.crmDealId) {
        return Promise.resolve();
      }
      return queryInterface.removeColumn('subscriptions', 'crmDealId');
    });
  },
};
