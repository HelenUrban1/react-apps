'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface) {
    const transaction = await queryInterface.sequelize.transaction();
    try {
      await queryInterface.sequelize.query(`CREATE TYPE "enum_auto_markup_status" AS ENUM ('In_Progress', 'Completed', 'Error')`);
      await queryInterface.sequelize.query(`ALTER TABLE "parts" ADD COLUMN "autoMarkupStatus" "enum_auto_markup_status"`);
      await queryInterface.sequelize.query(`ALTER TABLE "drawingSheets" ADD COLUMN "autoMarkupStatus" "enum_auto_markup_status"`);
      await transaction.commit();
    } catch (error) {
      await transaction.rollback();
      throw error;
    }
  },

  async down(queryInterface) {
    const transaction = await queryInterface.sequelize.transaction();
    try {
      await queryInterface.sequelize.query(`ALTER TABLE "parts" DROP COLUMN "autoMarkupStatus"`);
      await queryInterface.sequelize.query(`ALTER TABLE "drawingSheets" DROP COLUMN "autoMarkupStatus"`);
      await queryInterface.sequelize.query(`DROP TYPE "enum_auto_markup_status"`);
      await transaction.commit();
    } catch (error) {
      await transaction.rollback();
      throw error;
    }
  },
};
