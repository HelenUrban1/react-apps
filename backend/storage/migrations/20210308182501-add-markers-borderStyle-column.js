const { Sequelize } = require('sequelize');
const config = require('../../config')();

module.exports = {
  up: (queryInterface) => {
    // Not adding columns to primary database, only tenants
    if (queryInterface.sequelize.config.database === config.database.database) {
      return Promise.resolve();
    }
    return queryInterface.describeTable('markers').then((tableDefinition) => {
      if (tableDefinition.borderStyle) {
        return Promise.resolve();
      }
      return queryInterface.sequelize
        .query('DROP TYPE IF EXISTS "public"."enum_markers_borderStyle"') //
        .then(() => queryInterface.addColumn('markers', 'borderStyle', { type: Sequelize.DataTypes.ENUM, values: ['Solid', 'Dashed', 'Dotted'], allowNull: true, defaultValue: 'Solid' }).then((results) => queryInterface.sequelize.query(`UPDATE markers SET "borderStyle" = 'Solid' WHERE "borderStyle" IS NULL`)));
    });
  },
  down: (queryInterface) => {
    // Not adding columns to primary database, only tenants
    if (queryInterface.sequelize.config.database === config.database.database) {
      return Promise.resolve();
    }
    return queryInterface.describeTable('markers').then((tableDefinition) => {
      if (!tableDefinition.borderStyle) {
        return Promise.resolve();
      }
      return queryInterface.removeColumn('markers', 'borderStyle');
    });
  },
};
