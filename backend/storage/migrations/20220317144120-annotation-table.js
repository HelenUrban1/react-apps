const { Sequelize } = require('sequelize');

module.exports = {
  up: (queryInterface) => {
    return queryInterface
      .describeTable('annotations')
      .then(async () => {
        return Promise.resolve();
      })
      .catch(async () => {
        // if error, then table doesn't exist so we create it
        const transaction = await queryInterface.sequelize.transaction();

        try {
          await queryInterface.createTable(
            'annotations',
            {
              id: {
                type: Sequelize.DataTypes.UUID,
                defaultValue: Sequelize.DataTypes.UUIDV4,
                primaryKey: true,
              },
              annotation: {
                type: Sequelize.DataTypes.TEXT,
                allowNull: false,
                defaultValue: '',
                validate: {
                  notEmpty: true,
                },
              },
              fontSize: {
                type: Sequelize.DataTypes.INTEGER,
                allowNull: false,
                defaultValue: 24,
                validate: {
                  min: 4,
                  max: 100,
                },
              },
              fontColor: {
                type: Sequelize.DataTypes.TEXT,
                allowNull: false,
                defaultValue: '#000000',
              },
              borderColor: {
                type: Sequelize.DataTypes.TEXT,
                allowNull: false,
                defaultValue: '#E5B900',
              },
              backgroundColor: {
                type: Sequelize.DataTypes.TEXT,
                allowNull: false,
                defaultValue: '#FFFAE5',
              },
              drawingRotation: {
                type: Sequelize.DataTypes.DECIMAL,
                allowNull: false,
                defaultValue: 0,
                validate: {
                  min: -360,
                  max: 360,
                },
              },
              drawingScale: {
                type: Sequelize.DataTypes.DECIMAL,
                allowNull: false,
                defaultValue: 1.0,
              },
              boxLocationY: {
                type: Sequelize.DataTypes.DECIMAL,
                defaultValue: 0,
                allowNull: false,
              },
              boxLocationX: {
                type: Sequelize.DataTypes.DECIMAL,
                defaultValue: 0,
                allowNull: false,
              },
              boxWidth: {
                type: Sequelize.DataTypes.DECIMAL,
                defaultValue: 0,
                allowNull: false,
              },
              boxHeight: {
                type: Sequelize.DataTypes.DECIMAL,
                defaultValue: 0,
                allowNull: false,
              },
              boxRotation: {
                type: Sequelize.DataTypes.DECIMAL,
                defaultValue: 0,
                allowNull: false,
                validate: {
                  min: -360,
                  max: 360,
                },
              },
              drawingSheetIndex: {
                type: Sequelize.DataTypes.INTEGER,
                allowNull: false,
                validate: {
                  min: 1,
                },
                defaultValue: 1,
              },
              characteristicId: {
                type: Sequelize.DataTypes.UUID,
                allowNull: true,
                references: {
                  model: 'characteristics',
                  key: 'id',
                },
              },
              partId: {
                type: Sequelize.DataTypes.UUID,
                allowNull: true,
                references: {
                  model: 'parts',
                  key: 'id',
                },
              },
              drawingId: {
                type: Sequelize.DataTypes.UUID,
                allowNull: true,
                references: {
                  model: 'drawings',
                  key: 'id',
                },
              },
              drawingSheetId: {
                type: Sequelize.DataTypes.UUID,
                allowNull: true,
                references: {
                  model: 'drawingSheets',
                  key: 'id',
                },
              },
              originalAnnotation: {
                type: Sequelize.DataTypes.UUID,
                allowNull: true,
                references: {
                  model: 'annotations',
                  key: 'id',
                },
              },
              previousRevision: {
                type: Sequelize.DataTypes.UUID,
                allowNull: true,
                references: {
                  model: 'annotations',
                  key: 'id',
                },
              },
              nextRevision: {
                type: Sequelize.DataTypes.UUID,
                allowNull: true,
                references: {
                  model: 'annotations',
                  key: 'id',
                },
              },
              createdById: {
                type: Sequelize.DataTypes.UUID,
                allowNull: true,
                references: {
                  model: 'users',
                  key: 'id',
                },
              },
              updatedById: {
                type: Sequelize.DataTypes.UUID,
                allowNull: true,
                references: {
                  model: 'users',
                  key: 'id',
                },
              },
              createdAt: {
                type: Sequelize.DataTypes.DATE,
                allowNull: true,
              },
              updatedAt: {
                type: Sequelize.DataTypes.DATE,
                allowNull: true,
              },
              deletedAt: {
                type: Sequelize.DataTypes.DATE,
                allowNull: true,
              },
              importHash: {
                type: Sequelize.DataTypes.STRING(255),
                allowNull: true,
                unique: true,
              },
            },
            {
              timestamps: true,
              paranoid: true,
            },
            { transaction }
          );
          await transaction.commit();
        } catch (err) {
          await transaction.rollback();
          throw err;
        }
      });
  },

  down: (queryInterface) => {
    return queryInterface
      .describeTable('annotations')
      .then(() => {
        return queryInterface.dropTable('annotations');
      })
      .catch(() => {
        return Promise.resolve();
      });
  },
};
