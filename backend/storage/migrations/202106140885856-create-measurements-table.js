const { Sequelize } = require('sequelize');

module.exports = {
  up: (queryInterface) => {
    return queryInterface
      .describeTable('measurements')
      .then(async () => {
        return Promise.resolve();
      })
      .catch(async () => {
        // if error, then table doesn't exist so we create it
        const transaction = await queryInterface.sequelize.transaction();

        try {
          await queryInterface.createTable(
            'measurements',
            {
              id: {
                type: Sequelize.DataTypes.UUID,
                defaultValue: Sequelize.DataTypes.UUIDV4,
                primaryKey: true,
              },
              value: {
                type: Sequelize.DataTypes.TEXT,
                allowNull: true,
              },
              gage: {
                type: Sequelize.DataTypes.TEXT,
                allowNull: true,
              },
              machine: {
                type: Sequelize.DataTypes.TEXT,
                allowNull: true,
              },
              status: {
                type: Sequelize.DataTypes.ENUM,
                values: ['Unmeasured', 'Pass', 'Fail'],
                defaultValue: 'Unmeasured',
                allowNull: false,
              },
              characteristicId: {
                type: Sequelize.DataTypes.UUID,
                allowNull: true,
                references: {
                  model: 'characteristics',
                  key: 'id',
                },
              },
              sampleId: {
                type: Sequelize.DataTypes.UUID,
                allowNull: true,
                references: {
                  model: 'samples',
                  key: 'id',
                },
              },
              stationId: {
                type: Sequelize.DataTypes.UUID,
                allowNull: true,
                references: {
                  model: 'stations',
                  key: 'id',
                },
              },
              operationId: {
                type: Sequelize.DataTypes.UUID,
                allowNull: true,
                references: {
                  model: 'listItems',
                  key: 'id',
                },
              },
              methodId: {
                type: Sequelize.DataTypes.UUID,
                allowNull: true,
                references: {
                  model: 'listItems',
                  key: 'id',
                },
              },
              createdById: {
                type: Sequelize.DataTypes.UUID,
                allowNull: true,
              },
              updatedById: {
                type: Sequelize.DataTypes.UUID,
                allowNull: true,
              },
              importHash: {
                type: Sequelize.DataTypes.STRING(255),
                allowNull: true,
                unique: true,
              },
              createdAt: {
                type: Sequelize.DataTypes.DATE,
                allowNull: true,
              },
              updatedAt: {
                type: Sequelize.DataTypes.DATE,
                allowNull: true,
              },
              deletedAt: {
                type: Sequelize.DataTypes.DATE,
                allowNull: true,
              },
              createdById: {
                type: Sequelize.DataTypes.UUID,
                allowNull: true,
                references: {
                  model: 'operators',
                  key: 'id',
                },
              },
              updatedById: {
                type: Sequelize.DataTypes.UUID,
                allowNull: true,
                references: {
                  model: 'operators',
                  key: 'id',
                },
              },
            },
            {
              timestamps: true,
              paranoid: true,
            },
            { transaction }
          );
          await transaction.commit();
        } catch (err) {
          await transaction.rollback();
          throw err;
        }
      });
  },

  down: (queryInterface) => {
    return queryInterface
      .describeTable('measurements')
      .then(() => {
        return queryInterface.dropTable('measurements');
      })
      .catch(() => {
        return Promise.resolve();
      });
  },
};
