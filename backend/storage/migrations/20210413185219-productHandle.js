const { Sequelize } = require('sequelize');

module.exports = {
  up: async (queryInterface) => {
    return queryInterface.describeTable('subscriptions').then((tableDefinition) => {
      if (tableDefinition.providerProductHandle) {
        return Promise.resolve();
      }
      return queryInterface.sequelize.transaction((t) => {
        return queryInterface.addColumn(
          'subscriptions',
          'providerProductHandle',
          {
            type: Sequelize.DataTypes.TEXT,
            allowNull: false,
            defaultValue: 'ix3_standard',
          },
          { transaction: t }
        );
      });
    });
  },

  down: async (queryInterface) => {
    return queryInterface.describeTable('subscriptions').then((tableDefinition) => {
      if (!tableDefinition.providerProductHandle) {
        return Promise.resolve();
      }
      return queryInterface.sequelize.transaction((t) => {
        return queryInterface.removeColumn('subscriptions', 'providerProductHandle', { transaction: t });
      });
    });
  },
};
