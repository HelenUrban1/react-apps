const { Sequelize } = require('sequelize');

module.exports = {
  up: (queryInterface) => {
    return queryInterface.describeTable('characteristics').then((tableDefinition) => {
      if (tableDefinition.markerSize) {
        return Promise.resolve();
      }
      return queryInterface.addColumn('characteristics', 'markerSize', { type: Sequelize.DataTypes.INTEGER, defaultValue: 36, allowNull: false });
    });
  },
  down: (queryInterface) => {
    return queryInterface.describeTable('characteristics').then((tableDefinition) => {
      if (!tableDefinition.markerSize) {
        return Promise.resolve();
      }
      return queryInterface.removeColumn('characteristics', 'markerSize');
    });
  },
};
