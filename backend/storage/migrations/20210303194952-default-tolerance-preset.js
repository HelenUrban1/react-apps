const { Sequelize } = require('sequelize');
const { DefaultTolerances } = require('../../src/staticData/defaults');
const config = require('../../config')();

module.exports = {
  up: (queryInterface) => {
    // Not loading data into primary database, only tenants
    if (queryInterface.sequelize.config.database === config.database.database) {
      return Promise.resolve();
    }
    return queryInterface.sequelize.query('SELECT * FROM "settings" WHERE id = \'6eeae1ce-6ac8-4408-ad48-df46f2f9a983\'').then((result) => {
      // If entry already exists return
      if (result[0].length > 0) {
        return Promise.resolve();
      }
      return queryInterface.bulkInsert(
        'settings',
        [
          {
            id: '6eeae1ce-6ac8-4408-ad48-df46f2f9a983',
            name: 'tolerancePresets',
            value: 'default',
            metadata: JSON.stringify({ Default: { customers: [], setting: DefaultTolerances } }),
            siteId: null,
            userId: null,
            createdAt: Sequelize.literal('NOW()'),
            updatedAt: Sequelize.literal('NOW()'),
          },
        ],
        {}
      );
    });
  },

  down: (queryInterface) => {
    if (queryInterface.sequelize.config.database === config.database.database) {
      return Promise.resolve();
    }
    return queryInterface.sequelize.query('SELECT * FROM "settings" WHERE id = \'6eeae1ce-6ac8-4408-ad48-df46f2f9a983\'').then((result) => {
      // Entry already doesn't exist
      if (result[0].length === 0) {
        return Promise.resolve();
      }
      return queryInterface.bulkDelete(
        'settings',
        [
          {
            id: '6eeae1ce-6ac8-4408-ad48-df46f2f9a983',
          },
        ],
        {}
      );
    });
  },
};
