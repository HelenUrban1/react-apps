const { Sequelize } = require('sequelize');

module.exports = {
  up: async (queryInterface) => {
    return queryInterface.describeTable('subscriptions').then((tableDefinition) => {
      if (tableDefinition.partsUsed) {
        return Promise.resolve();
      }
      return queryInterface.sequelize.transaction((t) => {
        return queryInterface.addColumn(
          'subscriptions',
          'partsUsed',
          {
            type: Sequelize.DataTypes.INTEGER,
            allowNull: true,
          },
          { transaction: t }
        );
      });
    });
  },

  down: async (queryInterface) => {
    return queryInterface.describeTable('subscriptions').then((tableDefinition) => {
      if (!tableDefinition.partsUsed) {
        return Promise.resolve();
      }
      return queryInterface.sequelize.transaction((t) => {
        return queryInterface.removeColumn('subscriptions', 'partsUsed', { transaction: t });
      });
    });
  },
};
