const { Sequelize } = require('sequelize');

module.exports = {
  up: (queryInterface) => {
    return queryInterface.describeTable('samples').then(async (tableDefinition) => {
      if (tableDefinition.createdById) {
        return Promise.resolve();
      }
      // if error, then table doesn't exist so we create it
      const transaction = await queryInterface.sequelize.transaction();

      try {
        await Promise.all([
          queryInterface.addColumn('samples', 'createdById', {
            type: Sequelize.DataTypes.UUID,
            allowNull: true,
            references: {
              model: 'users',
              key: 'id',
            },
          }),
          queryInterface.addColumn('samples', 'updatedById', {
            type: Sequelize.DataTypes.UUID,
            allowNull: true,
            references: {
              model: 'users',
              key: 'id',
            },
          }),
        ]);

        await transaction.commit();
      } catch (err) {
        await transaction.rollback();
        throw err;
      }
    });
  },
  down: (queryInterface) => {
    return queryInterface.describeTable('samples').then(async (tableDefinition) => {
      if (!tableDefinition.createdAt) {
        return Promise.resolve();
      }
      return Promise.all([queryInterface.removeColumn('samples', 'createdById'), queryInterface.removeColumn('samples', 'updatedById')]);
    });
  },
};
