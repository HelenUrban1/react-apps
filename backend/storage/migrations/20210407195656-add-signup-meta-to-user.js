const { Sequelize } = require('sequelize');

module.exports = {
  up: (queryInterface) => {
    return queryInterface.describeTable('users').then((tableDefinition) => {
      if (tableDefinition.signupMeta) {
        return Promise.resolve();
      }
      return queryInterface.addColumn('users', 'signupMeta', { type: Sequelize.DataTypes.TEXT, allowNull: true });
    });
  },
  down: (queryInterface) => {
    return queryInterface.describeTable('users').then((tableDefinition) => {
      if (!tableDefinition.signupMeta) {
        return Promise.resolve();
      }

      return queryInterface.removeColumn('users', 'signupMeta');
    });
  },
};
