const { Sequelize } = require('sequelize');

module.exports = {
  up: (queryInterface) => {
    return queryInterface.sequelize
      .query(
        `
          SELECT enumlabel
          FROM pg_enum
          WHERE enumtypid = (
                        SELECT
                            oid
                        FROM
                            pg_type
                        WHERE
                            typname = 'enum_accountMembers_roles'
                      )
          ORDER BY oid;
        `
      )
      .then((roleEnums) => {
        if (roleEnums[0].find((role) => role.enumlabel === 'Reviewer')) {
          return Promise.resolve();
        }
        return queryInterface.sequelize.query(
          `ALTER TYPE "enum_accountMembers_roles" ADD VALUE 'Reviewer'`
        );
      });
  },
  down: (queryInterface) => {
    return queryInterface.sequelize
      .query(
        `
          SELECT enumlabel
          FROM pg_enum
          WHERE enumtypid = (
                        SELECT
                            oid
                        FROM
                            pg_type
                        WHERE
                            typname = 'enum_accountMembers_roles'
                      )
          ORDER BY oid;
        `
      )
      .then((roleEnums) => {
        if (!roleEnums[0].find((role) => role.enumlabel === 'Reviewer')) {
          return Promise.resolve();
        }
        return queryInterface.sequelize.query(`
          DELETE
          FROM
              pg_enum
          WHERE
              enumlabel = 'Reviewer' AND
              enumtypid = (
                  SELECT
                      oid
                  FROM
                      pg_type
                  WHERE
                      typname = 'enum_accountMembers_roles'
              )
        `);
      });
  },
};
