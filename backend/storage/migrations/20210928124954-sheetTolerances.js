const { Sequelize, QueryTypes } = require('sequelize');

module.exports = {
  up: (queryInterface) => {
    return queryInterface.describeTable('drawingSheets').then(async (tableDefinition) => {
      if (tableDefinition.defaultLinearTolerances) {
        return Promise.resolve();
      }
      await queryInterface.sequelize.transaction((t) => {
        return Promise.all([
          queryInterface.addColumn(
            'drawingSheets',
            'defaultLinearTolerances',
            {
              type: Sequelize.DataTypes.TEXT,
              allowNull: true,
            },
            { transaction: t }
          ),
          queryInterface.addColumn(
            'drawingSheets',
            'defaultAngularTolerances',
            {
              type: Sequelize.DataTypes.TEXT,
              allowNull: true,
            },
            { transaction: t }
          ),
        ]);
      });
      return queryInterface.sequelize
        .query('SELECT * FROM "drawingSheets" WHERE "defaultLinearTolerances" IS NULL AND "defaultAngularTolerances" IS NULL', {
          type: QueryTypes.SELECT,
        })
        .then((sheets) => {
          if (sheets.length === 0) {
            return Promise.resolve();
          }
          return Promise.all(
            sheets.map(async (sheet) => {
              const part = await queryInterface.sequelize.query('SELECT * FROM "parts" WHERE "id" = :partId', {
                replacements: {
                  partId: sheet.partId,
                },
              });
              if (part.length === 0 || !part[0] || !part[0][0]) {
                return Promise.resolve();
              }
              return queryInterface.sequelize.query('UPDATE "drawingSheets" SET "defaultLinearTolerances" = :linear, "defaultAngularTolerances" = :angular WHERE "id" = :id', {
                replacements: {
                  linear: part[0][0].defaultLinearTolerances,
                  angular: part[0][0].defaultAngularTolerances,
                  id: sheet.id,
                },
                type: QueryTypes.UPDATE,
              });
            })
          );
        });
    });
  },
  down: (queryInterface) => {
    return queryInterface.describeTable('drawingSheets').then(async (tableDefinition) => {
      if (!tableDefinition.defaultLinearTolerances) {
        return Promise.resolve();
      }
      return Promise.all([queryInterface.removeColumn('drawingSheets', 'defaultLinearTolerances'), queryInterface.removeColumn('drawingSheets', 'defaultAngularTolerances')]);
    });
  },
};
