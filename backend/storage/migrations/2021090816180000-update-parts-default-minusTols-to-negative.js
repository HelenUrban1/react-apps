const { QueryTypes } = require('sequelize');

module.exports = {
  up: (queryInterface) => {
    return queryInterface.sequelize.query(`UPDATE "parts" SET "defaultLinearTolerances" = replace("defaultLinearTolerances", 'minus":"', 'minus":"-'), "defaultAngularTolerances" = replace("defaultLinearTolerances", 'minus":"', 'minus":"-')`, {
      type: QueryTypes.UPDATE,
    });
  },
  down: (queryInterface) => {
    return queryInterface.sequelize.query(`UPDATE "parts" SET "defaultLinearTolerances" = replace("defaultLinearTolerances", 'minus":"-', 'minus":"'), "defaultAngularTolerances" = replace("defaultLinearTolerances", 'minus":"-', 'minus":"')`, {
      type: QueryTypes.UPDATE,
    });
  },
};
