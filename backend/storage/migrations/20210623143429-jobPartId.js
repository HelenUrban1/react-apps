const { Sequelize, QueryTypes } = require('sequelize');

module.exports = {
  up: (queryInterface) => {
    return queryInterface.describeTable('jobParts').then(async (tableDefinition) => {
      if (!tableDefinition.id) {
        await queryInterface.sequelize.transaction((t) => {
          return queryInterface.addColumn(
            'jobParts',
            'id',
            {
              type: Sequelize.DataTypes.UUID,
              defaultValue: Sequelize.DataTypes.UUIDV4,
              primaryKey: true,
            },
            { transaction: t }
          );
        });
      }
      return queryInterface.sequelize
        .query('SELECT * FROM "jobParts" WHERE "id" IS NULL', {
          type: QueryTypes.SELECT,
        })
        .then((result) => {
          if (result.length === 0) {
            return Promise.resolve();
          }
          return Promise.all(
            result.map((jp) => {
              const id = Sequelize.DataTypes.UUIDV4;
              return queryInterface.sequelize.query('UPDATE "jobParts" SET "id" = :id WHERE "jobId" = :jID AND "partId" = :pID', {
                replacements: {
                  id,
                  jID: jp.jobId,
                  pID: jp.partId,
                },
                type: QueryTypes.UPDATE,
              });
            })
          );
        });
    });
  },
  down: (queryInterface) => {
    return queryInterface.describeTable('jobParts').then(async (tableDefinition) => {
      if (!tableDefinition.id) {
        return Promise.resolve();
      }
      return Promise.all([queryInterface.removeColumn('jobParts', 'id')]);
    });
  },
};
