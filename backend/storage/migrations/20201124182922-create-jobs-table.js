const { Sequelize } = require('sequelize');

module.exports = {
  up: (queryInterface) => {
    return queryInterface
      .describeTable('jobs')
      .then(() => {
        return Promise.resolve();
      })
      .catch(() => {
        // if error, then table doesn't exist so we create it
        return queryInterface.createTable(
          'jobs',
          {
            id: {
              type: Sequelize.DataTypes.UUID,
              defaultValue: Sequelize.DataTypes.UUIDV4,
              primaryKey: true,
            },
            name: {
              type: Sequelize.DataTypes.STRING,
              allowNull: false,
              validate: {
                notEmpty: true,
              },
            },
            accessControl: {
              type: Sequelize.DataTypes.ENUM,
              values: ['None', 'ITAR'],
            },
            sampling: {
              type: Sequelize.DataTypes.ENUM,
              values: ['Timed', 'Quantity'],
            },
            jobStatus: {
              type: Sequelize.DataTypes.ENUM,
              values: ['Active', 'Inactive', 'Complete', 'Deleted'],
            },
            interval: {
              type: Sequelize.DataTypes.INTEGER,
            },
            samples: {
              type: Sequelize.DataTypes.INTEGER,
            },
            passing: {
              type: Sequelize.DataTypes.INTEGER,
            },
            scrapped: {
              type: Sequelize.DataTypes.INTEGER,
            },
          },
          {
            timestamps: true,
            paranoid: true,
          }
        );
      });
  },
  down: (queryInterface) => {
    return queryInterface
      .describeTable('jobs')
      .then(() => {
        return queryInterface.dropTable('jobs');
      })
      .catch(() => {
        return Promise.resolve();
      });
  },
};
