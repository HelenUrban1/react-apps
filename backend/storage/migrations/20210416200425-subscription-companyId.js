const { Sequelize } = require('sequelize');

module.exports = {
  up: (queryInterface) => {
    return queryInterface.describeTable('subscriptions').then((tableDefinition) => {
      if (tableDefinition.companyId) {
        return Promise.resolve();
      }
      return queryInterface.addColumn('subscriptions', 'companyId', { type: Sequelize.DataTypes.TEXT, defaultValue: '0', allowNull: false });
    });
  },
  down: (queryInterface) => {
    return queryInterface.describeTable('subscriptions').then((tableDefinition) => {
      if (!tableDefinition.companyId) {
        return Promise.resolve();
      }

      return queryInterface.removeColumn('subscriptions', 'companyId');
    });
  },
};
