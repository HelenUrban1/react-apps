const { invalidDomains } = require('../../src/staticData/defaults');

module.exports = {
  up: (queryInterface) => {
    return queryInterface.sequelize.query('SELECT * FROM "accounts"').then((result) => {
      if (result[0].length === 0) {
        return Promise.resolve();
      }
      return Promise.all(
        result[0].map((acct) => {
          const orgEmail = acct.orgEmail || '';
          if (!orgEmail) return Promise.resolve();
          const orgDomain = orgEmail.split('@').pop();
          let allowRequestPolicy = true;
          if (invalidDomains.includes(orgDomain) || result[0].find((a) => a.id !== acct.id && a.orgEmail.includes(orgDomain))) allowRequestPolicy = false;
          const newSetting = JSON.stringify({ ...JSON.parse(acct.setting || '{}'), allowRequestPolicy });
          return queryInterface.sequelize.query(`UPDATE accounts SET "settings" = '${newSetting}' WHERE "id" = '${acct.id}'`);
        })
      );
    });
  },

  down: (queryInterface) => {
    return Promise.resolve();
  },
};
