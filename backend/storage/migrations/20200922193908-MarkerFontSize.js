const { Sequelize } = require('sequelize');

module.exports = {
  up: (queryInterface) => {
    return queryInterface.describeTable('characteristics').then((tableDefinition) => {
      if (tableDefinition.markerFontSize) {
        return Promise.resolve();
      }
      return queryInterface.addColumn('characteristics', 'markerFontSize', { type: Sequelize.DataTypes.INTEGER, defaultValue: 18, allowNull: false });
    });
  },
  down: (queryInterface) => {
    return queryInterface.describeTable('characteristics').then((tableDefinition) => {
      if (!tableDefinition.markerFontSize) {
        return Promise.resolve();
      }
      return queryInterface.removeColumn('characteristics', 'markerFontSize');
    });
  },
};
