const { Sequelize } = require('sequelize');

module.exports = {
  up: (queryInterface) => {
    return queryInterface.describeTable('characteristics').then((tableDefinition) => {
      if (tableDefinition.reviewTaskId) {
        return Promise.resolve();
      }
      return queryInterface.addColumn('characteristics', 'reviewTaskId', { type: Sequelize.DataTypes.TEXT, allowNull: true });
    });
  },
  down: (queryInterface) => {
    return queryInterface.describeTable('characteristics').then((tableDefinition) => {
      if (!tableDefinition.reviewTaskId) {
        return Promise.resolve();
      }
      return queryInterface.removeColumn('characteristics', 'reviewTaskId');
    });
  },
};
