const { Sequelize, QueryTypes } = require('sequelize');

module.exports = {
  up: (queryInterface) => {
    return queryInterface.describeTable('drawings').then(async (tableDefinition) => {
      if (!tableDefinition.originalDrawing) {
        await queryInterface.sequelize.transaction((t) => {
          return Promise.all([
            queryInterface.addColumn(
              'drawings',
              'originalDrawing',
              {
                type: Sequelize.DataTypes.UUID,
                allowNull: true,
                references: {
                  model: 'drawings',
                  key: 'id',
                },
              },
              { transaction: t }
            ),
            queryInterface.addColumn(
              'drawings',
              'previousRevision',
              {
                type: Sequelize.DataTypes.UUID,
                allowNull: true,
                references: {
                  model: 'drawings',
                  key: 'id',
                },
              },
              { transaction: t }
            ),
          ]);
        });
      }
      return queryInterface.sequelize
        .query('SELECT * FROM "drawings" WHERE "originalDrawing" IS NULL', {
          type: QueryTypes.SELECT,
        })
        .then((result) => {
          if (result.length === 0) {
            return Promise.resolve();
          }
          return Promise.all(
            result.map((p) => {
              const { id } = p;
              return queryInterface.sequelize.query('UPDATE "drawings" SET "originalDrawing" = :id WHERE "id" = :id', {
                replacements: {
                  id,
                },
                type: QueryTypes.UPDATE,
              });
            })
          );
        });
    });
  },
  down: (queryInterface) => {
    return Promise.all([queryInterface.removeColumn('drawings', 'originalDrawing'), queryInterface.removeColumn('drawings', 'previousRevision')]);
  },
};
