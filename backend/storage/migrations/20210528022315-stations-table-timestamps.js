const { Sequelize } = require('sequelize');

module.exports = {
  up: (queryInterface) => {
    return queryInterface.describeTable('stations').then(async (tableDefinition) => {
      if (tableDefinition.createdAt) {
        return Promise.resolve();
      }
      // if error, then table doesn't exist so we create it
      const transaction = await queryInterface.sequelize.transaction();

      try {
        await Promise.all([
          queryInterface.addColumn('stations', 'createdAt', { type: Sequelize.DataTypes.DATE, allowNull: true }, { transaction }),
          queryInterface.addColumn('stations', 'updatedAt', { type: Sequelize.DataTypes.DATE, allowNull: true }, { transaction }),
          queryInterface.addColumn('stations', 'deletedAt', { type: Sequelize.DataTypes.DATE, allowNull: true }, { transaction }),
        ]);

        await transaction.commit();
      } catch (err) {
        await transaction.rollback();
        throw err;
      }
    });
  },
  down: (queryInterface) => {
    return queryInterface.describeTable('stations').then(async (tableDefinition) => {
      if (!tableDefinition.createdAt) {
        return Promise.resolve();
      }
      return Promise.all([queryInterface.removeColumn('stations', 'createdAt'), queryInterface.removeColumn('stations', 'deletedAt'), queryInterface.removeColumn('stations', 'updatedAt')]);
    });
  },
};
