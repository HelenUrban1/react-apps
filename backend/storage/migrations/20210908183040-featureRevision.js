const { Sequelize, QueryTypes } = require('sequelize');

module.exports = {
  up: (queryInterface) => {
    return queryInterface.describeTable('characteristics').then(async (tableDefinition) => {
      if (!tableDefinition.originalCharacteristic) {
        await queryInterface.sequelize.transaction((t) => {
          return Promise.all([
            queryInterface.addColumn(
              'characteristics',
              'originalCharacteristic',
              {
                type: Sequelize.DataTypes.UUID,
                allowNull: true,
                references: {
                  model: 'characteristics',
                  key: 'id',
                },
              },
              { transaction: t }
            ),
            queryInterface.addColumn(
              'characteristics',
              'previousRevision',
              {
                type: Sequelize.DataTypes.UUID,
                allowNull: true,
                references: {
                  model: 'characteristics',
                  key: 'id',
                },
              },
              { transaction: t }
            ),
          ]);
        });
      }
      return queryInterface.sequelize
        .query('SELECT * FROM "characteristics" WHERE "originalCharacteristic" IS NULL', {
          type: QueryTypes.SELECT,
        })
        .then((result) => {
          if (result.length === 0) {
            return Promise.resolve();
          }
          return Promise.all(
            result.map((p) => {
              const { id } = p;
              return queryInterface.sequelize.query('UPDATE "characteristics" SET "originalCharacteristic" = :id WHERE "id" = :id', {
                replacements: {
                  id,
                },
                type: QueryTypes.UPDATE,
              });
            })
          );
        });
    });
  },
  down: (queryInterface) => {
    return Promise.all([queryInterface.removeColumn('characteristics', 'originalCharacteristic'), queryInterface.removeColumn('characteristics', 'previousRevision')]);
  },
};
