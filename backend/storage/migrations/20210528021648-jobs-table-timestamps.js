const { Sequelize } = require('sequelize');

module.exports = {
  up: (queryInterface) => {
    return queryInterface.describeTable('jobs').then(async (tableDefinition) => {
      if (tableDefinition.createdAt) {
        return Promise.resolve();
      }
      // if error, then table doesn't exist so we create it
      const transaction = await queryInterface.sequelize.transaction();

      try {
        await Promise.all([
          queryInterface.addColumn('jobs', 'createdAt', { type: Sequelize.DataTypes.DATE, allowNull: true }),
          queryInterface.addColumn('jobs', 'updatedAt', { type: Sequelize.DataTypes.DATE, allowNull: true }),
          queryInterface.addColumn('jobs', 'deletedAt', { type: Sequelize.DataTypes.DATE, allowNull: true }),
        ]);

        await transaction.commit();
      } catch (err) {
        await transaction.rollback();
        throw err;
      }
    });
  },
  down: (queryInterface) => {
    return queryInterface.describeTable('jobs').then(async (tableDefinition) => {
      if (!tableDefinition.createdAt) {
        return Promise.resolve();
      }
      return Promise.all([queryInterface.removeColumn('jobs', 'createdAt'), queryInterface.removeColumn('jobs', 'deletedAt'), queryInterface.removeColumn('jobs', 'updatedAt')]);
    });
  },
};
