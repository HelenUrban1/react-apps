const { Sequelize } = require('sequelize');

module.exports = {
  up: (queryInterface) => {
    return queryInterface
      .describeTable('stations')
      .then(async () => {
        return Promise.resolve();
      })
      .catch(async () => {
        // if error, then table doesn't exist so we create it
        const transaction = await queryInterface.sequelize.transaction();

        try {
          await queryInterface.createTable(
            'stations',
            {
              id: {
                type: Sequelize.DataTypes.UUID,
                defaultValue: Sequelize.DataTypes.UUIDV4,
                primaryKey: true,
              },
              disabled: {
                type: Sequelize.DataTypes.BOOLEAN,
                allowNull: false,
                defaultValue: false,
              },
              isMaster: {
                type: Sequelize.DataTypes.BOOLEAN,
                allowNull: false,
                defaultValue: false,
              },
              name: {
                type: Sequelize.DataTypes.STRING,
                allowNull: false,
                validate: {
                  notEmpty: true,
                },
              },
              importHash: {
                type: Sequelize.DataTypes.STRING(255),
                allowNull: true,
                unique: true,
              },
            },
            {
              timestamps: true,
              paranoid: true,
            },
            { transaction }
          );

          await queryInterface.bulkInsert(
            'stations',
            [
              {
                id: '0ed5e900-9e38-4560-8af4-a0e3bc3a5fb0',
                name: 'Default',
                disabled: false,
                isMaster: true,
              },
            ],
            { transaction }
          );

          await transaction.commit();
        } catch (err) {
          await transaction.rollback();
          throw err;
        }
      });
  },

  down: (queryInterface) => {
    return queryInterface
      .describeTable('stations')
      .then(() => {
        return queryInterface.dropTable('stations');
      })
      .catch(() => {
        return Promise.resolve();
      });
  },
};
