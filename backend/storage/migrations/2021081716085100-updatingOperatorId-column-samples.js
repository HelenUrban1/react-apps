const { Sequelize } = require('sequelize');

module.exports = {
  up: (queryInterface) => {
    return queryInterface.describeTable('samples').then((tableDefinition) => {
      if (tableDefinition.updatingOperatorId) {
        return Promise.resolve();
      }
      return queryInterface.addColumn('samples', 'updatingOperatorId', {
        type: Sequelize.DataTypes.UUID,
        allowNull: true,
        references: {
          model: 'operators',
          key: 'id',
        },
      });
    });
  },
  down: (queryInterface) => {
    return queryInterface.describeTable('samples').then((tableDefinition) => {
      if (!tableDefinition.updatingOperatorId) {
        return Promise.resolve();
      }
      return queryInterface.removeColumn('samples', 'updatingOperatorId');
    });
  },
};
