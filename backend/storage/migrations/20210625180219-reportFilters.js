const { Sequelize } = require('sequelize');

module.exports = {
  up: (queryInterface) => {
    return queryInterface.describeTable('reports').then((tableDefinition) => {
      if (tableDefinition.filters) {
        return Promise.resolve();
      }
      return queryInterface.addColumn('reports', 'filters', { type: Sequelize.DataTypes.TEXT, defaultValue: '{}', allowNull: true });
    });
  },
  down: (queryInterface) => {
    return queryInterface.describeTable('reports').then((tableDefinition) => {
      if (!tableDefinition.filters) {
        return Promise.resolve();
      }

      return queryInterface.removeColumn('reports', 'filters');
    });
  },
};
