const { QueryTypes } = require('sequelize');

module.exports = {
  up: (queryInterface) => {
    return queryInterface.sequelize.query(`UPDATE "characteristics" SET "minusTol" = CONCAT('-', "minusTol") WHERE "minusTol" IS NOT NULL AND "minusTol" != '' AND NOT ("minusTol" LIKE '-%')`, {
      type: QueryTypes.UPDATE,
    });
  },
  down: (queryInterface) => {
    return queryInterface.sequelize.query(`UPDATE "characteristics" SET "minusTol" = translate("minusTol", '-', '') WHERE "minusTol" IS NOT NULL AND "minusTol" != '' AND ("minusTol" LIKE '-%')`, {
      type: QueryTypes.UPDATE,
    });
  },
};
