const config = require('../../config')();

module.exports = {
  up: (queryInterface) => {
    // Not loading data into primary database, only tenants
    if (queryInterface.sequelize.config.database === config.database.database) {
      return Promise.resolve();
    }
    return queryInterface.sequelize.query('SELECT * FROM "settings" WHERE id = \'9acfc012-9859-4364-a22b-5dd17637cca4\'').then((result) => {
      // If entry already exists return
      if (result[0].length === 0 || !result[0][0]) {
        return Promise.resolve();
      }
      const newMeta = result[0][0].metadata.replace(/assignedStyles/g, 'setting');
      // assignedStyles renamed to setting to allow for consistent use across different setting types
      return queryInterface.sequelize.query(`UPDATE settings SET "metadata" = '${newMeta}' WHERE "id" = '9acfc012-9859-4364-a22b-5dd17637cca4'`);
    });
  },

  down: (queryInterface) => {
    if (queryInterface.sequelize.config.database === config.database.database) {
      return Promise.resolve();
    }
    return queryInterface.sequelize.query('SELECT * FROM "settings" WHERE id = \'9acfc012-9859-4364-a22b-5dd17637cca4\'').then((result) => {
      // Entry already doesn't exist
      if (result[0].length === 0 || !result[0][0]) {
        return Promise.resolve();
      }
      const newMeta = result[0][0].metadata.replace(/setting/g, 'assignedStyles');
      return queryInterface.sequelize.query(`UPDATE settings SET "metadata" = '${newMeta}' WHERE "id" = '9acfc012-9859-4364-a22b-5dd17637cca4'`);
    });
  },
};
