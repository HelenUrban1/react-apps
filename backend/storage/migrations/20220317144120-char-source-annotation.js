const { Sequelize } = require('sequelize');

module.exports = {
  up: (queryInterface) => {
    return queryInterface.describeTable('characteristics').then((tableDefinition) => {
      if (tableDefinition.captureMethod.special.includes('Annotation')) {
        return Promise.resolve();
      }
      return queryInterface.sequelize.query(`ALTER TYPE "enum_characteristics_captureMethod" ADD VALUE 'Annotation'`);
    });
  },
  down: (queryInterface) => {
    return queryInterface.describeTable('characteristics').then((tableDefinition) => {
      if (!tableDefinition.captureMethod.special.includes('Annotation')) {
        return Promise.resolve();
      }
      return Promise.resolve();
    });
  },
};
