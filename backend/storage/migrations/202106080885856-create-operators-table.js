const { Sequelize } = require('sequelize');

module.exports = {
  up: (queryInterface) => {
    return queryInterface
      .describeTable('operators')
      .then(async () => {
        return Promise.resolve();
      })
      .catch(async () => {
        // if error, then table doesn't exist so we create it
        const transaction = await queryInterface.sequelize.transaction();

        try {
          await queryInterface.createTable(
            'operators',
            {
              id: {
                type: Sequelize.DataTypes.UUID,
                defaultValue: Sequelize.DataTypes.UUIDV4,
                primaryKey: true,
              },
              fullName: {
                type: Sequelize.DataTypes.STRING,
                allowNull: false,
                validate: {
                  notEmpty: true,
                },
              },
              firstName: {
                type: Sequelize.DataTypes.STRING,
                allowNull: false,
                defaultValue: 0,
              },
              lastName: {
                type: Sequelize.DataTypes.STRING,
                allowNull: true,
              },
              accessControl: {
                type: Sequelize.DataTypes.ENUM,
                values: ['None', 'ITAR'],
              },
              email: {
                type: Sequelize.DataTypes.STRING,
                allowNull: true,
              },
              createdById: {
                type: Sequelize.DataTypes.UUID,
                allowNull: true,
              },
              updatedById: {
                type: Sequelize.DataTypes.UUID,
                allowNull: true,
              },
            },
            {
              timestamps: true,
              paranoid: true,
            },
            { transaction }
          );
          await transaction.commit();
        } catch (err) {
          await transaction.rollback();
          throw err;
        }
      });
  },

  down: (queryInterface) => {
    return queryInterface
      .describeTable('operators')
      .then(() => {
        return queryInterface.dropTable('operators');
      })
      .catch(() => {
        return Promise.resolve();
      });
  },
};
