const { Sequelize } = require('sequelize');

module.exports = {
  up: (queryInterface) => {
    return queryInterface.describeTable('characteristics').then((tableDefinition) => {
      if (tableDefinition.captureMethod.special.includes('Custom')) {
        return Promise.resolve();
      }
      return queryInterface.sequelize.query(`ALTER TYPE "enum_characteristics_captureMethod" ADD VALUE 'Custom'`);
    });
  },
  down: (queryInterface) => {
    return queryInterface.describeTable('characteristics').then((tableDefinition) => {
      if (!tableDefinition.captureMethod.special.includes('Custom')) {
        return Promise.resolve();
      }
      return Promise.resolve();
    });
  },
};
