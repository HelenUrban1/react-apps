const { Sequelize } = require('sequelize');

module.exports = {
  up: (queryInterface) => {
    return queryInterface.describeTable('subscriptions').then((tableDefinition) => {
      if (
        tableDefinition.drawingPrice ||
        tableDefinition.drawingsPurchased ||
        tableDefinition.drawingsUsed ||
        tableDefinition.pendingDrawings ||
        tableDefinition.components
      ) {
        return Promise.resolve();
      }
      return queryInterface.sequelize.transaction((t) => {
        return Promise.all([
          queryInterface.addColumn(
            'subscriptions',
            'drawingPrice',
            {
              type: Sequelize.DataTypes.INTEGER,
              allowNull: true,
            },
            { transaction: t }
          ),
          queryInterface.addColumn(
            'subscriptions',
            'drawingsPurchased',
            {
              type: Sequelize.DataTypes.INTEGER,
              allowNull: true,
            },
            { transaction: t }
          ),
          queryInterface.addColumn(
            'subscriptions',
            'drawingsUsed',
            {
              type: Sequelize.DataTypes.INTEGER,
              allowNull: true,
            },
            { transaction: t }
          ),
          queryInterface.addColumn(
            'subscriptions',
            'components',
            {
              type: Sequelize.DataTypes.TEXT,
              allowNull: true,
            },
            { transaction: t }
          ),
          queryInterface.addColumn(
            'subscriptions',
            'pendingDrawings',
            {
              type: Sequelize.DataTypes.INTEGER,
              allowNull: true,
            },
            { transaction: t }
          ),
        ]);
      });
    });
  },

  down: (queryInterface) => {
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
        queryInterface.removeColumn('subscriptions', 'drawingPrice', { transaction: t }),
        queryInterface.removeColumn('subscriptions', 'drawingsPurchased', { transaction: t }),
        queryInterface.removeColumn('subscriptions', 'drawingsUsed', { transaction: t }),
        queryInterface.removeColumn('subscriptions', 'pendingDrawings', { transaction: t }),
        queryInterface.removeColumn('subscriptions', 'components', { transaction: t }),
      ]);
    });
  },
};
