const { Sequelize, QueryTypes } = require('sequelize');

module.exports = {
  up: (queryInterface) => {
    return queryInterface.describeTable('drawings').then(async (tableDefinition) => {
      if (!tableDefinition.drawingFileId) {
        await queryInterface.sequelize.transaction((t) => {
          return Promise.all([
            queryInterface.addColumn(
              'drawings',
              'drawingFileId',
              {
                type: Sequelize.DataTypes.UUID,
                allowNull: true,
                references: {
                  model: 'files',
                  key: 'id',
                },
              },
              { transaction: t }
            ),
          ]);
        });
      }
      return queryInterface.sequelize
        .query('SELECT * FROM "files"', {
          type: QueryTypes.SELECT,
        })
        .then((result) => {
          if (result.length === 0) {
            return Promise.resolve();
          }
          return Promise.all(
            result.map((f) => {
              const { id, belongsToId } = f;
              return queryInterface.sequelize.query('UPDATE "drawings" SET "drawingFileId" = :id WHERE "id" = :belongsToId AND "drawingFileId" IS NULL', {
                replacements: {
                  id,
                  belongsToId,
                },
                type: QueryTypes.UPDATE,
              });
            })
          );
        });
    });
  },
  down: (queryInterface) => {
    return queryInterface.describeTable('drawings').then(async (tableDefinition) => {
      if (tableDefinition.drawingFileId) {
        return Promise.all([queryInterface.removeColumn('drawings', 'drawingFileId')]);
      }

      return Promise.resolve();
    });
  },
};
