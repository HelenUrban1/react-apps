const { Sequelize } = require('sequelize');

module.exports = {
  up: (queryInterface) => {
    return queryInterface.describeTable('parts').then((tableDefinition) => {
      if (tableDefinition.presets) {
        return Promise.resolve();
      }
      return queryInterface.addColumn('parts', 'presets', { type: Sequelize.DataTypes.TEXT, allowNull: true });
    });
  },
  down: (queryInterface) => {
    return queryInterface.describeTable('parts').then((tableDefinition) => {
      if (!tableDefinition.presets) {
        return Promise.resolve();
      }
      return queryInterface.removeColumn('parts', 'presets');
    });
  },
};
