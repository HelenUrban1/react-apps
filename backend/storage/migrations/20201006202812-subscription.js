const moment = require('moment');
const { Sequelize } = require('sequelize');

module.exports = {
  up: (queryInterface) => {
    return queryInterface.describeTable('subscriptions').then((tableDefinition) => {
      if (tableDefinition.renewalDate || tableDefinition.pendingSeats || tableDefinition.pendingTier || tableDefinition.pendingBillingPeriod) {
        return Promise.resolve();
      }
      return queryInterface.sequelize.transaction((t) => {
        return Promise.all([
          queryInterface.addColumn(
            'subscriptions',
            'renewalDate',
            {
              type: Sequelize.DataTypes.DATEONLY,
              allowNull: false,
              get() {
                return this.getDataValue('renewalDate') ? moment.utc(this.getDataValue('renewalDate')).format('YYYY-MM-DD') : null;
              },
            },
            { transaction: t }
          ),
          queryInterface.addColumn(
            'subscriptions',
            'pendingSeats',
            {
              type: Sequelize.DataTypes.INTEGER,
              allowNull: true,
            },
            { transaction: t }
          ),
          queryInterface.addColumn(
            'subscriptions',
            'pendingTier',
            {
              type: Sequelize.DataTypes.INTEGER,
              allowNull: true,
            },
            { transaction: t }
          ),
          queryInterface.addColumn(
            'subscriptions',
            'pendingBillingPeriod',
            {
              type: Sequelize.DataTypes.ENUM,
              values: ['Pay_as_you_go', 'Monthly', 'Annually', 'Multi_Year'],
              allowNull: true,
            },
            { transaction: t }
          ),
        ]);
      });
    });
  },

  down: (queryInterface) => {
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
        queryInterface.removeColumn('subscriptions', 'renewalDate', { transaction: t }),
        queryInterface.removeColumn('subscriptions', 'pendingSeats', { transaction: t }),
        queryInterface.removeColumn('subscriptions', 'pendingTier', { transaction: t }),
        queryInterface.removeColumn('subscriptions', 'pendingBillingPeriod', { transaction: t }),
      ]);
    });
  },
};
