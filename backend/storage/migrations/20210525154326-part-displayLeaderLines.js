const { Sequelize } = require('sequelize');

module.exports = {
  up: (queryInterface) => {
    return queryInterface.describeTable('parts').then((tableDefinition) => {
      if (tableDefinition.displayLeaderLines) {
        return Promise.resolve();
      }
      return queryInterface.addColumn('parts', 'displayLeaderLines', { type: Sequelize.DataTypes.BOOLEAN, defaultValue: true });
    });
  },
  down: (queryInterface) => {
    return queryInterface.describeTable('parts').then((tableDefinition) => {
      if (!tableDefinition.displayLeaderLines) {
        return Promise.resolve();
      }
      return queryInterface.removeColumn('parts', 'displayLeaderLines');
    });
  },
};
