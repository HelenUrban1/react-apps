module.exports = {
  up: (queryInterface) => {
    return queryInterface.sequelize.query(
      `UPDATE "listItems" SET metadata = '{"unit":"deg","type":"Imperial,Metric","measurement":"plane_angle"}' WHERE id = 'e1150ac6-6803-41fa-b525-9b8aba2eccb2';
      UPDATE "listItems" SET metadata = '{"unit": "''","type":"Imperial,Metric","measurement":"plane_angle"}' WHERE id = '399ac24d-b734-4dae-b232-0b613e60bf03';
      UPDATE "listItems" SET metadata = '{"unit": "''''","type":"Imperial,Metric","measurement":"plane_angle"}' WHERE id = '763cb443-3404-4b57-b68e-34ebb2f33282';
      UPDATE "listItems" SET metadata = '{"unit":"ᵍ","type":"Imperial,Metric","measurement":"plane_angle"}' WHERE id = 'ecbef5a7-f56c-40ec-ab39-85213dc51aac';
      UPDATE "listItems" SET metadata = '{"unit":"circ","type":"Imperial,Metric","measurement":"plane_angle"}' WHERE id = '29193124-94fa-4395-9ed1-d9e733494a80';
      UPDATE "listItems" SET metadata = '{"unit":"µrad","type":"Imperial,Metric","measurement":"plane_angle"}' WHERE id = '9d68288f-c4b5-46e2-bd36-7759c0d0bc4e';
      UPDATE "listItems" SET metadata = '{"unit":"crad","type":"Imperial,Metric","measurement":"plane_angle"}' WHERE id = '8a857e3d-e665-42d4-aaa9-f978d8bfa4d7';
      UPDATE "listItems" SET metadata = '{"unit":"krad","type":"Imperial,Metric","measurement":"plane_angle"}' WHERE id = '17288d96-5fa8-4998-b5fe-a3ff3ed5122f';
      UPDATE "listItems" SET metadata = '{"unit":"mrad","type":"Imperial,Metric","measurement":"plane_angle"}' WHERE id = '6385a7f2-c165-41ff-bd12-0f2f3d2a2f2c';
      UPDATE "listItems" SET metadata = '{"unit":"rad","type":"Imperial,Metric","measurement":"plane_angle"}' WHERE id = 'd3f0cc70-6be7-4fe3-8291-a354ffdf0223';`
    );
  },

  down: (queryInterface) => {
    return queryInterface.sequelize.query(
      `UPDATE "listItems" SET metadata = '{"unit":"deg","type":"Imperial","measurement":"plane_angle"}' WHERE id = 'e1150ac6-6803-41fa-b525-9b8aba2eccb2';
      UPDATE "listItems" SET metadata = '{"unit": "''","type":"Imperial","measurement":"plane_angle"}' WHERE id = '399ac24d-b734-4dae-b232-0b613e60bf03';
      UPDATE "listItems" SET metadata = '{"unit": "''''","type":"Imperial","measurement":"plane_angle"}' WHERE id = '763cb443-3404-4b57-b68e-34ebb2f33282';
      UPDATE "listItems" SET metadata = '{"unit":"ᵍ","type":"Imperial","measurement":"plane_angle"}' WHERE id = 'ecbef5a7-f56c-40ec-ab39-85213dc51aac';
      UPDATE "listItems" SET metadata = '{"unit":"circ","type":"Imperial","measurement":"plane_angle"}' WHERE id = '29193124-94fa-4395-9ed1-d9e733494a80';
      UPDATE "listItems" SET metadata = '{"unit":"µrad","type":"Metric","measurement":"plane_angle"}' WHERE id = '9d68288f-c4b5-46e2-bd36-7759c0d0bc4e';
      UPDATE "listItems" SET metadata = '{"unit":"crad","type":"Metric","measurement":"plane_angle"}' WHERE id = '8a857e3d-e665-42d4-aaa9-f978d8bfa4d7';
      UPDATE "listItems" SET metadata = '{"unit":"krad","type":"Metric","measurement":"plane_angle"}' WHERE id = '17288d96-5fa8-4998-b5fe-a3ff3ed5122f';
      UPDATE "listItems" SET metadata = '{"unit":"mrad","type":"Metric","measurement":"plane_angle"}' WHERE id = '6385a7f2-c165-41ff-bd12-0f2f3d2a2f2c';
      UPDATE "listItems" SET metadata = '{"unit":"rad","type":"Metric","measurement":"plane_angle"}' WHERE id = 'd3f0cc70-6be7-4fe3-8291-a354ffdf0223';`
    );
  },
};
