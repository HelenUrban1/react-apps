const { Sequelize } = require('sequelize');

module.exports = {
  up: (queryInterface) => {
    return queryInterface.describeTable('operators').then(async (tableDefinition) => {
      if (tableDefinition.status) {
        return Promise.resolve();
      }

      return queryInterface.addColumn('operators', 'status', {
        type: Sequelize.DataTypes.STRING,
        allowNull: false,
        defaultValue: 'Active',
      });
    });
  },
  down: (queryInterface) => {
    return queryInterface.describeTable('operators').then(async (tableDefinition) => {
      if (!tableDefinition.status) {
        return Promise.resolve();
      }
      return queryInterface.removeColumn('operators', 'status');
    });
  },
};
