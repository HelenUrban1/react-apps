const { Sequelize } = require('sequelize');
const config = require('../../config')();

module.exports = {
  up: (queryInterface) => {
    // Not adding columns to primary database, only tenants
    if (queryInterface.sequelize.config.database === config.database.database) {
      return Promise.resolve();
    }
    return queryInterface.describeTable('parts').then((tableDefinition) => {
      if (tableDefinition.lastReportId) {
        return Promise.resolve();
      }
      return queryInterface.addColumn('parts', 'lastReportId', {
        type: Sequelize.DataTypes.STRING,
        allowNull: true,
        defaultValue: null,
      });
    });
  },
  down: (queryInterface) => {
    // Not adding columns to primary database, only tenants
    if (queryInterface.sequelize.config.database === config.database.database) {
      return Promise.resolve();
    }
    return queryInterface.describeTable('parts').then((tableDefinition) => {
      if (!tableDefinition.lastReportId) {
        return Promise.resolve();
      }
      return queryInterface.removeColumn('parts', 'lastReportId');
    });
  },
};
