const { Sequelize } = require('sequelize');

module.exports = {
  up: (queryInterface) => {
    return queryInterface.describeTable('accountMembers').then((tableDefinition) => {
      if (tableDefinition.accessControl) {
        return Promise.resolve();
      }
      return queryInterface.addColumn('accountMembers', 'accessControl', { type: Sequelize.DataTypes.BOOLEAN, defaultValue: true, allowNull: false });
    });
  },

  down: (queryInterface) => {
    return queryInterface.describeTable('accountMembers').then((tableDefinition) => {
      if (!tableDefinition.accessControl) {
        return Promise.resolve();
      }
      return queryInterface.removeColumn('accountMembers', 'accessControl');
    });
  },
};
