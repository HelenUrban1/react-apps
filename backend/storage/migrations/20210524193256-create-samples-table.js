const { Sequelize } = require('sequelize');

module.exports = {
  up: (queryInterface) => {
    return queryInterface
      .describeTable('samples')
      .then(async () => {
        return Promise.resolve();
      })
      .catch(async () => {
        // if error, then table doesn't exist so we create it
        const transaction = await queryInterface.sequelize.transaction();

        try {
          await queryInterface.createTable(
            'samples',
            {
              id: {
                type: Sequelize.DataTypes.UUID,
                defaultValue: Sequelize.DataTypes.UUIDV4,
                primaryKey: true,
              },
              serial: {
                type: Sequelize.DataTypes.TEXT,
                allowNull: false,
                validate: {
                  notEmpty: true,
                },
              },
              sampleIndex: {
                type: Sequelize.DataTypes.INTEGER,
                allowNull: false,
                defaultValue: 0,
              },
              featureCoverage: {
                type: Sequelize.DataTypes.TEXT,
                allowNull: true,
              },
              status: {
                type: Sequelize.DataTypes.ENUM,
                values: ['Unmeasured', 'Pass', 'Fail', 'Scrap'],
                defaultValue: 'Unmeasured',
                allowNull: false,
              },
              partId: {
                type: Sequelize.DataTypes.UUID,
                allowNull: true,
                references: {
                  model: 'parts',
                  key: 'id',
                },
              },
              jobId: {
                type: Sequelize.DataTypes.UUID,
                allowNull: true,
                references: {
                  model: 'jobs',
                  key: 'id',
                },
              },
              importHash: {
                type: Sequelize.DataTypes.STRING(255),
                allowNull: true,
                unique: true,
              },
            },
            {
              timestamps: true,
              paranoid: true,
            },
            { transaction }
          );
          await transaction.commit();
        } catch (err) {
          await transaction.rollback();
          throw err;
        }
      });
  },

  down: (queryInterface) => {
    return queryInterface
      .describeTable('samples')
      .then(() => {
        return queryInterface.dropTable('samples');
      })
      .catch(() => {
        return Promise.resolve();
      });
  },
};
