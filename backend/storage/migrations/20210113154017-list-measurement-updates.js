module.exports = {
  up: (queryInterface) => {
    return queryInterface.sequelize.query(
      `UPDATE "listItems" SET metadata = '{"measurement":"time"}' WHERE id = 'f1bffc87-c396-45b6-93c2-ee630bfcfe51';
      UPDATE "listItems" SET metadata = '{"measurement": "roughness_grade"}' WHERE id = '4af5dc77-6035-4663-bcaa-dea090785d0a';
      UPDATE "listItems" SET metadata = '{"measurement": "length"}' WHERE id = 'b262d2e7-667b-4924-a355-9cc30fa8dbc4';
      UPDATE "listItems" SET metadata = '{"measurement": "length"}' WHERE id = '5459261e-e336-4e9e-ae8a-ce87c5ca7332';
      UPDATE "listItems" SET metadata = '{"measurement": "length"}' WHERE id = 'c2111d76-7d5f-49fd-80e2-a62ac75d8a66';
      UPDATE "listItems" SET metadata = '{"measurement": "length"}' WHERE id = '5cfefd44-dd5b-42a1-b2bf-49aa0484eed4';
      UPDATE "listItems" SET metadata = '{"measurement": "force"}' WHERE id = 'b27e6b15-cc4d-40f6-8a71-aa94a1f749b0';
      UPDATE "listItems" SET metadata = '{"measurement": "electric_potential"}' WHERE id = '579e3c76-68be-4ba5-821f-58db20d4fd14';
      UPDATE "listItems" SET metadata = '{"measurement": "conductivity"}' WHERE id = '2961bfff-c11f-4ac6-8f73-9dd04ba0738e';
      UPDATE "listItems" SET metadata = '{"measurement": "temper_verification"}' WHERE id = 'd177b6a0-9bff-4610-b1cc-52d7d007b4b7';
      UPDATE "listItems" SET metadata = '{"measurement": "sound"}' WHERE id = '7f06b2ae-cc1a-49c2-9c83-8de1a4913442';
      UPDATE "listItems" SET metadata = '{"measurement": "electric_resistance"}' WHERE id = '9b282576-db5b-4f50-81bf-c66b5f9c6dd0';
      UPDATE "listItems" SET metadata = '{"measurement": "electric_charge"}' WHERE id = '2371efd2-8a12-4e59-b5f5-aa8d0638f3e1';
      UPDATE "listItems" SET metadata = '{"measurement": "electric_conductance"}' WHERE id = '5e678e2d-d54e-49f7-9752-0ab7d2f9d190';
      UPDATE "listItems" SET metadata = '{"measurement": "electric_flux"}' WHERE id = 'ea5988af-b3a1-4fbd-adc8-86bc8d6ba973';
      UPDATE "listItems" SET metadata = '{"measurement": "magnetic_field"}' WHERE id = '2cc928a9-f138-43cf-9183-fe682c5c5334';
      UPDATE "listItems" SET metadata = '{"measurement": "area"}' WHERE id = '89dcdf89-5b82-4116-b2d8-67bebfff8e1d';
      UPDATE "listItems" SET metadata = '{"measurement": "resistivity"}' WHERE id = '281991b9-6bd9-48f1-b58e-8616498015bb';
      UPDATE "listItems" SET metadata = '{"measurement": "electric_conductance"}' WHERE id = '414ed743-dde6-4de5-a77d-70618a2ed3e6';
      UPDATE "listItems" SET metadata = '{"measurement": "electric_conductance"}' WHERE id = 'a204a050-0e3a-4169-a4eb-91b91afbd49b';
      UPDATE "listItems" SET metadata = '{"measurement": "electric_field"}' WHERE id = '32e0c4fc-a1f9-4f98-83ca-3243a3b6918e';
      UPDATE "listItems" SET metadata = '{"unit":"F","type":"Imperial,Metric","measurement":"electric_capacitance"}' WHERE id = '2d1eabe1-f453-4621-be4d-c97ced674855';
      UPDATE "listItems" SET metadata = '{"unit":"A","type":"Imperial,Metric","measurement":"electric_current"}' WHERE id = '4c629e38-e5c0-43ce-a633-b3a26ffc7cb9';
      UPDATE "listItems" SET metadata = '{"unit":"N","type":"Imperial,Metric","measurement":"force"}' WHERE id = '405c573e-8cf7-4ae4-83ce-f78e11048040';
      UPDATE "listItems" SET metadata = '{"unit":"V","type":"Imperial,Metric","measurement":"electric_potential"}' WHERE id = '67489aaf-260d-4535-81ac-72e9a3780522';
      UPDATE "listItems" SET metadata = '{"unit":"Hz","type":"Imperial,Metric","measurement":"frequency"}' WHERE id = 'defda511-9d1a-4408-bcb5-6c8a2686e47b';
      UPDATE "listItems" SET metadata = '{"unit":"O","type":"Imperial,Metric","measurement":"electric_resistance"}' WHERE id = '6e70c078-bfd2-4bf7-9dca-e5f1aaead8dc';
      UPDATE "listItems" SET metadata = '{"unit":"H","type":"Imperial,Metric","measurement":"inductance"}' WHERE id = 'd5494858-717f-43dd-9368-9258b3356807';
      UPDATE "listItems" SET metadata = '{"unit":"W","type":"Imperial,Metric","measurement":"power"}' WHERE id = 'e8a47ed1-22b8-4ef0-baf9-374df6189b97';
      UPDATE "listItems" SET metadata = '{"unit":"mho","type":"Imperial,Metric","measurement":"electric_conductance"}' WHERE id = '4d9ab4c7-0ff8-4f6d-8b6d-17c44e175a7a';
      UPDATE "listItems" SET metadata = '{"unit":"S","type":"Imperial,Metric","measurement":"electric_conductance"}' WHERE id = '7ca901a9-1e7d-4be5-8e34-35b99d687329';
      UPDATE "listItems" SET metadata = '{"unit":"Wb","type":"Imperial,Metric","measurement":"magnetic_flux"}' WHERE id = 'd588f6a2-446c-4450-b7d2-2427d110d049';
      UPDATE "listItems" SET name = 'Additional Feature' WHERE id = '66308d4c-0e3b-474a-9755-18594c6db94c';
      `
    );
  },

  down: (queryInterface) => {
    return queryInterface.sequelize.query(
      `UPDATE "listItems" SET metadata = null WHERE id = 'f1bffc87-c396-45b6-93c2-ee630bfcfe51';
      UPDATE "listItems" SET metadata = null WHERE id = '4af5dc77-6035-4663-bcaa-dea090785d0a';
      UPDATE "listItems" SET metadata = null WHERE id = 'b262d2e7-667b-4924-a355-9cc30fa8dbc4';
      UPDATE "listItems" SET metadata = null WHERE id = '5459261e-e336-4e9e-ae8a-ce87c5ca7332';
      UPDATE "listItems" SET metadata = null WHERE id = 'c2111d76-7d5f-49fd-80e2-a62ac75d8a66';
      UPDATE "listItems" SET metadata = null WHERE id = '5cfefd44-dd5b-42a1-b2bf-49aa0484eed4';
      UPDATE "listItems" SET metadata = null WHERE id = 'b27e6b15-cc4d-40f6-8a71-aa94a1f749b0';
      UPDATE "listItems" SET metadata = null WHERE id = '579e3c76-68be-4ba5-821f-58db20d4fd14';
      UPDATE "listItems" SET metadata = null WHERE id = '2961bfff-c11f-4ac6-8f73-9dd04ba0738e';
      UPDATE "listItems" SET metadata = null WHERE id = 'd177b6a0-9bff-4610-b1cc-52d7d007b4b7';
      UPDATE "listItems" SET metadata = null WHERE id = '7f06b2ae-cc1a-49c2-9c83-8de1a4913442';
      UPDATE "listItems" SET metadata = null WHERE id = '9b282576-db5b-4f50-81bf-c66b5f9c6dd0';
      UPDATE "listItems" SET metadata = null WHERE id = '2371efd2-8a12-4e59-b5f5-aa8d0638f3e1';
      UPDATE "listItems" SET metadata = null WHERE id = '5e678e2d-d54e-49f7-9752-0ab7d2f9d190';
      UPDATE "listItems" SET metadata = null WHERE id = 'ea5988af-b3a1-4fbd-adc8-86bc8d6ba973';
      UPDATE "listItems" SET metadata = null WHERE id = '2cc928a9-f138-43cf-9183-fe682c5c5334';
      UPDATE "listItems" SET metadata = null WHERE id = '89dcdf89-5b82-4116-b2d8-67bebfff8e1d';
      UPDATE "listItems" SET metadata = null WHERE id = '281991b9-6bd9-48f1-b58e-8616498015bb';
      UPDATE "listItems" SET metadata = null WHERE id = '414ed743-dde6-4de5-a77d-70618a2ed3e6';
      UPDATE "listItems" SET metadata = null WHERE id = '32e0c4fc-a1f9-4f98-83ca-3243a3b6918e';
      UPDATE "listItems" SET metadata = null WHERE id = 'a204a050-0e3a-4169-a4eb-91b91afbd49b';
      UPDATE "listItems" SET metadata = '{"unit":"F","type":"Metric","measurement":"electric_capacitance"}' WHERE id = '2d1eabe1-f453-4621-be4d-c97ced674855';
      UPDATE "listItems" SET metadata = '{"unit":"A","type":"Metric","measurement":"electric_current"}' WHERE id = '4c629e38-e5c0-43ce-a633-b3a26ffc7cb9';
      UPDATE "listItems" SET metadata = '{"unit":"N","type":"Metric","measurement":"force"}' WHERE id = '405c573e-8cf7-4ae4-83ce-f78e11048040';
      UPDATE "listItems" SET metadata = '{"unit":"V","type":"Metric","measurement":"electric_potential"}' WHERE id = '67489aaf-260d-4535-81ac-72e9a3780522';
      UPDATE "listItems" SET metadata = '{"unit":"Hz","type":"Metric","measurement":"frequency"}' WHERE id = 'defda511-9d1a-4408-bcb5-6c8a2686e47b';
      UPDATE "listItems" SET metadata = '{"unit":"O","type":"Metric","measurement":"electric_resistance"}' WHERE id = '6e70c078-bfd2-4bf7-9dca-e5f1aaead8dc';
      UPDATE "listItems" SET metadata = '{"unit":"H","type":"Metric","measurement":"inductance"}' WHERE id = 'd5494858-717f-43dd-9368-9258b3356807';
      UPDATE "listItems" SET metadata = '{"unit":"W","type":"Metric","measurement":"power"}' WHERE id = 'e8a47ed1-22b8-4ef0-baf9-374df6189b97';
      UPDATE "listItems" SET metadata = '{"unit":"mho","type":"Metric","measurement":"electric_conductance"}' WHERE id = '4d9ab4c7-0ff8-4f6d-8b6d-17c44e175a7a';
      UPDATE "listItems" SET metadata = '{"unit":"Wb","type":"Metric","measurement":"magnetic_flux"}' WHERE id = 'd588f6a2-446c-4450-b7d2-2427d110d049';
      UPDATE "listItems" SET metadata = '{"unit":"S","type":"Metric","measurement":"electric_conductance"}' WHERE id = '7ca901a9-1e7d-4be5-8e34-35b99d687329';
      UPDATE "listItems" SET name = 'Additional Characteristic' WHERE id = '66308d4c-0e3b-474a-9755-18594c6db94c';
      `
    );
  },
};
