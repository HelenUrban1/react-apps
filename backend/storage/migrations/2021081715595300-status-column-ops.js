const { Sequelize } = require('sequelize');
const config = require('../../config')();

module.exports = {
  up: (queryInterface) => {
    // Not adding columns to primary database, only tenants
    if (queryInterface.sequelize.config.database === config.database.database) {
      return Promise.resolve();
    }
    return queryInterface.describeTable('operators').then((tableDefinition) => {
      if (tableDefinition.status) {
        return Promise.resolve();
      }
      return queryInterface.addColumn('operators', 'status', {
        type: Sequelize.DataTypes.STRING,
        allowNull: false,
        defaultValue: 'Active',
      });
    });
  },
  down: (queryInterface) => {
    // Not adding columns to primary database, only tenants
    if (queryInterface.sequelize.config.database === config.database.database) {
      return Promise.resolve();
    }
    return queryInterface.describeTable('operators').then((tableDefinition) => {
      if (!tableDefinition.status) {
        return Promise.resolve();
      }
      return queryInterface.removeColumn('operators', 'status');
    });
  },
};
