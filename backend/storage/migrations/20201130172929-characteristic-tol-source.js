const { Sequelize } = require('sequelize');

module.exports = {
  up: (queryInterface) => {
    return queryInterface.describeTable('characteristics').then((tableDefinition) => {
      if (tableDefinition.toleranceSource.special.includes('Document_Defined')) {
        return Promise.resolve();
      }
      return queryInterface.sequelize
        .query(`ALTER TYPE "enum_characteristics_toleranceSource" ADD VALUE 'Default_Tolerance'`)
        .then((results) => queryInterface.sequelize.query(`ALTER TYPE "enum_characteristics_toleranceSource" ADD VALUE 'Document_Defined'`))
        .then((results) => queryInterface.sequelize.query(`UPDATE characteristics SET "toleranceSource"='Document_Defined' WHERE "toleranceSource"='Captured'`))
        .then((results) => queryInterface.sequelize.query(`UPDATE characteristics SET "toleranceSource"='Default_Tolerance' WHERE "toleranceSource"='Inherited'`))
        .then((results) =>
          queryInterface.sequelize.query(`
            DELETE 
            FROM
                pg_enum
            WHERE
                enumlabel = 'Captured' AND
                enumtypid = (
                    SELECT
                        oid
                    FROM
                        pg_type
                    WHERE
                        typname = 'enum_characteristics_toleranceSource'
                )
          `)
        )
        .then((results) =>
          queryInterface.sequelize.query(`
            DELETE 
            FROM
                pg_enum
            WHERE
                enumlabel = 'Inherited' AND
                enumtypid = (
                    SELECT
                        oid
                    FROM
                        pg_type
                    WHERE
                        typname = 'enum_characteristics_toleranceSource'
                )
          `)
        );
    });
  },
  down: (queryInterface) => {
    return queryInterface.describeTable('characteristics').then((tableDefinition) => {
      if (tableDefinition.toleranceSource.special.includes('Captured')) {
        return Promise.resolve();
      }
      return queryInterface.sequelize
        .query(`ALTER TYPE "enum_characteristics_toleranceSource" ADD VALUE 'Captured'`)
        .then((results) => queryInterface.sequelize.query(`ALTER TYPE "enum_characteristics_toleranceSource" ADD VALUE 'Inherited'`))
        .then((results) => queryInterface.sequelize.query(`UPDATE characteristics SET "toleranceSource"='Captured' WHERE "toleranceSource"='Document_Defined'`))
        .then((results) => queryInterface.sequelize.query(`UPDATE characteristics SET "toleranceSource"='Inherited' WHERE "toleranceSource"='Default_Tolerance'`))
        .then((results) =>
          queryInterface.sequelize.query(`
          DELETE 
          FROM
              pg_enum
          WHERE
              enumlabel = 'Document_Defined' AND
              enumtypid = (
                  SELECT
                      oid
                  FROM
                      pg_type
                  WHERE
                      typname = 'enum_characteristics_toleranceSource'
              )
        `)
        )
        .then((results) =>
          queryInterface.sequelize.query(`
          DELETE 
          FROM
              pg_enum
          WHERE
              enumlabel = 'Default_Tolerance' AND
              enumtypid = (
                  SELECT
                      oid
                  FROM
                      pg_type
                  WHERE
                      typname = 'enum_characteristics_toleranceSource'
              )
        `)
        );
    });
  },
};
