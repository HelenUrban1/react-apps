const { Sequelize } = require('sequelize');

module.exports = {
  up: (queryInterface) => {
    return queryInterface.describeTable('samples').then(async (tableDefinition) => {
      if (tableDefinition.createdAt) {
        return Promise.resolve();
      }
      // if error, then table doesn't exist so we create it
      const transaction = await queryInterface.sequelize.transaction();

      try {
        await queryInterface.addColumn(
          'samples',
          'updatingOperator',
          {
            type: Sequelize.DataTypes.STRING,
            allowNull: true,
            references: {
              model: 'operators',
              key: 'id',
            },
          },
          { transaction }
        );
        await transaction.commit();
      } catch (err) {
        await transaction.rollback();
        throw err;
      }
    });
  },
  down: (queryInterface) => {
    return queryInterface.describeTable('samples').then(async (tableDefinition) => {
      if (!tableDefinition.updatingOperator) {
        return Promise.resolve();
      }
      return queryInterface.removeColumn('samples', 'updatingOperator');
    });
  },
};
