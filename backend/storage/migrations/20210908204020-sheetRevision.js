const { Sequelize, QueryTypes } = require('sequelize');

module.exports = {
  up: (queryInterface) => {
    return queryInterface.describeTable('drawingSheets').then(async (tableDefinition) => {
      if (!tableDefinition.originalSheet) {
        await queryInterface.sequelize.transaction((t) => {
          return Promise.all([
            queryInterface.addColumn(
              'drawingSheets',
              'originalSheet',
              {
                type: Sequelize.DataTypes.UUID,
                allowNull: true,
                references: {
                  model: 'drawingSheets',
                  key: 'id',
                },
              },
              { transaction: t }
            ),
            queryInterface.addColumn(
              'drawingSheets',
              'previousRevision',
              {
                type: Sequelize.DataTypes.UUID,
                allowNull: true,
                references: {
                  model: 'drawingSheets',
                  key: 'id',
                },
              },
              { transaction: t }
            ),
          ]);
        });
      }
      return queryInterface.sequelize
        .query('SELECT * FROM "drawingSheets" WHERE "originalSheet" IS NULL', {
          type: QueryTypes.SELECT,
        })
        .then((result) => {
          if (result.length === 0) {
            return Promise.resolve();
          }
          return Promise.all(
            result.map((p) => {
              const { id } = p;
              return queryInterface.sequelize.query('UPDATE "drawingSheets" SET "originalSheet" = :id WHERE "id" = :id', {
                replacements: {
                  id,
                },
                type: QueryTypes.UPDATE,
              });
            })
          );
        });
    });
  },
  down: (queryInterface) => {
    return Promise.all([queryInterface.removeColumn('drawingSheets', 'originalSheet'), queryInterface.removeColumn('drawingSheets', 'previousRevision')]);
  },
};
