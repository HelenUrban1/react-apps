const fs = require('fs');
const path = require('path');
// const { log } = require('../../src/logger');

const DB_SETUP_SQL_FILEPATH = path.resolve(__dirname, './db-first-migration.sql');

module.exports = {
  up: (queryInterface) => {
    let result;
    try {
      // log.debug('Loading SQL from: ', DB_SETUP_SQL_FILEPATH);
      const dbSetupSql = fs.readFileSync(DB_SETUP_SQL_FILEPATH, 'utf8').toString();
      result = queryInterface.sequelize.query(dbSetupSql.toString());
    } catch (error) {
      result = error;
      // log.error(error);
    }
    return result;
  },

  // DB Setup and Teardown files
  // down: (queryInterface) => {
  //    return queryInterface.sequelize.query('');
  // }
};
