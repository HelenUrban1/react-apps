const { Sequelize } = require('sequelize');

module.exports = {
  up: (queryInterface) => {
    return queryInterface.describeTable('subscriptions').then((tableDefinition) => {
      if (tableDefinition.billingId) {
        return Promise.resolve();
      }
      return queryInterface.sequelize.transaction((t) => {
        return Promise.all([
          queryInterface.addColumn(
            'subscriptions',
            'billingId',
            {
              type: Sequelize.DataTypes.TEXT,
              allowNull: false,
            },
            { transaction: t }
          ),
          queryInterface.addColumn(
            'subscriptions',
            'cardId',
            {
              type: Sequelize.DataTypes.INTEGER,
              allowNull: true,
            },
            { transaction: t }
          ),
          queryInterface.addColumn(
            'subscriptions',
            'maskedCardNumber',
            {
              type: Sequelize.DataTypes.TEXT,
              allowNull: true,
            },
            { transaction: t }
          ),
          queryInterface.addColumn(
            'subscriptions',
            'cardType',
            {
              type: Sequelize.DataTypes.TEXT,
              allowNull: true,
            },
            { transaction: t }
          ),
          queryInterface.addColumn(
            'subscriptions',
            'expirationMonth',
            {
              type: Sequelize.DataTypes.INTEGER,
              allowNull: true,
            },
            { transaction: t }
          ),
          queryInterface.addColumn(
            'subscriptions',
            'expirationYear',
            {
              type: Sequelize.DataTypes.INTEGER,
              allowNull: true,
            },
            { transaction: t }
          ),
          queryInterface.addColumn(
            'subscriptions',
            'customerId',
            {
              type: Sequelize.DataTypes.TEXT,
              allowNull: true,
            },
            { transaction: t }
          ),
        ]);
      });
    });
  },

  down: (queryInterface) => {
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
        queryInterface.removeColumn('subscriptions', 'billingId', { transaction: t }),
        queryInterface.removeColumn('subscriptions', 'cardId', { transaction: t }),
        queryInterface.removeColumn('subscriptions', 'maskedCardNumber', { transaction: t }),
        queryInterface.removeColumn('subscriptions', 'cardType', { transaction: t }),
        queryInterface.removeColumn('subscriptions', 'expirationMonth', { transaction: t }),
        queryInterface.removeColumn('subscriptions', 'expirationYear', { transaction: t }),
        queryInterface.removeColumn('subscriptions', 'customerId', { transaction: t }),
      ]);
    });
  },
};
