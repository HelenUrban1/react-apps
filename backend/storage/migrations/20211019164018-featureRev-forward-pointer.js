const { Sequelize } = require('sequelize');

module.exports = {
  up: (queryInterface) => {
    return queryInterface.describeTable('characteristics').then(async (tableDefinition) => {
      if (!tableDefinition.nextRevision) {
        await queryInterface.sequelize.transaction((t) => {
          return Promise.all([
            queryInterface.addColumn(
              'characteristics',
              'nextRevision',
              {
                type: Sequelize.DataTypes.UUID,
                allowNull: true,
                references: {
                  model: 'characteristics',
                  key: 'id',
                },
              },
              { transaction: t }
            ),
          ]);
        });
      }
    });
  },
  down: (queryInterface) => {
    return Promise.all([queryInterface.removeColumn('characteristics', 'nextRevision')]);
  },
};
