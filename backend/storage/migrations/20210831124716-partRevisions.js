const { Sequelize, QueryTypes } = require('sequelize');

module.exports = {
  up: (queryInterface) => {
    return queryInterface.describeTable('parts').then(async (tableDefinition) => {
      if (!tableDefinition.originalPart) {
        await queryInterface.sequelize.transaction((t) => {
          return Promise.all([
            queryInterface.addColumn(
              'parts',
              'originalPart',
              {
                type: Sequelize.DataTypes.UUID,
                allowNull: true,
                references: {
                  model: 'parts',
                  key: 'id',
                },
              },
              { transaction: t }
            ),
            queryInterface.addColumn(
              'parts',
              'previousRevision',
              {
                type: Sequelize.DataTypes.UUID,
                allowNull: true,
                references: {
                  model: 'parts',
                  key: 'id',
                },
              },
              { transaction: t }
            ),
          ]);
        });
      }
      return queryInterface.sequelize
        .query('SELECT * FROM "parts" WHERE "originalPart" IS NULL', {
          type: QueryTypes.SELECT,
        })
        .then((result) => {
          if (result.length === 0) {
            return Promise.resolve();
          }
          return Promise.all(
            result.map((p) => {
              const { id } = p;
              return queryInterface.sequelize.query('UPDATE "parts" SET "originalPart" = :id WHERE "id" = :id', {
                replacements: {
                  id,
                },
                type: QueryTypes.UPDATE,
              });
            })
          );
        });
    });
  },
  down: (queryInterface) => {
    return Promise.all([queryInterface.removeColumn('parts', 'originalPart'), queryInterface.removeColumn('parts', 'previousRevision')]);
  },
};
