'use strict';
const { DataTypes } = require('sequelize');

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.changeColumn('drawingSheets', 'foundCaptureCount', {
      type: DataTypes.INTEGER,
      allowNull: true,
      defaultValue: null,
    });
  },

  async down(queryInterface, Sequelize) {
    await queryInterface.changeColumn('drawingSheets', 'foundCaptureCount', {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0,
    });
  },
};
