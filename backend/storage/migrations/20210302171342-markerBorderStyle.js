const { Sequelize } = require('sequelize');

module.exports = {
  up: (queryInterface) => {
    return queryInterface.describeTable('markers').then((tableDefinition) => {
      if (!tableDefinition.borderStyle) {
        return Promise.resolve();
      }
      return Promise.all([
        queryInterface.sequelize.query(`UPDATE markers SET "borderStyle"='Solid' WHERE "markerStyleName"='Default'`),
        queryInterface.sequelize.query(`ALTER TABLE markers ALTER COLUMN "borderStyle" DROP DEFAULT`).then(() => {
          queryInterface.sequelize.query(`ALTER TABLE markers ALTER "borderStyle" SET DEFAULT 'Solid'`);
        }),
      ]);
    });
  },
  down: (queryInterface) => {
    return queryInterface.describeTable('markers').then((tableDefinition) => {
      if (!tableDefinition.borderStyle) {
        return Promise.resolve();
      }
      return queryInterface.sequelize.query(`ALTER TABLE markers ALTER COLUMN "borderStyle" DROP DEFAULT`).then(() => {
        queryInterface.sequelize.query(`ALTER TABLE markers ALTER "borderStyle" SET DEFAULT 'Dotted'`);
      });
    });
  },
};
