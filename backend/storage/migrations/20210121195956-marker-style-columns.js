const { Sequelize } = require('sequelize');
const config = require('../../config')();

module.exports = {
  up: (queryInterface) => {
    // Not adding columns to primary database, only tenants
    if (queryInterface.sequelize.config.database === config.database.database) {
      return Promise.resolve();
    }
    return queryInterface.describeTable('markers').then((tableDefinition) => {
      if (tableDefinition.leaderLineStyle) {
        return Promise.resolve();
      }
      return Promise.all([
        queryInterface.sequelize.query('DROP TYPE IF EXISTS "public"."enum_markers_leaderLineStyle"'), //
        queryInterface.sequelize.query('DROP TYPE IF EXISTS "public"."enum_markers_borderStyle"'),
      ]).then(
        Promise.all([
          queryInterface.addColumn('markers', 'leaderLineStyle', { type: Sequelize.DataTypes.ENUM, values: ['Solid', 'Dashed', 'Dotted'], allowNull: true, defaultValue: 'Solid' }).then((results) => queryInterface.sequelize.query(`UPDATE markers SET "leaderLineStyle" = 'Solid' WHERE "leaderLineStyle" IS NULL`)), //
          queryInterface.addColumn('markers', 'borderStyle', { type: Sequelize.DataTypes.ENUM, values: ['Solid', 'Dashed', 'Dotted'], allowNull: true, defaultValue: 'Dotted' }).then((results) => queryInterface.sequelize.query(`UPDATE markers SET "borderStyle" = 'Solid' WHERE "borderStyle" IS NULL`)),
        ])
      );
    });
  },
  down: (queryInterface) => {
    return queryInterface.describeTable('markers').then((tableDefinition) => {
      if (!tableDefinition.borderStyle) {
        return Promise.resolve();
      }
      return Promise.all([queryInterface.removeColumn('markers', 'leaderLineStyle').then(() => queryInterface.sequelize.query('DROP TYPE IF EXISTS "public"."enum_markers_leaderLineStyle"')), queryInterface.removeColumn('markers', 'borderStyle')]);
    });
  },
};
