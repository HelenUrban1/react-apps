const { Sequelize } = require('sequelize');

module.exports = {
  up: (queryInterface) => {
    return queryInterface
      .describeTable('jobParts')
      .then(async () => {
        return Promise.resolve();
      })
      .catch(async () => {
        // if error, then table doesn't exist so we create it
        const transaction = await queryInterface.sequelize.transaction();

        try {
          await queryInterface.createTable(
            'jobParts',
            {
              partId: {
                type: Sequelize.DataTypes.UUID,
                allowNull: true,
                references: {
                  model: 'parts',
                  key: 'id',
                },
              },
              jobId: {
                type: Sequelize.DataTypes.UUID,
                allowNull: true,
                references: {
                  model: 'jobs',
                  key: 'id',
                },
              },
              createdAt: {
                type: Sequelize.DataTypes.DATE,
                allowNull: true,
              },
              updatedAt: {
                type: Sequelize.DataTypes.DATE,
                allowNull: true,
              },
              deletedAt: {
                type: Sequelize.DataTypes.DATE,
                allowNull: true,
              },
            },
            {
              timestamps: true,
              paranoid: true,
            },
            { transaction }
          );
          await transaction.commit();
        } catch (err) {
          await transaction.rollback();
          throw err;
        }
      });
  },

  down: (queryInterface) => {
    return queryInterface
      .describeTable('jobParts')
      .then(() => {
        return queryInterface.dropTable('jobParts');
      })
      .catch(() => {
        return Promise.resolve();
      });
  },
};
