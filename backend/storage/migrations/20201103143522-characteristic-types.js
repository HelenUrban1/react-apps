const { Sequelize } = require('sequelize');
const { defaultTypeIDs } = require('../../src/staticData/defaults');

module.exports = {
  up: (queryInterface) => {
    return queryInterface.describeTable('characteristics').then((tableDefinition) => {
      if (tableDefinition.notationType.type === Sequelize.DataTypes.TEXT) {
        return Promise.resolve();
      }
      return queryInterface
        .addColumn('characteristics', 'tempType', { type: Sequelize.DataTypes.TEXT, defaultValue: 'fa3284ed-ad3e-43c9-b051-87bd2e95e927', allowNull: false })
        .then((results) => queryInterface.sequelize.query('UPDATE characteristics SET "tempType" = "notationType"'))
        .then((results) => queryInterface.removeColumn('characteristics', 'notationType'))
        .then((results) => queryInterface.renameColumn('characteristics', 'tempType', 'notationType'))
        .then((results) =>
          Promise.all(
            Object.keys(defaultTypeIDs).map((type) => {
              return queryInterface.sequelize.query(`UPDATE characteristics SET "notationType"='${defaultTypeIDs[type]}' WHERE "notationType"='${type.replace(' ', '_')}'`);
            })
          )
        );
    });
  },
  down: (queryInterface) => {
    return queryInterface.describeTable('characteristics').then((tableDefinition) => {
      if (tableDefinition.notationType.type === Sequelize.DataTypes.ENUM) {
        return Promise.resolve();
      }
      return queryInterface
        .addColumn('characteristics', 'tempType', { type: Sequelize.DataTypes.ENUM, defaultValue: 'Note', allowNull: false, values: ['Dimension', 'Geometric_Tolerance', 'Surface_Finish', 'Weld', 'Note', 'Other'] })
        .then((results) => queryInterface.sequelize.query('UPDATE characteristics SET "tempType" = "notationType"'))
        .then((results) => queryInterface.removeColumn('characteristics', 'notationType'))
        .then((results) => queryInterface.renameColumn('characteristics', 'tempType', 'notationType'))
        .then((results) =>
          Promise.all(
            Object.keys(defaultTypeIDs).map((type) => {
              return queryInterface.sequelize.query(`UPDATE characteristics SET "notationType"='${type.replace(' ', '_')}' WHERE "notationType"='${defaultTypeIDs[type]}'`);
            })
          )
        );
    });
  },
};
