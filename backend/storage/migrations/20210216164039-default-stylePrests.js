const { Sequelize } = require('sequelize');
const config = require('../../config')();

module.exports = {
  up: (queryInterface) => {
    // Not loading data into primary database, only tenants
    if (queryInterface.sequelize.config.database === config.database.database) {
      return Promise.resolve();
    }
    return queryInterface.sequelize.query('SELECT * FROM "settings" WHERE id = \'9acfc012-9859-4364-a22b-5dd17637cca4\'').then((result) => {
      // If entry already exists return
      if (result[0].length > 0) {
        return Promise.resolve();
      }
      return queryInterface.bulkInsert(
        'settings',
        [
          {
            id: '9acfc012-9859-4364-a22b-5dd17637cca4',
            name: 'stylePresets',
            value: 'default',
            metadata: '{"Default":{"customers":[],"setting":{"default":{"style":"f12c5566-7612-48e9-9534-ef90b276ebaa","assign":["Default"]}}}}',
            siteId: null,
            userId: null,
            createdAt: Sequelize.literal('NOW()'),
            updatedAt: Sequelize.literal('NOW()'),
          },
        ],
        {}
      );
    });
  },

  down: (queryInterface) => {
    if (queryInterface.sequelize.config.database === config.database.database) {
      return Promise.resolve();
    }
    return queryInterface.sequelize.query('SELECT * FROM "settings" WHERE id = \'9acfc012-9859-4364-a22b-5dd17637cca4\'').then((result) => {
      // Entry already doesn't exist
      if (result[0].length === 0) {
        return Promise.resolve();
      }
      return queryInterface.bulkDelete(
        'settings',
        [
          {
            id: '9acfc012-9859-4364-a22b-5dd17637cca4',
          },
        ],
        {}
      );
    });
  },
};
