const { Sequelize, QueryTypes } = require('sequelize');

module.exports = {
  up: (queryInterface) => {
    // Changes seeded user passwords to Testing12!
    return queryInterface.sequelize.query(`UPDATE "users" SET password = '$2a$12$OuRpvUaQpwJOT7tBfPLoTOVdBvykJChTI5hCA1WACmDcSCoUwABfm' WHERE password = '$2b$12$rvwQlGp8yvE6ChdsmO1Vr.qkNFGEgdZRXsUAFOrFEzJGyVQn2A2SW';
    UPDATE "users" SET password = '$2a$12$OuRpvUaQpwJOT7tBfPLoTOVdBvykJChTI5hCA1WACmDcSCoUwABfm' WHERE email LIKE '%inspectionxperttesters%';`);
  },

  down: (queryInterface) => {
    return Promise.resolve();
  },
};
