const { Sequelize } = require('sequelize');
const config = require('../../config')();

module.exports = {
  up: (queryInterface) => {
    // Not adding columns to primary database, only tenants
    if (queryInterface.sequelize.config.database === config.database.database) {
      return Promise.resolve();
    }
    return queryInterface.describeTable('drawingSheets').then((tableDefinition) => {
      if (tableDefinition.pageName) {
        return Promise.resolve();
      }
      return queryInterface.addColumn('drawingSheets', 'pageName', {
        type: Sequelize.DataTypes.TEXT,
        allowNull: false,
        defaultValue: '',
      });
    });
  },
  down: (queryInterface) => {
    // Not adding columns to primary database, only tenants
    if (queryInterface.sequelize.config.database === config.database.database) {
      return Promise.resolve();
    }
    return queryInterface.describeTable('drawingSheets').then((tableDefinition) => {
      if (!tableDefinition.pageName) {
        return Promise.resolve();
      }
      return queryInterface.removeColumn('drawingSheets', 'pageName');
    });
  },
};
