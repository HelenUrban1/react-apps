const { Sequelize } = require('sequelize');

module.exports = {
  up: (queryInterface) => {
    return queryInterface.describeTable('characteristics').then((tableDefinition) => {
      if (tableDefinition.balloonGridCoordinates || tableDefinition.connectionPointGridCoordinates) {
        return Promise.resolve();
      }
      return queryInterface.sequelize.transaction((t) => {
        return Promise.all([
          queryInterface.addColumn(
            'characteristics',
            'balloonGridCoordinates',
            {
              type: Sequelize.DataTypes.TEXT,
            },
            { transaction: t }
          ),
          queryInterface.addColumn(
            'characteristics',
            'connectionPointGridCoordinates',
            {
              type: Sequelize.DataTypes.TEXT,
            },
            { transaction: t }
          ),
        ]);
      });
    });
  },

  down: (queryInterface) => {
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([queryInterface.removeColumn('characteristics', 'balloonGridCoordinates', { transaction: t }), queryInterface.removeColumn('characteristics', 'connectionPointGridCoordinates', { transaction: t })]);
    });
  },
};
