const { Sequelize } = require('sequelize');
const moment = require('moment');

module.exports = {
  up: (queryInterface) => {
    return queryInterface.describeTable('subscriptions').then((tableDefinition) => {
      if (tableDefinition.termsAcceptedOn) {
        return Promise.resolve();
      }
      return queryInterface.addColumn('subscriptions', 'termsAcceptedOn', {
        //
        type: Sequelize.DataTypes.DATEONLY,
        allowNull: true,
        get() {
          return this.getDataValue('termsAcceptedOn') ? moment.utc(this.getDataValue('termsAcceptedOn')).format('YYYY-MM-DD') : null;
        },
      });
    });
  },
  down: (queryInterface) => {
    return queryInterface.describeTable('subscriptions').then((tableDefinition) => {
      if (!tableDefinition.termsAcceptedOn) {
        return Promise.resolve();
      }

      return queryInterface.removeColumn('subscriptions', 'termsAcceptedOn');
    });
  },
};
