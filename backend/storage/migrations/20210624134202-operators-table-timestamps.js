const { Sequelize } = require('sequelize');

module.exports = {
  up: (queryInterface) => {
    return queryInterface.describeTable('operators').then(async (tableDefinition) => {
      if (tableDefinition.createdAt) {
        return Promise.resolve();
      }

      const transaction = await queryInterface.sequelize.transaction();

      try {
        await Promise.all([
          queryInterface.addColumn('operators', 'createdAt', { type: Sequelize.DataTypes.DATE, allowNull: true }, { transaction }),
          queryInterface.addColumn('operators', 'updatedAt', { type: Sequelize.DataTypes.DATE, allowNull: true }, { transaction }),
          queryInterface.addColumn('operators', 'deletedAt', { type: Sequelize.DataTypes.DATE, allowNull: true }, { transaction }),
        ]);

        await transaction.commit();
      } catch (err) {
        await transaction.rollback();
        throw err;
      }
    });
  },
  down: (queryInterface) => {
    return queryInterface.describeTable('operators').then(async (tableDefinition) => {
      if (!tableDefinition.createdAt) {
        return Promise.resolve();
      }
      return Promise.all([queryInterface.removeColumn('operators', 'createdAt'), queryInterface.removeColumn('operators', 'deletedAt'), queryInterface.removeColumn('operators', 'updatedAt')]);
    });
  },
};
