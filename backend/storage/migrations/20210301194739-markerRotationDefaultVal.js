const { Sequelize } = require('sequelize');

module.exports = {
  up: (queryInterface) => {
    queryInterface.sequelize.query(`UPDATE characteristics SET "markerRotation"=0 WHERE "markerRotation" IS NULL`);
    return queryInterface.sequelize.transaction((t) => {
      return queryInterface.changeColumn(
        'characteristics',
        'markerRotation',
        {
          //
          type: Sequelize.DataTypes.INTEGER,
          defaultValue: 0,
        },
        { transaction: t }
      );
    });
  },

  down: () => {
    return Promise.resolve();
  },
};
