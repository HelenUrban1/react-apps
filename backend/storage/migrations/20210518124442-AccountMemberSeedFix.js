const { QueryTypes, literal } = require('sequelize');

module.exports = {
  up: async (queryInterface) => {
    const ACME_CORP_ACCOUNT_ID = 'e0a01769-c8ed-4090-bd24-9c1b2241fb75'; // This Account is created in *-ixc-account-users.js
    const ACME_CORP_SITE_ID = '31ef1d56-1b86-4ffa-b886-507ed8f021c5';
    return queryInterface.sequelize
      .query('SELECT * FROM "accountMembers" WHERE "accountId" = :ACME_CORP_ACCOUNT_ID AND "siteId" = :ACME_CORP_SITE_ID', {
        replacements: {
          ACME_CORP_ACCOUNT_ID,
          ACME_CORP_SITE_ID,
        },
        type: QueryTypes.SELECT,
      })
      .then((result) => {
        if (result.length === 0) {
          return Promise.resolve();
        }
        return Promise.all(
          result.map((am) => {
            if (am.roles.includes(literal(`ARRAY['Owner']::"enum_accountMembers_roles"[]`))) {
              // Delete old Owner AccountMemberships for QA Account
              return queryInterface.sequelize.query('UPDATE "accountMembers" SET "status" = "Archived" WHERE "id" = :id', {
                replacements: {
                  id: am.id,
                },
                type: QueryTypes.UPDATE,
              });
            }
            return Promise.resolve();
          })
        );
      });
  },

  down: async (queryInterface) => {
    const ACME_CORP_ACCOUNT_ID = 'e0a01769-c8ed-4090-bd24-9c1b2241fb75'; // This Account is created in *-ixc-account-users.js
    const ACME_CORP_SITE_ID = '31ef1d56-1b86-4ffa-b886-507ed8f021c5';
    return queryInterface.sequelize
      .query('SELECT * FROM "accountMembers" WHERE "accountId" = :ACME_CORP_ACCOUNT_ID AND "siteId" = :ACME_CORP_SITE_ID', {
        replacements: {
          ACME_CORP_ACCOUNT_ID,
          ACME_CORP_SITE_ID,
        },
        type: QueryTypes.SELECT,
      })
      .then((result) => {
        if (result.length === 0) {
          return Promise.resolve();
        }
        return Promise.all(
          result.map((am) => {
            if (am.roles.includes(literal(`ARRAY['Owner']::"enum_accountMembers_roles"[]`)) && am.status === 'Archived') {
              // restore old Owner AccountMemberships for QA Account
              return queryInterface.sequelize.query('UPDATE "accountMembers" SET "status" = "Active" WHERE "id" = :id', {
                replacements: {
                  id: am.id,
                },
                type: QueryTypes.UPDATE,
              });
            }
            return Promise.resolve();
          })
        );
      });
  },
};
