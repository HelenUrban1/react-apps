'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface) {
    const transaction = await queryInterface.sequelize.transaction();
    try {
      await queryInterface.sequelize.query(`UPDATE characteristics SET "notationClass" = 'N_A', "toleranceSource" = 'N_A' WHERE "notationType" = 'fa3284ed-ad3e-43c9-b051-87bd2e95e927'`); // fa3284ed-ad3e-43c9-b051-87bd2e95e927 is the Note type
      await transaction.commit();
    } catch (error) {
      await transaction.rollback();
    }
  },

  async down(queryInterface) {
    const transaction = await queryInterface.sequelize.transaction();
    try {
      await queryInterface.sequelize.query(`UPDATE characteristics SET "notationClass" = 'Tolerance', "toleranceSource" = 'Default_Tolerance' WHERE "notationType" = 'fa3284ed-ad3e-43c9-b051-87bd2e95e927'`); // fa3284ed-ad3e-43c9-b051-87bd2e95e927 is the Note type
      await transaction.commit();
    } catch (error) {
      await transaction.rollback();
    }
  },
};
