const { Sequelize } = require('sequelize');
const config = require('../../config')();

module.exports = {
  up: (queryInterface) => {
    // Not adding columns to primary database, only tenants
    if (queryInterface.sequelize.config.database === config.database.database) {
      return Promise.resolve();
    }
    return queryInterface.describeTable('markers').then((tableDefinition) => {
      if (tableDefinition.leaderLineStyle) {
        return Promise.resolve();
      }
      return queryInterface.addColumn('markers', 'leaderLineStyle', { type: Sequelize.DataTypes.ENUM, values: ['Solid', 'Dashed', 'Dotted'], allowNull: true, defaultValue: 'Solid' }).then((results) => queryInterface.sequelize.query(`UPDATE markers SET "leaderLineStyle" = 'Solid' WHERE "leaderLineStyle" IS NULL`));
    });
  },
  down: (queryInterface) => {
    return queryInterface.describeTable('markers').then((tableDefinition) => {
      if (!tableDefinition.leaderLineStyle) {
        return Promise.resolve();
      }

      return queryInterface.removeColumn('markers', 'leaderLineStyle').then(() => queryInterface.sequelize.query('DROP TYPE IF EXISTS "public"."enum_markers_leaderLineStyle"'));
    });
  },
};
