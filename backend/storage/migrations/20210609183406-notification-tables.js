const { Sequelize } = require('sequelize');

module.exports = {
  up: (queryInterface) => {
    return queryInterface
      .describeTable('notifications')
      .then(async () => {
        return queryInterface.describeTable('notificationUserStatuses').then(async () => {
          return Promise.resolve();
        });
      })
      .catch(async () => {
        // if error, then table doesn't exist so we create it
        const transaction = await queryInterface.sequelize.transaction();

        try {
          await queryInterface.createTable(
            'notifications',
            {
              id: {
                type: Sequelize.DataTypes.UUID,
                defaultValue: Sequelize.DataTypes.UUIDV4,
                primaryKey: true,
              },
              title: {
                type: Sequelize.DataTypes.STRING(255),
                allowNull: true,
              },
              content: {
                type: Sequelize.DataTypes.TEXT,
                allowNull: true,
              },
              action: {
                type: Sequelize.DataTypes.STRING(255),
                allowNull: true,
              },
              actionAt: {
                type: Sequelize.DataTypes.DATE,
                allowNull: true,
              },
              priority: {
                type: Sequelize.DataTypes.INTEGER,
                allowNull: false,
                defaultValue: 0,
              },
              relationId: {
                type: Sequelize.DataTypes.UUID,
                allowNull: true,
              },
              relationType: {
                type: Sequelize.DataTypes.STRING(30),
                allowNull: true,
              },
              actorId: {
                type: Sequelize.DataTypes.UUID,
                allowNull: true,
              },
              siteId: {
                type: Sequelize.DataTypes.UUID,
                allowNull: true,
              },
              createdById: {
                type: Sequelize.DataTypes.UUID,
                allowNull: true,
              },
              updatedById: {
                type: Sequelize.DataTypes.UUID,
                allowNull: true,
              },
              createdAt: {
                type: Sequelize.DataTypes.DATE,
                allowNull: false,
              },
              updatedAt: {
                type: Sequelize.DataTypes.DATE,
                allowNull: false,
              },
              deletedAt: {
                type: Sequelize.DataTypes.DATE,
                allowNull: true,
              },
            },
            {
              timestamps: true,
              paranoid: true,
            },
            { transaction }
          );
          await queryInterface.createTable(
            'notificationUserStatuses',
            {
              id: {
                type: Sequelize.DataTypes.UUID,
                defaultValue: Sequelize.DataTypes.UUIDV4,
                primaryKey: true,
              },
              readAt: {
                type: Sequelize.DataTypes.DATE,
                allowNull: true,
              },
              archivedAt: {
                type: Sequelize.DataTypes.DATE,
                allowNull: true,
              },
              notificationId: {
                type: Sequelize.DataTypes.UUID,
                allowNull: true,
                references: {
                  model: 'notifications',
                  key: 'id',
                },
              },
              userId: {
                type: Sequelize.DataTypes.UUID,
                allowNull: true,
              },
              createdAt: {
                type: Sequelize.DataTypes.DATE,
                allowNull: false,
              },
              updatedAt: {
                type: Sequelize.DataTypes.DATE,
                allowNull: false,
              },
              deletedAt: {
                type: Sequelize.DataTypes.DATE,
                allowNull: true,
              },
            },
            {
              timestamps: true,
              paranoid: true,
            },
            { transaction }
          );
          await transaction.commit();
        } catch (err) {
          await transaction.rollback();
          throw err;
        }
      });
  },

  down: (queryInterface) => {
    return queryInterface
      .describeTable('notificationUserStatuses')
      .then(() => {
        return queryInterface.dropTable('notificationUserStatuses').then(() => {
          return queryInterface.dropTable('notifications');
        });
      })
      .catch(() => {
        return Promise.resolve();
      });
  },
};
