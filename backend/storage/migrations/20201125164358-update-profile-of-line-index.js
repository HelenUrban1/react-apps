const { Sequelize } = require('sequelize');
const config = require('../../config')();

module.exports = {
  up: (queryInterface) => {
    // Not loading list data into primary database, only tenants
    if (queryInterface.sequelize.config.database === config.database.database) {
      return Promise.resolve();
    }
    return queryInterface.sequelize.query('SELECT * FROM "listItems" WHERE id = \'1e36505f-e05e-4796-a261-63701ad07b57\'').then((result) => {
      // If profile of a line already exist, update the index
      if (result[0].length > 0) {
        return queryInterface.sequelize.query('UPDATE "listItems" SET "itemIndex" = 13 WHERE id = \'1e36505f-e05e-4796-a261-63701ad07b57\'');
      }
      return queryInterface.bulkInsert(
        'listItems',
        [
          {
            id: '1e36505f-e05e-4796-a261-63701ad07b57',
            name: 'Profile of a Line',
            metadata: '{"measurement": "Length"}',
            default: true,
            status: 'Active',
            listType: 'Type',
            itemIndex: 13,
            parentId: 'f3c4d8e0-fb79-4c72-a3f8-3f584ea075a7',
            siteId: null,
            userId: null,
            createdById: null,
            deletedAt: null,
            updatedById: null,
            createdAt: Sequelize.literal('NOW()'),
            updatedAt: Sequelize.literal('NOW()'),
          },
        ],
        {}
      );
    });
  },

  down: (queryInterface) => {
    // Not loading list data into primary database, only tenants
    if (queryInterface.sequelize.config.database === config.database.database) {
      return Promise.resolve();
    }
    return queryInterface.sequelize.query('UPDATE listItems SET "itemIndex" = 14 WHERE id = \'1e36505f-e05e-4796-a261-63701ad07b57\'');
  },
};
