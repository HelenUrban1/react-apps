const { QueryTypes } = require('sequelize');

module.exports = {
  up: (queryInterface) => {
    return queryInterface.sequelize.query(`UPDATE "settings" SET "metadata" = replace("metadata", 'minus":"', 'minus":"-') WHERE "name" = 'tolerancePresets' AND NOT "metadata" LIKE '%minus":"-%'`, {
      type: QueryTypes.UPDATE,
    });
  },
  down: (queryInterface) => {
    return queryInterface.sequelize.query(`UPDATE "settings" SET "metadata" = replace("metadata", 'minus":"-', 'minus":"') WHERE "name" = 'tolerancePresets' AND "metadata" LIKE '%minus":"-%'`, {
      type: QueryTypes.UPDATE,
    });
  },
};
