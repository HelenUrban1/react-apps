const { Sequelize } = require('sequelize');

module.exports = {
  up: (queryInterface) => {
    return queryInterface
      .describeTable('reviewTasks')
      .then(() => {
        return Promise.resolve();
      })
      .catch(() => {
        // if error, then table doesn't exist so we create it
        return queryInterface.createTable(
          'reviewTasks',
          {
            id: {
              type: Sequelize.DataTypes.UUID,
              defaultValue: Sequelize.DataTypes.UUIDV4,
              primaryKey: true,
            },
            type: {
              type: Sequelize.DataTypes.ENUM,
              values: ['Capture', 'Grid'],
            },
            status: {
              type: Sequelize.DataTypes.ENUM,
              allowNull: false,
              values: ['Waiting', 'Processing', 'Accepted', 'Corrected', 'Rejected'],
            },
            metadata: {
              type: Sequelize.DataTypes.TEXT,
            },
            processingStart: {
              type: Sequelize.DataTypes.DATE,
            },
            processingEnd: {
              type: Sequelize.DataTypes.DATE,
            },
            createdAt: {
              allowNull: false,
              type: Sequelize.DataTypes.DATE,
            },
            updatedAt: {
              allowNull: false,
              type: Sequelize.DataTypes.DATE,
            },
            deletedAt: {
              type: Sequelize.DataTypes.DATE,
            },
            accountId: {
              type: Sequelize.DataTypes.UUID,
            },
            siteId: {
              type: Sequelize.DataTypes.UUID,
            },
            partId: {
              type: Sequelize.DataTypes.UUID,
            },
            drawingId: {
              type: Sequelize.DataTypes.UUID,
            },
            reviewerId: {
              type: Sequelize.DataTypes.UUID,
            },
          },
          {
            timestamps: true,
            paranoid: true,
          }
        );
      });
  },
  down: (queryInterface) => {
    return queryInterface
      .describeTable('reviewTasks')
      .then(() => {
        return queryInterface.dropTable('reviewTasks');
      })
      .catch(() => {
        return Promise.resolve();
      });
  },
};
