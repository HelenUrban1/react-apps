'use strict';
const { Sequelize } = require('sequelize');

module.exports = {
  up: (queryInterface) => {
    return queryInterface.describeTable('characteristics').then((tableDefinition) => {
      if (tableDefinition.captureError) {
        return Promise.resolve();
      }
      return queryInterface.addColumn('characteristics', 'captureError', { type: Sequelize.DataTypes.TEXT });
    });
  },

  down: (queryInterface) => {
    return queryInterface.describeTable('characteristics').then((tableDefinition) => {
      if (!tableDefinition.captureError) {
        return Promise.resolve();
      }
      return queryInterface.removeColumn('characteristics', 'captureError');
    });
  },
};
