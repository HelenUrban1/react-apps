#!/usr/bin/env node

/**
 Used for testing navigation notification functions that show new notifications for autoballooning.

 The result will be five notifications being created when it is complete.
 */
const axios = require('axios').default;
const config = require('../config')();
const { log } = require('../src/logger');
const PartService = require('../src/services/partService');

axios.defaults.withCredentials = true;
const backendUrl = 'https://ixc-local.com:8080/api';

// THIS IS DANGEROUS, ONLY USED TO GET AROUND SELF-SIGNED CERT ERROR LOCALLY FOR TESTING
process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';

// NEVER run against anything other than local development server
if (config.isNotDevEnv) throw Error(`This script is only for local testing and cannot run against production environments.`);

const authUser = async (email, password) => {
  const headers = {
    'Content-Type': 'application/json; charset=utf-8',
  };

  // Log in as the admin user to get a bearer token
  const authRequestData = {
    query: 'mutation AUTH_SIGN_IN($email: String!, $password: String!) { authSignIn(email: $email, password: $password) }',
    variables: {
      email,
      password,
    },
  };
  const authResponse = await axios.post(backendUrl, JSON.stringify(authRequestData), { headers });
  return authResponse.data.data.authSignIn;
};

// Have to call the api endpoint so the event emitter sends websocket events to the frontend client.
const createNewNotification = async (part) => {
  try {
    const headers = {
      'Content-Type': 'application/json; charset=utf-8',
    };

    const authToken = await authUser('dev.mail.monkey@gmail.com', 'test');

    // Add bearer token to the header of the request
    headers.Authorization = `Bearer ${authToken}`;

    const notificationInput = {
      title: `This is a fake title for ${part.name}`,
      content: '1 feature identified',
      action: 'Review later,Review features',
      priority: 1,
      relationId: part.id,
      relationType: 'part',
    };
    const requestData = {
      query: 'mutation NOTIFICATION_CREATE($data: NotificationInput!) { notificationCreate(data: $data) {id} }',
      variables: { data: notificationInput },
    };

    const response = await axios.post(backendUrl, JSON.stringify(requestData), { headers });
    return response.data.data.notificationCreate.id;
  } catch (error) {
    log.error(error);
    throw error;
  }
};

// Main function
(async () => {
  const context = { currentUser: { accountId: config.testAccountId } };
  const partService = new PartService(context);

  const parts = await partService.findAndCountAll({ limit: 20 });
  log.info(parts);
  for (let i = 0; i < parts.rows.length; i++) {
    await createNewNotification(parts.rows[i]);
  }
})()
  .then(() => {
    log.debug(`Finished adding notifications, returned without errors.`);
  })
  .catch((error) => {
    log.error(`Error occurred while adding notifications: ${error.message}`);
    log.error(error?.stack);
  });
