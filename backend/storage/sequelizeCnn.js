const config = require('../config')();

module.exports = {
  dialect: 'postgres',
  host: config.database.host,
  database: config.database.database,
  username: config.database.username,
  password: config.database.password,
  logging: console.log,
};
