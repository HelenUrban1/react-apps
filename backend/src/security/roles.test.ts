import Roles from './roles';

describe('Security Roles', () => {
  describe('values getter', () => {
    it('should provide the roles as an object with key/value pairs of roleName/roleName', () => {
      const result = Roles.values;
      const keys = Object.keys(result);
      const values = Object.values(result);

      expect(keys.length).toBeGreaterThan(0);

      keys.forEach((key, index) => {
        expect(key).toBe(values[index]);
      });
    });
  });

  describe('names getter', () => {
    it('should return an array of the roles names', () => {
      const result = Roles.names;

      expect(Array.isArray(result)).toBe(true);
      expect(result.length).toBeGreaterThan(0);
    });
  });

  describe('definitions getter', () => {
    it('should return an object', () => {
      const result = Roles.definitions;

      expect(Object.keys(result).length).toBeGreaterThan(0);
    });
  });
});
