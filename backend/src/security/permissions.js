const Roles = require('./roles');

const roles = Roles.values;

/**
 * List of Permissions and the Roles allowed of using them.
 */
class Permissions {
  static get values() {
    return {
      listItemEdit: {
        id: 'listItemEdit',
        allowedRoles: [roles.owner, roles.accountAdmin],
      },
      iamEdit: {
        id: 'iamEdit',
        allowedRoles: [roles.owner, roles.iamSecurityReviewer, roles.editor],
        allowedStorageFolders: ['user'],
      },
      iamCreate: {
        id: 'iamCreate',
        allowedRoles: [roles.owner, roles.iamSecurityReviewer, roles.editor],
      },
      iamImport: {
        id: 'iamImport',
        allowedRoles: [roles.owner, roles.iamSecurityReviewer, roles.editor],
      },
      iamRead: {
        id: 'iamRead',
        allowedRoles: [roles.owner, roles.iamSecurityReviewer, roles.editor, roles.viewer, roles.accountBilling],
      },
      iamUserAutocomplete: {
        id: 'iamUserAutocomplete',
        allowedRoles: [roles.owner, roles.editor, roles.accountAdmin, roles.viewer, roles.accountMemberEditor, roles.accountMemberViewer],
      },
      auditLogRead: {
        id: 'auditLogRead',
        allowedRoles: [roles.owner, roles.auditLogViewer, roles.viewer],
      },
      settingCreate: {
        id: 'settingCreate',
        allowedRoles: [roles.owner, roles.accountAdmin, roles.editor, roles.reviewer, roles.metro, roles.accountBilling, roles.collaborator],
      },
      settingEdit: {
        id: 'settingEdit',
        allowedRoles: [roles.owner, roles.accountAdmin, roles.editor, roles.reviewer],
      },
      accountImport: {
        id: 'accountImport',
        allowedRoles: [roles.owner, roles.editor, roles.accountAdmin, roles.entityEditor, roles.accountEditor],
      },
      accountCreate: {
        id: 'accountCreate',
        allowedRoles: [roles.owner, roles.editor, roles.accountAdmin, roles.entityEditor, roles.accountEditor],
        allowedStorageFolders: ['account'],
      },
      accountEdit: {
        id: 'accountEdit',
        allowedRoles: [roles.owner, roles.editor, roles.accountAdmin, roles.entityEditor, roles.accountEditor],
        allowedStorageFolders: ['account'],
      },
      accountDestroy: {
        id: 'accountDestroy',
        allowedRoles: [roles.owner, roles.editor, roles.accountAdmin, roles.entityEditor, roles.accountEditor],
        allowedStorageFolders: ['account'],
      },
      accountManagement: {
        id: 'accountManagement',
        allowedRoles: [roles.owner, roles.accountEditor],
        allowedStorageFolders: ['account'],
      },
      accountRead: {
        id: 'accountRead',
        allowedRoles: [roles.owner, roles.editor, roles.accountAdmin, roles.viewer, roles.entityEditor, roles.accountEditor, roles.accountViewer, roles.metro],
      },
      accountAutocomplete: {
        id: 'accountAutocomplete',
        allowedRoles: [
          roles.owner,
          roles.editor,
          roles.viewer,
          roles.entityEditor,
          roles.accountEditor,
          roles.accountViewer,
          roles.accountMemberEditor,
          roles.accountMemberViewer,
          roles.organizationEditor,
          roles.organizationViewer,
          roles.partEditor,
          roles.partViewer,
          roles.drawingEditor,
          roles.drawingViewer,
          roles.reportTemplateEditor,
          roles.reportTemplateViewer,
          roles.reportEditor,
          roles.reportViewer,
          roles.metro,
        ],
      },
      accountBillingAccess: {
        id: 'accountBillingAccess',
        allowedRoles: [roles.owner, roles.accountBilling],
      },
      accountMemberImport: {
        id: 'accountMemberImport',
        allowedRoles: [roles.owner, roles.editor, roles.accountAdmin, roles.entityEditor, roles.accountMemberEditor],
      },
      accountMemberCreate: {
        id: 'accountMemberCreate',
        allowedRoles: [roles.owner, roles.editor, roles.accountAdmin, roles.entityEditor, roles.accountMemberEditor],
        allowedStorageFolders: ['accountMember'],
      },
      accountMemberEdit: {
        id: 'accountMemberEdit',
        allowedRoles: [roles.owner, roles.editor, roles.accountAdmin, roles.entityEditor, roles.accountMemberEditor],
        allowedStorageFolders: ['accountMember'],
      },
      accountMemberDestroy: {
        id: 'accountMemberDestroy',
        allowedRoles: [roles.owner, roles.editor, roles.accountAdmin, roles.entityEditor, roles.accountMemberEditor],
        allowedStorageFolders: ['accountMember'],
      },
      accountMemberRead: {
        id: 'accountMemberRead',
        allowedRoles: [roles.owner, roles.editor, roles.accountAdmin, roles.viewer, roles.entityEditor, roles.accountMemberEditor, roles.accountMemberViewer, roles.metro],
      },
      accountMemberAutocomplete: {
        id: 'accountMemberAutocomplete',
        allowedRoles: [roles.owner, roles.editor, roles.accountAdmin, roles.viewer, roles.entityEditor, roles.accountMemberEditor, roles.accountMemberViewer, roles.accountEditor, roles.accountViewer, roles.metro],
      },
      optionsImport: {
        id: 'optionsImport',
        allowedRoles: [roles.owner, roles.editor, roles.accountAdmin, roles.entityEditor, roles.optionsEditor],
      },
      optionsCreate: {
        id: 'optionsCreate',
        allowedRoles: [roles.owner, roles.editor, roles.accountAdmin, roles.entityEditor, roles.optionsEditor],
        allowedStorageFolders: ['options'],
      },
      optionsEdit: {
        id: 'optionsEdit',
        allowedRoles: [roles.owner, roles.editor, roles.accountAdmin, roles.entityEditor, roles.optionsEditor],
        allowedStorageFolders: ['options'],
      },
      optionsDestroy: {
        id: 'optionsDestroy',
        allowedRoles: [roles.owner, roles.editor, roles.accountAdmin, roles.entityEditor, roles.optionsEditor],
        allowedStorageFolders: ['options'],
      },
      optionsRead: {
        id: 'optionsRead',
        allowedRoles: [roles.owner, roles.editor, roles.accountAdmin, roles.viewer, roles.entityEditor, roles.optionsEditor, roles.optionsViewer],
      },
      optionsAutocomplete: {
        id: 'optionsAutocomplete',
        allowedRoles: [roles.owner, roles.editor, roles.accountAdmin, roles.viewer, roles.entityEditor, roles.optionsEditor, roles.optionsViewer],
      },

      organizationCreate: {
        id: 'organizationCreate',
        allowedRoles: [roles.owner, roles.editor, roles.accountAdmin, roles.entityEditor, roles.organizationEditor],
        allowedStorageFolders: ['organization'],
      },
      organizationEdit: {
        id: 'organizationEdit',
        allowedRoles: [roles.owner, roles.editor, roles.accountAdmin, roles.entityEditor, roles.organizationEditor],
        allowedStorageFolders: ['organization'],
      },
      organizationDestroy: {
        id: 'organizationDestroy',
        allowedRoles: [roles.owner, roles.editor, roles.accountAdmin, roles.entityEditor, roles.organizationEditor],
        allowedStorageFolders: ['organization'],
      },
      organizationRead: {
        id: 'organizationRead',
        allowedRoles: [roles.owner, roles.editor, roles.accountAdmin, roles.viewer, roles.entityEditor, roles.organizationEditor, roles.organizationViewer, roles.metro, roles.accountBilling],
      },
      organizationAutocomplete: {
        id: 'organizationAutocomplete',
        allowedRoles: [
          roles.owner,
          roles.editor,
          roles.viewer,
          roles.entityEditor,
          roles.organizationEditor,
          roles.organizationViewer,
          roles.partEditor,
          roles.partViewer,
          roles.drawingEditor,
          roles.drawingViewer,
          roles.reportTemplateEditor,
          roles.reportTemplateViewer,
          roles.reportEditor,
          roles.reportViewer,
        ],
      },

      partImport: {
        id: 'partImport',
        allowedRoles: [roles.owner, roles.editor, roles.accountAdmin, roles.entityEditor],
      },
      partCreate: {
        id: 'partCreate',
        allowedRoles: [roles.owner, roles.editor, roles.accountAdmin, roles.entityEditor],
        allowedStorageFolders: ['part'],
      },
      partEdit: {
        id: 'partEdit',
        allowedRoles: [roles.owner, roles.editor, roles.accountAdmin, roles.entityEditor, roles.partEditor],
        allowedStorageFolders: ['part'],
      },
      partDestroy: {
        id: 'partDestroy',
        allowedRoles: [roles.owner, roles.editor, roles.accountAdmin, roles.entityEditor, roles.partEditor],
        allowedStorageFolders: ['part'],
      },
      partDelete: {
        id: 'partDelete',
        allowedRoles: [roles.owner, roles.editor, roles.accountAdmin, roles.entityEditor, roles.partEditor],
        allowedStorageFolders: ['part'],
      },
      partRead: {
        id: 'partRead',
        allowedRoles: [roles.owner, roles.editor, roles.accountAdmin, roles.viewer, roles.entityEditor, roles.partEditor, roles.partViewer, roles.metro],
      },
      partAutocomplete: {
        id: 'partAutocomplete',
        allowedRoles: [
          roles.owner,
          roles.editor,
          roles.viewer,
          roles.entityEditor,
          roles.partEditor,
          roles.partViewer,
          roles.accountEditor,
          roles.accountViewer,
          roles.organizationEditor,
          roles.organizationViewer,
          roles.drawingEditor,
          roles.drawingViewer,
          roles.drawingSheetEditor,
          roles.drawingSheetViewer,
          roles.characteristicEditor,
          roles.characteristicViewer,
          roles.reportEditor,
          roles.reportViewer,
        ],
      },

      drawingImport: {
        id: 'drawingImport',
        allowedRoles: [roles.owner, roles.accountAdmin, roles.editor, roles.entityEditor, roles.drawingEditor],
      },
      drawingCreate: {
        id: 'drawingCreate',
        allowedRoles: [roles.owner, roles.accountAdmin, roles.editor, roles.entityEditor, roles.drawingEditor],
        allowedStorageFolders: ['drawing'],
      },
      drawingEdit: {
        id: 'drawingEdit',
        allowedRoles: [roles.owner, roles.accountAdmin, roles.editor, roles.entityEditor, roles.drawingEditor],
        allowedStorageFolders: ['drawing'],
      },
      drawingDestroy: {
        id: 'drawingDestroy',
        allowedRoles: [roles.owner, roles.accountAdmin, roles.editor, roles.entityEditor, roles.drawingEditor],
        allowedStorageFolders: ['drawing'],
      },
      drawingRead: {
        id: 'drawingRead',
        allowedRoles: [roles.owner, roles.accountAdmin, roles.editor, roles.viewer, roles.entityEditor, roles.drawingEditor, roles.drawingViewer, roles.metro],
      },
      drawingAutocomplete: {
        id: 'drawingAutocomplete',
        allowedRoles: [
          roles.owner,
          roles.accountAdmin,
          roles.editor,
          roles.viewer,
          roles.entityEditor,
          roles.drawingEditor,
          roles.drawingViewer,
          roles.partEditor,
          roles.partViewer,
          roles.drawingSheetEditor,
          roles.drawingSheetViewer,
          roles.characteristicEditor,
          roles.characteristicViewer,
        ],
      },

      drawingSheetImport: {
        id: 'drawingSheetImport',
        allowedRoles: [roles.owner, roles.accountAdmin, roles.editor, roles.entityEditor, roles.drawingSheetEditor],
      },
      drawingSheetCreate: {
        id: 'drawingSheetCreate',
        allowedRoles: [roles.owner, roles.accountAdmin, roles.editor, roles.entityEditor, roles.drawingSheetEditor],
        allowedStorageFolders: ['drawingSheet'],
      },
      drawingSheetEdit: {
        id: 'drawingSheetEdit',
        allowedRoles: [roles.owner, roles.accountAdmin, roles.editor, roles.entityEditor, roles.drawingSheetEditor],
        allowedStorageFolders: ['drawingSheet'],
      },
      drawingSheetDestroy: {
        id: 'drawingSheetDestroy',
        allowedRoles: [roles.owner, roles.accountAdmin, roles.editor, roles.entityEditor, roles.drawingSheetEditor],
        allowedStorageFolders: ['drawingSheet'],
      },
      drawingSheetRead: {
        id: 'drawingSheetRead',
        allowedRoles: [roles.owner, roles.accountAdmin, roles.editor, roles.viewer, roles.entityEditor, roles.drawingSheetEditor, roles.drawingSheetViewer, roles.metro],
      },
      drawingSheetAutocomplete: {
        id: 'drawingSheetAutocomplete',
        allowedRoles: [
          roles.owner,
          roles.accountAdmin,
          roles.editor,
          roles.viewer,
          roles.entityEditor,
          roles.drawingSheetEditor,
          roles.drawingSheetViewer,
          roles.drawingEditor,
          roles.drawingViewer,
          roles.characteristicEditor,
          roles.characteristicViewer,
        ],
      },

      characteristicImport: {
        id: 'characteristicImport',
        allowedRoles: [roles.owner, roles.editor, roles.accountAdmin, roles.entityEditor, roles.characteristicEditor],
      },
      characteristicCreate: {
        id: 'characteristicCreate',
        allowedRoles: [roles.owner, roles.editor, roles.accountAdmin, roles.entityEditor, roles.characteristicEditor],
        allowedStorageFolders: ['characteristic'],
      },
      characteristicEdit: {
        id: 'characteristicEdit',
        allowedRoles: [roles.owner, roles.editor, roles.accountAdmin, roles.entityEditor, roles.characteristicEditor],
        allowedStorageFolders: ['characteristic'],
      },
      characteristicDestroy: {
        id: 'characteristicDestroy',
        allowedRoles: [roles.owner, roles.editor, roles.accountAdmin, roles.entityEditor, roles.characteristicEditor],
        allowedStorageFolders: ['characteristic'],
      },
      characteristicRead: {
        id: 'characteristicRead',
        allowedRoles: [roles.owner, roles.editor, roles.accountAdmin, roles.viewer, roles.entityEditor, roles.characteristicEditor, roles.characteristicViewer],
      },
      characteristicAutocomplete: {
        id: 'characteristicAutocomplete',
        allowedRoles: [
          roles.owner,
          roles.editor,
          roles.accountAdmin,
          roles.viewer,
          roles.entityEditor,
          roles.characteristicEditor,
          roles.characteristicViewer,
          roles.partEditor,
          roles.partViewer,
          roles.drawingEditor,
          roles.drawingViewer,
          roles.drawingSheetEditor,
          roles.drawingSheetViewer,
        ],
      },
      annotationImport: {
        id: 'annotationImport',
        allowedRoles: [roles.owner, roles.editor, roles.accountAdmin, roles.entityEditor, roles.characteristicEditor],
      },
      annotationCreate: {
        id: 'annotationCreate',
        allowedRoles: [roles.owner, roles.editor, roles.accountAdmin, roles.entityEditor, roles.characteristicEditor],
        allowedStorageFolders: ['annotation'],
      },
      annotationEdit: {
        id: 'annotationEdit',
        allowedRoles: [roles.owner, roles.editor, roles.accountAdmin, roles.entityEditor, roles.characteristicEditor],
        allowedStorageFolders: ['annotation'],
      },
      annotationDestroy: {
        id: 'annotationDestroy',
        allowedRoles: [roles.owner, roles.editor, roles.accountAdmin, roles.entityEditor, roles.characteristicEditor],
        allowedStorageFolders: ['annotation'],
      },
      annotationRead: {
        id: 'annotationRead',
        allowedRoles: [roles.owner, roles.editor, roles.accountAdmin, roles.viewer, roles.entityEditor, roles.characteristicEditor, roles.characteristicViewer],
      },
      annotationAutocomplete: {
        id: 'annotationAutocomplete',
        allowedRoles: [
          roles.owner,
          roles.editor,
          roles.accountAdmin,
          roles.viewer,
          roles.entityEditor,
          roles.characteristicEditor,
          roles.characteristicViewer,
          roles.partEditor,
          roles.partViewer,
          roles.drawingEditor,
          roles.drawingViewer,
          roles.drawingSheetEditor,
          roles.drawingSheetViewer,
        ],
      },
      markerImport: {
        id: 'markerImport',
        allowedRoles: [roles.owner, roles.editor, roles.accountAdmin, roles.entityEditor, roles.markerEditor],
      },
      markerCreate: {
        id: 'markerCreate',
        allowedRoles: [roles.owner, roles.editor, roles.accountAdmin, roles.entityEditor, roles.markerEditor],
        allowedStorageFolders: ['marker'],
      },
      markerEdit: {
        id: 'markerEdit',
        allowedRoles: [roles.owner, roles.editor, roles.accountAdmin, roles.entityEditor, roles.markerEditor],
        allowedStorageFolders: ['marker'],
      },
      markerDestroy: {
        id: 'markerDestroy',
        allowedRoles: [roles.owner, roles.editor, roles.accountAdmin, roles.entityEditor, roles.markerEditor],
        allowedStorageFolders: ['marker'],
      },
      markerRead: {
        id: 'markerRead',
        allowedRoles: [roles.owner, roles.editor, roles.accountAdmin, roles.viewer, roles.entityEditor, roles.markerEditor, roles.markerViewer, roles.metro, roles.accountBilling],
      },
      markerAutocomplete: {
        id: 'markerAutocomplete',
        allowedRoles: [roles.owner, roles.editor, roles.accountAdmin, roles.viewer, roles.entityEditor, roles.markerEditor, roles.markerViewer],
      },

      notificationImport: {
        id: 'notificationImport',
        allowedRoles: [roles.owner, roles.editor, roles.accountAdmin, roles.entityEditor, roles.notificationEditor],
      },
      notificationCreate: {
        id: 'notificationCreate',
        allowedRoles: [roles.owner, roles.editor, roles.accountAdmin, roles.entityEditor, roles.notificationEditor],
        allowedStorageFolders: ['notification'],
      },
      notificationEdit: {
        id: 'notificationEdit',
        allowedRoles: [roles.owner, roles.editor, roles.accountAdmin, roles.entityEditor, roles.notificationEditor],
        allowedStorageFolders: ['notification'],
      },
      notificationDestroy: {
        id: 'notificationDestroy',
        allowedRoles: [roles.owner, roles.editor, roles.accountAdmin, roles.entityEditor, roles.notificationEditor],
        allowedStorageFolders: ['notification'],
      },
      notificationRead: {
        id: 'notificationRead',
        allowedRoles: [roles.owner, roles.editor, roles.accountAdmin, roles.viewer, roles.entityEditor, roles.notificationEditor, roles.notificationViewer, roles.accountBilling, roles.collaborator],
      },

      reportTemplateImport: {
        id: 'reportTemplateImport',
        allowedRoles: [roles.owner, roles.accountAdmin],
      },
      reportTemplateCreate: {
        id: 'reportTemplateCreate',
        allowedRoles: [roles.owner, roles.accountAdmin],
        allowedStorageFolders: ['reportTemplate'],
      },
      reportTemplateEdit: {
        id: 'reportTemplateEdit',
        allowedRoles: [roles.owner, roles.accountAdmin],
        allowedStorageFolders: ['reportTemplate'],
      },
      reportTemplateDestroy: {
        id: 'reportTemplateDestroy',
        allowedRoles: [roles.owner, roles.accountAdmin],
        allowedStorageFolders: ['reportTemplate'],
      },
      reportTemplateRead: {
        id: 'reportTemplateRead',
        allowedRoles: [roles.owner, roles.accountAdmin],
      },
      reportTemplateAutocomplete: {
        id: 'reportTemplateAutocomplete',
        allowedRoles: [roles.owner, roles.accountAdmin],
      },

      reportImport: {
        id: 'reportImport',
        allowedRoles: [roles.owner, roles.editor, roles.reportEditor],
      },
      reportCreate: {
        id: 'reportCreate',
        allowedRoles: [roles.owner, roles.accountAdmin, roles.reportEditor],
        allowedStorageFolders: ['report'],
      },
      reportEdit: {
        id: 'reportEdit',
        allowedRoles: [roles.owner, roles.accountAdmin, roles.reportEditor],
        allowedStorageFolders: ['report'],
      },
      reportDestroy: {
        id: 'reportDestroy',
        allowedRoles: [roles.owner, roles.accountAdmin, roles.reportEditor],
        allowedStorageFolders: ['report'],
      },
      reportRead: {
        id: 'reportRead',
        allowedRoles: [roles.owner, roles.accountAdmin, roles.reportEditor, roles.reportViewer, roles.metro],
      },
      reportAutocomplete: {
        id: 'reportAutocomplete',
        allowedRoles: [roles.owner, roles.editor, roles.viewer, roles.entityEditor, roles.reportEditor, roles.reportViewer, roles.accountEditor, roles.accountViewer, roles.partEditor, roles.partViewer],
      },

      reviewTaskCreate: {
        id: 'reviewTaskCreate',
        allowedRoles: [roles.owner, roles.accountAdmin, roles.reviewer],
        allowedStorageFolders: ['reviewTask'],
      },
      reviewTaskEdit: {
        id: 'reviewTaskEdit',
        allowedRoles: [roles.owner, roles.accountAdmin, roles.reviewer],
        allowedStorageFolders: ['reviewTask'],
      },
      reviewTaskDestroy: {
        id: 'reviewTaskDestroy',
        allowedRoles: [roles.owner, roles.accountAdmin, roles.reviewer],
        allowedStorageFolders: ['reviewTask'],
      },
      reviewTaskRead: {
        id: 'reviewTaskRead',
        allowedRoles: [roles.owner, roles.accountAdmin, roles.reviewer],
      },
      reviewer: {
        id: 'reviewer',
        allowedRoles: [roles.reviewer],
      },
      siteImport: {
        id: 'siteImport',
        allowedRoles: [roles.owner, roles.editor, roles.accountAdmin, roles.entityEditor, roles.siteEditor],
      },
      siteCreate: {
        id: 'siteCreate',
        allowedRoles: [roles.owner, roles.editor, roles.accountAdmin, roles.entityEditor, roles.siteEditor],
        allowedStorageFolders: ['site'],
      },
      siteEdit: {
        id: 'siteEdit',
        allowedRoles: [roles.owner, roles.editor, roles.accountAdmin, roles.entityEditor, roles.siteEditor],
        allowedStorageFolders: ['site'],
      },
      siteDestroy: {
        id: 'siteDestroy',
        allowedRoles: [roles.owner, roles.editor, roles.accountAdmin, roles.entityEditor, roles.siteEditor],
        allowedStorageFolders: ['site'],
      },
      siteRead: {
        id: 'siteRead',
        allowedRoles: [roles.owner, roles.editor, roles.accountAdmin, roles.viewer, roles.entityEditor, roles.siteEditor, roles.siteViewer],
      },
      siteAutocomplete: {
        id: 'siteAutocomplete',
        allowedRoles: [
          roles.owner,
          roles.editor,
          roles.viewer,
          roles.entityEditor,
          roles.siteEditor,
          roles.siteViewer,
          roles.siteMemberEditor,
          roles.siteMemberViewer,
          roles.customerEditor,
          roles.customerViewer,
          roles.partEditor,
          roles.partViewer,
          roles.drawingEditor,
          roles.drawingViewer,
          roles.reportTemplateEditor,
          roles.reportTemplateViewer,
          roles.reportEditor,
          roles.reportViewer,
        ],
      },
      sphinxApiKeysAccess: {
        id: 'sphinxApiKeysAccess',
        allowedRoles: [roles.owner, roles.editor, roles.accountAdmin, roles.entityEditor, roles.partEditor],
      },
      metro: {
        id: 'metro',
        allowedRoles: [roles.owner, roles.editor, roles.viewer, roles.entityEditor, roles.siteEditor, roles.siteViewer, roles.metro, roles.collaborator],
      },
      jobCreate: {
        id: 'jobCreate',
        allowedRoles: [roles.owner, roles.editor, roles.accountAdmin, roles.entityEditor, roles.partEditor],
      },
      jobDestroy: {
        id: 'jobDestroy',
        allowedRoles: [roles.owner, roles.editor, roles.accountAdmin, roles.entityEditor, roles.partEditor],
      },
      jobEdit: {
        id: 'jobEdit',
        allowedRoles: [roles.owner, roles.editor, roles.accountAdmin, roles.entityEditor, roles.partEditor],
      },
      jobRead: {
        id: 'jobRead',
        allowedRoles: [roles.owner, roles.editor, roles.accountAdmin, roles.entityEditor, roles.partEditor, roles.viewer, roles.siteEditor, roles.siteViewer, roles.metro],
      },
    };
  }

  static get asArray() {
    return Object.keys(this.values).map((value) => {
      return this.values[value];
    });
  }
}

module.exports = Permissions;
