const assert = require('assert');
const { log } = require('../logger');
/**
 * Maps AccountMember roles, which are the roles that users can assign in the app, with
 * authorization roles, which are the roles defined in security/roles.js and used for
 * access control.
 */
class RolesMapper {
  constructor(params) {
    const { accountMemberRoles, authorizationRolesNames, authorizationRolesDefinitions } = params;

    assert(Array.isArray(accountMemberRoles), 'accountMemberRoles must be an array');
    assert(Array.isArray(authorizationRolesNames), 'authorizationRolesNames must be an array');
    assert(authorizationRolesDefinitions, 'authorizationRolesDefinitions must be defined');

    this.accountMemberRoles = accountMemberRoles;
    this.authorizationRolesNames = authorizationRolesNames;
    this.authorizationRolesDefinitions = authorizationRolesDefinitions;

    this._buildMap();
  }

  /**
   * Determines if there is a match between the authorization roles provided and the
   * provided user's roles, which are the roles in their active AccountMember record,
   * using the mapping between the two sets of roles created in this class.
   *
   * @param {Array<String>} userRoles
   * @param {Array<String>} authorizationRoles
   * @return {Boolean}
   */
  hasMatchingRole(userRoles = [], authorizationRoles = []) {
    const currentRoles = Array.isArray(userRoles) ? userRoles : [userRoles];
    return currentRoles.some((userRole) => {
      const entries = this.map.get(userRole.toLowerCase());
      if (!entries) {
        return false;
      }

      return entries.some((entry) => authorizationRoles.includes(entry));
    });
  }

  /**
   * Builds the roles mapping.
   *
   * @private
   */
  _buildMap() {
    this.map = new Map();

    this.accountMemberRoles.forEach((role) => this.map.set(role.toLowerCase(), []));

    this.authorizationRolesNames.forEach((authRole) => {
      this.accountMemberRoles.forEach((accountMemberRole) => {
        if (this.authorizationRolesDefinitions[authRole].mapsToAccountMemberRole === accountMemberRole) {
          const entry = this.map.get(accountMemberRole.toLowerCase());
          entry.push(authRole);
        }
      });
    });
  }
}

module.exports = RolesMapper;
