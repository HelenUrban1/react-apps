const RolesMapper = require('./rolesMapper');

describe('RolesMapper', () => {
  let rolesMapper;
  const accountMemberRoles = ['AccountMemberRoleA', 'AccountMemberRoleB'];
  const authorizationRolesNames = ['AuthRoleA', 'AuthRoleB'];
  const authorizationRolesDefinitions = {
    AuthRoleA: {
      mapsToAccountMemberRole: accountMemberRoles[0],
    },
    AuthRoleB: {
      mapsToAccountMemberRole: accountMemberRoles[1],
    },
  };

  beforeEach(() => {
    rolesMapper = new RolesMapper({
      accountMemberRoles,
      authorizationRolesNames,
      authorizationRolesDefinitions,
    });
  });

  describe('#hasMatchingRole()', () => {
    it('should return false for empty input', () => {
      const result = rolesMapper.hasMatchingRole();
      expect(result).toBe(false);
    });

    it('should return false for no matching user roles', () => {
      const userRoles = ['ThisDoesNotExist'];
      const result = rolesMapper.hasMatchingRole(userRoles, [authorizationRolesNames[0]]);
      expect(result).toBe(false);
    });

    it('should return false for no matching authorization roles', () => {
      const userRoles = [accountMemberRoles[0]];
      const result = rolesMapper.hasMatchingRole(userRoles, ['ThisDoesNotExist']);
      expect(result).toBe(false);
    });

    it('should return false for no matching user roles and no matching authorization roles', () => {
      const result = rolesMapper.hasMatchingRole(['ThisDoesNotExist'], ['ThisDoesNotExist']);
      expect(result).toBe(false);
    });

    it('should return true when all authorization roles match the mapping for their user roles', () => {
      const result = rolesMapper.hasMatchingRole(accountMemberRoles, authorizationRolesNames);
      expect(result).toBe(true);
    });

    it('should return true when one authorization roles matches the mapping for its user role', () => {
      const result = rolesMapper.hasMatchingRole([accountMemberRoles[0]], [authorizationRolesNames[0], authorizationRolesNames[1]]);
      expect(result).toBe(true);
    });
  });
});
