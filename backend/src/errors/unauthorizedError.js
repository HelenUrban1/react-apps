const { i18n, i18nExists } = require('../i18n');

/**
 * Error with code 400
 */
module.exports = class UnauthorizedError extends Error {
  constructor(language = 'en', messageCode) {
    let message;

    if (messageCode && i18nExists(language, messageCode)) {
      message = i18n(language, messageCode);
    }

    message = message || i18n(language, 'errors.unauthorized.message');

    super(message);
    this.code = 401;
  }
};
