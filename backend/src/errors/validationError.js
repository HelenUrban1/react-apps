const { i18n, i18nExists } = require('../i18n');

/**
 * Error with code 400
 */
module.exports = class ValidationError extends Error {
  constructor(language = 'en', messageCode, optional = '') {
    let message;

    if (messageCode && i18nExists(language, messageCode)) {
      message = i18n(language, messageCode);
    }

    message = message || i18n(language, 'errors.validation.message');
    message += optional;

    super(message);
    this.code = 400;
  }
};
