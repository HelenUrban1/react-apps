/**
 * Maps all the Resolvers of the application.
 * More about resolvers: https://www.apollographql.com/docs/graphql-tools/resolvers/
 */

const mergeResolvers = require('./shared/utils/mergeGraphqlResolvers');

const sharedTypes = require('./shared/types');

const settingTypes = require('./setting/types');
const settingQueries = require('./setting/queries');
const settingMutations = require('./setting/mutations');

const listItemTypes = require('./listItem/types');
const listItemQueries = require('./listItem/queries');
const listItemMutations = require('./listItem/mutations');

const authTypes = require('./auth/types');
const authQueries = require('./auth/queries');
const authMutations = require('./auth/mutations');

const iamTypes = require('./iam/types');
const iamQueries = require('./iam/queries');
const iamMutations = require('./iam/mutations');

const auditLogTypes = require('./auditLog/types');
const auditLogQueries = require('./auditLog/queries');
const auditLogMutations = require('./auditLog/mutations');

const accountTypes = require('./account/types');
const accountQueries = require('./account/queries');
const accountMutations = require('./account/mutations');

const accountMemberTypes = require('./accountMember/types');
const accountMemberQueries = require('./accountMember/queries');
const accountMemberMutations = require('./accountMember/mutations');

const annotationTypes = require('./annotation/types');
const annotationQueries = require('./annotation/queries');
const annotationMutations = require('./annotation/mutations');

const optionsTypes = require('./options/types');
const optionsQueries = require('./options/queries');
const optionsMutations = require('./options/mutations');

const partTypes = require('./part/types');
const partQueries = require('./part/queries');
const partMutations = require('./part/mutations');

const drawingTypes = require('./drawing/types');
const drawingQueries = require('./drawing/queries');
const drawingMutations = require('./drawing/mutations');

const drawingSheetTypes = require('./drawingSheet/types');
const drawingSheetQueries = require('./drawingSheet/queries');
const drawingSheetMutations = require('./drawingSheet/mutations');

const characteristicTypes = require('./characteristic/types');
const characteristicQueries = require('./characteristic/queries');
const characteristicMutations = require('./characteristic/mutations');

const markerTypes = require('./marker/types');
const markerQueries = require('./marker/queries');
const markerMutations = require('./marker/mutations');

const measurementTypes = require('./measurement/types');
const measurementQueries = require('./measurement/queries');
const measurementMutations = require('./measurement/mutations');

const reportTemplateTypes = require('./reportTemplate/types');
const reportTemplateQueries = require('./reportTemplate/queries');
const reportTemplateMutations = require('./reportTemplate/mutations');

const reportTypes = require('./report/types');
const reportQueries = require('./report/queries');
const reportMutations = require('./report/mutations');

const sampleTypes = require('./sample/types');
const sampleQueries = require('./sample/queries');
const sampleMutations = require('./sample/mutations');

const siteTypes = require('./site/types');
const siteQueries = require('./site/queries');
const siteMutations = require('./site/mutations');

const sphinxTypes = require('./sphinx/types');
const sphinxQueries = require('./sphinx/queries');
const sphinxMutations = require('./sphinx/mutations');

const stationTypes = require('./station/types');
const stationQueries = require('./station/queries');
const stationMutations = require('./station/mutations');

const organizationTypes = require('./organization/types');
const organizationQueries = require('./organization/queries');
const organizationMutations = require('./organization/mutations');

const operatorTypes = require('./operator/types');
const operatorQueries = require('./operator/queries');
const operatorMutations = require('./operator/mutations');

const subscriptionTypes = require('./subscription/types');
const subscriptionQueries = require('./subscription/queries');
const subscriptionMutations = require('./subscription/mutations');

const jobTypes = require('./job/types');
const jobQueries = require('./job/queries');
const jobMutations = require('./job/mutations');

const reviewTaskTypes = require('./reviewTask/types');
const reviewTaskQueries = require('./reviewTask/queries');
const reviewTaskMutations = require('./reviewTask/mutations');

const notificationTypes = require('./notification/types');
const notificationQueries = require('./notification/queries');
const notificationMutations = require('./notification/mutations');

const notificationUserStatusTypes = require('./notificationUserStatus/types');
const notificationUserStatusQueries = require('./notificationUserStatus/queries');
const notificationUserStatusMutations = require('./notificationUserStatus/mutations');

const types = [
  ...sharedTypes, //
  ...iamTypes,
  ...authTypes,
  ...auditLogTypes,
  ...settingTypes,
  ...listItemTypes,
  ...accountTypes,
  ...accountMemberTypes,
  ...optionsTypes,
  ...partTypes,
  ...drawingTypes,
  ...drawingSheetTypes,
  ...characteristicTypes,
  ...annotationTypes,
  ...markerTypes,
  ...measurementTypes,
  ...reportTemplateTypes,
  ...reportTypes,
  ...siteTypes,
  ...jobTypes,
  ...sampleTypes,
  ...sphinxTypes,
  ...stationTypes,
  ...organizationTypes,
  ...operatorTypes,
  ...subscriptionTypes,
  ...reviewTaskTypes,
  ...notificationTypes,
  ...notificationUserStatusTypes,
].map((type) => type.resolver);

const queries = [
  ...iamQueries, //
  ...authQueries,
  ...auditLogQueries,
  ...settingQueries,
  ...listItemQueries,
  ...accountQueries,
  ...accountMemberQueries,
  ...annotationQueries,
  ...optionsQueries,
  ...partQueries,
  ...drawingQueries,
  ...drawingSheetQueries,
  ...characteristicQueries,
  ...markerQueries,
  ...measurementQueries,
  ...reportTemplateQueries,
  ...reportQueries,
  ...sampleQueries,
  ...siteQueries,
  ...sphinxQueries,
  ...stationQueries,
  ...organizationQueries,
  ...operatorQueries,
  ...jobQueries,
  ...subscriptionQueries,
  ...reviewTaskQueries,
  ...notificationQueries,
  ...notificationUserStatusQueries,
].map((query) => query.resolver);

const mutations = [
  ...iamMutations, //
  ...authMutations,
  ...auditLogMutations,
  ...settingMutations,
  ...listItemMutations,
  ...accountMutations,
  ...accountMemberMutations,
  ...annotationMutations,
  ...optionsMutations,
  ...partMutations,
  ...drawingMutations,
  ...drawingSheetMutations,
  ...characteristicMutations,
  ...markerMutations,
  ...measurementMutations,
  ...reportTemplateMutations,
  ...jobMutations,
  ...reportMutations,
  ...sampleMutations,
  ...siteMutations,
  ...sphinxMutations,
  ...stationMutations,
  ...organizationMutations,
  ...operatorMutations,
  ...subscriptionMutations,
  ...reviewTaskMutations,
  ...notificationMutations,
  ...notificationUserStatusMutations,
].map((mutation) => mutation.resolver);

module.exports = mergeResolvers(types, queries, mutations);
