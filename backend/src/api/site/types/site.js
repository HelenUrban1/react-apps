const schema = `
  type Site {
    id: String!
    status: SiteStatusEnum
    settings: String
    siteName: String
    sitePhone: String
    siteEmail: String
    siteStreet: String
    siteStreet2: String
    siteCity: String
    siteRegion: SiteRegionEnum
    sitePostalcode: String
    siteCountry: SiteCountryEnum
    parts: [ Part! ]
    members: [ AccountMember! ]
    reportTemplates: [ ReportTemplate! ]
    reports: [ Report! ]
    createdAt: DateTime
    updatedAt: DateTime
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
