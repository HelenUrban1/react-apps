module.exports = [
    require('./site'),
    require('./siteInput'),
    require('./siteFilterInput'),
    require('./siteOrderByEnum'),
    require('./sitePage'),
    require('./siteEnums'),
  ];
  