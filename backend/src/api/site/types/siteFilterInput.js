const schema = `
  input SiteFilterInput {
    id: String
    status: SiteStatusEnum
    settings: String
    siteName: String
    sitePhone: String
    siteEmail: String
    siteStreet: String
    siteStreet2: String
    siteCity: String
    siteRegion: SiteRegionEnum
    sitePostalcode: String
    siteCountry: SiteCountryEnum
    createdAtRange: [ DateTime ]
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
