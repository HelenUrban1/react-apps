const schema = `
  input SiteInput {
    status: SiteStatusEnum!
    settings: String!
    siteName: String!
    sitePhone: String
    siteEmail: String!
    siteStreet: String
    siteStreet2: String
    siteCity: String
    siteRegion: SiteRegionEnum
    sitePostalcode: String
    siteCountry: SiteCountryEnum
    parts: [ String! ]
    members: [ String! ]
    reportTemplates: [ String! ]
    reports: [ String! ]
    accountId: String!
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
