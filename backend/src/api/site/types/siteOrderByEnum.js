const schema = `
  enum SiteOrderByEnum {
    id_ASC
    id_DESC
    status_ASC
    status_DESC
    settings_ASC
    settings_DESC
    siteName_ASC
    siteName_DESC
    sitePhone_ASC
    sitePhone_DESC
    siteEmail_ASC
    siteEmail_DESC
    siteStreet_ASC
    siteStreet_DESC
    siteStreet2_ASC
    siteStreet2_DESC
    siteCity_ASC
    siteCity_DESC
    siteRegion_ASC
    siteRegion_DESC
    sitePostalcode_ASC
    sitePostalcode_DESC
    siteCountry_ASC
    siteCountry_DESC
    createdAt_ASC
    createdAt_DESC
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
