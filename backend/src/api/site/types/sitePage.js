const schema = `
  type SitePage {
    rows: [Site!]!
    count: Int!
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
