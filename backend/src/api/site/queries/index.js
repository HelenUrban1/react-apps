module.exports = [
    require('./siteFind'),
    require('./siteList'),
    require('./siteAutocomplete'),
];
