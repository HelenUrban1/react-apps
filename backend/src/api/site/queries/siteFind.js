const SiteService = require('../../../services/siteService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions')
  .values;

const schema = `
  siteFind(id: String!): Site!
`;

const resolver = {
  siteFind: async (root, args, context) => {
    new PermissionChecker(context)
      .validateHas(permissions.siteRead);

    return new SiteService(context).findById(args.id);
  },
};

exports.schema = schema;
exports.resolver = resolver;
