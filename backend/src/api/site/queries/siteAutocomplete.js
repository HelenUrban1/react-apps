const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions')
  .values;
const SiteService = require('../../../services/siteService');

const schema = `
  siteAutocomplete(query: String, limit: Int): [AutocompleteOption!]!
`;

const resolver = {
  siteAutocomplete: async (root, args, context, info) => {
    new PermissionChecker(context)
      .validateHas(permissions.siteAutocomplete);

    return new SiteService(context).findAllAutocomplete(
      args.query,
      args.limit,
    );
  },
};

exports.schema = schema;
exports.resolver = resolver;
