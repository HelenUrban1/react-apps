const SiteService = require('../../../services/siteService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions')
  .values;
const graphqlSelectRequestedAttributes = require('../../shared/utils/graphqlSelectRequestedAttributes');

const schema = `
  siteList(filter: SiteFilterInput, limit: Int, offset: Int, orderBy: SiteOrderByEnum): SitePage!
`;

const resolver = {
  siteList: async (root, args, context, info) => {
    new PermissionChecker(context)
      .validateHas(permissions.siteRead);

    return new SiteService(context).findAndCountAll({
      ...args,
      requestedAttributes: graphqlSelectRequestedAttributes(
        info,
        'rows',
      ),
    });
  },
};

exports.schema = schema;
exports.resolver = resolver;
