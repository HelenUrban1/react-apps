module.exports = [
    require('./siteCreate'),
    require('./siteDestroy'),
    require('./siteUpdate'),
    require('./siteImport'),
];
