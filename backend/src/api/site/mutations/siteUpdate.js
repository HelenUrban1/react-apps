const SiteService = require('../../../services/siteService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions')
  .values;

const schema = `
  siteUpdate(id: String!, data: SiteInput!): Site!
`;

const resolver = {
  siteUpdate: async (root, args, context) => {
    new PermissionChecker(context)
      .validateHas(permissions.siteEdit);

    return new SiteService(context).update(
      args.id,
      args.data
    );
  },
};

exports.schema = schema;
exports.resolver = resolver;
