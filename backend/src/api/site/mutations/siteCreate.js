const SiteService = require('../../../services/siteService');

const schema = `
  siteCreate(data: SiteInput!): Site!
`;

const resolver = {
  siteCreate: async (root, args, context) => {
    return new SiteService(context).create(args.data);
  },
};

exports.schema = schema;
exports.resolver = resolver;
