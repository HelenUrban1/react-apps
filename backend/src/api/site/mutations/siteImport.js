const SiteService = require('../../../services/siteService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions')
  .values;

const schema = `
  siteImport(data: SiteInput!, importHash: String!): Boolean
`;

const resolver = {
  siteImport: async (root, args, context) => {
    new PermissionChecker(context)
      .validateHas(permissions.siteImport);

    await new SiteService(context).import(
      args.data,
      args.importHash
    );

    return true;
  },
};

exports.schema = schema;
exports.resolver = resolver;
