const SiteService = require('../../../services/siteService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions')
  .values;

const schema = `
  siteDestroy(ids: [String!]!): Boolean
`;

const resolver = {
  siteDestroy: async (root, args, context) => {
    new PermissionChecker(context)
      .validateHas(permissions.siteDestroy);

    await new SiteService(context).destroyAll(
      args.ids
    );

    return true;
  },
};

exports.schema = schema;
exports.resolver = resolver;
