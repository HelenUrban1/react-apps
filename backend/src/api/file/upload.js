const fs = require('fs');
const path = require('path');
const multiparty = require('multiparty');
const { s3 } = require('aws-wrapper');
const config = require('../../../config')();
const PermissionChecker = require('../../services/iam/permissionChecker');

const { log } = require('../../logger');

function ensureDirectoryExistence(filePath) {
  const dirname = path.dirname(filePath);

  if (fs.existsSync(dirname)) {
    return true;
  }

  ensureDirectoryExistence(dirname);
  fs.mkdirSync(dirname);
  if (fs.existsSync(dirname)) {
    return true;
  }
  return false;
}

// QUESTION: This looks like global state. Can it be encapsulated in a controller?
let responseCode = 200;
let responseError = null;

// TODO: Is this function still needed?
const trackError = (req, error = {}) => {
  const analytics = req.analyticsClient;
  analytics.track({
    req,
    userId: req.currentUser?.id || 'unauthenticated',
    ixcEvent: analytics.events.errorsFileUpload,
    properties: {
      error,
    },
  });
};

const validateFileType = (file, fileTypes) => {
  if (!file || !fileTypes || fileTypes.length === 0) return true;
  if (!fileTypes.includes(path.extname(file).replace('.', '').toLocaleLowerCase())) {
    responseError = `File ${file} does not have a valid extension of: ${fileTypes.join()}`;
    responseCode = 400;
    return false;
  }
  return true;
};

// QUESTION: Why do uploadToLocalStorage and uploadToRemoteStorage not have the same return value/pattern?
const uploadToLocalStorage = (file, fields, fileTypes) => {
  if (!file && !file.originalFilename) {
    // TODO: This error isn't logged or thrown, so it will never hit a trap
    fs.unlinkSync(file);
    responseCode = 500;
    responseError = 'Invalid Request: File or filename not provided.';
    return;
  }
  if (file.originalFilename && fileTypes && fileTypes.length > 0 && !validateFileType(file.originalFilename, fileTypes)) {
    log.error(`uploadToLocalStorage failed file type check: ${file.originalFilename} : ${fileTypes}`);
    return;
  }
  log.debug(`uploadToLocalStorage passed file type check: ${file.originalFilename} : ${fileTypes}`);
  const privateUrl = path.join(config.uploadDir, fields.accountId, fields.siteId, fields.uniqueFileName[0]);
  ensureDirectoryExistence(privateUrl);
  fs.renameSync(file.path, privateUrl);
  responseCode = 200;
};

const uploadToRemoteStorage = async (file, fields, fileTypes) => {
  try {
    let tags = fields.partId && fields.drawingId ? `partId=${fields.partId}&drawingId=${fields.drawingId}&accountId=${fields.accountId}&siteId=${fields.siteId}` : '';
    if (fields.partId && fields.drawingId && fields.autoballoon === 'true') {
      tags += '&autoballoon=true';
    }
    if (file.filename && fileTypes && fileTypes.length > 0 && !validateFileType(file.filename, fileTypes)) {
      // code and message set in validateFileType
      log.error(`uploadToRemoteStorage failed file type check: ${file.filename} : ${fileTypes}`);
      return responseCode;
    }
    log.debug(`uploadToRemoteStorage passed file type check: ${file.filename} : ${fileTypes}`);

    await s3.upload(config.s3UploadBucket, `${fields.accountId}/${fields.siteId}/${fields.uniqueFileName}`, file, tags);
    responseCode = 200;
    return responseCode;
  } catch (error) {
    // TODO: This error isn't logged or thrown, so it will never hit a trap
    log.error(`uploadToRemoteStorage error : ${error}`);
    responseCode = 500;
    responseError = error;
    return responseCode;
  }
};

const request =
  (
    folder,
    validations = {
      entity: null,
      maxFileSize: null,
      folderIncludesAuthenticationUid: false,
      fileType: null,
    }
  ) =>
  (req, res) => {
    // If user is not logged in, return 403 (access forbidden)
    if (!req.currentUser) {
      res.sendStatus(403);
      return;
    }

    // If user is logged in, but are not authorized to upload, return 403 (access forbidden)
    if (
      validations.entity &&
      !new PermissionChecker({
        language: null,
        currentUser: req.currentUser,
        rolesMapper: req.rolesMapper,
      }).hasStorageFolder(validations.entity)
    ) {
      res.sendStatus(403);
      return;
    }

    // If user is logged in, has file transfer permissions, but is trying to access a file that belongs to someone else, return 403 (access forbidden)
    if (validations.folderIncludesAuthenticationUid) {
      const newFolder = folder.replace(':userId', req.currentUser.authenticationUid);
      if (!req.currentUser.authenticationUid || !newFolder.includes(req.currentUser.authenticationUid)) {
        res.sendStatus(403);
        return;
      }
    }

    const fields = {};
    const multipartyConfig = {};

    // Configure multiparty
    // QUESTION: Should this be done globally on startup rather than during each request?
    if (!config.uploadRemote) {
      multipartyConfig.uploadDir = config.uploadDir;
    }

    if (validations && validations.maxFileSize) {
      multipartyConfig.maxFilesSize = validations.maxFileSize;
    }

    const form = new multiparty.Form(multipartyConfig);
    const analytics = req.analyticsClient;
    log.debug(`Upload files to remote = ${config.uploadRemote}`);

    if (config.uploadRemote) {
      form.on('field', (name, value) => {
        fields[name] = value;
      });

      fields.accountId = req.currentUser.accountId;
      fields.siteId = req.currentUser.siteId;

      form.on('part', async (part) => {
        await uploadToRemoteStorage(part, fields, validations.fileType);
      });

      form.on('close', () => {
        if (responseError) {
          res.sendStatus(responseCode).send(responseError);
          log.error(`failed remote upload: ${responseError}`);
          throw responseError;
        } else {
          res.sendStatus(responseCode);
          // Record successful upload event in product analytics
          analytics.track({
            req,
            userId: req.currentUser?.id || 'unauthenticated',
            ixcEvent: analytics.events.userFileUpload,
          });
        }
      });

      form.on('error', (err) => {
        log.error(err);
      });

      form.parse(req);
    } else {
      form.parse(req, async (err, fields, files) => {
        if (err) {
          log.error(err);
          res.sendStatus(500).send(err);
          // throw responseError;
          /*
        This error originated either by throwing inside of an async function without a catch block, or by rejecting a promise which was not handled with .catch(). The promise rejected with the reason:
      Error [ERR_HTTP_HEADERS_SENT]: Cannot set headers after they are sent to the client
        */
        } else {
          fields.accountId = req.currentUser.accountId;
          fields.siteId = req.currentUser.siteId;

          uploadToLocalStorage(files.file[0], fields, validations.fileType);
          res.sendStatus(responseCode);
          if (responseCode === 200) {
            // Record successful upload event in product analytics
            analytics.track({
              req,
              userId: req.currentUser?.id || 'unauthenticated',
              ixcEvent: analytics.events.userFileUpload,
            });
          }
        }
      });
    }
  };

const drawingFileTypes = ['pdf'];
const templateFileTypes = ['xlsx'];
const imageFileTypes = ['png', 'jpg', 'jpeg'];
const attachmentFileTypes = ['xlsx', 'pdf', 'doc', 'docx', 'txt', 'csv'];

const mapAllUploadRequests = (prefix, app, databaseMiddleware, authorizationLayer) => {
  app.post(
    `${prefix}/upload/user/avatars/iam`,
    databaseMiddleware,
    authorizationLayer,
    request('user/avatars/iam', {
      entity: 'user',
      maxFileSize: 10 * 1024 * 1024,
      folderIncludesAuthenticationUid: false,
      fileType: imageFileTypes,
    })
  );

  app.post(
    `${prefix}/upload/user/avatars/profile/:userId`,
    databaseMiddleware,
    authorizationLayer,
    request('user/avatars/profile/:userId', {
      entity: null,
      maxFileSize: 10 * 1024 * 1024,
      folderIncludesAuthenticationUid: true,
      fileType: imageFileTypes,
    })
  );

  app.post(
    `${prefix}/upload/account/orgLogo`,
    databaseMiddleware,
    authorizationLayer,
    request('account/orgLogo', {
      entity: 'account',
      maxFileSize: undefined,
      folderIncludesAuthenticationUid: false,
      fileType: imageFileTypes,
    })
  );

  app.post(
    `${prefix}/upload/drawing/file`,
    databaseMiddleware,
    authorizationLayer,
    request('drawing/file', {
      entity: 'drawing',
      maxFileSize: undefined,
      folderIncludesAuthenticationUid: false,
      fileType: drawingFileTypes,
    })
  );

  app.post(
    `${prefix}/upload/part/file`,
    databaseMiddleware,
    authorizationLayer,
    request('drawing/file', {
      entity: 'drawing',
      maxFileSize: undefined,
      folderIncludesAuthenticationUid: false,
      fileType: attachmentFileTypes,
    })
  );

  app.post(
    `${prefix}/upload/reportTemplate/file`,
    databaseMiddleware,
    authorizationLayer,
    request('reportTemplate/file', {
      entity: 'reportTemplate',
      maxFileSize: undefined,
      folderIncludesAuthenticationUid: false,
      fileType: templateFileTypes,
    })
  );

  app.post(
    `${prefix}/upload/report/file`,
    databaseMiddleware,
    authorizationLayer,
    request('report/file', {
      entity: 'report',
      maxFileSize: undefined,
      folderIncludesAuthenticationUid: false,
      fileType: templateFileTypes,
    })
  );
};

exports.mapAllUploadRequests = mapAllUploadRequests;
exports.uploadToRemoteStorage = uploadToRemoteStorage;
exports.uploadToLocalStorage = uploadToLocalStorage;
exports.ensureDirectoryExistence = ensureDirectoryExistence;
exports.validateFileType = validateFileType;
