const path = require('path');
const { s3 } = require('aws-wrapper');
const config = require('../../../config')();
const { log } = require('../../logger');

const downloadLocalFile = (req, res, privateUrl) => {
  try {
    if (!req.currentUser) {
      res.download(path.join(config.uploadDir, privateUrl));
    } else {
      res.download(path.join(config.uploadDir, req.currentUser.accountId, req.currentUser.siteId, privateUrl));
    }
  } catch (error) {
    log.error(`download.downloadLocalFile err: ${error}`);
    res.sendStatus(404).send(error);
  }
};

const downloadRemoteFile = async (req, res, privateUrl) => {
  try {
    const result = await s3.getObject(config.s3UploadBucket, `${req.currentUser.accountId}/${req.currentUser.siteId}/${privateUrl}`);
    res.type(privateUrl);
    res.writeHeader(200, {
      'Content-Disposition': `attachment; filename="${privateUrl}"`,
    });
    res.end(result.Body);
  } catch (error) {
    log.error(`download.downloadRemoteFile err: ${error}`);
    res.sendStatus(404).send(error);
  }
};

module.exports = async (req, res) => {
  const { privateUrl } = req.query;
  if (!privateUrl) {
    log.warn('No privateUrl found');
    return res.sendStatus(404);
  }
  log.debug(`downloading ${privateUrl}`);
  let fileDownload;

  if (config.uploadRemote) {
    fileDownload = await downloadRemoteFile(req, res, privateUrl);
  } else {
    fileDownload = downloadLocalFile(req, res, privateUrl);
  }

  log.debug(fileDownload);
  return fileDownload;
};

module.exports.downloadRemoteFile = downloadRemoteFile;
module.exports.downloadLocalFile = downloadLocalFile;
