const { uploadToRemoteStorage } = require('./upload');
const { validateFileType } = require('./upload');

const res = {};
const filename = 'test.pdf';
const uniqueFileName = '33333333-3333-3333-3333-333333333333.pdf';
const file = Buffer.from('file contents');
file.filename = filename;
const fields = {
  uniqueFileName,
  partId: '11111111-1111-1111-1111-111111111111',
  drawingId: '22222222-2222-2222-2222-222222222222',
  accountId: '44444444-4444-4444-4444-444444444444',
  siteId: '55555555-5555-5555-5555-555555555555',
  autoballoon: 'true',
};
const drawingFileTypes = ['pdf'];
const templateFileTypes = ['xlsx'];
const imageFileTypes = ['png', 'jpg', 'jpeg'];
const attachmentFileTypes = ['xlsx', 'pdf', 'doc', 'docx', 'txt', 'csv'];

describe('upload to remote storage', () => {
  beforeEach(async () => {});
  it('should return 200 status', async () => {
    const responseCode = await uploadToRemoteStorage(file, fields);
    expect(responseCode).toBe(200);
  });
  it('should validate the file type', () => {
    expect(validateFileType('test.pdf', drawingFileTypes)).toBeTruthy();
    expect(validateFileType('test.xlsx', drawingFileTypes)).toBeFalsy();
    expect(validateFileType('test.rndm', drawingFileTypes)).toBeFalsy();
    expect(validateFileType('test.xlsx', templateFileTypes)).toBeTruthy();
    expect(validateFileType('test.pdf', templateFileTypes)).toBeFalsy();
    expect(validateFileType('test.rndm', drawingFileTypes)).toBeFalsy();
    expect(validateFileType('test.jpg', imageFileTypes)).toBeTruthy();
    expect(validateFileType('test.jpeg', imageFileTypes)).toBeTruthy();
    expect(validateFileType('test.png', imageFileTypes)).toBeTruthy();
    expect(validateFileType('test.pdf', imageFileTypes)).toBeFalsy();
    expect(validateFileType('test.rndm', drawingFileTypes)).toBeFalsy();
    expect(validateFileType('test.pdf', attachmentFileTypes)).toBeTruthy();
    expect(validateFileType('test.xlsx', attachmentFileTypes)).toBeTruthy();
    expect(validateFileType('test.txt', attachmentFileTypes)).toBeTruthy();
    expect(validateFileType('test.csv', attachmentFileTypes)).toBeTruthy();
    expect(validateFileType('test.doc', attachmentFileTypes)).toBeTruthy();
    expect(validateFileType('test.docx', attachmentFileTypes)).toBeTruthy();
    expect(validateFileType('test.png', attachmentFileTypes)).toBeFalsy();
    expect(validateFileType('test.jpg', attachmentFileTypes)).toBeFalsy();
  });
  it('should disregard casing', () => {
    expect(validateFileType('test.PDF', drawingFileTypes)).toBeTruthy();
    expect(validateFileType('test.PdF', drawingFileTypes)).toBeTruthy();
    expect(validateFileType('test.XLSX', templateFileTypes)).toBeTruthy();
    expect(validateFileType('test.xLsX', templateFileTypes)).toBeTruthy();
    expect(validateFileType('test.JPG', imageFileTypes)).toBeTruthy();
    expect(validateFileType('test.jPg', imageFileTypes)).toBeTruthy();
  });
});
