const { PDFNet } = require('@pdftron/pdfnet-node');
const path = require('path');
const fs = require('fs');
const { s3 } = require('aws-wrapper');
const config = require('../../../config')();

const CharacteristicRepository = require('../../database/repositories/characteristicRepository');
const AnnotationRepository = require('../../database/repositories/annotationRepository');
const MarkerRepository = require('../../database/repositories/markerRepository');
const PartRepository = require('../../database/repositories/partRepository');

const { log } = require('../../logger');

PDFNet.runWithCleanup(() => {
  log.info('PDFTron Initialized.');
}, config.pdfTronSdkKey).catch((error) => {
  log.error('Error initializing PDFTron');
  log.error(error);
});

const getFileBuffer = async (saveFilesTo, configValues, privateUrl, fileSystem, currentUser) => {
  if (saveFilesTo === 's3') {
    try {
      const result = await fileSystem.getObject(configValues.s3UploadBucket, `${currentUser.accountId}/${currentUser.siteId}/${privateUrl}`);
      return result.Body;
    } catch (error) {
      log.error(error);
      return null;
    }
  }
  return fileSystem.readFileSync(path.join(configValues.uploadDir, currentUser.accountId, currentUser.siteId, privateUrl));
};

// Rotations are an enum, 0 = 0, 90 = 1, 180 = 2, 270 = 3;
// so we can add together and mod 4 to get the sum of the rotations
const getNewRotation = (pdfRotation, sheetRotation) => {
  let sheetRotationValue = 0;
  switch (sheetRotation) {
    case 'e_0':
      sheetRotationValue = 0;
      break;
    case 'e_90':
      sheetRotationValue = 1;
      break;
    case 'e_180':
      sheetRotationValue = 2;
      break;
    case 'e_270':
      sheetRotationValue = 3;
      break;
    default:
      break;
  }
  return (sheetRotationValue + pdfRotation) % 4;
};

const transformRectangle = (x, y, pageWidth, pageHeight, theta) => {
  let newTheta = theta;
  if (newTheta >= 360) {
    newTheta -= 360;
  }

  let xPrim = x;
  let yPrim = y;

  if (newTheta === 90) {
    xPrim = Math.round(pageWidth - y);
    yPrim = x;
  } else if (newTheta === 180) {
    xPrim = Math.round(pageWidth - x);
    yPrim = Math.round(pageHeight - y);
  } else if (newTheta === 270) {
    xPrim = y;
    yPrim = Math.round(pageHeight - x);
  }
  return { x: xPrim, y: yPrim };
};

const getBalloonLocation = async (characteristic, page) => {
  const pageHeight = await page.getPageHeight();
  const pageWidth = await page.getPageWidth();
  let newTheta = characteristic.markerLocationY;
  let rotation = 0;
  const pageRotation = await page.getRotation();
  if (pageRotation === 1) {
    rotation = 90;
  } else if (pageRotation === 2) {
    rotation = 180;
  } else if (pageRotation === 3) {
    rotation = 270;
  }
  let additionalRotation = 0;
  if (rotation !== characteristic.drawingRotation || rotation !== characteristic.markerRotation) {
    additionalRotation = rotation;
    if (rotation === characteristic.markerRotation) {
      // rotation and marker rotation match we don't need to change anything
      additionalRotation = 0;
    } else {
      // for everything else just use the marker rotation
      additionalRotation -= characteristic.markerRotation;
    }
  }
  newTheta += additionalRotation;
  if (newTheta > 360) {
    newTheta -= 360;
  }
  let angle;
  let x;
  let y;
  if (pageRotation === 1) {
    // page rotation is 90 degrees
    angle = (newTheta * Math.PI) / 180;
    x = pageWidth - (characteristic.boxLocationY + characteristic.boxHeight / 2);
    y = pageHeight - characteristic.boxLocationX - characteristic.boxWidth / 2;
  } else if (pageRotation === 2) {
    // page rotation is 180 degrees
    angle = (newTheta * Math.PI) / 180;
    x = pageWidth - characteristic.boxLocationX - characteristic.boxWidth / 2;
    y = characteristic.boxLocationY + characteristic.boxHeight / 2;
  } else if (pageRotation === 3) {
    // page rotation is 270 degrees
    angle = (newTheta * Math.PI) / 180;
    x = characteristic.boxLocationY + characteristic.boxHeight / 2;
    y = characteristic.boxLocationX + characteristic.boxWidth / 2;
  } else {
    // page rotation is 0
    angle = (newTheta * Math.PI) / 180;
    x = characteristic.boxLocationX + characteristic.boxWidth / 2;
    y = pageHeight - (characteristic.boxLocationY + characteristic.boxHeight / 2);
  }
  return { angle, x, y };
};

const getAnnotationDimensions = async (annotation, page) => {
  const pageHeight = await page.getPageHeight();
  const dims = {
    x: annotation.boxLocationX,
    y: pageHeight - annotation.boxHeight - annotation.boxLocationY,
    width: annotation.boxWidth,
    height: annotation.boxHeight,
    angle: annotation.boxRotation,
  };

  return dims;
};

const translateColorName = (colorName) => {
  switch (colorName) {
    case 'Purple':
      return '#963cbd';
    case 'Coral':
      return '#e56a54';
    case 'Sunshine':
      return '#efbe7d';
    case 'Seafoam':
      return '#69dbc8';
    case 'Black':
      return '#000000';
    case 'White':
      return '#ffffff';
    default:
      return colorName;
  }
};

const rgbToColorPt = async (color) => {
  return PDFNet.ColorPt.init(color.r / 255, color.g / 255, color.b / 255);
};

const hexToRgb = (hex) => {
  const result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
  return result
    ? {
        r: parseInt(result[1], 16),
        g: parseInt(result[2], 16),
        b: parseInt(result[3], 16),
      }
    : null;
};

const getColorPtFromHex = async (color) => {
  let htmlColor = translateColorName(color);
  if (!htmlColor.startsWith('#')) {
    htmlColor = `#${htmlColor}`;
  }
  return rgbToColorPt(hexToRgb(htmlColor));
};

const getAnnotationTextMatrix = async (shapeDefinition, textDefinition, textBoxWidth, textBoxHeight) => {
  switch (shapeDefinition.newRotation) {
    case 1: // drawing was rotated 90
      return PDFNet.Matrix2D.create(0, 1, -1, 0, shapeDefinition.midpointX + textBoxHeight / 2 + textDefinition.offset, shapeDefinition.midpointY + textDefinition.offset);
    case 2: // drawing was rotated 180
      return PDFNet.Matrix2D.create(-1, 0, 0, -1, shapeDefinition.midpointX + shapeDefinition.boxWidth - textDefinition.offset, shapeDefinition.midpointY + textBoxHeight / 2 + textDefinition.offset);
    case 3: // drawing was rotated 270
      return PDFNet.Matrix2D.create(0, -1, 1, 0, shapeDefinition.midpointX + shapeDefinition.boxWidth - textBoxHeight / 2 - textDefinition.offset, shapeDefinition.midpointY + shapeDefinition.boxHeight - textDefinition.offset);
    default:
      // 0
      return PDFNet.Matrix2D.create(1, 0, 0, 1, shapeDefinition.midpointX + textDefinition.offset, shapeDefinition.midpointY + shapeDefinition.boxHeight - textBoxHeight / 2 - textDefinition.offset);
  }
};

const getTextMatrix = async (shapeDefinition, textDefinition, textBoxWidth, textBoxHeight) => {
  if (shapeDefinition.boxHeight) {
    switch (shapeDefinition.newRotation) {
      case 1: // 90
        return PDFNet.Matrix2D.create(0, 1, -1, 0, shapeDefinition.midpointX + textBoxHeight / 2 + textDefinition.offset, shapeDefinition.midpointY - textBoxWidth / 2);
      case 2: // 180
        return PDFNet.Matrix2D.create(-1, 0, 0, -1, shapeDefinition.midpointX + textBoxWidth / 2, shapeDefinition.midpointY + textBoxHeight / 2 + textDefinition.offset);
      case 3: // 270
        return PDFNet.Matrix2D.create(0, -1, 1, 0, shapeDefinition.midpointX - textBoxHeight / 2 - textDefinition.offset, shapeDefinition.midpointY + textBoxWidth / 2);
      default:
        // 0
        return PDFNet.Matrix2D.create(1, 0, 0, 1, shapeDefinition.midpointX - shapeDefinition.boxWidth / 2 + textDefinition.offset, shapeDefinition.midpointY + shapeDefinition.boxHeight / 2 - textBoxHeight);
    }
  }
  switch (shapeDefinition.newRotation) {
    case 1: // 90
      return PDFNet.Matrix2D.create(0, 1, -1, 0, shapeDefinition.midpointX + textBoxHeight / 2 + textDefinition.offset, shapeDefinition.midpointY - textBoxWidth / 2);
    case 2: // 180
      return PDFNet.Matrix2D.create(-1, 0, 0, -1, shapeDefinition.midpointX + textBoxWidth / 2, shapeDefinition.midpointY + textBoxHeight / 2 + textDefinition.offset);
    case 3: // 270
      return PDFNet.Matrix2D.create(0, -1, 1, 0, shapeDefinition.midpointX - textBoxHeight / 2 - textDefinition.offset, shapeDefinition.midpointY + textBoxWidth / 2);
    default:
      // 0
      return PDFNet.Matrix2D.create(1, 0, 0, 1, shapeDefinition.midpointX - textBoxWidth / 2, shapeDefinition.midpointY - textBoxHeight / 2 - textDefinition.offset);
  }
};

const drawText = async (textDefinition, shapeDefinition, builder) => {
  const textElement = await builder.createTextRun(textDefinition.balloonText || textDefinition.annotationText, textDefinition.font, textDefinition.fontSize);
  const textGState = await textElement.getGState();
  await textGState.setFillColorSpace(await PDFNet.ColorSpace.createDeviceRGB());
  await textGState.setFillColorWithColorPt(await getColorPtFromHex(textDefinition.fontColor));

  const textBox = await textElement.getBBox();
  const textBoxHeight = await textBox.height();
  const textBoxWidth = await textBox.width();

  const textMatrix = textDefinition.annotationText ? await getAnnotationTextMatrix(shapeDefinition, textDefinition, textBoxWidth, textBoxHeight) : await getTextMatrix(shapeDefinition, textDefinition, textBoxWidth, textBoxHeight);

  await textElement.setTextMatrix(textMatrix);
  return textElement;
};

const setShapeElementOptions = async (shapeElement, fillColor, borderColor, borderWidth, borderStyle) => {
  // these options are independent of shape
  await shapeElement.setPathFill(true);
  await shapeElement.setPathStroke(true);
  const shapeGState = await shapeElement.getGState();
  await shapeGState.setFillColorSpace(await PDFNet.ColorSpace.createDeviceRGB());
  await shapeGState.setFillColorWithColorPt(await getColorPtFromHex(fillColor));
  await shapeGState.setLineWidth(borderWidth);
  if (borderStyle !== 'Solid') {
    await shapeGState.setDashPattern(borderStyle === 'Dashed' ? [5, 2] : [2, 2], 0);
  }
  await shapeGState.setStrokeColorSpace(await PDFNet.ColorSpace.createDeviceRGB());
  await shapeGState.setStrokeColorWithColorPt(await getColorPtFromHex(borderColor));
};

const drawPoints = async (builder, pathPoints) => {
  const pointsLength = pathPoints.length;
  await builder.pathBegin();
  if (pointsLength > 0) {
    await builder.moveTo(pathPoints[pointsLength - 1].x, pathPoints[pointsLength - 1].y);
    for (let j = 0; j < pointsLength; j++) {
      // eslint-disable-next-line no-await-in-loop
      await builder.lineTo(pathPoints[j].x, pathPoints[j].y);
    }
  }
  return builder.pathEnd();
};

const drawDiamond = async (shapeDefinition, builder) => {
  const pathPoints = [
    { x: shapeDefinition.midpointX + shapeDefinition.radius, y: shapeDefinition.midpointY },
    { x: shapeDefinition.midpointX, y: shapeDefinition.midpointY + shapeDefinition.radius },
    { x: shapeDefinition.midpointX - shapeDefinition.radius, y: shapeDefinition.midpointY },
    { x: shapeDefinition.midpointX, y: shapeDefinition.midpointY - shapeDefinition.radius },
  ];
  return drawPoints(builder, pathPoints);
};

const regularPolygonPoints = (sideCount, radius, left, top) => {
  const sweep = (Math.PI * 2) / sideCount;
  const points = [];

  for (let i = 0; i < sideCount; i++) {
    const x = Math.round((radius + radius * Math.cos(i * sweep) + top) * 10) / 10;
    const y = Math.round((radius * 1.2 + radius * Math.sin(i * sweep) + left) * 10) / 10;
    points.push({ x, y });
  }

  return points;
};

const getRotationMatrix = async (rotation) => {
  // Rotations are produced by [cos(A) sin(A) -sin(A) cos(A) 0 0],
  // which has the effect of rotating the coordinate system axes by an angle 'A'
  // counterclockwise.
  switch (rotation) {
    case 0:
      return PDFNet.Matrix2D.create(1, 0, 0, 1, 0, 0);
    case 1:
      return PDFNet.Matrix2D.create(0, 1, -1, 0, 0, 0);
    case 2:
      return PDFNet.Matrix2D.create(-1, 0, 0, -1, 0, 0);
    case 3:
      return PDFNet.Matrix2D.create(0, -1, 1, 0, 0, 0);
    default:
      return PDFNet.Matrix2D.create(1, 0, 0, 1, 0, 0);
  }
};

const correctPathForRotation = async (pathPoints, cx, cy, rotation) => {
  const rotationMatrix = await getRotationMatrix(rotation);
  const output = [];
  for (let i = 0; i < pathPoints.length; i++) {
    let point = pathPoints[i];
    // we define all coordinates as relative to the center
    // which means we can rotate around the origin to correct our orientation
    const x = point.x - cx;
    const y = point.y - cy;
    // eslint-disable-next-line no-await-in-loop
    point = await rotationMatrix.mult(x, y);
    point.x += cx;
    point.y += cy;
    output.push(point);
  }
  return output;
};

const rotatePoints = (pathPoints, rotation, midpointX, midpointY) => {
  const s = Math.sin((Math.PI / 180) * rotation);
  const c = Math.cos((Math.PI / 180) * rotation);

  const output = [];
  for (let i = 0; i < pathPoints.length; i++) {
    // translate point back to origin:
    let x = pathPoints[i].x - midpointX;
    let y = pathPoints[i].y - midpointY;

    // rotate point
    const xNew = x * c - y * s;
    const yNew = x * s + y * c;

    // translate point back:
    x = Math.round(xNew + midpointX);
    y = Math.round(yNew + midpointY);
    output.push({ x, y });
  }

  return output;
};

const drawSquare = async (shapeDefinition, builder) => {
  return builder.createRect(shapeDefinition.midpointX - shapeDefinition.radius, shapeDefinition.midpointY - shapeDefinition.radius, shapeDefinition.radius * 2, shapeDefinition.radius * 2);
};

const drawRectangle = async (shapeDefinition, builder) => {
  return builder.createRect(shapeDefinition.midpointX - shapeDefinition.hWidth, shapeDefinition.midpointY - shapeDefinition.hHeight, shapeDefinition.hWidth * 2, shapeDefinition.hHeigh * 2);
};

const drawHexagon = async (shapeDefinition, builder) => {
  let pathPoints = regularPolygonPoints(6, shapeDefinition.radius, shapeDefinition.midpointY - shapeDefinition.radius, shapeDefinition.midpointX - shapeDefinition.radius);
  pathPoints = await correctPathForRotation(pathPoints, shapeDefinition.midpointX, shapeDefinition.midpointY, shapeDefinition.newRotation);
  return drawPoints(builder, pathPoints);
};

const drawTriangle = async (shapeDefinition, builder) => {
  let pathPoints = regularPolygonPoints(3, shapeDefinition.radius * 1.4, shapeDefinition.midpointY - shapeDefinition.radius * 1.7, shapeDefinition.midpointX - shapeDefinition.radius * 1.6);
  pathPoints = rotatePoints(pathPoints, 90, shapeDefinition.midpointX, shapeDefinition.midpointY);
  pathPoints = await correctPathForRotation(pathPoints, shapeDefinition.midpointX, shapeDefinition.midpointY, shapeDefinition.newRotation);
  return drawPoints(builder, pathPoints);
};

const drawCircle = async (shapeDefinition, builder) => {
  return builder.createEllipse(shapeDefinition.midpointX, shapeDefinition.midpointY, shapeDefinition.radius, shapeDefinition.radius);
};

const drawLine = async (shapeDefinition, builder) => {
  const pathPoints = [
    { x: shapeDefinition.balloonLocationX, y: shapeDefinition.balloonLocationY },
    { x: shapeDefinition.connectionPointLocationX, y: shapeDefinition.connectionPointLocationY },
  ];
  return drawPoints(builder, pathPoints);
};

const drawShape = async (shapeDefinition, builder) => {
  let shapeElement;
  let offset;
  switch (shapeDefinition.shape.toLowerCase()) {
    case 'diamond':
    case 'rhombus':
      shapeElement = await drawDiamond(shapeDefinition, builder);
      offset = -shapeDefinition.radius * 0.1;
      break;
    case 'square':
      shapeElement = await drawSquare(shapeDefinition, builder);
      offset = -shapeDefinition.radius * 0.1;
      break;
    case 'hexagon':
      shapeElement = await drawHexagon(shapeDefinition, builder);
      offset = -shapeDefinition.radius * 0.3;
      break;
    case 'triangle':
      shapeElement = await drawTriangle(shapeDefinition, builder);
      offset = shapeDefinition.radius * 0.3;
      break;
    case 'circle':
      shapeElement = await drawCircle(shapeDefinition, builder);
      offset = -shapeDefinition.radius * 0.1;
      break;
    case 'line':
      shapeElement = await drawLine(shapeDefinition, builder);
      offset = 0;
      break;
    default:
      shapeElement = null;
      offset = 0;
      break;
  }

  return { shapeElement, offset };
};

// Font size scaling based on the length of a balloon label
// Shrink the text 20% for each character over 2
const getFontSizeModifier = (label) => {
  if (!label || label.length <= 2) {
    return 1;
  }
  if (label.length >= 7) {
    return 0.2;
  }
  return 1 - 0.2 * (label.length - 2);
};

const renderCharacteristic = async (characteristic, page, newRotation, pdf, part) => {
  // Shared balloon, skip
  if (characteristic.markerSubIndex !== null && characteristic.markerSubIndex !== -1 > 1 && characteristic.shared) {
    return;
  }
  const { angle, x, y } = await getBalloonLocation(characteristic, page);
  const writer = await PDFNet.ElementWriter.create();
  await writer.beginOnPage(page);

  const builder = await PDFNet.ElementBuilder.create();
  // need to render line first so balloons will be over it
  const width = characteristic.markerFontSize * 3;
  if (part.displayLeaderLines && characteristic.displayLeaderLine && (parseFloat(characteristic.leaderLineDistance) > width || characteristic.connectionPointIsFloating)) {
    let newX;
    let newY;
    // probably need to do some stupid math here
    if (characteristic.connectionPointIsFloating) {
      if (newRotation === 1) {
        // adjust for x and y and flip for 90 degree layout
        newX = (await page.getPageWidth()) - parseFloat(characteristic.connectionPointLocationY);
        newY = (await page.getPageHeight()) - parseFloat(characteristic.connectionPointLocationX);
      } else if (newRotation === 2) {
        // adjust the x for 180 degree layout
        newX = (await page.getPageWidth()) - parseFloat(characteristic.connectionPointLocationX);
        newY = parseFloat(characteristic.connectionPointLocationY);
      } else if (newRotation === 3) {
        // straight swap of x and y for 270 degree layout (I don't get this)
        newX = parseFloat(characteristic.connectionPointLocationY);
        newY = parseFloat(characteristic.connectionPointLocationX);
      } else {
        // adjust the y for 0 degree layout (default)
        newX = parseFloat(characteristic.connectionPointLocationX);
        newY = (await page.getPageHeight()) - parseFloat(characteristic.connectionPointLocationY);
      }
    } else {
      // sets rotation
      let rotation = newRotation;
      if (rotation === 1) {
        rotation = 90;
      } else if (rotation === 2) {
        rotation = 180;
      } else if (rotation === 3) {
        rotation = 270;
      }
      let additionalRotation = 0;
      // calculates additionalRotation
      if (rotation !== characteristic.drawingRotation || rotation !== characteristic.markerRotation) {
        additionalRotation = rotation;
        if (rotation === characteristic.markerRotation) {
          // rotation and marker rotation match we don't need to change anything
          additionalRotation = 0;
        } else {
          // for everything else just use the marker rotation
          additionalRotation -= characteristic.markerRotation;
        }
      }
      if (newRotation === 1) {
        // drawings with 90 degree base rotation
        if (additionalRotation === -90) {
          // swap x and y
          newX = parseFloat(characteristic.connectionPointLocationY);
          newY = parseFloat(characteristic.connectionPointLocationX);
        } else if (additionalRotation === -180) {
          // adjust for page width
          newX = (await page.getPageWidth()) - parseFloat(characteristic.connectionPointLocationX);
          newY = parseFloat(characteristic.connectionPointLocationY);
        } else if (additionalRotation === 90) {
          // adjust for page width and height
          newX = (await page.getPageWidth()) - parseFloat(characteristic.connectionPointLocationY);
          newY = (await page.getPageHeight()) - parseFloat(characteristic.connectionPointLocationX);
        } else {
          // adjust for page height
          newX = parseFloat(characteristic.connectionPointLocationX);
          newY = (await page.getPageHeight()) - parseFloat(characteristic.connectionPointLocationY);
        }
      } else if (newRotation === 2) {
        // drawings with 180 degree base rotation
        if (additionalRotation === -90) {
          // swap x and y
          newX = parseFloat(characteristic.connectionPointLocationY);
          newY = parseFloat(characteristic.connectionPointLocationX);
        } else if (Math.abs(additionalRotation) === 180) {
          // adjust for page width
          newX = (await page.getPageWidth()) - parseFloat(characteristic.connectionPointLocationX);
          newY = parseFloat(characteristic.connectionPointLocationY);
        } else if (additionalRotation === 90) {
          // adjust for page width and height
          newX = (await page.getPageWidth()) - parseFloat(characteristic.connectionPointLocationY);
          newY = (await page.getPageHeight()) - parseFloat(characteristic.connectionPointLocationX);
        } else {
          // adjust for page height
          newX = parseFloat(characteristic.connectionPointLocationX);
          newY = (await page.getPageHeight()) - parseFloat(characteristic.connectionPointLocationY);
        }
      } else if (newRotation === 3) {
        // drawings with 270 degree base rotation
        if (additionalRotation === 270) {
          // swap x and y
          newX = parseFloat(characteristic.connectionPointLocationY);
          newY = parseFloat(characteristic.connectionPointLocationX);
        } else if (Math.abs(additionalRotation) === 180) {
          // adjust for page width
          newX = (await page.getPageWidth()) - parseFloat(characteristic.connectionPointLocationX);
          newY = parseFloat(characteristic.connectionPointLocationY);
        } else if (additionalRotation === 90) {
          // adjust for page width and height
          newX = (await page.getPageWidth()) - parseFloat(characteristic.connectionPointLocationY);
          newY = (await page.getPageHeight()) - parseFloat(characteristic.connectionPointLocationX);
        } else {
          // adjust for page height
          newX = parseFloat(characteristic.connectionPointLocationX);
          newY = (await page.getPageHeight()) - parseFloat(characteristic.connectionPointLocationY);
        }
      } else if (Math.abs(additionalRotation) === 90) {
        // normal drawings with no rotation (default)
        // swap x and y
        newX = parseFloat(characteristic.connectionPointLocationY);
        newY = parseFloat(characteristic.connectionPointLocationX);
      } else if (Math.abs(additionalRotation) === 180) {
        // adjust for page width
        newX = (await page.getPageWidth()) - parseFloat(characteristic.connectionPointLocationX);
        newY = parseFloat(characteristic.connectionPointLocationY);
      } else if (Math.abs(additionalRotation) === 270) {
        // adjust for page width and height
        newX = (await page.getPageWidth()) - parseFloat(characteristic.connectionPointLocationY);
        newY = (await page.getPageHeight()) - parseFloat(characteristic.connectionPointLocationX);
      } else {
        // adjust for page height
        newX = parseFloat(characteristic.connectionPointLocationX);
        newY = (await page.getPageHeight()) - parseFloat(characteristic.connectionPointLocationY);
      }
    }
    const leaderLineShapeDefinition = {
      balloonLocationX: Math.round(x + Math.cos(angle) * Math.abs(characteristic.markerLocationX)),
      balloonLocationY: Math.round(y - Math.sin(angle) * Math.abs(characteristic.markerLocationX)),
      connectionPointLocationX: newX,
      connectionPointLocationY: newY,
      shape: 'line',
    };
    const leaderLineShapeElement = await (await drawShape(leaderLineShapeDefinition, builder)).shapeElement;
    if (leaderLineShapeElement !== null) {
      await setShapeElementOptions(leaderLineShapeElement, characteristic.marker.leaderLineColor, characteristic.marker.leaderLineColor, characteristic.marker.leaderLineWidth, characteristic.marker.leaderLineStyle);

      await writer.writeElement(leaderLineShapeElement);

      await builder.reset();
    }
  }

  const shapeDefinition = {
    radius: characteristic.markerFontSize,
    midpointX: Math.round(x + Math.cos(angle) * Math.abs(characteristic.markerLocationX)),
    midpointY: Math.round(y - Math.sin(angle) * Math.abs(characteristic.markerLocationX)),
    shape: characteristic.marker.shape,
    newRotation: 0, // this part doesn't rotate correctly. PDF's are always set to rotation 0
  };

  const { shapeElement, offset } = await drawShape(shapeDefinition, builder);

  if (shapeElement !== null) {
    await setShapeElementOptions(shapeElement, characteristic.marker.fillColor, characteristic.marker.borderColor, characteristic.marker.borderWidth, characteristic.marker.borderStyle);

    await writer.writeElement(shapeElement);

    // write digits inside
    await writer.writeElement(await builder.createTextBegin());

    const fontSize = characteristic.markerFontSize;
    const balloonText = characteristic.markerGroupShared ? characteristic.markerGroup : characteristic.markerLabel;
    const fontSizeModifier = getFontSizeModifier(balloonText);

    await builder.reset();

    const textDefinition = {
      font: await PDFNet.Font.create(pdf, PDFNet.Font.StandardType1Font.e_helvetica),
      fontSize: fontSize * fontSizeModifier,
      balloonText,
      offset,
      fontColor: characteristic.marker.fontColor,
    };

    const textElement = await drawText(textDefinition, shapeDefinition, builder);
    await writer.writeElement(textElement);
    await writer.writeElement(await builder.createTextEnd());
    await writer.flush();
  }
  await writer.end();
};

const drawNewLine = async (textDefinition, builder, writer) => {
  const ele = await builder.createTextNewLineWithOffset(0, -textDefinition.fontSize - 4);
  await writer.writeElement(ele);
};

const writeTextLine = async (textDefinition, builder, writer) => {
  const ele = await builder.createTextRun(textDefinition.annotationText, textDefinition.font, textDefinition.fontSize);
  await writer.writeElement(ele);
};

const renderAnnotation = async (annotation, page, newRotation, pdf, part) => {
  const { x, y, width, height } = await getAnnotationDimensions(annotation, page, newRotation);

  const midpointX = x;
  const midpointY = y;

  const writer = await PDFNet.ElementWriter.create();
  await writer.beginOnPage(page);

  const builder = await PDFNet.ElementBuilder.create();

  const shapeElement = await builder.createRect(x, y, width, height);

  if (shapeElement !== null) {
    await setShapeElementOptions(shapeElement, annotation.backgroundColor, annotation.borderColor, 1, 'Solid');

    await writer.writeElement(shapeElement);

    // write text inside
    let ele = await builder.createTextBegin();
    await writer.writeElement(ele);

    const { fontSize } = annotation;
    const annotationText = annotation.annotation;

    await builder.reset();

    const textDefinition = {
      font: await PDFNet.Font.createTrueTypeFont(pdf, 'src/templates/InspectionXpertGDT.ttf'),
      fontSize: Math.floor(fontSize * 0.75), // renders larger than FabricJS
      annotationText,
      offset: 6,
      fontColor: annotation.fontColor,
    };

    const shapeDefinition = {
      midpointX,
      midpointY,
      boxWidth: width,
      boxHeight: height,
      newRotation: annotation.drawingRotation / 90,
    };

    // Makes sure words wrap whether user-added new line or because of line length overruns box width
    const regex = new RegExp(/<WRAP>|<NEWLINE>/g);
    const splitText = annotationText.split(regex);

    // set draw properties and write first line
    ele = await drawText({ ...textDefinition, annotationText: splitText[0] }, shapeDefinition, builder);
    await (await ele.getGState()).setLeading(15);
    await writer.writeElement(ele);

    for (let i = 1; i < splitText.length; i++) {
      await drawNewLine(textDefinition, builder, writer);
      await writeTextLine({ ...textDefinition, annotationText: splitText[i] }, builder, writer);
    }

    builder.createTextNewLine();

    await writer.writeElement(await builder.createTextEnd());
    await writer.flush();
  }
  await writer.end();
};

const renderPage = async (pageIndex, allCharacteristics, allAnnotations, pdf, part) => {
  const pageNumber = pageIndex + 1;
  const characteristics = allCharacteristics.filter((c) => c.drawingSheetIndex === pageNumber && c.marker);
  const annotations = allAnnotations.filter((a) => a.drawingSheetIndex === pageNumber);
  if (characteristics.length > 0) {
    const currentSheet = characteristics[0].drawingSheet;
    const page = await pdf.getPage(pageNumber);

    if (await page.isValid()) {
      await page.setUserUnitSize(1);
      const pdfPageRotation = await page.getRotation();
      let newRotation = pdfPageRotation;
      if (currentSheet.rotation !== 0) {
        newRotation = getNewRotation(pdfPageRotation, currentSheet.rotation);
      }
      const chars = [];
      for (let index = 0; index < characteristics.length; index++) {
        chars.push(renderCharacteristic(characteristics[index], page, newRotation, pdf, part));
      }
      await Promise.all(chars);
      await page.setRotation(newRotation);
    }
  }
  if (annotations.length > 0) {
    const currentSheet = annotations[0].drawingSheet;
    const page = await pdf.getPage(pageNumber);

    if (await page.isValid()) {
      await page.setUserUnitSize(1);
      const pdfPageRotation = await page.getRotation();
      let newRotation = pdfPageRotation;
      if (currentSheet.rotation !== 0) {
        newRotation = getNewRotation(pdfPageRotation, currentSheet.rotation);
      }
      const annots = [];
      for (let index = 0; index < annotations.length; index++) {
        annots.push(renderAnnotation(annotations[index], page, newRotation, pdf, part));
      }
      await Promise.all(annots);
      await page.setRotation(newRotation);
    }
  }
};

const createBalloonedPDF = async (privateUrl, currentUser, partId) => {
  const options = {
    currentUser,
  };
  const saveFilesTo = config.uploadRemote ? 's3' : 'local-fs';

  const fileSystem = saveFilesTo === 'local-fs' ? fs : s3;
  const drawing = await getFileBuffer(saveFilesTo, config, privateUrl, fileSystem, currentUser);
  if (!drawing) {
    return false;
  }

  const pdf = await PDFNet.PDFDoc.createFromBuffer(drawing);
  if (!pdf) {
    log.error('no PDF loaded');
    return undefined;
  }
  const numberOfPages = await pdf.getPageCount();
  const partResult = await new PartRepository().findById(partId, options);
  const characteristicRepository = new CharacteristicRepository();
  const allCharacteristicsResult = await characteristicRepository.findAndCountAll(
    {
      filter: {
        part: partId,
        status: 'Active',
      },
      orderBy: 'markerIndex_ASC',
      requestedAttributes: [
        'id',
        'status',
        'captureMethod',
        'drawingSheetIndex',
        'notationType',
        'notationSubtype',
        'notationClass',
        'quantity',
        'drawingRotation',
        'drawingScale',
        'boxLocationY',
        'boxLocationX',
        'boxWidth',
        'boxHeight',
        'boxRotation',
        'markerGroup',
        'markerIndex',
        'markerSubIndex',
        'markerLabel',
        'markerStyle',
        'markerLocationX',
        'markerLocationY',
        'marker',
        'drawing',
        'drawingSheet',
        'deletedAt',
        'verified',
      ],
    },
    options
  );
  const allCharacteristics = [...allCharacteristicsResult.rows];
  allCharacteristics.forEach((char, index) => {
    allCharacteristics[index].boxLocationY = parseFloat(char.boxLocationY);
    allCharacteristics[index].boxLocationX = parseFloat(char.boxLocationX);
    allCharacteristics[index].boxWidth = parseFloat(char.boxWidth);
    allCharacteristics[index].boxHeight = parseFloat(char.boxHeight);
    allCharacteristics[index].boxRotation = parseFloat(char.boxRotation);
    allCharacteristics[index].markerLocationX = parseFloat(char.markerLocationX);
    allCharacteristics[index].markerLocationY = parseFloat(char.markerLocationY);
    allCharacteristics[index].drawingRotation = parseFloat(char.drawingRotation);
    allCharacteristics[index].drawingScale = parseFloat(char.drawingScale);
  });
  const annotationRepository = new AnnotationRepository();
  const allAnnotationsResult = await annotationRepository.findAndCountAll(
    {
      filter: {
        part: partId,
        status: 'Active',
      },
      requestedAttributes: [
        'id',
        'annotation',
        'fontColor',
        'fontSize',
        'borderColor',
        'backgroundColor',
        'drawingSheetIndex',
        'drawingRotation',
        'drawingScale',
        'boxLocationY',
        'boxLocationX',
        'boxWidth',
        'boxHeight',
        'boxRotation',
        'drawing',
        'drawingSheet',
        'deletedAt',
      ],
    },
    options
  );
  const allAnnotations = [...allAnnotationsResult.rows];
  allAnnotations.forEach((annotation, index) => {
    allAnnotations[index].boxLocationY = parseFloat(annotation.boxLocationY);
    allAnnotations[index].boxLocationX = parseFloat(annotation.boxLocationX);
    allAnnotations[index].boxWidth = parseFloat(annotation.boxWidth);
    allAnnotations[index].boxHeight = parseFloat(annotation.boxHeight);
    allAnnotations[index].boxRotation = parseFloat(annotation.boxRotation);
    allAnnotations[index].drawingRotation = parseFloat(annotation.drawingRotation);
    allAnnotations[index].drawingScale = parseFloat(annotation.drawingScale);
  });
  const markerRepository = new MarkerRepository();
  const markerIds = [...new Set(allCharacteristics.map((char) => char.markerStyle))];
  for (let markerIndex = 0; markerIndex < markerIds.length; markerIndex++) {
    const markerId = markerIds[markerIndex];
    // eslint-disable-next-line no-await-in-loop
    const marker = await markerRepository.findById(markerId, options);
    for (let characteristicIndex = 0; characteristicIndex < allCharacteristics.length; characteristicIndex++) {
      if (allCharacteristics[characteristicIndex].markerStyle === markerId) {
        allCharacteristics[characteristicIndex].marker = marker;
      }
    }
  }
  const pages = [];
  for (let pageIndex = 0; pageIndex < numberOfPages; pageIndex++) {
    pages.push(renderPage(pageIndex, allCharacteristics, allAnnotations, pdf, partResult));
  }
  await Promise.all(pages);
  const buffer = await pdf.saveMemoryBuffer(1);
  await pdf.destroy();
  return { drawing, ballooned: Buffer.from(buffer) };
};

module.exports = async (req, res) => {
  const { privateUrl } = req.query;
  if (!privateUrl) {
    return res.sendStatus(404);
  }
  const options = {
    currentUser: req.currentUser,
  };

  const saveFilesTo = config.uploadRemote ? 's3' : 'local-fs';

  const fileSystem = saveFilesTo === 'local-fs' ? fs : s3;
  const fileBuffer = await getFileBuffer(saveFilesTo, config, privateUrl, fileSystem, req.currentUser);
  if (!fileBuffer) {
    return res.sendStatus(404);
  }

  const pdf = await PDFNet.PDFDoc.createFromBuffer(fileBuffer);
  const numberOfPages = await pdf.getPageCount();
  const partResult = await new PartRepository().findById(req.query.partId, options);

  const characteristicRepository = new CharacteristicRepository();
  const allCharacteristicsResult = await characteristicRepository.findAndCountAll(
    {
      filter: {
        part: req.query.partId,
        drawing: req.query.drawingId,
        status: 'Active',
      },
      orderBy: 'markerIndex_ASC',
      requestedAttributes: [
        'id',
        'status',
        'captureMethod',
        'drawingSheetIndex',
        'notationType',
        'notationSubtype',
        'notationClass',
        'quantity',
        'drawingRotation',
        'drawingScale',
        'boxLocationY',
        'boxLocationX',
        'boxWidth',
        'boxHeight',
        'boxRotation',
        'markerGroup',
        'markerIndex',
        'markerSubIndex',
        'markerLabel',
        'markerStyle',
        'markerLocationX',
        'markerLocationY',
        'markerFontSize',
        'marker',
        'drawing',
        'drawingSheet',
        'deletedAt',
        'verified',
      ],
    },
    options
  );
  const allCharacteristics = [...allCharacteristicsResult.rows];
  allCharacteristics.forEach((char, index) => {
    allCharacteristics[index].boxLocationY = parseFloat(char.boxLocationY);
    allCharacteristics[index].boxLocationX = parseFloat(char.boxLocationX);
    allCharacteristics[index].boxWidth = parseFloat(char.boxWidth);
    allCharacteristics[index].boxHeight = parseFloat(char.boxHeight);
    allCharacteristics[index].boxRotation = parseFloat(char.boxRotation);
    allCharacteristics[index].markerLocationX = parseFloat(char.markerLocationX);
    allCharacteristics[index].markerLocationY = parseFloat(char.markerLocationY);
    allCharacteristics[index].drawingRotation = parseFloat(char.drawingRotation);
    allCharacteristics[index].drawingScale = parseFloat(char.drawingScale);
  });
  const annotationRepository = new AnnotationRepository();
  const allAnnotationsResult = await annotationRepository.findAndCountAll(
    {
      filter: {
        part: req.query.partId,
        drawing: req.query.drawingId,
        status: 'Active',
      },
      requestedAttributes: [
        'id',
        'annotation',
        'fontColor',
        'fontSize',
        'borderColor',
        'backgroundColor',
        'drawingSheetIndex',
        'drawingRotation',
        'drawingScale',
        'boxLocationY',
        'boxLocationX',
        'boxWidth',
        'boxHeight',
        'boxRotation',
        'drawing',
        'drawingSheet',
        'deletedAt',
      ],
    },
    options
  );
  const allAnnotations = [...allAnnotationsResult.rows];
  allAnnotations.forEach((annotation, index) => {
    allAnnotations[index].boxLocationY = parseFloat(annotation.boxLocationY);
    allAnnotations[index].boxLocationX = parseFloat(annotation.boxLocationX);
    allAnnotations[index].boxWidth = parseFloat(annotation.boxWidth);
    allAnnotations[index].boxHeight = parseFloat(annotation.boxHeight);
    allAnnotations[index].boxRotation = parseFloat(annotation.boxRotation);
    allAnnotations[index].drawingRotation = parseFloat(annotation.drawingRotation);
    allAnnotations[index].drawingScale = parseFloat(annotation.drawingScale);
  });
  const markerRepository = new MarkerRepository();
  const markerIds = [...new Set(allCharacteristics.map((char) => char.markerStyle))];
  for (let markerIndex = 0; markerIndex < markerIds.length; markerIndex++) {
    const markerId = markerIds[markerIndex];
    // eslint-disable-next-line no-await-in-loop
    const marker = await markerRepository.findById(markerId, options);
    for (let characteristicIndex = 0; characteristicIndex < allCharacteristics.length; characteristicIndex++) {
      if (allCharacteristics[characteristicIndex].markerStyle === markerId) {
        allCharacteristics[characteristicIndex].marker = marker;
      }
    }
  }
  const pages = [];
  for (let pageIndex = 0; pageIndex < numberOfPages; pageIndex++) {
    pages.push(renderPage(pageIndex, allCharacteristics, allAnnotations, pdf, partResult));
  }
  await Promise.all(pages);
  const buffer = await pdf.saveMemoryBuffer(1);
  await pdf.destroy();
  res.attachment(privateUrl.toLowerCase().replace('.pdf', 'ballooned.pdf'));
  res.type('pdf');
  res.send(Buffer.from(buffer));
  return true;
};

module.exports.getFileBuffer = getFileBuffer;
module.exports.renderPage = renderPage;
module.exports.getFontSizeModifier = getFontSizeModifier;
module.exports.renderCharacteristic = renderCharacteristic;
module.exports.drawShape = drawShape;
module.exports.drawDiamond = drawDiamond;
module.exports.drawHexagon = drawHexagon;
module.exports.drawTriangle = drawTriangle;
module.exports.drawCircle = drawCircle;
module.exports.drawPoints = drawPoints;
module.exports.setShapeElementOptions = setShapeElementOptions;
module.exports.drawText = drawText;
module.exports.getBalloonLocation = getBalloonLocation;
module.exports.transformRectangle = transformRectangle;
module.exports.getNewRotation = getNewRotation;
module.exports.regularPolygonPoints = regularPolygonPoints;
module.exports.rotatePoints = rotatePoints;
module.exports.createBalloonedPDF = createBalloonedPDF;
