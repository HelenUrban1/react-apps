const { PDFNet } = require('@pdftron/pdfnet-node');
const { s3 } = require('aws-wrapper');
const { drawShape, drawDiamond, drawHexagon, drawTriangle, drawCircle, getBalloonLocation, getFileBuffer, drawPoints, setShapeElementOptions, drawText, transformRectangle, getNewRotation, regularPolygonPoints, rotatePoints, getFontSizeModifier } = require('./downloadBallooned');
const config = require('../../../config')();

const mockBuilder = (builder) => {
  return {
    pathBegin: jest.fn().mockReturnValue(builder),
    moveTo: jest.fn().mockReturnValue(builder),
    lineTo: jest.fn().mockReturnValue(builder),
    pathEnd: jest.fn().mockReturnValue({ result: true }),
    createEllipse: jest.fn().mockReturnValue({ result: true }),
  };
};

describe('Download Ballooned Pdf', () => {
  afterAll(() => {
    PDFNet.shutdown();
  });

  describe('Rotate Points', () => {
    const pathPoints = [
      { x: 0, y: 0 },
      { x: 5, y: 6 },
      { x: 10, y: 4 },
    ];
    const midpointX = 10;
    const midpointY = 20;
    describe('rotatePoints 0 degrees', () => {
      const rotation = 0;
      const output = rotatePoints(pathPoints, rotation, midpointX, midpointY);
      it('should output the original points', async () => {
        expect(output[0].x).toBe(pathPoints[0].x);
        expect(output[0].y).toBe(pathPoints[0].y);
        expect(output[1].x).toBe(pathPoints[1].x);
        expect(output[1].y).toBe(pathPoints[1].y);
        expect(output[2].x).toBe(pathPoints[2].x);
        expect(output[2].y).toBe(pathPoints[2].y);
      });
    });
    describe('rotatePoints 90 degrees', () => {
      const rotation = 90;
      const output = rotatePoints(pathPoints, rotation, midpointX, midpointY);
      it('should output points rotated 90 degrees', async () => {
        expect(output[0].x).toBe(30);
        expect(output[0].y).toBe(10);
        expect(output[1].x).toBe(24);
        expect(output[1].y).toBe(15);
        expect(output[2].x).toBe(26);
        expect(output[2].y).toBe(20);
      });
    });
    describe('rotatePoints 180 degrees', () => {
      const rotation = 180;
      const output = rotatePoints(pathPoints, rotation, midpointX, midpointY);
      it('should output points rotated 180 degrees', async () => {
        expect(output[0].x).toBe(20);
        expect(output[0].y).toBe(40);
        expect(output[1].x).toBe(15);
        expect(output[1].y).toBe(34);
        expect(output[2].x).toBe(10);
        expect(output[2].y).toBe(36);
      });
    });
    describe('rotatePoints 270 degrees', () => {
      const rotation = 270;
      const output = rotatePoints(pathPoints, rotation, midpointX, midpointY);
      it('should output points rotated 270 degrees', async () => {
        expect(output[0].x).toBe(-10);
        expect(output[0].y).toBe(30);
        expect(output[1].x).toBe(-4);
        expect(output[1].y).toBe(25);
        expect(output[2].x).toBe(-6);
        expect(output[2].y).toBe(20);
      });
    });
    describe('rotatePoints 360 degrees', () => {
      const rotation = 360;
      const output = rotatePoints(pathPoints, rotation, midpointX, midpointY);
      it('should output the original points', async () => {
        expect(output[0].x === pathPoints[0].x).toBe(true); // hacky checks because -0 is a number in javascript
        expect(output[0].y === pathPoints[0].y).toBe(true);
        expect(output[1].x).toBe(pathPoints[1].x);
        expect(output[1].y).toBe(pathPoints[1].y);
        expect(output[2].x).toBe(pathPoints[2].x);
        expect(output[2].y).toBe(pathPoints[2].y);
      });
    });

    describe('rotatePoints 90 degrees four times', () => {
      const rotation = 90;
      const output1 = rotatePoints(pathPoints, rotation, midpointX, midpointY);
      const output2 = rotatePoints(output1, rotation, midpointX, midpointY);
      const output3 = rotatePoints(output2, rotation, midpointX, midpointY);
      const output4 = rotatePoints(output3, rotation, midpointX, midpointY);
      it('should output the original points', async () => {
        expect(output4[0].x === pathPoints[0].x).toBe(true); // hacky checks because -0 is a number in javascript
        expect(output4[0].y === pathPoints[0].y).toBe(true);
        expect(output4[1].x).toBe(pathPoints[1].x);
        expect(output4[1].y).toBe(pathPoints[1].y);
        expect(output4[2].x).toBe(pathPoints[2].x);
        expect(output4[2].y).toBe(pathPoints[2].y);
      });
    });
  });

  describe('regularPolygonPoints', () => {
    const radius = 4;
    const left = 0;
    const top = 0;
    it('3 sides should have 3 points', async () => {
      const sideCount = 3;
      const output = regularPolygonPoints(sideCount, radius, left, top);
      expect(output.length).toBe(3);
      expect(output[0].x).toBe(8);
      expect(output[0].y).toBe(4.8);
      expect(output[1].x).toBe(2);
      expect(output[1].y).toBe(8.3);
      expect(output[2].x).toBe(2);
      expect(output[2].y).toBe(1.3);
    });
    it('4 sides should have 4 points', async () => {
      const sideCount = 4;
      const output = regularPolygonPoints(sideCount, radius, left, top);
      expect(output.length).toBe(4);
      expect(output[0].x).toBe(8);
      expect(output[0].y).toBe(4.8);
      expect(output[1].x).toBe(4);
      expect(output[1].y).toBe(8.8);
      expect(output[2].x).toBe(0);
      expect(output[2].y).toBe(4.8);
      expect(output[3].x).toBe(4);
      expect(output[3].y).toBe(0.8);
    });
    it('6 sides should have 6 points', async () => {
      const sideCount = 6;
      const output = regularPolygonPoints(sideCount, radius, left, top);
      expect(output.length).toBe(6);
      expect(output[0].x).toBe(8);
      expect(output[0].y).toBe(4.8);
      expect(output[1].x).toBe(6);
      expect(output[1].y).toBe(8.3);
      expect(output[2].x).toBe(2);
      expect(output[2].y).toBe(8.3);
      expect(output[3].x).toBe(0);
      expect(output[3].y).toBe(4.8);
      expect(output[4].x).toBe(2);
      expect(output[4].y).toBe(1.3);
      expect(output[5].x).toBe(6);
      expect(output[5].y).toBe(1.3);
    });
  });

  describe('drawPoints', () => {
    describe('drawing a square', () => {
      let builder = {};
      let shapeElement = {};
      beforeEach(async () => {
        builder = mockBuilder(builder);
        const pathPoints = [
          { x: 0, y: 0 },
          { x: 10, y: 0 },
          { x: 10, y: 10 },
          { x: 0, y: 10 },
        ];
        shapeElement = await drawPoints(builder, pathPoints);
      });
      it('begins a path', () => {
        expect(builder.pathBegin.mock.calls.length).toBe(1);
      });
      it('moves to a starting point', () => {
        expect(builder.moveTo.mock.calls.length).toBe(1);
      });
      it('draws a line for each point', () => {
        expect(builder.lineTo.mock.calls.length).toBe(4);
      });
      it('ends a path', () => {
        expect(builder.pathEnd.mock.calls.length).toBe(1);
      });
      it('returns result of pathEnd', () => {
        expect(shapeElement.result).toBe(true);
      });
    });
  });

  describe('getNewRotation', () => {
    describe('pdfPageRotation is 0 and currentSheetRotation is 0', () => {
      it('should return new rotation of 0', () => {
        const pdfPageRotation = 0;
        const currentSheetRotation = 0;
        const newRotation = getNewRotation(pdfPageRotation, currentSheetRotation);
        expect(newRotation).toBe(0);
      });
    });
    describe('pdfPageRotation is 1 and currentSheetRotation is 0', () => {
      it('should return new rotation of 1', () => {
        const pdfPageRotation = 1;
        const currentSheetRotation = 'e_0';
        const newRotation = getNewRotation(pdfPageRotation, currentSheetRotation);
        expect(newRotation).toBe(1);
      });
    });
    describe('pdfPageRotation is 0 and currentSheetRotation is 1', () => {
      it('should return new rotation of 1', () => {
        const pdfPageRotation = 0;
        const currentSheetRotation = 'e_90';
        const newRotation = getNewRotation(pdfPageRotation, currentSheetRotation);
        expect(newRotation).toBe(1);
      });
    });
    describe('pdfPageRotation is 2 and currentSheetRotation is 2', () => {
      it('should return new rotation of 2', () => {
        const pdfPageRotation = 2;
        const currentSheetRotation = 'e_180';
        const newRotation = getNewRotation(pdfPageRotation, currentSheetRotation);
        expect(newRotation).toBe(0);
      });
    });
    describe('pdfPageRotation is 5 and currentSheetRotation is 2', () => {
      it('should return new rotation of 3', () => {
        const pdfPageRotation = 5;
        const currentSheetRotation = 'e_180';
        const newRotation = getNewRotation(pdfPageRotation, currentSheetRotation);
        expect(newRotation).toBe(3);
      });
    });
  });

  describe('transformRectangle', () => {
    describe('theta is 0', () => {
      it('returns starting x and y', async () => {
        const x = 10;
        const y = 12;
        const imageWidth = 100;
        const imageHeight = 80;
        const theta = 0;
        const points = transformRectangle(x, y, imageWidth, imageHeight, theta);
        expect(points.x).toBe(x);
        expect(points.y).toBe(y);
      });
    });
    describe('theta is 90', () => {
      it('returns new x and y', async () => {
        const x = 10;
        const y = 12;
        const imageWidth = 100;
        const imageHeight = 80;
        const theta = 90;
        const points = transformRectangle(x, y, imageWidth, imageHeight, theta);
        expect(points.x).toBe(88);
        expect(points.y).toBe(10);
      });
    });
    describe('theta is 180', () => {
      it('returns new x and y', async () => {
        const x = 10;
        const y = 12;
        const imageWidth = 100;
        const imageHeight = 80;
        const theta = 180;
        const points = transformRectangle(x, y, imageWidth, imageHeight, theta);
        expect(points.x).toBe(90);
        expect(points.y).toBe(68);
      });
    });
    describe('theta is 270', () => {
      it('returns new x and y', async () => {
        const x = 10;
        const y = 12;
        const imageWidth = 100;
        const imageHeight = 80;
        const theta = 270;
        const points = transformRectangle(x, y, imageWidth, imageHeight, theta);
        expect(points.x).toBe(12);
        expect(points.y).toBe(70);
      });
    });
  });

  describe('drawText', () => {
    describe('drawing some text', () => {
      const builder = {};
      let textElement = {};
      beforeEach(async () => {
        await PDFNet.initialize(config.pdfTronSdkKey);
        builder.createTextRun = jest.fn().mockReturnValue({
          result: true,
          getGState: jest.fn().mockReturnValue({
            setFillColorSpace: jest.fn(),
            setFillColorWithColorPt: jest.fn(),
            setLineWidth: jest.fn(),
          }),
          getBBox: jest.fn().mockReturnValue({
            height: jest.fn().mockReturnValue(10),
            width: jest.fn().mockReturnValue(18),
          }),
          setTextMatrix: jest.fn(),
        });
        const textDefinition = {
          balloonText: '1',
          font: 'helvetica',
          fontSize: 12,
          offset: 8,
          fontColor: '#000000',
        };
        const shapeDefinition = {
          midpointX: 10,
          midpointY: 12,
          newRotation: 0,
        };
        textElement = await drawText(textDefinition, shapeDefinition, builder);
      });
      it('creates a text run', () => {
        expect(builder.createTextRun.mock.calls.length).toBe(1);
      });
      it('gets GState', () => {
        expect(builder.createTextRun.mock.results[0].value.getGState.mock.calls.length).toBe(1);
      });
      it('sets text color', () => {
        expect(builder.createTextRun.mock.results[0].value.getGState.mock.results[0].value.setFillColorWithColorPt.mock.calls.length).toBe(1);
      });
      it('sets the text matrix', () => {
        expect(builder.createTextRun.mock.results[0].value.setTextMatrix.mock.calls.length).toBe(1);
      });
      it('returns the textElement', () => {
        expect(textElement.result).toBe(true);
      });
    });
  });

  describe('setShapeElementOptions', () => {
    const shapeElement = {};
    const fillColor = '#333333';
    const borderColor = '#666666';
    const strokeWidth = 1;
    const strokeStyle = 'Solid';
    beforeEach(async () => {
      await PDFNet.initialize(config.pdfTronSdkKey);
      shapeElement.setPathFill = jest.fn();
      shapeElement.setPathStroke = jest.fn();
      shapeElement.getGState = jest.fn().mockReturnValue({
        setFillColorSpace: jest.fn(),
        setFillColorWithColorPt: jest.fn(),
        setStrokeColorSpace: jest.fn(),
        setStrokeColorWithColorPt: jest.fn(),
        setLineWidth: jest.fn(),
      });
      await setShapeElementOptions(shapeElement, fillColor, borderColor, strokeWidth, strokeStyle);
    });
    it('should set Path Fill', async () => {
      expect(shapeElement.setPathFill.mock.calls.length).toBe(1);
      expect(shapeElement.setPathFill.mock.calls[0][0]).toBe(true);
    });
    it('should set Path Stroke', async () => {
      expect(shapeElement.setPathStroke.mock.calls.length).toBe(1);
      expect(shapeElement.setPathFill.mock.calls[0][0]).toBe(true);
    });
    it('should set Fill Color Space', async () => {
      expect(shapeElement.getGState.mock.results[0].value.setFillColorSpace.mock.calls.length).toBe(1);
      expect(shapeElement.getGState.mock.results[0].value.setFillColorSpace.mock.calls[0][0].name).toBe('ColorSpace');
    });
    it('should set Fill Color', async () => {
      expect(shapeElement.getGState.mock.results[0].value.setFillColorWithColorPt.mock.calls.length).toBe(1);
      expect(shapeElement.getGState.mock.results[0].value.setFillColorWithColorPt.mock.calls[0][0].name).toBe('ColorPt');
    });
    it('should set Stroke Color Space', async () => {
      expect(shapeElement.getGState.mock.results[0].value.setStrokeColorSpace.mock.calls.length).toBe(1);
      expect(shapeElement.getGState.mock.results[0].value.setStrokeColorSpace.mock.calls[0][0].name).toBe('ColorSpace');
    });
    it('should set Stroke Color', async () => {
      expect(shapeElement.getGState.mock.results[0].value.setStrokeColorWithColorPt.mock.calls.length).toBe(1);
      expect(shapeElement.getGState.mock.results[0].value.setStrokeColorWithColorPt.mock.calls[0][0].name).toBe('ColorPt');
    });
    it('should set Stroke Width', async () => {
      expect(shapeElement.getGState.mock.results[0].value.setLineWidth.mock.calls.length).toBe(1);
      expect(shapeElement.getGState.mock.results[0].value.setLineWidth.mock.calls[0][0]).toBe(1);
    });
  });

  // TODO: Removing this test, because its not specific to downloadingBallooned drawings. It just tests file storage which is now handled inside of aws-wrapper
  // describe('getFileBuffer', () => {
  //   const privateUrl = 'test.pdf';
  //   const currentUser = {
  //     accountId: '44444444-4444-4444-4444-444444444444',
  //     siteId: '55555555-5555-5555-5555-555555555555',
  //   };

  //   describe('Get drawing from S3', () => {
  //     it('read a file from s3', async () => {
  //       const saveFilesTo = 's3';
  //       const file = await getFileBuffer(saveFilesTo, config, privateUrl, s3, currentUser);
  //       expect(file.length).toBe(111699);
  //     });
  //   });
  //   describe('Get drawing from local file system', () => {
  //     const fileSystem = {
  //       readFileSync: jest.fn().mockReturnValue('download'),
  //     };
  //     it('should read a local file', async () => {
  //       const saveFilesTo = 'local-fs';
  //       const file = await getFileBuffer(saveFilesTo, config, privateUrl, fileSystem, currentUser);
  //       expect(file).toBe('download');
  //     });
  //   });
  // });

  describe('drawTriangle', () => {
    it('should draw a triangle', async () => {
      let builder = {};
      builder = mockBuilder(builder);
      const shapeDefinition = {
        midPointX: 10,
        midPointY: 12,
        radius: 2,
        newRotation: 0,
        shape: 'triangle',
      };
      const shapeElement = await drawTriangle(shapeDefinition, builder);
      expect(builder.lineTo.mock.calls.length).toBe(3);
      expect(shapeElement.result).toBe(true);
    });
  });

  describe('drawHexagon', () => {
    it('should draw a hexagon', async () => {
      let builder = {};
      builder = mockBuilder(builder);
      const shapeDefinition = {
        midPointX: 10,
        midPointY: 12,
        radius: 2,
        newRotation: 0,
        shape: 'hexagon',
      };
      const shapeElement = await drawHexagon(shapeDefinition, builder);
      expect(builder.lineTo.mock.calls.length).toBe(6);
      expect(shapeElement.result).toBe(true);
    });
  });

  describe('drawDiamond', () => {
    it('should draw a diamond', async () => {
      let builder = {};
      builder = mockBuilder(builder);
      const shapeDefinition = {
        midPointX: 10,
        midPointY: 12,
        radius: 2,
        newRotation: 0,
        shape: 'diamond',
      };
      const shapeElement = await drawDiamond(shapeDefinition, builder);
      expect(builder.lineTo.mock.calls.length).toBe(4);
      expect(shapeElement.result).toBe(true);
    });
  });

  describe('drawCircle', () => {
    it('should draw a circle', async () => {
      let builder = {};
      builder = mockBuilder(builder);
      const shapeDefinition = {
        midPointX: 10,
        midPointY: 12,
        radius: 2,
        newRotation: 0,
        shape: 'circle',
      };
      const shapeElement = await drawCircle(shapeDefinition, builder);
      expect(builder.createEllipse.mock.calls.length).toBe(1);
      expect(shapeElement.result).toBe(true);
    });
  });

  describe('drawShape', () => {
    let builder = {};
    const shapeDefinition = {
      midPointX: 10,
      midPointY: 12,
      radius: 2,
      newRotation: 0,
    };
    beforeEach(async () => {
      builder = mockBuilder(builder);
    });
    it('should draw a diamond', async () => {
      shapeDefinition.shape = 'diamond';
      const { shapeElement, offset } = await drawShape(shapeDefinition, builder);
      expect(builder.lineTo.mock.calls.length).toBe(4);
      expect(shapeElement.result).toBe(true);
      expect(offset).toBe(-0.2);
    });
    it('should draw a hexagon', async () => {
      shapeDefinition.shape = 'hexagon';
      const { shapeElement, offset } = await drawShape(shapeDefinition, builder);
      expect(builder.lineTo.mock.calls.length).toBe(6);
      expect(shapeElement.result).toBe(true);
      expect(offset).toBe(-0.6);
    });
    it('should draw a triangle', async () => {
      shapeDefinition.shape = 'triangle';
      const { shapeElement, offset } = await drawShape(shapeDefinition, builder);
      expect(builder.lineTo.mock.calls.length).toBe(3);
      expect(shapeElement.result).toBe(true);
      expect(offset).toBe(0.6);
    });
    it('should draw a circle', async () => {
      shapeDefinition.shape = 'circle';
      const { shapeElement, offset } = await drawShape(shapeDefinition, builder);
      expect(builder.createEllipse.mock.calls.length).toBe(1);
      expect(shapeElement.result).toBe(true);
      expect(offset).toBe(-0.2);
    });
  });

  describe('getBalloonLocation', () => {
    it('should return angle, x and y', async () => {
      const characteristic = {
        boxLocationX: 10,
        boxLocationY: 12,
        boxWidth: 30,
        boxHeight: 18,
        markerLocationY: 90,
        drawingRotation: 0,
        markerRotation: 0,
      };
      const page = {
        getPageWidth: jest.fn().mockReturnValue(100),
        getPageHeight: jest.fn().mockReturnValue(80),
        getRotation: jest.fn().mockReturnValue(0),
      };
      const { angle, x, y } = await getBalloonLocation(characteristic, page);
      expect(angle).toBe(1.5707963267948966);
      expect(x).toBe(25);
      expect(y).toBe(59);
    });
  });

  describe('getFontSizeModifier', () => {
    it('should return return 1 if label text is undefined', () => {
      let label;
      const fontSizeModifier = getFontSizeModifier(label);
      expect(fontSizeModifier).toBe(1);
    });
    it('should return return 1 if label text is empty', () => {
      const label = '';
      const fontSizeModifier = getFontSizeModifier(label);
      expect(fontSizeModifier).toBe(1);
    });
    it('should return return 1 if label text is less than 3 characters', () => {
      const label = '12';
      const fontSizeModifier = getFontSizeModifier(label);
      expect(fontSizeModifier).toBe(1);
    });
    it('should return return .8 if label text is 3 characters', () => {
      const label = '123';
      const fontSizeModifier = getFontSizeModifier(label);
      expect(fontSizeModifier).toBe(0.8);
    });
    it('should return return .6 if label text is 4 characters', () => {
      const label = '1234';
      const fontSizeModifier = getFontSizeModifier(label);
      expect(fontSizeModifier).toBe(0.6);
    });
    it('should return return .4 if label text is 5 characters', () => {
      const label = '12345';
      const fontSizeModifier = getFontSizeModifier(label);
      expect(fontSizeModifier).toBeCloseTo(0.4);
    });
    it('should return return .2 if label text length is 6 or more characters ', () => {
      const label1 = '123456';
      const fontSizeModifier1 = getFontSizeModifier(label1);
      expect(fontSizeModifier1).toBeCloseTo(0.2);
      const label2 = '123456789';
      const fontSizeModifier2 = getFontSizeModifier(label2);
      expect(fontSizeModifier2).toBeCloseTo(0.2);
    });
  });
});
