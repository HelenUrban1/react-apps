/**
 * Maps all the Schema of the application.
 * More about the schema: https://www.apollographql.com/docs/graphql-tools/generate-schema/
 */

const { makeExecutableSchema } = require('graphql-tools');
const resolvers = require('./resolvers');

const sharedTypes = require('./shared/types');

const settingTypes = require('./setting/types');
const settingQueries = require('./setting/queries');
const settingMutations = require('./setting/mutations');

const listItemTypes = require('./listItem/types');
const listItemQueries = require('./listItem/queries');
const listItemMutations = require('./listItem/mutations');

const authTypes = require('./auth/types');
const authQueries = require('./auth/queries');
const authMutations = require('./auth/mutations');

const iamTypes = require('./iam/types');
const iamQueries = require('./iam/queries');
const iamMutations = require('./iam/mutations');

const auditLogTypes = require('./auditLog/types');
const auditLogQueries = require('./auditLog/queries');
const auditLogMutations = require('./auditLog/mutations');

const accountTypes = require('./account/types');
const accountQueries = require('./account/queries');
const accountMutations = require('./account/mutations');

const accountMemberTypes = require('./accountMember/types');
const accountMemberQueries = require('./accountMember/queries');
const accountMemberMutations = require('./accountMember/mutations');

const annotationTypes = require('./annotation/types');
const annotationQueries = require('./annotation/queries');
const annotationMutations = require('./annotation/mutations');

const optionsTypes = require('./options/types');
const optionsQueries = require('./options/queries');
const optionsMutations = require('./options/mutations');

const partTypes = require('./part/types');
const partQueries = require('./part/queries');
const partMutations = require('./part/mutations');

const drawingTypes = require('./drawing/types');
const drawingQueries = require('./drawing/queries');
const drawingMutations = require('./drawing/mutations');

const drawingSheetTypes = require('./drawingSheet/types');
const drawingSheetQueries = require('./drawingSheet/queries');
const drawingSheetMutations = require('./drawingSheet/mutations');

const characteristicTypes = require('./characteristic/types');
const characteristicQueries = require('./characteristic/queries');
const characteristicMutations = require('./characteristic/mutations');

const markerTypes = require('./marker/types');
const markerQueries = require('./marker/queries');
const markerMutations = require('./marker/mutations');

const measurementTypes = require('./measurement/types');
const measurementQueries = require('./measurement/queries');
const measurementMutations = require('./measurement/mutations');

const reportTemplateTypes = require('./reportTemplate/types');
const reportTemplateQueries = require('./reportTemplate/queries');
const reportTemplateMutations = require('./reportTemplate/mutations');

const reportTypes = require('./report/types');
const reportQueries = require('./report/queries');
const reportMutations = require('./report/mutations');

const sampleTypes = require('./sample/types');
const sampleQueries = require('./sample/queries');
const sampleMutations = require('./sample/mutations');

const siteTypes = require('./site/types');
const siteQueries = require('./site/queries');
const siteMutations = require('./site/mutations');

const sphinxTypes = require('./sphinx/types');
const sphinxQueries = require('./sphinx/queries');
const sphinxMutations = require('./sphinx/mutations');

const stationTypes = require('./station/types');
const stationQueries = require('./station/queries');
const stationMutations = require('./station/mutations');

const operatorTypes = require('./operator/types');
const operatorQueries = require('./operator/queries');
const operatorMutations = require('./operator/mutations');

const organizationTypes = require('./organization/types');
const organizationQueries = require('./organization/queries');
const organizationMutations = require('./organization/mutations');

const subscriptionTypes = require('./subscription/types');
const subscriptionQueries = require('./subscription/queries');
const subscriptionMutations = require('./subscription/mutations');

const jobTypes = require('./job/types');
const jobQueries = require('./job/queries');
const jobMutations = require('./job/mutations');

const reviewTaskTypes = require('./reviewTask/types');
const reviewTaskQueries = require('./reviewTask/queries');
const reviewTaskMutations = require('./reviewTask/mutations');

const notificationTypes = require('./notification/types');
const notificationQueries = require('./notification/queries');
const notificationMutations = require('./notification/mutations');

const notificationUserStatusTypes = require('./notificationUserStatus/types');
const notificationUserStatusQueries = require('./notificationUserStatus/queries');
const notificationUserStatusMutations = require('./notificationUserStatus/mutations');

const types = [
  ...sharedTypes, //
  ...iamTypes,
  ...authTypes,
  ...auditLogTypes,
  ...settingTypes,
  ...listItemTypes,
  ...accountTypes,
  ...accountMemberTypes,
  ...optionsTypes,
  ...partTypes,
  ...drawingTypes,
  ...drawingSheetTypes,
  ...characteristicTypes,
  ...annotationTypes,
  ...markerTypes,
  ...measurementTypes,
  ...reportTemplateTypes,
  ...reportTypes,
  ...jobTypes,
  ...sampleTypes,
  ...siteTypes,
  ...sphinxTypes,
  ...stationTypes,
  ...organizationTypes,
  ...operatorTypes,
  ...subscriptionTypes,
  ...reviewTaskTypes,
  ...notificationTypes,
  ...notificationUserStatusTypes,
].map((type) => type.schema);

const mutations = [
  ...iamMutations, //
  ...authMutations,
  ...auditLogMutations,
  ...settingMutations,
  ...listItemMutations,
  ...accountMutations,
  ...accountMemberMutations,
  ...optionsMutations,
  ...partMutations,
  ...drawingMutations,
  ...drawingSheetMutations,
  ...characteristicMutations,
  ...annotationMutations,
  ...jobMutations,
  ...markerMutations,
  ...measurementMutations,
  ...reportTemplateMutations,
  ...reportMutations,
  ...sampleMutations,
  ...siteMutations,
  ...sphinxMutations,
  ...stationMutations,
  ...organizationMutations,
  ...operatorMutations,
  ...subscriptionMutations,
  ...reviewTaskMutations,
  ...notificationMutations,
  ...notificationUserStatusMutations,
].map((mutation) => mutation.schema);

const queries = [
  ...iamQueries, //
  ...authQueries,
  ...auditLogQueries,
  ...settingQueries,
  ...listItemQueries,
  ...accountQueries,
  ...accountMemberQueries,
  ...optionsQueries,
  ...partQueries,
  ...drawingQueries,
  ...drawingSheetQueries,
  ...characteristicQueries,
  ...annotationQueries,
  ...markerQueries,
  ...measurementQueries,
  ...reportTemplateQueries,
  ...reportQueries,
  ...sampleQueries,
  ...siteQueries,
  ...jobQueries,
  ...sphinxQueries,
  ...stationQueries,
  ...organizationQueries,
  ...operatorQueries,
  ...subscriptionQueries,
  ...reviewTaskQueries,
  ...notificationQueries,
  ...notificationUserStatusQueries,
].map((query) => query.schema);

const query = `
  type Query {
    ${queries.join('\n')}
  }
`;

const mutation = `
  type Mutation {
    ${mutations.join('\n')}
  }
`;

const schemaDefinition = `
  type Schema {
    query: Query
    mutation: Mutation
  }
`;

module.exports = makeExecutableSchema({
  typeDefs: [schemaDefinition, query, mutation, ...types],
  resolvers,
});
