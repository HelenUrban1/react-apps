const schema = `
  type SubscriptionBillingInvoiceDownload {
    id: String!
    base64Payload: String!
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
