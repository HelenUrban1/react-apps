const schema = `
  input SubscriptionUpdateInput {
    accountId: String!
    billingId: String!
    companyId: String!
    enforcePlan: Boolean
    providerProductId: Int
    providerProductHandle: String!
    providerFamilyHandle: String!
    previousStatus: String
    status: String
    activatedAt: String
    paidUserSeatsUsed: Int
    drawingsUsed: Int
    partsUsed: Int
    measurementsUsed: Int
    components: String
    pendingComponents: String
    currentPeriodStartedAt: String
    currentPeriodEndedAt: String
    cancelAtEndOfPeriod: Boolean
    canceledAt: String
    trialEndedAt: String
    trialStartedAt: String
    expiresAt: String
    updatedAt: String
    automaticallyResumeAt: String
    balanceInCents: Int
    cancellationMessage: String
    cancellationMethod: String
    couponCode: String
    couponCodes: [String]
    couponUseCount: Int
    couponUsesAllowed: Int
    createdAt: String
    pendingBillingAmountInCents: Int
    currentBillingAmountInCents: Int
    currentPeriodEndsAt: String
    delayedCancelAt: String
    netTerms: Int
    nextAssessmentAt: String
    nextProductHandle: String
    nextProductId: String
    offerId: String
    payerId: String
    paymentCollectionMethod: String
    paymentSystemId: String
    paymentType: String
    plan: String
    planNotes: String
    productPriceInCents: Int
    productPricePointId: String
    productVersionNumber: String
    reasonCode: String
    receivesInvoiceEmails: Boolean
    referralCode: String
    signupPaymentId: String
    signupRevenue: String
    snapDay: String
    storedCredentialTransactionId: String
    totalRevenueInCents: Int
    cardId: Int
    maskedCardNumber: String
    cardType: String
    expirationMonth: Int
    expirationYear: Int
    customerId: String
    termsAcceptedOn: String
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
