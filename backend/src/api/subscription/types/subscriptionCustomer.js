/**
 * @see {@link https://reference.chargify.com/v1/subscriptions/create-subscription}
 */
const schema = `
  type SubscriptionCustomer {
    first_name: String
    last_name: String
    email: String
    cc_emails: String
    organization: String
    id: Int
    created_at: String
    updated_at: String
    address: String
    address_2: String
    city: String
    state: String
    zip: String
    country: String
    phone: String
    verified: Boolean
    portal_customer_created_at: String
    portal_invite_last_sent_at: String
    tax_exempt: Boolean
    vat_number: String
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
