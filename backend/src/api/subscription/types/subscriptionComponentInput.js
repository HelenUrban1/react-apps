const schema = `
input SubscriptionComponentInput {
  component_id: Int!
  handle: String
  product_family_key: String!
  quantity: Int!
  memo: String
  price_point: String
  name: String
  type: String
  unit_price: String
  proration_downgrade_scheme: String
  payment_collection_method: String
  proration_upgrade_scheme: String
  accrue_charge: Boolean
  upgrade_charge: String
  downgrade_credit: String
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
