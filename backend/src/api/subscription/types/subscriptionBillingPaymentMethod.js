/**
 * @see {@link https://reference.chargify.com/v1/payment-profiles/create-a-payment-profile}
 */
const schema = `
  type SubscriptionBillingPaymentMethod {
    id: String
    paymentType: String
    cardId: Int
    maskedCardNumber: String
    cardType: String
    expirationMonth: Int
    expirationYear: Int
    customerId: String
    paymentCollectionMethod: String
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
