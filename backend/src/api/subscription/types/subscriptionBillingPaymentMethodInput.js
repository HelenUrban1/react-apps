const schema = `
  input SubscriptionBillingPaymentMethodInput {
    accountId: String!
    customerId: String!
    billingToken: String!
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
