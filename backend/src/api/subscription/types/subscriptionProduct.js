/**
 * @see {@link https://reference.chargify.com/v1/subscriptions/create-subscription}
 */
const schema = `
  type SubscriptionProduct {
    id: Int
    name: String
    handle: String
    description: String
    accounting_code: String
    price_in_cents: Int
    interval: Int
    interval_unit: String
    expiration_interval_unit: String
    trial_interval_unit: String
    initial_charge_after_trial: Boolean
    return_params: String
    request_credit_card: Boolean
    require_credit_card: Boolean
    created_at: String
    updated_at: String
    update_return_url: String
    update_return_params: String
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
