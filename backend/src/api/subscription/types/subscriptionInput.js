// accountId: IX3 account ID
// paymentSystemId:  Chargify account ID
// billingToken:  Chargify API key
// providerProductId: Chargify Product ID
const schema = `
  input SubscriptionInput {
    billingToken: String
    due: Int
    accountId: String!
    billingId: String!
    companyId: String!
    enforcePlan: Boolean!
    providerProductId: Int!
    providerProductHandle: String!
    providerFamilyHandle: String!
    previousStatus: String
    status: String!
    activatedAt: String
    paidUserSeatsUsed: Int
    drawingsUsed: Int
    partsUsed: Int
    measurementsUsed: Int
    components: String
    pendingComponents: String
    paymentType: String
    paymentSystemId: String
    billingPeriod: String
    renewalDate: String
    pendingTier: Int
    pendingBillingPeriod: String
    pendingBillingAmountInCents: Int
    currentBillingAmountInCents: Int
    currentPeriodStartedAt: String
    currentPeriodEndsAt: String
    cancelAtEndOfPeriod: Boolean
    canceledAt: String
    trialEndedAt: String
    trialStartedAt: String
    expiresAt: String
    updatedAt: String
    automaticallyResumeAt: String
    balanceInCents: Int
    cancellationMessage: String
    cancellationMethod: String
    couponCode: String
    couponCodes: [String]
    couponUseCount: Int
    couponUsesAllowed: Int
    createdAt: String
    delayedCancelAt: String
    netTerms: Int
    nextAssessmentAt: String
    nextProductHandle: String
    nextProductId: String
    offerId: String
    payerId: String
    paymentCollectionMethod: String
    plan: String
    planNotes: String
    productPriceInCents: Int
    productPricePointId: String
    productVersionNumber: String
    reasonCode: String
    receivesInvoiceEmails: Boolean
    referralCode: String
    signupPaymentId: String
    signupRevenue: String
    snapDay: String
    storedCredentialTransactionId: String
    totalRevenueInCents: Int
    cardId: Int
    maskedCardNumber: String
    cardType: String
    expirationMonth: Int
    expirationYear: Int
    customerId: String
    termsAcceptedOn: String
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
