/**
 * @see {@link https://reference.chargify.com/v1/subscriptions/create-subscription}
 */
const schema = `
  type SubscriptionChargify {
    id:  Int
    state:  String
    balance_in_cents: Int
    total_revenue_in_cents: Int
    product_price_in_cents: Int
    product_version_number: Int
    current_period_ends_at: String
    next_assessment_at: String
    activated_at: String
    created_at: String
    updated_at: String
    cancellation_message: String
    cancellation_method: String
    cancel_at_end_of_period: Boolean
    canceled_at: String
    current_period_started_at: String
    previous_state: String
    signup_payment_id: Int
    signup_revenue: String
    coupon_code: String
    payment_collection_method: String
    receives_invoice_emails: Boolean
    net_terms: Int
    paymentType: String
    referral_code: String
    coupon_use_count: Int
    coupon_uses_allowed: Int
    stored_credential_transaction_id: Int
    next_product_handle: String
    currency: String
    on_hold_at: String
    customer: SubscriptionCustomer
    credit_card: SubscriptionCreditCard
    product: SubscriptionProduct
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
