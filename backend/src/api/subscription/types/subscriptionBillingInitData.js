const schema = `
  type SubscriptionBillingInitData {
    publicKey: String!
    securityToken: String!
    siteUrl: String!
    subscription: Subscription
    catalog: String!
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
