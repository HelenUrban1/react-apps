const schema = `
  input SubscriptionFilterInput {
    id: String
    createdAtRange: [ DateTime ]
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
