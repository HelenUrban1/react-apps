const schema = `
  type SubscriptionPage {
    rows: [Subscription!]!
    count: Int!
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
