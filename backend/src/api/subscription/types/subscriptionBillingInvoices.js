const schema = `
  type SubscriptionBillingInvoices {
    uid: String!
    issue_date: String!
    product_name: String!
    status: String!
    total_amount: String!
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
