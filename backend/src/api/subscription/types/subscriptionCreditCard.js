/**
 * @see {@link https://reference.chargify.com/v1/subscriptions/create-subscription}
 */
const schema = `
  type SubscriptionCreditCard {
    id: Int
    paymentType: String
    first_name: String
    last_name: String
    masked_card_number: String
    card_type: String
    expiration_month: Int
    expiration_year: Int
    billing_address: String
    billing_address_2: String
    billing_city: String
    billing_state: String
    billing_country: String
    billing_zip: String
    current_vault: String
    vault_token: String
    customer_id: String
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
