/**
 * @see {@link https://help.chargify.com/webhooks/webhooks-reference.html#payment_failure-payload}
 */
const schema = `
  input PaymentSite {
    id: Int 
    subdomain: String
  }
  input PaymentTransaction {
    id: Int
    subscription_id: Int
    type: String
    kind: String
    transaction_type: String
    success: Boolean
    amount_in_cents: Int
    memo: String
    created_at: String
    starting_balance_in_cents: Int
    ending_balance_in_cents: Int
    gateway_used: String
    gateway_transaction_id: Int
    gateway_order_id: Int
    payment_id: Int
    product_id: Int
    tax_id: Int
    component_id: Int
    statement_id: Int
    customer_id: Int
    card_number: String
    card_expiration: String
    card_type: String
    refunded_amount_in_cents: Int
    invoice_id: Int
    currency: String
  }
  input PaymentCustomer {
    id: Int
    first_name: String
    last_name: String
    organization: String
    email: String
    created_at: String
    updated_at: String
    reference: String
    address: String
    address_2: String
    city_name: String
    state_name: String
    city: String
    state: String
    zip: String
    locale: String
    country_name: String
    country: String
    phone: String
    portal_invite_last_sent_at: String
    portal_invite_last_accepted_at: String
    verified: Boolean
    portal_customer_created_at: String
    vat_number: String
    cc_emails: String
    tax_exempt: Boolean
    parent_id: Int
  }
  input PaymentProductFamily {
    id: Int
    name: String
    description: String
    handle: String
    accounting_code: String
    created_at: String
    updated_at: String
}
  input PaymentProduct {
    id: Int
     name: String
     handle: String
     description: String
     accounting_code: String
     request_credit_card: Boolean
     expiration_interval: String
     expiration_interval_unit: String
     created_at: String
     updated_at: String
     price_in_cents: Int
     interval: Int
     interval_unit: String
     initial_charge_in_cents: String
     trial_price_in_cents: Int
     trial_interval: Int
     trial_interval_unit: String
     archived_at: String
     require_credit_card: Boolean
     request_billing_address: Boolean
     require_billing_address: Boolean
     require_shipping_address: Boolean
     return_params: String
     taxable: Boolean
     tax_code: String
     update_return_url: String
     initial_charge_after_trial: Boolean
     version_number: Int
     update_return_params: String
     default_product_price_point_id: Int
     product_price_point_id: Int
     product_price_point_handle: String
     product_price_point_name: String
     public_signup_pages: [String]
     product_family: PaymentProductFamily
  }
  input PaymentCard {
    id: Int
    first_name: String
    last_name: String
    masked_card_number: String
    card_type: String
    expiration_month: Int
    expiration_year: Int
    customer_id: Int
    current_vault: String
    vault_token: Int
    billing_address: String
    billing_city: String
    billing_state: String
    billing_zip: String
    billing_country: String
    customer_vault_token: String
    billing_address_2: String
    payment_type: String
  }
  input PaymentSubscription {
    id: Int
    offer_id: Int
    payer_id: Int
    reference: String
    state: String
    trial_started_at: String
    trial_ended_at: String
    activated_at: String
    created_at: String
    updated_at: String
    expires_at: String
    on_hold_at: String
    scheduled_cancellation_at: String
    group: String
    net_terms: String
    automatically_resume_at: String
    balance_in_cents: Int
    credit_balance_in_cents: Int
    prepayment_balance_in_cents: Int
    receives_invoice_emails: Boolean
    current_billing_amount_in_cents: Int
    current_period_ends_at: String
    next_assessment_at: String
    canceled_at: String
    cancellation_message: String
    locale: String
    next_product_id: Int
    stored_credential_transaction_id: Int
    cancel_at_end_of_period: Boolean
    payment_collection_method: String
    snap_day: String
    cancellation_method: String
    current_period_started_at:String
    previous_state: String
    signup_payment_id: Int
    signup_revenue: String
    delayed_cancel_at: String
    coupon_code: String
    coupon_codes: [String]
    reason_code: String
    total_revenue_in_cents: Int
    product_price_in_cents: Int
    product_version_number: Int
    payment_type: String
    referral_code: String
    coupon_use_count: String
    coupon_uses_allowed: String
    product_price_point_id: Int
    next_product_price_point_id: Int
    next_product_handle: String
    currency: String
    customer: PaymentCustomer
    product: PaymentProduct
    credit_card: PaymentCard
  }
  input PaymentFailure {
    site: PaymentSite
    subscription: PaymentSubscription
    transaction: PaymentTransaction
    event_id: Int
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
