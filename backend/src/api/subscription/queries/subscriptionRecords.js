const SubscriptionService = require('../../../services/subscriptionService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions').values;

const schema = `
subscriptionRecords(customerId: String!): [SubscriptionChargify]!
`;

const resolver = {
  subscriptionRecords: async (root, args, context) => {
    new PermissionChecker(context).validateHas(permissions.accountBillingAccess);

    return new SubscriptionService(context).getActiveSubscription(args.customerId);
  },
};

exports.schema = schema;
exports.resolver = resolver;
