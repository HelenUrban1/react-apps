const SubscriptionService = require('../../../services/subscriptionService');
const graphqlSelectRequestedAttributes = require('../../shared/utils/graphqlSelectRequestedAttributes');

const schema = `
  subscriptionList(filter: SubscriptionFilterInput, limit: Int, offset: Int, orderBy: SubscriptionOrderByEnum): SubscriptionPage!
`;

const resolver = {
  subscriptionList: async (root, args, context, info) => {
    return new SubscriptionService(context).findAndCountAll({
      ...args,
      requestedAttributes: graphqlSelectRequestedAttributes(info, 'rows'),
    });
  },
};

exports.schema = schema;
exports.resolver = resolver;
