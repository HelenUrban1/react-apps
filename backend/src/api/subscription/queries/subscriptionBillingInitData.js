const SubscriptionService = require('../../../services/subscriptionService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions')
  .values;

const schema = `
  subscriptionBillingInitData(accountId: String!): SubscriptionBillingInitData!
`;

const resolver = {
  subscriptionBillingInitData: async (root, args, context) => {
    new PermissionChecker(context)
      .validateHas(permissions.accountBillingAccess);

    return new SubscriptionService(context).getBillingInitData(args.accountId);
  },
};

exports.schema = schema;
exports.resolver = resolver;
