const SubscriptionService = require('../../../services/subscriptionService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions')
  .values;

const schema = `
  subscriptionBillingInvoiceDownload(id: String!): SubscriptionBillingInvoiceDownload!
`;

const resolver = {
  subscriptionBillingInvoiceDownload: async (root, args, context) => {
    new PermissionChecker(context)
      .validateHas(permissions.accountBillingAccess);

    return new SubscriptionService(context).downloadInvoice(args.id);
  },
};

exports.schema = schema;
exports.resolver = resolver;
