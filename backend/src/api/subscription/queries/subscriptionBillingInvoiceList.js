const SubscriptionService = require('../../../services/subscriptionService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions').values;

const schema = `
  subscriptionBillingInvoicesList(customer: String): [SubscriptionBillingInvoices!]!
`;

const resolver = {
  subscriptionBillingInvoicesList: async (root, args, context) => {
    new PermissionChecker(context).validateHas(permissions.accountBillingAccess);
    return new SubscriptionService(context).getInvoices(args.customer);
  },
};

exports.schema = schema;
exports.resolver = resolver;
