const SubscriptionService = require('../../../services/subscriptionService');

const schema = `
  subscriptionFind(accountId: String!): Subscription
`;

const resolver = {
  subscriptionFind: async (root, args, context) => {
    return new SubscriptionService(context).findByAccountId(args.accountId, context);
  },
};

exports.schema = schema;
exports.resolver = resolver;
