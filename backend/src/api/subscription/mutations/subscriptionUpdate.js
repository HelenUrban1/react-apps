const SubscriptionService = require('../../../services/subscriptionService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions').values;

const schema = `
  subscriptionUpdate(id: String!, data: SubscriptionInput!, type: String!, components: [SubscriptionComponentInput], billingToken: String, componentHandle: String): Subscription!
`;

const resolver = {
  subscriptionUpdate: async (root, args, context) => {
    if (args.type === 'Update_Usage') {
      // Allow Planners to update DB usage metrics
      new PermissionChecker(context).validateHas(permissions.accountManagement);
    } else {
      new PermissionChecker(context).validateHas(permissions.accountBillingAccess);
    }

    return new SubscriptionService(context).update(args.id, args.data, args.type, args.components, args.billingToken, args.componentHandle);
  },
};

exports.schema = schema;
exports.resolver = resolver;
