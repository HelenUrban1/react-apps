const schema = `
paymentFailure(payload: PaymentFailure): Boolean
`;

const resolver = {
  paymentFailure: async (root, args, context) => {
    return context.subscriptionWorker.paymentFailure(args.payload, context);
  },
};

exports.schema = schema;
exports.resolver = resolver;
