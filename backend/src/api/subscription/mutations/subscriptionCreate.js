const SubscriptionService = require('../../../services/subscriptionService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions')
  .values;

const schema = `
  subscriptionCreate(data: SubscriptionInput!): Subscription!
`;

const resolver = {
  subscriptionCreate: async (root, args, context) => {
    new PermissionChecker(context)
      .validateHas(permissions.accountBillingAccess);

    return new SubscriptionService(context).create(args.data);
  },
};

exports.schema = schema;
exports.resolver = resolver;
