const SubscriptionService = require('../../../services/subscriptionService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions').values;

const schema = `
  subscriptionBillingPaymentMethodCreate(id:String!, data: SubscriptionBillingPaymentMethodInput!): SubscriptionBillingPaymentMethod!
`;

const resolver = {
  subscriptionBillingPaymentMethodCreate: async (root, args, context) => {
    new PermissionChecker(context).validateHas(permissions.accountBillingAccess);

    return new SubscriptionService(context).update(args.id, args.data, 'Add_Payment');
  },
};

exports.schema = schema;
exports.resolver = resolver;
