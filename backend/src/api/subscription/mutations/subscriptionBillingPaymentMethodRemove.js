const SubscriptionService = require('../../../services/subscriptionService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions').values;

const schema = `
  subscriptionBillingPaymentMethodRemove(id:String! customer:String!): SubscriptionBillingPaymentMethod!
`;

const resolver = {
  subscriptionBillingPaymentMethodRemove: async (root, args, context) => {
    new PermissionChecker(context).validateHas(permissions.accountBillingAccess);

    // return new SubscriptionService(context).removePaymentMethod(args.id, args.customer);
    return new SubscriptionService(context).update(args.id, args.customer, 'Remove_Payment');
  },
};

exports.schema = schema;
exports.resolver = resolver;
