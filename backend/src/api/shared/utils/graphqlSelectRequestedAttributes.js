const graphqlFields = require('graphql-fields');
const get = require('lodash/get');

module.exports = function graphqlSelectRequestedAttributes(
  info,
  depth,
) {
  const root = graphqlFields(info);
  let obj = depth ? get(root, depth) : root;
  return Object.keys(obj);
};
