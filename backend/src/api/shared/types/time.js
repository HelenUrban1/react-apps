const { TimeScalar } = require('graphql-date-scalars');

const schema = `
  scalar Time
`;

const resolver = {
  Time: TimeScalar,
};

exports.schema = schema;
exports.Resolver = resolver;
