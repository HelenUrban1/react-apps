const { DateTimeScalar } = require('graphql-date-scalars');

const schema = `
  scalar DateTime
`;

const resolver = {
  DateTime: DateTimeScalar,
};

exports.schema = schema;
exports.Resolver = resolver;
