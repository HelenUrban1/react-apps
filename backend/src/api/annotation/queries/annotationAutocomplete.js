const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions').values;
const AnnotationService = require('../../../services/annotationService');

const schema = `
  annotationAutocomplete(query: String, limit: Int): [AutocompleteOption!]!
`;

const resolver = {
  annotationAutocomplete: async (root, args, context, info) => {
    new PermissionChecker(context).validateHas(permissions.annotationAutocomplete);

    return new AnnotationService(context).findAllAutocomplete(args.query, args.limit);
  },
};

exports.schema = schema;
exports.resolver = resolver;
