const AnnotationService = require('../../../services/annotationService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions').values;
const graphqlSelectRequestedAttributes = require('../../shared/utils/graphqlSelectRequestedAttributes');

const schema = `
  annotationList(filter: AnnotationFilterInput, limit: Int, offset: Int, orderBy: AnnotationOrderByEnum): AnnotationPage!
`;

const resolver = {
  annotationList: async (root, args, context, info) => {
    new PermissionChecker(context).validateHas(permissions.annotationRead);

    return new AnnotationService(context).findAndCountAll({
      ...args,
      requestedAttributes: graphqlSelectRequestedAttributes(info, 'rows'),
    });
  },
};

exports.schema = schema;
exports.resolver = resolver;
