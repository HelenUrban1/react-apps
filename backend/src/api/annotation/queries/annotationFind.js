const AnnotationService = require('../../../services/annotationService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions').values;

const schema = `
  annotationFind(id: String!): Annotation!
`;

const resolver = {
  annotationFind: async (root, args, context) => {
    new PermissionChecker(context).validateHas(permissions.annotationRead);

    return new AnnotationService(context).findById(args.id);
  },
};

exports.schema = schema;
exports.resolver = resolver;
