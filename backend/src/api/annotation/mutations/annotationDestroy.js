const AnnotationService = require('../../../services/annotationService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions').values;

const schema = `
  annotationDestroy(ids: [String!]!): Boolean
`;

const resolver = {
  annotationDestroy: async (root, args, context) => {
    new PermissionChecker(context).validateHas(permissions.annotationDestroy);

    await new AnnotationService(context).destroyAll(args.ids);

    return true;
  },
};

exports.schema = schema;
exports.resolver = resolver;
