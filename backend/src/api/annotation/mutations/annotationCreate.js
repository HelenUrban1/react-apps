const AnnotationService = require('../../../services/annotationService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions').values;

const schema = `
  annotationCreate(data: AnnotationInput!): Annotation!
`;

const resolver = {
  annotationCreate: async (root, args, context) => {
    new PermissionChecker(context).validateHas(permissions.annotationCreate);

    return new AnnotationService(context).create(args.data);
  },
};

exports.schema = schema;
exports.resolver = resolver;
