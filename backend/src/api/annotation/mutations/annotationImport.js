const AnnotationService = require('../../../services/annotationService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions').values;

const schema = `
  annotationImport(data: AnnotationInput!, importHash: String!): Boolean
`;

const resolver = {
  annotationImport: async (root, args, context) => {
    new PermissionChecker(context).validateHas(permissions.annotationImport);

    await new AnnotationService(context).import(args.data, args.importHash);

    return true;
  },
};

exports.schema = schema;
exports.resolver = resolver;
