module.exports = [
  require('./annotationCreate'), //
  require('./annotationDestroy'),
  require('./annotationUpdate'),
  require('./annotationImport'),
];
