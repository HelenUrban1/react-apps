const AnnotationService = require('../../../services/annotationService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions').values;

const schema = `
  annotationUpdate(id: String!, data: AnnotationInput!): Annotation!
`;

const resolver = {
  annotationUpdate: async (root, args, context) => {
    new PermissionChecker(context).validateHas(permissions.annotationEdit);

    return new AnnotationService(context).update(args.id, args.data);
  },
};

exports.schema = schema;
exports.resolver = resolver;
