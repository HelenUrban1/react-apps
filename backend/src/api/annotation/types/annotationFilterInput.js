const schema = `
  input AnnotationFilterInput {
    id: String
    annotation: String
    fontSize: Int
    fontColor: String
    borderColor: String
    backgroundColor: String
    previousRevision: String
    nextRevision: String
    originalAnnotation: String
    drawingRotationRange: [ Float ]
    drawingScaleRange: [ Float ]
    boxLocationYRange: [ Float ]
    boxLocationXRange: [ Float ]
    boxWidthRange: [ Float ]
    boxHeightRange: [ Float ]
    boxRotationRange: [ Float ]
    part: String
    characteristic: String
    drawing: String
    drawingSheet: String
    drawingSheetIndexRange: [ Int ]
    createdAtRange: [ DateTime ]
    deletedAtRange: [ DateTime ]
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
