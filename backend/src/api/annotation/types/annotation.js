const schema = `
  type Annotation {
    id: String!
    previousRevision: String
    nextRevision: String
    originalAnnotation: String
    annotation: String
    fontSize: Int
    fontColor: String
    borderColor: String
    backgroundColor: String
    drawingRotation: Float
    drawingScale: Float
    boxLocationY: Float
    boxLocationX: Float
    boxWidth: Float
    boxHeight: Float
    boxRotation: Float
    part: Part
    characteristic: Characteristic
    drawing: Drawing
    drawingSheet: DrawingSheet
    drawingSheetIndex: Int
    createdById: String
    updatedById: String
    createdAt: DateTime
    updatedAt: DateTime
    deletedAt: DateTime
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
