module.exports = [
  require('./annotation'), //
  require('./annotationInput'),
  require('./annotationFilterInput'),
  require('./annotationOrderByEnum'),
  require('./annotationPage'),
];
