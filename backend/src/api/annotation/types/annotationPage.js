const schema = `
  type AnnotationPage {
    rows: [Annotation!]!
    count: Int!
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
