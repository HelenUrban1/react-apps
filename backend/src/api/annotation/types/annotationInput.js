const schema = `
  input AnnotationInput {
    annotation: String
    fontSize: Int
    fontColor: String
    borderColor: String
    backgroundColor: String
    previousRevision: String
    nextRevision: String
    originalAnnotation: String
    drawingRotation: Float!
    drawingScale: Float!
    boxLocationY: Float!
    boxLocationX: Float!
    boxWidth: Float!
    boxHeight: Float!
    boxRotation: Float!
    partId: String
    drawingId: String
    drawingSheetId: String
    drawingSheetIndex: Int!
    characteristicId: String
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
