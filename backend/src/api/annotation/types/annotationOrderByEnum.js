const schema = `
  enum AnnotationOrderByEnum {
    id_ASC
    id_DESC
    annotation_ASC
    annotation_DESC
    fontSize_ASC
    fontSize_DESC
    fontColor_ASC
    fontColor_DESC
    borderColor_ASC
    borderColor_DESC
    backgroundColor_ASC
    backgroundColor_DESC
    drawingRotation_ASC
    drawingRotation_DESC
    drawingScale_ASC
    drawingScale_DESC
    boxLocationY_ASC
    boxLocationY_DESC
    boxLocationX_ASC
    boxLocationX_DESC
    boxWidth_ASC
    boxWidth_DESC
    boxHeight_ASC
    boxHeight_DESC
    boxRotation_ASC
    boxRotation_DESC
    drawingSheetIndex_ASC
    drawingSheetIndex_DESC
    updatedAt_ASC
    updatedAt_DESC
    createdAt_ASC
    createdAt_DESC
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
