module.exports = [
  require('./characteristicFind'),
  require('./characteristicList'),
  require('./characteristicAutocomplete'),
];
