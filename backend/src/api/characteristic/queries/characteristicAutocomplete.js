const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions')
  .values;
const CharacteristicService = require('../../../services/characteristicService');

const schema = `
  characteristicAutocomplete(query: String, limit: Int): [AutocompleteOption!]!
`;

const resolver = {
  characteristicAutocomplete: async (root, args, context, info) => {
    new PermissionChecker(context)
      .validateHas(permissions.characteristicAutocomplete);

    return new CharacteristicService(context).findAllAutocomplete(
      args.query,
      args.limit,
    );
  },
};

exports.schema = schema;
exports.resolver = resolver;
