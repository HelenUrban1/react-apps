const CharacteristicService = require('../../../services/characteristicService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions')
  .values;

const schema = `
  characteristicFind(id: String!): Characteristic!
`;

const resolver = {
  characteristicFind: async (root, args, context) => {
    new PermissionChecker(context)
      .validateHas(permissions.characteristicRead);

    return new CharacteristicService(context).findById(args.id);
  },
};

exports.schema = schema;
exports.resolver = resolver;
