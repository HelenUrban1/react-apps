const CharacteristicService = require('../../../services/characteristicService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions')
  .values;
const graphqlSelectRequestedAttributes = require('../../shared/utils/graphqlSelectRequestedAttributes');

const schema = `
  characteristicList(filter: CharacteristicFilterInput, limit: Int, offset: Int, orderBy: CharacteristicOrderByEnum): CharacteristicPage!
`;

const resolver = {
  characteristicList: async (root, args, context, info) => {
    new PermissionChecker(context)
      .validateHas(permissions.characteristicRead);

    return new CharacteristicService(context).findAndCountAll({
      ...args,
      requestedAttributes: graphqlSelectRequestedAttributes(
        info,
        'rows',
      ),
    });
  },
};

exports.schema = schema;
exports.resolver = resolver;
