const CharacteristicService = require('../../../services/characteristicService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions').values;

const schema = `
  characteristicCreateInAccount(data: CharacteristicInAccountInput!): Characteristic!
`;

const resolver = {
  characteristicCreateInAccount: async (root, args, context) => {
    new PermissionChecker(context).validateHas(permissions.characteristicCreate);

    return new CharacteristicService(context).createInAccount(args.data);
  },
};

exports.schema = schema;
exports.resolver = resolver;
