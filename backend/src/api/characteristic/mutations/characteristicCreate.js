const CharacteristicService = require('../../../services/characteristicService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions')
  .values;

const schema = `
  characteristicCreate(data: CharacteristicInput!): Characteristic!
`;

const resolver = {
  characteristicCreate: async (root, args, context) => {
    new PermissionChecker(context)
      .validateHas(permissions.characteristicCreate);

    return new CharacteristicService(context).create(
      args.data
    );
  },
};

exports.schema = schema;
exports.resolver = resolver;
