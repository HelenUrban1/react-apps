const CharacteristicService = require('../../../services/characteristicService');
const AnnotationService = require('../../../services/annotationService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions').values;

const schema = `
  characteristicDestroy(ids: [String!]!): Boolean
`;

const resolver = {
  characteristicDestroy: async (root, args, context) => {
    new PermissionChecker(context).validateHas(permissions.characteristicDestroy);

    await new CharacteristicService(context).destroyAll(args.ids);

    // Destroy any associated
    const annotationService = new AnnotationService(context);
    const annotations = await annotationService.findAndCountAll();

    if (annotations && annotations.rows) {
      const annotationsToDelete = annotations.rows.filter((a) => a.characteristic && args.ids.includes(a.characteristic.id));
      await annotationService.destroyAll(annotationsToDelete);
    }

    return true;
  },
};

exports.schema = schema;
exports.resolver = resolver;
