const CharacteristicService = require('../../../services/characteristicService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions')
  .values;

const schema = `
  characteristicImport(data: CharacteristicInput!, importHash: String!): Boolean
`;

const resolver = {
  characteristicImport: async (root, args, context) => {
    new PermissionChecker(context)
      .validateHas(permissions.characteristicImport);

    await new CharacteristicService(context).import(
      args.data,
      args.importHash
    );

    return true;
  },
};

exports.schema = schema;
exports.resolver = resolver;
