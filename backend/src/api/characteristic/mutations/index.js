module.exports = [
  require('./characteristicCreate'), //
  require('./characteristicCreateInAccount'),
  require('./characteristicCreateManyInAccount'),
  require('./characteristicDestroy'),
  require('./characteristicUpdate'),
  require('./characteristicImport'),
];
