const CharacteristicService = require('../../../services/characteristicService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions')
  .values;

const schema = `
  characteristicUpdate(id: String!, data: CharacteristicInput!): Characteristic!
`;

const resolver = {
  characteristicUpdate: async (root, args, context) => {
    new PermissionChecker(context)
      .validateHas(permissions.characteristicEdit);

    return new CharacteristicService(context).update(
      args.id,
      args.data
    );
  },
};

exports.schema = schema;
exports.resolver = resolver;
