const schema = `
  enum CharacteristicNotationTypeEnum {
    Dimension
    Geometric_Tolerance
    Surface_Finish
    Weld
    Note
    Other
  }
  
  enum CharacteristicNotationSubtypeEnum {
    Length
    Angle
    Radius
    Diameter
    Curvilinear
    Chamfer_Size
    Chamfer_Angle
    Bend_Radius
    Counterbore_Depth
    Counterbore_Diameter
    Counterbore_Angle
    Countersink_Diameter
    Countersink_Angle
    Depth
    Edge_Radius
    Thickness
    Thread
    Edge_Burr_Size
    Edge_Undercut_Size
    Edge_Passing_Size
    Profile_of_a_Surface
    Position
    Flatness
    Perpendicularity
    Angularity
    Circularity
    Runout
    Total_Runout
    Profile_of_a_Line
    Cylindricity
    Concentricity
    Symmetry
    Parallelism
    Straightness
    Roughness
    Lay
    Waviness
    Structure
    Material_Removal_Allowance
    Size
    Convexity
    Reinforcement
    Additional_Characteristic
    Bead_Contour
    Depth_of_Bevel
    Penetration
    Location
    Pitch
    Root_Opening
    Note
    Flag_Note
    Temperature
    Time
    Torque
    Voltage
    Temper_Conductivity
    Temper_Verification
    Sound
    Frequency
    Capacitance
    Electric_Current
    Resistance
    Electric_Charge
    Inductance
    Apparent_Power
    Reactive_Power
    Electric_Power
    Conductance
    Admittance
    Electric_Flux
    Magnetic_Field
    Magnetic_Flux
    Area
    Resistivity
    Conductivity
    Electric_Field
    Datum_Target
    Production_Method
    Annotation_Table
    Table
    Cell
    Callout
    Datum
    Balloon
  }

  enum CharacteristicNotationClassEnum {
    Tolerance
    Basic
    Reference
    N_A
  }

  enum CharacteristicCriticalityEnum {
    Incidental
    Minor
    Major
    Key
    Critical
  }

  enum CharacteristicCaptureMethodEnum {
    Automated
    Manual
    Custom
    Annotation
  }

  enum CharacteristicStatusEnum {
    Inactive
    Active
    Rejected
  }

  enum CharacteristicToleranceSourceEnum {
    Default_Tolerance
    Document_Defined
    Manually_Set
    N_A
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
