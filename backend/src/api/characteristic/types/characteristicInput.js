const schema = `
  input CharacteristicInput {
    previousRevision: String
    nextRevision: String
    originalCharacteristic: String
    notationType: String!
    notationSubtype: String!
    notationClass: CharacteristicNotationClassEnum!
    fullSpecification: String!
    quantity: Int
    nominal: String!
    upperSpecLimit: String
    lowerSpecLimit: String
    plusTol: String
    minusTol: String
    unit: String
    inspectionMethod: String
    notes: String
    drawingRotation: Float!
    drawingScale: Float!
    boxLocationY: Float!
    boxLocationX: Float!
    boxWidth: Float!
    boxHeight: Float!
    boxRotation: Float!
    markerGroup: String
    markerGroupShared: Boolean
    markerIndex: Int!
    markerSubIndex: Int
    markerLabel: String!
    markerStyle: String!
    markerLocationX: Float!
    markerLocationY: Float!
    markerFontSize: Int!
    markerSize: Int!
    markerRotation: Int!
    gridCoordinates: String
    balloonGridCoordinates: String
    connectionPointGridCoordinates: String
    part: String
    drawing: String
    drawingSheet: String
    drawingSheetIndex: Int!
    gdtSymbol: String
    gdtPrimaryToleranceZone: String
    gdtSecondaryToleranceZone: String
    gdtPrimaryDatum: String
    gdtSecondaryDatum: String
    gdtTertiaryDatum: String
    captureMethod: CharacteristicCaptureMethodEnum!
    captureError: String
    status: CharacteristicStatusEnum!
    criticality: String
    verified: Boolean
    fullSpecificationFromOCR: String
    confidence: Int
    operation: String
    toleranceSource: CharacteristicToleranceSourceEnum!
    connectionPointLocationX: Float!
    connectionPointLocationY: Float!
    connectionPointIsFloating: Boolean
    displayLeaderLine: Boolean
    reviewTaskId: String
    leaderLineDistance: Float
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
