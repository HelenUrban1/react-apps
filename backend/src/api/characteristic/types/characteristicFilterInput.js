const schema = `
  input CharacteristicFilterInput {
    id: String
    previousRevision: String
    nextRevision: String
    originalCharacteristic: String
    notationType: String
    notationSubtype: String
    notationClass: CharacteristicNotationClassEnum
    fullSpecification: String
    quantityRange: [ Int ]
    nominal: String
    upperSpecLimit: String
    lowerSpecLimit: String
    plusTol: String
    minusTol: String
    unit: String
    criticality: String
    inspectionMethod: String
    notes: String
    drawingRotationRange: [ Float ]
    drawingScaleRange: [ Float ]
    boxLocationYRange: [ Float ]
    boxLocationXRange: [ Float ]
    boxWidthRange: [ Float ]
    boxHeightRange: [ Float ]
    boxRotationRange: [ Float ]
    markerGroup: String
    markerGroupShared: Boolean
    markerIndexRange: [ Int ]
    markerSubIndexRange: [ Int ]
    markerLabel: String
    markerStyle: String
    markerLocationXRange: [ Float ]
    markerLocationYRange: [ Float ]
    markerFontSize: [ Int ]
    markerSize: [ Int ]
    gridCoordinates: String
    balloonGridCoordinates: String
    connectionPointGridCoordinates: String
    part: String
    drawing: String
    drawingSheet: String
    drawingSheetIndexRange: [ Int ]
    gdtSymbol: String
    gdtPrimaryToleranceZone: String
    gdtSecondaryToleranceZone: String
    gdtPrimaryDatum: String
    gdtSecondaryDatum: String
    gdtTertiaryDatum: String
    captureMethod: CharacteristicCaptureMethodEnum
    status: [CharacteristicStatusEnum]
    verified: Boolean
    fullSpecificationFromOCR: String
    confidenceRange: [ Int ]
    operation: String
    toleranceSource: CharacteristicToleranceSourceEnum
    createdAtRange: [ DateTime ]
    deletedAtRange: [ DateTime ]
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
