module.exports = [
  require('./characteristic'), //
  require('./characteristicInput'),
  require('./characteristicInAccountInput'),
  require('./characteristicFilterInput'),
  require('./characteristicOrderByEnum'),
  require('./characteristicPage'),
  require('./characteristicEnums'),
];
