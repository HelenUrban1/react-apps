const schema = `
  type CharacteristicPage {
    rows: [Characteristic!]!
    count: Int!
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
