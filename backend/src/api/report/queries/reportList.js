const ReportService = require('../../../services/reportService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions')
  .values;
const graphqlSelectRequestedAttributes = require('../../shared/utils/graphqlSelectRequestedAttributes');

const schema = `
  reportList(filter: ReportFilterInput, limit: Int, offset: Int, orderBy: ReportOrderByEnum): ReportPage!
`;

const resolver = {
  reportList: async (root, args, context, info) => {
    new PermissionChecker(context)
      .validateHas(permissions.reportRead);

    return new ReportService(context).findAndCountAll({
      ...args,
      requestedAttributes: graphqlSelectRequestedAttributes(
        info,
        'rows',
      ),
    });
  },
};

exports.schema = schema;
exports.resolver = resolver;
