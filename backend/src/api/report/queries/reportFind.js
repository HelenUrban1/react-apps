const ReportService = require('../../../services/reportService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions').values;

const schema = `
  reportFind(id: String!): Report!
`;

const resolver = {
  reportFind: async (root, args, context) => {
    new PermissionChecker(context).validateHas(permissions.reportRead);

    return new ReportService(context).findById(args.id);
  },
};

exports.schema = schema;
exports.resolver = resolver;
