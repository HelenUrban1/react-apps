const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions')
  .values;
const ReportService = require('../../../services/reportService');

const schema = `
  reportAutocomplete(query: String, limit: Int): [AutocompleteOption!]!
`;

const resolver = {
  reportAutocomplete: async (root, args, context, info) => {
    new PermissionChecker(context)
      .validateHas(permissions.reportAutocomplete);

    return new ReportService(context).findAllAutocomplete(
      args.query,
      args.limit,
    );
  },
};

exports.schema = schema;
exports.resolver = resolver;
