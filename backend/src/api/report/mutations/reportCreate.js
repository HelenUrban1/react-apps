const ReportService = require('../../../services/reportService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions')
  .values;

const schema = `
  reportCreate(data: ReportInput!): Report!
`;

const resolver = {
  reportCreate: async (root, args, context) => {
    new PermissionChecker(context)
      .validateHas(permissions.reportCreate);

    return new ReportService(context).create(
      args.data
    );
  },
};

exports.schema = schema;
exports.resolver = resolver;
