module.exports = [
  require('./reportCreate'),
  require('./reportDestroy'),
  require('./reportUpdate'),
  require('./reportImport'),
];
