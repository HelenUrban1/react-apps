const ReportService = require('../../../services/reportService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions')
  .values;

const schema = `
  reportImport(data: ReportInput!, importHash: String!): Boolean
`;

const resolver = {
  reportImport: async (root, args, context) => {
    new PermissionChecker(context)
      .validateHas(permissions.reportImport);

    await new ReportService(context).import(
      args.data,
      args.importHash
    );

    return true;
  },
};

exports.schema = schema;
exports.resolver = resolver;
