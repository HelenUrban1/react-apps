const ReportService = require('../../../services/reportService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions')
  .values;

const schema = `
  reportDestroy(ids: [String!]!): Boolean
`;

const resolver = {
  reportDestroy: async (root, args, context) => {
    new PermissionChecker(context)
      .validateHas(permissions.reportDestroy);

    await new ReportService(context).destroyAll(
      args.ids
    );

    return true;
  },
};

exports.schema = schema;
exports.resolver = resolver;
