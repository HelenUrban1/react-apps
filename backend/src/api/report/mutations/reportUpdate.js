const ReportService = require('../../../services/reportService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions')
  .values;

const schema = `
  reportUpdate(id: String!, data: ReportInput!): Report!
`;

const resolver = {
  reportUpdate: async (root, args, context) => {
    new PermissionChecker(context)
      .validateHas(permissions.reportEdit);

    return new ReportService(context).update(
      args.id,
      args.data
    );
  },
};

exports.schema = schema;
exports.resolver = resolver;
