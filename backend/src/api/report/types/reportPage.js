const schema = `
  type ReportPage {
    rows: [Report!]!
    count: Int!
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
