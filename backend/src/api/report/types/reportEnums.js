const schema = `
  enum ReportStatusEnum {
    Active
    Inactive
    Deleted
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
