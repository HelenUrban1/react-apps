const schema = `
  input ReportInput {
    customer: String
    file: [ FileInput! ]!
    part: String!
    partVersion: String!
    template: String
    templateUrl:String
    filters:String
    partUpdatedAt:String
    data: String
    title: String!
    status: ReportStatusEnum!
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
