const schema = `
  type Report {
    id: String!
    site: Site
    customer: Organization
    part: Part
    partVersion: String
    data: String
    title: String 
    file: [ File! ]
    template: ReportTemplate
    templateUrl: String!
    filters: String
    partUpdatedAt: String
    status: ReportStatusEnum
    createdAt: DateTime
    updatedAt: DateTime
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
