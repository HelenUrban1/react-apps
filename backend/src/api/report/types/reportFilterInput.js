const schema = `
  input ReportFilterInput {
    id: String
    site: String
    customer: String
    part: String
    partVersion: String
    template: String
    title: String
    status: ReportStatusEnum
    createdAtRange: [ DateTime ]
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
