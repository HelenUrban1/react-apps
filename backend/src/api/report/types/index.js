module.exports = [
  require('./report'),
  require('./reportInput'),
  require('./reportFilterInput'),
  require('./reportOrderByEnum'),
  require('./reportPage'),
  require('./reportEnums'),
];
