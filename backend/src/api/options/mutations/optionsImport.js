const OptionsService = require('../../../services/optionsService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions')
  .values;

const schema = `
  optionsImport(data: OptionsInput!, importHash: String!): Boolean
`;

const resolver = {
  optionsImport: async (root, args, context) => {
    new PermissionChecker(context)
      .validateHas(permissions.optionsImport);

    await new OptionsService(context).import(
      args.data,
      args.importHash
    );

    return true;
  },
};

exports.schema = schema;
exports.resolver = resolver;
