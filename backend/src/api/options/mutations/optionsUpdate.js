const OptionsService = require('../../../services/optionsService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions')
  .values;

const schema = `
  optionsUpdate(id: String!, data: OptionsInput!): Options!
`;

const resolver = {
  optionsUpdate: async (root, args, context) => {
    new PermissionChecker(context)
      .validateHas(permissions.optionsEdit);

    return new OptionsService(context).update(
      args.id,
      args.data
    );
  },
};

exports.schema = schema;
exports.resolver = resolver;
