const OptionsService = require('../../../services/optionsService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions')
  .values;

const schema = `
  optionsCreate(data: OptionsInput!): Options!
`;

const resolver = {
  optionsCreate: async (root, args, context) => {
    new PermissionChecker(context)
      .validateHas(permissions.optionsCreate);

    return new OptionsService(context).create(
      args.data
    );
  },
};

exports.schema = schema;
exports.resolver = resolver;
