const OptionsService = require('../../../services/optionsService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions')
  .values;

const schema = `
  optionsDestroy(ids: [String!]!): Boolean
`;

const resolver = {
  optionsDestroy: async (root, args, context) => {
    new PermissionChecker(context)
      .validateHas(permissions.optionsDestroy);

    await new OptionsService(context).destroyAll(
      args.ids
    );

    return true;
  },
};

exports.schema = schema;
exports.resolver = resolver;
