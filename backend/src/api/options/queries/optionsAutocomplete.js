const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions')
  .values;
const OptionsService = require('../../../services/optionsService');

const schema = `
  optionsAutocomplete(query: String, limit: Int): [AutocompleteOption!]!
`;

const resolver = {
  optionsAutocomplete: async (root, args, context, info) => {
    new PermissionChecker(context)
      .validateHas(permissions.optionsAutocomplete);

    return new OptionsService(context).findAllAutocomplete(
      args.query,
      args.limit,
    );
  },
};

exports.schema = schema;
exports.resolver = resolver;
