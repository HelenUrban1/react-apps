module.exports = [
  require('./optionsFind'),
  require('./optionsList'),
  require('./optionsAutocomplete'),
];
