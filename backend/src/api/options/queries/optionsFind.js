const OptionsService = require('../../../services/optionsService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions')
  .values;

const schema = `
  optionsFind(id: String!): Options!
`;

const resolver = {
  optionsFind: async (root, args, context) => {
    new PermissionChecker(context)
      .validateHas(permissions.optionsRead);

    return new OptionsService(context).findById(args.id);
  },
};

exports.schema = schema;
exports.resolver = resolver;
