const OptionsService = require('../../../services/optionsService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions')
  .values;
const graphqlSelectRequestedAttributes = require('../../shared/utils/graphqlSelectRequestedAttributes');

const schema = `
  optionsList(filter: OptionsFilterInput, limit: Int, offset: Int, orderBy: OptionsOrderByEnum): OptionsPage!
`;

const resolver = {
  optionsList: async (root, args, context, info) => {
    new PermissionChecker(context)
      .validateHas(permissions.optionsRead);

    return new OptionsService(context).findAndCountAll({
      ...args,
      requestedAttributes: graphqlSelectRequestedAttributes(
        info,
        'rows',
      ),
    });
  },
};

exports.schema = schema;
exports.resolver = resolver;
