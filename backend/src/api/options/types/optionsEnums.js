const schema = `
  enum OptionsEntityAssocitationEnum {
    Organization
    User
  }

  enum OptionsStatusEnum {
    Active
    Archived
  }

  enum OptionsTypeEnum {
    Text
    Integer
    Decimal
    Boolean
    Select
    Multiselect
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
