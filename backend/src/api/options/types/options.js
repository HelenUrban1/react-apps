const schema = `
  type Options {
    id: String!
    entityAssocitation: OptionsEntityAssocitationEnum
    status: OptionsStatusEnum
    label: String
    tip: String
    details: String
    accessPath: String
    type: OptionsTypeEnum
    options: String
    validations: String
    createdAt: DateTime
    updatedAt: DateTime
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
