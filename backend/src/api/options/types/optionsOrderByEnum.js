const schema = `
  enum OptionsOrderByEnum {
    id_ASC
    id_DESC
    entityAssocitation_ASC
    entityAssocitation_DESC
    status_ASC
    status_DESC
    label_ASC
    label_DESC
    tip_ASC
    tip_DESC
    details_ASC
    details_DESC
    accessPath_ASC
    accessPath_DESC
    type_ASC
    type_DESC
    options_ASC
    options_DESC
    validations_ASC
    validations_DESC
    createdAt_ASC
    createdAt_DESC
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
