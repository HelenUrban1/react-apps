const schema = `
  input OptionsInput {
    entityAssocitation: OptionsEntityAssocitationEnum!
    status: OptionsStatusEnum!
    label: String!
    tip: String
    details: String
    accessPath: String!
    type: OptionsTypeEnum!
    options: String
    validations: String
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
