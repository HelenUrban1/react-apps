const schema = `
  type OptionsPage {
    rows: [Options!]!
    count: Int!
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
