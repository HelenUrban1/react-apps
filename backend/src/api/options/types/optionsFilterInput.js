const schema = `
  input OptionsFilterInput {
    id: String
    entityAssocitation: OptionsEntityAssocitationEnum
    status: OptionsStatusEnum
    label: String
    tip: String
    details: String
    accessPath: String
    type: OptionsTypeEnum
    options: String
    validations: String
    createdAtRange: [ DateTime ]
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
