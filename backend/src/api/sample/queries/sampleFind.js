const SampleService = require('../../../services/sampleService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions').values;

const schema = `
  sampleFind(id: String!): Sample!
`;

const resolver = {
  sampleFind: async (root, args, context) => {
    new PermissionChecker(context).validateHas(permissions.metro);
    return new SampleService(context).findById(args.id);
  },
};

exports.schema = schema;
exports.resolver = resolver;
