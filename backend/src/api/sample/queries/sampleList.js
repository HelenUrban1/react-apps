const SampleService = require('../../../services/sampleService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions').values;
const graphqlSelectRequestedAttributes = require('../../shared/utils/graphqlSelectRequestedAttributes');

const schema = `
  sampleList(filter: SampleFilterInput!, limit: Int, offset: Int, orderBy: SampleOrderByEnum): SamplePage!
`;

const resolver = {
  sampleList: async (root, args, context, info) => {
    new PermissionChecker(context).validateHas(permissions.metro);

    return new SampleService(context).findAndCountAll({
      ...args,
      requestedAttributes: graphqlSelectRequestedAttributes(info, 'rows'),
    });
  },
};

exports.schema = schema;
exports.resolver = resolver;
