const schema = `
  input SampleFilterInput {
    id: String
    serial: String
    sampleIndex: Int
    featureCoverage: String
    status: SampleStatusEnum
    part: String
    job: String
    createdAtRange: [ DateTime ]
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
