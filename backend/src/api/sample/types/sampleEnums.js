const schema = `
  enum SampleStatusEnum {
    Unmeasured
    Pass
    Fail
    Scrap
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
