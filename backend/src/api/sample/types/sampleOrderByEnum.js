const schema = `
  enum SampleOrderByEnum {
    id_ASC
    id_DESC
    serial_ASC
    serial_DESC
    sampleIndex_ASC
    sampleIndex_DESC
    featureCoverage_ASC
    featureCoverage_DESC
    status_ASC
    status_DESC
    createdAt_ASC
    createdAt_DESC
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
