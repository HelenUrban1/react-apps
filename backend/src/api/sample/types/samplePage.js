const schema = `
  type SamplePage {
    rows: [Sample!]!
    count: Int!
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
