const schema = `
  input SampleInput {
    serial: String!
    sampleIndex: Int!
    featureCoverage: String
    status: SampleStatusEnum!
    part: String
    job: String
    updatingOperatorId: String
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
