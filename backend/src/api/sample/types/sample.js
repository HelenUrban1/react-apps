const schema = `
  type Sample {
    id: String!
    serial: String!
    sampleIndex: Int!
    featureCoverage: String
    status: SampleStatusEnum!
    partId: String
    part: Part
    job: Job
    measurements: [Measurement]
    updatingOperatorId: String
    createdAt: DateTime
    updatedAt: DateTime
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
