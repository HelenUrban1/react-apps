const SampleService = require('../../../services/sampleService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions').values;

const schema = `
  sampleCreate(data: SampleInput!): Sample!
`;

const resolver = {
  sampleCreate: async (root, args, context) => {
    new PermissionChecker(context).validateHas(permissions.metro);
    return new SampleService(context).create(args.data);
  },
};

exports.schema = schema;
exports.resolver = resolver;
