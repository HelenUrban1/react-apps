const SampleService = require('../../../services/sampleService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions').values;

const schema = `
  sampleDestroy(ids: [String!]!): Boolean
`;

const resolver = {
  sampleDestroy: async (root, args, context) => {
    new PermissionChecker(context).validateHas(permissions.metro);

    await new SampleService(context).destroyAll(args.ids);

    return true;
  },
};

exports.schema = schema;
exports.resolver = resolver;
