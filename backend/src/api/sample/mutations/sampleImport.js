const SampleService = require('../../../services/sampleService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions').values;

const schema = `
  sampleImport(data: SampleInput!, importHash: String!): Boolean
`;

const resolver = {
  sampleImport: async (root, args, context) => {
    new PermissionChecker(context).validateHas(permissions.metro);
    await new SampleService(context).import(args.data, args.importHash);
    return true;
  },
};

exports.schema = schema;
exports.resolver = resolver;
