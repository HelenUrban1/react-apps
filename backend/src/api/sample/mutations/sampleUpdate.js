const SampleService = require('../../../services/sampleService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions').values;

const schema = `
  sampleUpdate(id: String!, data: SampleInput!): Sample!
`;

const resolver = {
  sampleUpdate: async (root, args, context) => {
    new PermissionChecker(context).validateHas(permissions.metro);
    return new SampleService(context).update(args.id, args.data);
  },
};

exports.schema = schema;
exports.resolver = resolver;
