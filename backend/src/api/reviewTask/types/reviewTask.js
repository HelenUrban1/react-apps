const schema = `
  type ReviewTask {
    id: String!
    type: String
    status: String!
    metadata: String
    processingStart: DateTime
    processingEnd: DateTime
    accountId: String!
    siteId: String!
    partId: String!
    drawingId: String!
    reviewerId: String
    createdAt: DateTime!
    updatedAt: DateTime!
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
