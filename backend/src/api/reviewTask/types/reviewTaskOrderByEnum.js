const schema = `
  enum ReviewTaskOrderByEnum {
    id_ASC
    id_DESC
    type_ASC
    type_DESC
    status_ASC
    status_DESC
    processingStart_ASC
    processingStart_DESC
    processingEnd_ASC
    processingEnd_DESC
    createdAt_ASC
    createdAt_DESC
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
