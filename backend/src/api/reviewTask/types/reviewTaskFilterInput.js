const schema = `
  input ReviewTaskFilterInput {
    id: String
    type: String
    status: String
    processingStartRange: [ DateTime ]
    processingEndRange: [ DateTime ]
    accountId: String
    siteId: String
    partId: String
    drawingId: String
    reviewerId: String
    createdAtRange: [ DateTime ]
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
