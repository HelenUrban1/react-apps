const schema = `
  input ReviewTaskInput {
    type: String!
    status: String!
    metadata: String
    processingStart: DateTime
    processingEnd: DateTime
    accountId: String!
    siteId: String!
    partId: String!
    drawingId: String!
    reviewerId: String
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
