const reviewTask = require('./reviewTask');
const reviewTaskFilterInput = require('./reviewTaskFilterInput');
const reviewTaskInput = require('./reviewTaskInput');
const reviewTaskOrderByEnum = require('./reviewTaskOrderByEnum');
const reviewTaskPage = require('./reviewTaskPage');

module.exports = [
  reviewTask, //
  reviewTaskFilterInput,
  reviewTaskInput,
  reviewTaskOrderByEnum,
  reviewTaskPage,
];
