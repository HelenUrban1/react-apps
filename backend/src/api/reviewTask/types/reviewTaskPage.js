const schema = `
  type ReviewTaskPage {
    rows: [ReviewTask!]!
    count: Int!
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
