const ReviewTaskService = require('../../../services/reviewTaskService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions').values;

const schema = `
  reviewTaskProcessedCount(partId: String): Int!
`;

const resolver = {
  reviewTaskProcessedCount: async (root, args, context) => {
    new PermissionChecker(context).validateHas(permissions.reviewTaskRead);
    return new ReviewTaskService(context).processedTasksCount(args.partId);
  },
};

exports.schema = schema;
exports.resolver = resolver;
