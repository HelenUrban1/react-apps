const ReviewTaskService = require('../../../services/reviewTaskService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions').values;

const schema = `
  reviewTaskPartQueuedCaptureCount(partId: String): Int!
`;

const resolver = {
  reviewTaskPartQueuedCaptureCount: async (root, args, context) => {
    new PermissionChecker(context).validateHas(permissions.reviewTaskRead);
    return new ReviewTaskService(context).partQueuedCaptureCount(args.partId);
  },
};

exports.schema = schema;
exports.resolver = resolver;
