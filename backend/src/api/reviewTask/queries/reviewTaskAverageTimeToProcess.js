const ReviewTaskService = require('../../../services/reviewTaskService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions').values;

const schema = `
  reviewTaskAverageTimeToProcess: String!
`;

const resolver = {
  reviewTaskAverageTimeToProcess: async (root, args, context) => {
    new PermissionChecker(context).validateHas(permissions.reviewTaskRead);
    return new ReviewTaskService(context).averageTimeToProcess();
  },
};

exports.schema = schema;
exports.resolver = resolver;
