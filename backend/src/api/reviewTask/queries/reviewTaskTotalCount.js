const ReviewTaskService = require('../../../services/reviewTaskService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions').values;

const schema = `
  reviewTaskTotalCount(partId: String): Int!
`;

const resolver = {
  reviewTaskTotalCount: async (root, args, context) => {
    new PermissionChecker(context).validateHas(permissions.reviewTaskRead);
    return new ReviewTaskService(context).totalTasksCount(args.partId);
  },
};

exports.schema = schema;
exports.resolver = resolver;
