const ReviewTaskService = require('../../../services/reviewTaskService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions').values;

const schema = `
  reviewTaskPullFromQueue: ReviewTask
`;

const resolver = {
  reviewTaskPullFromQueue: async (root, args, context) => {
    new PermissionChecker(context).validateHas(permissions.reviewTaskRead);
    return new ReviewTaskService(context).pullFromQueue();
  },
};

exports.schema = schema;
exports.resolver = resolver;
