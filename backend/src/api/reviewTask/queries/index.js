const reviewTaskFind = require('./reviewTaskFind');
const reviewTaskList = require('./reviewTaskList');
const reviewTaskPullFromQueue = require('./reviewTaskPullFromQueue');
const reviewTaskPartQueuedCaptureCount = require('./reviewTaskPartQueuedCaptureCount');
const reviewTaskWaitingCount = require('./reviewTaskWaitingCount');
const reviewTaskProcessedCount = require('./reviewTaskProcessedCount');
const reviewTaskTotalCount = require('./reviewTaskTotalCount');
const reviewTaskAverageTimeInQueue = require('./reviewTaskAverageTimeInQueue');
const reviewTaskAverageTimeToProcess = require('./reviewTaskAverageTimeToProcess');

module.exports = [
  reviewTaskFind, //
  reviewTaskList,
  reviewTaskPullFromQueue,
  reviewTaskPartQueuedCaptureCount,
  reviewTaskWaitingCount,
  reviewTaskProcessedCount,
  reviewTaskTotalCount,
  reviewTaskAverageTimeInQueue,
  reviewTaskAverageTimeToProcess,
];
