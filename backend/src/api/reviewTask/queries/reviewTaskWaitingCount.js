const ReviewTaskService = require('../../../services/reviewTaskService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions').values;

const schema = `
  reviewTaskWaitingCount(partId: String): Int!
`;

const resolver = {
  reviewTaskWaitingCount: async (root, args, context) => {
    new PermissionChecker(context).validateHas(permissions.reviewTaskRead);
    return new ReviewTaskService(context).waitingTasksCount(args.partId);
  },
};

exports.schema = schema;
exports.resolver = resolver;
