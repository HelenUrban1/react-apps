const ReviewTaskService = require('../../../services/reviewTaskService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions').values;

const schema = `
  reviewTaskFindAllById(id: String!): ReviewTask!
`;

const resolver = {
  reviewTaskFindAllById: async (root, args, context) => {
    new PermissionChecker(context).validateHas(permissions.reviewTaskRead);
    return new ReviewTaskService(context).findById(args.id);
  },
};

exports.schema = schema;
exports.resolver = resolver;
