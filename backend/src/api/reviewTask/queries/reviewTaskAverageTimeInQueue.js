const ReviewTaskService = require('../../../services/reviewTaskService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions').values;

const schema = `
  reviewTaskAverageTimeInQueue: String!
`;

const resolver = {
  reviewTaskAverageTimeInQueue: async (root, args, context) => {
    new PermissionChecker(context).validateHas(permissions.reviewTaskRead);
    return new ReviewTaskService(context).averageTimeInQueue();
  },
};

exports.schema = schema;
exports.resolver = resolver;
