const ReviewTaskService = require('../../../services/reviewTaskService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions').values;

const schema = `
  reviewTaskAcceptCapture(id: String!, data: ReviewTaskInput!): Boolean
`;

const resolver = {
  reviewTaskAcceptCapture: async (root, args, context) => {
    new PermissionChecker(context).validateHas(permissions.reviewTaskEdit);

    await new ReviewTaskService(context).acceptCapture(args.id, args.data);

    return true;
  },
};

exports.schema = schema;
exports.resolver = resolver;
