const ReviewTaskService = require('../../../services/reviewTaskService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions').values;

const schema = `
  reviewTaskUpdate(id: String!, data: ReviewTaskInput!): ReviewTask!
`;

const resolver = {
  reviewTaskUpdate: async (root, args, context) => {
    new PermissionChecker(context).validateHas(permissions.reviewTaskEdit);

    return new ReviewTaskService(context).update(args.id, args.data);
  },
};

exports.schema = schema;
exports.resolver = resolver;
