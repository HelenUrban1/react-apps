const ReviewTaskService = require('../../../services/reviewTaskService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions').values;

const schema = `
  reviewTaskReturnToQueue(id: String!): Boolean
`;

const resolver = {
  reviewTaskReturnToQueue: async (root, args, context) => {
    new PermissionChecker(context).validateHas(permissions.reviewTaskEdit);

    await new ReviewTaskService(context).returnToQueue(args.id);

    return true;
  },
};

exports.schema = schema;
exports.resolver = resolver;
