const ReviewTaskService = require('../../../services/reviewTaskService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions').values;

const schema = `
  reviewTaskClearQueue: Int!
`;

const resolver = {
  reviewTaskClearQueue: async (root, args, context) => {
    new PermissionChecker(context).validateHas(permissions.reviewTaskDestroy);
    return new ReviewTaskService(context).clearQueue();
  },
};

exports.schema = schema;
exports.resolver = resolver;
