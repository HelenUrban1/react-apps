const ReviewTaskService = require('../../../services/reviewTaskService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions').values;

const schema = `
  reviewTaskCorrectCapture(id: String!, data: ReviewTaskInput!): Boolean
`;

const resolver = {
  reviewTaskCorrectCapture: async (root, args, context) => {
    new PermissionChecker(context).validateHas(permissions.reviewTaskEdit);

    await new ReviewTaskService(context).correctCapture(args.id, args.data);

    return true;
  },
};

exports.schema = schema;
exports.resolver = resolver;
