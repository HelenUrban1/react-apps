const ReviewTaskService = require('../../../services/reviewTaskService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions').values;

const schema = `
  reviewTaskAddToQueue(data: [ReviewTaskInput!]!): Boolean
`;

const resolver = {
  reviewTaskAddToQueue: async (root, args, context) => {
    new PermissionChecker(context).validateHas(permissions.reviewTaskEdit);

    await new ReviewTaskService(context).addToQueue(args.data);

    return true;
  },
};

exports.schema = schema;
exports.resolver = resolver;
