const reviewTaskCreate = require('./reviewTaskCreate');
const reviewTaskUpdate = require('./reviewTaskUpdate');
const reviewTaskDestroy = require('./reviewTaskDestroy');
const reviewTaskAddToQueue = require('./reviewTaskAddToQueue');
const reviewTaskReturnToQueue = require('./reviewTaskReturnToQueue');
const reviewTaskAcceptCapture = require('./reviewTaskAcceptCapture');
const reviewTaskCorrectCapture = require('./reviewTaskCorrectCapture');
const reviewTaskRejectCapture = require('./reviewTaskRejectCapture');
const reviewTaskClearQueue = require('./reviewTaskClearQueue');

module.exports = [
  reviewTaskCreate, //
  reviewTaskUpdate,
  reviewTaskDestroy,
  reviewTaskAddToQueue,
  reviewTaskReturnToQueue,
  reviewTaskAcceptCapture,
  reviewTaskCorrectCapture,
  reviewTaskRejectCapture,
  reviewTaskClearQueue,
];
