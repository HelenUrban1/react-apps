const ReviewTaskService = require('../../../services/reviewTaskService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions').values;

const schema = `
  reviewTaskCreate(data: ReviewTaskInput!): ReviewTask!
`;

const resolver = {
  reviewTaskCreate: async (root, args, context) => {
    new PermissionChecker(context).validateHas(permissions.reviewTaskEdit);

    return new ReviewTaskService(context).create(args.data);
  },
};

exports.schema = schema;
exports.resolver = resolver;
