const schema = `
  type PartPage {
    rows: [Part!]!
    count: Int!
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
