const schema = `
  enum PartOrderByEnum {
    id_ASC
    id_DESC
    purchaseOrderCode_ASC
    purchaseOrderCode_DESC
    purchaseOrderLink_ASC
    purchaseOrderLink_DESC
    type_ASC
    type_DESC
    name_ASC
    name_DESC
    number_ASC
    number_DESC
    revision_ASC
    revision_DESC
    drawingNumber_ASC
    drawingNumber_DESC
    drawingRevision_ASC
    drawingRevision_DESC
    defaultAngularUnit_ASC
    defaultAngularUnit_DESC
    defaultMarkerOptions_ASC
    defaultMarkerOptions_DESC
    defaultLinearTolerances_ASC
    defaultLinearTolerances_DESC
    defaultAngularTolerances_ASC
    defaultAngularTolerances_DESC
    presets_ASC
    presets_DESC
    status_ASC
    status_DESC
    workflowStage_ASC
    workflowStage_DESC
    reviewStatus_ASC
    reviewStatus_DESC
    completedAt_ASC
    completedAt_DESC
    balloonedAt_ASC
    balloonedAt_DESC
    reportGeneratedAt_ASC
    reportGeneratedAt_DESC
    notes_ASC
    notes_DESC
    accessControl_ASC
    accessControl_DESC
    tags_ASC
    tags_DESC
    createdAt_ASC
    createdAt_DESC
    updatedAt_ASC
    updatedAt_DESC
    customer_name_ASC
    customer_name_DESC
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
