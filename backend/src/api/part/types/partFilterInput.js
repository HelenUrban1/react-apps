const schema = `
  input PartFilterInput {
    id: String
    previousRevision: String
    nextRevision: String
    originalPart: String
    purchaseOrderCode: String
    purchaseOrderLink: String
    type: PartTypeEnum
    name: String
    number: String
    revision: String
    drawingNumber: String
    drawingRevision: String
    defaultMarkerOptions: String
    defaultLinearTolerances: String
    defaultAngularTolerances: String
    presets: String
    status: [PartStatusEnum]
    autoMarkupStatus: PartAutoMarkupStatusEnum
    reviewStatus: [PartReviewStatusEnum]
    workflowName: String
    workflowStage: Int
    completedAtRange: [ String ]
    balloonedAtRange: [ DateTime ]
    reportGeneratedAtRange: [ DateTime ]
    lastReportId: String
    notes: String
    accessControl: [PartAccessControlEnum]
    measurement:PartMeasurement
    tags: String
    site: String
    customer: [String]
    primaryDrawing: String
    createdAtRange: [ DateTime ]
    searchTerm: String
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
