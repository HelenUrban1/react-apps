module.exports = [
  require('./part'),
  require('./partRevision'),
  require('./partInput'),
  require('./partFilterInput'),
  require('./partOrderByEnum'),
  require('./partPage'),
  require('./partShallow'),
  require('./partShallowPage'),
  require('./partEnums'),
];
