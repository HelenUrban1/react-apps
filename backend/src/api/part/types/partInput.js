const schema = `
  input PartInput {
    previousRevision: String
    nextRevision: String
    originalPart: String
    primaryDrawing: String!
    workflowStage: Int!
    type: PartTypeEnum!
    status: PartStatusEnum!
    autoMarkupStatus: PartAutoMarkupStatusEnum
    reviewStatus: PartReviewStatusEnum!
    accessControl: PartAccessControlEnum!
    measurement:PartMeasurement!
    defaultMarkerOptions: String
    defaultLinearTolerances: String
    defaultAngularTolerances: String
    presets: String
    name: String
    number: String
    revision: String
    drawingNumber: String
    drawingRevision: String
    customer: String
    purchaseOrderCode: String
    purchaseOrderLink: String
    notes: String
    tags: String
    drawings: [ String ]
    characteristics: [ String ]
    reports: [ String! ]
    attachments: [ FileInput ]
    displayLeaderLines: Boolean
    workflowName: String
    completedAt: String
    balloonedAt: DateTime
    reportGeneratedAt: DateTime
    lastReportId: String
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
