const schema = `
  type PartShallow {
    id: String!
    name: String
    number: String
    revision: String
    characteristics: Boolean
    createdAt: DateTime
    updatedAt: DateTime
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
