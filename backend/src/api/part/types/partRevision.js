const schema = `
  type PartRevision {
    part: Part!
    drawings: [Drawing!]!
    sheets: [DrawingSheet!]!
    characteristics: [Characteristic]!
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
