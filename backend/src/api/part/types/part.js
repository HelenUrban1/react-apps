const schema = `
  type Part {
    id: String!
    previousRevision: String
    nextRevision: String
    originalPart: String
    purchaseOrderCode: String
    purchaseOrderLink: String
    type: PartTypeEnum
    name: String
    number: String
    revision: String
    drawingNumber: String
    drawingRevision: String
    defaultMarkerOptions: String
    defaultLinearTolerances: String
    defaultAngularTolerances: String
    presets: String
    status: PartStatusEnum
    autoMarkupStatus: PartAutoMarkupStatusEnum
    reviewStatus: PartReviewStatusEnum
    workflowName: String
    workflowStage: Int
    completedAt: String
    balloonedAt: DateTime
    reportGeneratedAt: DateTime
    lastReportId: String
    notes: String
    accessControl: PartAccessControlEnum
    measurement:PartMeasurement
    tags: String
    site: Site
    customer: Organization
    primaryDrawing: Drawing
    drawings: [ Drawing! ]
    attachments: [ File ]
    characteristics: [ Characteristic ]
    reports: [ Report! ]
    displayLeaderLines: Boolean
    createdAt: DateTime
    updatedAt: DateTime
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
