const schema = `
  enum PartTypeEnum {
    Part
    Assembly
  }

  enum PartStatusEnum {
    Active
    Inactive
    Deleted
  }

  enum PartReviewStatusEnum {
    Unverified
    Ready_for_Review
    Verified
  }

  enum PartAutoMarkupStatusEnum {
    In_Progress
    Completed
    Error
  }

  enum PartAccessControlEnum {
    None
    ITAR
  }

  enum PartMeasurement {
    Imperial
    Metric
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
