const PartService = require('../../../services/partService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions').values;

const schema = `
  partUpdate(id: String!, data: PartInput!): Part!
`;

const resolver = {
  partUpdate: async (root, args, context) => {
    new PermissionChecker(context).validateHas(permissions.partEdit);
    return new PartService(context).update(
      args.id,
      args.data
    );
  },
};

exports.schema = schema;
exports.resolver = resolver;
