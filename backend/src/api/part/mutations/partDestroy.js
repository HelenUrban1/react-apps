const PartService = require('../../../services/partService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions').values;

const schema = `
  partDestroy(ids: [String!]!): Boolean
`;

const resolver = {
  partDestroy: async (root, args, context) => {
    new PermissionChecker(context).validateHas(permissions.partDestroy);

    await new PartService(context).destroyAll(args.ids);

    return true;
  },
};

exports.schema = schema;
exports.resolver = resolver;
