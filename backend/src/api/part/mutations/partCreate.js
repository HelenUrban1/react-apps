const PartService = require('../../../services/partService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions').values;

const schema = `
  partCreate(data: PartInput!): Part!
`;

const resolver = {
  partCreate: async (root, args, context) => {
    new PermissionChecker(context).validateHas(permissions.partCreate);
    return new PartService(context).create(args.data);
  },
};

exports.schema = schema;
exports.resolver = resolver;
