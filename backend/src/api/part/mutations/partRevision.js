const RevisionService = require('../../../services/revisionService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions').values;

const schema = `
  partRevision(id: String!): PartRevision
`;

const resolver = {
  partRevision: async (root, args, context) => {
    new PermissionChecker(context).validateHas(permissions.partCreate);
    return new RevisionService(context).createPartRevision(args.id);
  },
};

exports.schema = schema;
exports.resolver = resolver;
