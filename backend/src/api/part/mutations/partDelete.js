const RevisionService = require('../../../services/revisionService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions').values;

const schema = `
  partDelete(id: String!): Boolean
`;

const resolver = {
  partDelete: async (root, args, context) => {
    new PermissionChecker(context).validateHas(permissions.partDelete);

    await new RevisionService(context).deletePart(args.id);

    return true;
  },
};

exports.schema = schema;
exports.resolver = resolver;
