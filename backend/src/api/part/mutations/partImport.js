const PartService = require('../../../services/partService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions')
  .values;

const schema = `
  partImport(data: PartInput!, importHash: String!): Boolean
`;

const resolver = {
  partImport: async (root, args, context) => {
    new PermissionChecker(context).validateHas(permissions.partImport);
    await new PartService(context).import(
      args.data,
      args.importHash
    );
    return true;
  },
};

exports.schema = schema;
exports.resolver = resolver;
