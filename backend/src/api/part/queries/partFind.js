const PartService = require('../../../services/partService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions')
  .values;

const schema = `
  partFind(id: String!): Part!
`;

const resolver = {
  partFind: async (root, args, context) => {
    new PermissionChecker(context).validateHas(permissions.partRead);
    return new PartService(context).findById(args.id);
  },
};

exports.schema = schema;
exports.resolver = resolver;
