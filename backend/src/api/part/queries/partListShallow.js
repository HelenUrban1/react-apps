const PartService = require('../../../services/partService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions').values;
const graphqlSelectRequestedAttributes = require('../../shared/utils/graphqlSelectRequestedAttributes');

const schema = `
  partListShallow(filter: PartFilterInput, limit: Int, offset: Int, orderBy: PartOrderByEnum): PartShallowPage!
`;

const resolver = {
  partListShallow: async (root, args, context, info) => {
    new PermissionChecker(context).validateHas(permissions.partRead);

    return new PartService(context).findAndCountAll({
      ...args,
      shallow: true,
      requestedAttributes: graphqlSelectRequestedAttributes(info, 'rows'),
    });
  },
};

exports.schema = schema;
exports.resolver = resolver;
