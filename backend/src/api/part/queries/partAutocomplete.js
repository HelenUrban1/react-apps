const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions')
  .values;
const PartService = require('../../../services/partService');

const schema = `
  partAutocomplete(query: String, limit: Int): [AutocompleteOption!]!
`;

const resolver = {
  partAutocomplete: async (root, args, context, info) => {
    new PermissionChecker(context)
      .validateHas(permissions.partAutocomplete);

    return new PartService(context).findAllAutocomplete(
      args.query,
      args.limit,
    );
  },
};

exports.schema = schema;
exports.resolver = resolver;
