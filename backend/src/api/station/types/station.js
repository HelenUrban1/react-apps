const schema = `
  type Station {
    id: String!
    disabled: Boolean
    isMaster: Boolean
    name: String!
    createdAt: DateTime
    updatedAt: DateTime
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
