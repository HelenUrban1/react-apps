const schema = `
  input StationInput {
    name: String!
    disabled: Boolean
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
