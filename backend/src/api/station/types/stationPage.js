const schema = `
  type StationPage {
    rows: [Station!]!
    count: Int!
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
