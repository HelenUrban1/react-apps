const schema = `
  input StationFilterInput {
    id: String
    name: String
    disabled: Boolean
    isMaster: Boolean
    createdAtRange: [ DateTime ]
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
