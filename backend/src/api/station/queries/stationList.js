const StationService = require('../../../services/stationService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions').values;
const graphqlSelectRequestedAttributes = require('../../shared/utils/graphqlSelectRequestedAttributes');

const schema = `
  stationList(filter: StationFilterInput, limit: Int, offset: Int, orderBy: StationOrderByEnum): StationPage!
`;

const resolver = {
  stationList: async (root, args, context, info) => {
    new PermissionChecker(context).validateHas(permissions.metro);

    return new StationService(context).findAndCountAll({
      ...args,
      requestedAttributes: graphqlSelectRequestedAttributes(info, 'rows'),
    });
  },
};

exports.schema = schema;
exports.resolver = resolver;
