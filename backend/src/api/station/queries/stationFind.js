const StationService = require('../../../services/stationService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions').values;

const schema = `
  stationFind(id: String!): Station!
`;

const resolver = {
  stationFind: async (root, args, context) => {
    new PermissionChecker(context).validateHas(permissions.metro);
    return new StationService(context).findById(args.id);
  },
};

exports.schema = schema;
exports.resolver = resolver;
