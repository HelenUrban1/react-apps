const StationService = require('../../../services/stationService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions').values;

const schema = `
  stationDestroy(ids: [String!]!): Boolean
`;

const resolver = {
  stationDestroy: async (root, args, context) => {
    new PermissionChecker(context).validateHas(permissions.metro);

    await new StationService(context).destroyAll(args.ids);

    return true;
  },
};

exports.schema = schema;
exports.resolver = resolver;
