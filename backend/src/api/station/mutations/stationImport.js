const StationService = require('../../../services/stationService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions').values;

const schema = `
  stationImport(data: StationInput!, importHash: String!): Boolean
`;

const resolver = {
  stationImport: async (root, args, context) => {
    new PermissionChecker(context).validateHas(permissions.metro);
    await new StationService(context).import(args.data, args.importHash);
    return true;
  },
};

exports.schema = schema;
exports.resolver = resolver;
