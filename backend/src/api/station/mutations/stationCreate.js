const StationService = require('../../../services/stationService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions').values;

const schema = `
  stationCreate(data: StationInput!): Station!
`;

const resolver = {
  stationCreate: async (root, args, context) => {
    new PermissionChecker(context).validateHas(permissions.metro);
    return new StationService(context).create(args.data);
  },
};

exports.schema = schema;
exports.resolver = resolver;
