const schema = `
  input NotificationInput {
    title: String!
    content: String
    action: String
    actionAt: DateTime
    priority: Int!
    relationId: String!
    relationType: String!
    userStatuses: [ String ]
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
