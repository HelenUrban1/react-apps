const schema = `
  input NotificationFilterInput {
    id: String    
    title: String
    content: String
    action: String
    actionAtRange: [ DateTime ]
    priority: Int
    relationId: String
    relationType: String
    actor: String
    site: String
    userStatuses: NotificationUserStatusFilterInput
    createdAtRange: [ DateTime ]
    updatedAtRange: [ DateTime ]
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
