const schema = `
  enum NotificationOrderByEnum {
    id_ASC
    id_DESC
    title_ASC
    title_DESC
    content_ASC
    content_DESC
    action_ASC
    action_DESC
    actionAt_ASC
    actionAt_DESC
    priority_ASC
    priority_DESC
    relationType_ASC
    relationType_DESC
    createdAt_ASC
    createdAt_DESC
    updatedAt_ASC
    updatedAt_DESC
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
