const schema = `
  type NotificationPage {
    rows: [Notification!]!
    count: Int!
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
