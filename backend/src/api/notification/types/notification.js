const schema = `
  type Notification {
    id: String!
    title: String!
    content: String
    action: String
    actionAt: DateTime
    priority: Int!
    relationId: String!
    relationType: String!
    site: Site
    userStatuses: [ NotificationUserStatus ]
    createdAt: DateTime
    updatedAt: DateTime
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
