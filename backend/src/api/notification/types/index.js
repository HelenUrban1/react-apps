module.exports = [
  require('./notification'), //
  require('./notificationInput'),
  require('./notificationFilterInput'),
  require('./notificationOrderByEnum'),
  require('./notificationPage'),
];
