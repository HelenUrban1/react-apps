const NotificationService = require('../../../services/notificationService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions').values;
const graphqlSelectRequestedAttributes = require('../../shared/utils/graphqlSelectRequestedAttributes');

const schema = `
notificationList(filter: NotificationFilterInput, limit: Int, offset: Int, orderBy: NotificationOrderByEnum): NotificationPage!
`;

const resolver = {
  notificationList: async (root, args, context, info) => {
    new PermissionChecker(context).validateHas(permissions.notificationRead);

    return new NotificationService(context).findAndCountAll({
      ...args,
      requestedAttributes: graphqlSelectRequestedAttributes(info, 'rows'),
    });
  },
};

exports.schema = schema;
exports.resolver = resolver;
