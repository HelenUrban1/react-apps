const NotificationService = require('../../../services/notificationService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions').values;

const schema = `
notificationImport(data: NotificationInput!, importHash: String!): Boolean
`;

const resolver = {
  notificationImport: async (root, args, context) => {
    new PermissionChecker(context).validateHas(permissions.notificationImport);
    await new NotificationService(context).import(args.data, args.importHash);
    return true;
  },
};

exports.schema = schema;
exports.resolver = resolver;
