const NotificationService = require('../../../services/notificationService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions').values;

const schema = `
notificationDestroy(ids: [String!]!): Boolean
`;

const resolver = {
  notificationDestroy: async (root, args, context) => {
    new PermissionChecker(context).validateHas(permissions.notificationDestroy);

    await new NotificationService(context).destroyAll(args.ids);

    return true;
  },
};

exports.schema = schema;
exports.resolver = resolver;
