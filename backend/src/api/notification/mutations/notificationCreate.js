const NotificationService = require('../../../services/notificationService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions').values;

const schema = `
notificationCreate(data: NotificationInput!): Notification!
`;

const resolver = {
  notificationCreate: async (root, args, context) => {
    new PermissionChecker(context).validateHas(permissions.notificationCreate);
    return new NotificationService(context).create(args.data);
  },
};

exports.schema = schema;
exports.resolver = resolver;
