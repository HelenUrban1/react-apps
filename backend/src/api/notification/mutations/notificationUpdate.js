const NotificationService = require('../../../services/notificationService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions').values;

const schema = `
notificationUpdate(id: String!, data: NotificationInput!): Notification!
`;

const resolver = {
  notificationUpdate: async (root, args, context) => {
    new PermissionChecker(context).validateHas(permissions.notificationEdit);
    return new NotificationService(context).update(args.id, args.data);
  },
};

exports.schema = schema;
exports.resolver = resolver;
