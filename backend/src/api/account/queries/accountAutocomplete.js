const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions')
  .values;
const AccountService = require('../../../services/accountService');

const schema = `
  accountAutocomplete(query: String, limit: Int): [AutocompleteOption!]!
`;

const resolver = {
  accountAutocomplete: async (root, args, context, info) => {
    new PermissionChecker(context)
      .validateHas(permissions.accountAutocomplete);

    return new AccountService(context).findAllAutocomplete(
      args.query,
      args.limit,
    );
  },
};

exports.schema = schema;
exports.resolver = resolver;
