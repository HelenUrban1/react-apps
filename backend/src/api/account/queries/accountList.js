const AccountService = require('../../../services/accountService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions')
  .values;
const graphqlSelectRequestedAttributes = require('../../shared/utils/graphqlSelectRequestedAttributes');

const schema = `
  accountList(filter: AccountFilterInput, limit: Int, offset: Int, orderBy: AccountOrderByEnum): AccountPage!
`;

const resolver = {
  accountList: async (root, args, context, info) => {
    new PermissionChecker(context)
      .validateHas(permissions.accountRead);

    return new AccountService(context).findAndCountAll({
      ...args,
      requestedAttributes: graphqlSelectRequestedAttributes(
        info,
        'rows',
      ),
    });
  },
};

exports.schema = schema;
exports.resolver = resolver;
