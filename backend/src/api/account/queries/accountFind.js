const AccountService = require('../../../services/accountService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions').values;

const schema = `
  accountFind(id: String!): Account!
`;

const resolver = {
  accountFind: async (root, args, context) => {
    if (args.id !== context.currentUser.accountId) {
      new PermissionChecker(context).validateHas(permissions.accountRead);
    }
    return new AccountService(context).findById(args.id);
  },
};

exports.schema = schema;
exports.resolver = resolver;
