const AccountService = require('../../../services/accountService');

const schema = `
  accountCreate(data: AccountInput!): Account!
`;

const resolver = {
  accountCreate: async (root, args, context) => {
    return new AccountService(context).create(args.data);
  },
};

exports.schema = schema;
exports.resolver = resolver;
