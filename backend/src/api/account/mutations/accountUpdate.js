const AccountService = require('../../../services/accountService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions').values;

const schema = `
  accountUpdate(id: String!, data: AccountInput!): Account!
`;

const resolver = {
  accountUpdate: async (root, args, context) => {
    new PermissionChecker(context).validateHas(permissions.accountEdit);

    return new AccountService(context).update(args.id, args.data);
  },
};

exports.schema = schema;
exports.resolver = resolver;
