const AccountService = require('../../../services/accountService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions')
  .values;

const schema = `
  accountImport(data: AccountInput!, importHash: String!): Boolean
`;

const resolver = {
  accountImport: async (root, args, context) => {
    new PermissionChecker(context)
      .validateHas(permissions.accountImport);

    await new AccountService(context).import(
      args.data,
      args.importHash
    );

    return true;
  },
};

exports.schema = schema;
exports.resolver = resolver;
