module.exports = [
  require('./accountCreate'),
  require('./accountDestroy'),
  require('./accountUpdate'),
  require('./accountImport'),
];
