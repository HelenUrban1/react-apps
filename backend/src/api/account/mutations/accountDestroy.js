const AccountService = require('../../../services/accountService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions')
  .values;

const schema = `
  accountDestroy(ids: [String!]!): Boolean
`;

const resolver = {
  accountDestroy: async (root, args, context) => {
    new PermissionChecker(context)
      .validateHas(permissions.accountDestroy);

    await new AccountService(context).destroyAll(
      args.ids
    );

    return true;
  },
};

exports.schema = schema;
exports.resolver = resolver;
