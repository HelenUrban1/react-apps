const schema = `
  type AccountPage {
    rows: [Account!]!
    count: Int!
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
