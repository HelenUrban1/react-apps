const schema = `
  input AccountInput {
    status: AccountStatusEnum!
    settings: String!
    orgName: String!
    orgPhone: String
    orgEmail: String!
    orgStreet: String
    orgStreet2: String
    orgCity: String
    orgRegion: AccountOrgRegionEnum
    orgPostalcode: String
    orgCountry: AccountOrgCountryEnum
    orgLogo: [ FileInput! ]
    orgUnitName: String
    orgUnitId: String
    parentAccountId: String
    dbHost: String!
    dbName: String!
    parts: [ String! ]
    members: [ String! ]
    reportTemplates: [ String! ]
    reports: [ String! ]
    organizationId: String
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
