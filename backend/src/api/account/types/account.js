const schema = `
  type Account {
    id: String!
    status: AccountStatusEnum
    settings: String
    orgName: String
    orgPhone: String
    orgEmail: String
    orgStreet: String
    orgStreet2: String
    orgCity: String
    orgRegion: AccountOrgRegionEnum
    orgPostalcode: String
    orgCountry: AccountOrgCountryEnum
    orgLogo: [ File! ]
    orgUnitName: String
    orgUnitId: String
    parentAccountId: String
    dbHost: String
    dbName: String
    parts: [ Part! ]
    members: [ AccountMember! ]
    reportTemplates: [ ReportTemplate! ]
    reports: [ Report! ]
    createdAt: DateTime
    updatedAt: DateTime
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
