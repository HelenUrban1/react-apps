const schema = `
  enum AccountOrderByEnum {
    id_ASC
    id_DESC
    status_ASC
    status_DESC
    settings_ASC
    settings_DESC
    orgName_ASC
    orgName_DESC
    orgPhone_ASC
    orgPhone_DESC
    orgEmail_ASC
    orgEmail_DESC
    orgStreet_ASC
    orgStreet_DESC
    orgStreet2_ASC
    orgStreet2_DESC
    orgCity_ASC
    orgCity_DESC
    orgRegion_ASC
    orgRegion_DESC
    orgPostalcode_ASC
    orgPostalcode_DESC
    orgCountry_ASC
    orgCountry_DESC
    orgUnitName_ASC
    orgUnitName_DESC
    orgUnitId_ASC
    orgUnitId_DESC
    parentAccountId_ASC
    parentAccountId_DESC
    createdAt_ASC
    createdAt_DESC
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
