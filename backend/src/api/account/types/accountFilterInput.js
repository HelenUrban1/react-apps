const schema = `
  input AccountFilterInput {
    id: String
    status: AccountStatusEnum
    settings: String
    orgName: String
    orgPhone: String
    orgEmail: String
    orgStreet: String
    orgStreet2: String
    orgCity: String
    orgRegion: AccountOrgRegionEnum
    orgPostalcode: String
    orgCountry: AccountOrgCountryEnum
    orgUnitName: String
    orgUnitId: String
    parentAccountId: String
    createdAtRange: [ DateTime ]
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
