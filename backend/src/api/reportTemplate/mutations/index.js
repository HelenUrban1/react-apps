module.exports = [
  require('./reportTemplateCreate'),
  require('./reportTemplateDestroy'),
  require('./reportTemplateUpdate'),
  require('./reportTemplateImport'),
];
