const ReportTemplateService = require('../../../services/reportTemplateService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions')
  .values;

const schema = `
  reportTemplateCreate(data: ReportTemplateInput!): ReportTemplate!
`;

const resolver = {
  reportTemplateCreate: async (root, args, context) => {
    new PermissionChecker(context)
      .validateHas(permissions.reportTemplateCreate);

    return new ReportTemplateService(context).create(
      args.data
    );
  },
};

exports.schema = schema;
exports.resolver = resolver;
