const ReportTemplateService = require('../../../services/reportTemplateService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions')
  .values;

const schema = `
  reportTemplateDestroy(ids: [String!]!): Boolean
`;

const resolver = {
  reportTemplateDestroy: async (root, args, context) => {
    new PermissionChecker(context)
      .validateHas(permissions.reportTemplateDestroy);

    await new ReportTemplateService(context).destroyAll(
      args.ids
    );

    return true;
  },
};

exports.schema = schema;
exports.resolver = resolver;
