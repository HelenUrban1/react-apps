const ReportTemplateService = require('../../../services/reportTemplateService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions')
  .values;

const schema = `
  reportTemplateUpdate(id: String!, data: ReportTemplateInput!): ReportTemplate!
`;

const resolver = {
  reportTemplateUpdate: async (root, args, context) => {
    new PermissionChecker(context)
      .validateHas(permissions.reportTemplateEdit);

    return new ReportTemplateService(context).update(
      args.id,
      args.data
    );
  },
};

exports.schema = schema;
exports.resolver = resolver;
