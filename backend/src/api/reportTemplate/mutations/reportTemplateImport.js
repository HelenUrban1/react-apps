const ReportTemplateService = require('../../../services/reportTemplateService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions')
  .values;

const schema = `
  reportTemplateImport(data: ReportTemplateInput!, importHash: String!): Boolean
`;

const resolver = {
  reportTemplateImport: async (root, args, context) => {
    new PermissionChecker(context)
      .validateHas(permissions.reportTemplateImport);

    await new ReportTemplateService(context).import(
      args.data,
      args.importHash
    );

    return true;
  },
};

exports.schema = schema;
exports.resolver = resolver;
