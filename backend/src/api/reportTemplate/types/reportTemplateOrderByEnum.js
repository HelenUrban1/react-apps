const schema = `
  enum ReportTemplateOrderByEnum {
    id_ASC
    id_DESC
    provider_ASC
    provider_DESC
    title_ASC
    title_DESC
    type_ASC
    type_DESC
    status_ASC
    status_DESC
    direction_ASC
    direction_DESC
    sortIndex_ASC
    sortIndex_DESC
    createdAt_ASC
    createdAt_DESC
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
