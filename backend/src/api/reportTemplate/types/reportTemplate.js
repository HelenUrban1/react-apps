const schema = `
  type ReportTemplate {
    id: String!
    site: Site
    provider: Organization
    title: String
    type:String
    status: ReportTemplateStatusEnum!
    direction: ReportTemplateDirectionEnum!
    settings: String
    file: [ File! ]
    sortIndex: Int
    filters: String
    createdAt: DateTime
    updatedAt: DateTime
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
