const schema = `
  input ReportTemplateInput {
    site: String
    title: String!
    provider: String
    type: String
    status: ReportTemplateStatusEnum!
    direction: ReportTemplateDirectionEnum!
    settings: String
    file: [ FileInput! ]
    sortIndex: Int
    filters: String
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
