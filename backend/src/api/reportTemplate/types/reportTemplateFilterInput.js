const schema = `
  input ReportTemplateFilterInput {
    id: String
    site: String
    title: String
    provider: [ String ]
    type: String
    status: [ ReportTemplateStatusEnum ]
    sortIndexRange: [ Int ]
    createdAtRange: [ DateTime ]
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
