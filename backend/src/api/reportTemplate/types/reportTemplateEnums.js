const schema = `
  enum ReportTemplateStatusEnum {
    Active
    Inactive
    Deleted
  }
  enum ReportTemplateDirectionEnum {
    Vertical
    Horizontal
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
