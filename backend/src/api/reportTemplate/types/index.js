module.exports = [
  require('./reportTemplate'),
  require('./reportTemplateInput'),
  require('./reportTemplateFilterInput'),
  require('./reportTemplateOrderByEnum'),
  require('./reportTemplatePage'),
  require('./reportTemplateEnums'),
];
