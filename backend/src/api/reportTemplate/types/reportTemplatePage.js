const schema = `
  type ReportTemplatePage {
    rows: [ReportTemplate!]!
    count: Int!
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
