const ReportTemplateService = require('../../../services/reportTemplateService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions').values;

const schema = `
  reportTemplateFind(id: String!): ReportTemplate
`;

const resolver = {
  reportTemplateFind: async (root, args, context) => {
    new PermissionChecker(context).validateHas(permissions.reportTemplateRead);

    return new ReportTemplateService(context).findById(args.id);
  },
};

exports.schema = schema;
exports.resolver = resolver;
