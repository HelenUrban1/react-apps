const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions')
  .values;
const ReportTemplateService = require('../../../services/reportTemplateService');

const schema = `
  reportTemplateAutocomplete(query: String, limit: Int): [AutocompleteOption!]!
`;

const resolver = {
  reportTemplateAutocomplete: async (root, args, context, info) => {
    new PermissionChecker(context)
      .validateHas(permissions.reportTemplateAutocomplete);

    return new ReportTemplateService(context).findAllAutocomplete(
      args.query,
      args.limit,
    );
  },
};

exports.schema = schema;
exports.resolver = resolver;
