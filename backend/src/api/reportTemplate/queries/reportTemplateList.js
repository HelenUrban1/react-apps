const ReportTemplateService = require('../../../services/reportTemplateService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions')
  .values;
const graphqlSelectRequestedAttributes = require('../../shared/utils/graphqlSelectRequestedAttributes');

const schema = `
  reportTemplateList(filter: ReportTemplateFilterInput, limit: Int, offset: Int, orderBy: ReportTemplateOrderByEnum): ReportTemplatePage!
`;

const resolver = {
  reportTemplateList: async (root, args, context, info) => {
    new PermissionChecker(context)
      .validateHas(permissions.reportTemplateRead);

    return new ReportTemplateService(context).findAndCountAll({
      ...args,
      requestedAttributes: graphqlSelectRequestedAttributes(
        info,
        'rows',
      ),
    });
  },
};

exports.schema = schema;
exports.resolver = resolver;
