module.exports = [
  require('./reportTemplateFind'),
  require('./reportTemplateList'),
  require('./reportTemplateAutocomplete'),
];
