const OrganizationService = require('../../../services/organizationService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions').values;
const { GraphQLError } = require("graphql")

const schema = `
  organizationCreate(data: OrganizationInput!): Organization!
`;

const resolver = {
  organizationCreate: async (root, args, context) => {
    new PermissionChecker(context).validateHas(permissions.organizationCreate);
    
    const orgDoesExist = await new OrganizationService(context).doesOrganizationExist(args.data.name)

    if (orgDoesExist === false) {
       return new OrganizationService(context).create(args.data);
    } else {
      throw new GraphQLError("Organization already exists!")
    }
    
  },
};

exports.schema = schema;
exports.resolver = resolver;
