const OrganizationService = require('../../../services/organizationService');

const schema = `
  organizationCreateDefault(data: OrganizationDefaultInput!): Organization!
`;

const resolver = {
  organizationCreateDefault: async (root, args, context) => {
    return new OrganizationService(context).createDefault(args.data);
  },
};

exports.schema = schema;
exports.resolver = resolver;
