module.exports = [
  require('./organizationCreate'), //
  require('./organizationDefaultCreate'),
  require('./organizationDestroy'),
  require('./organizationUpdate'),
];
