const OrganizationService = require('../../../services/organizationService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions').values;

const schema = `
  organizationUpdate(id: String!, data: OrganizationInput!): Organization!
`;

const resolver = {
  organizationUpdate: async (root, args, context) => {
    new PermissionChecker(context).validateHas(permissions.organizationEdit);

    return new OrganizationService(context).update(args.id, args.data);
  },
};

exports.schema = schema;
exports.resolver = resolver;
