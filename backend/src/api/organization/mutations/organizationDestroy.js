const OrganizationService = require('../../../services/organizationService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions').values;

const schema = `
  organizationDestroy(ids: [String!]!): Boolean
`;

const resolver = {
  organizationDestroy: async (root, args, context) => {
    new PermissionChecker(context).validateHas(permissions.organizationDestroy);

    await new OrganizationService(context).destroyAll(args.ids);

    return true;
  },
};

exports.schema = schema;
exports.resolver = resolver;
