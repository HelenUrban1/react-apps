module.exports = [
  require('./organization'), //
  require('./organizationInput'),
  require('./organizationDefaultInput'),
  require('./organizationFilterInput'),
  require('./organizationOrderByEnum'),
  require('./organizationPage'),
  require('./organizationEnums'),
];
