const schema = `
  enum OrganizationOrderByEnum {
    id_ASC
    id_DESC
    status_ASC
    status_DESC
    createdAt_ASC
    createdAt_DESC
    name_ASC
    name_DESC
    phone_ASC
    phone_DESC
    email_ASC
    email_DESC
    city_ASC
    city_DESC
    region_ASC
    region_DESC
    postalCode_ASC
    postalCode_DESC
    country_ASC
    country_DESC
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
