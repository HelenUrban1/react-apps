const schema = `
  type Organization {
    id: String!
    status: OrganizationStatusEnum!
    type: OrganizationTypeEnum!
    settings: String
    name: String!
    phone: String
    email: String
    street1: String
    street2: String
    city: String
    region: OrganizationRegionEnum
    postalCode: String
    country: OrganizationCountryEnum
    orgLogo:[ File! ]
    parts: [ Part! ]
    reports: [ Report! ]
    templates:[ ReportTemplate! ]
    site: Site
    createdAt: DateTime
    updatedAt: DateTime
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
