const schema = `
  input OrganizationInput {
    status: OrganizationStatusEnum!
    type: OrganizationTypeEnum!
    settings: String
    name: String!
    phone: String
    email: String
    street1: String
    street2: String
    city: String
    region: OrganizationRegionEnum
    postalCode: String
    country: OrganizationCountryEnum
    orgLogo: FileInput
    parts: [ String ]
    reports: [ String ]
    templates:[ String ]
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
