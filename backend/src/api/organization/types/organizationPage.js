const schema = `
  type OrganizationPage {
    rows: [Organization!]!
    count: Int!
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
