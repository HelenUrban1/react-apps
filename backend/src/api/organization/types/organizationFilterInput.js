const schema = `
  input OrganizationFilterInput {
    id: [String]
    status: [OrganizationStatusEnum]
    type: OrganizationTypeEnum
    settings: String
    name: String
    phone: String
    email: String
    street1: String
    street2: String
    city: [String]
    parts: [String]
    region: [OrganizationRegionEnum]
    postalCode: String
    country: [OrganizationCountryEnum]
    site: String
    createdAtRange: [ DateTime ]
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
