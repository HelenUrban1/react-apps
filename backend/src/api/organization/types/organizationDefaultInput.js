const schema = `
  input OrganizationDefaultInput {
    accountId: String
    siteId: String
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
