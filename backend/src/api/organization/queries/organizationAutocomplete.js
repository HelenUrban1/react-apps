const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions').values;
const OrganizationService = require('../../../services/organizationService');

const schema = `
  organizationAutocomplete(query: String, limit: Int): [AutocompleteOption!]!
`;

const resolver = {
  organizationAutocomplete: async (root, args, context, info) => {
    new PermissionChecker(context).validateHas(permissions.organizationAutocomplete);

    return new OrganizationService(context).findAllAutocomplete(args.query, args.limit);
  },
};

exports.schema = schema;
exports.resolver = resolver;
