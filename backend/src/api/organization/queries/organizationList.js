const OrganizationService = require('../../../services/organizationService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions').values;
const graphqlSelectRequestedAttributes = require('../../shared/utils/graphqlSelectRequestedAttributes');

const schema = `
  organizationList(filter: OrganizationFilterInput, limit: Int, offset: Int, orderBy: OrganizationOrderByEnum): OrganizationPage!
`;

const resolver = {
  organizationList: async (root, args, context, info) => {
    new PermissionChecker(context).validateHas(permissions.organizationRead);

    return new OrganizationService(context).findAndCountAll({
      ...args,
      requestedAttributes: graphqlSelectRequestedAttributes(info, 'rows'),
    });
  },
};

exports.schema = schema;
exports.resolver = resolver;
