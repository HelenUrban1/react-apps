const OrganizationService = require('../../../services/organizationService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions').values;

const schema = `
  organizationFind(id: String!): Organization!
`;

const resolver = {
  organizationFind: async (root, args, context) => {
    new PermissionChecker(context).validateHas(permissions.organizationRead);

    return new OrganizationService(context).findById(args.id);
  },
};

exports.schema = schema;
exports.resolver = resolver;
