module.exports = [
  require('./accountMember'),
  require('./accountMemberInput'),
  require('./accountMemberFilterInput'),
  require('./accountMemberOrderByEnum'),
  require('./accountMemberPage'),
  require('./accountMemberEnums'),
];
