// The Owner role has the requirement of only being assigned
// to a single Account's member at a time. The AccountMemberNonOwnerRolesEnum
// is therefore used for some schemas because modifying the Owner
// role is handled in specific functions, and all other functions
// that modify roles should not allow setting of the Owner role.

const schema = `
  enum AccountMemberRolesEnum {
    Admin
    Billing
    Collaborator
    Owner
    Viewer
    Reviewer
    Metro
  }

  enum AccountMemberNonOwnerRolesEnum {
    Admin
    Billing
    Collaborator
    Viewer
    Reviewer
    Metro
  }

  enum AccountMemberStatusEnum {
    Active
    Pending
    Requesting
    Suspended
    Archived
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
