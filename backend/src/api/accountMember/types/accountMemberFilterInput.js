const schema = `
  input AccountMemberFilterInput {
    id: String
    account: String
    site: String
    user: String
    settings: String
    roles: [AccountMemberRolesEnum]
    status: AccountMemberStatusEnum
    createdAtRange: [ DateTime ]
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
