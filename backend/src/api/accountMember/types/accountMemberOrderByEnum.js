const schema = `
  enum AccountMemberOrderByEnum {
    id_ASC
    id_DESC
    settings_ASC
    settings_DESC
    roles_ASC
    roles_DESC
    status_ASC
    status_DESC
    createdAt_ASC
    createdAt_DESC
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
