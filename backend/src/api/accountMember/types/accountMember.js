const schema = `
  type AccountMember {
    id: String!
    account: Account
    site: Site
    user: User
    accountId: String
    userId: String
    settings: String
    roles: [AccountMemberRolesEnum]
    status: AccountMemberStatusEnum
    accessControl: Boolean
    createdAt: DateTime
    updatedAt: DateTime
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
