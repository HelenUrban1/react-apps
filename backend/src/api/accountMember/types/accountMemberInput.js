const schema = `
  input AccountMemberInput {
    account: String!
    user: String!
    settings: String!
    roles: [AccountMemberRolesEnum!]
    status: AccountMemberStatusEnum!
    accessControl: Boolean
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
