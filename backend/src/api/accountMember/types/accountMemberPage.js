const schema = `
  type AccountMemberPage {
    rows: [AccountMember!]!
    count: Int!
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
