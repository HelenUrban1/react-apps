const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions')
  .values;
const AccountMemberService = require('../../../services/accountMemberService');

const schema = `
  accountMemberAutocomplete(query: String, limit: Int): [AutocompleteOption!]!
`;

const resolver = {
  accountMemberAutocomplete: async (root, args, context, info) => {
    new PermissionChecker(context)
      .validateHas(permissions.accountMemberAutocomplete);

    return new AccountMemberService(context).findAllAutocomplete(
      args.query,
      args.limit,
    );
  },
};

exports.schema = schema;
exports.resolver = resolver;
