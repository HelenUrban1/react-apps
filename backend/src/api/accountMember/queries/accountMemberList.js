const AccountMemberService = require('../../../services/accountMemberService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions')
  .values;
const graphqlSelectRequestedAttributes = require('../../shared/utils/graphqlSelectRequestedAttributes');

const schema = `
  accountMemberList(filter: AccountMemberFilterInput, limit: Int, offset: Int, orderBy: AccountMemberOrderByEnum): AccountMemberPage!
`;

const resolver = {
  accountMemberList: async (root, args, context, info) => {
    new PermissionChecker(context)
      .validateHas(permissions.accountMemberRead);

    return new AccountMemberService(context).findAndCountAll({
      ...args,
      requestedAttributes: graphqlSelectRequestedAttributes(
        info,
        'rows',
      ),
    });
  },
};

exports.schema = schema;
exports.resolver = resolver;
