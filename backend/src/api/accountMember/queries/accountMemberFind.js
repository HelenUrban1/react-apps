const AccountMemberService = require('../../../services/accountMemberService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions')
  .values;

const schema = `
  accountMemberFind(id: String!): AccountMember!
`;

const resolver = {
  accountMemberFind: async (root, args, context) => {
    new PermissionChecker(context)
      .validateHas(permissions.accountMemberRead);

    return new AccountMemberService(context).findById(args.id);
  },
};

exports.schema = schema;
exports.resolver = resolver;
