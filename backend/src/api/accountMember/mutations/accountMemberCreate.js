const AccountMemberService = require('../../../services/accountMemberService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions')
  .values;

const schema = `
  accountMemberCreate(data: AccountMemberInput!): AccountMember!
`;

const resolver = {
  accountMemberCreate: async (root, args, context) => {
    new PermissionChecker(context)
      .validateHas(permissions.accountMemberCreate);

    return new AccountMemberService(context).create(
      args.data
    );
  },
};

exports.schema = schema;
exports.resolver = resolver;
