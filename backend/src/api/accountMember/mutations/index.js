module.exports = [
  require('./accountMemberCreate'),
  require('./accountMemberDestroy'),
  require('./accountMemberUpdate'),
  require('./accountMemberImport'),
];
