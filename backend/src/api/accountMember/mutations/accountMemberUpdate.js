const AccountMemberService = require('../../../services/accountMemberService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions')
  .values;

const schema = `
  accountMemberUpdate(id: String!, data: AccountMemberInput!): AccountMember!
`;

const resolver = {
  accountMemberUpdate: async (root, args, context) => {
    new PermissionChecker(context)
      .validateHas(permissions.accountMemberEdit);

    return new AccountMemberService(context).update(
      args.id,
      args.data
    );
  },
};

exports.schema = schema;
exports.resolver = resolver;
