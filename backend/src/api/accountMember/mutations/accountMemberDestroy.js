const AccountMemberService = require('../../../services/accountMemberService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions')
  .values;

const schema = `
  accountMemberDestroy(ids: [String!]!): Boolean
`;

const resolver = {
  accountMemberDestroy: async (root, args, context) => {
    new PermissionChecker(context)
      .validateHas(permissions.accountMemberDestroy);

    await new AccountMemberService(context).destroyAll(
      args.ids
    );

    return true;
  },
};

exports.schema = schema;
exports.resolver = resolver;
