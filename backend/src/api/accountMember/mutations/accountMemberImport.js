const AccountMemberService = require('../../../services/accountMemberService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions')
  .values;

const schema = `
  accountMemberImport(data: AccountMemberInput!, importHash: String!): Boolean
`;

const resolver = {
  accountMemberImport: async (root, args, context) => {
    new PermissionChecker(context)
      .validateHas(permissions.accountMemberImport);

    await new AccountMemberService(context).import(
      args.data,
      args.importHash
    );

    return true;
  },
};

exports.schema = schema;
exports.resolver = resolver;
