const MeasurementService = require('../../../services/measurementService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions').values;
const graphqlSelectRequestedAttributes = require('../../shared/utils/graphqlSelectRequestedAttributes');

const schema = `
  measurementList(filter: MeasurementFilterInput, limit: Int, offset: Int, orderBy: MeasurementOrderByEnum): MeasurementPage!
`;

const resolver = {
  measurementList: async (root, args, context, info) => {
    new PermissionChecker(context).validateHas(permissions.metro);

    return new MeasurementService(context).findAndCountAll({
      ...args,
      requestedAttributes: graphqlSelectRequestedAttributes(info, 'rows'),
    });
  },
};

exports.schema = schema;
exports.resolver = resolver;
