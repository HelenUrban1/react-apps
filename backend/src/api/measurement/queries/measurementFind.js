const MeasurementService = require('../../../services/measurementService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions').values;

const schema = `
  measurementFind(id: String!): Measurement!
`;

const resolver = {
  measurementFind: async (root, args, context) => {
    new PermissionChecker(context).validateHas(permissions.metro);
    return new MeasurementService(context).findById(args.id);
  },
};

exports.schema = schema;
exports.resolver = resolver;
