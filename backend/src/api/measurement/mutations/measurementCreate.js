const MeasurementService = require('../../../services/measurementService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions').values;

const schema = `
  measurementCreate(data: MeasurementInput!): Measurement!
`;

const resolver = {
  measurementCreate: async (root, args, context) => {
    new PermissionChecker(context).validateHas(permissions.metro);
    return new MeasurementService(context).create(args.data);
  },
};

exports.schema = schema;
exports.resolver = resolver;
