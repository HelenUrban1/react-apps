const MeasurementService = require('../../../services/measurementService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions').values;

const schema = `
  measurementImport(data: MeasurementInput!, importHash: String!): Boolean
`;

const resolver = {
  measurementImport: async (root, args, context) => {
    new PermissionChecker(context).validateHas(permissions.metro);
    await new MeasurementService(context).import(args.data, args.importHash);
    return true;
  },
};

exports.schema = schema;
exports.resolver = resolver;
