const MeasurementService = require('../../../services/measurementService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions').values;

const schema = `
  measurementUpdate(id: String!, data: MeasurementInput!): Measurement!
`;

const resolver = {
  measurementUpdate: async (root, args, context) => {
    new PermissionChecker(context).validateHas(permissions.metro);
    return new MeasurementService(context).update(args.id, args.data);
  },
};

exports.schema = schema;
exports.resolver = resolver;
