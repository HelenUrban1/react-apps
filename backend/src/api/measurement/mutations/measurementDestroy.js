const MeasurementService = require('../../../services/measurementService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions').values;

const schema = `
  measurementDestroy(ids: [String!]!): Boolean
`;

const resolver = {
  measurementDestroy: async (root, args, context) => {
    new PermissionChecker(context).validateHas(permissions.metro);

    await new MeasurementService(context).destroyAll(args.ids);

    return true;
  },
};

exports.schema = schema;
exports.resolver = resolver;
