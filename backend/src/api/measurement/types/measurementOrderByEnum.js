const schema = `
  enum MeasurementOrderByEnum {
    id_ASC
    id_DESC
    value_ASC
    value_DESC
    status_ASC
    status_DESC
    gage_ASC
    gage_DESC
    operation_ASC
    operation_DESC
    method_ASC
    method_DESC
    machine_ASC
    machine_DESC
    createdAt_ASC
    createdAt_DESC
    updatedAt_ASC
    updatedAt_DESC
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
