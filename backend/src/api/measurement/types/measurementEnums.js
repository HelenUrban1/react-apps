const schema = `
  enum MeasurementStatusEnum {
    Unmeasured
    Pass
    Fail
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
