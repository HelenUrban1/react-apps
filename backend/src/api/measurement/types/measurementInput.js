const schema = `
  input MeasurementInput {
    value: String!
    status: MeasurementStatusEnum!
    gage: String
    machine: String
    operationId: String
    methodId: String
    sampleId: String
    stationId: String
    characteristicId: String
    operatorId: String
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
