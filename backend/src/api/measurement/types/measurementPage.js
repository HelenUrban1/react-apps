const schema = `
  type MeasurementPage {
    rows: [Measurement!]!
    count: Int!
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
