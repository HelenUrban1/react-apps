const schema = `
  type Measurement {
    id: String!
    value: String!
    status: MeasurementStatusEnum!
    gage: String
    machine: String
    sampleId: String
    sample: Sample
    characteristicId: String
    characteristic: Characteristic
    operationId: String
    operation: ListItem
    methodId: String
    method: ListItem
    stationId: String
    station: Station
    createdById: String
    createdBy: Operator
    updatedById: String
    updatedBy: Operator
    createdAt: DateTime
    updatedAt: DateTime
    deletedAt: DateTime
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
