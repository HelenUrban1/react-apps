const schema = `
  input MeasurementFilterInput {
    id: String
    value: String
    status: MeasurementStatusEnum
    gage: String
    machine: String
    operationId: String
    methodId: String
    stationId: String
    characteristicId: String
    operatorId: String
    sampleId: String
    createdById: String
    updatedById: String
    createdAtRange: [ DateTime ]
    updatedAtRange: [ DateTime ]
    deletedAtRange: [ DateTime ]
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
