const schema = `
  input ListItemInput {
    name: String!
    metadata: String
    default: Boolean!
    status: String!
    listType: String!
    itemIndex: Int
    parentId: String
    siteId: String
    userId: String
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
