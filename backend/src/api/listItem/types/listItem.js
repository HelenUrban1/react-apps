const schema = `
  type ListItem {
    id: String!
    name: String!
    metadata: String
    default: Boolean!
    status: String
    listType: String
    itemIndex: Int
    parentId: String
    siteId: String
    userId: String
    createdAt: DateTime
    updatedAt: DateTime
    deletedAt: DateTime
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
