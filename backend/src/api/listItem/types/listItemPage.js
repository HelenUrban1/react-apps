const schema = `
  type ListItemPage {
    rows: [ListItem!]!
    count: Int!
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
