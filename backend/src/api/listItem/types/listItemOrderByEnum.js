const schema = `
  enum ListItemOrderByEnum {
    id_ASC
    id_DESC
    name_ASC
    name_DESC
    default_ASC
    default_DESC
    status_ASC
    status_DESC
    listType_ASC
    listType_DESC
    itemIndex_ASC
    itemIndex_DESC
    createdAt_ASC
    createdAt_DESC
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
