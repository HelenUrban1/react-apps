const schema = `
  input ListItemFilterInput {
    id: String
    name: String
    status: String
    listType: String
    parentId: String
    siteId: String
    userId: String
    createdAtRange: [ DateTime ]
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
