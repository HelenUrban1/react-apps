const ListItemService = require('../../../services/listItemService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions').values;

const schema = `
  listItemDestroy(ids: [String!]!): Boolean
`;

const resolver = {
  listItemDestroy: async (root, args, context) => {
    new PermissionChecker(context).validateHas(permissions.listItemEdit);

    await new ListItemService(context).destroyAll(args.ids);

    return true;
  },
};

exports.schema = schema;
exports.resolver = resolver;
