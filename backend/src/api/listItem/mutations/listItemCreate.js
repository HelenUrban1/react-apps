const ListItemService = require('../../../services/listItemService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions').values;

const schema = `
  listItemCreate(data: ListItemInput!): ListItem!
`;

const resolver = {
  listItemCreate: async (root, args, context) => {
    new PermissionChecker(context).validateHas(permissions.listItemEdit);

    return new ListItemService(context).create(args.data);
  },
};

exports.schema = schema;
exports.resolver = resolver;
