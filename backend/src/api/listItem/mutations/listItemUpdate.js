const ListItemService = require('../../../services/listItemService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions').values;

const schema = `
  listItemUpdate(id: String!, data: ListItemInput!): ListItem!
`;

const resolver = {
  listItemUpdate: async (root, args, context) => {
    new PermissionChecker(context).validateHas(permissions.listItemEdit);

    return new ListItemService(context).update(args.id, args.data);
  },
};

exports.schema = schema;
exports.resolver = resolver;
