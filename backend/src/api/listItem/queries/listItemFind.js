const ListItemService = require('../../../services/listItemService');

const schema = `
  listItemFindAllByType(type: String!): ListItem!
`;

const resolver = {
  listItemFindAllByType: async (root, args, context) => {
    return new ListItemService(context).findByType(args.type);
  },
};

exports.schema = schema;
exports.resolver = resolver;
