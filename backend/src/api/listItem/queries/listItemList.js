const ListItemService = require('../../../services/listItemService');
const graphqlSelectRequestedAttributes = require('../../shared/utils/graphqlSelectRequestedAttributes');

const schema = `
  listItemList(filter: ListItemFilterInput, limit: Int, offset: Int, orderBy: ListItemOrderByEnum, paranoid: Boolean): ListItemPage!
`;

const resolver = {
  listItemList: async (root, args, context, info) => {
    return new ListItemService(context).findAndCountAll({
      ...args,
      requestedAttributes: graphqlSelectRequestedAttributes(info, 'rows'),
    });
  },
};

exports.schema = schema;
exports.resolver = resolver;
