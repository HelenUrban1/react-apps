const schema = `
  type Operator {
    id: String!
    accessControl: String!
    email: String
    fullName: String!
    firstName: String!
    lastName: String!
    status: String!
    pin: String
    createdById: String!
    updatedById: String!
    createdAt: DateTime
    updatedAt: DateTime
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
