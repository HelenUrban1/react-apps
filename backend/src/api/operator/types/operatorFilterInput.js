const schema = `
  input OperatorFilterInput {
    id: String
    fullName: String
    firstName: String
    lastName: String
    email: String
    createdById: String
    createdAtRange: [ DateTime ]
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
