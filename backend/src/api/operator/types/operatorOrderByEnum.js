const schema = `
  enum OperatorOrderByEnum {
    id_ASC
    id_DESC
    name_ASC
    name_DESC
    disabled_ASC
    disabled_DESC
    isMaster_ASC
    isMaster_DESC
    createdAt_ASC
    createdAt_DESC
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
