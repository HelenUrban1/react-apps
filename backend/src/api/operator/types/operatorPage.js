const schema = `
  type OperatorPage {
    rows: [Operator!]!
    count: Int!
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
