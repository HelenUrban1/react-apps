const OperatorService = require('../../../services/operatorService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions').values;
const graphqlSelectRequestedAttributes = require('../../shared/utils/graphqlSelectRequestedAttributes');

const schema = `
  operatorList(filter: OperatorFilterInput, limit: Int, offset: Int, orderBy: OperatorOrderByEnum): OperatorPage!
`;

const resolver = {
  operatorList: async (root, args, context, info) => {
    new PermissionChecker(context).validateHas(permissions.metro);

    return new OperatorService(context).findAndCountAll({
      ...args,
      requestedAttributes: graphqlSelectRequestedAttributes(info, 'rows'),
    });
  },
};

exports.schema = schema;
exports.resolver = resolver;
