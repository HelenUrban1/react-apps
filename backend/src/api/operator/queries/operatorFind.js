const OperatorService = require('../../../services/operatorService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions').values;

const schema = `
  operatorFind(id: String!): Operator!
`;

const resolver = {
  operatorFind: async (root, args, context) => {
    new PermissionChecker(context).validateHas(permissions.metro);
    return new OperatorService(context).findById(args.id);
  },
};

exports.schema = schema;
exports.resolver = resolver;
