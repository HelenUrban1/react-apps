const OperatorService = require('../../../services/operatorService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions').values;

const schema = `
  operatorImport(data: OperatorInput!, importHash: String!): Boolean
`;

const resolver = {
  operatorImport: async (root, args, context) => {
    new PermissionChecker(context).validateHas(permissions.metro);
    await new OperatorService(context).import(args.data, args.importHash);
    return true;
  },
};

exports.schema = schema;
exports.resolver = resolver;
