const OperatorService = require('../../../services/operatorService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions').values;

const schema = `
  operatorCreate(data: OperatorInput!): Operator!
`;

const resolver = {
  operatorCreate: async (root, args, context) => {
    new PermissionChecker(context).validateHas(permissions.metro);
    return new OperatorService(context).create(args.data);
  },
};

exports.schema = schema;
exports.resolver = resolver;
