const OperatorService = require('../../../services/operatorService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions').values;

const schema = `
  operatorUpdate(id: String!, data: OperatorInput!): Operator!
`;

const resolver = {
  operatorUpdate: async (root, args, context) => {
    new PermissionChecker(context).validateHas(permissions.metro);
    return new OperatorService(context).update(args.id, args.data);
  },
};

exports.schema = schema;
exports.resolver = resolver;
