const supertest = require('supertest');
const express = require('express');

jest.doMock('../services/externalEventService');

const path = require('path');
const { PDFNet } = require('@pdftron/pdfnet-node');
const { defaultOperationIDs, defaultMethodIDs } = require('../staticData/defaults');
const config = require('../../config')();
const app = require('./index');
const { sqsPostForSendgridEvent } = require('../__fixtures__/externalEvents/sendgridEventFixture');
const ExternalEventService = require('../services/externalEventService');

describe('server = app.listen', () => {
  let server;
  let request;
  const VALID_ORIGIN = 'https://ixc-local.com:3000';

  beforeAll(async () => {
    server = app.listen();
    request = supertest.agent(server);
    app.use(express.json());
    await PDFNet.initialize(config.pdfTronSdkKey);
  });

  afterAll(async () => {
    server.close();
    PDFNet.shutdown();
    await new Promise((resolve) => setTimeout(() => resolve(), 500));

    // await models.sequelize.close();
    // Running only this test file will result in an open handles warning from Jest
    // https://github.com/visionmedia/supertest/issues/520
  });

  it('GET invalid route should respond 404', async () => {
    const response = await request.set('Origin', VALID_ORIGIN).get('/kasjdflasjhdfkj');
    expect(response.status).toEqual(404);
  });

  describe('API Metadata Endpoints', () => {
    it('GET /api/health should indicate status of server', async () => {
      const response = await request.set('Origin', VALID_ORIGIN).get('/api/health');
      expect(response.status).toEqual(200);
    });

    it('GET /api/config should return config settings', async () => {
      const response = await request.set('Origin', VALID_ORIGIN).get('/api/config');
      expect(response.status).toEqual(200);
      expect(response.body.env).toEqual('test');
      expect(response.body.buildDatetime).toBeDefined();
      expect(response.body.hostname).toBeDefined();
      expect(response.body.package).toBeDefined();
      expect(response.body.package.name).toBeDefined();
      expect(response.body.package.version).toBeDefined();
      expect(response.body.scm).toBeDefined();
      expect(response.body.scm.tag).toBeDefined();
      expect(response.body.scm.branch).toBeDefined();
      expect(response.body.scm.commit).toBeDefined();
      // config.env !== 'production'
      expect(response.body.database).toBeDefined();
      expect(response.body.clientUrl).toBeDefined();
      expect(response.body.email).toBeDefined();
      expect(response.body.awsRegion).toBeDefined();
    });
  });

  describe('Reports', () => {
    const mergeData = {
      customer: {
        name: 'Fletch F. Fletcher',
      },
      part: {
        name: 'Fetzer Valve',
        number: '20200101-0001',
        revision: 'B',
      },
      drawing: {
        documentcontrol: "Boyd Aviation assembly specifications for Mr. Stanwyck's plane.",
        name: 'Fetzer Valve Assembly Detail',
        number: '20200101-0001-B-3',
        revision: '3',
      },
      characteristics: [
        {
          label: '1',
          type: 'Note',
          capturedvalue: 'ANODIZE PER GH52-1',
          nominal: '',
          plustolerance: '',
          minustolerance: '',
          upperspeclimit: '',
          lowerspeclimit: '',
          unitofmeasurement: '',
          classification: '',
          inspectionmethod: null,
          operation: null,
          notes: 'LOWER-PLATE-C4 pg.1, Zone 1.A',
          isverified: '',
        },
        {
          label: '2',
          type: 'Note',
          capturedvalue: 'BREAK ALL SHARP EDGES TO .02',
          nominal: '',
          plustolerance: '',
          minustolerance: '',
          upperspeclimit: ' ',
          lowerspeclimit: ' ',
          unitofmeasurement: ' ',
          classification: '',
          inspectionmethod: null,
          operation: null,
          notes: 'LOWER-PLATE-C4 pg.1, Zone 1.A',
          isverified: '',
        },
        {
          label: '3',
          type: 'Note',
          capturedvalue: 'INTERPRET PER ASME Y14.5',
          nominal: '',
          plustolerance: '',
          minustolerance: '',
          upperspeclimit: ' ',
          lowerspeclimit: ' ',
          unitofmeasurement: ' ',
          classification: '',
          inspectionmethod: null,
          operation: null,
          notes: 'LOWER-PLATE-C4 pg.1, Zone 1.A',
          isverified: '',
        },
        {
          label: '4',
          type: 'Length',
          capturedvalue: '.325±.020',
          nominal: '0.325',
          plustolerance: '0.02',
          minustolerance: '0.02',
          upperspeclimit: '0.345',
          lowerspeclimit: '0.305',
          unitofmeasurement: '',
          classification: '',
          inspectionmethod: defaultMethodIDs.CMM, // CMM
          operation: null,
          notes: 'LOWER-PLATE-C4 pg.1, Zone 2.A',
          isverified: '',
        },
        {
          label: '5',
          type: 'Length',
          capturedvalue: '.618',
          nominal: '.618',
          plustolerance: '0.02',
          minustolerance: '0.02',
          upperspeclimit: '0.638',
          lowerspeclimit: '0.598',
          unitofmeasurement: 'in',
          classification: 'Basic',
          inspectionmethod: defaultMethodIDs.CMM, // CMM
          operation: null,
          notes: 'LOWER-PLATE-C4 pg.1, Zone 2.A',
          isverified: '',
        },
        {
          label: '6',
          type: 'Length',
          capturedvalue: '.680',
          nominal: '.680',
          plustolerance: '0.0199999999999999',
          minustolerance: '0.02',
          upperspeclimit: '0.7',
          lowerspeclimit: '0.66',
          unitofmeasurement: 'in',
          classification: 'Basic',
          inspectionmethod: defaultMethodIDs.CMM, // CMM
          operation: null,
          notes: 'LOWER-PLATE-C4 pg.1, Zone 2.A',
          isverified: '',
        },
        {
          label: '7',
          type: 'Length',
          capturedvalue: '.750',
          nominal: '.750',
          plustolerance: '0.02',
          minustolerance: '0.02',
          upperspeclimit: '0.77',
          lowerspeclimit: '0.73',
          unitofmeasurement: 'in',
          classification: 'Basic',
          inspectionmethod: defaultMethodIDs.CMM, // CMM
          operation: null,
          notes: 'LOWER-PLATE-C4 pg.1, Zone 3.B',
          isverified: '',
        },
        {
          label: '8',
          type: 'Profile of a Surface',
          capturedvalue: '',
          nominal: '',
          plustolerance: '',
          minustolerance: '',
          upperspeclimit: '0.01',
          lowerspeclimit: '-0.01',
          unitofmeasurement: 'in',
          classification: '',
          inspectionmethod: defaultMethodIDs.CMM, // CMM
          operation: null,
          notes: 'LOWER-PLATE-C4 pg.1, Zone 2.C',
          isverified: '',
        },
        {
          label: '9.1',
          type: 'Radius',
          capturedvalue: 'R.125',
          nominal: '.125',
          plustolerance: '0.005',
          minustolerance: '0.005',
          upperspeclimit: '0.13',
          lowerspeclimit: '0.12',
          unitofmeasurement: 'in',
          classification: '',
          inspectionmethod: defaultMethodIDs.CMM, // CMM
          operation: defaultOperationIDs.Drilling, // Drilling
          notes: 'LOWER-PLATE-C4 pg.1, Zone 3.D',
          isverified: '',
        },
        {
          label: '9.2',
          type: 'Radius',
          capturedvalue: 'R.125',
          nominal: '.125',
          plustolerance: '0.005',
          minustolerance: '0.005',
          upperspeclimit: '0.13',
          lowerspeclimit: '0.12',
          unitofmeasurement: 'in',
          classification: 'Reference',
          inspectionmethod: defaultMethodIDs.CMM, // CMM
          operation: defaultOperationIDs.Drilling, // Drilling
          notes: 'LOWER-PLATE-C4 pg.1, Zone 3.D',
          isverified: '',
        },
        {
          label: '10',
          type: 'Perpendicularity',
          capturedvalue: '',
          nominal: '',
          plustolerance: '',
          minustolerance: '',
          upperspeclimit: '0.01',
          lowerspeclimit: '',
          unitofmeasurement: 'in',
          classification: '',
          inspectionmethod: defaultMethodIDs.CMM, // CMM
          operation: null,
          notes: 'LOWER-PLATE-C4 pg.1, Zone 3.D',
          isverified: '',
        },
        {
          label: '11',
          type: 'Flatness',
          capturedvalue: '',
          nominal: '',
          plustolerance: '',
          minustolerance: '',
          upperspeclimit: '0.002',
          lowerspeclimit: '',
          unitofmeasurement: 'in',
          classification: '',
          inspectionmethod: defaultMethodIDs.CMM, // CMM
          operation: null,
          notes: 'LOWER-PLATE-C4 pg.1, Zone 3.E',
          isverified: '',
        },
        {
          label: '12',
          type: 'Length',
          capturedvalue: '.250',
          nominal: '.250',
          plustolerance: '0.02',
          minustolerance: '0.02',
          upperspeclimit: '0.27',
          lowerspeclimit: '0.23',
          unitofmeasurement: 'in',
          classification: '',
          inspectionmethod: defaultMethodIDs.CMM, // CMM
          operation: null,
          notes: 'LOWER-PLATE-C4 pg.1, Zone 4.E',
          isverified: '',
        },
        {
          label: '13',
          type: 'Length',
          capturedvalue: '2.875',
          nominal: '2.875',
          plustolerance: '0.02',
          minustolerance: '0.02',
          upperspeclimit: '2.895',
          lowerspeclimit: '2.855',
          unitofmeasurement: 'in',
          classification: '',
          inspectionmethod: defaultMethodIDs.CMM, // CMM
          operation: null,
          notes: 'LOWER-PLATE-C4 pg.1, Zone 3.C',
          isverified: '',
        },
        {
          label: '14',
          type: 'Angle',
          capturedvalue: '3.206',
          nominal: '3.206',
          plustolerance: '0.00300000000000011',
          minustolerance: '0.004',
          upperspeclimit: '3.209',
          lowerspeclimit: '3.202',
          unitofmeasurement: 'deg',
          classification: 'Basic',
          inspectionmethod: defaultMethodIDs.CMM, // CMM
          operation: null,
          notes: 'LOWER-PLATE-C4 pg.1, Zone 4.C',
          isverified: '',
        },
        {
          label: '15',
          type: 'Length',
          capturedvalue: '3.503 / 3.496',
          nominal: '',
          plustolerance: '',
          minustolerance: '',
          upperspeclimit: '3.503',
          lowerspeclimit: '3.496',
          unitofmeasurement: 'in',
          classification: '',
          inspectionmethod: defaultMethodIDs['Gage Pins'], // Gage Pins
          operation: null,
          notes: 'LOWER-PLATE-C4 pg.1, Zone 4.B',
          isverified: '',
        },
        {
          label: '16',
          type: 'Perpendicularity',
          capturedvalue: '',
          nominal: '',
          plustolerance: '',
          minustolerance: '',
          upperspeclimit: '0.005',
          lowerspeclimit: '',
          unitofmeasurement: 'in',
          classification: '',
          inspectionmethod: defaultMethodIDs['Gage Pins'], // Gage Pins
          operation: null,
          notes: 'LOWER-PLATE-C4 pg.1, Zone 4.A',
          isverified: '',
        },
        {
          label: '17.1',
          type: 'Diameter',
          capturedvalue: 'Ø.281',
          nominal: '0.281',
          plustolerance: '0.00499999999999995',
          minustolerance: '0.005',
          upperspeclimit: '0.286',
          lowerspeclimit: '0.276',
          unitofmeasurement: 'in',
          classification: '',
          inspectionmethod: defaultMethodIDs['Gage Pins'], // Gage Pins
          operation: defaultOperationIDs.Drilling, // Drilling
          notes: 'LOWER-PLATE-C4 pg.2, Zone 2.C',
          isverified: '',
        },
        {
          label: '17.2',
          type: 'Position',
          capturedvalue: '',
          nominal: '',
          plustolerance: '',
          minustolerance: '',
          upperspeclimit: '0.02',
          lowerspeclimit: '',
          unitofmeasurement: 'in',
          classification: '',
          inspectionmethod: defaultMethodIDs['Gage Pins'], // Gage Pins
          operation: null,
          notes: 'LOWER-PLATE-C4 pg.2, Zone 2.C',
          isverified: '',
        },
        {
          label: '18.1',
          type: 'Counterbore Diameter',
          capturedvalue: '.406',
          nominal: '.406',
          plustolerance: '0.00499999999999995',
          minustolerance: '0.005',
          upperspeclimit: '0.411',
          lowerspeclimit: '0.401',
          unitofmeasurement: 'in',
          classification: 'Basic',
          inspectionmethod: defaultMethodIDs['Gage Pins'], // Gage Pins
          operation: defaultMethodIDs.Boring, // Boring
          notes: 'LOWER-PLATE-C4 pg.2, Zone 3.C',
          isverified: '',
        },
        {
          label: '18.2',
          type: 'Counterbore Depth',
          capturedvalue: '.156',
          nominal: '.156',
          plustolerance: '0.005',
          minustolerance: '0.005',
          upperspeclimit: '0.161',
          lowerspeclimit: '0.151',
          unitofmeasurement: 'in',
          classification: 'Basic',
          inspectionmethod: defaultMethodIDs['Gage Pins'], // Gage Pins
          operation: defaultMethodIDs.Boring, // Boring
          notes: 'LOWER-PLATE-C4 pg.2, Zone 3.D',
          isverified: '',
        },
        {
          label: '18.3',
          type: 'Position',
          capturedvalue: '',
          nominal: '',
          plustolerance: '',
          minustolerance: '',
          upperspeclimit: '0.02',
          lowerspeclimit: '',
          unitofmeasurement: 'in',
          classification: '',
          inspectionmethod: defaultMethodIDs['Gage Pins'], // Gage Pins
          operation: defaultOperationIDs.Drilling, // Drilling
          notes: 'LOWER-PLATE-C4 pg.2, Zone 3.D',
          isverified: '',
        },
        {
          label: '19.1',
          type: 'Diameter',
          capturedvalue: 'Ø.125',
          nominal: '.125',
          plustolerance: '0.005',
          minustolerance: '0.005',
          upperspeclimit: '0.13',
          lowerspeclimit: '0.12',
          unitofmeasurement: 'in',
          classification: 'Basic',
          inspectionmethod: defaultMethodIDs['Gage Pins'], // Gage Pins
          operation: defaultOperationIDs.Drilling, // Drilling
          notes: 'LOWER-PLATE-C4 pg.2, Zone 4.A',
          isverified: '',
        },
        {
          label: '19.2',
          type: 'Position',
          capturedvalue: '',
          nominal: '',
          plustolerance: '',
          minustolerance: '',
          upperspeclimit: '0.02',
          lowerspeclimit: '',
          unitofmeasurement: 'in',
          classification: '',
          inspectionmethod: defaultMethodIDs['Gage Pins'], // Gage Pins
          operation: defaultOperationIDs.Drilling, // Drilling
          notes: 'LOWER-PLATE-C4 pg.2, Zone 4.A',
          isverified: '',
        },
        {
          label: '20.1',
          type: 'Diameter',
          capturedvalue: 'Ø.125',
          nominal: '.125',
          plustolerance: '0.005',
          minustolerance: '0.005',
          upperspeclimit: '0.13',
          lowerspeclimit: '0.12',
          unitofmeasurement: 'in',
          classification: 'Basic',
          inspectionmethod: defaultMethodIDs['Gage Pins'], // Gage Pins
          operation: defaultOperationIDs.Drilling, // Drilling
          notes: 'LOWER-PLATE-C4 pg.2, Zone 4.A',
          isverified: '',
        },
        {
          label: '20.2',
          type: 'Position',
          capturedvalue: '',
          nominal: '',
          plustolerance: '',
          minustolerance: '',
          upperspeclimit: '0.02',
          lowerspeclimit: '',
          unitofmeasurement: 'in',
          classification: '',
          inspectionmethod: defaultMethodIDs['Gage Pins'], // Gage Pins
          operation: defaultOperationIDs.Drilling, // Drilling
          notes: 'LOWER-PLATE-C4 pg.2, Zone 4.A',
          isverified: '',
        },
        {
          label: '21.1',
          type: 'Diameter',
          capturedvalue: 'Ø.125',
          nominal: '.125',
          plustolerance: '0.005',
          minustolerance: '0.005',
          upperspeclimit: '0.13',
          lowerspeclimit: '0.12',
          unitofmeasurement: 'in',
          classification: 'Reference',
          inspectionmethod: defaultMethodIDs['Gage Pins'], // Gage Pins
          operation: defaultOperationIDs.Drilling, // Drilling
          notes: 'LOWER-PLATE-C4 pg.2, Zone 4.A',
          isverified: '',
        },
        {
          label: '21.2',
          type: 'Position',
          capturedvalue: '',
          nominal: '',
          plustolerance: '',
          minustolerance: '',
          upperspeclimit: '0.02',
          lowerspeclimit: '',
          unitofmeasurement: 'in',
          classification: '',
          inspectionmethod: defaultMethodIDs['Gage Pins'], // Gage Pins
          operation: defaultOperationIDs.Drilling, // Drilling
          notes: 'LOWER-PLATE-C4 pg.2, Zone 4.A',
          isverified: '',
        },
      ],
    };
    const filename = `${mergeData.part.number}${mergeData.part.revision}_all-ix-files.zip`;
    const drawings = [
      {
        name: 'Graytech',
        url: 'Graytech Size D Drawing (1).pdf',
      },
      {
        name: 'Graytech2',
        url: 'Graytech Size D Drawing (1).pdf',
      },
    ];
    const reports = [
      {
        title: 'Alpha Demo Report',
        url: 'SeedReport.xlsx',
      },
      {
        title: 'Beta Demo Report',
        url: 'SeedReport.xlsx',
      },
      {
        title: 'Delta Demo Report',
        url: 'SeedReport.xlsx',
      },
      {
        title: 'Named Ranges Report',
        url: 'SeedReport-NamedRanges.xlsx',
      },
    ];
    const user = {
      accountId: 'e0a01769-c8ed-4090-bd24-9c1b2241fb75',
      siteId: '31ef1d56-1b86-4ffa-b886-507ed8f021c5',
    };
    const partId = 'f516034d-897c-493e-9964-03700f5346f1';
    const drawingURL = 'Graytech Size D Drawing (1).pdf';

    describe('/excel', () => {
      it('returns an xlsx report', async () => {
        const response = await request.set('Origin', VALID_ORIGIN).post('/api/excel').set('account', user.accountId).set('site', user.siteId).send({
          privateUrl: reports[0].url,
          type: 'xlsx',
          font: false,
          unframed: false,
        });
        expect(response.status).toEqual(200);
        expect(response.headers['content-type']).toEqual('application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        expect(response.headers['content-disposition']).toEqual(`attachment; filename=SeedReport.xlsx`);
      });

      it('returns a csv report', async () => {
        const response = await request.set('Origin', VALID_ORIGIN).post('/api/excel').set('account', user.accountId).set('site', user.siteId).send({
          privateUrl: reports[0].url,
          type: 'csv',
          font: false,
          unframed: false,
        });

        expect(response.status).toEqual(200);
        expect(response.headers['content-type']).toEqual('application/x-zip-compressed');
        expect(response.headers['content-disposition']).toEqual(`attachment; filename=SeedReport.zip`);
      });

      it('returns an xlsx report with font', async () => {
        const response = await request.set('Origin', VALID_ORIGIN).post('/api/excel').set('account', user.accountId).set('site', user.siteId).send({
          privateUrl: reports[0].url,
          type: 'xlsx',
          font: true,
          unframed: false,
        });
        expect(response.status).toEqual(200);
        expect(response.headers['content-type']).toEqual('application/x-zip-compressed');
        expect(response.headers['content-disposition']).toEqual(`attachment; filename=SeedReport.zip`);
      });

      it('returns a csv report with font', async () => {
        const response = await request.set('Origin', VALID_ORIGIN).post('/api/excel').set('account', user.accountId).set('site', user.siteId).send({
          privateUrl: reports[0].url,
          type: 'csv',
          font: false,
          unframed: false,
        });
        expect(response.status).toEqual(200);
        expect(response.headers['content-type']).toEqual('application/x-zip-compressed');
        expect(response.headers['content-disposition']).toEqual(`attachment; filename=SeedReport.zip`);
      });

      it('returns a json report when no type is provided', async () => {
        const response = await request.set('Origin', VALID_ORIGIN).post('/api/excel').set('account', user.accountId).set('site', user.siteId).send({
          privateUrl: reports[0].url,
          type: null,
          font: null,
          unframed: null,
        });
        expect(response.status).toEqual(200);
        expect(response.headers['content-type']).toEqual('application/json; charset=utf-8');
        expect(response.body.data[0].name).toEqual('Sheet1');
        expect(response.body.unsupportedFeatures).toEqual(null);
      });

      it('returns unsupported features value when file contains named ranges', async () => {
        const response = await request.set('Origin', VALID_ORIGIN).post('/api/excel').set('account', user.accountId).set('site', user.siteId).send({
          privateUrl: reports[3].url,
          type: null,
          font: null,
          unframed: null,
        });
        expect(response.status).toEqual(200);
        expect(response.headers['content-type']).toEqual('application/json; charset=utf-8');
        expect(response.body.data[0].name).toEqual('Sheet1');
        expect(response.body.unsupportedFeatures).toEqual('Named Ranges');
      });
    });

    describe('/report', () => {
      it('responds with error if not passed data', async () => {
        const response = await request.set('Origin', VALID_ORIGIN).post('/api/report').set('account', user.accountId).set('site', user.siteId).send();
        expect(response.status).toEqual(400);
      });

      it('returns a zip of all documents', async () => {
        const response = await request.set('Origin', VALID_ORIGIN).post('/api/report').set('account', user.accountId).set('site', user.siteId).send({
          partId,
          filename,
          drawingURL,
          drawings,
          reports,
        });
        expect(response.status).toEqual(200);
        expect(response.headers['content-type']).toEqual('application/x-zip-compressed');
        expect(response.headers['content-disposition']).toEqual(`attachment; filename=${filename}`);
      });
    });

    describe('/template', () => {
      it('returns a data array for handsontable and url for file', async () => {
        const response = await request.set('Origin', VALID_ORIGIN).post('/api/template').send({
          data: mergeData,
          direction: 'Vertical',
          privateUrl: '/templates/All Data.xlsx',
          reportFile: 'testFile',
          templateSettings:
            '{"saved": true,"direction":"Vertical", "footers":{"0":{ "B12":{"row":"11", "col":"1", "footer":"36"},"C12":{"row":"11", "col":"2", "footer":"36"},"D12":{"row":"11", "col":"3", "footer":"36"},"E12":{"row":"11", "col":"4", "footer":"36"},"F12":{"row":"11", "col":"5", "footer":"36"},"G12":{"row":"11", "col":"6", "footer":"36"},"H12":{"row":"11", "col":"7", "footer":"36"},"I12":{"row":"11", "col":"8", "footer":"36"},"J12":{"row":"11", "col":"9", "footer":"36"},"K12":{"row":"11", "col":"10", "footer":"36"},"L12":{"row":"11", "col":"11", "footer":"36"},"M12":{"row":"11", "col":"12", "footer":"36"},"N12":{"row":"11", "col":"13", "footer":"36"},"O12":{"row":"11", "col":"14", "footer":"36"},"P12":{"row":"11", "col":"15", "footer":"36"},"Q12":{"row":"11", "col":"16", "footer":"36"},"R12":{"row":"11", "col":"17", "footer":"36"},"S12":{"row":"11", "col":"18", "footer":"36"},"T12":{"row":"11", "col":"19", "footer":"36"},"U12":{"row":"11", "col":"20", "footer":"36"},"V12":{"row":"11", "col":"21", "footer":"36"}}}}',
        });
        expect(response.status).toEqual(200);
        expect(response.body.local).toEqual(true);
        expect(response.body.data).toHaveLength(1);
        expect(response.body.data[0]).toHaveProperty('name');
        expect(response.body.data[0]).toHaveProperty('orientation');
        expect(response.body.data[0]).toHaveProperty('columns');
        expect(response.body.data[0].columns.length).toBeGreaterThan(1);
        expect(response.body.data[0]).toHaveProperty('merges');
        expect(response.body.data[0].merges.length).toBeGreaterThan(1);
        expect(response.body.data[0]).toHaveProperty('rows');
        expect(response.body.data[0].rows.length).toBeGreaterThan(1);
        expect(response.body.data[0]).toHaveProperty('theme');
        expect(response.body.data[0]).toHaveProperty('showGrid');
        expect(response.body.report).toEqual(path.join(config.uploadDir, 'testFile'));
      });

      it('uploads to user folders if user is set', async () => {
        const response = await request.set('Origin', VALID_ORIGIN).post('/api/template').send({
          data: mergeData,
          direction: 'Vertical',
          privateUrl: '/templates/All Data.xlsx',
          reportFile: 'testFile',
          user,
          templateSettings:
            '{"saved": true,"direction":"Vertical", "footers":{"0":{ "B12":{"row":"11", "col":"1", "footer":"36"},"C12":{"row":"11", "col":"2", "footer":"36"},"D12":{"row":"11", "col":"3", "footer":"36"},"E12":{"row":"11", "col":"4", "footer":"36"},"F12":{"row":"11", "col":"5", "footer":"36"},"G12":{"row":"11", "col":"6", "footer":"36"},"H12":{"row":"11", "col":"7", "footer":"36"},"I12":{"row":"11", "col":"8", "footer":"36"},"J12":{"row":"11", "col":"9", "footer":"36"},"K12":{"row":"11", "col":"10", "footer":"36"},"L12":{"row":"11", "col":"11", "footer":"36"},"M12":{"row":"11", "col":"12", "footer":"36"},"N12":{"row":"11", "col":"13", "footer":"36"},"O12":{"row":"11", "col":"14", "footer":"36"},"P12":{"row":"11", "col":"15", "footer":"36"},"Q12":{"row":"11", "col":"16", "footer":"36"},"R12":{"row":"11", "col":"17", "footer":"36"},"S12":{"row":"11", "col":"18", "footer":"36"},"T12":{"row":"11", "col":"19", "footer":"36"},"U12":{"row":"11", "col":"20", "footer":"36"},"V12":{"row":"11", "col":"21", "footer":"36"}}}}',
        });
        expect(response.status).toEqual(200);
        expect(response.body.local).toEqual(true);
        expect(response.body.data).toHaveLength(1);
        expect(response.body.data[0]).toHaveProperty('name');
        expect(response.body.data[0]).toHaveProperty('orientation');
        expect(response.body.data[0]).toHaveProperty('columns');
        expect(response.body.data[0].columns.length).toBeGreaterThan(1);
        expect(response.body.data[0]).toHaveProperty('merges');
        expect(response.body.data[0].merges.length).toBeGreaterThan(1);
        expect(response.body.data[0]).toHaveProperty('rows');
        expect(response.body.data[0].rows.length).toBeGreaterThan(1);
        expect(response.body.data[0]).toHaveProperty('theme');
        expect(response.body.data[0]).toHaveProperty('showGrid');
        expect(response.body.report).toEqual(path.join(config.uploadDir, user.accountId, user.siteId, 'testFile'));
      });

      it('uploads remotely', async () => {
        config.uploadRemote = true;
        const response = await request.set('Origin', VALID_ORIGIN).post('/api/template').send({
          data: mergeData,
          direction: 'Vertical',
          privateUrl: '/templates/All Data.xlsx',
          reportFile: 'testFile',
          user,
          templateSettings:
            '{"saved": true,"direction":"Vertical", "footers":{"0":{ "B12":{"row":"11", "col":"1", "footer":"36"},"C12":{"row":"11", "col":"2", "footer":"36"},"D12":{"row":"11", "col":"3", "footer":"36"},"E12":{"row":"11", "col":"4", "footer":"36"},"F12":{"row":"11", "col":"5", "footer":"36"},"G12":{"row":"11", "col":"6", "footer":"36"},"H12":{"row":"11", "col":"7", "footer":"36"},"I12":{"row":"11", "col":"8", "footer":"36"},"J12":{"row":"11", "col":"9", "footer":"36"},"K12":{"row":"11", "col":"10", "footer":"36"},"L12":{"row":"11", "col":"11", "footer":"36"},"M12":{"row":"11", "col":"12", "footer":"36"},"N12":{"row":"11", "col":"13", "footer":"36"},"O12":{"row":"11", "col":"14", "footer":"36"},"P12":{"row":"11", "col":"15", "footer":"36"},"Q12":{"row":"11", "col":"16", "footer":"36"},"R12":{"row":"11", "col":"17", "footer":"36"},"S12":{"row":"11", "col":"18", "footer":"36"},"T12":{"row":"11", "col":"19", "footer":"36"},"U12":{"row":"11", "col":"20", "footer":"36"},"V12":{"row":"11", "col":"21", "footer":"36"}}}}',
        });
        expect(response.status).toEqual(200);
        expect(response.body.local).toEqual(false);
        expect(response.body.data).toHaveLength(1);
        expect(response.body.data[0]).toHaveProperty('name');
        expect(response.body.data[0]).toHaveProperty('orientation');
        expect(response.body.data[0]).toHaveProperty('columns');
        expect(response.body.data[0].columns.length).toBeGreaterThan(1);
        expect(response.body.data[0]).toHaveProperty('merges');
        expect(response.body.data[0].merges.length).toBeGreaterThan(1);
        expect(response.body.data[0]).toHaveProperty('rows');
        expect(response.body.data[0].rows.length).toBeGreaterThan(1);
        expect(response.body.data[0]).toHaveProperty('theme');
        expect(response.body.data[0]).toHaveProperty('showGrid');
        expect(response.body.report).toEqual('testFile');
        config.uploadRemote = false;
      });
    });

    describe('/export', () => {
      it('responds with error if not passed data', async () => {
        const response = await request.set('Origin', VALID_ORIGIN).post('/api/export').set('account', user.accountId).set('site', user.siteId).send();
        expect(response.status).toEqual(400);
      });

      it('returns a zip of xml and json part', async () => {
        const response = await request.set('Origin', VALID_ORIGIN).post('/api/export').set('account', user.accountId).set('site', user.siteId).send({
          partId,
          filename,
        });
        expect(response.status).toEqual(200);
        expect(response.headers['content-type']).toEqual('application/x-zip-compressed');
        expect(response.headers['content-disposition']).toEqual(`attachment; filename=${filename}`);
      });
    });
  });

  describe('ReviewTask Status', () => {
    it('GET /api/reviewtask/status should return error without age parameter', async () => {
      const response = await request.set('Origin', VALID_ORIGIN).get('/api/reviewtask/status');
      expect(response.status).toEqual(400);
      expect(response.text).toEqual('Error getting review task info.');
    });

    it('GET /api/reviewtask/status should return error if age parameter is blank', async () => {
      const response = await request.set('Origin', VALID_ORIGIN).get('/api/reviewtask/status?age=');
      expect(response.status).toEqual(400);
      expect(response.text).toEqual('Error getting review task info.');
    });

    it('GET /api/reviewtask/status should return error if age parameter is not a number', async () => {
      const response = await request.set('Origin', VALID_ORIGIN).get('/api/reviewtask/status?age=a');
      expect(response.status).toEqual(400);
      expect(response.text).toEqual('Error getting review task info.');
    });

    it('GET /api/reviewtask/status should return message if age parameter is valid', async () => {
      const response = await request.set('Origin', VALID_ORIGIN).get('/api/reviewtask/status?age=10');
      expect(response.status).toEqual(200);
      expect(response.text).toContain('review tasks over 10 minutes old');
    });
  });

  describe('External Event Delivery', () => {
    beforeEach(async () => {
      jest.resetAllMocks();
    });

    it('POST /api/event should return 501 not implemented if a handler for the webhook source is not defined', async () => {
      ExternalEventService.processEvent.mockResolvedValue({
        status: 501,
        message: `No handler matched the event type FOO`,
      });
      const response = await request
        .set(sqsPostForSendgridEvent.headers)
        .set('Origin', VALID_ORIGIN)
        .post('/api/event')
        .send({
          event: {
            data: [{ email: 'dev.mail.monkey@gmail.com' }],
            attributes: {
              webhookSource: 'FOO',
            },
          },
        });
      expect(ExternalEventService.processEvent).toBeCalledTimes(1);
      expect(response.status).toEqual(501);
      expect(response.body.message).toEqual(`No handler matched the event type FOO`);
    });

    describe('SendGrid Event Handling', () => {
      it('POST /api/event should return 200 success when all events are posted successfuly', async () => {
        ExternalEventService.processEvent.mockResolvedValue({
          results: sqsPostForSendgridEvent.expectedResultForSqsPostForSendgridEvent,
          message: `Processed ${sqsPostForSendgridEvent.body.data.length} of ${sqsPostForSendgridEvent.body.data.length} SendGrid events successfully`,
        });

        const response = await request.set(sqsPostForSendgridEvent.headers).set('Origin', VALID_ORIGIN).post('/api/event').send(sqsPostForSendgridEvent.body);
        expect(ExternalEventService.processEvent).toBeCalledTimes(1);
        expect(response.status).toEqual(200);
        expect(response.body.message).toEqual('Processed 11 of 11 SendGrid events successfully');
      });

      it('POST /api/event should return 200 when some events fail', async () => {
        ExternalEventService.processEvent.mockResolvedValue({
          message: 'Processed 0 of 1 SendGrid events successfully',
          results: [
            {
              success: false,
              sendGridEvent: {
                email: 'FOO@gmail.com',
                timestamp: 1626317952,
                'smtp-id': '<14c5d75ce93.dfd.64b469@ismtpd-555>',
                event: 'processed',
                category: ['cat facts'],
                sg_event_id: 'Maehyp6M7Gj15MlndO7lxg==',
                sg_message_id: '14c5d75ce93.dfd.64b469.filter0001.16648.5515E0B88.0',
              },
              error: {
                level: '\u001b[31m\u001b[1merror\u001b[22m\u001b[39m',
                label: 'SERVER',
                timestamp: '2021-07-25T19:42:33.167Z',
              },
            },
          ],
        });
        const response = await request
          .set(sqsPostForSendgridEvent.headers)
          .set('Origin', VALID_ORIGIN)
          .post('/api/event')
          .send({
            event: {
              data: [
                {
                  email: 'FOO@gmail.com',
                  timestamp: 1626317952,
                  'smtp-id': '<14c5d75ce93.dfd.64b469@ismtpd-555>',
                  event: 'processed',
                  category: ['cat facts'],
                  sg_event_id: 'Maehyp6M7Gj15MlndO7lxg==',
                  sg_message_id: '14c5d75ce93.dfd.64b469.filter0001.16648.5515E0B88.0',
                },
              ],
              attributes: {
                linuxTimestamp: 1626317953987,
                awsRequestId: 'db5b7e1d-1d28-4249-acc7-ce2fe4c9eb47',
                isoTimestamp: '2021-07-15T02:59:13.987Z',
                webhookSource: 'sendgrid',
              },
            },
          });
        expect(ExternalEventService.processEvent).toBeCalledTimes(1);
        expect(response.status).toEqual(200);
        expect(response.body.message).toEqual('Processed 0 of 1 SendGrid events successfully');
      });
    });
  });
});
