const fs = require('fs');
const path = require('path');
const express = require('express');
const archiver = require('archiver');
const cors = require('cors');
const axios = require('axios');
// const qs = require('qs');
const helmet = require('helmet');
const Amplify = require('aws-amplify');
const jwkToPem = require('jwk-to-pem');
// const { createProxyMiddleware } = require('http-proxy-middleware');
const Sentry = require('@sentry/node');
const createHandler = require('graphql-http/lib/use/express').createHandler;
const { PassThrough } = require('stream');
const { s3 } = require('aws-wrapper');
const AWS = require('aws-sdk');
const { v4: uuid } = require('uuid');
const cookieParser = require('cookie-parser');
const EventEmitter = require('events');
const CryptoJS = require('crypto-js');
const { i18n } = require('../i18n');
const ValidationError = require('../errors/validationError');
// const { isTypeSystemExtensionNode } = require('graphql');
const config = require('../../config')();
const AnalyticsClient = require('../clients/analyticsClient');
const makeCognitoClient = require('../clients/cognitoClient').default;
const makeBillingClient = require('../clients/billingClient').default;
const makeCrmClient = require('../clients/crmClient').default;
const SubscriptionWorker = require('../clients/subscriptionWorker');
const { log, clientLogger } = require('../logger');
const schema = require('./schema');
// const appManifest = require('../../package');
const authMiddleware = require('../auth/authMiddleware');
const cognitoAuthMiddleware = require('../auth/cognitoAuthMiddleware');
const AuthService = require('../services/auth/authService');
const CognitoAuthService = require('../services/auth/cognitoAuthService');
const PartService = require('../services/partService');
const UserRepository = require('../database/repositories/userRepository');
const AccountMemberRepository = require('../database/repositories/accountMemberRepository');
const ExternalEventService = require('../services/externalEventService');
const ReportFactory = require('../services/reportFactory');
const TemplateFactory = require('../services/templateFactory');
const ReviewTaskService = require('../services/reviewTaskService');
const { readFile, loadFile, saveBuffer, writeFile, writeStream, writeCSV } = require('../services/shared/exceljs/fileUtilities');
const Roles = require('../security/roles');
const RolesMapper = require('../security/rolesMapper');
const databaseConstants = require('../database/constants');
const { init: databaseInit, middleware: databaseMiddleware } = require('../database/databaseInit');
const { SphinxClient } = require('../clients/sphinxClient');
const ForbiddenError = require('../errors/forbiddenError');
const UnauthorizedError = require('../errors/unauthorizedError');
const PartFormatter = require('../services/shared/utils/partFormatter');
const defaults = require('../staticData/defaults');
const AuthenticationHelper = require('./AuthenticationHelper');

const downloadBallooned = require('./file/downloadBallooned');
const download = require('./file/download');
const upload = require('./file/upload');
const validate = require('../clients/validate');
const CognitoAuthCommands = require('../services/auth/cognitoAuthCommands');
const { convertEmailToCognitoUsername } = require('../services/auth/authUtils');

const { Auth } = Amplify;
const securePhassphrase = process.env.IXC_PASSWORD_ENCRYPTION_PASSPHRASE;
// Amplify.Logger.LOG_LEVEL = 'DEBUG';

const app = express();

// Validate config
require('../../config/validateConfig')({ config, log });

// TODO: Fix data upload without using this module, since the module breaks the tests
// const fileUpload = require('express-fileupload');
// app.use(fileUpload());

Amplify.default.configure({
  Auth: {
    mandatorySignIn: true,
    region: config.cognito.REGION,
    userPoolId: config.cognito.USER_POOL_ID,
    userPoolWebClientId: config.cognito.APP_CLIENT_ID,
    // endpoint: config.cognito.ENDPOINT,
    authenticationFlowType: 'USER_PASSWORD_AUTH',
  },
});

AWS.config.apiVersions.cognitoidentityserviceprovider = '2016-04-18';

AWS.config.logger = console;

const isTestEnv = process.env.NODE_ENV === 'test';

// Initialize app monitoring
Sentry.init({ dsn: config.appMonitoring.sentryClientKey });

// This app monitoring request handler must be the first middleware used
app.use(Sentry.Handlers.requestHandler());

const pems = {};
let issuer;

(async () => {
  if (!config.jwt.authStrategy || config.jwt.authStrategy !== 'cognito') return;

  log.debug('Getting PEMs');

  try {
    issuer = `https://cognito-idp.${config.cognito.REGION}.amazonaws.com/${config.cognito.USER_POOL_ID}`;
    await axios({
      method: 'GET',
      url: `${issuer}/.well-known/jwks.json`,
    })
      .then((response) => {
        if (response.status !== 200) throw new Error(`Failed to recover JWKs in Cognito Auth Strategy with statusCode ${response.status}`);

        const { keys } = response.data;
        keys.forEach((key) => {
          const pem = {
            n: key.n,
            kty: key.kty,
            e: key.e,
          };
          pems[key.kid] = jwkToPem(pem);
        });
      })
      .catch((e) => {
        log.error(e);
        throw new Error('Error retrieving AWS Cognito User Pool JSON Web Key');
      });
  } catch (e) {
    log.error(e);
    // process.exit(1);
  }
})();

// Enables CORS
const corsOptionsDelegate = (req, callback) => {
  const origin = req.header('Origin') || '';
  const useragent = req.header('user-agent') || '';
  const corsOptions = { credentials: true };
  // ELB-Healthchecker's IP is unknown and does not provide origin header, so bypass CORS check for
  if (config.corsRegExp.test(origin) || /ELB-HealthChecker/i.test(useragent)) {
    corsOptions.origin = true;
  } else if (useragent === 'CypressTestRunner') {
    log.debug('API call made from within Cypress');
    corsOptions.origin = true;
  } else {
    // Disable CORS for this request and log the request details
    corsOptions.origin = false;
    log.warn('Failed CORS check', {
      origin,
      useragent,
      corsRegExp: config.corsRegExp,
      originCheckPassed: config.corsRegExp.test(origin),
      useragentCheckPassed: /ELB-HealthChecker/i.test(useragent),
      method: req.method, // "DELETE"
      protocol: req.protocol, // "https"
      subdomains: req.subdomains, // "['ocean']"
      hostname: req.hostname, // "example.com"
      originalUrl: req.originalUrl, // "/creatures?filter=sharks"
    });
  }
  // Callback expects two parameters: error and options
  callback(null, corsOptions);
};
app.use(cors(corsOptionsDelegate));

// TODO: Delete old Cors config after production deploy works
// app.use(
//   cors({
//     origin: true,
//     credentials: true,
//   })
// );

// Any plugin that parses/transforms the body must be after createProxyMiddleware
// https://expressjs.com/en/api.html#express.json
// Ensure large reports and PDFs can be downloaded and uploaded
app.use(express.json({ limit: '50mb' }));
app.use(express.urlencoded({ limit: '50mb', extended: true }));

// Enables Helmet, a set of tools to increase security.
app.use(helmet());

app.use(cookieParser());

app.use((req, res, next) => {
  if (pems) req.pems = pems;
  if (issuer) req.issuer = issuer;
  next();
});

// Append Winston, Morgan, and Correlation ID logger
require('../logger/expressMiddleware')(app);

// Create RolesMapper for DI
const rolesMapper = new RolesMapper({
  accountMemberRoles: databaseConstants.accountMember.ROLES,
  authorizationRolesNames: Roles.names,
  authorizationRolesDefinitions: Roles.definitions,
});
app.use((req, res, next) => {
  req.rolesMapper = rolesMapper;
  next();
});

// Define endpoint for client side logger to relay log events to
app.post('/api/logger', express.json(), (req, res) => {
  const count = req.body.logs.length;
  req.body.logs.forEach((logs) => {
    clientLogger[logs.level]({
      timestamp: logs.timestamp,
      message: logs,
    });
  });
  res.status(200).send(`Recorded ${count} log events sent from the client`);
});

// Initialize analytics client and store a reference for all downstream middleware
const analyticsClient = new AnalyticsClient({
  writeKey: config.analytics.segmentSourceWriteKey,
});

app.use((req, res, next) => {
  req.analyticsClient = analyticsClient;
  next();
});

// Initialize subscription billing client
// TODO: Make this initialization adapter agnostic by renaming config params
const billingClient = makeBillingClient({
  useAdapter: config.billing.useAdapter,
  siteName: config.billing.chargifySiteName,
  publicKey: config.billing.chargifyPublicKey,
  privateKey: config.billing.chargifyPrivateKey,
  apiKey: config.billing.chargifyApiKey,
  logger: log,
});

// Initialize subscription billing client
log.debug('log debug index.js');
console.log('console log index.js');
const crmClient = makeCrmClient({
  logger: log,
  useAdapter: config.crm.useAdapter,
  ...config.crm, // Copy in the CRM config
});

// Build the subscriptionWorker
// TODO: Move this to async process behind SQS queue
const subscriptionWorker = new SubscriptionWorker({
  s3,
  crmClient,
  billingClient,
  logger: log,
});

(async () => {
  // Load the sales pipeline and timeline event references
  await crmClient.initialize();
  // Load the product catalog
  await subscriptionWorker.loadCatalog({ useCache: false, updateCache: true });
  log.info(`Catalog loaded: ${subscriptionWorker.inspectCatalog()}`);
})().catch((err) => {
  log.error(`Error occurred while loading catalog: ${err}`);
});

/* -- Cognito --*/
log.info(`Auth Strategy: ${config.jwt.authStrategy}`);
const authorizationLayer = async (req, res, next) => (config.jwt.authStrategy === 'cognito' ? cognitoAuthMiddleware(req, res, next) : authMiddleware(req, res, next));

// Initialize the Cognito Client
const cognitoClient = makeCognitoClient({
  logger: log,
});

// Register new user
app.use('/api/cognitoSignup', databaseMiddleware, async (req, res, next) => {
  log.debug('INDEX /api/cognitoSignup START');
  try {
    validate.required(req.body, ['username', 'email', 'firstName']);
    const { username, email, firstName } = req.body;
    let { password } = req.body;

    // cognito requires a password on sign up no matter what
    if (!password) {
      password = email.split('').sort().join('');
      const { index } = /[a-zA-Z]/i.exec(password);
      password = password.charAt(index).toUpperCase() + password + index;
    }

    const source = await AuthService.signupSubmit(email, firstName, { subscriptionWorker, cognitoClient });
    log.debug(`INDEX CognitoAuthCommands.signupResponse Auth.signUp : source= ${source} ${typeof source}`);

    const cognitoUser = await CognitoAuthCommands.cognitoGetUser(email, { cognitoClient });
    log.debug('INDEX CognitoAuthCommands.cognitoGetUser AFTER');
    log.debug(cognitoUser);
    let signupResponse;

    if (cognitoUser) {
      // User already exists, may have been a canceled invite or rejected request
      // Send emailAddressVerification email to prevent repeated access request attempts
      log.debug('INDEX CognitoAuthCommands.cognitoEnableUser BEFORE');
      await CognitoAuthCommands.cognitoEnableUser(email, { cognitoClient });
      log.debug('INDEX CognitoAuthCommands.cognitoEnableUser AFTER');
      log.debug('INDEX Auth.resendSignUp BEFORE');
      signupResponse = await Auth.resendSignUp(username, {
        source: 'emailAddressVerification',
        sender: firstName || email,
      });
      log.debug('INDEX Auth.resendSignUp AFTER');
    } else {
      log.debug(`INDEX CognitoAuthCommands.signupResponse Auth.signUp BEFORE : source= ${source} ${typeof source}`); // source = 'true';
      signupResponse = await Auth.signUp({
        username,
        password,
        clientMetadata: {
          sender: firstName || email,
          source: `${source}`,
        },
        attributes: {
          email,
          'custom:status': `${source}`,
        },
      });
      log.debug('INDEX CognitoAuthCommands.signupResponse Auth.signUp AFTER');
    }
    log.debug('INDEX /api/cognitoSignup END');
    log.debug(signupResponse);
    res.status(200).send(signupResponse);
  } catch (e) {
    e.type = 'cognito';
    next(e);
  }
});

app.use('/api/cognitoConfirmSignup', databaseMiddleware, async (req, res, next) => {
  const { email, code } = req.body;
  try {
    validate.required(req.body, ['email', 'code']);

    const username = convertEmailToCognitoUsername(email);

    const signupResponse = await Auth.confirmSignUp(username, code);
    res.status(200).send(signupResponse);
  } catch (e) {
    e.type = 'cognito';
    next(e);
  }
});

app.use('/api/cognitoAcceptInvitation', databaseMiddleware, async (req, res, next) => {
  try {
    validate.required(req.body, ['firstName', 'familyName', 'email', 'newPassword']);
    const { firstName, familyName, email, newPassword } = req.body;
    const decryptedPassword = CryptoJS.AES.decrypt(newPassword, securePhassphrase).toString(CryptoJS.enc.Utf8);

    let password = email.split('').sort().join('');
    const { index } = /[a-zA-Z]/i.exec(password);
    password = password.charAt(index).toUpperCase() + password + index;

    const username = convertEmailToCognitoUsername(email);

    Auth.signIn(username, password)
      .then((user) => {
        log.debug('cognitoAcceptInvitation signed in');
        Auth.changePassword(user, password, decryptedPassword)
          .then(async () => {
            log.debug('cognitoAcceptInvitation password updated');

            const apiUser = await CognitoAuthService.cognitoRegisterInvitedUser(email, password, { firstName, lastName: familyName }, { subscriptionWorker });

            let membership = null;
            if (apiUser.activeAccountMemberId) {
              membership = await AccountMemberRepository.findById(apiUser.activeAccountMemberId, { subscriptionWorker });
            }

            Auth.updateUserAttributes(user, {
              given_name: firstName,
              family_name: familyName,
              'custom:account': membership.account.id,
              'custom:status': 'active',
            })
              .then(() => {
                log.debug('cognitoAcceptInvitation user updated');
                Auth.signOut().then(() => {
                  log.debug('cognitoAcceptInvitation signed out');
                  res.status(200).send(apiUser);
                });
              })
              .catch((e) => {
                throw e;
              });
          })
          .catch((e) => {
            throw e;
          });
      })
      .catch((e) => {
        throw e;
      });
  } catch (e) {
    e.type = 'cognito';
    next(e);
  }
});

// Set up MFA device for account
app.use('/api/cognitoEnableTotpMfa', databaseMiddleware, cognitoAuthMiddleware, async (req, res, next) => {
  try {
    const accessToken = req.headers.authorization;
    if (!accessToken) {
      throw new Error('Missing Access Token');
    }

    const codeResponse = await CognitoAuthCommands.cognitoAssociateSoftwareToken(accessToken, { cognitoClient });
    res.status(200).send(codeResponse.SecretCode);
  } catch (e) {
    e.type = 'cognito';
    next(e);
  }
});
// Confirm MFA device setup
app.use('/api/cognitoVerifyTotpMfa', databaseMiddleware, cognitoAuthMiddleware, async (req, res, next) => {
  try {
    validate.required(req.body, ['email', 'code', 'friendlyDeviceName']);
    validate.required(req.headers, ['authorization']);
    const { email, code, friendlyDeviceName } = req.body;
    const accessToken = req.headers.authorization;

    await CognitoAuthCommands.cognitoVerifySoftwareToken(accessToken, friendlyDeviceName, code, { cognitoClient });
    const enableMfa = true;
    const prefResponse = await CognitoAuthCommands.cognitoSetUserMFAPreference(email, enableMfa, { cognitoClient });

    res.status(200).send(prefResponse);
  } catch (e) {
    e.type = 'cognito';
    next(e);
  }
});
// Disable MFA for account
app.use('/api/cognitoDisableTotpMfa', databaseMiddleware, cognitoAuthMiddleware, async (req, res, next) => {
  try {
    validate.required(req.body, ['email', 'mfaCode', 'friendlyDeviceName']);
    validate.required(req.headers, ['authorization']);
    const { email, mfaCode, friendlyDeviceName } = req.body;
    const accessToken = req.headers.authorization;

    await CognitoAuthCommands.cognitoVerifySoftwareToken(accessToken, friendlyDeviceName, mfaCode, { cognitoClient });
    const enableMfa = false;
    const prefResponse = await CognitoAuthCommands.cognitoSetUserMFAPreference(email, enableMfa, { cognitoClient });

    res.status(200).send(prefResponse);
  } catch (e) {
    e.type = 'cognito';
    next(e);
  }
});
// Register new cognito user
app.use('/api/cognitoFinishRegistration', databaseMiddleware, async (req, res, next) => {
  try {
    validate.required(req.body, ['firstName', 'email', 'newPassword']);
    const { firstName, familyName, email, newPassword, orgName, phoneNumber } = req.body;
    const newDecryptedPassword = CryptoJS.AES.decrypt(newPassword, securePhassphrase).toString(CryptoJS.enc.Utf8);
    let password = email.split('').sort().join('');
    const { index } = /[a-zA-Z]/i.exec(password);
    password = password.charAt(index).toUpperCase() + password + index;

    const username = convertEmailToCognitoUsername(email);

    Auth.signIn(username, password)
      .then((user) => {
        log.debug('Cognito Finish Registration Signed In');
        Auth.changePassword(user, password, newDecryptedPassword)
          .then(async () => {
            log.debug('Cognito Finish Registration Password Updated');

            const apiUser = await CognitoAuthService.cognitoFinishRegistration(email, firstName, familyName, newDecryptedPassword, orgName, phoneNumber, { subscriptionWorker, cognitoClient });
            let membership = null;

            if (apiUser.activeAccountMemberId) {
              membership = await AccountMemberRepository.findById(apiUser.activeAccountMemberId, { subscriptionWorker });
            }

            Auth.updateUserAttributes(user, {
              given_name: firstName,
              family_name: familyName || '',
              'custom:account': membership.account.id,
              'custom:status': 'active',
            })
              .then(() => {
                log.debug('Cognito Finish Registration User Updated');
                Auth.signOut().then(() => {
                  log.debug('Cognito Finish Registration Signed Out');
                  res.status(200).send(apiUser);
                });
              })
              .catch((e) => {
                throw e;
              });
          })
          .catch((e) => {
            throw e;
          });
      })
      .catch((e) => {
        throw e;
      });
  } catch (e) {
    e.type = 'cognito';
    next(e);
  }
});
// Refresh auth token for cognito auth flow
app.use('/api/cognitoRefreshToken', async (req, res) => {
  log.debug('cognitoRefreshToken');
  if (!req.cookies.refresh_token && !req.cookies.device_key) {
    return res.status(401).send('Unauthorized');
  }

  const refreshToken = req.cookies.refresh_token;
  const deviceKey = req.cookies.device_key;

  try {
    const initResponse = await CognitoAuthCommands.cognitoAdminInitiateAuth(refreshToken, deviceKey, { cognitoClient });

    res.cookie('session', new Date().valueOf(), {
      httpOnly: true,
      secure: true,
      domain: config.hostname,
    });
    res.cookie('last_activity', new Date().valueOf(), {
      httpOnly: true,
      secure: true,
      domain: config.hostname,
    });
    res.status(200).send({
      authSignIn: initResponse.AuthenticationResult.AccessToken,
      challengeName: initResponse.ChallengeName,
      challengeParameters: initResponse.ChallengeParameters,
    });
  } catch (error) {
    log.error('Error while verifying refresh token:', error);

    res.clearCookie('refresh_token', {
      httpOnly: true,
      secure: req.protocol === 'https',
      domain: req.hostname || 'ixc-local.com',
    });

    res.clearCookie('device_key', {
      httpOnly: true,
      secure: req.protocol === 'https',
      domain: req.hostname || 'ixc-local.com',
    });

    res.status(401).send('Unauthorized');
  }
});
// Check MFA code for account
app.use('/api/cognitoMfaCheck', async (req, res, next) => {
  try {
    validate.required(req.body, ['email', 'mfaCode']);
    const { email, Session, mfaCode, rememberMe } = req.body;
    const rememberStatus = rememberMe ? 'remembered' : 'not_remembered';

    const userPoolId = config.cognito.USER_POOL_ID;
    const authHelper = new AuthenticationHelper(userPoolId.split('_')[1]); // in the event we need to use USER_SRP_AUTH, this helps

    const mfaResponse = await CognitoAuthCommands.cognitoRespondToAuthChallenge(email, mfaCode, Session, { cognitoClient });

    const { AuthenticationResult } = mfaResponse;

    const deviceKey = await CognitoAuthService.deviceKeyHandler(email, AuthenticationResult, rememberStatus, mfaResponse, authHelper, { cognitoClient });

    if (deviceKey.errGenHash) {
      return res.status(500).send(deviceKey.errGenHash);
    }

    // Set cookies so the frontend can authenticate on refresh
    res.cookie('refresh_token', AuthenticationResult ? AuthenticationResult.RefreshToken : undefined, {
      httpOnly: true,
      maxAge: config.jwt.refreshExpiry * 1000,
      secure: config.protocol === 'https',
      domain: config.hostname,
    });

    res.cookie('device_key', deviceKey, {
      httpOnly: true,
      maxAge: config.jwt.refreshExpiry * 1000,
      secure: config.protocol === 'https',
      domain: config.hostname,
    });
    res.cookie('session', new Date().valueOf(), {
      httpOnly: true,
      secure: true,
      domain: config.hostname,
    });
    res.cookie('last_activity', new Date().valueOf(), {
      httpOnly: true,
      secure: true,
      domain: config.hostname,
    });
    res.status(200).send({
      authSignIn: AuthenticationResult.AccessToken,
    });
  } catch (error) {
    error.type = 'cognito';
    next(error);
  }
});
// Sign in with cognito auth flow
app.use('/api/cognitoSignin', async (req, res, next) => {
  try {
    // validate required fields
    validate.required(req.body, ['email', 'password']);
    const { email, password, mfaCode, rememberMe } = req.body;
    const decryptedPassword = CryptoJS.AES.decrypt(password, securePhassphrase).toString(CryptoJS.enc.Utf8);
    const rememberStatus = rememberMe ? 'remembered' : 'not_remembered';

    const userPoolId = config.cognito.USER_POOL_ID;
    const authHelper = new AuthenticationHelper(userPoolId.split('_')[1]); // in the event we need to use USER_SRP_AUTH, this helps

    // Get User Information
    const cognitoUser = await CognitoAuthCommands.cognitoGetUser(email, { cognitoClient });

    let initResponse;

    if (cognitoUser) {
      initResponse = await CognitoAuthService.cognitoFinishSignin(email, decryptedPassword, mfaCode, rememberStatus, authHelper, { cognitoClient });
    } else {
      const user = await UserRepository.findByEmail(email, {});
      if (user && user.emailVerified && user.accountId) {
        initResponse = await CognitoAuthService.cognitoMigrateUser(email, decryptedPassword, mfaCode, rememberStatus, authHelper, { cognitoClient });
      }
    }

    if (initResponse) {
      if (initResponse.refreshToken) {
        // Set cookies so the frontend can authenticate on refresh
        res.cookie('refresh_token', initResponse.refreshToken, {
          httpOnly: true,
          maxAge: config.jwt.refreshExpiry * 1000,
          secure: config.protocol === 'https',
          domain: config.hostname,
        });
        res.cookie('device_key', initResponse.deviceKey, {
          httpOnly: true,
          maxAge: config.jwt.refreshExpiry * 1000,
          secure: config.protocol === 'https',
          domain: config.hostname,
        });
        res.cookie('session', new Date().valueOf(), {
          httpOnly: true,
          secure: true,
          domain: config.hostname,
        });
        res.cookie('last_activity', new Date().valueOf(), {
          httpOnly: true,
          secure: true,
          domain: config.hostname,
        });
      }
      res.status(initResponse.status).send(initResponse.payload);
    }
    throw new ValidationError('en', 'auth.cognito.NotAuthorizedException');
  } catch (error) {
    // Set error type to cognito so it will be caught by the error trap at the bottom of this file
    // And formatted so the frontend can access the error message correctly
    error.type = 'cognito';
    next(error);
  }
});
// Authenticate cognito user
app.use('/api/cognitoAuthMe', databaseMiddleware, cognitoAuthMiddleware, async (req, res, next) => {
  try {
    const language = req.headers['accept-language'] && req.headers['accept-language'].includes('en') ? 'en' : req.headers['accept-language'];
    if (!req.currentUser) throw new UnauthorizedError(language);
    if (!req.currentUser.id) throw new ForbiddenError(language);

    if (req.currentUser.profile) {
      req.currentUser.firstName = req.currentUser.profile.firstName;
      req.currentUser.lastName = req.currentUser.profile.lastName;
      req.currentUser.phoneNumber = req.currentUser.profile.phoneNumber;
      req.currentUser.avatars = req.currentUser.profile.avatars;
    }

    req.currentUser.isServiceAccount = Object.values(defaults.serviceAccounts).includes(req.currentUser.accountId);
    req.currentUser.isReviewAccount = req.currentUser.accountId === defaults.serviceAccounts.review;

    if (!req.currentUser.mfaRegistered) {
      const cognitoUser = await CognitoAuthCommands.cognitoGetUser(req.currentUser.email, { cognitoClient });
      req.currentUser.mfaRegistered = cognitoUser && cognitoUser.PreferredMfaSetting === 'SOFTWARE_TOKEN_MFA';
    }

    res.status(200).send({ authMe: req.currentUser });
  } catch (error) {
    error.type = 'cognito';
    next(error);
  }
});
// Resend cognito registration code
app.use('/api/cognitoResendCode', async (req, res, next) => {
  try {
    validate.required(req.body, ['username']);
    const { username } = req.body;
    await Auth.resendSignUp(username);
    res.status(200);
  } catch (error) {
    error.type = 'cognito';
    next(error);
  }
});

app.use('/api/cognitoConfirmSignin', async (req, res, next) => {
  try {
    validate.required(req.body, ['username', 'code']);
    const { username, code } = req.body;
    await Auth.confirmSignIn(username, code);
    res.status(200);
  } catch (error) {
    error.type = 'cognito';
    next(error);
  }
});

/* -- Cognito End -- */

app.use('/api/chargifyReset', databaseMiddleware, authorizationLayer, async (req, res) => {
  const { seatSS, seatPU, partSS } = req.body;
  const components = [];
  if (partSS) {
    // Reset Freemium
    await subscriptionWorker.updateSubscription({
      subscriptionId: 37003057,
      components: [
        {
          component_id: 1108426,
          product_family_key: 'inspectionxpert-cloud',
          quantity: 5,
          upgrade_charge: 'none',
          memo: 'reset',
        },
      ],
      providerProductId: 5371107,
    });
    res.status(200).send('Subscription Reset to Seed Default');
  } else {
    // Reset Default
    components.push({
      component_id: 1108426,
      product_family_key: 'inspectionxpert-cloud',
      upgrade_charge: 'none',
      quantity: 0,
      memo: 'reset',
    });
    if (seatSS) {
      components.push({
        component_id: 1213884,
        product_family_key: 'inspectionxpert-cloud',
        upgrade_charge: 'none',
        quantity: 1,
        memo: 'reset',
      });
    }
    if (seatPU) {
      components.push({
        component_id: 1028184,
        product_family_key: 'inspectionxpert-cloud',
        quantity: 30,
        upgrade_charge: 'none',
        memo: 'reset',
      });
    }
    await subscriptionWorker.updateSubscription({
      subscriptionId: 37003269,
      components,
      providerProductId: 5371107,
      billingInterval: 'Monthly',
      remittance: true,
    });
    res.status(200).send('Subscription Reset to Seed Default');
  }
});

// Initialize SPHINX client
const sphinxClient = new SphinxClient({
  baseUrl: config.sphinx.baseUrl,
  basePath: config.sphinx.basePath,
  sharedKey: config.sphinx.sharedKey,
  logger: log,
});

// Initializes the Database
databaseInit().catch((error) => log.error(error));

// Sets up the Upload endpoint, which is required to be REST

upload.mapAllUploadRequests('/api', app, databaseMiddleware, authorizationLayer);

// Sets up the Download endpoint, which is required to be REST

app.get('/api/download', databaseMiddleware, authorizationLayer, download);

app.get('/api/download/ballooned', databaseMiddleware, authorizationLayer, downloadBallooned);

// Define healthcheck endpoint
app.get('/api/health', (req, res) => {
  // TODO: Add DB Checks, etc
  res.status(200).send(`API /api/health is Healthy`);
});

// Define endpoint that returns backend config details
app.get('/api/config', (req, res) => {
  // Explicity copy in config, so that secrets aren't leaked
  const response = {
    env: config.env,
    buildDatetime: config.buildDatetime,
    hostname: config.hostname,
    package: config.package,
    scm: config.scm,
  };
  if (config.env !== 'production') {
    response.database = { host: config.database.host };
    response.clientUrl = config.clientUrl;
    response.email = { from: config.email.from };
    response.awsRegion = config.awsRegion;
  }
  res.status(200).send(response);
});

// TODO: This should be moved into a report service modules
let template = 'src/templates/All Data.xlsx';
// log.debug(`config.hostname: ${config.hostname}`);
// log.debug(`local template: ${path.resolve(template)}`);
// log.debug(`local template exists: ${fs.existsSync(path.resolve(template))}`);
(async () => {
  const templatePath = `https://www.${config.hostname}/templates/All Data.xlsx`;
  if (!fs.existsSync(path.resolve(template)) && config.hostname !== 'ixc-local.com') {
    log.debug(`Fetching report template from: ${templatePath}`);
    const templateResponse = await axios.get(templatePath);
    if (templateResponse && templateResponse.data) {
      template = templateResponse.data;
    }
    return templatePath;
  }
  return template;
})()
  .then((templatePath) => {
    log.debug(`Initialized report service without errors. Using template: ${templatePath}`);
  })
  .catch((err) => {
    log.error(`Error occurred while looking up report template: ${err}`);
    log.debug(`Initializing report service after error. Using template: ${template}`);
  });

// Convert temp report file to permanent file
app.use('/api/report/new', databaseMiddleware, authorizationLayer, async (req, res) => {
  const { privateUrl, file, report } = req.body;
  if (!privateUrl || !file || !report) {
    log.debug({ privateUrl, file, report });
    res.status(400).send('Missing required parameters');
  } else {
    let fileDownload;
    if (config.uploadRemote) {
      const fields = {
        accountId: req.currentUser.accountId,
        siteId: req.currentUser.siteId,
        uniqueFileName: privateUrl,
      };
      try {
        // Download Temp File
        const result = await s3.getObject(config.s3UploadBucket, `${req.currentUser.accountId}/${req.currentUser.siteId}/${file}`);
        fileDownload = result.Body;
        // Upload Temp file to correct folder for DB link
        const code = await upload.uploadToRemoteStorage(fileDownload, fields);
        if (code !== 200) {
          log.error('Temporary file could not be uploaded to S3');
        }
        // Delete Temp File
        log.debug(`deleting previous temp file: ${file}`);
        s3.deleteObject(config.s3UploadBucket, `${req.currentUser.accountId}/${req.currentUser.siteId}/${file}`, (err, data) => {
          if (err) {
            log.error(err);
          }
          if (!data) {
            log.error('Could not delete temporary report. Please check that you have permissions');
          }
          log.debug(`temporary file deleted: ${file}`);
        });
        res.status(201).send('File Saved');
      } catch (error) {
        res.status(404).send(error);
        throw error;
      }
    } else {
      // Rename Temp file to match DB link
      if (req.currentUser) {
        fileDownload = path.join(config.uploadDir, req.currentUser.accountId, req.currentUser.siteId, privateUrl);
      } else {
        fileDownload = path.join(config.uploadDir, privateUrl);
      }
      fs.renameSync(file, fileDownload);
      res.status(201).send('File Saved');
    }
  }
});

// Update report file so report downloads correct file
app.use('/api/report/update', databaseMiddleware, authorizationLayer, async (req, res) => {
  const { privateUrl, changes } = req.body;
  if (!privateUrl || !changes) {
    log.debug({ privateUrl, changes });
    res.status(400).send('Missing required parameters');
  } else {
    let fileDownload;
    let workbook;
    if (config.uploadRemote) {
      const fields = {
        accountId: req.currentUser.accountId,
        siteId: req.currentUser.siteId,
        uniqueFileName: privateUrl,
      };
      try {
        const result = await s3.getObject(config.s3UploadBucket, `${req.currentUser.accountId}/${req.currentUser.siteId}/${privateUrl}`);
        fileDownload = result.Body;
        workbook = await loadFile(fileDownload);
        const changedFile = await ReportFactory.update(changes, workbook);
        const buffer = await saveBuffer(changedFile);
        await upload.uploadToRemoteStorage(buffer, fields);
      } catch (error) {
        res.status(404).send(error);
        throw error;
      }
      res.status(200).send('good');
    } else {
      try {
        if (req.currentUser) {
          fileDownload = path.join(config.uploadDir, req.currentUser.accountId, req.currentUser.siteId, privateUrl);
        } else {
          fileDownload = path.join(config.uploadDir, privateUrl);
        }
        workbook = await readFile(fileDownload).catch((err) => {
          res.status(400).send({
            error: err,
            message: 'The report file could not be loaded.',
            file: fileDownload,
          });
          // throw err;
        });
        const changedFile = await ReportFactory.update(changes, workbook);
        if (!changedFile) {
          log.error("couldn't update workbook");
          res.status(400).send("couldn't update workbook");
        }
        if (req.currentUser) {
          await writeFile(changedFile, path.join(config.uploadDir, req.currentUser.accountId, req.currentUser.siteId, privateUrl)).catch((err) => {
            res.status(400).send({
              error: err,
              message: 'The report file could not be updated.',
              file: path.join(config.uploadDir, req.currentUser.accountId, req.currentUser.siteId, privateUrl),
            });
            // throw err;
          });
        } else {
          await writeFile(changedFile, path.join(config.uploadDir, privateUrl)).catch((err) => {
            res.status(400).send({
              error: err,
              message: 'The report file could not be updated.',
              file: path.join(config.uploadDir, privateUrl),
            });
            // throw err;
          });
        }
        res.status(200).send('Update saved');
      } catch (err) {
        res.status(404).send(`Update could not be saved: ${err}`);
      }
    }
  }
});

app.use('/api/template/copy', databaseMiddleware, authorizationLayer, async (req, res) => {
  const { newFileName } = req.body;
  let { privateUrl } = req.body;
  let fileDownload;
  if (!privateUrl || !newFileName) {
    log.debug({ privateUrl });
    res.status(400).send('Missing required parameters');
  }
  const privInd = privateUrl.indexOf('?privateUrl=');
  privateUrl = privInd < 0 ? privateUrl : privateUrl.substring(privInd + 12);
  const defaultInd = privateUrl.indexOf('/templates/');

  if (config.uploadRemote) {
    if (defaultInd >= 0) {
      // Default Template
      privateUrl = privateUrl.substring(defaultInd);
      fs.readFile(`src${privateUrl}`, async (err, data) => {
        if (err) {
          res.status(404).send(err);
          throw err;
        }
        const fields = {
          accountId: req.currentUser.accountId,
          siteId: req.currentUser.siteId,
          uniqueFileName: newFileName,
        };
        const code = await upload.uploadToRemoteStorage(data, fields);
        if (code !== 200) {
          log.error('Temporary file could not be uploaded to S3');
          res.status(404).send({ message: 'Temporary file could not be uploaded to S3' });
        } else {
          res.status(200).send({ file: newFileName });
        }
      });
    } else {
      // Template already on s3
      const key = `${req.currentUser.accountId}/${req.currentUser.siteId}/${newFileName}`; // new file name
      const bucket = config.s3UploadBucket; // destination bucket
      const source = `${config.s3UploadBucket}/${req.currentUser.accountId}/${req.currentUser.siteId}/${privateUrl}`; // source bucket + source file

      try {
        // TODO: confirm this works on dev
        log.debug(`COPY - KEY:${key}, SOURCE:${source}`);
        await s3.copyObject(bucket, key, source);
      } catch (error) {
        log.debug('COPY ERROR');
        log.debug(error);
        res.status(404).send(error);
        throw error;
      }
      res.status(200).send({ file: newFileName });
    }
  } else {
    if (req.currentUser) {
      fileDownload = path.join(config.uploadDir, req.currentUser.accountId, req.currentUser.siteId, newFileName);
    } else {
      fileDownload = path.join(config.uploadDir, newFileName);
    }
    if (defaultInd >= 0) {
      // Default Template
      privateUrl = privateUrl.substring(defaultInd);
      log.debug(privateUrl);
      try {
        log.debug(`src${privateUrl}, ${fileDownload}`);
        fs.copyFileSync(`src${privateUrl}`, fileDownload);
        res.status(200).send({ file: fileDownload });
      } catch (error) {
        res.status(400).send(error);
        throw error;
      }
    } else {
      // Local Custom Template
      if (req.currentUser) {
        privateUrl = path.join(config.uploadDir, req.currentUser.accountId, req.currentUser.siteId, privateUrl);
      } else {
        privateUrl = path.join(config.uploadDir, privateUrl);
      }
      try {
        fs.copyFileSync(privateUrl, fileDownload);
        res.status(200).send({ file: fileDownload });
      } catch (error) {
        res.status(400).send(error);
        throw error;
      }
    }
  }
});

// Download the report excel in the specified format
app.use('/api/excel', databaseMiddleware, authorizationLayer, async (req, res) => {
  let { privateUrl } = req.body;
  const { type, font, unframed, filename } = req.body;

  if (!privateUrl) {
    log.debug({ req: req.body });
    res.status(400).send('Missing private URL');
  } else {
    if (process.env.NODE_ENV === 'test') {
      // pass user to jest for api testing.
      // TODO: Set this in config?
      req.currentUser = { accountId: req.headers?.account, siteId: req.headers?.site };
    }
    let fileDownload;
    let workbook;
    const privInd = privateUrl.indexOf('?privateUrl=');
    privateUrl = privInd < 0 ? privateUrl : privateUrl.substring(privInd + 12);
    const defaultInd = privateUrl.indexOf('/templates/');
    // Get file
    if (defaultInd >= 0) {
      // Default Template
      workbook = await readFile(`src${privateUrl.substring(defaultInd)}`).catch((err) => {
        res.status(400).send({ error: err, message: 'The file could not be loaded.', file: privateUrl });
        throw err;
      });
      if (workbook) {
        // Check conditional formats
        const shouldOverwrite = ReportFactory.writeValidatedConditionalFormatting(workbook);
        if (shouldOverwrite) {
          await writeFile(workbook, `src${privateUrl.substring(defaultInd)}`).catch((err) => {
            res.status(400).send({
              error: err,
              message: 'The report file could not be updated.',
              file: `src${privateUrl.substring(defaultInd)}`,
            });
            // throw err;
          });
        }
      }
    } else if (config.uploadRemote) {
      // Template Already on S3
      try {
        const result = await s3.getObject(config.s3UploadBucket, `${req.currentUser.accountId}/${req.currentUser.siteId}/${privateUrl}`);
        fileDownload = result.Body;
        workbook = await loadFile(fileDownload);
        if (workbook) {
          // Check conditional formats
          const shouldOverwrite = ReportFactory.writeValidatedConditionalFormatting(workbook);
          if (shouldOverwrite) {
            const fields = {
              accountId: req.currentUser.accountId,
              siteId: req.currentUser.siteId,
              uniqueFileName: privateUrl,
            };
            const buffer = await saveBuffer(workbook);
            await upload.uploadToRemoteStorage(buffer, fields);
          }
        }
      } catch (error) {
        res.status(404).send(error);
        throw error;
      }
    } else {
      // Local Custom Template
      if (req.currentUser) {
        fileDownload = path.join(config.uploadDir, req.currentUser.accountId, req.currentUser.siteId, privateUrl);
      } else {
        fileDownload = path.join(config.uploadDir, privateUrl);
      }
      workbook = await readFile(fileDownload).catch((err) => {
        res.status(400).send({ error: err, message: 'The file could not be loaded.', file: fileDownload });
        throw err;
      });
      if (workbook) {
        // Check conditional formats
        const shouldOverwrite = ReportFactory.writeValidatedConditionalFormatting(workbook);
        if (shouldOverwrite) {
          await writeFile(workbook, fileDownload).catch((err) => {
            res.status(400).send({
              error: err,
              message: 'The report file could not be updated.',
              file: fileDownload,
            });
            // throw err;
          });
        }
      }
    }

    if (!workbook) {
      log.error("couldn't load workbook");
      res.status(400).send("couldn't load workbook");
    }
    // Convert file
    if (type) {
      log.debug(privateUrl.replace(/\.[0-9a-z]+$/i, ''));
      if (unframed) {
        workbook = await ReportFactory.unFrame(workbook);
      }
      if (font) {
        log.debug('writing zip file');
        // Set up zipper
        const archive = archiver('zip', {
          zlib: { level: 9 }, // Sets the compression level.
        });
        archive.on('warning', (err) => {
          if (err.code === 'ENOENT') {
            log.warn('ENOENT');
          } else {
            // throw error
            log.debug('archiver error');
            throw err;
          }
        });
        archive.on('error', (err) => {
          throw err;
        });
        // Set up return headers
        res.setHeader('Content-Type', 'application/x-zip-compressed');
        res.setHeader('Content-Disposition', `attachment; filename=${privateUrl.replace(/\.[0-9a-z]+$/i, '')}.zip`);
        archive.pipe(res);
        if (type === 'csv') {
          // Write each sheet to a CSV file
          await Promise.all(
            workbook.worksheets.map(async (sheet, id) => {
              log.debug(id);
              const output = new PassThrough();
              await writeCSV(workbook, sheet.name, output);
              archive.append(output, { name: `${filename}-${sheet.name}.csv` });
            }),
          );
        } else if (type === 'xlsx') {
          // Attach xlsx file to the zip
          const output = new PassThrough();
          await writeStream(workbook, output);
          archive.append(output, { name: `${filename}.xlsx` });
        }
        const fontFile = fs.createReadStream('src/templates/InspectionXpertGDT.ttf');
        archive.append(fontFile, { name: 'InspectionXpertGDT.ttf' });

        log.debug('finalize');
        archive.finalize();
      } else if (type === 'xlsx') {
        log.debug(`writing xlsx ${privateUrl}`);
        // const output = new PassThrough();
        res.setHeader('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        res.setHeader('Content-Disposition', `attachment; filename=${privateUrl.replace(/\.[0-9a-z]+$/i, '')}.xlsx`);
        await writeStream(workbook, res);
        // output.pipe(res);
      } else if (type === 'csv') {
        log.debug('writing csv');
        const archive = archiver('zip', {
          zlib: { level: 9 }, // Sets the compression level.
        });
        archive.on('warning', (err) => {
          if (err.code === 'ENOENT') {
            log.warn('ENOENT');
          } else {
            // throw error
            log.debug('archiver error');
            throw err;
          }
        });
        archive.on('error', (err) => {
          throw err;
        });

        res.setHeader('Content-Type', 'application/x-zip-compressed');
        res.setHeader('Content-Disposition', `attachment; filename=${privateUrl.replace(/\.[0-9a-z]+$/i, '')}.zip`);
        archive.pipe(res);

        await Promise.all(
          workbook.worksheets.map(async (sheet, id) => {
            log.debug(id);
            const output = new PassThrough();
            await writeCSV(workbook, sheet.name, output);
            archive.append(output, { name: `${filename}-${sheet.name}.csv` });
          }),
        );
        log.debug('finalize');
        archive.finalize();
      } else {
        res.status(400).send("Couldn't write to specified type");
      }
    } else {
      // Return UI
      const data = await TemplateFactory.returnUI(fileDownload, workbook);
      let unsupportedFeatures = null;
      if (Object.keys(workbook.definedNames.matrixMap).length > 0) {
        unsupportedFeatures = 'Named Ranges';
      }
      res.status(200).send({ data, unsupportedFeatures });
    }
  }
});

app.use('/api/report', databaseMiddleware, authorizationLayer, async (req, res) => {
  const { partId, filename, drawingURL, drawings, reports } = req.body;
  if (process.env.NODE_ENV === 'test') {
    // pass user to jest for api testing.
    // TODO: Set this in config?
    req.currentUser = { accountId: req.headers?.account, siteId: req.headers?.site };
  }
  if (!partId) {
    log.error(`Expected part ID, received ${partId}`);
    res.status(400).send('Missing part ID');
    return;
  }
  if (!drawingURL) {
    log.error(`Expected drawing URL, received ${drawingURL}`);
    res.status(400).send('Missing drawing URL');
    return;
  }

  try {
    const manifest = {
      name: filename,
      description: 'Zip file of all part data.',
    };
    const { drawing, ballooned } = await downloadBallooned.createBalloonedPDF(drawingURL, req.currentUser, partId);
    if (!drawing || !ballooned) {
      log.error("couldn't load drawing PDF");
      res.status(400).send("couldn't load drawing PDF");
    }
    // Set up zipper
    const archive = archiver('zip', {
      zlib: { level: 9 }, // Sets the compression level.
    });
    archive.on('warning', (err) => {
      if (err.code === 'ENOENT') {
        log.warn('ENOENT');
      } else {
        // throw error
        log.debug('archiver error');
        throw err;
      }
    });
    archive.on('error', (err) => {
      throw err;
    });
    // Set up return headers
    res.setHeader('Content-Type', 'application/x-zip-compressed');
    res.setHeader('Content-Disposition', `attachment; filename=${filename}`);
    archive.pipe(res);
    // Attached ballooned drawing
    log.debug('attach ballooned');
    archive.append(ballooned, { name: 'Ballooned.pdf' });
    const saveFilesTo = config.uploadRemote ? 's3' : 'local-fs';
    const fileSystem = saveFilesTo === 'local-fs' ? fs : s3;

    manifest.files = ['Ballooned.pdf'];
    const manifestReports = [];
    // attach any reports
    if (reports) {
      for (let r = 0; r < reports.length; r++) {
        const item = reports[r];
        const temp = await downloadBallooned.getFileBuffer(saveFilesTo, config, item.url, fileSystem, req.currentUser);
        if (temp) {
          const regexMatch = item.url.match(/\.[0-9a-z]+$/i);
          const extension = regexMatch ? regexMatch[0] : null;
          log.debug(`attach ${item.name}${extension}`);
          if (item.name && item.name.includes(extension)) {
            log.debug(`attach ${item.name}`);
            archive.append(temp, { name: item.name });
            manifestReports.push(item.name);
          } else {
            log.debug(`attach ${item.name}${extension}`);
            archive.append(temp, { name: item.name + extension });
            manifestReports.push(item.name + extension);
          }
        }
      }
    }
    manifest.reports = manifestReports;
    const manifestDrawings = [];
    // attach any other files
    if (drawings) {
      for (let i = 0; i < drawings.length; i++) {
        const item = drawings[i];
        const temp = await downloadBallooned.getFileBuffer(saveFilesTo, config, item.url, fileSystem, req.currentUser);
        if (temp) {
          const regexMatch = item.url.match(/\.[0-9a-z]+$/i);
          const extension = regexMatch ? regexMatch[0] : null;
          if (item.name && item.name.includes(extension)) {
            log.debug(`attach ${item.name}`);
            archive.append(temp, { name: item.name });
            manifestDrawings.push(item.name);
          } else {
            log.debug(`attach ${item.name}${extension}`);
            archive.append(temp, { name: item.name + extension });
            manifestDrawings.push(item.name + extension);
          }
        }
      }
    }
    manifest.drawings = manifestDrawings;

    const partService = new PartService({ currentUser: req.currentUser });

    const part = await partService.findByIdForExport(partId);
    if (part) {
      archive.append(JSON.stringify(part), { name: 'fullPart.json' });
      manifest.files.push('fullPart.json');
    }
    // append manifest
    archive.append(JSON.stringify(manifest), { name: 'manifest.json' });

    // finish
    archive.finalize();
  } catch (error) {
    res.status(404).send(error);
    throw error;
  }
});

app.use('/api/export', databaseMiddleware, authorizationLayer, async (req, res) => {
  const { partId, filename } = req.body;
  if (process.env.NODE_ENV === 'test') {
    // pass user to jest for api testing.
    // TODO: Set this in config?
    req.currentUser = { accountId: req.headers?.account, siteId: req.headers?.site };
  }
  if (!partId) {
    log.error(`Expected part ID, received ${partId}`);
    res.status(400).send('Missing part ID');
    return;
  }

  try {
    // Set up zipper
    const archive = archiver('zip', {
      zlib: { level: 9 }, // Sets the compression level.
    });
    archive.on('warning', (err) => {
      if (err.code === 'ENOENT') {
        log.warn('ENOENT');
      } else {
        // throw error
        log.debug('archiver error');
        throw err;
      }
    });
    archive.on('error', (err) => {
      throw err;
    });
    // Set up return headers
    res.setHeader('Content-Type', 'application/x-zip-compressed');
    res.setHeader('Content-Disposition', `attachment; filename=${filename}`);
    archive.pipe(res);

    const partService = new PartService({ currentUser: req.currentUser });

    const part = await partService.findByIdForExport(partId);
    if (part) {
      const formattedPart = await PartFormatter.netInspectJSON(part);
      const formattedXmlPart = await PartFormatter.netInspectXML(part);

      archive.append(JSON.stringify(part), { name: 'fullPart.json' });
      archive.append(JSON.stringify(formattedPart), { name: 'netInspectFAI.json' });
      archive.append(formattedXmlPart, { name: 'netInspectFAI.xml' });
    }

    // finish
    archive.finalize();
  } catch (error) {
    res.status(404).send(error);
    throw error;
  }
});

// load template data
app.use('/api/template', databaseMiddleware, authorizationLayer, async (req, res) => {
  // for testing
  if (config.isTestEnv && req.body.user) {
    req.currentUser = req.body.user;
  }

  let templateFile = req.body.privateUrl;
  const part = req.body.data;
  const { reportFile, templateSettings, direction, file } = req.body;
  let output = `${uuid()}.xlsx`;
  let workbook;
  const privInd = templateFile.indexOf('?privateUrl=');
  const privateUrl = privInd < 0 ? templateFile : templateFile.substring(privInd + 12);
  const defaultInd = templateFile.indexOf('/templates/');
  if (!part) {
    return res.status(400).send('No Data Received');
  }
  if (!templateFile) {
    return res.status(400).send('template file path not received');
  }
  if (config.uploadRemote) {
    // Template is uploaded to S3
    try {
      if (defaultInd >= 0) {
        log.debug('default template');
        // Template is default template
        workbook = await readFile(`src${templateFile.substring(defaultInd)}`).catch((err) => {
          res.status(400).send({
            error: err,
            message: 'The template file could not be loaded.',
            file: privateUrl,
          });
          throw err;
        });
      } else {
        const result = await s3.getObject(config.s3UploadBucket, `${req.currentUser.accountId}/${req.currentUser.siteId}/${privateUrl}`);
        // const result = await s3.getObject(getObjectParams).promise();
        templateFile = result.Body;
        workbook = await loadFile(templateFile);
      }
      // replace tokens with part data
      const report = await ReportFactory.preview(part, workbook, templateSettings, direction);
      // Save temp report to s3
      if (reportFile) {
        // don't use generated UUID if one has already been saved to the DB
        output = reportFile;
      }
      const buffer = await saveBuffer(report);
      const fields = {
        accountId: req.currentUser.accountId,
        siteId: req.currentUser.siteId,
        uniqueFileName: output,
      };
      const code = await upload.uploadToRemoteStorage(buffer, fields);
      if (code !== 200) {
        log.error('Temporary file could not be uploaded to S3');
      }
      // parse report to handsontable data structure
      const data = await TemplateFactory.returnUI(templateFile, report);
      if (file) {
        log.debug(`deleting previous temp file: ${file}`);
        // Delete Previous Temp File
        s3.deleteObject(config.s3UploadBucket, `${req.currentUser.accountId}/${req.currentUser.siteId}/${file}`, (err, d) => {
          if (err) {
            log.error(err);
          }
          if (!d) {
            log.error('Could not delete temporary report. Please check that you have permissions');
          }
          log.debug(`${file} deleted`);
        });
      }
      return res.status(200).send({ data, report: output, local: false });
    } catch (error) {
      res.status(404).send(error);
      throw error;
    }
  } else {
    // Template is uploaded to local uploads folder
    if (defaultInd >= 0) {
      templateFile = `src${templateFile.substring(defaultInd)}`;
    } else if (req.currentUser) {
      templateFile = path.join(config.uploadDir, req.currentUser.accountId, req.currentUser.siteId, privateUrl);
    } else {
      templateFile = path.join(config.uploadDir, privateUrl);
    }

    workbook = await readFile(templateFile).catch((err) => {
      res.status(400).send({
        error: err,
        message: 'The template file could not be loaded.',
        file: templateFile,
      });
      throw err;
    });
    // replace tokens with part data
    const report = await ReportFactory.preview(part, workbook, templateSettings, direction).catch((err) => {
      res.status(400).send({ error: err, message: 'The template could not be populated with data' });
      throw err;
    });
    if (reportFile) {
      // don't use generated UUID if one has already been saved to the DB
      if (req.currentUser) {
        output = path.join(config.uploadDir, req.currentUser.accountId, req.currentUser.siteId, reportFile);
      } else {
        output = path.join(config.uploadDir, reportFile);
      }
    } else {
      output = path.join(config.uploadDir, output);
    }
    await writeFile(report, output).catch((err) => {
      res.status(400).send({ error: err, message: 'A report file could not be created.' });
      throw err;
    });
    // parse report to handsontable data structure
    const data = await TemplateFactory.returnUI(templateFile, report).catch((err) => {
      res.status(400).send({
        error: err,
        message: 'The generated report could not be parsed for visual representation.',
      });
      throw err;
    });
    if (file && fs.existsSync(file)) {
      log.debug(`deleting previous temp file: ${file}`);
      fs.unlinkSync(file);
    }
    return res.status(200).send({ data, report: output, local: true });
  }
});

// Create a new template
// TODO: replace this route with DB call in FE and delete here
app.use('/api/templatedemo', databaseMiddleware, authorizationLayer, async (req, res) => {
  log.debug(req.files.file.data);
  const templateFile = req.files;
  if (!templateFile) {
    res.status(400).send('template file not received');
  } else if (path.extname(templateFile.file.name) !== '.xlsx' && path.extname(templateFile.file.name) !== '.csv') {
    try {
      const wkbk = await TemplateFactory.loadFile(templateFile.file.data);
      const filePath = req.body.data ? req.body.data.path : `src/templates/${templateFile.file.name}`;
      const fileWriteStream = fs.createWriteStream(filePath);
      fileWriteStream.on('finish', () => {
        log.debug('file saved successfully');
      });
      fileWriteStream.end(templateFile.file.data);
      const data = await TemplateFactory.returnUI(templateFile.file.data, wkbk);
      res.status(200).send({ data, url: filePath });
    } catch (error) {
      log.debug(error);
      log.debug(path.extname(templateFile.file.name));
      res.status(400).send('Cannot parse template file. Please try using .xlsx or .csv');
    }
  } else {
    const filePath = req.body.data ? req.body.data.path : `src/templates/${templateFile.file.name}`;
    const fileWriteStream = fs.createWriteStream(filePath);
    fileWriteStream.on('finish', () => {
      log.debug('file saved successfully');
    });
    fileWriteStream.end(templateFile.file.data);
    const data = await TemplateFactory.returnUI(templateFile.file.data);
    // log.debug(data);
    if (!data) {
      log.warn('no data');
      res.status(400).send('No data parsed from template');
    } else {
      // log.debug(data); // TODO: save file somewhere and return the URL for it or an ID or something
      res.status(200).send({ data, url: filePath });
    }
  }
});

// Endpoint that gets checked by pingdom to alert on aging tasks
// https://my.pingdom.com/app/reports/uptime#check=7557502
app.get('/api/reviewTask/status', databaseMiddleware, authorizationLayer, async (req, res) => {
  try {
    const age = parseInt(req.query.age, 10);
    const count = await ReviewTaskService.agingTasksCount(age);
    res.status(200).send(`${count} review tasks over ${age} minutes old`);
  } catch (error) {
    log.debug(error);
    res.status(400).send('Error getting review task info.');
  }
});

app.use('/api/refreshToken', databaseMiddleware, async (req, res) => {
  const FIFTEEN_MINUTES = 60 * 15 * 1000;

  if (!req.cookies.refresh_token) {
    res.status(401).send('Unauthorized');
    return;
  }

  const refreshToken = req.cookies.refresh_token;

  try {
    const currentUser = await AuthService.findByRefreshToken(refreshToken);

    const jwt = await AuthService.jwt(currentUser);

    // reset refreshToken expiry
    res.cookie('refresh_token', refreshToken, {
      maxAge: config.jwt.refreshExpiry * 1000,
      httpOnly: true,
      secure: req.protocol === 'https',
      domain: req.hostname || 'ixc-local.com',
    });

    let currentSessionStart = req.cookies.session;
    const lastActivity = req.cookies.last_activity;

    if (!currentSessionStart || new Date() - new Date(Number.parseInt(lastActivity, 10)) > FIFTEEN_MINUTES) currentSessionStart = new Date().valueOf();

    res.cookie('last_activity', new Date().valueOf(), {
      httpOnly: true,
      secure: true,
      domain: req.hostname || 'ixc-local.com',
    });
    res.cookie('session', currentSessionStart, {
      httpOnly: true,
      secure: true,
      domain: req.hostname || 'ixc-local.com',
    });

    res.status(200).send(jwt);
  } catch (error) {
    log.error('Error while verifying refresh token:', error);

    res.clearCookie('refresh_token', {
      httpOnly: true,
      secure: req.protocol === 'https',
      domain: req.hostname || 'ixc-local.com',
    });

    res.status(401).send('Unauthorized');
  }
});

app.use('/api/pwaToken', databaseMiddleware, authorizationLayer, async (req, res) => {
  if (!req.cookies.pwa_refresh_token) {
    return res.status(401).send('Unauthorized');
  }

  const refreshToken = req.cookies.pwa_refresh_token;

  try {
    const currentUser = await AuthService.findByRefreshToken(refreshToken);

    const jwt = await AuthService.jwt(currentUser);

    // reset refreshToken expiry
    res.cookie('pwa_refresh_token', refreshToken, {
      httpOnly: true,
      secure: req.protocol === 'https',
      domain: req.hostname || 'ixc-local.com',
    });

    res.status(200).send(jwt);
  } catch (error) {
    log.error('Error while verifying PWA refresh token:', error);

    res.clearCookie('pwa_refresh_token', {
      httpOnly: true,
      secure: req.protocol === 'https',
      domain: req.hostname || 'ixc-local.com',
    });

    res.status(401).send('Unauthorized');
  }
});

app.use('/api/deviceToken', async (req, res) => {
  const deviceToken = 'footoken';

  try {
    res.cookie('device_token', deviceToken, {
      httpOnly: true,
      secure: req.protocol === 'https',
      domain: req.hostname || 'ixc-local.com',
    });

    res.status(200).send(deviceToken);
  } catch (error) {
    log.error('Error while retrieving device token:', error);

    res.clearCookie('device_token', {
      httpOnly: true,
      secure: req.protocol === 'https',
      domain: req.hostname || 'ixc-local.com',
    });

    res.status(401).send('Unauthorized');
  }
});

// Define endpoint for handling events delivered from external systems
// See /services/webhookConsumer/src/services/ixcApi.js
app.post('/api/event', express.json(), async (req, res) => {
  // Extract event data and validate
  const event = req.body;

  // Process event
  const result = await ExternalEventService.processEvent({
    logger: log,
    analyticsClient,
    subscriptionWorker,
    event,
  });

  // Prepare the response
  let status = result.status || 200;
  const message = result.message || `The event was processed successfully`;
  const body = { message };

  if (result.error) {
    status = 500;
    body.message = `An error was encountered while processing the event`;
    if (result.error) body.error = result.error;
  }
  res.status(status).send(body);
});

const eventEmitter = new EventEmitter();
app.set('eventEmitter', eventEmitter);

// Helper function used to filter what kind of errors are reported to Amplitude
function shouldReportError(error) {
  if (!error) return false;
  if (error.message === 'You are not logged in') return false;
  return error.type !== 'UnauthorizedError' && error.type !== 'ForbiddenError';
}

app.all(
  '/api',
  databaseMiddleware,
  authorizationLayer,
  createHandler({
    schema,
    context: (req) => ({
      req: req.raw,
      config,
      res: req.raw.res,
      analyticsClient,
      billingClient,
      cognitoClient,
      crmClient,
      subscriptionWorker,
      sphinxClient,
      rolesMapper,
      currentUser: req.raw.currentUser,
      language: req.raw.headers['accept-language'] && req.raw.headers['accept-language'].includes('en') ? 'en' : req.raw.headers['accept-language'],
      logger: log,
      eventEmitter,
    }),
  }),
);

// Uncomment to expose the build dir of the frontend as the root / of the server
// Note, that doing this will cause 404 test to fail, because React will route the
// requests to a 404ErrorPage with status 200.
/*
const frontendDir = path.join(
  __dirname,
  '../../../frontend/build',
);

if (fs.existsSync(frontendDir)) {
  app.use('/', express.static(frontendDir));

  app.get('*', function(request, response) {
    response.sendFile(
      path.resolve(frontendDir, 'index.html'),
    );
  });
}
*/

// TODO: This valid hostname list should be the same as the one used for CORs on line 35 above
app.use((req, res, next) => {
  if (config.uploadRemote) {
    req.saveFilesTo = 's3';
  } else {
    req.saveFilesTo = 'local-fs';
  }
  next();
});

// This error handler must be BEFORE any other error middleware and AFTER all non-error middleware
app.use(Sentry.Handlers.errorHandler());

// Middleware to re-format cognito errors
app.use((error, req, res, next) => {
  if (!error || error.type !== 'cognito') {
    next(error);
  }
  let code = error.code || 500;
  if (error.httpStatusCode) {
    code = error.httpStatusCode;
  }
  if (error.$metadata && error.$metadata.httpStatusCode) {
    code = error.$metadata.httpStatusCode;
  }
  next({
    ...error,
    name: error.name,
    message: error.message,
    code,
  });
});

// Middleware to capture and report backend errors to Amplitude
app.use((error, req, res, next) => {
  const reqSummary = {
    'user-agent': req.header('user-agent'),
    'route.path': req.route ? req.route.path : undefined,
    method: req.method, // "DELETE"
    protocol: req.protocol, // "https"
    subdomains: req.subdomains, // "['ocean']"
    hostname: req.hostname, // "example.com"
    path: req.path, // "/creatures"
    originalUrl: req.originalUrl, // "/creatures?filter=sharks"
  };
  log.debug('API reqSummary', reqSummary);
  log.error('API errorTrap', error);
  log.info('API errorTrap', reqSummary, error);
  if (!isTestEnv && shouldReportError(error)) {
    analyticsClient.track({
      req,
      ixcEvent: analyticsClient.events.errorsNonspecific,
      userId: req.currentUser && req.currentUser.id ? req.currentUser.id : 'unauthenticated',
      error,
      properties: {
        ...reqSummary,
      },
    });
  }

  // If an error code isn't set, default to 500 Internal Server Errror
  const code = error.code || 500;
  // TODO: Handle specific error codes
  // if (error.code === 301) return res.status(301).redirect('/not-found');

  if (res.headersSent) {
    log.warn('Headers have already been sent to the client');
    // delegate to the default Express error handler, when the headers have already been sent to the client
    return next(error);
  }
  return res.status(code).json({ error });
});

module.exports = app;
