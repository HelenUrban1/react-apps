/**
 * Provides a way to use this definition in multiple schemas without duplicating it.
 */
const AccountSiteUserIds = `
  accountId: String!
  siteId: String!
  userId: String!
`;

module.exports = AccountSiteUserIds;
