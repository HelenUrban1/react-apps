/**
 * @see {@link https://inspectionxpert.atlassian.net/wiki/x/BYCtN}
 */
const schema = `
  type ApiKey {
    apiKey: String!
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
