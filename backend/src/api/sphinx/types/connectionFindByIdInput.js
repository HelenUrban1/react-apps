/**
 * @see {@link https://inspectionxpert.atlassian.net/wiki/x/BYCtN}
 */
const schema = `
  input ConnectionFindByIdInput {
    connectionId: String!
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
