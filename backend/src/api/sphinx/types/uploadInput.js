/**
 * @see {@link https://inspectionxpert.atlassian.net/wiki/x/BYCtN}
 */
const schema = `
  input UploadInput {
    apiKey: String!
    payload: String
    connectionId: String
    partId: String
    firstArticleId: String
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
