/**
 * @see {@link https://inspectionxpert.atlassian.net/wiki/x/BYCtN}
 */
const schema = `
  type UploadSuccess {
    destination: String!
    data: String
  }
  type UploadOutput {
    succeeded: [UploadSuccess]!
    failed: [String]!
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
