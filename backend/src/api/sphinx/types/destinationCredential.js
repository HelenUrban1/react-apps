/**
 * @see {@link https://inspectionxpert.atlassian.net/wiki/x/BYCtN}
 */

const DestinationCredentialType = `
  destinationId: String!
  userName: String
  password: String
  company: String
`;

const schema = `
  type DestinationCredential {
    ${DestinationCredentialType}
  }

  input DestinationCredentialInput {
    ${DestinationCredentialType}
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
