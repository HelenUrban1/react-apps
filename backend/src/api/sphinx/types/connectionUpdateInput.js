/**
 * @see {@link https://inspectionxpert.atlassian.net/wiki/x/BYCtN}
 */

const schema = `
  input ConnectionUpdateInput {
    connectionId: String!
    sourceId: String
    destinationIds: [String]
    destinationCredentials: [DestinationCredentialInput]
    transformations: [TransformationInput]
    enabled: Boolean
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
