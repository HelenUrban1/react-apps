/**
 * @see {@link https://inspectionxpert.atlassian.net/wiki/x/BYCtN}
 */
const TransformationType = `
  id: String!
  priority: Int!
`;

const schema = `
  type Transformation {
    ${TransformationType}
  }

  input TransformationInput {
    ${TransformationType}
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
