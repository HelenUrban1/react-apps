/**
 * @see {@link https://inspectionxpert.atlassian.net/wiki/x/BYCtN}
 */
const schema = `
  type Connection {
    id: String!
    apiKey: String!
    accountId: String!
    siteId: String!
    userId: String!
    accountIdIndexSortKey: String!
    enabled: Boolean!
    created: String!
    updated: String!
    sourceId: String
    destinationIds: [String]
    destinationCredentials: [DestinationCredential]
    transformations: [Transformation]
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
