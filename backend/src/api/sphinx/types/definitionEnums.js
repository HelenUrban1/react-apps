/**
 * @see {@link https://inspectionxpert.atlassian.net/wiki/x/BYCtN}
 */
const schema = `
  enum DefinitionItemTypeEnum {
    source
    transformation
    destination
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
