const AccountSiteUserIds = require('./accountSiteUserIds');

/**
 * @see {@link https://inspectionxpert.atlassian.net/wiki/x/BYCtN}
 */
const schema = `
  input ConnectionFindInput {
    ${AccountSiteUserIds}
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
