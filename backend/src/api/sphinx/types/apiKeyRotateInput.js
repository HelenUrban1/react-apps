/**
 * @see {@link https://inspectionxpert.atlassian.net/wiki/x/BYCtN}
 */
const schema = `
  input ApiKeyRotateInput {
    apiKey: String!
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
