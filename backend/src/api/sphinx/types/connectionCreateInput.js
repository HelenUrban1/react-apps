/**
 * @see {@link https://inspectionxpert.atlassian.net/wiki/x/BYCtN}
 */

const schema = `
  input ConnectionCreateInput {
    accountId: String!
    siteId: String!
    userId: String!
    sourceId: String!
    enabled: Boolean
    created: String
    updated: String
    destinationIds: [String]
    destinationCredentials: [DestinationCredentialInput]
    transformations: [TransformationInput]
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
