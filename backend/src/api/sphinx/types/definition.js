/**
 * @see {@link https://inspectionxpert.atlassian.net/wiki/x/BYCtN}
 */
const schema = `
  type Definition {
    id: String!
    name: String!
    type: DefinitionItemTypeEnum!
    enabled: Boolean!
    version: String
    created: String!
    updated: String!
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
