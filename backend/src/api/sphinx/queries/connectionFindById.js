const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions').values;

const schema = `
  connectionFindById(data: ConnectionFindByIdInput!): Connection!
`;

const resolver = {
  connectionFindById: async (root, args, context) => {
    new PermissionChecker(context).validateHas(permissions.sphinxApiKeysAccess);

    return context.sphinxClient.connectionsGetById(args.data);
  },
};

exports.schema = schema;
exports.resolver = resolver;
