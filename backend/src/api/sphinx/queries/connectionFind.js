const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions').values;

const schema = `
  connectionFind(data: ConnectionFindInput!): [Connection!]
`;

const resolver = {
  connectionFind: async (root, args, context) => {
    new PermissionChecker(context).validateHas(permissions.sphinxApiKeysAccess);

    return context.sphinxClient.connectionsGet(args.data);
  },
};

exports.schema = schema;
exports.resolver = resolver;
