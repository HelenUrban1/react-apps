const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions').values;

const schema = `
  connectionHealth(data: ApiKeyRotateInput!): String!
`;

const resolver = {
  connectionHealth: async (root, args, context) => {
    new PermissionChecker(context).validateHas(permissions.sphinxApiKeysAccess);

    return context.sphinxClient.healthGet(args.data);
  },
};

exports.schema = schema;
exports.resolver = resolver;
