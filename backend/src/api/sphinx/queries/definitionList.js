const { log } = require('../../../logger');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions').values;

const schema = `
  definitionList: [Definition!]!
`;

const resolver = {
  definitionList: async (root, args, context) => {
    new PermissionChecker(context).validateHas(permissions.sphinxApiKeysAccess);
    log.debug('SPHINX.resolver.definitionList PermissionChecker PASSED');
    return context.sphinxClient.definitionsGet();
  },
};

exports.schema = schema;
exports.resolver = resolver;
