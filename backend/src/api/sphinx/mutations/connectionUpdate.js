const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions').values;

const schema = `
  connectionUpdate(data: ConnectionUpdateInput!): Connection!
`;

const resolver = {
  connectionUpdate: async (root, args, context) => {
    new PermissionChecker(context).validateHas(permissions.sphinxApiKeysAccess);

    return context.sphinxClient.connectionsUpdate(args.data);
  },
};

exports.schema = schema;
exports.resolver = resolver;
