const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions').values;

const schema = `
  connectionCreate(data: ConnectionCreateInput!): Connection!
`;

const resolver = {
  connectionCreate: async (root, args, context) => {
    new PermissionChecker(context).validateHas(permissions.sphinxApiKeysAccess);

    return context.sphinxClient.connectionsCreate(args.data);
  },
};

exports.schema = schema;
exports.resolver = resolver;
