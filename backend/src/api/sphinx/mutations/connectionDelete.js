const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions').values;

const schema = `
  connectionDelete(data: ConnectionDeleteInput!): String!
`;

const resolver = {
  connectionDelete: async (root, args, context) => {
    new PermissionChecker(context).validateHas(permissions.sphinxApiKeysAccess);

    return context.sphinxClient.connectionsDelete(args.data);
  },
};

exports.schema = schema;
exports.resolver = resolver;
