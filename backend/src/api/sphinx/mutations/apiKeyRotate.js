const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions').values;

const schema = `
  apiKeyRotate(data: ApiKeyRotateInput!): String!
`;

const resolver = {
  apiKeyRotate: async (root, args, context) => {
    new PermissionChecker(context).validateHas(permissions.sphinxApiKeysAccess);

    return context.sphinxClient.apiKeyRotate(args.data);
  },
};

exports.schema = schema;
exports.resolver = resolver;
