const PermissionChecker = require('../../../services/iam/permissionChecker');
const PartService = require('../../../services/partService');
const PartFormatter = require('../../../services/shared/utils/partFormatter');
const permissions = require('../../../security/permissions').values;
const { log } = require('../../../logger');

const schema = `
  upload(data: UploadInput!): UploadOutput!
`;

const resolver = {
  upload: async (root, args, context) => {
    try {
      log.debug('SPHINX.resolver.upload CALLED');
      new PermissionChecker(context).validateHas(permissions.sphinxApiKeysAccess);
      log.debug('SPHINX.resolver.upload PermissionChecker PASSED');

      // get part and convert into net-inspect xml
      const part = await new PartService(context).findByIdForExport(args.data.partId);

      const formattedXmlPart = await PartFormatter.netInspectXML(part);
      const results = await context.sphinxClient.upload({ apiKey: args.data.apiKey, payload: formattedXmlPart, firstArticleId: args.data.firstArticleId });
      log.debug('SPHINX.resolver.upload.results', results);

      // Save latest FAI report ID on part record
      // {data: {upload: {succeeded: [{destination: "Net-Inspect", data: "7449496"}], failed: []}}}
      if (results.succeeded && results.succeeded[0]) {
        const result = results.succeeded[0];
        if (result.destination === 'Net-Inspect' && result.data) {
          const partUpdateResult = await new PartService(context).update(part.id, { lastReportId: result.data });
          log.debug('SPHINX.resolver.upload.partUpdateResult', partUpdateResult);
        }
      }

      return results;
    } catch (error) {
      log.debug('SPHINX.resolver.upload.error');
      log.error(error);
      return error;
    }
  },
};

exports.schema = schema;
exports.resolver = resolver;
