const MarkerService = require('../../../services/markerService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions')
  .values;

const schema = `
  markerFind(id: String!): Marker!
`;

const resolver = {
  markerFind: async (root, args, context) => {
    new PermissionChecker(context)
      .validateHas(permissions.markerRead);

    return new MarkerService(context).findById(args.id);
  },
};

exports.schema = schema;
exports.resolver = resolver;
