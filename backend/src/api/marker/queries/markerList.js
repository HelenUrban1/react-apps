const MarkerService = require('../../../services/markerService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions').values;
const graphqlSelectRequestedAttributes = require('../../shared/utils/graphqlSelectRequestedAttributes');

const schema = `
  markerList(filter: MarkerFilterInput, limit: Int, offset: Int, orderBy: MarkerOrderByEnum, paranoid: Boolean): MarkerPage!
`;

const resolver = {
  markerList: async (root, args, context, info) => {
    new PermissionChecker(context).validateHas(permissions.markerRead);

    return new MarkerService(context).findAndCountAll({
      ...args,
      requestedAttributes: graphqlSelectRequestedAttributes(info, 'rows'),
    });
  },
};

exports.schema = schema;
exports.resolver = resolver;
