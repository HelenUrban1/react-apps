const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions')
  .values;
const MarkerService = require('../../../services/markerService');

const schema = `
  markerAutocomplete(query: String, limit: Int): [AutocompleteOption!]!
`;

const resolver = {
  markerAutocomplete: async (root, args, context, info) => {
    new PermissionChecker(context)
      .validateHas(permissions.markerAutocomplete);

    return new MarkerService(context).findAllAutocomplete(
      args.query,
      args.limit,
    );
  },
};

exports.schema = schema;
exports.resolver = resolver;
