const MarkerService = require('../../../services/markerService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions')
  .values;

const schema = `
  markerUpdate(id: String!, data: MarkerInput!): Marker!
`;

const resolver = {
  markerUpdate: async (root, args, context) => {
    new PermissionChecker(context)
      .validateHas(permissions.markerEdit);

    return new MarkerService(context).update(
      args.id,
      args.data
    );
  },
};

exports.schema = schema;
exports.resolver = resolver;
