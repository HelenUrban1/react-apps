const MarkerService = require('../../../services/markerService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions').values;

const schema = `
  markerDestroy(ids: [String!]!): [String]
`;

const resolver = {
  markerDestroy: async (root, args, context) => {
    new PermissionChecker(context).validateHas(permissions.markerDestroy);

    await new MarkerService(context).destroyAll(args.ids);

    return args.ids;
  },
};

exports.schema = schema;
exports.resolver = resolver;
