const MarkerService = require('../../../services/markerService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions')
  .values;

const schema = `
  markerCreate(data: MarkerInput!): Marker!
`;

const resolver = {
  markerCreate: async (root, args, context) => {
    new PermissionChecker(context)
      .validateHas(permissions.markerCreate);

    return new MarkerService(context).create(
      args.data
    );
  },
};

exports.schema = schema;
exports.resolver = resolver;
