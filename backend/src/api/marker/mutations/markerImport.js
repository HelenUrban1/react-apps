const MarkerService = require('../../../services/markerService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions')
  .values;

const schema = `
  markerImport(data: MarkerInput!, importHash: String!): Boolean
`;

const resolver = {
  markerImport: async (root, args, context) => {
    new PermissionChecker(context)
      .validateHas(permissions.markerImport);

    await new MarkerService(context).import(
      args.data,
      args.importHash
    );

    return true;
  },
};

exports.schema = schema;
exports.resolver = resolver;
