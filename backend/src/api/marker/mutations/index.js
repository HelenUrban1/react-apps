module.exports = [
  require('./markerCreateDefaults'), //
  require('./markerCreate'),
  require('./markerDestroy'),
  require('./markerUpdate'),
  require('./markerImport'),
];
