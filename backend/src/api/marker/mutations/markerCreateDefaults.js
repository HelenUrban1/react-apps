const MarkerService = require('../../../services/markerService');

const schema = `
  markerCreateDefaults(accountId: String!): Boolean
`;

const resolver = {
  markerCreateDefaults: async (root, args, context) => {
    await new MarkerService(context).createDefaults(args.accountId);
    return true;
  },
};

exports.schema = schema;
exports.resolver = resolver;
