module.exports = [
  require('./marker'),
  require('./markerInput'),
  require('./markerFilterInput'),
  require('./markerOrderByEnum'),
  require('./markerPage'),
  require('./markerEnums'),
];
