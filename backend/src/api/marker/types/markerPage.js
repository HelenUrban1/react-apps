const schema = `
  type MarkerPage {
    rows: [Marker!]!
    count: Int!
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
