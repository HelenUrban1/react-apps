const schema = `
  type Marker {
    id: String!
    markerStyleName: String
    shape: MarkerShapeEnum
    defaultPosition: String
    hasLeaderLine: Boolean
    fontSize: Int
    fontColor: String
    fillColor: String
    borderColor: String
    borderWidth: Int
    borderStyle: MarkerStrokeStyleEnum
    leaderLineColor: String
    leaderLineWidth: Int
    leaderLineStyle: MarkerStrokeStyleEnum
    createdAt: DateTime
    updatedAt: DateTime
    deletedAt: DateTime
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
