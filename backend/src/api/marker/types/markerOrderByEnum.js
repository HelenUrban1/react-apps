const schema = `
  enum MarkerOrderByEnum {
    id_ASC
    id_DESC
    markerStyleName_ASC
    markerStyleName_DESC
    shape_ASC
    shape_DESC
    defaultPosition_ASC
    defaultPosition_DESC
    hasLeaderLine_ASC
    hasLeaderLine_DESC
    fontSize_ASC
    fontSize_DESC
    fontColor_ASC
    fontColor_DESC
    fillColor_ASC
    fillColor_DESC
    borderColor_ASC
    borderColor_DESC
    borderWidth_ASC
    borderWidth_DESC
    borderStyle_ASC
    borderStyle_DESC
    leaderLineColor_ASC
    leaderLineColor_DESC
    leaderLineWidth_ASC
    leaderLineWidth_DESC
    leaderLineStyle_ASC
    leaderLineStyle_DESC
    createdAt_ASC
    createdAt_DESC
    deletedAt_ASC
    deletedAt_DESC
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
