const schema = `
  input MarkerFilterInput {
    id: [ String ]
    markerStyleName: String
    shape: MarkerShapeEnum
    defaultPosition: String
    hasLeaderLine: Boolean
    fontSizeRange: [ Int ]
    fontColor: String
    fillColor: String
    borderColor: String
    borderStyle: MarkerStrokeStyleEnum
    borderWidthRange: [ Int ]
    leaderLineColor: String
    leaderLineStyle: MarkerStrokeStyleEnum
    leaderLineWidthRange: [ Int ]
    createdAtRange: [ DateTime ]
    deletedAtRange: [ DateTime ]
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
