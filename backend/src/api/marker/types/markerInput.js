const schema = `
  input MarkerInput {
    markerStyleName: String!
    shape: MarkerShapeEnum!
    defaultPosition: String!
    hasLeaderLine: Boolean
    fontSize: Int!
    fontColor: String!
    fillColor: String!
    borderColor: String!
    borderWidth: Int!
    borderStyle: MarkerStrokeStyleEnum!
    leaderLineColor: String!
    leaderLineWidth: Int!
    leaderLineStyle: MarkerStrokeStyleEnum!
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
