const schema = `
  enum MarkerShapeEnum {
    Circle
    Pin
    Triangle
    Square
    Diamond
    Pentagon
    Hexagon
    Heptagon
    Octagon
    Star
  }

  enum MarkerStrokeStyleEnum {
    Solid
    Dashed
    Dotted
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
