const schema = `
  input DrawingSheetInAccountInput {
    pageIndex: Int!
    previousRevision: String
    nextRevision: String
    originalSheet: String
    foundCaptureCount: Int
    acceptedCaptureCount: Int
    acceptedTimeoutCaptureCount: Int
    rejectedCaptureCount: Int
    reviewedCaptureCount: Int
    reviewedTimeoutCaptureCount: Int
    markerSize: Int
    useGrid: Boolean
    part: String!
    drawing: String!
    accountId: String!
    siteId: String!
    defaultLinearTolerances: String
    defaultAngularTolerances: String
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
