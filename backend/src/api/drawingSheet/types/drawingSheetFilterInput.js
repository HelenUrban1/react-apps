const schema = `
  input DrawingSheetFilterInput {
    id: String
    pageName: String
    previousRevision: String
    nextRevision: String
    originalSheet: String
    pageIndexRange: [ Int ]
    heightRange: [ Int ]
    widthRange: [ Int ]
    rotation: String
    grid: String
    useGrid: Boolean
    displayLines: Boolean
    displayLabels: Boolean
    foundCaptureCountRange: [ Int ]
    acceptedCaptureCountRange: [ Int ]
    acceptedTimeoutCaptureCountRange: [ Int ]
    rejectedCaptureCountRange: [ Int ]
    reviewedCaptureCountRange: [ Int ]
    reviewedTimeoutCaptureCountRange: [ Int ]
    part: String
    drawing: String
    createdAtRange: [ DateTime ]
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
