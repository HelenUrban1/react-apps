const schema = `
  input DrawingSheetInput {
    pageName: String
    previousRevision: String
    nextRevision: String
    originalSheet: String
    pageIndex: Int!
    height: Int
    width: Int
    markerSize: Int
    rotation: String
    grid: String
    useGrid: Boolean
    displayLines: Boolean
    displayLabels: Boolean
    foundCaptureCount: Int
    acceptedCaptureCount: Int
    acceptedTimeoutCaptureCount: Int
    rejectedCaptureCount: Int
    reviewedCaptureCount: Int
    reviewedTimeoutCaptureCount: Int
    part: String!
    drawing: String!
    characteristics: [ String ]
    defaultLinearTolerances: String
    defaultAngularTolerances: String
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
