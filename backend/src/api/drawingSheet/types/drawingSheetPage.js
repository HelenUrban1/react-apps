const schema = `
  type DrawingSheetPage {
    rows: [DrawingSheet!]!
    count: Int!
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
