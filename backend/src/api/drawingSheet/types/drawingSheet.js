const schema = `
  type DrawingSheet {
    id: String!
    previousRevision: String
    nextRevision: String
    originalSheet: String
    pageName: String
    pageIndex: Int
    height: Int
    width: Int
    markerSize: Int
    rotation: String
    grid: String
    useGrid: Boolean
    displayLines: Boolean
    displayLabels: Boolean
    foundCaptureCount: Int
    acceptedCaptureCount: Int
    acceptedTimeoutCaptureCount: Int
    rejectedCaptureCount: Int
    reviewedCaptureCount: Int
    reviewedTimeoutCaptureCount: Int
    part: Part
    drawing: Drawing
    characteristics: [ Characteristic ]
    defaultLinearTolerances: String
    defaultAngularTolerances: String
    createdAt: DateTime
    updatedAt: DateTime
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
