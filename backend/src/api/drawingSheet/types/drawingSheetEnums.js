const schema = `
enum DrawingSheetStatusEnum {
    In_Progress
    Completed
    Error
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
