const schema = `
  enum DrawingSheetOrderByEnum {
    id_ASC
    id_DESC
    pageIndex_ASC
    pageIndex_DESC
    pageName_ASC
    pageName_DESC
    height_ASC
    height_DESC
    width_ASC
    width_DESC
    markerSize_ASC
    markerSize_DESC
    rotation_ASC
    rotation_DESC
    grid_ASC
    grid_DESC
    useGrid_ASC
    useGrid_DESC
    displayLines_ASC
    displayLines_DESC
    displayLabels_ASC
    displayLabels_DESC
    createdAt_ASC
    createdAt_DESC
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
