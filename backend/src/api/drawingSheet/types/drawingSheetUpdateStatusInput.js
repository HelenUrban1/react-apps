const schema = `
  input DrawingSheetUpdateStatusInput {
    pageIndex: Int!
    part: String!
    drawing: String!
    accountId: String!
    siteId: String!       
    autoMarkupStatus: DrawingSheetStatusEnum!
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
