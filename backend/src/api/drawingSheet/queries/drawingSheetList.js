const DrawingSheetService = require('../../../services/drawingSheetService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions')
  .values;
const graphqlSelectRequestedAttributes = require('../../shared/utils/graphqlSelectRequestedAttributes');

const schema = `
  drawingSheetList(filter: DrawingSheetFilterInput, limit: Int, offset: Int, orderBy: DrawingSheetOrderByEnum): DrawingSheetPage!
`;

const resolver = {
  drawingSheetList: async (root, args, context, info) => {
    new PermissionChecker(context)
      .validateHas(permissions.drawingSheetRead);

    return new DrawingSheetService(context).findAndCountAll({
      ...args,
      requestedAttributes: graphqlSelectRequestedAttributes(
        info,
        'rows',
      ),
    });
  },
};

exports.schema = schema;
exports.resolver = resolver;
