const DrawingSheetService = require('../../../services/drawingSheetService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions')
  .values;

const schema = `
  drawingSheetFind(id: String!): DrawingSheet!
`;

const resolver = {
  drawingSheetFind: async (root, args, context) => {
    new PermissionChecker(context)
      .validateHas(permissions.drawingSheetRead);

    return new DrawingSheetService(context).findById(args.id);
  },
};

exports.schema = schema;
exports.resolver = resolver;
