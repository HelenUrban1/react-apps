const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions')
  .values;
const DrawingSheetService = require('../../../services/drawingSheetService');

const schema = `
  drawingSheetAutocomplete(query: String, limit: Int): [AutocompleteOption!]!
`;

const resolver = {
  drawingSheetAutocomplete: async (root, args, context, info) => {
    new PermissionChecker(context)
      .validateHas(permissions.drawingSheetAutocomplete);

    return new DrawingSheetService(context).findAllAutocomplete(
      args.query,
      args.limit,
    );
  },
};

exports.schema = schema;
exports.resolver = resolver;
