const DrawingSheetService = require('../../../services/drawingSheetService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions').values;

const schema = `
  drawingSheetIncrementInAccount(data: DrawingSheetInAccountInput!): DrawingSheet
`;

const resolver = {
  drawingSheetIncrementInAccount: async (root, args, context) => {
    new PermissionChecker(context).validateHas(permissions.drawingSheetEdit);

    return new DrawingSheetService(context).incrementInAccount(args.data);
  },
};

exports.schema = schema;
exports.resolver = resolver;
