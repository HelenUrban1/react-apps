const DrawingSheetService = require('../../../services/drawingSheetService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions').values;

const schema = `
  drawingSheetStatusUpdate(data: DrawingSheetUpdateStatusInput!): Boolean!
`;

const resolver = {
  drawingSheetStatusUpdate: async (root, args, context) => {
    new PermissionChecker(context).validateHas(permissions.drawingSheetEdit);

    return new DrawingSheetService(context).statusUpdateInAccount(args.data);
  },
};

exports.schema = schema;
exports.resolver = resolver;
