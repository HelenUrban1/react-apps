const DrawingSheetService = require('../../../services/drawingSheetService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions')
  .values;

const schema = `
  drawingSheetImport(data: DrawingSheetInput!, importHash: String!): Boolean
`;

const resolver = {
  drawingSheetImport: async (root, args, context) => {
    new PermissionChecker(context)
      .validateHas(permissions.drawingSheetImport);

    await new DrawingSheetService(context).import(
      args.data,
      args.importHash
    );

    return true;
  },
};

exports.schema = schema;
exports.resolver = resolver;
