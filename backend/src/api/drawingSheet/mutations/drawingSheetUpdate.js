const DrawingSheetService = require('../../../services/drawingSheetService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions')
  .values;

const schema = `
  drawingSheetUpdate(id: String!, data: DrawingSheetInput!): DrawingSheet!
`;

const resolver = {
  drawingSheetUpdate: async (root, args, context) => {
    new PermissionChecker(context)
      .validateHas(permissions.drawingSheetEdit);

    return new DrawingSheetService(context).update(
      args.id,
      args.data
    );
  },
};

exports.schema = schema;
exports.resolver = resolver;
