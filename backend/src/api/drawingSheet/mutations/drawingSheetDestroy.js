const DrawingSheetService = require('../../../services/drawingSheetService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions')
  .values;

const schema = `
  drawingSheetDestroy(ids: [String!]!): Boolean
`;

const resolver = {
  drawingSheetDestroy: async (root, args, context) => {
    new PermissionChecker(context)
      .validateHas(permissions.drawingSheetDestroy);

    await new DrawingSheetService(context).destroyAll(
      args.ids
    );

    return true;
  },
};

exports.schema = schema;
exports.resolver = resolver;
