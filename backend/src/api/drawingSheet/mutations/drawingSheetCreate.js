const DrawingSheetService = require('../../../services/drawingSheetService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions').values;

const schema = `
  drawingSheetCreate(data: DrawingSheetInput!): DrawingSheet!
`;

const resolver = {
  drawingSheetCreate: async (root, args, context) => {
    new PermissionChecker(context).validateHas(permissions.drawingSheetCreate);

    return new DrawingSheetService(context).create(args.data);
  },
};

exports.schema = schema;
exports.resolver = resolver;
