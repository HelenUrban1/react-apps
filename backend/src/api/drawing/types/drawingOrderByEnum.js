const schema = `
  enum DrawingOrderByEnum {
    id_ASC
    id_DESC
    sheetCount_ASC
    sheetCount_DESC
    documentType_ASC
    documentType_DESC
    name_ASC
    name_DESC
    number_ASC
    number_DESC
    revision_ASC
    revision_DESC
    linearUnit_ASC
    linearUnit_DESC
    angularUnit_ASC
    angularUnit_DESC
    markerOptions_ASC
    markerOptions_DESC
    gridOptions_ASC
    gridOptions_DESC
    linearTolerances_ASC
    linearTolerances_DESC
    tags_ASC
    tags_DESC
    angularTolerances_ASC
    angularTolerances_DESC
    createdAt_ASC
    createdAt_DESC
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
