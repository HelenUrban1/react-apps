const schema = `
  type DrawingPage {
    rows: [Drawing!]!
    count: Int!
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
