const schema = `
  input DrawingInput {
    part: String
    status: DrawingStatusEnum
    documentType: DocumentTypeEnum
    file: [ FileInput! ]!
    drawingFile: FileInput
    sheetCount: Int!
    name: String!
    number: String
    revision: String
    previousRevision: String
    nextRevision: String
    originalDrawing: String
    linearUnit: String!
    angularUnit: String!
    markerOptions: String!
    gridOptions: String
    linearTolerances: String!
    tags: String!
    angularTolerances: String!
    sheets: [ String! ]
    characteristics: [ String ]
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
