const schema = `
  type Drawing {
    id: String!
    site: Site
    status:DrawingStatusEnum!
    documentType: DocumentTypeEnum!
    part: Part
    file: [ File! ]
    drawingFile: File
    sheetCount: Int
    name: String
    number: String
    revision: String
    previousRevision: String
    nextRevision: String
    originalDrawing: String
    linearUnit: String
    angularUnit: String
    markerOptions: String
    gridOptions: String
    linearTolerances: String
    tags: String
    angularTolerances: String
    sheets: [ DrawingSheet! ]
    characteristics: [ Characteristic ]
    createdAt: DateTime
    updatedAt: DateTime
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
