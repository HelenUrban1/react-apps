const schema = `
  type DrawingRevision {
    part: Part!
    drawing: Drawing!
    sheets: [DrawingSheet!]!
    characteristics: [Characteristic]!
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
