const schema = `
  input DrawingFilterInput {
    id: String
    site: String
    documentType: DocumentTypeEnum
    status: DrawingStatusEnum
    part: String
    sheetCount: Int
    name: String
    number: String
    revision: String
    previousRevision: String
    nextRevision: String
    originalDrawing: String
    linearUnit: String
    angularUnit: String
    markerOptions: String
    gridOptions: String
    linearTolerances: String
    tags: String
    angularTolerances: String
    createdAtRange: [ DateTime ]
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
