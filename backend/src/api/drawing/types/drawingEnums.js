const schema = `
enum DrawingStatusEnum {
  Active
  Deleted
}
enum DocumentTypeEnum {
  Drawing
  Materials
  Specifications
  Notes
  Other
}
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
