const RevisionService = require('../../../services/revisionService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions').values;

const schema = `
  drawingRevision(drawingId: String!, partId: String!): DrawingRevision
`;

const resolver = {
  drawingRevision: async (root, args, context) => {
    new PermissionChecker(context).validateHas(permissions.partCreate);
    return new RevisionService(context).createDrawingRevision(args.drawingId, args.partId);
  },
};

exports.schema = schema;
exports.resolver = resolver;
