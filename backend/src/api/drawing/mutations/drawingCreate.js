const DrawingService = require('../../../services/drawingService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions').values;
// const SubscriptionService = require('../../../services/subscriptionService');
// const config = require('../../../../config')();
// const { log } = require('../../../logger');

const schema = `
  drawingCreate(data: DrawingInput!): Drawing!
`;

const resolver = {
  drawingCreate: async (root, args, context) => {
    new PermissionChecker(context).validateHas(permissions.drawingCreate);

    // Don't block file upload if subscription update fails
    // NOTE: Deal creation moved to registerSubscription
    // TODO: Delete after functionality confirmed
    // try {
    //   const subServ = new SubscriptionService(context);
    //   const sub = await subServ.findByAccountId(context.req.currentUser.accountId, context);
    //   // TODO: Why pass in config data here instead in the subscriptionService
    //   const data = {
    //     crmPipelineName: config.crm.pipelineName,
    //     crmPipelineStage: config.crm.pipelineStage,
    //     crmDealType: config.crm.dealType,
    //     crmDealTrialType: config.crm.dealTrialType,
    //     accountId: context.req.currentUser.accountId,
    //     email: context.req.currentUser.email,
    //     trialStartedAt: sub.trialStartedAt,
    //     trialExpiresAt: sub.trialEndedAt,
    //   };
    //   subServ.update(sub.id, data, 'Register_Deal');
    // } catch (error) {
    //   log.error('drawingCreate subscriptionService.update failed');
    //   log.error(error);
    // }

    return new DrawingService(context).create(args.data);
  },
};

exports.schema = schema;
exports.resolver = resolver;
