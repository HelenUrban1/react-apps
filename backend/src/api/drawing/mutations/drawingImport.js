const DrawingService = require('../../../services/drawingService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions')
  .values;

const schema = `
  drawingImport(data: DrawingInput!, importHash: String!): Boolean
`;

const resolver = {
  drawingImport: async (root, args, context) => {
    new PermissionChecker(context)
      .validateHas(permissions.drawingImport);

    await new DrawingService(context).import(
      args.data,
      args.importHash
    );

    return true;
  },
};

exports.schema = schema;
exports.resolver = resolver;
