const { resolver } = require('./drawingCreate');
const DrawingService = require('../../../services/drawingService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const SubscriptionService = require('../../../services/subscriptionService');

jest.mock('../../../services/drawingService');
jest.mock('../../../services/subscriptionService', () => jest.fn());

const findByAccountId = jest.fn(() =>
  Promise.resolve({
    id: 'foo',
  })
);

const update = jest.fn(() => Promise.resolve('some thing'));

SubscriptionService.mockImplementation(() => ({ findByAccountId, update }));
jest.mock('../../../services/iam/permissionChecker');

const data = {
  // crmPipelineName: 'IX3 New Sales Pipeline',
  // crmPipelineStage: '2. Qualification',
  crmPipelineName: 'IX3 New Sales Pipeline',
  crmPipelineStage: '2. Qualification IX3',
  trialExpiresAt: undefined,
  trialStartedAt: undefined,
  crmDealType: 'newbusiness',
  crmDealTrialType: 'Time Limited',
  accountId: 'accountfoo',
  email: 'person@superwebs.muffin',
};

DrawingService.create = () => {
  'bar';
};

PermissionChecker.validateHas = () => true;

// Skipping this test, because DEAL creation moved back to registerSubscription
describe.skip('drawingSheetCreate', () => {
  it('triggers a HubSpot deal state change as a side effect', async () => {
    const context = {
      req: {
        currentUser: {
          accountId: 'accountfoo',
          email: 'person@superwebs.muffin',
        },
      },
    };

    await resolver.drawingCreate({}, {}, context);

    // expect(findByAccountId).toHaveBeenCalledWith('accountfoo', context);
    // Deal creation moved to registerSubscription
    // expect(update).toHaveBeenCalledWith('foo', data, 'Register_Deal');
  });
});
