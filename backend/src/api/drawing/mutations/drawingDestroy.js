const DrawingService = require('../../../services/drawingService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions')
  .values;

const schema = `
  drawingDestroy(ids: [String!]!): Boolean
`;

const resolver = {
  drawingDestroy: async (root, args, context) => {
    new PermissionChecker(context)
      .validateHas(permissions.drawingDestroy);

    await new DrawingService(context).destroyAll(
      args.ids
    );

    return true;
  },
};

exports.schema = schema;
exports.resolver = resolver;
