const DrawingService = require('../../../services/drawingService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions')
  .values;

const schema = `
  drawingUpdate(id: String!, data: DrawingInput!): Drawing!
`;

const resolver = {
  drawingUpdate: async (root, args, context) => {
    new PermissionChecker(context)
      .validateHas(permissions.drawingEdit);

    return new DrawingService(context).update(
      args.id,
      args.data
    );
  },
};

exports.schema = schema;
exports.resolver = resolver;
