module.exports = [
  require('./drawingFind'),
  require('./drawingList'),
  require('./drawingAutocomplete'),
];
