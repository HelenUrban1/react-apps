const DrawingService = require('../../../services/drawingService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions')
  .values;

const schema = `
  drawingFind(id: String!): Drawing!
`;

const resolver = {
  drawingFind: async (root, args, context) => {
    new PermissionChecker(context).validateHas(permissions.drawingRead);
    return new DrawingService(context).findById(args.id);
  },
};

exports.schema = schema;
exports.resolver = resolver;
