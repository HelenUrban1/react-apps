const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions')
  .values;
const DrawingService = require('../../../services/drawingService');

const schema = `
  drawingAutocomplete(query: String, limit: Int): [AutocompleteOption!]!
`;

const resolver = {
  drawingAutocomplete: async (root, args, context, info) => {
    new PermissionChecker(context)
      .validateHas(permissions.drawingAutocomplete);

    return new DrawingService(context).findAllAutocomplete(
      args.query,
      args.limit,
    );
  },
};

exports.schema = schema;
exports.resolver = resolver;
