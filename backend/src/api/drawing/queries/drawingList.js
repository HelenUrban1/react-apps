const DrawingService = require('../../../services/drawingService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions')
  .values;
const graphqlSelectRequestedAttributes = require('../../shared/utils/graphqlSelectRequestedAttributes');

const schema = `
  drawingList(filter: DrawingFilterInput, limit: Int, offset: Int, orderBy: DrawingOrderByEnum): DrawingPage!
`;

const resolver = {
  drawingList: async (root, args, context, info) => {
    new PermissionChecker(context)
      .validateHas(permissions.drawingRead);

    return new DrawingService(context).findAndCountAll({
      ...args,
      requestedAttributes: graphqlSelectRequestedAttributes(
        info,
        'rows',
      ),
    });
  },
};

exports.schema = schema;
exports.resolver = resolver;
