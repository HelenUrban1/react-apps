const JobService = require('../../../services/jobService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions').values;
const SampleService = require('../../../services/sampleService');

const schema = `
  jobUpdate(id: String!, data: JobInput!): Job!
`;

const resolver = {
  jobUpdate: async (root, args, context) => {
    new PermissionChecker(context).validateHas(permissions.jobEdit);
    const updatedJob = await new JobService(context).update(args.id, args.data);
    const sampleService = new SampleService(context);

    // add or remove samples based on new quantity
    updatedJob.parts.forEach((part) => {
      const partSamples = updatedJob.jobSamples.filter((s) => s.dataValues.partId === part.id);
      const sampleChange = updatedJob.samples - partSamples.length;
      if (sampleChange > 0) {
        let nextIndex = partSamples.length;
        for (let i = 0; i < sampleChange; i++) {
          sampleService.create({ sampleIndex: nextIndex, serial: `Sample ${nextIndex + 1}`, featureCoverage: '100%', status: 'Unmeasured', part: part.id, job: updatedJob.id });
          nextIndex += 1;
        }
      } else if (sampleChange < 0) {
        // handle removing samples
        const samplesToRemove = Math.abs(sampleChange);
        const sortedSamples = partSamples.sort((a, b) => (a.sampleIndex < b.sampleIndex ? 1 : -1));
        const samplesToDestroy = sortedSamples.slice(0, samplesToRemove);
        sampleService.reduce();
        sampleService.destroyAll(samplesToDestroy.map((s) => s.id));
      }
    });
    return updatedJob;
  },
};

exports.schema = schema;
exports.resolver = resolver;
