const JobService = require('../../../services/jobService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions').values;
const SampleService = require('../../../services/sampleService');

const schema = `
  jobCreate(data: JobInput!): Job!
`;

const resolver = {
  jobCreate: async (root, args, context) => {
    new PermissionChecker(context).validateHas(permissions.jobCreate);
    const newJob = await new JobService(context).create(args.data);

    if (newJob.samples > 0) {
      const sampleService = new SampleService(context);
      newJob.parts.forEach((part) => {
        for (let i = 0; i < newJob.samples; i++) {
          sampleService.create({ sampleIndex: i, serial: `Sample ${i + 1}`, featureCoverage: '100%', status: 'Unmeasured', part: part.id, job: newJob.id });
        }
      });
    }

    return newJob;
  },
};

exports.schema = schema;
exports.resolver = resolver;
