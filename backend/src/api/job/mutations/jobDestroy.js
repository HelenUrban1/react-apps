const JobService = require('../../../services/jobService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions').values;

const schema = `
  jobDestroy(ids: [String!]!): Boolean
`;

const resolver = {
  jobDestroy: async (root, args, context) => {
    new PermissionChecker(context).validateHas(permissions.jobDestroy);

    await new JobService(context).destroyAll(args.ids);

    return true;
  },
};

exports.schema = schema;
exports.resolver = resolver;
