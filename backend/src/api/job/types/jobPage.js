const schema = `
  type JobPage {
    rows: [Job!]!
    count: Int!
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
