const schema = `
  type Job {
    id: String!
    name: String
    parts: [Part]
    jobSamples: [Sample]
    sampling: SamplingEnum
    jobStatus: JobStatusEnum
    interval: Int
    samples: Int
    passing: Int
    scrapped: Int
    accessControl: JobAccessControlEnum
    createdAt: DateTime
    updatedAt: DateTime
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
