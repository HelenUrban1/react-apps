const schema = `
  input JobInput {
    name: String
    parts: [String!]!
    sampling: SamplingEnum!
    jobStatus: JobStatusEnum!
    interval: Int!
    samples: Int
    passing: Int
    accessControl: JobAccessControlEnum!
    scrapped: Int
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
