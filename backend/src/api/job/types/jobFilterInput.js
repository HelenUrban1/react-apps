const schema = `
  input JobFilterInput {
    id: String
    part: String
    name: String
    sampling: [SamplingEnum]
    jobStatus: [JobStatusEnum]
    interval: Int
    samples: Int
    passing: Int
    scrapped: Int
    accessControl: [JobAccessControlEnum]
    createdAt: DateTime
    updatedAt: DateTime
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
