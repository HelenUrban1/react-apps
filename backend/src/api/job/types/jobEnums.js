const schema = `
  enum SamplingEnum {
    Timed
    Quantity
  }

  enum JobStatusEnum {
    Active
    Inactive
    Complete
    Deleted
  }

  enum JobAccessControlEnum {
    None
    ITAR
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
