const JobService = require('../../../services/jobService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions').values;
const graphqlSelectRequestedAttributes = require('../../shared/utils/graphqlSelectRequestedAttributes');

const schema = `
  jobList(filter: JobFilterInput, limit: Int, offset: Int, orderBy: JobOrderByEnum): JobPage!
`;

const resolver = {
  jobList: async (root, args, context, info) => {
    new PermissionChecker(context).validateHas(permissions.jobRead);

    return new JobService(context).findAndCountAll({
      ...args,
      requestedAttributes: graphqlSelectRequestedAttributes(info, 'rows'),
    });
  },
};

exports.schema = schema;
exports.resolver = resolver;
