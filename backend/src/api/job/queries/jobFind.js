const JobService = require('../../../services/jobService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions').values;

const schema = `
  jobFind(id: String!): Job!
`;

const resolver = {
  jobFind: async (root, args, context) => {
    new PermissionChecker(context).validateHas(permissions.jobRead);
    return new JobService(context).findById(args.id);
  },
};

exports.schema = schema;
exports.resolver = resolver;
