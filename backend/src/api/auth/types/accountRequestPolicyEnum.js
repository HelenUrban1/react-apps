const schema = `
enum RequestState {
  New
  Open
  Closed
  Email
}
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
