const schema = `
  type User {
    id: String!
    fullName: String
    firstName: String
    lastName: String
    phoneNumber: String
    email: String!
    avatars: [File!]
    authenticationUid: String
    emailVerified: Boolean
    emailVerificationTokenExpiresAt: DateTime
    disabled: Boolean
    createdAt: DateTime
    updatedAt: DateTime
    password: String
    emailVerificationToken: String
    passwordResetToken: String
    passwordResetTokenExpiresAt: DateTime
    activeAccountMemberId: String
    importHash:String
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
