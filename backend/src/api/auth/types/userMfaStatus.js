const schema = `
  type UserMfaStatus {
    mfaEnabled: Boolean
    mfaRegistered: Boolean
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
