const schema = `
  type UserWithRoles {
    id: String!
    authenticationUid: String
    emailVerified: Boolean
    emailVerificationToken: String
    emailVerificationTokenExpiresAt: DateTime
    fullName: String
    firstName: String
    lastName: String
    phoneNumber: String
    email: String!
    avatars: [File!]
    activeAccountMemberId: String
    activeAccountId: String
    isServiceAccount: Boolean
    isReviewAccount: Boolean
    accountMemberships: [AccountMember!]
    roles: [AccountMemberRolesEnum!]
    paid: Boolean
    status: UserStatusEnum
    accountId: String
    accountSettings: String
    accountName: String
    siteId: String
    siteName: String
    disabled: Boolean
    mfaRegistered: Boolean
    createdAt: DateTime
    updatedAt: DateTime
    account: Account
    subscription: Subscription
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
