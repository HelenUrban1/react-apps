module.exports = [
  require('./user'),
  require('./userFilterInput'),
  require('./userMfaStatus'),
  require('./userWithRoles'),
  require('./userWithRolesOrderByEnum'),
  require('./userWithRolesPage'),
  require('./userProfileInput'),
  require('./userValidTokenStatesEnum'),
  require('./accountRequestPolicyEnum'),
  require('./account'),
];
