const schema = `
enum ValidState {
  Valid
  Expired
  Canceled
  Requested
}
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
