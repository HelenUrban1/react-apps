const schema = `
  enum UserOrderByEnum {
    id_ASC
    id_DESC
    settings_ASC
    settings_DESC
    roles_ASC
    roles_DESC
    status_ASC
    status_DESC
    createdAt_ASC
    createdAt_DESC
  }

  enum UserRolesEnum {
    Admin
    Billing
    Collaborator
    Owner
    Viewer
  }

  enum UserNonOwnerRolesEnum {
    Admin
    Billing
    Collaborator
    Viewer
  }

  enum UserStatusEnum {
    Active
    Pending
    Requesting
    Suspended
    Archived
  }

  input UserFilterInput {
    roles: [UserRolesEnum]
    account: String
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
