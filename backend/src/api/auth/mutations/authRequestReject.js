const ValidationError = require('../../../errors/validationError');
const AuthService = require('../../../services/auth/authService');
const CognitoAuthService = require('../../../services/auth/cognitoAuthService');
const config = require('../../../../config')();

const { log } = require('../../../logger');

const schema = `
  authRequestReject(adminEmail: String!, token: String!): Boolean
`;

const resolver = {
  authRequestReject: async (root, args, context) => {
    if (!args.token) {
      log.error(`Token: ${args.token}`);
      throw new ValidationError(context.language, 'auth.userMembershipAcceptFailed');
    }
    if (config.jwt.authStrategy && config.jwt.authStrategy === 'cognito') {
      log.debug('Running Cognito Reject User Request');
      return CognitoAuthService.cognitoRejectAccessRequest(args.adminEmail, args.token, context);
    }
    return AuthService.rejectAccessRequest(args.adminEmail, args.token, context);
  },
};

exports.schema = schema;
exports.resolver = resolver;
