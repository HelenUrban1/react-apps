const AuthEmails = require('../../../services/auth/authEmails');
const CognitoAuthEmails = require('../../../services/auth/cognitoAuthEmails');
const CognitoAuthCommands = require('../../../services/auth/cognitoAuthCommands');
const CognitoAuthService = require('../../../services/auth/cognitoAuthService');
const UserRepository = require('../../../database/repositories/userRepository');
const ValidationError = require('../../../errors/validationError');
const config = require('../../../../config')();
const { log } = require('../../../logger');

const schema = `
  authSendPasswordResetEmail(email: String!): Boolean
`;

const resolver = {
  authSendPasswordResetEmail: async (root, args, context) => {
    if (config.jwt.authStrategy && config.jwt.authStrategy === 'cognito') {
      log.debug('AuthSendPasswordResetEmail through Cognito');

      const { cognitoClient } = context;
      const cognitoUser = await CognitoAuthCommands.cognitoGetUser(args.email, { cognitoClient });
      if (!cognitoUser) {
        const user = await UserRepository.findByEmail(args.email, {});
        if (!user) {
          throw new ValidationError(context.language, 'auth.passwordReset.error');
        }
        return CognitoAuthService.cognitoMigrateUser(args.email, user.password, user.mfaCode, user.rememberStatus, {}, { cognitoClient });
      }
      return CognitoAuthEmails.cognitoSendPasswordResetEmail(args.email, context);
    }
    await AuthEmails.sendPasswordResetEmail(context.language, args.email, context);

    return true;
  },
};

exports.schema = schema;
exports.resolver = resolver;
