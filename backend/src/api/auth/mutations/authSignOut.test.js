const { resolver } = require('./authSignOut');
const AuthService = require('../../../services/auth/authService');

describe.only('authSignOut', () => {
  it('clears a cookie', async () => {
    const date = new Date();
    const cookies = {};
    const context = {
      config: {
        protocol: 'http',
        hostname: 'ixc-local.com',
      },
      req: {
        res: {
          clearCookie: (cookie, options) => {
            cookies[cookie] = { ...options, expires: date, token: '' };
          },
        },
      },
    };

    await resolver.authSignOut({}, {}, context);
    expect(cookies.refresh_token).toBeDefined();
    expect(cookies.refresh_token.token).toBe('');
    expect(cookies.refresh_token.expires).toBe(date);
    expect(cookies.refresh_token.domain).toBe('ixc-local.com');
    expect(cookies.refresh_token.secure).toBe(false);
  });
});
