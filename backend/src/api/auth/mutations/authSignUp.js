const AuthService = require('../../../services/auth/authService');

const schema = `
  authSignUp(email: String!, password: String!, firstName: String, lastName: String, accountId: String, siteId: String): String
`;

const resolver = {
  authSignUp: async (root, args, context) => {
    return AuthService.signup(
      args.email, //
      args.password,
      args.firstName,
      args.lastName,
      args.accountId,
      args.siteId,
      context
    );
  },
};

exports.schema = schema;
exports.resolver = resolver;
