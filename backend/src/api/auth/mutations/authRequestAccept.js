const ValidationError = require('../../../errors/validationError');
const AuthService = require('../../../services/auth/authService');
const CognitoAuthService = require('../../../services/auth/cognitoAuthService');
const config = require('../../../../config')();
const { log } = require('../../../logger');

const schema = `
  authRequestAccept(email: String!, token: String, userId: String): String
`;

const resolver = {
  authRequestAccept: async (root, args, context) => {
    if (!args.token && !args.userId) {
      log.error(`Token: ${args.token}, userId: ${args.userId}`);
      throw new ValidationError(context.language, 'auth.userMembershipRequestFailed');
    }

    if (config.jwt.authStrategy && config.jwt.authStrategy === 'cognito') {
      return CognitoAuthService.cognitoAcceptAccessRequest(args.email, args.token, args.userId, context);
    }
    const [jwt, refreshToken] = await AuthService.acceptAccessRequest(args.email, args.token, args.userId, context);

    context.req.res.cookie('refresh_token', refreshToken, {
      httpOnly: true,
      maxAge: config.jwt.refreshExpiry * 1000,
      secure: context.config.protocol === 'https',
      domain: context.config.hostname,
    });

    context.req.res.cookie('session', new Date().valueOf(), {
      httpOnly: true, 
      secure: true,
      domain: context.config.hostname,
    });

    context.req.res.cookie('last_activity', new Date().valueOf(), {
      httpOnly: true, 
      secure: true,
      domain: context.config.hostname,
    });

    return jwt;
  },
};

exports.schema = schema;
exports.resolver = resolver;
