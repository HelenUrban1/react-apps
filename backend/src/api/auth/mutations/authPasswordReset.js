const CryptoJS = require('crypto-js');
const AuthService = require('../../../services/auth/authService');
const config = require('../../../../config')();
const { log } = require('../../../logger');
const CognitoAuthService = require('../../../services/auth/cognitoAuthService');

const securePhassphrase = process.env.IXC_PASSWORD_ENCRYPTION_PASSPHRASE;

const schema = `
  authPasswordReset(token: String!, email: String!, password: String!): Boolean
`;

const resolver = {
  authPasswordReset: async (root, args, context) => {
    const password = CryptoJS.AES.decrypt(args.password, securePhassphrase).toString(CryptoJS.enc.Utf8);
    if (config.jwt.authStrategy && config.jwt.authStrategy === 'cognito') {
      log.debug('AuthPasswordReset through Cognito');
      return CognitoAuthService.cognitoPasswordReset(args.token, args.email, password, context);
    }
    await AuthService.passwordReset(args.token, password, { ...context, userOnly: true });

    return true;
  },
};

exports.schema = schema;
exports.resolver = resolver;
