const AuthService = require('../../../services/auth/authService');

const schema = `
  authCreateOrphan(email: String!, password: String!, type: String!): Boolean
`;

const resolver = {
  authCreateOrphan: async (root, args, context) => {
    return AuthService.createOrphanUser(args.email, args.password, args.type, context);
  },
};

exports.schema = schema;
exports.resolver = resolver;
