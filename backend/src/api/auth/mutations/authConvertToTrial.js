const AuthService = require('../../../services/auth/authService');
const AuthUtils = require('../../../services/auth/authUtils');
const config = require('../../../../config')();
const CognitoAuthService = require('../../../services/auth/cognitoAuthService');

const schema = `
  authConvertToTrial(email: String!): String
`;

const resolver = {
  authConvertToTrial: async (root, args, context) => {
    const orphanedUser = await AuthUtils.convertToOrphan(args.email, context);
    return config.jwt.authStrategy && config.jwt.authStrategy === 'cognito' ? CognitoAuthService.cognitoStartTrialForOrphan(orphanedUser, context) : AuthService.startTrialForOrphan(orphanedUser, context);
  },
};

exports.schema = schema;
exports.resolver = resolver;
