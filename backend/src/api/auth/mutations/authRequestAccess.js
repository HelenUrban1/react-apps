const AuthService = require('../../../services/auth/authService');

const schema = `
  authRequestAccess(email: String!, adminEmail: String, token: String!): Boolean
`;

const resolver = {
  authRequestAccess: async (root, args, context) => {
    return AuthService.sendAccessRequest(args.email, args.adminEmail, args.token, ['Collaborator'], context);
  },
};

exports.schema = schema;
exports.resolver = resolver;
