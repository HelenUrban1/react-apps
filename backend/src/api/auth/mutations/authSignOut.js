const schema = `
  authSignOut(email: String!): String
`;

const resolver = {
  authSignOut: async (root, args, context) => {
    context.req.res.clearCookie('refresh_token', {
      httpOnly: true,
      secure: context.config.protocol === 'https',
      domain: context.config.hostname,
    });

    return '';
  },
};

exports.schema = schema;
exports.resolver = resolver;
