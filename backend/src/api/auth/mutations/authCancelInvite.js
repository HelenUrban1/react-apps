const ForbiddenError = require('../../../errors/forbiddenError');
const AuthService = require('../../../services/auth/authService');
const CognitoAuthService = require('../../../services/auth/cognitoAuthService');
const config = require('../../../../config')();
const { log } = require('../../../logger');

const schema = `
authCancelInvite(email: String!): Boolean
`;

const resolver = {
  authCancelInvite: async (root, args, context) => {
    if (!context.currentUser) {
      throw new ForbiddenError(context.language);
    }

    if (config.jwt.authStrategy && config.jwt.authStrategy === 'cognito') {
      log.debug('Running Cognito Cancel User Invite');
      return CognitoAuthService.cognitoCancelInvitedUser(args.email, context);
    }

    return AuthService.cancelInvitedUser(args.email, context);
  },
};

exports.schema = schema;
exports.resolver = resolver;
