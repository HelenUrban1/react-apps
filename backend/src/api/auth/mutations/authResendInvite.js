const config = require('../../../../config')();
const ForbiddenError = require('../../../errors/forbiddenError');
const AuthService = require('../../../services/auth/authService');
const CognitoAuthEmails = require('../../../services/auth/cognitoAuthEmails');

const { log } = require('../../../logger');

const schema = `
authResendInvite(email: String!): Boolean
authSendReminderInvite(email: String!): Boolean
`;

const resolver = {
  authResendInvite: async (root, args, context) => {
    if (!context.currentUser) {
      throw new ForbiddenError(context.language);
    }
    if (config.jwt.authStrategy && config.jwt.authStrategy === 'cognito') {
      log.debug('Running Cognito Resend Invite Auth Strategy');
      return CognitoAuthEmails.cognitoResendUserInvite(args.email, context.currentUser?.firstName, context);
    }
    return AuthService.resendUserInvite(args.email, context.currentUser?.firstName, context);
  },
  authSendReminderInvite: async (root, args, context) => {
    // resends the correct verification email baised on the user's email
    return AuthService.resendVerificationEmail(args.email, { ...context, isReminder: true });
  },
};

exports.schema = schema;
exports.resolver = resolver;
