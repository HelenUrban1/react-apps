const AuthService = require('../../../services/auth/authService');

const schema = `
  authSignUpSubmit(email: String!, firstName: String): Boolean
`;

const resolver = {
  authSignUpSubmit: async (root, args, context) => {
    return AuthService.signupSubmit(
      args.email, //
      args.firstName,
      context
    );
  },
};

exports.schema = schema;
exports.resolver = resolver;
