const CognitoAuthService = require('../../../services/auth/cognitoAuthService');

const schema = `
  authResetMfa(email: String!): Boolean
`;

const resolver = {
  authResetMfa: async (root, args, context) => {
    await CognitoAuthService.cognitoResetMfa(args.email, context);
    return true;
  },
};

exports.schema = schema;
exports.resolver = resolver;
