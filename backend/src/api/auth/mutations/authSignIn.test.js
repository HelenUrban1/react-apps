const { resolver } = require('./authSignIn');
const AuthService = require('../../../services/auth/authService');

jest.mock('../../../services/auth/authService');

AuthService.signin = () => Promise.resolve('foo');

describe('authSignIn', () => {
  it('sets a JWT cookie', async () => {
    const cookies = {};
    const context = {
      config: {
        protocol: 'http',
        hostname: 'ixc-local.com',
      },
      req: {
        res: {
          cookie: (cookie, options) => {
            cookies[cookie] = options;
          },
        },
      },
    };

    const args = {
      email: 'foo',
      password: 'bar',
    };

    const token = await resolver.authSignIn({}, args, context);
    expect(token).toBe('f');
    expect(cookies.refresh_token).toBeDefined();
  });
});
