const UserRepository = require('../../../database/repositories/userRepository');
const { log } = require('../../../logger');

const schema = `
  authCheckForPassword(token: String!): Boolean
`;

const resolver = {
  authCheckForPassword: async (root, args, context) => {
    const user = await UserRepository.findByEmailVerificationToken(args.token, { ...context, userOnly: true, ignoreVerification: true });
    if (user && user.password && user.password.length > 0) return true;
    return false;
  },
};

exports.schema = schema;
exports.resolver = resolver;
