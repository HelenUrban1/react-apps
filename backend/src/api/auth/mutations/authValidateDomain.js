const { validateDomain } = require('../../../services/auth/authUtils');

const schema = `
  authValidateDomain(email: String!): RequestState
`;

const resolver = {
  authValidateDomain: async (root, args, context) => {
    return validateDomain(args.email, context);
  },
};

exports.schema = schema;
exports.resolver = resolver;
