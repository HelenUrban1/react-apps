const AccountMemberRepository = require('../../../database/repositories/accountMemberRepository');
const UserRepository = require('../../../database/repositories/userRepository');

const schema = `
  authCheckForStatus(email: String!): String
`;

const resolver = {
  authCheckForStatus: async (root, args, context) => {
    const user = await UserRepository.findByEmail(args.email, context);
    if (!user.accountMemberships || user.accountMemberships.length === 0) return 'Active';
    const membership = await AccountMemberRepository.findByUserAndAccount(user.id, user.accountId, context);
    return membership.status;
  },
};

exports.schema = schema;
exports.resolver = resolver;
