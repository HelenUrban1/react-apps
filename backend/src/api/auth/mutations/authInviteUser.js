const config = require('../../../../config')();
const ForbiddenError = require('../../../errors/forbiddenError');
const AuthService = require('../../../services/auth/authService');
const CognitoAuthService = require('../../../services/auth/cognitoAuthService');

const { log } = require('../../../logger');

const schema = `
  authInviteUser(email: String!, roles:[String], control: Boolean): Boolean
`;

const resolver = {
  authInviteUser: async (root, args, context) => {
    if (!context.currentUser) {
      throw new ForbiddenError(context.language);
    }

    if (config.jwt.authStrategy && config.jwt.authStrategy === 'cognito') {
      log.debug('Running Cognito Invite User Auth Strategy');
      return CognitoAuthService.cognitoInviteNewUser(args.email, args.roles, args.control, context.currentUser.accountId, context.currentUser.siteId, context.currentUser.firstName || context.currentUser.email, context);
    }

    return AuthService.inviteNewUser(args.email, args.roles, args.control, context.currentUser.accountId, context.currentUser.siteId, context.currentUser.firstName || context.currentUser.email, context);
  },
};

exports.schema = schema;
exports.resolver = resolver;
