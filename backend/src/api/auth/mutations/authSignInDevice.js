const AuthService = require('../../../services/auth/authService');
const config = require('../../../../config')();

const schema = `
  authSignInDevice: String
`;

const resolver = {
  authSignInDevice: async (root, args, context) => {
    const result = await AuthService.signinWithDevice(context.req.cookies['device_token'], context.req.currentUser.account);

    const [jwt, refreshToken] = result;

    context.req.res.cookie('refresh_token', refreshToken, {
      httpOnly: true,
      maxAge: config.jwt.refreshExpiry * 1000,
      secure: context.config.protocol === 'https',
      domain: context.config.hostname,
    });

    context.req.res.cookie('session', new Date().valueOf(), {
      httpOnly: true, 
      secure: true,
      domain: context.config.hostname,
    });

    context.req.res.cookie('last_activity', new Date().valueOf(), {
      httpOnly: true, 
      secure: true,
      domain: context.config.hostname,
    });

    return jwt;
  },
};

exports.schema = schema;
exports.resolver = resolver;
