const AuthService = require('../../../services/auth/authService');
const config = require('../../../../config')();
const CognitoAuthEmails = require('../../../services/auth/cognitoAuthEmails');

const schema = `
  authSendEmailAddressVerificationEmail(email: String!): Boolean
`;

const resolver = {
  authSendEmailAddressVerificationEmail: async (root, args, context) => {
    return config.jwt.authStrategy !== 'cognito' ? AuthService.resendVerificationEmail(args.email, context) : CognitoAuthEmails.cognitoResendVerificationEmail(args.email, context);
  },
};

exports.schema = schema;
exports.resolver = resolver;
