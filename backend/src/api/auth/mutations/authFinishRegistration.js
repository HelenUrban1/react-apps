const AuthService = require('../../../services/auth/authService');

const schema = `
  authFinishRegistration(email: String!, password: String, firstName: String, lastName: String, orgName: String, phoneNumber: String, token: String): Boolean
`;

const resolver = {
  authFinishRegistration: async (root, args, context) => {
    return AuthService.finishRegistration(args.email, args.password, args.firstName, args.lastName, args.orgName, args.phoneNumber, args.token, context);
  },
};

exports.schema = schema;
exports.resolver = resolver;
