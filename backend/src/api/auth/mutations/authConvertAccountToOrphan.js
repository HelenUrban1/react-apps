const AuthService = require('../../../services/auth/authService');

const schema = `
authConvertAccountToOrphan(accountId: String!): Boolean
`;

const resolver = {
  authConvertAccountToOrphan: async (root, args, context) => {
    return AuthService.convertAccountToOrphan(args.accountId, context);
  },
};

exports.schema = schema;
exports.resolver = resolver;
