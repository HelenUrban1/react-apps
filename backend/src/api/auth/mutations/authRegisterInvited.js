const AuthService = require('../../../services/auth/authService');

const schema = `
authRegisterInvited(token: String!, email: String!, password: String!, profile: UserProfileInput!): Boolean
`;

const resolver = {
  authRegisterInvited: async (root, args, context) => {
    return AuthService.registerInvitedUser(args.token, args.email, args.password, args.profile, context);
  },
};

exports.schema = schema;
exports.resolver = resolver;
