const UserRepository = require('../../../database/repositories/userRepository');
const AuthService = require('../../../services/auth/authService');
const config = require('../../../../config')();

const { log } = require('../../../logger');
const CognitoAuthEmails = require('../../../services/auth/cognitoAuthEmails');
const CognitoAuthService = require('../../../services/auth/cognitoAuthService');

const schema = `
  authValidSignupToken(token: String!, email: String): ValidState
`;

const resolver = {
  authValidSignupToken: async (root, args, context) => {
    const tempUser = await UserRepository.findByEmailWithoutAvatar(args.email, context);
    const token = config.jwt.authStrategy !== 'cognito' ? args.token : tempUser.emailVerificationToken;
    const user = await UserRepository.findValidTokenOrCanceledInvite(token, context);

    if (!user) {
      const res = config.jwt.authStrategy !== 'cognito' ? AuthService.resendVerificationEmail(args.email, context) : CognitoAuthEmails.cognitoResendVerificationEmail(args.email, context);
      return 'Expired';
    }
    if (user.disabled) return 'Canceled';
    if (args.email && user.email !== args.email) return 'Requested';
    if (user.emailVerified && user.password) return 'Active';
    return 'Valid';
  },
};

exports.schema = schema;
exports.resolver = resolver;
