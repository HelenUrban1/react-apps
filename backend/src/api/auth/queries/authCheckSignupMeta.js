const UserRepository = require('../../../database/repositories/userRepository');

const schema = `
  authCheckSignupMeta(email: String): String
`;

const resolver = {
  authCheckSignupMeta: async (root, args, context) => {
    const user = await UserRepository.findByEmail(args.email, { ...context, userOnly: true });
    if (user && user.signupMeta) {
      return user.signupMeta;
    }
    return '{}';
  },
};

exports.schema = schema;
exports.resolver = resolver;
