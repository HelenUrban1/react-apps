const UserRepository = require('../../../database/repositories/userRepository');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions').values;

const schema = `
  userList(filter: UserFilterInput, limit: Int, offset: Int, orderBy: UserOrderByEnum): [UserWithRoles]!
`;

const resolver = {
  userList: async (root, args, context, info) => {
    // new PermissionChecker(context).validateHas(permissions.iamRead);
    new PermissionChecker(context).validateHas(permissions.iamRead);
    return UserRepository.findAllByRolesArray(args, context.currentUser);
  },
};

exports.schema = schema;
exports.resolver = resolver;
