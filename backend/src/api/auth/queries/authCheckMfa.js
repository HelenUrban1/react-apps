const CognitoAuthCommands = require('../../../services/auth/cognitoAuthCommands');
const UserRepository = require('../../../database/repositories/userRepository');
const SettingRepository = require('../../../database/repositories/settingRepository');

const schema = `
  authCheckMfa(email: String): UserMfaStatus
`;

const resolver = {
  authCheckMfa: async (root, args, context) => {
    // Check if Mfa has been registered
    const cognitoUser = await CognitoAuthCommands.cognitoGetUser(args.email, context);
    const mfaRegistered = cognitoUser && cognitoUser.PreferredMfaSetting === 'SOFTWARE_TOKEN_MFA';

    // Check settings to see if MFA is enabled
    const currentUser = await UserRepository.findByEmailWithoutAvatar(args.email);
    const mfaEnabledSetting = await SettingRepository.findByName('mfaEnabled', { currentUser });
    const mfaEnabled = mfaEnabledSetting && mfaEnabledSetting.value === 'true';

    return { mfaEnabled, mfaRegistered };
  },
};

exports.schema = schema;
exports.resolver = resolver;
