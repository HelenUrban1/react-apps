const ForbiddenError = require('../../../errors/forbiddenError');
const UnauthorizedError = require('../../../errors/unauthorizedError');
const defaults = require('../../../staticData/defaults');

const schema = `
  authMe: UserWithRoles
`;

const resolver = {
  authMe(root, args, context) {
    if (!context.currentUser) throw new UnauthorizedError(context.language);
    if (!context.currentUser.id) throw new ForbiddenError(context.language);

    // To support isolated multi-tenancy user details were moved into a profile object
    // The block below elevates the profile details to the top level of the currentUser record
    if (context.currentUser.profile) {
      context.currentUser.firstName = context.currentUser.profile.firstName;
      context.currentUser.lastName = context.currentUser.profile.lastName;
      context.currentUser.phoneNumber = context.currentUser.profile.phoneNumber;
      context.currentUser.avatars = context.currentUser.profile.avatars;
    }
    context.currentUser.isServiceAccount = Object.values(defaults.serviceAccounts).includes(context.currentUser.accountId);
    context.currentUser.isReviewAccount = context.currentUser.accountId === defaults.serviceAccounts.review;

    return context.currentUser;
  },
};

exports.schema = schema;
exports.resolver = resolver;

/* 
// Below is the structure of the user record returned by the 
// userRepo to the Express authMiddleware during login/authentication

const currentUser = {
  id: 'e0a01779-c8ed-4090-bd44-9c1b2242fb75',
  password: '$2b$12$rvwQlGp8yvE6ChdsmO1Vr.qkNFGEgdZRXsUAFOrFEzJGyVQn2A2SW',
  emailVerified: true,
  emailVerificationToken: null,
  emailVerificationTokenExpiresAt: null,
  passwordResetToken: null,
  passwordResetTokenExpiresAt: null,
  email: 'dev.mail.monkey@gmail.com',
  signupMeta: null,
  activeAccountMemberId: '007516cc-3168-40e4-8ad3-d42c92f66caa',
  authenticationUid: 'c7bf4eca-cd00-4d65-81a8-5d4da86ea824',
  disabled: false,
  importHash: null,
  createdAt: 2021-04-25T17:26:08.109Z,
  updatedAt: 2021-04-25T17:26:08.109Z,
  deletedAt: null,
  createdById: null,
  updatedById: null,
  avatars: [],
  accountMemberships: [
    accountMember {
      dataValues: [Object],
      _previousDataValues: [Object],
      _changed: Set {},
      _options: [Object],
      isNewRecord: false
    }
  ],
  accountId: 'e0a01769-c8ed-4090-bd24-9c1b2241fb75',
  siteId: '31ef1d56-1b86-4ffa-b886-507ed8f021c5',
  accountName: 'Acme Corp',
  accountSettings: '{"allowRequestPolicy":false}',
  siteName: undefined,
  roles: [ 'Owner' ],
  activeAccountId: 'e0a01769-c8ed-4090-bd24-9c1b2241fb75',
  status: 'Active',
  account: {
    id: 'e0a01769-c8ed-4090-bd24-9c1b2241fb75',
    status: 'Active',
    settings: '{"allowRequestPolicy":false}',
    orgName: 'Acme Corp',
    orgPhone: '987-654-3210',
    orgEmail: 'dev.mail.monkey@gmail.com',
    orgStreet: '1 Glenwood Ave',
    orgStreet2: '',
    orgCity: 'Raleigh',
    orgRegion: 'North_Carolina',
    orgPostalcode: '27603',
    orgCountry: 'United_States',
    orgUnitName: null,
    orgUnitId: null,
    parentAccountId: null,
    importHash: null,
    dbHost: 'localhost',
    dbName: 'ixc_account_development_e0a01769-c8ed-4090-bd24-9c1b2241fb75',
    createdAt: 2021-04-25T17:26:08.112Z,
    updatedAt: 2021-04-25T17:26:08.112Z,
    deletedAt: null,
    createdById: 'e0a01779-c8ed-4090-bd44-9c1b2242fb75',
    updatedById: 'e0a01779-c8ed-4090-bd44-9c1b2242fb75'
  },
  paid: true,
  profile: {
    id: '3bf87c59-69ca-4bf6-a0a1-f36d75f54876',
    fullName: 'Dev Monkey',
    firstName: 'Dev',
    lastName: 'Monkey',
    phoneNumber: '123-456-7890',
    roles: [ 'Owner' ],
    importHash: null,
    createdAt: 2021-04-25T17:26:11.492Z,
    updatedAt: 2021-04-25T17:26:11.492Z,
    deletedAt: null,
    userId: 'e0a01779-c8ed-4090-bd44-9c1b2242fb75',
    createdById: null,
    updatedById: null
  },
  fullName: 'Dev Monkey',
  firstName: 'Dev',
  lastName: 'Monkey',
  phoneNumber: '123-456-7890',
  subscription: {
    renewalDate: '2022-04-25',
    activatedAt: '2020-05-04',
    automaticallyResumeAt: null,
    canceledAt: null,
    currentPeriodEndsAt: '2022-04-25',
    currentPeriodStartedAt: '2021-04-25',
    delayedCancelAt: null,
    expiresAt: '2022-04-25',
    nextAssessmentAt: '2022-04-25',
    trialEndedAt: null,
    trialStartedAt: null,
    id: '07d26541-9294-41a4-969e-eb5ec077a176',
    accountId: 'e0a01769-c8ed-4090-bd24-9c1b2241fb75',
    billingId: 'e0a01779-c8ed-4090-bd44-9c1b2242fb75',
    companyId: '5788481594',
    paidUserSeatsUsed: 27,
    partsUsed: 17,
    drawingsUsed: 6,
    measurementsUsed: 0,
    components: '[{\n' +
      '            "component_id": 1213884,\n' +
      '            "subscription_id": 37003269,\n' +
      '            "component_handle": "paid_user_seat_ss",\n' +
      '            "allocated_quantity": 1,\n' +
      '            "name": "User Seats (SS)",\n' +
      '            "kind": "quantity_based_component",\n' +
      '            "unit_name": "paid user seat",\n' +
      '            "pricing_scheme": "stairstep",\n' +
      '            "price_point_id": 1202165,\n' +
      '            "price_point_handle": "v20210101"\n' +
      '          },\n' +
      '          {\n' +
      '            "component_id": 1028184,\n' +
      '            "subscription_id": 37003269,\n' +
      '            "component_handle": "paid_user_seat_pu",\n' +
      '            "allocated_quantity": 30,\n' +
      '            "name": "User Seats (PU)",\n' +
      '            "kind": "quantity_based_component",\n' +
      '            "unit_name": "paid user seat",\n' +
      '            "pricing_scheme": "per_unit",\n' +
      '            "price_point_id": 961586,\n' +
      '            "price_point_handle": "current"\n' +
      '          }]',
    pendingComponents: '{}',
    enforcePlan: true,
    providerProductId: 5233891,
    providerProductHandle: 'ix_standard',
    providerFamilyHandle: 'inspectionxpert-cloud',
    billingPeriod: 'Annually',
    pendingTier: null,
    pendingBillingPeriod: null,
    importHash: null,
    pendingBillingAmountInCents: null,
    balanceInCents: 375000,
    cancelAtEndOfPeriod: false,
    cancellationMessage: null,
    cancellationMethod: null,
    couponCode: null,
    couponCodes: null,
    couponUseCount: null,
    couponUsesAllowed: null,
    createdAt: 2021-04-25T17:26:08.184Z,
    currentBillingAmountInCents: 387500,
    netTerms: null,
    nextProductHandle: null,
    nextProductId: null,
    nextProductPricePointId: null,
    offerId: null,
    payerId: 37892060,
    paymentCollectionMethod: 'automatic',
    paymentSystemId: 37003269,
    paymentType: '25573100',
    previousStatus: 'active',
    productPriceInCents: 387500,
    productPricePointId: '914840',
    productVersionNumber: '1',
    reasonCode: null,
    receivesInvoiceEmails: null,
    referralCode: null,
    signupPaymentId: 421144154,
    signupRevenue: '0.00',
    snapDay: null,
    status: 'active',
    storedCredentialTransactionId: null,
    totalRevenueInCents: 0,
    updatedAt: 2021-04-25T17:26:08.184Z,
    cardId: 12345,
    maskedCardNumber: 'XXXX-XXXX-XXXX-3442',
    cardType: 'visa',
    expirationMonth: 9,
    expirationYear: 2021,
    customerId: '37892060',
    deletedAt: null,
    createdById: 'e0a01779-c8ed-4090-bd44-9c1b2242fb75',
    updatedById: 'e0a01779-c8ed-4090-bd44-9c1b2242fb75'
  }
}

*/
