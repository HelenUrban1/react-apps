const UserRepository = require('../../../database/repositories/userRepository');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions').values;

const schema = `
  incompleteUserList(emailVerificationTokenExpiresAt: DateTime, emailVerified: Boolean): [User]!
`;

const resolver = {
  incompleteUserList: async (root, args, context, info) => {
    return UserRepository.findAllIncomplete(args, context.currentUser);
  },
};

exports.schema = schema;
exports.resolver = resolver;
