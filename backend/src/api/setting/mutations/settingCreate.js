const SettingService = require('../../../services/settingService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions').values;

const schema = `
  settingCreate(data: SettingInput!): Setting!
`;

const resolver = {
  settingCreate: async (root, args, context) => {
    new PermissionChecker(context).validateHas(permissions.settingCreate);
    console.log({ args });
    return new SettingService(context).create(args.data);
  },
};

exports.schema = schema;
exports.resolver = resolver;
