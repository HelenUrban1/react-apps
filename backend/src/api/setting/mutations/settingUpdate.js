const SettingService = require('../../../services/settingService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions').values;

const schema = `
  settingUpdate(id: String!, data: SettingInput!): Setting!
`;

const resolver = {
  settingUpdate: async (root, args, context) => {
    new PermissionChecker(context).validateHas(permissions.settingEdit);

    return new SettingService(context).update(args.id, args.data);
  },
};

exports.schema = schema;
exports.resolver = resolver;
