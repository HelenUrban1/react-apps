const schema = `
  type Setting {
    id: String!
    name: String!
    value: String
    metadata: String
    siteId: String
    userId: String
    createdAt: DateTime
    updatedAt: DateTime
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
