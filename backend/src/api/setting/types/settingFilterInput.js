const schema = `
  input SettingFilterInput {
    id: String
    name: String
    siteId: String
    userId: String
    createdAtRange: [ DateTime ]
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
