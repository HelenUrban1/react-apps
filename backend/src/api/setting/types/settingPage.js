const schema = `
  type SettingPage {
    rows: [Setting!]!
    count: Int!
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
