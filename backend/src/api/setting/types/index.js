module.exports = [
  require('./setting'), //
  require('./settingFilterInput'),
  require('./settingInput'),
  require('./settingOrderByEnum'),
  require('./settingPage'),
];
