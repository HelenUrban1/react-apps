const schema = `
  input SettingInput {
    name: String!
    value: String
    metadata: String
    siteId: String
    userId: String
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
