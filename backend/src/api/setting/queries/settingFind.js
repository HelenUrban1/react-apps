const SettingService = require('../../../services/settingService');

const schema = `
  settingFind(name: String!): Setting!
`;

const resolver = {
  settingFind: async (root, args, context) => {
    return new SettingService(context).findByName(args.name);
  },
};

exports.schema = schema;
exports.resolver = resolver;
