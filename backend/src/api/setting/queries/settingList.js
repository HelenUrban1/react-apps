const SettingService = require('../../../services/settingService');
const graphqlSelectRequestedAttributes = require('../../shared/utils/graphqlSelectRequestedAttributes');

const schema = `
  settingList(filter: SettingFilterInput, limit: Int, offset: Int, orderBy: SettingOrderByEnum): SettingPage!
`;

const resolver = {
  settingList: async (root, args, context, info) => {
    return new SettingService(context).findAndCountAll({
      ...args,
      requestedAttributes: graphqlSelectRequestedAttributes(info, 'rows'),
    });
  },
};

exports.schema = schema;
exports.resolver = resolver;
