const schema = `
  input NotificationUserStatusFilterInput {
    id: String
    readAtRange: [ DateTime ]
    readAt: Boolean
    archivedAtRange: [ DateTime ]
    archivedAt: Boolean
    user: String
    notification: String
    createdAtRange: [ DateTime ]
    updatedAtRange: [ DateTime ]
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
