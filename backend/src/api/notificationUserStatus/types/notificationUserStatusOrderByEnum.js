const schema = `
  enum NotificationUserStatusOrderByEnum {
    id_ASC
    id_DESC
    readAt_ASC
    readAt_DESC
    archivedAt_ASC
    archivedAt_DESC
    user_ASC
    user_DESC
    notification_ASC
    notification_DESC
    createdAt_ASC
    createdAt_DESC
    updatedAt_ASC
    updatedAt_DESC
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
