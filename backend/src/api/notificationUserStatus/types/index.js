module.exports = [
  require('./notificationUserStatus'), //
  require('./notificationUserStatusInput'),
  require('./notificationUserStatusFilterInput'),
  require('./notificationUserStatusOrderByEnum'),
  require('./notificationUserStatusPage'),
];
