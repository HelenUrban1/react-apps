const schema = `
  input NotificationUserStatusInput {
    readAt: DateTime
    archivedAt: DateTime
    user: String!
    notification: String!
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
