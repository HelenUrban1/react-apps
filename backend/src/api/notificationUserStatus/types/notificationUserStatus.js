const schema = `
  type NotificationUserStatus {
    id: String!
    readAt: DateTime
    archivedAt: DateTime
    user: String!
    notification: Notification!
    createdAt: DateTime
    updatedAt: DateTime
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
