const schema = `
  type NotificationUserStatusPage {
    rows: [NotificationUserStatus!]!
    count: Int!
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
