const NotificationUserStatusService = require('../../../services/notificationUserStatusService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions').values;

const schema = `
notificationUserStatusFind(id: String!): NotificationUserStatus!
`;

const resolver = {
  notificationUserStatusFind: async (root, args, context) => {
    new PermissionChecker(context).validateHas(permissions.notificationRead);
    return new NotificationUserStatusService(context).findById(args.id);
  },
};

exports.schema = schema;
exports.resolver = resolver;
