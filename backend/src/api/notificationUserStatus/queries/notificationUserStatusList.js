const NotificationUserStatusService = require('../../../services/notificationUserStatusService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions').values;
const graphqlSelectRequestedAttributes = require('../../shared/utils/graphqlSelectRequestedAttributes');

const schema = `
notificationUserStatusList(filter: NotificationUserStatusFilterInput, limit: Int, offset: Int, orderBy: NotificationUserStatusOrderByEnum): NotificationUserStatusPage!
`;

const resolver = {
  notificationUserStatusList: async (root, args, context, info) => {
    new PermissionChecker(context).validateHas(permissions.notificationRead);

    return new NotificationUserStatusService(context).findAndCountAll({
      ...args,
      requestedAttributes: graphqlSelectRequestedAttributes(info, 'rows'),
    });
  },
};

exports.schema = schema;
exports.resolver = resolver;
