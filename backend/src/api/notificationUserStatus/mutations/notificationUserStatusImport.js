const NotificationUserStatusService = require('../../../services/notificationUserStatusService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions').values;

const schema = `
notificationUserStatusImport(data: NotificationUserStatusInput!, importHash: String!): Boolean
`;

const resolver = {
  notificationUserStatusImport: async (root, args, context) => {
    new PermissionChecker(context).validateHas(permissions.notificationImport);
    await new NotificationUserStatusService(context).import(args.data, args.importHash);
    return true;
  },
};

exports.schema = schema;
exports.resolver = resolver;
