const NotificationUserStatusService = require('../../../services/notificationUserStatusService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions').values;

const schema = `
notificationUserStatusCreate(data: NotificationUserStatusInput!): NotificationUserStatus!
`;

const resolver = {
  notificationUserStatusCreate: async (root, args, context) => {
    new PermissionChecker(context).validateHas(permissions.notificationCreate);
    return new NotificationUserStatusService(context).create(args.data);
  },
};

exports.schema = schema;
exports.resolver = resolver;
