const NotificationUserStatusService = require('../../../services/notificationUserStatusService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions').values;

const schema = `
notificationUserStatusUpdate(id: String!, data: NotificationUserStatusInput!): NotificationUserStatus!
`;

const resolver = {
  notificationUserStatusUpdate: async (root, args, context) => {
    new PermissionChecker(context).validateHas(permissions.notificationEdit);
    return new NotificationUserStatusService(context).update(args.id, args.data);
  },
};

exports.schema = schema;
exports.resolver = resolver;
