const NotificationUserStatusService = require('../../../services/notificationUserStatusService');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions').values;

const schema = `
notificationUserStatusDestroy(ids: [String!]!): Boolean
`;

const resolver = {
  notificationUserStatusDestroy: async (root, args, context) => {
    new PermissionChecker(context).validateHas(permissions.notificationDestroy);

    await new NotificationUserStatusService(context).destroyAll(args.ids);

    return true;
  },
};

exports.schema = schema;
exports.resolver = resolver;
