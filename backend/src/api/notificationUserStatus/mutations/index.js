module.exports = [
  require('./notificationUserStatusCreate'), //
  require('./notificationUserStatusDestroy'),
  require('./notificationUserStatusUpdate'),
  require('./notificationUserStatusImport'),
];
