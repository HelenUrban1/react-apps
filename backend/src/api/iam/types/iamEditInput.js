const schema = `
  input IamEditInput {
    id: String!
    firstName: String
    lastName: String
    phoneNumber: String
    avatars: [FileInput!]
    roles: [String]
    accessControl: Boolean
    status: AccountStatusEnum
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
