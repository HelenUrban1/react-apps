const schema = `
  input IamCreateInput {
    emails: [ String! ]!
    firstName: String
    lastName: String
    phoneNumber: String
    avatars: [FileInput!]
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
