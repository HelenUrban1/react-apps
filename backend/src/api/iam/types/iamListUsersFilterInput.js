const schema = `
  input IamListUsersFilterInput {
    id: String
    fullName: String
    email: String
    status: String
    activeAccountId: String
    createdAtRange: [DateTime]
  }
`;

const resolver = {};

exports.schema = schema;
exports.resolver = resolver;
