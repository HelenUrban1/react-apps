const IamEditor = require('../../../services/iam/iamEditor');
const PermissionChecker = require('../../../services/iam/permissionChecker');
const permissions = require('../../../security/permissions').values;
const UserRepository = require('../../../database/repositories/userRepository');

const schema = `
  iamEdit(data: IamEditInput!): UserWithRoles!
  iamChangeOwner(data: IamChangeOwnerInput!): [UserWithRoles!]
  iamArchiveOverflow(paid:Int!): Boolean
`;

const resolver = {
  iamEdit: async (root, args, context) => {
    new PermissionChecker(context).validateHas(permissions.iamEdit);

    const editor = new IamEditor(context.currentUser, context.language);
    await editor.update(args.data, context);
    return UserRepository.findById(args.data.id, context);
  },

  iamChangeOwner: async (root, args, context) => {
    new PermissionChecker(context).validateHas(permissions.iamEdit);

    const editor = new IamEditor(context.currentUser, context.language);

    await editor.transferOwner(args.data, context);

    const updatedUsers = [UserRepository.findById(context.currentUser.id, context), UserRepository.findById(args.data.id, context)];

    return updatedUsers;
  },
  iamArchiveOverflow: async (root, args, context) => {
    new PermissionChecker(context).validateHas(permissions.iamEdit);

    const editor = new IamEditor(context.currentUser, context.language);
    await editor.archiveOverflow(args.paid);
    return true;
  },
};

exports.schema = schema;
exports.resolver = resolver;
