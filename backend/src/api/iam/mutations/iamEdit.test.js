const { resolver } = require('./iamEdit');
const IamEditor = require('../../../services/iam/iamEditor');
const UserRepository = require('../../../database/repositories/userRepository');
const RolesMapper = require('../../../security/rolesMapper');
const Roles = require('../../../security/roles');
const databaseConstants = require('../../../database/constants');
const config = require('../../../../config')();

jest.mock('../../../services/iam/iamEditor');
const findUser = jest.spyOn(UserRepository, 'findById');

IamEditor.update = () => Promise.resolve();
IamEditor.transferOwner = () => Promise.resolve();

const rolesMapper = new RolesMapper({
  accountMemberRoles: databaseConstants.accountMember.ROLES,
  authorizationRolesNames: Roles.names,
  authorizationRolesDefinitions: Roles.definitions,
});

const context = {
  currentUser: {
    accountId: config.testAccountId,
    roles: ['Owner'],
  },
  rolesMapper,
  language: 'en',
};

const args = { data: { id: '8efe26a5-a0bc-4b09-a86f-2302a167dea1' } };
const root = {};
describe('iamEdit Resolvers', () => {
  afterAll(() => {
    jest.clearAllMocks();
  });

  describe('iamEdit', () => {
    it('throws an error if the account lacks permission', async () => {
      await expect(resolver.iamEdit(root, args, { ...context, currentUser: { roles: ['billing'] } })).rejects.toThrowError('Forbidden');
    });
    it('returns the edited user on success', async () => {
      findUser.mockResolvedValueOnce('user');
      await resolver.iamEdit(root, args, context);
      expect(findUser).toHaveBeenCalledTimes(1);
      findUser.mockClear();
    });
  });

  describe('iamChangeOwner', () => {
    it('throws an error if the account lacks permission', async () => {
      await expect(resolver.iamChangeOwner(root, args, { ...context, currentUser: { roles: ['billing'] } })).rejects.toThrowError('Forbidden');
    });
    it('returns both changes users on success', async () => {
      findUser.mockResolvedValueOnce('user');
      await resolver.iamChangeOwner(root, args, context);
      expect(findUser).toHaveBeenCalledTimes(2);
      findUser.mockClear();
    });
  });
});
