const dotenv = require('dotenv');

(async () => {
  const userId = process.argv[2];
  if (!userId) {
    console.error('Please provide an user ID');
    return Promise.resolve();
  }

  dotenv.config({ path: '../../.env' });

  const MetroWorker = require('../clients/metroWorker');
  const AccountService = require('../services/accountService');
  const AccountMemberService = require('../services/accountMemberService');
  const OperatorService = require('../services/operatorService');
  const UserService = require('../services/userService');

  const userService = new UserService({});
  const currentUser = await userService.findById(userId, { userOnly: true });
  const accountMemberService = new AccountMemberService({ currentUser });
  const currentAccountMember = await accountMemberService.findById(currentUser.activeAccountMemberId);
  const accountId = currentAccountMember.accountId;
  currentUser.accountId = accountId;

  const metroWorker = new MetroWorker({
    AccountService: new AccountService({ currentUser }),
    AccountMemberService: accountMemberService,
    UserService: new UserService({ currentUser }), //can't use the old one since it doesn't have currentUser
    OperatorService: new OperatorService({ currentUser }),
  });

  console.log('Enabling Metro...');
  await metroWorker.enableMetro(accountId);
  console.log('Metro enabled!');
  console.log('--------------------------------------------------------------------');

  console.log('Creating operators...');
  const operators = await metroWorker.createOperators(accountId);
  operators.forEach((operator) => {
    if (!operator || !operator.firstName) return;
    console.log('Created an operator for ', `${operator.firstName} ${operator.lastName}`);
  });
  console.log('Operators created!');
  console.log('--------------------------------------------------------------------');

  console.log('Creating service account...');
  await metroWorker.createServiceAccountMember(accountId);
  console.log('Service Account created!');

  console.log('Metro iniitalized for account ', accountId);
})().then(process.exit);
