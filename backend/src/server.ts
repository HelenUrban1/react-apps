/**
 * Starts the application on the port specified.
 */
import 'newrelic';
import { Server } from 'socket.io';
import { authorize } from '@thream/socketio-jwt';
import { createClient } from 'redis';
import { createAdapter } from '@socket.io/redis-adapter';
import https from 'https';
import { log } from './logger';
import ws from './ws';
import CognitoAuthService from './services/auth/cognitoAuthService';

const fs = require('fs');

const isDev = process.env.NODE_ENV === 'development';

const config = require('../config')();

const api = require('./api');

const PORT = process.env.PORT || 8080;
let server;

if (isDev) {
  const cert = fs.readFileSync('../ixc-local.com.crt');
  const key = fs.readFileSync('../device.key');
  const credentials = { key, cert };

  server = https.createServer(credentials, api);

  server.listen(PORT);
} else {
  server = api.listen(PORT, () => {
    log.info(`Listening on port ${PORT}`);
  });
}
const io = new Server(server, {
  path: '/api/socket.io/',
});

// Allows socket.io to send messages to users connected to any server
if (config.redis.enabled === true) {
  try {
    log.info(`Connecting socket.io to redis: ${config.redis.host}:${config.redis.port}`);
    const pubClient = createClient({
      host: config.redis.host,
      port: Number.parseInt(config.redis.port, 10),
    });
    const subClient = pubClient.duplicate();
    io.adapter(createAdapter(pubClient, subClient));
  } catch (error) {
    log.error('Error connect socket.io to redis', error);
  }
}

/**
 * Middleware to authorize requests to Socket.io and add the user to the socket.
 */
io.use(async (socket, next) => {
  try {
    const currentUser = await CognitoAuthService.validateUserToken(socket.handshake.auth.token);
    socket.user = currentUser;
    next();
  } catch (e) {
    log.error(e);
  }
});

api.set('io', io);

ws();
