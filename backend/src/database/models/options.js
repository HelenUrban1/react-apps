const moment = require('moment');

/**
 * Options database model.
 * See https://sequelize.org/v5/manual/models-definition.html to learn how to customize it.
 */
module.exports = function(sequelize, DataTypes) {
  const options = sequelize.define(
    'options',
    {
      id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        primaryKey: true,
      },
      entityAssocitation: {
        type: DataTypes.ENUM,
        allowNull: false,
        values: [
          "Organization",
          "User"
        ],
      },
      status: {
        type: DataTypes.ENUM,
        allowNull: false,
        values: [
          "Active",
          "Archived"
        ],
      },
      label: {
        type: DataTypes.TEXT,
        allowNull: false,
        validate: {
          notEmpty: true,
        }
      },
      tip: {
        type: DataTypes.TEXT,
      },
      details: {
        type: DataTypes.TEXT,
      },
      accessPath: {
        type: DataTypes.TEXT,
        allowNull: false,
        validate: {
          notEmpty: true,
        }
      },
      type: {
        type: DataTypes.ENUM,
        allowNull: false,
        values: [
          "Text",
          "Integer",
          "Decimal",
          "Boolean",
          "Select",
          "Multiselect"
        ],
      },
      options: {
        type: DataTypes.TEXT,
      },
      validations: {
        type: DataTypes.TEXT,
      },
      importHash: {
        type: DataTypes.STRING(255),
        allowNull: true,
        unique: true,
      },
    },
    {
      timestamps: true,
      paranoid: true,
    },
  );

  options.associate = (models) => {




    models.options.belongsTo(models.user, {
      as: 'createdBy',
      constraints: false,
    });

    models.options.belongsTo(models.user, {
      as: 'updatedBy',
      constraints: false,
    });
  };

  return options;
};
