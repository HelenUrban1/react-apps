const { ROLES, PAID_ROLES, STATUSES } = require('../constants').accountMember;
const { log } = require('../../logger');

/**
 * AccountMember database model.
 * See https://sequelize.org/v5/manual/models-definition.html to learn how to customize it.
 */
module.exports = function (sequelize, DataTypes) {
  const accountMember = sequelize.define(
    'accountMember',
    {
      id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        primaryKey: true,
      },
      settings: {
        type: DataTypes.TEXT,
        allowNull: false,
        validate: {
          notEmpty: true,
        },
      },
      roles: {
        type: DataTypes.ARRAY(DataTypes.ENUM(ROLES)),
        defaultValue: [],
        allowNull: false,
      },
      paid: {
        type: DataTypes.VIRTUAL,
        get(rolesOverride = []) {
          // The `this.role` value is populated when running in any scenario that is not
          // a unit test of this model using sequelize-mock. The `rolesOverride` param is
          // provided to allow unit testing of this field because the testing object does
          // not have `this` populated the same way it is when running outside of tests.
          const roles = this.roles || rolesOverride;

          if (!roles.length || !roles.some) {
            return true;
          }

          try {
            // log.debug('setting up paid role');
            // log.debug(roles);
            // Sometimes roles.some is not a function, unclear why when roles.length exists, usually works after the first load

            return roles.some((role) => PAID_ROLES.includes(role));
          } catch (error) {
            log.error(error);
            return false;
          }
        },
        set() {
          throw new Error('The paid field is a virtual field that cannot be manually set. Its value is determined dynamically based on the roles field.');
        },
      },
      status: {
        type: DataTypes.ENUM,
        allowNull: false,
        values: STATUSES,
      },
      inviteStatus: {
        type: DataTypes.TEXT,
        allowNull: true,
      },
      accessControl: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: false,
      },
      importHash: {
        type: DataTypes.STRING(255),
        allowNull: true,
        unique: true,
      },
    },
    {
      timestamps: true,
      paranoid: true,
    }
  );

  accountMember.associate = (models) => {
    models.accountMember.belongsTo(models.account, {
      as: 'account',
      constraints: false,
    });

    models.accountMember.belongsTo(models.site, {
      as: 'site',
      constraints: false,
    });

    models.accountMember.belongsTo(models.user, {
      as: 'user',
      constraints: false,
    });

    models.accountMember.belongsTo(models.user, {
      as: 'createdBy',
    });

    models.accountMember.belongsTo(models.user, {
      as: 'updatedBy',
    });
  };

  return accountMember;
};
