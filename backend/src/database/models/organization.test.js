const modelModule = require('./organization');
const SequelizeMock = require('sequelize-mock');

const { data } = require('../../__fixtures__').organization;

describe('Organization Model', () => {
  let mockSequelize;
  let mockDataTypes;
  let model;

  beforeEach(() => {
    mockSequelize = new SequelizeMock();
    mockDataTypes = mockSequelize.Sequelize;
    model = modelModule(mockSequelize, mockDataTypes);
  });

  describe('#create()', () => {
    it('should create a valid model instance', async () => {
      const fixture = data[0];
      const instance = await model.create(fixture);

      expect(instance.id).toBe(fixture.id);
    });
  });

  describe('#save()', () => {
    it('should honor a change to field', async () => {
      const fixture = data[0];
      const instance = await model.create(fixture);

      expect(instance.settings).toBe(fixture.settings);
      const newSettings = 'some-new-settings';

      instance.settings = newSettings;
      await instance.save();

      expect(instance.settings).toBe(newSettings);
    });
  });
});
