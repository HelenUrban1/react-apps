const modelModule = require('./account');
const SequelizeMock = require('sequelize-mock');

const { data } = require('../../__fixtures__').account;

describe('Account Model', () => {
  let mockSequelize;
  let mockDataTypes;
  let model;

  beforeEach(() => {
    mockSequelize = new SequelizeMock();
    mockDataTypes = mockSequelize.Sequelize;
    model = modelModule(mockSequelize, mockDataTypes);
  });

  describe('#create()', () => {
    it('should create a valid model instance', async () => {
      const fixture = data[0];
      const instance = await model.create(fixture);

      expect(instance.id).toBe(fixture.id);
    });
  });

  describe('#save()', () => {
    it('should honor a change to field', async () => {
      const fixture = data[0];
      const instance = await model.create(fixture);

      expect(instance.orgName).toBe(fixture.orgName);
      const newSettings = 'some-new-title';

      instance.orgName = newSettings;
      await instance.save();

      expect(instance.orgName).toBe(newSettings);
    });
  });
});
