const moment = require('moment');

/**
 * Part database model.
 * See https://sequelize.org/v5/manual/models-definition.html to learn how to customize it.
 */
module.exports = function (sequelize, DataTypes) {
  const measurement = sequelize.define(
    'measurement',
    {
      id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        primaryKey: true,
      },
      value: {
        type: DataTypes.TEXT,
        allowNull: true,
      },
      gage: {
        type: DataTypes.TEXT,
        allowNull: true,
      },
      machine: {
        type: DataTypes.TEXT,
        allowNull: true,
      },
      status: {
        type: DataTypes.ENUM,
        values: ['Unmeasured', 'Pass', 'Fail'],
      },
    },
    {
      timestamps: true,
      paranoid: true,
    }
  );

  measurement.associate = (models) => {
    models.measurement.belongsTo(models.characteristic, {
      as: 'characteristic',
      constraints: false,
    });
    models.measurement.belongsTo(models.sample, {
      as: 'sample',
      constraints: false,
    });
    models.measurement.belongsTo(models.station, {
      as: 'station',
      constraints: false,
    });
    models.measurement.belongsTo(models.listItem, {
      as: 'method',
      constraints: false,
    });
    models.measurement.belongsTo(models.listItem, {
      as: 'operation',
      constraints: false,
    });
    models.measurement.belongsTo(models.operator, {
      as: 'createdBy',
      constraints: false,
    });
    models.measurement.belongsTo(models.operator, {
      as: 'updatedBy',
      constraints: false,
    });
  };

  return measurement;
};
