const modelModule = require('./characteristic');
const SequelizeMock = require('sequelize-mock');

const { data } = require('../../__fixtures__').characteristic;

describe('Characteristic Model', () => {
  let mockSequelize;
  let mockDataTypes;
  let model;

  beforeEach(() => {
    mockSequelize = new SequelizeMock();
    mockDataTypes = mockSequelize.Sequelize;
    model = modelModule(mockSequelize, mockDataTypes);
  });

  describe('#create()', () => {
    it('should create a valid model instance', async () => {
      const fixture = data[0];
      const instance = await model.create(fixture);

      expect(instance.id).toBe(fixture.id);
    });
  });

  describe('#save()', () => {
    it('should honor a change to field', async () => {
      const fixture = data[0];
      const instance = await model.create(fixture);

      expect(instance.nominal).toBe(fixture.nominal);
      const newSettings = '2.55 +.01 -.02';

      instance.nominal = newSettings;
      await instance.save();

      expect(instance.nominal).toBe(newSettings);
    });
  });
});