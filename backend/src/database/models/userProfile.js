const { ROLES } = require('../constants').accountMember;
/**
 * UserProfile database model.
 * See https://sequelize.org/v5/manual/models-definition.html to learn how to customize it.
 */
module.exports = function (sequelize, DataTypes) {
  const userProfile = sequelize.define(
    'userProfile',
    {
      id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        primaryKey: true,
      },
      fullName: {
        type: DataTypes.STRING(255),
        allowNull: true,
      },
      firstName: {
        type: DataTypes.STRING(80),
        allowNull: true,
      },
      lastName: {
        type: DataTypes.STRING(175),
        allowNull: true,
      },
      phoneNumber: {
        type: DataTypes.STRING(24),
        allowNull: true,
      },
      roles: {
        type: DataTypes.ARRAY(DataTypes.ENUM(ROLES)),
        defaultValue: [],
        allowNull: true,
      },
      importHash: {
        type: DataTypes.STRING(255),
        allowNull: true,
        unique: true,
      },
    },
    {
      timestamps: true,
      paranoid: true,
    }
  );

  userProfile.associate = (models) => {
    models.userProfile.belongsTo(models.user, {
      as: 'user',
      constraints: false,
    });
    models.userProfile.belongsTo(models.user, {
      as: 'createdBy',
      constraints: false,
    });

    models.userProfile.belongsTo(models.user, {
      as: 'updatedBy',
      constraints: false,
    });
  };

  /**
   * The model.addHook() syntax is needed instead of model.hookMethod()
   * due to the sequelize-mock testing lib not yet supporting the latter
   * syntax and throwing an error upon encountering it.
   */
  userProfile.addHook('beforeCreate', (userProfile, options) => {
    userProfile = trimStringFields(userProfile);
    userProfile.fullName = buildFullName(userProfile.firstName, userProfile.lastName);
  });

  /**
   * The model.addHook() syntax is needed instead of model.hookMethod()
   * due to the sequelize-mock testing lib not yet supporting the latter
   * syntax and throwing an error upon encountering it.
   */
  userProfile.addHook('beforeUpdate', (userProfile, options) => {
    userProfile = trimStringFields(userProfile);
    userProfile.fullName = buildFullName(userProfile.firstName, userProfile.lastName);
  });

  return userProfile;
};

function buildFullName(firstName, lastName) {
  if (!firstName && !lastName) {
    return null;
  }
  return `${(firstName || '').trim()} ${(lastName || '').trim()}`.trim();
}

function trimStringFields(userProfile) {
  userProfile.firstName = userProfile.firstName ? userProfile.firstName.trim() : null;
  userProfile.lastName = userProfile.lastName ? userProfile.lastName.trim() : null;
  return userProfile;
}
