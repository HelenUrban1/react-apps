const SequelizeMock = require('sequelize-mock');
const modelModule = require('./annotation');

const { data } = require('../../__fixtures__').annotation;

describe('Annotation Model', () => {
  let mockSequelize;
  let mockDataTypes;
  let model;

  beforeEach(() => {
    mockSequelize = new SequelizeMock();
    mockDataTypes = mockSequelize.Sequelize;
    model = modelModule(mockSequelize, mockDataTypes);
  });

  describe('#create()', () => {
    it('should create a valid model instance', async () => {
      const fixture = data[0];
      const instance = await model.create(fixture);

      expect(instance.id).toBe(fixture.id);
    });
  });

  describe('#save()', () => {
    it('should honor a change to field', async () => {
      const fixture = data[0];
      const instance = await model.create(fixture);

      expect(instance.annotation).toBe(fixture.annotation);
      const newAnnotation = 'new annotation';

      instance.annotation = newAnnotation;
      await instance.save();

      expect(instance.annotation).toBe(newAnnotation);
    });
  });
});
