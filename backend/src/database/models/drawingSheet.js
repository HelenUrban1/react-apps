/**
 * DrawingSheet database model.
 * See https://sequelize.org/v5/manual/models-definition.html to learn how to customize it.
 */
module.exports = function (sequelize, DataTypes) {
  const drawingSheet = sequelize.define(
    'drawingSheet',
    {
      id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        primaryKey: true,
      },
      pageName: {
        type: DataTypes.TEXT,
        defaultValue: '',
        allowNull: false,
      },
      pageIndex: {
        type: DataTypes.INTEGER,
        allowNull: false,
        validate: {
          min: 0,
        },
      },
      height: {
        type: DataTypes.INTEGER,
        validate: {},
      },
      width: {
        type: DataTypes.INTEGER,
      },
      markerSize: {
        type: DataTypes.INTEGER,
        allowNull: true,
      },
      rotation: {
        type: DataTypes.TEXT,
        validate: {
          len: [-360, 360],
        },
      },
      grid: {
        type: DataTypes.TEXT,
      },
      useGrid: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: true,
      },
      displayLines: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: false,
      },
      displayLabels: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: false,
      },
      importHash: {
        type: DataTypes.STRING(255),
        allowNull: true,
        unique: true,
      },
      foundCaptureCount: {
        type: DataTypes.INTEGER,
        allowNull: false,
        defaultValue: 0,
      },
      acceptedCaptureCount: {
        type: DataTypes.INTEGER,
        allowNull: false,
        defaultValue: 0,
      },
      acceptedTimeoutCaptureCount: {
        type: DataTypes.INTEGER,
        allowNull: false,
        defaultValue: 0,
      },
      rejectedCaptureCount: {
        type: DataTypes.INTEGER,
        allowNull: false,
        defaultValue: 0,
      },
      reviewedCaptureCount: {
        type: DataTypes.INTEGER,
        allowNull: false,
        defaultValue: 0,
      },
      reviewedTimeoutCaptureCount: {
        type: DataTypes.INTEGER,
        allowNull: false,
        defaultValue: 0,
      },
      defaultLinearTolerances: {
        type: DataTypes.TEXT,
      },
      defaultAngularTolerances: {
        type: DataTypes.TEXT,
      },
      autoMarkupStatus: {
        type: DataTypes.ENUM,
        values: ['In_Progress', 'Completed', 'Error'],
        allowNull: true,
        defaultValue: null,
      },
    },
    {
      timestamps: true,
      paranoid: true,
    },
  );

  drawingSheet.associate = (models) => {
    models.drawingSheet.belongsTo(models.part, {
      as: 'part',
      constraints: false,
    });
    models.drawingSheet.belongsTo(models.drawingSheet, {
      as: 'PreviousRevision',
      constraints: false,
      foreignKey: 'previousRevision',
    });
    models.drawingSheet.belongsTo(models.drawingSheet, {
      as: 'NextRevision',
      constraints: false,
      foreignKey: 'nextRevision',
    });
    models.drawingSheet.belongsTo(models.drawingSheet, {
      as: 'OriginalSheet',
      constraints: false,
      foreignKey: 'originalSheet',
    });

    models.drawingSheet.belongsTo(models.drawing, {
      as: 'drawing',
      constraints: false,
    });

    models.drawingSheet.hasMany(models.characteristic, {
      as: 'characteristics',
      constraints: false,
      foreignKey: 'drawingSheetId',
    });

    models.drawingSheet.belongsTo(models.user, {
      as: 'createdBy',
      constraints: false,
    });

    models.drawingSheet.belongsTo(models.user, {
      as: 'updatedBy',
      constraints: false,
    });
  };

  return drawingSheet;
};
