const moment = require('moment');

/**
 * Account database model.
 * See https://sequelize.org/v5/manual/models-definition.html to learn how to customize it.
 */
module.exports = function (sequelize, DataTypes) {
  const account = sequelize.define(
    'account',
    {
      id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        primaryKey: true,
      },
      status: {
        type: DataTypes.ENUM,
        allowNull: false,
        values: ['Active', 'Past_Due', 'Suspended', 'Archived'],
      },
      settings: {
        type: DataTypes.TEXT,
        allowNull: false,
        validate: {
          notEmpty: true,
        },
      },
      orgName: {
        type: DataTypes.TEXT,
        allowNull: false,
        validate: {
          notEmpty: true,
        },
      },
      orgPhone: {
        type: DataTypes.TEXT,
      },
      orgEmail: {
        type: DataTypes.TEXT,
        allowNull: false,
        validate: {
          notEmpty: true,
        },
      },
      orgStreet: {
        type: DataTypes.TEXT,
      },
      orgStreet2: {
        type: DataTypes.TEXT,
      },
      orgCity: {
        type: DataTypes.TEXT,
      },
      orgRegion: {
        type: DataTypes.ENUM,
        values: [
          'Alabama',
          'Alaska',
          'American_Samoa',
          'Arizona',
          'Arkansas',
          'California',
          'Colorado',
          'Connecticut',
          'Delaware',
          'District_of_Columbia',
          'Florida',
          'Georgia',
          'Guam',
          'Hawaii',
          'Idaho',
          'Illinois',
          'Indiana',
          'Iowa',
          'Kansas',
          'Kentucky',
          'Louisiana',
          'Maine',
          'Maryland',
          'Massachusetts',
          'Michigan',
          'Minnesota',
          'Minor_Outlying_Islands',
          'Mississippi',
          'Missouri',
          'Montana',
          'Nebraska',
          'Nevada',
          'New_Hampshire',
          'New_Jersey',
          'New_Mexico',
          'New_York',
          'North_Carolina',
          'North_Dakota',
          'Northern_Mariana_Islands',
          'Ohio',
          'Oklahoma',
          'Oregon',
          'Pennsylvania',
          'Puerto_Rico',
          'Rhode_Island',
          'South_Carolina',
          'South_Dakota',
          'Tennessee',
          'Texas',
          'US_Virgin_Islands',
          'Utah',
          'Vermont',
          'Virginia',
          'Washington',
          'West_Virginia',
          'Wisconsin',
          'Wyoming',
        ],
      },
      orgPostalcode: {
        type: DataTypes.TEXT,
      },
      orgCountry: {
        type: DataTypes.ENUM,
        values: ['United_States'],
      },
      orgUnitName: {
        type: DataTypes.TEXT,
      },
      orgUnitId: {
        type: DataTypes.UUID,
        allowNull: true,
        validate: {
          customValidator(value) {
            if (value === null && this.parentAccountId !== null) {
              throw new Error('orgUnitId is required for accounts with a parent account');
            }
          },
        },
      },
      parentAccountId: {
        type: DataTypes.UUID,
        allowNull: true,
        validate: {
          customValidator(value) {
            if (value === null && this.orgUnitId !== null) {
              throw new Error('parentAccountId is required for accounts with an org unit set');
            }
          },
        },
      },
      importHash: {
        type: DataTypes.STRING(255),
        allowNull: true,
        unique: true,
      },
      dbHost: {
        type: DataTypes.STRING(255),
        allowNull: false,
      },
      dbName: {
        type: DataTypes.STRING(255),
        allowNull: false,
      },
    },
    {
      timestamps: true,
      paranoid: true,
    }
  );

  account.associate = (models) => {
    models.account.hasMany(models.accountMember, {
      as: 'members',
      constraints: false,
      foreignKey: 'accountId',
    });

    models.account.hasMany(models.subscription, {
      foreignKey: 'accountId',
      constraints: false,
    });

    models.account.belongsTo(models.user, {
      as: 'createdBy',
    });

    models.account.belongsTo(models.user, {
      as: 'updatedBy',
    });
  };

  return account;
};
