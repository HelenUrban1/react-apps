/**
 * Sample database model.
 * See https://sequelize.org/v5/manual/models-definition.html to learn how to customize it.
 */
module.exports = function (sequelize, DataTypes) {
  const sample = sequelize.define(
    'sample',
    {
      id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        primaryKey: true,
      },
      serial: {
        type: DataTypes.TEXT,
        allowNull: false,
        validate: {
          notEmpty: true,
        },
      },
      sampleIndex: {
        type: DataTypes.INTEGER,
        allowNull: false,
        defaultValue: 0,
      },
      featureCoverage: {
        type: DataTypes.TEXT,
        allowNull: true,
      },
      status: {
        type: DataTypes.ENUM,
        values: ['Unmeasured', 'Pass', 'Fail', 'Scrap'],
        defaultValue: 'Unmeasured',
        allowNull: false,
      },
      importHash: {
        type: DataTypes.STRING(255),
        allowNull: true,
        unique: true,
      },
    },
    {
      timestamps: true,
      paranoid: true,
    }
  );

  sample.associate = (models) => {
    models.sample.belongsTo(models.part, {
      as: 'part',
      constraints: false,
    });

    models.sample.belongsTo(models.job, {
      as: 'job',
      constraints: false,
    });

    models.sample.belongsTo(models.operator, {
      as: 'updatingOperator',
      constraints: false,
    });

    models.sample.hasMany(models.measurement, {
      as: 'measurements',
      constraints: false,
    });
  };

  return sample;
};
