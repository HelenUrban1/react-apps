'use strict';
module.exports = (sequelize, DataTypes) => {
  const organization = sequelize.define(
    'organization',
    {
      id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        primaryKey: true,
      },
      status: {
        type: DataTypes.ENUM,
        allowNull: false,
        values: ['Active', 'Archived'],
      },
      type: {
        type: DataTypes.ENUM,
        allowNull: false,
        values: ['Account', 'Customer'],
      },
      settings: {
        type: DataTypes.TEXT,
      },
      name: {
        type: DataTypes.TEXT,
        allowNull: false,
        validate: {
          notEmpty: true,
        },
      },
      phone: {
        type: DataTypes.TEXT,
      },
      email: {
        type: DataTypes.TEXT,
      },
      street1: {
        type: DataTypes.TEXT,
      },
      street2: {
        type: DataTypes.TEXT,
      },
      city: {
        type: DataTypes.TEXT,
      },
      region: {
        type: DataTypes.ENUM,
        values: [
          'Alabama',
          'Alaska',
          'American_Samoa',
          'Arizona',
          'Arkansas',
          'California',
          'Colorado',
          'Connecticut',
          'Delaware',
          'District_of_Columbia',
          'Florida',
          'Georgia',
          'Guam',
          'Hawaii',
          'Idaho',
          'Illinois',
          'Indiana',
          'Iowa',
          'Kansas',
          'Kentucky',
          'Louisiana',
          'Maine',
          'Maryland',
          'Massachusetts',
          'Michigan',
          'Minnesota',
          'Minor_Outlying_Islands',
          'Mississippi',
          'Missouri',
          'Montana',
          'Nebraska',
          'Nevada',
          'New_Hampshire',
          'New_Jersey',
          'New_Mexico',
          'New_York',
          'North_Carolina',
          'North_Dakota',
          'Northern_Mariana_Islands',
          'Ohio',
          'Oklahoma',
          'Oregon',
          'Pennsylvania',
          'Puerto_Rico',
          'Rhode_Island',
          'South_Carolina',
          'South_Dakota',
          'Tennessee',
          'Texas',
          'US_Virgin_Islands',
          'Utah',
          'Vermont',
          'Virginia',
          'Washington',
          'West_Virginia',
          'Wisconsin',
          'Wyoming',
        ],
      },
      postalCode: {
        type: DataTypes.TEXT,
      },
      country: {
        type: DataTypes.ENUM,
        values: ['United_States'],
      },
    },
    {
      timestamps: true,
      paranoid: true,
    },
  );
  organization.associate = models => {
    models.organization.belongsTo(models.site, {
      as: 'site',
      constraints: false,
    });

    models.organization.hasMany(models.file, {
      as: 'orgLogo',
      foreignKey: 'belongsToId',
      constraints: false,
      scope: {
        belongsTo: models.organization.getTableName(),
        belongsToColumn: 'orgLogo',
      },
    });

    models.organization.hasMany(models.part, {
      as: 'parts',
      constraints: false,
      foreignKey: 'customerId',
    });

    models.organization.hasMany(models.report, {
      as: 'reports',
      constraints: false,
      foreignKey: 'customerId',
    });

    models.organization.hasMany(models.reportTemplate, {
      as: 'templates',
      constraints: false,
      foreignKey: 'providerId',
    });

    models.organization.belongsTo(models.user, {
      as: 'createdBy',
      constraints: false,
    });

    models.organization.belongsTo(models.user, {
      as: 'updatedBy',
      constraints: false,
    });
  };
  return organization;
};
