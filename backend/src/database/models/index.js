/**
 * This module creates the Sequelize to the database and
 * exports all the models.
 */

const fs = require('fs');
const path = require('path');
const { Sequelize, DataTypes } = require('sequelize');
const Umzug = require('umzug');

const basename = path.basename(module.filename);
const { secretsManager } = require('aws-wrapper');
const config = require('../../../config')();
const { log } = require('../../logger');

const db = {};

log.debug('Setting up Sequelize connection');
const isMigration = !!process.env.MIGRATION_ENV;
const env = isMigration ? process.env.MIGRATION_ENV : process.env.NODE_ENV;

const sequelize = new Sequelize(
  config.database.database, //
  config.database.username,
  config.database.password,
  {
    ...config.database,
    logging: log.trace.bind(log),
  }
);

const sequelizeAdmin = new Sequelize(
  config.databaseAdmin.database, //
  config.databaseAdmin.username,
  config.databaseAdmin.password,
  {
    ...config.databaseAdmin,
    logging: log.trace.bind(log),
  }
);

fs.readdirSync(`${__dirname}`)
  .filter((file) => {
    return file.indexOf('.') !== 0 && file !== basename && file.slice(-3) === '.js' && file.slice(-8) !== '.test.js';
  })
  .forEach((file) => {
    const model = require(path.join(`${__dirname}`, file))(sequelize, DataTypes);
    db[model.name] = model;
  });

Object.keys(db).forEach((modelName) => {
  if (db[modelName].associate) {
    db[modelName].associate(db);
  }
});

try {
  if (env !== 'test') {
    const umzug = new Umzug({
      migrations: {
        path: path.join(__dirname, '../../../storage/migrations'),
        params: [sequelize.getQueryInterface()],
      },
      storage: 'sequelize',
      storageOptions: {
        sequelize,
      },
    });

    (async () => {
      await umzug.up();
    })();
  }
} catch (error) {
  log.error(`Error migrating primary database`, error);
  log.error(error.errors);
}

db.sequelize = sequelize;
db.sequelizeAdmin = sequelizeAdmin;
db.Sequelize = Sequelize;

const createTenant = async (accountId, initialize, skipMigrate, dbPassword) => {
  log.info(`Creating tenant connection for ${accountId}`);
  let accountResponse = await db.account.findByPk(accountId);

  // QUESTION: Why is the error check disabled during testing? Failure cases should be tested.
  if (env !== 'test' && !accountResponse) {
    log.error(`Looking up tenant account by id failed, accountId: ${accountId}`);
    throw new Error('Account not found, try logging out and back in.');
  } else if (env === 'test') {
    // if testing, construct the needed account values so we don't have to populate the db for every test
    accountResponse = {
      dataValues: {
        id: accountId,
        dbName: `${config.accountDbPrefix}test_${accountId}`, // This string format should be pulled from a central location
        dbHost: config.database.host,
      },
    };
  }
  const account = accountResponse.dataValues;

  let accountDbPassword = dbPassword;
  if (!dbPassword) {
    log.info(`Getting database password for ${account.id}`);
    accountDbPassword = await secretsManager.getDatabasePassword(account.id);
  }

  // Initialize the connection
  const tenant = new Sequelize(account.dbName, `${config.accountDbPrefix}${env}_${accountId}`, accountDbPassword, {
    dialect: 'postgres', // TODO: This should move to config
    host: config.database.host, //  Value was `account.dbHost`.
    logging: log.trace.bind(log),
  });

  // Load the models
  const tenantDb = {};
  fs.readdirSync(`${__dirname}`)
    .filter((file) => {
      return file.indexOf('.') !== 0 && file !== basename && file.slice(-3) === '.js' && file.slice(-8) !== '.test.js';
    })
    .forEach((file) => {
      const model = require(path.join(`${__dirname}`, file))(tenant, DataTypes);
      tenantDb[model.name] = model;
    });
  Object.keys(tenantDb).forEach((modelName) => {
    if (tenantDb[modelName].associate) {
      tenantDb[modelName].associate(tenantDb);
    }
  });
  tenantDb.sequelize = tenant;
  tenantDb.Sequelize = Sequelize;
  // Checking for a specific string here so its not too easy to pass true and mess with existing data
  if (initialize === 'syncDatabase') {
    await tenantDb.sequelize.sync();
  }

  try {
    if (env !== 'test' && !skipMigrate) {
      const umzugTenant = new Umzug({
        migrations: {
          path: path.join(__dirname, '../../../storage/migrations'),
          params: [tenant.getQueryInterface()],
        },
        storage: 'sequelize',
        storageOptions: {
          sequelize: tenant,
        },
      });

      await umzugTenant.up();
    }
  } catch (error) {
    log.error(`Error migrating tenant: ${accountId}`, error);
    log.error(error.errors);
  }

  return tenantDb;
};

// Load tenants as they are requested
const tenants = {};
db.getTenant = async (accountId, initialize, skipMigrate, dbPassword) => {
  if (!accountId) {
    log.error('Tenant requested without passing in an accountId');
    throw new Error('Account ID not found, try logging out and back in.');
  }
  // Return loaded tenant connection if it exists
  if (tenants[accountId]) {
    return tenants[accountId];
  }

  // Not loaded, find the tenant and cache
  tenants[accountId] = await createTenant(accountId, initialize, skipMigrate, dbPassword);
  return tenants[accountId];
};

module.exports = db;
