const SequelizeMock = require('sequelize-mock');
const modelModule = require('./reviewTask');

const { data } = require('../../__fixtures__').reviewTask;

describe('Review Task Model', () => {
  let mockSequelize;
  let mockDataTypes;
  let model;

  beforeEach(() => {
    mockSequelize = new SequelizeMock();
    mockDataTypes = mockSequelize.Sequelize;
    model = modelModule(mockSequelize, mockDataTypes);
  });

  describe('#create()', () => {
    it('should create a valid model instance', async () => {
      const fixture = data[0];
      const instance = await model.create(fixture);

      expect(instance.id).toBe(fixture.id);
    });
  });

  describe('#save()', () => {
    it('should honor a change to field', async () => {
      const fixture = data[0];
      const instance = await model.create(fixture);

      expect(instance.metadata).toBe(fixture.metadata);
      const newMetadata = 'some-new-value';

      instance.metadata = newMetadata;
      await instance.save();

      expect(instance.metadata).toBe(newMetadata);
    });
  });
});
