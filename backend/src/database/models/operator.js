const moment = require('moment');

/**
 * Station database model.
 * See https://sequelize.org/v5/manual/models-definition.html to learn how to customize it.
 */
module.exports = function (sequelize, DataTypes) {
  const operator = sequelize.define(
    'operator',
    {
      id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        primaryKey: true,
      },
      accessControl: {
        type: DataTypes.ENUM,
        values: ['None', 'ITAR'],
      },
      email: {
        type: DataTypes.STRING(255),
        allowNull: true,
      },
      fullName: {
        type: DataTypes.STRING(255),
        allowNull: true,
      },
      firstName: {
        type: DataTypes.STRING(80),
        allowNull: true,
      },
      lastName: {
        type: DataTypes.STRING(175),
        allowNull: true,
      },
      status: {
        type: DataTypes.STRING(80),
        allowNull: false,
        defaultValue: 'Active',
      },
      pin: {
        type: DataTypes.STRING(8),
        allowNull: true,
      },
    },
    {
      timestamps: true,
      paranoid: true,
    }
  );

  operator.associate = (models) => {
    models.operator.belongsTo(models.accountMember, {
      as: 'createdBy',
      constraints: false,
    });

    models.operator.belongsTo(models.user, {
      as: 'updatedBy',
      constraints: false,
    });
  };

  return operator;
};
