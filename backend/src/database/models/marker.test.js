const modelModule = require('./marker');
const SequelizeMock = require('sequelize-mock');

const { data } = require('../../__fixtures__').marker;

describe('Marker Model', () => {
  let mockSequelize;
  let mockDataTypes;
  let model;

  beforeEach(() => {
    mockSequelize = new SequelizeMock();
    mockDataTypes = mockSequelize.Sequelize;
    model = modelModule(mockSequelize, mockDataTypes);
  });

  describe('#create()', () => {
    it('should create a valid model instance', async () => {
      const fixture = data[0];
      const instance = await model.create(fixture);

      expect(instance.id).toBe(fixture.id);
    });
  });

  describe('#save()', () => {
    it('should honor a change to field', async () => {
      const fixture = data[0];
      const instance = await model.create(fixture);

      expect(instance.markerStyleName).toBe(fixture.markerStyleName);
      const newSettings = 'some-new-name';

      instance.markerStyleName = newSettings;
      await instance.save();

      expect(instance.markerStyleName).toBe(newSettings);
    });
  });
});