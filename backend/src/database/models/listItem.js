/**
 * Setting database model.
 * See https://sequelize.org/v5/manual/models-definition.html to learn how to customize it.
 */
module.exports = (sequelize, DataTypes) => {
  const listItem = sequelize.define(
    'listItem',
    {
      id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        primaryKey: true,
      },
      name: {
        type: DataTypes.TEXT,
        allowNull: false,
        validate: {
          notEmpty: true,
        },
      },
      metadata: {
        type: DataTypes.TEXT,
      },
      default: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: false,
      },
      status: {
        type: DataTypes.ENUM,
        allowNull: false,
        defaultValue: 'Active',
        values: ['Inactive', 'Active', 'Rejected'],
      },
      listType: {
        type: DataTypes.ENUM,
        allowNull: false,
        values: [
          'Type', //
          'Unit_of_Measurement',
          'Classification',
          'Operation',
          'Inspection_Method',
          'Access_Control',
        ],
      },
      itemIndex: {
        type: DataTypes.INTEGER,
        allowNull: false,
        defaultValue: 0,
      },
    },
    {
      indexes: [
        {
          unique: false,
          fields: ['listType'],
        },
      ],
      timestamps: true,
      paranoid: true,
    }
  );

  listItem.associate = (models) => {
    models.listItem.belongsTo(models.site, {
      as: 'site',
      constraints: false,
    });

    models.listItem.belongsTo(models.user, {
      as: 'user',
      constraints: false,
    });

    models.listItem.belongsTo(models.listItem, {
      as: 'parent',
      constraints: false,
    });

    models.listItem.belongsTo(models.user, {
      as: 'createdBy',
      constraints: false,
    });

    models.listItem.belongsTo(models.user, {
      as: 'updatedBy',
      constraints: false,
    });
  };

  return listItem;
};
