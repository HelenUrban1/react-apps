const moment = require('moment');

/**
 * Part database model.
 * See https://sequelize.org/v5/manual/models-definition.html to learn how to customize it.
 */
module.exports = function (sequelize, DataTypes) {
  const job = sequelize.define(
    'job',
    {
      id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        primaryKey: true,
      },
      name: {
        type: DataTypes.STRING,
        allowNull: false,
        validate: {
          notEmpty: true,
        },
      },
      accessControl: {
        type: DataTypes.ENUM,
        values: ['None', 'ITAR'],
      },
      sampling: {
        type: DataTypes.ENUM,
        values: ['Timed', 'Quantity'],
      },
      jobStatus: {
        type: DataTypes.ENUM,
        values: ['Active', 'Inactive', 'Complete', 'Deleted'],
      },
      interval: {
        type: DataTypes.INTEGER,
      },
      samples: {
        type: DataTypes.INTEGER,
      },
      passing: {
        type: DataTypes.INTEGER,
      },
      scrapped: {
        type: DataTypes.INTEGER,
      },
    },
    {
      timestamps: true,
      paranoid: true,
    }
  );

  job.associate = (models) => {
    models.job.hasMany(models.sample, {
      as: 'jobSamples',
      constraints: false,
      foreignKey: 'jobId',
    });
    models.job.belongsToMany(models.part, {
      through: 'jobParts',
      as: 'parts',
      foreign_key: 'jobId',
    });
  };

  return job;
};
