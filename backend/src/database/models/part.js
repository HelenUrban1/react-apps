const moment = require('moment');

/**
 * Part database model.
 * See https://sequelize.org/v5/manual/models-definition.html to learn how to customize it.
 */
module.exports = function (sequelize, DataTypes) {
  const part = sequelize.define(
    'part',
    {
      id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        primaryKey: true,
      },
      purchaseOrderCode: {
        type: DataTypes.TEXT,
      },
      purchaseOrderLink: {
        type: DataTypes.TEXT,
      },
      type: {
        type: DataTypes.ENUM,
        values: ['Part', 'Assembly'],
      },
      name: {
        type: DataTypes.TEXT,
        allowNull: false,
      },
      number: {
        type: DataTypes.TEXT,
      },
      revision: {
        type: DataTypes.TEXT,
      },
      drawingNumber: {
        type: DataTypes.TEXT,
      },
      drawingRevision: {
        type: DataTypes.TEXT,
      },
      defaultMarkerOptions: {
        type: DataTypes.TEXT,
      },
      defaultLinearTolerances: {
        type: DataTypes.TEXT,
      },
      defaultAngularTolerances: {
        type: DataTypes.TEXT,
      },
      presets: {
        type: DataTypes.TEXT,
        allowNull: true,
      },
      status: {
        type: DataTypes.ENUM,
        allowNull: false,
        values: ['Active', 'Inactive', 'Deleted'],
      },
      workflowStage: {
        type: DataTypes.INTEGER,
        allowNull: false,
        validate: {
          notEmpty: true,
        },
      },
      reviewStatus: {
        type: DataTypes.ENUM,
        allowNull: false,
        values: ['Unverified', 'Ready_for_Review', 'Verified'],
      },
      autoMarkupStatus: {
        type: DataTypes.ENUM,
        values: ['In_Progress', 'Completed', 'Error'],
      },
      completedAt: {
        type: DataTypes.DATEONLY,
        get() {
          return this.getDataValue('completedAt') ? moment.utc(this.getDataValue('completedAt')).format('YYYY-MM-DD') : null;
        },
      },
      balloonedAt: {
        type: DataTypes.DATE,
      },
      reportGeneratedAt: {
        type: DataTypes.DATE,
      },
      // TODO: This should be saved in a history table of some sort
      lastReportId: {
        type: DataTypes.TEXT,
      },
      notes: {
        type: DataTypes.TEXT,
      },
      accessControl: {
        type: DataTypes.ENUM,
        values: ['None', 'ITAR'],
      },
      measurement: {
        type: DataTypes.ENUM,
        values: ['Metric', 'Imperial'],
      },
      tags: {
        type: DataTypes.TEXT,
      },
      displayLeaderLines: {
        type: DataTypes.BOOLEAN,
        defaultValue: true,
      },
      importHash: {
        type: DataTypes.STRING(255),
        allowNull: true,
        unique: true,
      },
    },
    {
      timestamps: true,
      paranoid: true,
    },
  );

  part.associate = (models) => {
    models.part.belongsTo(models.part, {
      as: 'PreviousRevision',
      constraints: false,
      foreignKey: 'previousRevision',
    });
    models.part.belongsTo(models.part, {
      as: 'NextRevision',
      constraints: false,
      foreignKey: 'nextRevision',
    });
    models.part.belongsTo(models.part, {
      as: 'OriginalPart',
      constraints: false,
      foreignKey: 'originalPart',
    });

    models.part.belongsTo(models.site, {
      as: 'site',
      constraints: false,
    });

    models.part.belongsTo(models.organization, {
      as: 'customer',
      constraints: false,
    });

    models.part.belongsTo(models.drawing, {
      as: 'primaryDrawing',
      constraints: false,
    });

    models.part.hasMany(models.drawing, {
      as: 'drawings',
      constraints: false,
      foreignKey: 'partId',
    });

    models.part.hasMany(models.file, {
      as: 'attachments',
      constraints: false,
      foreignKey: 'belongsToId',
    });

    models.part.hasMany(models.characteristic, {
      as: 'characteristics',
      constraints: false,
      foreignKey: 'partId',
    });

    models.part.hasMany(models.report, {
      as: 'reports',
      constraints: false,
      foreignKey: 'partId',
    });

    models.part.belongsToMany(models.job, {
      through: 'jobParts',
      as: 'jobs',
      foreignKey: 'partId',
    });

    models.part.belongsTo(models.user, {
      as: 'createdBy',
      constraints: false,
    });

    models.part.belongsTo(models.user, {
      as: 'updatedBy',
      constraints: false,
    });
  };

  return part;
};
