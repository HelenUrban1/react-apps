const SequelizeMock = require('sequelize-mock');
const modelModule = require('./user');

const { data } = require('../../__fixtures__').user;

describe('User Model', () => {
  let mockSequelize;
  let mockDataTypes;
  let model;

  beforeEach(() => {
    mockSequelize = new SequelizeMock();
    mockDataTypes = mockSequelize.Sequelize;
    model = modelModule(mockSequelize, mockDataTypes);
  });

  describe('#create()', () => {
    it('should create a valid model instance', async () => {
      const fixture = data[0];

      const instance = await model.create(fixture);
      expect(instance.id).toBe(fixture.id);
    });
  });

  describe('#save()', () => {
    it('should honor a change to field', async () => {
      const fixture = data[0];
      const instance = await model.create(fixture);

      expect(instance.profile.firstName).toBe(fixture.profile.firstName);
      const newSettings = 'some-new-first-name';

      instance.profile.firstName = newSettings;
      await instance.save();

      expect(instance.profile.firstName).toBe(newSettings);
    });
  });
});
