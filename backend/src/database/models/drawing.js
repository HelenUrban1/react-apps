const moment = require('moment');

/**
 * Drawing database model.
 * See https://sequelize.org/v5/manual/models-definition.html to learn how to customize it.
 */
module.exports = function (sequelize, DataTypes) {
  const drawing = sequelize.define(
    'drawing',
    {
      id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        primaryKey: true,
      },
      sheetCount: {
        type: DataTypes.INTEGER,
        defaultValue: 1,
        allowNull: false,
        validate: {
          notEmpty: true,
        },
      },
      documentType: {
        type: DataTypes.ENUM,
        values: ['Drawing', 'Materials', 'Specifications', 'Notes', 'Other'],
        defaultValue: 'Drawing',
      },
      status: {
        type: DataTypes.ENUM,
        allowNull: false,
        values: ['Active', 'Deleted'],
      },
      name: {
        type: DataTypes.TEXT,
        allowNull: false,
      },
      number: {
        type: DataTypes.TEXT,
      },
      revision: {
        type: DataTypes.TEXT,
      },
      linearUnit: {
        type: DataTypes.TEXT,
        allowNull: false,
        validate: {
          notEmpty: true,
        },
      },
      angularUnit: {
        type: DataTypes.TEXT,
        allowNull: false,
        validate: {
          notEmpty: true,
        },
      },
      markerOptions: {
        type: DataTypes.TEXT,
        allowNull: false,
        validate: {
          notEmpty: true,
        },
      },
      gridOptions: {
        type: DataTypes.TEXT,
      },
      linearTolerances: {
        type: DataTypes.TEXT,
        allowNull: false,
        validate: {
          notEmpty: true,
        },
      },
      tags: {
        type: DataTypes.TEXT,
        allowNull: false,
        validate: {
          notEmpty: true,
        },
      },
      angularTolerances: {
        type: DataTypes.TEXT,
        allowNull: false,
        validate: {
          notEmpty: true,
        },
      },
      importHash: {
        type: DataTypes.STRING(255),
        allowNull: true,
        unique: true,
      },
    },
    {
      timestamps: true,
      paranoid: true,
    }
  );

  drawing.associate = (models) => {
    models.drawing.belongsTo(models.site, {
      as: 'site',
      constraints: false,
    });
    models.drawing.belongsTo(models.drawing, {
      as: 'PreviousRevision',
      constraints: false,
      foreignKey: 'previousRevision',
    });
    models.drawing.belongsTo(models.drawing, {
      as: 'NextRevision',
      constraints: false,
      foreignKey: 'nextRevision',
    });
    models.drawing.belongsTo(models.drawing, {
      as: 'OriginalDrawing',
      constraints: false,
      foreignKey: 'originalDrawing',
    });
    models.drawing.belongsTo(models.file, {
      as: 'drawingFile',
      constraints: false,
      foreignKey: 'drawingFileId',
    });
    models.drawing.belongsTo(models.part, {
      as: 'part',
      constraints: false,
    });

    models.drawing.hasMany(models.drawingSheet, {
      as: 'sheets',
      constraints: false,
      foreignKey: 'drawingId',
    });

    models.drawing.hasMany(models.characteristic, {
      as: 'characteristics',
      constraints: false,
      foreignKey: 'drawingId',
    });

    models.drawing.hasMany(models.file, {
      as: 'file',
      foreignKey: 'belongsToId',
      constraints: false,
      scope: {
        belongsTo: models.drawing.getTableName(),
        belongsToColumn: 'file',
      },
    });

    models.drawing.belongsTo(models.user, {
      as: 'createdBy',
      constraints: false,
    });

    models.drawing.belongsTo(models.user, {
      as: 'updatedBy',
      constraints: false,
    });
  };

  return drawing;
};
