/**
 * Setting database model.
 * See https://sequelize.org/v5/manual/models-definition.html to learn how to customize it.
 */
module.exports = (sequelize, DataTypes) => {
  const setting = sequelize.define(
    'setting',
    {
      id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        primaryKey: true,
      },
      name: {
        type: DataTypes.TEXT,
        allowNull: false,
        validate: {
          notEmpty: true,
        },
      },
      value: {
        type: DataTypes.TEXT,
      },
      metadata: {
        type: DataTypes.TEXT,
      },
    },
    {
      indexes: [
        {
          unique: false,
          fields: ['name'],
        },
      ],
      timestamps: true,
      paranoid: true,
    }
  );

  setting.associate = (models) => {
    models.setting.belongsTo(models.site, {
      as: 'site',
      constraints: false,
    });

    models.setting.belongsTo(models.user, {
      as: 'user',
      constraints: false,
    });

    models.setting.belongsTo(models.user, {
      as: 'createdBy',
      constraints: false,
    });

    models.setting.belongsTo(models.user, {
      as: 'updatedBy',
      constraints: false,
    });
  };

  return setting;
};
