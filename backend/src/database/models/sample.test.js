const SequelizeMock = require('sequelize-mock');
const modelModule = require('./sample');

const { data } = require('../../__fixtures__').sample;

describe('Sample Model', () => {
  let mockSequelize;
  let mockDataTypes;
  let model;

  beforeEach(() => {
    mockSequelize = new SequelizeMock();
    mockDataTypes = mockSequelize.Sequelize;
    model = modelModule(mockSequelize, mockDataTypes);
  });

  describe('#create()', () => {
    it('should create a valid model instance', async () => {
      const fixture = data[0];

      const instance = await model.create(fixture);
      expect(instance.serial).toBe(fixture.serial);
    });
  });

  describe('#save()', () => {
    it('should honor a change to field', async () => {
      const fixture = data[0];
      const instance = await model.create(fixture);

      expect(instance.serial).toBe(fixture.serial);
      const newSerial = 'some-new-serial';

      instance.serial = newSerial;
      await instance.save();

      expect(instance.serial).toBe(newSerial);
    });
  });
});
