/**
 * User database model.
 * See https://sequelize.org/v5/manual/models-definition.html to learn how to customize it.
 */
module.exports = function (sequelize, DataTypes) {
  const user = sequelize.define(
    'user',
    {
      id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        primaryKey: true,
      },
      password: {
        type: DataTypes.STRING(255),
        allowNull: true,
      },
      emailVerified: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: false,
      },
      emailVerificationToken: {
        type: DataTypes.STRING(255),
        allowNull: true,
      },
      emailVerificationTokenExpiresAt: {
        type: DataTypes.DATE,
      },
      passwordResetToken: {
        type: DataTypes.STRING(255),
        allowNull: true,
      },
      passwordResetTokenExpiresAt: {
        type: DataTypes.DATE,
      },
      email: {
        type: DataTypes.STRING(255),
        allowNull: false,
        unique: true,
        validate: {
          isEmail: true,
          notEmpty: true,
        },
      },
      crmContactId: {
        type: DataTypes.TEXT,
        allowNull: true,
      },
      signupMeta: {
        type: DataTypes.TEXT,
        allowNull: true,
      },
      activeAccountMemberId: {
        type: DataTypes.UUID,
        allowNull: true,
      },
      authenticationUid: {
        type: DataTypes.STRING(255),
        allowNull: true,
      },
      disabled: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: false,
      },
      importHash: {
        type: DataTypes.STRING(255),
        allowNull: true,
        unique: true,
      },
    },
    {
      timestamps: true,
      paranoid: true,
    }
  );

  user.associate = (models) => {
    models.user.hasMany(models.file, {
      as: { singular: 'avatar', plural: 'avatars' },
      foreignKey: 'belongsToId',
      constraints: false,
      scope: {
        belongsTo: models.user.getTableName(),
        belongsToColumn: 'avatars',
      },
    });

    models.user.hasMany(models.accountMember, {
      as: 'accountMemberships',
      constraints: false,
      foreignKey: 'userId',
    });

    models.user.hasOne(models.userProfile, {
      foreignKey: 'userId',
      as: 'profile',
      constraints: false,
    });

    models.user.belongsTo(models.user, {
      as: 'createdBy',
    });

    models.user.belongsTo(models.user, {
      as: 'updatedBy',
    });
  };

  return user;
};
