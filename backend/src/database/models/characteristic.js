/**
 * Characteristic database model.
 * See https://sequelize.org/v5/manual/models-definition.html to learn how to customize it.
 */
module.exports = function (sequelize, DataTypes) {
  const characteristic = sequelize.define(
    'characteristic',
    {
      id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        primaryKey: true,
      },
      notationType: {
        type: DataTypes.TEXT,
        allowNull: false,
        defaultValue: 'fa3284ed-ad3e-43c9-b051-87bd2e95e927', // Note
      },
      notationSubtype: {
        type: DataTypes.TEXT,
        allowNull: false,
        defaultValue: '805fd539-8063-4158-8361-509e10a5d371', // Note
      },
      notationClass: {
        type: DataTypes.ENUM,
        allowNull: false,
        values: ['Tolerance', 'Basic', 'Reference', 'N_A'],
      },
      fullSpecification: {
        type: DataTypes.TEXT,
        allowNull: false,
      },
      quantity: {
        type: DataTypes.INTEGER,
      },
      nominal: {
        type: DataTypes.TEXT,
        allowNull: false,
      },
      upperSpecLimit: {
        type: DataTypes.TEXT,
      },
      lowerSpecLimit: {
        type: DataTypes.TEXT,
      },
      plusTol: {
        type: DataTypes.TEXT,
      },
      minusTol: {
        type: DataTypes.TEXT,
      },
      unit: {
        type: DataTypes.TEXT,
      },
      criticality: {
        type: DataTypes.TEXT,
        allowNull: true,
      },
      inspectionMethod: {
        type: DataTypes.TEXT,
      },
      notes: {
        type: DataTypes.TEXT,
      },
      drawingRotation: {
        type: DataTypes.DECIMAL,
        allowNull: false,
        validate: {
          min: -360,
          max: 360,
        },
        get() {
          return parseFloat(this.getDataValue('drawingRotation'));
        },
      },
      drawingScale: {
        type: DataTypes.DECIMAL,
        allowNull: false,
        validate: {},
        get() {
          return parseFloat(this.getDataValue('drawingScale'));
        },
      },
      boxLocationY: {
        type: DataTypes.DECIMAL,
        allowNull: false,
        validate: {},
        get() {
          return parseFloat(this.getDataValue('boxLocationY'));
        },
      },
      boxLocationX: {
        type: DataTypes.DECIMAL,
        allowNull: false,
        validate: {},
        get() {
          return parseFloat(this.getDataValue('boxLocationX'));
        },
      },
      boxWidth: {
        type: DataTypes.DECIMAL,
        allowNull: false,
        validate: {},
        get() {
          return parseFloat(this.getDataValue('boxWidth'));
        },
      },
      boxHeight: {
        type: DataTypes.DECIMAL,
        allowNull: false,
        validate: {},
        get() {
          return parseFloat(this.getDataValue('boxHeight'));
        },
      },
      boxRotation: {
        type: DataTypes.DECIMAL,
        allowNull: false,
        validate: {
          min: -360,
          max: 360,
        },
        get() {
          return parseFloat(this.getDataValue('boxRotation'));
        },
      },
      markerGroup: {
        type: DataTypes.TEXT,
      },
      markerGroupShared: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: false,
      },
      markerIndex: {
        type: DataTypes.INTEGER,
        allowNull: false,
        validate: {},
      },
      markerSubIndex: {
        type: DataTypes.INTEGER,
        validate: {},
      },
      markerLabel: {
        type: DataTypes.TEXT,
        allowNull: false,
        validate: {
          notEmpty: true,
        },
      },
      markerStyle: {
        type: DataTypes.TEXT,
        allowNull: false,
        validate: {
          notEmpty: true,
        },
      },
      markerLocationX: {
        type: DataTypes.DECIMAL,
        allowNull: false,
        validate: {},
        get() {
          return parseFloat(this.getDataValue('markerLocationX'));
        },
      },
      markerLocationY: {
        type: DataTypes.DECIMAL,
        allowNull: false,
        validate: {},
        get() {
          return parseFloat(this.getDataValue('markerLocationY'));
        },
      },
      markerFontSize: {
        type: DataTypes.INTEGER,
        allowNull: false,
        validate: {},
      },
      markerSize: {
        type: DataTypes.INTEGER,
        allowNull: false,
        defaultValue: 36,
        validate: {},
      },
      markerRotation: {
        type: DataTypes.INTEGER,
      },
      gridCoordinates: {
        type: DataTypes.TEXT,
      },
      balloonGridCoordinates: {
        type: DataTypes.TEXT,
      },
      connectionPointGridCoordinates: {
        type: DataTypes.TEXT,
      },
      drawingSheetIndex: {
        type: DataTypes.INTEGER,
        allowNull: false,
        validate: {
          min: 1,
        },
      },
      gdtSymbol: {
        type: DataTypes.TEXT,
      },
      gdtPrimaryToleranceZone: {
        type: DataTypes.TEXT,
      },
      gdtSecondaryToleranceZone: {
        type: DataTypes.TEXT,
      },
      gdtPrimaryDatum: {
        type: DataTypes.TEXT,
      },
      gdtSecondaryDatum: {
        type: DataTypes.TEXT,
      },
      gdtTertiaryDatum: {
        type: DataTypes.TEXT,
      },
      captureMethod: {
        type: DataTypes.ENUM,
        allowNull: false,
        values: ['Automated', 'Manual', 'Custom', 'Annotation'],
      },
      captureError: {
        type: DataTypes.TEXT,
      },
      status: {
        type: DataTypes.ENUM,
        allowNull: false,
        values: ['Inactive', 'Active', 'Rejected'],
      },
      verified: {
        type: DataTypes.BOOLEAN,
      },
      fullSpecificationFromOCR: {
        type: DataTypes.TEXT,
      },
      confidence: {
        type: DataTypes.INTEGER,
        validate: {
          min: -1,
          max: 100,
        },
      },
      connectionPointLocationX: {
        type: DataTypes.DECIMAL,
        allowNull: false,
        defaultValue: 0, // we just need something here. It will be calculated later
        validate: {},
        get() {
          return parseFloat(this.getDataValue('connectionPointLocationX'));
        },
      },
      connectionPointLocationY: {
        type: DataTypes.DECIMAL,
        allowNull: false,
        defaultValue: 0, // we just need something here. It will be calculated later
        validate: {},
        get() {
          return parseFloat(this.getDataValue('connectionPointLocationY'));
        },
      },
      connectionPointIsFloating: {
        type: DataTypes.BOOLEAN,
      },
      displayLeaderLine: {
        type: DataTypes.BOOLEAN,
        defaultValue: true, // default to true so leader line shows up
      },
      leaderLineDistance: {
        type: DataTypes.DECIMAL,
        defaultValue: 0,
        get() {
          return null ? null : parseFloat(this.getDataValue('leaderLineDistance'));
        },
      },
      operation: {
        type: DataTypes.TEXT,
      },
      toleranceSource: {
        type: DataTypes.ENUM,
        allowNull: false,
        values: ['Default_Tolerance', 'Document_Defined', 'Manually_Set'],
      },
      importHash: {
        type: DataTypes.STRING(255),
        allowNull: true,
        unique: true,
      },
    },
    {
      timestamps: true,
      paranoid: true,
    },
  );

  characteristic.associate = (models) => {
    models.characteristic.belongsTo(models.part, {
      as: 'part',
      constraints: false,
    });
    models.characteristic.belongsTo(models.characteristic, {
      as: 'PreviousRevision',
      constraints: false,
      foreignKey: 'previousRevision',
    });
    models.characteristic.belongsTo(models.characteristic, {
      as: 'NextRevision',
      constraints: false,
      foreignKey: 'nextRevision',
    });
    models.characteristic.belongsTo(models.characteristic, {
      as: 'OriginalCharacteristic',
      constraints: false,
      foreignKey: 'originalCharacteristic',
    });

    models.characteristic.belongsTo(models.drawing, {
      as: 'drawing',
      constraints: false,
    });

    models.characteristic.belongsTo(models.drawingSheet, {
      as: 'drawingSheet',
      constraints: false,
    });

    models.characteristic.belongsTo(models.user, {
      as: 'createdBy',
      constraints: false,
    });

    models.characteristic.belongsTo(models.user, {
      as: 'updatedBy',
      constraints: false,
    });

    models.characteristic.belongsTo(models.reviewTask, {
      as: 'reviewTask',
      constraints: false,
    });

    models.characteristic.hasMany(models.measurement, {
      as: 'measurements',
      constraints: false,
    });
  };

  return characteristic;
};
