/**
 * Review Task database model.
 * See https://sequelize.org/v5/manual/models-definition.html to learn how to customize it.
 */
module.exports = (sequelize, DataTypes) => {
  const reviewTask = sequelize.define(
    'reviewTask',
    {
      id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        primaryKey: true,
      },
      type: {
        type: DataTypes.ENUM,
        values: ['Capture', 'Grid'],
      },
      status: {
        type: DataTypes.ENUM,
        allowNull: false,
        values: ['Waiting', 'Processing', 'Accepted', 'Corrected', 'Rejected'],
      },
      metadata: {
        type: DataTypes.TEXT,
      },
      processingStart: {
        type: DataTypes.DATE,
      },
      processingEnd: {
        type: DataTypes.DATE,
      },
    },
    {
      timestamps: true,
      paranoid: true,
    }
  );

  reviewTask.associate = (models) => {
    models.reviewTask.belongsTo(models.account, {
      as: 'account',
      constraints: false,
    });

    models.reviewTask.belongsTo(models.site, {
      as: 'site',
      constraints: false,
    });

    models.reviewTask.belongsTo(models.part, {
      as: 'part',
      constraints: false,
    });

    models.reviewTask.belongsTo(models.drawing, {
      as: 'drawing',
      constraints: false,
    });

    models.reviewTask.belongsTo(models.user, {
      as: 'reviewer',
      constraints: false,
    });
  };

  return reviewTask;
};
