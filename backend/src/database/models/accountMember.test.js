const modelModule = require('./accountMember');
const SequelizeMock = require('sequelize-mock');
const constants = require('../constants').accountMember;

const { data } = require('../../__fixtures__').accountMember;

describe('AccountMember Model', () => {
  let mockSequelize;
  let mockDataTypes;
  let model;

  beforeEach(() => {
    mockSequelize = new SequelizeMock();
    mockDataTypes = mockSequelize.Sequelize;
    model = modelModule(mockSequelize, mockDataTypes);
  });

  describe('#create()', () => {
    it('should create a valid model instance', async () => {
      const fixture = data.accountAUserAPaidRoles;
      const instance = await model.create(fixture);

      expect(instance.id).toBe(fixture.id);
    });

    it('should provide a false `paid` value when the user does not have a paid role', async () => {
      const fixture = data.accountAUserBUnpaidRoles;
      const instance = await model.create(fixture);

      // The `paid.get()` syntax is an artifact of sequelize-mock not supporting virtual data types.
      // Outside of these tests, the value is present at `instance.paid`.
      const paid = instance.paid.get(instance.dataValues.roles);

      expect(paid).toBe(false);
    });

    it('should provide a true `paid` value when the user has only a paid roles', async () => {
      const fixture = data.accountAUserAPaidRoles;
      const instance = await model.create(fixture);

      // The `paid.get()` syntax is an artifact of sequelize-mock not supporting virtual data types.
      // Outside of these tests, the value is present at `instance.paid`.
      const paid = instance.paid.get(instance.dataValues.roles);
      expect(paid).toBe(true);
    });

    it('should provide a true `paid` value when the user has one or more paid roles and unpaid roles', async () => {
      const fixture = data.accountBUserCPaidAndUnpaidRoles;
      const instance = await model.create(fixture);

      // The `paid.get()` syntax is an artifact of sequelize-mock not supporting virtual data types.
      // Outside of these tests, the value is present at `instance.paid`.
      const paid = instance.paid.get(instance.dataValues.roles);
      expect(paid).toBe(true);
    });

    it('should provide a true `paid` value when the user has an empty roles value', async () => {
      const fixture = data.emptyRoles;
      const instance = await model.create(fixture);

      // The `paid.get()` syntax is an artifact of sequelize-mock not supporting virtual data types.
      // Outside of these tests, the value is present at `instance.paid`.
      const paid = instance.paid.get(instance.dataValues.roles);
      expect(paid).toBe(true);
    });
  });

  describe('#save()', () => {
    it('should honor a chnage to field', async () => {
      const fixture = data.accountAUserAPaidRoles;
      const instance = await model.create(fixture);

      expect(instance.settings).toBe(fixture.settings);
      const newSettings = 'some-new-settings';

      instance.settings = newSettings;
      await instance.save();

      expect(instance.settings).toBe(newSettings);
    });

    it('should provide a true `paid` value when the user now has only paid roles', async () => {
      const fixture = data.accountAUserBUnpaidRoles;
      const instance = await model.create(fixture);

      // The `paid.get()` syntax is an artifact of sequelize-mock not supporting virtual data types.
      // Outside of these tests, the value is present at `instance.paid`.
      let paid = instance.paid.get(instance.dataValues.roles);

      expect(paid).toBe(false);

      instance.roles = [constants.PAID_ROLES[0]];
      await instance.save();

      paid = instance.paid.get(instance.dataValues.roles);
      expect(paid).toBe(true);
    });

    it('should provide a true `paid` value when the user now has one or more paid roles and unpaid roles', async () => {
      const fixture = data.accountAUserBUnpaidRoles;
      const instance = await model.create(fixture);

      // The `paid.get()` syntax is an artifact of sequelize-mock not supporting virtual data types.
      // Outside of these tests, the value is present at `instance.paid`.
      let paid = instance.paid.get(instance.dataValues.roles);

      expect(paid).toBe(false);

      instance.roles = [constants.UNPAID_ROLES[0], constants.PAID_ROLES[0]];
      await instance.save();

      paid = instance.paid.get(instance.dataValues.roles);
      expect(paid).toBe(true);
    });

    it('should provide a false `paid` value when the user no longer has a paid role', async () => {
      const fixture = data.accountAUserAPaidRoles;
      const instance = await model.create(fixture);

      // The `paid.get()` syntax is an artifact of sequelize-mock not supporting virtual data types.
      // Outside of these tests, the value is present at `instance.paid`.
      let paid = instance.paid.get(instance.dataValues.roles);

      expect(paid).toBe(true);

      instance.roles = [constants.UNPAID_ROLES[0]];
      await instance.save();

      paid = instance.paid.get(instance.dataValues.roles);
      expect(paid).toBe(false);
    });
  });
});
