const SequelizeMock = require('sequelize-mock');
const modelModule = require('./notificationUserStatus');

const { data } = require('../../__fixtures__').notificationUserStatus;

describe('NotificationUserStatus Model', () => {
  let mockSequelize;
  let mockDataTypes;
  let model;

  beforeEach(() => {
    mockSequelize = new SequelizeMock();
    mockDataTypes = mockSequelize.Sequelize;
    model = modelModule(mockSequelize, mockDataTypes);
  });

  describe('#create()', () => {
    it('should create a valid model instance', async () => {
      const fixture = data[0];
      const instance = await model.create(fixture);

      expect(instance.id).toBe(fixture.id);
    });
  });

  describe('#save()', () => {
    it('should honor a change to field', async () => {
      const fixture = data[0];
      const instance = await model.create(fixture);

      expect(instance.readAt).toBe(fixture.readAt);
      const newReadAt = new Date();

      instance.readAt = newReadAt;
      await instance.save();

      expect(instance.readAt).toBe(newReadAt);
    });
  });
});
