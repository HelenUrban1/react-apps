/**
 * Notification User Status database model.
 * See https://sequelize.org/v5/manual/models-definition.html to learn how to customize it.
 */
module.exports = (sequelize, DataTypes) => {
  const notificationUserStatus = sequelize.define(
    'notificationUserStatus',
    {
      id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        primaryKey: true,
      },
      readAt: {
        type: DataTypes.DATE,
      },
      archivedAt: {
        type: DataTypes.DATE,
      },
    },
    {
      timestamps: true,
      paranoid: true,
    }
  );

  notificationUserStatus.associate = (models) => {
    models.notificationUserStatus.belongsTo(models.notification, {
      as: 'notification',
      constraints: true,
    });
    models.notificationUserStatus.belongsTo(models.user, {
      as: 'user',
      constraints: false,
    });
  };

  return notificationUserStatus;
};
