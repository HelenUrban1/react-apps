const moment = require('moment');

/**
 * Report database model.
 * See https://sequelize.org/v5/manual/models-definition.html to learn how to customize it.
 */
module.exports = function (sequelize, DataTypes) {
  const report = sequelize.define(
    'report',
    {
      id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        primaryKey: true,
      },
      partVersion: {
        type: DataTypes.TEXT,
        allowNull: false,
        validate: {
          notEmpty: true,
        },
      },
      data: {
        type: DataTypes.TEXT,
      },
      title: {
        type: DataTypes.TEXT,
        allowNull: false,
        validate: {
          notEmpty: true,
        },
      },
      filters: {
        type: DataTypes.TEXT,
        allowNull: true,
        defaultValue: '{}',
      },
      templateUrl: {
        type: DataTypes.STRING(2083),
        allowNull: false,
        validate: {
          notEmpty: true,
        },
      },
      partUpdatedAt: {
        type: DataTypes.TEXT,
        allowNull: true,
      },
      status: {
        type: DataTypes.ENUM,
        allowNull: false,
        values: ['Active', 'Inactive', 'Deleted'],
      },
      importHash: {
        type: DataTypes.STRING(255),
        allowNull: true,
        unique: true,
      },
    },
    {
      timestamps: true,
      paranoid: true,
    }
  );

  report.associate = (models) => {
    models.report.belongsTo(models.site, {
      as: 'site',
      constraints: false,
    });

    models.report.belongsTo(models.organization, {
      as: 'customer',
      constraints: false,
    });

    models.report.belongsTo(models.part, {
      as: 'part',
      constraints: false,
    });

    models.report.hasOne(models.reportTemplate, {
      as: 'template',
      constraints: false,
    });

    models.report.hasMany(models.file, {
      as: 'file',
      foreignKey: 'belongsToId',
      constraints: false,
      scope: {
        belongsTo: models.report.getTableName(),
        belongsToColumn: 'file',
      },
    });

    models.report.belongsTo(models.user, {
      as: 'createdBy',
      constraints: false,
    });

    models.report.belongsTo(models.user, {
      as: 'updatedBy',
      constraints: false,
    });
  };

  return report;
};
