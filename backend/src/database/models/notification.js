/**
 * Notification database model.
 * See https://sequelize.org/v5/manual/models-definition.html to learn how to customize it.
 */
module.exports = (sequelize, DataTypes) => {
  const notification = sequelize.define(
    'notification',
    {
      id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        primaryKey: true,
      },
      title: {
        type: DataTypes.STRING(255),
      },
      content: {
        type: DataTypes.TEXT,
        allowNull: true,
      },
      action: {
        type: DataTypes.STRING(255),
        allowNull: true,
      },
      actionAt: {
        type: DataTypes.DATE,
        allowNull: true,
      },
      priority: {
        type: DataTypes.INTEGER,
        defaultValue: 0,
      },
      relationId: {
        type: DataTypes.UUID,
      },
      relationType: {
        type: DataTypes.STRING(30),
      },
    },
    {
      timestamps: true,
      paranoid: true,
    }
  );

  notification.associate = (models) => {
    models.notification.hasMany(models.notificationUserStatus, {
      as: 'userStatuses',
      constraints: true,
      foreignKey: 'notificationId',
    });
    models.notification.belongsTo(models.user, {
      as: 'actor',
      constraints: false,
    });
    models.notification.belongsTo(models.site, {
      as: 'site',
      constraints: false,
    });
    models.notification.belongsTo(models.user, {
      as: 'createdBy',
      constraints: false,
    });
    models.notification.belongsTo(models.user, {
      as: 'updatedBy',
      constraints: false,
    });
  };

  return notification;
};
