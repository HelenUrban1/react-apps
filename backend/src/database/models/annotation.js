const moment = require('moment');

/**
 * Annotation database model.
 * See https://sequelize.org/v5/manual/models-definition.html to learn how to customize it.
 */
module.exports = function (sequelize, DataTypes) {
  const annotation = sequelize.define(
    'annotation',
    {
      id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        primaryKey: true,
      },
      annotation: {
        type: DataTypes.TEXT,
        allowNull: false,
        defaultValue: '',
        validate: {
          notEmpty: true,
        },
      },
      fontSize: {
        type: DataTypes.INTEGER,
        allowNull: false,
        defaultValue: 24,
        validate: {
          min: 4,
          max: 100,
        },
      },
      fontColor: {
        type: DataTypes.TEXT,
        allowNull: false,
        defaultValue: '#000000',
      },
      borderColor: {
        type: DataTypes.TEXT,
        allowNull: false,
        defaultValue: '#E5B900',
      },
      backgroundColor: {
        type: DataTypes.TEXT,
        allowNull: false,
        defaultValue: '#FFFAE5',
      },
      drawingRotation: {
        type: DataTypes.DECIMAL,
        allowNull: false,
        defaultValue: 0,
        validate: {
          min: -360,
          max: 360,
        },
      },
      drawingScale: {
        type: DataTypes.DECIMAL,
        allowNull: false,
        defaultValue: 1.0,
      },
      boxLocationY: {
        type: DataTypes.DECIMAL,
        defaultValue: 0,
        allowNull: false,
      },
      boxLocationX: {
        type: DataTypes.DECIMAL,
        defaultValue: 0,
        allowNull: false,
      },
      boxWidth: {
        type: DataTypes.DECIMAL,
        defaultValue: 0,
        allowNull: false,
      },
      boxHeight: {
        type: DataTypes.DECIMAL,
        defaultValue: 0,
        allowNull: false,
      },
      boxRotation: {
        type: DataTypes.DECIMAL,
        defaultValue: 0,
        allowNull: false,
        validate: {
          min: -360,
          max: 360,
        },
      },
      drawingSheetIndex: {
        type: DataTypes.INTEGER,
        allowNull: false,
        validate: {
          min: 1,
        },
        defaultValue: 1,
      },
      importHash: {
        type: DataTypes.STRING(255),
        allowNull: true,
        unique: true,
      },
    },
    {
      timestamps: true,
      paranoid: true,
    }
  );

  annotation.associate = (models) => {
    models.annotation.belongsTo(models.part, {
      as: 'part',
      constraints: false,
    });
    models.annotation.belongsTo(models.characteristic, {
      as: 'characteristic',
      constraints: false,
    });
    models.annotation.belongsTo(models.annotation, {
      as: 'PreviousRevision',
      constraints: false,
      foreignKey: 'previousRevision',
    });
    models.annotation.belongsTo(models.annotation, {
      as: 'NextRevision',
      constraints: false,
      foreignKey: 'nextRevision',
    });
    models.annotation.belongsTo(models.annotation, {
      as: 'OriginalAnnotation',
      constraints: false,
      foreignKey: 'originalAnnotation',
    });
    models.annotation.belongsTo(models.drawing, {
      as: 'drawing',
      constraints: false,
    });
    models.annotation.belongsTo(models.drawingSheet, {
      as: 'drawingSheet',
      constraints: false,
    });
    models.annotation.belongsTo(models.user, {
      as: 'createdBy',
      constraints: false,
    });
    models.annotation.belongsTo(models.user, {
      as: 'updatedBy',
      constraints: false,
    });
  };

  return annotation;
};
