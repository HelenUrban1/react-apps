const moment = require('moment');

/**
 * Marker database model.
 * See https://sequelize.org/v5/manual/models-definition.html to learn how to customize it.
 */
module.exports = function (sequelize, DataTypes) {
  const marker = sequelize.define(
    'marker',
    {
      id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        primaryKey: true,
      },
      markerStyleName: {
        type: DataTypes.TEXT,
        allowNull: false,
        validate: {
          notEmpty: true,
        },
      },
      shape: {
        type: DataTypes.ENUM,
        allowNull: false,
        values: ['Circle', 'Pin', 'Triangle', 'Square', 'Diamond', 'Pentagon', 'Hexagon', 'Heptagon', 'Octagon', 'Star'],
      },
      defaultPosition: {
        type: DataTypes.TEXT,
        allowNull: false,
        validate: {
          notEmpty: true,
        },
      },
      hasLeaderLine: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: false,
      },
      fontSize: {
        type: DataTypes.INTEGER,
        allowNull: false,
        validate: {
          min: 4,
        },
      },
      fontColor: {
        type: DataTypes.TEXT,
        allowNull: false,
        validate: {
          notEmpty: true,
        },
      },
      fillColor: {
        type: DataTypes.TEXT,
        allowNull: false,
        validate: {
          notEmpty: true,
        },
      },
      borderColor: {
        type: DataTypes.TEXT,
        allowNull: false,
        validate: {
          notEmpty: true,
        },
      },
      borderWidth: {
        type: DataTypes.INTEGER,
        allowNull: false,
        validate: {},
      },
      borderStyle: {
        type: DataTypes.ENUM,
        allowNull: true,
        values: ['Solid', 'Dashed', 'Dotted'],
        defaultValue: 'Solid',
      },
      leaderLineColor: {
        type: DataTypes.TEXT,
        allowNull: false,
        validate: {
          notEmpty: true,
        },
      },
      leaderLineWidth: {
        type: DataTypes.INTEGER,
        allowNull: false,
        validate: {
          min: 1,
        },
      },
      leaderLineStyle: {
        type: DataTypes.ENUM,
        allowNull: true,
        values: ['Solid', 'Dashed', 'Dotted'],
        defaultValue: 'Solid',
      },
      importHash: {
        type: DataTypes.STRING(255),
        allowNull: true,
        unique: true,
      },
    },
    {
      timestamps: true,
      paranoid: true,
    }
  );

  marker.associate = (models) => {
    models.marker.belongsTo(models.user, {
      as: 'createdBy',
      constraints: false,
    });

    models.marker.belongsTo(models.user, {
      as: 'updatedBy',
      constraints: false,
    });
  };

  return marker;
};
