const moment = require('moment');

/**
 * ReportTemplate database model.
 * See https://sequelize.org/v5/manual/models-definition.html to learn how to customize it.
 */
module.exports = function (sequelize, DataTypes) {
  const reportTemplate = sequelize.define(
    'reportTemplate',
    {
      id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        primaryKey: true,
      },
      title: {
        type: DataTypes.TEXT,
        allowNull: false,
        validate: {
          notEmpty: true,
        },
      },
      status: {
        type: DataTypes.ENUM,
        allowNull: false,
        values: ['Active', 'Inactive', 'Deleted'],
      },
      type: {
        type: DataTypes.TEXT,
      },
      sortIndex: {
        type: DataTypes.INTEGER,
      },
      direction: {
        type: DataTypes.ENUM,
        allowNull: false,
        values: ['Vertical', 'Horizontal'],
        defaultValue: 'Vertical',
      },
      settings: {
        type: DataTypes.TEXT,
      },
      filters: {
        type: DataTypes.TEXT,
      },
      importHash: {
        type: DataTypes.STRING(255),
        allowNull: true,
        unique: true,
      },
    },
    {
      timestamps: true,
      paranoid: true,
    }
  );

  reportTemplate.associate = (models) => {
    models.reportTemplate.belongsTo(models.site, {
      as: 'site',
      constraints: false,
    });
    models.reportTemplate.belongsTo(models.organization, {
      as: 'provider',
      constraints: false,
    });
    models.reportTemplate.belongsTo(models.report, {
      as: 'template',
      constraints: false,
    });

    models.reportTemplate.hasMany(models.file, {
      as: 'file',
      foreignKey: 'belongsToId',
      constraints: false,
      scope: {
        belongsTo: models.reportTemplate.getTableName(),
        belongsToColumn: 'file',
      },
    });

    models.reportTemplate.belongsTo(models.user, {
      as: 'createdBy',
      constraints: false,
    });

    models.reportTemplate.belongsTo(models.user, {
      as: 'updatedBy',
      constraints: false,
    });
  };

  return reportTemplate;
};
