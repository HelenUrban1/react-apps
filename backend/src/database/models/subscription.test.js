const SequelizeMock = require('sequelize-mock');
const moment = require('moment');
const modelModule = require('./subscription');

const fixtures = require('../../__fixtures__').subscription.data;

describe('Subscription Model', () => {
  let mockSequelize;
  let mockDataTypes;

  beforeEach(() => {
    mockSequelize = new SequelizeMock();
    mockDataTypes = mockSequelize.Sequelize;
  });

  const assertUnderscoresPresence = (values, shouldExist) => {
    const input = Array.isArray(values) ? values.join() : values;
    expect(input.includes('_')).toBe(shouldExist);
  };

  describe('FIELD_NAMES property', () => {
    it('should populate the `FIELD_NAMES` property', () => {
      const model = modelModule(mockSequelize, mockDataTypes);
      expect(Array.isArray(model.FIELD_NAMES)).toBe(true);
      expect(model.FIELD_NAMES.length).toBeGreaterThan(0);
      assertUnderscoresPresence(model.FIELD_NAMES, false);
    });

    it('should prevent extending the `FIELD_NAMES` property', () => {
      const model = modelModule(mockSequelize, mockDataTypes);
      expect(() => {
        model.FIELD_NAMES.push('this should not work');
      }).toThrow();
    });

    it('should prevent truncating the `FIELD_NAMES` property', () => {
      const model = modelModule(mockSequelize, mockDataTypes);
      expect(() => {
        model.FIELD_NAMES.pop();
      }).toThrow();
    });
  });

  describe('#translateChargifyFieldsToIxc()', () => {
    it('should not error with empty input', () => {
      const model = modelModule(mockSequelize, mockDataTypes);
      const data = undefined;
      model.translateChargifyFieldsToIxc(data);
      expect(data).toBeUndefined();
    });

    it('should not error with an unpopulated input object', () => {
      const model = modelModule(mockSequelize, mockDataTypes);
      const data = {};
      const unmodifiedFixtureData = { ...data };
      model.translateChargifyFieldsToIxc(data);
      expect(data).toEqual(unmodifiedFixtureData);
    });

    it('should convert Chargify snake case field names to camel case', async () => {
      modelModule(mockSequelize, mockDataTypes);
      const model = mockSequelize.models.subscription;
      const data = fixtures[0];
      const unmodifiedFixtureData = JSON.parse(JSON.stringify(data));
      expect(data).toEqual(unmodifiedFixtureData);
      assertUnderscoresPresence(Object.keys(data), true);

      const changedData = model.translateChargifyFieldsToIxc(data);
      assertUnderscoresPresence(Object.keys(changedData), false);
      expect(changedData).not.toEqual(unmodifiedFixtureData);

      const instance = await model.create(changedData);
      expect(instance.get('signupRevenue')).toBe(unmodifiedFixtureData.signup_revenue);
    });
  });

  describe('#translateIxcFieldsToChargify()', () => {
    it('should not error with empty input', () => {
      const model = modelModule(mockSequelize, mockDataTypes);
      const data = undefined;
      model.translateIxcFieldsToChargify(data);
      expect(data).toBeUndefined();
    });

    it('should not error with an unpopulated input object', () => {
      const model = modelModule(mockSequelize, mockDataTypes);
      const data = {};
      const unmodifiedFixtureData = { ...data };
      model.translateIxcFieldsToChargify(data);
      expect(data).toEqual(unmodifiedFixtureData);
    });

    it('should convert Chargify snake case field names to camel case', async () => {
      modelModule(mockSequelize, mockDataTypes);
      const model = mockSequelize.models.subscription;
      const data = model.translateChargifyFieldsToIxc(fixtures[0]);
      assertUnderscoresPresence(Object.keys(data), false);
      expect(data).not.toEqual(fixtures[0]);
      // Check that it calculates annual renewal date
      expect(data.renewalDate).toEqual(moment(fixtures[0].activated_at).add(1, 'years').format());

      const changedData = model.translateIxcFieldsToChargify(data);
      assertUnderscoresPresence(Object.keys(changedData), true);
      expect(changedData).not.toEqual(data);

      const instance = await model.create(changedData);
      expect(instance.get('signup_revenue')).toBe(data.signupRevenue);
    });
  });

  describe('#prepareProviderId()', () => {
    it('should not error with empty input', () => {
      const model = modelModule(mockSequelize, mockDataTypes);
      const data = undefined;
      model.prepareProviderId(data);
      expect(data).toBeUndefined();
    });

    it('should not error with an unpopulated input object', () => {
      const model = modelModule(mockSequelize, mockDataTypes);
      const data = {};
      const unmodifiedFixtureData = { ...data };
      model.prepareProviderId(data);
      expect(data).toEqual(unmodifiedFixtureData);
    });

    it('should set the `providerProductId` value', async () => {
      modelModule(mockSequelize, mockDataTypes);
      const model = mockSequelize.models.subscription;
      const data = fixtures[0];
      const unmodifiedFixtureData = JSON.parse(JSON.stringify(data));
      expect(data).toEqual(unmodifiedFixtureData);
      expect(data.providerProductId).toBeUndefined();
      expect(data.providerProductHandle).toBeUndefined();
      const changedData = model.prepareProviderId(data);

      const instance = await model.create(changedData);
      expect(instance.providerProductId).toBeDefined();
      expect(instance.providerProductId).toBe(data.product.id);
      expect(instance.providerProductHandle).toBeDefined();
      expect(instance.providerProductHandle).toBe(data.product.handle);
    });
  });
});
