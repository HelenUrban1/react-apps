const SequelizeMock = require('sequelize-mock');
const modelModule = require('./setting');

const { data } = require('../../__fixtures__').setting;

describe('Setting Model', () => {
  let mockSequelize;
  let mockDataTypes;
  let model;

  beforeEach(() => {
    mockSequelize = new SequelizeMock();
    mockDataTypes = mockSequelize.Sequelize;
    model = modelModule(mockSequelize, mockDataTypes);
  });

  describe('#create()', () => {
    it('should create a valid model instance', async () => {
      const fixture = data[0];
      const instance = await model.create(fixture);

      expect(instance.id).toBe(fixture.id);
    });
  });

  describe('#save()', () => {
    it('should honor a change to field', async () => {
      const fixture = data[0];
      const instance = await model.create(fixture);

      expect(instance.value).toBe(fixture.value);
      const newValue = 'new-value';

      instance.value = newValue;
      await instance.save();

      expect(instance.value).toBe(newValue);
    });
  });
});
