const moment = require('moment');

/**
 * Subscription database model.
 *
 * This model handles Chargify third-party data, which is formatted using
 * snake_case field names. It allows conversion of field names to camelCase
 * so that the DB and the IXC app can refer to the fields in the same manner
 * as the fields of all other models.
 *
 * See https://sequelize.org/v5/manual/models-definition.html to learn how to
 * customize the model.
 */
module.exports = function (sequelize, DataTypes) {
  // TODO: This translation of field names should happen inside of billing adapter
  // because each billing vendor will have different fields and may have a different
  // naming convention.

  // Mapping of camelCase names used in code and as DB column names
  // to their snake_case equivalents from the third-party Chargify data.
  const ixcToChargifyFieldsMapping = {
    activatedAt: 'activated_at',
    automaticallyResumeAt: 'automatically_resume_at',
    balanceInCents: 'balance_in_cents',
    billingPeriod: 'billing_interval',
    cancelAtEndOfPeriod: 'cancel_at_end_of_period',
    canceledAt: 'canceled_at',
    cancellationMessage: 'cancellation_message',
    cancellationMethod: 'cancellation_method',
    couponCode: 'coupon_code',
    couponCodes: 'coupon_codes',
    couponUseCount: 'coupon_use_count',
    couponUsesAllowed: 'coupon_uses_allowed',
    createdAt: 'created_at',
    currentBillingAmountInCents: 'current_billing_amount_in_cents',
    currentPeriodEndsAt: 'current_period_ends_at',
    currentPeriodStartedAt: 'current_period_started_at',
    delayedCancelAt: 'delayed_cancel_at',
    expiresAt: 'expires_at',
    netTerms: 'net_terms',
    nextAssessmentAt: 'next_assessment_at',
    nextProductHandle: 'next_product_handle',
    nextProductId: 'next_product_id',
    nextProductPricePointId: 'next_product_price_point_id',
    offerId: 'offer_id',
    payerId: 'payer_id',
    paymentCollectionMethod: 'payment_collection_method',
    paymentSystemId: 'id',
    paymentType: 'payment_type',
    previousStatus: 'previous_state',
    productPriceInCents: 'product_price_in_cents',
    productPricePointId: 'product_price_point_id',
    productVersionNumber: 'product_version_number',
    reasonCode: 'reason_code',
    receivesInvoiceEmails: 'receives_invoice_emails',
    referralCode: 'referral_code',
    signupPaymentId: 'signup_payment_id',
    signupRevenue: 'signup_revenue',
    snapDay: 'snap_day',
    status: 'state',
    storedCredentialTransactionId: 'stored_credential_transaction_id',
    totalRevenueInCents: 'total_revenue_in_cents',
    termsAcceptedOn: 'terms_accepted_on',
    trialEndedAt: 'trial_ended_at',
    trialStartedAt: 'trial_started_at',
    updatedAt: 'updated_at',
    creditCard: 'credit_card',
    cardId: 'card_id',
    maskedCardNumber: 'masked_card_number',
    cardType: 'card_type',
    expirationMonth: 'expiration_month',
    expirationYear: 'expiration_year',
    customerId: 'payer_id',
    dealId: 'reference',
  };

  const chargifyToIxcFieldsMapping = {};
  Object.keys(ixcToChargifyFieldsMapping).forEach((ixcField) => {
    const chargifyField = ixcToChargifyFieldsMapping[ixcField];
    chargifyToIxcFieldsMapping[chargifyField] = ixcField;
  });

  // All of the field names. Exposed for SubscriptionRepository.
  const FIELD_NAMES = Object.freeze([
    //
    ...Object.keys(ixcToChargifyFieldsMapping),
    'accountId',
    'billingId',
    'companyId',
    'billingPeriod',
    'status',
    'renewalDate',
    'pendingTier',
    'pendingBillingPeriod',
    'pendingBillingAmountInCents',
    'enforcePlan',
    'id',
    'importHash',
    'providerProductId',
    'providerProductHandle',
    'providerFamilyHandle',
    'paidUserSeatsUsed',
    'partsUsed',
    'drawingsUsed',
    'measurementsUsed',
    'components',
    'pendingComponents',
    'termsAcceptedOn',
  ]);

  const subscription = sequelize.define(
    'subscription',
    {
      id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        primaryKey: true,
      },
      accountId: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        allowNull: false,
      },
      billingId: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        allowNull: false,
      },
      companyId: {
        type: DataTypes.TEXT,
        defaultValue: '0',
        allowNull: false,
      },
      paidUserSeatsUsed: {
        type: DataTypes.INTEGER,
      },
      partsUsed: {
        type: DataTypes.INTEGER,
      },
      drawingsUsed: {
        type: DataTypes.INTEGER,
      },
      measurementsUsed: {
        type: DataTypes.INTEGER,
      },
      components: {
        type: DataTypes.TEXT,
      },
      pendingComponents: {
        type: DataTypes.TEXT,
      },
      enforcePlan: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: true,
      },
      providerProductId: {
        // Allows mapping of subscription records with entries in the products.json file stored in this repo
        type: DataTypes.INTEGER,
        allowNull: false,
      },
      providerProductHandle: {
        // Allows mapping of subscription records with entries in the products.json file stored in this repo
        type: DataTypes.TEXT,
        allowNull: false,
        defaultValue: 'ix_standard',
      },
      providerFamilyHandle: {
        // Allows mapping of subscription records with entries in the products.json file stored in this repo
        type: DataTypes.TEXT,
        allowNull: false,
        defaultValue: 'inspectionxpert-cloud',
      },
      billingPeriod: {
        type: DataTypes.ENUM,
        values: ['Pay_as_you_go', 'Monthly', 'Annually', 'Multi_Year'],
      },
      renewalDate: {
        type: DataTypes.DATEONLY,
        allowNull: false,
        get() {
          return this.getDataValue('renewalDate') ? moment.utc(this.getDataValue('renewalDate')).format('YYYY-MM-DD') : null;
        },
      },
      pendingTier: {
        type: DataTypes.INTEGER,
        allowNull: true,
      },
      pendingBillingPeriod: {
        type: DataTypes.ENUM,
        values: ['Pay_as_you_go', 'Monthly', 'Annually', 'Multi_Year'],
        allowNull: true,
      },
      importHash: {
        type: DataTypes.STRING(255),
        allowNull: true,
        unique: true,
      },
      pendingBillingAmountInCents: {
        type: DataTypes.INTEGER,
        allowNull: true,
      },

      // Start of fields that map to Chargify subscription object fields. Most of the names
      // map to their snake_case equivalents, but some differ to conform to other IXC models.
      // See `ixcToChargifyFieldsMapping`.
      //
      // See https://reference.chargify.com/v1/subscriptions/subscription-output-attributes
      activatedAt: {
        type: DataTypes.DATEONLY,
        allowNull: true,
        get() {
          return this.getDataValue('activatedAt') ? moment.utc(this.getDataValue('activatedAt')).format('YYYY-MM-DD') : null;
        },
      },
      automaticallyResumeAt: {
        type: DataTypes.DATEONLY,
        allowNull: true,
        get() {
          return this.getDataValue('automaticallyResumeAt') ? moment.utc(this.getDataValue('automaticallyResumeAt')).format('YYYY-MM-DD') : null;
        },
      },
      balanceInCents: {
        type: DataTypes.INTEGER,
        allowNull: false,
        validate: {},
      },
      cancelAtEndOfPeriod: {
        type: DataTypes.BOOLEAN,
        allowNull: true,
      },
      canceledAt: {
        type: DataTypes.DATEONLY,
        allowNull: true,
        get() {
          return this.getDataValue('canceledAt') ? moment.utc(this.getDataValue('canceledAt')).format('YYYY-MM-DD') : null;
        },
      },
      cancellationMessage: {
        type: DataTypes.TEXT,
        allowNull: true,
      },
      cancellationMethod: {
        type: DataTypes.TEXT,
        allowNull: true,
      },
      couponCode: {
        type: DataTypes.TEXT,
        allowNull: true,
      },
      couponCodes: {
        type: DataTypes.ARRAY(DataTypes.TEXT),
        allowNull: true,
      },
      couponUseCount: {
        type: DataTypes.INTEGER,
        allowNull: true,
      },
      couponUsesAllowed: {
        type: DataTypes.INTEGER,
        allowNull: true,
      },
      createdAt: {
        type: DataTypes.DATE,
        allowNull: true,
      },
      currentBillingAmountInCents: {
        type: DataTypes.INTEGER,
        allowNull: true,
      },
      currentPeriodEndsAt: {
        type: DataTypes.DATEONLY,
        allowNull: true,
        get() {
          return this.getDataValue('currentPeriodEndsAt') ? moment.utc(this.getDataValue('currentPeriodEndsAt')).format('YYYY-MM-DD') : null;
        },
      },
      currentPeriodStartedAt: {
        type: DataTypes.DATEONLY,
        allowNull: true,
        get() {
          return this.getDataValue('currentPeriodStartedAt') ? moment.utc(this.getDataValue('currentPeriodStartedAt')).format('YYYY-MM-DD') : null;
        },
      },
      delayedCancelAt: {
        type: DataTypes.DATEONLY,
        allowNull: true,
        get() {
          return this.getDataValue('delayedCancelAt') ? moment.utc(this.getDataValue('delayedCancelAt')).format('YYYY-MM-DD') : null;
        },
      },
      expiresAt: {
        type: DataTypes.DATEONLY,
        allowNull: true,
        get() {
          return this.getDataValue('expiresAt') ? moment.utc(this.getDataValue('expiresAt')).format('YYYY-MM-DD') : null;
        },
      },
      netTerms: {
        type: DataTypes.INTEGER,
        allowNull: true,
      },
      nextAssessmentAt: {
        type: DataTypes.DATEONLY,
        allowNull: true,
        get() {
          return this.getDataValue('nextAssessmentAt') ? moment.utc(this.getDataValue('nextAssessmentAt')).format('YYYY-MM-DD') : null;
        },
      },
      nextProductHandle: {
        type: DataTypes.TEXT,
        allowNull: true,
      },
      nextProductId: {
        type: DataTypes.TEXT,
        allowNull: true,
      },
      nextProductPricePointId: {
        type: DataTypes.TEXT,
        allowNull: true,
      },
      offerId: {
        type: DataTypes.TEXT,
        allowNull: true,
      },
      payerId: {
        type: DataTypes.INTEGER,
        allowNull: true,
      },
      paymentCollectionMethod: {
        type: DataTypes.TEXT,
        allowNull: true,
      },
      paymentSystemId: {
        // The Chargify subscription `id` value
        type: DataTypes.INTEGER,
        allowNull: false,
      },
      paymentType: {
        type: DataTypes.TEXT,
        allowNull: true,
      },
      previousStatus: {
        type: DataTypes.TEXT,
        allowNull: true,
      },
      productPriceInCents: {
        type: DataTypes.INTEGER,
        allowNull: true,
      },
      productPricePointId: {
        type: DataTypes.TEXT,
        allowNull: true,
      },
      productVersionNumber: {
        type: DataTypes.TEXT,
        allowNull: true,
      },
      reasonCode: {
        type: DataTypes.TEXT,
        allowNull: true,
      },
      receivesInvoiceEmails: {
        type: DataTypes.BOOLEAN,
        allowNull: true,
      },
      referralCode: {
        type: DataTypes.TEXT,
        allowNull: true,
      },
      signupPaymentId: {
        type: DataTypes.INTEGER,
        allowNull: true,
      },
      signupRevenue: {
        type: DataTypes.TEXT,
        allowNull: true,
      },
      snapDay: {
        type: DataTypes.TEXT,
        allowNull: true,
      },
      status: {
        type: DataTypes.TEXT,
        allowNull: false,
      },
      storedCredentialTransactionId: {
        type: DataTypes.INTEGER,
        allowNull: true,
      },
      totalRevenueInCents: {
        type: DataTypes.INTEGER,
        allowNull: true,
      },
      trialEndedAt: {
        type: DataTypes.DATEONLY,
        allowNull: true,
        get() {
          return this.getDataValue('trialEndedAt') ? moment.utc(this.getDataValue('trialEndedAt')).format('YYYY-MM-DD') : null;
        },
      },
      trialStartedAt: {
        type: DataTypes.DATEONLY,
        allowNull: true,
        get() {
          return this.getDataValue('trialStartedAt') ? moment.utc(this.getDataValue('trialStartedAt')).format('YYYY-MM-DD') : null;
        },
      },
      updatedAt: {
        type: DataTypes.DATE,
        allowNull: true,
      },
      // Chargify payment fields
      cardId: {
        type: DataTypes.INTEGER,
        allowNull: true,
      },
      maskedCardNumber: {
        type: DataTypes.TEXT,
        allowNull: true,
      },
      cardType: {
        type: DataTypes.TEXT,
        allowNull: true,
      },
      expirationMonth: {
        type: DataTypes.INTEGER,
        allowNull: true,
      },
      expirationYear: {
        type: DataTypes.INTEGER,
        allowNull: true,
      },
      customerId: {
        // NOTE: bsSubscription.companyId = crmCompany.crmId = crmCompanyId
        type: DataTypes.TEXT,
        allowNull: false,
      },
      crmDealId: {
        type: DataTypes.TEXT,
        allowNull: true,
      },
      // End of fields that map to Chargify subscription object fields
      termsAcceptedOn: {
        type: DataTypes.DATEONLY,
        allowNull: true,
        get() {
          return this.getDataValue('termsAcceptedOn') ? moment.utc(this.getDataValue('termsAcceptedOn')).format('YYYY-MM-DD') : null;
        },
      },
    },
    {
      timestamps: true,
      paranoid: true,
    }
  );

  subscription.associate = (models) => {
    models.subscription.belongsTo(models.account, {
      as: 'account',
      constraints: false,
    });

    models.subscription.belongsTo(models.user, {
      as: 'createdBy',
    });

    models.subscription.belongsTo(models.user, {
      as: 'updatedBy',
    });
  };

  /**
   * Translate Chargify-formatted fields that are in snake_case to
   * IXC-formatted fields that are in camelCase. A few fields are also
   * renamed to conform to IXC naming conventions.
   *
   * @param {Object} data - The data from Chargify
   */
  subscription.translateChargifyFieldsToIxc = (data) => {
    const returnedData = { ...data };
    if (returnedData !== Object(returnedData)) {
      return returnedData;
    }

    Object.keys(returnedData).forEach((chargifyField) => {
      const ixcField = chargifyToIxcFieldsMapping[chargifyField];
      if (ixcField === undefined) {
        return;
      }

      returnedData[ixcField] = returnedData[chargifyField];
      delete returnedData[chargifyField];
    });

    // Calculate Renewal Date
    const startDate = returnedData.activatedAt || returnedData.trialEndedAt;

    if (startDate) {
      returnedData.renewalDate = moment(startDate).add(1, 'years');
      const today = moment();
      let infiniteCatch = 0;
      while (returnedData.renewalDate.isBefore(today) && infiniteCatch < 2) {
        infiniteCatch += 1; // This is just because while loops make me anxious
        returnedData.renewalDate = returnedData.renewalDate.add(1, 'years');
      }
      returnedData.renewalDate = returnedData.renewalDate.format();
    }
    return returnedData;
  };

  /**
   * Translate IXC-formatted fields that are in camelCase to
   * Chargify-formatted fields that are in snake_case.
   *
   * @param {Object} data - The data from IXC
   */
  subscription.translateIxcFieldsToChargify = (data) => {
    const returnedData = { ...data };
    if (returnedData !== Object(returnedData)) {
      return returnedData;
    }
    // console.log('==================================', returnedData);

    Object.keys(returnedData).forEach((ixcField) => {
      const chargifyField = ixcToChargifyFieldsMapping[ixcField];
      if (chargifyField === undefined) {
        return;
      }

      returnedData[chargifyField] = returnedData[ixcField];
      delete returnedData[ixcField];
    });
    return returnedData;
  };

  /**
   * Copy the id value in the `product` child object of the Chargify
   * subscription API response to a top-level field to allow the model
   * to capture it as `providerProductId`.
   *
   * @param {Object} data - The data from Chargify
   */
  subscription.prepareProviderId = (data) => {
    const returnedData = { ...data };
    if (returnedData !== Object(returnedData)) {
      return returnedData;
    }

    const { product } = returnedData;
    if (!product || !product.id) {
      return returnedData;
    }

    returnedData.providerProductId = product.id;
    // Use name as backup if handle is blank in chargify
    returnedData.providerProductHandle = product.handle || product.name.toLowerCase().replace(/ /g, '_');

    if (!product.product_family || !product.product_family.id) {
      return returnedData;
    }
    returnedData.providerFamilyHandle = product.product_family.handle || product.product_family.name.toLowerCase().replace(/ /g, '_');

    return returnedData;
  };

  /**
   * Copy the payment values in the `credit_card` child object of the Chargify
   * subscription API response to top-level fields to allow the model
   * to capture in the database
   *
   * @param {Object} data - The data from Chargify
   */
  subscription.preparePaymentInfo = (data) => {
    const returnedData = { ...data };
    if (returnedData !== Object(returnedData)) {
      return returnedData;
    }
    const { customer } = returnedData;
    if (customer && customer.id) {
      returnedData.customerId = customer.id;
    }

    const card = returnedData.creditCard;
    if (!card || !card.id) {
      returnedData.cardId = null;
      returnedData.paymentType = null;
      returnedData.maskedCardNumber = null;
      returnedData.cardType = null;
      returnedData.expirationMonth = null;
      returnedData.expirationYear = null;
      delete returnedData.creditCard;
      return returnedData;
    }

    returnedData.cardId = card.id;
    returnedData.paymentType = card.payment_type;
    returnedData.maskedCardNumber = card.masked_card_number;
    returnedData.cardType = card.card_type;
    returnedData.expirationMonth = card.expiration_month;
    returnedData.expirationYear = card.expiration_year;
    delete returnedData.creditCard;
    return returnedData;
  };

  /**
   * Copy the components of the Chargify subscription to a JSON string
   * to allow the model to capture in the database
   *
   * @param {Object} data - The data from Chargify
   */
  subscription.prepareComponentInfo = (data) => {
    const returnedData = { ...data };
    if (returnedData !== Object(returnedData)) {
      returnedData.components = '';
      return returnedData;
    }

    let { components } = returnedData;
    if (typeof components === 'string') {
      try {
        components = JSON.parse(components);
      } catch (error) {
        returnedData.components = '';
        return returnedData;
      }
    }

    if (!components || components.length <= 0) {
      returnedData.components = '';
      return returnedData;
    }

    components.map((component) => {
      if (component.allocated_quantity > 0 || component.component_handle.includes('_addon')) {
        return component;
      }
      return null;
    });
    returnedData.components = JSON.stringify(components);
    return returnedData;
  };

  subscription.FIELD_NAMES = FIELD_NAMES;

  return subscription;
};
