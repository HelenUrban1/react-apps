const SequelizeMock = require('sequelize-mock');
const modelModule = require('./station');

const { data } = require('../../__fixtures__').station;

describe('Station Model', () => {
  let mockSequelize;
  let mockDataTypes;
  let model;

  beforeEach(() => {
    mockSequelize = new SequelizeMock();
    mockDataTypes = mockSequelize.Sequelize;
    model = modelModule(mockSequelize, mockDataTypes);
  });

  describe('#create()', () => {
    it('should create a valid model instance', async () => {
      const fixture = data[0];

      const instance = await model.create(fixture);
      expect(instance.name).toBe(fixture.name);
    });
  });

  describe('#save()', () => {
    it('should honor a change to field', async () => {
      const fixture = data[0];
      const instance = await model.create(fixture);

      expect(instance.name).toBe(fixture.name);
      const newName = 'some-new-station-name';

      instance.name = newName;
      await instance.save();

      expect(instance.name).toBe(newName);
    });
  });
});
