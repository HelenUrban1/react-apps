/**
 * Site database model.
 * See https://sequelize.org/v5/manual/models-definition.html to learn how to customize it.
 */
module.exports = function(sequelize, DataTypes) {
  const site = sequelize.define(
    'site',
    {
      id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        primaryKey: true,
      },
      status: {
        type: DataTypes.ENUM,
        allowNull: false,
        values: ['Active', 'Past_Due', 'Suspended', 'Archived'],
      },
      settings: {
        type: DataTypes.TEXT,
        allowNull: false,
        validate: {
          notEmpty: true,
        },
      },
      siteName: {
        type: DataTypes.TEXT,
        allowNull: false,
        validate: {
          notEmpty: true,
        },
      },
      sitePhone: {
        type: DataTypes.TEXT,
      },
      siteEmail: {
        type: DataTypes.TEXT,
        allowNull: false,
        validate: {
          notEmpty: true,
        },
      },
      siteStreet: {
        type: DataTypes.TEXT,
      },
      siteStreet2: {
        type: DataTypes.TEXT,
      },
      siteCity: {
        type: DataTypes.TEXT,
      },
      siteRegion: {
        type: DataTypes.ENUM,
        values: [
          'Alabama',
          'Alaska',
          'American_Samoa',
          'Arizona',
          'Arkansas',
          'California',
          'Colorado',
          'Connecticut',
          'Delaware',
          'District_of_Columbia',
          'Florida',
          'Georgia',
          'Guam',
          'Hawaii',
          'Idaho',
          'Illinois',
          'Indiana',
          'Iowa',
          'Kansas',
          'Kentucky',
          'Louisiana',
          'Maine',
          'Maryland',
          'Massachusetts',
          'Michigan',
          'Minnesota',
          'Minor_Outlying_Islands',
          'Mississippi',
          'Missouri',
          'Montana',
          'Nebraska',
          'Nevada',
          'New_Hampshire',
          'New_Jersey',
          'New_Mexico',
          'New_York',
          'North_Carolina',
          'North_Dakota',
          'Northern_Mariana_Islands',
          'Ohio',
          'Oklahoma',
          'Oregon',
          'Pennsylvania',
          'Puerto_Rico',
          'Rhode_Island',
          'South_Carolina',
          'South_Dakota',
          'Tennessee',
          'Texas',
          'US_Virgin_Islands',
          'Utah',
          'Vermont',
          'Virginia',
          'Washington',
          'West_Virginia',
          'Wisconsin',
          'Wyoming',
        ],
      },
      sitePostalcode: {
        type: DataTypes.TEXT,
      },
      siteCountry: {
        type: DataTypes.ENUM,
        values: ['United_States'],
      },
      importHash: {
        type: DataTypes.STRING(255),
        allowNull: true,
        unique: true,
      },
    },
    {
      timestamps: true,
      paranoid: true,
    },
  );

  site.associate = models => {
    models.site.hasMany(models.part, {
      as: 'parts',
      constraints: false,
      foreignKey: 'siteId',
    });

    models.site.hasMany(models.accountMember, {
      as: 'members',
      constraints: false,
      foreignKey: 'siteId',
    });

    models.site.hasMany(models.reportTemplate, {
      as: 'reportTemplates',
      constraints: false,
      foreignKey: 'siteId',
    });

    models.site.hasMany(models.report, {
      as: 'reports',
      constraints: false,
      foreignKey: 'siteId',
    });

    models.site.belongsTo(models.user, {
      as: 'createdBy',
      constraints: false,
    });

    models.site.belongsTo(models.user, {
      as: 'updatedBy',
      constraints: false,
    });
  };

  return site;
};
