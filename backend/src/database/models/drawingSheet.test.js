const modelModule = require('./drawingSheet');
const SequelizeMock = require('sequelize-mock');

const { data } = require('../../__fixtures__').drawingSheet;

describe('DrawingSheet Model', () => {
  let mockSequelize;
  let mockDataTypes;
  let model;

  beforeEach(() => {
    mockSequelize = new SequelizeMock();
    mockDataTypes = mockSequelize.Sequelize;
    model = modelModule(mockSequelize, mockDataTypes);
  });

  describe('#create()', () => {
    it('should create a valid model instance', async () => {
      const fixture = data[0];
      const instance = await model.create(fixture);

      expect(instance.id).toBe(fixture.id);
    });
  });

  describe('#save()', () => {
    it('should honor a change to field', async () => {
      const fixture = data[0];
      const instance = await model.create(fixture);

      expect(instance.displayLines).toBe(fixture.displayLines);
      const newSettings = true;

      instance.displayLines = newSettings;
      await instance.save();

      expect(instance.displayLines).toBe(newSettings);
    });
  });
});