const pick = require('lodash/pick');
const Sequelize = require('sequelize');
const models = require('../models');
const SequelizeRepository = require('./sequelizeRepository');
const AuditLogRepository = require('./auditLogRepository');
const SequelizeFilterUtils = require('../utils/sequelizeFilterUtils');

const { Op } = Sequelize;

/**
 * Handles database operations for the ListItems.
 * See https://sequelize.org/v5/index.html to learn how to customize it.
 */
class ListItemRepository {
  /**
   * Creates the Site.
   *
   * @param {Object} data
   * @param {Object} [options]
   */
  static async create(data, options) {
    const currentUser = SequelizeRepository.getCurrentUser(options);

    const tenant = await models.getTenant(currentUser.accountId);

    const transaction = SequelizeRepository.getTransaction(options);

    const record = await tenant.listItem.create(
      {
        ...pick(data, [
          'id', //
          'name',
          'metadata',
          'default',
          'status',
          'listType',
          'itemIndex',
          'parentId',
          'siteId',
          'userId',
          'updatedAt',
          'createdAt',
        ]),
        createdById: currentUser.id,
        updatedById: currentUser.id,
      },
      {
        transaction,
      }
    );

    await ListItemRepository.createAuditLog(AuditLogRepository.CREATE, record, data, options);

    return ListItemRepository.findById(record.id, options);
  }

  static async createGlobal(data, options) {
    const currentUser = SequelizeRepository.getCurrentUser(options);
    const transaction = SequelizeRepository.getTransaction(options);
    const record = await models.listItem.create(
      {
        ...pick(data, [
          'id', //
          'name',
          'metadata',
          'default',
          'status',
          'listType',
          'itemIndex',
          'parentId',
          'siteId',
          'userId',
          'updatedAt',
          'createdAt',
        ]),
        createdById: currentUser.id,
        updatedById: currentUser.id,
      },
      {
        transaction,
      }
    );
    await ListItemRepository.createAuditLog(AuditLogRepository.CREATE, record, data, options);

    return ListItemRepository.findById(record.id, options);
  }

  /**
   * Updates the ListItem.
   *
   * @param {Object} data
   * @param {Object} [options]
   */
  static async update(id, data, options) {
    const currentUser = SequelizeRepository.getCurrentUser(options);

    const tenant = await models.getTenant(currentUser.accountId);

    const transaction = SequelizeRepository.getTransaction(options);

    let record = await tenant.listItem.findByPk(id, {
      transaction,
    });

    record = await record.update(
      {
        ...pick(data, [
          'id', //
          'name',
          'metadata',
          'default',
          'status',
          'listType',
          'itemIndex',
          'parentId',
          'siteId',
          'userId',
          'updatedAt',
          'createdAt',
        ]),
        updatedById: currentUser.id,
      },
      {
        transaction,
      }
    );

    await ListItemRepository.createAuditLog(AuditLogRepository.UPDATE, record, data, options);

    return ListItemRepository.findById(record.id, options);
  }

  /**
   * Deletes the ListItem.
   *
   * @param {string} id
   * @param {Object} [options]
   */
  static async destroy(id, options) {
    const currentUser = SequelizeRepository.getCurrentUser(options);

    const tenant = await models.getTenant(currentUser.accountId);

    const transaction = SequelizeRepository.getTransaction(options);

    const record = await tenant.listItem.findByPk(id, {
      transaction,
    });

    await record.destroy({
      transaction,
    });

    await ListItemRepository.createAuditLog(AuditLogRepository.DELETE, record, null, options);
  }

  /**
   * Finds the ListItem.
   *
   * @param {string} id
   * @param {Object} [options]
   */
  static async findById(id, options) {
    const currentUser = SequelizeRepository.getCurrentUser(options);

    const tenant = await models.getTenant(currentUser.accountId);

    const transaction = SequelizeRepository.getTransaction(options);

    const include = [];

    let record = await tenant.listItem.findByPk(id, {
      include,
      transaction,
    });

    if (!record) {
      record = await models.listItem.findByPk(id, {
        include,
        transaction,
      });
    }

    return ListItemRepository.fillWithRelations(record, options);
  }

  static async sort(listItems) {
    const sortedItems = [];

    for (let index = 0; index < listItems.length; index++) {
      const listItem = listItems[index];
      if (listItem.parentId === null) {
        sortedItems.push(listItem);
        const childItems = listItems.filter((x) => x.parentId === listItem.id).sort((a, b) => a.itemIndex - b.itemIndex);
        sortedItems.push(...childItems);
      }
    }

    return sortedItems;
  }

  /**
   * Finds all list items for a list type.
   * If user is logged in, will include items added by the
   * user and by the user's site and account.
   * @param {string} listType
   * @param {object} options
   */
  static async findByType(listType, options) {
    const currentUser = SequelizeRepository.getCurrentUser(options);
    const include = [];
    let where = {
      listType,
    };
    const order = ['parentId', 'itemIndex'];
    const allRecords = [];
    const globalRecords = await models.listItem.findAndCountAll({ where, include });
    allRecords.push(...globalRecords.rows);
    if (currentUser && currentUser.accountId) {
      const tenant = await models.getTenant(currentUser.accountId);
      if (currentUser.siteId) {
        where = {
          ...where,
          [Op.or]: [{ siteId: currentUser.siteId }, { siteId: null }],
        };
      }
      if (currentUser.userId) {
        where = {
          ...where,
          [Op.or]: [{ userId: currentUser.userId }, { userId: null }],
        };
      }
      const tenantRecords = await tenant.listItem.findAndCountAll({ where, order, include });
      allRecords.push(...tenantRecords.rows);
    }

    return ListItemRepository.sort(await ListItemRepository.fillWithRelationsForRows(allRecords, options));
  }

  /**
   * Finds the ListItems based on the query.
   * See https://sequelize.org/v5/manual/querying.html to learn how to
   * customize the query.
   *
   * @param {Object} query
   * @param {Object} query.filter
   * @param {number} query.limit
   * @param  {number} query.offset
   * @param  {string} query.orderBy
   * @param {Object} [options]
   *
   * @returns {Promise<Object>} response - Object containing the rows and the count.
   */
  static async findAndCountAll(
    { filter, limit, offset, orderBy, paranoid } = {
      filter: null,
      limit: 0,
      offset: 0,
      orderBy: null,
      paranoid: true,
    },
    options
  ) {
    const currentUser = SequelizeRepository.getCurrentUser(options);

    const tenant = await models.getTenant(currentUser.accountId);

    let where = {};
    const include = [];

    if (filter) {
      if (filter.id) {
        where = {
          ...where,
          id: SequelizeFilterUtils.uuid(filter.id),
        };
      }

      if (filter.name) {
        where = {
          ...where,
          [Op.and]: SequelizeFilterUtils.ilike('listItem', 'name', filter.name),
        };
      }

      if (filter.siteId) {
        where = {
          ...where,
          siteId: SequelizeFilterUtils.uuid(filter.siteId),
        };
      }

      if (filter.userId) {
        where = {
          ...where,
          userId: SequelizeFilterUtils.uuid(filter.userId),
        };
      }

      if (filter.createdAtRange) {
        const [start, end] = filter.createdAtRange;

        if (start !== undefined && start !== null && start !== '') {
          where = {
            ...where,
            createdAt: {
              ...where.createdAt,
              [Op.gte]: start,
            },
          };
        }

        if (end !== undefined && end !== null && end !== '') {
          where = {
            ...where,
            createdAt: {
              ...where.createdAt,
              [Op.lte]: end,
            },
          };
        }
      }
    }

    const { rows, count } = await tenant.listItem.findAndCountAll({
      where,
      include,
      limit: limit ? Number(limit) : undefined,
      offset: offset ? Number(offset) : undefined,
      order: orderBy ? [orderBy.split('_')] : [['createdAt', 'DESC']],
      paranoid,
      transaction: SequelizeRepository.getTransaction(options),
    });

    const outputRows = await ListItemRepository.fillWithRelationsForRows(rows, options);

    return { rows: outputRows, count };
  }

  /**
   * Creates an audit log of the operation.
   *
   * @param {string} action - The action [create, update or delete].
   * @param {object} record - The sequelize record
   * @param {object} data - The new data passed on the request
   * @param {object} options
   */
  static async createAuditLog(action, record, data, options) {
    let values = {};

    if (data) {
      values = {
        ...record.get({ plain: true }),
      };
    }

    await AuditLogRepository.log(
      {
        entityName: 'listItem',
        entityId: record.id,
        action,
        values,
      },
      options
    );
  }

  /**
   * Fills an array of ListItem.
   *
   * @param {Array} rows
   * @param {Object} [options]
   */
  static async fillWithRelationsForRows(rows, options) {
    if (!rows) {
      return rows;
    }

    return Promise.all(rows.map((record) => ListItemRepository.fillWithRelations(record, options)));
  }

  /**
   * Fill the ListItem.
   *
   * @param {Object} record
   * @param {Object} [options]
   */
  static async fillWithRelations(record, options) {
    if (!record) {
      return record;
    }

    return record.get({ plain: true });
  }
}

module.exports = ListItemRepository;
