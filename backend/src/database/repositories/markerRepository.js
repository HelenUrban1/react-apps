const pick = require('lodash/pick');
const Sequelize = require('sequelize');
const models = require('../models');
const SequelizeRepository = require('./sequelizeRepository');
const AuditLogRepository = require('./auditLogRepository');
const SequelizeFilterUtils = require('../utils/sequelizeFilterUtils');

const { Op } = Sequelize;

/**
 * Handles database operations for the Marker.
 * See https://sequelize.org/v5/index.html to learn how to customize it.
 */
class MarkerRepository {
  /**
   * Creates the Marker.
   *
   * @param {Object} data
   * @param {Object} [options]
   */
  async create(data, options) {
    const currentUser = SequelizeRepository.getCurrentUser(options);

    const tenant = await models.getTenant(currentUser.accountId);

    const transaction = SequelizeRepository.getTransaction(options);

    const record = await tenant.marker.create(
      {
        ...pick(data, ['id', 'markerStyleName', 'shape', 'defaultPosition', 'hasLeaderLine', 'fontSize', 'fontColor', 'fillColor', 'borderColor', 'borderWidth', 'borderStyle', 'leaderLineColor', 'leaderLineWidth', 'leaderLineStyle', 'importHash', 'updatedAt', 'createdAt', 'deletedAt']),
        createdById: currentUser.id,
        updatedById: currentUser.id,
      },
      {
        transaction,
      }
    );

    await this._createAuditLog(AuditLogRepository.CREATE, record, data, options);

    return this.findById(record.id, options);
  }

  /**
   * Updates the Marker.
   *
   * @param {Object} data
   * @param {Object} [options]
   */
  async update(id, data, options) {
    const currentUser = SequelizeRepository.getCurrentUser(options);

    const tenant = await models.getTenant(currentUser.accountId);

    const transaction = SequelizeRepository.getTransaction(options);

    let record = await tenant.marker.findByPk(id, {
      transaction,
    });

    record = await record.update(
      {
        ...pick(data, ['id', 'markerStyleName', 'shape', 'defaultPosition', 'hasLeaderLine', 'fontSize', 'fontColor', 'fillColor', 'borderColor', 'borderWidth', 'borderStyle', 'leaderLineColor', 'leaderLineWidth', 'leaderLineStyle', 'importHash', 'updatedAt', 'createdAt', 'deletedAt']),
        updatedById: currentUser.id,
      },
      {
        transaction,
      }
    );

    await this._createAuditLog(AuditLogRepository.UPDATE, record, data, options);

    return this.findById(record.id, options);
  }

  /**
   * Deletes the Marker.
   *
   * @param {string} id
   * @param {Object} [options]
   */
  async destroy(id, options) {
    const currentUser = SequelizeRepository.getCurrentUser(options);

    const tenant = await models.getTenant(currentUser.accountId);

    const transaction = SequelizeRepository.getTransaction(options);

    const record = await tenant.marker.findByPk(id, {
      transaction,
    });

    await record.destroy({
      transaction,
    });

    await this._createAuditLog(AuditLogRepository.DELETE, record, null, options);
  }

  /**
   * Finds the Marker and its relations.
   *
   * @param {string} id
   * @param {Object} [options]
   */
  async findById(id, options) {
    const currentUser = SequelizeRepository.getCurrentUser(options);

    const tenant = await models.getTenant(currentUser.accountId);

    const transaction = SequelizeRepository.getTransaction(options);

    const include = [];

    const record = await tenant.marker.findByPk(id, {
      include,
      transaction,
    });

    return this._fillWithRelationsAndFiles(record, options);
  }

  /**
   * Counts the number of Markers based on the filter.
   *
   * @param {Object} filter
   * @param {Object} [options]
   */
  async count(filter, options) {
    const currentUser = SequelizeRepository.getCurrentUser(options);

    const tenant = await models.getTenant(currentUser.accountId);

    const transaction = SequelizeRepository.getTransaction(options);

    return tenant.marker.count(
      {
        where: filter,
      },
      {
        transaction,
      }
    );
  }

  /**
   * Finds the Markers based on the query.
   * See https://sequelize.org/v5/manual/querying.html to learn how to
   * customize the query.
   *
   * @param {Object} query
   * @param {Object} query.filter
   * @param {number} query.limit
   * @param  {number} query.offset
   * @param  {string} query.orderBy
   * @param {boolean} query.paranoid
   * @param {Object} [options]
   *
   * @returns {Promise<Object>} response - Object containing the rows and the count.
   */
  async findAndCountAll(
    { filter, limit, offset, orderBy, paranoid } = {
      filter: null,
      limit: 0,
      offset: 0,
      orderBy: null,
      paranoid: true, // discards entries with a non-null deletedAt value when true
    },
    options
  ) {
    const currentUser = SequelizeRepository.getCurrentUser(options);

    const tenant = await models.getTenant(currentUser.accountId);

    let where = {};
    const include = [];

    if (filter) {
      if (filter.id) {
        where = {
          ...where,
          [Op.and]: SequelizeFilterUtils.ilike('marker', 'id', filter.id),
          // ['id']: SequelizeFilterUtils.uuid(filter.id),
        };
      }

      if (filter.markerStyleName) {
        where = {
          ...where,
          [Op.and]: SequelizeFilterUtils.ilike('marker', 'markerStyleName', filter.markerStyleName),
        };
      }

      if (filter.shape) {
        where = {
          ...where,
          shape: filter.shape,
        };
      }

      if (filter.defaultPosition) {
        where = {
          ...where,
          [Op.and]: SequelizeFilterUtils.ilike('marker', 'defaultPosition', filter.defaultPosition),
        };
      }

      if (filter.hasLeaderLine === true || filter.hasLeaderLine === 'true' || filter.hasLeaderLine === false || filter.hasLeaderLine === 'false') {
        where = {
          ...where,
          hasLeaderLine: filter.hasLeaderLine === true || filter.hasLeaderLine === 'true',
        };
      }

      if (filter.fontSizeRange) {
        const [start, end] = filter.fontSizeRange;

        if (start !== undefined && start !== null && start !== '') {
          where = {
            ...where,
            fontSize: {
              ...where.fontSize,
              [Op.gte]: start,
            },
          };
        }

        if (end !== undefined && end !== null && end !== '') {
          where = {
            ...where,
            fontSize: {
              ...where.fontSize,
              [Op.lte]: end,
            },
          };
        }
      }

      if (filter.fontColor) {
        where = {
          ...where,
          [Op.and]: SequelizeFilterUtils.ilike('marker', 'fontColor', filter.fontColor),
        };
      }

      if (filter.fillColor) {
        where = {
          ...where,
          [Op.and]: SequelizeFilterUtils.ilike('marker', 'fillColor', filter.fillColor),
        };
      }

      if (filter.borderColor) {
        where = {
          ...where,
          [Op.and]: SequelizeFilterUtils.ilike('marker', 'borderColor', filter.borderColor),
        };
      }

      if (filter.borderWidthRange) {
        const [start, end] = filter.borderWidthRange;

        if (start !== undefined && start !== null && start !== '') {
          where = {
            ...where,
            borderWidth: {
              ...where.borderWidth,
              [Op.gte]: start,
            },
          };
        }

        if (end !== undefined && end !== null && end !== '') {
          where = {
            ...where,
            borderWidth: {
              ...where.borderWidth,
              [Op.lte]: end,
            },
          };
        }
      }

      if (filter.leaderLineColor) {
        where = {
          ...where,
          [Op.and]: SequelizeFilterUtils.ilike('marker', 'leaderLineColor', filter.leaderLineColor),
        };
      }

      if (filter.leaderLineWidthRange) {
        const [start, end] = filter.leaderLineWidthRange;

        if (start !== undefined && start !== null && start !== '') {
          where = {
            ...where,
            leaderLineWidth: {
              ...where.leaderLineWidth,
              [Op.gte]: start,
            },
          };
        }

        if (end !== undefined && end !== null && end !== '') {
          where = {
            ...where,
            leaderLineWidth: {
              ...where.leaderLineWidth,
              [Op.lte]: end,
            },
          };
        }
      }

      if (filter.createdAtRange) {
        const [start, end] = filter.createdAtRange;

        if (start !== undefined && start !== null && start !== '') {
          where = {
            ...where,
            createdAt: {
              ...where.createdAt,
              [Op.gte]: start,
            },
          };
        }

        if (end !== undefined && end !== null && end !== '') {
          where = {
            ...where,
            createdAt: {
              ...where.createdAt,
              [Op.lte]: end,
            },
          };
        }
      }

      if (filter.deletedAtRange) {
        const [start, end] = filter.deletedAtRange;

        if (start !== undefined && start !== null && start !== '') {
          where = {
            ...where,
            deletedAt: {
              ...where.deletedAt,
              [Op.gte]: start,
            },
          };
        }

        if (end !== undefined && end !== null && end !== '') {
          where = {
            ...where,
            deletedAt: {
              ...where.deletedAt,
              [Op.lte]: end,
            },
          };
        }
      }
    }

    let { rows, count } = await tenant.marker.findAndCountAll({
      where,
      include,
      limit: limit ? Number(limit) : undefined,
      offset: offset ? Number(offset) : undefined,
      order: orderBy ? [orderBy.split('_')] : [['createdAt', 'DESC']],
      transaction: SequelizeRepository.getTransaction(options),
      paranoid,
    });

    rows = await this._fillWithRelationsAndFilesForRows(rows, options);

    return { rows, count };
  }

  /**
   * Lists the Markers to populate the autocomplete.
   * See https://sequelize.org/v5/manual/querying.html to learn how to
   * customize the query.
   *
   * @param {string} query
   * @param {number} limit
   */
  async findAllAutocomplete(query, limit, options) {
    const currentUser = SequelizeRepository.getCurrentUser(options);

    const tenant = await models.getTenant(currentUser.accountId);

    let where = {};

    if (query) {
      where = {
        [Op.or]: [
          { id: SequelizeFilterUtils.uuid(query) },
          {
            [Op.and]: SequelizeFilterUtils.ilike('marker', 'markerStyleName', query),
          },
        ],
      };
    }

    const records = await tenant.marker.findAll({
      attributes: ['id', 'markerStyleName'],
      where,
      limit: limit ? Number(limit) : undefined,
      orderBy: [['markerStyleName', 'ASC']],
    });

    return records.map((record) => ({
      id: record.id,
      label: record.markerStyleName,
    }));
  }

  /**
   * Creates an audit log of the operation.
   *
   * @param {string} action - The action [create, update or delete].
   * @param {object} record - The sequelize record
   * @param {object} data - The new data passed on the request
   * @param {object} options
   */
  async _createAuditLog(action, record, data, options) {
    let values = {};

    if (data) {
      values = {
        ...record.get({ plain: true }),
      };
    }

    await AuditLogRepository.log(
      {
        entityName: 'marker',
        entityId: record.id,
        action,
        values,
      },
      options
    );
  }

  /**
   * Fills an array of Marker with relations and files.
   *
   * @param {Array} rows
   * @param {Object} [options]
   */
  async _fillWithRelationsAndFilesForRows(rows, options) {
    if (!rows) {
      return rows;
    }

    return Promise.all(rows.map((record) => this._fillWithRelationsAndFiles(record, options)));
  }

  /**
   * Fill the Marker with the relations and files.
   *
   * @param {Object} record
   * @param {Object} [options]
   */
  async _fillWithRelationsAndFiles(record, options) {
    if (!record) {
      return record;
    }

    const output = record.get({ plain: true });

    const transaction = SequelizeRepository.getTransaction(options);

    return output;
  }
}

module.exports = MarkerRepository;
