const Sequelize = require('sequelize');
const pick = require('lodash/pick');
const ValidationError = require('../../errors/validationError');
const models = require('../models');
const SequelizeRepository = require('./sequelizeRepository');
const AuditLogRepository = require('./auditLogRepository');
const AccountMemberRepository = require('./accountMemberRepository');
const SequelizeFilterUtils = require('../utils/sequelizeFilterUtils');
const { log } = require('../../logger');

const { Op } = Sequelize;

/**
 * Handles database operations for the Account.
 * See https://sequelize.org/v5/index.html to learn how to customize it.
 */
class AccountRepository {
  /**
   * Creates the Account.
   *
   * @param {Object} data
   * @param {Object} [options]
   */
  async create(data, options) {
    const currentUser = SequelizeRepository.getCurrentUser(options);

    const transaction = SequelizeRepository.getTransaction(options);

    const record = await models.account.create(
      {
        ...pick(data, [
          'id',
          'status',
          'settings',
          'orgName',
          'orgPhone',
          'orgEmail',
          'orgStreet',
          'orgStreet2',
          'orgCity',
          'orgRegion',
          'orgPostalcode',
          'orgCountry',
          'orgUnitName',
          'orgUnitId',
          'parentAccountId',
          'importHash',
          'dbHost',
          'dbName',
          'updatedAt',
          'createdAt',
        ]),
        createdById: currentUser.id,
        updatedById: currentUser.id,
      },
      {
        transaction,
      }
    );

    /*
    await record.setSites(data.sites || [], {
      transaction,
    });
    await record.setParts(data.parts || [], {
      transaction,
    }); */
    await record.setMembers(data.members || [], {
      transaction,
    });
    /* await record.setReportTemplates(data.reportTemplates || [], {
      transaction,
    });
    await record.setReports(data.reports || [], {
      transaction,
    }); */

    /* await FileRepository.replaceRelationFiles(
      {
        belongsTo: models.account.getTableName(),
        belongsToColumn: 'orgLogo',
        belongsToId: record.id,
      },
      data.orgLogo,
      options,
    ); */

    await this._createAuditLog(AuditLogRepository.CREATE, record, data, options);

    return this.findById(record.id, options);
  }

  /**
   * Updates the Account.
   *
   * @param {Object} data
   * @param {Object} [options]
   */
  async update(id, data, options) {
    const currentUser = SequelizeRepository.getCurrentUser(options);

    const transaction = SequelizeRepository.getTransaction(options);

    let record = await models.account.findByPk(id, {
      transaction,
    });

    record = await record.update(
      {
        ...pick(data, [
          'id',
          'status',
          'settings',
          'orgName',
          'orgPhone',
          'orgEmail',
          'orgStreet',
          'orgStreet2',
          'orgCity',
          'orgRegion',
          'orgPostalcode',
          'orgCountry',
          'orgUnitName',
          'orgUnitId',
          'parentAccountId',
          'importHash',
          'dbHost',
          'dbName',
          'updatedAt',
          'createdAt',
        ]),
        updatedById: currentUser.id,
      },
      {
        transaction,
      }
    );

    /* await record.setSites(data.sites || [], {
      transaction,
    });
    await record.setParts(data.parts || [], {
      transaction,
    }); */
    // Question: Why is it necessary to update this on every Account update? Was causing a loss of accountId on memberships when trying to update settings field on Account
    if (!options || !options.skipMembersUpdate) {
      await record.setMembers(data.members || [], {
        transaction,
      });
    }
    /* await record.setReportTemplates(data.reportTemplates || [], {
      transaction,
    });
    await record.setReports(data.reports || [], {
      transaction,
    });

    await FileRepository.replaceRelationFiles(
      {
        belongsTo: models.account.getTableName(),
        belongsToColumn: 'orgLogo',
        belongsToId: record.id,
      },
      data.orgLogo,
      options,
    ); */

    await this._createAuditLog(AuditLogRepository.UPDATE, record, data, options);

    return this.findById(record.id, options);
  }

  /**
   * Deletes the Account and all of its AccountMembers.
   *
   * @param {string} id
   * @param {Object} [options]
   */
  async destroy(id, options) {
    const transaction = SequelizeRepository.getTransaction(options);

    const record = await models.account.findByPk(id, {
      transaction,
    });

    const accountMembers = await record.getMembers();
    const accountMemberIds = accountMembers.map((accountMember) => accountMember.id);

    await record.destroy({
      transaction,
    });

    await AccountMemberRepository.destroyAll(accountMemberIds);

    await this._createAuditLog(AuditLogRepository.DELETE, record, null, options);
  }

  /**
   * Finds the Account and its relations.
   *
   * @param {string} id
   * @param {Object} [options]
   */
  async findById(id, options) {
    const transaction = SequelizeRepository.getTransaction(options);

    const include = [];

    const record = await models.account.findByPk(id, {
      include,
      transaction,
    });

    return this._fillWithRelationsAndFiles(record, options);
  }

  /**
   * Counts the number of Accounts based on the filter.
   *
   * @param {Object} filter
   * @param {Object} [options]
   */
  async count(filter, options) {
    const transaction = SequelizeRepository.getTransaction(options);

    return models.account.count(
      {
        where: filter,
      },
      {
        transaction,
      }
    );
  }

  /**
   * Finds the Accounts based on the query.
   * See https://sequelize.org/v5/manual/querying.html to learn how to
   * customize the query.
   *
   * @param {Object} query
   * @param {Object} query.filter
   * @param {number} query.limit
   * @param  {number} query.offset
   * @param  {string} query.orderBy
   * @param {Object} [options]
   *
   * @returns {Promise<Object>} response - Object containing the rows and the count.
   */
  async findAndCountAll(
    { filter, limit, offset, orderBy } = {
      filter: null,
      limit: 0,
      offset: 0,
      orderBy: null,
    },
    options
  ) {
    let where = {};
    const include = [];

    if (filter) {
      if (filter.id) {
        where = {
          ...where,
          id: SequelizeFilterUtils.uuid(filter.id),
        };
      }

      if (filter.status) {
        where = {
          ...where,
          status: filter.status,
        };
      }

      if (filter.settings) {
        where = {
          ...where,
          [Op.and]: SequelizeFilterUtils.ilike('account', 'settings', filter.settings),
        };
      }

      if (filter.orgName) {
        where = {
          ...where,
          [Op.and]: SequelizeFilterUtils.ilike('account', 'orgName', filter.orgName),
        };
      }

      if (filter.orgPhone) {
        where = {
          ...where,
          [Op.and]: SequelizeFilterUtils.ilike('account', 'orgPhone', filter.orgPhone),
        };
      }

      if (filter.orgEmail) {
        where = {
          ...where,
          [Op.and]: SequelizeFilterUtils.ilike('account', 'orgEmail', filter.orgEmail),
        };
      }

      if (filter.orgStreet) {
        where = {
          ...where,
          [Op.and]: SequelizeFilterUtils.ilike('account', 'orgStreet', filter.orgStreet),
        };
      }

      if (filter.orgStreet2) {
        where = {
          ...where,
          [Op.and]: SequelizeFilterUtils.ilike('account', 'orgStreet2', filter.orgStreet2),
        };
      }

      if (filter.orgCity) {
        where = {
          ...where,
          [Op.and]: SequelizeFilterUtils.ilike('account', 'orgCity', filter.orgCity),
        };
      }

      if (filter.orgRegion) {
        where = {
          ...where,
          orgRegion: filter.orgRegion,
        };
      }

      if (filter.orgPostalcode) {
        where = {
          ...where,
          [Op.and]: SequelizeFilterUtils.ilike('account', 'orgPostalcode', filter.orgPostalcode),
        };
      }

      if (filter.orgCountry) {
        where = {
          ...where,
          orgCountry: filter.orgCountry,
        };
      }

      if (filter.orgUnitName) {
        where = {
          ...where,
          [Op.and]: SequelizeFilterUtils.ilike('account', 'orgUnitName', filter.orgUnitName),
        };
      }

      if (filter.orgUnitId) {
        where = {
          ...where,
          orgUnitId: SequelizeFilterUtils.uuid(filter.orgUnitId),
        };
      }

      if (filter.parentAccount) {
        where = {
          ...where,
          parentAccountId: SequelizeFilterUtils.uuid(filter.parentAccount),
        };
      }

      if (filter.createdAtRange) {
        const [start, end] = filter.createdAtRange;

        if (start !== undefined && start !== null && start !== '') {
          where = {
            ...where,
            createdAt: {
              ...where.createdAt,
              [Op.gte]: start,
            },
          };
        }

        if (end !== undefined && end !== null && end !== '') {
          where = {
            ...where,
            createdAt: {
              ...where.createdAt,
              [Op.lte]: end,
            },
          };
        }
      }
    }

    let { rows, count } = await models.account.findAndCountAll({
      where,
      include,
      limit: limit ? Number(limit) : undefined,
      offset: offset ? Number(offset) : undefined,
      order: orderBy ? [orderBy.split('_')] : [['createdAt', 'DESC']],
      transaction: SequelizeRepository.getTransaction(options),
    });

    rows = await this._fillWithRelationsAndFilesForRows(rows, options);

    return { rows, count };
  }

  /**
   * Finds an Account by a given Email Domain ignoring common Domains (e.g. gmail)
   *
   * @param {String} email
   */
  async findByDomain(email, options = {}) {
    const { rows } = await this.findAndCountAll();
    let existingAccount;

    if (rows) {
      const domain = email.split('@').pop();
      rows.forEach((acct) => {
        if (acct.orgEmail) {
          const acctDomain = acct.orgEmail.split('@').pop();
          if (acctDomain && acctDomain === domain) {
            existingAccount = acct;
          }
        }
      });
    }

    return existingAccount;
  }

  /**
   * Lists the Accounts to populate the autocomplete.
   * See https://sequelize.org/v5/manual/querying.html to learn how to
   * customize the query.
   *
   * @param {string} query
   * @param {number} limit
   */
  async findAllAutocomplete(query, limit) {
    let where = {};

    if (query) {
      where = {
        [Op.or]: [
          { id: SequelizeFilterUtils.uuid(query) },
          {
            [Op.and]: SequelizeFilterUtils.ilike('account', 'orgName', query),
          },
        ],
      };
    }

    const records = await models.account.findAll({
      attributes: ['id', 'orgName'],
      where,
      limit: limit ? Number(limit) : undefined,
      orderBy: [['orgName', 'ASC']],
    });

    return records.map((record) => ({
      id: record.id,
      label: record.orgName,
    }));
  }

  /**
   * Creates an audit log of the operation.
   *
   * @param {string} action - The action [create, update or delete].
   * @param {object} record - The sequelize record
   * @param {object} data - The new data passed on the request
   * @param {object} options
   */
  async _createAuditLog(action, record, data, options) {
    let values = {};

    if (data) {
      values = {
        ...record.get({ plain: true }),
        orgLogo: data.orgLogo,
        partsIds: data.parts,
        membersIds: data.members,
        reportTemplatesIds: data.reportTemplates,
        reportsIds: data.reports,
      };
    }

    await AuditLogRepository.log(
      {
        entityName: 'account',
        entityId: record.id,
        action,
        values,
      },
      options
    );
  }

  /**
   * Fills an array of Account with relations and files.
   *
   * @param {Array} rows
   * @param {Object} [options]
   */
  async _fillWithRelationsAndFilesForRows(rows, options) {
    if (!rows) {
      return rows;
    }

    return Promise.all(rows.map((record) => this._fillWithRelationsAndFiles(record, options)));
  }

  /**
   * Fill the Account with the relations and files.
   *
   * @param {Object} record
   * @param {Object} [options]
   */
  async _fillWithRelationsAndFiles(record, options) {
    if (!record) {
      return record;
    }

    const output = record.get({ plain: true });

    const transaction = SequelizeRepository.getTransaction(options);

    // output.orgLogo = await record.getOrgLogo({
    //   transaction,
    // });
    // output.parts = await record.getParts({
    //   transaction,
    // });
    output.members = await record.getMembers({
      transaction,
    });
    // output.subscriptions = await record.getSubscriptions({
    //   transaction,
    // });
    // output.reportTemplates = await record.getReportTemplates({
    //   transaction,
    // });
    // output.reports = await record.getReports({
    //   transaction,
    // });

    return output;
  }
}

module.exports = AccountRepository;
