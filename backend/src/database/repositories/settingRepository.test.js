const SettingRepository = require('./settingRepository');
const SequelizeRepository = require('./sequelizeRepository');
const { data } = require('../../__fixtures__').setting;
const config = require('../../../config')();

describe('SettingRepository', () => {
  const repository = SettingRepository;
  const options = {
    currentUser: {
      accountId: config.testAccountId,
      siteId: '31ef1d56-1b86-4ffa-b886-507ed8f021c5',
      userId: 'e0a01779-c8ed-4090-bd44-9c1b2242fb75',
    },
  };

  beforeEach(async () => {
    await SequelizeRepository.cleanDatabase();
    await SequelizeRepository.cleanDatabase(options.currentUser.accountId);
  });

  describe('#create()', () => {
    it('should create an item', async () => {
      const fixture = data[1];
      const item = await repository.create(fixture, options);

      expect(item.id).toBe(fixture.id);
    });
  });

  describe('#createGlobal()', () => {
    it('should create an item in the primary database', async () => {
      const fixture = data[1];
      const item = await repository.createGlobal(fixture, options);

      expect(item.id).toBe(fixture.id);
    });
  });

  describe('#update()', () => {
    it('should update an item', async () => {
      const originalItem = await repository.create(data[1], options);

      const updatedValue = 'dark';
      const updatedItem = await repository.update(originalItem.id, { value: updatedValue }, options);

      expect(originalItem.id).toBe(updatedItem.id);
      expect(updatedItem.value).toBe(updatedValue);
    });
  });

  describe('#destroy()', () => {
    it('should remove an item', async () => {
      const originalItem = await repository.create(data[1], options);

      await repository.destroy(originalItem.id, options);

      const foundItem = await repository.findById(originalItem.id, options);
      expect(foundItem).toBe(null);
    });
  });

  describe('#findById()', () => {
    it('should find an existing item', async () => {
      const originalItem = await repository.create(data[1], options);

      const foundItem = await repository.findById(originalItem.id, options);
      expect(originalItem).toStrictEqual(foundItem);
    });
  });

  describe('#findByName()', () => {
    it('should return a setting for the user when it exists', async () => {
      const userItem = await repository.create(data[3], options);
      // site
      await repository.create(data[2], options);
      // account
      await repository.create(data[1], options);
      // global
      await repository.createGlobal(data[0], options);

      const foundItem = await repository.findByName('theme', options);
      expect(foundItem).toStrictEqual(userItem);
    });

    it('should return a setting for the site when it exists', async () => {
      const siteItem = await repository.create(data[2], options);
      // account
      await repository.create(data[1], options);
      // global
      await repository.createGlobal(data[0], options);

      const foundItem = await repository.findByName('theme', options);
      expect(foundItem).toStrictEqual(siteItem);
    });

    it('should return a setting for the account when it exists', async () => {
      const accountItem = await repository.create(data[1], options);
      // global
      await repository.createGlobal(data[0], options);

      const foundItem = await repository.findByName('theme', options);
      expect(foundItem).toStrictEqual(accountItem);
    });

    it('should return a global setting when it exists', async () => {
      const globalItem = await repository.createGlobal(data[0], options);

      const foundItem = await repository.findByName('theme', options);
      expect(foundItem).toStrictEqual(globalItem);
    });
  });

  describe('#findAndCountAll()', () => {
    it('should find and count all the items', async () => {
      const originalItem = await repository.create(data[1], options);

      const result = await repository.findAndCountAll({}, options);
      expect(result.count).toBe(1);
      expect(result.rows[0]).toStrictEqual(originalItem);
    });
  });

  afterAll(async () => {
    await SequelizeRepository.closeConnections(options.currentUser.accountId);
    await SequelizeRepository.closeConnections();
  });
});
