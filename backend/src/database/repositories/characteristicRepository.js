const pick = require('lodash/pick');
const Sequelize = require('sequelize');
const models = require('../models');
const SequelizeRepository = require('./sequelizeRepository');
const AuditLogRepository = require('./auditLogRepository');
const SequelizeFilterUtils = require('../utils/sequelizeFilterUtils');

const { Op } = Sequelize;
const fields = [
  'id',
  'previousRevision',
  'nextRevision',
  'originalCharacteristic',
  'notationType',
  'notationSubtype',
  'notationClass',
  'fullSpecification',
  'quantity',
  'nominal',
  'upperSpecLimit',
  'lowerSpecLimit',
  'plusTol',
  'minusTol',
  'unit',
  'criticality',
  'inspectionMethod',
  'notes',
  'drawingRotation',
  'drawingScale',
  'boxLocationY',
  'boxLocationX',
  'boxWidth',
  'boxHeight',
  'boxRotation',
  'markerGroup',
  'markerGroupShared',
  'markerIndex',
  'markerSubIndex',
  'markerLabel',
  'markerStyle',
  'markerLocationX',
  'markerLocationY',
  'markerFontSize',
  'markerSize',
  'markerRotation',
  'gridCoordinates',
  'balloonGridCoordinates',
  'connectionPointGridCoordinates',
  'drawingSheetIndex',
  'gdtSymbol',
  'gdtPrimaryToleranceZone',
  'gdtSecondaryToleranceZone',
  'gdtPrimaryDatum',
  'gdtSecondaryDatum',
  'gdtTertiaryDatum',
  'captureMethod',
  'captureError',
  'status',
  'verified',
  'fullSpecificationFromOCR',
  'confidence',
  'operation',
  'toleranceSource',
  'importHash',
  'updatedAt',
  'createdAt',
  'connectionPointLocationX',
  'connectionPointLocationY',
  'connectionPointIsFloating',
  'displayLeaderLine',
  'leaderLineDistance',
];

/**
 * Handles database operations for the Characteristic.
 * See https://sequelize.org/v5/index.html to learn how to customize it.
 */
class CharacteristicRepository {
  /**
   * Creates the Characteristic.
   *
   * @param {Object} data
   * @param {Object} [options]
   */
  async create(data, options) {
    const currentUser = SequelizeRepository.getCurrentUser(options);

    const tenant = await models.getTenant(currentUser.accountId);

    return this.createForUserAndTenant(currentUser, tenant, data, options);
  }

  async createInAccount(data, options) {
    const currentUser = SequelizeRepository.getCurrentUser(options);

    const tenant = await models.getTenant(data.accountId);

    return this.createForUserAndTenant(currentUser, tenant, data, options);
  }

  async createForUserAndTenant(currentUser, tenant, data, options) {
    const transaction = SequelizeRepository.getTransaction(options);

    const record = await tenant.characteristic.create(
      {
        ...pick(data, fields),
        createdById: currentUser.id,
        updatedById: currentUser.id,
      },
      {
        transaction,
      }
    );

    await record.setPart(data.part || null, {
      transaction,
    });
    await record.setDrawing(data.drawing || null, {
      transaction,
    });
    await record.setDrawingSheet(data.drawingSheet || null, {
      transaction,
    });
    if (!data.originalCharacteristic) {
      // If this is the original characteristic, set originalCharacteristic to the id
      await record.setOriginalCharacteristic(record.id, { transaction });
    }
    if (data.previousRevision) {
      // if this was a new revision, set the nextRevision pointer on the previousRevision
      const previousRecord = await tenant.characteristic.findByPk(data.previousRevision.id || data.previousRevision, {
        transaction,
      });
      await previousRecord.setNextRevision(record.id || null, {
        transaction,
      });
    }

    await this._createAuditLog(AuditLogRepository.CREATE, record, data, options);

    return this.findById(record.id, options);
  }

  /**
   * Updates the Characteristic.
   *
   * @param {Object} data
   * @param {Object} [options]
   */
  async update(id, data, options) {
    const currentUser = SequelizeRepository.getCurrentUser(options);

    const tenant = await models.getTenant(currentUser.accountId);

    const transaction = SequelizeRepository.getTransaction(options);

    let record = await tenant.characteristic.findByPk(id, {
      transaction,
    });

    record = await record.update(
      {
        ...pick(data, fields),
        updatedById: currentUser.id,
      },
      {
        transaction,
      }
    );

    await record.setPart(data.part || null, {
      transaction,
    });
    await record.setDrawing(data.drawing || null, {
      transaction,
    });
    await record.setDrawingSheet(data.drawingSheet || null, {
      transaction,
    });

    await this._createAuditLog(AuditLogRepository.UPDATE, record, data, options);

    return this.findById(record.id, options);
  }

  async updateFromReview(id, data, options) {
    const currentUser = SequelizeRepository.getCurrentUser(options);

    const tenant = await models.getTenant(data.accountId);

    const transaction = SequelizeRepository.getTransaction(options);

    let record = await tenant.characteristic.findByPk(id, {
      transaction,
    });

    record = await record.update(
      {
        ...pick(data, [
          'id', //
          'notationType',
          'notationSubtype',
          'notationClass',
          'fullSpecification',
          'quantity',
          'nominal',
          'upperSpecLimit',
          'lowerSpecLimit',
          'plusTol',
          'minusTol',
          'unit',
          'criticality',
          'inspectionMethod',
          'notes',
          'drawingRotation',
          'drawingScale',
          'boxLocationY',
          'boxLocationX',
          'boxWidth',
          'boxHeight',
          'boxRotation',
          'markerGroup',
          'markerGroupShared',
          'markerIndex',
          'markerSubIndex',
          'markerLabel',
          'markerStyle',
          'markerLocationX',
          'markerLocationY',
          'markerFontSize',
          'markerSize',
          'markerRotation',
          'gridCoordinates',
          'balloonGridCoordinates',
          'connectionPointGridCoordinates',
          'drawingSheetIndex',
          'gdtSymbol',
          'gdtPrimaryToleranceZone',
          'gdtSecondaryToleranceZone',
          'gdtPrimaryDatum',
          'gdtSecondaryDatum',
          'gdtTertiaryDatum',
          'captureMethod',
          'captureError',
          'status',
          'verified',
          'fullSpecificationFromOCR',
          'confidence',
          'operation',
          'toleranceSource',
          'importHash',
          'updatedAt',
          'createdAt',
          'reviewTaskId',
          'connectionPointLocationX',
          'connectionPointLocationY',
          'connectionPointIsFloating',
          'displayLeaderLine',
          'leaderLineDistance',
        ]),
        updatedById: currentUser.id,
      },
      {
        transaction,
      }
    );

    await this._createAuditLog(AuditLogRepository.UPDATE, record, data, options);

    if (data.deletedAt) {
      await record.destroy({
        transaction,
      });

      await this._createAuditLog(AuditLogRepository.DELETE, record, null, options);
      return null;
    }

    return this.findByIdInAccount(record.id, data.accountId, options);
  }

  /**
   * Deletes the Characteristic.
   *
   * @param {string} id
   * @param {Object} [options]
   */
  async destroy(id, options) {
    const currentUser = SequelizeRepository.getCurrentUser(options);

    const tenant = await models.getTenant(currentUser.accountId);

    const transaction = SequelizeRepository.getTransaction(options);

    const record = await tenant.characteristic.findByPk(id, {
      transaction,
    });

    await record.destroy({
      transaction,
    });

    await this._createAuditLog(AuditLogRepository.DELETE, record, null, options);
  }

  async destroyFromReview(id, data, options) {
    const tenant = await models.getTenant(data.accountId);

    const transaction = SequelizeRepository.getTransaction(options);

    const record = await tenant.characteristic.findByPk(id, {
      transaction,
    });

    await record.destroy({
      transaction,
    });

    await this._createAuditLog(AuditLogRepository.DELETE, record, null, options);

    return record;
  }

  /**
   * Finds the Characteristic and its relations.
   *
   * @param {string} id
   * @param {Object} [options]
   */
  async findById(id, options) {
    const currentUser = SequelizeRepository.getCurrentUser(options);

    const tenant = await models.getTenant(currentUser.accountId);

    const transaction = SequelizeRepository.getTransaction(options);

    const include = [
      {
        model: tenant.part,
        as: 'part',
      },
      {
        model: tenant.drawing,
        as: 'drawing',
      },
      {
        model: tenant.drawingSheet,
        as: 'drawingSheet',
      },
    ];

    const record = await tenant.characteristic.findByPk(id, {
      include,
      transaction,
    });

    return this._fillWithRelationsAndFiles(record, options);
  }

  async findByIdInAccount(id, accountId, options) {
    const tenant = await models.getTenant(accountId);

    const transaction = SequelizeRepository.getTransaction(options);

    const include = [
      {
        model: tenant.part,
        as: 'part',
      },
      {
        model: tenant.drawing,
        as: 'drawing',
      },
      {
        model: tenant.drawingSheet,
        as: 'drawingSheet',
      },
    ];

    const record = await tenant.characteristic.findByPk(id, {
      include,
      transaction,
    });

    return this._fillWithRelationsAndFiles(record, options);
  }

  /**
   * Counts the number of Characteristics based on the filter.
   *
   * @param {Object} filter
   * @param {Object} [options]
   */
  async count(filter, options) {
    const currentUser = SequelizeRepository.getCurrentUser(options);

    const tenant = await models.getTenant(currentUser.accountId);

    const transaction = SequelizeRepository.getTransaction(options);

    return tenant.characteristic.count(
      {
        where: filter,
      },
      {
        transaction,
      }
    );
  }

  /**
   * Finds the Characteristics based on the query.
   * See https://sequelize.org/v5/manual/querying.html to learn how to
   * customize the query.
   *
   * @param {Object} query
   * @param {Object} query.filter
   * @param {number} query.limit
   * @param  {number} query.offset
   * @param  {string} query.orderBy
   * @param {Object} [options]
   *
   * @returns {Promise<Object>} response - Object containing the rows and the count.
   */
  async findAndCountAll(
    { filter, limit, offset, orderBy } = {
      filter: null,
      limit: 0,
      offset: 0,
      orderBy: null,
    },
    options
  ) {
    const currentUser = SequelizeRepository.getCurrentUser(options);

    const tenant = await models.getTenant(currentUser.accountId);

    let where = {};
    const include = [
      {
        model: tenant.part,
        as: 'part',
      },
      {
        model: tenant.drawing,
        as: 'drawing',
      },
      {
        model: tenant.drawingSheet,
        as: 'drawingSheet',
      },
    ];

    if (filter) {
      if (filter.id) {
        where = {
          ...where,
          id: SequelizeFilterUtils.uuid(filter.id),
        };
      }

      if (filter.notationType) {
        where = {
          ...where,
          notationType: filter.notationType,
        };
      }

      if (filter.notationSubtype) {
        where = {
          ...where,
          notationSubtype: filter.notationSubtype,
        };
      }

      if (filter.notationClass) {
        where = {
          ...where,
          notationClass: filter.notationClass,
        };
      }

      if (filter.fullSpecification) {
        where = {
          ...where,
          [Op.and]: SequelizeFilterUtils.ilike('characteristic', 'fullSpecification', filter.fullSpecification),
        };
      }

      if (filter.quantityRange) {
        const [start, end] = filter.quantityRange;

        if (start !== undefined && start !== null && start !== '') {
          where = {
            ...where,
            quantity: {
              ...where.quantity,
              [Op.gte]: start,
            },
          };
        }

        if (end !== undefined && end !== null && end !== '') {
          where = {
            ...where,
            quantity: {
              ...where.quantity,
              [Op.lte]: end,
            },
          };
        }
      }

      if (filter.nominal) {
        where = {
          ...where,
          [Op.and]: SequelizeFilterUtils.ilike('characteristic', 'nominal', filter.nominal),
        };
      }

      if (filter.upperSpecLimit) {
        where = {
          ...where,
          [Op.and]: SequelizeFilterUtils.ilike('characteristic', 'upperSpecLimit', filter.upperSpecLimit),
        };
      }

      if (filter.lowerSpecLimit) {
        where = {
          ...where,
          [Op.and]: SequelizeFilterUtils.ilike('characteristic', 'lowerSpecLimit', filter.lowerSpecLimit),
        };
      }

      if (filter.plusTol) {
        where = {
          ...where,
          [Op.and]: SequelizeFilterUtils.ilike('characteristic', 'plusTol', filter.plusTol),
        };
      }

      if (filter.minusTol) {
        where = {
          ...where,
          [Op.and]: SequelizeFilterUtils.ilike('characteristic', 'minusTol', filter.minusTol),
        };
      }

      if (filter.unit) {
        where = {
          ...where,
          [Op.and]: SequelizeFilterUtils.ilike('characteristic', 'unit', filter.unit),
        };
      }

      if (filter.criticality) {
        where = {
          ...where,
          criticality: filter.criticality,
        };
      }

      if (filter.inspectionMethod) {
        where = {
          ...where,
          [Op.and]: SequelizeFilterUtils.ilike('characteristic', 'inspectionMethod', filter.inspectionMethod),
        };
      }

      if (filter.notes) {
        where = {
          ...where,
          [Op.and]: SequelizeFilterUtils.ilike('characteristic', 'notes', filter.notes),
        };
      }

      if (filter.drawingRotationRange) {
        const [start, end] = filter.drawingRotationRange;

        if (start !== undefined && start !== null && start !== '') {
          where = {
            ...where,
            drawingRotation: {
              ...where.drawingRotation,
              [Op.gte]: start,
            },
          };
        }

        if (end !== undefined && end !== null && end !== '') {
          where = {
            ...where,
            drawingRotation: {
              ...where.drawingRotation,
              [Op.lte]: end,
            },
          };
        }
      }

      if (filter.drawingScaleRange) {
        const [start, end] = filter.drawingScaleRange;

        if (start !== undefined && start !== null && start !== '') {
          where = {
            ...where,
            drawingScale: {
              ...where.drawingScale,
              [Op.gte]: start,
            },
          };
        }

        if (end !== undefined && end !== null && end !== '') {
          where = {
            ...where,
            drawingScale: {
              ...where.drawingScale,
              [Op.lte]: end,
            },
          };
        }
      }

      if (filter.boxLocationYRange) {
        const [start, end] = filter.boxLocationYRange;

        if (start !== undefined && start !== null && start !== '') {
          where = {
            ...where,
            boxLocationY: {
              ...where.boxLocationY,
              [Op.gte]: start,
            },
          };
        }

        if (end !== undefined && end !== null && end !== '') {
          where = {
            ...where,
            boxLocationY: {
              ...where.boxLocationY,
              [Op.lte]: end,
            },
          };
        }
      }

      if (filter.boxLocationXRange) {
        const [start, end] = filter.boxLocationXRange;

        if (start !== undefined && start !== null && start !== '') {
          where = {
            ...where,
            boxLocationX: {
              ...where.boxLocationX,
              [Op.gte]: start,
            },
          };
        }

        if (end !== undefined && end !== null && end !== '') {
          where = {
            ...where,
            boxLocationX: {
              ...where.boxLocationX,
              [Op.lte]: end,
            },
          };
        }
      }

      if (filter.boxWidthRange) {
        const [start, end] = filter.boxWidthRange;

        if (start !== undefined && start !== null && start !== '') {
          where = {
            ...where,
            boxWidth: {
              ...where.boxWidth,
              [Op.gte]: start,
            },
          };
        }

        if (end !== undefined && end !== null && end !== '') {
          where = {
            ...where,
            boxWidth: {
              ...where.boxWidth,
              [Op.lte]: end,
            },
          };
        }
      }

      if (filter.boxHeightRange) {
        const [start, end] = filter.boxHeightRange;

        if (start !== undefined && start !== null && start !== '') {
          where = {
            ...where,
            boxHeight: {
              ...where.boxHeight,
              [Op.gte]: start,
            },
          };
        }

        if (end !== undefined && end !== null && end !== '') {
          where = {
            ...where,
            boxHeight: {
              ...where.boxHeight,
              [Op.lte]: end,
            },
          };
        }
      }

      if (filter.boxRotationRange) {
        const [start, end] = filter.boxRotationRange;

        if (start !== undefined && start !== null && start !== '') {
          where = {
            ...where,
            boxRotation: {
              ...where.boxRotation,
              [Op.gte]: start,
            },
          };
        }

        if (end !== undefined && end !== null && end !== '') {
          where = {
            ...where,
            boxRotation: {
              ...where.boxRotation,
              [Op.lte]: end,
            },
          };
        }
      }

      if (filter.markerGroup) {
        where = {
          ...where,
          [Op.and]: SequelizeFilterUtils.ilike('characteristic', 'markerGroup', filter.markerGroup),
        };
      }

      if (
        filter.markerGroupShared === true || //
        filter.markerGroupShared === 'true' ||
        filter.markerGroupShared === false ||
        filter.markerGroupShared === 'false'
      ) {
        where = {
          ...where,
          markerGroupShared: filter.markerGroupShared === true || filter.markerGroupShared === 'true',
        };
      }

      if (filter.markerIndexRange) {
        const [start, end] = filter.markerIndexRange;

        if (start !== undefined && start !== null && start !== '') {
          where = {
            ...where,
            markerIndex: {
              ...where.markerIndex,
              [Op.gte]: start,
            },
          };
        }

        if (end !== undefined && end !== null && end !== '') {
          where = {
            ...where,
            markerIndex: {
              ...where.markerIndex,
              [Op.lte]: end,
            },
          };
        }
      }

      if (filter.markerSubIndexRange) {
        const [start, end] = filter.markerSubIndexRange;

        if (start !== undefined && start !== null && start !== '') {
          where = {
            ...where,
            markerSubIndex: {
              ...where.markerSubIndex,
              [Op.gte]: start,
            },
          };
        }

        if (end !== undefined && end !== null && end !== '') {
          where = {
            ...where,
            markerSubIndex: {
              ...where.markerSubIndex,
              [Op.lte]: end,
            },
          };
        }
      }

      if (filter.markerLabel) {
        where = {
          ...where,
          [Op.and]: SequelizeFilterUtils.ilike('characteristic', 'markerLabel', filter.markerLabel),
        };
      }

      if (filter.markerStyle) {
        where = {
          ...where,
          [Op.and]: SequelizeFilterUtils.ilike('characteristic', 'markerStyle', filter.markerStyle),
        };
      }

      if (filter.markerLocationXRange) {
        const [start, end] = filter.markerLocationXRange;

        if (start !== undefined && start !== null && start !== '') {
          where = {
            ...where,
            markerLocationX: {
              ...where.markerLocationX,
              [Op.gte]: start,
            },
          };
        }

        if (end !== undefined && end !== null && end !== '') {
          where = {
            ...where,
            markerLocationX: {
              ...where.markerLocationX,
              [Op.lte]: end,
            },
          };
        }
      }

      if (filter.markerLocationYRange) {
        const [start, end] = filter.markerLocationYRange;

        if (start !== undefined && start !== null && start !== '') {
          where = {
            ...where,
            markerLocationY: {
              ...where.markerLocationY,
              [Op.gte]: start,
            },
          };
        }

        if (end !== undefined && end !== null && end !== '') {
          where = {
            ...where,
            markerLocationY: {
              ...where.markerLocationY,
              [Op.lte]: end,
            },
          };
        }
      }

      if (filter.connectionPointLocationXRange) {
        const [start, end] = filter.connectionPointLocationXRange;

        if (start !== undefined && start !== null && start !== '') {
          where = {
            ...where,
            connectionPointLocationX: {
              ...where.connectionPointLocationX,
              [Op.gte]: start,
            },
          };
        }

        if (end !== undefined && end !== null && end !== '') {
          where = {
            ...where,
            connectionPointLocationX: {
              ...where.connectionPointLocationX,
              [Op.lte]: end,
            },
          };
        }
      }

      if (filter.connectionPointLocationYRange) {
        const [start, end] = filter.connectionPointLocationYRange;

        if (start !== undefined && start !== null && start !== '') {
          where = {
            ...where,
            connectionPointLocationY: {
              ...where.connectionPointLocationY,
              [Op.gte]: start,
            },
          };
        }

        if (end !== undefined && end !== null && end !== '') {
          where = {
            ...where,
            connectionPointLocationY: {
              ...where.connectionPointLocationY,
              [Op.lte]: end,
            },
          };
        }
      }

      if (filter.connectionPointIsFloating) {
        where = {
          ...where,
          connectionPointIsFloating: filter.connectionPointIsFloating,
        };
      }

      if (filter.displayLeaderLine) {
        where = {
          ...where,
          displayLeaderLine: filter.displayLeaderLine,
        };
      }

      if (filter.gridCoordinates) {
        where = {
          ...where,
          [Op.and]: SequelizeFilterUtils.ilike('characteristic', 'gridCoordinates', filter.gridCoordinates),
        };
      }

      if (filter.balloonGridCoordinates) {
        where = {
          ...where,
          [Op.and]: SequelizeFilterUtils.ilike('characteristic', 'balloonGridCoordinates', filter.balloonGridCoordinates),
        };
      }

      if (filter.connectionPointGridCoordinates) {
        where = {
          ...where,
          [Op.and]: SequelizeFilterUtils.ilike('characteristic', 'connectionPointGridCoordinates', filter.connectionPointGridCoordinates),
        };
      }

      if (filter.part) {
        where = {
          ...where,
          partId: SequelizeFilterUtils.uuid(filter.part),
        };
      }

      if (filter.drawing) {
        where = {
          ...where,
          drawingId: SequelizeFilterUtils.uuid(filter.drawing),
        };
      }

      if (filter.drawingSheet) {
        where = {
          ...where,
          drawingSheetId: SequelizeFilterUtils.uuid(filter.drawingSheet),
        };
      }

      if (filter.originalCharacteristic) {
        where = {
          ...where,
          originalCharacteristic: SequelizeFilterUtils.uuid(filter.originalCharacteristic),
        };
      }

      if (filter.drawingSheetIndexRange) {
        const [start, end] = filter.drawingSheetIndexRange;

        if (start !== undefined && start !== null && start !== '') {
          where = {
            ...where,
            drawingSheetIndex: {
              ...where.drawingSheetIndex,
              [Op.gte]: start,
            },
          };
        }

        if (end !== undefined && end !== null && end !== '') {
          where = {
            ...where,
            drawingSheetIndex: {
              ...where.drawingSheetIndex,
              [Op.lte]: end,
            },
          };
        }
      }

      if (filter.gdtSymbol) {
        where = {
          ...where,
          [Op.and]: SequelizeFilterUtils.ilike('characteristic', 'gdtSymbol', filter.gdtSymbol),
        };
      }

      if (filter.gdtPrimaryToleranceZone) {
        where = {
          ...where,
          [Op.and]: SequelizeFilterUtils.ilike('characteristic', 'gdtPrimaryToleranceZone', filter.gdtPrimaryToleranceZone),
        };
      }

      if (filter.gdtSecondaryToleranceZone) {
        where = {
          ...where,
          [Op.and]: SequelizeFilterUtils.ilike('characteristic', 'gdtSecondaryToleranceZone', filter.gdtSecondaryToleranceZone),
        };
      }

      if (filter.gdtPrimaryDatum) {
        where = {
          ...where,
          [Op.and]: SequelizeFilterUtils.ilike('characteristic', 'gdtPrimaryDatum', filter.gdtPrimaryDatum),
        };
      }

      if (filter.gdtSecondaryDatum) {
        where = {
          ...where,
          [Op.and]: SequelizeFilterUtils.ilike('characteristic', 'gdtSecondaryDatum', filter.gdtSecondaryDatum),
        };
      }

      if (filter.gdtTertiaryDatum) {
        where = {
          ...where,
          [Op.and]: SequelizeFilterUtils.ilike('characteristic', 'gdtTertiaryDatum', filter.gdtTertiaryDatum),
        };
      }

      if (filter.captureMethod) {
        where = {
          ...where,
          captureMethod: filter.captureMethod,
        };
      }

      if (filter.captureError) {
        where = {
          ...where,
          captureError: filter.captureError,
        };
      }

      if (filter.status) {
        where = {
          ...where,
          status: filter.status,
        };
      }

      if (filter.verified) {
        where = {
          ...where,
          verified: filter.verified,
        };
      }

      if (filter.fullSpecificationFromOCR) {
        where = {
          ...where,
          [Op.and]: SequelizeFilterUtils.ilike('characteristic', 'fullSpecificationFromOCR', filter.fullSpecificationFromOCR),
        };
      }

      if (filter.confidenceRange) {
        const [start, end] = filter.confidenceRange;

        if (start !== undefined && start !== null && start !== '') {
          where = {
            ...where,
            confidence: {
              ...where.confidence,
              [Op.gte]: start,
            },
          };
        }

        if (end !== undefined && end !== null && end !== '') {
          where = {
            ...where,
            confidence: {
              ...where.confidence,
              [Op.lte]: end,
            },
          };
        }
      }

      if (filter.operation) {
        where = {
          ...where,
          [Op.and]: SequelizeFilterUtils.ilike('characteristic', 'operation', filter.operation),
        };
      }

      if (filter.toleranceSource) {
        where = {
          ...where,
          toleranceSource: filter.toleranceSource,
        };
      }

      if (filter.createdAtRange) {
        const [start, end] = filter.createdAtRange;

        if (start !== undefined && start !== null && start !== '') {
          where = {
            ...where,
            createdAt: {
              ...where.createdAt,
              [Op.gte]: start,
            },
          };
        }

        if (end !== undefined && end !== null && end !== '') {
          where = {
            ...where,
            createdAt: {
              ...where.createdAt,
              [Op.lte]: end,
            },
          };
        }
      }
    }

    let { rows, count } = await tenant.characteristic.findAndCountAll({
      where,
      include,
      limit: limit ? Number(limit) : undefined,
      offset: offset ? Number(offset) : undefined,
      order: orderBy ? [orderBy.split('_')] : [['createdAt', 'DESC']],
      transaction: SequelizeRepository.getTransaction(options),
    });

    rows = await this._fillWithRelationsAndFilesForRows(rows, options);

    return { rows, count };
  }

  async findAndCountAllForReview(partId, accountId, options) {
    const tenant = await models.getTenant(accountId);

    let { rows, count } = await tenant.characteristic.findAndCountAll({
      where: {
        partId: SequelizeFilterUtils.uuid(partId),
        deletedAt: null,
      },
      include: [],
      limit: undefined,
      offset: undefined,
      order: [['markerIndex', 'ASC']],
      transaction: SequelizeRepository.getTransaction(options),
    });

    rows = await this._fillWithRelationsAndFilesForRows(rows, options);

    return { rows, count };
  }

  /**
   * Lists the Characteristics to populate the autocomplete.
   * See https://sequelize.org/v5/manual/querying.html to learn how to
   * customize the query.
   *
   * @param {string} query
   * @param {number} limit
   */
  async findAllAutocomplete(query, limit, options) {
    const currentUser = SequelizeRepository.getCurrentUser(options);

    const tenant = await models.getTenant(currentUser.accountId);

    let where = {};

    if (query) {
      where = {
        [Op.or]: [
          { id: SequelizeFilterUtils.uuid(query) },
          {
            [Op.and]: SequelizeFilterUtils.ilike('characteristic', 'markerLabel', query),
          },
        ],
      };
    }

    const records = await tenant.characteristic.findAll({
      attributes: ['id', 'markerLabel'],
      where,
      limit: limit ? Number(limit) : undefined,
      orderBy: [['markerLabel', 'ASC']],
    });

    return records.map((record) => ({
      id: record.id,
      label: record.markerLabel,
    }));
  }

  /**
   * Creates an audit log of the operation.
   *
   * @param {string} action - The action [create, update or delete].
   * @param {object} record - The sequelize record
   * @param {object} data - The new data passed on the request
   * @param {object} options
   */
  async _createAuditLog(action, record, data, options) {
    let values = {};

    if (data) {
      values = {
        ...record.get({ plain: true }),
      };
    }

    await AuditLogRepository.log(
      {
        entityName: 'characteristic',
        entityId: record.id,
        action,
        values,
      },
      options
    );
  }

  /**
   * Fills an array of Characteristic with relations and files.
   *
   * @param {Array} rows
   * @param {Object} [options]
   */
  async _fillWithRelationsAndFilesForRows(rows, options) {
    if (!rows) {
      return rows;
    }

    return Promise.all(rows.map((record) => this._fillWithRelationsAndFiles(record, options)));
  }

  /**
   * Fill the Characteristic with the relations and files.
   *
   * @param {Object} record
   * @param {Object} [options]
   */
  async _fillWithRelationsAndFiles(record, options) {
    if (!record) {
      return record;
    }

    const output = record.get({ plain: true });

    const transaction = SequelizeRepository.getTransaction(options);

    output.part = await record.getPart({
      transaction,
    });

    output.drawing = await record.getDrawing({
      transaction,
    });

    output.drawingSheet = await record.getDrawingSheet({
      transaction,
    });

    return output;
  }

  static convertToInput(item) {
    return {
      drawingSheetIndex: item.drawingSheetIndex,
      previousRevision: item.previousRevision,
      originalCharacteristic: item.originalCharacteristic,
      status: item.status,
      captureMethod: item.captureMethod,
      captureError: item.captureError,
      notationType: item.notationType,
      notationSubtype: item.notationSubtype,
      notationClass: item.notationClass,
      fullSpecification: item.fullSpecification,
      quantity: item.quantity,
      nominal: item.nominal,
      upperSpecLimit: item.upperSpecLimit,
      lowerSpecLimit: item.lowerSpecLimit,
      plusTol: item.plusTol,
      minusTol: item.minusTol,
      unit: item.unit,
      gdtSymbol: item.gdtSymbol,
      gdtPrimaryToleranceZone: item.gdtPrimaryToleranceZone,
      gdtSecondaryToleranceZone: item.gdtSecondaryToleranceZone,
      gdtPrimaryDatum: item.gdtPrimaryDatum,
      gdtSecondaryDatum: item.gdtSecondaryDatum,
      gdtTertiaryDatum: item.gdtTertiaryDatum,
      inspectionMethod: item.inspectionMethod,
      criticality: item.criticality,
      notes: item.notes,
      drawingRotation: item.drawingRotation,
      drawingScale: item.drawingScale,
      boxLocationY: item.boxLocationY,
      boxLocationX: item.boxLocationX,
      boxWidth: item.boxWidth,
      boxHeight: item.boxHeight,
      boxRotation: item.boxRotation,
      markerGroup: item.markerGroup,
      markerGroupShared: item.markerGroupShared,
      markerIndex: item.markerIndex,
      markerSubIndex: item.markerSubIndex,
      markerLabel: item.markerLabel,
      markerStyle: item.markerStyle,
      markerLocationX: item.markerLocationX,
      markerLocationY: item.markerLocationY,
      markerFontSize: item.markerFontSize,
      markerSize: item.markerSize,
      markerRotation: item.markerRotation || 0,
      gridCoordinates: item.gridCoordinates,
      balloonGridCoordinates: item.balloonGridCoordinates,
      connectionPointGridCoordinates: item.connectionPointGridCoordinates,
      toleranceSource: item.toleranceSource,
      operation: item.operation,
      part: item.part ? item.part.id || item.part : null,
      drawing: item.drawing ? item.drawing.id || item.drawing : null,
      drawingSheet: item.drawingSheet ? item.drawingSheet.id || item.drawingSheet : null,
      verified: item.verified,
      fullSpecificationFromOCR: item.fullSpecificationFromOCR,
      confidence: item.confidence,
      connectionPointLocationX: item.connectionPointLocationX,
      connectionPointLocationY: item.connectionPointLocationY,
      connectionPointIsFloating: item.connectionPointIsFloating,
      displayLeaderLine: item.displayLeaderLine,
      leaderLineDistance: item.leaderLineDistance,
    };
  }
}

module.exports = CharacteristicRepository;
