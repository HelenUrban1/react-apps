const models = require('../models');

/**
 * Abstracts some basic Sequelize operations.
 * See https://sequelize.org/v5/index.html to learn how to customize it.
 */
module.exports = class SequelizeRepository {
  /**
   * Cleans the database.
   * Passing in an accountId will clean for that tenant,
   * otherwise will clean primary database.
   */
  static async cleanDatabase(accountId) {
    if (process.env.NODE_ENV !== 'test') {
      throw new Error('Clean database only allowed for test!');
    }

    if (accountId) {
      const tenant = await models.getTenant(accountId);
      await tenant.sequelize.sync({ force: true });
    } else {
      await models.sequelize.sync({ force: true });
    }
  }

  /**
   * Closes all database connections.
   * Passing in an accountId will close connections for that tenant,
   * otherwise will close connections to primary database.
   */
  static async closeConnections(accountId) {
    if (process.env.NODE_ENV !== 'test') {
      throw new Error('Closing database connections only allowed for test!');
    }

    if (accountId) {
      const tenant = await models.getTenant(accountId);
      await tenant.sequelize.close();
    } else {
      await models.sequelize.close();
      await models.sequelizeAdmin.close();
    }
  }

  /**
   * Returns the currentUser if it exists on the options.
   *
   * @param {object} options
   */
  static getCurrentUser(options) {
    return (options && options.currentUser) || { id: null, siteId: null, accountId: null };
  }

  /**
   * Returns the targetUser if it exists on the options.
   *
   * @param {object} options
   */
  static getTargetUser(options) {
    return (options && options.targetUser) || { id: null, siteId: null, accountId: null };
  }

  /**
   * Returns the transaction if it exists on the options.
   *
   * @param {object} options
   */
  static getTransaction(options) {
    return (options && options.transaction) || undefined;
  }

  /**
   * Creates a database transaction.
   * Passing in an accountId will create a transaction for that tenant,
   * otherwise will create a transaction for the primary database.
   */
  static async createTransaction(accountId) {
    if (accountId) {
      const tenant = await models.getTenant(accountId);
      return tenant.sequelize.transaction();
    }
    return models.sequelize.transaction();
  }

  /**
   * Commits a database transaction.
   */
  static async commitTransaction(transaction) {
    return transaction.commit();
  }

  /**
   * Rolls back a database transaction.
   */
  static async rollbackTransaction(transaction) {
    return transaction.rollback();
  }
};
