const pick = require('lodash/pick');
const Sequelize = require('sequelize');
const models = require('../models');
const SequelizeRepository = require('./sequelizeRepository');
const AuditLogRepository = require('./auditLogRepository');
const SequelizeFilterUtils = require('../utils/sequelizeFilterUtils');

const { Op } = Sequelize;
const fields = [
  'id',
  'annotation',
  'fontSize',
  'fontColor',
  'borderColor',
  'backgroundColor',
  'drawingRotation',
  'drawingScale',
  'boxLocationY',
  'boxLocationX',
  'boxWidth',
  'boxHeight',
  'boxRotation',
  'drawingSheetIndex',
  'characteristicId',
  'partId',
  'drawingId',
  'drawingSheetId',
  'originalAnnotation',
  'previousRevision',
  'nextRevision',
  'createdById',
  'updatedById',
  'createdAt',
  'updatedAt',
  'deletedAt',
];

/**
 * Handles database operations for the Annotation.
 * See https://sequelize.org/v5/index.html to learn how to customize it.
 */
class AnnotationRepository {
  /**
   * Creates the Annotation.
   *
   * @param {Object} data
   * @param {Object} [options]
   */
  async create(data, options) {
    const currentUser = SequelizeRepository.getCurrentUser(options);

    const transaction = SequelizeRepository.getTransaction(options);

    const tenant = await models.getTenant(currentUser.accountId);

    const record = await tenant.annotation.create(
      {
        ...pick(data, fields),
        createdById: currentUser.id,
        updatedById: currentUser.id,
      },
      {
        transaction,
      }
    );

    if (data.characteristicId) {
      const charRecord = await tenant.characteristic.findByPk(data.characteristicId, { transaction });
      await record.setCharacteristic(charRecord, { transaction });
    }

    if (data.partId) {
      const partRecord = await tenant.part.findByPk(data.partId, { transaction });
      await record.setPart(partRecord, { transaction });
    }

    if (data.drawingId) {
      const drawingRecord = await tenant.drawing.findByPk(data.drawingId, { transaction });
      await record.setDrawing(drawingRecord, { transaction });
    }

    if (data.drawingSheetId) {
      const drawingSheetRecord = await tenant.drawingSheet.findByPk(data.drawingSheetId, { transaction });
      await record.setDrawingSheet(drawingSheetRecord, { transaction });
    }

    if (!data.originalAnnotation) {
      // If this is the original annotation, set originalAnnotation to the id
      await record.setOriginalAnnotation(record.id, { transaction });
    }

    if (data.previousRevision) {
      // if this was a new revision, set the nextRevision pointer on the previousRevision
      const previousRecord = await tenant.annotation.findByPk(data.previousRevision.id || data.previousRevision, {
        transaction,
      });
      await previousRecord.setNextRevision(record.id || null, {
        transaction,
      });
    }

    await this._createAuditLog(AuditLogRepository.CREATE, record, data, options);

    return this.findById(record.id, options);
  }

  /**
   * Updates the Annotation.
   *
   * @param {Object} data
   * @param {Object} [options]
   */
  async update(id, data, options) {
    const currentUser = SequelizeRepository.getCurrentUser(options);

    const transaction = SequelizeRepository.getTransaction(options);

    const tenant = await models.getTenant(currentUser.accountId);

    let record = await tenant.annotation.findByPk(id, {
      transaction,
    });

    record = await record.update(
      {
        ...pick(data, fields),
        updatedById: data.operatorId,
      },
      {
        transaction,
      }
    );

    if (data.characteristicId) {
      const charRecord = await tenant.characteristic.findByPk(data.characteristicId, { transaction });
      await record.setCharacteristic(charRecord, { transaction });
    }

    if (data.partId) {
      const partRecord = await tenant.part.findByPk(data.partId, { transaction });
      await record.setPart(partRecord, { transaction });
    }

    if (data.drawingId) {
      const drawingRecord = await tenant.drawing.findByPk(data.drawingId, { transaction });
      await record.setDrawing(drawingRecord, { transaction });
    }

    if (data.drawingSheetId) {
      const drawingSheetRecord = await tenant.drawingSheet.findByPk(data.drawingSheetId, { transaction });
      await record.setDrawingSheet(drawingSheetRecord, { transaction });
    }

    await this._createAuditLog(AuditLogRepository.UPDATE, record, data, options);

    return this.findById(record.id, options);
  }

  /**
   * Deletes the Annotation.
   *
   * @param {string} id
   * @param {Object} [options]
   */
  async destroy(id, options) {
    const currentUser = SequelizeRepository.getCurrentUser(options);

    const transaction = SequelizeRepository.getTransaction(options);

    const tenant = await models.getTenant(currentUser.accountId);

    const record = await tenant.annotation.findByPk(id, {
      transaction,
    });

    await record.destroy({
      transaction,
    });

    await this._createAuditLog(AuditLogRepository.DELETE, record, null, options);
  }

  /**
   * Finds the Annotation and its relations.
   *
   * @param {string} id
   * @param {Object} [options]
   */
  async findById(id, options) {
    const currentUser = SequelizeRepository.getCurrentUser(options);
    const transaction = SequelizeRepository.getTransaction(options);

    const tenant = await models.getTenant(currentUser.accountId);

    const include = [
      {
        model: tenant.part,
        as: 'part',
      },
      {
        model: tenant.characteristic,
        as: 'characteristic',
      },
      {
        model: tenant.drawing,
        as: 'drawing',
      },
      {
        model: tenant.drawingSheet,
        as: 'drawingSheet',
      },
    ];

    const record = await tenant.annotation.findByPk(id, {
      include,
      transaction,
    });

    return this._fillWithRelations(record, options);
  }

  /**
   * Counts the number of Annotations based on the filter.
   *
   * @param {Object} filter
   * @param {Object} [options]
   */
  async count(filter, options) {
    const currentUser = SequelizeRepository.getCurrentUser(options);

    const transaction = SequelizeRepository.getTransaction(options);

    const tenant = await models.getTenant(currentUser.accountId);

    return tenant.annotation.count(
      {
        where: filter,
      },
      {
        transaction,
      }
    );
  }

  /**
   * Finds the Annotations based on the query.
   * See https://sequelize.org/v5/manual/querying.html to learn how to
   * customize the query.
   *
   * @param {Object} query
   * @param {Object} query.filter
   * @param {number} query.limit
   * @param {number} query.offset
   * @param {string} query.orderBy
   * @param {Object} [options]
   *
   * @returns {Promise<Object>} response - Object containing the rows and the count.
   */
  async findAndCountAll(
    { filter, limit, offset, orderBy } = {
      filter: null,
      limit: 0,
      offset: 0,
      orderBy: null,
    },
    options
  ) {
    const currentUser = SequelizeRepository.getCurrentUser(options);

    const tenant = await models.getTenant(currentUser.accountId);

    let where = {};
    const include = [
      {
        model: tenant.part,
        as: 'part',
      },
      {
        model: tenant.characteristic,
        as: 'characteristic',
      },
      {
        model: tenant.drawing,
        as: 'drawing',
      },
      {
        model: tenant.drawingSheet,
        as: 'drawingSheet',
      },
      {
        model: tenant.user,
        as: 'createdBy',
      },
      {
        model: tenant.user,
        as: 'updatedBy',
      },
    ];
    if (filter) {
      if (filter.id) {
        where = {
          ...where,
          id: SequelizeFilterUtils.uuid(filter.id),
        };
      }

      if (filter.annotation) {
        where = {
          ...where,
          [Op.and]: SequelizeFilterUtils.ilike('annotation', 'annotation', filter.annotation),
        };
      }

      if (filter.fontColor) {
        where = {
          ...where,
          fontColor: filter.fontColor,
        };
      }

      if (filter.fontSize) {
        where = {
          ...where,
          fontSize: filter.fontSize,
        };
      }

      if (filter.fontColor) {
        where = {
          ...where,
          fontColor: filter.fontColor,
        };
      }
      if (filter.borderColor) {
        where = {
          ...where,
          borderColor: filter.borderColor,
        };
      }
      if (filter.backgroundColor) {
        where = {
          ...where,
          backgroundColor: filter.backgroundColor,
        };
      }
      if (filter.drawingRotation) {
        where = {
          ...where,
          drawingRotation: filter.drawingRotation,
        };
      }
      if (filter.drawingScale) {
        where = {
          ...where,
          drawingScale: filter.drawingScale,
        };
      }
      if (filter.drawingSheetIndex) {
        where = {
          ...where,
          drawingSheetIndex: filter.drawingSheetIndex,
        };
      }
      if (filter.part) {
        where = {
          ...where,
          partId: SequelizeFilterUtils.uuid(filter.part),
        };
      }
      if (filter.characteristicId) {
        where = {
          ...where,
          characteristicId: SequelizeFilterUtils.uuid(filter.characteristicId),
        };
      }
      if (filter.drawingId) {
        where = {
          ...where,
          drawingId: SequelizeFilterUtils.uuid(filter.drawingId),
        };
      }
      if (filter.drawingSheetId) {
        where = {
          ...where,
          drawingSheetId: SequelizeFilterUtils.uuid(filter.drawingSheetId),
        };
      }
      if (filter.userId) {
        where = {
          ...where,
          createdById: SequelizeFilterUtils.uuid(filter.operatorId),
          updatedById: SequelizeFilterUtils.uuid(filter.operatorId),
        };
      }

      if (filter.createdAtRange) {
        const [start, end] = filter.createdAtRange;

        if (start !== undefined && start !== null && start !== '') {
          where = {
            ...where,
            createdAt: {
              ...where.createdAt,
              [Op.gte]: start,
            },
          };
        }

        if (end !== undefined && end !== null && end !== '') {
          where = {
            ...where,
            createdAt: {
              ...where.createdAt,
              [Op.lte]: end,
            },
          };
        }
      }

      if (filter.updatedAtRange) {
        const [start, end] = filter.updatedAtRange;

        if (start !== undefined && start !== null && start !== '') {
          where = {
            ...where,
            createdAt: {
              ...where.updatedAt,
              [Op.gte]: start,
            },
          };
        }

        if (end !== undefined && end !== null && end !== '') {
          where = {
            ...where,
            createdAt: {
              ...where.updatedAt,
              [Op.lte]: end,
            },
          };
        }
      }
    }

    let { rows, count } = await tenant.annotation.findAndCountAll({
      where,
      include,
      limit: limit ? Number(limit) : undefined,
      offset: offset ? Number(offset) : undefined,
      order: orderBy ? [orderBy.split('_')] : [['createdAt', 'DESC']],
      transaction: SequelizeRepository.getTransaction(options),
    });

    rows = await this._fillWithRelationsForRows(rows, options);

    return { rows, count };
  }

  /**
   * Lists the Annotations to populate the autocomplete.
   * See https://sequelize.org/v5/manual/querying.html to learn how to
   * customize the query.
   *
   * @param {string} query
   * @param {number} limit
   */
  async findAllAutocomplete(query, limit, options) {
    const currentUser = SequelizeRepository.getCurrentUser(options);

    const tenant = await models.getTenant(currentUser.accountId);

    let where = {};

    if (query) {
      where = {
        [Op.or]: [
          { id: SequelizeFilterUtils.uuid(query) },
          {
            [Op.and]: SequelizeFilterUtils.ilike('annotation', 'annotation', query),
          },
        ],
      };
    }

    const records = await tenant.annotation.findAll({
      attributes: fields,
      where,
      limit: limit ? Number(limit) : undefined,
      orderBy: [['value', 'ASC']],
    });

    return records.map((record) => ({
      id: record.id,
      annotation: record.annotation,
    }));
  }

  /**
   * Creates an audit log of the operation.
   *
   * @param {string} action - The action [create, update or delete].
   * @param {object} record - The sequelize record
   * @param {object} data - The new data passed on the request
   * @param {object} options
   */
  async _createAuditLog(action, record, data, options) {
    let values = {};

    if (data) {
      values = {
        ...record.get({ plain: true }),
      };
    }

    await AuditLogRepository.log(
      {
        entityName: 'annotation',
        entityId: record.id,
        action,
        values,
      },
      options
    );
  }

  /**
   * Fills an array of Annotation with relations.
   *
   * @param {Array} rows
   * @param {Object} [options]
   */
  async _fillWithRelationsForRows(rows, options) {
    if (!rows) {
      return rows;
    }

    return Promise.all(rows.map((record) => this._fillWithRelations(record, options)));
  }

  /**
   * Fill the Annotation with the relations.
   *
   * @param {Object} record
   * @param {Object} [options]
   */
  async _fillWithRelations(record, options) {
    if (!record) {
      return record;
    }

    const output = record.get({ plain: true });

    const transaction = SequelizeRepository.getTransaction(options);

    output.part = await record.getPart({
      transaction,
    });

    output.characteristic = await record.getCharacteristic({
      transaction,
    });

    output.drawing = await record.getDrawing({
      transaction,
    });

    output.drawingSheet = await record.getDrawingSheet({
      transaction,
    });

    output.createdBy = await record.getCreatedBy({
      transaction,
    });

    output.updatedBy = await record.getUpdatedBy({
      transaction,
    });

    return output;
  }
}

module.exports = AnnotationRepository;
