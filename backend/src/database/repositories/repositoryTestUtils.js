const AccountMemberRepository = require('./accountMemberRepository');
const UserRepository = require('./userRepository');
const models = require('../models');

module.exports.createUserWithAccountAssociation = async (params, options) => {
  let { accountMemberFixture } = params;
  let { userFixture } = params;
  accountMemberFixture = JSON.parse(JSON.stringify(accountMemberFixture));
  userFixture = JSON.parse(JSON.stringify(userFixture));
  userFixture.id = accountMemberFixture.user;
  userFixture.activeAccountMemberId = accountMemberFixture.id;

  await AccountMemberRepository.create(accountMemberFixture, options);

  let user = await UserRepository.create(userFixture, options);

  // Use models instead of repositories to create the association
  // because the repository findById methods return raw objects
  user = await models.user.findByPk(user.id);

  const accountMember = await models.accountMember.findByPk(accountMemberFixture.id);

  user.setAccountMemberships([accountMember]);

  user = await UserRepository.findById(user.id, options);

  return {
    user,
    accountMember,
  };
};
