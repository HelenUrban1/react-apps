const SequelizeRepository = require('./sequelizeRepository');
const { data, createRepoInstance } = require('../../__fixtures__').station;
const config = require('../../../config')();

describe('StationRepository', () => {
  let repository;
  let auditLogSpy;
  const options = {
    currentUser: {
      accountId: config.testAccountId,
    },
  };

  beforeEach(async () => {
    await SequelizeRepository.cleanDatabase(options.currentUser.accountId);
    repository = createRepoInstance();
    auditLogSpy = jest.spyOn(repository, '_createAuditLog');
  });

  describe('#create()', () => {
    it('should create a station', async () => {
      const fixture = data[0];
      const station = await repository.create(fixture, options);

      expect(station.name).toBe(fixture.name);
    });
  });

  describe('#update()', () => {
    it('should update a station', async () => {
      const originalStation = await repository.create(data[0], options);

      const updatedName = 'Milling';
      const updatedStation = await repository.update(originalStation.id, { name: updatedName }, options);

      expect(originalStation.id).toBe(updatedStation.id);
      expect(updatedStation.name).toBe(updatedName);
    });
  });

  describe('#destroy()', () => {
    it('should remove a station', async () => {
      const originalStation = await repository.create(data[0], options);

      await repository.destroy(originalStation.id, options);

      const foundStation = await repository.findById(originalStation.id, options);
      expect(foundStation).toBe(null);
    });
  });

  describe('#findById()', () => {
    it('should find an existing station', async () => {
      const originalStation = await repository.create(data[0], options);

      const foundStation = await repository.findById(originalStation.id, options);
      expect(originalStation).toStrictEqual(foundStation);
    });
  });

  describe('#count()', () => {
    it('should count the total number of stations', async () => {
      await repository.create(data[0], options);

      const count = await repository.count({}, options);
      expect(count).toBe(1);
    });
  });
  describe('#findAndCountAll()', () => {
    it('should find and count all the stations', async () => {
      const originalStation = await repository.create(data[0], options);

      const result = await repository.findAndCountAll({}, options);
      expect(result.count).toBe(1);
      expect(result.rows[0]).toStrictEqual(originalStation);
    });
  });

  describe('#findAllAutocomplete()', () => {
    it('should find all stations for a list', async () => {
      const originalStation = await repository.create(data[0], options);

      const result = await repository.findAllAutocomplete(null, null, options);
      expect(result[0].id).toBe(originalStation.id);
    });
  });

  afterAll(async () => {
    await SequelizeRepository.closeConnections(options.currentUser.accountId);
    await SequelizeRepository.closeConnections();
  });
});
