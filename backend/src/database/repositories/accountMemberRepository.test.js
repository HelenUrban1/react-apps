const AccountMemberRepository = require('./accountMemberRepository');
const SequelizeRepository = require('./sequelizeRepository');
const { data, USER_ID_A, ACCOUNT_ID_A } = require('../../__fixtures__').accountMember;
const usersData = require('../../__fixtures__').user.data;
const ValidationError = require('../../errors/validationError');
const constants = require('../constants').accountMember;
const { createUserWithAccountAssociation } = require('./repositoryTestUtils');
const config = require('../../../config')();

const roles = {};
constants.ROLES.forEach((role) => (roles[role] = role));

const SAMPLE_UUID = 'f44ffabd-790e-49e4-8417-95abf99a1e19'; // Generated value that is extremely unlikely to match a dynamically generated UUID value
let options = {};

describe('AccountMemberRepository', () => {
  let auditLogSpy;

  const isPaidUser = (roles) => {
    if (!roles || !roles.length) {
      return true;
    }
    return constants.PAID_ROLES.some((paidRole) => roles.includes(paidRole));
  };

  const createAllRecordsFromFixtures = async () => {
    // The accountAUserCOwner fixture is used in beforeEach() and so will
    // already have a record created from it.
    // const fixtureNames = Object.keys(data).filter((key) => key !== 'accountAUserCOwner');
    const fixtureNames = Object.keys(data).filter((key) => key.startsWith('accountA') && key !== 'accountAUserCOwner');

    await Promise.all(
      fixtureNames.map((fixtureName) => {
        return AccountMemberRepository.create(data[fixtureName], options);
      })
    );
  };

  beforeEach(async () => {
    options = {
      currentUser: {
        accountId: config.testAccountId,
      },
    };
    await SequelizeRepository.cleanDatabase(options.currentUser.accountId);
    await SequelizeRepository.cleanDatabase();
    auditLogSpy = jest.spyOn(AccountMemberRepository, '_createAuditLog');

    // Create a record beforehand because otherwise every test
    // would go through the first-member-of-account flow. 99%
    // of operations for the repository will not involve that
    // special flow, and the tests that target that flow can
    // create those conditions themselves.
    const accountMemberFixture = data.accountAUserCOwner;
    await createUserWithAccountAssociation({ accountMemberFixture, userFixture: usersData[0] }, options);
  });

  afterAll(async () => {
    await SequelizeRepository.closeConnections(options.currentUser.accountId);
    await SequelizeRepository.closeConnections();
  });

  describe('#create()', () => {
    it('should create an item', async () => {
      const accountMemberFixture = JSON.parse(JSON.stringify(data.accountAUserAPaidRoles));
      const item = await AccountMemberRepository.create(accountMemberFixture, options);

      expect(item).toBeDefined();
      expect(item.id).toBe(accountMemberFixture.id);
      expect(auditLogSpy).toHaveBeenCalled();
    });

    it('should throw an error if the userId is not provided', async () => {
      const fixture = { ...data.accountAUserAPaidRoles };
      delete fixture.user;
      await expect(AccountMemberRepository.create(fixture, options)).rejects.toThrow(ValidationError);
    });

    it('should throw an error if the accountId is not provided', async () => {
      const fixture = { ...data.accountAUserAPaidRoles };
      delete fixture.account;
      await expect(AccountMemberRepository.create(fixture, options)).rejects.toThrow(ValidationError);
    });

    it('should throw an error instead of creating a record with duplicate User and Account associations', async () => {
      const accountMemberFixture = JSON.parse(JSON.stringify(data.accountAUserAPaidRoles));
      const item = await AccountMemberRepository.create(accountMemberFixture, options);

      expect(item).toBeDefined();
      expect(item.id).toBe(accountMemberFixture.id);
      expect(auditLogSpy).toHaveBeenCalled();

      const duplicateFixture = JSON.parse(JSON.stringify(data.accountAUserAPaidRoles));
      duplicateFixture.id = '0532555a-92a8-4db8-bcdf-cd0c1cd31f0b';
      await expect(AccountMemberRepository.create(duplicateFixture, options)).rejects.toThrow(ValidationError);
    });

    it('should assign the Owner role to the first member of an Account', async () => {
      const accountMemberFixture = data.emptyRoles;
      expect(accountMemberFixture.roles.includes(roles.Owner)).toBe(false);

      const { user } = await createUserWithAccountAssociation({ accountMemberFixture, userFixture: usersData[1] }, options);

      expect(user.roles.includes(roles.Owner)).toBe(true);
      expect(auditLogSpy).toHaveBeenCalled();
    });
  });

  describe('#update()', () => {
    it('should update an item', async () => {
      const originalItem = await AccountMemberRepository.create(data.accountAUserAPaidRoles, options);
      expect(originalItem.status).toBe('Active');

      const updatedStatus = 'Suspended';
      const updatedItem = await AccountMemberRepository.update(originalItem.id, { status: updatedStatus }, options);

      expect(originalItem.id).toBe(updatedItem.id);
      expect(updatedItem.status).toBe(updatedStatus);
      expect(auditLogSpy).toHaveBeenCalled();
    });

    it('should throw an error instead of updating so that a record exists with duplicate User and Account associations', async () => {
      const fixture = data.accountAUserAPaidRoles;
      await AccountMemberRepository.create(fixture, options);
      const secondFixture = data.accountAUserBUnpaidRoles;
      await AccountMemberRepository.create(secondFixture, options);

      const updateData = {
        account: fixture.account,
        user: fixture.user,
      };

      await expect(AccountMemberRepository.update(secondFixture.id, updateData, options)).rejects.toThrow(ValidationError);
    });

    it('should update `paid` when appropriate when updating `role`', async () => {
      const originalItem = await AccountMemberRepository.create(data.accountAUserAPaidRoles, options);
      expect(originalItem.paid).toBe(true);

      const updatedRoles = [constants.UNPAID_ROLES[0]];
      const updatedItem = await AccountMemberRepository.update(originalItem.id, { roles: updatedRoles }, options);

      expect(originalItem.id).toBe(updatedItem.id);
      expect(updatedItem.paid).toBe(false);
      expect(auditLogSpy).toHaveBeenCalled();
    });
  });

  describe('#destroy()', () => {
    it('should destroy an item', async () => {
      await createAllRecordsFromFixtures();
      const originalCount = await AccountMemberRepository.count({}, options);
      expect(originalCount).toBeGreaterThan(0);

      await AccountMemberRepository.destroy(data.accountAUserAPaidRoles.id, options);

      const newCount = await AccountMemberRepository.count({}, options);
      expect(newCount).not.toBe(originalCount);
      expect(newCount < originalCount).toBe(true);
      expect(auditLogSpy).toHaveBeenCalled();
    });
  });

  describe('#findById()', () => {
    it('should return null when a matching item does not exist', async () => {
      const item = await AccountMemberRepository.findById(SAMPLE_UUID, options);
      expect(item).toBe(null);
    });

    it('should find an item that exists', async () => {
      await createAllRecordsFromFixtures();
      const item = await AccountMemberRepository.findById(data.accountAUserAPaidRoles.id, options);
      expect(item.id).toBe(Object.values(data)[0].id);
    });
  });

  describe('#findByUserAndAccount()', () => {
    it('should return null when a matching item does not exist', async () => {
      const item = await AccountMemberRepository.findByUserAndAccount(SAMPLE_UUID, SAMPLE_UUID, options);
      expect(item).toBe(null);
    });

    it('should find an item that exists', async () => {
      await createAllRecordsFromFixtures();

      const fixture = data.accountAUserAPaidRoles;

      const item = await AccountMemberRepository.findByUserAndAccount(fixture.user, fixture.account, options);
      expect(item.id).toBe(fixture.id);
    });
  });

  describe('#findByUser()', () => {
    it('should return an empty array when matching items do not exist', async () => {
      const item = await AccountMemberRepository.findByUser(SAMPLE_UUID, options);
      expect(item).toEqual([]);
    });

    it('should find all items that exist', async () => {
      await createAllRecordsFromFixtures();

      const userId = USER_ID_A;
      const expectedCount = Object.values(data).filter((item) => {
        return item.user === userId && item.account == ACCOUNT_ID_A;
      }).length;

      const results = await AccountMemberRepository.findByUser(userId, options);
      expect(results.length).toBe(expectedCount);
    });
  });

  describe('#addRoles()', () => {
    it('should return null when a matching item does not exist', async () => {
      const item = await AccountMemberRepository.addRoles(SAMPLE_UUID, options);
      expect(item).toBe(null);
    });

    it('should not allow adding the Owner role unless explicitly specifying intent to do so', async () => {
      const fixture = data.accountAUserAPaidRoles;
      await AccountMemberRepository.create(fixture, options);

      const newRoles = [roles.Owner];

      await expect(AccountMemberRepository.addRoles(fixture.id, newRoles, options)).rejects.toThrow(ValidationError);
    });

    it('should allow adding the Owner role when explicitly specifying intent to do so', async () => {
      const fixture = data.accountAUserAPaidRoles;
      await AccountMemberRepository.create(fixture, options);

      const newRoles = [roles.Owner];
      options.allowOwnerAddition = true;

      const result = await AccountMemberRepository.addRoles(fixture.id, newRoles, options);
      expect(result.roles.includes(roles.Owner)).toBe(true);
    });

    it('should add roles that are not already present', async () => {
      const fixture = data.accountAUserAPaidRoles;
      await AccountMemberRepository.create(fixture, options);

      options.accountId = fixture.account;
      const newRoles = [roles.Collaborator];
      expect(fixture.roles.includes(newRoles[0])).toBe(false);

      const item = await AccountMemberRepository.addRoles(fixture.id, newRoles, options);
      expect(item.id).toBe(fixture.id);
      expect(item.roles.includes(newRoles[0])).toBe(true);
    });

    it('should not add roles that are already present', async () => {
      const fixture = data.accountAUserAPaidRoles;
      await AccountMemberRepository.create(fixture, options);

      options.accountId = fixture.account;
      const newRoles = [roles.Admin, roles.Billing];
      expect(fixture.roles.includes(newRoles[0])).toBe(true);
      expect(fixture.roles.includes(newRoles[1])).toBe(false);

      const item = await AccountMemberRepository.addRoles(fixture.id, newRoles, options);
      expect(item.id).toBe(fixture.id);
      expect(item.roles.includes(newRoles[0])).toBe(true);
      expect(item.roles.includes(newRoles[1])).toBe(true);
      let existingRoleCount = 0;
      item.roles.forEach((role) => {
        if (role === newRoles[0]) {
          existingRoleCount++;
        }
      });
      expect(existingRoleCount).toBe(1);
    });
  });

  describe('#removeRoles()', () => {
    it('should return null when a matching item does not exist', async () => {
      const item = await AccountMemberRepository.removeRoles(SAMPLE_UUID, options);
      expect(item).toBe(null);
    });

    it('should not allow removing the Owner role unless explicitly specifying intent to do so', async () => {
      const fixture = data.accountAUserCOwner;
      const rolesToRemove = [roles.Owner];

      await expect(AccountMemberRepository.removeRoles(fixture.id, rolesToRemove, options)).rejects.toThrow(ValidationError);
    });

    it('should allow removing the Owner role when explicitly specifying intent to do so', async () => {
      const fixture = data.accountAUserCOwner;
      const rolesToRemove = [roles.Owner];
      options.allowOwnerRemoval = true;

      const result = await AccountMemberRepository.removeRoles(fixture.id, rolesToRemove, options);
      expect(result.roles.includes(roles.Owner)).toBe(false);
    });

    it('should remove roles', async () => {
      const fixture = data.accountAUserDPaidAndUnpaidRoles;
      await AccountMemberRepository.create(fixture, options);

      options.accountId = fixture.account;
      const rolesToRemove = [roles.Billing];
      expect(fixture.roles.includes(rolesToRemove[0])).toBe(true);

      const item = await AccountMemberRepository.removeRoles(fixture.id, rolesToRemove, options);
      expect(item.id).toBe(fixture.id);
      expect(item.roles.includes(rolesToRemove[0])).toBe(false);
    });

    it('should result in `paid` state changing from true to false when removing all paid roles', async () => {
      const fixture = data.accountAUserDPaidAndUnpaidRoles;
      await AccountMemberRepository.create(fixture, options);

      options.accountId = fixture.account;
      const rolesToRemove = [roles.Admin];
      expect(fixture.roles.includes(rolesToRemove[0])).toBe(true);

      let item = await AccountMemberRepository.findById(fixture.id, options);
      expect(item.paid).toBe(true);

      item = await AccountMemberRepository.removeRoles(fixture.id, rolesToRemove, options);
      expect(item.id).toBe(fixture.id);
      expect(item.roles.includes(rolesToRemove[0])).toBe(false);
      expect(item.paid).toBe(false);
    });
  });

  describe('#resetRoles()', () => {
    it('should return null when a matching item does not exist', async () => {
      const item = await AccountMemberRepository.resetRoles(SAMPLE_UUID, options);
      expect(item).toBe(null);
    });

    it('should not allow setting the Owner role unless explicitly specifying intent to do so', async () => {
      const fixture = data.accountAUserAPaidRoles;
      await AccountMemberRepository.create(fixture, options);

      const newRoles = [roles.Owner];

      await expect(AccountMemberRepository.resetRoles(fixture.id, newRoles, options)).rejects.toThrow(ValidationError);
    });

    it('should allow setting the Owner role when explicitly specifying intent to do so', async () => {
      const fixture = data.accountAUserAPaidRoles;
      await AccountMemberRepository.create(fixture, options);

      const newRoles = [roles.Owner];
      options.allowOwnerSet = true;

      const result = await AccountMemberRepository.resetRoles(fixture.id, newRoles, options);
      expect(result.roles.includes(roles.Owner)).toBe(true);
    });

    it('should reset roles to match a populated set of roles', async () => {
      const fixture = data.accountAUserDPaidAndUnpaidRoles;
      await AccountMemberRepository.create(fixture, options);

      options.accountId = fixture.account;
      const newRoles = [roles.Viewer];
      expect(fixture.roles.includes(newRoles[0])).toBe(false);

      let item = await AccountMemberRepository.findById(fixture.id, options);
      expect(item.paid).toBe(true);

      item = await AccountMemberRepository.resetRoles(fixture.id, newRoles, options);
      expect(item.id).toBe(fixture.id);
      expect(item.roles.includes(newRoles[0])).toBe(true);
      expect(item.roles.length).toBe(1);
      expect(item.paid).toBe(false);
    });

    it('should reset roles to an empty array if provided', async () => {
      const fixture = data.accountAUserDPaidAndUnpaidRoles;
      await AccountMemberRepository.create(fixture, options);

      options.accountId = fixture.account;
      const newRoles = [];
      expect(fixture.roles.length > 0).toBe(true);

      let item = await AccountMemberRepository.findById(fixture.id, options);
      expect(item.paid).toBe(true);

      item = await AccountMemberRepository.resetRoles(fixture.id, newRoles, options);
      expect(item.id).toBe(fixture.id);
      expect(item.roles.length).toBe(0);
      expect(item.paid).toBe(true);
    });
  });

  describe('#changeAccountOwnerFromCurrentUser()', () => {
    it('should throw a ValidationError if the current Account Owner User record does not have any AccountMember records', async () => {
      await expect(AccountMemberRepository.changeAccountOwnerFromCurrentUser({ options })).rejects.toThrow(ValidationError);
    });

    it('should throw a ValidationError if the user initiating the change is not the Account Owner', async () => {
      const originalOwnerFixture = JSON.parse(JSON.stringify(data.accountBUserBOwner));
      const userFixture = JSON.parse(JSON.stringify(usersData[1]));
      userFixture.id = originalOwnerFixture.user;
      userFixture.activeAccountMemberId = originalOwnerFixture.id;

      const { user } = await createUserWithAccountAssociation(
        {
          accountMemberFixture: originalOwnerFixture,
          userFixture,
        },
        options
      );
      const originalOwner = user;

      await AccountMemberRepository.removeRoles(originalOwner.activeAccountMemberId, [roles.Owner], { allowOwnerRemoval: true });

      const newOwnerFixture = data.accountBUserCPaidAndUnpaidRoles;
      const newOwner = await AccountMemberRepository.create(newOwnerFixture, options);

      options.currentUser = originalOwner;

      const params = {
        newOwnerAccountMemberId: newOwner.id,
        options,
      };

      await expect(AccountMemberRepository.changeAccountOwnerFromCurrentUser(params)).rejects.toThrow(ValidationError);
    });

    // it('should throw a ValidationError if the user to be made the new Account Owner is not an Admin', async () => {
    //   const originalOwnerFixture = JSON.parse(JSON.stringify(data.accountBUserBOwner));
    //   const userFixture = JSON.parse(JSON.stringify(usersData[1]));
    //   userFixture.id = originalOwnerFixture.user;
    //   userFixture.activeAccountMemberId = originalOwnerFixture.id;
    //   userFixture.roles = [roles.Owner];

    //   const { user } = await createUserWithAccountAssociation(
    //     {
    //       accountMemberFixture: originalOwnerFixture,
    //       userFixture,
    //     },
    //     options
    //   );
    //   const originalOwner = user;

    //   const newOwnerFixture = data.accountAUserBUnpaidRoles; // Triggers the exception because this record has no Admin role
    //   const newOwner = await AccountMemberRepository.create(newOwnerFixture, options);

    //   options.currentUser = originalOwner;

    //   const params = {
    //     newOwnerAccountMemberId: newOwner.id,
    //     options,
    //   };

    //   await expect(AccountMemberRepository.changeAccountOwnerFromCurrentUser(params)).rejects.toThrow(ValidationError);
    // });

    it('should throw a ValidationError if the new Account Owner is associated with a different account than the current Account Owner', async () => {
      const originalOwnerFixture = JSON.parse(JSON.stringify(data.accountBUserBOwner));
      const userFixture = JSON.parse(JSON.stringify(usersData[1]));
      userFixture.id = originalOwnerFixture.user;
      userFixture.activeAccountMemberId = originalOwnerFixture.id;
      userFixture.roles = [roles.Owner];

      const { user } = await createUserWithAccountAssociation(
        {
          accountMemberFixture: originalOwnerFixture,
          userFixture,
        },
        options
      );
      const originalOwner = user;

      const newOwnerFixture = data.accountAUserAPaidRoles;
      const newOwner = await AccountMemberRepository.create(newOwnerFixture, options);

      options.currentUser = originalOwner;

      const params = {
        newOwnerAccountMemberId: newOwner.id,
        options,
      };

      await expect(AccountMemberRepository.changeAccountOwnerFromCurrentUser(params)).rejects.toThrow(ValidationError);
    });

    it('should update the Account Owner when all preconditions are met', async () => {
      const originalOwnerAccountMemberFixture = JSON.parse(JSON.stringify(data.accountBUserBOwner));
      const originalOwnerUserFixture = JSON.parse(JSON.stringify(usersData[1]));
      originalOwnerUserFixture.id = originalOwnerAccountMemberFixture.user;
      originalOwnerUserFixture.activeAccountMemberId = originalOwnerAccountMemberFixture.id;
      originalOwnerUserFixture.roles = [roles.Owner];
      const { user } = await createUserWithAccountAssociation(
        {
          accountMemberFixture: originalOwnerAccountMemberFixture,
          userFixture: originalOwnerUserFixture,
        },
        options
      );

      const originalOwner = user;

      const newAccountMemberFixture = data.accountBUserAUnpaidRoles;
      newAccountMemberFixture.roles = [roles.Admin];
      const newOwnerUserFixture = JSON.parse(JSON.stringify(usersData[2]));
      newOwnerUserFixture.roles = [roles.Admin];
      newOwnerUserFixture.activeAccountMemberId = newAccountMemberFixture.id;
      const { user: newOwner } = await createUserWithAccountAssociation({ accountMemberFixture: newAccountMemberFixture, userFixture: newOwnerUserFixture }, options);

      expect(newOwner.roles.includes(roles.Owner)).toBe(false);

      options.currentUser = originalOwner;
      options.targetUser = newOwner;

      const newAccountOwnerRecord = await AccountMemberRepository.changeAccountOwnerFromCurrentUser(options.targetUser, options.currentUser, options);
      expect(newAccountOwnerRecord.roles.includes(roles.Owner)).toBe(true);

      const originalOwnerRecord = await AccountMemberRepository.findByUserAndAccount(originalOwner.id, originalOwnerAccountMemberFixture.account, options);
      expect(originalOwnerRecord.roles.includes(roles.Owner)).toBe(false);
    });
  });

  describe('#count()', () => {
    it('should return the current item count when no items exist', async () => {
      const count = await AccountMemberRepository.count({}, options);
      expect(count).toBe(1); // 1 because of the record created in beforeEach()
    });

    it('should return the current item count when items exist', async () => {
      await createAllRecordsFromFixtures();
      const count = await AccountMemberRepository.count({}, options);
      const expectedCount = Object.values(data).filter((item) => item.account === ACCOUNT_ID_A).length;
      expect(count).toBe(expectedCount);
    });
  });

  describe('#findAndCountAll()', () => {
    const items = Object.values(data).filter((item) => item.account === ACCOUNT_ID_A);
    const activeRecrodsCount = items.filter((item) => item.status === 'Active').length;
    const paidRecordsCount = items.filter((item) => isPaidUser(item.roles)).length;
    const unpaidRecordCount = items.filter((item) => !isPaidUser(item.roles)).length;

    beforeEach(async () => {
      await createAllRecordsFromFixtures();
    });

    it('should honor a simple filter', async () => {
      const filter = { status: 'Active' };
      const results = await AccountMemberRepository.findAndCountAll({ filter }, options);

      expect(results.count).toBe(activeRecrodsCount);
    });

    it('should filter on `paid` status of true', async () => {
      const filter = { paid: true, PAID_ROLES: constants.PAID_ROLES };
      const results = await AccountMemberRepository.findAndCountAll({ filter }, options);

      expect(results.count).toBe(paidRecordsCount);
      expect(results.rows.every((row) => row.paid === true)).toBe(true);
    });

    it('should filter on `paid` status of false', async () => {
      const filter = { paid: false, PAID_ROLES: constants.PAID_ROLES };
      const results = await AccountMemberRepository.findAndCountAll({ filter }, options);

      expect(results.count).toBe(unpaidRecordCount);
      expect(results.rows.every((row) => row.paid === false)).toBe(true);
    });

    it('should filter for a single role', async () => {
      const filter = { roles: [roles.Admin] };
      const results = await AccountMemberRepository.findAndCountAll({ filter }, options);

      const expectedCount = Object.values(data).filter((item) => item.roles && item.roles.includes(filter.roles[0])).length - 1;

      expect(results.count).toBe(expectedCount);
    });

    it('should filter for roles that contain a given set', async () => {
      const filter = { roles: [roles.Admin, roles.Billing] };
      const results = await AccountMemberRepository.findAndCountAll({ filter }, options);

      const expectedCount = Object.values(data).filter((item) => item.roles && (item.roles.includes(filter.roles[0]) || item.roles.includes(filter.roles[1]))).length - 1;

      expect(results.count).toBe(expectedCount);
    });
  });

  describe('#findAllAutocomplete()', () => {
    it('should honor a simple query', async () => {
      await createAllRecordsFromFixtures();

      const results = await AccountMemberRepository.findAllAutocomplete(data.accountAUserAPaidRoles.id);

      expect(results.length).toBe(1);
      expect(results[0].id).toBe(data.accountAUserAPaidRoles.id);
    });
  });
});
