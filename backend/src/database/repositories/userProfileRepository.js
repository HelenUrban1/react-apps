const Sequelize = require('sequelize');
const SequelizeRepository = require('./sequelizeRepository');
const models = require('../models');
const AuditLogRepository = require('./auditLogRepository');
const SequelizeFilterUtils = require('../utils/sequelizeFilterUtils');
const { log } = require('../../logger');

const { Op } = Sequelize;

/**
 * Handles database operations for Users.
 * See https://sequelize.org/v5/index.html to learn how to customize it.
 */
module.exports = class UserProfileRepository {
  /**
   * Creates a user profile.
   */
  static async create(userId, data, options) {
    const currentUser = SequelizeRepository.getCurrentUser(options);

    const transaction = SequelizeRepository.getTransaction(options);

    const tenant = await models.getTenant(currentUser.accountId);

    const userProfile = await tenant.userProfile.create(
      {
        firstName: data.firstName || null,
        lastName: data.lastName || null,
        phoneNumber: data.phoneNumber || null,
        importHash: data.importHash || null,
        roles: data.roles || null,
        userId,
        createdById: currentUser.id,
        updatedById: currentUser.id,
      },
      { transaction }
    );
    await AuditLogRepository.log(
      {
        entityName: 'userProfile',
        entityId: userProfile.id,
        action: AuditLogRepository.CREATE,
        values: {
          ...userProfile.get({ plain: true }),
        },
      },
      options
    );

    return this.findById(userProfile.id, options);
  }

  /**
   * Updates the profile of the user.
   *
   * @param {*} id
   * @param {*} data
   * @param {*} [options]
   */
  static async updateProfile(id, data, options) {
    const currentUser = SequelizeRepository.getCurrentUser(options);

    const userProfile = await models.userProfile.findByPk(id);

    if (userProfile) {
      await userProfile.update({
        firstName: data.firstName || null,
        lastName: data.lastName || null,
        phoneNumber: data.phoneNumber || null,
        roles: data.roles || null,
        updatedById: currentUser.id,
      });

      await AuditLogRepository.log(
        {
          entityName: 'userProfile',
          entityId: userProfile.id,
          action: AuditLogRepository.UPDATE,
          values: {
            ...userProfile.get({ plain: true }),
          },
        },
        options
      );

      return this.findById(userProfile.id, options);
    }

    return null;
  }

  /**
   * Updates the roles of the user profile.
   *
   * @param {*} id
   * @param {*} roles
   * @param {*} [options]
   */
  static async updateRoles(id, roles, options) {
    const currentUser = SequelizeRepository.getCurrentUser(options);

    const transaction = SequelizeRepository.getTransaction(options);

    const userProfile = await models.userProfile.findByPk(id, {
      transaction,
    });

    if (userProfile) {
      await userProfile.update(
        {
          roles: roles || null,
          updatedById: currentUser.id,
        },
        { transaction }
      );

      await AuditLogRepository.log(
        {
          entityName: 'userProfile',
          entityId: userProfile.id,
          action: AuditLogRepository.UPDATE,
          values: {
            roles,
          },
        },
        options
      );

      return this.findById(userProfile.id, options);
    }

    return null;
  }

  /**
   * Updates a User.
   *
   * @param {*} id
   * @param {*} data
   * @param {*} [options]
   */
  static async update(userId, data, options) {
    const currentUser = SequelizeRepository.getCurrentUser(options);

    const tenant = await models.getTenant(currentUser.accountId);
    const userProfile = await tenant.userProfile.findOne({ where: { userId } });

    if (userProfile && data) {
      await userProfile.update({
        firstName: data.firstName || userProfile.firstName,
        lastName: data.lastName || userProfile.lastName,
        phoneNumber: data.phoneNumber || userProfile.phoneNumber,
        roles: data.roles || userProfile.roles,
        updatedById: currentUser.id,
      });

      await AuditLogRepository.log(
        {
          entityName: 'userProfile',
          entityId: userProfile.id,
          action: AuditLogRepository.UPDATE,
          values: {
            ...userProfile.get({ plain: true }),
          },
        },
        options
      );

      return this.findById(userProfile.id, options);
    }

    return null;
  }

  /**
   * Deletes the UserProfile.
   *
   * @param {string} id
   * @param {Object} [options]
   */
  static async destroy(id, options) {
    const transaction = SequelizeRepository.getTransaction(options);
    const currentUser = SequelizeRepository.getCurrentUser(options);
    const tenant = await models.getTenant(currentUser.accountId);

    const record = await tenant.userProfile.findByPk(id, {
      transaction,
    });

    await record.destroy({
      transaction,
    });

    await this._createAuditLog(AuditLogRepository.DELETE, record, null, options);
  }

  /**
   * Finds the user based on the query.
   *
   * @param {Object} query
   * @param {Object} query.filter
   * @param {number} query.limit
   * @param  {number} query.offset
   * @param  {string} query.orderBy
   *
   * @returns {Promise<Object>} response - Object containing the rows and the count.
   */
  static async findAndCountAll(
    { filter, limit, offset, orderBy } = {
      filter: null,
      limit: 0,
      offset: 0,
      orderBy: null,
    },
    options
  ) {
    const currentUser = SequelizeRepository.getCurrentUser(options);

    const transaction = SequelizeRepository.getTransaction(options);

    const tenant = await models.getTenant(currentUser.accountId);

    let where = {};
    const include = [];

    if (filter) {
      if (filter.id) {
        where = {
          ...where,
          id: SequelizeFilterUtils.uuid(filter.id),
        };
      }

      if (filter.fullName) {
        where = {
          ...where,
          [Op.and]: SequelizeFilterUtils.ilike('user', 'fullName', filter.fullName),
        };
      }

      if (filter.createdAtRange) {
        const [start, end] = filter.createdAtRange;

        if (start !== undefined && start !== null && start !== '') {
          where = {
            ...where,
            createdAt: {
              ...where.createdAt,
              [Op.gte]: start,
            },
          };
        }

        if (end !== undefined && end !== null && end !== '') {
          where = {
            ...where,
            createdAt: {
              ...where.createdAt,
              [Op.lte]: end,
            },
          };
        }
      }
    }

    let { rows, count } = await tenant.userProfile.findAndCountAll({
      where,
      include,
      limit: limit ? Number(limit) : undefined,
      offset: offset ? Number(offset) : undefined,
      order: orderBy ? [orderBy.split('_')] : [['createdAt', 'DESC']],
      transaction,
    });

    rows = await this._fillWithRelationsAndFilesForRows(rows, options);

    return { rows, count };
  }

  /**
   * Lists the users to populate the autocomplete.
   *
   * @param {Object} query
   * @param {number} limit
   */
  static async findAllAutocomplete(query, limit) {
    const currentUser = SequelizeRepository.getCurrentUser(options);

    const tenant = await models.getTenant(currentUser.accountId);

    let where = {};

    if (query) {
      where = {
        [Op.or]: [{ id: SequelizeFilterUtils.uuid(query) }, SequelizeFilterUtils.ilike('userProfile', 'fullName', query)],
      };
    }

    const userProfiles = await tenant.userProfile.findAll({
      attributes: ['id', 'fullName'],
      where,
      limit: limit ? Number(limit) : undefined,
      orderBy: [['fullName', 'ASC']],
    });

    const buildText = (userProfile) => {
      return `${userProfile.fullName}`;
    };

    return userProfiles.map((userProfile) => ({
      id: userProfile.id,
      label: buildText(userProfile),
    }));
  }

  /**
   * Finds the user profile and all its relations.
   *
   * @param {string} id
   * @param {Object} [options]
   */
  static async findById(id, options) {
    const currentUser = SequelizeRepository.getCurrentUser(options);

    const transaction = SequelizeRepository.getTransaction(options);

    const tenant = await models.getTenant(currentUser.accountId);

    const record = await tenant.userProfile.findByPk(id, {
      transaction,
    });

    return this._fillWithRelationsAndFiles(record, options);
  }

  /**
   * Finds the user profile by user id and all its relations.
   *
   * @param {string} userId
   * @param {Object} [options]
   */
  static async findByUserId(userId, options) {
    const currentUser = SequelizeRepository.getCurrentUser(options);
    const transaction = SequelizeRepository.getTransaction(options);

    if (!currentUser || !currentUser.accountId) {
      log.warn(`Failed to find user profile, accountId: ${currentUser.accountId}`);
      return;
    }
    const tenant = await models.getTenant(currentUser.accountId);

    const record = await tenant.userProfile.findOne({ where: { userId } });

    return this._fillWithRelationsAndFiles(record, options);
  }

  /**
   * Counts the users based on the filter.
   *
   * @param {*} [filter]
   * @param {*} [options]
   */
  static async count(filter, options) {
    const currentUser = SequelizeRepository.getCurrentUser(options);

    const transaction = SequelizeRepository.getTransaction(options);

    const tenant = await models.getTenant(currentUser.accountId);

    return tenant.user.count(
      {
        where: filter,
      },
      { transaction }
    );
  }

  /**
   * Fills the users with the relations and files.
   *
   * @param {*} rows
   * @param {*} [options]
   */
  static async _fillWithRelationsAndFilesForRows(rows, options) {
    if (!rows) {
      return rows;
    }

    return Promise.all(rows.map((record) => this._fillWithRelationsAndFiles(record, options)));
  }

  /**
   * Fills a user profile with the relations and files.
   *
   * @param {*} record
   * @param {*} [options]
   */
  static async _fillWithRelationsAndFiles(record, options) {
    if (!record) {
      return record;
    }

    const output = record.get({ plain: true });

    return output;
  }

  /**
   * Creates an audit log of the operation.
   *
   * @param {string} action - The action [create, update or delete].
   * @param {object} record - The sequelize record
   * @param {object} data - The new data passed on the request
   * @param {object} options
   */
  static async _createAuditLog(action, record, data, options) {
    let values = {};

    if (data) {
      values = {
        ...record.get({ plain: true }),
      };
    }

    await AuditLogRepository.log(
      {
        entityName: 'userProfile',
        entityId: record.id,
        action,
        values,
      },
      options
    );
  }
};
