const models = require('../models');
const SequelizeRepository = require('./sequelizeRepository');
const AuditLogRepository = require('./auditLogRepository');
const FileRepository = require('./fileRepository');
const pick = require('lodash/pick');
const SequelizeFilterUtils = require('../utils/sequelizeFilterUtils');

const { Sequelize } = models;
const { Op } = Sequelize;

/**
 * Handles database operations for the ReportTemplate.
 * See https://sequelize.org/v5/index.html to learn how to customize it.
 */
class ReportTemplateRepository {
  /**
   * Creates the ReportTemplate.
   *
   * @param {Object} data
   * @param {Object} [options]
   */
  async create(data, options) {
    const currentUser = SequelizeRepository.getCurrentUser(options);

    const tenant = await models.getTenant(currentUser.accountId);

    const transaction = SequelizeRepository.getTransaction(
      options,
    );

    const record = await tenant.reportTemplate.create(
      {
        ...pick(data, ['id', 'title', 'type', 'status', 'direction', 'settings', 'file', 'sortIndex', 'filters', 'createdAt', 'updatedAt']),
        siteId: currentUser.siteId,
        createdById: currentUser.id,
        updatedById: currentUser.id,
      },
      {
        transaction,
      }
    );

    await record.setProvider(data.provider || null, {
      transaction,
    });
    await FileRepository.replaceRelationFiles(
      {
        belongsTo: models.reportTemplate.getTableName(),
        belongsToColumn: 'file',
        belongsToId: record.id,
      },
      data.file,
      options
    );

    await this._createAuditLog(AuditLogRepository.CREATE, record, data, options);

    return this.findById(record.id, options);
  }

  /**
   * Updates the ReportTemplate.
   *
   * @param {Object} data
   * @param {Object} [options]
   */
  async update(id, data, options) {
    const currentUser = SequelizeRepository.getCurrentUser(options);

    const tenant = await models.getTenant(currentUser.accountId);

    const transaction = SequelizeRepository.getTransaction(
      options,
    );

    let record = await tenant.reportTemplate.findByPk(id, {
      transaction,
    });

    record = await record.update(
      {
        ...pick(data, ['id', 'title', 'type', 'status', 'direction', 'settings', 'file', 'sortIndex', 'filters', 'createdAt', 'updatedAt']),
        siteId: currentUser.siteId,
        updatedById: currentUser.id,
      },
      {
        transaction,
      }
    );

    await record.setProvider(data.provider || null, {
      transaction,
    });
    await FileRepository.replaceRelationFiles(
      {
        belongsTo: models.reportTemplate.getTableName(),
        belongsToColumn: 'file',
        belongsToId: record.id,
      },
      data.file,
      options
    );

    await this._createAuditLog(AuditLogRepository.UPDATE, record, data, options);

    return this.findById(record.id, options);
  }

  /**
   * Deletes the ReportTemplate.
   *
   * @param {string} id
   * @param {Object} [options]
   */
  async destroy(id, options) {
    const currentUser = SequelizeRepository.getCurrentUser(
      options,
    );

    const tenant = await models.getTenant(currentUser.accountId);

    const transaction = SequelizeRepository.getTransaction(
      options,
    );

    const record = await tenant.reportTemplate.findByPk(id, {
      transaction,
    });

    await record.destroy({
      transaction,
    });

    await this._createAuditLog(AuditLogRepository.DELETE, record, null, options);
  }

  /**
   * Finds the ReportTemplate and its relations.
   *
   * @param {string} id
   * @param {Object} [options]
   */
  async findById(id, options) {    
    const currentUser = SequelizeRepository.getCurrentUser(
      options,
    );

    const tenant = await models.getTenant(currentUser.accountId);

    const transaction = SequelizeRepository.getTransaction(
      options,
    );

    const include = [
      {
        model: models.organization,
        as: 'provider',
      },
    ];

    const record = await tenant.reportTemplate.findByPk(id, {
      include,
      transaction,
    });

    return this._fillWithRelationsAndFiles(record, options);
  }

  /**
   * Counts the number of ReportTemplates based on the filter.
   *
   * @param {Object} filter
   * @param {Object} [options]
   */
  async count(filter, options) {
    const currentUser = SequelizeRepository.getCurrentUser(
      options,
    );

    const tenant = await models.getTenant(currentUser.accountId);
    
    const transaction = SequelizeRepository.getTransaction(
      options,
    );

    return tenant.reportTemplate.count(
      {
        where: filter,
      },
      {
        transaction,
      }
    );
  }

  /**
   * Finds the ReportTemplates based on the query.
   * See https://sequelize.org/v5/manual/querying.html to learn how to
   * customize the query.
   *
   * @param {Object} query
   * @param {Object} query.filter
   * @param {number} query.limit
   * @param  {number} query.offset
   * @param  {string} query.orderBy
   * @param {Object} [options]
   *
   * @returns {Promise<Object>} response - Object containing the rows and the count.
   */
  async findAndCountAll(
    { filter, limit, offset, orderBy } = {
      filter: null,
      limit: 0,
      offset: 0,
      orderBy: null,
    },
    options
  ) {
    const currentUser = SequelizeRepository.getCurrentUser(
      options,
    );

    const tenant = await models.getTenant(currentUser.accountId);

    let where = {};
    const include = [
      {
        model: models.organization,
        as: 'provider',
      },
    ];

    if (filter) {
      if (filter.id) {
        where = {
          ...where,
          id: SequelizeFilterUtils.uuid(filter.id),
        };
      }

      if (filter.site) {
        where = {
          ...where,
          siteId: SequelizeFilterUtils.uuid(filter.site),
        };
      }

      if (filter.providerId) {
        where = {
          ...where,
          providerId: filter.providerId,
        };
      }

      if (filter.type) {
        where = {
          ...where,
          type: filter.type,
        };
      }

      if (filter.title) {
        where = {
          ...where,
          [Op.and]: SequelizeFilterUtils.ilike('reportTemplate', 'title', filter.title),
        };
      }

      if (filter.status) {
        where = {
          ...where,
          status: filter.status,
        };
      }

      if (filter.sortIndexRange) {
        const [start, end] = filter.sortIndexRange;

        if (start !== undefined && start !== null && start !== '') {
          where = {
            ...where,
            sortIndex: {
              ...where.sortIndex,
              [Op.gte]: start,
            },
          };
        }

        if (end !== undefined && end !== null && end !== '') {
          where = {
            ...where,
            sortIndex: {
              ...where.sortIndex,
              [Op.lte]: end,
            },
          };
        }
      }

      if (filter.createdAtRange) {
        const [start, end] = filter.createdAtRange;

        if (start !== undefined && start !== null && start !== '') {
          where = {
            ...where,
            createdAt: {
              ...where.createdAt,
              [Op.gte]: start,
            },
          };
        }

        if (end !== undefined && end !== null && end !== '') {
          where = {
            ...where,
            createdAt: {
              ...where.createdAt,
              [Op.lte]: end,
            },
          };
        }
      }
    }

    let { rows, count } = await tenant.reportTemplate.findAndCountAll({
      where,
      include,
      limit: limit ? Number(limit) : undefined,
      offset: offset ? Number(offset) : undefined,
      order: orderBy ? [orderBy.split('_')] : [['createdAt', 'DESC']],
      transaction: SequelizeRepository.getTransaction(options),
    });

    rows = await this._fillWithRelationsAndFilesForRows(rows, options);

    return { rows, count };
  }

  /**
   * Lists the ReportTemplates to populate the autocomplete.
   * See https://sequelize.org/v5/manual/querying.html to learn how to
   * customize the query.
   *
   * @param {string} query
   * @param {number} limit
   */
  async findAllAutocomplete(query, limit, options) {
    const currentUser = SequelizeRepository.getCurrentUser(
      options,
    );

    const tenant = await models.getTenant(currentUser.accountId);

    let where = {};

    if (query) {
      where = {
        [Op.or]: [
          { id: SequelizeFilterUtils.uuid(query) },
          {
            [Op.and]: SequelizeFilterUtils.ilike('reportTemplate', 'title', query),
          },
        ],
      };
    }

    const records = await tenant.reportTemplate.findAll({
      attributes: ['id', 'title'],
      where,
      limit: limit ? Number(limit) : undefined,
      orderBy: [['title', 'ASC']],
    });

    return records.map((record) => ({
      id: record.id,
      label: record.title,
    }));
  }

  /**
   * Creates an audit log of the operation.
   *
   * @param {string} action - The action [create, update or delete].
   * @param {object} record - The sequelize record
   * @param {object} data - The new data passed on the request
   * @param {object} options
   */
  async _createAuditLog(action, record, data, options) {
    let values = {};

    if (data) {
      values = {
        ...record.get({ plain: true }),
        file: data.file,
      };
    }

    await AuditLogRepository.log(
      {
        entityName: 'reportTemplate',
        entityId: record.id,
        action,
        values,
      },
      options
    );
  }

  /**
   * Fills an array of ReportTemplate with relations and files.
   *
   * @param {Array} rows
   * @param {Object} [options]
   */
  async _fillWithRelationsAndFilesForRows(rows, options) {
    if (!rows) {
      return rows;
    }

    return Promise.all(rows.map((record) => this._fillWithRelationsAndFiles(record, options)));
  }

  /**
   * Fill the ReportTemplate with the relations and files.
   *
   * @param {Object} record
   * @param {Object} [options]
   */
  async _fillWithRelationsAndFiles(record, options) {
    if (!record) {
      return record;
    }

    const output = record.get({ plain: true });

    const transaction = SequelizeRepository.getTransaction(options);

    output.file = await record.getFile({
      transaction,
    });

    output.provider = await record.getProvider({
      transaction,
    });
    if (output.provider) {
      // Get Nested Association
      output.provider.orgLogo = await output.provider.getOrgLogo({
        transaction,
      });
    }

    return output;
  }
}

module.exports = ReportTemplateRepository;
