const SequelizeRepository = require('./sequelizeRepository');
const { data, createRepoInstance } = require('../../__fixtures__').sample;
const config = require('../../../config')();

describe('SampleRepository', () => {
  let repository;
  let auditLogSpy;
  const options = {
    currentUser: {
      accountId: config.testAccountId,
    },
  };

  beforeEach(async () => {
    await SequelizeRepository.cleanDatabase(options.currentUser.accountId);
    repository = createRepoInstance();
    auditLogSpy = jest.spyOn(repository, '_createAuditLog');
  });

  describe('#create()', () => {
    it('should create a sample', async () => {
      const fixture = data[0];
      const sample = await repository.create(fixture, options);

      expect(sample.serial).toBe(fixture.serial);
    });
  });

  describe('#update()', () => {
    it('should update a sample', async () => {
      const originalSample = await repository.create(data[0], options);

      const updatedSerial = 'serial';
      const updatedSample = await repository.update(originalSample.id, { serial: updatedSerial }, options);

      expect(originalSample.id).toBe(updatedSample.id);
      expect(updatedSample.serial).toBe(updatedSerial);
    });
  });

  describe('#destroy()', () => {
    it('should remove a sample', async () => {
      const originalSample = await repository.create(data[0], options);

      await repository.destroy(originalSample.id, options);

      const foundSample = await repository.findById(originalSample.id, options);
      expect(foundSample).toBe(null);
    });
  });

  describe('#findById()', () => {
    it('should find an existing sample', async () => {
      const originalSample = await repository.create(data[0], options);

      const foundSample = await repository.findById(originalSample.id, options);
      expect(originalSample).toStrictEqual(foundSample);
    });
  });

  describe('#count()', () => {
    it('should count the total number of samples', async () => {
      await repository.create(data[0], options);

      const count = await repository.count({}, options);
      expect(count).toBe(1);
    });
  });
  describe('#findAndCountAll()', () => {
    it('should find and count all the samples', async () => {
      const originalSample = await repository.create(data[0], options);

      const result = await repository.findAndCountAll({}, options);
      expect(result.count).toBe(1);
      expect(result.rows[0]).toStrictEqual(originalSample);
    });
  });

  describe('#findAllAutocomplete()', () => {
    it('should find all samples for a list', async () => {
      const originalSample = await repository.create(data[0], options);

      const result = await repository.findAllAutocomplete(null, null, options);
      expect(result[0].id).toBe(originalSample.id);
    });
  });

  afterAll(async () => {
    await SequelizeRepository.closeConnections(options.currentUser.accountId);
    await SequelizeRepository.closeConnections();
  });
});
