const pick = require('lodash/pick');
const models = require('../models');
const SequelizeRepository = require('./sequelizeRepository');
const AuditLogRepository = require('./auditLogRepository');
const FileRepository = require('./fileRepository');
const SequelizeFilterUtils = require('../utils/sequelizeFilterUtils');

const { Sequelize } = models;
const { Op } = Sequelize;

/**
 * Handles database operations for the Report.
 * See https://sequelize.org/v5/index.html to learn how to customize it.
 */
class ReportRepository {
  /**
   * Creates the Report.
   *
   * @param {Object} data
   * @param {Object} [options]
   */
  async create(data, options) {
    const currentUser = SequelizeRepository.getCurrentUser(options);

    const tenant = await models.getTenant(currentUser.accountId);

    const transaction = SequelizeRepository.getTransaction(options);

    const record = await tenant.report.create(
      {
        ...pick(data, ['id', 'partVersion', 'data', 'title', 'templateUrl', 'filters', 'partUpdatedAt', 'status', 'importHash', 'updatedAt', 'createdAt']),
        siteId: currentUser.siteId,
        createdById: currentUser.id,
        updatedById: currentUser.id,
      },
      {
        transaction,
      }
    );

    await record.setCustomer(data.customer || null, {
      transaction,
    });
    await record.setPart(data.part || null, {
      transaction,
    });
    await record.setTemplate(data.template || null, {
      transaction,
    });

    await FileRepository.replaceRelationFiles(
      {
        belongsTo: tenant.report.getTableName(),
        belongsToColumn: 'file',
        belongsToId: record.id,
      },
      data.file,
      options
    );

    await this._createAuditLog(AuditLogRepository.CREATE, record, data, options);

    return this.findById(record.id, options);
  }

  /**
   * Updates the Report.
   *
   * @param {Object} data
   * @param {Object} [options]
   */
  async update(id, data, options) {
    const currentUser = SequelizeRepository.getCurrentUser(options);

    const tenant = await models.getTenant(currentUser.accountId);

    const transaction = SequelizeRepository.getTransaction(options);

    let record = await tenant.report.findByPk(id, {
      transaction,
    });

    record = await record.update(
      {
        ...pick(data, ['id', 'partVersion', 'data', 'title', 'templateUrl', 'filters', 'partUpdatedAt', 'status', 'importHash', 'updatedAt', 'createdAt']),
        siteId: currentUser.siteId,
        updatedById: currentUser.id,
      },
      {
        transaction,
      }
    );

    await record.setCustomer(data.customer || null, {
      transaction,
    });
    await record.setPart(data.part || null, {
      transaction,
    });
    await record.setTemplate(data.template || null, {
      transaction,
    });

    await FileRepository.replaceRelationFiles(
      {
        belongsTo: tenant.report.getTableName(),
        belongsToColumn: 'file',
        belongsToId: record.id,
      },
      data.file,
      options
    );

    await this._createAuditLog(AuditLogRepository.UPDATE, record, data, options);

    return this.findById(record.id, options);
  }

  /**
   * Deletes the Report.
   *
   * @param {string} id
   * @param {Object} [options]
   */
  async destroy(id, options) {
    const currentUser = SequelizeRepository.getCurrentUser(options);

    const tenant = await models.getTenant(currentUser.accountId);

    const transaction = SequelizeRepository.getTransaction(options);

    const record = await tenant.report.findByPk(id, {
      transaction,
    });

    await record.destroy({
      transaction,
    });

    await this._createAuditLog(AuditLogRepository.DELETE, record, null, options);
  }

  /**
   * Finds the Report and its relations.
   *
   * @param {string} id
   * @param {Object} [options]
   */
  async findById(id, options) {
    const currentUser = SequelizeRepository.getCurrentUser(options);

    const tenant = await models.getTenant(currentUser.accountId);

    const transaction = SequelizeRepository.getTransaction(options);

    const include = [
      {
        model: tenant.organization,
        model: models.organization,
        as: 'customer',
      },
      {
        model: tenant.part,
        as: 'part',
      },
      {
        model: tenant.reportTemplate,
        as: 'template',
      },
    ];

    const record = await tenant.report.findByPk(id, {
      include,
      transaction,
    });

    return this._fillWithRelationsAndFiles(record, options);
  }

  /**
   * Counts the number of Reports based on the filter.
   *
   * @param {Object} filter
   * @param {Object} [options]
   */
  async count(filter, options) {
    const currentUser = SequelizeRepository.getCurrentUser(options);

    const tenant = await models.getTenant(currentUser.accountId);

    const transaction = SequelizeRepository.getTransaction(options);

    return tenant.report.count(
      {
        where: filter,
      },
      {
        transaction,
      }
    );
  }

  /**
   * Finds the Reports based on the query.
   * See https://sequelize.org/v5/manual/querying.html to learn how to
   * customize the query.
   *
   * @param {Object} query
   * @param {Object} query.filter
   * @param {number} query.limit
   * @param  {number} query.offset
   * @param  {string} query.orderBy
   * @param {Object} [options]
   *
   * @returns {Promise<Object>} response - Object containing the rows and the count.
   */
  async findAndCountAll(
    { filter, limit, offset, orderBy } = {
      filter: null,
      limit: 0,
      offset: 0,
      orderBy: null,
    },
    options
  ) {
    const currentUser = SequelizeRepository.getCurrentUser(options);

    const tenant = await models.getTenant(currentUser.accountId);

    let where = {};
    const include = [
      {
        model: tenant.organization,
        as: 'customer',
      },
      {
        model: tenant.part,
        as: 'part',
      },
      {
        model: tenant.reportTemplate,
        as: 'template',
      },
    ];

    if (filter) {
      if (filter.id) {
        where = {
          ...where,
          id: SequelizeFilterUtils.uuid(filter.id),
        };
      }

      if (filter.site) {
        where = {
          ...where,
          siteId: SequelizeFilterUtils.uuid(filter.site),
        };
      }

      if (filter.customer) {
        where = {
          ...where,
          customerId: SequelizeFilterUtils.uuid(filter.customer),
        };
      }

      if (filter.part) {
        where = {
          ...where,
          partId: SequelizeFilterUtils.uuid(filter.part),
        };
      }

      if (filter.partVersion) {
        where = {
          ...where,
          [Op.and]: SequelizeFilterUtils.ilike('report', 'partVersion', filter.partVersion),
        };
      }

      if (filter.title) {
        where = {
          ...where,
          [Op.and]: SequelizeFilterUtils.ilike('report', 'title', filter.title),
        };
      }

      if (filter.status) {
        where = {
          ...where,
          status: filter.status,
        };
      }

      if (filter.createdAtRange) {
        const [start, end] = filter.createdAtRange;

        if (start !== undefined && start !== null && start !== '') {
          where = {
            ...where,
            createdAt: {
              ...where.createdAt,
              [Op.gte]: start,
            },
          };
        }

        if (end !== undefined && end !== null && end !== '') {
          where = {
            ...where,
            createdAt: {
              ...where.createdAt,
              [Op.lte]: end,
            },
          };
        }
      }
    }

    let { rows, count } = await tenant.report.findAndCountAll({
      where,
      include,
      limit: limit ? Number(limit) : undefined,
      offset: offset ? Number(offset) : undefined,
      order: orderBy ? [orderBy.split('_')] : [['createdAt', 'DESC']],
      transaction: SequelizeRepository.getTransaction(options),
    });

    rows = await this._fillWithRelationsAndFilesForRows(rows, options);

    return { rows, count };
  }

  /**
   * Lists the Reports to populate the autocomplete.
   * See https://sequelize.org/v5/manual/querying.html to learn how to
   * customize the query.
   *
   * @param {string} query
   * @param {number} limit
   */
  async findAllAutocomplete(query, limit, options) {
    const currentUser = SequelizeRepository.getCurrentUser(options);

    const tenant = await models.getTenant(currentUser.accountId);

    let where = {};

    if (query) {
      where = {
        [Op.or]: [
          { id: SequelizeFilterUtils.uuid(query) },
          {
            [Op.and]: SequelizeFilterUtils.ilike('report', 'title', query),
          },
        ],
      };
    }

    const records = await tenant.report.findAll({
      attributes: ['id', 'title'],
      where,
      limit: limit ? Number(limit) : undefined,
      orderBy: [['title', 'ASC']],
    });

    return records.map((record) => ({
      id: record.id,
      label: record.title,
    }));
  }

  /**
   * Marks the desired report as deleted in the database.
   *
   * @param {string} id
   * @param {options} options
   */
  async markAsDeleted(id, options) {
    const currentUser = SequelizeRepository.getCurrentUser(options);
    const transaction = SequelizeRepository.getTransaction(options);
    const tenant = await models.getTenant(currentUser.accountId);

    const record = await tenant.report.findByPk(id, {
      transaction,
    });

    const data = {
      status: 'Deleted',
      updatedById: currentUser.id,
      deletedAt: new Date(),
    };

    await record.update(data, { transaction });

    await this._createAuditLog(AuditLogRepository.DELETE, record, data, options);
  }

  /**
   * Creates an audit log of the operation.
   *
   * @param {string} action - The action [create, update or delete].
   * @param {object} record - The sequelize record
   * @param {object} data - The new data passed on the request
   * @param {object} options
   */
  async _createAuditLog(action, record, data, options) {
    let values = {};

    if (data) {
      values = {
        ...record.get({ plain: true }),
        file: data.file,
      };
    }

    await AuditLogRepository.log(
      {
        entityName: 'report',
        entityId: record.id,
        action,
        values,
      },
      options
    );
  }

  /**
   * Fills an array of Report with relations and files.
   *
   * @param {Array} rows
   * @param {Object} [options]
   */
  async _fillWithRelationsAndFilesForRows(rows, options) {
    if (!rows) {
      return rows;
    }

    return Promise.all(rows.map((record) => this._fillWithRelationsAndFiles(record, options)));
  }

  /**
   * Fill the Report with the relations and files.
   *
   * @param {Object} record
   * @param {Object} [options]
   */
  // eslint-disable-next-line class-methods-use-this
  async _fillWithRelationsAndFiles(record, options) {
    if (!record) {
      return record;
    }

    const output = record.get({ plain: true });

    const transaction = SequelizeRepository.getTransaction(options);

    output.file = await record.getFile({
      transaction,
    });

    if (output?.template?.id && !output.template.file) {
      const tenant = await models.getTenant(options.currentUser.accountId);
      const include = [
        {
          model: models.organization,
          as: 'provider',
        },
      ];

      const temp = await tenant.reportTemplate.findByPk(output.template.id, {
        include,
        transaction,
      });
      output.template = { ...output.template, ...temp };
      output.template.file = await temp.getFile({
        transaction,
      });
    }

    return output;
  }
}

module.exports = ReportRepository;
