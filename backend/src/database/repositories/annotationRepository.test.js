const SequelizeRepository = require('./sequelizeRepository');
const AnnotationRepository = require('./annotationRepository');
const { data, createRepoInstance } = require('../../__fixtures__').annotation;
const { data: accountData } = require('../../__fixtures__').account;
const config = require('../../../config')();

describe('CharacteristicRepository', () => {
  let repository;
  let auditLogSpy;
  const options = {
    currentUser: {
      accountId: config.testAccountId,
    },
  };

  beforeEach(async () => {
    await SequelizeRepository.cleanDatabase(options.currentUser.accountId);
    repository = createRepoInstance();
    auditLogSpy = jest.spyOn(repository, '_createAuditLog');
  });

  describe('#create()', () => {
    it('should create an annotation', async () => {
      const fixture = data[0];
      const annotation = await repository.create(fixture, options);

      expect(annotation.id).toBe(fixture.id);
    });

    it('should create an item and update a previousRevision', async () => {
      const fixture = data[0];
      const fixtureTwo = data[1];
      const annotation = await repository.create(fixture, options);

      const annotationTwo = await repository.create({ ...fixtureTwo, previousRevision: annotation.id }, options);
      const updatedAnnotation = await repository.findById(annotation.id, options);

      expect(annotation.id).toBe(fixture.id);
      expect(annotationTwo.id).toBe(fixtureTwo.id);
      expect(annotationTwo.previousRevision).toBe(fixture.id);
      expect(updatedAnnotation.nextRevision).toBe(annotationTwo.id);
    });
  });

  describe('#update()', () => {
    it('should update an annotation', async () => {
      const originalAnnotation = await repository.create(data[0], options);

      const updated = 'Updated';
      const updatedAnnotation = await repository.update(originalAnnotation.id, { annotation: updated }, options);

      expect(originalAnnotation.id).toBe(updatedAnnotation.id);
      expect(updatedAnnotation.annotation).toBe(updated);
    });
  });

  describe('#destroy()', () => {
    it('should remove an annotation', async () => {
      const originalAnnotation = await repository.create(data[0], options);

      await repository.destroy(originalAnnotation.id, options);

      const foundAnnotation = await repository.findById(originalAnnotation.id, options);
      expect(foundAnnotation).toBe(null);
    });
  });

  describe('#findById()', () => {
    it('should find an existing item', async () => {
      const originalItem = await repository.create(data[0], options);

      const foundItem = await repository.findById(originalItem.id, options);
      expect(originalItem).toStrictEqual(foundItem);
    });
  });

  describe('#count()', () => {
    it('should count the total number of items', async () => {
      await repository.create(data[0], options);

      const count = await repository.count(null, options);
      expect(count).toBe(1);
    });
  });

  describe('#findAndCountAll()', () => {
    it('should find and count all the annotations', async () => {
      const originalAnnotation = await repository.create(data[0], options);

      const result = await repository.findAndCountAll({}, options);
      expect(result.count).toBe(1);
      expect(result.rows[0]).toStrictEqual(originalAnnotation);
    });
  });

  describe('#findAllAutocomplete()', () => {
    it('should find all annotations for a list', async () => {
      const originalAnnotation = await repository.create(data[0], options);

      const result = await repository.findAllAutocomplete(null, null, options);
      expect(result[0].id).toBe(originalAnnotation.id);
    });
  });

  afterAll(async () => {
    await SequelizeRepository.closeConnections(options.currentUser.accountId);
    await SequelizeRepository.closeConnections();
  });
});
