const pick = require('lodash/pick');
const models = require('../models');
const SequelizeRepository = require('./sequelizeRepository');
const AuditLogRepository = require('./auditLogRepository');
const SequelizeFilterUtils = require('../utils/sequelizeFilterUtils');
const NotificationUserStatusRepository = require('./notificationUserStatusRepository');
const UserRepository = require('./userRepository');
const constants = require('../constants').accountMember;

const { log } = require('../../logger');

const { Sequelize } = models;
const { Op } = Sequelize;

/**
 * Handles database operations for the Notification.
 * See https://sequelize.org/v5/index.html to learn how to customize it.
 */
class NotificationRepository {
  /**
   * Creates the Notification.
   *
   * @param {Object} data
   * @param {Object} [options]
   */
  async create(data, options) {
    log.debug(`notificationRepository.create: ${data.title} ${data.content}`);
    const currentUser = SequelizeRepository.getCurrentUser(options);

    const tenant = await models.getTenant(currentUser.accountId);

    const transaction = SequelizeRepository.getTransaction(options);

    const record = await tenant.notification.create(
      {
        ...pick(data, [
          'id', //
          'title',
          'content',
          'action',
          'actionAt',
          'priority',
          'relationId',
          'relationType',
          'actorId',
          'createdAt',
          'updatedAt',
        ]),
        siteId: currentUser.siteId,
        createdById: currentUser.id,
        updatedById: currentUser.id,
      },
      {
        transaction,
      }
    );

    // Notifications just going to Paid Roles for now
    const notificationRoles = constants.PAID_ROLES;
    const args = { roles: notificationRoles, orderBy: null, options };
    const notificationUsers = await UserRepository.findAllByRolesArray(args, currentUser);
    const notificationUserStatusRepository = new NotificationUserStatusRepository();

    // Create a status for each user so they can mark as read or archived for themselves
    const createUserStatuses = notificationUsers.map((user) => {
      return notificationUserStatusRepository.create(
        {
          readAt: null,
          archivedAt: null,
          notificationId: record.id,
          userId: user.id,
        },
        options
      );
    });

    await Promise.all(createUserStatuses);

    await this._createAuditLog(AuditLogRepository.CREATE, record, data, options);

    return this.findById(record.id, options);
  }

  async createInAccount(data, options) {
    log.debug(`notificationRepository.createInAccount: ${data.title} ${data.content}`);
    const currentUser = SequelizeRepository.getCurrentUser(options);

    const tenant = await models.getTenant(data.accountId);

    const transaction = SequelizeRepository.getTransaction(options);

    const record = await tenant.notification.create(
      {
        ...pick(data, [
          'id', //
          'title',
          'content',
          'action',
          'actionAt',
          'priority',
          'relationId',
          'relationType',
          'actorId',
          'createdAt',
          'updatedAt',
        ]),
        siteId: data.siteId,
        createdById: currentUser.id,
        updatedById: currentUser.id,
      },
      {
        transaction,
      }
    );

    // Notifications just going to Paid Roles for now
    const notificationRoles = constants.PAID_ROLES;
    const args = { roles: notificationRoles, orderBy: null, options, accountId: data.accountId };
    const notificationUsers = await UserRepository.findAllByRolesArrayInAccount(args);
    const notificationUserStatusRepository = new NotificationUserStatusRepository();

    // Create a status for each user so they can mark as read or archived for themselves
    const createUserStatuses = notificationUsers.map((user) => {
      return notificationUserStatusRepository.createInAccount(
        {
          readAt: null,
          archivedAt: null,
          notificationId: record.id,
          userId: user.id,
          accountId: data.accountId,
        },
        options
      );
    });

    await Promise.all(createUserStatuses);

    await this._createAuditLog(AuditLogRepository.CREATE, record, data, options);

    return this.findByIdInAccount(record.id, data.accountId, options);
  }

  /**
   * Updates the Notification.
   *
   * @param {Object} data
   * @param {Object} [options]
   */
  async update(id, data, options) {
    const currentUser = SequelizeRepository.getCurrentUser(options);

    const tenant = await models.getTenant(currentUser.accountId);

    const transaction = SequelizeRepository.getTransaction(options);

    let record = await tenant.notification.findByPk(id, {
      transaction,
    });

    record = await record.update(
      {
        ...pick(data, [
          'id', //
          'title',
          'content',
          'action',
          'actionAt',
          'priority',
          'relationId',
          'relationType',
          'actorId',
          'updatedAt',
        ]),
        siteId: currentUser.siteId,
        updatedById: currentUser.id,
      },
      {
        transaction,
      }
    );

    await this._createAuditLog(AuditLogRepository.UPDATE, record, data, options);

    return this.findById(record.id, options);
  }

  /**
   * Deletes the Notification.
   *
   * @param {string} id
   * @param {Object} [options]
   */
  async destroy(id, options) {
    const currentUser = SequelizeRepository.getCurrentUser(options);

    const tenant = await models.getTenant(currentUser.accountId);

    const transaction = SequelizeRepository.getTransaction(options);

    const record = await tenant.notification.findByPk(id, {
      transaction,
    });

    await record.destroy({
      transaction,
    });

    await this._createAuditLog(AuditLogRepository.DELETE, record, null, options);
  }

  /**
   * Finds the Notification and its relations.
   *
   * @param {string} id
   * @param {Object} [options]
   */
  async findById(id, options) {
    const currentUser = SequelizeRepository.getCurrentUser(options);

    const tenant = await models.getTenant(currentUser.accountId);

    const transaction = SequelizeRepository.getTransaction(options);

    const include = [];

    const record = await tenant.notification.findByPk(id, {
      include,
      transaction,
    });

    return this._fillWithRelationsAndFiles(record, options);
  }

  async findByIdInAccount(id, accountId, options) {
    const tenant = await models.getTenant(accountId);

    const transaction = SequelizeRepository.getTransaction(options);

    const include = [];

    const record = await tenant.notification.findByPk(id, {
      include,
      transaction,
    });

    return this._fillWithRelationsAndFiles(record, options);
  }

  /**
   * Counts the number of Notifications based on the filter.
   *
   * @param {Object} filter
   * @param {Object} [options]
   */
  // eslint-disable-next-line class-methods-use-this
  async count(filter, options) {
    const currentUser = SequelizeRepository.getCurrentUser(options);

    const tenant = await models.getTenant(currentUser.accountId);

    const transaction = SequelizeRepository.getTransaction(options);

    return tenant.notification.count(
      {
        where: filter,
      },
      {
        transaction,
      }
    );
  }

  /**
   * Finds the Notifications based on the query.
   * See https://sequelize.org/v5/manual/querying.html to learn how to
   * customize the query.
   *
   * @param {Object} query
   * @param {Object} query.filter
   * @param {number} query.limit
   * @param  {number} query.offset
   * @param  {string} query.orderBy
   * @param {Object} [options]
   *
   * @returns {Promise<Object>} response - Object containing the rows and the count.
   */
  async findAndCountAll(
    { filter, limit, offset, orderBy } = {
      filter: null,
      limit: 0,
      offset: 0,
      orderBy: null,
    },
    options
  ) {
    const currentUser = SequelizeRepository.getCurrentUser(options);

    const tenant = await models.getTenant(currentUser.accountId);

    let where = {};
    const include = {
      model: models.notificationUserStatus, //
      as: 'userStatuses',
      required: true,
      duplicating: false,
    };

    if (filter) {
      if (filter.id) {
        where = {
          ...where,
          id: filter.id,
        };
      }

      if (filter.site) {
        where = {
          ...where,
          siteId: SequelizeFilterUtils.uuid(filter.site),
        };
      }

      if (filter.title) {
        where = {
          ...where,
          [Op.and]: SequelizeFilterUtils.ilike('notification', 'title', filter.title),
        };
      }

      if (filter.content) {
        where = {
          ...where,
          [Op.and]: SequelizeFilterUtils.ilike('notification', 'content', filter.content),
        };
      }

      if (filter.action) {
        where = {
          ...where,
          [Op.and]: SequelizeFilterUtils.ilike('notification', 'action', filter.action),
        };
      }

      if (filter.status) {
        where = {
          ...where,
          status: filter.status,
        };
      }

      if (filter.relationType) {
        where = {
          ...where,
          type: filter.relationType,
        };
      }

      if (filter.relation) {
        where = {
          ...where,
          relationId: SequelizeFilterUtils.uuid(filter.relation),
        };
      }

      if (filter.priority) {
        where = {
          ...where,
          priority: filter.priority,
        };
      }

      if (filter.actor) {
        where = {
          ...where,
          actorId: SequelizeFilterUtils.uuid(filter.actor),
        };
      }

      if (filter.actionAtRange) {
        const [start, end] = filter.actionAtRange;

        if (start !== undefined && start !== null && start !== '') {
          where = {
            ...where,
            actionAt: {
              ...where.actionAt,
              [Op.gte]: start,
            },
          };
        }

        if (end !== undefined && end !== null && end !== '') {
          where = {
            ...where,
            actionAt: {
              ...where.actionAt,
              [Op.lte]: end,
            },
          };
        }
      }

      if (filter.createdAtRange) {
        const [start, end] = filter.createdAtRange;

        if (start !== undefined && start !== null && start !== '') {
          where = {
            ...where,
            createdAt: {
              ...where.createdAt,
              [Op.gte]: start,
            },
          };
        }

        if (end !== undefined && end !== null && end !== '') {
          where = {
            ...where,
            createdAt: {
              ...where.createdAt,
              [Op.lte]: end,
            },
          };
        }
      }

      // userStatuses filters are inner joins
      if (filter.userStatuses && filter.userStatuses.user) {
        where = {
          ...where,
          '$userStatuses.userId$': filter.userStatuses.user,
        };
      }

      if (filter.userStatuses && filter.userStatuses.readAt === false) {
        where = {
          ...where,
          '$userStatuses.readAt$': null,
        };
      }

      if (filter.userStatuses && filter.userStatuses.archivedAt === false) {
        where = {
          ...where,
          '$userStatuses.archivedAt$': null,
        };
      } else {
        where = {
          ...where,
          '$userStatuses.archivedAt$': {
            [Op.ne]: null,
          },
        };
      }
    }

    const { rows: rows0, count } = await tenant.notification.findAndCountAll({
      where,
      limit: limit ? Number(limit) : undefined,
      offset: offset ? Number(offset) : undefined,
      order: orderBy ? [orderBy.split('_')] : [['createdAt', 'DESC']],
      transaction: SequelizeRepository.getTransaction(options),
      include,
    });

    const rows = await this._fillWithRelationsAndFilesForRows(rows0, options);

    return { rows, count };
  }

  /**
   * Creates an audit log of the Notification.
   *
   * @param {string} action - The action [create, update or delete].
   * @param {object} record - The sequelize record
   * @param {object} data - The new data passed on the request
   * @param {object} options
   */
  // eslint-disable-next-line class-methods-use-this
  async _createAuditLog(action, record, data, options) {
    let values = {};

    if (data) {
      values = {
        ...record.get({ plain: true }),
      };
    }

    await AuditLogRepository.log(
      {
        entityName: 'notification',
        entityId: record.id,
        action,
        values,
      },
      options
    );
  }

  /**
   * Fills an array of Notification with relations and files.
   *
   * @param {Array} rows
   * @param {Object} [options]
   */
  async _fillWithRelationsAndFilesForRows(rows, options) {
    if (!rows) {
      return rows;
    }

    return Promise.all(rows.map((record) => this._fillWithRelationsAndFiles(record, options)));
  }

  /**
   * Fill the Notification with the relations and files.
   *
   * @param {Object} record
   * @param {Object} [options]
   */
  // eslint-disable-next-line class-methods-use-this
  async _fillWithRelationsAndFiles(record, options) {
    if (!record) {
      return record;
    }

    const output = record.get({ plain: true });

    const transaction = SequelizeRepository.getTransaction(options);

    const userStatuses = await record.getUserStatuses({
      transaction,
    });

    // Only loading the status for the current user
    const currentUser = SequelizeRepository.getCurrentUser(options);
    userStatuses.forEach((userStatus) => (userStatus.user = userStatus.userId));
    output.userStatuses = userStatuses.filter((userStatus) => userStatus.userId === currentUser.id);

    return output;
  }
}

module.exports = NotificationRepository;
