const ListItemRepository = require('./listItemRepository');
const SequelizeRepository = require('./sequelizeRepository');
const { data } = require('../../__fixtures__').listItem;
const config = require('../../../config')();

describe('ListItemRepository', () => {
  const repository = ListItemRepository;
  const options = {
    currentUser: {
      accountId: config.testAccountId,
      siteId: '31ef1d56-1b86-4ffa-b886-507ed8f021c5',
      userId: 'e0a01779-c8ed-4090-bd44-9c1b2242fb75',
    },
  };

  const loadAllFixtures = async () => {
    const items = [];
    // Load list items for classifications and types
    for (let classificationIndex = 0; classificationIndex < data[0].length; classificationIndex++) {
      const item = data[0][classificationIndex];
      items.push(repository.create(item, options));
    }
    for (let typeIndex = 0; typeIndex < data[1].length; typeIndex++) {
      const typeItem = data[1][typeIndex];
      items.push(repository.create(typeItem, options));
    }
    await Promise.all(items);
  };

  beforeEach(async () => {
    await SequelizeRepository.cleanDatabase();
    await SequelizeRepository.cleanDatabase(options.currentUser.accountId);
  });

  describe('#create()', () => {
    it('should create an item', async () => {
      const fixture = data[0][0];
      const item = await repository.create(fixture, options);

      expect(item.id).toBe(fixture.id);
    });
  });

  describe('#createGlobal()', () => {
    it('should create an item in the primary database', async () => {
      const fixture = data[0][0];
      const item = await repository.createGlobal(fixture, options);

      expect(item.id).toBe(fixture.id);
    });
  });

  describe('#update()', () => {
    it('should update an item', async () => {
      const originalItem = await repository.create(data[0][0], options);

      const updatedStatus = 'Inactive';
      const updatedItem = await repository.update(originalItem.id, { status: updatedStatus }, options);

      expect(originalItem.id).toBe(updatedItem.id);
      expect(updatedItem.status).toBe(updatedStatus);
    });
  });

  describe('#destroy()', () => {
    it('should remove an item', async () => {
      const originalItem = await repository.create(data[0][0], options);

      await repository.destroy(originalItem.id, options);

      const foundItem = await repository.findById(originalItem.id, options);
      expect(foundItem).toBe(null);
    });
  });

  describe('#findById()', () => {
    it('should find an existing item', async () => {
      const originalItem = await repository.create(data[0][0], options);

      const foundItem = await repository.findById(originalItem.id, options);
      expect(originalItem).toStrictEqual(foundItem);
    });
  });

  describe('#findByType()', () => {
    it('should return an array of listItems, including items for the user and site, and not other users', async () => {
      await loadAllFixtures();

      const foundItems = await repository.findByType('Classification', options);
      expect(foundItems.length).toBe(7);

      const siteItem = foundItems.find((x) => x.id === '8b5de9f8-a225-4d32-9126-bc3e0c5e22e5');
      // Ignoring the date fields so we can do a strict comparison
      delete siteItem.createdAt;
      delete siteItem.updatedAt;
      expect(siteItem).toStrictEqual(data[0][5]);

      const userItem = foundItems.find((x) => x.id === 'c1c95001-c20f-464e-8a3b-860d7f6e1b51');
      // Ignoring the date fields so we can do a strict comparison
      delete userItem.createdAt;
      delete userItem.updatedAt;
      expect(userItem).toStrictEqual(data[0][6]);

      // Check that listItem for a different user is not in list
      const otherUserItem = foundItems.find((x) => x.id === '8b5de9f8-a225-4d32-9126-bc3e0c5e22e5');
      expect(otherUserItem).toBe(otherUserItem);
    });

    it('should return child items in the array of list items', async () => {
      await loadAllFixtures();

      const foundItems = await repository.findByType('Type', options);
      expect(foundItems.length).toBe(10);

      // Items should be sorted in same order as the fixtures
      for (let typeIndex = 0; typeIndex < data[1].length; typeIndex++) {
        const foundItem = foundItems[typeIndex];
        // Ignoring the date fields so we can do a strict comparison
        delete foundItem.createdAt;
        delete foundItem.updatedAt;
        const typeItem = data[1][typeIndex];
        expect(foundItem).toStrictEqual(typeItem);
      }
    });
  });

  describe('#findAndCountAll()', () => {
    it('should find and count all the items', async () => {
      const originalItem = await repository.create(data[0][0], options);

      const result = await repository.findAndCountAll({}, options);
      expect(result.count).toBe(1);
      expect(result.rows[0]).toStrictEqual(originalItem);
    });
  });

  afterAll(async () => {
    await SequelizeRepository.closeConnections(options.currentUser.accountId);
    await SequelizeRepository.closeConnections();
  });
});
