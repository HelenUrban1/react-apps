const AccountMemberRepository = require('./accountMemberRepository');
const UserRepository = require('./userRepository');
const SequelizeRepository = require('./sequelizeRepository');
const { data } = require('../../__fixtures__').user;
const config = require('../../../config')();
const accountMemberFixture = require('../../__fixtures__').accountMember;

const accountMemberData = accountMemberFixture.data;
const constants = require('../constants');
const { createUserWithAccountAssociation } = require('./repositoryTestUtils');
const ValidationError = require('../../errors/validationError');

// Generated values that are extremely unlikely to match a dynamically generated UUID value
const SAMPLE_UUIDS = ['f44ffabd-790e-49e4-8417-95abf99a1e19', 'c4ece6e0-dfa7-4240-a110-a3c1d8f49433'];

describe('UserRepository', () => {
  let options = {};

  const ROLES_DICTIONARY = {};
  constants.accountMember.ROLES.forEach((role) => {
    ROLES_DICTIONARY[role] = role;
  });
  const currentUser = { accountId: accountMemberFixture.ACCOUNT_ID_A };

  beforeEach(async () => {
    options = {
      currentUser: {
        accountId: config.testAccountId,
      },
    };
    await SequelizeRepository.cleanDatabase(options.currentUser.accountId);
    await SequelizeRepository.cleanDatabase();
  });

  afterAll(async () => {
    await SequelizeRepository.closeConnections(options.currentUser.accountId);
    await SequelizeRepository.closeConnections();
  });

  describe('#create()', () => {
    it('should create a user', async () => {
      const fixture = data[0];
      const item = await UserRepository.create(fixture, options);

      expect(item.id).toBe(fixture.id);
    });
  });

  describe('#update()', () => {
    it('should update an user', async () => {
      const originalItem = await UserRepository.create(data[0], options);

      const updatedFirstName = 'Moe';
      const updatedItem = await UserRepository.update(originalItem.id, { firstName: updatedFirstName }, options);

      expect(originalItem.id).toBe(updatedItem.id);
      expect(updatedItem.profile.firstName).toBe(updatedFirstName);
    });
  });

  describe('#generateEmailVerificationToken()', () => {
    it('should error if no user exists', async () => {
      await expect(UserRepository.generateEmailVerificationToken(data[0].email, options)).rejects.toThrow(ValidationError);
    });

    it('should generate a token for an existing user', async () => {
      await UserRepository.create(data[0], options);
      const token = await UserRepository.generateEmailVerificationToken(data[0].email, options);

      expect(token).not.toBe(null);
    });

    it('should be case insensitive when looking for users', async () => {
      await UserRepository.create(data[0], options);
      const token = await UserRepository.generateEmailVerificationToken(data[0].email.toUpperCase(), options);

      expect(token).not.toBe(null);
    });
  });

  describe('#generatePasswordResetToken()', () => {
    it('should return a null token and user for invalid email', async () => {
      await expect(await UserRepository.generatePasswordResetToken(data[0].email, options)).toStrictEqual({ token: null, user: null });
    });

    it('should generate a token for an existing user', async () => {
      await UserRepository.create(data[0], options);
      const token = await UserRepository.generatePasswordResetToken(data[0].email, options);

      expect(token).not.toBe(null);
    });

    it('should be case insensitive when looking for users', async () => {
      await UserRepository.create(data[0], options);
      const token = await UserRepository.generatePasswordResetToken(data[0].email.toUpperCase(), options);

      expect(token).not.toBe(null);
    });
  });

  describe('#findByEmail()', () => {
    it('should return null if no user exists', async () => {
      const user = await UserRepository.findByEmailWithoutAvatar(data[0].email, options);
      expect(user).toBe(null);
    });

    it('should generate a token for an existing user', async () => {
      await UserRepository.create(data[0], options);
      const user = await UserRepository.findByEmail(data[0].email, options);

      expect(data[0].id).toBe(user.id);
    });

    it('should be case insensitive when looking for users', async () => {
      await UserRepository.create(data[0], options);
      const user = await UserRepository.findByEmail(data[0].email.toUpperCase(), options);

      expect(data[0].id).toBe(user.id);
    });
  });

  describe('#findByEmailWithoutAvatar()', () => {
    it('should return null if no user exists', async () => {
      const user = await UserRepository.findByEmailWithoutAvatar(data[0].email, options);
      expect(user).toBe(null);
    });

    it('should generate a token for an existing user', async () => {
      await UserRepository.create(data[0], options);
      const user = await UserRepository.findByEmailWithoutAvatar(data[0].email, options);

      expect(data[0].id).toBe(user.id);
    });

    it('should be case insensitive when looking for users', async () => {
      await UserRepository.create(data[0], options);
      const user = await UserRepository.findByEmailWithoutAvatar(data[0].email.toUpperCase(), options);

      expect(data[0].id).toBe(user.id);
    });
  });

  describe('#setActiveAccountMembership()', () => {
    it('should return the matching AccountMember record when the user has activeAccountMemberId set', async () => {
      let { user, accountMember } = await createUserWithAccountAssociation(
        {
          accountMemberFixture: accountMemberData.accountAUserCOwner,
          userFixture: data[0],
        },
        options
      );

      let accountMemberRecordsForUser = await AccountMemberRepository.findByUser(user.id, options);
      expect(accountMemberRecordsForUser[0].id).toBe(accountMember.id);

      const accountMemberRecords = user.accountMemberships;
      expect(accountMemberRecords.length).toBe(1);

      const secondAccountMemberRecordFixture = { ...accountMemberRecords[0].get({ plain: true }), id: SAMPLE_UUIDS[0], user: user.id, account: SAMPLE_UUIDS[1] };

      const secondAccountMemberRecord = await AccountMemberRepository.create(secondAccountMemberRecordFixture, options);
      expect(user.activeAccountMemberId).not.toBe(secondAccountMemberRecord.id);

      accountMemberRecordsForUser = await AccountMemberRepository.findByUser(user.id, options);
      expect(accountMemberRecordsForUser.length).toBe(2);

      await UserRepository.setActiveAccountMembership(user.id, secondAccountMemberRecord.id, options);

      user = await UserRepository.findById(user.id, options, user);
      expect(user.activeAccountMemberId).not.toBe(accountMember.id);
      expect(user.activeAccountMemberId).toBe(secondAccountMemberRecord.id);
    });
  });

  describe('#getActiveAccountMemberRecord()', () => {
    it('should return null when a user record is not provided', async () => {
      const result = await UserRepository.getActiveAccountMemberRecord(null, options);
      expect(result).toBe(null);
    });

    it('should return null when a user has no account memberships', async () => {
      const fixture = data[0];
      const user = await UserRepository.create(fixture, options);

      const result = await UserRepository.getActiveAccountMemberRecord(user, options);
      expect(result).toBe(null);
    });

    it('should return the matching AccountMember record when the user has activeAccountMemberId set', async () => {
      const { user } = await createUserWithAccountAssociation(
        {
          accountMemberFixture: accountMemberData.accountAUserCOwner,
          userFixture: data[0],
        },
        options
      );

      const accountMemberRecords = await AccountMemberRepository.findByUser(user.id, options);
      expect(accountMemberRecords.length).toBe(1);
      expect(accountMemberRecords[0].id).toBe(user.activeAccountMemberId);

      const result = await UserRepository.getActiveAccountMemberRecord(user, options);
      expect(result).not.toBe(null);
      expect(result.id).toBe(accountMemberRecords[0].id);
      expect(result.userId).toBe(user.id);
    });
  });

  describe('#findAllByRole()', () => {
    it('should return an empty array for no matching users', async () => {
      expect(data[0].roles.includes(ROLES_DICTIONARY.Collaborator)).toBe(false);

      await UserRepository.create(data[0], options);

      const users = await UserRepository.findAllByRole({ role: ROLES_DICTIONARY.Collaborator }, options, currentUser);
      expect(Array.isArray(users)).toBe(true);
      expect(users.length).toBe(0);
    });

    it('should return all matching users', async () => {
      expect(data[0].roles.includes(ROLES_DICTIONARY.Admin)).toBe(true);
      expect(data[2].roles.includes(ROLES_DICTIONARY.Admin)).toBe(true);

      const { user } = await createUserWithAccountAssociation(
        {
          accountMemberFixture: accountMemberData.accountAUserCOwner,
          userFixture: data[0],
        },
        options
      );

      await createUserWithAccountAssociation(
        {
          accountMemberFixture: accountMemberData.accountAUserAPaidRoles,
          userFixture: data[2],
        },
        options
      );

      const users = await UserRepository.findAllByRole({ role: ROLES_DICTIONARY.Admin }, options, user);
      expect(users.length).toBe(2);
      users.forEach((person) => {
        expect(person.roles.includes(ROLES_DICTIONARY.Admin)).toBe(true);
      });
    });
  });

  describe('#findAllByRoleWithRole()', () => {
    it('should return an empty array for no matching users', async () => {
      expect(data[0].roles.includes(ROLES_DICTIONARY.collaborator)).toBe(false);

      await UserRepository.create(data[0], options);

      const results = await UserRepository.findAllByRoleWithRole({ filter: { role: ROLES_DICTIONARY.Collaborator } }, options, currentUser);
      expect(Array.isArray(results)).toBe(true);
      expect(results.length).toBe(0);
    });

    it('should return all matching users', async () => {
      expect(data[1].roles.includes(ROLES_DICTIONARY.Billing)).toBe(true);
      expect(data[2].roles.includes(ROLES_DICTIONARY.Billing)).toBe(true);
      const fixtures = [data[1], data[2]];

      await Promise.all(fixtures.map((fixture) => UserRepository.create(fixture, options)));

      const results = await UserRepository.findAllByRoleWithRole({ filter: { role: ROLES_DICTIONARY.Billing } }, options, currentUser);
      expect(Array.isArray(results)).toBe(true);
      results.forEach((result) => {
        expect(result.role).toBe(ROLES_DICTIONARY.Billing);
        result.users.forEach((user) => expect(user.roles.includes(ROLES_DICTIONARY.Billing)).toBe(true));
      });
    });
  });

  describe('#findById()', () => {
    it('should find an existing item', async () => {
      const originalItem = await UserRepository.create(data[0], options);

      const foundItem = await UserRepository.findById(originalItem.id, options, currentUser);
      expect(originalItem).toStrictEqual(foundItem);
    });
  });

  describe('#count()', () => {
    it('should count the total number of items', async () => {
      await createUserWithAccountAssociation(
        {
          accountMemberFixture: accountMemberData.accountAUserCOwner,
          userFixture: data[0],
        },
        options
      );

      const count = await UserRepository.count(null, options, currentUser);
      expect(count).toBe(1);
    });
  });

  describe('#findAndCountAll()', () => {
    it('should find and count all the items', async () => {
      const { user } = await createUserWithAccountAssociation(
        {
          accountMemberFixture: accountMemberData.accountAUserCOwner,
          userFixture: data[0],
        },
        options
      );

      const result = await UserRepository.findAndCountAll({}, options, currentUser);
      expect(result.count).toBe(1);
      expect(result.rows[0]).toStrictEqual(user);
    });
    it('should only find users in the same account as the current user', async () => {
      const { user: accountA } = await createUserWithAccountAssociation(
        {
          accountMemberFixture: accountMemberData.accountAUserCOwner,
          userFixture: { ...data[0], id: accountMemberFixture.USER_ID_C },
        },
        options
      );
      const { user: accountB } = await createUserWithAccountAssociation(
        {
          accountMemberFixture: accountMemberData.accountBUserBOwner,
          userFixture: { ...data[1], id: accountMemberFixture.USER_ID_B },
        },
        options
      );
      const { user: accountC } = await createUserWithAccountAssociation(
        {
          accountMemberFixture: accountMemberData.accountCUserAPaidRoles,
          userFixture: { ...data[2], id: accountMemberFixture.USER_ID_A },
        },
        options
      );

      const resultA = await UserRepository.findAndCountAll({}, options, currentUser);
      expect(resultA.count).toBe(1);
      expect(resultA.rows[0]).toStrictEqual(accountA);
      const resultB = await UserRepository.findAndCountAll({}, options, { accountId: accountMemberFixture.ACCOUNT_ID_B });
      expect(resultB.count).toBe(1);
      expect(resultB.rows[0]).toStrictEqual(accountB);
      const resultC = await UserRepository.findAndCountAll({}, options, { accountId: accountMemberFixture.ACCOUNT_ID_C });
      expect(resultC.count).toBe(1);
      expect(resultC.rows[0]).toStrictEqual(accountC);
    });
  });

  describe('#findAllAutocomplete()', () => {
    it('should find all items for a list', async () => {
      const { user } = await createUserWithAccountAssociation(
        {
          accountMemberFixture: accountMemberData.accountAUserCOwner,
          userFixture: data[0],
        },
        options
      );

      const result = await UserRepository.findAllAutocomplete(null, null, options, currentUser);
      expect(result[0].id).toBe(user.id);
    });
  });
});
