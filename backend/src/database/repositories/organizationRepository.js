const lodash = require('lodash');
const pick = require('lodash/pick');
const models = require('../models');
const SequelizeRepository = require('./sequelizeRepository');
const FileRepository = require('./fileRepository');
const AuditLogRepository = require('./auditLogRepository');
const SequelizeFilterUtils = require('../utils/sequelizeFilterUtils');
const { log } = require('../../logger');

const { Sequelize } = models;
const { Op } = Sequelize;

/**
 * Handles database operations for the Organization.
 * See https://sequelize.org/v5/index.html to learn how to customize it.
 */
class OrganizationRepository {
  /**
   * Creates the Organization.
   *
   * @param {Object} data
   * @param {Object} [options]
   */
  async create(data, options) {
    const currentUser = SequelizeRepository.getCurrentUser(options);

    const tenant = await models.getTenant(currentUser.accountId);

    const transaction = SequelizeRepository.getTransaction(options);

    const record = await tenant.organization.create(
      {
        ...pick(data, ['id', 'status', 'type', 'settings', 'name', 'phone', 'email', 'street1', 'street2', 'city', 'region', 'postalCode', 'country', 'createdAt', 'updatedAt']),
        siteId: currentUser.siteId,
        createdById: currentUser.id,
        updatedById: currentUser.id,
      },
      {
        transaction,
      }
    );

    await record.setTemplates(data.templates || [], {
      transaction,
    });
    await record.setParts(data.parts || [], {
      transaction,
    });
    await record.setReports(data.reports || [], {
      transaction,
    });

    await FileRepository.replaceRelationFiles(
      {
        belongsTo: models.organization.getTableName(),
        belongsToColumn: 'orgLogo',
        belongsToId: record.id,
      },
      data.orgLogo,
      options
    );

    await this._createAuditLog(AuditLogRepository.CREATE, record, data, options);

    return this.findById(record.id, options);
  }

  /**
   * Updates the Organization.
   *
   * @param {Object} data
   * @param {Object} [options]
   */
  async update(id, data, options) {
    const currentUser = SequelizeRepository.getCurrentUser(options);

    const tenant = await models.getTenant(currentUser.accountId);

    const transaction = SequelizeRepository.getTransaction(options);

    let record = await tenant.organization.findByPk(id, {
      transaction,
    });

    record = await record.update(
      {
        ...pick(data, ['id', 'status', 'type', 'settings', 'name', 'phone', 'email', 'street1', 'street2', 'city', 'region', 'postalCode', 'country', 'createdAt', 'updatedAt']),
        siteId: currentUser.siteId,
        updatedById: currentUser.id,
      },
      {
        transaction,
      }
    );

    await record.setTemplates(data.templates || [], {
      transaction,
    });
    await record.setParts(data.parts || [], {
      transaction,
    });
    await record.setReports(data.reports || [], {
      transaction,
    });

    await FileRepository.replaceRelationFiles(
      {
        belongsTo: models.organization.getTableName(),
        belongsToColumn: 'orgLogo',
        belongsToId: record.id,
      },
      data.orgLogo,
      options
    );

    await this._createAuditLog(AuditLogRepository.UPDATE, record, data, options);

    return this.findById(record.id, options);
  }

  /**
   * Deletes the Organization.
   *
   * @param {string} id
   * @param {Object} [options]
   */
  async destroy(id, options) {
    const currentUser = SequelizeRepository.getCurrentUser(options);

    const tenant = await models.getTenant(currentUser.accountId);

    const transaction = SequelizeRepository.getTransaction(options);

    const record = await tenant.organization.findByPk(id, {
      transaction,
    });

    await record.destroy({
      transaction,
    });

    await this._createAuditLog(AuditLogRepository.DELETE, record, null, options);
  }

  /**
   * Finds the Organization and its relations.
   *
   * @param {string} id
   * @param {Object} [options]
   */
  async findById(id, options) {
    const currentUser = SequelizeRepository.getCurrentUser(options);

    const tenant = await models.getTenant(currentUser.accountId);

    const transaction = SequelizeRepository.getTransaction(options);

    const include = [];

    const record = await tenant.organization.findByPk(id, {
      include,
      transaction,
    });

    return this._fillWithRelationsAndFiles(record, options);
  }

  /**
  * Checks if an organization exists
  *
  * @param {string} name
  * @param {Object} [options]
  * @returns {Boolean}
  */
  async getDoesOrganizationExist(name, options) {
    const currentUser = SequelizeRepository.getCurrentUser(options);

    const tenant = await models.getTenant(currentUser.accountId);
  
    const count = await tenant.organization.count({
      where: { 
        [Op.and]: 
          [
            {siteId: currentUser.siteId}, 
            {name: name},
            {type: 'Customer'},
          ],
        },
      });
      return count !== 0;
    }

  /**
   * Counts the number of Organizations based on the filter.
   *
   * @param {Object} filter
   * @param {Object} [options]
   */
  async count(filter, options) {
    const currentUser = SequelizeRepository.getCurrentUser(options);

    const tenant = await models.getTenant(currentUser.accountId);

    const transaction = SequelizeRepository.getTransaction(options);

    return tenant.organization.count(
      {
        where: filter,
      },
      {
        transaction,
      }
    );
  }

  /**
   * Finds the Organizations based on the query.
   * See https://sequelize.org/v5/manual/querying.html to learn how to
   * customize the query.
   *
   * @param {Object} query
   * @param {Object} query.filter
   * @param {number} query.limit
   * @param  {number} query.offset
   * @param  {string} query.orderBy
   * @param {Object} [options]
   *
   * @returns {Promise<Object>} response - Object containing the rows and the count.
   */
  async findAndCountAll(
    { filter, limit, offset, orderBy } = {
      filter: null,
      limit: 0,
      offset: 0,
      orderBy: null,
    },
    options
  ) {
    const currentUser = SequelizeRepository.getCurrentUser(options);

    const tenant = await models.getTenant(currentUser.accountId);

    let where = {};

    // TODO: this was causing Orgs w/o any Parts to not come back in the query. Ask Devin about it.
    const include =
      filter && filter.parts
        ? [
            {
              model: models.part,
              as: 'parts',
              where: filter.parts ? { id: filter.parts } : {},
            },
          ]
        : [];

    if (filter) {
      if (filter.id) {
        where = {
          ...where,
          id: filter.id,
        };
      }

      if (filter.site) {
        where = {
          ...where,
          siteId: SequelizeFilterUtils.uuid(filter.site),
        };
      }

      if (filter.status) {
        where = {
          ...where,
          status: filter.status,
        };
      }

      if (filter.type) {
        where = {
          ...where,
          type: filter.type,
        };
      }

      if (filter.settings) {
        where = {
          ...where,
          [Op.and]: SequelizeFilterUtils.ilike('organization', 'settings', filter.settings),
        };
      }

      if (filter.name) {
        where = {
          ...where,
          [Op.and]: SequelizeFilterUtils.ilike('organization', 'name', filter.name),
        };
      }

      if (filter.phone) {
        where = {
          ...where,
          [Op.and]: SequelizeFilterUtils.ilike('organization', 'phone', filter.phone),
        };
      }

      if (filter.email) {
        where = {
          ...where,
          [Op.and]: SequelizeFilterUtils.ilike('organization', 'email', filter.email),
        };
      }

      if (filter.street1) {
        where = {
          ...where,
          [Op.and]: SequelizeFilterUtils.ilike('organization', 'street1', filter.street1),
        };
      }

      if (filter.street2) {
        where = {
          ...where,
          [Op.and]: SequelizeFilterUtils.ilike('organization', 'street2', filter.street2),
        };
      }

      if (filter.city) {
        where = {
          ...where,
          [Op.and]: SequelizeFilterUtils.ilike('organization', 'city', filter.city),
        };
      }

      if (filter.region) {
        where = {
          ...where,
          region: filter.region,
        };
      }

      if (filter.postalCode) {
        where = {
          ...where,
          [Op.and]: SequelizeFilterUtils.ilike('organization', 'postalCode', filter.postalCode),
        };
      }

      if (filter.country) {
        where = {
          ...where,
          country: filter.country,
        };
      }

      // if (filter.parts) {
      //   where = {
      //     ...where,
      //     parts: filter.parts,
      //   };
      // }

      if (filter.createdAtRange) {
        const [start, end] = filter.createdAtRange;

        if (start !== undefined && start !== null && start !== '') {
          where = {
            ...where,
            createdAt: {
              ...where.createdAt,
              [Op.gte]: start,
            },
          };
        }

        if (end !== undefined && end !== null && end !== '') {
          where = {
            ...where,
            createdAt: {
              ...where.createdAt,
              [Op.lte]: end,
            },
          };
        }
      }
    }

    let { rows, count } = await tenant.organization.findAndCountAll({
      where,
      include,
      limit: limit ? Number(limit) : undefined,
      offset: offset ? Number(offset) : undefined,
      order: orderBy ? [orderBy.split('_')] : [['createdAt', 'DESC']],
      transaction: SequelizeRepository.getTransaction(options),
    });

    rows = await this._fillWithRelationsAndFilesForRows(rows, options);

    return { rows, count };
  }

  /**
   * Lists the Organizations to populate the autocomplete.
   * See https://sequelize.org/v5/manual/querying.html to learn how to
   * customize the query.
   *
   * @param {string} query
   * @param {number} limit
   */
  async findAllAutocomplete(query, limit, options) {
    const currentUser = SequelizeRepository.getCurrentUser(options);

    const tenant = await models.getTenant(currentUser.accountId);

    let where = {};

    if (query) {
      where = {
        [Op.or]: [
          { id: SequelizeFilterUtils.uuid(query) },
          {
            [Op.and]: SequelizeFilterUtils.ilike('organization', 'name', query),
          },
        ],
      };
    }

    const records = await tenant.organization.findAll({
      attributes: ['id', 'name'],
      where,
      limit: limit ? Number(limit) : undefined,
      orderBy: [['name', 'ASC']],
    });

    return records.map((record) => ({
      id: record.id,
      label: record.name,
    }));
  }

  /**
   * Creates an audit log of the operation.
   *
   * @param {string} action - The action [create, update or delete].
   * @param {object} record - The sequelize record
   * @param {object} data - The new data passed on the request
   * @param {object} options
   */
  async _createAuditLog(action, record, data, options) {
    let values = {};

    if (data) {
      values = {
        ...record.get({ plain: true }),
        orgLogo: data.orgLogo,
      };
    }

    await AuditLogRepository.log(
      {
        entityName: 'organization',
        entityId: record.id,
        action,
        values,
      },
      options
    );
  }

  /**
   * Fills an array of Organization with relations and files.
   *
   * @param {Array} rows
   * @param {Object} [options]
   */
  async _fillWithRelationsAndFilesForRows(rows, options) {
    if (!rows) {
      return rows;
    }

    return Promise.all(rows.map((record) => this._fillWithRelationsAndFiles(record, options)));
  }

  /**
   * Fill the Organization with the relations and files.
   *
   * @param {Object} record
   * @param {Object} [options]
   */
  async _fillWithRelationsAndFiles(record, options) {
    if (!record) {
      return record;
    }

    const output = record.get({ plain: true });

    const transaction = SequelizeRepository.getTransaction(options);

    output.orgLogo = await record.getOrgLogo({
      transaction,
    });
    output.parts = await record.getParts({
      transaction,
    });
    output.reports = await record.getReports({
      transaction,
    });
    output.templates = await record.getTemplates({
      transaction,
    });

    if (output.templates && output.templates.length > 0) {
      // Get Nested Association
      for (let i = 0; i < output.templates.length; i++) {
        output.templates[i].file = await output.templates[i].getFile({
          transaction,
        });
      }
    }

    return output;
  }
}

module.exports = OrganizationRepository;
