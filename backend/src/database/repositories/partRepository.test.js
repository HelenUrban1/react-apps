const SequelizeRepository = require('./sequelizeRepository');
const PartRepository = require('./partRepository');
const { data, createRepoInstance } = require('../../__fixtures__').part;
const config = require('../../../config')();

describe('PartRepository', () => {
  let repository;
  let auditLogSpy;
  const options = {
    currentUser: {
      accountId: config.testAccountId,
    },
  };

  beforeEach(async () => {
    await SequelizeRepository.cleanDatabase(options.currentUser.accountId);
    repository = createRepoInstance();
    auditLogSpy = jest.spyOn(repository, '_createAuditLog');
  });

  describe('#create()', () => {
    it('should create an item', async () => {
      const fixture = data[0];
      const item = await repository.create(fixture, options);

      expect(item.id).toBe(fixture.id);
    });

    it('should create an item and update a previousRevision', async () => {
      const fixture = data[0];
      const fixtureTwo = data[1];
      const item = await repository.create(fixture, options);

      const itemTwo = await repository.create({ ...fixtureTwo, previousRevision: item.id }, options);
      const updatedItem = await repository.findById(item.id, options);

      expect(item.id).toBe(fixture.id);
      expect(itemTwo.id).toBe(fixtureTwo.id);
      expect(itemTwo.previousRevision).toBe(fixture.id);
      expect(updatedItem.nextRevision).toBe(itemTwo.id);
    });
  });

  describe('#update()', () => {
    it('should update an item', async () => {
      const originalItem = await repository.create(data[0], options);

      const updatedName = 'Moe';
      const updatedItem = await repository.update(originalItem.id, { name: updatedName }, options);

      expect(originalItem.id).toBe(updatedItem.id);
      expect(updatedItem.name).toBe(updatedName);
    });
  });

  describe('#updateForUserAndTenant()', () => {
    it('should update an item in account', async () => {
      const originalItem = await repository.create(data[0], options);

      const updatedName = 'Moe';
      const updatedItem = await repository.updateForUserAndTenant(originalItem.id, config.testAccountId, { name: updatedName }, options);

      expect(originalItem.id).toBe(updatedItem.id);
      expect(updatedItem.name).toBe(updatedName);
    });
  });

  describe('#destroy()', () => {
    it('should remove an item', async () => {
      const originalItem = await repository.create(data[0], options);

      await repository.destroy(originalItem.id, options);

      const foundItem = await repository.findById(originalItem.id, options);
      expect(foundItem).toBe(null);
    });
  });

  describe('#findById()', () => {
    it('should find an existing item', async () => {
      const originalItem = await repository.create(data[0], options);

      const foundItem = await repository.findById(originalItem.id, options);
      expect(originalItem).toStrictEqual(foundItem);
    });
  });

  describe('#count()', () => {
    it('should count the total number of items', async () => {
      await repository.create(data[0], options);

      const count = await PartRepository.count(null, options);
      expect(count).toBe(1);
    });
  });

  describe('#findAndCountAll()', () => {
    it('should find and count all the items', async () => {
      const originalItem = await repository.create(data[0], options);

      const result = await repository.findAndCountAll({}, options);
      expect(result.count).toBe(1);
      expect(result.rows[0]).toStrictEqual(originalItem);
    });
  });

  describe('#findAllAutocomplete()', () => {
    it('should find all items for a list', async () => {
      const originalItem = await repository.create(data[0], options);

      const result = await repository.findAllAutocomplete(null, null, options);
      expect(result[0].id).toBe(originalItem.id);
    });
  });

  describe('#convertToInput', () => {
    it('should convert a part into a graphQL input', async () => {
      const originalItem = await repository.create(data[0], options);
      const input = PartRepository.convertToInput({ ...originalItem });
      expect(input.id).toBe(undefined);
      expect(input.originalPart).toBe(originalItem.id);
    });
  });

  describe('#markAsDeleted', () => {
    it('should update the desired part to have the "Deleted" status', async () => {
      const originalItem = await repository.create(data[0], options);

      await repository.markAsDeleted(originalItem.id, options);

      const foundItem = await repository.findById(originalItem.id, options);
      expect(foundItem.status).toEqual('Deleted');
    });
  });

  afterAll(async () => {
    await SequelizeRepository.closeConnections(options.currentUser.accountId);
    await SequelizeRepository.closeConnections();
  });
});
