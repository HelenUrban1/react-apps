const crypto = require('crypto');
const Sequelize = require('sequelize');
const models = require('../models');
const SequelizeRepository = require('./sequelizeRepository');
const UserProfileRepository = require('./userProfileRepository');
const AccountMemberRepository = require('./accountMemberRepository');
const FileRepository = require('./fileRepository');
const AuditLogRepository = require('./auditLogRepository');
const SequelizeFilterUtils = require('../utils/sequelizeFilterUtils');
const EmailSender = require('../../services/shared/email/emailSender');
const ValidationError = require('../../errors/validationError');
const CognitoAuthCommands = require('../../services/auth/cognitoAuthCommands');
const { log } = require('../../logger');
const config = require('../../../config')();
const { PAID_ROLES } = require('../constants').accountMember;

const { Op } = Sequelize;

/**
 * Handles database operations for Users.
 * See https://sequelize.org/v5/index.html to learn how to customize it.
 */

module.exports = class UserRepository {
  static async createUserProfile(id, data, options) {
    let profile = { ...data.profile };
    if (!profile) {
      profile = {};
    }
    // saving a copy of roles in the profile for reference in backups
    profile.roles = data.roles || null;
    await UserProfileRepository.create(id, profile, options).catch((err) => {
      log.error(err);
      log.debug({
        id,
        profile,
        options,
      });
    });
  }

  /**
   * Creates a user.
   */
  static async create(data, options = {}) {
    const currentUser = SequelizeRepository.getCurrentUser(options);

    const transaction = SequelizeRepository.getTransaction(options);

    const user = await models.user
      .create(
        {
          id: data.id || undefined,
          email: data.email,
          authenticationUid: data.authenticationUid || null,
          activeAccountMemberId: data.activeAccountMemberId || null,
          importHash: data.importHash || null,
          createdById: currentUser.id,
          updatedById: currentUser.id,
        },
        { transaction }
      )
      .catch((err) => {
        log.error(err);
        log.debug({
          id: data.id || undefined,
          email: data.email,
          authenticationUid: data.authenticationUid || null,
          activeAccountMemberId: data.activeAccountMemberId || null,
          importHash: data.importHash || null,
          createdById: currentUser.id,
          updatedById: currentUser.id,
          transaction,
        });
      });

    if (!user) {
      return null;
    }
    if (data.avatars) {
      await FileRepository.replaceRelationFiles(
        {
          belongsTo: models.user.getTableName(),
          belongsToColumn: 'avatars',
          belongsToId: user.id,
        },
        data.avatars,
        options
      ).catch((err) => {
        log.error(err);
        log.debug('Failed to replace files');
      });
    }
    await this.createUserProfile(user.id, data, options);

    await AuditLogRepository.log(
      {
        entityName: 'user',
        entityId: user.id,
        action: AuditLogRepository.CREATE,
        values: {
          ...user.get({ plain: true }),
          avatars: data.avatars,
          roles: data.roles || [],
        },
      },
      options
    );

    return this.findById(user.id, options);
  }

  /**
   * Creates the user based on the auth information.
   *
   * @param {Object} data
   * @param {Object} [options]
   */
  static async createFromAuth(data, options = {}) {
    const transaction = SequelizeRepository.getTransaction(options);
    // check for a user and if they exist update instead here
    const existingUser = await this.findByEmail(data.email, options);
    let user;
    if (existingUser) {
      // update the user and continue
      await this.updateUserOnly(existingUser.id, data, options);
      user = await models.user.findByPk(existingUser.id, {
        transaction,
      });
    } else {
      user = await models.user
        .create(
          {
            email: data.email,
            authenticationUid: data.authenticationUid,
            password: data.password,
            activeAccountMemberId: data.activeAccountMemberId || null,
            signupMeta: data.signupMeta || null,
          },
          { transaction }
        )
        .catch((err) => {
          log.error(err);
          log.debug({
            email: data.email,
            authenticationUid: data.authenticationUid,
            password: data.password,
            activeAccountMemberId: data.activeAccountMemberId || null,
            transaction,
          });
          throw new Error('Could not create user.');
        });
    }

    const accountMemberData = {
      user: user.id,
      account: data.accountId,
      site: data.siteId,
      settings: '{}',
      roles: data.roles || [], // '{Owner}',
      status: options.status || 'Active',
      // use control bool passed in, else if roles was passed in set to true, if roles wasn't passed in then this is for the Owner of a new account, set to false
      accessControl: data.control !== undefined ? data.control : !!data.roles,
    };
    const accountMember = await AccountMemberRepository.create(accountMemberData, options).catch((err) => {
      log.error(err);
      return null;
    });
    if (!accountMember) {
      await user.destroy();
      log.error('Account member creation failed. Associated user has been destroyed.');
      throw new Error('Experienced an error in account creation. Please try again later.');
    }

    await user
      .update(
        {
          authenticationUid: user.id,
          activeAccountMemberId: accountMember.id,
        },
        { transaction }
      )
      .catch((err) => {
        log.error(err);
        return null;
      });

    if (!data.profile) {
      data.profile = {
        firstName: data.firstName,
        lastName: data.lastName,
      };
    }

    if (options.accountId) {
      options.currentUser = {
        accountId: data.accountId,
      };
      // saving a copy of roles in the profile for reference in backups
      data.profile.roles = data.roles;

      const accountProfile = await UserProfileRepository.create(user.id, data, options).catch((err) => {
        log.error(err);
        return null;
      });
      if (!accountProfile) {
        await user.destroy();
        await accountMember.destroy();
        log.error('Account profile creation failed. Associated user and account member have been destroyed.');
        throw new Error('Experienced an error in account creation. Please try again later.');
      }
    }
    delete user.password;
    await AuditLogRepository.log(
      {
        entityName: 'user',
        entityId: user.id,
        action: AuditLogRepository.CREATE,
        values: {
          ...user.get({ plain: true }),
          avatars: data.avatars,
          roles: data.roles || [],
        },
      },
      options
    );

    return this.findById(user.id, options, options.currentUser);
  }

  /**
   * Creates the user based on the auth information with no Account membership
   * @param {C} id
   * @param {*} data
   * @param {*} options
   * @returns
   */
  static async createOrphanFromAuth(data, options = {}) {
    const transaction = SequelizeRepository.getTransaction(options);
    log.debug('createOrphan data');
    log.debug(data);
    const user = await models.user
      .create(
        {
          email: data.email,
          authenticationUid: data.authenticationUid || null,
          password: data.password,
          activeAccountMemberId: data.activeAccountMemberId || null,
          signupMeta: data.signupMeta || null,
        },
        { transaction }
      )
      .catch((err) => {
        log.error(err);
        log.debug({
          email: data.email,
          authenticationUid: data.authenticationUid,
          password: data.password,
          activeAccountMemberId: data.activeAccountMemberId || null,
          transaction,
        });
        throw new Error('Could not create user.');
      });

    return this.findById(user.id, { ...options, userOnly: true });
  }

  static async adoptOrphanToAccount(userData, data, options = {}) {
    const transaction = SequelizeRepository.getTransaction(options);

    const user = await models.user.findByPk(userData.id, {
      transaction,
    });

    const accountMemberData = {
      user: data.id,
      account: data.accountId,
      site: data.siteId,
      settings: '{}',
      roles: data.roles || [], // '{Owner}',
      status: options.status || 'Active',
      accessControl: options.accessControl || true,
    };
    const accountMember = await AccountMemberRepository.create(accountMemberData, options).catch((err) => {
      log.error(err);
      return null;
    });
    if (!accountMember || !user) {
      log.error('Account member creation failed. Associated user has been destroyed.');
      throw new Error('Experienced an error in account creation. Please try again later.');
    }

    const hasPassword = !!data.password || !!user.password;

    await user
      .update(
        {
          password: data.password || user.password,
          authenticationUid: data.id,
          activeAccountMemberId: accountMember.id,
          emailVerified: hasPassword,
          emailVerificationToken: hasPassword ? null : user.emailVerificationToken,
          emailVerificationTokenExpiresAt: hasPassword ? null : user.emailVerificationTokenExpiresAt,
          disabled: false,
        },
        { transaction }
      )
      .catch((err) => {
        log.error(err);
        return null;
      });

    if (accountMember.accountId) {
      options.currentUser = {
        accountId: accountMember.accountId,
      };

      const profileData = {
        fullName: '',
        firstName: data.firstName,
        lastName: data.lastName,
        roles: data.roles,
      };

      const accountProfile = await UserProfileRepository.create(user.id, profileData, options).catch((err) => {
        log.error(err);
        return null;
      });
      if (!accountProfile) {
        await user.destroy();
        await accountMember.destroy();
        log.error('Account profile creation failed. Associated user and account member have been destroyed.');
        throw new Error('Experienced an error in account creation. Please try again later.');
      }
    }

    delete user.password;
    await AuditLogRepository.log(
      {
        entityName: 'user',
        entityId: user.id,
        action: AuditLogRepository.CREATE,
        values: {
          ...user.get({ plain: true }),
          avatars: data.avatars,
          roles: data.roles || [],
        },
      },
      options
    );

    return this.findById(user.id, options, options.currentUser);
  }

  /**
   * Updates the password of the user.
   *
   * @param {*} id
   * @param {*} password
   * @param {*} [options]
   */
  static async updatePassword(id, password, options) {
    const currentUser = SequelizeRepository.getCurrentUser(options);

    const transaction = SequelizeRepository.getTransaction(options);

    const user = await models.user.findByPk(id, {
      transaction,
    });

    await user.update(
      {
        password,
        authenticationUid: id,
        updatedById: currentUser.id,
      },
      { transaction }
    );

    await AuditLogRepository.log(
      {
        entityName: 'user',
        entityId: user.id,
        action: AuditLogRepository.UPDATE,
        values: {
          id,
          authenticationUid: id,
        },
      },
      options
    );

    return this.findById(user.id, options);
  }

  /**
   * Generates the email verification token.
   *
   * @param {*} email
   * @param {*} [options]
   */
  static async generateEmailVerificationToken(email, options) {
    const currentUser = SequelizeRepository.getCurrentUser(options);

    const transaction = SequelizeRepository.getTransaction(options);

    const user = await models.user.findOne(
      {
        where: { email: { [Op.iLike]: `%${email}%` } },
      },
      {
        transaction,
      }
    );

    if (!user) {
      throw new ValidationError(options.language, 'auth.userNotFound');
    }
    const emailVerificationToken = crypto.randomBytes(20).toString('hex');
    const emailVerificationTokenExpiresAt = Date.now() + 1800000; // 360000; // 1360000 ms is 6 min, 30 min is 1,800,000 ms

    await user.update(
      {
        emailVerificationToken,
        emailVerificationTokenExpiresAt,
        updatedById: currentUser.id,
      },
      { transaction }
    );

    await AuditLogRepository.log(
      {
        entityName: 'user',
        entityId: user.id,
        action: AuditLogRepository.UPDATE,
        values: {
          id: user.id,
          emailVerificationToken,
          emailVerificationTokenExpiresAt,
        },
      },
      options
    );

    return emailVerificationToken;
  }

  /**
   * Generates the password reset token.
   *
   * @param {*} email
   * @param {*} [options]
   */
  static async generatePasswordResetToken(email, options) {
    const currentUser = SequelizeRepository.getCurrentUser(options);

    const transaction = SequelizeRepository.getTransaction(options);

    let user = await models.user.findOne(
      {
        where: { email: { [Op.iLike]: `%${email}%` } },
      },
      {
        transaction,
      }
    );

    if (!user || !user.emailVerified) {
      return { token: null, user };
    }

    const passwordResetToken = crypto.randomBytes(20).toString('hex');
    const passwordResetTokenExpiresAt = Date.now() + 360000;

    await user.update(
      {
        passwordResetToken,
        passwordResetTokenExpiresAt,
        updatedById: currentUser.id,
      },
      { transaction }
    );

    await AuditLogRepository.log(
      {
        entityName: 'user',
        entityId: user.id,
        action: AuditLogRepository.UPDATE,
        values: {
          id: user.id,
          passwordResetToken,
          passwordResetTokenExpiresAt,
        },
      },
      options
    );
    if (!options.userOnly) user = await this._fillWithRelationsAndFiles(user, options);
    return { token: passwordResetToken, user };
  }

  /**
   * Updates the status of the user: Disabled or Enabled.
   *
   * @param {*} id
   * @param {*} disabled
   * @param {*} [options]
   */
  static async updateStatus(id, disabled, options) {
    const currentUser = SequelizeRepository.getCurrentUser(options);

    const transaction = SequelizeRepository.getTransaction(options);

    const user = await models.user.findByPk(id, {
      transaction,
    });

    await user.update(
      {
        disabled,
        updatedById: currentUser.id,
      },
      { transaction }
    );

    await AuditLogRepository.log(
      {
        entityName: 'user',
        entityId: user.id,
        action: AuditLogRepository.UPDATE,
        values: {
          id,
          disabled,
        },
      },
      options
    );

    return this.findById(user.id, options);
  }

  /**
   * Updates the roles of the user.
   *
   * @param {String} id
   * @param {Object} roles
   * @param {Object} [options]
   */
  static async updateRoles(id, roles = [], options = {}) {
    const currentUser = SequelizeRepository.getCurrentUser(options);
    const transaction = SequelizeRepository.getTransaction(options);

    const user = await models.user.findByPk(id, {
      transaction,
    });

    if (options.addRoles) {
      await AccountMemberRepository.addRoles(user.activeAccountMemberId, roles, options);
    } else if (options.removeOnlyInformedRoles) {
      await AccountMemberRepository.removeRoles(user.activeAccountMemberId, roles, options);
    } else {
      await AccountMemberRepository.resetRoles(user.activeAccountMemberId, roles, options);
    }

    const newRoles = await UserRepository.findAllByUser(user.id, options);

    // Add the current set of roles to the profile so they are stored with the account db for audit purposes
    await UserProfileRepository.updateRoles(user.id, newRoles, options);

    await AuditLogRepository.log(
      {
        entityName: 'user',
        entityId: user.id,
        action: AuditLogRepository.UPDATE,
        values: {
          roles,
        },
      },
      options
    );

    return this.findById(user.id, options);
  }

  /**
   * Updates the profile of the user.
   *
   * @param {String} id
   * @param {Object} data
   * @param {Object} [options]
   */
  static async updateProfile(id, data, options = {}) {
    const transaction = SequelizeRepository.getTransaction(options);

    const user = await models.user.findByPk(id, {
      transaction,
    });

    await UserProfileRepository.update(user.id, data, options);

    await FileRepository.replaceRelationFiles(
      {
        belongsTo: models.user.getTableName(),
        belongsToColumn: 'avatars',
        belongsToId: user.id,
      },
      data.avatars,
      options
    );

    const accountMember = await this.getActiveAccountMemberRecord(user, options);
    const roles = accountMember ? accountMember.roles : [];

    await AuditLogRepository.log(
      {
        entityName: 'user',
        entityId: user.id,
        action: AuditLogRepository.UPDATE,
        values: {
          ...user.get({ plain: true }),
          avatars: data.avatars,
          roles,
        },
      },
      options
    );

    return this.findById(user.id, options, user);
  }

  /**
   * Updates a User.
   *
   * @param {String} id
   * @param {Object} data
   * @param {Object} [options]
   */
  static async update(id, data, options) {
    const currentUser = SequelizeRepository.getCurrentUser(options);

    const transaction = SequelizeRepository.getTransaction(options);

    const user = await models.user.findByPk(id, {
      transaction,
    });

    const roles = data.roles || [];

    await UserProfileRepository.update(user.id, data, options);

    await user.update(
      {
        activeAccountMemberId: data.activeAccountMemberId || null,
        updatedById: currentUser.id,
      },
      { transaction }
    );

    if (data.avatars) {
      await FileRepository.replaceRelationFiles(
        {
          belongsTo: models.user.getTableName(),
          belongsToColumn: 'avatars',
          belongsToId: user.id,
        },
        data.avatars,
        options
      );
    }

    // Only reset roles if roles are provided to prevent wiping out existing roles
    // unintentionally.
    if (Array.isArray(data.roles)) {
      await AccountMemberRepository.resetRoles(user.activeAccountMemberId, roles, options);
    }

    await AuditLogRepository.log(
      {
        entityName: 'user',
        entityId: user.id,
        action: AuditLogRepository.UPDATE,
        values: {
          ...user.get({ plain: true }),
          avatars: data.avatars,
          roles,
        },
      },
      options
    );

    return this.findById(user.id, options);
  }

  /**
   * Updates the User only
   * @param {*} id
   * @param {*} data
   * @param {*} options
   * @returns
   */
  static async updateUserOnly(id, data, options) {
    const currentUser = SequelizeRepository.getCurrentUser(options);

    const transaction = SequelizeRepository.getTransaction(options);

    const user = await models.user.findByPk(id, {
      transaction,
    });

    if (!user) {
      log.error('Target user was not returned');
      throw new ValidationError(options.language, 'iam.errors.userNotFound');
    }

    await user.update(
      {
        ...data,
        updatedById: currentUser.id,
      },
      { transaction }
    );

    await AuditLogRepository.log(
      {
        entityName: 'user',
        entityId: user.id,
        action: AuditLogRepository.UPDATE,
        values: {
          ...user.get({ plain: true }),
        },
      },
      options
    );

    return this.findById(user.id, options);
  }

  /**
   * Sets the user's activeAccountMemberId.
   *
   * @param {String} id
   * @param {String} activeAccountMemberId
   * @param {Object} [options]
   */
  static async setActiveAccountMembership(id, activeAccountMemberId, options) {
    const currentUser = SequelizeRepository.getCurrentUser(options);

    const transaction = SequelizeRepository.getTransaction(options);

    const user = await models.user.findByPk(id, {
      transaction,
    });

    await user.update(
      {
        activeAccountMemberId: activeAccountMemberId || null,
        updatedById: currentUser.id,
      },
      { transaction }
    );

    await AuditLogRepository.log(
      {
        entityName: 'user',
        entityId: user.id,
        action: AuditLogRepository.UPDATE,
        values: {
          ...user.get({ plain: true }),
        },
      },
      options
    );

    return this.findById(user.id, options);
  }

  /**
   * Finds the user by email.
   *
   * @param {String} email
   * @param {Object} [options]
   */
  static async findByEmail(email, options) {
    const transaction = SequelizeRepository.getTransaction(options);
    const query = { where: { email: { [Op.iLike]: `%${email}%` } } };
    if (options.attributes) {
      query.attributes = options.attributes;
    }
    const record = await models.user.findOne(query, { transaction });
    if (options && options.userOnly) return record;

    return this._fillWithRelationsAndFiles(record, options);
  }

  /**
   * Find the user by email, but without fetching the avatar.
   *
   * @param {String} email
   * @param {Object} [options]
   */
  static async findByEmailWithoutAvatar(email, options = {}) {
    const query = { where: { email: { [Op.iLike]: `%${email}%` } } };
    if (options.attributes) {
      query.attributes = options.attributes;
    }
    const record = await models.user.findOne(query);
    if (record && !EmailSender.isConfigured) {
      if (record.user) {
        record.user.emailVerified = true;
      } else {
        record.emailVerified = true;
      }
    }

    try {
      const fullRecord = await this._fillWithRelationsAndFiles(record, options);
      if (fullRecord && fullRecord.accountMemberships && fullRecord.accountMemberships.length === 0) {
        fullRecord.orphan = true;
      }
      return this._fillWithRelationsAndFiles(record, options);
    } catch (error) {
      log.error(error);
      return record;
    }
  }

  static setIDFilter(filter, where) {
    let newWhere = { ...where };
    if (filter?.id) {
      newWhere = {
        ...where,
        id: SequelizeFilterUtils.uuid(filter.id),
      };
    }
    return newWhere;
  }

  static setFullNameFilter(filter, where) {
    let newWhere = { ...where };
    if (filter.fullName) {
      newWhere = {
        ...where,
        [Op.and]: SequelizeFilterUtils.ilike('user', 'fullName', filter.fullName),
      };
    }
    return newWhere;
  }

  static setEmailFilter(filter, where) {
    let newWhere = { ...where };
    if (filter?.email) {
      newWhere = {
        ...where,
        [Op.and]: SequelizeFilterUtils.ilike('user', 'email', filter.email),
      };
    }
    return newWhere;
  }

  static setStatusFilter(filter, where) {
    let newWhere = { ...where };
    if (filter?.status) {
      const disabled = filter.status === 'disabled';

      newWhere = {
        ...where,
        disabled,
      };
    }
    return newWhere;
  }

  static setCreatedAtRangeFilter(filter, where) {
    let newWhere = { ...where };
    if (filter?.createdAtRange) {
      const [start, end] = filter.createdAtRange;

      if (start !== undefined && start !== null && start !== '') {
        newWhere = {
          ...newWhere,
          createdAt: {
            ...newWhere.createdAt,
            [Op.gte]: start,
          },
        };
      }

      if (end !== undefined && end !== null && end !== '') {
        newWhere = {
          ...newWhere,
          createdAt: {
            ...newWhere.createdAt,
            [Op.lte]: end,
          },
        };
      }
    }
    return newWhere;
  }

  static setFilters(filter) {
    let where = {};

    where = this.setIDFilter(filter, where);

    // TODO: Search profile from user
    // where = this.setFullNameFilter(filter, where);

    where = this.setEmailFilter(filter, where);

    where = this.setStatusFilter(filter, where);

    where = this.setCreatedAtRangeFilter(filter, where);

    return where;
  }

  static setAccountScope(currentUser, includes = []) {
    // TODO: This is a messy way to extract accounts. We should have a standard representation of the user object.
    if (!currentUser) return includes;
    let accountId;
    if (currentUser && currentUser.accountId) {
      accountId = currentUser.accountId;
    } else if (currentUser.account) {
      accountId = currentUser.account.id;
    }
    if (!accountId) {
      return includes;
    }

    const newInclude = [
      ...includes,
      {
        model: models.accountMember,
        as: 'accountMemberships',
        where: { accountId: accountId || null }, //
      },
    ];
    return newInclude;
  }

  /**
   * Finds the user based on the query.
   *
   * @param {Object} query
   * @param {Object} query.filter
   * @param {number} query.limit
   * @param  {number} query.offset
   * @param  {string} query.orderBy
   *
   * @returns {Promise<Object>} response - Object containing the rows and the count.
   */
  static async findAndCountAll(
    { filter, limit, offset, orderBy } = {
      filter: null,
      limit: 0,
      offset: 0,
      orderBy: null,
    },
    options,
    currentUser
  ) {
    const transaction = SequelizeRepository.getTransaction(options);

    const where = this.setFilters(filter);
    const include = this.setAccountScope(currentUser);

    let { rows, count } = await models.user.findAndCountAll({
      where,
      include,
      limit: limit ? Number(limit) : undefined,
      offset: offset ? Number(offset) : undefined,
      order: orderBy ? [orderBy.split('_')] : [['createdAt', 'DESC']],
      transaction,
    });
    rows = await this._fillWithRelationsAndFilesForRows(rows, options);

    return { rows, count };
  }

  /**
   * Lists the users to populate the autocomplete.
   *
   * @param {Object} query
   * @param {number} limit
   */
  static async findAllAutocomplete(query, limit, options, currentUser) {
    let where = {};

    if (query) {
      where = {
        [Op.or]: [{ id: SequelizeFilterUtils.uuid(query) }, SequelizeFilterUtils.ilike('user', 'email', query)],
      };
    }
    const include = this.setAccountScope(currentUser);

    const users = await models.user.findAll(
      {
        attributes: ['id', 'email'],
        where,
        include,
        limit: limit ? Number(limit) : undefined,
        orderBy: [['email', 'ASC']],
      },
      options
    );

    const buildText = (user) => {
      if (!user.fullName) {
        return user.email;
      }

      return user.email;
    };
    if (!users || users.length <= 0) {
      log.error('No users');
      throw new Error('Sorry, something went wrong');
    }
    return users.map((user) => ({
      id: user.id,
      label: buildText(user),
    }));
  }

  /**
   * Finds the user and all its relations.
   *
   * @param {String} id
   * @param {Object} [options]
   */
  static async findById(id, options = {}) {
    const transaction = SequelizeRepository.getTransaction(options);

    const record = await models.user.findByPk(id, {
      transaction,
    });

    if (options.userOnly) {
      return record;
    }

    return this._fillWithRelationsAndFiles(record, options);
  }

  /**
   * Finds the user, without fetching the avatar.
   *
   * @param {string} id
   * @param {Object} [options]
   */
  static async findByIdWithoutAvatar(id, options) {
    const record = await models.user.findByPk(id);

    try {
      const filledRecord = await this._fillWithRelationsAndFiles(record, options);
      return filledRecord;
    } catch (error) {
      log.error(error);
      return record;
    }
  }

  /**
   * Finds the users with the ids and filters based on the disabled flag.
   *
   * @param {*} ids
   * @param {*} disabled
   * @param {*} [options]
   */
  static async findAllByDisabled(ids, disabled, options, currentUser) {
    const transaction = SequelizeRepository.getTransaction(options);
    const include = this.setAccountScope(currentUser);

    const users = await models.user.findAll({
      where: {
        id: {
          [Op.in]: ids,
        },
        disabled: !!disabled,
      },
      include,
      transaction,
    });

    return users;
  }

  /**
   * Finds the users with the ids and filters based on the disabled flag.
   *
   * @param {*} data
   * @param {*} [options]
   */
  static async findAllIncomplete(data, options, currentUser) {
    const transaction = SequelizeRepository.getTransaction(options);
    const include = this.setAccountScope(currentUser);
    const users = await models.user.findAll({
      where: {
        emailVerificationTokenExpiresAt: {
          [Op.gte]: data.emailVerificationTokenExpiresAt,
        },
      },
      include,
      transaction,
    });

    return users;
  }

  /**
   * Finds the user by the password token if not expired.
   *
   * @param {String} token
   * @param {Object} [options]
   */
  static async findByPasswordResetToken(token, options, currentUser) {
    const transaction = SequelizeRepository.getTransaction(options);
    const include = this.setAccountScope(currentUser);

    const record = await models.user.findOne(
      {
        where: {
          passwordResetToken: token,
          passwordResetTokenExpiresAt: {
            [Op.gt]: Date.now(),
          },
        },
        include,
      },
      { transaction }
    );

    return options.userOnly ? record : this._fillWithRelationsAndFiles(record, options);
  }

  /**
   * Finds the user by the email verification token if not expired.
   *
   * @param {String} token
   * @param {Object} [options]
   */
  static async findByEmailVerificationToken(token, options, currentUser) {
    const transaction = SequelizeRepository.getTransaction(options);
    const include = this.setAccountScope(currentUser);
    const where = {
      emailVerificationToken: token,
      emailVerificationTokenExpiresAt: {
        [Op.gt]: Date.now(),
      },
    };

    if (!options.ignoreVerification) where.emailVerified = false;

    return models.user
      .findOne(
        {
          where,
          include,
        },
        { transaction }
      )
      .then(async (record) => {
        if (options.userOnly) return record;
        return this._fillWithRelationsAndFiles(record, options);
      })
      .catch((err) => {
        log.error(err);
        return null;
      });
  }

  /**
   * Finds the user by the email verification token if not expired, or if the account is disabled
   *
   * @param {String} token
   * @param {Object} [options]
   */
  static async findValidTokenOrCanceledInvite(token, options, currentUser) {
    const transaction = SequelizeRepository.getTransaction(options);
    const include = this.setAccountScope(currentUser);
    return models.user
      .findOne(
        {
          where: {
            emailVerificationToken: token,
            [Op.or]: [
              {
                disabled: true,
              },
              {
                emailVerificationTokenExpiresAt: {
                  [Op.gt]: Date.now(),
                },
              },
            ],
          },
          include,
        },
        { transaction }
      )
      .then(async (record) => {
        return this._fillWithRelationsAndFiles(record, options);
      })
      .catch((err) => {
        log.error(err);
        return null;
      });
  }

  /**
   * Finds all users who have a role.
   *
   * @param {Object}
   * @prop  {String} role
   * @prop  {String} [orderBy]
   * @prop  {Object} [options]
   * @return {Promise<Array<User>>}
   */
  static async findAllByRole({ role, orderBy, options }, currentUser) {
    const { rows, count } = await AccountMemberRepository.findAndCountAll({
      orderBy,
      filter: {
        roles: [role],
        account: currentUser.accountId,
      },
      options,
    });

    if (count === 0) {
      return [];
    }

    if (!rows || rows.length <= 0) {
      log.error('No users');
      throw new Error('Sorry, something went wrong');
    }
    const users = await Promise.all(
      rows
        .map((row) => {
          if (row.userId) {
            const user = this.findById(row.userId, options);
            return user;
          }
          return null;
        })
        .filter((row) => row !== null)
    );

    if (!users || users.length <= 0) {
      log.error('No users');
    }
    return users;
  }

  /**
   * Finds all users who have any of the provided roles.
   *
   * @param {Object}
   * @prop  {array} roles
   * @prop  {String} [orderBy]
   * @prop  {Object} [options]
   * @return {Promise<Array<User>>}
   */
  static async findAllByRolesArray({ roles, orderBy, options }, currentUser) {
    const { rows, count } = await AccountMemberRepository.findAndCountAll({
      orderBy,
      filter: {
        roles,
        account: currentUser.accountId,
      },
      options,
    });

    if (count === 0) {
      return [];
    }

    if (!rows || rows.length <= 0) {
      log.error('No users');
      throw new Error('Sorry, something went wrong');
    }

    const users = rows.filter((row) => row.userId).map((row) => row.user);

    if (!users || users.length <= 0) {
      log.error('No users');
    }

    return users;
  }

  static async findAllByRolesArrayInAccount({ roles, orderBy, accountId, options }) {
    const { rows, count } = await AccountMemberRepository.findAndCountAll({
      orderBy,
      filter: {
        roles,
        account: accountId,
      },
      options,
    });

    if (count === 0) {
      return [];
    }

    if (!rows || rows.length <= 0) {
      log.error('No users');
      throw new Error('Sorry, something went wrong');
    }

    const users = rows.filter((row) => row.userId).map((row) => row.user);

    if (!users || users.length <= 0) {
      log.error('No users');
    }

    return users;
  }

  /**
   * Finds all users who have a role and formats the results to conform to
   * the schema: [RoleWithUsers!]!
   *
   * @param {Object}
   * @prop  {Object} filter
   * @prop  {String} [orderBy]
   * @prop  {Object} [options]
   * @return {Promise<[RoleWithUsers!]!>}
   */
  static async findAllByRoleWithRole({ filter, orderBy, options }, currentUser) {
    const { role } = filter;
    const users = await this.findAllByRole({ role, orderBy, options }, currentUser);

    if (users.length === 0) {
      return users;
    }

    return [
      {
        role,
        users,
      },
    ];
  }

  /**
   * Marks the user email as verified.
   *
   * @param {*} id
   * @param {*} [options]
   */
  static async markEmailVerified(id, options) {
    const currentUser = SequelizeRepository.getCurrentUser(options);

    const transaction = SequelizeRepository.getTransaction(options);

    const user = await models.user.findByPk(id, {
      transaction,
    });

    await user.update(
      {
        emailVerified: true,
        emailVerificationToken: null,
        emailVerificationTokenExpiresAt: null,
        disabled: false,
        updatedById: currentUser.id,
      },
      { transaction }
    );

    await AuditLogRepository.log(
      {
        entityName: 'user',
        entityId: user.id,
        action: AuditLogRepository.UPDATE,
        values: {
          id,
          emailVerified: true,
        },
      },
      options
    );

    return user;
  }

  /**
   * Counts the users based on the filter.
   *
   * @param {*} [filter]
   * @param {*} [options]
   */
  static async count(filter, options, currentUser) {
    const transaction = SequelizeRepository.getTransaction(options);
    console.log('userRepository.count currentUser', currentUser);
    const include = this.setAccountScope(currentUser);

    return models.user.count(
      {
        where: filter,
        include,
      },
      { transaction }
    );
  }

  /**
   * Find the user's active AccountMember record.
   *
   * @param {User|Object} userRecord
   * @param {Object} [options]
   * @return {Promise<AccountMember|null>}
   */
  static async getActiveAccountMemberRecord(userRecord, options) {
    if (!userRecord) {
      return userRecord;
    }

    if (!userRecord.activeAccountMemberId) {
      return null;
    }

    const isUserModelInstance = userRecord instanceof models.user;
    if (!isUserModelInstance) {
      const transaction = SequelizeRepository.getTransaction(options);
      userRecord = await models.user.findByPk(userRecord.id, {
        transaction,
      });
      if (!userRecord) {
        return userRecord;
      }
    }

    const accountMemberRecords = await userRecord.getAccountMemberships();

    const activeRecord = accountMemberRecords.find((record) => record.id === userRecord.activeAccountMemberId);
    if (!activeRecord) {
      return null;
    }

    return activeRecord;
  }

  /**
   * Fills the users with the relations and files.
   *
   * @param {Array<Object>} rows
   * @param {Object} [options]
   */
  static async _fillWithRelationsAndFilesForRows(rows, options) {
    if (!rows) {
      return rows;
    }
    return Promise.all(rows.map((record) => this._fillWithRelationsAndFiles(record, options)));
  }

  static assignAccountMember(output, accountMembers) {
    if (!accountMembers?.rows || accountMembers.rows.length <= 0) {
      return output;
    }
    const newOutput = { ...output };
    const currentMembership = accountMembers.rows[0];
    newOutput.accountId = currentMembership.accountId;
    newOutput.siteId = currentMembership.siteId;
    /*
       NOTE: To add additional details to currentUser
         Add values to output below
         Update GraphQL schema at backend/src/api/auth/types/userWithRoles.js
         Update GraphQL resolver (if necessary) at backend/src/api/auth/queries/authMe.js
    */

    newOutput.accountName = currentMembership?.account?.orgName;
    newOutput.accountSettings = currentMembership?.account?.settings;
    newOutput.siteName = currentMembership?.site?.siteName;
    newOutput.roles = currentMembership?.roles || [];
    newOutput.activeAccountId = currentMembership?.accountId || '';
    newOutput.status = newOutput.status || currentMembership?.status || '';
    newOutput.account = currentMembership.account;
    newOutput.paid = newOutput.roles.some((role) => PAID_ROLES.includes(role));

    return newOutput;
  }

  static assignAccountProfile(output, profile) {
    const newOutput = { ...output };

    newOutput.fullName = profile?.fullName || newOutput.fullName;
    newOutput.firstName = profile?.firstName || newOutput.firstName;
    newOutput.lastName = profile?.lastName || newOutput.lastName;
    newOutput.phoneNumber = profile?.phoneNumber || newOutput.phoneNumber;
    newOutput.avatars = profile?.avatars || newOutput.avatars;

    return newOutput;
  }

  static assignNewOptions(output, options = {}) {
    let newOptions = { ...options };
    if (!newOptions) {
      newOptions = {
        currentUser: {
          accountId: output.accountId,
          siteId: output.siteId,
        },
      };
    } else if (!newOptions.currentUser) {
      newOptions.currentUser = {
        accountId: output.accountId,
        siteId: output.siteId,
      };
    } else if (!newOptions.currentUser.accountId) {
      newOptions.currentUser.accountId = output.accountId;
      newOptions.currentUser.siteId = output.siteId;
    }
    return newOptions;
  }

  /**
   * Fills a user with the relations and files.
   *
   * @param {User} record
   * @param {Object} [options]
   */
  static async _fillWithRelationsAndFiles(record, options) {
    if (!record) {
      return record;
    }
    try {
      let output = record.get({ plain: true });
      let newOptions = { ...options };

      // Fetch user avatars
      output.avatars = await record
        .getAvatars({
          transaction: SequelizeRepository.getTransaction(options),
        })
        .catch((err) => {
          log.warn(`Failed to fill avatar relation: ${err}`);
          return null;
        });

      // Fetch account memberships
      output.accountMemberships = await record.getAccountMemberships().catch((err) => {
        log.warn(`Failed to fill accountMember relation: ${err}`);
        return null;
      });
      const accountMembers = await AccountMemberRepository.findAndCountAll(
        {
          filter: {
            user: record.id,
          },
        },
        newOptions
      ).catch((err) => {
        log.warn(`Failed to fill accountMember relation: ${err}`);
        return null;
      });
      // When users can be in more than one site or account, this will need
      // to change to accommodate that. For now it just takes the first record.
      output = this.assignAccountMember(output, accountMembers);
      newOptions = this.assignNewOptions(output, newOptions);

      // Fetch user profile
      const profile = await UserProfileRepository.findByUserId(record.id, newOptions).catch((err) => {
        // If there is no profile b/c this is an orphan, continue processing
        log.warn(`Failed to fill userProfile relation: ${err}`);
        return null;
      });
      output.profile = output.profile || profile;
      output = this.assignAccountProfile(output, profile);

      if (config && config.jwt.authStrategy === 'cognito' && !!options.cognitoClient) {
        const cognitoUser = await CognitoAuthCommands.cognitoGetUser(record.email, options);
        output.mfaRegistered = cognitoUser && cognitoUser.PreferredMfaSetting === 'SOFTWARE_TOKEN_MFA';
      }

      return output;
    } catch (err) {
      // Don't want the user to error out if we can't retrieve the profile, but will log the error to determine why
      console.log(err);
      log.warn(`Failed to fill accountMember relation: ${err}`);
    }
  }
};
