const SequelizeRepository = require('./sequelizeRepository');

describe('sequelizeRepository', () => {
  describe('getCurrentUser', () => {
    it('Returns a currentUser from options when it exists on options', () => {
      const options = {
        currentUser: {
          id: '25a24a64-d551-4b33-8576-91f814c43f67',
          siteId: 'c4407aed-d8ea-40c4-8654-bf63e1af8754',
          accountId: '9951c535-acdd-430a-bf7f-875d055ad91a',
        },
      };
      const currentUser = SequelizeRepository.getCurrentUser(options);
      expect(currentUser).toBe(options.currentUser);
    });
    it('Returns a null id, siteId and accountId when currentUser does not exist on options', () => {
      const options = {};
      const currentUser = SequelizeRepository.getCurrentUser(options);
      expect(currentUser.id).toBe(null);
      expect(currentUser.siteId).toBe(null);
      expect(currentUser.accountId).toBe(null);
    });
  });

  describe('getTransaction', () => {
    it('Returns a transaction from options when it exists on options', () => {
      const options = {
        transaction: {
          id: 'a5001533-07e5-41bf-9796-fdd5bf97dff4',
        },
      };
      const transaction = SequelizeRepository.getTransaction(options);
      expect(transaction).toBe(options.transaction);
    });
    it('Returns undefined when transaction does not exist on options', () => {
      const options = {};
      const transaction = SequelizeRepository.getTransaction(options);
      expect(transaction).toBe(undefined);
    });
  });
});
