const pick = require('lodash/pick');
const models = require('../models');
const SequelizeRepository = require('./sequelizeRepository');
const AuditLogRepository = require('./auditLogRepository');
const SequelizeFilterUtils = require('../utils/sequelizeFilterUtils');
const AccountMemberRepository = require('./accountMemberRepository');
const PartRepository = require('./partRepository');
const DrawingRepository = require('./drawingRepository');
const { PAID_ROLES } = require('../constants').accountMember;

const { Sequelize } = models;
const { Op } = Sequelize;

/**
 * Handles database operations for the Subscription.
 * See https://sequelize.org/v5/index.html to learn how to customize it.
 */
module.exports = class SubscriptionRepository {
  /**
   * Creates the Subscription.
   *
   * @param {Object} data
   * @param {Object} [options]
   */
  async create(data, options) {
    const currentUser = SequelizeRepository.getCurrentUser(options);

    const transaction = SequelizeRepository.getTransaction(options);

    let returnedData = { ...data };

    const { accountId, billingId, companyId } = returnedData;
    returnedData = models.subscription.translateChargifyFieldsToIxc(returnedData);
    returnedData = models.subscription.prepareProviderId(returnedData);
    returnedData = models.subscription.preparePaymentInfo(returnedData);
    returnedData = models.subscription.prepareComponentInfo(returnedData);

    this._convertEmptyArrayToNull(returnedData, 'couponCodes');

    returnedData.accountId = accountId || currentUser.accountId;
    returnedData.billingId = billingId;
    returnedData.companyId = companyId?.toString();

    const record = await models.subscription.create(
      {
        ...pick(returnedData, models.subscription.FIELD_NAMES),
        createdById: currentUser.id,
        updatedById: currentUser.id,
      },
      {
        transaction,
      }
    );

    await this._createAuditLog(AuditLogRepository.CREATE, record, returnedData, options);

    return this.findById(record.id, options);
  }

  /**
   * Updates the Subscription.
   *
   * @param {Object} data
   * @param {Object} [options]
   */
  async update(id, data, options) {
    const currentUser = SequelizeRepository.getCurrentUser(options);

    const transaction = SequelizeRepository.getTransaction(options);

    let record = await models.subscription.findByPk(id, {
      transaction,
    });
    let returnedData = { ...data };
    returnedData = models.subscription.translateChargifyFieldsToIxc(returnedData);
    returnedData = models.subscription.prepareProviderId(returnedData);
    returnedData = models.subscription.preparePaymentInfo(returnedData);
    if (returnedData.components) {
      returnedData = models.subscription.prepareComponentInfo(returnedData);
    }
    this._convertEmptyArrayToNull(returnedData, 'couponCodes');

    record = await record.update(
      {
        ...pick(returnedData, models.subscription.FIELD_NAMES),
        updatedById: currentUser.id,
      },
      {
        transaction,
      }
    );

    await this._createAuditLog(AuditLogRepository.UPDATE, record, returnedData, options);

    return this.findById(record.id, options);
  }

  /**
   * Deletes the Subscription.
   *
   * @param {string} id
   * @param {Object} [options]
   */
  async destroy(id, options) {
    const transaction = SequelizeRepository.getTransaction(options);

    const record = await models.subscription.findByPk(id, {
      transaction,
    });

    await record.destroy({
      transaction,
    });

    await this._createAuditLog(AuditLogRepository.DELETE, record, null, options);
  }

  /**
   * Finds the Subscription and its relations.
   *
   * @param {string} id
   * @param {Object} [options]
   */
  async findById(id, options) {
    const transaction = SequelizeRepository.getTransaction(options);

    const include = [];

    const record = await models.subscription.findByPk(id, {
      include,
      transaction,
    });
    if (!record) {
      return null;
    }
    const relationOptions = { ...options };
    if (!relationOptions?.accountId) {
      relationOptions.accountId = record.accountId;
    }
    if (!relationOptions?.currentUser) {
      relationOptions.currentUser = { accountId: relationOptions.accountId };
    }
    return this._fillWithRelationsAndFiles(record, relationOptions);
  }

  /**
   * Finds the Subscription and its relations.
   *
   * @param {string} id
   * @param {Object} [options]
   */
  async findOne(filters, options) {
    const include = [];

    const record = await models.subscription.findOne({ ...filters, include });

    return this._fillWithRelationsAndFiles(record, options);
  }

  /**
   * Finds the Subscription and its relations.
   *
   * @param {string} accountId
   * @param {Object} [options]
   */
  async findByAccountId(accountId, options) {
    const include = [];
    const where = {
      accountId,
    };
    const relationOptions = { ...options };
    if (!relationOptions.accountId) {
      relationOptions.accountId = accountId;
    }

    const record = await models.subscription.findOne({ where, include });

    return this._fillWithRelationsAndFiles(record, relationOptions);
  }

  /**
   * Counts the number of Subscriptions based on the filter.
   *
   * @param {Object} filter
   * @param {Object} [options]
   */
  async count(filter, options) {
    const transaction = SequelizeRepository.getTransaction(options);

    return models.subscription.count(
      {
        where: filter,
      },
      {
        transaction,
      }
    );
  }

  /**
   * Finds the Subscriptions based on the query.
   * See https://sequelize.org/v5/manual/querying.html to learn how to
   * customize the query.
   *
   * @param {Object} query
   * @param {Object} query.filter
   * @param {number} query.limit
   * @param {number} query.offset
   * @param {string} query.orderBy
   * @param {Object} [options]
   *
   * @returns {Promise<Object>} response - Object containing the rows and the count.
   */
  async findAndCountAll({ filter, limit = 0, offset = 0, orderBy, withRelations = true, ...options }) {
    let where = {};
    const include = [];

    if (filter) {
      if (filter.id) {
        where = {
          ...where,
          id: SequelizeFilterUtils.uuid(filter.id),
        };
      }

      if (filter.account) {
        where = {
          ...where,
          accountId: SequelizeFilterUtils.uuid(filter.account),
        };
      }

      if (filter.paymentSystemId) {
        where = {
          ...where,
          paymentSystemId: filter.paymentSystemId,
        };
      }

      if (filter.seatsUsedRange) {
        const [start, end] = filter.seatsUsedRange;

        if (start !== undefined && start !== null && start !== '') {
          where = {
            ...where,
            paidUserSeatsUsed: {
              ...where.paidUserSeatsUsed,
              [Op.gte]: start,
            },
          };
        }

        if (end !== undefined && end !== null && end !== '') {
          where = {
            ...where,
            paidUserSeatsUsed: {
              ...where.paidUserSeatsUsed,
              [Op.lte]: end,
            },
          };
        }
      }

      if (filter.partsUsedRange) {
        const [start, end] = filter.partsUsedRange;

        if (start !== undefined && start !== null && start !== '') {
          where = {
            ...where,
            partsUsed: {
              ...where.partsUsed,
              [Op.gte]: start,
            },
          };
        }

        if (end !== undefined && end !== null && end !== '') {
          where = {
            ...where,
            partsUsed: {
              ...where.partsUsed,
              [Op.lte]: end,
            },
          };
        }
      }

      if (filter.drawingsUsedRange) {
        const [start, end] = filter.drawingsUsedRange;

        if (start !== undefined && start !== null && start !== '') {
          where = {
            ...where,
            drawingsUsed: {
              ...where.drawingsUsed,
              [Op.gte]: start,
            },
          };
        }

        if (end !== undefined && end !== null && end !== '') {
          where = {
            ...where,
            drawingsUsed: {
              ...where.drawingsUsed,
              [Op.lte]: end,
            },
          };
        }
      }

      if (filter.measurementsUsedRange) {
        const [start, end] = filter.measurementsUsedRange;

        if (start !== undefined && start !== null && start !== '') {
          where = {
            ...where,
            measurementsUsed: {
              ...where.measurementsUsed,
              [Op.gte]: start,
            },
          };
        }

        if (end !== undefined && end !== null && end !== '') {
          where = {
            ...where,
            measurementsUsed: {
              ...where.measurementsUsed,
              [Op.lte]: end,
            },
          };
        }
      }

      if (filter.enforcePlan === true || filter.enforcePlan === 'true' || filter.enforcePlan === false || filter.enforcePlan === 'false') {
        where = {
          ...where,
          enforcePlan: filter.enforcePlan === true || filter.enforcePlan === 'true',
        };
      }

      if (filter.providerProductId) {
        where = {
          ...where,
          providerProductId: filter.providerProductId,
        };
      }

      if (filter.providerProductHandle) {
        where = {
          ...where,
          providerProductHandle: filter.providerProductHandle,
        };
      }

      if (filter.billingPeriod) {
        where = {
          ...where,
          billingPeriod: filter.billingPeriod,
        };
      }

      if (filter.status) {
        where = {
          ...where,
          status: filter.status,
        };
      }

      if (filter.previousStatus) {
        where = {
          ...where,
          status: filter.previousStatus,
        };
      }

      if (filter.balanceInCentsRange) {
        const [start, end] = filter.balanceInCentsRange;

        if (start !== undefined && start !== null && start !== '') {
          where = {
            ...where,
            balanceInCents: {
              ...where.balanceInCents,
              [Op.gte]: start,
            },
          };
        }

        if (end !== undefined && end !== null && end !== '') {
          where = {
            ...where,
            balanceInCents: {
              ...where.balanceInCents,
              [Op.lte]: end,
            },
          };
        }
      }

      if (filter.totalRevenueInCentsRange) {
        const [start, end] = filter.totalRevenueInCentsRange;

        if (start !== undefined && start !== null && start !== '') {
          where = {
            ...where,
            totalRevenueInCents: {
              ...where.totalRevenueInCents,
              [Op.gte]: start,
            },
          };
        }

        if (end !== undefined && end !== null && end !== '') {
          where = {
            ...where,
            totalRevenueInCents: {
              ...where.totalRevenueInCents,
              [Op.lte]: end,
            },
          };
        }
      }

      if (filter.currentBillingAmountInCentsRange) {
        const [start, end] = filter.currentBillingAmountInCentsRange;

        if (start !== undefined && start !== null && start !== '') {
          where = {
            ...where,
            currentBillingAmountInCents: {
              ...where.currentBillingAmountInCents,
              [Op.gte]: start,
            },
          };
        }

        if (end !== undefined && end !== null && end !== '') {
          where = {
            ...where,
            currentBillingAmountInCents: {
              ...where.currentBillingAmountInCents,
              [Op.lte]: end,
            },
          };
        }
      }

      if (filter.productPriceInCentsRange) {
        const [start, end] = filter.productPriceInCentsRange;

        if (start !== undefined && start !== null && start !== '') {
          where = {
            ...where,
            productPriceInCents: {
              ...where.productPriceInCents,
              [Op.gte]: start,
            },
          };
        }

        if (end !== undefined && end !== null && end !== '') {
          where = {
            ...where,
            productPriceInCents: {
              ...where.productPriceInCents,
              [Op.lte]: end,
            },
          };
        }
      }

      if (filter.productVersionNumber) {
        where = {
          ...where,
          productVersionNumber: filter.productVersionNumber,
        };
      }

      if (filter.currentPeriodStartedAtRange) {
        const [start, end] = filter.currentPeriodStartedAtRange;

        if (start !== undefined && start !== null && start !== '') {
          where = {
            ...where,
            currentPeriodStartedAt: {
              ...where.currentPeriodStartedAt,
              [Op.gte]: start,
            },
          };
        }

        if (end !== undefined && end !== null && end !== '') {
          where = {
            ...where,
            currentPeriodStartedAt: {
              ...where.currentPeriodStartedAt,
              [Op.lte]: end,
            },
          };
        }
      }

      if (filter.currentPeriodEndsAtRange) {
        const [start, end] = filter.currentPeriodEndsAtRange;

        if (start !== undefined && start !== null && start !== '') {
          where = {
            ...where,
            currentPeriodEndsAt: {
              ...where.currentPeriodEndsAt,
              [Op.gte]: start,
            },
          };
        }

        if (end !== undefined && end !== null && end !== '') {
          where = {
            ...where,
            currentPeriodEndsAt: {
              ...where.currentPeriodEndsAt,
              [Op.lte]: end,
            },
          };
        }
      }

      if (filter.nextAssessmentAtRange) {
        const [start, end] = filter.nextAssessmentAtRange;

        if (start !== undefined && start !== null && start !== '') {
          where = {
            ...where,
            nextAssessmentAt: {
              ...where.nextAssessmentAt,
              [Op.gte]: start,
            },
          };
        }

        if (end !== undefined && end !== null && end !== '') {
          where = {
            ...where,
            nextAssessmentAt: {
              ...where.nextAssessmentAt,
              [Op.lte]: end,
            },
          };
        }
      }

      if (filter.trialStartedAtRange) {
        const [start, end] = filter.trialStartedAtRange;

        if (start !== undefined && start !== null && start !== '') {
          where = {
            ...where,
            trialStartedAt: {
              ...where.trialStartedAt,
              [Op.gte]: start,
            },
          };
        }

        if (end !== undefined && end !== null && end !== '') {
          where = {
            ...where,
            trialStartedAt: {
              ...where.trialStartedAt,
              [Op.lte]: end,
            },
          };
        }
      }

      if (filter.trialEndedAtRange) {
        const [start, end] = filter.trialEndedAtRange;

        if (start !== undefined && start !== null && start !== '') {
          where = {
            ...where,
            trialEndedAt: {
              ...where.trialEndedAt,
              [Op.gte]: start,
            },
          };
        }

        if (end !== undefined && end !== null && end !== '') {
          where = {
            ...where,
            trialEndedAt: {
              ...where.trialEndedAt,
              [Op.lte]: end,
            },
          };
        }
      }

      if (filter.activatedAtRange) {
        const [start, end] = filter.activatedAtRange;

        if (start !== undefined && start !== null && start !== '') {
          where = {
            ...where,
            activatedAt: {
              ...where.activatedAt,
              [Op.gte]: start,
            },
          };
        }

        if (end !== undefined && end !== null && end !== '') {
          where = {
            ...where,
            activatedAt: {
              ...where.activatedAt,
              [Op.lte]: end,
            },
          };
        }
      }

      if (filter.automaticallyResumeAtRange) {
        const [start, end] = filter.automaticallyResumeAtRange;

        if (start !== undefined && start !== null && start !== '') {
          where = {
            ...where,
            automaticallyResumeAt: {
              ...where.automaticallyResumeAt,
              [Op.gte]: start,
            },
          };
        }

        if (end !== undefined && end !== null && end !== '') {
          where = {
            ...where,
            automaticallyResumeAt: {
              ...where.automaticallyResumeAt,
              [Op.lte]: end,
            },
          };
        }
      }

      if (filter.expiresAtRange) {
        const [start, end] = filter.expiresAtRange;

        if (start !== undefined && start !== null && start !== '') {
          where = {
            ...where,
            expiresAt: {
              ...where.expiresAt,
              [Op.gte]: start,
            },
          };
        }

        if (end !== undefined && end !== null && end !== '') {
          where = {
            ...where,
            expiresAt: {
              ...where.expiresAt,
              [Op.lte]: end,
            },
          };
        }
      }

      if (filter.createdAtRange) {
        const [start, end] = filter.createdAtRange;

        if (start !== undefined && start !== null && start !== '') {
          where = {
            ...where,
            createdAt: {
              ...where.createdAt,
              [Op.gte]: start,
            },
          };
        }

        if (end !== undefined && end !== null && end !== '') {
          where = {
            ...where,
            createdAt: {
              ...where.createdAt,
              [Op.lte]: end,
            },
          };
        }
      }

      if (filter.updatedAtRange) {
        const [start, end] = filter.updatedAtRange;

        if (start !== undefined && start !== null && start !== '') {
          where = {
            ...where,
            updatedAt: {
              ...where.updatedAt,
              [Op.gte]: start,
            },
          };
        }

        if (end !== undefined && end !== null && end !== '') {
          where = {
            ...where,
            updatedAt: {
              ...where.updatedAt,
              [Op.lte]: end,
            },
          };
        }
      }

      if (filter.canceledAtRange) {
        const [start, end] = filter.canceledAtRange;

        if (start !== undefined && start !== null && start !== '') {
          where = {
            ...where,
            canceledAt: {
              ...where.canceledAt,
              [Op.gte]: start,
            },
          };
        }

        if (end !== undefined && end !== null && end !== '') {
          where = {
            ...where,
            canceledAt: {
              ...where.canceledAt,
              [Op.lte]: end,
            },
          };
        }
      }

      if (filter.delayedCancelAtRange) {
        const [start, end] = filter.delayedCancelAtRange;

        if (start !== undefined && start !== null && start !== '') {
          where = {
            ...where,
            delayedCancelAt: {
              ...where.delayedCancelAt,
              [Op.gte]: start,
            },
          };
        }

        if (end !== undefined && end !== null && end !== '') {
          where = {
            ...where,
            delayedCancelAt: {
              ...where.delayedCancelAt,
              [Op.lte]: end,
            },
          };
        }
      }

      if (filter.cancellationMessage) {
        where = {
          ...where,
          [Op.and]: SequelizeFilterUtils.ilike('subscription', 'cancellationMessage', filter.cancellationMessage),
        };
      }

      if (filter.cancellationMethod) {
        where = {
          ...where,
          cancellationMethod: filter.cancellationMethod,
        };
      }

      if (filter.cancelAtEndOfPeriod === true || filter.cancelAtEndOfPeriod === 'true' || filter.cancelAtEndOfPeriod === false || filter.cancelAtEndOfPeriod === 'false') {
        where = {
          ...where,
          cancelAtEndOfPeriod: filter.cancelAtEndOfPeriod === true || filter.cancelAtEndOfPeriod === 'true',
        };
      }

      if (filter.signupPaymentId) {
        where = {
          ...where,
          signupPaymentId: filter.signupPaymentId,
        };
      }

      if (filter.signupRevenue) {
        where = {
          ...where,
          signupRevenue: filter.signupRevenue,
        };
      }

      if (filter.couponCode) {
        where = {
          ...where,
          couponCode: filter.couponCode,
        };
      }

      if (filter.couponCodes && filter.couponCodes.length) {
        where = {
          ...where,
          couponCodes: {
            [Op.in]: filter.couponCodes,
          },
        };
      }
      if (filter.paymentCollectionMethod) {
        where = {
          ...where,
          paymentCollectionMethod: filter.paymentCollectionMethod,
        };
      }

      if (filter.snapDay) {
        where = {
          ...where,
          snapDay: filter.snapDay,
        };
      }

      if (filter.reasonCode) {
        where = {
          ...where,
          reasonCode: filter.reasonCode,
        };
      }

      if (filter.offerId) {
        where = {
          ...where,
          offerId: filter.offerId,
        };
      }

      if (filter.payerId) {
        where = {
          ...where,
          payerId: filter.payerId,
        };
      }

      if (filter.productPricePointId) {
        where = {
          ...where,
          productPricePointId: filter.productPricePointId,
        };
      }

      if (filter.receivesInvoiceEmails === true || filter.receivesInvoiceEmails === 'true' || filter.receivesInvoiceEmails === false || filter.receivesInvoiceEmails === 'false') {
        where = {
          ...where,
          receivesInvoiceEmails: filter.receivesInvoiceEmails === true || filter.receivesInvoiceEmails === 'true',
        };
      }

      if (filter.netTermsRange) {
        const [start, end] = filter.netTermsRange;

        if (start !== undefined && start !== null && start !== '') {
          where = {
            ...where,
            netTerms: {
              ...where.netTerms,
              [Op.gte]: start,
            },
          };
        }

        if (end !== undefined && end !== null && end !== '') {
          where = {
            ...where,
            netTerms: {
              ...where.netTerms,
              [Op.lte]: end,
            },
          };
        }
      }

      if (filter.paymentType) {
        where = {
          ...where,
          paymentType: filter.paymentType,
        };
      }

      if (filter.referralCode) {
        where = {
          ...where,
          referralCode: filter.referralCode,
        };
      }

      if (filter.nextProductId) {
        where = {
          ...where,
          nextProductId: filter.nextProductId,
        };
      }

      if (filter.nextProductHandle) {
        where = {
          ...where,
          nextProductHandle: filter.nextProductHandle,
        };
      }

      if (filter.couponUseCountRange) {
        const [start, end] = filter.couponUseCountRange;

        if (start !== undefined && start !== null && start !== '') {
          where = {
            ...where,
            couponUseCount: {
              ...where.couponUseCount,
              [Op.gte]: start,
            },
          };
        }

        if (end !== undefined && end !== null && end !== '') {
          where = {
            ...where,
            couponUseCount: {
              ...where.couponUseCount,
              [Op.lte]: end,
            },
          };
        }
      }

      if (filter.couponUsesAllowedRange) {
        const [start, end] = filter.couponUsesAllowedRange;

        if (start !== undefined && start !== null && start !== '') {
          where = {
            ...where,
            couponUsesAllowed: {
              ...where.couponUsesAllowed,
              [Op.gte]: start,
            },
          };
        }

        if (end !== undefined && end !== null && end !== '') {
          where = {
            ...where,
            couponUsesAllowed: {
              ...where.couponUsesAllowed,
              [Op.lte]: end,
            },
          };
        }
      }

      if (filter.storedCredentialTransactionId) {
        where = {
          ...where,
          storedCredentialTransactionId: filter.storedCredentialTransactionId,
        };
      }
    }

    let { rows, count } = await models.subscription.findAndCountAll({
      where,
      include,
      limit: limit ? Number(limit) : undefined,
      offset: offset ? Number(offset) : undefined,
      order: orderBy ? [orderBy.split('_')] : [['createdAt', 'DESC']],
      transaction: SequelizeRepository.getTransaction(options),
    });

    if (withRelations) {
      rows = await this._fillWithRelationsAndFilesForRows(rows, options);
    }

    return { rows, count };
  }

  /**
   * Lists the Subscriptions to populate the autocomplete.
   * See https://sequelize.org/v5/manual/querying.html to learn how to
   * customize the query.
   *
   * @param {string} query
   * @param {number} limit
   */
  async findAllAutocomplete(query, limit) {
    let where = {};

    if (query) {
      where = {
        [Op.or]: [{ id: SequelizeFilterUtils.uuid(query) }],
      };
    }

    const records = await models.subscription.findAll({
      attributes: ['id', 'id'],
      where,
      limit: limit ? Number(limit) : undefined,
      orderBy: [['id', 'ASC']],
    });

    return records.map((record) => ({
      id: record.id,
      label: record.id,
    }));
  }

  /**
   * Convert by reference a field from an empty array to `null` to workaround
   * the Postgres error: "cannot determine type of empty array."
   *
   * @param {Object} data - The object housing the field
   * @param {String} fieldName - The name of the field
   */
  _convertEmptyArrayToNull(data, fieldName) {
    if (Array.isArray(data[fieldName]) && !data[fieldName].length) {
      data[fieldName] = null;
    }
  }

  /**
   * Creates an audit log of the operation.
   *
   * @param {string} action - The action [create, update or delete].
   * @param {object} record - The sequelize record
   * @param {object} data - The new data passed on the request
   * @param {object} options
   */
  async _createAuditLog(action, record, data, options) {
    let values = {};

    if (data) {
      values = {
        ...record.get({ plain: true }),
      };
    }

    await AuditLogRepository.log(
      {
        entityName: 'subscription',
        entityId: record.id,
        action,
        values,
      },
      options
    );
  }

  /**
   * Fills an array of Subscriptions with relations and files.
   *
   * @param {Array} rows
   * @param {Object} [options]
   */
  async _fillWithRelationsAndFilesForRows(rows, options) {
    if (!rows) {
      return rows;
    }

    return Promise.all(rows.map((record) => this._fillWithRelationsAndFiles(record, options)));
  }

  /**
   * Fill the Subscription with the relations and files.
   *
   * @param {Object} record
   * @param {Object} [options]
   */
  /* eslint-disable */
  async _fillWithRelationsAndFiles(record, options) {
    if (!record) {
      return record;
    }

    // The subscription row
    const output = record.get({ plain: true });
    const relationOptions = { ...options };

    // If an accountId is not present on the currentUser, then use the accountId on the subscription record that is being updated
    if (!relationOptions.accountId) {
      relationOptions.currentUser = { accountId: output.accountId };
    }

    const paidRolesFilter = AccountMemberRepository._buildPaidRoleQuery(PAID_ROLES);

    const paidSeats = await AccountMemberRepository.count({
      accountId: relationOptions.currentUser.accountId,
      roles: {
        [Op.or]: paidRolesFilter,
      },
    });
    output.paidUserSeatsUsed = paidSeats || 0;

    const parts = await PartRepository.count({ status: ['Active', 'Inactive'] }, relationOptions);
    output.partsUsed = parts || 0;

    const drawings = await DrawingRepository.count({ status: ['Active'] }, relationOptions);
    output.drawingsUsed = drawings || 0;

    // TODO: Add measurements

    return output;
  }
};
