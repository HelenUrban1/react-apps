const pick = require('lodash/pick');
const Sequelize = require('sequelize');
const models = require('../models');
const SequelizeRepository = require('./sequelizeRepository');
const AuditLogRepository = require('./auditLogRepository');
const FileRepository = require('./fileRepository');
const SequelizeFilterUtils = require('../utils/sequelizeFilterUtils');

const { Op } = Sequelize;

const fields = [
  'id',
  'previousRevision',
  'nextRevision',
  'originalDrawing',
  'status',
  'documentType',
  'sheetCount',
  'name',
  'number',
  'revision',
  'linearUnit',
  'angularUnit',
  'markerOptions',
  'gridOptions',
  'linearTolerances',
  'tags',
  'angularTolerances',
  'importHash',
  'updatedAt',
  'createdAt',
];
/**
 * Handles database operations for the Drawing.
 * See https://sequelize.org/v5/index.html to learn how to customize it.
 */
class DrawingRepository {
  /**
   * Creates the Drawing.
   *
   * @param {Object} data
   * @param {Object} [options]
   */
  async create(data, options) {
    const currentUser = SequelizeRepository.getCurrentUser(options);

    const tenant = await models.getTenant(currentUser.accountId);

    const transaction = SequelizeRepository.getTransaction(options);

    const record = await tenant.drawing.create(
      {
        ...pick(data, fields),
        siteId: currentUser.siteId,
        createdById: currentUser.id,
        updatedById: currentUser.id,
      },
      {
        transaction,
      }
    );

    await record.setPart(data.part || null, {
      transaction,
    });
    await record.setSheets(data.sheets || [], {
      transaction,
    });
    await record.setCharacteristics(data.characteristics || [], {
      transaction,
    });
    if (!data.originalDrawing) {
      // If this is the original drawing, set the originalDrawing to the record id
      await record.setOriginalDrawing(record.id, { transaction });
    }

    const files = await FileRepository.replaceRelationFiles(
      {
        belongsTo: tenant.drawing.getTableName(),
        belongsToColumn: 'file',
        belongsToId: record.id,
      },
      data.file,
      options
    );

    await record.setDrawingFile(files[0] || data.drawingFile?.id, {
      transaction,
    });

    if (data.previousRevision) {
      // if this was a new revision, set the nextRevision pointer on the previousRevision
      const previousRecord = await tenant.drawing.findByPk(data.previousRevision.id || data.previousRevision, {
        transaction,
      });
      await previousRecord.setNextRevision(record.id || null, {
        transaction,
      });
    }

    await this._createAuditLog(AuditLogRepository.CREATE, record, data, options);

    return this.findById(record.id, options);
  }

  /**
   * Updates the Drawing.
   *
   * @param {Object} data
   * @param {Object} [options]
   */
  async update(id, data, options) {
    const currentUser = SequelizeRepository.getCurrentUser(options);

    const tenant = await models.getTenant(currentUser.accountId);

    const transaction = SequelizeRepository.getTransaction(options);

    let record = await tenant.drawing.findByPk(id, {
      transaction,
    });

    record = await record.update(
      {
        ...pick(data, fields),
        siteId: currentUser.siteId,
        updatedById: currentUser.id,
      },
      {
        transaction,
      }
    );

    await record.setPart(data.part || null, {
      transaction,
    });
    await record.setSheets(data.sheets || [], {
      transaction,
    });
    /* await record.setCharacteristics(data.characteristics || [], {
      transaction,
    }); */

    await FileRepository.replaceRelationFiles(
      {
        belongsTo: tenant.drawing.getTableName(),
        belongsToColumn: 'file',
        belongsToId: record.id,
      },
      data.file,
      options
    );

    await this._createAuditLog(AuditLogRepository.UPDATE, record, data, options);

    return this.findById(record.id, options);
  }

  /**
   * Deletes the Drawing.
   *
   * @param {string} id
   * @param {Object} [options]
   */
  async destroy(id, options) {
    const currentUser = SequelizeRepository.getCurrentUser(options);

    const tenant = await models.getTenant(currentUser.accountId);

    const transaction = SequelizeRepository.getTransaction(options);

    const record = await tenant.drawing.findByPk(id, {
      transaction,
    });

    await record.destroy({
      transaction,
    });

    await this._createAuditLog(AuditLogRepository.DELETE, record, null, options);
  }

  /**
   * Finds the Drawing and its relations.
   *
   * @param {string} id
   * @param {Object} [options]
   */
  async findById(id, options) {
    const currentUser = SequelizeRepository.getCurrentUser(options);

    const tenant = await models.getTenant(currentUser.accountId);
    const transaction = SequelizeRepository.getTransaction(options);

    const include = [
      {
        model: tenant.part,
        as: 'part',
      },
    ];

    const record = await tenant.drawing.findByPk(id, {
      include,
      transaction,
    });

    return this._fillWithRelationsAndFiles(record, options);
  }

  /**
   * Counts the number of Drawings based on the filter.
   *
   * @param {Object} filter
   * @param {Object} [options]
   */
  static async count(filter, options) {
    const currentUser = SequelizeRepository.getCurrentUser(options);

    const tenant = await models.getTenant(currentUser.accountId);

    return tenant.drawing.count({
      where: filter,
    });
  }

  /**
   * Finds the Drawings based on the query.
   * See https://sequelize.org/v5/manual/querying.html to learn how to
   * customize the query.
   *
   * @param {Object} query
   * @param {Object} query.filter
   * @param {number} query.limit
   * @param  {number} query.offset
   * @param  {string} query.orderBy
   * @param {Object} [options]
   *
   * @returns {Promise<Object>} response - Object containing the rows and the count.
   */
  async findAndCountAll(
    { filter, limit, offset, orderBy } = {
      filter: null,
      limit: 0,
      offset: 0,
      orderBy: null,
    },
    options
  ) {
    const currentUser = SequelizeRepository.getCurrentUser(options);

    const tenant = await models.getTenant(currentUser.accountId);

    let where = {};
    const include = [
      {
        model: tenant.part,
        as: 'part',
      },
    ];

    if (filter) {
      if (filter.id) {
        where = {
          ...where,
          id: SequelizeFilterUtils.uuid(filter.id),
        };
      }

      if (filter.site) {
        where = {
          ...where,
          siteId: SequelizeFilterUtils.uuid(filter.site),
        };
      }

      if (filter.part) {
        where = {
          ...where,
          partId: SequelizeFilterUtils.uuid(filter.part),
        };
      }

      if (filter.status) {
        where = {
          ...where,
          status: filter.status,
        };
      }

      if (filter.documentType) {
        where = {
          ...where,
          documentType: filter.documentType,
        };
      }

      if (filter.sheetCount) {
        where = {
          ...where,
          [Op.and]: SequelizeFilterUtils.ilike('drawing', 'sheetCount', filter.sheetCount),
        };
      }

      if (filter.name) {
        where = {
          ...where,
          [Op.and]: SequelizeFilterUtils.ilike('drawing', 'name', filter.name),
        };
      }

      if (filter.number) {
        where = {
          ...where,
          [Op.and]: SequelizeFilterUtils.ilike('drawing', 'number', filter.number),
        };
      }

      if (filter.revision) {
        where = {
          ...where,
          [Op.and]: SequelizeFilterUtils.ilike('drawing', 'revision', filter.revision),
        };
      }

      if (filter.linearUnit) {
        where = {
          ...where,
          [Op.and]: SequelizeFilterUtils.ilike('drawing', 'linearUnit', filter.linearUnit),
        };
      }

      if (filter.angularUnit) {
        where = {
          ...where,
          [Op.and]: SequelizeFilterUtils.ilike('drawing', 'angularUnit', filter.angularUnit),
        };
      }

      if (filter.markerOptions) {
        where = {
          ...where,
          [Op.and]: SequelizeFilterUtils.ilike('drawing', 'markerOptions', filter.markerOptions),
        };
      }

      if (filter.gridOptions) {
        where = {
          ...where,
          [Op.and]: SequelizeFilterUtils.ilike('drawing', 'gridOptions', filter.gridOptions),
        };
      }

      if (filter.linearTolerances) {
        where = {
          ...where,
          [Op.and]: SequelizeFilterUtils.ilike('drawing', 'linearTolerances', filter.linearTolerances),
        };
      }

      if (filter.tags) {
        where = {
          ...where,
          [Op.and]: SequelizeFilterUtils.ilike('drawing', 'tags', filter.tags),
        };
      }

      if (filter.angularTolerances) {
        where = {
          ...where,
          [Op.and]: SequelizeFilterUtils.ilike('drawing', 'angularTolerances', filter.angularTolerances),
        };
      }

      if (filter.createdAtRange) {
        const [start, end] = filter.createdAtRange;

        if (start !== undefined && start !== null && start !== '') {
          where = {
            ...where,
            createdAt: {
              ...where.createdAt,
              [Op.gte]: start,
            },
          };
        }

        if (end !== undefined && end !== null && end !== '') {
          where = {
            ...where,
            createdAt: {
              ...where.createdAt,
              [Op.lte]: end,
            },
          };
        }
      }
    }

    let { rows, count } = await tenant.drawing.findAndCountAll({
      where,
      include,
      limit: limit ? Number(limit) : undefined,
      offset: offset ? Number(offset) : undefined,
      order: orderBy ? [orderBy.split('_')] : [['createdAt', 'DESC']],
      transaction: SequelizeRepository.getTransaction(options),
    });

    rows = await this._fillWithRelationsAndFilesForRows(rows, options);

    return { rows, count };
  }

  /**
   * Lists the Drawings to populate the autocomplete.
   * See https://sequelize.org/v5/manual/querying.html to learn how to
   * customize the query.
   *
   * @param {string} query
   * @param {number} limit
   */
  async findAllAutocomplete(query, limit, options) {
    const currentUser = SequelizeRepository.getCurrentUser(options);

    const tenant = await models.getTenant(currentUser.accountId);

    let where = {};

    if (query) {
      where = {
        [Op.or]: [
          { id: SequelizeFilterUtils.uuid(query) },
          {
            [Op.and]: SequelizeFilterUtils.ilike('drawing', 'name', query),
          },
        ],
      };
    }

    const records = await tenant.drawing.findAll({
      attributes: ['id', 'name'],
      where,
      limit: limit ? Number(limit) : undefined,
      orderBy: [['name', 'ASC']],
    });

    return records.map((record) => ({
      id: record.id,
      label: record.name,
    }));
  }

  /**
   * Marks the desired drawing as deleted in the database.
   *
   * @param {string} id
   * @param {options} options
   */
  async markAsDeleted(id, options) {
    const currentUser = SequelizeRepository.getCurrentUser(options);
    const transaction = SequelizeRepository.getTransaction(options);
    const tenant = await models.getTenant(currentUser.accountId);

    const record = await tenant.drawing.findByPk(id, {
      transaction,
    });

    const data = {
      status: 'Deleted',
      updatedById: currentUser.id,
      deletedAt: new Date(),
    };

    await record.update(data, { transaction });

    await this._createAuditLog(AuditLogRepository.DELETE, record, data, options);
  }

  /**
   * Creates an audit log of the operation.
   *
   * @param {string} action - The action [create, update or delete].
   * @param {object} record - The sequelize record
   * @param {object} data - The new data passed on the request
   * @param {object} options
   */
  async _createAuditLog(action, record, data, options) {
    let values = {};

    if (data) {
      values = {
        ...record.get({ plain: true }),
        sheetsIds: data.sheets,
        characteristicsIds: data.characteristics,
        file: data.file,
      };
    }

    await AuditLogRepository.log(
      {
        entityName: 'drawing',
        entityId: record.id,
        action,
        values,
      },
      options
    );
  }

  /**
   * Fills an array of Drawing with relations and files.
   *
   * @param {Array} rows
   * @param {Object} [options]
   */
  async _fillWithRelationsAndFilesForRows(rows, options) {
    if (!rows) {
      return rows;
    }

    return Promise.all(rows.map((record) => this._fillWithRelationsAndFiles(record, options)));
  }

  /**
   * Fill the Drawing with the relations and files.
   *
   * @param {Object} record
   * @param {Object} [options]
   */
  async _fillWithRelationsAndFiles(record, options) {
    if (!record) {
      return record;
    }

    const output = record.get({ plain: true });

    const transaction = SequelizeRepository.getTransaction(options);

    output.sheets = await record.getSheets({
      transaction,
    });
    output.characteristics = await record.getCharacteristics({
      transaction,
      include: [
        {
          model: models.drawing,
          as: 'drawing',
        },
        {
          model: models.drawingSheet,
          as: 'drawingSheet',
        },
      ],
    });
    output.file = await record.getFile({
      transaction,
    });
    output.drawingFile = await record.getDrawingFile({
      transaction,
    });

    return output;
  }

  static convertToInput(drawing) {
    const validInput = {
      status: drawing.status ? drawing.status : 'Active',
      previousRevision: drawing.previousRevision,
      nextRevision: drawing.nextRevision,
      originalDrawing: drawing.originalDrawing,
      documentType: drawing.documentType ? drawing.documentType : 'Drawing',
      part: drawing.part ? drawing.part.id : drawing.id,
      file: drawing.file ? [...drawing.file] : [],
      drawingFile: drawing.drawingFile,
      sheetCount: drawing.sheetCount ? drawing.sheetCount : 0,
      name: drawing.name ? drawing.name : '',
      number: drawing.number ? drawing.number : '',
      revision: drawing.revision ? drawing.revision : '',
      linearUnit: drawing.linearUnit ? drawing.linearUnit : '',
      angularUnit: drawing.angularUnit ? drawing.angularUnit : '',
      markerOptions: drawing.markerOptions ? drawing.markerOptions : '',
      gridOptions: drawing.gridOptions ? drawing.gridOptions : JSON.stringify({ status: true }),
      linearTolerances: drawing.linearTolerances ? drawing.linearTolerances : '',
      tags: drawing.tags ? drawing.tags : '',
      angularTolerances: drawing.angularTolerances ? drawing.angularTolerances : '',
      sheets: drawing.sheets ? drawing.sheets.map((sheet) => sheet.id) : [],
      characteristics: drawing.characteristics ? drawing.characteristics.map((char) => char.id) : [],
    };
    return validInput;
  }
}

module.exports = DrawingRepository;
