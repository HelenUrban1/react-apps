const SequelizeRepository = require('./sequelizeRepository');
const { data, createRepoInstance } = require('../../__fixtures__').subscription;
// const config = require('../../../config')();

const SAMPLE_UUID = 'f44ffabd-790e-49e4-8417-95abf99a1e19'; // Generated value that is extremely unlikely to match a dynamically generated UUID value

describe('SubscriptionRepository', () => {
  let subscriptionRepository;
  const options = {
    currentUser: {
      // accountId: config.testAccountId,
      accountId: 'e0a01769-c8ed-4090-bd24-9c1b2241fb75',
      siteId: '31ef1d56-1b86-4ffa-b886-507ed8f021c5',
      billingId: 'e0a01779-c8ed-4090-bd44-9c1b2242fb75',
      companyId: '5788481594',
    },
  };

  beforeEach(async () => {
    await SequelizeRepository.cleanDatabase();
    subscriptionRepository = createRepoInstance();
  });

  afterAll(async () => {
    await SequelizeRepository.closeConnections();
  });

  describe('#create()', () => {
    it('should create an item', async () => {
      let count = await subscriptionRepository.count({});
      expect(count).toBe(0);
      const item = await subscriptionRepository.create(data[0], options);

      count = await subscriptionRepository.count({});
      expect(count).toBe(1);

      expect(item).toBeDefined();
      expect(item.activatedAt).toBeDefined();
    });

    it('should replace an empty couponCodes value with null before creation', async () => {
      let count = await subscriptionRepository.count({});
      expect(count).toBe(0);
      const item = await subscriptionRepository.create(data[0], { ...options, test: true });

      count = await subscriptionRepository.count({});
      expect(count).toBe(1);

      expect(item.couponCodes).toBe(null);
    });

    it('should preserve a populated couponCodes value', async () => {
      let count = await subscriptionRepository.count({});
      expect(count).toBe(0);

      const item = await subscriptionRepository.create(data[1], options);

      count = await subscriptionRepository.count({});
      expect(count).toBe(1);

      expect(Array.isArray(item.couponCodes)).toBe(true);
    });
  });

  describe('#update()', () => {
    it('should update an item', async () => {
      const originalItem = await subscriptionRepository.create(data[0], options);
      const originalProductPriceInCents = originalItem.productPriceInCents;

      const updatedProductPriceInCents = 666;
      const updatedItem = await subscriptionRepository.update(originalItem.id, { product_price_in_cents: updatedProductPriceInCents }, options);

      expect(originalItem.id).toBe(updatedItem.id);
      expect(originalProductPriceInCents).not.toBe(updatedItem.productPriceInCents);
    });

    it('should handle a null to populated array update', async () => {
      const originalItem = await subscriptionRepository.create(data[0], options);
      const originalCouponCodes = originalItem.couponCodes;
      expect(originalCouponCodes).toBe(null);

      const updatedCouponCodes = ['a'];
      const updatedItem = await subscriptionRepository.update(originalItem.id, { coupon_codes: updatedCouponCodes }, options);
      expect(Array.isArray(updatedItem.couponCodes)).toBe(true);

      expect(originalItem.id).toBe(updatedItem.id);
      expect(originalCouponCodes).not.toBe(updatedItem.couponCodes);
    });

    it('should handle a populated array to empty array update', async () => {
      const originalItem = await subscriptionRepository.create(data[1], options);
      const originalCouponCodes = originalItem.couponCodes;
      expect(Array.isArray(originalItem.couponCodes)).toBe(true);

      const updatedCouponCodes = [];
      const updatedItem = await subscriptionRepository.update(originalItem.id, { coupon_codes: updatedCouponCodes }, options);
      expect(updatedItem.couponCodes).toBe(null);

      expect(originalItem.id).toBe(updatedItem.id);
      expect(originalCouponCodes).not.toBe(updatedItem.couponCodes);
    });
  });

  describe('#destroy()', () => {
    it('should destroy an item', async () => {
      const items = [];
      for (const item of data) {
        items.push(await subscriptionRepository.create(item, options));
      }
      await subscriptionRepository.destroy(items[0].id);

      const count = await subscriptionRepository.count({});
      expect(count).toBe(data.length - 1);
    });
  });

  describe('#findById()', () => {
    it('should return null when a matching item does not exist', async () => {
      const item = await subscriptionRepository.findById(SAMPLE_UUID, options);
      expect(item).toBe(null);
    });

    it('should find an item that exists', async () => {
      const items = [];
      for (const item of data) {
        items.push(await subscriptionRepository.create(item, options));
      }
      const item = await subscriptionRepository.findById(items[0].id, options);
      expect(item.id).toBe(items[0].id);
    });
  });

  describe('#count()', () => {
    it('should return the current item count', async () => {
      const count = await subscriptionRepository.count({});
      expect(count).toBe(0);
    });

    it('should return the current item count', async () => {
      for (const item of data) {
        await subscriptionRepository.create(item, options);
      }
      const count = await subscriptionRepository.count({});
      expect(count).toBe(data.length);
    });
  });

  describe('#findAndCountAll()', () => {
    it('should honor a simple filter', async () => {
      for (const item of data) {
        await subscriptionRepository.create(item, options);
      }

      const firstItemPaymentSystemId = data[0].id;

      const filter = {
        paymentSystemId: firstItemPaymentSystemId,
      };
      const results = await subscriptionRepository.findAndCountAll({ filter, ...options });
      expect(results.count).toBe(1);
      expect(results.rows[0].paymentSystemId).toBe(firstItemPaymentSystemId);
    });
  });
});
