const pick = require('lodash/pick');
const Sequelize = require('sequelize');
const models = require('../models');
const SequelizeRepository = require('./sequelizeRepository');
const AuditLogRepository = require('./auditLogRepository');
const SequelizeFilterUtils = require('../utils/sequelizeFilterUtils');

const { Op } = Sequelize;

/**
 * Handles database operations for the Settings.
 * See https://sequelize.org/v5/index.html to learn how to customize it.
 */
class SettingRepository {
  /**
   * Creates the Site.
   *
   * @param {Object} data
   * @param {Object} [options]
   */
  static async create(data, options) {
    const currentUser = SequelizeRepository.getCurrentUser(options);

    const tenant = await models.getTenant(currentUser.accountId);

    const transaction = SequelizeRepository.getTransaction(options);

    const record = await tenant.setting.create(
      {
        ...pick(data, [
          'id', //
          'name',
          'value',
          'metadata',
          'siteId',
          'userId',
          'updatedAt',
          'createdAt',
        ]),
        createdById: currentUser.id,
        updatedById: currentUser.id,
      },
      {
        transaction,
      }
    );

    await SettingRepository.createAuditLog(AuditLogRepository.CREATE, record, data, options);

    return SettingRepository.findById(record.id, options);
  }

  static async createGlobal(data, options) {
    const currentUser = SequelizeRepository.getCurrentUser(options);
    const transaction = SequelizeRepository.getTransaction(options);
    const record = await models.setting.create(
      {
        ...pick(data, [
          'id', //
          'name',
          'value',
          'metadata',
          'siteId',
          'userId',
          'updatedAt',
          'createdAt',
        ]),
        createdById: currentUser.id,
        updatedById: currentUser.id,
      },
      {
        transaction,
      }
    );
    await SettingRepository.createAuditLog(AuditLogRepository.CREATE, record, data, options);

    return SettingRepository.findById(record.id, options);
  }

  /**
   * Updates the Setting.
   *
   * @param {Object} data
   * @param {Object} [options]
   */
  static async update(id, data, options) {
    const currentUser = SequelizeRepository.getCurrentUser(options);

    const tenant = await models.getTenant(currentUser.accountId);

    const transaction = SequelizeRepository.getTransaction(options);

    let record = await tenant.setting.findByPk(id, {
      transaction,
    });

    record = await record.update(
      {
        ...pick(data, [
          'id', //
          'name',
          'value',
          'metadata',
          'siteId',
          'userId',
          'updatedAt',
          'createdAt',
        ]),
        updatedById: currentUser.id,
      },
      {
        transaction,
      }
    );

    await SettingRepository.createAuditLog(AuditLogRepository.UPDATE, record, data, options);

    return SettingRepository.findById(record.id, options);
  }

  /**
   * Deletes the Setting.
   *
   * @param {string} id
   * @param {Object} [options]
   */
  static async destroy(id, options) {
    const currentUser = SequelizeRepository.getCurrentUser(options);

    const tenant = await models.getTenant(currentUser.accountId);

    const transaction = SequelizeRepository.getTransaction(options);

    const record = await tenant.setting.findByPk(id, {
      transaction,
    });

    await record.destroy({
      transaction,
    });

    await SettingRepository.createAuditLog(AuditLogRepository.DELETE, record, null, options);
  }

  /**
   * Finds the Setting.
   *
   * @param {string} id
   * @param {Object} [options]
   */
  static async findById(id, options) {
    const currentUser = SequelizeRepository.getCurrentUser(options);

    const tenant = await models.getTenant(currentUser.accountId);

    const transaction = SequelizeRepository.getTransaction(options);

    const include = [];

    let record = await tenant.setting.findByPk(id, {
      include,
      transaction,
    });

    if (!record) {
      record = await models.setting.findByPk(id, {
        include,
        transaction,
      });
    }

    return SettingRepository.fillWithRelations(record, options);
  }

  /**
   * Finds most specific setting by name.
   * If user is logged in, will look for user setting,
   * then site setting, then account setting, then global setting,
   * returning the first match it finds.
   * @param {string} name
   * @param {object} options
   */
  static async findByName(name, options) {
    const currentUser = SequelizeRepository.getCurrentUser(options);
    let record;
    const include = [];
    let where = {
      name,
    };
    const order = ['userId', 'siteId'];
    if (currentUser && currentUser.accountId) {
      const tenant = await models.getTenant(currentUser.accountId);
      if (currentUser.siteId) {
        where = {
          ...where,
          [Op.or]: [{ siteId: currentUser.siteId }, { siteId: null }],
        };
      }
      if (currentUser.userId) {
        where = {
          ...where,
          [Op.or]: [{ userId: currentUser.userId }, { userId: null }],
        };
      }
      record = tenant && tenant.setting ? await tenant.setting.findOne({ where, order, include }) : await models.setting.findOne({ where, include });
    }
    if (!record) {
      record = await models.setting.findOne({ where, include });
    }
    return SettingRepository.fillWithRelations(record, options);
  }

  /**
   * Finds the Settings based on the query.
   * See https://sequelize.org/v5/manual/querying.html to learn how to
   * customize the query.
   *
   * @param {Object} query
   * @param {Object} query.filter
   * @param {number} query.limit
   * @param  {number} query.offset
   * @param  {string} query.orderBy
   * @param {Object} [options]
   *
   * @returns {Promise<Object>} response - Object containing the rows and the count.
   */
  static async findAndCountAll(
    { filter, limit, offset, orderBy } = {
      filter: null,
      limit: 0,
      offset: 0,
      orderBy: null,
    },
    options
  ) {
    const currentUser = SequelizeRepository.getCurrentUser(options);

    const tenant = await models.getTenant(currentUser.accountId);

    let where = {};
    const include = [];

    if (filter) {
      if (filter.id) {
        where = {
          ...where,
          id: SequelizeFilterUtils.uuid(filter.id),
        };
      }

      if (filter.name) {
        where = {
          ...where,
          [Op.and]: SequelizeFilterUtils.ilike('setting', 'name', filter.name),
        };
      }

      if (filter.siteId) {
        where = {
          ...where,
          siteId: SequelizeFilterUtils.uuid(filter.siteId),
        };
      }

      if (filter.userId) {
        where = {
          ...where,
          userId: SequelizeFilterUtils.uuid(filter.userId),
        };
      }

      if (filter.createdAtRange) {
        const [start, end] = filter.createdAtRange;

        if (start !== undefined && start !== null && start !== '') {
          where = {
            ...where,
            createdAt: {
              ...where.createdAt,
              [Op.gte]: start,
            },
          };
        }

        if (end !== undefined && end !== null && end !== '') {
          where = {
            ...where,
            createdAt: {
              ...where.createdAt,
              [Op.lte]: end,
            },
          };
        }
      }
    }

    const { rows, count } = await tenant.setting.findAndCountAll({
      where,
      include,
      limit: limit ? Number(limit) : undefined,
      offset: offset ? Number(offset) : undefined,
      order: orderBy ? [orderBy.split('_')] : [['createdAt', 'DESC']],
      transaction: SequelizeRepository.getTransaction(options),
    });

    const outputRows = await SettingRepository.fillWithRelationsForRows(rows, options);

    return { rows: outputRows, count };
  }

  /**
   * Creates an audit log of the operation.
   *
   * @param {string} action - The action [create, update or delete].
   * @param {object} record - The sequelize record
   * @param {object} data - The new data passed on the request
   * @param {object} options
   */
  static async createAuditLog(action, record, data, options) {
    let values = {};

    if (data) {
      values = {
        ...record.get({ plain: true }),
      };
    }

    await AuditLogRepository.log(
      {
        entityName: 'setting',
        entityId: record.id,
        action,
        values,
      },
      options
    );
  }

  /**
   * Fills an array of Setting.
   *
   * @param {Array} rows
   * @param {Object} [options]
   */
  static async fillWithRelationsForRows(rows, options) {
    if (!rows) {
      return rows;
    }

    return Promise.all(rows.map((record) => SettingRepository.fillWithRelations(record, options)));
  }

  /**
   * Fill the Setting.
   *
   * @param {Object} record
   * @param {Object} [options]
   */
  static async fillWithRelations(record, options) {
    if (!record) {
      return record;
    }
    return record.get({ plain: true });
  }
}

module.exports = SettingRepository;
