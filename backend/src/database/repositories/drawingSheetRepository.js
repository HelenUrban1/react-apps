const pick = require('lodash/pick');
const Sequelize = require('sequelize');
const models = require('../models');
const SequelizeRepository = require('./sequelizeRepository');
const AuditLogRepository = require('./auditLogRepository');
const SequelizeFilterUtils = require('../utils/sequelizeFilterUtils');
const PartRepository = require('../../database/repositories/partRepository');
const { log } = require('../../logger');
const config = require('../../../config')();

const { Op } = Sequelize;
const fields = [
  'id',
  'previousRevision',
  'nextRevision',
  'originalSheet',
  'pageName',
  'pageIndex',
  'height',
  'width',
  'markerSize',
  'rotation',
  'grid',
  'useGrid',
  'displayLines',
  'displayLabels',
  'importHash',
  'foundCaptureCount',
  'acceptedCaptureCount',
  'acceptedTimeoutCaptureCount',
  'rejectedCaptureCount',
  'reviewedCaptureCount',
  'reviewedTimeoutCaptureCount',
  'defaultLinearTolerances',
  'defaultAngularTolerances',
  'updatedAt',
  'createdAt',
  'autoMarkupStatus',
];

/**
 * Handles database operations for the DrawingSheet.
 * See https://sequelize.org/v5/index.html to learn how to customize it.
 */
class DrawingSheetRepository {
  /**
   * Creates the DrawingSheet.
   *
   * @param {Object} data
   * @param {Object} [options]
   */
  async create(data, options) {
    const currentUser = SequelizeRepository.getCurrentUser(options);

    const tenant = await models.getTenant(currentUser.accountId);

    const transaction = SequelizeRepository.getTransaction(options);

    const record = await tenant.drawingSheet.create(
      {
        ...pick(data, fields),
        createdById: currentUser.id,
        updatedById: currentUser.id,
      },
      {
        transaction,
      },
    );

    await record.setPart(data.part || null, {
      transaction,
    });
    await record.setDrawing(data.drawing || null, {
      transaction,
    });
    await record.setCharacteristics(data.characteristics || [], {
      transaction,
    });
    if (!data.originalSheet) {
      // If this is the original sheet, set originalSheet to the record id
      await record.setOriginalSheet(record.id, { transaction });
    }
    if (data.previousRevision) {
      // if this was a new revision, set the nextRevision pointer on the previousRevision
      const previousRecord = await tenant.drawingSheet.findByPk(data.previousRevision.id || data.previousRevision, {
        transaction,
      });
      await previousRecord.setNextRevision(record.id || null, {
        transaction,
      });
    }

    await this._createAuditLog(AuditLogRepository.CREATE, record, data, options);

    return this.findById(record.id, options);
  }

  /**
   * Updates the DrawingSheet.
   *
   * @param {Object} data
   * @param {Object} [options]
   */
  async update(id, data, options) {
    const currentUser = SequelizeRepository.getCurrentUser(options);

    const tenant = await models.getTenant(currentUser.accountId);

    const transaction = SequelizeRepository.getTransaction(options);

    let record = await tenant.drawingSheet.findByPk(id, {
      transaction,
    });

    record = await record.update(
      {
        ...pick(data, fields),
        updatedById: currentUser.id,
      },
      {
        transaction,
      },
    );

    if (data.part) {
      await record.setPart(data.part, {
        transaction,
      });
    }
    if (data.drawing) {
      await record.setDrawing(data.drawing, {
        transaction,
      });
    }

    await this._createAuditLog(AuditLogRepository.UPDATE, record, data, options);

    return this.findById(record.id, options);
  }

  /**
   * Increments Capture Counts on a DrawingSheet by the provided amounts for the following fields:
   * foundCaptureCount, acceptedCaptureCount, rejectedCaptureCount, acceptedTimeoutCaptureCount, reviewedCaptureCount, reviewedTimeoutCaptureCount.
   * The values provided are added to the existing values in the database not replaced by them.
   * DrawingSheet is looked up by partId, drawingId and pageIndex, not by id.
   * Method is called by auto-balloon lambdas, so accountId and siteId are pulled off the passed in data object, not from the currentUser.
   * @param {*} data
   * @param {*} options
   * @returns updated DrawingSheet
   */
  async incrementInAccount(data, options) {
    const tenant = await models.getTenant(data.accountId);

    // Lambda calls to this method won't have id.
    // partId, drawingId and pageIndex should uniquely identify 1 sheet
    const record = await tenant.drawingSheet.findOne({
      where: {
        partId: data.part, //
        drawingId: data.drawing,
        pageIndex: data.pageIndex,
      },
    });

    if (!record) {
      return null;
    }

    const oldCounts = DrawingSheetRepository.checkCountCompletion(record);

    const newRecord = await record.increment({
      foundCaptureCount: data.foundCaptureCount || 0,
      acceptedCaptureCount: data.acceptedCaptureCount || 0,
      rejectedCaptureCount: data.rejectedCaptureCount || 0,
      acceptedTimeoutCaptureCount: data.acceptedTimeoutCaptureCount || 0,
      reviewedCaptureCount: data.reviewedCaptureCount || 0,
      reviewedTimeoutCaptureCount: data.reviewedTimeoutCaptureCount || 0,
    });

    log.debug(`drawingSheetRepository.incrementInAccount: ${newRecord.id}`);

    await this._createAuditLog(AuditLogRepository.UPDATE, record, data, options);

    const result = await this.findById(record.id, options);
    if (result) {
      const newCounts = DrawingSheetRepository.checkCountCompletion(newRecord);

      result.autoballoonProcessingComplete = false;
      if (!oldCounts.completedAcceptOrReject && newCounts.completedAcceptOrReject) {
        const isProcessingCheck = true;
        // Sheet is now done creating features from captures, check if remaining sheets are also done and create a notification
        if (await DrawingSheetRepository.checkSiblingsCompleted(newRecord, tenant, isProcessingCheck)) {
          result.autoballoonProcessingComplete = true;
        }
      }

      result.autoballoonReviewComplete = false;
      if (!oldCounts.completedReview && newCounts.completedReview) {
        const isProcessingCheck = false;
        // Sheet is now done being reviewed, check if remaining sheets are also done and create a notification
        if (await DrawingSheetRepository.checkSiblingsCompleted(newRecord, tenant, isProcessingCheck)) {
          result.autoballoonReviewComplete = true;
        }
      }
    }
    return result;
  }

  /**
   * Checks sibling drawing sheets to see if the counts are complete for processing or reviewing captures
   * @param {*} drawingSheet
   * @param {*} tenant
   * @param {*} transaction
   * @param {*} isProcessing True if checking processing, false if checking reviewing
   * @returns True if all sibling drawing sheets are complete or there are no siblings
   */
  static async checkSiblingsCompleted(drawingSheet, tenant, isProcessing) {
    const records = await tenant.drawingSheet.findAll({
      where: {
        partId: drawingSheet.part || drawingSheet.partId, //
        drawingId: drawingSheet.drawing || drawingSheet.drawingId,
        pageIndex: { [Op.ne]: drawingSheet.pageIndex },
      },
    });

    // Sheet has no siblings, so captures are done
    if (records.length === 0) {
      return true;
    }

    for (let index = 0; index < records.length; index++) {
      const sibling = records[index];
      const { completedAcceptOrReject, completedReview } = DrawingSheetRepository.checkCountCompletion(sibling);
      if (isProcessing) {
        // if any sheets are not complete, return immediately
        if (!completedAcceptOrReject) {
          return false;
        }
      } else if (!completedReview && config.autoBallooning.useHumanReview) {
        // only return false if human review is enabled
        return false;
      }
    }
    // if we get here, no sheets are incomplete
    return true;
  }

  /**
   * Check if the accepted, rejected and acceptedTimeout capture counts sum to equal the found capture count
   * and if the reviewed and reviewedTimeout capture counts sum to equal the accepted capture count.
   * @param {*} drawingSheet
   * @returns [bool, bool] [completedAcceptOrReject, completed Review]
   */
  static checkCountCompletion(drawingSheet) {
    let completedAcceptOrReject = false;
    let completedReview = false;

    if (drawingSheet.foundCaptureCount > 0 && drawingSheet.foundCaptureCount <= drawingSheet.acceptedCaptureCount + drawingSheet.rejectedCaptureCount + drawingSheet.acceptedTimeoutCaptureCount) {
      completedAcceptOrReject = true;
    }

    if (drawingSheet.acceptedCaptureCount > 0 && drawingSheet.acceptedCaptureCount <= drawingSheet.reviewedCaptureCount + drawingSheet.reviewedTimeoutCaptureCount) {
      completedReview = true;
    }

    return { completedAcceptOrReject, completedReview };
  }

  /**
   * Deletes the DrawingSheet.
   *
   * @param {string} id
   * @param {Object} [options]
   */
  async destroy(id, options) {
    const currentUser = SequelizeRepository.getCurrentUser(options);

    const tenant = await models.getTenant(currentUser.accountId);

    const transaction = SequelizeRepository.getTransaction(options);

    const record = await tenant.drawingSheet.findByPk(id, {
      transaction,
    });

    await record.destroy({
      transaction,
    });

    await this._createAuditLog(AuditLogRepository.DELETE, record, null, options);
  }

  /**
   * Finds the DrawingSheet and its relations.
   *
   * @param {string} id
   * @param {Object} [options]
   */
  async findById(id, options) {
    const currentUser = SequelizeRepository.getCurrentUser(options);

    const tenant = await models.getTenant(currentUser.accountId);

    const transaction = SequelizeRepository.getTransaction(options);

    const include = [
      {
        model: tenant.part,
        as: 'part',
      },
      {
        model: tenant.drawing,
        as: 'drawing',
      },
    ];

    const record = await tenant.drawingSheet.findByPk(id, {
      include,
      transaction,
    });

    return this._fillWithRelationsAndFiles(record, options);
  }

  /**
   * Finds the DrawingSheet count given a drawing Id
   *
   * @param {string} id
   * @param {Object} [options]
   */
  async findAllByDrawingIdInAccount(drawingId, accountId) {
    const tenant = await models.getTenant(accountId);

    const include = [
      {
        model: tenant.part,
        as: 'part',
      },
      {
        model: tenant.drawing,
        as: 'drawing',
      },
    ];

    const records = await tenant.drawingSheet.findAll({
      where: {
        drawingId: drawingId,
      },
      include,
    });

    return records;
  }

  /**
   * Counts the number of DrawingSheets based on the filter.
   *
   * @param {Object} filter
   * @param {Object} [options]
   */
  async count(filter, options) {
    const currentUser = SequelizeRepository.getCurrentUser(options);

    const tenant = await models.getTenant(currentUser.accountId);

    const transaction = SequelizeRepository.getTransaction(options);

    return tenant.drawingSheet.count(
      {
        where: filter,
      },
      {
        transaction,
      },
    );
  }

  /**
   * Finds the DrawingSheets based on the query.
   * See https://sequelize.org/v5/manual/querying.html to learn how to
   * customize the query.
   *
   * @param {Object} query
   * @param {Object} query.filter
   * @param {number} query.limit
   * @param  {number} query.offset
   * @param  {string} query.orderBy
   * @param {Object} [options]
   *
   * @returns {Promise<Object>} response - Object containing the rows and the count.
   */
  async findAndCountAll(
    { filter, limit, offset, orderBy } = {
      filter: null,
      limit: 0,
      offset: 0,
      orderBy: null,
    },
    options,
  ) {
    const currentUser = SequelizeRepository.getCurrentUser(options);

    const tenant = await models.getTenant(currentUser.accountId);

    let where = {};
    const include = [
      {
        model: tenant.part,
        as: 'part',
      },
      {
        model: tenant.drawing,
        as: 'drawing',
      },
    ];

    if (filter) {
      if (filter.id) {
        where = {
          ...where,
          id: SequelizeFilterUtils.uuid(filter.id),
        };
      }

      if (filter.pageName) {
        where = {
          ...where,
          [Op.and]: SequelizeFilterUtils.ilike('drawingSheet', 'pageName', filter.pageName),
        };
      }

      if (filter.pageIndexRange) {
        const [start, end] = filter.pageIndexRange;

        if (start !== undefined && start !== null && start !== '') {
          where = {
            ...where,
            pageIndex: {
              ...where.pageIndex,
              [Op.gte]: start,
            },
          };
        }

        if (end !== undefined && end !== null && end !== '') {
          where = {
            ...where,
            pageIndex: {
              ...where.pageIndex,
              [Op.lte]: end,
            },
          };
        }
      }

      if (filter.heightRange) {
        const [start, end] = filter.heightRange;

        if (start !== undefined && start !== null && start !== '') {
          where = {
            ...where,
            height: {
              ...where.height,
              [Op.gte]: start,
            },
          };
        }

        if (end !== undefined && end !== null && end !== '') {
          where = {
            ...where,
            height: {
              ...where.height,
              [Op.lte]: end,
            },
          };
        }
      }

      if (filter.widthRange) {
        const [start, end] = filter.widthRange;

        if (start !== undefined && start !== null && start !== '') {
          where = {
            ...where,
            width: {
              ...where.width,
              [Op.gte]: start,
            },
          };
        }

        if (end !== undefined && end !== null && end !== '') {
          where = {
            ...where,
            width: {
              ...where.width,
              [Op.lte]: end,
            },
          };
        }
      }

      if (filter.rotation) {
        where = {
          ...where,
          [Op.and]: SequelizeFilterUtils.ilike('drawingSheet', 'rotation', filter.rotation),
        };
      }

      if (filter.grid) {
        where = {
          ...where,
          [Op.and]: SequelizeFilterUtils.ilike('drawingSheet', 'grid', filter.grid),
        };
      }

      if (filter.displayLines === true || filter.displayLines === 'true' || filter.displayLines === false || filter.displayLines === 'false') {
        where = {
          ...where,
          displayLines: filter.displayLines === true || filter.displayLines === 'true',
        };
      }

      if (filter.displayLabels === true || filter.displayLabels === 'true' || filter.displayLabels === false || filter.displayLabels === 'false') {
        where = {
          ...where,
          displayLabels: filter.displayLabels === true || filter.displayLabels === 'true',
        };
      }

      if (filter.part) {
        where = {
          ...where,
          partId: SequelizeFilterUtils.uuid(filter.part),
        };
      }

      if (filter.drawing) {
        where = {
          ...where,
          drawingId: SequelizeFilterUtils.uuid(filter.drawing),
        };
      }

      if (filter.createdAtRange) {
        const [start, end] = filter.createdAtRange;

        if (start !== undefined && start !== null && start !== '') {
          where = {
            ...where,
            createdAt: {
              ...where.createdAt,
              [Op.gte]: start,
            },
          };
        }

        if (end !== undefined && end !== null && end !== '') {
          where = {
            ...where,
            createdAt: {
              ...where.createdAt,
              [Op.lte]: end,
            },
          };
        }
      }

      if (filter.foundCaptureCountRange) {
        const [start, end] = filter.foundCaptureCountRange;

        if (start !== undefined && start !== null && start !== '') {
          where = {
            ...where,
            foundCaptureCount: {
              ...where.foundCaptureCount,
              [Op.gte]: start,
            },
          };
        }

        if (end !== undefined && end !== null && end !== '') {
          where = {
            ...where,
            foundCaptureCount: {
              ...where.foundCaptureCount,
              [Op.lte]: end,
            },
          };
        }
      }

      if (filter.acceptedCaptureCountRange) {
        const [start, end] = filter.acceptedCaptureCountRange;

        if (start !== undefined && start !== null && start !== '') {
          where = {
            ...where,
            acceptedCaptureCount: {
              ...where.acceptedCaptureCount,
              [Op.gte]: start,
            },
          };
        }

        if (end !== undefined && end !== null && end !== '') {
          where = {
            ...where,
            acceptedCaptureCount: {
              ...where.acceptedCaptureCount,
              [Op.lte]: end,
            },
          };
        }
      }

      if (filter.acceptedTimeoutCaptureCountRange) {
        const [start, end] = filter.acceptedTimeoutCaptureCountRange;

        if (start !== undefined && start !== null && start !== '') {
          where = {
            ...where,
            acceptedTimeoutCaptureCount: {
              ...where.acceptedTimeoutCaptureCount,
              [Op.gte]: start,
            },
          };
        }

        if (end !== undefined && end !== null && end !== '') {
          where = {
            ...where,
            acceptedTimeoutCaptureCount: {
              ...where.acceptedTimeoutCaptureCount,
              [Op.lte]: end,
            },
          };
        }
      }

      if (filter.rejectedCaptureCountRange) {
        const [start, end] = filter.rejectedCaptureCountRange;

        if (start !== undefined && start !== null && start !== '') {
          where = {
            ...where,
            rejectedCaptureCount: {
              ...where.rejectedCaptureCount,
              [Op.gte]: start,
            },
          };
        }

        if (end !== undefined && end !== null && end !== '') {
          where = {
            ...where,
            rejectedCaptureCount: {
              ...where.rejectedCaptureCount,
              [Op.lte]: end,
            },
          };
        }
      }

      if (filter.reviewedCaptureCountRange) {
        const [start, end] = filter.reviewedCaptureCountRange;

        if (start !== undefined && start !== null && start !== '') {
          where = {
            ...where,
            reviewedCaptureCount: {
              ...where.reviewedCaptureCount,
              [Op.gte]: start,
            },
          };
        }

        if (end !== undefined && end !== null && end !== '') {
          where = {
            ...where,
            reviewedCaptureCount: {
              ...where.reviewedCaptureCount,
              [Op.lte]: end,
            },
          };
        }
      }

      if (filter.reviewedTimeoutCaptureCountRange) {
        const [start, end] = filter.reviewedTimeoutCaptureCountRange;

        if (start !== undefined && start !== null && start !== '') {
          where = {
            ...where,
            reviewedTimeoutCaptureCount: {
              ...where.reviewedTimeoutCaptureCount,
              [Op.gte]: start,
            },
          };
        }

        if (end !== undefined && end !== null && end !== '') {
          where = {
            ...where,
            reviewedTimeoutCaptureCount: {
              ...where.reviewedTimeoutCaptureCount,
              [Op.lte]: end,
            },
          };
        }
      }
    }

    let { rows, count } = await tenant.drawingSheet.findAndCountAll({
      where,
      include,
      limit: limit ? Number(limit) : undefined,
      offset: offset ? Number(offset) : undefined,
      order: orderBy ? [orderBy.split('_')] : [['createdAt', 'DESC']],
      transaction: SequelizeRepository.getTransaction(options),
    });

    rows = await this._fillWithRelationsAndFilesForRows(rows, options);

    return { rows, count };
  }

  /**
   * Lists the DrawingSheets to populate the autocomplete.
   * See https://sequelize.org/v5/manual/querying.html to learn how to
   * customize the query.
   *
   * @param {string} query
   * @param {number} limit
   */
  async findAllAutocomplete(query, limit, options) {
    const currentUser = SequelizeRepository.getCurrentUser(options);

    const tenant = await models.getTenant(currentUser.accountId);

    let where = {};

    if (query) {
      where = {
        [Op.or]: [{ id: SequelizeFilterUtils.uuid(query) }, { pageIndex: query }],
      };
    }

    const records = await tenant.drawingSheet.findAll({
      attributes: ['id', 'pageIndex'],
      where,
      limit: limit ? Number(limit) : undefined,
      orderBy: [['pageIndex', 'ASC']],
    });

    return records.map((record) => ({
      id: record.id,
      label: record.pageIndex,
    }));
  }

  /**
   * Creates an audit log of the operation.
   *
   * @param {string} action - The action [create, update or delete].
   * @param {object} record - The sequelize record
   * @param {object} data - The new data passed on the request
   * @param {object} options
   */
  async _createAuditLog(action, record, data, options) {
    let values = {};

    if (data) {
      values = {
        ...record.get({ plain: true }),
        characteristicsIds: data.characteristics,
      };
    }

    await AuditLogRepository.log(
      {
        entityName: 'drawingSheet',
        entityId: record.id,
        action,
        values,
      },
      options,
    );
  }

  /**
   * Fills an array of DrawingSheet with relations and files.
   *
   * @param {Array} rows
   * @param {Object} [options]
   */
  async _fillWithRelationsAndFilesForRows(rows, options) {
    if (!rows) {
      return rows;
    }

    return Promise.all(rows.map((record) => this._fillWithRelationsAndFiles(record, options)));
  }

  /**
   * Fill the DrawingSheet with the relations and files.
   *
   * @param {Object} record
   * @param {Object} [options]
   */
  async _fillWithRelationsAndFiles(record, options) {
    if (!record) {
      return record;
    }

    const output = record.get({ plain: true });

    const transaction = SequelizeRepository.getTransaction(options);

    output.part = await record.getPart({ transaction });
    output.drawing = await record.getDrawing({ transaction });

    output.characteristics = await record.getCharacteristics({
      transaction,
    });

    return output;
  }

  static convertToInput(drawingSheet) {
    log.debug(`converting to drawing sheet input ${drawingSheet.id}`);
    return {
      part: drawingSheet.part ? drawingSheet.part.id : '',
      previousRevision: drawingSheet.previousRevision,
      nextRevision: drawingSheet.nextRevision,
      originalSheet: drawingSheet.originalSheet || drawingSheet.id,
      drawing: drawingSheet.drawing ? drawingSheet.drawing.id : '',
      pageName: drawingSheet.pageName || 'Sheet',
      pageIndex: drawingSheet.pageIndex || 0,
      height: drawingSheet.height,
      width: drawingSheet.width,
      rotation: drawingSheet.rotation,
      grid: drawingSheet.grid,
      useGrid: drawingSheet.useGrid,
      markerSize: drawingSheet.markerSize,
      displayLines: drawingSheet.displayLines,
      displayLabels: drawingSheet.displayLabels,
      characteristics: drawingSheet.characteristics ? drawingSheet.characteristics.map((char) => char.id) : [],
      foundCaptureCount: drawingSheet.foundCaptureCount,
      acceptedCaptureCount: drawingSheet.acceptedCaptureCount,
      acceptedTimeoutCaptureCount: drawingSheet.acceptedTimeoutCaptureCount,
      rejectedCaptureCount: drawingSheet.rejectedCaptureCount,
      reviewedCaptureCount: drawingSheet.reviewedCaptureCount,
      reviewedTimeoutCaptureCount: drawingSheet.reviewedTimeoutCaptureCount,
      defaultLinearTolerances: drawingSheet.defaultLinearTolerances || null,
      defaultAngularTolerances: drawingSheet.defaultAngularTolerances || null,
    };
  }

  /**
   * Get the DrawingSheet record on partid, drawingid, pageindex.
   *
   * @param {Object} data
   * @returns {record}
   */
  async findByPartDrawingPage(data) {
    const tenant = await models.getTenant(data.accountId);
    // partId, drawingId and pageIndex should uniquely identify 1 sheet
    const record = await tenant.drawingSheet.findOne({
      where: {
        partId: data.part, //
        drawingId: data.drawing,
        pageIndex: data.pageIndex,
      },
    });
    return record;
  }

  /**
   * Check all drawingsheets associated with a drawing and part id have processed.
   * Automarkupstatus will either be null, completed, error.
   * null = still in process by eda in lambda find-image-captures.
   * completed\error for lambda result.
   * If lambda fails outside of eda value will reamin null?
   *
   * @param {Object} data
   * @returns {boolean}
   */
  async allSheetsProcessed(data) {
    const tenant = await models.getTenant(data.accountId);
    const unprocessedRecords = await tenant.drawingSheet.findAll({
      where: {
        partId: data.part, //
        drawingId: data.drawing,
        autoMarkupStatus: null,
      },
    });
    return unprocessedRecords.length > 0 ? false : true;
  }

  /**
   * Check drawingsheets pagesindex for Error on part,drawing.
   *
   * @param {Object} data
   * @returns {boolean}
   */
  async sheetWithErrorStatus(data) {
    const tenant = await models.getTenant(data.accountId);
    const erroredRecords = await tenant.drawingSheet.findAll({
      where: {
        partId: data.part, //
        drawingId: data.drawing,
        autoMarkupStatus: 'Error',
      },
    });
    return erroredRecords.length > 0 ? true : false;
  }

  /**
   * Update drawingsheet : automarkupStatus to error or complete.
   *
   * @param {string} id
   * @param {Object} data
   * @returns {boolean}
   */
  async updateDrawingSheetAutomarkupStatus(id, data) {
    try {
      const transaction = await SequelizeRepository.createTransaction(data.accountId);

      await this.update(id, data, {
        transaction,
        currentUser: {
          accountId: data.accountId,
        },
      });
      await SequelizeRepository.commitTransaction(transaction);
      log.debug(`drawingSheetService.updateDrawingSheetAutomarkupStatus for update: ${id}`);
    } catch (error) {
      log.debug(`error: ${error}`);
      await SequelizeRepository.rollbackTransaction(transaction);
      return false;
    }
    return true;
  }

  /**
   * Update part automarkupStatus to error.
   *
   * @param {Object} data
   * @param {string} status
   * @returns {boolean}
   */
  async updatePartAutomarkupStatus(data, status) {
    const accountId = data.accountId;
    const siteId = data.siteId;

    const transaction = await SequelizeRepository.createTransaction(accountId);
    const autoMarkupstatus = { autoMarkupStatus: status };

    try {
      const partRecord = await new PartRepository().update(data.part, autoMarkupstatus, {
        transaction,
        currentUser: {
          accountId: accountId,
        },
      });

      await SequelizeRepository.commitTransaction(transaction);

      if (this.eventEmitter) {
        this.eventEmitter.emit('part.update', { accountId, siteId, data: partRecord });
      }

      return partRecord;
    } catch (error) {
      await SequelizeRepository.rollbackTransaction(transaction);
      throw error;
    }
  }
}

module.exports = DrawingSheetRepository;
