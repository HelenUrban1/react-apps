const pick = require('lodash/pick');
const Sequelize = require('sequelize');
const models = require('../models');
const SequelizeRepository = require('./sequelizeRepository');
const AuditLogRepository = require('./auditLogRepository');
const SequelizeFilterUtils = require('../utils/sequelizeFilterUtils');
const { log } = require('../../logger');

const { Op } = Sequelize;

/**
 * Handles database operations for the Sample.
 * See https://sequelize.org/v5/index.html to learn how to customize it.
 */
class SampleRepository {
  /**
   * Creates the Sample.
   *
   * @param {Object} data
   * @param {Object} [options]
   */
  async create(data, options) {
    const currentUser = SequelizeRepository.getCurrentUser(options);

    const transaction = SequelizeRepository.getTransaction(options);

    const tenant = await models.getTenant(currentUser.accountId);

    const record = await tenant.sample.create(
      {
        ...pick(data, ['serial', 'sampleIndex', 'featureCoverage', 'status', 'partId', 'updatedAt', 'createdAt']),
        createdById: currentUser.id,
        updatedById: currentUser.id,
      },
      {
        transaction,
      }
    );

    const partRecord = await tenant.part.findByPk(data.part, { transaction });
    await record.setPart(partRecord, { transaction });

    const jobRecord = await tenant.job.findByPk(data.job, { transaction });
    await record.setJob(jobRecord, { transaction });

    await this._createAuditLog(AuditLogRepository.CREATE, record, data, options);

    return this.findById(record.id, options);
  }

  /**
   * Updates the Sample.
   *
   * @param {Object} data
   * @param {Object} [options]
   */
  async update(id, data, options) {
    const currentUser = SequelizeRepository.getCurrentUser(options);

    const transaction = SequelizeRepository.getTransaction(options);

    const tenant = await models.getTenant(currentUser.accountId);

    let record = await tenant.sample.findByPk(id, {
      transaction,
    });

    record = await record.update(
      {
        ...pick(data, ['serial', 'sampleIndex', 'featureCoverage', 'status', 'partId', 'createdAt', 'updatingOperatorId', 'updatedAt']),
        updatedById: currentUser.id,
      },
      {
        transaction,
      }
    );

    if (data.part) {
      await record.setPart(data.part, {
        transaction,
      });
    }

    if (data.job) {
      await record.setJob(data.job, {
        transaction,
      });
    }

    await this._createAuditLog(AuditLogRepository.UPDATE, record, data, options);

    return this.findById(record.id, options);
  }

  /**
   * Deletes the Sample.
   *
   * @param {string} id
   * @param {Object} [options]
   */
  async destroy(id, options) {
    const currentUser = SequelizeRepository.getCurrentUser(options);

    const transaction = SequelizeRepository.getTransaction(options);

    const tenant = await models.getTenant(currentUser.accountId);

    const record = await tenant.sample.findByPk(id, {
      transaction,
    });

    await record.destroy({
      transaction,
    });

    await this._createAuditLog(AuditLogRepository.DELETE, record, null, options);
  }

  /**
   * Finds the Sample and its relations.
   *
   * @param {string} id
   * @param {Object} [options]
   */
  async findById(id, options) {
    const currentUser = SequelizeRepository.getCurrentUser(options);
    const transaction = SequelizeRepository.getTransaction(options);

    const tenant = await models.getTenant(currentUser.accountId);

    const include = [
      {
        model: tenant.part,
        as: 'part',
      },
      {
        model: tenant.job,
        as: 'job',
      },
    ];

    const record = await tenant.sample.findByPk(id, {
      include,
      transaction,
    });

    return await this._fillWithRelations(record, options);
  }

  /**
   * Counts the number of Samples based on the filter.
   *
   * @param {Object} filter
   * @param {Object} [options]
   */
  async count(filter, options) {
    const currentUser = SequelizeRepository.getCurrentUser(options);

    const transaction = SequelizeRepository.getTransaction(options);

    const tenant = await models.getTenant(currentUser.accountId);

    return tenant.sample.count(
      {
        where: filter,
      },
      {
        transaction,
      }
    );
  }

  /**
   * Finds the Samples based on the query.
   * See https://sequelize.org/v5/manual/querying.html to learn how to
   * customize the query.
   *
   * @param {Object} query
   * @param {Object} query.filter
   * @param {number} query.limit
   * @param {number} query.offset
   * @param {string} query.orderBy
   * @param {Object} [options]
   *
   * @returns {Promise<Object>} response - Object containing the rows and the count.
   */
  async findAndCountAll(
    { filter, limit, offset, orderBy } = {
      filter: null,
      limit: 0,
      offset: 0,
      orderBy: null,
    },
    options
  ) {
    const currentUser = SequelizeRepository.getCurrentUser(options);

    const tenant = await models.getTenant(currentUser.accountId);

    let where = {};
    const include = [
      {
        model: tenant.part,
        as: 'part',
      },
      {
        model: tenant.job,
        as: 'job',
      },
    ];
    if (filter) {
      if (filter.id) {
        where = {
          ...where,
          id: SequelizeFilterUtils.uuid(filter.id),
        };
      }

      if (filter.sample) {
        where = {
          ...where,
          [Op.and]: SequelizeFilterUtils.ilike('sample', 'sample', filter.sample),
        };
      }

      if (filter.serial) {
        where = {
          ...where,
          serial: filter.serial,
        };
      }

      if (filter.sampleIndex) {
        where = {
          ...where,
          sampleIndex: filter.sampleIndex,
        };
      }

      if (filter.featureCoverage) {
        where = {
          ...where,
          featureCoverage: filter.featureCoverage,
        };
      }

      if (filter.status) {
        where = {
          ...where,
          status: filter.status,
        };
      }

      if (filter.part) {
        where = {
          ...where,
          partId: SequelizeFilterUtils.uuid(filter.part),
        };
      }

      if (filter.job) {
        where = {
          ...where,
          jobId: SequelizeFilterUtils.uuid(filter.job),
        };
      }

      if (filter.partId) {
        where = {
          ...where,
          partId: filter.partId,
        };
      }

      if (filter.jobId) {
        where = {
          ...where,
          jobId: filter.jobId,
        };
      }

      if (filter.createdAtRange) {
        const [start, end] = filter.createdAtRange;

        if (start !== undefined && start !== null && start !== '') {
          where = {
            ...where,
            createdAt: {
              ...where.createdAt,
              [Op.gte]: start,
            },
          };
        }

        if (end !== undefined && end !== null && end !== '') {
          where = {
            ...where,
            createdAt: {
              ...where.createdAt,
              [Op.lte]: end,
            },
          };
        }
      }
    }

    let { rows, count } = await tenant.sample.findAndCountAll({
      where,
      include,
      limit: limit ? Number(limit) : undefined,
      offset: offset ? Number(offset) : undefined,
      order: orderBy ? [orderBy.split('_')] : [['createdAt', 'DESC']],
      transaction: SequelizeRepository.getTransaction(options),
    });

    rows = await this._fillWithRelationsForRows(rows, options);

    return { rows, count };
  }

  /**
   * Lists the Samples to populate the autocomplete.
   * See https://sequelize.org/v5/manual/querying.html to learn how to
   * customize the query.
   *
   * @param {string} query
   * @param {number} limit
   */
  async findAllAutocomplete(query, limit, options) {
    const currentUser = SequelizeRepository.getCurrentUser(options);

    const tenant = await models.getTenant(currentUser.accountId);

    let where = {};

    if (query) {
      where = {
        [Op.or]: [
          { id: SequelizeFilterUtils.uuid(query) },
          {
            [Op.and]: SequelizeFilterUtils.ilike('sample', 'serial', query),
          },
        ],
      };
    }

    const records = await tenant.sample.findAll({
      attributes: ['id', 'serial', 'sampleIndex', 'featureCoverage', 'status'],
      where,
      limit: limit ? Number(limit) : undefined,
      orderBy: [['sampleIndex', 'ASC']],
    });

    return records.map((record) => ({
      id: record.id,
      label: record.serial,
    }));
  }

  /**
   * Creates an audit log of the operation.
   *
   * @param {string} action - The action [create, update or delete].
   * @param {object} record - The sequelize record
   * @param {object} data - The new data passed on the request
   * @param {object} options
   */
  async _createAuditLog(action, record, data, options) {
    let values = {};

    if (data) {
      values = {
        ...record.get({ plain: true }),
      };
    }

    await AuditLogRepository.log(
      {
        entityName: 'sample',
        entityId: record.id,
        action,
        values,
      },
      options
    );
  }

  /**
   * Fills an array of Sample with relations.
   *
   * @param {Array} rows
   * @param {Object} [options]
   */
  async _fillWithRelationsForRows(rows, options) {
    if (!rows) {
      return rows;
    }

    return Promise.all(rows.map((record) => this._fillWithRelations(record, options)));
  }

  /**
   * Fill the Sample with the relations.
   *
   * @param {Object} record
   * @param {Object} [options]
   */
  async _fillWithRelations(record, options) {
    if (!record) {
      return record;
    }

    const output = record.get({ plain: true });

    const transaction = SequelizeRepository.getTransaction(options);

    output.part = await record.getPart({
      transaction,
    });

    output.job = await record.getJob({
      transaction,
    });

    output.measurements = await record.getMeasurements({
      transaction,
    });

    return output;
  }
}

module.exports = SampleRepository;
