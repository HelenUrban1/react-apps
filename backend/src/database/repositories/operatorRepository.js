const pick = require('lodash/pick');
const Sequelize = require('sequelize');
const models = require('../models');
const SequelizeRepository = require('./sequelizeRepository');
const AuditLogRepository = require('./auditLogRepository');
const SequelizeFilterUtils = require('../utils/sequelizeFilterUtils');
const { log } = require('../../logger');

const { Op } = Sequelize;

/**
 * Handles database operations for the Operator.
 * See https://sequelize.org/v5/index.html to learn how to customize it.
 */
class OperatorRepository {
  /**
   * Creates the Operator.
   *
   * @param {Object} data
   * @param {Object} [options]
   */
  async create(data, options) {
    const currentUser = SequelizeRepository.getCurrentUser(options);

    const transaction = SequelizeRepository.getTransaction(options);

    const tenant = await models.getTenant(currentUser.accountId);

    const record = await tenant.operator.create(
      {
        ...pick(data, ['accessControl', 'fullName', 'email', 'firstName', 'lastName', 'status', 'pin']),
        createdById: currentUser.id,
        updatedById: currentUser.id, // foreign key error?
      },
      {
        transaction,
      }
    );

    await this._createAuditLog(AuditLogRepository.CREATE, record, data, options);

    return this.findById(record.id, options);
  }

  /**
   * Updates the Operator.
   *
   * @param {Object} data
   * @param {Object} [options]
   */
  async update(id, data, options) {
    const currentUser = SequelizeRepository.getCurrentUser(options);

    const transaction = SequelizeRepository.getTransaction(options);

    const tenant = await models.getTenant(currentUser.accountId);

    let record = await tenant.operator.findByPk(id, {
      transaction,
    });

    record = await record.update(
      {
        ...pick(data, ['id', 'fullName', 'accessControl', 'firstName', 'lastName', 'email', 'status', 'pin']),
        updatedById: currentUser.id,
      },
      {
        transaction,
      }
    );

    await this._createAuditLog(AuditLogRepository.UPDATE, record, data, options);

    return this.findById(record.id, options);
  }

  /**
   * Deletes the Operator.
   *
   * @param {string} id
   * @param {Object} [options]
   */
  async destroy(id, options) {
    const currentUser = SequelizeRepository.getCurrentUser(options);

    const transaction = SequelizeRepository.getTransaction(options);

    const tenant = await models.getTenant(currentUser.accountId);

    const record = await tenant.operator.findByPk(id, {
      transaction,
    });

    await record.destroy({
      transaction,
    });

    await this._createAuditLog(AuditLogRepository.DELETE, record, null, options);
  }

  /**
   * Finds the Operator and its relations.
   *
   * @param {string} id
   * @param {Object} [options]
   */
  async findById(id, options) {
    const currentUser = SequelizeRepository.getCurrentUser(options);
    const transaction = SequelizeRepository.getTransaction(options);

    const tenant = await models.getTenant(currentUser.accountId);

    const include = [];

    const record = await tenant.operator.findByPk(id, {
      include,
      transaction,
    });

    return await this._fillWithRelations(record, options);
  }

  /**
   * Counts the number of Operators based on the filter.
   *
   * @param {Object} filter
   * @param {Object} [options]
   */
  async count(filter, options) {
    const currentUser = SequelizeRepository.getCurrentUser(options);

    const transaction = SequelizeRepository.getTransaction(options);

    const tenant = await models.getTenant(currentUser.accountId);

    return tenant.operator.count(
      {
        where: filter,
      },
      {
        transaction,
      }
    );
  }

  /**
   * Finds the Operators based on the query.
   * See https://sequelize.org/v5/manual/querying.html to learn how to
   * customize the query.
   *
   * @param {Object} query
   * @param {Object} query.filter
   * @param {number} query.limit
   * @param {number} query.offset
   * @param {string} query.orderBy
   * @param {Object} [options]
   *
   * @returns {Promise<Object>} response - Object containing the rows and the count.
   */
  async findAndCountAll(
    { filter, limit, offset, orderBy } = {
      filter: null,
      limit: 0,
      offset: 0,
      orderBy: null,
    },
    options
  ) {
    const currentUser = SequelizeRepository.getCurrentUser(options);

    const tenant = await models.getTenant(currentUser.accountId);

    let where = {};
    const include = [];
    if (filter) {
      if (filter.id) {
        where = {
          ...where,
          id: SequelizeFilterUtils.uuid(filter.id),
        };
      }

      if (filter.fullName) {
        where = {
          ...where,
          [Op.and]: SequelizeFilterUtils.ilike('operator', 'fullName', filter.fullName),
        };
      }

      if (filter.accessControl) {
        where = {
          ...where,
          accessControl: filter.accessControl,
        };
      }

      if (filter.email) {
        where = {
          ...where,
          email: filter.email,
        };
      }

      if (filter.status) {
        where = {
          ...where,
          status: filter.status,
        };
      }

      if (filter.createdAtRange) {
        const [start, end] = filter.createdAtRange;

        if (start !== undefined && start !== null && start !== '') {
          where = {
            ...where,
            createdAt: {
              ...where.createdAt,
              [Op.gte]: start,
            },
          };
        }

        if (end !== undefined && end !== null && end !== '') {
          where = {
            ...where,
            createdAt: {
              ...where.createdAt,
              [Op.lte]: end,
            },
          };
        }
      }
    }

    let { rows, count } = await tenant.operator.findAndCountAll({
      where,
      include,
      limit: limit ? Number(limit) : undefined,
      offset: offset ? Number(offset) : undefined,
      order: orderBy ? [orderBy.split('_')] : [['createdAt', 'DESC']],
      transaction: SequelizeRepository.getTransaction(options),
    });

    rows = await this._fillWithRelationsForRows(rows, options);

    return { rows, count };
  }

  /**
   * Lists the Operators to populate the autocomplete.
   * See https://sequelize.org/v5/manual/querying.html to learn how to
   * customize the query.
   *
   * @param {string} query
   * @param {number} limit
   */
  async findAllAutocomplete(query, limit, options) {
    const currentUser = SequelizeRepository.getCurrentUser(options);

    const tenant = await models.getTenant(currentUser.accountId);

    let where = {};

    if (query) {
      where = {
        [Op.or]: [
          { id: SequelizeFilterUtils.uuid(query) },
          {
            [Op.and]: SequelizeFilterUtils.ilike('operator', 'fullName', query),
          },
        ],
      };
    }

    const records = await tenant.operator.findAll({
      attributes: ['id', 'fullName', 'firstName', 'lastName', 'accessControl', 'email', 'status'],
      where,
      limit: limit ? Number(limit) : undefined,
      orderBy: [['fullName', 'ASC']],
    });

    return records.map((record) => ({
      id: record.id,
      label: record.fullName,
    }));
  }

  /**
   * Creates an audit log of the operation.
   *
   * @param {string} action - The action [create, update or delete].
   * @param {object} record - The sequelize record
   * @param {object} data - The new data passed on the request
   * @param {object} options
   */
  async _createAuditLog(action, record, data, options) {
    let values = {};

    if (data) {
      values = {
        ...record.get({ plain: true }),
      };
    }

    await AuditLogRepository.log(
      {
        entityName: 'operator',
        entityId: record.id,
        action,
        values,
      },
      options
    );
  }

  /**
   * Fills an array of Operator with relations.
   *
   * @param {Array} rows
   * @param {Object} [options]
   */
  async _fillWithRelationsForRows(rows, options) {
    if (!rows) {
      return rows;
    }

    return Promise.all(rows.map((record) => this._fillWithRelations(record, options)));
  }

  /**
   * Fill the Operator with the relations.
   *
   * @param {Object} record
   * @param {Object} [options]
   */
  async _fillWithRelations(record, options) {
    if (!record) {
      return record;
    }

    const output = record.get({ plain: true });

    const transaction = SequelizeRepository.getTransaction(options);

    return output;
  }
}

module.exports = OperatorRepository;
