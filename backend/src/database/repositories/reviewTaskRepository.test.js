const Sequelize = require('sequelize');
const ReviewTaskRepository = require('./reviewTaskRepository');
const SequelizeRepository = require('./sequelizeRepository');
const { data } = require('../../__fixtures__').reviewTask;
const config = require('../../../config')();

const { Op } = Sequelize;

describe('ReviewTaskRepository', () => {
  const repository = ReviewTaskRepository;
  const options = {
    currentUser: {
      accountId: config.testAccountId,
      siteId: '31ef1d56-1b86-4ffa-b886-507ed8f021c5',
      userId: 'e0a01779-c8ed-4090-bd44-9c1b2242fb75',
    },
  };

  beforeEach(async () => {
    await Promise.all([
      SequelizeRepository.cleanDatabase(), //
      SequelizeRepository.cleanDatabase(config.reviewAccountId),
    ]);
  });

  describe('#create()', () => {
    it('should create an item', async () => {
      const fixture = data[0];
      const item = await repository.create(fixture, options);

      expect(item.id).toBe(fixture.id);
    });
  });

  describe('#update()', () => {
    it('should update an item', async () => {
      const originalItem = await repository.create(data[0], options);

      const updatedStatus = 'Processing';
      const updatedItem = await repository.update(originalItem.id, { status: updatedStatus }, options);

      expect(originalItem.id).toBe(updatedItem.id);
      expect(updatedItem.status).toBe(updatedStatus);
    });
  });

  describe('#destroy()', () => {
    it('should remove an item', async () => {
      const originalItem = await repository.create(data[0], options);

      await repository.destroy(originalItem.id, options);

      const foundItem = await repository.findById(originalItem.id, options);
      expect(foundItem).toBe(null);
    });
  });

  describe('#findById()', () => {
    it('should find an existing item', async () => {
      const originalItem = await repository.create(data[0], options);

      const foundItem = await repository.findById(originalItem.id, options);
      expect(originalItem).toStrictEqual(foundItem);
    });
  });

  describe('#findAndCountAll()', () => {
    it('should find and count all the items', async () => {
      const originalItem = await repository.create(data[0], options);

      const result = await repository.findAndCountAll({}, options);
      expect(result.count).toBe(1);
      expect(result.rows[0]).toStrictEqual(originalItem);
    });
  });

  describe('#findNextQueuedTask()', () => {
    it('should set the status and processingStart for the task', async () => {
      const originalItem = await repository.create(data[0], options);

      expect(originalItem.status).toBe('Waiting');
      expect(originalItem.processingStart).toBe(null);

      const item = await repository.findNextQueuedTask(options);

      expect(item.status).toBe('Processing');
      expect(item.processingStart).toBeTruthy();
    });

    it('should return null if there are no items in the queue', async () => {
      const item = await repository.findNextQueuedTask(options);
      expect(item).toBe(null);
    });

    it('should return the oldest item in the queue', async () => {
      const originalItem1 = await repository.create(data[0], options);
      const originalItem2 = await repository.create({ ...data[0], id: 'b49b7446-fbe3-422c-8481-b9362644c0cb' }, options);
      const originalItem3 = await repository.create({ ...data[0], id: '33b7f297-5df7-4d9b-82fb-4eef5941043b' }, options);

      const item = await repository.findNextQueuedTask(options);

      expect(originalItem1.createdAt).not.toStrictEqual(originalItem2.createdAt);
      expect(originalItem1.createdAt).not.toStrictEqual(originalItem3.createdAt);
      expect(item.createdAt).toStrictEqual(originalItem1.createdAt);
    });
  });

  describe('#clearQueue()', () => {
    it('should delete all waiting items in the queue', async () => {
      const originalItem1 = await repository.create(data[0], options);
      await repository.create({ ...data[0], id: 'b49b7446-fbe3-422c-8481-b9362644c0cb' }, options);
      const originalItem2 = await repository.create({ ...data[0], id: '33b7f297-5df7-4d9b-82fb-4eef5941043b', status: 'Accepted' }, options);

      const item = await repository.findNextQueuedTask(options);
      expect(item.createdAt).toStrictEqual(originalItem1.createdAt);

      const result = await repository.clearQueue();
      // Confirm 2 items in the queue were updated
      expect(result).toBe(2);

      // Confirm nothing in queue
      const emptyItem = await repository.findNextQueuedTask(options);
      expect(emptyItem).toBe(null);

      // Confirm processed item is untouched
      const existingItem = await repository.findAndCountAll({}, options);
      expect(existingItem.count).toBe(1);
      expect(existingItem.rows[0]).toStrictEqual(originalItem2);
    });
  });

  describe('#returnTaskToQueue()', () => {
    it('should set the status and processingStart for the task', async () => {
      const originalItem = await repository.create(data[0], options);

      expect(originalItem.status).toBe('Waiting');
      expect(originalItem.processingStart).toBe(null);

      const item = await repository.findNextQueuedTask(options);

      expect(item.status).toBe('Processing');
      expect(item.processingStart).toBeTruthy();

      await repository.returnTaskToQueue(item.id, options);
      const returnedItem = await repository.findById(item.id, options);
      expect(returnedItem.status).toBe('Waiting');
      expect(returnedItem.processingStart).toBe(null);
    });
  });

  describe('#count()', () => {
    it('should filter by partId', async () => {
      await repository.create(data[0], options);
      await repository.create({ ...data[0], id: 'b49b7446-fbe3-422c-8481-b9362644c0cb', partId: 'f2945fc3-0f54-4c8c-88af-61a16e6fd23d' }, options);
      await repository.create({ ...data[0], id: '33b7f297-5df7-4d9b-82fb-4eef5941043b', partId: 'd6c5bc49-9fb2-4fbb-8a61-77bc8d18036f' }, options);

      const filter = { partId: 'd6c5bc49-9fb2-4fbb-8a61-77bc8d18036f' };
      const count = await repository.count(filter, options);

      expect(count).toBe(2);
    });

    it('should filter by status', async () => {
      await repository.create(data[0], options);
      await repository.create({ ...data[0], id: 'b49b7446-fbe3-422c-8481-b9362644c0cb', status: 'Accepted', partId: 'f2945fc3-0f54-4c8c-88af-61a16e6fd23d' }, options);
      await repository.create({ ...data[0], id: '33b7f297-5df7-4d9b-82fb-4eef5941043b', status: 'Processing', partId: 'd6c5bc49-9fb2-4fbb-8a61-77bc8d18036f' }, options);

      const filter = {
        status: {
          [Op.or]: ['Waiting', 'Processing'],
        },
      };
      const count = await repository.count(filter, options);

      expect(count).toBe(2);
    });

    it('should filter by status and partId', async () => {
      await repository.create(data[0], options);
      await repository.create({ ...data[0], id: 'b49b7446-fbe3-422c-8481-b9362644c0cb', status: 'Accepted', partId: 'd6c5bc49-9fb2-4fbb-8a61-77bc8d18036f' }, options);
      await repository.create({ ...data[0], id: 'd6e68c27-4215-410f-9bf7-c1b58a540dab', status: 'Waiting', partId: 'f2945fc3-0f54-4c8c-88af-61a16e6fd23d' }, options);
      await repository.create({ ...data[0], id: '33b7f297-5df7-4d9b-82fb-4eef5941043b', status: 'Processing', partId: 'd6c5bc49-9fb2-4fbb-8a61-77bc8d18036f' }, options);

      const filter = {
        status: {
          [Op.or]: ['Waiting', 'Processing'],
        },
        partId: 'd6c5bc49-9fb2-4fbb-8a61-77bc8d18036f',
      };
      const count = await repository.count(filter, options);

      expect(count).toBe(2);
    });

    it('should filter by status and dateCreated', async () => {
      // Create tasks that are 20 minutes old
      await repository.create({ ...data[0], createdAt: new Date(Date.now() - 20 * 60 * 1000) }, options);
      await repository.create({ ...data[0], createdAt: new Date(Date.now() - 20 * 60 * 1000), id: 'b49b7446-fbe3-422c-8481-b9362644c0cb', status: 'Waiting', partId: 'd6c5bc49-9fb2-4fbb-8a61-77bc8d18036f' }, options);
      await repository.create({ ...data[0], createdAt: new Date(Date.now() - 20 * 60 * 1000), id: 'd6e68c27-4215-410f-9bf7-c1b58a540dab', status: 'Waiting', partId: 'f2945fc3-0f54-4c8c-88af-61a16e6fd23d' }, options);
      await repository.create({ ...data[0], createdAt: new Date(Date.now() - 20 * 60 * 1000), id: '33b7f297-5df7-4d9b-82fb-4eef5941043b', status: 'Waiting', partId: 'd6c5bc49-9fb2-4fbb-8a61-77bc8d18036f' }, options);

      const tenMinutes = 10;
      const filterTenMinutes = {
        status: 'Waiting',
        createdAt: {
          [Op.lte]: new Date(Date.now() - tenMinutes * 60 * 1000),
        },
      };
      // Count all tasks more than 10 minutes old
      const countAll = await repository.count(filterTenMinutes, options);
      expect(countAll).toBe(4);

      const thirtyMinutes = 30;
      const filterThirtyMinutes = {
        status: 'Waiting',
        createdAt: {
          [Op.lte]: new Date(Date.now() - thirtyMinutes * 60 * 1000),
        },
      };
      // Count all tasks more than 30 minutes old
      const countNone = await repository.count(filterThirtyMinutes, options);
      expect(countNone).toBe(0);
    });
  });

  describe('#elapsedTime()', () => {
    it('should average all records unfiltered', async () => {
      await repository.create({ ...data[0], processingStart: '2021-01-08 00:00:00.000-05', processingEnd: '2021-01-08 00:01:00.000-05' }, options);
      await repository.create({ ...data[0], id: 'b49b7446-fbe3-422c-8481-b9362644c0cb', partId: 'f2945fc3-0f54-4c8c-88af-61a16e6fd23d', processingStart: '2021-01-08 00:00:00.000-05', processingEnd: '2021-01-08 00:03:00.000-05' }, options);
      await repository.create({ ...data[0], id: '33b7f297-5df7-4d9b-82fb-4eef5941043b', partId: 'd6c5bc49-9fb2-4fbb-8a61-77bc8d18036f', processingStart: '2021-01-08 00:00:00.000-05', processingEnd: '2021-01-08 00:05:00.000-05' }, options);

      const filter = null;
      const elapsedTime = await repository.elapsedTime(filter, 'processingStart', 'processingEnd', options);

      expect(elapsedTime[0].dataValues.elapsed.minutes).toBe(3);
    });

    it('should average records filtered by partId', async () => {
      await repository.create({ ...data[0], processingStart: '2021-01-08 00:00:00.000-05', processingEnd: '2021-01-08 00:01:00.000-05' }, options);
      await repository.create({ ...data[0], id: 'b49b7446-fbe3-422c-8481-b9362644c0cb', partId: 'f2945fc3-0f54-4c8c-88af-61a16e6fd23d', processingStart: '2021-01-08 00:00:00.000-05', processingEnd: '2021-01-08 00:03:00.000-05' }, options);
      await repository.create({ ...data[0], id: '33b7f297-5df7-4d9b-82fb-4eef5941043b', partId: 'd6c5bc49-9fb2-4fbb-8a61-77bc8d18036f', processingStart: '2021-01-08 00:00:00.000-05', processingEnd: '2021-01-08 00:05:00.000-05' }, options);

      const filter = { partId: 'd6c5bc49-9fb2-4fbb-8a61-77bc8d18036f' };
      const elapsedTime = await repository.elapsedTime(filter, 'processingStart', 'processingEnd', options);

      expect(elapsedTime[0].dataValues.elapsed.minutes).toBe(3);
    });

    it('should not allow sql injection', async () => {
      await repository.create({ ...data[0], processingStart: '2021-01-08 00:00:00.000-05', processingEnd: '2021-01-08 00:01:00.000-05' }, options);
      await repository.create({ ...data[0], id: 'b49b7446-fbe3-422c-8481-b9362644c0cb', partId: 'f2945fc3-0f54-4c8c-88af-61a16e6fd23d', processingStart: '2021-01-08 00:00:00.000-05', processingEnd: '2021-01-08 00:03:00.000-05' }, options);
      await repository.create({ ...data[0], id: '33b7f297-5df7-4d9b-82fb-4eef5941043b', partId: 'd6c5bc49-9fb2-4fbb-8a61-77bc8d18036f', processingStart: '2021-01-08 00:00:00.000-05', processingEnd: '2021-01-08 00:05:00.000-05' }, options);

      const filter = null;
      try {
        await repository.elapsedTime(filter, 'processingStart")  AS "elapsed" FROM "reviewTasks" as "reviewTask"; DELETE FROM "reviewTasks" -- ', 'processingEnd', options);
        expect(true).toBe(false);
      } catch (error) {
        expect(error).toBeTruthy();
      }
    });
  });

  afterAll(async () => {
    await Promise.all([
      SequelizeRepository.closeConnections(config.reviewAccountId), //
      SequelizeRepository.closeConnections(),
    ]);
  });
});
