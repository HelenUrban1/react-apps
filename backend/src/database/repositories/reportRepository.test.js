const SequelizeRepository = require('./sequelizeRepository');
const { data, createRepoInstance } = require('../../__fixtures__').report;
const config = require('../../../config')();

describe('ReportRepository', () => {
  let reportRepository;
  let auditLogSpy;
  const options = {
    currentUser: {
      accountId: config.testAccountId,
    },
  };

  beforeEach(async () => {
    await SequelizeRepository.cleanDatabase(options.currentUser.accountId);
    reportRepository = createRepoInstance();
    auditLogSpy = jest.spyOn(reportRepository, '_createAuditLog');
  });

  describe('#create()', () => {
    it('should create an item', async () => {
      const fixture = data[0];
      const item = await reportRepository.create(fixture, options);

      expect(item.id).toBe(fixture.id);
    });
  });

  describe('#update()', () => {
    it('should update an item', async () => {
      const originalItem = await reportRepository.create(data[0], options);

      const updatedTitle = 'new-title';
      const updatedItem = await reportRepository.update(originalItem.id, { title: updatedTitle }, options);
      expect(originalItem.id).toBe(updatedItem.id);
      expect(updatedItem.title).toBe(updatedTitle);
    });
  });

  describe('#markAsDeleted', () => {
    it('should update the desired report to have the "Deleted" status', async () => {
      const originalItem = await reportRepository.create(data[0], options);

      await reportRepository.markAsDeleted(originalItem.id, options);

      const foundItem = await reportRepository.findById(originalItem.id, options);
      expect(foundItem.status).toEqual('Deleted');
    });
  });

  afterAll(async () => {
    await SequelizeRepository.closeConnections(options.currentUser.accountId);
    await SequelizeRepository.closeConnections();
  });
});
