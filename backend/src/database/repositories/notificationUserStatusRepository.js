const pick = require('lodash/pick');
const models = require('../models');
const SequelizeRepository = require('./sequelizeRepository');
const AuditLogRepository = require('./auditLogRepository');
const SequelizeFilterUtils = require('../utils/sequelizeFilterUtils');

const { Sequelize } = models;
const { Op } = Sequelize;

/**
 * Handles database operations for the NotificationUserStatus.
 * See https://sequelize.org/v5/index.html to learn how to customize it.
 */
class NotificationUserStatusRepository {
  /**
   * Creates the NotificationUserStatus.
   *
   * @param {Object} data
   * @param {Object} [options]
   */
  async create(data, options) {
    const currentUser = SequelizeRepository.getCurrentUser(options);

    const tenant = await models.getTenant(currentUser.accountId);

    const transaction = SequelizeRepository.getTransaction(options);

    const record = await tenant.notificationUserStatus.create(
      {
        ...pick(data, [
          'id', //
          'readAt',
          'archivedAt',
          'notificationId',
          'userId',
          'createdAt',
          'updatedAt',
        ]),
      },
      {
        transaction,
      }
    );

    await this._createAuditLog(AuditLogRepository.CREATE, record, data, options);

    return this.findById(record.id, options);
  }

  async createInAccount(data, options) {
    const tenant = await models.getTenant(data.accountId);

    const transaction = SequelizeRepository.getTransaction(options);

    const record = await tenant.notificationUserStatus.create(
      {
        ...pick(data, [
          'id', //
          'readAt',
          'archivedAt',
          'notificationId',
          'userId',
          'createdAt',
          'updatedAt',
        ]),
      },
      {
        transaction,
      }
    );

    await this._createAuditLog(AuditLogRepository.CREATE, record, data, options);

    return this.findById(record.id, options);
  }

  /**
   * Updates the NotificationUserStatus.
   *
   * @param {Object} data
   * @param {Object} [options]
   */
  async update(id, data, options) {
    const currentUser = SequelizeRepository.getCurrentUser(options);

    const tenant = await models.getTenant(currentUser.accountId);

    const transaction = SequelizeRepository.getTransaction(options);

    let record = await tenant.notificationUserStatus.findByPk(id, {
      transaction,
    });

    record = await record.update(
      {
        ...pick(data, [
          'id', //
          'readAt',
          'archivedAt',
          'notificationId',
          'userId',
          'updatedAt',
        ]),
        updatedById: currentUser.id,
      },
      {
        transaction,
      }
    );

    await this._createAuditLog(AuditLogRepository.UPDATE, record, data, options);

    return this.findById(record.id, options);
  }

  /**
   * Deletes the NotificationUserStatus.
   *
   * @param {string} id
   * @param {Object} [options]
   */
  async destroy(id, options) {
    const currentUser = SequelizeRepository.getCurrentUser(options);

    const tenant = await models.getTenant(currentUser.accountId);

    const transaction = SequelizeRepository.getTransaction(options);

    const record = await tenant.notificationUserStatus.findByPk(id, {
      transaction,
    });

    await record.destroy({
      transaction,
    });

    await this._createAuditLog(AuditLogRepository.DELETE, record, null, options);
  }

  /**
   * Finds the NotificationUserStatus and its relations.
   *
   * @param {string} id
   * @param {Object} [options]
   */
  async findById(id, options) {
    const currentUser = SequelizeRepository.getCurrentUser(options);

    const tenant = await models.getTenant(currentUser.accountId);

    const transaction = SequelizeRepository.getTransaction(options);

    const include = [];

    const record = await tenant.notificationUserStatus.findByPk(id, {
      include,
      transaction,
    });

    return this._fillWithRelationsAndFiles(record, options);
  }

  /**
   * Counts the number of NotificationUserStatuses based on the filter.
   *
   * @param {Object} filter
   * @param {Object} [options]
   */
  // eslint-disable-next-line class-methods-use-this
  async count(filter, options) {
    const currentUser = SequelizeRepository.getCurrentUser(options);

    const tenant = await models.getTenant(currentUser.accountId);

    const transaction = SequelizeRepository.getTransaction(options);

    return tenant.notificationUserStatus.count(
      {
        where: filter,
      },
      {
        transaction,
      }
    );
  }

  /**
   * Finds the NotificationUserStatuses based on the query.
   * See https://sequelize.org/v5/manual/querying.html to learn how to
   * customize the query.
   *
   * @param {Object} query
   * @param {Object} query.filter
   * @param {number} query.limit
   * @param  {number} query.offset
   * @param  {string} query.orderBy
   * @param {Object} [options]
   *
   * @returns {Promise<Object>} response - Object containing the rows and the count.
   */
  async findAndCountAll(
    { filter, limit, offset, orderBy } = {
      filter: null,
      limit: 0,
      offset: 0,
      orderBy: null,
    },
    options
  ) {
    const currentUser = SequelizeRepository.getCurrentUser(options);

    const tenant = await models.getTenant(currentUser.accountId);

    let where = {};

    if (filter) {
      if (filter.id) {
        where = {
          ...where,
          id: filter.id,
        };
      }

      if (filter.notification) {
        where = {
          ...where,
          notificationId: SequelizeFilterUtils.uuid(filter.notification),
        };
      }

      if (filter.user) {
        where = {
          ...where,
          userId: SequelizeFilterUtils.uuid(filter.user),
        };
      }

      if (filter.readAtRange) {
        const [start, end] = filter.readAtRange;

        if (start !== undefined && start !== null && start !== '') {
          where = {
            ...where,
            readAt: {
              ...where.readAt,
              [Op.gte]: start,
            },
          };
        }

        if (end !== undefined && end !== null && end !== '') {
          where = {
            ...where,
            readAt: {
              ...where.readAt,
              [Op.lte]: end,
            },
          };
        }
      }

      if (filter.archivedAtRange) {
        const [start, end] = filter.archivedAtRange;

        if (start !== undefined && start !== null && start !== '') {
          where = {
            ...where,
            archivedAt: {
              ...where.archivedAt,
              [Op.gte]: start,
            },
          };
        }

        if (end !== undefined && end !== null && end !== '') {
          where = {
            ...where,
            archivedAt: {
              ...where.archivedAt,
              [Op.lte]: end,
            },
          };
        }
      }

      if (filter.createdAtRange) {
        const [start, end] = filter.createdAtRange;

        if (start !== undefined && start !== null && start !== '') {
          where = {
            ...where,
            createdAt: {
              ...where.createdAt,
              [Op.gte]: start,
            },
          };
        }

        if (end !== undefined && end !== null && end !== '') {
          where = {
            ...where,
            createdAt: {
              ...where.createdAt,
              [Op.lte]: end,
            },
          };
        }
      }
    }

    const { rows: rows0, count } = await tenant.notificationUserStatus.findAndCountAll({
      where,
      limit: limit ? Number(limit) : undefined,
      offset: offset ? Number(offset) : undefined,
      order: orderBy ? [orderBy.split('_')] : [['createdAt', 'DESC']],
      transaction: SequelizeRepository.getTransaction(options),
    });

    const rows = await this._fillWithRelationsAndFilesForRows(rows0, options);

    return { rows, count };
  }

  /**
   * Creates an audit log of the NotificationUserStatus.
   *
   * @param {string} action - The action [create, update or delete].
   * @param {object} record - The sequelize record
   * @param {object} data - The new data passed on the request
   * @param {object} options
   */
  // eslint-disable-next-line class-methods-use-this
  async _createAuditLog(action, record, data, options) {
    let values = {};

    if (data) {
      values = {
        ...record.get({ plain: true }),
      };
    }

    await AuditLogRepository.log(
      {
        entityName: 'notificationUserStatus',
        entityId: record.id,
        action,
        values,
      },
      options
    );
  }

  /**
   * Fills an array of NotificationUserStatus with relations and files.
   *
   * @param {Array} rows
   * @param {Object} [options]
   */
  async _fillWithRelationsAndFilesForRows(rows, options) {
    if (!rows) {
      return rows;
    }

    return Promise.all(rows.map((record) => this._fillWithRelationsAndFiles(record, options)));
  }

  /**
   * Fill the NotificationUserStatus with the relations and files.
   *
   * @param {Object} record
   * @param {Object} [options]
   */
  // eslint-disable-next-line class-methods-use-this
  async _fillWithRelationsAndFiles(record) {
    if (!record) {
      return record;
    }

    const output = record.get({ plain: true });

    return output;
  }
}

module.exports = NotificationUserStatusRepository;
