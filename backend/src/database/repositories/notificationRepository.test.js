const SequelizeRepository = require('./sequelizeRepository');
const { data, createRepoInstance } = require('../../__fixtures__').notification;
const { data: notificationUserStatusData, createRepoInstance: notificationUserStatusCreateRepoInstance } = require('../../__fixtures__').notificationUserStatus;
const config = require('../../../config')();

describe('NotificationRepository', () => {
  let repository;
  let notificationUserStatusRepository;
  let auditLogSpy;
  const options = {
    currentUser: {
      accountId: config.testAccountId,
    },
  };

  beforeEach(async () => {
    await SequelizeRepository.cleanDatabase(options.currentUser.accountId);
    repository = createRepoInstance();
    notificationUserStatusRepository = notificationUserStatusCreateRepoInstance();
    auditLogSpy = jest.spyOn(repository, '_createAuditLog');
  });

  describe('#create()', () => {
    it('should create an item', async () => {
      const fixture = data[0];
      const item = await repository.create(fixture, options);

      expect(item.id).toBe(fixture.id);
      expect(auditLogSpy).toBeCalledTimes(1);
    });
  });

  describe('#update()', () => {
    it('should update an item', async () => {
      const originalItem = await repository.create(data[0], options);

      const updatedTitle = 'New Title';
      const updatedItem = await repository.update(originalItem.id, { title: updatedTitle }, options);

      expect(originalItem.id).toBe(updatedItem.id);
      expect(updatedItem.title).toBe(updatedTitle);
      expect(auditLogSpy).toBeCalledTimes(2);
    });
  });

  describe('#destroy()', () => {
    it('should remove an item', async () => {
      const originalItem = await repository.create(data[0], options);

      await repository.destroy(originalItem.id, options);

      const foundItem = await repository.findById(originalItem.id, options);
      expect(foundItem).toBe(null);
      expect(auditLogSpy).toBeCalledTimes(2);
    });
  });

  describe('#findById()', () => {
    it('should find an existing item', async () => {
      const originalItem = await repository.create(data[0], options);

      const foundItem = await repository.findById(originalItem.id, options);
      expect(originalItem).toStrictEqual(foundItem);
    });
  });

  describe('#count()', () => {
    it('should count the total number of items', async () => {
      await repository.create(data[0], options);
      await notificationUserStatusRepository.create(notificationUserStatusData[0], options);

      const count = await repository.count(null, options);
      expect(count).toBe(1);
    });
  });

  describe('#findAndCountAll()', () => {
    it('should find and count all the items', async () => {
      const originalItem = await repository.create(data[0], options);
      await notificationUserStatusRepository.create(notificationUserStatusData[0], options);

      const result = await repository.findAndCountAll({}, options);
      expect(result.count).toBe(1);
      expect(result.rows[0]).toStrictEqual(originalItem);
    });
  });

  afterAll(async () => {
    await SequelizeRepository.closeConnections(options.currentUser.accountId);
    await SequelizeRepository.closeConnections();
  });
});
