const SequelizeRepository = require('./sequelizeRepository');
const { data, createRepoInstance } = require('../../__fixtures__').measurement;
const config = require('../../../config')();

describe('MeasurementRepository', () => {
  let repository;
  let auditLogSpy;
  const options = {
    currentUser: {
      accountId: config.testAccountId,
    },
  };

  beforeEach(async () => {
    await SequelizeRepository.cleanDatabase(options.currentUser.accountId);
    repository = createRepoInstance();
    auditLogSpy = jest.spyOn(repository, '_createAuditLog');
  });

  describe('#create()', () => {
    it('should create a measurement', async () => {
      const fixture = data[0];
      const measurement = await repository.create(fixture, options);

      expect(measurement.value).toBe(fixture.value);
    });
  });

  describe('#update()', () => {
    it('should update a measurement', async () => {
      const originalMeasurement = await repository.create(data[0], options);

      const updatedValue = 'Testing';
      const updatedMeasurement = await repository.update(originalMeasurement.id, { value: updatedValue }, options);

      expect(originalMeasurement.id).toBe(updatedMeasurement.id);
      expect(updatedMeasurement.value).toBe(updatedValue);
    });
  });

  describe('#destroy()', () => {
    it('should remove a measurement', async () => {
      const originalMeasurement = await repository.create(data[0], options);

      await repository.destroy(originalMeasurement.id, options);

      const foundMeasurement = await repository.findById(originalMeasurement.id, options);
      expect(foundMeasurement).toBe(null);
    });
  });

  describe('#findById()', () => {
    it('should find an existing measurement', async () => {
      const originalMeasurement = await repository.create(data[0], options);

      const foundMeasurement = await repository.findById(originalMeasurement.id, options);
      expect(originalMeasurement).toStrictEqual(foundMeasurement);
    });
  });

  describe('#count()', () => {
    it('should count the total number of measurements', async () => {
      await repository.create(data[0], options);

      const count = await repository.count({}, options);
      expect(count).toBe(1);
    });
  });
  describe('#findAndCountAll()', () => {
    it('should find and count all the measurements', async () => {
      const originalMeasurement = await repository.create(data[0], options);

      const result = await repository.findAndCountAll({}, options);
      expect(result.count).toBe(1);
      expect(result.rows[0]).toStrictEqual(originalMeasurement);
    });
  });

  describe('#findAllAutocomplete()', () => {
    it('should find all measurements for a list', async () => {
      const originalMeasurement = await repository.create(data[0], options);

      const result = await repository.findAllAutocomplete(null, null, options);
      expect(result[0].id).toBe(originalMeasurement.id);
    });
  });

  afterAll(async () => {
    await SequelizeRepository.closeConnections(options.currentUser.accountId);
    await SequelizeRepository.closeConnections();
  });
});
