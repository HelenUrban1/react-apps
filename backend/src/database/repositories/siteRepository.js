const pick = require('lodash/pick');
const Sequelize = require('sequelize');
const models = require('../models');
const SequelizeRepository = require('./sequelizeRepository');
const AuditLogRepository = require('./auditLogRepository');
const SequelizeFilterUtils = require('../utils/sequelizeFilterUtils');

const { Op } = Sequelize;

/**
 * Handles database operations for the Site.
 * See https://sequelize.org/v5/index.html to learn how to customize it.
 */
class SiteRepository {
  /**
   * Creates the Site.
   *
   * @param {Object} data
   * @param {Object} [options]
   */
  async create(data, options) {
    const currentUser = SequelizeRepository.getCurrentUser(options);

    const tenant = await models.getTenant(currentUser.accountId);

    const transaction = SequelizeRepository.getTransaction(options);

    const record = await tenant.site.create(
      {
        ...pick(data, [
          'id', //
          'status',
          'settings',
          'siteName',
          'sitePhone',
          'siteEmail',
          'siteStreet',
          'siteStreet2',
          'siteCity',
          'siteRegion',
          'sitePostalcode',
          'siteCountry',
          'importHash',
          'updatedAt',
          'createdAt',
        ]),
        createdById: currentUser.id,
        updatedById: currentUser.id,
      },
      {
        transaction,
      }
    );

    await record.setParts(data.parts || [], {
      transaction,
    });
    await record.setMembers(data.members || [], {
      transaction,
    });
    await record.setReportTemplates(data.reportTemplates || [], {
      transaction,
    });
    await record.setReports(data.reports || [], {
      transaction,
    });

    await this._createAuditLog(AuditLogRepository.CREATE, record, data, options);

    return this.findById(record.id, options);
  }

  /**
   * Updates the Site.
   *
   * @param {Object} data
   * @param {Object} [options]
   */
  async update(id, data, options) {
    const currentUser = SequelizeRepository.getCurrentUser(options);

    const tenant = await models.getTenant(currentUser.accountId);

    const transaction = SequelizeRepository.getTransaction(options);

    let record = await tenant.site.findByPk(id, {
      transaction,
    });

    record = await record.update(
      {
        ...pick(data, [
          'id', //
          'status',
          'settings',
          'siteName',
          'sitePhone',
          'siteEmail',
          'siteStreet',
          'siteStreet2',
          'siteCity',
          'siteRegion',
          'sitePostalcode',
          'siteCountry',
          'importHash',
          'updatedAt',
          'createdAt',
        ]),
        updatedById: currentUser.id,
      },
      {
        transaction,
      }
    );

    await record.setParts(data.parts || [], {
      transaction,
    });
    await record.setMembers(data.members || [], {
      transaction,
    });
    await record.setReportTemplates(data.reportTemplates || [], {
      transaction,
    });
    await record.setReports(data.reports || [], {
      transaction,
    });

    await this._createAuditLog(AuditLogRepository.UPDATE, record, data, options);

    return this.findById(record.id, options);
  }

  /**
   * Deletes the Site.
   *
   * @param {string} id
   * @param {Object} [options]
   */
  async destroy(id, options) {
    const currentUser = SequelizeRepository.getCurrentUser(options);

    const tenant = await models.getTenant(currentUser.accountId);

    const transaction = SequelizeRepository.getTransaction(options);

    const record = await tenant.site.findByPk(id, {
      transaction,
    });

    await record.destroy({
      transaction,
    });

    await this._createAuditLog(AuditLogRepository.DELETE, record, null, options);
  }

  /**
   * Finds the Site and its relations.
   *
   * @param {string} id
   * @param {Object} [options]
   */
  async findById(id, options) {
    const currentUser = SequelizeRepository.getCurrentUser(options);

    const tenant = await models.getTenant(currentUser.accountId);

    const transaction = SequelizeRepository.getTransaction(options);

    const include = [];

    const record = await tenant.site.findByPk(id, {
      include,
      transaction,
    });

    return this._fillWithRelationsAndFiles(record, options);
  }

  /**
   * Counts the number of Sites based on the filter.
   *
   * @param {Object} filter
   * @param {Object} [options]
   */
  async count(filter, options) {
    const currentUser = SequelizeRepository.getCurrentUser(options);

    const tenant = await models.getTenant(currentUser.accountId);

    const transaction = SequelizeRepository.getTransaction(options);

    return tenant.site.count(
      {
        where: filter,
      },
      {
        transaction,
      }
    );
  }

  /**
   * Finds the Sites based on the query.
   * See https://sequelize.org/v5/manual/querying.html to learn how to
   * customize the query.
   *
   * @param {Object} query
   * @param {Object} query.filter
   * @param {number} query.limit
   * @param  {number} query.offset
   * @param  {string} query.orderBy
   * @param {Object} [options]
   *
   * @returns {Promise<Object>} response - Object containing the rows and the count.
   */
  async findAndCountAll(
    { filter, limit, offset, orderBy } = {
      filter: null,
      limit: 0,
      offset: 0,
      orderBy: null,
    },
    options
  ) {
    const currentUser = SequelizeRepository.getCurrentUser(options);

    const tenant = await models.getTenant(currentUser.accountId);

    let where = {};
    const include = [];

    if (filter) {
      if (filter.id) {
        where = {
          ...where,
          id: SequelizeFilterUtils.uuid(filter.id),
        };
      }

      if (filter.status) {
        where = {
          ...where,
          status: filter.status,
        };
      }

      if (filter.settings) {
        where = {
          ...where,
          [Op.and]: SequelizeFilterUtils.ilike('site', 'settings', filter.settings),
        };
      }

      if (filter.siteName) {
        where = {
          ...where,
          [Op.and]: SequelizeFilterUtils.ilike('site', 'siteName', filter.siteName),
        };
      }

      if (filter.sitePhone) {
        where = {
          ...where,
          [Op.and]: SequelizeFilterUtils.ilike('site', 'sitePhone', filter.sitePhone),
        };
      }

      if (filter.siteEmail) {
        where = {
          ...where,
          [Op.and]: SequelizeFilterUtils.ilike('site', 'siteEmail', filter.siteEmail),
        };
      }

      if (filter.siteStreet) {
        where = {
          ...where,
          [Op.and]: SequelizeFilterUtils.ilike('site', 'siteStreet', filter.siteStreet),
        };
      }

      if (filter.siteStreet2) {
        where = {
          ...where,
          [Op.and]: SequelizeFilterUtils.ilike('site', 'siteStreet2', filter.siteStreet2),
        };
      }

      if (filter.siteCity) {
        where = {
          ...where,
          [Op.and]: SequelizeFilterUtils.ilike('site', 'siteCity', filter.siteCity),
        };
      }

      if (filter.siteRegion) {
        where = {
          ...where,
          siteRegion: filter.siteRegion,
        };
      }

      if (filter.sitePostalcode) {
        where = {
          ...where,
          [Op.and]: SequelizeFilterUtils.ilike('site', 'sitePostalcode', filter.sitePostalcode),
        };
      }

      if (filter.siteCountry) {
        where = {
          ...where,
          siteCountry: filter.siteCountry,
        };
      }

      if (filter.createdAtRange) {
        const [start, end] = filter.createdAtRange;

        if (start !== undefined && start !== null && start !== '') {
          where = {
            ...where,
            createdAt: {
              ...where.createdAt,
              [Op.gte]: start,
            },
          };
        }

        if (end !== undefined && end !== null && end !== '') {
          where = {
            ...where,
            createdAt: {
              ...where.createdAt,
              [Op.lte]: end,
            },
          };
        }
      }
    }

    let { rows, count } = await tenant.site.findAndCountAll({
      where,
      include,
      limit: limit ? Number(limit) : undefined,
      offset: offset ? Number(offset) : undefined,
      order: orderBy ? [orderBy.split('_')] : [['createdAt', 'DESC']],
      transaction: SequelizeRepository.getTransaction(options),
    });

    rows = await this._fillWithRelationsAndFilesForRows(rows, options);

    return { rows, count };
  }

  /**
   * Lists the Sites to populate the autocomplete.
   * See https://sequelize.org/v5/manual/querying.html to learn how to
   * customize the query.
   *
   * @param {string} query
   * @param {number} limit
   */
  async findAllAutocomplete(query, limit, options) {
    const currentUser = SequelizeRepository.getCurrentUser(options);

    const tenant = await models.getTenant(currentUser.accountId);

    let where = {};

    if (query) {
      where = {
        [Op.or]: [
          { id: SequelizeFilterUtils.uuid(query) },
          {
            [Op.and]: SequelizeFilterUtils.ilike('site', 'siteName', query),
          },
        ],
      };
    }

    const records = await tenant.site.findAll({
      attributes: ['id', 'siteName'],
      where,
      limit: limit ? Number(limit) : undefined,
      orderBy: [['siteName', 'ASC']],
    });

    return records.map((record) => ({
      id: record.id,
      label: record.siteName,
    }));
  }

  /**
   * Creates an audit log of the operation.
   *
   * @param {string} action - The action [create, update or delete].
   * @param {object} record - The sequelize record
   * @param {object} data - The new data passed on the request
   * @param {object} options
   */
  async _createAuditLog(action, record, data, options) {
    let values = {};

    if (data) {
      values = {
        ...record.get({ plain: true }),
        partsIds: data.parts,
        membersIds: data.members,
        reportTemplatesIds: data.reportTemplates,
        reportsIds: data.reports,
      };
    }

    await AuditLogRepository.log(
      {
        entityName: 'site',
        entityId: record.id,
        action,
        values,
      },
      options
    );
  }

  /**
   * Fills an array of Site with relations and files.
   *
   * @param {Array} rows
   * @param {Object} [options]
   */
  async _fillWithRelationsAndFilesForRows(rows, options) {
    if (!rows) {
      return rows;
    }

    return Promise.all(rows.map((record) => this._fillWithRelationsAndFiles(record, options)));
  }

  /**
   * Fill the Site with the relations and files.
   *
   * @param {Object} record
   * @param {Object} [options]
   */
  async _fillWithRelationsAndFiles(record, options) {
    if (!record) {
      return record;
    }

    const output = record.get({ plain: true });

    const transaction = SequelizeRepository.getTransaction(options);

    output.parts = await record.getParts({
      transaction,
    });
    output.members = await record.getMembers({
      transaction,
    });
    output.reportTemplates = await record.getReportTemplates({
      transaction,
    });
    output.reports = await record.getReports({
      transaction,
    });

    return output;
  }
}

module.exports = SiteRepository;
