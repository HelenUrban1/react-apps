const pick = require('lodash/pick');
const Sequelize = require('sequelize');
const models = require('../models');
const SequelizeRepository = require('./sequelizeRepository');
const AuditLogRepository = require('./auditLogRepository');
const UserProfileRepository = require('./userProfileRepository');
const SequelizeFilterUtils = require('../utils/sequelizeFilterUtils');
const ValidationError = require('../../errors/validationError');
const constants = require('../constants').accountMember;
const config = require('../../../config')();
const { log } = require('../../logger');

const { Op } = Sequelize;

const ROLES = {};
constants.ROLES.forEach((role) => (ROLES[role] = role));

/**
 * Handles database operations for the AccountMember.
 * See https://sequelize.org/v5/index.html to learn how to customize it.
 */
class AccountMemberRepository {
  /**
   * Creates the AccountMember.
   *
   * @param {Object} data
   * @param {Object} [options]
   */
  static async create(data, options = {}) {
    if (!data.user || !data.account) {
      log.error('Missing user or account');
      log.debug(`user: ${!!data.user}, account: ${!!data.account}`);
      throw new ValidationError(options.language, 'accountMembers.errors.userAndAccountRequired');
    }

    // Prevent the creation of a duplicate record with the same User and Account associations,
    // as only one AccountMember record can represent a User's membership of a specific Account.
    const existingRecord = await this.findByUserAndAccount(data.user, data.account);
    if (existingRecord) {
      log.error(`Record already exists: ${existingRecord.id}`);
      throw new ValidationError(options.language, 'accountMembers.errors.recordExists');
    }

    // The first member of an Account is automatically granted the Owner role
    const accountMembersCount = await this.count({
      accountId: data.account,
    });

    const isFirstAccountMember = accountMembersCount === 0;
    if (isFirstAccountMember && !options.isTest) {
      data.roles = [ROLES.Owner, ROLES.Admin];
    } else if (!data.roles) {
      data.roles = [];
    }

    const currentUser = SequelizeRepository.getCurrentUser(options);
    const transaction = SequelizeRepository.getTransaction(options);

    const record = await models.accountMember.create(
      {
        ...pick(data, [
          'id', //
          'settings',
          'roles',
          'status',
          'inviteStatus',
          'accessControl',
          'importHash',
          'updatedAt',
          'createdAt',
        ]),
        createdById: currentUser.id,
        updatedById: currentUser.id,
      },
      {
        transaction,
      }
    );

    await record.setAccount(data.account, {
      transaction,
    });

    await record.setSite(data.site || null, {
      transaction,
    });

    await record.setUser(data.user || null, {
      transaction,
    });

    await this._createAuditLog(AuditLogRepository.CREATE, record, data, options);
    return this.findById(record.id, options);
  }

  /**
   * Updates the AccountMember.
   *
   * @param {String} id
   * @param {Object} data
   * @param {Object} [options]
   */
  static async update(id, data, options = {}) {
    // Prevent an update from resulting in the existence of a duplicate record with the same User and Account
    // associations, as only one AccountMember record can represent a User's membership of a specific Account.
    await this._validateDuplicateRecord(id, data, options);

    const currentUser = SequelizeRepository.getCurrentUser(options);
    const targetUser = options.targetUser ? SequelizeRepository.getTargetUser(options) : currentUser;
    if (!currentUser || !targetUser) {
      throw new ValidationError(options.language, 'accountMembers.errors.noCurrentUser');
    }
    const transaction = SequelizeRepository.getTransaction(options);

    let record = await models.accountMember.findByPk(id, {
      transaction,
    });

    // if roles are included, update roles
    if (data.roles && data.roles.length > 0) {
      if (!config || config.env !== 'test')
        await UserProfileRepository.update(targetUser.id, { roles: data.roles }, options).catch((err) => {
          log.error(err);
          log.debug({ targetUser: targetUser.id, roles: data.roles });
          throw err;
        });
    }

    record = await record.update(
      {
        ...pick(data, [
          'id', //
          'settings',
          'roles',
          'status',
          'inviteStatus',
          'accessControl',
          'importHash',
          'updatedAt',
          'createdAt',
        ]),
        updatedById: currentUser.id,
      },
      {
        transaction,
      }
    );
    await this._updateRelations(record, data, transaction);
    await this._createAuditLog(AuditLogRepository.UPDATE, record, data, options);
    return this.findById(record.id, options);
  }

  /**
   *
   * @param {Object} record
   * @param {Object} data
   * @param {Object} transaction
   */
  static async _updateRelations(record, data, transaction) {
    const accountId = data.account || record.accountId;
    await record.setAccount(accountId || null, {
      transaction,
    });
    const siteId = data.site || record.siteId;
    await record.setSite(siteId || null, {
      transaction,
    });
    const userId = data.user || record.userId;
    await record.setUser(userId || null, {
      transaction,
    });
  }

  /**
   * Prevent an update from resulting in the existence of a duplicate record with the same User and Account
   * associations, as only one AccountMember record can represent a User's membership of a specific Account.
   *
   * @param {String} id
   * @param {Object} data
   * @param {Object} options
   */
  static async _validateDuplicateRecord(id, data, options) {
    if (data.user && data.account) {
      const existingRecord = await this.findByUserAndAccount(data.user, data.account);
      if (existingRecord && existingRecord.id !== id && existingRecord.userId === data.user && existingRecord.accountId === data.account) {
        throw new ValidationError(options.language, 'accountMembers.errors.recordExists');
      }
    }
  }

  /**
   * Deletes the AccountMember.
   *
   * @param {string} id
   * @param {Object} [options]
   */
  static async destroy(id, options) {
    const transaction = SequelizeRepository.getTransaction(options);

    const record = await models.accountMember.findByPk(id, {
      transaction,
    });

    await record.destroy({
      transaction,
      force: (options && options.force) || false,
    });

    await this._createAuditLog(AuditLogRepository.DELETE, record, null, options);
  }

  /**
   * Deletes the AccountMembers.
   *
   * @param {Array<String>} ids
   * @param {Object} [options]
   */
  static async destroyAll(ids, options) {
    ids = Array.isArray(ids) ? ids : [ids];

    await Promise.all(ids.map((id) => this.destroy(id, options)));
  }

  /**
   * Finds the AccountMember and its relations.
   *
   * @param {string} id
   * @param {Object} [options]
   */
  static async findById(id, options) {
    const transaction = SequelizeRepository.getTransaction(options);

    const include = [
      {
        model: models.account,
        as: 'account',
      },
      {
        model: models.site,
        as: 'site',
      },
      {
        model: models.user,
        as: 'user',
      },
    ];

    const record = await models.accountMember.findByPk(id, {
      include,
      transaction,
    });

    return this._fillWithRelationsAndFiles(record, options);
  }

  /**
   * Finds the AccountMember by userId and accountId and its relations.
   *
   * @param {String} userId
   * @param {String} accountId
   * @param {Object} [options]
   */
  static async findByUserAndAccount(userId, accountId, options) {
    const transaction = SequelizeRepository.getTransaction(options);

    const include = [
      {
        model: models.account,
        as: 'account',
      },
      {
        model: models.user,
        as: 'user',
      },
    ];

    const record = await models.accountMember.findOne({
      where: {
        userId,
        accountId,
      },
      include,
      transaction,
    });

    return this._fillWithRelationsAndFiles(record, options);
  }

  /**
   * Finds the Account owner
   * @param {String} accountId
   * @param {*} options
   * @returns
   */
  static async findOwnerByAccountId(accountId, options = {}) {
    const transaction = SequelizeRepository.getTransaction(options);

    const include = [
      {
        model: models.account,
        as: 'account',
      },
      {
        model: models.user,
        as: 'user',
      },
    ];

    const rows = await models.accountMember.findAll({
      where: {
        accountId,
      },
      include,
      transaction,
    });

    const ownerMembership = rows.find((membership) => membership.roles.includes('Owner'));
    const owner = await models.user.findByPk(ownerMembership.userId, { transaction });

    if (!owner) {
      throw new ValidationError(options.language, `errors.validation.invalidAdmin`);
    }

    return owner;
  }

  /**
   * Finds the AccountMembers by userId and its relations.
   * Note that if multiple records are returned, callers cannot assume
   * any particular record corresponds to the user's currently selected
   * account association. If the accountId is known, the
   * `findByUserAndAccount()` method should be used instead.
   *
   * @param {String} userId
   * @param {Object} [options]
   */
  static async findByUser(userId, options) {
    const transaction = SequelizeRepository.getTransaction(options);

    const include = [
      {
        model: models.account,
        as: 'account',
      },
      {
        model: models.user,
        as: 'user',
      },
    ];

    let rows = await models.accountMember.findAll({
      where: {
        userId,
      },
      include,
      transaction,
    });

    rows = await this._fillWithRelationsAndFilesForRows(rows, options);

    return rows;
  }

  /**
   * Adds roles to the AccountMember.
   *
   * @param {String} id
   * @param {Array<String>} roles
   * @param {Object} [options]
   * @prop  {Boolean} [options.allowOwnerAddition]
   */
  static async addRoles(id, roles = [], options = {}) {
    const record = await this.findById(id, options);
    if (!record) {
      return record;
    }

    if (options.allowOwnerAddition !== true && roles.includes(ROLES.Owner)) {
      throw new ValidationError(options.language, 'accountMembers.errors.allowOwnerAddition');
    }

    const updatedRoles = new Set(record.roles);
    roles.forEach((role) => updatedRoles.add(role));

    const data = {
      roles: Array.from(updatedRoles),
    };

    return this.update(record.id, data, options);
  }

  /**
   * Remove roles from the AccountMember.
   *
   * @param {String} id
   * @param {Array<String>} roles
   * @param {Object} [options]
   * @prop  {Boolean} [options.allowOwnerRemoval]
   */
  static async removeRoles(id, roles = [], options = {}) {
    const record = await this.findById(id, options);
    if (!record) {
      return record;
    }

    if (options.allowOwnerRemoval !== true && roles.includes(ROLES.Owner)) {
      throw new ValidationError(options.language, 'accountMembers.errors.allowOwnerRemoval');
    }

    const updatedRoles = new Set(record.roles);
    roles.forEach((role) => updatedRoles.delete(role));

    const data = {
      roles: Array.from(updatedRoles),
    };

    return this.update(record.id, data, options);
  }

  /**
   * Reset roles for the AccountMember.
   *
   * @param {String} id
   * @param {Array<String>} roles
   * @param {Object} [options]
   * @prop  {Boolean} [options.allowOwnerSet]
   */
  static async resetRoles(id, roles = [], options = {}) {
    const record = await this.findById(id, options);

    if (!record) {
      log.warn('No Record Found when Resetting User Roles');
      return record;
    }

    if (options.allowOwnerSet !== true && roles.includes(ROLES.Owner)) {
      throw new ValidationError(options.language, 'accountMembers.errors.allowOwnerSet');
    }

    const updatedRoles = new Set(roles);

    const data = {
      roles: Array.from(updatedRoles),
    };

    return this.update(record.id, data, options);
  }

  /**
   * Change an Account's Owner from the current user to another user.
   *
   * @param {Object} params
   * @prop  {String} newOwnerAccountMemberId
   * @prop  {Object} [options]
   *
   * @throws ValidationError
   */
  static async changeAccountOwnerFromCurrentUser(targetUser, currentUser, options = {}) {
    this._validateUserRecord(currentUser, options);
    this._validateUserRecord(targetUser, options);

    const currentUserAccountMember = currentUser.accountMemberships.find((item) => item.id === currentUser.activeAccountMemberId);
    const targetUserAccountMember = targetUser.accountMemberships.find((item) => item.id === targetUser.activeAccountMemberId);

    if (!currentUserAccountMember || !targetUserAccountMember) {
      throw new ValidationError(options.language, 'accountMembers.errors.recordDoesNotExist');
    }

    return this._changeAccountOwner(targetUser, currentUser, targetUserAccountMember, currentUserAccountMember, options);
  }

  /**
   * Validates that the User object is valid
   *
   * @param {Object} currentUser
   * @param {Object} options
   */
  static _validateUserRecord(currentUser, options) {
    if (!currentUser) {
      throw new ValidationError(options.language, 'accountMembers.errors.noCurrentUser');
    }

    // Validate that the User initiating the action is the current Account Owner
    if (!currentUser.accountMemberships || !currentUser.accountMemberships.length) {
      throw new ValidationError(options.language, 'accountMembers.errors.noAccountMembershipsCurrentUser');
    }

    if (currentUser.accountMemberships.length > 1 && !currentUser.activeAccountMemberId) {
      throw new ValidationError(options.language, 'accountMembers.errors.noActiveMembershipCurrentUser');
    }
  }

  /**
   * Change an Account's Owner.
   * Each Account can have only one member who has the Owner role.
   *
   * @param {Object} params
   * @prop  {AccountMember} currentOwnerRecord
   * @prop  {String} newOwnerAccountMemberId
   * @prop  {Object} [options]
   *
   * @throws ValidationError
   */
  static async _changeAccountOwner(targetUser, currentUser, targetUserAccountMember, currentUserAccountMember, options = {}) {
    if (currentUserAccountMember.accountId !== targetUserAccountMember.accountId) {
      throw new ValidationError(options.language, 'accountMembers.errors.accountsDoNotMatch');
    }

    // if (!targetUserAccountMember.roles.includes(ROLES.Admin)) {
    //   throw new ValidationError(options.language, 'accountMembers.errors.newAccountOnwerMustBeAdmin');
    // }

    // swap current owner's roles with the target account member's roles, or convert them to an Admin by default
    const currentUserRoles = targetUserAccountMember.roles && targetUserAccountMember.roles.length > 0 ? targetUserAccountMember.roles : [ROLES.Admin];

    const currentUserOptions = { ...options, currentUser, allowOwnerSet: true };
    await this.resetRoles(currentUserAccountMember.id, currentUserRoles, currentUserOptions);

    const newUserOptions = { ...options, currentUser: targetUser, allowOwnerSet: true };
    await this.resetRoles(targetUserAccountMember.id, [ROLES.Owner], newUserOptions);

    return this.findById(targetUserAccountMember.id, options);
  }

  /**
   * Counts the number of AccountMembers based on the filter.
   *
   * @param {Object} filter
   * @param {Object} [options]
   */
  static async count(filter, options) {
    const transaction = SequelizeRepository.getTransaction(options);

    return models.accountMember.count(
      {
        where: filter,
      },
      {
        transaction,
      }
    );
  }

  /**
   * Finds the AccountMembers based on the query.
   * See https://sequelize.org/v5/manual/querying.html to learn how to
   * customize the query.
   *
   * @param {Object} query
   * @prop  {Object} [query.filter]
   * @prop  {Number} [query.limit]
   * @prop  {Number} [query.offset]
   * @prop  {String} [query.orderBy]
   * @param {Object} [options]
   *
   * @returns {Promise<Object>} response - Object containing the rows and the count.
   */
  static async findAndCountAll(
    { filter, limit, offset, orderBy } = {
      filter: null,
      limit: 0,
      offset: 0,
      orderBy: null,
    },
    options
  ) {
    let where = {};
    const include = [
      {
        model: models.account,
        as: 'account',
      },
      {
        model: models.site,
        as: 'site',
      },
      {
        model: models.user,
        as: 'user',
      },
    ];

    if (filter) {
      if (filter.id) {
        where = {
          ...where,
          id: SequelizeFilterUtils.uuid(filter.id),
        };
      }

      if (filter.account) {
        where = {
          ...where,
          accountId: SequelizeFilterUtils.uuid(filter.account),
        };
      }

      if (filter.site) {
        where = {
          ...where,
          siteId: SequelizeFilterUtils.uuid(filter.site),
        };
      }

      if (filter.user) {
        where = {
          ...where,
          userId: SequelizeFilterUtils.uuid(filter.user),
        };
      }

      if (filter.settings) {
        where = {
          ...where,
          [Op.and]: SequelizeFilterUtils.ilike('accountMember', 'settings', filter.settings),
        };
      }

      if (filter.paid === true && Array.isArray(filter.PAID_ROLES)) {
        where = {
          ...where,
          roles: {
            [Op.or]: this._buildPaidRoleQuery(filter.PAID_ROLES),
          },
        };
      }

      if (filter.paid === false && Array.isArray(filter.PAID_ROLES)) {
        where = {
          ...where,
          [Op.not]: {
            roles: {
              [Op.or]: this._buildPaidRoleQuery(filter.PAID_ROLES),
            },
          },
        };
      }

      if (filter.roles && Array.isArray(filter.roles)) {
        where = {
          ...where,
          roles: {
            [Op.or]: filter.roles.map((role) => {
              return {
                [Op.contains]: [role],
              };
            }),
          },
        };
      }

      if (filter.status) {
        where = {
          ...where,
          status: filter.status,
        };
      }

      if (filter.inviteStatus) {
        where = {
          ...where,
          inviteStatus: filter.inviteStatus,
        };
      }

      if (filter.createdAtRange) {
        const [start, end] = filter.createdAtRange;

        if (start !== undefined && start !== null && start !== '') {
          where = {
            ...where,
            createdAt: {
              ...where.createdAt,
              [Op.gte]: start,
            },
          };
        }

        if (end !== undefined && end !== null && end !== '') {
          where = {
            ...where,
            createdAt: {
              ...where.createdAt,
              [Op.lte]: end,
            },
          };
        }
      }
    }

    let { rows, count } = await models.accountMember.findAndCountAll({
      where,
      include,
      limit: limit ? Number(limit) : undefined,
      offset: offset ? Number(offset) : undefined,
      order: orderBy ? [orderBy.split('_')] : [['createdAt', 'DESC']],
      transaction: SequelizeRepository.getTransaction(options),
    });

    rows = await this._fillWithRelationsAndFilesForRows(rows, options);

    return { rows, count };
  }

  /**
   * Lists the AccountMembers to populate the autocomplete.
   * See https://sequelize.org/v5/manual/querying.html to learn how to
   * customize the query.
   *
   * @param {string} query
   * @param {number} limit
   */
  static async findAllAutocomplete(query, limit) {
    let where = {};

    if (query) {
      where = {
        [Op.or]: [{ id: SequelizeFilterUtils.uuid(query) }],
      };
    }

    const records = await models.accountMember.findAll({
      attributes: ['id', 'id'],
      where,
      limit: limit ? Number(limit) : undefined,
      orderBy: [['id', 'ASC']],
    });

    return records.map((record) => ({
      id: record.id,
      label: record.id,
    }));
  }

  /**
   * Creates an audit log of the operation.
   *
   * @param {string} action - The action [create, update or delete].
   * @param {object} record - The sequelize record
   * @param {object} data - The new data passed on the request
   * @param {object} options
   */
  static async _createAuditLog(action, record, data, options) {
    let values = {};

    if (data) {
      values = {
        ...record.get({ plain: true }),
      };
    }

    await AuditLogRepository.log(
      {
        entityName: 'accountMember',
        entityId: record.id,
        action,
        values,
      },
      options
    );
  }

  /**
   * Fills an array of AccountMember with relations and files.
   *
   * @param {Array} rows
   * @param {Object} [options]
   */
  static async _fillWithRelationsAndFilesForRows(rows, options) {
    if (!rows) {
      return rows;
    }

    return Promise.all(rows.map((record) => this._fillWithRelationsAndFiles(record, options)));
  }

  /**
   * Fill the AccountMember with the relations and files.
   *
   * @param {Object} record
   * @param {Object} [options]
   */
  static async _fillWithRelationsAndFiles(record, options = {}) {
    if (!record) {
      return record;
    }

    if (options.modelInstance !== true) {
      return record.get({ plain: true });
    }
    return record;
  }

  /**
   * The `paid` field is virtual and cannot be directly queried. A filter based
   * on `roles` must be used, and unfortunately the business logic that determines
   * the paid/unpaid status that is housed in roles.js needs to be duplicated here.
   *
   * @param {Array<String>} paidRoles - The list of paid roles
   * @return {Array<Object>} - The query for records with `roles` that results in `paid` being true
   */
  static _buildPaidRoleQuery(paidRoles = []) {
    const paidRolesFilter = paidRoles.map((paidRole) => {
      return {
        [Op.contains]: [paidRole],
      };
    });

    // Records with an empty `roles` field count as paid
    paidRolesFilter.push({ [Op.eq]: [] });

    return paidRolesFilter;
  }
}

module.exports = AccountMemberRepository;
