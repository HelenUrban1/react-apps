const SequelizeRepository = require('./sequelizeRepository');
const CharacteristicRepository = require('./characteristicRepository');
const { data, createRepoInstance } = require('../../__fixtures__').characteristic;
const { data: accountData } = require('../../__fixtures__').account;
const config = require('../../../config')();

describe('CharacteristicRepository', () => {
  let repository;
  let auditLogSpy;
  const options = {
    currentUser: {
      accountId: config.testAccountId,
    },
  };

  beforeEach(async () => {
    await SequelizeRepository.cleanDatabase(options.currentUser.accountId);
    repository = createRepoInstance();
    auditLogSpy = jest.spyOn(repository, '_createAuditLog');
  });

  describe('#create()', () => {
    it('should create an item', async () => {
      const fixture = data[0];
      const item = await repository.create(fixture, options);

      expect(item.id).toBe(fixture.id);
    });

    it('should create an item and update a previousRevision', async () => {
      const fixture = data[0];
      const fixtureTwo = data[1];
      const item = await repository.create(fixture, options);

      const itemTwo = await repository.create({ ...fixtureTwo, previousRevision: item.id }, options);
      const updatedItem = await repository.findById(item.id, options);

      expect(item.id).toBe(fixture.id);
      expect(itemTwo.id).toBe(fixtureTwo.id);
      expect(itemTwo.previousRevision).toBe(fixture.id);
      expect(updatedItem.nextRevision).toBe(itemTwo.id);
    });
  });

  describe('#createInAccount()', () => {
    it('should create an item', async () => {
      const fixture = { ...data[0], accountId: accountData[0].id };
      const item = await repository.createInAccount(fixture, options);

      expect(item.id).toBe(fixture.id);
    });
  });

  describe('#update()', () => {
    it('should update an item', async () => {
      const originalItem = await repository.create(data[0], options);

      const updatedNominal = '1.20';
      const updatedItem = await repository.update(originalItem.id, { nominal: updatedNominal }, options);

      expect(originalItem.id).toBe(updatedItem.id);
      expect(updatedItem.nominal).toBe(updatedNominal);
    });
  });

  describe('#destroy()', () => {
    it('should remove an item', async () => {
      const originalItem = await repository.create(data[0], options);

      await repository.destroy(originalItem.id, options);

      const foundItem = await repository.findById(originalItem.id, options);
      expect(foundItem).toBe(null);
    });
  });

  describe('#findById()', () => {
    it('should find an existing item', async () => {
      const originalItem = await repository.create(data[0], options);

      const foundItem = await repository.findById(originalItem.id, options);
      expect(originalItem).toStrictEqual(foundItem);
    });
  });

  describe('#count()', () => {
    it('should count the total number of items', async () => {
      await repository.create(data[0], options);

      const count = await repository.count(null, options);
      expect(count).toBe(1);
    });
  });

  describe('#findAndCountAll()', () => {
    it('should find and count all the items', async () => {
      const originalItem = await repository.create(data[0], options);

      const result = await repository.findAndCountAll({}, options);
      expect(result.count).toBe(1);
      expect(result.rows[0]).toStrictEqual(originalItem);
    });
  });

  describe('#findAllAutocomplete()', () => {
    it('should find all items for a list', async () => {
      const originalItem = await repository.create(data[0], options);

      const result = await repository.findAllAutocomplete(null, null, options);
      expect(result[0].id).toBe(originalItem.id);
    });
  });

  describe('#convertToInput', () => {
    it('should convert a characteristic into a graphQL input', async () => {
      const originalItem = await repository.create(data[0], options);
      const input = CharacteristicRepository.convertToInput({ ...originalItem });
      expect(input.id).toBe(undefined);
      expect(input.originalCharacteristic).toBe(originalItem.id);
    });
  });

  afterAll(async () => {
    await SequelizeRepository.closeConnections(options.currentUser.accountId);
    await SequelizeRepository.closeConnections();
  });
});
