const pick = require('lodash/pick');
const Sequelize = require('sequelize');
const models = require('../models');
const SequelizeRepository = require('./sequelizeRepository');
const AuditLogRepository = require('./auditLogRepository');
const FileRepository = require('./fileRepository');
const SequelizeFilterUtils = require('../utils/sequelizeFilterUtils');
const part = require('../models/part');

const { log } = require('../../logger');

const { Op } = Sequelize;
const fields = [
  'id',
  'purchaseOrderCode',
  'purchaseOrderLink',
  'type',
  'name',
  'number',
  'revision',
  'drawingNumber',
  'drawingRevision',
  'defaultMarkerOptions',
  'defaultLinearTolerances',
  'defaultAngularTolerances',
  'presets',
  'measurement',
  'status',
  'reviewStatus',
  'autoMarkupStatus',
  'workflowStage',
  'currentStep',
  'maxStep',
  'completedAt',
  'balloonedAt',
  'reportGeneratedAt',
  'lastReportId',
  'notes',
  'accessControl',
  'measurement',
  'tags',
  'importHash',
  'displayLeaderLines',
  'previousRevision',
  'nextRevision',
  'originalPart',
  'updatedAt',
  'createdAt',
];

/**
 * Handles database operations for the Part.
 * See https://sequelize.org/v5/index.html to learn how to customize it.
 */
class PartRepository {
  /**
   * Creates the Part.
   *
   * @param {Object} data
   * @param {Object} [options]
   */
  async create(data, options) {
    const currentUser = SequelizeRepository.getCurrentUser(options);

    const transaction = SequelizeRepository.getTransaction(options);

    const tenant = await models.getTenant(currentUser.accountId);

    const record = await tenant.part.create(
      {
        ...pick(data, fields),
        siteId: currentUser.siteId,
        createdById: currentUser.id,
        updatedById: currentUser.id,
      },
      {
        transaction,
      },
    );

    await record.setCustomer(data.customer || null, {
      transaction,
    });
    await record.setPrimaryDrawing(data.primaryDrawing || null, {
      transaction,
    });
    await record.setDrawings(data.drawings || [], {
      transaction,
    });
    await record.setCharacteristics(data.characteristics || [], {
      transaction,
    });
    await record.setReports(data.reports || [], {
      transaction,
    });

    if (!data.originalPart) {
      // If this is the original part, set originalPart to the id
      await record.setOriginalPart(record.id, { transaction });
    }

    if (data.previousRevision) {
      // if this was a new revision, set the nextRevision pointer on the previousRevision
      const previousRecord = await tenant.part.findByPk(data.previousRevision.id || data.previousRevision, {
        transaction,
      });
      await previousRecord.setNextRevision(record.id || null, {
        transaction,
      });
    }

    if (data.attachments) {
      await FileRepository.replaceRelationFiles(
        {
          belongsTo: tenant.part.getTableName(),
          belongsToColumn: 'file',
          belongsToId: record.id,
        },
        data.attachments,
        options,
      );
    }

    await this._createAuditLog(AuditLogRepository.CREATE, record, data, options);

    return this.findById(record.id, options);
  }

  /**
   * Updates the Part.
   *
   * @param {Object} data
   * @param {Object} [options]
   */
  async update(id, data, options) {
    const currentUser = SequelizeRepository.getCurrentUser(options);

    const transaction = SequelizeRepository.getTransaction(options);

    const tenant = await models.getTenant(currentUser.accountId);

    let record = await tenant.part.findByPk(id, {
      transaction,
    });

    record = await record.update(
      {
        ...pick(data, fields),
        siteId: currentUser.siteId,
        updatedById: currentUser.id,
      },
      {
        transaction,
      },
    );

    if (data.customer) {
      await record.setCustomer(data.customer, {
        transaction,
      });
    }
    if (data.primaryDrawing) {
      await record.setPrimaryDrawing(data.primaryDrawing, {
        transaction,
      });
      log.debug('set new primary drawing');
    }
    if (data.drawings) {
      await record.setDrawings(data.drawings || [], {
        transaction,
      });
    }
    /* await record.setCharacteristics(data.characteristics || [], {
      transaction,
    }); */
    if (data.reports) {
      await record.setReports(data.reports || [], {
        transaction,
      });
    }

    if (data.attachments) {
      await FileRepository.replaceRelationFiles(
        {
          belongsTo: tenant.part.getTableName(),
          belongsToColumn: 'file',
          belongsToId: id,
        },
        data.attachments,
        options,
      );
    }

    await this._createAuditLog(AuditLogRepository.UPDATE, record, data, options);

    log.debug('finding by ID');

    return this.findById(record.id, options);
  }

  /**
   * A method to update a part in a tenant database using the specified account id.
   * @param {string} id
   * @param {Object} data
   * @param {string} accountId
   * @param {Object} options
   * @returns The updated part with all relationships
   */
  async updateForUserAndTenant(id, accountId, data, options) {
    const currentUser = SequelizeRepository.getCurrentUser(options);

    const transaction = SequelizeRepository.getTransaction(options);

    const tenant = await models.getTenant(accountId);

    let record = await tenant.part.findByPk(id, {
      transaction,
    });

    record = await record.update(
      {
        ...pick(data, fields),
        siteId: currentUser.siteId,
        updatedById: currentUser.id,
      },
      {
        transaction,
      },
    );

    if (data.customer) {
      await record.setCustomer(data.customer, {
        transaction,
      });
    }
    if (data.primaryDrawing) {
      await record.setPrimaryDrawing(data.primaryDrawing, {
        transaction,
      });
      log.debug('set new primary drawing');
    }
    if (data.drawings) {
      await record.setDrawings(data.drawings || [], {
        transaction,
      });
    }
    /* await record.setCharacteristics(data.characteristics || [], {
      transaction,
    }); */
    if (data.reports) {
      await record.setReports(data.reports || [], {
        transaction,
      });
    }

    if (data.attachments) {
      await FileRepository.replaceRelationFiles(
        {
          belongsTo: tenant.part.getTableName(),
          belongsToColumn: 'file',
          belongsToId: id,
        },
        data.attachments,
        options,
      );
    }

    await this._createAuditLog(AuditLogRepository.UPDATE, record, data, options);

    log.debug('finding by ID');

    return this.findById(record.id, options);
  }

  /**
   * Deletes the Part.
   *
   * @param {string} id
   * @param {Object} [options]
   */
  async destroy(id, options) {
    const currentUser = SequelizeRepository.getCurrentUser(options);

    const transaction = SequelizeRepository.getTransaction(options);

    const tenant = await models.getTenant(currentUser.accountId);

    const record = await tenant.part.findByPk(id, {
      transaction,
    });

    await record.destroy({
      transaction,
    });

    await this._createAuditLog(AuditLogRepository.DELETE, record, null, options);
  }

  /**
   * Finds the Part and its relations.
   *
   * @param {string} id
   * @param {Object} [options]
   */
  async findById(id, options) {
    const currentUser = SequelizeRepository.getCurrentUser(options);
    const transaction = SequelizeRepository.getTransaction(options);

    const tenant = await models.getTenant(currentUser.accountId);

    const include = [
      {
        model: tenant.organization,
        as: 'customer',
        required: false,
      },
      {
        model: tenant.drawing,
        as: 'primaryDrawing',
        required: false,
      },
    ];

    const record = await tenant.part.findByPk(id, {
      include,
      transaction,
    });

    log.debug('filling with relations and files');

    return this._fillWithRelationsAndFiles(record, options);
  }

  async findByIdInAccount(id, accountId, options) {
    const transaction = SequelizeRepository.getTransaction(options);

    const tenant = await models.getTenant(accountId);

    const include = [
      {
        model: tenant.organization,
        as: 'customer',
        required: false,
      },
      {
        model: tenant.drawing,
        as: 'primaryDrawing',
        required: false,
      },
    ];

    const record = await tenant.part.findByPk(id, {
      include,
      transaction,
    });

    return this._fillWithRelationsAndFiles(record, options);
  }

  /**
   * Counts the number of Parts based on the filter.
   *
   * @param {Object} filter
   * @param {Object} [options]
   */
  static async count(filter, options) {
    const currentUser = SequelizeRepository.getCurrentUser(options);

    const tenant = await models.getTenant(currentUser.accountId);

    return tenant.part.count({
      where: filter,
    });
  }

  /**
   * Finds the Parts based on the query.
   * See https://sequelize.org/v5/manual/querying.html to learn how to
   * customize the query.
   *
   * @param {Object} query
   * @param {Object} query.filter
   * @param {number} query.limit
   * @param  {number} query.offset
   * @param  {string} query.orderBy
   * @param {Object} [options]
   *
   * @returns {Promise<Object>} response - Object containing the rows and the count.
   */
  async findAndCountAll(
    { filter, limit, offset, orderBy, shallow } = {
      filter: null,
      limit: 0,
      offset: 0,
      orderBy: null,
      shallow: false,
    },
    options,
  ) {
    const currentUser = SequelizeRepository.getCurrentUser(options);

    const tenant = await models.getTenant(currentUser.accountId);

    let where = {};
    const include = [
      {
        model: tenant.organization,
        as: 'customer',
      },
      {
        model: tenant.drawing,
        as: 'primaryDrawing',
        include: [
          {
            model: tenant.drawingSheet,
            as: 'sheets',
            separate: true,
          },
        ],
      },
      // Removed to fix count, since metro is currently not in use or development
      // {
      //   model: tenant.job,
      //   as: 'jobs',
      //   through: {
      //     attributes: ['jobId', 'partId'],
      //   },
      //   duplicating: false,
      // },
    ];

    if (filter) {
      if (filter.id) {
        where = {
          ...where,
          id: SequelizeFilterUtils.uuid(filter.id),
        };
      }

      if (filter.originalPart) {
        where = {
          ...where,
          originalPart: SequelizeFilterUtils.uuid(filter.originalPart),
        };
      }

      if (filter.purchaseOrderCode) {
        where = {
          ...where,
          [Op.and]: SequelizeFilterUtils.ilike('part', 'purchaseOrderCode', filter.purchaseOrderCode),
        };
      }

      if (filter.nextRevision || filter.nextRevision === null) {
        where = {
          ...where,
          nextRevision: filter.nextRevision,
        };
      }

      if (filter.purchaseOrderLink) {
        where = {
          ...where,
          [Op.and]: SequelizeFilterUtils.ilike('part', 'purchaseOrderLink', filter.purchaseOrderLink),
        };
      }

      if (filter.type) {
        where = {
          ...where,
          type: filter.type,
        };
      }

      if (filter.name) {
        where = {
          ...where,
          [Op.and]: SequelizeFilterUtils.ilike('part', 'name', filter.name),
        };
      }

      if (filter.number) {
        where = {
          ...where,
          [Op.and]: SequelizeFilterUtils.ilike('part', 'number', filter.number),
        };
      }

      if (filter.revision) {
        where = {
          ...where,
          [Op.and]: SequelizeFilterUtils.ilike('part', 'revision', filter.revision),
        };
      }

      if (filter.drawingNumber) {
        where = {
          ...where,
          [Op.and]: SequelizeFilterUtils.ilike('part', 'drawingNumber', filter.drawingNumber),
        };
      }

      if (filter.drawingRevision) {
        where = {
          ...where,
          [Op.and]: SequelizeFilterUtils.ilike('part', 'drawingRevision', filter.drawingRevision),
        };
      }

      if (filter.defaultMarkerOptions) {
        where = {
          ...where,
          [Op.and]: SequelizeFilterUtils.ilike('part', 'defaultMarkerOptions', filter.defaultMarkerOptions),
        };
      }

      if (filter.defaultLinearTolerances) {
        where = {
          ...where,
          [Op.and]: SequelizeFilterUtils.ilike('part', 'defaultLinearTolerances', filter.defaultLinearTolerances),
        };
      }

      if (filter.defaultAngularTolerances) {
        where = {
          ...where,
          [Op.and]: SequelizeFilterUtils.ilike('part', 'defaultAngularTolerances', filter.defaultAngularTolerances),
        };
      }

      if (filter.presets) {
        where = {
          ...where,
          [Op.and]: SequelizeFilterUtils.ilike('part', 'presets', filter.presets),
        };
      }

      if (filter.measurement) {
        where = {
          ...where,
          measurement: filter.measurement,
        };
      }

      if (filter.status) {
        where = {
          ...where,
          status: filter.status,
        };
      }

      if (filter.workflowStage) {
        where = {
          ...where,
          [Op.and]: SequelizeFilterUtils.ilike('part', 'workflowStage', filter.workflowStage),
        };
      }

      if (filter.reviewStatus && filter.reviewStatus.length > 0) {
        where = {
          ...where,
          reviewStatus: filter.reviewStatus,
        };
      }

      if (filter.completedAtRange) {
        const [start, end] = filter.completedAtRange;

        if (start !== undefined && start !== null && start !== '') {
          where = {
            ...where,
            completedAt: {
              ...where.completedAt,
              [Op.gte]: start,
            },
          };
        }

        if (end !== undefined && end !== null && end !== '') {
          where = {
            ...where,
            completedAt: {
              ...where.completedAt,
              [Op.lte]: end,
            },
          };
        }
      }

      if (filter.balloonedAtRange) {
        const [start, end] = filter.balloonedAtRange;

        if (start !== undefined && start !== null && start !== '') {
          where = {
            ...where,
            balloonedAt: {
              ...where.balloonedAt,
              [Op.gte]: start,
            },
          };
        }

        if (end !== undefined && end !== null && end !== '') {
          where = {
            ...where,
            balloonedAt: {
              ...where.balloonedAt,
              [Op.lte]: end,
            },
          };
        }
      }

      if (filter.reportGeneratedAtRange) {
        const [start, end] = filter.reportGeneratedAtRange;

        if (start !== undefined && start !== null && start !== '') {
          where = {
            ...where,
            reportGeneratedAt: {
              ...where.reportGeneratedAt,
              [Op.gte]: start,
            },
          };
        }

        if (end !== undefined && end !== null && end !== '') {
          where = {
            ...where,
            reportGeneratedAt: {
              ...where.reportGeneratedAt,
              [Op.lte]: end,
            },
          };
        }
      }

      if (filter.lastReportId) {
        where = {
          ...where,
          type: filter.type,
        };
      }

      if (filter.notes) {
        where = {
          ...where,
          [Op.and]: SequelizeFilterUtils.ilike('part', 'notes', filter.notes),
        };
      }

      if (filter.accessControl && filter.accessControl.length > 0) {
        where = {
          ...where,
          accessControl: filter.accessControl,
        };
      }

      if (filter.tags) {
        where = {
          ...where,
          [Op.and]: SequelizeFilterUtils.ilike('part', 'tags', filter.tags),
        };
      }

      if (filter.site) {
        where = {
          ...where,
          siteId: SequelizeFilterUtils.uuid(filter.site),
        };
      }
      if (filter.customer && filter.customer.length > 0) {
        where = {
          ...where,
          customerId: filter.customer.map((item) => {
            return SequelizeFilterUtils.uuid(item);
          }),
        };
      }

      if (filter.primaryDrawing) {
        where = {
          ...where,
          primaryDrawingId: SequelizeFilterUtils.uuid(filter.primaryDrawing),
        };
      }

      if (filter.jobs) {
        where = {
          ...where,
          jobId: SequelizeFilterUtils.uuid(filter.jobs),
        };
      }

      if (filter.createdAtRange) {
        const [start, end] = filter.createdAtRange;

        if (start !== undefined && start !== null && start !== '') {
          where = {
            ...where,
            createdAt: {
              ...where.createdAt,
              [Op.gte]: start,
            },
          };
        }

        if (end !== undefined && end !== null && end !== '') {
          where = {
            ...where,
            createdAt: {
              ...where.createdAt,
              [Op.lte]: end,
            },
          };
        }
      }
      if (filter.searchTerm) {
        where = {
          ...where,
          [Op.or]: [SequelizeFilterUtils.ilike('part', 'name', filter.searchTerm), SequelizeFilterUtils.ilike('part', 'number', filter.searchTerm), SequelizeFilterUtils.ilike('customer', 'name', filter.searchTerm)],
        };
      }
      if (filter.displayLeaderLines) {
        where = {
          ...where,
          displayLeaderLines: filter.displayLeaderLines,
        };
      }
    }

    let { rows, count } = await tenant.part.findAndCountAll({
      where,
      include: shallow ? [] : include,
      limit: limit ? Number(limit) : undefined,
      offset: offset ? Number(offset) : undefined,
      order: orderBy ? [orderBy.split('_')] : [['createdAt', 'DESC']],
      transaction: SequelizeRepository.getTransaction(options),
      logging: console.log,
      subQuery: false,
    });

    rows = await this._fillWithRelationsAndFilesForRows(rows, { ...options, shallow });

    return { rows, count };
  }

  /**
   * Lists the Parts to populate the autocomplete.
   * See https://sequelize.org/v5/manual/querying.html to learn how to
   * customize the query.
   *
   * @param {string} query
   * @param {number} limit
   */
  async findAllAutocomplete(query, limit, options) {
    const currentUser = SequelizeRepository.getCurrentUser(options);

    const tenant = await models.getTenant(currentUser.accountId);

    let where = {};

    if (query) {
      where = {
        [Op.or]: [
          { id: SequelizeFilterUtils.uuid(query) },
          {
            [Op.and]: SequelizeFilterUtils.ilike('part', 'name', query),
          },
        ],
      };
    }

    const records = await tenant.part.findAll({
      attributes: ['id', 'name'],
      where,
      limit: limit ? Number(limit) : undefined,
      orderBy: [['name', 'ASC']],
    });

    return records.map((record) => ({
      id: record.id,
      label: record.name,
    }));
  }

  /**
   * Marks the desired part as deleted in the database.
   *
   * @param {string} id
   * @param {options} options
   */
  async markAsDeleted(id, options) {
    const currentUser = SequelizeRepository.getCurrentUser(options);
    const transaction = SequelizeRepository.getTransaction(options);
    const tenant = await models.getTenant(currentUser.accountId);

    const record = await tenant.part.findByPk(id, {
      transaction,
    });

    const data = {
      status: 'Deleted',
      updatedById: currentUser.id,
      deletedAt: new Date(),
    };

    await record.update(data, { transaction });

    await this._createAuditLog(AuditLogRepository.DELETE, record, data, options);
  }

  /**
   * Creates an audit log of the operation.
   *
   * @param {string} action - The action [create, update or delete].
   * @param {object} record - The sequelize record
   * @param {object} data - The new data passed on the request
   * @param {object} options
   */
  async _createAuditLog(action, record, data, options) {
    let values = {};

    if (data) {
      values = {
        ...record.get({ plain: true }),
        drawingsIds: data.drawings,
        characteristicsIds: data.characteristics,
        reportsIds: data.reports,
      };
    }

    await AuditLogRepository.log(
      {
        entityName: 'part',
        entityId: record.id,
        action,
        values,
      },
      options,
    );
  }

  /**
   * Fills an array of Part with relations and files.
   *
   * @param {Array} rows
   * @param {Object} [options]
   */
  async _fillWithRelationsAndFilesForRows(rows, options) {
    if (!rows) {
      return rows;
    }

    return Promise.all(rows.map((record) => this._fillWithRelationsAndFiles(record, options)));
  }

  /**
   * Fill the Part with the relations and files.
   *
   * @param {Object} record
   * @param {Object} [options]
   */
  async _fillWithRelationsAndFiles(record, options) {
    if (!record) {
      return record;
    }

    const output = record.get({ plain: true });

    const transaction = SequelizeRepository.getTransaction(options);

    output.characteristics = await record.getCharacteristics({
      transaction,
      include: [
        {
          model: models.drawing,
          as: 'drawing',
        },
        {
          model: models.drawingSheet,
          as: 'drawingSheet',
        },
      ],
    });

    if (options.shallow) {
      output.characteristics = output.characteristics && output.characteristics.length > 0;
      return output;
    }

    output.drawings = await record.getDrawings({
      transaction,
      include: [
        {
          model: models.drawingSheet,
          as: 'sheets',
        },
        {
          model: models.file,
          as: 'drawingFile',
        },
      ],
    });

    output.reports = await record.getReports({
      transaction,
    });
    output.jobs = await record.getJobs({
      transaction,
    });
    output.attachments = await record.getAttachments({
      transaction,
    });

    return output;
  }

  static convertToInput(record) {
    if (!record || !record.drawings) {
      throw new Error(`No ${record ? 'drawings' : 'data'} passed to create an input`);
    }
    const drawings = record.drawings ? record.drawings.map((drawing) => drawing.id) : [];
    const characteristics = record.characteristics ? record.characteristics.map((char) => char.id) : [];
    const reports = record.reports ? record.reports.map((report) => report.id) : [];
    const input = {
      customer: record.customer ? record.customer.id || '' : null,
      type: record.type ? record.type : 'Part',
      originalPart: record.originalPart,
      previousRevision: record.previousRevision,
      nextRevision: record.nextRevision,
      name: record.name,
      number: record.number,
      revision: record.revision,
      drawingName: record.drawingName,
      drawingNumber: record.drawingNumber,
      drawingRevision: record.drawingRevision,
      defaultMarkerOptions: record.defaultMarkerOptions,
      defaultLinearTolerances: record.defaultLinearTolerances,
      defaultAngularTolerances: record.defaultAngularTolerances,
      presets: record.presets,
      status: record.status,
      reviewStatus: record.reviewStatus,
      workflowStage: record.workflowStage,
      completedAt: record.completedAt,
      balloonedAt: record.balloonedAt,
      reportGeneratedAt: record.reportGeneratedAt,
      notes: record.notes,
      accessControl: record.accessControl,
      measurement: record.measurement,
      displayLeaderLines: record.displayLeaderLines,
      tags: record.tags,
      purchaseOrderCode: record.purchaseOrderCode,
      purchaseOrderLink: record.purchaseOrderLink,
      primaryDrawing: record.primaryDrawing ? record.primaryDrawing.id : drawings[0],
      attachments: record.attachments,
      drawings,
      characteristics, // characteristics,
      reports,
    };

    return input;
  }
}

module.exports = PartRepository;
