const AccountRepository = require('./accountRepository');
const AccountMemberRepository = require('./accountMemberRepository');
const SequelizeRepository = require('./sequelizeRepository');
const { data, createRepoInstance } = require('../../__fixtures__').account;
const  accountMemberData  = require('../../__fixtures__').accountMember.data;

const SAMPLE_UUID = 'f44ffabd-790e-49e4-8417-95abf99a1e19'; // Generated value that is extremely unlikely to match a dynamically generated UUID value

describe('AccountRepository', () => {
  let accountRepository;
  let auditLogSpy;

  beforeEach(async () => {
    await SequelizeRepository.cleanDatabase();
    accountRepository = createRepoInstance();
    auditLogSpy = jest.spyOn(accountRepository, '_createAuditLog');
  });

  afterAll(async () => {
    await SequelizeRepository.closeConnections();
  });

  describe('#create()', () => {
    it('should create an item', async () => {
      let count = await accountRepository.count({});
      expect(count).toBe(0);

      const fixture = data[0];

      const item = await accountRepository.create(fixture);
      expect(item).toBeDefined();
      expect(item.id).toBe(fixture.id);
      expect(auditLogSpy).toHaveBeenCalled();

      count = await accountRepository.count({});
      expect(count).toBe(1);
    });

    it('should associated provided AccountMembers', async () => {
      const accountMemberFixtures = Object.values(accountMemberData).map((item) => {
        return Object.assign({}, item, { account: data[0].id });
      });

      await Promise.all(accountMemberFixtures.map((item) => AccountMemberRepository.create(item)));

      const fixture = Object.assign({}, data[0], { members: accountMemberFixtures.map((item) => item.id) });

      const item = await accountRepository.create(fixture);

      expect(item.id).toBe(fixture.id);
      expect(auditLogSpy).toHaveBeenCalled();

      expect(item.members.length).toBe(accountMemberFixtures.length);
    });

  });

  describe('#update()', () => {
    it('should update an item', async () => {
      const updatedPhoneNumber = '111-222-3333';
      const originalItem = await accountRepository.create(data[0]);
      expect(originalItem.phone).not.toBe(updatedPhoneNumber);

      const updatedItem = await accountRepository.update(originalItem.id, { orgPhone: updatedPhoneNumber });

      expect(originalItem.id).toBe(updatedItem.id);
      expect(updatedItem.orgPhone).toBe(updatedPhoneNumber);
      expect(auditLogSpy).toHaveBeenCalled();
    });
  });

  describe('#destroy()', () => {
    it('should destroy an item', async () => {
      await Promise.all(data.map((item) => accountRepository.create(item)));

      await accountRepository.destroy(data[0].id);

      let count = await accountRepository.count({});
      expect(count).toBe(data.length - 1);
      expect(auditLogSpy).toHaveBeenCalled();
    });

    it('should destory all of the AccountMember records', async () => {
      const accountMemberFixtures = Object.values(accountMemberData).map((item) => {
        return Object.assign({}, item, { account: data[0].id });
      });

      await Promise.all(accountMemberFixtures.map((item) => AccountMemberRepository.create(item)));

      const fixture = Object.assign({}, data[0], { members: accountMemberFixtures.map((item) => item.id) });
      await accountRepository.create(fixture);

      let accountMembers = await AccountMemberRepository.count({ accountId: data[0].id });
      expect(accountMembers).toBe(accountMemberFixtures.length);

      await accountRepository.destroy(data[0].id);

      accountMembers = await AccountMemberRepository.count({ accountId: data[0].id });
      expect(accountMembers).toBe(0);
      expect(auditLogSpy).toHaveBeenCalled();
    });

  });

  describe('#findById()', () => {
    it('should return null when a matching item does not exist', async () => {
      const item = await accountRepository.findById(SAMPLE_UUID);
      expect(item).toBe(null);
    });

    it('should find an item that exists', async () => {
      await Promise.all(data.map((item) => accountRepository.create(item)));

      const item = await accountRepository.findById(data[0].id);
      expect(item.id).toBe(data[0].id);
    });
  });

  describe('#count()', () => {
    it('should return the current item count', async () => {
      const count = await accountRepository.count({});
      expect(count).toBe(0);
    });

    it('should return the current item count', async () => {
      await Promise.all(data.map((item) => accountRepository.create(item)));

      const count = await accountRepository.count({});
      expect(count).toBe(data.length);
    });
  });

  describe('#findAndCountAll()', () => {
    it('should honor a simple filter', async () => {
      await Promise.all(data.map((item) => accountRepository.create(item)));

      const firstItemPhoneNumber = data[0].orgPhone;

      const filter = {
        orgPhone: firstItemPhoneNumber,
      };
      const results = await accountRepository.findAndCountAll({ filter });
      expect(results.count).toBe(1);
      expect(results.rows[0].orgPhone).toBe(firstItemPhoneNumber);
    });
  });

});
