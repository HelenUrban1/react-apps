const SequelizeRepository = require('./sequelizeRepository');
const { data, createRepoInstance } = require('../../__fixtures__').operator;
const config = require('../../../config')();

describe('OperatorRepository', () => {
  let repository;
  let auditLogSpy;
  const options = {
    currentUser: {
      accountId: config.testAccountId,
    },
  };

  beforeEach(async () => {
    await SequelizeRepository.cleanDatabase(options.currentUser.accountId);
    repository = createRepoInstance();
    console.log(repository);
    auditLogSpy = jest.spyOn(repository, '_createAuditLog');
  });

  describe('#create()', () => {
    it('should create a operator', async () => {
      const fixture = data[0];
      const operator = await repository.create(fixture, options);

      expect(operator.name).toBe(fixture.name);
    });
  });

  describe('#update()', () => {
    it('should update a operator', async () => {
      const originalOperator = await repository.create(data[0], options);

      const updatedName = 'Joel';
      const updatedOperator = await repository.update(originalOperator.id, { firstName: updatedName }, options);

      expect(originalOperator.id).toBe(updatedOperator.id);
      expect(updatedOperator.firstName).toBe(updatedName);
    });
  });

  describe('#destroy()', () => {
    it('should remove a operator', async () => {
      const originalOperator = await repository.create(data[0], options);

      await repository.destroy(originalOperator.id, options);

      const foundOperator = await repository.findById(originalOperator.id, options);
      expect(foundOperator).toBe(null);
    });
  });

  describe('#findById()', () => {
    it('should find an existing operator', async () => {
      const originalOperator = await repository.create(data[0], options);

      const foundOperator = await repository.findById(originalOperator.id, options);
      expect(originalOperator).toStrictEqual(foundOperator);
    });
  });

  describe('#count()', () => {
    it('should count the total number of operators', async () => {
      await repository.create(data[0], options);

      const count = await repository.count({}, options);
      expect(count).toBe(1);
    });
  });
  describe('#findAndCountAll()', () => {
    it('should find and count all the operators', async () => {
      const originalOperator = await repository.create(data[0], options);

      const result = await repository.findAndCountAll({}, options);
      expect(result.count).toBe(1);
      expect(result.rows[0]).toStrictEqual(originalOperator);
    });
  });

  describe('#findAllAutocomplete()', () => {
    it('should find all operators for a list', async () => {
      const originalOperator = await repository.create(data[0], options);

      const result = await repository.findAllAutocomplete(null, null, options);
      expect(result[0].id).toBe(originalOperator.id);
    });
  });

  afterAll(async () => {
    await SequelizeRepository.closeConnections(options.currentUser.accountId);
    await SequelizeRepository.closeConnections();
  });
});
