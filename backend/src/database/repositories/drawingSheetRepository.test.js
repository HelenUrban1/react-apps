const SequelizeRepository = require('./sequelizeRepository');
const DrawingSheetRepository = require('./drawingSheetRepository');
const { data, createRepoInstance } = require('../../__fixtures__').drawingSheet;
const config = require('../../../config')();

describe('DrawingSheetRepository', () => {
  let repository;
  let auditLogSpy;
  const options = {
    currentUser: {
      accountId: config.testAccountId,
    },
  };

  beforeEach(async () => {
    await SequelizeRepository.cleanDatabase(options.currentUser.accountId);
    repository = createRepoInstance();
    auditLogSpy = jest.spyOn(repository, '_createAuditLog');
  });

  describe('#create()', () => {
    it('should create an item', async () => {
      const fixture = data[0];
      const item = await repository.create(fixture, options);

      expect(item.id).toBe(fixture.id);
    });

    it('should create an item and update a previousRevision', async () => {
      const fixture = data[0];
      const fixtureTwo = data[1];
      const item = await repository.create(fixture, options);

      const itemTwo = await repository.create({ ...fixtureTwo, previousRevision: item.id }, options);
      const updatedItem = await repository.findById(item.id, options);

      expect(item.id).toBe(fixture.id);
      expect(itemTwo.id).toBe(fixtureTwo.id);
      expect(itemTwo.previousRevision).toBe(fixture.id);
      expect(updatedItem.nextRevision).toBe(itemTwo.id);
    });
  });

  describe('#update()', () => {
    it('should update an item', async () => {
      const originalItem = await repository.create(data[0], options);

      const updatedRotation = '180';
      const updatedItem = await repository.update(originalItem.id, { rotation: updatedRotation, markerSize: null }, options);

      expect(originalItem.id).toBe(updatedItem.id);
      expect(updatedItem.rotation).toBe(updatedRotation);
      expect(updatedItem.markerSize).toBe(null);
    });
  });

  describe('#incrementInAccount()', () => {
    it('should update counts on an item', async () => {
      const originalItem = await repository.create(
        {
          ...data[0], //
          foundCaptureCount: 0,
          acceptedCaptureCount: 1,
          rejectedCaptureCount: 2,
          acceptedTimeoutCaptureCount: 10,
          reviewedCaptureCount: 100,
          reviewedTimeoutCaptureCount: 1000,
        },
        options
      );

      const updatedItem = await repository.incrementInAccount(
        {
          part: data[0].part,
          drawing: data[0].drawing,
          pageIndex: originalItem.pageIndex,
          accountId: config.testAccountId,
          siteId: '31ef1d56-1b86-4ffa-b886-507ed8f021c5',
          foundCaptureCount: 1000,
          acceptedCaptureCount: 100,
          rejectedCaptureCount: 10,
          acceptedTimeoutCaptureCount: 2,
          reviewedCaptureCount: 1,
          reviewedTimeoutCaptureCount: 0,
        },
        options
      );
      expect(updatedItem.foundCaptureCount).toBe(1000);
      expect(updatedItem.acceptedCaptureCount).toBe(101);
      expect(updatedItem.rejectedCaptureCount).toBe(12);
      expect(updatedItem.acceptedTimeoutCaptureCount).toBe(12);
      expect(updatedItem.reviewedCaptureCount).toBe(101);
      expect(updatedItem.reviewedTimeoutCaptureCount).toBe(1000);
    });
  });

  describe('#destroy()', () => {
    it('should remove an item', async () => {
      const originalItem = await repository.create(data[0], options);

      await repository.destroy(originalItem.id, options);

      const foundItem = await repository.findById(originalItem.id, options);
      expect(foundItem).toBe(null);
    });
  });

  describe('#findById()', () => {
    it('should find an existing item', async () => {
      const originalItem = await repository.create(data[0], options);

      const foundItem = await repository.findById(originalItem.id, options);
      expect(originalItem).toStrictEqual(foundItem);
    });
  });

  describe('#count()', () => {
    it('should count the total number of items', async () => {
      await repository.create(data[0], options);

      const count = await repository.count(null, options);
      expect(count).toBe(1);
    });
  });

  describe('#findAndCountAll()', () => {
    it('should find and count all the items', async () => {
      const originalItem = await repository.create(data[0], options);

      const result = await repository.findAndCountAll({}, options);
      expect(result.count).toBe(1);
      expect(result.rows[0]).toStrictEqual(originalItem);
    });
  });

  describe('#findAllAutocomplete()', () => {
    it('should find all items for a list', async () => {
      const originalItem = await repository.create(data[0], options);

      const result = await repository.findAllAutocomplete(null, null, options);
      expect(result[0].id).toBe(originalItem.id);
    });
  });

  describe('#convertToInput', () => {
    it('should convert a drawing sheet into a graphQL input', async () => {
      const originalItem = await repository.create(data[0], options);
      const input = DrawingSheetRepository.convertToInput({ ...originalItem });
      expect(input.id).toBe(undefined);
      expect(input.originalSheet).toBe(originalItem.id);
    });
  });

  afterAll(async () => {
    await SequelizeRepository.closeConnections(options.currentUser.accountId);
    await SequelizeRepository.closeConnections();
  });
});
