const pick = require('lodash/pick');
const Sequelize = require('sequelize');
const models = require('../models');
const { sequelize } = require('../models');
const SequelizeRepository = require('./sequelizeRepository');
const AuditLogRepository = require('./auditLogRepository');
const SequelizeFilterUtils = require('../utils/sequelizeFilterUtils');
const config = require('../../../config')();

const { Op } = Sequelize;

/**
 * Handles database operations for the ReviewTasks.
 * See https://sequelize.org/v5/index.html to learn how to customize it.
 */
class ReviewTaskRepository {
  /**
   * Creates the ReviewTask.
   *
   * @param {Object} data
   * @param {Object} [options]
   */
  static async create(data, options) {
    const tenant = await models.getTenant(config.reviewAccountId);

    const transaction = SequelizeRepository.getTransaction(options);

    const record = await tenant.reviewTask.create(
      {
        ...pick(data, [
          'id', //
          'status',
          'type',
          'metadata',
          'accountId',
          'siteId',
          'partId',
          'drawingId',
          'reviewerId',
          'processingStart',
          'processingEnd',
          'updatedAt',
          'createdAt',
        ]),
      },
      {
        transaction,
      }
    );

    await ReviewTaskRepository.createAuditLog(AuditLogRepository.CREATE, record, data, options);

    return ReviewTaskRepository.findById(record.id, options);
  }

  /**
   * Updates the ReviewTask.
   *
   * @param {Object} data
   * @param {Object} [options]
   */
  static async update(id, data, options) {
    const tenant = await models.getTenant(config.reviewAccountId);

    const transaction = SequelizeRepository.getTransaction(options);

    let record = await tenant.reviewTask.findByPk(id, {
      transaction,
    });

    record = await record.update(
      {
        ...pick(data, [
          'id', //
          'status',
          'type',
          'metadata',
          'accountId',
          'siteId',
          'partId',
          'drawingId',
          'reviewerId',
          'processingStart',
          'processingEnd',
          'updatedAt',
          'createdAt',
        ]),
      },
      {
        transaction,
      }
    );

    await ReviewTaskRepository.createAuditLog(AuditLogRepository.UPDATE, record, data, options);

    return ReviewTaskRepository.findById(record.id, options);
  }

  /**
   * Puts an uncompleted task from "Processing" to "Waiting"
   * @param {*} id
   * @param {*} options
   */
  static async returnTaskToQueue(id, options) {
    const tenant = await models.getTenant(config.reviewAccountId);

    const transaction = SequelizeRepository.getTransaction(options);

    const record = await tenant.reviewTask.findByPk(id, {
      transaction,
    });

    const data = {
      processingStart: null,
      status: 'Waiting',
    };

    await record.update(
      {
        ...data,
      },
      {
        transaction,
      }
    );

    await ReviewTaskRepository.createAuditLog(AuditLogRepository.UPDATE, record, data, options);
  }

  /**
   * Deletes the ReviewTask.
   *
   * @param {string} id
   * @param {Object} [options]
   */
  static async destroy(id, options) {
    const tenant = await models.getTenant(config.reviewAccountId);

    const transaction = SequelizeRepository.getTransaction(options);

    const record = await tenant.reviewTask.findByPk(id, {
      transaction,
    });

    await record.destroy({
      transaction,
    });

    await ReviewTaskRepository.createAuditLog(AuditLogRepository.DELETE, record, null, options);
  }

  /**
   * Finds the ReviewTask.
   *
   * @param {string} id
   * @param {Object} [options]
   */
  static async findById(id, options) {
    const tenant = await models.getTenant(config.reviewAccountId);

    const transaction = SequelizeRepository.getTransaction(options);

    const include = [];

    const record = await tenant.reviewTask.findByPk(id, {
      include,
      transaction,
    });

    return ReviewTaskRepository.fillWithRelations(record, options);
  }

  /**
   * Gets the oldest task with a status of waiting
   * @param {*} options
   */
  static async findNextQueuedTask(options) {
    const tenant = await models.getTenant(config.reviewAccountId);

    const transaction = SequelizeRepository.getTransaction(options);

    const record = await tenant.reviewTask.findOne({
      where: { status: 'Waiting' },
      order: [['createdAt', 'ASC']],
      transaction,
    });

    if (!record) {
      return null;
    }

    const data = {
      status: 'Processing',
      processingStart: new Date(),
    };

    await record.update(
      {
        ...data,
      },
      {
        transaction,
      }
    );

    await ReviewTaskRepository.createAuditLog(AuditLogRepository.UPDATE, record, data, options);

    return ReviewTaskRepository.fillWithRelations(record, options);
  }

  /**
   * Removes items in Waiting or Processing status from the Review Queue by soft deleting them.
   *
   * Only intended to be run on non-production environments where the queue is not actively being managed.
   *
   * Can be triggered by running the review-task-queue-clearer lambda.
   *
   * Can undo by setting deletedBy field back to null on any records deleted in a time span.
   *
   * @returns Number of items removed from queue
   */
  static async clearQueue() {
    if (!config.isBeforeProduction) {
      throw new Error('Cannot clear queue on production.');
    }
    const tenant = await models.getTenant(config.reviewAccountId);
    const records = await tenant.reviewTask.destroy({
      where: {
        status: {
          [Op.or]: ['Waiting', 'Processing'],
        },
        deletedAt: null,
      },
    });
    return records;
  }

  /**
   * Finds the ReviewTasks based on the query.
   * See https://sequelize.org/v5/manual/querying.html to learn how to
   * customize the query.
   *
   * @param {Object} query
   * @param {Object} query.filter
   * @param {number} query.limit
   * @param  {number} query.offset
   * @param  {string} query.orderBy
   * @param {Object} [options]
   *
   * @returns {Promise<Object>} response - Object containing the rows and the count.
   */
  static async findAndCountAll(
    { filter, limit, offset, orderBy } = {
      filter: null,
      limit: 0,
      offset: 0,
      orderBy: null,
    },
    options
  ) {
    const tenant = await models.getTenant(config.reviewAccountId);

    let where = {};
    const include = [];

    if (filter) {
      if (filter.id) {
        where = {
          ...where,
          id: SequelizeFilterUtils.uuid(filter.id),
        };
      }

      if (filter.accountId) {
        where = {
          ...where,
          accountId: SequelizeFilterUtils.uuid(filter.accountId),
        };
      }

      if (filter.siteId) {
        where = {
          ...where,
          siteId: SequelizeFilterUtils.uuid(filter.siteId),
        };
      }

      if (filter.partId) {
        where = {
          ...where,
          partId: SequelizeFilterUtils.uuid(filter.partId),
        };
      }

      if (filter.drawingId) {
        where = {
          ...where,
          drawingId: SequelizeFilterUtils.uuid(filter.drawingId),
        };
      }

      if (filter.reviewerId) {
        where = {
          ...where,
          reviewerId: SequelizeFilterUtils.uuid(filter.reviewerId),
        };
      }

      if (filter.createdAtRange) {
        const [start, end] = filter.createdAtRange;

        if (start !== undefined && start !== null && start !== '') {
          where = {
            ...where,
            createdAt: {
              ...where.createdAt,
              [Op.gte]: start,
            },
          };
        }

        if (end !== undefined && end !== null && end !== '') {
          where = {
            ...where,
            createdAt: {
              ...where.createdAt,
              [Op.lte]: end,
            },
          };
        }
      }
    }

    const { rows, count } = await tenant.reviewTask.findAndCountAll({
      where,
      include,
      limit: limit ? Number(limit) : undefined,
      offset: offset ? Number(offset) : undefined,
      order: orderBy ? [orderBy.split('_')] : [['createdAt', 'DESC']],
      transaction: SequelizeRepository.getTransaction(options),
    });

    const outputRows = await ReviewTaskRepository.fillWithRelationsForRows(rows, options);

    return { rows: outputRows, count };
  }

  /**
   * Counts the number of reviewTasks based on the filter.
   *
   * @param {Object} filter
   * @param {Object} [options]
   */
  static async count(filter, options) {
    const tenant = await models.getTenant(config.reviewAccountId);

    const transaction = SequelizeRepository.getTransaction(options);

    return tenant.reviewTask.count(
      {
        where: filter,
      },
      {
        transaction,
      }
    );
  }

  /**
   * Returns the average elapsed time between two columns (end - start)
   * @param {*} filter
   * @param {*} start
   * @param {*} end
   * @param {*} options
   */
  static async elapsedTime(filter, start, end, options) {
    const tenant = await models.getTenant(config.reviewAccountId);

    const transaction = SequelizeRepository.getTransaction(options);
    return tenant.reviewTask.findAll({
      where: filter,
      attributes: [[sequelize.fn('AVG', sequelize.fn('AGE', Sequelize.col(end), sequelize.col(start))), 'elapsed']],
      transaction,
    });
  }

  /**
   * Creates an audit log of the operation.
   *
   * @param {string} action - The action [create, update or delete].
   * @param {object} record - The sequelize record
   * @param {object} data - The new data passed on the request
   * @param {object} options
   */
  static async createAuditLog(action, record, data, options) {
    let values = {};

    if (data) {
      values = {
        ...record.get({ plain: true }),
      };
    }

    await AuditLogRepository.log(
      {
        entityName: 'reviewTask',
        entityId: record.id,
        action,
        values,
      },
      options
    );
  }

  /**
   * Fills an array of ReviewTask.
   *
   * @param {Array} rows
   * @param {Object} [options]
   */
  static async fillWithRelationsForRows(rows, options) {
    if (!rows) {
      return rows;
    }

    return Promise.all(rows.map((record) => ReviewTaskRepository.fillWithRelations(record, options)));
  }

  /**
   * Fill the ReviewTask.
   *
   * @param {Object} record
   * @param {Object} [options]
   */
  static async fillWithRelations(record) {
    if (!record) {
      return record;
    }

    return record.get({ plain: true });
  }
}

module.exports = ReviewTaskRepository;
