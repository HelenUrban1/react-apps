const SequelizeRepository = require('./sequelizeRepository');
const { data, createRepoInstance } = require('../../__fixtures__').reportTemplate;
const config = require('../../../config')();

describe('ReportTemplateRepository', () => {
  let reportTemplateRepository;
  let auditLogSpy;
  const options = {
    currentUser: {
      accountId: config.testAccountId,
    },
  };

  beforeEach(async () => {
    await SequelizeRepository.cleanDatabase(options.currentUser.accountId);
    reportTemplateRepository = createRepoInstance();
    auditLogSpy = jest.spyOn(reportTemplateRepository, '_createAuditLog');
  });

  describe('#create()', () => {
    it('should create an item', async () => {
      const fixture = data[0];
      const item = await reportTemplateRepository.create(fixture, options);

      expect(item.id).toBe(fixture.id);
    });
  });

  describe('#update()', () => {
    it('should update an item', async () => {
      const originalItem = await reportTemplateRepository.create(data[0], options);

      const updatedTitle = 'Moe';
      const updatedItem = await reportTemplateRepository.update(originalItem.id, { title: updatedTitle }, options);

      expect(originalItem.id).toBe(updatedItem.id);
      expect(updatedItem.title).toBe(updatedTitle);
    });
  });
  afterAll(async () => {
    await SequelizeRepository.closeConnections();
  });
});
