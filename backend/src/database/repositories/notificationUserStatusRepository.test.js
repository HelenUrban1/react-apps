const SequelizeRepository = require('./sequelizeRepository');
const { data, createRepoInstance } = require('../../__fixtures__').notificationUserStatus;
const { data: notificationData, createRepoInstance: notificationCreateRepoInstance } = require('../../__fixtures__').notification;
const config = require('../../../config')();

describe('NotificationUserStatusRepository', () => {
  let repository;
  let notificationRepository;
  let auditLogSpy;
  const options = {
    currentUser: {
      accountId: config.testAccountId,
    },
  };

  beforeEach(async () => {
    await SequelizeRepository.cleanDatabase(options.currentUser.accountId);
    repository = createRepoInstance();
    notificationRepository = notificationCreateRepoInstance();
    auditLogSpy = jest.spyOn(repository, '_createAuditLog');
  });

  describe('#create()', () => {
    it('should create an item', async () => {
      await notificationRepository.create(notificationData[0], options);

      const fixture = data[0];
      const item = await repository.create(fixture, options);

      expect(item.id).toBe(fixture.id);
      expect(auditLogSpy).toBeCalledTimes(1);
    });
  });

  describe('#update()', () => {
    it('should update an item', async () => {
      await notificationRepository.create(notificationData[0], options);
      const originalItem = await repository.create(data[0], options);

      const updatedReadAt = new Date();
      const updatedItem = await repository.update(originalItem.id, { readAt: updatedReadAt }, options);

      expect(originalItem.id).toBe(updatedItem.id);
      expect(updatedItem.readAt).toStrictEqual(updatedReadAt);
      expect(auditLogSpy).toBeCalledTimes(2);
    });
  });

  describe('#destroy()', () => {
    it('should remove an item', async () => {
      await notificationRepository.create(notificationData[0], options);
      const originalItem = await repository.create(data[0], options);

      await repository.destroy(originalItem.id, options);

      const foundItem = await repository.findById(originalItem.id, options);
      expect(foundItem).toBe(null);
      expect(auditLogSpy).toBeCalledTimes(2);
    });
  });

  describe('#findById()', () => {
    it('should find an existing item', async () => {
      await notificationRepository.create(notificationData[0], options);
      const originalItem = await repository.create(data[0], options);

      const foundItem = await repository.findById(originalItem.id, options);
      expect(originalItem).toStrictEqual(foundItem);
    });
  });

  describe('#count()', () => {
    it('should count the total number of items', async () => {
      await notificationRepository.create(notificationData[0], options);
      await repository.create(data[0], options);

      const count = await repository.count(null, options);
      expect(count).toBe(1);
    });
  });

  describe('#findAndCountAll()', () => {
    it('should find and count all the items', async () => {
      await notificationRepository.create(notificationData[0], options);
      const originalItem = await repository.create(data[0], options);

      const result = await repository.findAndCountAll({}, options);
      expect(result.count).toBe(1);
      expect(result.rows[0]).toStrictEqual(originalItem);
    });
  });

  afterAll(async () => {
    await SequelizeRepository.closeConnections(options.currentUser.accountId);
    await SequelizeRepository.closeConnections();
  });
});
