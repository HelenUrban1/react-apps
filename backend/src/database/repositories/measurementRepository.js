const pick = require('lodash/pick');
const Sequelize = require('sequelize');
const models = require('../models');
const SequelizeRepository = require('./sequelizeRepository');
const AuditLogRepository = require('./auditLogRepository');
const SequelizeFilterUtils = require('../utils/sequelizeFilterUtils');

const { Op } = Sequelize;

/**
 * Handles database operations for the Measurement.
 * See https://sequelize.org/v5/index.html to learn how to customize it.
 */
class MeasurementRepository {
  /**
   * Creates the Measurement.
   *
   * @param {Object} data
   * @param {Object} [options]
   */
  async create(data, options) {
    const currentUser = SequelizeRepository.getCurrentUser(options);

    const transaction = SequelizeRepository.getTransaction(options);

    const tenant = await models.getTenant(currentUser.accountId);

    const record = await tenant.measurement.create(
      {
        ...pick(data, ['value', 'status', 'gage', 'machine', 'sampleId', 'stationId', 'operationId', 'methodId', 'characteristicId', 'updatedAt', 'createdAt']),
        createdById: data.operatorId,
        updatedById: data.operatorId,
      },
      {
        transaction,
      }
    );

    if (data.sampleId) {
      const sampleRecord = await tenant.sample.findByPk(data.sampleId, { transaction });
      await record.setSample(sampleRecord, { transaction });
    }

    if (data.characteristicId) {
      const characteristicRecord = await tenant.characteristic.findByPk(data.characteristicId, { transaction });
      await record.setCharacteristic(characteristicRecord, { transaction });
    }

    if (data.stationId) {
      const stationRecord = await tenant.station.findByPk(data.stationId, { transaction });
      await record.setStation(stationRecord, { transaction });
    }

    if (data.methodId) {
      const methodRecord = await tenant.listItem.findByPk(data.methodId, { transaction });
      await record.setMethod(methodRecord, { transaction });
    }

    if (data.operationId) {
      const operationRecord = await tenant.listItem.findByPk(data.operationId, { transaction });
      await record.setOperation(operationRecord, { transaction });
    }

    await this._createAuditLog(AuditLogRepository.CREATE, record, data, options);

    return this.findById(record.id, options);
  }

  /**
   * Updates the Measurement.
   *
   * @param {Object} data
   * @param {Object} [options]
   */
  async update(id, data, options) {
    const currentUser = SequelizeRepository.getCurrentUser(options);

    const transaction = SequelizeRepository.getTransaction(options);

    const tenant = await models.getTenant(currentUser.accountId);

    let record = await tenant.measurement.findByPk(id, {
      transaction,
    });

    record = await record.update(
      {
        ...pick(data, ['value', 'status', 'gage', 'machine', 'sampleId', 'stationId', 'operationId', 'methodId', 'characteristicId', 'updatedAt', 'createdAt']),
        updatedById: data.operatorId,
      },
      {
        transaction,
      }
    );

    if (data.sample) {
      const sampleRecord = await tenant.sample.findByPk(data.sample, { transaction });
      await record.setSample(sampleRecord, { transaction });
    }

    if (data.characteristic) {
      const characteristicRecord = await tenant.characteristic.findByPk(data.characteristic, { transaction });
      await record.setCharacteristic(characteristicRecord, { transaction });
    }

    if (data.station) {
      const stationRecord = await tenant.station.findByPk(data.station, { transaction });
      await record.setStation(stationRecord, { transaction });
    }

    if (data.method) {
      const methodRecord = await tenant.listItem.findByPk(data.method, { transaction });
      await record.setMethod(methodRecord, { transaction });
    }

    if (data.operation) {
      const operationRecord = await tenant.listItem.findByPk(data.operation, { transaction });
      await record.setOperation(operationRecord, { transaction });
    }

    await this._createAuditLog(AuditLogRepository.UPDATE, record, data, options);

    return this.findById(record.id, options);
  }

  /**
   * Deletes the Measurement.
   *
   * @param {string} id
   * @param {Object} [options]
   */
  async destroy(id, options) {
    const currentUser = SequelizeRepository.getCurrentUser(options);

    const transaction = SequelizeRepository.getTransaction(options);

    const tenant = await models.getTenant(currentUser.accountId);

    const record = await tenant.measurement.findByPk(id, {
      transaction,
    });

    await record.destroy({
      transaction,
    });

    await this._createAuditLog(AuditLogRepository.DELETE, record, null, options);
  }

  /**
   * Finds the Measurement and its relations.
   *
   * @param {string} id
   * @param {Object} [options]
   */
  async findById(id, options) {
    const currentUser = SequelizeRepository.getCurrentUser(options);
    const transaction = SequelizeRepository.getTransaction(options);

    const tenant = await models.getTenant(currentUser.accountId);

    const include = [
      {
        model: tenant.sample,
        as: 'sample',
      },
      {
        model: tenant.characteristic,
        as: 'characteristic',
      },
      {
        model: tenant.station,
        as: 'station',
      },
      {
        model: tenant.listItem,
        as: 'operation',
      },
      {
        model: tenant.listItem,
        as: 'method',
      },
    ];

    const record = await tenant.measurement.findByPk(id, {
      include,
      transaction,
    });

    return await this._fillWithRelations(record, options);
  }

  /**
   * Counts the number of Measurements based on the filter.
   *
   * @param {Object} filter
   * @param {Object} [options]
   */
  async count(filter, options) {
    const currentUser = SequelizeRepository.getCurrentUser(options);

    const transaction = SequelizeRepository.getTransaction(options);

    const tenant = await models.getTenant(currentUser.accountId);

    return tenant.measurement.count(
      {
        where: filter,
      },
      {
        transaction,
      }
    );
  }

  /**
   * Finds the Measurements based on the query.
   * See https://sequelize.org/v5/manual/querying.html to learn how to
   * customize the query.
   *
   * @param {Object} query
   * @param {Object} query.filter
   * @param {number} query.limit
   * @param {number} query.offset
   * @param {string} query.orderBy
   * @param {Object} [options]
   *
   * @returns {Promise<Object>} response - Object containing the rows and the count.
   */
  async findAndCountAll(
    { filter, limit, offset, orderBy } = {
      filter: null,
      limit: 0,
      offset: 0,
      orderBy: null,
    },
    options
  ) {
    const currentUser = SequelizeRepository.getCurrentUser(options);

    const tenant = await models.getTenant(currentUser.accountId);

    let where = {};
    const include = [
      {
        model: tenant.sample,
        as: 'sample',
      },
      {
        model: tenant.characteristic,
        as: 'characteristic',
      },
      {
        model: tenant.station,
        as: 'station',
      },
      {
        model: tenant.listItem,
        as: 'operation',
      },
      {
        model: tenant.listItem,
        as: 'method',
      },
      {
        model: tenant.operator,
        as: 'createdBy',
      },
      {
        model: tenant.operator,
        as: 'updatedBy',
      },
    ];
    if (filter) {
      if (filter.id) {
        where = {
          ...where,
          id: SequelizeFilterUtils.uuid(filter.id),
        };
      }

      if (filter.measurement) {
        where = {
          ...where,
          [Op.and]: SequelizeFilterUtils.ilike('measurement', 'measurement', filter.measurement),
        };
      }

      if (filter.value) {
        where = {
          ...where,
          value: filter.value,
        };
      }

      if (filter.status) {
        where = {
          ...where,
          status: filter.status,
        };
      }

      if (filter.gage) {
        where = {
          ...where,
          gage: filter.gage,
        };
      }

      if (filter.machine) {
        where = {
          ...where,
          machine: filter.machine,
        };
      }

      if (filter.sampleId) {
        where = {
          ...where,
          sampleId: SequelizeFilterUtils.uuid(filter.sampleId),
        };
      }

      if (filter.characteristicId) {
        where = {
          ...where,
          characteristicId: SequelizeFilterUtils.uuid(filter.characteristicId),
        };
      }

      if (filter.operatorId) {
        where = {
          ...where,
          createdById: SequelizeFilterUtils.uuid(filter.operatorId),
          updatedById: SequelizeFilterUtils.uuid(filter.operatorId),
        };
      }

      if (filter.stationId) {
        where = {
          ...where,
          stationId: SequelizeFilterUtils.uuid(filter.stationId),
        };
      }

      if (filter.operationId) {
        where = {
          ...where,
          operationId: SequelizeFilterUtils.uuid(filter.operationId),
        };
      }

      if (filter.methodId) {
        where = {
          ...where,
          methodId: SequelizeFilterUtils.uuid(filter.methodId),
        };
      }

      if (filter.createdAtRange) {
        const [start, end] = filter.createdAtRange;

        if (start !== undefined && start !== null && start !== '') {
          where = {
            ...where,
            createdAt: {
              ...where.createdAt,
              [Op.gte]: start,
            },
          };
        }

        if (end !== undefined && end !== null && end !== '') {
          where = {
            ...where,
            createdAt: {
              ...where.createdAt,
              [Op.lte]: end,
            },
          };
        }
      }

      if (filter.updatedAtRange) {
        const [start, end] = filter.updatedAtRange;

        if (start !== undefined && start !== null && start !== '') {
          where = {
            ...where,
            createdAt: {
              ...where.updatedAt,
              [Op.gte]: start,
            },
          };
        }

        if (end !== undefined && end !== null && end !== '') {
          where = {
            ...where,
            createdAt: {
              ...where.updatedAt,
              [Op.lte]: end,
            },
          };
        }
      }
    }

    let { rows, count } = await tenant.measurement.findAndCountAll({
      where,
      include,
      limit: limit ? Number(limit) : undefined,
      offset: offset ? Number(offset) : undefined,
      order: orderBy ? [orderBy.split('_')] : [['createdAt', 'DESC']],
      transaction: SequelizeRepository.getTransaction(options),
    });

    rows = await this._fillWithRelationsForRows(rows, options);

    return { rows, count };
  }

  /**
   * Lists the Measurements to populate the autocomplete.
   * See https://sequelize.org/v5/manual/querying.html to learn how to
   * customize the query.
   *
   * @param {string} query
   * @param {number} limit
   */
  async findAllAutocomplete(query, limit, options) {
    const currentUser = SequelizeRepository.getCurrentUser(options);

    const tenant = await models.getTenant(currentUser.accountId);

    let where = {};

    if (query) {
      where = {
        [Op.or]: [
          { id: SequelizeFilterUtils.uuid(query) },
          {
            [Op.and]: SequelizeFilterUtils.ilike('measurement', 'value', query),
          },
        ],
      };
    }

    const records = await tenant.measurement.findAll({
      attributes: ['id', 'value', 'status', 'machine', 'updatedAt', 'createdAt'],
      where,
      limit: limit ? Number(limit) : undefined,
      orderBy: [['value', 'ASC']],
    });

    return records.map((record) => ({
      id: record.id,
      label: record.value,
    }));
  }

  /**
   * Creates an audit log of the operation.
   *
   * @param {string} action - The action [create, update or delete].
   * @param {object} record - The sequelize record
   * @param {object} data - The new data passed on the request
   * @param {object} options
   */
  async _createAuditLog(action, record, data, options) {
    let values = {};

    if (data) {
      values = {
        ...record.get({ plain: true }),
      };
    }

    await AuditLogRepository.log(
      {
        entityName: 'measurement',
        entityId: record.id,
        action,
        values,
      },
      options
    );
  }

  /**
   * Fills an array of Measurement with relations.
   *
   * @param {Array} rows
   * @param {Object} [options]
   */
  async _fillWithRelationsForRows(rows, options) {
    if (!rows) {
      return rows;
    }

    return Promise.all(rows.map((record) => this._fillWithRelations(record, options)));
  }

  /**
   * Fill the Measurement with the relations.
   *
   * @param {Object} record
   * @param {Object} [options]
   */
  async _fillWithRelations(record, options) {
    if (!record) {
      return record;
    }

    const output = record.get({ plain: true });

    const transaction = SequelizeRepository.getTransaction(options);

    output.sample = await record.getSample({
      transaction,
    });

    output.characteristic = await record.getCharacteristic({
      transaction,
    });

    output.station = await record.getStation({
      transaction,
    });

    output.operation = await record.getOperation({
      transaction,
    });

    output.method = await record.getMethod({
      transaction,
    });

    output.createdBy = await record.getCreatedBy({
      transaction,
    });

    output.updatedBy = await record.getUpdatedBy({
      transaction,
    });

    return output;
  }
}

module.exports = MeasurementRepository;
