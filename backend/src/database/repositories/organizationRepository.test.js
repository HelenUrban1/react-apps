const SequelizeRepository = require('./sequelizeRepository');
const { data, createRepoInstance } = require('../../__fixtures__').organization;
const config = require('../../../config')();

describe('OrganizationRepository', () => {
  let organizationRepository;
  let auditLogSpy;
  const options = {
    currentUser: {
      accountId: config.testAccountId,
    },
  };

  beforeEach(async () => {
    await SequelizeRepository.cleanDatabase(options.currentUser.accountId);
    organizationRepository = createRepoInstance();
    auditLogSpy = jest.spyOn(organizationRepository, '_createAuditLog');
  });

  describe('#create()', () => {
    it('should create an item', async () => {
      const fixture = data[0];
      const item = await organizationRepository.create(fixture, options);

      expect(item.id).toBe(fixture.id);
    });
  });

  describe('#update()', () => {
    it('should update an item', async () => {
      const originalItem = await organizationRepository.create(data[0], options);

      const updatedName = 'Moe';
      const updatedItem = await organizationRepository.update(originalItem.id, { name: updatedName }, options);

      expect(originalItem.id).toBe(updatedItem.id);
      expect(updatedItem.name).toBe(updatedName);
    });
  });
  afterAll(async () => {
    await SequelizeRepository.closeConnections(options.currentUser.accountId);
    await SequelizeRepository.closeConnections();
  });
  // TODO: Add tests for the rest of the public OrganizationRepository methods
});
