module.exports = Object.freeze({
  accountMember: {
    ROLES: ['Admin', 'Billing', 'Collaborator', 'Owner', 'Viewer', 'Reviewer', 'Metro'],
    PAID_ROLES: ['Admin', 'Owner'],
    UNPAID_ROLES: ['Billing', 'Collaborator', 'Viewer', 'Reviewer', 'Metro'],
    STATUSES: ['Active', 'Archived', 'Pending', 'Requesting', 'Suspended'],
    DEFAULT_SETTINGS: '{}', // This value allows for JSON.parse() on it and eventual merging with account and system settings
  },
});
