/* eslint-disable no-console */
const axios = require('axios');
const config = require('../../config')();
const { log } = require('../logger');

if (config.env === 'development' && config.enpUrl.includes('ixc-local')) {
  const path = require('path');
  const https = require('https');
  const rootCas = require('ssl-root-cas').create();

  rootCas.addFile(path.resolve(__dirname, '../../../rootCA.pem'));
  https.globalAgent.options.ca = rootCas;
}

const interpretNotationText = async (text, debug = false) => {
  // Prepare a standard result object
  // const result: { result?: any; error?: any } = {};
  // const result;

  // Validate input
  // TODO: Throw real InvalidParameter error object
  if (!text && text !== '') throw new Error(`Notation Interpreter requires text as input`);

  // Prepare the post body
  const data = { text };

  // Determine if debugging should be enabled
  if (debug || config.env === 'development') data.debug = true;

  const response = await axios({
    url: config.enpUrl,
    dataType: 'json',
    method: 'POST',
    headers: { 'Content-Type': 'application/json; charset=utf-8' },
    data,
  });
  // If debugging is enabled, write the output to the console
  if (data.debug) {
    log.debug('enp.response', response);
    log.debug('enp.response.data.trace', response.data.trace);
    log.debug('enp.response.data.result', response.data.result);
    log.debug('enp.response.data.error', response.data.error);
  }
  return response.data.result;
};

export { interpretNotationText as default };
