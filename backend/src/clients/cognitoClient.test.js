const makeCognitoClient = require('./cognitoClient').default;

describe('cognitoClient', () => {
  let cognitoClient;
  const loggerMock = {
    trace: jest.fn(),
    debug: jest.fn(),
    log: jest.fn(),
    info: jest.fn(),
    warn: jest.fn(),
    error: jest.fn(),
  };

  beforeEach(() => {
    jest.resetAllMocks();
  });

  describe('makeCognitoClient', () => {
    it('should populate the result properly if given a valid inpt', async () => {
      cognitoClient = await makeCognitoClient({
        logger: loggerMock,
      });

      expect(cognitoClient.middlewareStack.add).toBeDefined();
      expect(cognitoClient.config.requestHandler).toBeDefined();
    });
  });
});
