// TODO: Move all validators into central module

module.exports = {
  required(givenParams, requiredParams) {
    const missing = requiredParams.filter((param) => !Object.prototype.hasOwnProperty.call(givenParams, param));
    if (missing.length > 0) {
      throw new Error(`Required param(s) are not defined ${missing}.`);
    }
  },
};
