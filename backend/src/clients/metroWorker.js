module.exports = class MetroWorker {
  constructor(params) {
    this.AccountService = params.AccountService;
    this.AccountMemberService = params.AccountMemberService;
    this.UserService = params.UserService;
    this.OperatorService = params.OperatorService;
  }

  async enableMetro(accountId) {
    const account = await this.AccountService.findById(accountId);
    const settings = JSON.parse(account.settings);
    settings.metro = true;
    account.settings = JSON.stringify(settings);
    return this.AccountService.update(accountId, account);
  }

  async createOperators(accountId) {
    const account = await this.AccountService.findById(accountId);
    const operators = await Promise.all(
      account.members.map(async (member) => {
        const user = await this.UserService.findById(member.userId, { eagerReturn: true });
        if (!user) return; //not sure how we get to this state but there seems to be an account member that doesn't map to a user in the default seed

        const existingOperator = await this.OperatorService.findAndCountAll({
          filter: {
            email: user.email,
          },
        });

        if (existingOperator.count > 0) return Promise.resolve();

        return this.OperatorService.create({
          accessControl: user.accessControl ? user.accessControl : 'None',
          email: user.email,
          fullName: user.fullName,
          firstName: user.firstName,
          lastName: user.lastName ? user.lastName : '',
        });
      })
    );

    return operators;
  }

  async createServiceAccountMember(accountId) {
    const account = await this.AccountService.findById(accountId);
    if (!account) throw new Error('Cannot find account', accountId);
    const serviceUser = await this.UserService.create(
      {
        email: `service+${accountId}@inspectionxpert.com`,
        password: '',
        activeAccountMemberId: null,
        roles: ['Metro'],
      },
      { userOnly: true }
    );
    const serviceAccountMember = await this.AccountMemberService.create({
      roles: ['Metro'],
      settings: '{}',
      status: 'Active',
      accessControl: false,
      user: serviceUser.id,
      account: account.id,
    });
    return {
      user: serviceUser,
      accountMember: serviceAccountMember,
    };
  }
};
