const AWS = require('aws-sdk');
const moment = require('moment');
const { cloneDeep } = require('lodash');
const config = require('../../config')();
const validate = require('./validate');
const UserRepository = require('../database/repositories/userRepository');
const { log } = require('../logger');
const EmailSender = require('../services/shared/email/emailSender');
const EmailTemplate = require('../emails/template');

// Configre real AWS
AWS.config.update({ region: config.awsRegion });

/**
 *
 * Subscription Worker that synchronizes data in back office systems
 *
 ---------------------------------------------------------------------------------

                               Two Types of Trials
                                      
  This module supports two ways of running trials.

  Activity Bound Trials (Freemium)
  * The system is free to use as long as there are less than n number of something.
  * Add more than n number of that thing requires a paid subscription.
  * Lifecycle:
    1) Create new freemium subscription
    2) Add user to existing trial
    3) Update trial stage
    4) Update activity count

  Time Bound Trials
  * After n number of days, the trial expires or is converted to a paid subscription
  * Example : 7 Day Fully Functioning Free Trial
  * Lifecycle:
    1) Create new trial
    2) Add user to existing trial
    3) Update trial stage
    4) Extend trial
    5) Expire trial
    6) Reactivate trial
    7) Convert trial to subscription

---------------------------------------------------------------------------------

                             System Integration

The following are custom fields that need to be setup in the CRM and Billing systems:

CRM:Company
  Information Group
    IXC Account ID     ixc_account_id
    IXC Account Name   ixc_account_name
    IX Chargify ID     ix_chargify_id
    IX Chargify Link   ix_chargify_link
  Web Analytics Group
    Number of IX Parts      number_of_ix_user_accounts
    Number of IX Parts      number_of_ix_parts
    Number of IX Drawings   number_of_ix_drawings

CRM:Contact
  Information Group 
    IXC User ID        ixc_user_id
    IXC Account ID     ixc_account_id
    IX Amplitude Link  ix_amplitude_link

CRM:Deals
  Information Group
    IXC Account ID      ix_account_id
    IX Chargify ID      ix_chargify_id
    IX Chargify Link    ix_chargify_link     
    Trial Type          trial_type
    Trial End Date      trial_end_date
    Trial Started Date  trial_started_date

  Activity Group
    Trial Started Date   trial_tarted_date
    Trial End Date       trial_end_date

CRM:Pipeline
  Name: 
    Online Trial
  Stages:
    Trial Requested
    First Login
    Drawing Ballooned
    Report Created
    Closed Won
    Closed Lost

Billing System:Customer
  HubSpot Company ID
  IXC Account ID

Billing System:Subscription
  HubSpot Deal ID
  billing_interval

*/

// FROM: https://inspectionxpert-test.chargify.com/product_families/1526431/quantity_based_components
const IXC_PRODUCT_FAMILY_HANDLE = 'inspectionxpert-cloud';
const INTERNAL_COMPONENT_HANDLE = 'internal_account_ss';
const SEATS_SS_COMPONENT_HANDLE = 'paid_user_seat_ss';
const PARTS_SS_COMPONENT_HANDLE = 'parts_ss';
const IXC_STANDARD_HANDLE = 'ix_standard';
const NET_INSPECT_HANDLE = 'net_inspect_addon';

// TODO: Catalog structure is specific to the vendor, so this logic should live in the adapter
/**
 * Helper function that returns the product record with the handle specified
 *
 * @private
 * @param {Object} productHandle - The api handle of the product
 */
function lookupProductByHandle(catalog, productHandle) {
  return catalog[IXC_PRODUCT_FAMILY_HANDLE].products[productHandle];
}

// TODO: Catalog structure is specific to the vendor, so this logic should live in the adapter
/**
 * Helper function that returns the component record with the handle specified
 *
 * @private
 * @param {Object} componentHandle - The api handle of the component
 */
function lookupComponentByHandle(catalog, componentHandle) {
  return catalog[IXC_PRODUCT_FAMILY_HANDLE].components[componentHandle];
}

/**
 * Helper function that returns link to IXC user's activty timeline in Amplitude
 *
 * @param {String} ixcUserId - The user's IXC id
 */
function amplitudeUserActivtyLink(ixcUserId) {
  let amplitudeProjectId;
  if (config.isLocalHost) amplitudeProjectId = '309450';
  else if (config.isDevHost) amplitudeProjectId = '292839';
  else if (config.isQaHost) amplitudeProjectId = 'NOT-TRACKED';
  else if (config.isStageHost) amplitudeProjectId = '292838';
  else if (config.isProdHost) amplitudeProjectId = '250975';
  return `https://analytics.amplitude.com/ixc/project/${amplitudeProjectId}/search/user_id=${ixcUserId}`;
}

module.exports = class SubscriptionWorker {
  /**
   * @param {Object} params
   * @prop  {String} crmClient - The crmClient client
   * @prop  {String} billingClient - The billingClient client
   * @prop  {Object} [logger] - The logging utility
   * @return {SubscriptionWorker}
   */
  constructor(params) {
    const { crmClient, billingClient, s3, logger } = params;
    try {
      // Setup defaults
      this.logger = logger || console;
      this.s3 = s3 || new AWS.S3();

      // Validate inputs
      validate.required(params, ['crmClient', 'billingClient']);
      this.crmClient = crmClient;
      this.billingClient = billingClient;
      this.isMocked = Boolean(crmClient.isMocked || billingClient.isMocked);
      this.productFamilyId = config.product.familyId;
      this.productFamilyHandle = config.product.familyHandle;
      this.s3Bucket = config.product.s3Bucket;
      this.catalogFileName = `product-catalog-${this.billingClient.adapter.name}.json`;
      this.tagString = `catalog_source=${this.billingClient.adapter.name}&product_family_id=${this.productFamilyId}&product_family_handle=${this.productFamilyHandle}`;

      this.catalog = {};
    } catch (error) {
      logger.error(`SubscriptionWorker.new failed`);
      throw error;
    }
  }

  /*---------------------------------------------------------------------------------*/
  //                                Catalog Management
  /*---------------------------------------------------------------------------------*/

  // inspectCatalog
  // loadCatalog
  // fetchCatalog
  // cacheCatalog
  // getPriceOnDate({productHandle, componentHandle, onDate=now()})

  /**
   * Return a string that summarizes the loaded catalog
   *
   * @return {String} Query string containg key/value pairs that describe the loaded catalog
   */
  inspectCatalog() {
    const products = Object.keys(this.catalog)
      .map((k) => `&product_family_name=${k}&product_ids=${Object.keys(this.catalog[k].products).join(',')}`)
      .join(',');
    return `${this.tagString}${products}`;
    // TODO: Include prices?
    // .map((k) => catalog[k])
    // .join(',')}`;
  }

  /**
   * Retrieves the product catalog from the billing system
   *
   * @param {Object} params
   * @prop  {Boolean} updateCache - Whether to update the cached version of the catalog on S3 (default is false)
   * @return {Object} Catalog object
   */
  async loadCatalog(params = {}) {
    const { updateCache = false, useCache = false } = params;
    try {
      // Get a copy of the catalog
      this.catalog = await this.fetchCatalog({ useCache });

      // Update the backup version if necessary
      // TODO: Fingerprint files so that we don't save to S3 unless the file has changed
      if (updateCache) this.cacheCatalog({ catalog: this.catalog });
      return this.catalog;
    } catch (error) {
      this.logError(error, `synchronizeCatalog failed`);
      throw error;
    }
  }

  /**
   * Retrieves a JSON object from the billing system or S3 if the billing system doesn't respond
   *
   * @return {Object} Catalog object
   */
  async fetchCatalog(params = {}) {
    const { useCache = true } = params;
    let catalog;
    try {
      if (useCache) {
        // Get catalog from cache
        this.logger.debug('Attempting to load catalog from cache');
        catalog = await this.loadCatalogFromCache();
      } else {
        // Get the catalog from the billing client
        this.logger.debug('Attempting to load catalog from billing client');
        catalog = await this.billingClient.getCatalog({ productFamilyId: this.productFamilyId });
      }
      return catalog;
    } catch (error) {
      try {
        if (useCache) {
          // The cached version failed, so try getting the catalog from the billing client
          this.logger.warn(`Retrieving the cached catalog failed. Attempting to retrieve catalog from billing adapter`);
          catalog = await this.billingClient.getCatalog({ productFamilyId: this.productFamilyId });
        } else {
          // The billing client failed, so try getting the catalog from the cache
          this.logger.warn(`Retrieving the catalog from the billing adapter failed. Attempting to retrieve catalog from cache`);
          catalog = await this.loadCatalogFromCache();
        }
        return catalog;
      } catch (error2) {
        this.logError(error2, `The catalog could not be loaded from the cache or billing provider`);
        throw error2;
      }
    }
  }

  /**
   * Saves a copy of the product catalog object on S3
   *
   * @param {Object} params
   * @prop  {Object} catalog - Whether to update the cached version of the catalog on S3 (default is false)
   * @return {Object} Result of the request to save to S3
   */
  async cacheCatalog(params) {
    const { catalog } = params;
    try {
      // Store cached version in S3
      const result = await this.saveAsJsonToS3({
        s3: this.s3, //
        s3Bucket: this.s3Bucket,
        fileName: this.catalogFileName,
        jsObject: catalog,
        tagString: this.tagString,
        logger: this.logger,
      });
      return result;
    } catch (error) {
      this.logError(error, `cacheCatalog failed`);
      throw error;
    }
  }

  /**
   * Loads a copy of the product catalog object from S3
   *
   * @return {Object} Result of the request to save to S3
   */
  async loadCatalogFromCache() {
    // Get the catalog from the S3 folder
    const catalog = await this.loadJsonFromS3({
      s3: this.s3,
      s3Bucket: this.s3Bucket,
      fileName: this.catalogFileName,
      logger: this.logger,
    });
    return catalog;
  }

  // TODO: This should be in a utility class
  async loadJsonFromS3(params) {
    const { s3, s3Bucket, fileName, logger } = params;
    try {
      // Native AWS method
      // const data = await s3
      //   .getObject({
      //     Bucket: s3Bucket,
      //     Key: fileName,
      //     // ResponseContentType: 'application/json',
      //   })
      //   .promise();
      // AWS-Wrapper method
      const data = await s3.getObject(s3Bucket, fileName);
      if (data.mock) {
        logger.debug(`S3mock read file from: ${data.path}`);
      } else {
        logger.debug(`Successfully read file from S3 /${s3Bucket}/${fileName}`);
      }
      const fileContents = data.Body.toString('utf-8');
      const json = JSON.parse(fileContents);
      return json;
    } catch (error) {
      this.logError(error, `loadJsonFromS3 failed`);
      throw error;
    }
  }

  // TODO: This should be in a utility class
  async saveAsJsonToS3(params) {
    const { s3, s3Bucket, fileName, jsObject, tagString, logger } = params;
    try {
      const jsonString = JSON.stringify(jsObject);
      // result = await s3.upload({
      //   Bucket: s3Bucket,
      //   Key: fileName,
      //   Tagging: tagString,
      //   Body: jsonString,
      //   ContentType: 'application/json; charset=utf-8',
      // }).promise();
      const result = await s3.upload(s3Bucket, fileName, jsonString, tagString);
      if (result.mock) {
        logger.debug(`S3mock saved file to: ${result.path}`);
      } else {
        logger.debug(`Successfully uploaded file to S3 /${s3Bucket}/${fileName} with ETag ${result.ETag}`);
      }
      return result;
    } catch (error) {
      this.logError(error, `saveAsJsonToS3 failed`);
      throw error;
    }
  }

  /*---------------------------------------------------------------------------------*/
  //                                   Subscriptions
  /*---------------------------------------------------------------------------------*/

  // Subscription renewal
  // Subscription update
  // Subscription cancelation
  // Subscription reactivation

  /*---------------------------------------------------------------------------------
                             Activity Bound Trials (Freemium)

  The system is free to use as long as there are less than n number of something.
  Add more than n number of that thing requires a paid subscription.

  Example : Free to use and fully functioning up to 5 drawings.

  Lifecycle:
    1) Create new freemium subscription   createFreemium()
    2) Add user to existing trial         addContactToAccount()
    3) Update trial stage                 updateDealStage()
    4) Update activity count              updateFreemiumQuantity()
  ---------------------------------------------------------------------------------*/

  /**
   * Register a freemium subscription in all back-office systems
   *
   * @param  {String} params - context for the subscription request
   * @prop  {Object} user - Details about the user requesting the trial
   * @prop  {Object} account - Details about the account requesting the trial
   * @returns {Promise<Object>} - A object containing all of the response objects from the CRM and billing systems
   */
  async createFreemium(params) {
    this.logger.debug(`subscriptionWorker.createFreemium`, params);
    return this.registerSubscription({
      ...params,
      product: lookupProductByHandle(this.catalog, 'ix3_freemium'),
      crmPipelineName: 'FREEMIUM',
      crmPipelineStage: 'TRIAL REQUESTED',
      crmCustomerType: 'PROSPECT', // TODO: This doesn't seem to be used anywhere
      crmDealType: 'newbusiness',
      crmDealTrialType: 'Freemium',
    });
  }

  /**
   * Update the allocated quantity of a freemium subscription's metered item
   *
   * @param  {String} accountId - The IXC account's id
   * @returns {Promise<Object>} - Returns { success: true }
   */
  async updateFreemiumQuantity(params) {
    try {
      // Validate inputs
      validate.required(params, ['accountId', 'quantity']);

      const { accountId, quantity } = params;

      // Prepare a result object
      const result = { accountId };

      // Get the trial subscription
      let bsSubscription = await this.getSubscriptionForAccountId(accountId);
      // TODO: THIS IS NOT HOW FREEMIUM WORKS ANYMORE
      // Change paid seats to quantity (or 1 if not specified)
      this.validateCatalogKey(IXC_PRODUCT_FAMILY_HANDLE);
      this.validateComponentHandle(IXC_PRODUCT_FAMILY_HANDLE, PARTS_SS_COMPONENT_HANDLE);
      await this.billingClient.updateSubscriptionQuantity({
        id: bsSubscription.id,
        product: this.catalog[config.product.familyHandle],
        newQuantity: quantity,
        memo: `Freemium subscription updated to ${quantity}`,
      });

      // Get the new value of the adjusted subscription
      bsSubscription = await this.billingClient.getSubscriptionById(bsSubscription.id);
      result.bsSubscription = bsSubscription;

      const updateDealParams = {
        id: bsSubscription.reference, // reference is set to same value as hubspot_deal_id
        // TODO: Record quantity of metered items here
        // freemium_quantity: quantity,
      };

      // If the quantity is over the freemium limit, the subscription will have an amount
      if (bsSubscription.current_billing_amount_in_cents > 0) {
        // When the subscription has an amount, the deal should be closed won
        updateDealParams.amount = this.calculateDealValue(bsSubscription.current_billing_amount_in_cents / 100, params.billing_interval === 'Annually');
        updateDealParams.dealstage = config.crm.closedWon;
        updateDealParams.closedate = this.crmClient.dateInCrmFormat();
      }

      // Update crmDeal
      const crmDeal = await this.crmClient.updateDeal(updateDealParams);
      result.crmDeal = crmDeal;

      // Update company ???

      result.success = true;
      return result;
    } catch (error) {
      this.logError(error, `updateFreemiumQuantity failed`);
      throw error;
    }
  }

  async paymentFailure(payload, options) {
    this.logger.debug(`subscriptionWorker.paymentFailure`, payload);
    if (!EmailSender.isConfigured && !options.isTest) {
      throw new Error(`Email provider is not configured. Please configure it at backend/config/<environment>.json.`);
    }
    const { subscription, transaction } = payload;

    if (!subscription) {
      this.logError('No Subscription to query');
      throw new Error('No Subscription to query');
    }
    const date = transaction?.created_at ? moment(transaction.created_at, 'YYYY-MM-DDTHH:mm:ssZ') : moment();

    const { id, email } = subscription.customer;

    // Get Billing Portal Link
    let portal;
    try {
      portal = await this.billingClient.getBillingPortalLink(id);
    } catch (error) {
      this.logError(error, `billingClient.getBillingPortalLink failed`);
    }

    // Set Up Email
    let paymentFailureEmail;
    try {
      const billingUrl = `${config.protocol}://${config.hostname}${config.hostname.includes('local') && ':3000'}/settings/billing`;
      paymentFailureEmail = new EmailTemplate(options.language, 'paymentFailed', email, billingUrl, portal?.url);
      paymentFailureEmail.setRecipient = email;
      paymentFailureEmail.setDate = date.format('MMMM Do YYYY, h:mm a');
      if (portal?.expires_at) {
        paymentFailureEmail.disclaimer = portal?.expires_at;
      }
    } catch (error) {
      this.logError(error, `paymentFailureEmail failed`);
      throw error;
    }
    if (!paymentFailureEmail) {
      return false;
    }

    return options.isTest ? true : new EmailSender(paymentFailureEmail).send({ from: 'orders.qcessentials@ideagen.com' });
  }

  /*---------------------------------------------------------------------------------
                                  Time Bound Trials

  After n number of days, the trial expires or is converted to a paid subscription.
  Example : 7 Day Fully Functioning Free Trial

  Lifecycle:
    1) Create new trial                 createTrial()
    2) Add user to existing trial       addContactToAccount()
    3) Update trial stage               updateDealStage()
    4) Extend trial                     extendTrial()
    5) Expire trial                     expireTrial()
    6) Reactivate trial                 
    7) Convert trial to subscription    convertTrialToSubscription()

  ---------------------------------------------------------------------------------*/

  /**
   * Register a trial subscription in all back-office systems
   *
   * @param  {String} params - context for the subscription request
   * @prop  {Object} user - Details about the user requesting the trial
   * @prop  {Object} account - Details about the account requesting the trial
   * @returns {Promise<Object>} - A object containing all of the response objects from the CRM and billing systems
   */
  async createTrial(params) {
    this.logger.debug(`subscriptionWorker.createTrial`, params);
    return this.registerSubscription({
      ...params,
      product: lookupProductByHandle(this.catalog, IXC_STANDARD_HANDLE),
      crmCustomerType: 'PROSPECT', // TODO: This doesn't seem to be used anywhere
      crmPipelineName: config.crm.pipelineName, // 'ONLINE TRIAL',
      crmPipelineStage: config.crm.pipelineStage, // 'TRIAL REQUESTED',
      crmDealType: config.crm.dealType, // 'newbusiness',
      crmDealTrialType: config.crm.dealTrialType, // 'Time Limited',
    });
  }

  /**
   * Extend an existing trial
   *
   * @param  {String} accountId - The IXC account's id
   * @param  {String} newEndDate - Date on which trial should end
   * @returns {Promise<Object>} - Returns { success: true }
   */
  async extendTrial(params) {
    try {
      // Validate required fields
      validate.required(params, ['accountId', 'newEndDate']);
      const { accountId, newEndDate } = params;

      // Prepare a result object
      const result = { input: { accountId, newEndDate } };

      // Set new date to midnight UTC
      const newDate = new Date(newEndDate);
      newDate.setUTCHours(0, 0, 0, 0);

      // Get the trial subscription
      let bsSubscription = await this.getSubscriptionForAccountId(accountId, IXC_STANDARD_HANDLE);

      // Update the expires at date
      bsSubscription = await this.billingClient.updateSubscription({
        id: bsSubscription.id,
        // TODO: trial_ended_at is a historical record, not a controling field. Try changing "Next Billing Date" to see if that extends the trial.
        trial_ended_at: newDate.toISOString(), // TODO: Date Formatting should be done in Billing adapter
      });
      result.bsSubscription = bsSubscription;

      const crmNewDate = this.crmClient.dateInCrmFormat(newDate);

      const crmDeal = await this.crmClient.updateDeal({
        id: bsSubscription.reference, // reference is set to same value as hubspot_deal_id
        closedate: crmNewDate,
        trial_end_date: crmNewDate,
      });
      result.crmDeal = crmDeal;
      result.success = true;

      return result;
    } catch (error) {
      this.logError(error, `extendTrial failed`);
      throw error;
    }
  }

  /**
   * Expire an existing trial
   *
   * @param  {String} accountId - The IXC account's id
   * @returns {Promise<Object>} - Returns { success: true }
   */
  async expireTrial({ accountId }) {
    // Update the CRM system's deal stage
    const result = this.updateDealStage({
      accountId,
      stageName: config.crm.closedLost, // TODO: This should be a constant in the CRM adapter
    });
    // TODO: Update the billing system???
    return result;
  }
  // TODO: tests

  async rollbackTrialConversion(params) {
    // Validate required fields
    validate.required(params, ['subscriptionId', 'original', 'changed', 'metaData', 'rollback']);
    const { subscriptionId, original, changed, metaData, rollback } = params;

    try {
      if (rollback.subscription && (changed.state === 'active' || changed.state === 'trialing')) {
        this.logger.debug('Rolling back trial update...');
        await this.billingClient.updateSubscription({
          id: changed.id,
          product: original.product,
          next_billing_at: original.next_billing_at,
        });
      } else if (rollback.subscription) {
        this.logger.debug('Rolling back product update...');
        await this.billingClient.updateSubscription({
          id: changed.id,
          product: original.product,
        });
      }

      if (rollback.invoices) {
        this.logger.debug('Rolling back generated invoices...');
        await this.voidAllOpenInvoices({ subscriptionId: changed.id });
      }

      if (rollback.components) {
        this.logger.debug('Rolling back components updates...');
        const revert = original.components.map((comp) => {
          return {
            component_id: comp.component_id,
            handle: comp.component_handle,
            name: comp.name,
            quantity: comp.allocated_quantity,
            price_point: comp.price_point_id,
          };
        });
        await this.updateComponents({ components: revert, subscriptionComponents: changed.components, subscriptionId: changed.id, memo: 'Rollback' });
      }

      if (rollback.metadata) {
        this.logger.debug('Rolling back custom field updates...');
        const revert = [];
        const del = [];
        metaData.forEach((meta) => {
          if (original[meta.name]) {
            revert.push({ name: meta.name, value: original[meta.name] });
          } else {
            del.push(meta.name);
          }
        });
        if (revert.length > 0) {
          this.logger.debug('Reverting Custom Fields...');
          await this.billingClient.updateSubscriptionCustomFields({
            subscriptionId,
            changes: revert,
          });
        }
        if (del.length > 0) {
          this.logger.debug('Deleting Custom Fields...');
          await this.billingClient.deleteSubscriptionCustomFields({
            subscriptionId,
            changes: del,
          });
        }
      }
    } catch (error) {
      this.logError(error, `rollbackTrialConversion failed`);
      throw error;
    }
  }

  /**
   * Convert a trial to a Subscription
   *
   * @see {@link https://help.chargify.com/subscriptions/trialing-subscriptions.html#ending-a-trial-early}
   *
   * @param  {String} subscriptionId - Subscription ID in Billing System
   * @param  {String} data - The Subscription DB Input
   * @returns {Promise<Object>} - Returns { success: true, bsSubscription: Subscription, crmDeal: Deal }
   */
  async convertTrialToSubscription(params) {
    let originalSub;
    let bsSubscription;

    // Prepare a result object
    const result = {};
    const metaData = [];
    const rollback = {
      subscription: false,
      invoices: false,
      components: false,
      metadata: false,
    };

    try {
      // Validate required fields
      validate.required(params, ['subscriptionId', 'data']);
      this.validateCatalogKey(IXC_PRODUCT_FAMILY_HANDLE);

      const { subscriptionId, billingToken, data, interval = 'Annually' } = params;
      let { components } = params;

      // Get the trial subscription
      originalSub = await this.getSubscriptionForId(subscriptionId);
      bsSubscription = cloneDeep(originalSub);
      const subscribedBefore = !!data.termsAcceptedOn || !!bsSubscription.terms_accepted_on;
      const domain = bsSubscription.customer && bsSubscription.customer.email ? bsSubscription.customer.email.split('@')[1] : '';
      const isStaff = domain === 'inspectionxpert.com';

      // Use Default Component if None Are Provided
      if (!components || components.length <= 0) {
        this.logger.debug('Using Default Components');

        // Use $0 component for staff in production
        const componentHandle = isStaff ? INTERNAL_COMPONENT_HANDLE : SEATS_SS_COMPONENT_HANDLE;
        this.validateComponentHandle(IXC_PRODUCT_FAMILY_HANDLE, componentHandle);

        // Create array of default components
        components = [];
        const defaultComponent = lookupComponentByHandle(this.catalog, componentHandle);
        const defaultComponentPricePoint = await this.billingClient.getCurrentPrice(interval, defaultComponent.prices);

        const DEFAULT_COMPONENTS = {
          component_id: defaultComponent.id,
          handle: componentHandle,
          name: defaultComponent.name,
          quantity: 1,
          price_point: defaultComponentPricePoint.handle,
          unit_price: isStaff ? '0.00' : defaultComponentPricePoint.prices[0].unit_price,
        };

        components.push(DEFAULT_COMPONENTS);
      } else {
        // Make sure the component price point matches the product billing interval
        components = await Promise.all(
          components.map(async (comp) => {
            let componentHandle = isStaff ? INTERNAL_COMPONENT_HANDLE : SEATS_SS_COMPONENT_HANDLE;
            if (params.componentHandle) componentHandle = params.componentHandle;
            if (comp.name && comp.name.toLowerCase().includes('net-inspect')) componentHandle = NET_INSPECT_HANDLE;
            this.validateComponentHandle(IXC_PRODUCT_FAMILY_HANDLE, componentHandle);
            const fullComponent = lookupComponentByHandle(this.catalog, componentHandle);
            const componentPricePoint = await this.billingClient.getCurrentPrice(interval, fullComponent.prices);
            if (componentPricePoint.handle !== comp.price_point) {
              return { ...comp, price_point: componentPricePoint.handle, unit_price: componentPricePoint.prices[0].unit_price };
            }
            return comp;
          })
        );
      }

      // Set Subscription Values
      const subscriptionDurationInYears = 1;
      const memo = `${bsSubscription.product.name} Trial converted To Paid Subscription`;
      const startDate = moment.utc().endOf('day');
      const endDate = moment.utc().add(subscriptionDurationInYears, 'years');

      // Set Product
      this.validateProductHandle(IXC_PRODUCT_FAMILY_HANDLE, bsSubscription.product.handle);
      const product = this.catalog[IXC_PRODUCT_FAMILY_HANDLE].products[bsSubscription.product.handle];
      const productPricePoint = await this.billingClient.getCurrentPrice(interval, product.prices);
      product.product_price_point_handle = productPricePoint.handle;
      product.product_price_point_id = productPricePoint.id;

      // Set Billing Field
      if (interval !== bsSubscription.billing_interval) {
        metaData.push({ name: 'billing_interval', value: interval });
      }
      // Change Component Quantity
      await this.updateComponents({ components, subscriptionComponents: bsSubscription.components, subscriptionId: bsSubscription.id });
      rollback.components = true;

      let paymentMethod;
      // Add Payment Method if passed
      if (billingToken) {
        this.logger.debug('Updating Payment Method...');
        paymentMethod = await this.billingClient.addPaymentMethod({ customerId: bsSubscription.customer.id, billingToken, subscription: bsSubscription });
        this.logger.debug('Payment Method Updated');
      }

      // Void Previously Generated Invoices
      if (subscribedBefore) {
        await this.voidAllOpenInvoices({ subscriptionId: bsSubscription.id });
      } else {
        metaData.push({ name: 'terms_accepted_on', value: moment.utc().toDate() });
      }

      // Update Custom Fields
      if (metaData.length > 0) {
        this.logger.debug('Updating Custom Fields...');
        await this.billingClient.updateSubscriptionCustomFields({
          subscriptionId,
          changes: metaData,
        });
        this.logger.debug('Custom Fields Updated');
        rollback.metadata = true;
      }

      // Generate Invoice
      if (!billingToken && !this.hasActivePaymentMethod(paymentMethod)) {
        await this.generateInvoice({ components, bsSubscription, memo, endDate, startDate, interval });
        rollback.invoices = true;
        if (bsSubscription.state !== 'trialing') {
          /* Pay By Invoice - Expired Trial */
          this.logger.warn(`Remittance Subscription with id ${bsSubscription.id} has expired. Subscription cannot be activated until payment is received.`);
          // Update payment method to remittance
          await this.billingClient.updateSubscription({
            id: bsSubscription.id,
            payment_collection_method: 'remittance',
          });
          // Return Subscription
          bsSubscription = await this.getSubscriptionForId(bsSubscription.id);
          bsSubscription.termsAcceptedOn = data.termsAcceptedOn || moment.utc().toDate();
          return { bsSubscription, success: false };
        }
      }

      // Trigger End of Trial
      if (bsSubscription.state === 'trialing') {
        /* Pay By Credit Card Automatically - Active Trial */
        // Update dates and product
        this.logger.debug('Updating Trial Date and Product...');
        await this.billingClient.updateSubscription({
          id: bsSubscription.id,
          product,
          next_billing_at: startDate.format(), // This should convert the trial to a paid subscription within 30 minutes
          payment_collection_method: billingToken && this.hasActivePaymentMethod(paymentMethod) ? 'automatic' : 'remittance',
        });
        this.logger.debug('Trial Date and Product Updated');
      } else {
        /* Pay By Credit Card Automatically - Expired Trial */
        // Can't update next billing date for subscriptions that have ended
        this.logger.debug('Updating Product...');
        await this.billingClient.updateSubscription({
          id: bsSubscription.id,
          product,
          payment_collection_method: billingToken && this.hasActivePaymentMethod(paymentMethod) ? 'automatic' : 'remittance',
        });
        this.logger.debug('Product Updated');
        if (billingToken && this.hasActivePaymentMethod(paymentMethod)) {
          // Reactivate Expired trial and start it at today
          await this.billingClient.reactivateEndedTrial(bsSubscription.id);
          this.logger.debug('Subscription Activated');
        } else {
          this.logger.warn(`Remittance Subscription with id ${bsSubscription.id} has expired. Subscription cannot be activated until payment is received.`);
          bsSubscription.termsAcceptedOn = data.termsAcceptedOn || moment.utc().toDate();
        }
      }
      rollback.subscription = true;

      // Get Updated Subscription
      bsSubscription = await this.getSubscriptionForId(bsSubscription.id);
      // Set Accepted Value if not Previously Set
      bsSubscription.termsAcceptedOn = data.termsAcceptedOn || moment.utc().toDate();
      result.bsSubscription = bsSubscription;
      // TODO: Add Product to Deal

      // Update crmDeal
      if (bsSubscription.reference) {
        const crmDeal = await this.crmClient.updateDeal({
          id: bsSubscription.reference, // reference is set to same value as hubspot_deal_id
          amount: this.calculateDealValue(bsSubscription.current_billing_amount_in_cents / 100, interval === 'Annually'),
          dealstage: config.crm.closedWon, // TODO: This should be a constant in the CRM adapter
          closedate: startDate.valueOf(), // TODO: Date Formatting should be done in CRM adapter
        });
        result.crmDeal = crmDeal;
      }
      result.success = true;

      return result;
    } catch (error) {
      this.logError(error, `convertTrialToSubscription failed`);
      await this.rollbackTrialConversion({ subscriptionId: params.subscriptionId, original: originalSub, changed: bsSubscription, metaData, rollback });
      this.logger.debug('Roll Back Complete');
      throw error;
    }
  }

  /*---------------------------------------------------------------------------------*/
  //                        Universal to all Trial Types
  /*---------------------------------------------------------------------------------*/

  async setIxcDbId(params) {
    try {
      // Validate required fields
      validate.required(params, ['dbId', 'subscriptionId']);
      const { subscriptionId, dbId } = params;

      // Set Custom Field ixc_subscription_id to the passed ID
      const result = await this.billingClient.updateSubscriptionCustomFields({ subscriptionId, changes: [{ name: 'ixc_subscription_id', value: dbId }] });

      return result;
    } catch (error) {
      this.logError(error, `setIxcDbId failed`);
      throw error;
    }
  }

  /**
   * Update Components in the Billing Adapter
   *
   * @param {Array} components - Array of Components to Change
   * @param {Array} subscriptionComponents - Array of Current Components
   * @param {String} subscriptionId - Subscription ID in Billing System
   */
  async updateComponents(params) {
    try {
      validate.required(params, ['components', 'subscriptionComponents', 'subscriptionId']);
      const { components, subscriptionComponents, subscriptionId, memo } = params;
      this.logger.debug('Updating Components...');
      this.logger.debug(components);

      await Promise.all(
        components.map((comp) => {
          validate.required(comp, ['component_id', 'price_point', 'quantity']);
          const current = subscriptionComponents.find((c) => c.component_id === comp.component_id);
          if (current) {
            validate.required(current, ['allocated_quantity', 'price_point_id']);
          }
          if (current && current.allocated_quantity !== comp.quantity) {
            return this.billingClient.updateSubscriptionQuantity({
              id: subscriptionId,
              componentId: comp.component_id,
              pricePoint: comp.price_point,
              newQuantity: comp.quantity,
              upgrade: 'none',
              acrue: false,
              memo,
            });
          }
          if (current && current.price_point_id !== comp.price_point) {
            return this.billingClient.updateComponentPrice({
              id: subscriptionId,
              componentId: comp.component_id,
              pricePoint: comp.price_point,
            });
          }
          return Promise.resolve();
        })
      );

      this.logger.debug('Components Updated');
    } catch (error) {
      this.logError(error, `updateComponents failed`);
      log.error(error);
      throw error;
    }
  }

  /**
   * Search Billing System for Open Invoices and Void them
   *
   * @param {String} subscriptionId - Subscription ID in the Billing System
   */
  async voidAllOpenInvoices(params) {
    try {
      validate.required(params, ['subscriptionId']);
      const { subscriptionId } = params;
      this.logger.debug(`Checking for Previous Invoices...`);
      const invoices = await this.billingClient.getInvoicesForSubscription(subscriptionId);
      await Promise.all(
        invoices.map((invoice) => {
          if (!invoice) {
            return Promise.resolve();
          }
          validate.required(invoice, ['uid']);
          this.logger.debug(`Voiding Previous Invoice ${invoice.number}...`);
          return this.billingClient.voidInvoice(invoice.uid, `Voided in place of Newer Invoice`);
        })
      );
      this.logger.debug('Previous Invoices Voided');
    } catch (error) {
      this.logError(error, `voidAllOpenInvoices failed`);
      throw error;
    }
  }

  async generateInvoice(params) {
    try {
      const { components, bsSubscription, memo, startDate, endDate, interval } = params;

      const domain = bsSubscription.customer && bsSubscription.customer.email ? bsSubscription.customer.email.split('@')[1] : '';
      const isStaff = domain === 'inspectionxpert.com';

      // Generate Line Items for Invoice (Remittance)
      const lineItems = components.map((comp) => {
        return {
          component_id: comp.component_id,
          name: comp.name,
          allocated_quantity: comp.type === 'stairstep' ? 1 : comp.quantity,
          unit_price: isStaff ? '0.00' : comp.unit_price,
          start: startDate.format('YYYY-MM-DD'),
          end: interval === 'Annually' ? endDate.format('YYYY-MM-DD') : moment.utc().add(1, 'months').format('YYYY-MM-DD'),
        };
      });

      // Generate Invoice
      /* Pay By Invoice Subscription */
      // Generate Invoice
      this.logger.debug('Generating Invoice...');
      await this.billingClient.issueInvoice(bsSubscription.id, lineItems, 1, memo);
      this.logger.debug('Invoice Generated');
    } catch (error) {
      this.logError(error, `generateInvoice failed`);
      throw error;
    }
  }

  /**
   * Update the terms of the subscription
   *
   * @param  {String} subscriptionId - The BC subscription's id
   * @param  {String} billingPeriod - The subscription new net terms
   * @param  {String} providerProductId - The id of the BC product
   * @param  {String} components - The subscription components to update
   * @returns {Promise<Object>} - Returns { success: true }
   */
  async updateSubscription(params) {
    try {
      // Validate inputs
      validate.required(params, ['subscriptionId']);

      const { subscriptionId, billingPeriod, providerProductId, components, remittance } = params;

      // Prepare a result object
      const result = {};

      // Get the current subscription
      let bsSubscription = await this.getSubscriptionForId(subscriptionId);

      // Prepare the request object
      const request = {};
      let changed = false;

      // Set Billing Interval
      if (billingPeriod && billingPeriod !== bsSubscription.billing_interval) {
        // TODO: Delay this change to the end of the annual contract
        await this.billingClient.updateSubscriptionCustomFields({
          subscriptionId,
          changes: [{ name: 'billing_interval', value: billingPeriod }],
        });
        bsSubscription.billing_interval = billingPeriod;
        const started = bsSubscription.activated_at ? moment.utc(bsSubscription.activated_at, 'YYYY-MM-DDTHH:mm:ssZ') : moment.utc(bsSubscription.trial_ended_at, 'YYYY-MM-DDTHH:mm:ssZ');
        const prices = this.catalog[bsSubscription.product.product_family.handle]?.products[bsSubscription.product.handle]?.prices || [];
        const productPricePoint = await this.billingClient.getCurrentPrice(billingPeriod, prices);
        request.product_price_point_handle = productPricePoint.handle;
        const term = billingPeriod === 'Annually' ? 'years' : 'months';
        const currentStartAt = moment.utc(bsSubscription.current_period_started_at, 'YYYY-MM-DDTHH:mm:ssZ');
        request.next_billing_at = billingPeriod === 'Annually' ? started.add(1, 'years') : currentStartAt.add(1, 'months');
        const today = moment.utc();
        while (request.next_billing_at.isSameOrBefore(today)) {
          request.next_billing_at = request.next_billing_at.add(1, term);
        }
        request.next_billing_at = request.next_billing_at.toString();
      }

      // Set Product for Subscription
      if (providerProductId && providerProductId !== bsSubscription.product.id) {
        request.product_id = providerProductId;
      }

      if (remittance) {
        request.payment_collection_method = 'remittance';
      }

      // Update Chargify Subscription
      if (request !== {}) {
        this.logger.debug('Updating Subscription...');
        await this.billingClient.updateSubscription({ ...request, id: subscriptionId });
        changed = true;
        this.logger.debug('Subscription Updated');
      }

      // Change Component Quantity
      if (components && components.length > 0) {
        changed = await this.updateComponents({ components, subscriptionComponents: bsSubscription.components, subscriptionId: bsSubscription.id });
      }

      if (bsSubscription.state === 'trialing' && bsSubscription.payment_collection_method === 'remittance') {
        // subscription was upgraded after a trial converted with remittance, but before they paid their balance
        // void the old invoice, generate the new invoice
        await this.voidAllOpenInvoices({ subscriptionId });

        const memo = `${bsSubscription.product.name} Paid Subscription upgraded during Remittance Grace Period`;
        // Set Subscription Values
        const subscriptionDurationInYears = 1;
        const startDate = moment.utc().endOf('day');
        const endDate = moment.utc().add(subscriptionDurationInYears, 'years');
        const interval = billingPeriod || bsSubscription.billing_interval;

        await this.generateInvoice({ components, bsSubscription, memo, endDate, startDate, interval });
      }

      bsSubscription = await this.getSubscriptionForId(subscriptionId);

      // Get the new value of the adjusted subscription
      result.update = bsSubscription;
      // TODO: Fix this for CRM updates of metrics
      // if (bsSubscription.reference && changed) {
      //   this.logger.debug('Updating CRM...');

      //   const updateDealParams = {
      //     ...changed,
      //     id: bsSubscription.reference, // reference is set to same value as hubspot_deal_id
      //     // TODO: Record quantity of metered items here
      //     // freemium_quantity: quantity,
      //   };

      if (bsSubscription.reference) {
        const crmDeal = await this.crmClient.updateDeal({
          id: bsSubscription.reference, // reference is set to same value as hubspot_deal_id
          amount: this.calculateDealValue(bsSubscription.current_billing_amount_in_cents / 100, billingPeriod === 'Annually'),
        });
        result.crmDeal = crmDeal;
      }
      // }

      result.success = true;
      return result;
    } catch (error) {
      this.logError(error, `updateSubscription failed`);
      throw error;
    }
  }

  /**
   * Create and add a IXC team member to the CRM deal and company associated with the IXC account
   *
   * @param  {String} params
   * @prop  {Object} account - The IXC account
   * @prop  {Object} user - The contact's IXC user
   * @prop {Object} subscription - The IXC account's subscription
   * @returns {Promise<Object>} - Returns { success: true }
   */
  async addContactToAccount(params) {
    try {
      // Validate required fields
      validate.required(params, ['user', 'account']);

      // Standardize the input
      const data = SubscriptionWorker.extractAppContext(params);

      // Prepare a result object
      const result = { input: { ...data } };

      // Lookup the Customer and Company associated with the IXC account
      const bsSubscription = await this.getSubscriptionForAccountId(data.accountId);
      const bsCustomerMetadata = await this.billingClient.getMetadataForCustomerWithId(bsSubscription.customer.id);
      const crmCompany = await this.crmClient.getCompanyById(bsCustomerMetadata.hubspot_company_id.trim());

      // Create or update a CRM Contact
      const crmContact = await this.crmClient.createOrUpdateContact({
        ixc_user_id: data.userId,
        ix_amplitude_link: amplitudeUserActivtyLink(data.userId),
        ixc_account_id: data.accountId,
        ixc_role: data.roles,
        firstname: data.firstName,
        lastname: data.lastName,
        company: data.orgName,
        email: data.email,
        phone: data.phone,
        website: data.domain,
        city: data.orgCity,
        state: data.orgRegion,
      });
      // TODO: Save user.crmContactId = crmContact.crmId;
      result.crmContact = crmContact;

      // Add the CRM Contact to the CRM Company
      await this.crmClient.addContactToCompany({
        contactVid: crmContact.crmId,
        companyId: crmCompany.crmId,
      });

      // Update billingCustomer if this Contact is the subscription Customer or the current Owner
      if (bsSubscription.customer.email === data.email || data.roles.includes('Owner')) {
        const bsCustomerProps = {
          id: bsSubscription.customer.id,
        };
        if (data.firstName) bsCustomerProps.first_name = data.firstName;
        if (data.lastName) bsCustomerProps.last_name = data.lastName;
        if (data.email) {
          bsCustomerProps.email = data.email;
          bsCustomerProps.cc_emails = data.email;
        }
        const bsCustomer = await this.billingClient.updateCustomer(bsCustomerProps);
        result.bsCustomer = bsCustomer;
      }

      // Update deal if it exists
      if (bsSubscription.reference || bsCustomerMetadata.hubspot_deal_id) {
        const crmDeal = await this.crmClient.getDealById(bsSubscription.reference || bsCustomerMetadata.hubspot_deal_id);

        if (crmDeal && crmDeal.status !== 'error') {
          // Add the CRM Contact to the CRM Deal
          await this.crmClient.addContactToDeal({
            contactVid: crmContact.crmId,
            dealId: crmDeal.crmId,
          });
        }
      }

      result.success = true;
      return result;
    } catch (error) {
      this.logError(error, `addContactToAccount failed`);
      throw error;
    }
  }

  /**
   * Remove a IXC team member to the CRM deal and company associated with the IXC account
   *
   * @param  {String} params
   * @prop  {Object} account - The IXC account
   * @prop  {Object} user - The contact's IXC user
   * @prop {Object} subscription - The IXC account's subscription
   * @returns {Promise<Object>} - Returns { success: true }
   */
  async removeContactFromAccount(params) {
    try {
      // Validate required fields
      validate.required(params, ['user', 'account']);

      // Standardize the input
      const data = SubscriptionWorker.extractAppContext(params);

      // Prepare a result object
      const result = { input: { ...data } };

      // Lookup the Customer and Company associated with the IXC account
      const bsSubscription = await this.getSubscriptionForAccountId(data.accountId);
      const bsCustomerMetadata = await this.billingClient.getMetadataForCustomerWithId(bsSubscription.customer.id);
      const crmCompany = await this.crmClient.getCompanyById(bsCustomerMetadata.hubspot_company_id.trim());

      // Lookup the CRM Contact
      const crmContact = await this.crmClient.getContactByEmail(data.email);
      result.crmContact = crmContact;

      // Remove the CRM Contact from the CRM Company
      await this.crmClient.removeContactFromCompany({
        contactVid: crmContact.crmId,
        companyId: crmCompany.crmId,
      });

      // Only update the deal if it exists
      if (bsSubscription.reference) {
        const crmDeal = await this.crmClient.getDealById(bsSubscription.reference);
        if (crmDeal && crmDeal.status !== 'error') {
          // Remove the CRM Contact to the CRM Deal
          await this.crmClient.removeContactFromDeal({
            contactVid: crmContact.crmId,
            dealId: crmDeal.crmId,
          });
        }
      }

      result.success = true;
      return result;
    } catch (error) {
      this.logError(error, `removeContactFromAccount failed`);
      throw error;
    }
  }

  /**
   * Register a Subscription in all back-office systems
   *
   * @param  {String} params
   * @prop  {String} catalogItem - The lookup key that corresponds to an entry into the product catalog constant in the billingAdapter (ie. ix_standard)
   * @prop  {String} crmPipelineName - The name of the pipeline / funnel that the subscription should be registered in (ie. 'ONLINE TRIAL')
   * @prop  {String} crmCustomerType - The category of contact that the subscription's requestor should be added as (ie. 'PROSPECT')
   * @prop  {String} catalogItem - The lookup key that corresponds to an entry into the product catalog constant in the billingAdapter (ie. ix3_trial)
   * @prop  {...}  - All remaining values in params will be interpreted as context for the request
   * @returns {Promise<Object>} - A object containing all of the response objects from the CRM and billing systems
   */
  async beginRegistration(params) {
    this.logger.debug(`subscriptionWorker.beginRegistration`, params);
    try {
      // Validate required fields and standardize the input
      validate.required(params, ['user', 'account']);
      const data = SubscriptionWorker.extractAppContext(params);

      // Prepare a result object
      const result = { input: { ...data } };

      const { forceCreateCompany = false } = params;

      // Create or update the CRM Company
      const crmCompany = await this.crmClient.createOrUpdateCompany({
        type: params.crmType,
        ixc_account_id: data.accountId,
        name: data.orgName,
        // phone: data.phone,
        domain: data.domain,
        website: data.domain,
        // address: data.orgStreet,
        // address2: data.orgStreet2,
        // city: data.orgCity,
        // zip: data.orgPostalcode,
        // state: data.orgRegion,
        // country: data.orgCountry,
        forceCreateCompany,
      });
      result.crmCompany = crmCompany;

      // Create the CRM Contact
      const crmContact = await this.crmClient.createOrUpdateContact({
        ixc_user_id: data.userId,
        ix_amplitude_link: amplitudeUserActivtyLink(data.userId),
        ixc_account_id: data.accountId,
        ixc_role: 'Owner,Admin',
        firstname: data.firstName,
        lastname: data.lastName,
        company: data.orgName,
        email: data.email,
        phone: data.phone,
        website: data.domain,
        // address: data.orgStreet,
        // address2: data.orgStreet2,
        city: data.orgCity,
        // zip: data.orgPostalcode,
        state: data.orgRegion,
        // country: data.orgCountry,
      });
      // TODO: Save user.crmContactId = crmContact.crmId;
      result.crmContact = crmContact;

      // Add the CRM Contact to the CRM Company
      await this.crmClient.addContactToCompany({
        contactVid: crmContact.crmId,
        companyId: crmCompany.crmId,
      });

      result.success = true;
      return result;
    } catch (error) {
      this.logError(error, `beginRegistration failed`);
      throw error;
    }
  }

  /**
   * Update the User and Account details on record in all back office systems
   *
   * @param  {String} params
   * @prop  {String} accountId - The IXC account's id
   * @prop  {String} firstName - The last name of the IXC Account owner
   * @prop  {String} lastName - The first name of the IXC Account owner
   * @prop  {String} orgName - The name of the IXC Account
   * @returns {Promise<Object>} - Returns { success: true, bsCustomer, crmContact, crmCompany, crmDeal }
   */
  async completeRegistration(params) {
    this.logger.debug(`subscriptionWorker.completeRegistration`, params);
    try {
      // Validate required fields
      validate.required(params, ['user', 'account', 'orgName']);

      // Standardize the input
      const data = SubscriptionWorker.extractAppContext(params);
      const { user, account, email, firstName, lastName, orgName, phoneNumber } = params;
      data.email = email;
      data.firstName = firstName;
      data.lastName = lastName;
      data.orgName = orgName;
      data.phone = phoneNumber;

      // Use $0 component for staff in production
      const componentHandle = data.domain === 'inspectionxpert.com' ? INTERNAL_COMPONENT_HANDLE : SEATS_SS_COMPONENT_HANDLE;

      // Prepare a result object
      const result = { input: { ...data } };

      // crmCompany passed in as param if finishing registration for orphan account
      const crmCompany = params.crmCompany ? params.crmCompany : await this.crmClient.getCompanyByAccountId(account.id);

      let crmContact = await this.crmClient.getContactByEmail(email);

      const crmContactInput = {
        id: crmContact.crmId,
      };

      const crmCompanyInput = {
        id: crmCompany.crmId,
        name: orgName,
        // If the existing company's name is its domain name, update it with the new value (because they're completing their registration)
        updateAllNameFields: crmCompany.name.toLowerCase() === crmCompany.domain.toLowerCase(),
      };

      if (firstName) {
        crmContactInput.firstName = firstName;
      }

      if (lastName) {
        crmContactInput.lastName = lastName;
      }

      if (phoneNumber) {
        crmContactInput.phone = phoneNumber;
        if (!crmCompany.phone) crmCompanyInput.phone = phoneNumber; // Overwrite for Contact but not for Company.
      }

      // Update the CRM Contact
      crmContact = await this.crmClient.updateContact(crmContactInput);
      result.crmContact = crmContact;

      // Create the CRM Deal
      // console.log('crmDeal.params', params);
      let crmDeal = await this.crmClient.createDeal({
        ixc_account_id: account.id,
        dealname: `${orgName} : ${firstName} ${lastName}`,
        dealtype: params.crmDealType || config.crm.dealType, // 'newbusiness',
        pipeline: params.crmPipelineName || config.crm.pipelineName, // 'ONLINE TRIAL',,
        dealstage: params.crmPipelineStage || config.crm.pipelineStage, // 'TRIAL REQUESTED',
        trial_type: params.crmDealTrialType || config.crm.dealTrialType, // 'Time Limited',,
      });
      result.crmDeal = crmDeal;

      // Create the CRM Contact to the CRM Deal
      await this.crmClient.addContactToDeal({
        contactVid: crmContact.crmId,
        dealId: result.crmDeal.crmId,
      });

      // Create the CRM Company to the CRM Deal
      await this.crmClient.addCompanyToDeal({
        companyId: crmCompany.crmId,
        dealId: result.crmDeal.crmId,
      });

      // Create the Billing System Customer
      // TODO: Chargify : createOrUpdateCustomer from getCustomer, createCustomer, updateCustomer
      // console.log({ ixc_account_id: data.accountId, hubspot_company_id: crmCompany.crmId });
      const bsCustomer = await this.billingClient.createOrUpdateCustomer({
        reference: account.id,
        metafields: {
          ixc_account_id: account.id,
          hubspot_company_id: crmCompany.crmId,
        },
        ...data,
      });
      result.bsCustomer = bsCustomer;

      // Create the Billing System Subscription
      // TODO: createOrUpdateSubscription from getSubscriptionsForCustomer, createSubscription
      const product = { ...lookupProductByHandle(this.catalog, IXC_STANDARD_HANDLE) };
      const productPricePoint = await this.billingClient.getCurrentPrice(params.billing_interval || 'Annually', product.prices);
      product.product_price_point_handle = productPricePoint.handle;
      product.product_price_point_id = productPricePoint.id;

      let bsSubscription = await this.billingClient.createSubscription({
        product,
        customerId: bsCustomer.id,
        cancel_at_end_of_period: true, // Setting to true so trial expires on bsSubscription.expires_at //TODO: is this still the right way to handle trials
        // coupon_code
        reference: crmDeal.crmId,
        metafields: {
          hubspot_deal_id: crmDeal.crmId,
          billing_interval: params.billing_interval || 'Annually', // Billing interval Annually or Monthly
        },
      });

      // Change component to quantity
      const defaultComponent = lookupComponentByHandle(this.catalog, componentHandle);
      const defaultComponentPricePoint = await this.billingClient.getCurrentPrice(params.billing_interval || 'Annually', defaultComponent.prices);

      await this.billingClient.updateSubscriptionQuantity({
        id: bsSubscription.id,
        componentId: defaultComponent.id,
        pricePoint: defaultComponentPricePoint.handle,
        newQuantity: 1,
        upgrade: 'none',
        acrue: false,
        memo: 'Initial seat component for new trial',
      });

      await this.billingClient.updateComponentPrice({
        id: bsSubscription.id,
        componentId: defaultComponent.id,
        pricePoint: defaultComponentPricePoint.handle,
      });

      bsSubscription = {
        ...bsSubscription,
        components: [
          {
            component_id: defaultComponent.id,
            subscription_id: bsSubscription.id,
            component_handle: defaultComponent.handle,
            price_point_id: defaultComponentPricePoint.id,
            price_point_handle: defaultComponentPricePoint.handle,
            allocated_quantity: 1,
            name: defaultComponent.name,
            kind: defaultComponent.kind,
            unit_name: defaultComponent.unit_name,
            pricing_scheme: defaultComponent.pricing_scheme,
          },
        ],
      };

      result.bsSubscription = bsSubscription;
      result.bsSubscription.companyId = crmCompany.crmId;

      // Add Billing System record IDs to CRM records
      result.crmCompany = await this.crmClient.updateCompany({
        id: crmCompany.crmId,
        ix_chargify_id: bsCustomer.id,
        ix_chargify_link: this.billingClient.getVendorUrlForCustomerWithId(bsCustomer.id),
        ...crmCompanyInput,
      });

      // Get ready to create the deal
      const dealParams = {
        id: crmDeal.crmId,
        amount: this.calculateDealValue(bsSubscription.current_billing_amount_in_cents / 100),
        ixc_account_id: account.id,
        ix_chargify_id: bsSubscription.id,
        ix_chargify_link: this.billingClient.getVendorUrlForSubscriptionWithId(bsSubscription.id), // TODO: This should be provided by the adapter
      };

      // If this is a time bound trial, the trial_ended_at value will be set
      if (bsSubscription.trial_ended_at) {
        const cd = this.crmClient.dateInCrmFormat(bsSubscription.trial_ended_at);
        dealParams.trial_started_date = this.crmClient.dateInCrmFormat(bsSubscription.trial_started_at);
        dealParams.trial_end_date = cd;
        dealParams.closedate = cd;
      }

      // Update the CRM Deal
      crmDeal = await this.crmClient.updateDeal(dealParams);
      result.crmDeal = crmDeal;

      // Record the event on all timelines
      this.recordTimelineEvent({
        currentUser: user,
        crmContactId: crmContact.crmId,
        crmCompanyId: crmCompany.crmId,
        crmDealId: crmDeal.crmId,
        eventName: 'IXC Account Registration Finished',
        eventData: {
          // Tokens
          ixcAccountId: account.id,
          accountName: orgName,
          ixcUserId: user.id,
          userName: `${firstName} ${lastName}`,
          userEmail: email,
          userPhone: phoneNumber,
          // extraData
          subscription: bsSubscription,
        },
      });

      result.success = true;
      return result;
    } catch (error) {
      this.logError(error, `subscriptionWorker.completeRegistration failed`);
      throw error;
    }
  }

  /**
   * Register a Subscription in all back-office systems
   *
   * @param  {String} params
   * @prop  {String} catalogItem - The lookup key that corresponds to an entry into the product catalog constant in the billingAdapter (ie. ix_standard)
   * @prop  {String} crmPipelineName - The name of the pipeline / funnel that the subscription should be registered in (ie. 'ONLINE TRIAL')
   * @prop  {String} crmCustomerType - The category of contact that the subscription's requestor should be added as (ie. 'PROSPECT')
   * @prop  {String} catalogItem - The lookup key that corresponds to an entry into the product catalog constant in the billingAdapter (ie. ix3_trial)
   * @prop  {...}  - All remaining values in params will be interpreted as context for the request
   * @returns {Promise<Object>} - A object containing all of the response objects from the CRM and billing systems
   */
  async registerSubscription(params) {
    this.logger.debug(`subscriptionWorker.registerSubscription`, params);
    try {
      // Validate required fields
      validate.required(params, [
        'user', //
        'account',
        'product',
        'crmCustomerType', // TODO: This doesn't seem to be used anywhere
        'crmPipelineName',
        'crmPipelineStage',
        'crmDealType',
      ]);

      // Standardize the input
      const data = SubscriptionWorker.extractAppContext(params);

      // Use $0 component for staff in production
      const componentHandle = data.domain === 'inspectionxpert.com' ? INTERNAL_COMPONENT_HANDLE : SEATS_SS_COMPONENT_HANDLE;

      // Prepare a result object
      const result = { input: { ...data } };

      // Create or update the CRM Company
      const crmCompany = await this.crmClient.createOrUpdateCompany({
        type: params.crmType,
        ixc_account_id: data.accountId,
        name: data.orgName,
        // phone: data.phone,
        domain: data.domain,
        website: data.domain,
        // address: data.orgStreet,
        // address2: data.orgStreet2,
        // city: data.orgCity,
        // zip: data.orgPostalcode,
        // state: data.orgRegion,
        // country: data.orgCountry,
      });
      result.crmCompany = crmCompany;

      // Create the CRM Contact
      const crmContact = await this.crmClient.createOrUpdateContact({
        ixc_user_id: data.userId,
        ix_amplitude_link: amplitudeUserActivtyLink(data.userId),
        ixc_account_id: data.accountId,
        ixc_role: 'Owner,Admin',
        firstname: data.firstName,
        lastname: data.lastName,
        company: data.orgName,
        email: data.email,
        phone: data.phone,
        website: data.domain,
        // address: data.orgStreet,
        // address2: data.orgStreet2,
        city: data.orgCity,
        // zip: data.orgPostalcode,
        state: data.orgRegion,
        // country: data.orgCountry,
      });
      // TODO: Save user.crmContactId = crmContact.crmId;
      result.crmContact = crmContact;

      // Add the CRM Contact to the CRM Company
      await this.crmClient.addContactToCompany({
        contactVid: crmContact.crmId,
        companyId: crmCompany.crmId,
      });

      // Create the CRM Deal
      // console.log('crmDeal.params', params);
      const crmDeal = await this.crmClient.createDeal({
        ixc_account_id: params.accountId,
        dealname: `${data.domain} : ${data.email}`,
        dealtype: params.crmDealType,
        pipeline: params.crmPipelineName,
        dealstage: params.crmPipelineStage,
        trial_type: params.crmDealTrialType,
      });
      result.crmDeal = crmDeal;

      // Create the CRM Contact to the CRM Deal
      await this.crmClient.addContactToDeal({
        contactVid: crmContact.crmId,
        dealId: result.crmDeal.crmId,
      });

      // Create the CRM Company to the CRM Deal
      await this.crmClient.addCompanyToDeal({
        companyId: crmCompany.crmId,
        dealId: result.crmDeal.crmId,
      });

      // Create the Billing System Customer
      // TODO: Chargify : createOrUpdateCustomer from getCustomer, createCustomer, updateCustomer
      // console.log({ ixc_account_id: data.accountId, hubspot_company_id: crmCompany.crmId });
      const bsCustomer = await this.billingClient.createOrUpdateCustomer({
        reference: data.accountId,
        metafields: {
          ixc_account_id: data.accountId,
          hubspot_company_id: crmCompany.crmId,
        },
        ...data,
      });

      result.bsCustomer = bsCustomer;
      // Create the Billing System Subscription
      // TODO: createOrUpdateSubscription from getSubscriptionsForCustomer, createSubscription

      const product = { ...params.product };
      const productPricePoint = await this.billingClient.getCurrentPrice(params.billing_interval || 'Annually', product.prices);
      product.product_price_point_handle = productPricePoint.handle;
      product.product_price_point_id = productPricePoint.id;

      let bsSubscription = await this.billingClient.createSubscription({
        product,
        customerId: bsCustomer.id,
        cancel_at_end_of_period: true, // Setting to true so trial expires on bsSubscription.expires_at //TODO: is this still the right way to handle trials
        // coupon_code
        reference: crmDeal.crmId,
        metafields: {
          hubspot_deal_id: crmDeal.crmId,
          billing_interval: params.billing_interval || 'Annually', // Billing interval Annually or Monthly
        },
      });

      // Change component to quantity
      const defaultComponent = lookupComponentByHandle(this.catalog, componentHandle);
      const defaultComponentPricePoint = await this.billingClient.getCurrentPrice(params.billing_interval || 'Annually', defaultComponent.prices);

      await this.billingClient.updateSubscriptionQuantity({
        id: bsSubscription.id,
        componentId: defaultComponent.id,
        pricePoint: defaultComponentPricePoint.handle,
        newQuantity: 1,
        upgrade: 'none',
        acrue: false,
        memo: 'Initial seat component for new trial',
      });
      await this.billingClient.updateComponentPrice({
        id: bsSubscription.id,
        componentId: defaultComponent.id,
        pricePoint: defaultComponentPricePoint.handle,
      });
      bsSubscription = {
        ...bsSubscription,
        components: [
          {
            component_id: defaultComponent.id,
            subscription_id: bsSubscription.id,
            component_handle: defaultComponent.handle,
            price_point_id: defaultComponentPricePoint.id,
            price_point_handle: defaultComponentPricePoint.handle,
            allocated_quantity: 1,
            name: defaultComponent.name,
            kind: defaultComponent.kind,
            unit_name: defaultComponent.unit_name,
            pricing_scheme: defaultComponent.pricing_scheme,
          },
        ],
      };

      result.bsSubscription = bsSubscription;
      result.bsSubscription.companyId = crmCompany.crmId;

      // Add Billing System record IDs to CRM records
      result.crmCompany = await this.crmClient.updateCompany({
        id: crmCompany.crmId,
        ix_chargify_id: bsCustomer.id,
        ix_chargify_link: this.billingClient.getVendorUrlForCustomerWithId(bsCustomer.id),
      });

      // Get ready to create the deal
      const dealParams = {
        id: crmDeal.crmId,
        amount: this.calculateDealValue(bsSubscription.current_billing_amount_in_cents / 100),
        ixc_account_id: data.accountId,
        ix_chargify_id: bsSubscription.id,
        ix_chargify_link: this.billingClient.getVendorUrlForSubscriptionWithId(bsSubscription.id), // TODO: This should be provided by the adapter
      };

      // If this is a time bound trial, the trial_ended_at value will be set
      if (bsSubscription.trial_ended_at) {
        const cd = this.crmClient.dateInCrmFormat(bsSubscription.trial_ended_at);
        dealParams.trial_started_date = this.crmClient.dateInCrmFormat(bsSubscription.trial_started_at);
        dealParams.trial_end_date = cd;
        dealParams.closedate = cd;
      }
      result.crmDeal = await this.crmClient.updateDeal(dealParams);
      result.success = true;
      return result;
    } catch (error) {
      this.logError(error, `registerSubscription failed`);
      throw error;
    }
  }

  /**
   * Create CRM deal for existing subscription
   *
   * @param {String} accountId
   * @param {String} email
   * @prop  {String} crmPipelineStage - The name of the pipeline / funnel stage that the subscription should start at (ie. 'TRIAL REQUESTED')
   * @prop  {String} crmPipelineName - The name of the pipeline / funnel that the subscription should be registered in (ie. 'ONLINE TRIAL')
   * @prop  {String} crmDealType - The category of deal that the subscription opportunity should be added as in the CRM (ie. 'newbusiness')
   * @prop  {String} crmDealTrialType - The name of the trial mode
   * @returns {Promise<Object>} deal object
   */
  async registerSalesAcceptedLead(params) {
    this.logger.debug(`subscriptionWorker.registerSalesAcceptedLead`, params);
    try {
      validate.required(params, ['accountId', 'email', 'crmPipelineStage', 'crmPipelineName', 'crmDealTrialType', 'crmDealType']);
      let bsSubscription = await this.getSubscriptionForAccountId(params.accountId);

      const bsCustomerMetadata = await this.billingClient.getMetadataForCustomerWithId(bsSubscription.customer.id); // TODO: Move this call into billingAdapter
      const crmCompany = await this.crmClient.getCompanyById(bsCustomerMetadata.hubspot_company_id.trim());
      const crmContact = await this.crmClient.getContactByEmail(params.email);
      const result = {};

      if (bsSubscription.reference) {
        result.crmDeal = await this.crmClient.getDealById(bsSubscription.reference);
      }

      if (!result.crmDeal || (result.crmDeal.status && result.crmDeal.status === 'error')) {
        const dealParams = {
          ixc_account_id: params.accountId,
          dealname: `${bsSubscription.customer.organization} : ${bsSubscription.customer.first_name} ${bsSubscription.customer.last_name}`,
          dealtype: params.crmDealType,
          pipeline: params.crmPipelineName,
          dealstage: params.crmPipelineStage,
          trial_type: params.crmDealTrialType,
          ix_chargify_id: bsSubscription.id,
          ix_chargify_link: this.billingClient.getVendorUrlForSubscriptionWithId(bsSubscription.id),
        };

        // If this is a time bound trial, the trial_ended_at value will be set
        if (bsSubscription.trial_ended_at) {
          const cd = this.crmClient.dateInCrmFormat(bsSubscription.trial_ended_at);
          dealParams.trial_started_date = this.crmClient.dateInCrmFormat(bsSubscription.trial_started_at);
          dealParams.trial_end_date = cd;
          dealParams.closedate = cd;
        }
        // console.log('subscriptionWorker.registerSalesAcceptedLead.dealParams', dealParams);
        result.crmDeal = await this.crmClient.createDeal(dealParams);
      }

      // Create the CRM Contact to the CRM Deal
      await this.crmClient.addContactToDeal({
        contactVid: crmContact.crmId,
        dealId: result.crmDeal.crmId,
      });

      // Create the CRM Company to the CRM Deal
      await this.crmClient.addCompanyToDeal({
        companyId: crmCompany.crmId,
        dealId: result.crmDeal.crmId,
      });

      bsSubscription = await this.billingClient.updateSubscription({
        id: bsSubscription.id,
        // update reference to be the crm deal
        reference: result.crmDeal.crmId,
      });
      // set the metafield for the hubspot deal id on the chargify (or whatever) subscription
      await this.billingClient.updateSubscriptionCustomFields({
        subscriptionId: bsSubscription.id,
        changes: {
          name: 'hubspot_deal_id',
          value: result.crmDeal.crmId,
        },
      });
      result.success = true;
      return result;
    } catch (error) {
      this.logError(error, `registerSalesAcceptedLead failed`);
      throw error;
    }
  }

  /**
   * Advance an existing trial to a new stage in the CRM pipeline
   *
   * @param  {String} accountId - The IXC account's id
   * @param  {String} stageName - Data to apply
   * @returns {Promise<Object>} - Returns { success: true }
   */
  async updateDealStage(params) {
    this.logger.debug(`subscriptionWorker.updateDealStage`, params);
    try {
      // Validate required fields
      validate.required(params, ['accountId', 'stageName']);
      const { accountId, stageName } = params;

      // Prepare a result object
      const result = { input: { accountId, stageName } };

      // Get the subscription
      const bsSubscription = await this.getSubscriptionForAccountId(accountId);

      // Update crmDeal
      const crmDeal = await this.crmClient.updateDeal({
        id: bsSubscription.reference, // reference is set to same value as hubspot_deal_id
        dealstage: stageName,
      });
      result.crmDeal = crmDeal;
      result.success = true;

      return result;
    } catch (error) {
      this.logError(error, `updateDealStage failed`);
      throw error;
    }
  }

  /**
   * Update the User and Account details on record in all back office systems
   *
   * @param  {String} params
   * @prop  {String} accountId - The IXC account's id
   * @prop  {String} firstName - The last name of the IXC Account owner
   * @prop  {String} lastName - The first name of the IXC Account owner
   * @prop  {String} orgName - The name of the IXC Account
   * @returns {Promise<Object>} - Returns { success: true, bsCustomer, crmContact, crmCompany, crmDeal }
   */
  async updateRegistrationDetails(params) {
    this.logger.debug(`subscriptionWorker.updateRegistrationDetails`, params);
    try {
      // Validate required fields
      validate.required(params, ['accountId', 'orgName']);
      const { user, accountId, email, firstName, lastName, orgName, phoneNumber } = params;

      // Prepare a result object
      const result = { input: { accountId } };

      // Get the subscription, crmDeal, and crmCompany records
      const bsSubscription = await this.getSubscriptionForAccountId(accountId);
      const bsCustomerMetadata = await this.billingClient.getMetadataForCustomerWithId(bsSubscription.customer.id); // TODO: Move this call into billingAdapter

      // Get crmDealId
      const crmDealId = bsSubscription.reference || bsCustomerMetadata.hubspot_deal_id;

      let crmCompany = await this.crmClient.getCompanyById(bsCustomerMetadata.hubspot_company_id.trim());
      let crmContact = await this.crmClient.getContactByEmail(email);

      let bsCustomer = await this.billingClient.getCustomerByReference(accountId);

      const bsCustomerInput = {
        id: bsSubscription.customer.id,
        organization: orgName,
      };

      const crmContactInput = {
        id: crmContact.crmId,
      };

      const crmCompanyInput = {
        id: crmCompany.crmId,
        name: orgName,
        // If the existing company's name is its domain name, update it with the new value (because they're completing their registration)
        updateAllNameFields: crmCompany.name.toLowerCase() === crmCompany.domain.toLowerCase(),
      };

      const crmDealInput = {
        id: crmDealId,
      };

      if (firstName) {
        bsCustomerInput.first_name = firstName;
        crmContactInput.firstName = firstName;
      }

      if (lastName) {
        bsCustomerInput.last_name = lastName;
        crmContactInput.lastName = lastName;
        crmDealInput.dealname = `${orgName} : ${firstName} ${lastName}`;
      }

      if (phoneNumber) {
        bsCustomer.phone = phoneNumber;
        crmContactInput.phone = phoneNumber;
        crmCompanyInput.phone = phoneNumber;
        bsCustomerInput.phoneNumber = phoneNumber;
      }

      // Update the Billing Customer
      bsCustomer = await this.billingClient.updateCustomer(bsCustomerInput);
      result.bsCustomer = bsCustomer;

      // Update the CRM Contact
      crmContact = await this.crmClient.updateContact(crmContactInput);
      result.crmContact = crmContact;

      // Update the CRM Company
      crmCompany = await this.crmClient.updateCompany(crmCompanyInput);
      result.crmCompany = crmCompany;

      // Update the CRM Deal
      result.crmDeal = await this.crmClient.updateDeal(crmDealInput);

      // Record the event on all timelines
      this.recordTimelineEvent({
        currentUser: user,
        crmContactId: crmContact.crmId,
        crmCompanyId: crmCompany.crmId,
        crmDealId,
        eventName: 'IXC Account Registration Finished',
        eventData: {
          // Tokens
          ixcAccountId: accountId,
          accountName: orgName,
          ixcUserId: user.id,
          userName: `${firstName} ${lastName}`,
          userEmail: email,
          userPhone: phoneNumber,
          // extraData
          subscription: bsSubscription,
        },
      });

      result.success = true;
      return result;
    } catch (error) {
      this.logError(error, `subscriptionWorker.updateRegistrationDetails failed`);
      throw error;
    }
  }

  /**
   * Update the Account details on record in all back office systems
   *
   * @param  {String} params
   * @prop  {String} accountId - The IXC account's id
   * @prop  {String} orgName - The name of the IXC Account
   * @prop  {String} orgPhone
   * @prop  {String} orgStreet
   * @prop  {String} orgStreet2
   * @prop  {String} orgCity
   * @prop  {String} orgRegion
   * @prop  {String} orgPostalcode
   * @prop  {String} orgCountry
   * @returns {Promise<Object>} - Returns { success: true, bsCustomer, crmContact, crmCompany, crmDeal }
   */
  async updateAccountDetails(params) {
    this.logger.debug(`subscriptionWorker.updateAccountDetails`, params);
    try {
      // Validate required fields
      validate.required(params, ['accountId', 'orgName']);
      const { accountId, firstName, lastName, orgName, orgPhone, orgStreet, orgStreet2, orgCity, orgPostalcode, orgRegion, orgCountry } = params;

      // Prepare a result object
      const result = { input: { accountId } };

      // Get the subscription, crmDeal, and crmCompany records
      const bsSubscription = await this.getSubscriptionForAccountId(accountId);
      const bsCustomerMetadata = await this.billingClient.getMetadataForCustomerWithId(bsSubscription.customer.id); // TODO: Move this call into billingAdapter
      let crmCompany = await this.crmClient.getCompanyById(bsCustomerMetadata.hubspot_company_id.trim()); // this is coming back with a tab in front for some reason

      // Update billingCustomer organization so invoices reflect name change
      const bsCustomerProps = {
        id: bsSubscription.customer.id,
        organization: orgName,
      };
      if (firstName) bsCustomerProps.first_name = firstName;
      if (lastName) bsCustomerProps.last_name = lastName;
      if (orgName) bsCustomerProps.name = orgName;
      if (orgPhone) bsCustomerProps.phone = orgPhone;
      if (orgStreet) bsCustomerProps.address = orgStreet;
      if (orgStreet2) bsCustomerProps.address_2 = orgStreet2;
      if (orgCity) bsCustomerProps.city = orgCity;
      if (orgPostalcode) bsCustomerProps.zip = orgPostalcode;
      if (orgRegion) bsCustomerProps.state_name = orgRegion.replace(/_/g, ' ');
      if (orgCountry) bsCustomerProps.country_name = orgCountry.replace(/_/g, ' ');
      const bsCustomer = await this.billingClient.updateCustomer(bsCustomerProps);
      result.bsCustomer = bsCustomer;

      // Update orgName in crmCompany
      const crmCompanyProps = {
        id: crmCompany.crmId,
      };
      // TODO: Field mapping should occur inside the adapter
      // NOTE: Sales doesn't want their edits to the HS Account so update ix_account_name instead of name
      if (orgName) crmCompanyProps.name = orgName;
      if (orgPhone) crmCompanyProps.phone = orgPhone;
      if (orgStreet) crmCompanyProps.address = orgStreet;
      if (orgStreet2) crmCompanyProps.address2 = orgStreet2;
      if (orgCity) crmCompanyProps.city = orgCity;
      if (orgPostalcode) crmCompanyProps.zip = orgPostalcode;
      if (orgRegion) crmCompanyProps.state = orgRegion.replace(/_/g, ' ');
      if (orgCountry) crmCompanyProps.country = orgCountry.replace(/_/g, ' ');
      // If the existing company's name is its domain name, update it with the new value (because they're completing their registration)
      crmCompanyProps.updateAllNameFields = crmCompany.name.toLowerCase() === crmCompany.domain.toLowerCase();
      crmCompany = await this.crmClient.updateCompany(crmCompanyProps);
      result.crmCompany = crmCompany;

      let crmDeal;
      // TODO: Must check for existence of an account
      if (bsSubscription.reference || bsCustomerMetadata.hubspot_deal_id) crmDeal = await this.crmClient.getDealById(bsSubscription.reference || bsCustomerMetadata.hubspot_deal_id);

      if (crmDeal && crmDeal.status !== 'error') {
        // Update crmDeal name, which starts with orgName
        let newDealName = crmDeal.dealname;
        try {
          const [, contactName] = crmDeal.dealname.split(' : ');
          newDealName = `${orgName} : ${contactName}`;
        } catch (error) {
          this.logError(error);
        }
        crmDeal = await this.crmClient.updateDeal({
          id: bsSubscription.reference, // reference is set to same value as hubspot_deal_id
          dealname: newDealName,
        });
        result.crmDeal = crmDeal;
      }

      result.success = true;
      return result;
    } catch (error) {
      this.logError(error, `subscriptionWorker.updateAccountDetails failed`);
      throw error;
    }
  }

  /**
   * Helper function that extracts and flattens fields from IX user and account objects
   *
   * @param {Object} user - The applications user record
   * @param {Object} account - The applications account record
   * @returns {<Object>} - A single JS object containing the user and account details
   */
  static extractAppContext({ user, account }) {
    const data = {
      userId: user.id,
      accountId: account.id,
      firstName: user.firstName,
      lastName: user.lastName,
      email: user.email,
      phone: user.profile ? user.profile.phoneNumber : undefined,
      roles: user.roles?.toString() || '',
      orgName: account.orgName,
      orgStreet: account.orgStreet,
      orgStreet2: account.orgStreet2,
      orgCity: account.orgCity,
      orgRegion: account.orgRegion,
      orgPostalcode: account.orgPostalcode,
      orgCountry: account.orgCountry,
    };
    if (user.email) {
      data.domain = user.email.split('@')[1];
    }
    return data;
  }

  /**
   * Helper method that extracts the CRM and Payments system IDs from the current user record
   *
   * @param  {String} params
   * @returns {Promise<Object>} - A object containing all of the response objects from the CRM and billing systems
   */
  async lookupBackOfficeIDs(currentUser) {
    const ixcUserId = currentUser.id;
    let ixcAccountId;
    if (currentUser.account && currentUser.account.id) {
      ixcAccountId = currentUser.account.id;
    } else if (currentUser.accountId) {
      ixcAccountId = currentUser.accountId;
    }
    let bsSubscriptionId;
    let bsCustomerId;
    let crmContactId;
    let crmCompanyId;
    let crmDealId;

    // CRM contact.id
    if (currentUser.crmContactId) {
      crmContactId = currentUser.crmContactId;
    } else {
      const crmContact = await this.crmClient.getContactByEmail(currentUser.email);
      crmContactId = crmContact.crmId;
      // Save in local DB to prevent API call in the future
      await UserRepository.updateUserOnly(
        ixcUserId,
        {
          crmContactId,
        },
        {
          currentUser,
          language: 'en',
          userOnly: true,
        }
      );
    }

    if (!currentUser.subscription) {
      // TODO: Synchronize subscription here?
      this.logger.debug('lookupBackOfficeIDs currentUser.subscription was null. TODO: Synchronize subscription here.');
    } else {
      // Chargify subscription.id
      if (currentUser.subscription.paymentSystemId) bsSubscriptionId = currentUser.subscription.paymentSystemId;
      // Chargify customer.id
      if (currentUser.subscription.customerId) bsCustomerId = currentUser.subscription.customerId;
      // CRM company.id
      if (currentUser.subscription.companyId) crmCompanyId = currentUser.subscription.companyId;
      // CRM deal.id
      if (currentUser.subscription.crmDealId) crmDealId = currentUser.subscription.crmDealId;
    }
    this.logger.debug('lookupBackOfficeIDs IDs after unpacking currentUser.subscription', {
      ixcUserId,
      ixcAccountId,
      bsSubscriptionId,
      bsCustomerId,
      crmContactId,
      crmCompanyId,
      crmDealId,
    });

    // If any of the values are not present, look them up and save them locally
    if (!bsSubscriptionId || !bsCustomerId || !crmCompanyId || !crmDealId) {
      // Get the CRM company
      const bsSubscription = await this.getSubscriptionForAccountId(ixcAccountId);
      bsSubscriptionId = bsSubscription.id;
      bsCustomerId = bsSubscription.customer.id;
      crmDealId = bsSubscription.reference;
      // Get the CRM company
      if (!crmCompanyId) {
        const bsCustomerMetadata = await this.billingClient.getMetadataForCustomerWithId(bsSubscription.customer.id); // TODO: Move this call into billingAdapter
        crmCompanyId = bsCustomerMetadata.hubspot_company_id.trim();
        // const crmCompany = await this.crmClient.getCompanyById(bsCustomerMetadata.hubspot_company_id.trim());
        // crmCompanyId = crmCompany.crmId;
      }
      // TODO: Append subscription here
    }
    this.logger.debug('lookupBackOfficeIDs final returned values', {
      ixcUserId,
      ixcAccountId,
      bsSubscriptionId,
      bsCustomerId,
      crmContactId,
      crmCompanyId,
      crmDealId,
    });
    return {
      ixcUserId,
      ixcAccountId,
      bsSubscriptionId,
      bsCustomerId,
      crmContactId,
      crmCompanyId,
      crmDealId,
    };
  }

  /**
   * Return the billing system's subscription for the given app Account ID
   *
   * @param  {String} accountId - The IXC account's id
   * @returns {Promise<Object>} - The Billing System's Paid Subscription
   */
  async getSubscriptionForId(subscriptionId) {
    try {
      const bsSubscription = await this.billingClient.getSubscriptionById(subscriptionId);
      if (!bsSubscription) {
        this.logger.warn(`getSubscriptionForId did not find a subscription for ${subscriptionId}`);
        return null;
      }

      bsSubscription.components = await this.billingClient.getSubscriptionComponents(subscriptionId);
      const customFields = await this.billingClient.getSubscriptionCustomFields(subscriptionId);
      if (customFields) {
        for (let index = 0; index < customFields.length; index++) {
          const field = customFields[index];
          bsSubscription[field.name] = field.value;
        }
      }

      return bsSubscription;
    } catch (error) {
      this.logError(error, `getSubscriptionForId failed`);
      throw error;
    }
  }

  /**
   * Return the billing system's subscription for the given app Account ID
   *
   * @param  {String} accountId - The IXC account's id
   * * @param  {String} handle - The IXC account's id
   * @returns {Promise<Object>} - The Billing System's Paid Subscription
   */
  async getSubscriptionForAccountId(accountId, handle = null) {
    try {
      let bsSubscription = await this.getSubscriptionsForAccountId(accountId);
      if (!bsSubscription || bsSubscription.length <= 0) {
        this.logger.warn(`getSubscriptionForAccountId did not find a subscription for ${accountId}`);
        return null;
      }

      // If a handle is provided, return it
      if (handle) {
        bsSubscription = bsSubscription.filter((subscription) => {
          // TODO: Should we also be matching on components or expiration date?
          // This should match ix_standard regardless of whether it was created
          // by ix3_freemium or a converted ix_standard
          // subscription.state ="trialing"
          return subscription.product.handle === handle; // 'ix_standard'
        });
      }
      if (bsSubscription.length > 1) {
        this.logger.warn(`getSubscriptionForAccountId found more than one subscription found for ${accountId}`);
      }
      if (!bsSubscription || bsSubscription.length <= 0) {
        this.logger.warn(`getSubscriptionForAccountId did not find subscription for ${accountId}`);
        return null;
      }

      const [result] = bsSubscription;
      result.components = await this.billingClient.getSubscriptionComponents(result.id);
      const customFields = await this.billingClient.getSubscriptionCustomFields(result.id);
      if (customFields) {
        for (let index = 0; index < customFields.length; index++) {
          const field = customFields[index];
          result[field.name] = field.value;
        }
      }

      return result;
    } catch (error) {
      this.logError(error, `getSubscriptionForAccountId failed`);
      throw error;
    }
  }

  /**
   * Get all subscriptions belonging to the given IXC app Account ID
   *
   * @param  {String} accountId - The IXC account's id
   * @returns {Promise<Object>} - An array of subscription objects from the Billing System
   */
  // TODO: This functionality is duplicated in the billing adapter. It should live there.
  async getSubscriptionsForAccountId(accountId) {
    try {
      // Lookup Customer by reference
      const bsCustomer = await this.billingClient.getCustomerByReference(accountId);
      if (!bsCustomer) {
        this.logger.warn(`getSubscriptionsForAccountId did not find a bsCustomer for ${accountId}`);
        return null;
      }

      // Lookup and return the subscriptions
      let sub = await this.billingClient.getSubscriptionsForCustomer(bsCustomer.id);
      if (!sub) {
        this.logger.warn(`getSubscriptionsForAccountId did not subscriptions for customer ${bsCustomer.id}`);
        return null;
      }

      // Don't import legacy subscription
      sub = sub.filter((s) => s.product && s.product.product_family && s.product.product_family.handle === this.productFamilyHandle);

      for (let index = 0; index < sub.length; index++) {
        const s = sub[index];
        if (s && !s.current_billing_amount_in_cents) {
          const fullSub = await this.billingClient.getSubscriptionById(s.id);
          return [{ ...s, ...fullSub }];
        }
      }
      return sub;
    } catch (error) {
      this.logError(error, `getSubscriptionsForAccountId failed`);
      throw error;
    }
  }

  /**
   * Retrieves the latest product catalog and subscription info for the account
   *
   * @param {String} accountId - The IXC Account ID
   * @returns {Object} - An object containing both the current product catalog and the users current subscription data
   */
  async getBillingInitData(accountId) {
    try {
      // Get Chargify Tokens
      const data = this.billingClient.getInitData(accountId);

      // Get Up to Date Product Catalog
      const catalog = await this.fetchCatalog();
      data.catalog = JSON.stringify(catalog);

      if (!config.isStageHost) {
        // Get Up to Date Subscription
        data.subscription = await this.getSubscriptionForAccountId(accountId);
      }
      return data;
    } catch (error) {
      this.logError(error, `getBillingInitData failed`);
      throw error;
    }
  }

  /**
   * Billing systems and sales system can be setup to track MRR or ARR
   *
   * @param  {Number} subscriptionAmount - The IXC account's id
   * @returns {Number} - The value of a deal adjusted for whether CRM and Billing Systems track arr or mrr
   */
  calculateDealValue(subscriptionAmount, subscriptionArr = this.billingClient.subscription_value_is_arr) {
    let value = subscriptionAmount;
    if (this.crmClient.deal_value_is_arr && !subscriptionArr) {
      value = subscriptionAmount * 12;
    } else if (!this.crmClient.deal_value_is_arr && subscriptionArr) {
      value = subscriptionAmount / 12;
    }
    return value;
  }

  /*---------------------------------------------------------------------------------*/
  //                            Timeline Event Management
  /*---------------------------------------------------------------------------------*/

  /**
   * Register an event in the Timeline of CRM objects
   * TODO: Record same events in Billing System Notes system ???
   *
   * @param  {String} params
   * @prop  {String} currentUser
   * @prop  {String} eventName - A valid CRM Timeline Event Type Name (case sensitive). See backend/src/clients/serviceAdapters/timelineEventTypes.
   * @prop  {String} tokens - A JS object containing the values of fields expected by the CRM Timeline Event Type template
   * @prop  {String} extraData - a JS object containing any data that should be stored on the CRM event object
   * @returns {Promise<Object>} - A object containing all of the response objects from the CRM and billing systems
   */
  async recordTimelineEvent(params) {
    this.logger.debug(`subscriptionWorker.recordTimelineEvent`, params);
    try {
      // Prepare a result object
      const result = { input: { ...params } };

      validate.required(params, ['currentUser', 'eventName', 'eventData']);
      const { currentUser, eventName, eventData, crmContactId, crmCompanyId, crmDealId } = params;

      // If IDs are not provided but the currentUser is, then look them up in the currentUser record
      let ids;
      if ((!crmContactId || !crmCompanyId || !crmDealId) && currentUser) {
        ids = await this.lookupBackOfficeIDs(currentUser);
      }
      result.ids = ids;

      // Record timeline event
      result.response = await this.crmClient.recordTimelineEvent({
        eventName,
        eventData,
        contactId: crmContactId,
        companyId: crmCompanyId,
        dealId: crmDealId,
      });

      result.success = true;
      return result;
    } catch (error) {
      this.logError(error, `recordTimelineEvent failed`);
      throw error;
    }
  }

  /*---------------------------------------------------------------------------------*/
  //                                    Helpers
  /*---------------------------------------------------------------------------------*/

  /**
   * Log an error, ensuring that axios errors are properly handled.
   *
   * @private
   * @param {Error} error - The error object
   * @param {String} message - The message to include
   */
  logError(error, message) {
    if (error && error.response && error.response.data && error.response.data.errors) {
      this.logger.error(`${message} ${error.response.data.errors}`);
      log.error(error.response.data.errors);
    } else {
      // This is a non-axios error
      // this.logger.info(message);
      this.logger.error(message, error);
      log.error(error);
    }
  }

  /**
   * Confirm the Product Family Exists in the Catalog
   *
   * @param {String} catalogKey - Product Family Handle in the Billing Client
   */
  validateCatalogKey(catalogKey) {
    if (!Object.prototype.hasOwnProperty.call(this.catalog, catalogKey)) {
      throw new Error(`The catalog key "${catalogKey}" does not exist in the product catalog.`);
    }
  }

  /**
   * Confirm the Component Exists in the Catalog
   *
   * @param {String} componentId - Component ID in the Billing Client
   */
  validateComponentId(catalogKey, componentId) {
    const found = Object.values(this.catalog[catalogKey].components).find((comp) => comp.id === componentId);
    if (!found) {
      throw new Error(`The component key "${componentId}" does not exist in the product catalog.`);
    }
  }

  /**
   * Confirm the Component Exists in the Catalog
   *
   * @param {String} componentHandle - Component Handle in the Billing Client
   */
  validateComponentHandle(catalogKey, componentHandle) {
    if (!Object.prototype.hasOwnProperty.call(this.catalog[catalogKey].components, componentHandle)) {
      throw new Error(`The component handle "${componentHandle}" does not exist in the product catalog.`);
    }
  }

  /**
   * Confirm the Product Exists in the Catalog
   *
   * @param {String} productHandle - Product Handle in the Billing Client
   */
  validateProductHandle(catalogKey, productHandle) {
    if (!Object.prototype.hasOwnProperty.call(this.catalog[catalogKey].products, productHandle)) {
      throw new Error(`The product handle "${productHandle}" does not exist in the product catalog.`);
    }
  }

  /**
   *
   * @param {*} paymentMethod
   * @returns
   */
  hasActivePaymentMethod(paymentMethod) {
    log.debug('subscriptionWorker.paymentMethod');
    if (!paymentMethod) return false;
    const method = paymentMethod.update || paymentMethod;
    if (!method || !method.credit_card || method.credit_card.disabled || !method.credit_card.masked_card_number) return false;
    return true;
  }
};
