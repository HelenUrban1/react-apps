const dotenv = require('dotenv');

dotenv.config({ path: '../.env' });

const HubspotCRMAdapter = require('./serviceAdapters/hubspotCRMAdapter');
const ChargifyBillingAdapter = require('./serviceAdapters/chargifyBillingAdapter');
const SubscriptionWorker = require('./subscriptionWorker');

// Data submitted by form on SignupPage.jsx to doRegisterAccountAndUser
const freemiumSignupPageFormData = {
  user: {
    id: 'e0a01779-c8ed-4090-bd44-9c1b2242fb75',
    firstName: 'Freemium',
    lastName: 'Monkey',
    fullName: null,
    email: 'freemium.monkey@freemium.com',
    emailVerified: true,
    phoneNumber: '123-456-7890',
    avatars: null,
    paid: true,
    accountId: 'e01769a0-c8ed-4090-bd24-41fb759c1b22',
    accountName: 'Freemium Corp',
    activeAccountMemberId: '007516cc-3168-40e4-8ad3-d42c92f66caa',
    siteId: '1d5631ef-1b86-4ffa-b886-f021c5507ed8',
    siteName: null,
    roles: ['Owner'],
    accountMemberships: [
      {
        id: '007516cc-3168-40e4-8ad3-d42c92f66caa',
        accountId: 'e01769a0-c8ed-4090-bd24-41fb759c1b22',
        roles: ['Owner'],
        status: 'Active',
      },
    ],
    authenticationUid: 'c7bf4eca-cd00-4d65-81a8-5d4da86ea824',
  },
  account: {
    orgName: 'Freemium Corp',
    orgPhone: '123-456-7890',
    orgEmail: 'freemium.monkey@freemium.com',
    orgStreet: '1 Glendwood Ave',
    orgStreet2: 'WeWork Floor 5',
    orgCity: 'Raleigh',
    orgRegion: 'NC',
    orgPostalcode: '27603',
    orgCountry: 'US',
    // Added by doRegisterAccountAndUser before calling AccountService.create
    status: 'Active',
    settings: '{}',
    dbHost: '',
    dbName: '',
    // Available only after doRegisterAccountAndUser
    id: 'e01769a0-c8ed-4090-bd24-41fb759c1b22',
  },
  // Created by doRegisterAccountAndUser after AccountService.create
  site: {
    id: '1d5631ef-1b86-4ffa-b886-f021c5507ed8',
    accountId: 'e01769a0-c8ed-4090-bd24-41fb759c1b22', // newAccount.id,
    status: 'Active',
    settings: '{}',
    siteName: 'Freemium Corp', // account.orgName,
    sitePhone: '123-456-7890', // account.orgPhone,
    siteEmail: 'freemium.monkey@freemium.com', // account.orgEmail,
    siteStreet: '1 Glendwood Ave', // account.orgStreet,
    siteStreet2: 'WeWork Floor 5', // account.orgStreet2,
    siteCity: 'Raleigh', // account.orgCity,
    siteRegion: 'NC', // account.orgRegion,
    sitePostalcode: '27603', // account.orgPostalcode,
    siteCountry: 'US', // account.orgCountry,
  },
};

// Data submitted by form on SignupPage.jsx to doRegisterAccountAndUser
const timeBoundSignupPageFormData = {
  user: {
    id: '1779e0a0-c8ed-9040-bd44-42fb759c1b22',
    firstName: 'Trial',
    lastName: 'Monkey',
    fullName: null,
    email: 'trial.monkey@trialcorp.com',
    emailVerified: true,
    phoneNumber: '456-789-0123',
    avatars: null,
    paid: true,
    accountId: 'e0a01769-c8ed-4090-bd24-9c1b2241fb75',
    accountName: 'Trial Corp',
    activeAccountMemberId: '007516cc-3168-40e4-8ad3-d42c92f66caa',
    siteId: '31ef1d56-1b86-4ffa-b886-507ed8f021c5',
    siteName: null,
    roles: ['Owner'],
    accountMemberships: [
      {
        id: '007516cc-3168-40e4-8ad3-d42c92f66caa',
        accountId: 'e0a01769-c8ed-4090-bd24-9c1b2241fb75',
        roles: ['Owner'],
        status: 'Active',
      },
    ],
    authenticationUid: 'c7bf4eca-cd00-4d65-81a8-5d4da86ea824',
  },
  account: {
    orgName: 'Trial Corp',
    orgPhone: '456-789-0123',
    orgEmail: 'trial.monkey@trialcorp.com',
    orgStreet: '1 Glendwood Ave',
    orgStreet2: 'WeWork Floor 5',
    orgCity: 'Raleigh',
    orgRegion: 'NC',
    orgPostalcode: '27603',
    orgCountry: 'US',
    // Added by doRegisterAccountAndUser before calling AccountService.create
    status: 'Active',
    settings: '{}',
    dbHost: '',
    dbName: '',
    // Available only after doRegisterAccountAndUser
    id: 'e0a01769-c8ed-4090-bd24-9c1b2241fb75',
  },
  // Created by doRegisterAccountAndUser after AccountService.create
  site: {
    id: '1769e0a0-4090-c8ed-bd24-41fb759c1b22',
    accountId: 'e0a01769-c8ed-4090-bd24-9c1b2241fb75', // newAccount.id,
    status: 'Active',
    settings: '{}',
    siteName: 'Trial Corp', // account.orgName,
    sitePhone: '456-789-0123', // account.orgPhone,
    siteEmail: 'trial.monkey@trialcorp.com', // account.orgEmail,
    siteStreet: '1 Glendwood Ave', // account.orgStreet,
    siteStreet2: 'WeWork Floor 5', // account.orgStreet2,
    siteCity: 'Raleigh', // account.orgCity,
    siteRegion: 'NC', // account.orgRegion,
    sitePostalcode: '27603', // account.orgPostalcode,
    siteCountry: 'US', // account.orgCountry,
  },
};

let result;

// Keypress handler that allows tester to advance through lifecycle one step at a time
const keypress = async () => {
  process.stdin.setRawMode(true);
  return new Promise((resolve) =>
    process.stdin.once('data', (data) => {
      const byteArray = [...data];
      if (byteArray.length > 0 && byteArray[0] === 3) {
        console.log('^C');
        process.exit(1);
      }
      process.stdin.setRawMode(false);
      resolve();
    })
  );
};

// Main loop of function
(async () => {
  // Create Real CRM Connection
  const crmClient = new HubspotCRMAdapter({
    privateAccessToken: process.env.IXC_HUBSPOT_PRIVATE_ACCESS_TOKEN,
    logger: console,
  });

  // Create Real Billing Connection
  const billingClient = new ChargifyBillingAdapter({
    useAdapter: 'chargify',
    siteName: 'inspectionxpert-test',
    publicKey: process.env.IXC_CHARGIFY_PUBLIC_KEY,
    privateKey: process.env.IXC_CHARGIFY_PRIVATE_KEY,
    apiKey: process.env.IXC_CHARGIFY_API_KEY,
    logger: console,
  });

  // Create Subscription worker using live APIs
  const subWorker = new SubscriptionWorker({
    crmClient,
    billingClient,
    logger: console,
  });

  console.log('SubscriptionWorker initalized.');
  console.log('Open a browser to the following urls:');
  console.log(`  https://inspectionxpert-test.chargify.com/subscriptions`);
  console.log(`  https://app.hubspot.com/contacts/8445733/deals/board/view/all/`);
  console.log(`Press ctrl+C at any time to exit.`);
  console.log(`Press any other key to continue.`);
  await keypress();

  console.log('// ----------------------------- Freemium Trials ----------------------------- /');
  console.log('Press any key to continue with the Freemium subscription lifecycle.');

  // Create a freemium
  console.log('Creating Freemium subscription using createFreemium()');
  result = await subWorker.createFreemium(freemiumSignupPageFormData);
  const freemiumDealId = result.crmDeal.crmId;
  console.log('Done. Verify that the freemium deal was created in hubspot and chargify.');
  await keypress();

  // Move the freemium through the pipeline
  console.log('Moving the Freemium subscription across pipeline using updateDealStage()');
  result = await subWorker.updateDealStage({
    accountId: freemiumSignupPageFormData.account.id,
    // stageName: 'TRIAL REQUESTED'
    stageName: 'FIRST LOGIN',
    // stageName: 'UPLOADED DRAWING 1'
    // stageName: 'UPLOADED DRAWING 2'
    // stageName: 'UPLOADED DRAWING 3'
    // stageName: 'UPLOADED DRAWING 4'
    // stageName: 'UPLOADED DRAWING 5'
    // stageName: 'ATTEMPTED UPLOAD DRAWING 6'
    // stageName: 'CLOSED WON'
    // stageName: 'CLOSED LOST'
  });
  console.log('Done. Verify freemium trial is at stage FIRST LOGIN');
  await keypress();

  // Move the freemium through the pipeline
  console.log('Move Freemium subscription across pipeline using updateDealStage() to UPLOADED DRAWING 1');
  result = await subWorker.updateDealStage({
    accountId: freemiumSignupPageFormData.account.id,
    stageName: 'UPLOADED DRAWING 1',
  });
  console.log('Done. Verify freemium trial is at stage UPLOADED DRAWING 1');
  await keypress();

  // Move the freemium through the pipeline
  console.log('Move Freemium subscription across pipeline using updateDealStage() to UPLOADED DRAWING 5');
  result = await subWorker.updateDealStage({
    accountId: freemiumSignupPageFormData.account.id,
    stageName: 'UPLOADED DRAWING 5',
  });
  console.log('Done. Verify freemium trial is at stage UPLOADED DRAWING 5');
  await keypress();

  // Update the quantity of metered components (over 5 results in a closed won deal and fee)
  console.log('Update the quantity of metered components using updateFreemiumQuantity() Note: over 5 results in a closed won deal and fee.');
  result = await subWorker.updateFreemiumQuantity({
    accountId: freemiumSignupPageFormData.account.id,
    quantity: 6,
  });
  console.log('Done. Verify freemium trial drawings quantity is 6. The CRM deal should be closed won.');
  await keypress();
  console.log('Freemium trial lifecycle complete.');

  // ----------------------------- Time Bound Trials ----------------------------- /

  console.log('// ----------------------------- Time Bound Trials ----------------------------- /');
  console.log('Press any key to continue with the Time Bound subscription lifecycle.');
  await keypress();

  console.log('Creating Time Bound subscription using createTrial()');
  result = await subWorker.createTrial(timeBoundSignupPageFormData);
  const timeBoundTrialDealId = result.crmDeal.crmId;
  console.log('Done. Verify that the time bound trial was created.');
  await keypress();

  // Move the time bound trial through the pipeline
  console.log('Move time bound trial subscription using updateDealStage() to FIRST LOGIN');
  result = await subWorker.updateDealStage({
    accountId: timeBoundSignupPageFormData.account.id,
    // stageName: 'TRIAL REQUESTED'
    stageName: 'FIRST LOGIN',
    // stageName: 'DRAWING BALLOONED'
    // stageName: 'REPORT CREATED'
    // stageName: 'CLOSED WON'
    // stageName: 'CLOSED LOST'
  });
  console.log('Done. Verify time bound trial is at stage FIRST LOGIN');
  await keypress();

  // Move the time bound trial through the pipeline
  console.log('Move time bound trial subscription using updateDealStage() to DRAWING BALLOONED');
  result = await subWorker.updateDealStage({
    accountId: timeBoundSignupPageFormData.account.id,
    stageName: 'DRAWING BALLOONED',
  });
  console.log('Done. Verify time bound trial is at stage DRAWING BALLOONED');
  await keypress();

  // Move the time bound trial through the pipeline
  console.log('Move time bound trial subscription using updateDealStage() to REPORT CREATED');
  result = await subWorker.updateDealStage({
    accountId: timeBoundSignupPageFormData.account.id,
    stageName: 'REPORT CREATED',
  });
  console.log('Done. Verify time bound trial is at stage REPORT CREATED');
  await keypress();

  // Expire the time bound trial
  console.log('Extending the time bound trial subscription using extendTrial() to 2021-01-01');
  result = await subWorker.extendTrial({
    accountId: timeBoundSignupPageFormData.account.id,
    newEndDate: '2021-01-01', // YYYY-MM-DD
  });
  console.log('Done. Verify time bound trial will expire at 2021-01-01');
  await keypress();

  // Not expirting trial, because we want to convert it
  // console.log('Expire the time bound trial subscription using expireTrial()');
  // result = await subWorker.expireTrial({
  //   accountId: timeBoundSignupPageFormData.account.id,
  // });
  // console.log('Done. Verify time bound trial is expired and the CRM deal is closed lost.');
  // await keypress();

  // Convert the time bound trial to a paid subscription
  console.log('Convert the time bound trial to a paid subscription using convertTrialToSubscription()');
  result = await subWorker.convertTrialToSubscription({
    accountId: timeBoundSignupPageFormData.account.id,
    catalogKey: 'ix_standard',
  });
  console.log('Done. Verify time bound trial is a paid subscription (can take up to 30 minutes)');
  await keypress();
  console.log('Time bound trial lifecycle complete.');

  // ----------------------------- Time Bound Trials ----------------------------- /

  console.log('Press any key to delete the CRM deals. Chargify subscriptions must be deleted manually.');
  await keypress();

  // Convert the time bound trial to a paid subscription
  console.log('Deleting deals and associated contacts and companies');
  result = await crmClient.deleteDeal(freemiumDealId);
  result = await crmClient.deleteDeal(timeBoundTrialDealId);
  console.log('Done. Verify CRM deals and associated contacts and companies have been deleted.');
})().then(process.exit);
