const { SphinxClient, SIGNATURE_HEADER, API_KEY_HEADER, CONNECTION_ID_HEADER } = require('./sphinxClient');

const SHARED_KEY = 'a shared key';
const BASE_PATH = '/api/v1';
const ACCOUNT_ID = 'an-account-id';
const SITE_ID = 'a-site-id';
const USER_ID = 'a-user-id';
const API_KEY = 'an-API-key';
const CONNECTION_ID = 'a-connection-id';
const ERROR = new Error('an error');

const accountSiteUserIds = { accountId: ACCOUNT_ID, siteId: SITE_ID, userId: USER_ID };

const createMockConnection = () => {
  return {
    id: 'd41d5366-ec4b-48c6-915c-11da8d299f7d',
    apiKey: '22f8fd5e-23b0-445c-85cd-eb87bd8d273e',
    accountId: 'e9a916a6-b22f-4b6b-923d-4086864919df',
    userId: '16cd73f2-670a-4338-981b-bb60f8efcbba',
    siteId: 'e2c38a79-8dde-4004-bf67-0beb11cfc1c5',
    accountIdIndexSortKey: 'e2c38a79-8dde-4004-bf67-0beb11cfc1c5#16cd73f2-670a-4338-981b-bb60f8efcbba',
    sourceId: '5093e47f-d3aa-4dd3-b591-1543b298b365',
    destinationIds: ['aedaf945-8472-4624-acbf-4e8f9c9bc255'],
    destinationCredentials: [],
    enabled: true,
    created: '2021-01-07T21:54:24.749Z',
    updated: '2021-01-07T21:54:24.749Z',
  };
};

const createMockDefinitions = () => {
  return [
    {
      id: 'f3a59014-5b4d-4aec-8238-013a1b920864',
      type: 'source',
      name: 'IXOD',
      version: '2.0',
      enabled: true,
      created: '2020-08-30T17:28:17.522Z',
      updated: '2020-08-30T17:28:17.522Z',
    },
    {
      id: '2e6ed84b-4b68-4af9-92f3-7fcfa7728c4e',
      type: 'source',
      name: 'IXC',
      version: '1.0',
      enabled: true,
      created: '2020-08-30T17:28:17.522Z',
      updated: '2020-08-30T17:28:17.522Z',
    },
  ];
};

describe('SphinxClient', () => {
  let sphinxClient;
  let mockHttpClient;
  let mockHttpClientWrapper;
  let logErrorSpy;

  beforeEach(() => {
    mockHttpClient = {
      delete: jest.fn(),
      get: jest.fn(),
      post: jest.fn(),
      put: jest.fn(),
    };

    mockHttpClientWrapper = {
      create: () => mockHttpClient,
    };

    sphinxClient = new SphinxClient({
      sharedKey: SHARED_KEY,
      basePath: BASE_PATH,
      httpClient: mockHttpClientWrapper,
      logger: console,
    });

    logErrorSpy = jest.spyOn(sphinxClient, 'logError');
  });

  describe('#apiKeyRotate()', () => {
    it('should throw an error when not provided with required input', async () => {
      await expect(sphinxClient.apiKeyRotate()).rejects.toThrow(/Required param/);
      expect(logErrorSpy).toHaveBeenCalled();
    });

    it('should log and propagate an API request failure', async () => {
      mockHttpClient.delete.mockRejectedValue(ERROR);

      await expect(sphinxClient.apiKeyRotate({ apiKey: API_KEY })).rejects.toThrow(ERROR);
      expect(logErrorSpy).toHaveBeenCalled();
    });

    it('should return the response data', async () => {
      const newApiKey = '5fc54dd9-c625-4f01-8372-7456f1338c40';
      mockHttpClient.delete.mockResolvedValue({ data: { apiKey: newApiKey } });

      const result = await sphinxClient.apiKeyRotate({ apiKey: API_KEY });
      expect(result.apiKey).toBe(newApiKey);
      expect(logErrorSpy).not.toHaveBeenCalled();

      const [url, { headers }] = mockHttpClient.delete.mock.calls[0];
      expect(url.startsWith(BASE_PATH)).toBe(true);
      expect(url.includes(API_KEY)).toBe(true);
      expect(headers[SIGNATURE_HEADER]).toBeDefined();
    });
  });

  describe('#connectionsGet()', () => {
    it('should throw an error when not provided with required input', async () => {
      await expect(sphinxClient.connectionsGet()).rejects.toThrow(/Required param/);
      expect(logErrorSpy).toHaveBeenCalled();
    });

    it('should log and propagate an API request failure', async () => {
      mockHttpClient.get.mockRejectedValue(ERROR);

      await expect(sphinxClient.connectionsGet(accountSiteUserIds)).rejects.toThrow(ERROR);
      expect(logErrorSpy).toHaveBeenCalled();
    });

    it('should return the response data', async () => {
      const connection = createMockConnection();
      mockHttpClient.get.mockResolvedValue({ data: [connection] });

      const result = await sphinxClient.connectionsGet(accountSiteUserIds);
      expect(result).toEqual([connection]);
      expect(logErrorSpy).not.toHaveBeenCalled();

      const [url, { headers }] = mockHttpClient.get.mock.calls[0];
      expect(url.startsWith(BASE_PATH)).toBe(true);
      expect(url.includes(ACCOUNT_ID)).toBe(true);
      expect(url.includes(SITE_ID)).toBe(true);
      expect(url.includes(USER_ID)).toBe(true);
      expect(headers[SIGNATURE_HEADER]).toBeDefined();
    });
  });

  describe('#healthGet()', () => {
    it('should throw an error when not provided with required input', async () => {
      await expect(sphinxClient.healthGet()).rejects.toThrow(/Required param/);
      expect(logErrorSpy).toHaveBeenCalled();
    });

    it('should log and propagate an API request failure', async () => {
      mockHttpClient.get.mockRejectedValue(ERROR);

      await expect(sphinxClient.healthGet({ apiKey: 'an-id' })).rejects.toThrow(ERROR);
      expect(logErrorSpy).toHaveBeenCalled();
    });

    it('should return the response data', async () => {
      const connection = createMockConnection();
      mockHttpClient.get.mockResolvedValue({ data: 'OK' });

      const result = await sphinxClient.healthGet({ apiKey: connection.apiKey });
      expect(result).toEqual('OK');
      expect(logErrorSpy).not.toHaveBeenCalled();

      const [url, { headers }] = mockHttpClient.get.mock.calls[0];
      expect(url.startsWith(BASE_PATH)).toBe(true);
      expect(url.includes(connection.apiKey)).toBe(true);
      expect(headers[SIGNATURE_HEADER]).toBeDefined();
    });
  });

  describe('#connectionsGetById()', () => {
    it('should throw an error when not provided with required input', async () => {
      await expect(sphinxClient.connectionsGetById()).rejects.toThrow(/Required param/);
      expect(logErrorSpy).toHaveBeenCalled();
    });

    it('should log and propagate an API request failure', async () => {
      mockHttpClient.get.mockRejectedValue(ERROR);

      await expect(sphinxClient.connectionsGetById({ connectionId: 'an-id' })).rejects.toThrow(ERROR);
      expect(logErrorSpy).toHaveBeenCalled();
    });

    it('should return the response data', async () => {
      const connection = createMockConnection();
      mockHttpClient.get.mockResolvedValue({ data: [connection] });

      const result = await sphinxClient.connectionsGetById({ connectionId: connection.id });
      expect(result).toEqual([connection]);
      expect(logErrorSpy).not.toHaveBeenCalled();

      const [url, { headers }] = mockHttpClient.get.mock.calls[0];
      expect(url.startsWith(BASE_PATH)).toBe(true);
      expect(url.includes(connection.id)).toBe(true);
      expect(headers[SIGNATURE_HEADER]).toBeDefined();
    });
  });

  describe('#connectionsUpdate()', () => {
    it('should throw an error when not provided with required input', async () => {
      await expect(sphinxClient.connectionsUpdate()).rejects.toThrow(/Required param/);
      expect(logErrorSpy).toHaveBeenCalled();
    });

    it('should log and propagate an API request failure', async () => {
      mockHttpClient.put.mockRejectedValue(ERROR);

      const input = { connectionId: CONNECTION_ID, updateData: { enabled: false } };

      await expect(sphinxClient.connectionsUpdate(input)).rejects.toThrow(ERROR);
      expect(logErrorSpy).toHaveBeenCalled();
    });

    it('should return the response data', async () => {
      const connection = createMockConnection();
      mockHttpClient.put.mockResolvedValue({ data: connection });

      const input = { connectionId: CONNECTION_ID, updateData: { enabled: false } };

      const result = await sphinxClient.connectionsUpdate(input);
      expect(result).toEqual(connection);
      expect(logErrorSpy).not.toHaveBeenCalled();

      const [url, payload, { headers }] = mockHttpClient.put.mock.calls[0];
      expect(url.startsWith(BASE_PATH)).toBe(true);
      expect(payload).toEqual({ updateData: { enabled: false } });
      expect(headers[SIGNATURE_HEADER]).toBeDefined();
    });
  });

  describe('#connectionsCreate()', () => {
    it('should throw an error when not provided with required input', async () => {
      await expect(sphinxClient.connectionsCreate()).rejects.toThrow(/Required param/);
      expect(logErrorSpy).toHaveBeenCalled();
    });

    it('should log and propagate an API request failure', async () => {
      mockHttpClient.post.mockRejectedValue(ERROR);

      const input = { ...accountSiteUserIds, apiKey: API_KEY };

      await expect(sphinxClient.connectionsCreate(input)).rejects.toThrow(ERROR);
      expect(logErrorSpy).toHaveBeenCalled();
    });

    it('should return the response data', async () => {
      const connection = createMockConnection();
      mockHttpClient.post.mockResolvedValue({ data: connection });

      const input = { ...accountSiteUserIds, apiKey: API_KEY };

      const result = await sphinxClient.connectionsCreate(input);
      expect(result).toEqual(connection);
      expect(logErrorSpy).not.toHaveBeenCalled();

      const [url, payload, { headers }] = mockHttpClient.post.mock.calls[0];
      expect(url.startsWith(BASE_PATH)).toBe(true);
      expect(payload).toEqual(input);
      expect(headers[SIGNATURE_HEADER]).toBeDefined();
    });
  });

  describe('#connectionsDelete()', () => {
    it('should throw an error when not provided with required input', async () => {
      await expect(sphinxClient.connectionsDelete()).rejects.toThrow(/Required param/);
      expect(logErrorSpy).toHaveBeenCalled();
    });

    it('should log and propagate an API request failure', async () => {
      mockHttpClient.delete.mockRejectedValue(ERROR);

      await expect(sphinxClient.connectionsDelete({ connectionId: 'an-id' })).rejects.toThrow(ERROR);
      expect(logErrorSpy).toHaveBeenCalled();
    });

    it('should return the response data', async () => {
      const connection = createMockConnection();
      mockHttpClient.delete.mockResolvedValue(connection.id);

      const result = await sphinxClient.connectionsDelete({ connectionId: connection.id });
      expect(result).toEqual(connection.id);
      expect(logErrorSpy).not.toHaveBeenCalled();

      const [url, { headers }] = mockHttpClient.delete.mock.calls[0];
      expect(url.startsWith(BASE_PATH)).toBe(true);
      expect(url.includes(connection.id)).toBe(true);
      expect(headers[SIGNATURE_HEADER]).toBeDefined();
    });
  });

  describe('#definitionsGet()', () => {
    it('should log and propagate an API request failure', async () => {
      mockHttpClient.get.mockRejectedValue(ERROR);

      await expect(sphinxClient.definitionsGet()).rejects.toThrow(ERROR);
      expect(logErrorSpy).toHaveBeenCalled();
    });

    it('should return the response data', async () => {
      const definitions = createMockDefinitions();
      mockHttpClient.get.mockResolvedValue({ data: definitions });

      const result = await sphinxClient.definitionsGet();
      expect(result).toEqual(definitions);
      expect(logErrorSpy).not.toHaveBeenCalled();

      const [url, { headers }] = mockHttpClient.get.mock.calls[0];
      expect(url.startsWith(BASE_PATH)).toBe(true);
      expect(headers[SIGNATURE_HEADER]).toBeDefined();
    });
  });

  describe('#upload()', () => {
    const apiKey = 'api key';
    const connectionId = 'connection id';
    const reportText = 'report text';

    it('should throw an error when not provided with required input', async () => {
      await expect(sphinxClient.upload()).rejects.toThrow(/Required param/);
      expect(logErrorSpy).toHaveBeenCalled();

      await expect(sphinxClient.upload({ apiKey })).rejects.toThrow(/Required param/);
      expect(logErrorSpy).toHaveBeenCalled();

      await expect(sphinxClient.upload({ payload: reportText })).rejects.toThrow(/Required param/);
      expect(logErrorSpy).toHaveBeenCalled();
    });

    it('should log and propagate an API request failure', async () => {
      mockHttpClient.post.mockRejectedValue(ERROR);

      await expect(sphinxClient.upload({ apiKey, payload: reportText })).rejects.toThrow(ERROR);
      expect(logErrorSpy).toHaveBeenCalled();
    });

    it('should return the response data', async () => {
      mockHttpClient.post.mockResolvedValue({ data: { 'Net-Inspect': true } });

      const result = await sphinxClient.upload({ apiKey, payload: reportText });
      expect(logErrorSpy).not.toHaveBeenCalled();

      const [url, payload, { headers }] = mockHttpClient.post.mock.calls[0];
      expect(url.startsWith(BASE_PATH)).toBe(true);
      expect(payload).toEqual({ payload: reportText });
      expect(headers[SIGNATURE_HEADER]).toBeDefined();
      expect(headers[API_KEY_HEADER]).toBe(apiKey);
      expect(result['Net-Inspect']).toBe(true);
    });

    it('should use the connectionId if supplied', async () => {
      mockHttpClient.post.mockResolvedValue({ data: { 'Net-Inspect': true } });

      const result = await sphinxClient.upload({ apiKey, connectionId, payload: reportText });
      expect(logErrorSpy).not.toHaveBeenCalled();

      const [url, payload, { headers }] = mockHttpClient.post.mock.calls[0];
      expect(url.startsWith(BASE_PATH)).toBe(true);
      expect(payload).toEqual({ payload: reportText });
      expect(headers[SIGNATURE_HEADER]).toBeDefined();
      expect(headers[API_KEY_HEADER]).toBe(apiKey);
      expect(headers[CONNECTION_ID_HEADER]).toBe(connectionId);
      expect(result['Net-Inspect']).toBe(true);
    });
  });
});
