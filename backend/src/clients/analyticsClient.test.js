const AnalyticsClient = require('./analyticsClient');
const constants = require('./analyticsClientConstants');

const WRITE_KEY = 'a write key';
const USER_ID = 'a user id';
const ANONYMOUS_ID = '3694d233-64eb-4676-94ea-5e9e2802dac2';
const ERROR_MESSAGE = 'an error message';

describe('AnalyticsClient', () => {
  let analyticsClient;
  let MockSdk;
  let mockSdk;
  let onErrorSpy;

  beforeAll(() => {
    mockSdk = {
      track: jest.fn(),
      identify: jest.fn(),
      group: jest.fn(),
    };
    MockSdk = jest.fn(() => mockSdk);

    analyticsClient = new AnalyticsClient({
      writeKey: WRITE_KEY,
      ClientSdk: MockSdk,
    });
    onErrorSpy = jest.spyOn(analyticsClient, 'onError');
  });

  afterEach(() => {
    jest.resetAllMocks();
  });

  describe('Instantiation', () => {
    it('should pass the write key and target url to the SDK constructor', () => {
      const { mock } = MockSdk;
      expect(mock.calls.length).toBe(1);
      const input = mock.calls[0];
      expect(input[0]).toBe(WRITE_KEY);
    });

    it('should populate the `events` property', () => {
      expect(analyticsClient.events).toBeDefined();
      expect(Object.keys(analyticsClient.events).length).toBeGreaterThan(0);
    });
  });

  describe(`#identify()`, () => {
    it('should populate the anonymous id when neither the user id nor the anonymous id are set', () => {
      analyticsClient.identify({});
      const { mock } = mockSdk.identify;
      expect(mock.calls.length).toBe(1);
      expect(onErrorSpy.mock.calls.length).toBe(0);

      const input = mock.calls[0][0];
      expect(input.userId).toBeUndefined();
      expect(input.anonymousId).toBe(constants.UNDEFINED_ANONYMOUS_ID);
    });

    it('should use the anonymous id set in the request headers', () => {
      const headers = {};
      headers[constants.ANONYMOUS_ID_HEADER] = ANONYMOUS_ID;
      const mockReq = { headers };
      analyticsClient.identify({ req: mockReq });
      const { mock } = mockSdk.identify;
      expect(mock.calls.length).toBe(1);
      expect(onErrorSpy.mock.calls.length).toBe(0);

      const input = mock.calls[0][0];
      expect(input.userId).toBeUndefined();
      expect(input.anonymousId).toBe(ANONYMOUS_ID);
    });

    it('should pass through a user id', () => {
      analyticsClient.identify({ userId: USER_ID });
      const { mock } = mockSdk.identify;
      expect(mock.calls.length).toBe(1);
      expect(onErrorSpy.mock.calls.length).toBe(0);

      const input = mock.calls[0][0];
      expect(input.userId).toBe(USER_ID);
      expect(input.anonymousId).toBeUndefined();
    });

    it('should set common event metadata', () => {
      analyticsClient.identify();
      const { mock } = mockSdk.identify;
      expect(mock.calls.length).toBe(1);
      expect(onErrorSpy.mock.calls.length).toBe(0);

      const input = mock.calls[0][0];
      const metadataContainer = input.traits;
      expect(metadataContainer.eventVersion).toMatch(/\d\.\d\.\d/);
      expect(metadataContainer.source).toBe(constants.EVENT_SOURCE_BACKEND);
    });

    it('should remove custom params from the object passed to the SDK', () => {
      const mockReq = { headers: {} };
      analyticsClient.identify({ req: mockReq });
      const { mock } = mockSdk.identify;
      expect(mock.calls.length).toBe(1);
      expect(onErrorSpy.mock.calls.length).toBe(0);
      const input = mock.calls[0][0];
      expect(input.req).toBeUndefined();
      expect(input.ixcEvent).toBeUndefined();
    });
  });

  describe('Common tests across all analyticsClient methods', () => {
    ['track', 'group'].forEach((method) => {
      describe(`#${method}()`, () => {
        it('should catch an exception from the SDK call', () => {
          analyticsClient[method](); // Triggers an exception due to missing input.

          expect(mockSdk[method].mock.calls.length).toBe(0);
          expect(onErrorSpy.mock.calls.length).toBe(1);
        });

        it('should populate the anonymous id when neither the user id nor the anonymous id are set', () => {
          const { mock } = mockSdk[method];
          analyticsClient[method]({});
          expect(mock.calls.length).toBe(1);
          expect(onErrorSpy.mock.calls.length).toBe(0);

          const input = mock.calls[0][0];
          expect(input.userId).toBeUndefined();
          expect(input.anonymousId).toBe(constants.UNDEFINED_ANONYMOUS_ID);
        });

        it('should use the anonymous id set in the request headers', () => {
          const headers = {};
          headers[constants.ANONYMOUS_ID_HEADER] = ANONYMOUS_ID;
          const mockReq = { headers };
          analyticsClient[method]({ req: mockReq });
          const { mock } = mockSdk[method];
          expect(mock.calls.length).toBe(1);
          expect(onErrorSpy.mock.calls.length).toBe(0);

          const input = mock.calls[0][0];
          expect(input.userId).toBeUndefined();
          expect(input.anonymousId).toBe(ANONYMOUS_ID);
        });

        it('should pass through a user id', () => {
          analyticsClient[method]({ userId: USER_ID });
          const { mock } = mockSdk[method];
          expect(mock.calls.length).toBe(1);
          expect(onErrorSpy.mock.calls.length).toBe(0);

          const input = mock.calls[0][0];
          expect(input.userId).toBe(USER_ID);
          expect(input.anonymousId).toBeUndefined();
        });

        it('should set the event name and version', () => {
          const event = analyticsClient.events.errorsNonspecific;
          analyticsClient[method]({ ixcEvent: event });
          const { mock } = mockSdk[method];
          expect(mock.calls.length).toBe(1);
          expect(onErrorSpy.mock.calls.length).toBe(0);

          const input = mock.calls[0][0];
          expect(input.event).toBe(event.name);
          const metadataContainer = input.properties || input.traits;
          expect(metadataContainer.eventVersion).toBe(event.version);
        });

        it('should set common event metadata', () => {
          const event = analyticsClient.events.errorsNonspecific;
          analyticsClient[method]({ ixcEvent: event });
          const { mock } = mockSdk[method];
          expect(mock.calls.length).toBe(1);
          expect(onErrorSpy.mock.calls.length).toBe(0);

          const input = mock.calls[0][0];
          expect(input.event).toBe(event.name);
          const metadataContainer = input.properties || input.traits;
          expect(metadataContainer.eventVersion).toMatch(/\d\.\d\.\d/);
          expect(metadataContainer.source).toBe(constants.EVENT_SOURCE_BACKEND);
        });

        it('should remove custom params from the object passed to the SDK', () => {
          const mockReq = { headers: {} };
          const event = analyticsClient.events.errorsNonspecific;
          analyticsClient[method]({ ixcEvent: event, req: mockReq });
          const { mock } = mockSdk[method];
          expect(mock.calls.length).toBe(1);
          expect(onErrorSpy.mock.calls.length).toBe(0);

          const input = mock.calls[0][0];
          expect(input.req).toBeUndefined();
          expect(input.ixcEvent).toBeUndefined();
        });
      });
    });
  });

  describe('Event Attribute Extraction from Objects', () => {
    describe(`Errors`, () => {
      it('should include an error when provided', () => {
        const event = analyticsClient.events.errorsNonspecific;
        analyticsClient.track({
          ixcEvent: event,
          properties: {
            error: new Error(ERROR_MESSAGE),
          },
        });
        const { mock } = mockSdk.track;
        expect(mock.calls.length).toBe(1);
        expect(onErrorSpy.mock.calls.length).toBe(0);

        const input = mock.calls[0][0];
        expect(input.event).toBe(event.name);
        expect(input.properties.eventVersion).toBe(event.version);
        expect(input.properties.error.message).toBe(ERROR_MESSAGE);
      });
    });
  });
});
