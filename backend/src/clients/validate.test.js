const validate = require('./validate');

describe('Validate', () => {
  describe('#required', () => {
    const params = {
      one: 1,
      two: 2,
      three: 3,
    };

    it('should throw an error if all values are NOT present', () => {
      expect(() => {
        validate.required(params, ['one', 'two', 'three', 'four']);
      }).toThrow(Error);
    });

    it('should NOT throw an error if all values ARE present', async () => {
      expect(() => {
        validate.required(params, ['one', 'two']);
      }).not.toThrow(Error);
    });
  });
});
