const makeBillingClient = require('./billingClient').default;

// prettier-ignore
const {
  API_KEY,
  PUBLIC_KEY,
  PRIVATE_KEY,
  SITE_NAME,
  getHttpClientMock,
} = require('./serviceAdapters/fixtures/chargify.fixtures');

describe('billingClient', () => {
  let billingClient;
  const httpClientMock = getHttpClientMock();
  const loggerMock = {
    trace: jest.fn(),
    debug: jest.fn(),
    log: jest.fn(),
    info: jest.fn(),
    warn: jest.fn(),
    error: jest.fn(),
  };

  beforeEach(() => {
    jest.resetAllMocks();
  });

  describe('makeBillingClient', () => {
    it('should throw an error if required input is not provided', async () => {
      expect(() => {
        makeBillingClient({ logger: loggerMock });
      }).toThrow();
      expect(loggerMock.error).toHaveBeenCalledTimes(1);
    });

    it('should throw an error if an unrecognized adapter is requested', async () => {
      expect(() => {
        makeBillingClient({
          useAdapter: 'foo',
          apiKey: API_KEY,
          publicKey: PUBLIC_KEY,
          privateKey: PRIVATE_KEY,
          siteName: SITE_NAME,
          httpClient: httpClientMock,
          logger: loggerMock,
        });
      }).toThrow();
      expect(loggerMock.error).toHaveBeenCalledTimes(1);
    });

    it('should populate the result properly if given a valid adapter', async () => {
      billingClient = await makeBillingClient({
        useAdapter: 'chargify',
        apiKey: API_KEY,
        publicKey: PUBLIC_KEY,
        privateKey: PRIVATE_KEY,
        siteName: SITE_NAME,
        httpClient: httpClientMock,
        logger: loggerMock,
      });

      // Adapter metadata
      expect(billingClient.adapter).toBeDefined();
      expect(billingClient.adapter.name).toBe('chargify');
      expect(billingClient.adapter.isMocked).toBeDefined();
      // Get config from billing adapter?
      expect(billingClient.getInitData).toBeDefined();
      // Customers
      expect(billingClient.createOrUpdateCustomer).toBeDefined();
      expect(billingClient.createCustomer).toBeDefined();
      expect(billingClient.getCustomerById).toBeDefined();
      expect(billingClient.getCustomerByReference).toBeDefined();
      expect(billingClient.findCustomer).toBeDefined();
      expect(billingClient.updateCustomer).toBeDefined();
      // Subscriptions
      expect(billingClient.createSubscription).toBeDefined();
      expect(billingClient.getSubscriptionById).toBeDefined();
      expect(billingClient.getSubscriptionByReference).toBeDefined();
      expect(billingClient.getSubscriptionsForCustomer).toBeDefined();
      expect(billingClient.updateSubscription).toBeDefined();
      expect(billingClient.updateSubscriptionQuantity).toBeDefined();
      expect(billingClient.delayedCancelSubscription).toBeDefined();
      expect(billingClient.immediatelyCancelSubscription).toBeDefined();
      expect(billingClient.removeDelayedCancel).toBeDefined();
      expect(billingClient.reactivateSubscription).toBeDefined();
      // Payment Methods
      expect(billingClient.addPaymentMethod).toBeDefined();
      expect(billingClient.changeDefaultPaymentMethod).toBeDefined();
      // Payment History
      expect(billingClient.getInvoices).toBeDefined();
      expect(billingClient.downloadInvoice).toBeDefined();
    });

    it('should fall back to a valid adapter if one is not specified', async () => {
      billingClient = await makeBillingClient({
        apiKey: API_KEY,
        publicKey: PUBLIC_KEY,
        privateKey: PRIVATE_KEY,
        siteName: SITE_NAME,
        httpClient: httpClientMock,
        logger: loggerMock,
      });
      // Adapter metadata
      expect(billingClient.adapter).toBeDefined();
      expect(billingClient.adapter.name).toBe('chargify');
      expect(billingClient.adapter.isMocked).toBeDefined();
      // Get config from billing adapter?
      expect(billingClient.getInitData).toBeDefined();
      // Customers
      expect(billingClient.createOrUpdateCustomer).toBeDefined();
      expect(billingClient.createCustomer).toBeDefined();
      expect(billingClient.getCustomerById).toBeDefined();
      expect(billingClient.getCustomerByReference).toBeDefined();
      expect(billingClient.findCustomer).toBeDefined();
      expect(billingClient.updateCustomer).toBeDefined();
      // Subscriptions
      expect(billingClient.createSubscription).toBeDefined();
      expect(billingClient.getSubscriptionById).toBeDefined();
      expect(billingClient.getSubscriptionByReference).toBeDefined();
      expect(billingClient.getSubscriptionsForCustomer).toBeDefined();
      expect(billingClient.updateSubscription).toBeDefined();
      expect(billingClient.updateSubscriptionQuantity).toBeDefined();
      expect(billingClient.delayedCancelSubscription).toBeDefined();
      expect(billingClient.immediatelyCancelSubscription).toBeDefined();
      expect(billingClient.removeDelayedCancel).toBeDefined();
      expect(billingClient.reactivateSubscription).toBeDefined();
      // Payment Methods
      expect(billingClient.addPaymentMethod).toBeDefined();
      expect(billingClient.changeDefaultPaymentMethod).toBeDefined();
      // Payment History
      expect(billingClient.getInvoices).toBeDefined();
      expect(billingClient.downloadInvoice).toBeDefined();
    });
  });
});
