const { CognitoIdentityProviderClient } = require('@aws-sdk/client-cognito-identity-provider');

/**
 * cognitoClient provides the application with interface to Cognito
 *
 * @param {Object} params
 * @prop  {Object} [logger] - The logging utility
 * @return {cognitoClient}
 */
export default function makeCognitoClient(params = {}) {
  const { logger = console } = params;
  try {
    const cognitoClient = new CognitoIdentityProviderClient();
    return cognitoClient;
  } catch (error) {
    logger.error(`cognitoClient.makeCognitoClient failed. ${error}`);
    throw error;
  }
}
