module.exports = {
  EVENT_SOURCE_BACKEND: 'BE',
  UNDEFINED_ANONYMOUS_ID: 'unknown-anonymous-user',
  ANONYMOUS_ID_HEADER: 'x-ixc-anonymous-id',
};
