const axios = require('axios');
const crypto = require('crypto');
const validate = require('./validate');

const SIGNATURE_HEADER = 'x-ix-signature';
const API_KEY_HEADER = 'x-api-key';
const CONNECTION_ID_HEADER = 'x-connection-id';
const HASH_ENCODING = 'hex';
const HMAC_ALGORITHM = 'sha256';

/**
 * Client for SPHINX APIs.
 *
 * @see {@link https://inspectionxpert.atlassian.net/wiki/x/BYCtN}
 */
class SphinxClient {
  /**
   * @param   {Object} params
   * @prop    {String} sharedKey
   * @prop    {String} basePath
   * @prop    {String} baseUrl
   * @prop    {Object} logger
   * @prop    {Object} [httpClient]
   * @returns {SphinxClient}
   */
  constructor(params) {
    this.sharedKey = params.sharedKey;
    this.basePath = params.basePath;
    this.logger = params.logger;

    this.logger.debug(`SphinxClient.params.baseUrl: ${params.baseUrl}`);
    this.logger.debug(`SphinxClient.params.basePath: ${params.basePath}`);
    const httpClient = params.httpClient || axios;
    this.httpClient = httpClient.create({ baseURL: params.baseUrl });
  }

  /**
   * Rotates an API key.
   *
   * @param   {Object} params
   * @prop    {String} apiKey
   * @returns {Promise<String>} the new API key
   */
  async apiKeyRotate(params = {}) {
    try {
      validate.required(params, ['apiKey']);

      const { apiKey } = params;

      const urlPath = `${this.basePath}/connection/api-key/${apiKey}`;

      const signature = this.generateSignature({ urlPath });
      const headers = this.setHeaders(signature);

      const response = await this.httpClient.delete(urlPath, { headers });

      return response.data;
    } catch (error) {
      this.logError(error, `API key rotate failed for ${params.apiKey}`);
      throw error;
    }
  }

  /**
   * Gets a user's Connections.
   *
   * @param   {Object} params
   * @prop    {String} accountId
   * @prop    {String} siteId
   * @prop    {String} userId
   * @returns {Promise<Connection[]>}
   */
  async connectionsGet(params = {}) {
    try {
      validate.required(params, ['accountId', 'siteId', 'userId']);

      const { accountId, siteId, userId } = params;

      const urlPath = `${this.basePath}/connection/account/${accountId}/site/${siteId}/user/${userId}`;

      const signature = this.generateSignature({ urlPath });
      const headers = this.setHeaders(signature);

      const response = await this.httpClient.get(urlPath, { headers });

      return response.data;
    } catch (error) {
      this.logError(error, `Connections get failed for account ${params.accountId} / site ${params.siteId} / user ${params.userId}.`);
      throw error;
    }
  }

  /**
   * Gets a Connection by id.
   *
   * @param   {Object} params
   * @prop    {String} connectionId
   * @returns {Promise<Connection>}
   */
  async connectionsGetById(params = {}) {
    try {
      validate.required(params, ['connectionId']);

      const { connectionId } = params;

      const urlPath = `${this.basePath}/connection/${connectionId}`;

      const signature = this.generateSignature({ urlPath });
      const headers = this.setHeaders(signature);

      const response = await this.httpClient.get(urlPath, { headers });

      return response.data;
    } catch (error) {
      this.logError(error, `Connections get failed for Connection ${params.connectionId}`);
      throw error;
    }
  }

  /**
   * Updates a Connection.
   *
   * @param   {Object} params
   * @prop    {String} connectionId
   * @prop    {Object} updateData
   * @returns {Promise<Object>} the updated Connection object
   */
  async connectionsUpdate(params = {}) {
    try {
      validate.required(params, ['connectionId']);

      const { connectionId } = params;
      const payload = { ...params };
      delete payload.connectionId;

      const urlPath = `${this.basePath}/connection/${connectionId}`;

      const signature = this.generateSignature({ urlPath, payload });
      const headers = this.setHeaders(signature);

      const response = await this.httpClient.put(urlPath, payload, { headers });

      return response.data;
    } catch (error) {
      this.logError(error, `Connection update failed for id ${params.connectionId} with data ${params}.`);
      throw error;
    }
  }

  /**
   * Creates a Connection.
   *
   * @param   {Object} params
   * @prop    {String} accountId
   * @prop    {String} siteId
   * @prop    {String} userId
   * @prop    {String} [apiKey]
   * @prop    {*}      * - see ConnectionInput type
   * @returns {Promise<Object>} the new Connection object
   */
  async connectionsCreate(params = {}) {
    try {
      validate.required(params, ['accountId', 'siteId', 'userId']);

      const response = await this.createConnection(params);

      return response.data;
    } catch (error) {
      this.logError(error, `Connection create failed for API key ${params.apiKey} and user ${params.userId}.`);
      throw error;
    }
  }

  /**
   * Deletes a Connection.
   *
   * @param   {Object} params
   * @prop    {String} connectionId
   * @returns {Promise<String>} the id of the Connection that was deleted
   */
  async connectionsDelete(params = {}) {
    try {
      validate.required(params, ['connectionId']);

      const { connectionId } = params;
      const urlPath = `${this.basePath}/connection/${connectionId}`;

      const signature = this.generateSignature({ urlPath });
      const headers = this.setHeaders(signature);

      await this.httpClient.delete(urlPath, { headers });

      return connectionId;
    } catch (error) {
      this.logError(error, `Connection delete failed for id ${params.connectionId}.`);
      throw error;
    }
  }

  /**
   * Gets all Definitions.
   *
   * @returns {Promise<Definition[]>}
   */
  async definitionsGet() {
    try {
      const urlPath = `${this.basePath}/definition`;

      const signature = this.generateSignature({ urlPath });
      const headers = this.setHeaders(signature);

      this.logger.debug(`SPHINX.client.definitionsGet ${JSON.stringify({ basePath: this.basePath, urlPath, signature, headers })}`);

      const response = await this.httpClient.get(urlPath, { headers });

      return response.data;
    } catch (error) {
      this.logError(error, 'Definitions get failed.');
      throw error;
    }
  }

  /**
   * Gets Connection Status.
   *
   * @returns {Promise<Definition[]>}
   */
  async healthGet(params = {}) {
    try {
      validate.required(params, ['apiKey']);

      const { apiKey } = params;

      const urlPath = `${this.basePath}/connection/health/${apiKey}`;

      const signature = this.generateSignature({ urlPath });
      const headers = this.setHeaders(signature);

      const response = await this.httpClient.get(urlPath, { headers });

      return response.data;
    } catch (error) {
      this.logError(error, `Connection Health Check failed for ${params.apiKey}`);
      throw error;
    }
  }

  /**
   * Uploads a report to the configured Destinations of the Connection.
   *
   * @param   {Object} params
   * @prop    {String} apiKey
   * @prop    {String} payload the report as a UTF-8 string
   * @prop    {String} [connectionId] must be provided if multiple Connections exist for the user's API key
   * @returns {Promise<Object>} an object in the form of { succeeded: [destinationNameA, ...], failed: [destinationNameB, ...] }
   */
  async upload(params = {}) {
    try {
      validate.required(params, ['apiKey', 'payload']);

      const { connectionId, payload, firstArticleId } = params;
      const urlPath = `${this.basePath}/upload`;
      this.logger.debug(`SPHINX.client.upload.urlPath ${urlPath}`);

      const signature = this.generateSignature({ urlPath, payload });

      const nonSignatureHeaders = {
        [API_KEY_HEADER]: params.apiKey,
      };

      if (connectionId) {
        nonSignatureHeaders[CONNECTION_ID_HEADER] = connectionId;
      }

      const headers = this.setHeaders(signature, nonSignatureHeaders);
      let response;
      this.logger.debug(`SPHINX.client.upload.firstArticleId ${firstArticleId}`);
      if (firstArticleId) {
        // if there is a faiId then you need to use a put request
        response = await this.httpClient.put(urlPath, { payload, firstArticleId }, { headers });
      } else {
        response = await this.httpClient.post(urlPath, { payload }, { headers });
      }
      this.logger.debug('SPHINX.client.upload.response.data', response.data);
      if (response.data && response.data.failed && response.data.failed.length > 0) {
        throw new Error(response.data.failed);
      }
      return response.data;
    } catch (error) {
      this.logger.debug('SPHINX.client.upload.error');
      this.logError(error);
      throw error;
    }
  }

  /**
   * Creates a Connection.
   *
   * @private
   * @param   {Object} payload
   * @returns {Promise<Object>} the new Connection object
   */
  createConnection(payload) {
    const urlPath = `${this.basePath}/connection/`;

    const signature = this.generateSignature({ payload, urlPath });
    const headers = this.setHeaders(signature);

    return this.httpClient.post(urlPath, payload, { headers });
  }

  /**
   * Signs a request.
   *
   * @private
   * @param   {Object} params
   * @prop    {String} urlPath
   * @prop    {Object} [payload] - the request payload
   * @returns {String} the signature
   */
  generateSignature(params) {
    const { payload, urlPath } = params;

    const signingData = payload && Object.keys(payload).length ? JSON.stringify(payload) : urlPath;
    return crypto.createHmac(HMAC_ALGORITHM, this.sharedKey).update(signingData).digest(HASH_ENCODING);
  }

  /**
   * Sets the object used for request headers.
   *
   * @private
   * @param   {String} signature
   * @param   {Object} [nonSignatureHeaders]
   * @returns {Object}
   */
  setHeaders(signature, nonSignatureHeaders = {}) {
    return {
      ...nonSignatureHeaders,
      [SIGNATURE_HEADER]: signature,
    };
  }

  /**
   * Logs an error, ensuring that axios errors are properly handled.
   *
   * @private
   * @param {Error} error - The error object
   * @param {String} message - The message to include
   */
  logError(error, message) {
    /* istanbul ignore next */
    if (process.env.NODE_ENV === 'test') {
      return;
    }

    // Axios errors are instances of Error but have `reponse.data.errors` populated
    /* istanbul ignore next */
    if (error && error.response && error.response.data && error.response.data.errors) {
      this.logger.error(`${message} ${error.response.data.errors}`);
    } else {
      // This is a non-axios error
      this.logger.error(`${message} ${error}`);
    }
  }
}

module.exports = {
  API_KEY_HEADER,
  CONNECTION_ID_HEADER,
  SIGNATURE_HEADER,
  SphinxClient,
};
