Hubspot Dataflow
https://docs.google.com/document/d/1XaD5VEY00nESxU6O_JyloTxpcgmx0Gj8HBwBqfh_ufQ/edit

Flowchart
https://app.lucidchart.com/documents/edit/f5f65755-ef19-4cd2-870a-f923ab7d0d2b/4-ZLDWLUKybJ#?folder_id=home&browser=icon

trialRequest Endpoint
sendToSQS trailRequestData
<= 200

onSQSMessage
emailExists(email) => user.account
emailDomainExists(email) => account
Send Mail | snsMsgToTopic('Existing account requested trial')

createIxcAccountAndUser => ixcAccountId, ixcOwnerId

createTrial(data)
crmClient.createTrial()
crmAdapter.createOrUpdateCompany(emailDomain) => companyId
crmAdapter.createOrUpdateContact(email, company: companyId)
subscriptionClient.createSubscription()
subscriptionAdapter.addCustomer
subscriptionAdapter.addSubscription
subscriptionAdapter.addProduct
subscriptionAdapter.addComponents
<= TrialRecord | Error
snsMsgToTopic('Existing account requested trial')

snsSubscribers
Email Sales

Hubspot Company Properties
transformToAppSchema(hubspotCompanyRecord) => {
companyId: 4512375702,
portalId: 8445733,
isDeleted: false
is_public: 'false',
createdate: '1600806236464',
hs_created_by_user_id: '9283091',
hs_lastmodifieddate: '1600806298864',

    hubspot_owner_id: '50963116',
    hubspot_owner_assigneddate: '1600806258372',

    name: 'H+A International',
    description: 'H+A International is a full service integrated marketing communications company dedicated to helping its clients achieve the most effective use of resources.',
    phone: '3123324650',
    address: '1195 Mustang Dr',
    address2: 'STE 1220',
    city: 'Chicago',
    state: 'IL',
    zip: '60601',
    country: 'United States',
    timezone: 'America/Chicago',
    industry: 'MARKETING_AND_ADVERTISING',
    annualrevenue: '1000000',
    numberofemployees: '10',
    founded_year: '1984',

    domain: 'halligan.com',
    website: 'halligan.com',
    twitterhandle: 'MarComPros',
    facebook_company_page: 'https://facebook.com/ha-international-inc-marketing-communications',
    linkedin_company_page: 'https://www.linkedin.com/company/h-a-international',
    linkedinbio: 'H+A International is a full service integrated marketing communications company dedicated to helping its clients achieve the most effective use of resources.',
    web_technologies: 'google_cloud;go_squared;facebook_connect;google_analytics;google_apps;double_click;google_tag_manager',

    num_associated_contacts: '1',
    first_contact_createdate: '1599845113883',
    hs_object_id: '4512375702',
    hs_updated_by_user_id: '9283091',
    hs_user_ids_of_all_owners: '9283091',
    hs_all_owner_ids: '50963116',
    hs_num_child_companies: '0',
    hs_num_blockers: '0',
    hs_num_contacts_with_buying_roles: '0',
    hs_num_decision_makers: '0',

    hs_analytics_first_timestamp: '1599845113548',
    hs_analytics_num_page_views: '0',
    hs_analytics_num_visits: '0',
    hs_analytics_source_data_1: 'API',
    hs_analytics_source_data_2: 'sample-contact',
    hs_analytics_source: 'OFFLINE',
    hs_predictivecontactscore_v2: '2.79',
    hs_target_account_probability: '0.38625118136405945',

}
