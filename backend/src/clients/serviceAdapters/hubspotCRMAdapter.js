const Hubspot = require('hubspot');
const path = require('path');
const fs = require('fs');
const validate = require('../validate');
const { log } = require('../../logger');
const config = require('../../../config')();

const ADAPTER_NAME = 'hubspot';
const CRM_TYPE_COMPANY = 'company';
const CRM_DEFAULT_RETURN_PROPERTIES_COMPANY = [
  'domain', //
  'name',
  'ix_chargify_id',
  'createdate',
  'hs_lastmodifieddate',
];
const CRM_TYPE_CONTACT = 'contact';
const CRM_DEFAULT_RETURN_PROPERTIES_CONTACT = [
  'firstname', //
  'lastname',
  'email',
  'hs_email_domain',
  'company',
  'associatedcompanyid',
  'ixc_user_id',
  'ixc_account_id',
  'createdate',
];
const CRM_TYPE_DEAL = 'deal';
const CRM_DEFAULT_RETURN_PROPERTIES_DEAL = [
  'dealname', //
  'amount',
  'closedate',
  'pipeline',
  'dealstage',
  'createdate',
  'hs_lastmodifieddate',
  'hs_object_id',
  'ix_chargify_id',
];

const CRM_TIMELINE_TYPES = ['contacts', 'companies', 'deals', 'tickets'];

const CRM_SEARCH_TYPES = ['companies', 'contacts', 'deals', 'products', 'tickets', 'line_items', 'quotes', 'custom objects'];
const CRM_SEARCH_OPERATORS = ['EQ', 'NEQ', 'LT', 'LTE', 'GT', 'GTE', 'HAS_PROPERTY', 'NOT_HAS_PROPERTY', 'CONTAINS_TOKEN', 'NOT_CONTAINS_TOKEN'];

/**
 * Hubspot client that interfaces with the Hubspot service.
 *
 * @see {@link https://github.com/HubSpot/hubspot-api-nodejs}
 */
module.exports = class HubspotCRMAdapter {
  /**
   * @param {Object} params
   * @prop  {String} privateAccessToken - The Hubspot private access token
   * @prop  {String} apiKey - The Hubspot API key
   * @prop  {String} privateAccessToken - The Hubspot private access token
   * @prop  {String} appId - The App ID of the IX Hubspot Bridge app
   * @prop  {String} appDevApiKey - The Hubspot API key
   * @prop  {Object} [logger] - The logging utility
   * @return {HubspotCRMAdapter}
   */
  constructor(params) {
    if (!params.privateAccessToken) {
      throw new Error(`HubspotCRMAdapter requires an access token, but none was provided`);
    }

    this.logger = params.logger || console;
    // this.apiKey = params.apiKey;
    this.appId = params.appId;
    // this.appDevApiKey = params.appDevApiKey;

    if (params.hubspotMock) {
      this.hubspot = params.hubspotMock;
      this.ixHubspotBridge = params.hubspotMock;
      this.hubspotTimeline = params.hubspotMock;
      this.isMocked = true;
    } else {
      // Client interface used by all api calls to Hubspot's own system API
      this.hubspot = new Hubspot({ accessToken: params.privateAccessToken });

      // Client interface used to update Timeline Event Templates (must be API key of developer who owns ixHubspotBridge app)
      this.ixHubspotBridge = new Hubspot({
        apiKey: params.appDevApiKey,
      });
      // Client interface used to add Timeline Events based on the ixHubspotBridge event templates
      this.hubspotTimeline = new Hubspot({
        clientId: params.appClientId,
        clientSecret: params.appClientSecret,
        refreshToken: params.appOauthRefreshToken,
        redirectUri: 'http://localhost:3000/oauth-callback',
      });
      this.isMocked = false;
    }

    this.adapter = {
      name: ADAPTER_NAME,
      isMocked: this.isMocked,
    };

    // Prepare to create a reference cache of Timeline Event IDs
    this.timelineEvents;

    // Prepare to create a reference cache of pipeline and stage names
    this.pipelines;

    // CRM deals can be setup to track MRR or ARR
    this.deal_value_is_arr = params.deal_value_is_arr || true;
  }

  /**
   * Calls asynchronous functions that warm up local caches
   *
   */
  async initialize() {
    await this.buildTimelineEventReference();
    await this.buildPipelineReference();
  }

  /**
   * Retrieve details about this adapter
   *
   * @returns {<Object>} - A JS object containing the name of the adapter and its mock status
   *
   */
  getMetadata() {
    return {
      name: ADAPTER_NAME,
      isMocked: this.isMocked,
    };
  }

  /*---------------------------------------------------------------------------------*/
  //                                 Sales Pipelines
  /*---------------------------------------------------------------------------------*/

  async validateENVPipeline(pipelines) {
    const errors = [];

    this.logger.debug('Validating CRM Pipeline Names');
    const pipeline = pipelines[config.crm.pipelineName.toUpperCase()];
    if (!pipeline) {
      errors.push(`ENV Pipeline name "${config.crm.pipelineName}" does not match any CRM Pipeline name.`);
      const alt = Object.keys(pipelines).find((p) => p.includes('IX3'));
      if (alt) {
        errors.push(`Closed Lost Stage Name may be "${alt}".`);
      }
      // Quit early if the pipeline is wrong
      throw new Error('CRM Pipeline Validation Failed. See warnings for details.');
    }
    const mqaStage = pipeline.stages[config.crm.pipelineStage.toUpperCase()];
    if (!mqaStage) {
      errors.push(`ENV Stage name "${config.crm.pipelineStage}" does not match any CRM Stage name.`);
      const alt = Object.keys(pipeline.stages).find((s) => s.includes('QUAL'));
      if (alt) {
        errors.push(`MQA Stage Name may be "${alt}".`);
      }
    }
    const closedWon = pipeline.stages[config.crm.closedWon.toUpperCase()];
    if (!closedWon) {
      errors.push(`ENV Stage name "${config.crm.closedWon}" does not match any CRM Stage name.`);
      const alt = Object.keys(pipeline.stages).find((s) => s.includes('(WON)'));
      if (alt) {
        errors.push(`Closed Won Stage Name may be "${alt}".`);
      }
    }
    const closedLost = pipeline.stages[config.crm.closedLost.toUpperCase()];
    if (!closedLost) {
      errors.push(`ENV Stage name "${config.crm.closedLost}" does not match any CRM Stage name.`);
      const alt = Object.keys(pipeline.stages).find((s) => s.includes('(LOST)'));
      if (alt) {
        errors.push(`Closed Lost Stage Name may be "${alt}".`);
      }
    }
    if (errors.length > 0) {
      for (let e = 0; e < errors.length; e++) {
        this.logger.warn(errors[e]);
      }
      throw new Error(`CRM Pipeline Validation Failed due to following ${errors.length} errors: ${errors.join(' ')}`);
    }
  }

  /**
   * Retrieve pipeline and stage id for the given pipeline and stage names
   *
   * @see {@link https://developers.hubspot.com/docs/api/crm/pipelines}
   *
   * @param  {String} pipelineName - The ID or name of the pipeline
   * @param  {String} stageName - The ID or name of the pipeline stage
   * @returns {Promise<Object>} - The hubspot company data in the app's expected object schema
   */
  async getPipelineLabelIds(pipelineName, stageName) {
    if (!this.pipelines) await this.buildPipelineReference();

    const result = {
      pipelineId: undefined,
      stageId: undefined,
    };

    // Standardize lookup keys so that values are uppercase strings (which is how they are stored buildPipelineReference)
    const standardizedPipelineName = `${pipelineName}`.toUpperCase();
    const standardizedStageName = `${stageName}`.toUpperCase();

    // Validate pipeline name
    if (Object.prototype.hasOwnProperty.call(this.pipelines, standardizedPipelineName)) {
      result.pipelineId = this.pipelines[standardizedPipelineName].id;
      // pipeline was valid, so validate stage name
      if (Object.prototype.hasOwnProperty.call(this.pipelines[standardizedPipelineName].stages, standardizedStageName)) {
        result.stageId = this.pipelines[standardizedPipelineName].stages[standardizedStageName].id;
      }
    }

    // Return an object that can be destructured
    return result;
  }

  /**
   * Retrieve pipelines from the CRM
   *
   * @see {@link https://developers.hubspot.com/docs/api/crm/pipelines}
   *
   * @prop  {String} id
   * @returns {Promise<Object>} - The hubspot company data in the app's expected object schema
   */
  async buildPipelineReference() {
    try {
      const pipelines = await this.getPipelines();
      const ref = {};
      for (let i = 0; i < pipelines.length; i++) {
        const pipeline = pipelines[i];
        const stages = {};
        for (let j = 0; j < pipeline.stages.length; j++) {
          const stage = pipeline.stages[j];
          const s = {
            id: stage.stageId,
            name: stage.label,
            active: stage.active,
          };
          // Support stage lookup by ID or label
          stages[`${stage.stageId}`] = s;
          stages[stage.label.toUpperCase()] = s;
        }
        const p = {
          id: pipeline.pipelineId,
          name: pipeline.label,
          active: pipeline.active,
          stages,
        };
        // Support pipeline lookup by ID or label
        ref[`${pipeline.pipelineId}`] = p;
        ref[pipeline.label.toUpperCase()] = p;
      }

      // Validate Pipeline
      await this.validateENVPipeline(ref);

      // Initialize the pipeline reference
      this.pipelines = ref;
      return ref;
    } catch (error) {
      this.logError(error, `Error emitted by buildPipelineReference.`);
      throw error;
    }
  }

  /**
   * Retrieve pipelines from the CRM
   *
   * @see {@link https://developers.hubspot.com/docs/api/crm/pipelines}
   *
   * @prop  {String} id
   * @returns {Promise<Object>} - The hubspot company data in the app's expected object schema
   */
  async getPipelines() {
    try {
      const result = await this.hubspot.pipelines.get();
      return result;
    } catch (error) {
      this.logError(error, `Error emitted by getPipelines`);
      throw error;
    }
  }

  /*---------------------------------------------------------------------------------*/
  //                                    Companies
  /*---------------------------------------------------------------------------------*/

  // hubspot.companies.get(opts)
  // hubspot.companies.getById(id)
  // hubspot.companies.getByDomain(domain)
  // hubspot.companies.create(data)
  // hubspot.companies.update(id, data) // data = [{ objectId: 123, properties: [] }]
  // hubspot.companies.delete(id)

  /**
   * Retrieve a company by id
   *
   * @see {@link https://developers.hubspot.com/docs/api/crm/companies}
   *
   * @prop  {String} id
   * @returns {Promise<Object>} - The hubspot company data in the app's expected object schema
   */
  async getCompanyById(id) {
    try {
      if (!id) throw new Error('Required param `id` not defined');
      const result = await this.hubspot.companies.getById(id);
      return HubspotCRMAdapter.transformToAppSchema(CRM_TYPE_COMPANY, result.companyId, result);
    } catch (error) {
      this.logError(error, `Error emitted by getCompanyById ${id}`);
      throw error;
    }
  }

  /**
   * Retrieve a company by domain
   *
   * @see {@link https://developers.hubspot.com/docs/api/crm/companies}
   *
   * @prop  {String} domain
   * @prop  {Array[String]} properties - Array of company props to return
   * @prop  {String} limit - Maximum number of records to return
   * @returns {Promise<Object>} - The hubspot company data in the app's expected object schema
   */
  async getCompanyByDomain(domain, properties = CRM_DEFAULT_RETURN_PROPERTIES_COMPANY, limit = 2) {
    try {
      if (!domain) throw new Error('Required param `domain` not defined');
      const payload = HubspotCRMAdapter.companyRequestPayload(properties, limit);
      const result = await this.hubspot.companies.getByDomain(domain, payload);
      // Return none
      if (result.results.length === 0) {
        return [];
      }
      // Format all and return
      return result.results.map((record) => HubspotCRMAdapter.transformToAppSchema(CRM_TYPE_COMPANY, record.companyId, record));
    } catch (error) {
      this.logError(error, `Error emitted by getCompanyByDomain ${domain}`);
      throw error;
    }
  }

  /**
   * Retrieve a company by is IXC account ID
   *
   * @see {@link https://developers.hubspot.com/docs/api/crm/companies}
   *
   * @prop  {String} ixcAccountId - IXC Account.id
   * @prop  {Array[String]} properties - Array of company props to return
   * @prop  {String} limit - Maximum number of records to return
   * @returns {Promise<Object>} - The hubspot company data in the app's expected object schema
   */
  async getCompanyByAccountId(ixcAccountId, properties = CRM_DEFAULT_RETURN_PROPERTIES_COMPANY) {
    try {
      if (!ixcAccountId) throw new Error('Required param `ixcAccountId` not defined');
      const results = await this.searchCompanies({
        properties,
        filterValues: {
          ixc_account_id: { value: ixcAccountId },
        },
      });
      // Return none
      if (results.length === 0) return;
      if (results.length > 1) log.warn(`More than one company found with ixcAccountId = ${ixcAccountId}`);
      return results[0];
    } catch (error) {
      this.logError(error, `Error emitted by getCompanyByAccountId ${ixcAccountId}`);
      throw error;
    }
  }

  /**
   * Create a company
   *
   * @see {@link https://developers.hubspot.com/docs/api/crm/companies}
   *
   * @param {Object} params
   * @prop  {String} reference - The id of the IXC comapny record
   * @prop  {String} name
   * @prop  {String} domain
   * @returns {Promise<Object>} - The Hubspot comapny record
   */
  async createCompany(params = {}) {
    const { name, domain } = params;
    try {
      // Validate required fields
      // TODO: Get required params from hubspot during initilization instead of hardcoding them
      // Validate required fields
      validate.required(params, ['name', 'domain']);

      // Sales doesn't want their edits to the Account Name to be overwritten, so update ix_account_name instead of name
      params.ix_account_name = name;

      const payload = HubspotCRMAdapter.buildPayloadWithName(params);
      const result = await this.hubspot.companies.create(payload);
      return HubspotCRMAdapter.transformToAppSchema(CRM_TYPE_COMPANY, result.companyId, result);
    } catch (error) {
      this.logError(error, `Error creating company with name ${name} and domain ${domain}`);
      throw error;
    }
  }

  /**
   * Update a company
   *
   * @see {@link https://developers.hubspot.com/docs/api/crm/companies}
   *
   * @prop {String} id - The companyId that should be updated
   * @prop {Boolean} updateAllNameFields - Whether or not both company.name and company.ix_account_name should be updated, or just ix_account_name (default is false)
   * @param {Object} params - An object whose key/value pairs represent the updates that should be made
   * @returns {Promise<Object>} - The Hubspot comapny record
   */
  async updateCompany({ id, updateAllNameFields = false, ...params }) {
    try {
      // Validate required fields
      if (!id) throw new Error('Required param `id` not defined');

      if (params.name) {
        // Sales doesn't want their edits to the Account Name to be overwritten, so update ix_account_name instead of name
        params.ix_account_name = params.name;
        if (!updateAllNameFields) delete params.name;
      }

      // Update the company
      const payload = HubspotCRMAdapter.buildPayloadWithName(params);
      const result = await this.hubspot.companies.update(id, payload);

      // Hubspot contacts have a .company field which also holds the company name
      // If a new name was passed, update company field in all associated contacts as well
      if (params.ix_account_name) {
        const contactIds = await this.getCompanysAssociatedContactIds({ companyId: id });
        const contactUpdates = [];
        for (let i = 0; i < contactIds.length; i++) {
          contactUpdates.push({ id: contactIds[i], company: params.ix_account_name });
        }
        this.updateContacts(contactUpdates);
      }

      // Return the company record
      return HubspotCRMAdapter.transformToAppSchema(CRM_TYPE_COMPANY, result.companyId, result);
    } catch (error) {
      this.logError(error, `Error updating the company with id ${id}`);
      throw error;
    }
  }

  /**
   * Create or update a Company
   *
   * @see {@link https://developers.hubspot.com/docs/api/crm/contacts}
   *
   * @param {Object} params
   * @prop  {String} reference - The id of the IXC company record
   * @prop  {String} name
   * @prop  {String} domain
   * @returns {Promise<Object>} - The Hubspot customer record
   */
  async createOrUpdateCompany(params = {}) {
    const { name, domain, forceCreateCompany = false } = params;
    delete params.forceCreateCompany;

    try {
      // Validate required fields
      // TODO: Get required params from hubspot during initilization instead of hardcoding them
      validate.required(params, ['name', 'domain']);

      // Find existing companies with the same domain
      const existingCompanies = await this.getCompanyByDomain(domain);

      let company;
      if (existingCompanies.length === 0) {
        this.logger.debug(`Creating new company with the domain ${domain}`);
        company = await this.createCompany(params);
      } else if (existingCompanies.length === 1) {
        [company] = existingCompanies;
        if (forceCreateCompany) {
          // This was an orphan user who chose to create their own account instead of request access
          const parentCompany = { ...company };
          this.logger.debug(`Creating new company with the domain ${domain} as child of ${parentCompany.crmId}`);
          company = await this.createCompany(params);
          this.addChildCompanyToParentCompany({
            parentCompanyId: parentCompany.crmId,
            childCompanyId: company.crmId,
          });
        } else {
          this.logger.debug(`Updating existing company ${company.name} (${company.crmId}) with the domain ${domain}`);
          // If the existing company's name is its domain name, update it with the new value (because they're completing their registration)
          const updateAllNameFields = company.name.toLowerCase() === domain.toLowerCase();
          company = await this.updateCompany({ id: company.crmId, updateAllNameFields, ...params });
        }
      } else {
        // There is more than one existing company. Determine which company should be the parent.
        const parentCompany = await this.identifyParent(existingCompanies);
        // Create a company and make it a child of the parent
        this.logger.debug(`Creating new company with the domain ${domain} as child of ${parentCompany.crmId}`);
        company = await this.createCompany(params);
        // Make the new company a child of the parent
        this.addChildCompanyToParentCompany({
          parentCompanyId: parentCompany.crmId,
          childCompanyId: company.crmId,
        });
      }
      return company;
    } catch (error) {
      this.logError(error, `Error creating or updating company with name ${name} and domain ${domain}`);
      throw error;
    }
  }

  /**
   * Delete a company
   *
   * @see {@link https://developers.hubspot.com/docs/api/crm/companies}
   *
   * @param {Object} id - The companyId that should be deleted
   * @returns {Promise<Object>} - The Hubspot comapny record
   */
  async deleteCompany({ id }) {
    try {
      // Validate required fields
      if (!id) throw new Error('Required param `id` not defined');
      const result = await this.hubspot.companies.delete(id);
      return result;
    } catch (error) {
      this.logError(error, `Error deleting the company with id ${id}`);
      throw error;
    }
  }

  /**
   * Determine which of multiple company objects should be the parent
   *
   * @see {@link https://legacydocs.hubspot.com/docs/methods/crm-associations/crm-associations-overview}
   *
   * @param {Object} params
   * @prop  {String} parentCompanyId - The Hubspot ID of the company that will be the parent
   * @prop  {String} childCompanyId - The Hubspot ID of the company that will be the child
   * @returns {<Object>} - Returns { success: true } if no error is encountered
   */
  async identifyParent(existingCompanies) {
    try {
      if (existingCompanies.length <= 1) throw new Error(`Invalid input, existingCompanies array must contain 2 or more records but was ${existingCompanies.length}`);
      const canidates = [];
      for (let i = 0; i < existingCompanies.length; i++) {
        const company = existingCompanies[i];
        this.logger.debug(`Getting children of company ${company.name} (${company.crmId}) with the domain ${company.domain}`);
        const children = await this.getChildCompanyIds({ companyId: company.crmId });
        canidates.push({
          crmId: company.crmId,
          numberOfChildren: children.length,
          lastmodified: company.hs_lastmodifieddate,
        });
      }
      // Find the canidate with the most children then by which has been most recently modified
      canidates.sort((a, b) => b.numberOfChildren - a.numberOfChildren || b.lastmodified - a.lastmodified);
      return canidates[0];
    } catch (error) {
      this.logError(error, `Error emitted by identifyParent`);
      throw error;
    }
  }

  /**
   * Associate a child company to a parent company
   *
   * @see {@link https://legacydocs.hubspot.com/docs/methods/crm-associations/crm-associations-overview}
   *
   * @param {Object} params
   * @prop  {String} parentCompanyId - The Hubspot ID of the company that will be the parent
   * @prop  {String} childCompanyId - The Hubspot ID of the company that will be the child
   * @returns {<Object>} - Returns { success: true } if no error is encountered
   */
  async addChildCompanyToParentCompany(params) {
    try {
      const { parentCompanyId, childCompanyId } = params;
      if (!parentCompanyId) throw new Error('Required param `parentCompanyId` not defined');
      if (!childCompanyId) throw new Error('Required param `childCompanyId` not defined');
      // https://github.com/MadKudu/node-hubspot/blob/70ed09ca517c01ef53baff5222a57666f41072dd/lib/company.js#L86
      await this.hubspot.crm.associations.create({
        fromObjectId: parentCompanyId,
        toObjectId: childCompanyId,
        category: 'HUBSPOT_DEFINED', // The category of the association. Must be "HUBSPOT_DEFINED"
        definitionId: 13, // Parent company to child company
      });
      return { success: true };
    } catch (error) {
      this.logError(error, `Error emitted by addChildCompanyToParentCompany`, params);
      throw error;
    }
  }

  /**
   * Associate a contact to a company
   *
   * @see {@link https://developers.hubspot.com/docs/api/crm/companies}
   *
   * @param {Object} params
   * @prop  {String} companyId - The Hubspot ID of the company the contact should be added to
   * @prop  {String} contactVid - The Hubspot ID of the contact that should be added to the company
   * @returns {<Object>} - Returns { success: true } if no error is encountered
   */
  async addContactToCompany(params) {
    try {
      const { companyId, contactVid } = params;
      if (!companyId) throw new Error('Required param `companyId` not defined');
      if (!contactVid) throw new Error('Required param `contactVid` not defined');
      // https://github.com/MadKudu/node-hubspot/blob/70ed09ca517c01ef53baff5222a57666f41072dd/lib/company.js#L86
      await this.hubspot.companies.addContactToCompany({ companyId, contactVid });
      return { success: true };
    } catch (error) {
      this.logError(error, `Error emitted by addContactToCompany`, params);
      throw error;
    }
  }

  /**
   * Disassociate a contact to a company
   *
   * @see {@link https://legacydocs.hubspot.com/docs/methods/crm-associations/crm-associations-overview}
   *
   * @param {Object} params
   * @prop  {String} companyId - The Hubspot ID of the company the contact should be added to
   * @prop  {String} contactVid - The Hubspot ID of the contact that should be added to the company
   * @returns {<Object>} - Returns { success: true } if no error is encountered
   */
  async removeContactFromCompany(params) {
    try {
      const { companyId, contactVid } = params;
      if (!companyId) throw new Error('Required param `companyId` not defined');
      if (!contactVid) throw new Error('Required param `contactVid` not defined');
      await this.hubspot.crm.associations.delete({
        fromObjectId: companyId,
        toObjectId: contactVid,
        category: 'HUBSPOT_DEFINED',
        definitionId: 2,
        // Contact to company	1
        // Company to contact	2
      });
      return { success: true };
    } catch (error) {
      this.logError(error, `Error emitted by removeContactFromCompany`, params);
      throw error;
    }
  }

  /**
   * Helper function that returns the request payload that accompanies company search functions
   *
   * @see {@link https://developers.hubspot.com/docs/api/crm/companies}
   *
   * @param  {Array[String]} properties - The list of fields that should be returned.
   * @param  {Number} limit - The maximum number of records to return. Default is 2.
   * @returns {Promise<Object>} - The payload object expected by hubspot api searches for companies
   */
  static companyRequestPayload(properties = CRM_DEFAULT_RETURN_PROPERTIES_COMPANY, limit = 2) {
    return {
      limit,
      requestOptions: {
        properties,
      },
      offset: {
        isPrimary: true,
        companyId: 0,
      },
    };
  }

  /*---------------------------------------------------------------------------------*/
  //                                    Contacts
  /*---------------------------------------------------------------------------------*/

  // hubspot.contacts.search(query)
  // hubspot.contacts.get(opts)
  // hubspot.contacts.getByEmail(email)
  // hubspot.contacts.getById(id)
  // hubspot.contacts.update(id, data)
  // hubspot.contacts.create(data) // data = [{ vid/email: '', properties: [] }]
  // hubspot.contacts.createOrUpdate(email, data)
  // hubspot.contacts.updateByEmail(email, data)
  // hubspot.contacts.delete(id)

  // hubspot.contacts.getAll(opts)
  // hubspot.contacts.merge(primaryId, secondaryId)

  // /**
  //  * Search for contacts by email address, first and last name, phone number, or company
  //  *
  //  * @see {@link https://developers.hubspot.com/docs/api/crm/contacts}
  //  *
  //  * @param {Object} params - A set of key value pairs which correspond to the fields and values that should be searched for
  //  * @prop {Object} searchText - The text string to search for (can be partial match)
  //  * @prop {[String]} properties - The list of properties that should be returned
  //  * @prop {Integer} count - How manu records to return (default is 100)
  //  * @prop {Integer} offset - What index in paged result set to begin returning results from
  //  * @prop {String} sort - The name of the field to sort on (default is vid)
  //  * @prop {Enum} order - Direction of sort, options are "DESC" or "ASC" (default is "DESC")
  //  * @returns {Promise<Object>} - The hubspot deal data in the app's expected object schema
  //  */
  // async lookupContacts(params) {
  //   const { searchText, properties = [], count = 100, offset, sort = 'vid', order = 'DESC' } = params;
  //   try {
  //     if (!searchText) throw new Error('Required param `searchText` not defined');

  //     let queryString = `?q=${searchText}`;

  //     properties.forEach((property) => {
  //       queryString += `&property=${property}`;
  //     });

  //     if (count) queryString += `&count=${count}`;
  //     if (offset) queryString += `&offset=${offset}`;
  //     if (sort) queryString += `&sort=${sort}`;
  //     if (order) queryString += `&order=${order}`;

  //     const response = await this.hubspot.apiRequest({
  //       method: 'GET',
  //       path: `/contacts/v1/search/query${queryString}`,
  //     });
  //     return response;
  //     // return HubspotCRMAdapter.transformToAppSchema(CRM_TYPE_CONTACT, result.vid, result);
  //   } catch (error) {
  //     this.logError(error, `Error emitted by searchContacts`);
  //     throw error;
  //   }
  // }

  /**
   * Retrieve a contact by id
   *
   * @see {@link https://developers.hubspot.com/docs/api/crm/contacts}
   *
   * @param  {String} id
   * @returns {Promise<Object>} - The hubspot contact data in the app's expected object schema
   */
  async getContactById(id) {
    try {
      if (!id) throw new Error('Required param `id` not defined');
      const result = await this.hubspot.contacts.getById(id);
      return HubspotCRMAdapter.transformToAppSchema(CRM_TYPE_CONTACT, result.vid, result);
    } catch (error) {
      this.logError(error, `Error emitted by getContactById ${id}`);
      throw error;
    }
  }

  /**
   * Retrieve a contact by email
   *
   * @see {@link https://developers.hubspot.com/docs/api/crm/contacts}
   *
   * @param  {String} email
   * @returns {Promise<Object>} - The hubspot contact data in the app's expected object schema
   */
  async getContactByEmail(email) {
    try {
      if (!email) throw new Error('Required param `email` not defined');
      const result = await this.hubspot.contacts.getByEmail(email);
      // if (!result) return;
      return HubspotCRMAdapter.transformToAppSchema(CRM_TYPE_CONTACT, result.vid, result);
    } catch (error) {
      if (error.message.includes('contact does not exist')) {
        this.logger.warn(`getContactByEmail(${email}) recieved response ${error.message}`);
        return error;
      }
      this.logError(error, `Error emitted by getContactByEmail ${email}`);
      throw error;
    }
  }

  /**
   * Create or update a contact
   *
   * @see {@link https://developers.hubspot.com/docs/api/crm/contacts}
   *
   * @param {Object} params
   * @prop  {String} reference - The id of the IXC User record
   * @prop  {String} email
   * @prop  {String} firstName
   * @prop  {String} lastName
   * @returns {Promise<Object>} - The Hubspot customer record
   */
  async createOrUpdateContact(params = {}) {
    const { email } = params;
    try {
      // Validate required fields
      // TODO: Get required params from hubspot during initilization instead of hardcoding them
      validate.required(params, ['email', 'firstname', 'lastname']);

      // Prepare data and submit
      const payload = HubspotCRMAdapter.buildPayloadWithProperty(params);
      const response = await this.hubspot.contacts.createOrUpdate(email, payload);

      // => { vid: 101, isNew: false }
      const hubspotContact = await this.getContactById(response.vid);
      return hubspotContact;
    } catch (error) {
      this.logError(error, `Error creating contact with email ${email}`);
      throw error;
    }
  }

  /**
   * Update a contact
   *
   * @see {@link https://developers.hubspot.com/docs/api/crm/contacts}
   *
   * @prop {Object} id - The vid that should be updated
   * @param {Object} params - An object whose key/value pairs represent the updates that should be made
   * @returns {Promise<Object>} - The Hubspot contact record
   */
  async updateContact({ id, ...params }) {
    try {
      // Validate required fields
      if (!id) throw new Error('Required param `id` not defined');
      const payload = HubspotCRMAdapter.buildPayloadWithProperty(params);
      let result = await this.hubspot.contacts.update(id, payload);
      result = await this.getContactById(id);
      return result;
    } catch (error) {
      this.logError(error, `Error updating the contact with id ${id}`);
      throw error;
    }
  }

  /**
   * Batch update a contacts
   *
   * @see {@link https://developers.hubspot.com/docs/api/crm/contacts}
   *
   * @prop {Object} id - The vid that should be updated
   * @param {Object} params - An object whose key/value pairs represent the updates that should be made
   * @returns {Promise<Object>} - The Hubspot comapny record
   */
  async updateContacts(updates) {
    try {
      const payload = [];
      // Loop over all the records and build a HS payload for each
      for (let i = 0; i < updates.length; i++) {
        const record = updates[i];
        const data = {};
        // Add the identity
        if (record.id) {
          data.vid = record.id;
          delete record.id; // Don't pass ID as a property
        } else if (record.email) {
          data.email = record.email;
        } else {
          throw new Error(`Required param id or email were not defined for record ${i} of ${updates.length}`);
        }
        // Add the properties
        const properties = HubspotCRMAdapter.buildPayloadWithProperty(record);
        payload.push({ ...data, ...properties });
      }
      // When successful, this call returns 204 Accepeted with no body
      await this.hubspot.contacts.createOrUpdateBatch(payload);
      return { success: true };
    } catch (error) {
      this.logError(error, `Error batch updating contacts`);
      throw error;
    }
  }

  /**
   * Delete a contact
   *
   * @see {@link https://developers.hubspot.com/docs/api/crm/contacts}
   *
   * @param {Object} id - The vid that should be deleted
   * @returns {Promise<Object>} - An object containing the delete result and id of the deleted contact
   */
  async deleteContact({ id }) {
    try {
      // Validate required fields
      if (!id) throw new Error('Required param `id` not defined');
      const result = await this.hubspot.contacts.delete(id);
      return result;
    } catch (error) {
      this.logError(error, `Error deleting the contact with id ${id}`);
      throw error;
    }
  }

  /*---------------------------------------------------------------------------------*/
  //                                    Deals
  /*---------------------------------------------------------------------------------*/

  // hubspot.deals.create(data)
  // hubspot.deals.updateById(id, data)
  // hubspot.deals.associate(id, objectType, associatedObjectId)
  // hubspot.deals.removeAssociation(id, objectType, associatedObjectId)
  // hubspot.deals.get(opts)
  // hubspot.deals.getById(id)
  // hubspot.deals.getAssociated(objectType, objectId, opts)

  /**
   * Retrieve a deal by id
   *
   * @see {@link https://developers.hubspot.com/docs/api/crm/deals}
   *
   * @param  {String} id
   * @returns {Promise<Object>} - The hubspot deal data in the app's expected object schema OR
   */
  async getDealById(id) {
    try {
      if (!id) {
        this.logger.warn('getDealById Required param `id` not defined'); // deal may be non-existent in which case id is expected to be null/undefined
        return;
      }
      const result = await this.hubspot.deals.getById(id);
      return HubspotCRMAdapter.transformToAppSchema(CRM_TYPE_DEAL, result.dealId, result);
    } catch (error) {
      if (error.message.includes('does not exist')) {
        this.logger.warn(`getDealById(${id}) recieved response ${error.message}`);
        return error;
      }
      this.logError(error, `Error emitted by getDealById ${id}`);
      throw error;
    }
  }

  /**
   * Create a deal
   *
   * @see {@link https://developers.hubspot.com/docs/api/crm/deals}
   *
   * @param {Object} params
   * @prop  {String} reference - The id of the IXC account record
   * @prop  {String} dealname
   * @prop  {String} dealtype
   * @prop  {String} amount
   * @returns {Promise<Object>} - The Hubspot customer record
   */
  async createDeal(params) {
    const { dealname } = params;
    try {
      // Validate required fields
      const useParams = {
        dealtype: 'newbusiness',
        amount: 0,
        pipeline: 'default',
        ...params,
      };

      // Validate and set pipeline and pipeline stage names
      if (params.pipeline && params.dealstage) {
        const { pipelineId, stageId } = await this.getPipelineLabelIds(params.pipeline, params.dealstage);
        if (!pipelineId) {
          throw new Error(`Unrecognized pipeline name "${params.pipeline}"`);
        }
        if (!stageId) {
          throw new Error(`Unrecognized stage name "${params.dealstage}"`);
        }
        useParams.pipeline = pipelineId;
        useParams.dealstage = stageId;
      }

      // TODO: Get required params from hubspot during initilization instead of hardcoding them
      validate.required(useParams, ['dealname', 'dealtype', 'amount']);

      const payload = HubspotCRMAdapter.buildPayloadWithName(useParams);
      const result = await this.hubspot.deals.create(payload);
      return HubspotCRMAdapter.transformToAppSchema(CRM_TYPE_DEAL, result.dealId, result);
    } catch (error) {
      this.logError(error, `Error creating deal with name ${dealname}`);
      throw error;
    }
  }

  /**
   * Update a deal
   *
   * @see {@link https://developers.hubspot.com/docs/api/crm/deals}
   *
   * @prop {Object} id - The dealId that should be updated
   * @param {Object} params - An object whose key/value pairs represent the updates that should be made
   * @returns {Promise<Object>} - The Hubspot comapny record
   */
  async updateDeal({ id, ...params }) {
    try {
      // Validate required fields
      if (!id) throw new Error('Required param `id` not defined');

      // Validate dealstage
      const useParams = { ...params };

      // If dealstage is provided, validate and set pipeline and pipeline stage names
      let { pipeline, dealstage } = params;
      if (dealstage) {
        // If pipeline isn't provided, lookup the existing pipeline
        if (!pipeline) {
          const deal = await this.getDealById(id);
          pipeline = deal.pipeline;
        }

        // Validate and set pipeline and pipeline stage names
        const { pipelineId, stageId } = await this.getPipelineLabelIds(pipeline, dealstage);
        if (!pipelineId) {
          throw new Error(`Unrecognized pipeline name "${pipeline}"`);
        }
        if (!stageId) {
          throw new Error(`Unrecognized stage name "${dealstage}"`);
        }
        useParams.pipeline = pipelineId;
        useParams.dealstage = stageId;
      }

      const payload = HubspotCRMAdapter.buildPayloadWithName(useParams);
      const result = await this.hubspot.deals.updateById(id, payload);
      return HubspotCRMAdapter.transformToAppSchema(CRM_TYPE_DEAL, result.dealId, result);
    } catch (error) {
      this.logError(error, `Error updating the deal with id ${id}`);
      throw error;
    }
  }

  /**
   * Associate contact with deal
   *
   * @see {@link https://developers.hubspot.com/docs/api/crm/deals}
   *
   * @param {Object} params
   * @prop  {String} dealId - The Hubspot ID of the deal the contact should be added to
   * @prop  {String} contactVid - The Hubspot ID of the contact that should be added to the deal
   * @returns {<Object>} - Returns { success: true } if no error is encountered
   */
  async addContactToDeal(params) {
    try {
      const { dealId, contactVid } = params;
      if (!dealId) throw new Error('Required param `dealId` not defined');
      if (!contactVid) throw new Error('Required param `contactVid` not defined');
      // https://github.com/MadKudu/node-hubspot/blob/70ed09ca517c01ef53baff5222a57666f41072dd/lib/deal.js#L86
      await this.hubspot.deals.associate(dealId, 'CONTACT', contactVid);
      return { success: true };
    } catch (error) {
      this.logError(error, `Error emitted by addContactToDeal`, params);
      throw error;
    }
  }

  /**
   * Associate company with deal
   *
   * @see {@link https://developers.hubspot.com/docs/api/crm/deals}
   *
   * @param {Object} params
   * @prop  {String} dealId - The Hubspot ID of the deal the contact should be added to
   * @prop  {String} companyId - The Hubspot ID of the company that should be added to the deal
   * @returns {<Object>} - Returns { success: true } if no error is encountered
   */
  async addCompanyToDeal(params) {
    try {
      const { dealId, companyId } = params;
      if (!dealId) throw new Error('Required param `dealId` not defined');
      if (!companyId) throw new Error('Required param `companyId` not defined');
      await this.hubspot.deals.associate(dealId, 'COMPANY', companyId);
      return { success: true };
    } catch (error) {
      this.logError(error, `Error emitted by addCompanyToDeal`, params);
      throw error;
    }
  }

  /**
   * Remove a contact from a deal
   *
   * @see {@link https://developers.hubspot.com/docs/api/crm/deals}
   *
   * @param {Object} params
   * @prop  {String} dealId - The Hubspot ID of the deal the contact should be added to
   * @prop  {String} contactVid - The Hubspot ID of the contact that should be added to the deal
   * @returns {<Object>} - Returns { success: true } if no error is encountered
   */
  async removeContactFromDeal(params) {
    try {
      const { dealId, contactVid } = params;
      if (!dealId) throw new Error('Required param `dealId` not defined');
      if (!contactVid) throw new Error('Required param `contactVid` not defined');
      await this.hubspot.deals.removeAssociation(dealId, 'CONTACT', contactVid);
      return { success: true };
    } catch (error) {
      this.logError(error, `Error emitted by removeContactFromDeal`, params);
      throw error;
    }
  }

  /**
   * Remove a company from a deal
   *
   * @see {@link https://developers.hubspot.com/docs/api/crm/deals}
   *
   * @param {Object} params
   * @prop  {String} dealId - The Hubspot ID of the deal the contact should be added to
   * @prop  {String} companyId - The Hubspot ID of the company that should be added to the deal
   * @returns {<Object>} - Returns { success: true } if no error is encountered
   */
  async removeCompanyFromDeal(params) {
    try {
      const { dealId, companyId } = params;
      if (!dealId) throw new Error('Required param `dealId` not defined');
      if (!companyId) throw new Error('Required param `companyId` not defined');
      await this.hubspot.deals.removeAssociation(dealId, 'COMPANY', companyId);
      return { success: true };
    } catch (error) {
      this.logError(error, `Error emitted by removeCompanyFromDeal`, params);
      throw error;
    }
  }

  /**
   * Delete a deal
   *
   * @see {@link https://developers.hubspot.com/docs/api/crm/deals}
   *
   * @param {Object} params
   * @param {Object} id - The vid that should be deleted
   * @returns {Promise<Object>} - An object containing the delete result and id of the deleted contact
   */
  async deleteDeal(params) {
    const { id } = params;
    try {
      // Validate required fields
      if (!id) throw new Error('Required param `id` not defined');
      const result = await this.hubspot.deals.deleteById(id);
      return result;
    } catch (error) {
      this.logError(error, `Error emitted by deleteDeal`, params);
      throw error;
    }
  }

  /**
   * Deletes a deal and all associated companies and contacts
   *
   * @see {@link https://developers.hubspot.com/docs/api/crm/deals}
   *
   * @prop  {String} dealId - The Hubspot ID of the deal the contact should be added to
   * @returns {<Object>} - Returns { success: true } if no error is encountered
   */
  async purgeDeal(dealId) {
    try {
      const deal = await this.hubspot.deals.getById(dealId);

      const tasks = [];

      // Delete the company
      const companies = deal.associations.associatedCompanyIds;
      for (let i = 0; i < companies.length; i++) {
        // await this.hubspot.companies.delete(companies[i]);
        tasks.push(this.hubspot.companies.delete(companies[i]));
      }

      // Delete the contacts
      const contacts = deal.associations.associatedVids;
      for (let j = 0; j < contacts.length; j++) {
        // await this.hubspot.contacts.delete(contacts[j]);
        tasks.push(this.hubspot.contacts.delete(contacts[j]));
      }

      // Delete the deal
      // await this.hubspot.deals.deleteById(dealId);
      tasks.push(this.hubspot.deals.deleteById(dealId));

      // Perform all deletes in parallel
      const results = await Promise.all(tasks);

      return { success: true, results };
    } catch (error) {
      this.logError(error, `Error emitted by purgeDeal ${dealId}`);
      throw error;
    }
  }

  /*---------------------------------------------------------------------------------*/
  //                             CRM Search Functions
  /*---------------------------------------------------------------------------------*/

  /**
   * Search for deals using V3 CRM search API
   *
   * @see {@link https://developers.hubspot.com/docs/api/crm/contacts}
   *
   * @param {Object} params - A set of key value pairs which correspond to the fields and values that should be searched for
   * @prop {Object} searchText - The text string to search for (can be partial match)
   * @prop {Object} filterValues - Accept filters as { property: {operator: <CRM_SEARCH_OPERATORS>, value: 'string' }} and interpret them all as AND
   * @prop {Object} filterGroups - Allows querying using Hubspot's native filter group syntax that provides support for AND and OR conditions
   * @prop {[String]} properties - The list of properties that should be returned or CRM_DEFAULT_RETURN_PROPERTIES_COMPANY
   * @prop {Integer} limit - How manu records to return (default is 100)
   * @prop {Integer} after - What index in paged result set to begin returning results from
   * @prop {String} sorts - The name of the field to sort on (default is vid)
   * @prop {Enum} order - Direction of sort, options are "DESC" or "ASC" (default is "DESC")
   * @returns {Promise<Object>} - The hubspot deal data in the app's expected object schema
   */
  async searchDeals(params) {
    const { properties = CRM_DEFAULT_RETURN_PROPERTIES_DEAL } = params;
    return this.searchCrm({
      crmObjectType: 'deals',
      properties,
      ...params,
    });
  }

  /**
   * Search for companies using V3 CRM search API
   *
   * @see {@link https://developers.hubspot.com/docs/api/crm/contacts}
   *
   * @param {Object} params - A set of key value pairs which correspond to the fields and values that should be searched for
   * @prop {Object} searchText - The text string to search for (can be partial match)
   * @prop {Object} filterValues - Accept filters as { property: {operator: <CRM_SEARCH_OPERATORS>, value: 'string' }} and interpret them all as AND
   * @prop {Object} filterGroups - Allows querying using Hubspot's native filter group syntax that provides support for AND and OR conditions
   * @prop {[String]} properties - The list of properties that should be returned or CRM_DEFAULT_RETURN_PROPERTIES_COMPANY
   * @prop {Integer} limit - How manu records to return (default is 100)
   * @prop {Integer} after - What index in paged result set to begin returning results from
   * @prop {String} sorts - The name of the field to sort on (default is vid)
   * @prop {Enum} order - Direction of sort, options are "DESC" or "ASC" (default is "DESC")
   * @returns {Promise<Object>} - The hubspot deal data in the app's expected object schema
   */
  async searchCompanies(params) {
    const { properties = CRM_DEFAULT_RETURN_PROPERTIES_COMPANY } = params;
    return this.searchCrm({
      crmObjectType: 'companies',
      properties,
      ...params,
    });
  }

  /**
   * Search for contacts using V3 CRM search API
   *
   * @see {@link https://developers.hubspot.com/docs/api/crm/contacts}
   *
   * @param {Object} params - A set of key value pairs which correspond to the fields and values that should be searched for
   * @prop {Object} searchText - The text string to search for (can be partial match)
   * @prop {Object} filterValues - Accept filters as { property: {operator: <CRM_SEARCH_OPERATORS>, value: 'string' }} and interpret them all as AND
   * @prop {Object} filterGroups - Allows querying using Hubspot's native filter group syntax that provides support for AND and OR conditions
   * @prop {[String]} properties - The list of properties that should be returned or CRM_DEFAULT_RETURN_PROPERTIES_CONTACT
   * @prop {Integer} limit - How manu records to return (default is 100)
   * @prop {Integer} after - What index in paged result set to begin returning results from
   * @prop {String} sorts - The name of the field to sort on (default is vid)
   * @prop {Enum} order - Direction of sort, options are "DESC" or "ASC" (default is "DESC")
   * @returns {Promise<Object>} - The hubspot deal data in the app's expected object schema
   */
  async searchContacts(params) {
    const { properties = CRM_DEFAULT_RETURN_PROPERTIES_CONTACT } = params;
    return this.searchCrm({
      crmObjectType: 'contacts',
      properties,
      ...params,
    });
  }

  /**
   * Search for CRM objects by any field using any operator
   *
   * @see {@link https://developers.hubspot.com/docs/api/crm/search}
   *
   * When multiple filters are provided within a filterGroup, they will be combined using a logical AND operator.
   * When multiple filterGroups are provided, they will be combined using a logical OR operator.
   * The system supports a maximum of three filterGroups with up to three filters each.
   *
   * @param {Object} params - A set of key value pairs which correspond to the fields and values that should be searched for
   * @prop {String} crmObjectType - Must be one of deals, contacts, companies
   * @prop {Object} searchText - The text string to search for (can be partial match). Performs a text search against all property values for an object type. By default, the results will be returned in order of object creation (oldest first), but you can override this with sorting.
   * @prop {Object} filterValues - Accept filters as { property: {operator: <CRM_SEARCH_OPERATORS>, value: 'string' }} and interpret them all as AND
   * @prop {Object} filterGroups - Allows querying using Hubspot's native filter group syntax that provides support for AND and OR conditions
   * @prop {[String]} properties - The list of properties that should be returned
   * @prop {Integer} limit - How manu records to return (default is 100)
   * @prop {Integer} after - What index in paged result set to begin returning results from
   * @prop {String} sorts - The name of the field to sort on (default is vid)
   * @prop {Enum} order - Direction of sort, options are "DESC" or "ASC" (default is "DESC")
   * @returns {Promise<Object>} - The hubspot deal data in the app's expected object schema
   */
  async searchCrm(params) {
    const { crmObjectType, searchText, filterValues, filterGroups, properties = [], limit = 100, after = 0, sorts = [] } = params;
    try {
      if (!CRM_SEARCH_TYPES.includes(crmObjectType)) throw new Error(`Invalid crmObjectType ${crmObjectType}. Value must be one of ${CRM_SEARCH_TYPES.join(', ')}`);

      // Prepare the query post Body payload
      const payload = {};
      if (searchText) payload.query = searchText;
      if (properties) payload.properties = properties;
      if (sorts) payload.sorts = sorts;
      if (limit) payload.limit = limit;
      if (after) payload.after = after;

      // Support { property: {operator: <CRM_SEARCH_OPERATORS>, value: 'string' }}
      if (filterValues) {
        const filters = Object.entries(filterValues).reduce((obj, [property, evaluation]) => {
          const { value, operator = 'EQ' } = evaluation;
          if (!CRM_SEARCH_OPERATORS.includes(operator)) throw new Error(`Invalid crm search operator ${operator}. Value must be one of ${CRM_SEARCH_OPERATORS.join(', ')}`);
          obj.push({
            value,
            propertyName: property,
            operator,
          });
          return obj;
        }, []);
        payload.filterGroups = [{ filters }];
      } else if (filterGroups) {
        // Support complex AND/OR queries
        payload.filterGroups = filterGroups;
      }

      // Submit to V3 endpoint
      const response = await this.hubspot.apiRequest({
        method: 'POST',
        path: `/crm/v3/objects/${crmObjectType}/search`,
        body: payload,
      });

      // Cast the returned results to the IXC data schema
      let schema;
      if (crmObjectType === 'companies') {
        schema = CRM_TYPE_COMPANY;
      } else if (crmObjectType === 'contacts') {
        schema = CRM_TYPE_CONTACT;
      } else if (crmObjectType === 'deals') {
        schema = CRM_TYPE_DEAL;
      }

      for (let i = 0; i < response.results.length; i++) {
        response.results[i] = HubspotCRMAdapter.transformToAppSchema(schema, response.results[i].id, response.results[i]);
      }

      return response.results;
    } catch (error) {
      this.logError(error, `Error emitted by searchCrm`);
      throw error;
    }
  }

  /*---------------------------------------------------------------------------------*/
  //                                  Get Associations
  /*---------------------------------------------------------------------------------*/

  /**
   * Get objects associated with a specified object
   *
   * @see {@link https://legacydocs.hubspot.com/docs/methods/crm-associations/get-associations}
   *
   * @param {Object} params
   * @prop {String} objectId - The id of Hubspot Company
   * @prop {String} associationId - The id of the Chargify customer
   *                                 Association type	Definition ID
   *                                 1 = Contact to company
   *                                 2 = Company to contact
   *                                 3 = Deal to contact
   *                                 4 = Contact to deal
   *                                 5 = Deal to company
   *                                 6 = Company to deal
   *                                 7 = Company to engagement
   *                                 8 = Engagement to company
   *                                 9 = Contact to engagement
   *                                 10 = Engagement to contact
   *                                 11 = Deal to engagement
   *                                 12 = Engagement to deal
   *                                 13 = Parent company to child company
   *                                 14 = Child company to parent company
   *                                 15 = Contact to ticket
   *                                 16 = Ticket to contact
   *                                 17 = Ticket to engagement
   *                                 18 = Engagement to ticket
   *                                 19 = Deal to line item
   *                                 20 = Line item to deal
   *                                 25 = Company to ticket
   *                                 26 = Ticket to company
   *                                 27 = Deal to ticket
   *                                 28 = Ticket to deal
   * @returns {Array} - Returns an array of object IDs matching the asspciation type
   */
  async getAssociations(params) {
    try {
      const { objectId, associationId, limit = 100 } = params;
      if (!objectId) throw new Error('Required param `objectId` not defined');
      if (!associationId) throw new Error('Required param `associationId` not defined');
      // Call hubspot API directly, because crm associations are implemented in the npm module
      const response = await this.hubspot.apiRequest({
        method: 'GET',
        path: `/crm-associations/v1/associations/${objectId}/HUBSPOT_DEFINED/${associationId}?limit=${limit}`,
      });
      // NOTE: May need to add support for pagination here in the future "hasMore": false, "offset": 5203149086
      return response.results;
    } catch (error) {
      this.logError(error, `Error emitted by getAssociations`, params);
      throw error;
    }
  }

  /**
   * Get child companies of the specified company
   *
   * @see {@link https://legacydocs.hubspot.com/docs/methods/crm-associations/get-associations}
   *
   * @param {Object} params
   * @prop {String} companyId - The id of Hubspot Company
   * @returns {Array} - Returns an array of company IDs
   */
  async getChildCompanyIds(params) {
    const { companyId } = params;
    if (!companyId) throw new Error('Required param `companyId` not defined');
    return this.getAssociations({ objectId: companyId, associationId: 13 });
  }

  /**
   * Get child companies of the specified company
   *
   * @see {@link https://legacydocs.hubspot.com/docs/methods/crm-associations/get-associations}
   *
   * @param {Object} params
   * @prop {String} companyId - The id of Hubspot Company
   * @returns {Array} - Returns an array of company IDs
   */
  async getParentCompanyIds(params) {
    const { companyId } = params;
    if (!companyId) throw new Error('Required param `companyId` not defined');
    return this.getAssociations({ objectId: companyId, associationId: 14 });
  }

  /**
   * Get a company's associated contacts
   *
   * @see {@link https://legacydocs.hubspot.com/docs/methods/crm-associations/get-associations}
   *
   * @param {Object} params
   * @prop {String} companyId - The id of Hubspot Company
   * @returns {Array} - Returns an array of contact IDs
   */
  async getCompanysAssociatedContactIds(params) {
    const { companyId } = params;
    if (!companyId) throw new Error('Required param `companyId` not defined');
    return this.getAssociations({ objectId: companyId, associationId: 2 });
  }

  /**
   * Get a company's associated deals
   *
   * @see {@link https://legacydocs.hubspot.com/docs/methods/crm-associations/get-associations}
   *
   * @param {Object} params
   * @prop {String} companyId - The id of Hubspot Company
   * @returns {Array} - Returns an array of deal IDs
   */
  async getCompanysAssociatedDealIds(params) {
    const { companyId } = params;
    if (!companyId) throw new Error('Required param `companyId` not defined');
    return this.getAssociations({ objectId: companyId, associationId: 6 });
  }

  /*---------------------------------------------------------------------------------*/
  //                               Timeline Event Templates
  /*---------------------------------------------------------------------------------*/

  /**
   * Loads the timeline events and builds a cache of them
   *
   * @see {@link https://developers.hubspot.com/docs/api/crm/timeline}
   *
   * @prop  {String} id
   * @returns {Promise<Object>} - The hubspot company data in the app's expected object schema
   */
  async buildTimelineEventReference(params = {}) {
    try {
      await this.ensureAccessToken();

      // Load Build a list of the EventTemplates in the source code
      this.timelineEvents = params.localTimelineEvents || this.loadLocalTimelineEventTypes();

      // Fetch EventTemplates that already exist in the CRM and compare them to the version defined in the code
      const existingTimelineEvents = await this.listTimelineEventTypes();
      for (let i = 0; i < existingTimelineEvents.length; i++) {
        const existingEventType = existingTimelineEvents[i];

        // If the version defined in the code doesn't match the existing remote EventTemplate, update the remote EventTemplates
        if (!Object.prototype.hasOwnProperty.call(this.timelineEvents[existingEventType.objectType], existingEventType.name)) {
          this.logger.warn(`Remote timelineEventType template ${existingEventType.objectType}:${existingEventType.name} does not exist in local source. Restore it if it was deleted or delete the remote event via the web UI.`);
        } else {
          this.logger.debug(`Comparing local and remote timelineEventType template ${existingEventType.objectType}:${existingEventType.name}`);
          const eventObject = this.timelineEvents[existingEventType.objectType][existingEventType.name];
          if (this.timelineEventsAreDifferent(eventObject, existingEventType)) {
            this.logger.debug(`Updating remote timelineEventType template ${existingEventType.objectType}:${existingEventType.name} to match local version`);
            await this.updateTimelineEventType({ eventTemplateId: existingEventType.id, ...eventObject }); // { name, headerTemplate, detailTemplate, tokens });
          }

          // Update the event reference with the remote EventTemplate IDs and timestamps
          await this.extendTimelineEventReference(existingEventType);
        }
      }

      // Create any new Event Templates that are not already registered
      for (let i = 0; i < CRM_TIMELINE_TYPES.length; i++) {
        const objectType = CRM_TIMELINE_TYPES[i];
        const eventObjects = Object.values(this.timelineEvents[objectType]);
        for (let j = 0; j < eventObjects.length; j++) {
          const eventObject = eventObjects[j];
          const event = this.timelineEvents[objectType][eventObject.name];
          // If the event doesn't exist create it
          let newEventType;
          if (!event.id) {
            this.logger.debug(`Creating new remote timelineEventType template ${event.name}`);
            newEventType = await this.createTimelineEventType(event); // { name, headerTemplate, detailTemplate, tokens });
            this.extendTimelineEventReference(newEventType);
          }
        }
      }

      return { success: true, timelineEventTypes: this.timelineEvents };
    } catch (error) {
      this.logError(error, `Error emitted by buildTimelineEventReference`);
      throw error;
    }
  }

  /**
   * Loads the timeline events in the source code's timelineEventTypes folder
   *
   * @see {@link https://developers.hubspot.com/docs/api/crm/timeline}
   *
   * @returns {<Object>} - A object containing JS objects that represent timeline events for contacts, companies, deals, and tickets
   */
  loadLocalTimelineEventTypes() {
    try {
      const timelineEventTypes = {
        contacts: {},
        companies: {},
        deals: {},
        tickets: {},
      };
      const eventTemplateDirPath = `${__dirname}/timelineEventTypes`;

      // Build a list of the EventTemplates in the source code
      const localEventTemplates = fs.readdirSync(eventTemplateDirPath);
      for (let i = 0; i < localEventTemplates.length; i++) {
        const name = localEventTemplates[i];
        const eventTemplateJson = require(`${eventTemplateDirPath}/${name}`);
        // Create an eventTemplate for each object type listed in forObjectTypes, then delete the non-standard forObjectTypes attribute
        for (let j = 0; j < eventTemplateJson.forObjectTypes.length; j++) {
          const objectType = eventTemplateJson.forObjectTypes[j];
          const newObjectType = {
            ...eventTemplateJson,
            objectType,
          };
          timelineEventTypes[objectType][eventTemplateJson.name] = newObjectType;
          delete timelineEventTypes[objectType][eventTemplateJson.name].forObjectTypes;
        }
      }
      return timelineEventTypes;
    } catch (error) {
      this.logError(error, `Error emitted by loadLocalTimelineEventTypes`);
      throw error;
    }
  }

  // Helper function that extends the local timeline event object to include attributes that are only on the CRM object
  extendTimelineEventReference(eventType) {
    this.timelineEvents[eventType.objectType][eventType.name].id = eventType.id;
    this.timelineEvents[eventType.objectType][eventType.name].createdAt = eventType.createdAt;
    this.timelineEvents[eventType.objectType][eventType.name].updatedAt = eventType.updatedAt;
  }

  // Helper function that extends the local timeline event object to include attributes that are only on the CRM object
  timelineEventsAreDifferent(eventTypeA, eventTypeB) {
    // Check attributes
    if (
      eventTypeA.objectType !== eventTypeB.objectType ||
      eventTypeA.name !== eventTypeB.name ||
      eventTypeA.headerTemplate !== eventTypeB.headerTemplate ||
      eventTypeA.detailTemplate !== eventTypeB.detailTemplate ||
      eventTypeA.tokens.length !== eventTypeB.tokens.length
    )
      return true;

    // Check tokens
    const tokenMapA = eventTypeA.tokens.reduce((map, token) => {
      map[token.name] = token;
      return map;
    }, {});

    const tokenMapB = eventTypeB.tokens.reduce((map, token) => {
      map[token.name] = token;
      return map;
    }, {});

    for (let i = 0; i < eventTypeA.tokens.length; i++) {
      const tokenA = eventTypeA.tokens[i];
      if (!tokenMapA.hasOwnProperty(tokenA.name)) return true;
      const tokenB = tokenMapB[tokenA.name];
      if (
        tokenA.name !== tokenB.name || //
        tokenA.type !== tokenB.type ||
        tokenA.label !== tokenB.label
      )
        return true;
      if (tokenA.objectPropertyName && tokenA.objectPropertyName !== tokenB.objectPropertyName) return true;
      if (tokenA.options && tokenA.options.length !== tokenB.options.length) return true; // NOTE: We could check option values here instead of just length
    }
    return false;
  }

  /**
   * List timeline event types
   *
   * @see {@link https://developers.hubspot.com/docs/api/crm/timeline}
   * @see {@link https://legacydocs.hubspot.com/docs/methods/timeline/timeline-overview}
   *
   * @returns {[<Object>]} - Returns an array of event types
   */
  async listTimelineEventTypes() {
    try {
      if (!this.appId) throw new Error('Required config `appId` not defined');
      const response = await this.ixHubspotBridge.apiRequest({
        method: 'GET',
        path: `/crm/v3/timeline/${this.appId}/event-templates`,
      });
      return response.results;
    } catch (error) {
      this.logError(error, `Error emitted by listTimelineEventTypes`);
      throw error;
    }
  }

  /**
   * Create a timeline event type
   *
   * @see {@link https://developers.hubspot.com/docs/api/crm/timeline}
   * @see {@link https://legacydocs.hubspot.com/docs/methods/timeline/timeline-overview}
   *
   * @param {Object} params
   * @prop  {String} name
   * @prop  {String} headerTemplate - Single line summary. Supports markdown syntax with Handlebars and event-specific data to render HTML
   * @prop  {String} detailTemplate - Markdown syntax with Handlebars and event-specific data to render HTML
   * @prop  {[Object]} tokens - A collection of tokens that can be used as custom properties on the event and to create fully fledged CRM objects.
   *    {
   *      "name": "petName",
   *      "label": "Pet Name",
   *      "type": "string",        // string, number, enumeration, date
   *      "options": []
   *    }
   * @returns {<Object>} - Returns { success: true } if no error is encountered
   */
  async createTimelineEventType(params) {
    try {
      if (!this.appId) throw new Error('Required config `appId` not defined');
      validate.required(params, ['name', 'headerTemplate', 'detailTemplate', 'tokens']);
      const { name, headerTemplate, detailTemplate, tokens } = params;
      const objectType = params.objectType || 'contacts';

      // Prepare the query post Body payload
      const payload = {
        name,
        headerTemplate,
        detailTemplate,
        tokens,
        objectType,
      };

      // Submit to V3 endpoint
      const response = await this.ixHubspotBridge.apiRequest({
        method: 'POST',
        path: `/crm/v3/timeline/${this.appId}/event-templates`,
        body: payload,
      });
      return response;
    } catch (error) {
      this.logError(error, `Error emitted by createTimelineEventType`, params);
      throw error;
    }
  }

  /**
   * Create a timeline event type
   *
   * @see {@link https://developers.hubspot.com/docs/api/crm/timeline}
   * @see {@link https://legacydocs.hubspot.com/docs/methods/timeline/timeline-overview}
   *
   * @prop  {String} applicationId - The ID of the application that the event type was created for.  The eventTypeId used in the request body must line up with this application-id.
   * @prop  {String} name
   * @prop  {String} headerTemplate - Single line summary. Supports markdown syntax with Handlebars and event-specific data to render HTML
   * @prop  {String} detailTemplate - Markdown syntax with Handlebars and event-specific data to render HTML
   * @prop  {[Object]} tokens - A collection of tokens that can be used as custom properties on the event and to create fully fledged CRM objects.
   *    {
   *      "name": "petName",
   *      "label": "Pet Name",
   *      "type": "string",        // string, number, enumeration, date
   *      "options": []
   *    }
   * @returns {<Object>} - Returns { success: true } if no error is encountered
   */
  async updateTimelineEventType(params) {
    try {
      if (!this.appId) throw new Error('Required config `appId` not defined');
      validate.required(params, ['eventTemplateId', 'name', 'headerTemplate', 'detailTemplate', 'tokens']);
      const { eventTemplateId, name, headerTemplate, detailTemplate, tokens } = params;

      // Prepare the query post Body payload
      const payload = {
        id: eventTemplateId,
        name,
        headerTemplate,
        detailTemplate,
        tokens,
      };

      // Submit to V3 endpoint
      const response = await this.ixHubspotBridge.apiRequest({
        method: 'PUT',
        path: `/crm/v3/timeline/${this.appId}/event-templates/${eventTemplateId}`,
        body: payload,
      });
      return response;
    } catch (error) {
      this.logError(error, `Error emitted by updateTimelineEventType`, params);
      throw error;
    }
  }

  /*---------------------------------------------------------------------------------*/
  //                               Timeline Event Creation
  /*---------------------------------------------------------------------------------*/

  /**
   * Requests a fresh access token for client interface used to add Timeline Events based on the ixHubspotBridge event templates
   *
   */
  async ensureAccessToken() {
    // If a access token hasn't been set, request one
    if (!this.hubspotTimeline.accessToken) {
      await this.hubspotTimeline.refreshAccessToken();
    } else {
      // If the access token has expired, request one
      const now = Math.floor(Date.now() / 1000);
      const elapsedTime = now - this.hubspotTimeline.accessTokenUpdatedAt;
      if (elapsedTime > this.hubspotTimeline.accessTokenExpiresIn) await this.hubspotTimeline.refreshAccessToken();
    }
  }

  /**
   * Get a timeline event type by ID
   *
   * @see {@link https://developers.hubspot.com/docs/api/crm/timeline}
   * @see {@link https://legacydocs.hubspot.com/docs/methods/timeline/timeline-overview}
   *
   * @param  {String} params
   * @prop  {String} eventTemplateId - The Hubspot App developers API Key
   * @returns {[<Object>]} - Returns an array of event types
   */

  async getTimelineEventType({ eventTemplateId }) {
    try {
      if (!eventTemplateId) throw new Error('Required param `eventTemplateId` not defined');
      const response = await this.ixHubspotBridge.apiRequest({
        method: 'GET',
        path: `/crm/v3/timeline/${this.appId}/event-templates/${eventTemplateId}`,
      });
      return response;
    } catch (error) {
      this.logError(error, `Error emitted by getTimelineEventType`, eventTemplateId);
      throw error;
    }
  }

  /**
   * Create a timeline event
   *
   * @see {@link https://developers.hubspot.com/docs/api/crm/timeline}
   * @see {@link https://legacydocs.hubspot.com/docs/methods/timeline/timeline-overview}
   *
   * @param {Object} params
   * @prop  {String} eventTemplateId - The Event Type Template ID
   * @prop  {String} objectId - The contact, company, or deal's objectId
   * @prop  {Object} tokens - A collection of tokens that can be used as custom properties on the event and to create fully fledged CRM objects.
   * @prop  {Object} extraData - Markdown syntax with Handlebars and event-specific data to render HTML
   * @returns {<Object>} - Returns { success: true } if no error is encountered
   */
  async createTimelineEvent(params) {
    try {
      validate.required(params, ['eventTemplateId', 'objectId']);
      const { eventTemplateId, objectId, email, tokens, extraData, timelineIFrame } = params;

      // Prepare the query post Body payload
      const payload = {
        eventTemplateId,
        objectId,
        email,
        tokens,
        extraData,
      };

      // Submit to V3 endpoint
      const response = await this.ixHubspotBridge.apiRequest({
        method: 'POST',
        path: `/crm/v3/timeline/events`,
        body: payload,
      });
      return response;
    } catch (error) {
      this.logError(error, `Error emitted by createTimelineEvent`, params);
      throw error;
    }
  }

  /**
   * Create multiple timeline events
   *
   * @see {@link https://developers.hubspot.com/docs/api/crm/timeline}
   * @see {@link https://legacydocs.hubspot.com/docs/methods/timeline/timeline-overview}
   *
   * @param {Object} params
   * @prop  {[Object]} inputs - An array of event create objects
   * @prop  {String} eventTemplateId - The Event Type Template ID
   * @prop  {String} objectId - The contact, company, or deal's objectId
   * @prop  {Object} tokens - A collection of tokens that can be used as custom properties on the event and to create fully fledged CRM objects.
   * @prop  {Object} extraData - Markdown syntax with Handlebars and event-specific data to render HTML
   * @returns {<Object>} - Returns eventId if no error is encountered
   */
  async createTimelineEvents(params) {
    try {
      await this.ensureAccessToken();

      if (!params.inputs) throw new Error('Required param `inputs` not defined');
      const { inputs } = params;
      const payload = {
        inputs,
      };

      // Submit to V3 endpoint
      const response = await this.hubspotTimeline.apiRequest({
        method: 'POST',
        path: `/crm/v3/timeline/events/batch/create`,
        body: payload,
      });
      return response;
    } catch (error) {
      this.logError(error, `Error emitted by createTimelineEvents`, params);
      throw error;
    }
  }

  /**
   * Get a timeline event type by ID
   *
   * @see {@link https://developers.hubspot.com/docs/api/crm/timeline}
   * @see {@link https://legacydocs.hubspot.com/docs/methods/timeline/timeline-overview}
   *
   * @param  {String} params
   * @prop  {String} eventTemplateId - The Hubspot App developers API Key
   * @prop  {String} eventId - The Hubspot App developers API Key
   * @returns {Object} - The timeline event
   */

  async getTimelineEvent(params) {
    try {
      validate.required(params, ['eventTemplateId', 'eventId']);
      const { eventTemplateId, eventId } = params;
      const response = await this.ixHubspotBridge.apiRequest({
        method: 'GET',
        path: `/crm/v3/timeline/events/${eventTemplateId}/${eventId}`,
      });
      return response.results;
    } catch (error) {
      this.logError(error, `Error emitted by getTimelineEvent`, params);
      throw error;
    }
  }

  /**
   * Records an event on one or more CRM objects
   *
   * @see {@link https://developers.hubspot.com/docs/api/crm/timeline}
   * @see {@link https://legacydocs.hubspot.com/docs/methods/timeline/timeline-overview}
   *
   * @param {Object} params
   * @prop  {String} eventName - The name of the event (will be used to lookup the eventTypeId)
   * @prop  {String} contactId - Optional ID of Contact
   * @prop  {String} companyId - Optional ID of Company
   * @prop  {String} dealId - Optional ID of Deal
   * @prop  {Object} tokens - A collection of tokens that can be used as custom properties on the event and to create fully fledged CRM objects.
   * @prop  {Object} extraData - Markdown syntax with Handlebars and event-specific data to render HTML
   * @returns {<Object>} - Returns { success: true } if no error is encountered
   */
  async recordTimelineEvent(params) {
    try {
      // Lazyload the timeline event templates
      if (!this.timelineEvents) await this.buildTimelineEventReference(params);

      if (!params.eventName) throw new Error('Required param `eventName` not defined');

      const { contactId, companyId, dealId, ticketId } = params;
      const inputs = [];

      if (contactId) {
        try {
          inputs.push(this.buildTimelineEventPayload({ objectType: 'contacts', objectId: contactId, ...params }));
        } catch (error) {
          this.logger.warn(`Unable to build TimelineEvent payload for Hubspot contact ${contactId} `, error);
        }
      }

      if (companyId) {
        try {
          inputs.push(this.buildTimelineEventPayload({ objectType: 'companies', objectId: companyId, ...params }));
        } catch (error) {
          this.logger.warn(`Unable to build TimelineEvent payload for Hubspot company ${companyId} `, error);
        }
      }

      if (dealId) {
        try {
          inputs.push(this.buildTimelineEventPayload({ objectType: 'deals', objectId: dealId, ...params }));
        } catch (error) {
          this.logger.warn(`Unable to build TimelineEvent payload for Hubspot deal ${dealId} `, error);
        }
      }

      if (ticketId) {
        try {
          inputs.push(this.buildTimelineEventPayload({ objectType: 'tickets', objectId: ticketId, ...params }));
        } catch (error) {
          this.logger.warn(`Unable to build TimelineEvent payload for Hubspot ticket ${ticketId} `, error);
        }
      }

      return await this.createTimelineEvents({ inputs });
    } catch (error) {
      this.logError(error, `Error emitted by recordEvent`, params);
      throw error;
    }
  }

  /**
   * Builds the create payload of a timeline event
   *
   * @see {@link https://developers.hubspot.com/docs/api/crm/timeline}
   * @see {@link https://legacydocs.hubspot.com/docs/methods/timeline/timeline-overview}
   *
   * @param {Object} params
   * @prop  {String} eventName - The name of the event (will be used to lookup the eventTypeId)
   * @prop  {String} objectType - contact, company, deal
   * @prop  {String} objectId - The ID of the object
   * @prop  {Object} tokens - A collection of tokens that can be used as custom properties on the event and to create fully fledged CRM objects.
   * @prop  {Object} extraData - Markdown syntax with Handlebars and event-specific data to render HTML
   * @returns {<Object>} - Returns { success: true } if no error is encountered
   */
  buildTimelineEventPayload(params) {
    try {
      validate.required(params, ['objectType', 'eventName']);
      const { eventName, objectType, objectId, eventData = {} } = params;

      // Lookup the event details
      if (!Object.prototype.hasOwnProperty.call(this.timelineEvents[objectType], eventName))
        throw new Error(`Could not build create event payload for event template "${objectType}:${eventName}" because eventName was not found in the event reference. Verify that the event name exists in the reference.`);
      const timelineEventTemplate = this.timelineEvents[objectType][eventName];

      // Use provided extra data or use a copy of eventData
      const extraData = params.extraData || { ...params.eventData };

      // If tokens aren't provided, extract them from eventData
      let tokens;
      if (params.tokens) tokens = params.tokens;
      if (!tokens) {
        tokens = {};
        for (let i = 0; i < timelineEventTemplate.tokens.length; i++) {
          const expectedToken = timelineEventTemplate.tokens[i];
          if (!Object.prototype.hasOwnProperty.call(eventData, expectedToken.name)) {
            throw new Error(`Timeline eventData does not contain required token "${expectedToken.name}"`);
          } else {
            tokens[expectedToken.name] = eventData[expectedToken.name];
          }
        }
      }

      // Prepare the payload
      const payload = {
        eventTemplateId: timelineEventTemplate.id,
        objectId,
        tokens,
        extraData,
      };
      return payload;
    } catch (error) {
      this.logError(error, `Error emitted by buildTimelineEventPayload`, params);
      throw error;
    }
  }

  /*---------------------------------------------------------------------------------*/
  //                                    Helpers
  /*---------------------------------------------------------------------------------*/

  /**
   * Helper function that returns a date in the format expected by Hubspot
   *
   * For values that represent a specific date, the complete date format will be used:
   *   YYYY-MM-DD      (e.g. 2020-02-29)
   *
   * For values that represent a specific data and time, the complete data plus hours,
   * minutes, seconds, and a decimal fraction of a second format will be used:
   *   YYYY-MM-DDThh:mm:ss.sTZD      (e.g. 2020-02-29T03:30:17.000Z)
   *
   * Note that all times will be represented in UTC, so the values will always use the
   * UTC designator "Z."
   *
   * @see {@link https://developers.hubspot.com/docs/api/faq#timestamps}
   *
   * @param {Date} date - A Javascript date object
   * @returns {<Object>} - The date format expected by Hubspot
   *
   */
  dateInCrmFormat(date = new Date()) {
    // Set new date to midnight UTC (required by CRM datepicker property)
    const newDate = new Date(date);
    newDate.setUTCHours(0, 0, 0, 0);
    return newDate.getTime();
  }

  /**
   * Helper function that returns the create request payload that accompanies object creation functions
   *
   * @see {@link https://developers.hubspot.com/docs/api/crm}
   *
   * @param {Object} params - A object whose key/value pairs represent values for the create object
   * @prop  {Array[String]} properties - The list of fields that should be returned.
   * @returns {Promise<Object>} - The payload object expected by hubspot api searches for companies
   */
  static buildPayloadWithName(params = {}) {
    if (Object.keys(params).length === 0) throw new Error('Required params not provided');
    const properties = Object.entries(params).reduce((obj, [property, value]) => {
      obj.push({
        name: property,
        value,
      });
      return obj;
    }, []);
    return { properties };
  }

  /**
   * Helper function that returns the create request payload that accompanies object creation functions
   *
   * @see {@link https://developers.hubspot.com/docs/api/crm}
   *
   * @param {Object} params - A object whose key/value pairs represent values for the create object
   * @prop  {Array[String]}  properties - The list of fields that should be returned.
   * @returns {Promise<Object>} - The payload object expected by hubspot api searches for companies
   */
  static buildPayloadWithProperty(params = {}) {
    if (Object.keys(params).length === 0) throw new Error('Required params not provided');
    const properties = Object.entries(params).reduce((obj, [property, value]) => {
      obj.push({
        property,
        value,
      });
      return obj;
    }, []);
    return { properties };
  }

  /**
   * Helper function that converts hubspot objects to the object schema that the App is expecting
   *
   * @prop  {Object} company
   * @returns {Object} - The App's company object
   */
  static transformToAppSchema(crmType, crmId, hubspotRecord) {
    // Flattend the response object properties collection
    const record = Object.entries(hubspotRecord.properties).reduce((obj, [prop, propData]) => {
      if (propData && Object.prototype.hasOwnProperty.call(propData, 'value')) {
        obj[prop] = propData.value;
      } else {
        obj[prop] = propData || undefined;
      }
      return obj;
    }, {});
    // TODO: Try passing in full record and compressing properties instead of extracting it
    // }, hubspotRecord);
    // delete hubspotRecord.properties;

    // Get the associations if they are present
    if (hubspotRecord.associations) record.associations = hubspotRecord.associations;

    // Add the top level record attributes
    record.crm = ADAPTER_NAME;
    record.crmType = crmType;
    record.crmId = crmId;

    return record;
  }

  /**
   * Log an error, ensuring that axios errors are properly handled.
   *
   * @private
   * @param {Error} error - The error object
   * @param {String} message - The message to include
   */
  logError(error, message, data = {}) {
    this.logger.error(message, error, data);
  }
};
