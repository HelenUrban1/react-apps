const axios = require('axios');
const jwt = require('jsonwebtoken');
const { v4: uuid } = require('uuid');
const pick = require('lodash/pick');
const validate = require('../validate');

// TODO: Pulling config in now so we can get productFamilyHandle
const config = require('../../../config')();

const ADAPTER_NAME = 'chargify';

// const TEMP_CHARGIFY_CUSTOMER_REFERENCE = 'ad5aed30-6b10-40a9-88c0-7264c3c27583'; // TODO: Remove this as part of Account Owner / Billing Contact work for IXC-471
// const TEMP_CHARGIFY_CUSTOMER_ID_WITH_INVOICES = '34607180'; // TODO: Remove this as part of Account Owner / Billing Contact work for IXC-471

// TODO: If these are application preferences, move them into billingClient. If they're charfigy preferences, leave them here.
const INVOICES_RESULTS_PER_PAGE_MAX_SUPPORTED = 200;
const INVOICES_REQUESTS_COUNT_MAX = 20; // Count represents INVOICES_RESULTS_PER_PAGE_MAX_SUPPORTED * this value

// Products in Test Env - https://inspectionxpert-test.chargify.com/products
// Values are passed as payload to createSubscription
/* 
// https://help.chargify.com/chargify-overview/chargify-glossary.html#components
"components": [{
  "component_id": 2500,
  "enabled": true          // On-Off Component
  "unit_balance": 20       // Metered Components
  "allocated_quantity": 14 // Quantity-Based Component
}]
*/

/**
 * Helper function that extracts the product family identifier from the given params
 *
 * @private
 * @param {Object} params - The error object
 * @prop {String} productFamilyId - Optional : The numeric ID of the Chargify product family
 * @prop {String} productFamilyHandle - Optional : The string alias of the Chargify product family
 * @prop {Boolean} required - Optional : Whether to raise an error if neither are present. Default is true.
 */
function extractProductFamilyIdentifier(params, required = true) {
  // Require either productId or productHandle
  let productFamilyIdentifier;
  if (params.productFamilyHandle) {
    productFamilyIdentifier = `handle:${params.productFamilyHandle}`;
  } else if (params.productFamilyId) {
    productFamilyIdentifier = params.productFamilyId;
  } else if (required) {
    throw new Error(`Required param(s) are not defined. Provide either productFamilyId or productFamilyHandle.`);
  }
  return productFamilyIdentifier;
}

/**
 * Helper function that extracts the product identifier from the given params
 *
 * @private
 * @param {Object} params - The error object
 * @prop {String} productId - Optional : The numeric ID of the Chargify product
 * @prop {String} productHandle - Optional : The string alias of the Chargify product
 * @prop {Boolean} required - Optional : Whether to raise an error if neither are present. Default is true.
 */
function extractProductIdentifier(params, required = true) {
  // Require either productId or productHandle
  let productIdentifier;
  if (params.productHandle) {
    productIdentifier = `handle:${params.productHandle}`;
  } else if (params.productId) {
    productIdentifier = params.productId;
  } else if (required) {
    throw new Error(`Required param(s) are not defined. Provide either productId or productHandle.`);
  }
  return productIdentifier;
}

/**
 * Helper function that extracts the component identifier from the given params
 *
 * @private
 * @param {Object} params - The error object
 * @prop {String} componentId - Optional : The numeric ID of the Chargify component
 * @prop {String} componentHandle - Optional : The string alias of the Chargify component
 * @prop {Boolean} required - Optional : Whether to raise an error if neither are present. Default is true.
 */
function extractComponentIdentifier(params, required = true) {
  // Require either componentId or componentHandle
  let componentIdentifier;
  if (params.componentHandle) {
    componentIdentifier = `handle:${params.componentHandle}`;
  } else if (params.componentId) {
    componentIdentifier = params.componentId;
  } else if (required) {
    throw new Error(`Required param(s) are not defined. Provide either componentId or componentHandle.`);
  }
  return componentIdentifier;
}

/**
 * Billing client that interfaces with the Chargify service.
 *
 * @see {@link https://reference.chargify.com/v1/basics/introduction}
 * @see {@link https://inspectionxpert.atlassian.net/wiki/spaces/IC/pages/152698893/Subscriptions+Billing}
 */
module.exports = class ChargifyBillingAdapter {
  /**
   * @param {Object} params
   * @prop  {String} apiKey - The Chargify API key
   * @prop  {String} publicKey - The Chargify public key
   * @prop  {String} privateKey - The Chargify private key
   * @prop  {String} siteName - The Chargify site name
   * @prop  {Object} [httpClient] - The HTTP client
   * @prop  {Object} [logger] - The logging utility
   * @return {BillingClient}
   */
  constructor(params) {
    // Validate required fields
    validate.required(params, ['apiKey', 'publicKey', 'privateKey', 'siteName']);

    this.apiKey = params.apiKey;
    this.publicKey = params.publicKey;
    this.privateKey = params.privateKey;
    this.siteName = params.siteName;
    this.logger = params.logger || console;

    // Determine if adapter is using a mock or the real axios
    if (params.httpClient) {
      this.httpClient = params.httpClient;
      this.isMocked = true;
    } else {
      this.httpClient = axios;
      this.isMocked = false;
    }

    // Requests include the IX-specific subdomain per https://reference.chargify.com/v1/basics/overview#url
    this.siteUrlBase = `https://${this.siteName}.chargify.com`;

    // Set auth per https://developer.chargify.com/content/getting-started/authentication.html
    this.basicAuth = { username: this.apiKey, password: 'x' };

    this.adapter = {
      name: ADAPTER_NAME,
      siteName: this.siteName,
      isMocked: this.isMocked,
    };

    // Billing systems can be setup to track MRR or ARR
    this.subscription_value_is_arr = params.subscription_value_is_arr || false;
  }

  /**
   * Retrieve details about this adapter
   *   *
   * @returns {<Object>} - A JS object containing the name of the adapter and its mock status
   *
   */
  getMetadata() {
    return {
      name: ADAPTER_NAME,
      siteName: this.siteName,
      isMocked: this.isMocked,
    };
  }

  /**
   * Returns true or false reflecting whether the adapter is connected to a test site
   *
   * @returns {Boolean} - Whether the adapter is connected to a test site
   *
   */
  isTestSite() {
    return /test/gi.test(this.siteName);
  }

  /**
   * Returns the URL of the customer record in Chargify
   *
   * @param   {String} recordId - The Chargify customer's record ID
   * @returns {String} - The full URL of the customer record in Chargify
   *
   */
  getVendorUrlForCustomerWithId(recordId) {
    return `https://${this.siteName}.chargify.com/customers/${recordId}`;
  }

  /**
   * Returns the URL of the subscription record in Chargify
   *
   * @param   {String} recordId - The Chargify subscription's record ID
   * @returns {String} - The full URL of the subscription record in Chargify
   *
   */
  getVendorUrlForSubscriptionWithId(recordId) {
    return `https://${this.siteName}.chargify.com/subscriptions/${recordId}`;
  }

  /**
   * Provide data required to initialize billing transactions through Chargify.js.
   *
   * The security token is a JWT used for requesting the billing token that is
   * returned by Chargify upon successful billing validation by the payment gateway.
   *
   * The security token generated here is required to generate the Chargify
   * billing token. The security token prevents this app from being used to
   * programmatically validate stolen credit card numbers in bulk, as card
   * validation is done as part of payment data submission to the payment
   * gateway (i.e., Stripe).
   *
   * @see {@link https://developer.chargify.com/content/chargify-js/chargify-js.html#security}
   *
   * @param   {String} accountId - The user's account id
   * @returns {Object} - Conforms to AccountBillingInitData schema
   */
  getInitData(accountId) {
    try {
      if (!accountId) {
        throw new Error('Required `accountId` param not defined.');
      }

      const payload = {
        iss: this.publicKey,
        jti: uuid(),
        sub: accountId,
      };
      const options = {
        algorithm: 'HS256',
        noTimestamp: true,
      };
      const securityToken = jwt.sign(payload, this.privateKey, options);

      return {
        securityToken,
        publicKey: this.publicKey,
        siteUrl: this.siteUrlBase,
      };
    } catch (error) {
      this.logError(error, `Error generating billing init data for account ${accountId}.`);
      throw error;
    }
  }

  /*---------------------------------------------------------------------------------*/
  //                                    Customers
  /*---------------------------------------------------------------------------------*/

  /**
   * Create or update a customer.
   *
   * All supported input provided will be passed to the create
   *
   * @param {Object} params
   * @prop  {String} [data.firstName] - The customer's first name
   * @prop  {String} [data.lastName] - The customer's last name
   * @prop  {String} [data.email] - The customer's email address
   * @prop  {String} [data.phoneNumber] - The customer's phone number
   * @returns {Promise<Object>} - The Chargify customer record
   */
  async createOrUpdateCustomer(params) {
    const { email } = params;
    try {
      let customer;
      const matches = await this.findCustomer(email);
      if (matches.length <= 0) {
        customer = await this.createCustomer(params);
      } else {
        customer = await this.updateCustomer({
          id: matches[0].id,
          ...params,
        });
      }
      return customer;
    } catch (error) {
      this.logError(error, `Error creating or updating chargify customer with email ${email}.`);
      throw error;
    }
  }

  /**
   * Create a customer
   *
   * All supported input provided will be passed to the create
   *
   * @see {@link https://reference.chargify.com/v1/customers/create-a-customer}
   *
   * @param {Object} params
   * @prop  {String} email
   * @prop  {String} firstName
   * @prop  {String} lastName
   * @returns {Promise<Object>} - The Chargify customer record
   */
  async createCustomer(params) {
    const { email, firstName, lastName, ...properties } = params;

    const url = `${this.siteUrlBase}/customers.json`;
    const payload = {
      customer: {
        // Required props
        email,
        first_name: firstName || '.',
        last_name: lastName || '.',
        // Anything else
        ...properties,
      },
    };
    const options = {
      auth: this.basicAuth,
    };
    try {
      const response = await this.httpClient.post(url, payload, options);
      return response.data.customer;
    } catch (error) {
      this.logError(error, `Error creating chargify customer with email ${email}.`);
      throw error;
    }
  }

  /**
   * Get a customer by its Chargify customer id
   *
   * @see {@link https://reference.chargify.com/v1/customers/read-the-customer-via-chargify-id}
   *
   * @param {String} id - The id of the Chargify customer
   * @returns {Promise<Object>} - The Chargify customer record
   */
  async getCustomerById(id) {
    const url = `${this.siteUrlBase}/customers/${id}.json`;
    const options = {
      auth: this.basicAuth,
    };

    try {
      const response = await this.httpClient.get(url, options);
      return response.data.customer;
    } catch (error) {
      this.logError(error, `Error getting chargify customer with id ${id}.`);
      throw error;
    }
  }

  /**
   * Get a customer by its Chargify reference id
   *
   * @see {@link https://reference.chargify.com/v1/customers/read-the-customer-via-chargify-id}
   *
   * @param {String} reference - The reference of the Chargify customer
   * @returns {Promise<Object>} - The Chargify customer record
   */
  async getCustomerByReference(reference) {
    const url = `${this.siteUrlBase}/customers/lookup.json?reference=${reference}`;
    const options = {
      auth: this.basicAuth,
    };

    try {
      const response = await this.httpClient.get(url, options);
      return response.data.customer;
    } catch (error) {
      this.logError(error, `Error getting chargify customer with reference ${reference}.`);
      throw error;
    }
  }

  /**
   * Get a customer's custom metadata by its Chargify customer id
   *
   * @see {@link https://reference.chargify.com/v1/custom-fields/list-metadata-for-subscriber}
   *
   * @param {String} id - The id of the Chargify customer
   * @returns {Promise<Object>} - The Chargify customer's custom metadata record
   */
  async getMetadataForCustomerWithId(id) {
    const url = `${this.siteUrlBase}/customers/${id}/metadata.json`;
    const options = {
      auth: this.basicAuth,
    };
    try {
      const response = await this.httpClient.get(url, options);
      // console.log('getMetadataForCustomerWithId.response', response);
      const metadata = {};
      if (response.data && response.data.metadata) {
        const list = response.data.metadata;
        for (let i = 0; i < list.length; i++) {
          metadata[list[i].name] = list[i].value;
        }
      }
      return metadata;
    } catch (error) {
      this.logError(error, `Error getting chargify customer with id ${id}.`);
      throw error;
    }
  }

  /**
   * Look up a customer to retrieve the Chargify customer record.
   *   Search by email
   *   Search by Chargify ID
   *   Search by Reference (Your App)
   *   Search by Organization
   *
   * @see {@link https://reference.chargify.com/v1/customers/search-for-customer}
   *
   * @param {String} identifier
   * @returns {Promise<Object>} - The Chargify customer record
   */
  async findCustomer(identifier) {
    const url = `${this.siteUrlBase}/customers.json`;
    const options = {
      auth: this.basicAuth,
      params: {
        q: identifier,
      },
    };

    try {
      const response = await this.httpClient.get(url, options);
      return response.data;
    } catch (error) {
      this.logError(error, `Error getting chargify customer record with ${identifier}.`);
      throw error;
    }
  }

  // Search by email
  // Search by Chargify ID
  // Search by Reference (Your App)
  // Search by Organization

  /**
   * Updates a Chargify customer record.
   *
   * All supported input provided will be used for the update (other than `id`
   * which is required for the API call but cannot itself be changed). To only update
   * some of the customer's data, callers should only pass in the desired fields.
   *
   * @see {@link https://reference.chargify.com/v1/customers/update-the-customer}
   *
   * @param   {Object} data
   * @prop    {String} data.id - The customer's Chargify id. Used to construct the API URL
   * @prop    {String} [data.reference] - The customer's Chargify reference (the UUID of the AccountMember in IXC)
   * @prop    {String} [data.firstName] - The customer's first name
   * @prop    {String} [data.lastName] - The customer's last name
   * @prop    {String} [data.email] - The customer's email address
   * @returns {Promise<Object>} - The Chargify customer record
   */
  async updateCustomer(params) {
    const { id, firstName, lastName, phoneNumber, ...properties } = params;
    try {
      if (!id) {
        throw new Error('Required `id` property not defined.');
      }

      const payload = {
        customer: {
          ...properties,
        },
      };
      // Convert app prop names to API prop names
      if (firstName) payload.customer.first_name = firstName;
      if (lastName) payload.customer.last_name = lastName;
      if (phoneNumber) payload.customer.phone = phoneNumber;

      const options = {
        auth: this.basicAuth,
      };
      const url = `${this.siteUrlBase}/customers/${id}.json`;
      const response = await this.httpClient.put(url, payload, options);
      return response.data.customer;
    } catch (error) {
      this.logError(error, `Error updating chargify customer ${JSON.stringify(params)}.`);
      throw error;
    }
  }

  /*---------------------------------------------------------------------------------*/
  //                          Catalog : Products & Components
  /*---------------------------------------------------------------------------------*/

  async getCatalog(params = {}) {
    try {
      const catalog = {};

      // Get Products
      const products = await this.getProducts(params);
      const productCount = products.length;
      let p;
      for (p = 0; p < productCount; p++) {
        const { product } = products[p];
        if (product) {
          product.prices = await this.getProductPrices({ productId: product.id });

          if (!catalog[product.product_family.handle]) {
            // Add familiy if it doesn't exist
            catalog[product.product_family.handle] = {
              ...product.product_family,
              products: {},
              components: {},
            };

            // Add Components
            // eslint-disable-next-line
            const productComponents = await this.getProductComponents({ productFamilyId: product.product_family.id });
            const productComponentCount = productComponents.length;
            let c;
            for (c = 0; c < productComponentCount; c++) {
              const { component } = productComponents[c];
              if (component) {
                const handle = component.handle || component.name.toLowerCase().replace(/ /g, '_');
                component.prices = await this.getComponentPrices({ componentHandle: handle });
                // Add Component
                catalog[product.product_family.handle].components[component.handle || component.name.toLowerCase().replace(/ /g, '_')] = component;
              }
            }
          }

          // Add Product
          catalog[product.product_family.handle].products[product.handle || product.name.toLowerCase().replace(/ /g, '_')] = product;
        }
      }
      return catalog;
    } catch (error) {
      this.logError(error, `Error getting chargify product catalog.`);
      throw error;
    }
  }

  /**
   * Returns all product families
   *
   * @see {@link https://reference.chargify.com/v1/product-families/list-product-family-via-site}
   *
   * @returns {Promise<Object>} - An array of objects, each of which includes a product_family object
   */
  async getProductFamilies() {
    try {
      const url = `${this.siteUrlBase}/product_families.json`;
      const options = {
        auth: this.basicAuth,
      };
      const response = await this.httpClient.get(url, options);
      return response.data;
    } catch (error) {
      this.logError(error, `Error getting chargify product families.`);
      throw error;
    }
  }

  /**
   * Returns all products
   *
   * @see {@link https://reference.chargify.com/v1/product-families/list-product-family-via-site}
   *
   * @param   {Object} params
   * @prop    {String} [productFamilyId] - Optional id that will filter products so that only those in the product family are returned
   * @returns {Promise<Object>} - An array of objects, each of which includes a product object
   */
  async getProducts(params = {}) {
    try {
      let url;
      const productFamilyIdentifier = extractProductFamilyIdentifier(params, false); // Don't raise error if not present
      if (!productFamilyIdentifier) {
        url = `${this.siteUrlBase}/products.json`;
      } else {
        url = `${this.siteUrlBase}/product_families/${productFamilyIdentifier}/products.json?include_archived=true`;
      }
      const options = {
        auth: this.basicAuth,
      };
      const response = await this.httpClient.get(url, options);
      return response.data;
    } catch (error) {
      this.logError(error, `Error getting chargify products.`);
      throw error;
    }
  }

  /**
   * Returns prices for specified product
   *
   * @see {@link https://reference.chargify.com/v1/products-price-points/read-a-product-price-point}
   *
   * @param   {Object} productId - Id of the product
   * @param   {Object} productHandle - The api handle of the product
   * @returns {Promise<Object>} - An array of price point objects
   */
  async getProductPrices(params = {}) {
    const { productId } = params; // Product prices doesn't accept the handle as an option
    try {
      validate.required(params, ['productId']);
      const url = `${this.siteUrlBase}/products/${productId}/price_points.json`;
      const options = {
        auth: this.basicAuth,
      };
      const response = await this.httpClient.get(url, options);
      return response.data.price_points;
    } catch (error) {
      this.logError(error, `Error getting chargify product prices.`);
      throw error;
    }
  }

  /**
   * Returns all product components in the specified product family
   *
   * @see {@link https://reference.chargify.com/v1/components/list-components-for-a-product-family}
   *
   * @param   {Object} params
   * @prop    {String} [productFamilyId] - Optional id that will filter products so that only those in the product family are returned
   * @returns {Promise<Object>} - An array of objects, each of which includes a product object
   */
  async getProductComponents(params = {}) {
    try {
      const productFamilyIdentifier = extractProductFamilyIdentifier(params);
      const url = `${this.siteUrlBase}/product_families/${productFamilyIdentifier}/components.json?include_archived=false`;
      const options = {
        auth: this.basicAuth,
      };
      const response = await this.httpClient.get(url, options);
      return response.data;
    } catch (error) {
      this.logError(error, `Error getting chargify product components for product family.`);
      throw error;
    }
  }

  /**
   * Lookup a component by its handle
   *
   * @see {@link https://reference.chargify.com/v1/components/lookup-specific-component-by-handle}
   *
   * @param   {Object} params
   * @prop    {String} handle - Handle of the component that should be returned
   * @returns {Promise<Object>} - The matching component
   */
  async getComponent(params = {}) {
    const { handle } = params;
    try {
      validate.required(params, ['handle']);
      const url = `${this.siteUrlBase}/components/lookup.json?handle=${handle}`;
      const options = {
        auth: this.basicAuth,
      };
      const response = await this.httpClient.get(url, options);
      return response.data.component;
    } catch (error) {
      this.logError(error, `Error getting chargify component for handle ${handle}.`);
      throw error;
    }
  }

  /**
   * Returns the set of price points for a product component
   *
   * @see {@link https://reference.chargify.com/v1/components-price-points/components-price-point-intro}
   *
   * @param   {String} componentId - Id of the product component
   * @param   {String} componentHandle - API handle of the product component
   * @returns {Promise<Object>} - An array of price point objects
   */
  async getComponentPrices(params = {}) {
    try {
      // Require either componentId or componentHandle
      const componentIdentifier = extractComponentIdentifier(params);
      const url = `${this.siteUrlBase}/components/${componentIdentifier}/price_points.json`;
      const options = {
        auth: this.basicAuth,
      };
      const response = await this.httpClient.get(url, options);
      return response.data.price_points;
    } catch (error) {
      this.logError(error, `Error getting chargify component prices.`);
      throw error;
    }
  }

  getCurrentPrice(interval, prices, quantity) {
    try {
      validate.required({ interval, prices }, ['interval', 'prices']);
      const pricePoint = prices
        .filter((price) => {
          const { handle } = price;
          return handle && handle.slice(handle.indexOf('_') + 1) === interval.toLowerCase();
        })
        .sort((a, b) => {
          const aDate = new Date(a.handle.slice(1, a.handle.indexOf('_')));
          const bDate = new Date(b.handle.slice(1, b.handle.indexOf('_')));
          return bDate - aDate;
        })
        .slice(0, 1)[0];
      if (!pricePoint) {
        this.logger.warn('Price Point for current interval does not exist');
        return null;
      }
      if (quantity !== undefined && pricePoint.prices) {
        for (let index = 0; index < pricePoint.prices.length; index++) {
          const price = pricePoint.prices[index];
          if (price.starting_quantity <= quantity && (!price.ending_quantity || price.ending_quantity >= quantity)) {
            return price;
          }
        }
        this.logger.warn('Price for current quantity does not exist');
        return null;
      }
      return pricePoint;
    } catch (error) {
      this.logError(error, `Error getting current chargify component price.`);
      throw error;
    }
  }

  /**
   * Get the components associated with a subscription
   *
   * @see {@link https://reference.chargify.com/v1/components/lookup-specific-component-by-handle}
   *
   * @param   {Object} params
   * @prop    {String} subscriptionId - The id of the subscription that holds the components
   * @returns {Promise<Object>} - The matching component
   */
  async getSubscriptionComponents(subscriptionId) {
    try {
      const url = `${this.siteUrlBase}/subscriptions/${subscriptionId}/components.json`;
      const options = {
        auth: this.basicAuth,
      };
      const response = await this.httpClient.get(url, options);
      let returnVal = [];
      if (response.data) {
        returnVal = response.data.filter((item) => item.component.allocated_quantity > 0 || (item.component.component_handle && item.component.component_handle.includes('_addon'))).map((item) => item.component);
      }
      return returnVal;
    } catch (error) {
      this.logError(error, `Error getting chargify components for subscription with id ${subscriptionId}.`);
      throw error;
    }
  }

  /*---------------------------------------------------------------------------------*/
  //                                    Subscriptions
  /*---------------------------------------------------------------------------------*/

  /**
   * Create a subscription for an existing customer.
   *
   * All supported input provided will be used for the update (other than `id`
   * which is required for the API call but cannot itself be changed). To only update
   * some of the customer's data, callers should only pass in the desired fields.
   *
   * @see {@link https://reference.chargify.com/v1/subscriptions/create-subscription}
   *
   * @param {Object} params
   * @prop  {String} params.catalogKey - The product being subscribed to
   * @prop  {String} params.customerId - The customerId of the Chargify customer
   * @prop  {String} [params.billingToken] - Optional value for billing token returned from Chargify
   * @returns {Promise<Object>} - The newly created subscription data
   */
  async createSubscription(params) {
    try {
      const { product, customerId, billingToken, ...data } = params;

      // Validate inputs
      validate.required(params, ['product', 'customerId']);

      // Prepare the payload
      const payload = {
        subscription: {
          // Get product details from catalog

          // TODO: Chargify needs these values
          //     product_handle: 'ix_standard',
          //     product_price_point_handle: 'standard',
          //     components: [
          //       {
          //         component_id: 'handle:user_seat',
          //         price_point_id: 'handle:standard',
          //         allocated_quantity: 1,
          //       },
          //     ],
          ...product,
          product_id: product.id,
          customer_id: customerId,
          // Copy other details like customer_id, reference, and metafields
          ...data,
          payment_collection_method: 'automatic',
        },
      };
      // console.log('payload', payload);

      // If this is a paid trial, pass the token
      if (billingToken) payload.credit_card_attributes = { chargify_token: params.billingToken };

      // const payload = {
      //   subscription: {
      //     product_handle: 'Pro',
      //     // product_id: '482734',
      //     product_price_point_handle: 'v3',
      //     // product_price_point_id: '615342',
      //     coupon_code: '5SNN6HFK3GBH',
      //     payment_collection_method: 'automatic',
      //     receives_invoice_emails: false,
      //     net_terms: '10',
      //     next_billing_at: '1/1/2020',
      //     stored_credential_transaction_id: 166465511220288,
      //     payment_profile_id: '54321',
      //     components: [
      //       {
      //         component_id: 2500,
      //         enabled: true,
      //       },
      //       {
      //         component_id: 195268,
      //         unit_balance: 20,
      //       },
      //       {
      //         component_id: 2500,
      //         allocated_quantity: 14,
      //         price_point_id: 499,
      //       },
      //     ],
      //     reference: 'my-ref-123',
      //     metafields: {
      //       color: 'blue',
      //       comments: 'Thanks!',
      //     },
      //     customer_id: '12345',
      //     customer_attributes: {
      //       first_name: 'Curtis',
      //       last_name: 'Test',
      //       email: 'curtis@example.com',
      //       cc_emails: 'jeff@example.com',
      //       organization: '',
      //       reference: null,
      //       address: '123 Anywhere Street',
      //       address_2: '',
      //       city: 'Boulder',
      //       state: 'CO',
      //       zip: '80302',
      //       country: 'US',
      //       phone: '',
      //       verified: false,
      //       tax_exempt: false,
      //       vat_number: '012345678',
      //     },
      //     calendar_billing: {
      //       snap_day: 1,
      //       calendar_billing_first_charge: 'immediate',
      //     },
      //     credit_card_attributes: {
      //       chargify_token: 'tok_cwhvpfcnbtgkd8nfkzf9dnjn',
      //       id: 10191713,
      //       payment_type: 'credit_card',
      //       first_name: 'Curtis',
      //       last_name: 'Test',
      //       masked_card_number: 'XXXX-XXXX-XXXX-1',
      //       full_number: '',
      //       card_type: 'bogus',
      //       expiration_month: 1,
      //       expiration_year: 2026,
      //       billing_address: '123 Anywhere Street',
      //       billing_address_2: '',
      //       billing_city: 'Boulder',
      //       billing_state: null,
      //       billing_country: '',
      //       billing_zip: '80302',
      //       current_vault: 'bogus',
      //       vault_token: '1',
      //       customer_vault_token: '1234',
      //       customer_id: 14714298,
      //       paypal_email: 'amelia@example.com',
      //       payment_method_nonce: 'abc123',
      //     },
      //     bank_account_attributes: {
      //       chargify_token: 'tok_cwhvpfcnbtgkd8nfkzf9dnjn',
      //       bank_name: 'Best Bank',
      //       bank_routing_number: '021000089',
      //       bank_account_number: '111111111111',
      //       bank_account_type: 'checking',
      //       bank_branch_code: '00006',
      //       bank_iban: 'FR1420041010050500013M02606',
      //       bank_account_holder_type: 'business',
      //       payment_type: 'bank_account',
      //     },
      //   },
      // };

      // Send the responses
      const options = { auth: this.basicAuth };
      const url = `${this.siteUrlBase}/subscriptions.json`;
      const response = await this.httpClient.post(url, payload, options);
      return response.data.subscription;
    } catch (error) {
      this.logError(error, `Error creating subscription.`);
      throw error;
    }
  }

  /**
   * Look up all Chargify subscription records.
   *
   * @see {@link https://reference.chargify.com/v1/subscriptions/read-subscription}
   *
   * @param {Object} params
   * @prop {Integer} page - Result records are organized in pages. By default, the first page of results is displayed. The page parameter specifies a page number of results to fetch. You can start navigating through the pages to consume the results. You do this by passing in a page parameter. Retrieve the next page by adding ?page=2 to the query string. If there are no results to return, then an empty result set will be returned.
   * @prop {Integer} per_page - This optional parameter indicates how many records to fetch in each request. Default value is 20. The maximum allowed values is 200; any per_page value over 200 will be changed to 200.
   * @prop {String} state - The current state of the subscription.
   * @prop {String} product - The product id of the subscription. (Note that the product handle cannot be used.)
   * @prop {Integer} product_price_point_id - The ID of the product price point. If supplied, product is required
   * @prop {String} coupon - The numeric id of the coupon currently applied to the subscription. (This can be found in the URL when editing a coupon. Note that the coupon code cannot be used.)
   * @prop {String} date_field - The type of filter you'd like to apply to your search.
   * @prop {String} start_date - The start date (format YYYY-MM-DD) with which to filter the date_field. Returns subscriptions with a timestamp at or after midnight (12:00:00 AM) in your site’s time zone on the date specified
   * @prop {String} end_date - The end date (format YYYY-MM-DD) with which to to filter the date_field. Returns subscriptions with a timestamp up to and including 11:59:59PM in your site’s time zone on the date specified
   * @prop {String} start_datetime - The start date and time (format YYYY-MM-DD HH:MM:SS) with which to filter the date_field. Returns subscriptions with a timestamp at or after exact time provided in query. You can specify timezone in query - otherwise your site's time zone will be used. If provided, this parameter will be used instead of start_date.
   * @prop {String} end_datetime - The end date and time (format YYYY-MM-DD HH:MM:SS) with which to filter the date_field. Returns subscriptions with a timestamp at or before exact time provided in query. You can specify timezone in query - otherwise your site's time zone will be used. If provided, this parameter will be used instead of end_date.
   * @prop {String} metadata[Field Name] - The value of the metadata field specified in the parameter
   * @prop {String} direction - Controls the order in which results are returned. Can either be asc for ascending or desc for descending
   * @prop {String} sort - Allowed sort keys. Allowed Values: signup_date, period_start, period_end, next_assessment, updated_at, created_at
   * @returns {Promise<Object>} - An array of Chargify subscription records
   */
  async getSubscriptions(params = {}) {
    try {
      let qs = [];
      Object.entries(params).forEach(([key, value]) => {
        if (key === 'per_page' && value > 200) {
          throw new Error(`${key} must be an integer less than or equal to 200 but was ${value}`);
        }
        if (key === 'state' && !['active', 'canceled', 'expired', 'expired_cards', 'on_hold', 'past_due', 'pending_cancellation', 'pending_renewal', 'suspended', 'trial_ended', 'trialing', 'unpaid'].includes(value)) {
          throw new Error();
        }
        if ((key === 'start_date' || key === 'end_date') && !/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/.test(value)) {
          throw new Error(`${key} must be in YYYY-MM-DD format but was ${value}`);
        }
        if ((key === 'start_datetime' || key === 'end_datetime') && !/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1]) (2[0-3]|[01][0-9]):[0-5][0-9]:[0-5][0-9]$/.test(value)) {
          throw new Error(`${key} must be in YYYY-MM-DD HH:MM:SS format but was ${value}`);
        }
        if (key === 'sort' && !['signup_date', 'period_start', 'period_end', 'next_assessment', 'updated_at', 'created_at'].includes(value)) {
          throw new Error();
        }
        if (key === 'direction' && value !== 'asc' && value !== 'desc') {
          throw new Error(`${key} must be asc or desc but was ${value}`);
        }
        qs.push(`${key}=${value}`);
      });
      qs = qs.join('&');

      const url = `${this.siteUrlBase}/subscriptions.json?${qs}`;
      const options = {
        auth: this.basicAuth,
      };
      const response = await this.httpClient.get(url, options);

      return response.data;
    } catch (error) {
      this.logError(error, `Error emitted by getSubscriptions`, params);
      throw error;
    }
  }

  /**
   * Look up a subscription to retrieve the Chargify subscription record.
   *
   * @see {@link https://reference.chargify.com/v1/subscriptions/read-subscription}
   *
   * @param {String} id - The Chargify subscription ID
   * @returns {Promise<Object>} - The Chargify subscription record
   */
  async getSubscriptionById(id) {
    try {
      const url = `${this.siteUrlBase}/subscriptions/${id}.json`;
      const options = {
        auth: this.basicAuth,
      };
      const response = await this.httpClient.get(url, options);
      return response.data.subscription;
    } catch (error) {
      this.logError(error, `Error getting chargify subscription with id ${id}.`);
      throw error;
    }
  }

  /**
   * Look up a subscription to retrieve the Chargify subscription record.
   *
   * @see {@link https://reference.chargify.com/v1/subscriptions/lookup-subscription}
   *
   * @param {String} reference - The reference ID for the Chargify subscription ID
   * @returns {Promise<Object>} - The Chargify subscription record
   */
  async getSubscriptionByReference(reference) {
    const url = `${this.siteUrlBase}/subscriptions/lookup.json`;
    const options = {
      auth: this.basicAuth,
      params: {
        reference,
      },
    };

    try {
      const response = await this.httpClient.get(url, options);
      return response.data;
    } catch (error) {
      this.logError(error, `Error getting chargify subscription with reference ${reference}.`);
      throw error;
    }
  }

  /**
   * Get the subscriptions associated with a customer.
   *
   * @see {@link https://reference.chargify.com/v1/subscriptions/list-by-customer}
   *
   * @param {String} customerId
   * @returns {Promise<Array<Object>>} - A list of subscriptions
   */
  // TODO: This functionality is duplicated in the subscription worker
  async getSubscriptionsForCustomer(customerId) {
    try {
      const url = `${this.siteUrlBase}/customers/${customerId}/subscriptions.json`;
      const options = {
        auth: this.basicAuth,
      };
      const response = await this.httpClient.get(url, options);

      // Remove the self-name nested object
      const result = response.data.reduce((collection, member) => {
        if (member.subscription.product.product_family.handle === config.product.familyHandle) collection.push(member.subscription);
        return collection;
      }, []);

      return result;
    } catch (error) {
      this.logError(error, `Error getting chargify subscriptions for chargify customer ${customerId}.`);
      throw error;
    }
  }

  /**
   * Updates a Chargify subscription record.
   *
   * All supported input provided will be used for the update (other than `id`
   * which is required for the API call but cannot itself be changed). To only update
   * some of the subscription's data, callers should only pass in the desired fields.
   *
   * @see {@link https://reference.chargify.com/v1/subscriptions/update-subscription}
   *
   * @param   {Object} params
   * @prop    {String} data.id - The subscription's Chargify id. Used to construct the API URL
   * @returns {Promise<Object>} - The Chargify subscription record
   */
  async updateSubscription(params) {
    // Extract the ID and values that are used to build the PUT payload
    const { id, product, billingToken, ...data } = params;
    // Extract attributes that can only be set using the override endpoint /subscriptions/{subscription_id}/override.json
    // eslint-disable-next-line
    const { activated_at, canceled_at, cancellation_message, expires_at, ...remainingData } = data;
    try {
      if (!id) {
        throw new Error('Required `id` property not defined.');
      }

      // Prepare to call multiple endpoints
      let url;
      let response;
      let result;
      const options = {
        auth: this.basicAuth,
      };

      let payload = {
        subscription: {
          ...remainingData,
        },
      };

      // If we're updating products
      if (product) {
        // If this is a paid trial, pass the token
        payload.subscription = {
          ...product,
          ...remainingData,
        };
      }

      // If a billing token is present, add it
      if (billingToken) payload.subscription.credit_card_attributes = { chargify_token: params.billingToken };

      // All other attributes can be pushed to regular subscription endpoint
      if (Object.entries(payload.subscription).length > 0) {
        url = `${this.siteUrlBase}/subscriptions/${id}.json`;
        response = await this.httpClient.put(url, payload, options);
        result = response.data.subscription;
        // console.log('standard response', response.data);
      }

      // canceled_at
      // can be used to record an external cancellation date.
      // Chargify sets this field automatically when a subscription is canceled,
      // whether by request or via dunning.

      // expires_at
      // stores an external expiration date.This field is set automatically
      // when a subscription expires(ceases billing) after a prescribed amount of time.

      const overrides = pick(params, ['activated_at', 'canceled_at', 'cancellation_message', 'expires_at']);
      if (Object.entries(overrides).length > 0) {
        url = `${this.siteUrlBase}/subscriptions/${id}/override.json`;
        payload = {
          subscription: {
            ...overrides,
          },
        };
        // console.log('override payload', payload);
        response = await this.httpClient.put(url, payload, options);
        // Override endpoint doesn't return the subscription, so get it
        result = await this.getSubscriptionById(id);
        // console.log('override response', response);
      }

      return result;
    } catch (error) {
      this.logError(error, `Error updating chargify subscription ${JSON.stringify(params)}.`);
      throw error;
    }
  }

  /**
   * Updates a Chargify subscription record.
   *
   * All supported input provided will be used for the update (other than `id`
   * which is required for the API call but cannot itself be changed). To only update
   * some of the subscription's data, callers should only pass in the desired fields.
   *
   * @see {@link https://reference.chargify.com/v1/subscriptions/update-subscription}
   *
   * @param   {Object} params
   * @prop    {String} id - The subscription's Chargify id. Used to construct the API URL
   * @prop    {String} componentHandle - The components API handle (string alias)
   * @prop    {String} componentId - The components numeric ID
   * @prop    {Integer} newQuantity - The new quantity allocated
   * @prop    {String} memo - A memo to apply to the change log
   * @returns {Promise<Object>} - The Chargify subscription record
   */
  async updateSubscriptionQuantity(params) {
    // Extract the ID and values that are used to build the PUT payload
    const { id, newQuantity, memo, upgrade, accrue, pricePoint } = params;
    try {
      // Validate inputs
      validate.required(params, ['id', 'newQuantity']);

      // Require either componentId or componentHandle
      const componentIdentifier = extractComponentIdentifier(params);

      // Build the payload
      const payload = {
        allocation: {
          quantity: newQuantity,
          upgrade_charge: upgrade,
          accrue_charge: accrue,
          price_point_id: pricePoint,
        },
      };
      if (memo) payload.allocation.memo = memo;

      // Post to the endpoint
      const url = `${this.siteUrlBase}/subscriptions/${id}/components/${componentIdentifier}/allocations.json`;
      const options = {
        auth: this.basicAuth,
      };
      const response = await this.httpClient.post(url, payload, options);

      return response.data.allocation;
    } catch (error) {
      this.logError(error, `Error updating chargify subscription ${JSON.stringify(params)}.`);
      throw error;
    }
  }

  /**
   * Updates a Chargify subscription record.
   *
   * All supported input provided will be used for the update (other than `id`
   * which is required for the API call but cannot itself be changed). To only update
   * some of the subscription's data, callers should only pass in the desired fields.
   *
   * @see {@link https://reference.chargify.com/v1/subscriptions/update-subscription}
   *
   * @param   {Object} params
   * @prop    {String} id - The subscription's Chargify id. Used to construct the API URL
   * @prop    {String} componentHandle - The components API handle (string alias)
   * @prop    {String} componentId - The components numeric ID
   * @prop    {Integer} newQuantity - The new quantity allocated
   * @prop    {String} memo - A memo to apply to the change log
   * @returns {Promise<Object>} - The Chargify subscription record
   */
  async updateComponentPrice(params) {
    // Extract the ID and values that are used to build the PUT payload
    const { id, componentId, pricePoint } = params;
    try {
      // Validate inputs
      validate.required(params, ['id', 'componentId', 'pricePoint']);

      // TODO

      // Post to the endpoint
      const url = `${this.siteUrlBase}/subscriptions/${id}/price_points.json`;
      const options = {
        auth: this.basicAuth,
      };
      const payload = {
        components: [
          {
            component_id: componentId,
            price_point: pricePoint,
          },
        ],
      };
      const response = await this.httpClient.post(url, payload, options);

      return response.data.components;
    } catch (error) {
      this.logError(error, `Error updating chargify subscription ${JSON.stringify(params)}.`);
      throw error;
    }
  }

  /**
   * Cancel a subscription but maintain access until the end of the billing period
   *
   * @see {@link https://reference.chargify.com/v1/subscriptions-cancellations/cancel-subscription-delayed-method-1}
   *
   * @prop  {String} subscriptionId - The subscription id
   * @returns {Promise<Boolean>} - Set to true for success and false otherwise
   */
  async delayedCancelSubscription(subscriptionId) {
    const url = `${this.siteUrlBase}/subscriptions/${subscriptionId}/delayed_cancel.json`;
    const payload = {
      resume: true,
    };
    const options = {
      auth: this.basicAuth,
    };

    try {
      const response = await this.httpClient.post(url, payload, options);

      // NOTE: The API docs at the time of this implementation incorrectly note the response
      // status for a success as being 201 instead of the 200 that is actually returned. To
      // protect against the API being changed to match its documentation, the success check
      // is determined based on the status code being 2XX.
      return response.status >= 200 && response.status <= 299;
    } catch (error) {
      this.logError(error, `Error canceling chargify subscription with id ${subscriptionId}.`);
      throw error;
    }
  }

  /**
   * Immediately cancel a subscription skipping the Pending Cancellation period
   *
   * @see {@link https://reference.chargify.com/v1/subscriptions-cancellations/cancel-subscription}
   *
   * @prop  {String} subscriptionId - The subscription id
   * @returns {Promise<Boolean>} - Set to true for success and false otherwise
   */
  async immediatelyCancelSubscription(subscriptionId) {
    const url = `${this.siteUrlBase}/subscriptions/${subscriptionId}.json`;

    const options = {
      auth: this.basicAuth,
    };

    try {
      const response = await this.httpClient.delete(url, options);

      return response.status >= 200 && response.status <= 299;
    } catch (error) {
      this.logError(error, `Error canceling the chargify subscription with id ${subscriptionId}.`);
      throw error;
    }
  }

  /**
   * Immediately cancel a subscription skipping the Pending Cancellation period
   *
   * @see {@link https://reference.chargify.com/v1/subscriptions-cancellations/cancel-subscription-remove-delayed-method}
   *
   * @prop  {String} subscriptionId - The subscription id
   * @returns {Promise<Boolean>} - Set to true for success and false otherwise
   */
  async removeDelayedCancel(subscriptionId) {
    const url = `${this.siteUrlBase}/subscriptions/${subscriptionId}/delayed_cancel.json`;

    const options = {
      auth: this.basicAuth,
    };

    try {
      const response = await this.httpClient.delete(url, options);

      return response.status >= 200 && response.status <= 299;
    } catch (error) {
      this.logError(error, `Error removing the delayed cancellation of chargify subscription ${subscriptionId}.`);
      throw error;
    }
  }

  /**
   * Reactivate a canceled (not pending) subscription
   *
   * @see {@link https://reference.chargify.com/v1/subscriptions-reactivations/reactivate-subscription}
   *
   * @prop  {String} subscriptionId - The subscription id
   * @returns {Promise<Boolean>} - Set to true for success and false otherwise
   */
  async reactivateSubscription(subscriptionId) {
    const url = `${this.siteUrlBase}/subscriptions/${subscriptionId}/reactivate.json`;

    // only reactivates the subscription if it can resume the current billing period
    // prevents this from being used in a scenario where reactivation requires a payment
    const payload = {
      resume: { require_resume: true },
    };

    const options = {
      auth: this.basicAuth,
    };

    try {
      const response = await this.httpClient.put(url, payload, options);
      return response.status >= 200 && response.status <= 299;
    } catch (error) {
      this.logError(error, `Error reactivating the chargify subscription ${subscriptionId}.`);
      throw error;
    }
  }

  /**
   * Reactivate a trial_ended subscription
   *
   * @see {@link https://reference.chargify.com/v1/subscriptions-reactivations/reactivate-subscription}
   *
   * @prop  {String} subscriptionId - The subscription id
   * @returns {Promise<Boolean>} - Set to true for success and false otherwise
   */
  async reactivateEndedTrial(subscriptionId) {
    const url = `${this.siteUrlBase}/subscriptions/${subscriptionId}/reactivate.json`;
    const options = {
      auth: this.basicAuth,
    };

    try {
      const response = await this.httpClient.put(url, {}, options);
      return response.status >= 200 && response.status <= 299;
    } catch (error) {
      this.logError(error, `Error reactivating the chargify subscription ${subscriptionId}.`);
      throw error;
    }
  }

  /**
   * Change the collection method for a subscription.
   *
   * @see {@link https://reference.chargify.com/v1/subscriptions/update-subscription#update-subscription}
   *
   * @prop  {String} subscriptionId - The subscription id
   * @prop  {Object} subscriptionInfo - The payment method id
   * @returns {Promise<Boolean>} - Set to true for success and false otherwise
   */
  async changeCollectionMethod(subscriptionId, subscriptionInfo) {
    const url = `${this.siteUrlBase}/subscriptions/${subscriptionId}.json`;
    const payload = { subscription: subscriptionInfo };
    const options = {
      auth: this.basicAuth,
    };
    try {
      const response = await this.httpClient.put(url, payload, options);

      // NOTE: The API docs at the time of this implementation incorrectly note the response
      // status for a success as being 201 instead of the 200 that is actually returned. To
      // protect against the API being changed to match its documentation, the success check
      // is determined based on the status code being 2XX.
      return response.status >= 200 && response.status <= 299;
    } catch (error) {
      this.logError(error, `Error changing collection method to ${subscriptionInfo.payment_collection_method} for subscription ${subscriptionId}.`);
      throw error;
    }
  }

  /**
   * Permantly hard delete a subscription (works in test accounts only)
   *
   * @see {@link https://reference.chargify.com/v1/subscriptions/purge-subscription}
   *
   * @param {Object} params
   * @prop  {String} subscriptionId
   * @prop  {String} customerId
   * @prop  {Boolean} cascade - Whether to delete the customer record and payment profile data as well. Default is true.
   * @returns {Promise<Object>} - Response contains the subscription that was purged
   */
  async purgeSubscription(params) {
    const { subscriptionId, customerId, cascade = true } = params;
    try {
      // Validate that his only being attempted in a test site
      if (!this.siteUrlBase.includes('test')) {
        throw new Error(`Purging a subscription and user is only permitted in sites with the word "test" in their name. This request was submitted to ${this.siteUrlBase}.`);
      }
      validate.required(params, ['subscriptionId', 'customerId']);

      const cascadeQuerystring = cascade ? '&cascade[]=customer&cascade[]=payment_profile' : '';
      const url = `${this.siteUrlBase}/subscriptions/${subscriptionId}/purge.json?ack=${customerId}${cascadeQuerystring}`;
      const payload = {};
      const options = {
        auth: this.basicAuth,
      };

      const response = await this.httpClient.post(url, payload, options);
      return response.data.subscription;
    } catch (error) {
      this.logError(error, `Error purging subscription ${subscriptionId} of customer ${customerId}`);
      throw error;
    }
  }

  /*---------------------------------------------------------------------------------*/
  //                                    Custom Fields
  /*---------------------------------------------------------------------------------*/

  /**
   *
   * @param {String} subscriptionId // The ID for the Chargify Subscription
   */
  async getSubscriptionCustomFields(subscriptionId) {
    if (!subscriptionId) {
      this.logError('missing parameter to fetch custom fields');
      throw new Error('missing parameter to fetch custom fields');
    }
    try {
      const url = `${this.siteUrlBase}/subscriptions/${subscriptionId}/metadata.json`;
      const options = {
        auth: this.basicAuth,
      };
      const response = await this.httpClient.get(url, options);
      return response.data.metadata;
    } catch (error) {
      this.logError(error, `Error getting chargify custom fields for subscription id ${subscriptionId}.`);
      throw error;
    }
  }

  /**
   *
   * @param {String} subscriptionId // The ID for the Chargify Subscription
   * @param {Array} changes // Array of Custom Fields to change
   *  {
   *   name: string // Name of Custom Field to change
   *   value: string // New Custom Field Value
   *  }
   */
  async updateSubscriptionCustomFields(params) {
    const { subscriptionId, changes } = params;

    try {
      // Validate inputs
      validate.required(params, ['subscriptionId', 'changes']);

      // Prepare Change Payload
      const metadata = [];
      for (let index = 0; index < changes.length; index++) {
        const customField = changes[index];
        metadata.push({
          name: customField.name,
          value: customField.value,
        });
      }
      const payload = {
        metadata,
      };

      // Send the request
      const options = { auth: this.basicAuth };
      const url = `${this.siteUrlBase}/subscriptions/${subscriptionId}/metadata.json`;
      const response = await this.httpClient.put(url, payload, options);
      return response.data;
    } catch (error) {
      this.logError(error, `Error updating custom fields for subscription id ${subscriptionId}.`);
      throw error;
    }
  }

  /**
   *
   * @param {String} subscriptionId // The ID for the Chargify Subscription
   * @param {Array} changes // Array of Custom Field Names to change
   *  name: string // Name of Custom Field to change
   */
  async deleteSubscriptionCustomFields(params) {
    const { subscriptionId, changes } = params;

    try {
      // Validate inputs
      validate.required(params, ['subscriptionId', 'changes']);

      // Send the request
      const options = { auth: this.basicAuth };
      let url = `${this.siteUrlBase}/subscriptions/${subscriptionId}/metadata.json?`;
      for (let index = 0; index < changes.length; index++) {
        const element = changes[index];
        if (index !== 0) {
          url += '&';
        }
        url += `names[]=${element}`;
      }
      const response = await this.httpClient.delete(url, options);
      return response;
    } catch (error) {
      this.logError(error, `Error deleting custom fields for subscription id ${subscriptionId}.`);
      throw error;
    }
  }

  /*---------------------------------------------------------------------------------*/
  //                                    Payments
  /*---------------------------------------------------------------------------------*/

  /**
   *
   * @param {String} paymentID // ID of the new payment to remove
   * @param {String} subscriptionID // ID of the chargify subscription
   * @param {String} previousPayment  // (optional) ID of the previously used payment, if it needs to be rolled back in chargify
   */
  async rollbackAddPayment(paymentID, subscriptionID, previousPayment) {
    try {
      const url = `${this.siteUrlBase}/subscriptions/${subscriptionID}/payment_profiles/${paymentID}.json`;

      const payload = {};
      const options = {
        auth: this.basicAuth,
      };

      const response = await this.httpClient.delete(url, payload, options);
      if (!response) {
        throw new Error('Error removing payment method');
      }
      if (previousPayment) {
        await this.changeDefaultPaymentMethod(subscriptionID, previousPayment);
      }
    } catch (error) {
      this.logError(error, `Error rolling back new payment method.`);
      throw error;
    }
  }

  /**
   * Add a payment method for an existing customer and set it as their default
   * payment method.
   *
   * @see {@link https://reference.chargify.com/v1/payment-profiles/create-a-payment-profile}
   *
   * @param {Object} params
   * @prop  {String} customerId - The id of the Chargify customer
   * @prop  {String} billingToken - The billing token returned from Chargify
   * @returns {Promise<Object>} - The newly created payment method  data
   */
  async addPaymentMethod(params) {
    try {
      // Validate inputs
      validate.required(params, ['customerId', 'billingToken']);

      const url = `${this.siteUrlBase}/payment_profiles.json`;

      const payload = {
        payment_profile: {
          customer_id: params.customerId,
          chargify_token: params.billingToken,
        },
      };
      // console.log(payload);
      const options = {
        auth: this.basicAuth,
      };

      const response = await this.httpClient.post(url, payload, options);

      if (!response) {
        throw new Error('Error adding new payment method');
      }

      // NOTE: The API docs at the time of this implementation incorrectly note the response
      // object as `payment` instead of `payment_profile`. The documented `payment` object
      // contains fields such as `card_number` that for compliance reasons cannot be consumed
      // by this app, but the `payment_profile` object contains a `masked_card_number` field
      // that includes only the last 4 digits of the card number.
      const paymentProfile = response.data.payment_profile;

      // Adding the payment method only stores it with Chargify. Make a followup request
      // to set the new payment method as default.
      let update;
      let previousPayment;
      if (params.subscription) {
        const { subscription } = params;
        previousPayment = subscription.credit_card ? subscription.credit_card.id : null;
        const result = await this.changeDefaultPaymentMethod(subscription.id, paymentProfile.id);

        if (!result) {
          // rollback add payment
          await this.rollbackAddPayment(paymentProfile.id, subscription.id);
          throw new Error(`Error setting new payment method ${paymentProfile.id} as default`);
        }
        const updateInfo = {
          next_billing_at: subscription.next_billing_at,
          product_handle: subscription.product_handle,
          product_id: subscription.product_id,
          payment_collection_method: 'automatic',
          receives_invoice_emails: subscription.receives_invoice_emails,
          net_terms: subscription.net_terms,
          stored_credential_transaction_id: subscription.stored_credential_transaction_id,
        };
        const successful = await this.changeCollectionMethod(subscription.id, updateInfo);

        if (!successful) {
          // rollback add payment
          await this.rollbackAddPayment(paymentProfile.id, subscription.id, previousPayment);
          throw new Error(`Error setting new payment method ${paymentProfile.id} as default`);
        }

        update = {
          ...subscription,
          payment_collection_method: 'automatic',
          credit_card: { ...paymentProfile },
        };
      } else {
        const subscriptions = await this.getSubscriptionsForCustomer(params.customerId);

        for (let i = 0; i < subscriptions.length; i++) {
          // for (const item of subscriptions) {
          const subscription = subscriptions[i];
          previousPayment = subscription.credit_card?.id || null;
          const result = await this.changeDefaultPaymentMethod(subscription.id, paymentProfile.id);

          if (!result) {
            // rollback add payment
            await this.rollbackAddPayment(paymentProfile.id, subscription.id);
            throw new Error(`Error setting new payment method ${paymentProfile.id} as default`);
          }
          const updateInfo = {
            next_billing_at: subscription.next_billing_at,
            product_handle: subscription.product_handle,
            product_id: subscription.product_id,
            payment_collection_method: 'automatic',
            receives_invoice_emails: subscription.receives_invoice_emails,
            net_terms: subscription.net_terms,
            stored_credential_transaction_id: subscription.stored_credential_transaction_id,
          };
          const successful = await this.changeCollectionMethod(subscription.id, updateInfo);

          if (!successful) {
            // rollback add payment
            await this.rollbackAddPayment(paymentProfile.id, subscription.id, previousPayment);
            throw new Error(`Error setting new payment method ${paymentProfile.id} as default`);
          }
          update = {
            ...subscription,
            payment_collection_method: 'automatic',
            credit_card: { ...paymentProfile },
          };
        }

        const customFields = await this.getSubscriptionCustomFields(update.id);
        if (customFields) {
          for (let index = 0; index < customFields.length; index++) {
            const field = customFields[index];
            update[field.name] = field.value;
          }
        }
      }

      return { update, previousPayment };
    } catch (error) {
      this.logError(error, `Error adding payment method ${params.billingToken} for customer ${params.customerId}.`);
      throw error;
    }
  }

  /**
   * Change the default payment method for a subscription.
   *
   * @see {@link https://reference.chargify.com/v1/payment-profiles/change-default-payment-profile}
   *
   * @prop  {String} subscriptionId - The subscription id
   * @prop  {String} paymentMethodId - The payment method id
   * @returns {Promise<Boolean>} - Set to true for success and false otherwise
   */
  async changeDefaultPaymentMethod(subscriptionId, paymentMethodId) {
    const url = `${this.siteUrlBase}/subscriptions/${subscriptionId}/payment_profiles/${paymentMethodId}/change_payment_profile.json`;
    const payload = {};
    const options = {
      auth: this.basicAuth,
    };

    try {
      const response = await this.httpClient.post(url, payload, options);

      // NOTE: The API docs at the time of this implementation incorrectly note the response
      // status for a success as being 201 instead of the 200 that is actually returned. To
      // protect against the API being changed to match its documentation, the success check
      // is determined based on the status code being 2XX.
      return response.data.payment_profile;
    } catch (error) {
      this.logError(error, `Error changing default payment method to ${paymentMethodId} for chargify subscription ${subscriptionId}.`);
      throw error;
    }
  }

  /**
   * Remove a payment method for an existing customer and set their account to remittance
   * payment method.
   *
   * @see {@link https://reference.chargify.com/v1/payment-profiles/create-a-payment-profile}
   *
   * @param {Object} data
   * @prop  {String} data.billingToken - The billing token returned from Chargify
   * @returns {Promise<Object>} - The newly created payment method  data
   */
  async removePaymentMethod(customer) {
    try {
      // Adding the payment method only stores it with Chargify. Make a followup request
      // to set the new payment method as default.
      const subscriptions = await this.getSubscriptionsForCustomer(customer);
      let update;

      for (let i = 0; i < subscriptions.length; i++) {
        // for (const subscription of subscriptions) {
        const subscription = subscriptions[i];
        const updateInfo = {
          next_billing_at: subscription.next_billing_at,
          product_handle: subscription.product_handle,
          product_id: subscription.product_id,
          payment_collection_method: 'remittance',
          receives_invoice_emails: subscription.receives_invoice_emails,
          net_terms: subscription.net_terms,
          stored_credential_transaction_id: subscription.stored_credential_transaction_id,
        };
        const successful = await this.changeCollectionMethod(subscription.id, updateInfo);
        if (!successful) {
          throw new Error(`Error removing payment method. Make sure there is a fallback payment method in the profile.`);
        }
        update = {
          ...subscription,
          payment_collection_method: 'remittance',
          credit_card: {
            id: null,
            masked_card_number: null,
            card_type: null,
            expiration_month: null,
            expiration_year: null,
          },
        };
      }

      return { update };
    } catch (error) {
      this.logError(error, `Error removing payment method.`);
      throw error;
    }
  }

  /**
   *
   * @param {String} subscriptionId - The subscription id
   * @param {Int} amount - The amount to prepay
   * @param {String} paymentProfile - The chargify payment profile id
   * @param {String} memo - Optional memo
   * @returns PrePayment
   */
  async chargeSubscriptionPrePayment(subscriptionId, amount, paymentProfile, memo = 'Annual Subscription Payment') {
    try {
      validate.required({ subscriptionId, amount, paymentProfile, memo }, ['subscriptionId', 'amount', 'paymentProfile']);

      const url = `${this.siteUrlBase}/subscriptions/${subscriptionId}/prepayments.json`;
      const payload = {
        prepayment: {
          amount,
          details: memo,
          memo,
          method: 'credit_card_on_file',
          payment_profile_id: paymentProfile,
        },
      };
      const options = {
        auth: this.basicAuth,
      };

      const response = await this.httpClient.post(url, payload, options);

      return response.data.prepayment;
    } catch (error) {
      this.logError(error, `Error prepaying the chargify subscription with id ${subscriptionId}.`);
      throw error;
    }
  }

  async getBillingPortalLink(customerId) {
    try {
      validate.required({ customerId }, ['customerId']);

      const url = `${this.siteUrlBase}/portal/customers/${customerId}/management_link.json`;
      const options = {
        auth: this.basicAuth,
      };

      const response = await this.httpClient.get(url, options);

      return response.data;
    } catch (error) {
      this.logError(error, `Error fetching Billing Portal link for the chargify customer with id ${customerId}.`);
      throw error;
    }
  }

  /*---------------------------------------------------------------------------------*/
  //                                    Invoices
  /*---------------------------------------------------------------------------------*/

  /**
   * Get all invoices for all of an account's subscriptions.
   *
   * @see {@link https://reference.chargify.com/v1/relationship-invoicing/read-all-invoices}
   *
   * @param {String} customer - The chargify customer id
   * @param {Number} [resultsPerPage] - The number of results per page of invoices
   * @returns {Promise<Array<Object>>} - A list of invoices
   */
  async getInvoices(customer, resultsPerPage = INVOICES_RESULTS_PER_PAGE_MAX_SUPPORTED) {
    const url = `${this.siteUrlBase}/invoices.json`;
    try {
      const subscriptions = await this.getSubscriptionsForCustomer(customer);

      const allInvoices = [];

      for (let s = 0; s < subscriptions.length; s++) {
        // for (const subscription of subscriptions) {
        const subscription = subscriptions[s];
        let currentPage = 1;

        // Instead of doing `while(true)` and breaking when no more invoices are
        // retrieved for the subscription, a limit is used because if there were
        // an incorrect implementation on Chargify's end (e.g., pagination support
        // failed silently), it would cause an infinite loop here.
        for (let i = 0; i < INVOICES_REQUESTS_COUNT_MAX; i++) {
          const options = {
            auth: this.basicAuth,
            params: {
              page: currentPage,
              subscription_id: subscription.id,
              per_page: resultsPerPage,
            },
          };

          const response = await this.httpClient.get(url, options);
          if (!response || !response.data || !response.data.invoices || !response.data.invoices.length) {
            break;
          }

          allInvoices.push(...response.data.invoices);
          currentPage += 1;
        }
      }

      return allInvoices;
    } catch (error) {
      this.logError(error, `Error getting chargify invoices for user ${customer}.`);
      throw error;
    }
  }

  /**
   *
   * @param {String} subscriptionId - The subscription id
   * @param {String} startDate - Optional The beginning date range for the invoice's Due Date, in the YYYY-MM-DD format.
   * @param {String} endDate - Optional memo The ending date range for the invoice's Due Date, in the YYYY-MM-DD format.
   * @param {String} status - Optional The status of the invoice
   * @returns Array of Invoices
   */
  async getInvoicesForSubscription(subscriptionId, startDate, endDate, status = 'open') {
    try {
      validate.required({ subscriptionId }, ['subscriptionId']);

      let url = `${this.siteUrlBase}/invoices.json?status=${status}&subscription_id=${subscriptionId}&page=1&per_page=20&direction=desc&line_items=false&discounts=false&taxes=false&credits=false&payments=false&custom_fields=false`;
      if (startDate) {
        url += `&start_date=${startDate}`;
      }
      if (endDate) {
        url += `&end_date=${endDate}`;
      }

      const options = {
        auth: this.basicAuth,
      };

      const response = await this.httpClient.get(url, options);

      return response.data.invoices;
    } catch (error) {
      this.logError(error, `Error getting the chargify invoices for the subscription with id ${subscriptionId}.`);
      throw error;
    }
  }

  /**
   * Download an invoice PDF (to memory only) from Chargify and provide the
   * base64 payload for the client to save as a PDF file.
   *
   * The retrieval of the invoice in PDF format is not documented in Chargify's
   * API documentation at the time of this writing, but it works and is documented
   * for retrieving statements in PDF format; it seems certain that it is yet
   * another problem with Chargify's documentation and not an unsupported API
   * feature, especially with invoicing being the newer feature than statements.
   *
   * @see {@link https://reference.chargify.com/v1/relationship-invoicing/read-one-invoice}
   *
   * @param {String} id - The invoice id
   * @returns {Promise<Object>} - Conforms to the AccountBillingInvoiceDownload schema
   */
  async downloadInvoice(id) {
    try {
      const url = `${this.siteUrlBase}/invoices/${id}.pdf`;
      const options = {
        auth: this.basicAuth,
        responseType: 'arraybuffer',
      };

      const response = await this.httpClient.get(url, options);

      const base64Payload = Buffer.from(response.data, 'binary').toString('base64');

      return {
        id,
        base64Payload,
      };
    } catch (error) {
      this.logError(error, `Error downloading chargify invoice ${id}.`);
      throw error;
    }
  }

  /**
   *
   * @param {String} subscriptionId - The subscription id
   * @param {Array} components - The components in the subscription
   * @param {Int} months - Optional number of months to charge
   * @param {String} memo - Optional memo
   * @returns Invoice
   */
  async issueInvoice(subscriptionId, components, months = 1, memo = 'App Generated Invoice') {
    try {
      validate.required({ subscriptionId, components, months }, ['subscriptionId', 'components']);

      const url = `${this.siteUrlBase}/subscriptions/${subscriptionId}/invoices.json`;
      const payload = {
        invoice: {
          memo,
        },
      };
      const lineItems = [];

      // Add Line Items (one per month per component)
      for (let index = 0; index < components.length; index++) {
        const component = components[index];
        if (component) {
          // Add line item
          lineItems.push({
            title: component.name,
            quantity: component.prorated_quantity || component.allocated_quantity, // Allow for prorated amount for partial month
            unit_price: component.unit_price,
            taxable: true,
            period_range_start: component.start,
            period_range_end: component.end,
          });
          if (months > 1) {
            // Add additional line items for each month
            const orgStart = new Date(component.start);
            const orgEnd = new Date(component.end);
            for (let m = 0; m < months; m++) {
              const start = new Date(orgStart.setMonth(orgStart.getMonth() + m)).toString();
              const end = new Date(orgEnd.setMonth(orgEnd.getMonth() + m)).toString();
              lineItems.push({
                title: component.name,
                quantity: component.allocated_quantity,
                unit_price: component.unit_price,
                taxable: true,
                period_range_start: start,
                period_range_end: end,
              });
            }
          }
        }
      }
      if (!lineItems) {
        this.logError('Cannot create invoice without line items');
        throw new Error('Empty Invoice Not Created');
      }
      payload.invoice.line_items = lineItems;

      const options = {
        auth: this.basicAuth,
      };

      const response = await this.httpClient.post(url, payload, options);

      return response.data.invoice;
    } catch (error) {
      this.logError(error, `Error preparing invoice for the chargify subscription with id ${subscriptionId}.`);
      throw error;
    }
  }

  /**
   *
   * @param {String} id - The invoice id
   * @param {Int} amount - The amount to pay
   * @param {String} type - Optional the type of payment received (defaults to using prepaid balance)
   * @param {String} memo - Optional memo
   * @returns Invoice
   */
  async payInvoice(id, amount, type, memo = '') {
    try {
      validate.required({ id, amount }, ['id', 'amount']);
      if (!type) {
        this.logger.warn('No payment type passed. Defaulting to prepayment');
        type = 'prepayment';
      }

      const url = `${this.siteUrlBase}/invoices/${id}/payments.json`;
      const payload = {
        type,
        payment: {
          amount,
          memo,
        },
      };

      const options = {
        auth: this.basicAuth,
      };

      const response = await this.httpClient.post(url, payload, options);

      return response.data;
    } catch (error) {
      this.logError(error, `Error paying the chargify invoice with uuid ${id}.`);
      throw error;
    }
  }

  /**
   *
   * @param {String} id - The invoice id
   * @param {String} memo - Optional memo
   * @returns Invoice
   */
  async voidInvoice(id, memo = '') {
    try {
      validate.required({ id }, ['id']);

      const url = `${this.siteUrlBase}/invoices/${id}/void.json`;
      const payload = {
        void: {
          reason: memo,
        },
      };

      const options = {
        auth: this.basicAuth,
      };

      const response = await this.httpClient.post(url, payload, options);

      return response.data;
    } catch (error) {
      this.logError(error, `Error voiding the chargify invoice with uuid ${id}.`);
      throw error;
    }
  }

  /*---------------------------------------------------------------------------------*/
  //                                    Helpers
  /*---------------------------------------------------------------------------------*/

  /**
   * Log an error, ensuring that axios errors are properly handled.
   *
   * @private
   * @param {Error} error - The error object
   * @param {String} message - The message to include
   */
  logError(error, message) {
    // Axios errors are instances of Error but have `reponse.data.errors` populated
    if (error && error.response && error.response.data && error.response.data.errors) {
      this.logger.error(`${message} ${error.response.data.errors}`);
      // console.log(`${message} ${error.response.data.errors}`);
    } else {
      // This is a non-axios error
      this.logger.error(`${message} ${error}`);
      // console.error(error);
    }
  }
};
