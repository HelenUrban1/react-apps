const dotenv = require('dotenv');

dotenv.config({ path: '../.env' });

const HubspotCRMAdapter = require('./hubspotCRMAdapter');

// prettier-ignore
const {
  ERROR_MESSAGE,
  TEST_CONTACT_ID,
  TEST_CONTACT_UPDATE_ID,
  TEST_COMPANY_ID,
  TEST_COMPANY_DOMAIN,
  TEST_COMPANY_UPDATE_ID,
  TEST_PARENT_COMPANY_ID,
  TEST_CHILD_COMPANY_ID,
  TEST_DEAL_ID,
  PIPELINES,
  TIMELINE_EVENTS_IN_SOURCE,
  TIMELINE_EVENT_SINGLE_RESPONSE,
  TIMELINE_EVENT_TYPES_LIST_RESPONSE,
  getHubspotMock,
  getSearchCrmResponse,
  getCompanyResponse,
  getContactResponse,
  getDealResponse
} = require('./fixtures/hubspot.fixtures');
const config = require('../../../config')();

const USE_MOCKS = true;

describe('HubspotCRMAdapter', () => {
  let crmClient;
  const hubspotMock = getHubspotMock();
  const loggerMock = {
    trace: jest.fn(),
    debug: jest.fn(),
    log: jest.fn(),
    info: jest.fn(),
    warn: jest.fn(),
    error: jest.fn(),
  };

  beforeEach(async () => {
    jest.resetAllMocks();
    if (USE_MOCKS) {
      crmClient = new HubspotCRMAdapter({
        hubspotMock,
        logger: loggerMock,
        privateAccessToken: process.env.IXC_HUBSPOT_PRIVATE_ACCESS_TOKEN,
        apiKey: process.env.IXC_HUBSPOT_API_KEY,
        appId: process.env.IXC_HUBSPOT_APP_ID,
        appOauthAccessToken: process.env.IXC_HUBSPOT_APP_OAUTH_ACCESS_TOKEN,
        appOauthRefreshToken: process.env.IXC_HUBSPOT_APP_OAUTH_REFRESH_TOKEN,
        appClientId: process.env.IXC_HUBSPOT_APP_CLIENT_ID,
        appClientSecret: process.env.IXC_HUBSPOT_APP_CLIENT_SECRET,
        appDevApiKey: process.env.IXC_HUBSPOT_APP_DEV_API_KEY,
      });
    } else {
      crmClient = new HubspotCRMAdapter({
        logger: loggerMock,
        privateAccessToken: process.env.IXC_HUBSPOT_PRIVATE_ACCESS_TOKEN,
        apiKey: process.env.IXC_HUBSPOT_API_KEY,
        appId: process.env.IXC_HUBSPOT_APP_ID,
        appOauthAccessToken: process.env.IXC_HUBSPOT_APP_OAUTH_ACCESS_TOKEN,
        appOauthRefreshToken: process.env.IXC_HUBSPOT_APP_OAUTH_REFRESH_TOKEN,
        appClientId: process.env.IXC_HUBSPOT_APP_CLIENT_ID,
        appClientSecret: process.env.IXC_HUBSPOT_APP_CLIENT_SECRET,
        appDevApiKey: process.env.IXC_HUBSPOT_APP_DEV_API_KEY,
      });
    }
    // Warm up local caches
    // await crmClient.initialize();
  });

  describe('#getMetadata', () => {
    it('should return name and mock status of adapter', () => {
      const md = crmClient.getMetadata();
      expect(md).toBeDefined();
      expect(md.name).toBe('hubspot');
      expect(md.isMocked).toBeDefined();
      expect(loggerMock.error).toHaveBeenCalledTimes(0);
    });
  });

  describe('Pipelines', () => {
    describe('#getPipelines', () => {
      it('should retrieve the pipelines configured in the CRM', async () => {
        hubspotMock.pipelines.get.mockResolvedValueOnce(PIPELINES);
        const result = await crmClient.getPipelines();
        expect(result.length).toBeGreaterThan(0);
        expect(result[0].pipelineId).toBeDefined();
        expect(result[0].label).toBeDefined();
        expect(result[0].stages.length).toBeGreaterThan(0);
        expect(result[0].stages[0].stageId).toBeDefined();
        expect(result[0].stages[0].label).toBeDefined();
        expect(loggerMock.error).toHaveBeenCalledTimes(0);
      });
    });

    describe('#buildPipelineReference', () => {
      it('should return a reference object that contains a map of the pipelines and stages stored in the CRM', async () => {
        hubspotMock.pipelines.get.mockResolvedValueOnce(PIPELINES);
        const ref = await crmClient.buildPipelineReference();
        expect(Object.keys(ref).length).toBeGreaterThan(0);
        // Make sure pipeline lookup values are flexible
        expect(ref['6510422'].id).toBe('6510422');
        expect(ref['6510422'].name).toBe('Freemium');
        expect(ref.FREEMIUM.id).toBe('6510422');
        expect(ref.FREEMIUM.name).toBe('Freemium');
        // Make sure pipeline stage lookup values are flexible
        expect(ref.FREEMIUM.stages['6510425'].id).toBe('6510425');
        expect(ref.FREEMIUM.stages['UPLOADED DRAWING 1'].name).toBe('Uploaded Drawing 1');
        expect(ref.FREEMIUM.stages['6510425'].id).toBe('6510425');
        expect(ref.FREEMIUM.stages['UPLOADED DRAWING 1'].name).toBe('Uploaded Drawing 1');
        expect(loggerMock.error).toHaveBeenCalledTimes(0);
      });
    });

    describe('#getPipelineLabelIds', () => {
      it('should return a reference object that contains a map of the pipelines and stages stored in the CRM', async () => {
        hubspotMock.pipelines.get.mockResolvedValueOnce(PIPELINES);
        let params = {
          pipeline: 'Freemium',
          dealstage: 'Uploaded Drawing 1',
        };
        let result = await crmClient.getPipelineLabelIds(params.pipeline, params.dealstage);
        expect(result.pipelineId).toBe('6510422');
        expect(result.stageId).toBe('6510425');
        params = {
          pipeline: 'Online Trial',
          dealstage: 'Drawing Ballooned',
        };
        result = await crmClient.getPipelineLabelIds(params.pipeline, params.dealstage);
        expect(result.pipelineId).toBe('5743684');
        expect(result.stageId).toBe('5743687');
        expect(loggerMock.error).toHaveBeenCalledTimes(0);
      });
    });

    describe('#validateENVPipeline', () => {
      it('should throw a warning if names do not match with suggested correction', async () => {
        const pipeline = {
          [config.crm.pipelineName.toUpperCase()]: {
            stages: {
              a: {},
              b: {},
              c: {},
            },
          },
        };
        await expect(crmClient.validateENVPipeline(pipeline)).rejects.toThrow();
        expect(loggerMock.warn).toHaveBeenCalled();
      });

      it('should throw an error if names does not match', async () => {
        const pipeline = {
          [config.crm.pipelineName.toUpperCase()]: {
            stages: {
              a: {},
              b: {},
              c: {},
            },
          },
        };
        await expect(crmClient.validateENVPipeline(pipeline)).rejects.toThrow();
        // expect(loggerMock.error).toHaveBeenCalledTimes(1);
      });

      it('should not throw a warning if config and pipelines match', async () => {
        const pipeline = {
          [config.crm.pipelineName.toUpperCase()]: {
            stages: {
              [config.crm.pipelineStage.toUpperCase()]: {},
              [config.crm.closedWon.toUpperCase()]: {},
              [config.crm.closedLost.toUpperCase()]: {},
            },
          },
        };
        await crmClient.validateENVPipeline(pipeline);
        expect(loggerMock.warn).toHaveBeenCalledTimes(0);
        expect(loggerMock.error).toHaveBeenCalledTimes(0);
      });
    });
  });

  describe('Companies', () => {
    describe('#getCompanyById()', () => {
      it('should throw an error if required input is not provided', async () => {
        await expect(crmClient.getCompanyById()).rejects.toThrow();
        expect(loggerMock.error).toHaveBeenCalledTimes(1);
      });

      it('should populate the result properly', async () => {
        hubspotMock.companies.getById.mockResolvedValueOnce(getCompanyResponse());
        const result = await crmClient.getCompanyById(TEST_COMPANY_ID);
        expect(result.crm).toBe('hubspot');
        expect(result.crmType).toBe('company');
        expect(result.crmId).toBe(TEST_COMPANY_ID);
        expect(result.name).toBeDefined();
        expect(loggerMock.error).toHaveBeenCalledTimes(0);
      });

      it('should have custom properties', async () => {
        hubspotMock.companies.getById.mockResolvedValueOnce(getCompanyResponse());
        const result = await crmClient.getCompanyById(TEST_COMPANY_ID);
        expect(result.ixc_account_id).toBeDefined();
        expect(result.ix_chargify_id).toBeDefined();
        expect(result.ix_chargify_link).toBeDefined();
        expect(loggerMock.error).toHaveBeenCalledTimes(0);
      });
    });

    describe('#getCompanyByDomain()', () => {
      it('should throw an error if required input is not provided', async () => {
        await expect(crmClient.getCompanyByDomain()).rejects.toThrow();
        expect(loggerMock.error).toHaveBeenCalledTimes(1);
      });

      it('should populate the result with default fields properly', async () => {
        hubspotMock.companies.getByDomain.mockResolvedValueOnce(getCompanyResponse({ returnMany: true }));
        const result = await crmClient.getCompanyByDomain(TEST_COMPANY_DOMAIN);
        expect(result[0].crm).toBe('hubspot');
        expect(result[0].crmType).toBe('company');
        expect(result[0].crmId).toBe(TEST_COMPANY_ID);
        expect(result[0].domain).toBe(TEST_COMPANY_DOMAIN);
        expect(result[0].name).toBeDefined();
        expect(loggerMock.error).toHaveBeenCalledTimes(0);
      });

      it('should populate the result with custom fields if provided', async () => {
        hubspotMock.companies.getByDomain.mockResolvedValueOnce(getCompanyResponse({ returnMany: true, returnProps: ['name'] }));
        const result = await crmClient.getCompanyByDomain(TEST_COMPANY_DOMAIN, ['name']);
        expect(result[0].crm).toBe('hubspot');
        expect(result[0].crmType).toBe('company');
        expect(result[0].crmId).toBe(TEST_COMPANY_ID);
        expect(result[0].domain).toBeUndefined();
        expect(result[0].name).toBeDefined();
        expect(loggerMock.error).toHaveBeenCalledTimes(0);
      });
    });

    describe('#getCompanyByAccountId()', () => {
      const ixcAccountId = '15f64b4d-cf8c-470b-a282-0e9d48f4d83d';

      it('should throw an error if required input is not provided', async () => {
        await expect(crmClient.getCompanyByAccountId()).rejects.toThrow();
        expect(loggerMock.error).toHaveBeenCalledTimes(1);
      });

      it('should populate the result with default fields properly', async () => {
        hubspotMock.apiRequest.mockResolvedValueOnce(
          getSearchCrmResponse({
            crmObjectType: 'companies',
            returnProps: ['name', 'domain', 'createdate', 'hs_lastmodifieddate', 'ixc_account_id'],
            id: TEST_COMPANY_ID,
            createdAt: '2021-07-15T19:38:13.365Z',
            updatedAt: '2021-08-02T23:05:41.693Z',
            archived: false,
            properties: {
              hs_object_id: '6581530659',
              name: "Mr meenan's toolbox Co",
              domain: 'inspectionxpert.com',
              ixc_account_id: ixcAccountId,
              createdate: '2021-07-15T19:38:13.365Z',
              hs_lastmodifieddate: '2021-08-02T23:05:41.693Z',
            },
          })
        );
        const result = await crmClient.getCompanyByAccountId(ixcAccountId);
        expect(result.crm).toBe('hubspot');
        expect(result.crmType).toBe('company');
        expect(result.crmId).toBe(TEST_COMPANY_ID);
        expect(result.domain).toBe('inspectionxpert.com');
        expect(result.name).toBe("Mr meenan's toolbox Co");
        expect(loggerMock.error).toHaveBeenCalledTimes(0);
      });

      it('should populate the result with custom fields if provided', async () => {
        hubspotMock.apiRequest.mockResolvedValueOnce(
          getSearchCrmResponse({
            crmObjectType: 'companies',
            returnProps: ['name', 'domain', 'createdate', 'hs_lastmodifieddate', 'ixc_account_id'],
            id: TEST_COMPANY_ID,
            createdAt: '2021-07-15T19:38:13.365Z',
            updatedAt: '2021-08-02T23:05:41.693Z',
            archived: false,
            properties: {
              hs_object_id: '6581530659',
              name: "Mr meenan's toolbox Co",
              domain: 'inspectionxpert.com',
              ixc_account_id: ixcAccountId,
              createdate: '2021-07-15T19:38:13.365Z',
              hs_lastmodifieddate: '2021-08-02T23:05:41.693Z',
            },
          })
        );
        const result = await crmClient.getCompanyByAccountId(ixcAccountId, ['name']);
        expect(result.crm).toBe('hubspot');
        expect(result.crmType).toBe('company');
        expect(result.crmId).toBe(TEST_COMPANY_ID);
        expect(result.domain).toBe('inspectionxpert.com');
        expect(result.name).toBe("Mr meenan's toolbox Co");
        expect(loggerMock.error).toHaveBeenCalledTimes(0);
      });

      it('should return a single result when the lookup succeeds', async () => {
        hubspotMock.apiRequest.mockResolvedValueOnce(
          getSearchCrmResponse({
            crmObjectType: 'companies',
            returnProps: ['name', 'domain', 'createdate', 'hs_lastmodifieddate', 'ixc_account_id'],
            id: TEST_COMPANY_ID,
            createdAt: '2021-07-15T19:38:13.365Z',
            updatedAt: '2021-08-02T23:05:41.693Z',
            archived: false,
            properties: {
              hs_object_id: '6581530659',
              name: "Mr meenan's toolbox Co",
              domain: 'inspectionxpert.com',
              ixc_account_id: ixcAccountId,
              createdate: '2021-07-15T19:38:13.365Z',
              hs_lastmodifieddate: '2021-08-02T23:05:41.693Z',
            },
          })
        );
        const result = await crmClient.getCompanyByAccountId(ixcAccountId);
        console.log('result', result.results);
        expect(result.crm).toBe('hubspot');
        expect(result.crmType).toBe('company');
        expect(result.crmId).toBe(TEST_COMPANY_ID);
        expect(result.name).toBe("Mr meenan's toolbox Co");
        expect(result.domain).toBe('inspectionxpert.com');
        expect(result.ixc_account_id).toBe(ixcAccountId);
        expect(loggerMock.error).toHaveBeenCalledTimes(0);
      });
    });

    describe('#createCompany()', () => {
      let params;

      beforeEach(() => {
        params = {
          // reference: 'some-reference',
          name: 'Foo Company',
          domain: 'foo.com',
        };
      });

      it('should throw an error if required input is not provided', async () => {
        await expect(crmClient.createCompany({})).rejects.toThrow();
        expect(loggerMock.error).toHaveBeenCalledTimes(1);
      });

      it('should throw an error if the request fails', async () => {
        // This test fails when using the real hubspot API, because the record is created successfully
        if (crmClient.isMocked) {
          hubspotMock.companies.create.mockRejectedValueOnce(new Error(ERROR_MESSAGE));
          await expect(crmClient.createCompany(params)).rejects.toThrow();
          expect(loggerMock.error).toHaveBeenCalledTimes(1);
        }
      });

      it('should create and return a company record when the request succeeds', async () => {
        hubspotMock.companies.create.mockResolvedValueOnce(
          getCompanyResponse({
            properties: {
              ix_account_name: params.name,
              ...params,
            },
          })
        );
        const result = await crmClient.createCompany(params);
        expect(result).toBeDefined();
        expect(result.name).toBe(params.name);
        // Sales doesn't want their edits to the Account Name to be overwritten
        // To do so maintain ix_account_name in addition to name
        expect(result.ix_account_name).toBe(params.name);
        expect(result.domain).toBe(params.domain);
        expect(loggerMock.error).toHaveBeenCalledTimes(0);
      });
    });

    describe('#updateCompany()', () => {
      let params;

      beforeEach(() => {
        params = {
          // reference: 'some-reference',
          name: 'Foo Company',
          domain: 'hulligan.com',
        };
      });

      it('should throw an error if required input is not provided', async () => {
        await expect(crmClient.updateCompany({})).rejects.toThrow();
        expect(loggerMock.error).toHaveBeenCalledTimes(1);
      });

      it('should throw an error if the request fails', async () => {
        // This test fails when using the real hubspot API, because the record is updated successfully
        if (crmClient.isMocked) {
          hubspotMock.companies.update.mockRejectedValueOnce(new Error(ERROR_MESSAGE));
          await expect(crmClient.updateCompany(TEST_COMPANY_UPDATE_ID, params)).rejects.toThrow();
          expect(loggerMock.error).toHaveBeenCalledTimes(1);
        }
      });

      it('should update and return a company record when the request succeeds', async () => {
        hubspotMock.companies.update.mockResolvedValueOnce(
          getCompanyResponse({
            companyId: 'an-id',
            properties: {
              name: params.name,
              domain: params.domain,
            },
          })
        );

        hubspotMock.apiRequest.mockResolvedValueOnce({
          results: ['1234567'], // numberOfContacts: 1
          hasMore: false,
          offset: 1,
        });

        const result = await crmClient.updateCompany({ id: TEST_COMPANY_UPDATE_ID, ...params });
        expect(result).toBeDefined();
        expect(result.name).toBe(params.name);
        expect(result.domain).toBe(params.domain);
        expect(loggerMock.error).toHaveBeenCalledTimes(0);
      });

      it('should update company ix_account_name instead of name', async () => {
        hubspotMock.companies.update.mockResolvedValueOnce(
          getCompanyResponse({
            companyId: 'an-id',
            properties: {
              name: 'Old Name',
              ix_account_name: params.name,
              domain: params.domain,
            },
          })
        );

        hubspotMock.apiRequest.mockResolvedValueOnce({
          results: ['1234567'], // numberOfContacts: 1
          hasMore: false,
          offset: 1,
        });

        const result = await crmClient.updateCompany({ id: TEST_COMPANY_UPDATE_ID, ...params });
        expect(result).toBeDefined();
        expect(result.name).toBe('Old Name');
        expect(result.ix_account_name).toBe(params.name);
        expect(result.domain).toBe(params.domain);
        expect(loggerMock.error).toHaveBeenCalledTimes(0);
      });

      it('should update associated contacts .company attribute when a new company name is provided', async () => {
        hubspotMock.companies.update.mockResolvedValueOnce(
          getCompanyResponse({
            companyId: 'an-id',
            properties: {
              name: 'Old Name',
              domain: params.domain,
            },
          })
        );

        hubspotMock.apiRequest.mockResolvedValueOnce({
          results: ['1234567'], // numberOfContacts: 1
          hasMore: false,
          offset: 1,
        });

        const createOrUpdateBatchSpy = jest.spyOn(hubspotMock.contacts, 'createOrUpdateBatch');
        await crmClient.updateCompany({ id: TEST_COMPANY_UPDATE_ID, ...params });
        expect(createOrUpdateBatchSpy).toBeCalled();
        expect(loggerMock.error).toHaveBeenCalledTimes(0);
      });
    });

    describe('#createOrUpdateCompany()', () => {
      let createParams;
      let updateParams;

      beforeEach(() => {
        createParams = {
          // reference: 'some-reference',
          name: 'Foo Company New',
          domain: 'foobar.com',
        };
        updateParams = {
          // reference: 'some-reference',
          name: 'Foo Company Updated',
          domain: 'foobar.com',
        };
      });

      it('should throw an error if required input is not provided', async () => {
        await expect(crmClient.createOrUpdateCompany({})).rejects.toThrow();
        expect(loggerMock.error).toHaveBeenCalledTimes(1);
      });

      it('should throw an error if the request fails', async () => {
        hubspotMock.companies.update.mockRejectedValueOnce(new Error(ERROR_MESSAGE));
        await expect(crmClient.createOrUpdateCompany('foo', createParams)).rejects.toThrow();
        expect(loggerMock.error).toHaveBeenCalledTimes(1);
      });

      it('should create or update and return a company record when the request succeeds', async () => {
        // No companies with domain name exist
        hubspotMock.companies.getByDomain.mockResolvedValueOnce({
          results: [],
        });
        hubspotMock.companies.create.mockResolvedValueOnce(
          getCompanyResponse({
            companyId: 'an-id',
            properties: {
              name: createParams.name,
              domain: createParams.domain,
            },
          })
        );

        // One company with domain name is found
        hubspotMock.companies.getByDomain.mockResolvedValueOnce({
          results: [
            getCompanyResponse({
              companyId: 'an-id',
              properties: {
                name: createParams.name,
                domain: createParams.domain,
              },
            }),
          ],
        });
        hubspotMock.companies.update.mockResolvedValueOnce(
          getCompanyResponse({
            companyId: 'an-id',
            properties: {
              name: updateParams.name,
              domain: updateParams.domain,
            },
          })
        );

        hubspotMock.apiRequest.mockResolvedValueOnce({
          results: ['1234567'], // numberOfContacts: 1
          hasMore: false,
          offset: 1,
        });

        // Test create
        let result = await crmClient.createOrUpdateCompany(createParams);
        expect(result).toBeDefined();
        expect(result.name).toBe(createParams.name);
        expect(result.domain).toBe(createParams.domain);
        // Test update
        result = await crmClient.createOrUpdateCompany(updateParams);
        expect(result).toBeDefined();
        expect(result.name).toBe(updateParams.name);
        expect(result.domain).toBe(updateParams.domain);
        expect(loggerMock.error).toHaveBeenCalledTimes(0);
      });

      it('should create a child company of the existing parent when more than one company is found', async () => {
        // One company with domain name is found
        hubspotMock.companies.getByDomain.mockResolvedValueOnce({
          results: [
            getCompanyResponse({
              companyId: 'id-A',
              properties: {
                name: `${createParams.name} A`,
                domain: createParams.domain,
                hs_lastmodifieddate: 400,
              },
            }),
            getCompanyResponse({
              companyId: 'id-B',
              properties: {
                name: `${createParams.name} B`,
                domain: createParams.domain,
                hs_lastmodifieddate: 400,
              },
            }),
          ],
        });

        hubspotMock.apiRequest.mockResolvedValueOnce({
          results: ['id-B'], // numberOfChildren: 1
          hasMore: false,
          offset: 1,
        });

        hubspotMock.apiRequest.mockResolvedValueOnce({
          results: [], // numberOfChildren: 0
          hasMore: false,
          offset: 1,
        });

        // Create the child company
        hubspotMock.companies.create.mockResolvedValueOnce(
          getCompanyResponse({
            companyId: 'id-C',
            properties: {
              name: `${createParams.name} C`,
              domain: createParams.domain,
            },
          })
        );

        // C becomes a child of A and peer of B
        hubspotMock.crm.associations.create.mockResolvedValueOnce({ success: true });

        // Test create
        const result = await crmClient.createOrUpdateCompany(createParams);
        expect(result).toBeDefined();
        expect(result.name).toBe(`${createParams.name} C`);
        expect(result.domain).toBe(createParams.domain);

        expect(loggerMock.error).toHaveBeenCalledTimes(0);
      });
    });

    describe('#deleteCompany()', () => {
      it('should throw an error if required input is not provided', async () => {
        await expect(crmClient.deleteCompany({})).rejects.toThrow();
        expect(loggerMock.error).toHaveBeenCalledTimes(1);
      });

      it('should throw an error if the request fails', async () => {
        // This test fails when using the real hubspot API, because the record is updated successfully
        if (crmClient.isMocked) {
          hubspotMock.companies.delete.mockRejectedValueOnce(new Error(ERROR_MESSAGE));
          await expect(crmClient.deleteCompany({ id: 123 })).rejects.toThrow();
          expect(loggerMock.error).toHaveBeenCalledTimes(1);
        }
      });

      it('should delete a company record when the request succeeds', async () => {
        hubspotMock.companies.delete.mockResolvedValueOnce({
          companyId: 123,
          deleted: true,
        });
        const result = await crmClient.deleteCompany({ id: 123 });
        expect(result).toBeDefined();
        expect(result.companyId).toBe(123);
        expect(result.deleted).toBe(true);
        expect(loggerMock.error).toHaveBeenCalledTimes(0);
      });
    });

    describe('#identifyParent()', () => {
      it('should throw an error if required input is not provided', async () => {
        await expect(crmClient.identifyParent()).rejects.toThrow();
        await expect(crmClient.identifyParent([])).rejects.toThrow();
        await expect(crmClient.identifyParent([1])).rejects.toThrow();
        expect(loggerMock.error).toHaveBeenCalledTimes(3);
      });

      it('should throw an error if the request fails', async () => {
        if (crmClient.isMocked) {
          hubspotMock.apiRequest.mockRejectedValueOnce(new Error(ERROR_MESSAGE));
          await expect(
            crmClient.identifyParent([
              {
                crmId: 'A',
                hs_lastmodifieddate: 200,
              },
              {
                crmId: 'B',
                hs_lastmodifieddate: 300,
              },
            ])
          ).rejects.toThrow();
          expect(loggerMock.error).toHaveBeenCalledTimes(2);
        }
      });

      it('should return the company with the most children which has been most recently modified', async () => {
        const canidates = [];

        // Most children but last modified longer ago
        canidates.push({
          crmId: 'A',
          hs_lastmodifieddate: 200,
        });
        hubspotMock.apiRequest.mockResolvedValueOnce({
          results: [1, 2, 3, 4, 5], // numberOfChildren: 5
          hasMore: false,
          offset: 1,
        });

        // NOT the most children and modified longer ago
        canidates.push({
          crmId: 'B',
          hs_lastmodifieddate: 200,
        });
        hubspotMock.apiRequest.mockResolvedValueOnce({
          results: [1, 2, 3, 4], // numberOfChildren: 4
          hasMore: false,
          offset: 1,
        });

        // Least children and modified longest ago
        canidates.push({
          crmId: 'C',
          hs_lastmodifieddate: 100,
        });
        hubspotMock.apiRequest.mockResolvedValueOnce({
          results: [1, 2, 3], // numberOfChildren: 3
          hasMore: false,
          offset: 1,
        });

        // Most children and modified most recently
        canidates.push({
          crmId: 'D',
          hs_lastmodifieddate: 300,
        });
        hubspotMock.apiRequest.mockResolvedValueOnce({
          results: [1, 2, 3, 4, 5], // numberOfChildren: 5
          hasMore: false,
          offset: 1,
        });

        // Least children and modified MOST recently
        canidates.push({
          crmId: 'E',
          hs_lastmodifieddate: 400,
        });
        hubspotMock.apiRequest.mockResolvedValueOnce({
          results: [1, 2, 3], // numberOfChildren: 3
          hasMore: false,
          offset: 1,
        });

        const parent = await crmClient.identifyParent(canidates);
        expect(parent.crmId).toBe('D');
        expect(loggerMock.error).toHaveBeenCalledTimes(0);
      });
    });

    describe('#addChildCompanyToParentCompany()', () => {
      it('should throw an error if required input is not provided', async () => {
        await expect(crmClient.addChildCompanyToParentCompany()).rejects.toThrow();
        await expect(crmClient.addChildCompanyToParentCompany({ parentCompanyId: 1 })).rejects.toThrow();
        await expect(crmClient.addChildCompanyToParentCompany({ childCompanyId: 1 })).rejects.toThrow();
        expect(loggerMock.error).toHaveBeenCalledTimes(3);
      });

      it('should throw an error if the request fails', async () => {
        // This test fails when using the real hubspot API, because first
        // runs will create records that are found and returned by subsequent runs.
        if (crmClient.isMocked) {
          hubspotMock.crm.associations.create.mockRejectedValueOnce(new Error(ERROR_MESSAGE));
          await expect(crmClient.addChildCompanyToParentCompany({ parentCompanyId: TEST_PARENT_COMPANY_ID, childCompanyId: TEST_CHILD_COMPANY_ID })).rejects.toThrow();
          expect(loggerMock.error).toHaveBeenCalledTimes(1);
        }
      });

      it('should populate the result with custom fields if provided', async () => {
        hubspotMock.crm.associations.create.mockResolvedValueOnce({ success: true });
        const result = await crmClient.addChildCompanyToParentCompany({ parentCompanyId: TEST_PARENT_COMPANY_ID, childCompanyId: TEST_CHILD_COMPANY_ID });
        expect(result.success).toBe(true);
        expect(loggerMock.error).toHaveBeenCalledTimes(0);
      });
    });

    describe('#addContactToCompany()', () => {
      it('should throw an error if required input is not provided', async () => {
        await expect(crmClient.addContactToCompany()).rejects.toThrow();
        await expect(crmClient.addContactToCompany({ companyId: 1 })).rejects.toThrow();
        await expect(crmClient.addContactToCompany({ contactVid: 1 })).rejects.toThrow();
        expect(loggerMock.error).toHaveBeenCalledTimes(3);
      });

      it('should throw an error if the request fails', async () => {
        // This test fails when using the real hubspot API, because first
        // runs will create records that are found and returned by subsequent runs.
        if (crmClient.isMocked) {
          hubspotMock.companies.addContactToCompany.mockRejectedValueOnce(new Error(ERROR_MESSAGE));
          await expect(crmClient.addContactToCompany({ companyId: TEST_COMPANY_ID, contactVid: TEST_CONTACT_ID })).rejects.toThrow();
          expect(loggerMock.error).toHaveBeenCalledTimes(1);
        }
      });

      it('should populate the result with custom fields if provided', async () => {
        hubspotMock.companies.addContactToCompany.mockResolvedValueOnce({ success: true });
        const result = await crmClient.addContactToCompany({ companyId: TEST_COMPANY_ID, contactVid: TEST_CONTACT_ID });
        expect(result.success).toBe(true);
        expect(loggerMock.error).toHaveBeenCalledTimes(0);
      });
    });

    describe('#removeContactFromCompany()', () => {
      it('should throw an error if required input is not provided', async () => {
        await expect(crmClient.removeContactFromCompany()).rejects.toThrow();
        await expect(crmClient.removeContactFromCompany({ companyId: 1 })).rejects.toThrow();
        await expect(crmClient.removeContactFromCompany({ contactVid: 1 })).rejects.toThrow();
        expect(loggerMock.error).toHaveBeenCalledTimes(3);
      });

      it('should throw an error if the request fails', async () => {
        // This test fails when using the real hubspot API, because first
        // runs will create records that are found and returned by subsequent runs.
        if (crmClient.isMocked) {
          // hubspotMock.deals.removeAssociation.mockRejectedValueOnce(new Error(ERROR_MESSAGE));
          hubspotMock.crm.associations.delete.mockRejectedValueOnce(new Error(ERROR_MESSAGE));
          await expect(crmClient.removeContactFromCompany({ companyId: TEST_COMPANY_ID, contactVid: TEST_CONTACT_ID })).rejects.toThrow();
          expect(loggerMock.error).toHaveBeenCalledTimes(1);
        }
      });

      it('should remove the specified contact from the deal', async () => {
        hubspotMock.deals.removeAssociation.mockResolvedValueOnce({ success: true });
        const result = await crmClient.removeContactFromCompany({ companyId: TEST_COMPANY_ID, contactVid: TEST_CONTACT_ID });
        expect(result.success).toBe(true);
        expect(loggerMock.error).toHaveBeenCalledTimes(0);
      });
    });
  });

  describe('Contacts', () => {
    describe('#getContactById()', () => {
      it('should throw an error if required input is not provided', async () => {
        await expect(crmClient.getContactById()).rejects.toThrow();
        expect(loggerMock.error).toHaveBeenCalledTimes(1);
      });

      it('should populate the result properly', async () => {
        hubspotMock.contacts.getById.mockResolvedValueOnce(getContactResponse());
        const result = await crmClient.getContactById(TEST_CONTACT_ID);
        expect(result.crm).toBe('hubspot');
        expect(result.crmType).toBe('contact');
        expect(result.crmId).toBe(TEST_CONTACT_ID);
        expect(result.email).toBeDefined();
        // expect(result.hs_email_domain).toBeDefined();
        expect(loggerMock.error).toHaveBeenCalledTimes(0);
      });

      it('should have custom properties', async () => {
        hubspotMock.contacts.getById.mockResolvedValueOnce(getContactResponse());
        const result = await crmClient.getContactById(TEST_CONTACT_UPDATE_ID);
        expect(result.ixc_user_id).toBeDefined();
        expect(loggerMock.error).toHaveBeenCalledTimes(0);
      });
    });

    describe('#getContactByEmail()', () => {
      it('should throw an error if required input is not provided', async () => {
        await expect(crmClient.getContactByEmail()).rejects.toThrow();
        expect(loggerMock.error).toHaveBeenCalledTimes(1);
      });

      it('should populate the result properly', async () => {
        hubspotMock.contacts.getByEmail.mockResolvedValueOnce(getContactResponse());
        const result = await crmClient.getContactByEmail('bh@hubspot.com');
        expect(result.crm).toBe('hubspot');
        expect(result.crmType).toBe('contact');
        expect(result.crmId).toBe(TEST_CONTACT_ID);
        expect(result.email).toBe('bh@hubspot.com');
        expect(loggerMock.error).toHaveBeenCalledTimes(0);
      });
    });

    describe('#createOrUpdateContact()', () => {
      let params;

      beforeEach(() => {
        params = {
          // reference: 'some-reference',
          email: 'some-email@foo.com',
          firstname: 'some-first-name',
          lastname: 'some-last-name',
        };
      });

      it('should throw an error if required input is not provided', async () => {
        await expect(crmClient.createOrUpdateContact({})).rejects.toThrow();
        expect(loggerMock.error).toHaveBeenCalledTimes(1);
      });

      it('should throw an error if the request fails', async () => {
        // This test fails when using the real hubspot API, because first
        // runs will create records that are found and returned by subsequent runs.
        if (crmClient.isMocked) {
          hubspotMock.contacts.createOrUpdate.mockRejectedValueOnce(new Error(ERROR_MESSAGE));
          await expect(crmClient.createOrUpdateContact(params)).rejects.toThrow();
          expect(loggerMock.error).toHaveBeenCalledTimes(1);
        }
      });

      it('should create and return a contact record when the request succeeds', async () => {
        hubspotMock.contacts.createOrUpdate.mockResolvedValueOnce({ vid: 'an-id', isNew: false });
        hubspotMock.contacts.getById.mockResolvedValueOnce(
          getContactResponse({
            vid: 'an-id',
            properties: {
              email: params.email,
            },
          })
        );
        const result = await crmClient.createOrUpdateContact(params);
        expect(result).toBeDefined();
        expect(result.email).toBe(params.email);
        expect(loggerMock.error).toHaveBeenCalledTimes(0);
      });
    });

    describe('#updateContact()', () => {
      let params;

      beforeEach(() => {
        params = {
          // reference: 'some-reference',
          email: 'some-email@foo.com',
          firstname: 'some-first-name',
          lastname: 'some-last-name',
        };
      });

      it('should throw an error if required input is not provided', async () => {
        await expect(crmClient.updateContact({})).rejects.toThrow();
        expect(loggerMock.error).toHaveBeenCalledTimes(1);
      });

      it('should throw an error if the request fails', async () => {
        hubspotMock.contacts.update.mockRejectedValueOnce(new Error(ERROR_MESSAGE));
        await expect(crmClient.updateContact(params)).rejects.toThrow();
        expect(loggerMock.error).toHaveBeenCalledTimes(1);
      });

      it('should update and return a contact record when the request succeeds', async () => {
        hubspotMock.contacts.update.mockResolvedValueOnce(
          getContactResponse({
            vid: TEST_CONTACT_UPDATE_ID,
          })
        );
        hubspotMock.contacts.getById.mockResolvedValueOnce(
          getContactResponse({
            vid: TEST_CONTACT_UPDATE_ID,
            properties: {
              email: params.email,
              firstname: params.firstname,
              lastname: params.lastname,
            },
          })
        );
        const result = await crmClient.updateContact({ id: TEST_CONTACT_UPDATE_ID, ...params });
        expect(result).toBeDefined();
        expect(result.crmId).toBe(TEST_CONTACT_UPDATE_ID);
        expect(result.crmType).toBe('contact');
        expect(result.email).toBe(params.email);
        expect(result.firstname).toBe(params.firstname);
        expect(result.lastname).toBe(params.lastname);
        expect(loggerMock.error).toHaveBeenCalledTimes(0);
      });
    });

    describe('#updateContacts()', () => {
      let update;

      beforeEach(() => {
        update = [
          {
            id: '123',
            firstname: 'some-first-name',
            lastname: 'some-last-name',
          },
          {
            email: 'john@doe.com',
            firstname: 'some-first-name',
            lastname: 'some-last-name',
          },
        ];
      });

      it('should throw an error if required input is not provided', async () => {
        await expect(
          crmClient.updateContacts([
            {
              id: '123',
              firstname: 'some-first-name',
              lastname: 'some-last-name',
            },
            {
              // Missing ID and Email
              firstname: 'some-first-name',
              lastname: 'some-last-name',
            },
          ])
        ).rejects.toThrow();
        expect(loggerMock.error).toHaveBeenCalledTimes(1);
      });

      it('should throw an error if the request fails', async () => {
        hubspotMock.contacts.createOrUpdateBatch.mockRejectedValueOnce(new Error(ERROR_MESSAGE));
        await expect(crmClient.updateContacts(update)).rejects.toThrow();
        expect(loggerMock.error).toHaveBeenCalledTimes(1);
      });

      it('should update and return a contact record when the request succeeds', async () => {
        hubspotMock.contacts.createOrUpdateBatch.mockResolvedValueOnce();
        const result = await crmClient.updateContacts(update);
        expect(result).toBeDefined();
        expect(result.success).toBe(true);
        expect(loggerMock.error).toHaveBeenCalledTimes(0);
      });
    });
    describe('#deleteContact()', () => {
      it('should throw an error if required input is not provided', async () => {
        await expect(crmClient.deleteContact({})).rejects.toThrow();
        expect(loggerMock.error).toHaveBeenCalledTimes(1);
      });

      it('should throw an error if the request fails', async () => {
        // This test fails when using the real hubspot API, because the record is updated successfully
        if (crmClient.isMocked) {
          hubspotMock.contacts.delete.mockRejectedValueOnce(new Error(ERROR_MESSAGE));
          await expect(crmClient.deleteContact({ id: 123 })).rejects.toThrow();
          expect(loggerMock.error).toHaveBeenCalledTimes(1);
        }
      });

      it('should delete a contact record when the request succeeds', async () => {
        hubspotMock.contacts.delete.mockResolvedValueOnce({
          vid: 123,
          deleted: true,
        });
        const result = await crmClient.deleteContact({ id: 123 });
        expect(result).toBeDefined();
        expect(result.vid).toBe(123);
        expect(result.deleted).toBe(true);
        expect(loggerMock.error).toHaveBeenCalledTimes(0);
      });
    });
  });

  describe('Deals', () => {
    describe('#getDealById()', () => {
      it('should return undefined if required input is not provided', async () => {
        const deal = await crmClient.getDealById();
        await expect(deal).toBeUndefined();
        expect(loggerMock.error).toHaveBeenCalledTimes(0);
      });

      it('should populate the result properly', async () => {
        hubspotMock.deals.getById.mockResolvedValueOnce(getDealResponse());
        const result = await crmClient.getDealById(TEST_DEAL_ID);
        expect(result.crm).toBe('hubspot');
        expect(result.crmType).toBe('deal');
        expect(result.crmId).toBe(TEST_DEAL_ID);
        expect(loggerMock.error).toHaveBeenCalledTimes(0);
      });

      it('should have custom properties', async () => {
        hubspotMock.deals.getById.mockResolvedValueOnce(getDealResponse());
        const result = await crmClient.getDealById(TEST_DEAL_ID);
        expect(result.ixc_account_id).toBeDefined();
        expect(result.ix_chargify_id).toBeDefined();
        expect(result.ix_chargify_link).toBeDefined();
        expect(loggerMock.error).toHaveBeenCalledTimes(0);
      });
    });

    describe('#createDeal()', () => {
      let params;

      beforeEach(() => {
        params = {
          // reference: 'some-reference',
          dealname: 'Foo Deal',
          dealtype: 'newbusiness',
          amount: '1000',
        };
      });

      it('should throw an error if required input is not provided', async () => {
        await expect(crmClient.createDeal({})).rejects.toThrow();
        expect(loggerMock.error).toHaveBeenCalledTimes(1);
      });

      it('should throw an error if the request fails', async () => {
        // This test fails when using the real hubspot API, because the record is created successfully
        if (crmClient.isMocked) {
          hubspotMock.deals.create.mockRejectedValueOnce(new Error(ERROR_MESSAGE));
          await expect(crmClient.createDeal(params)).rejects.toThrow();
          expect(loggerMock.error).toHaveBeenCalledTimes(1);
        }
      });

      it('should create and return a deal record when the request succeeds', async () => {
        hubspotMock.deals.create.mockResolvedValueOnce(
          getDealResponse({
            dealId: 'an-id',
            properties: {
              dealname: params.dealname,
              dealtype: params.dealtype,
              amount: params.amount,
            },
          })
        );
        const result = await crmClient.createDeal(params);
        expect(result).toBeDefined();
        expect(result.dealname).toBe(params.dealname);
        expect(result.dealtype).toBe(params.dealtype);
        expect(result.amount).toBe(params.amount);
        expect(loggerMock.error).toHaveBeenCalledTimes(0);
      });
    });

    describe('#updateDeal()', () => {
      let params;

      beforeEach(() => {
        params = {
          // reference: 'some-reference',
          dealname: 'Foo Deal',
          dealtype: 'newbusiness',
          amount: '1000',
        };
      });

      it('should throw an error if required input is not provided', async () => {
        await expect(crmClient.updateDeal({})).rejects.toThrow();
        expect(loggerMock.error).toHaveBeenCalledTimes(1);
      });

      it('should throw an error if the request fails', async () => {
        // This test fails when using the real hubspot API, because the record is updated successfully
        if (crmClient.isMocked) {
          hubspotMock.deals.updateById.mockRejectedValueOnce(new Error(ERROR_MESSAGE));
          await expect(crmClient.updateDeal(TEST_DEAL_ID, params)).rejects.toThrow();
          expect(loggerMock.error).toHaveBeenCalledTimes(1);
        }
      });

      it('should create and return a deal record when the request succeeds', async () => {
        hubspotMock.deals.updateById.mockResolvedValueOnce(
          getDealResponse({
            dealId: 'an-id',
            properties: {
              dealname: params.dealname,
              dealtype: params.dealtype,
              amount: params.amount,
            },
          })
        );
        const result = await crmClient.updateDeal({ id: TEST_DEAL_ID, params });
        expect(result).toBeDefined();
        expect(result.dealname).toBe(params.dealname);
        expect(result.dealtype).toBe(params.dealtype);
        expect(result.amount).toBe(params.amount);
        expect(loggerMock.error).toHaveBeenCalledTimes(0);
      });
    });

    describe('#addContactToDeal()', () => {
      it('should throw an error if required input is not provided', async () => {
        await expect(crmClient.addContactToDeal()).rejects.toThrow();
        await expect(crmClient.addContactToDeal({ dealId: 1 })).rejects.toThrow();
        await expect(crmClient.addContactToDeal({ contactVid: 1 })).rejects.toThrow();
        expect(loggerMock.error).toHaveBeenCalledTimes(3);
      });

      it('should throw an error if the request fails', async () => {
        // This test fails when using the real hubspot API, because first
        // runs will create records that are found and returned by subsequent runs.
        if (crmClient.isMocked) {
          hubspotMock.deals.associate.mockRejectedValueOnce(new Error(ERROR_MESSAGE));
          await expect(crmClient.addContactToDeal({ dealId: TEST_DEAL_ID, contactVid: TEST_CONTACT_ID })).rejects.toThrow();
          expect(loggerMock.error).toHaveBeenCalledTimes(1);
        }
      });

      it('should associate the specified contact with the deal', async () => {
        hubspotMock.deals.associate.mockResolvedValueOnce({ success: true });
        const result = await crmClient.addContactToDeal({ dealId: TEST_DEAL_ID, contactVid: TEST_CONTACT_ID });
        expect(result.success).toBe(true);
        expect(loggerMock.error).toHaveBeenCalledTimes(0);
      });
    });

    describe('#addCompanyToDeal()', () => {
      it('should throw an error if required input is not provided', async () => {
        await expect(crmClient.addCompanyToDeal()).rejects.toThrow();
        await expect(crmClient.addCompanyToDeal({ dealId: 1 })).rejects.toThrow();
        await expect(crmClient.addCompanyToDeal({ companyId: 1 })).rejects.toThrow();
        expect(loggerMock.error).toHaveBeenCalledTimes(3);
      });

      it('should throw an error if the request fails', async () => {
        // This test fails when using the real hubspot API, because first
        // runs will create records that are found and returned by subsequent runs.
        if (crmClient.isMocked) {
          hubspotMock.deals.associate.mockRejectedValueOnce(new Error(ERROR_MESSAGE));
          await expect(crmClient.addCompanyToDeal({ dealId: TEST_DEAL_ID, companyId: TEST_COMPANY_ID })).rejects.toThrow();
          expect(loggerMock.error).toHaveBeenCalledTimes(1);
        }
      });

      it('should associate the specified company with the deal', async () => {
        hubspotMock.deals.associate.mockResolvedValueOnce({ success: true });
        const result = await crmClient.addCompanyToDeal({ dealId: TEST_DEAL_ID, companyId: TEST_COMPANY_ID });
        expect(result.success).toBe(true);
        expect(loggerMock.error).toHaveBeenCalledTimes(0);
      });
    });

    describe('#removeContactFromDeal()', () => {
      it('should throw an error if required input is not provided', async () => {
        await expect(crmClient.removeContactFromDeal()).rejects.toThrow();
        await expect(crmClient.removeContactFromDeal({ dealId: 1 })).rejects.toThrow();
        await expect(crmClient.removeContactFromDeal({ contactVid: 1 })).rejects.toThrow();
        expect(loggerMock.error).toHaveBeenCalledTimes(3);
      });

      it('should throw an error if the request fails', async () => {
        // This test fails when using the real hubspot API, because first
        // runs will create records that are found and returned by subsequent runs.
        if (crmClient.isMocked) {
          hubspotMock.deals.removeAssociation.mockRejectedValueOnce(new Error(ERROR_MESSAGE));
          await expect(crmClient.removeContactFromDeal({ dealId: TEST_DEAL_ID, contactVid: TEST_CONTACT_ID })).rejects.toThrow();
          expect(loggerMock.error).toHaveBeenCalledTimes(1);
        }
      });

      it('should remove the specified contact from the deal', async () => {
        hubspotMock.deals.removeAssociation.mockResolvedValueOnce({ success: true });
        const result = await crmClient.removeContactFromDeal({ dealId: TEST_DEAL_ID, contactVid: TEST_CONTACT_ID });
        expect(result.success).toBe(true);
        expect(loggerMock.error).toHaveBeenCalledTimes(0);
      });
    });

    describe('#removeCompanyFromDeal()', () => {
      it('should throw an error if required input is not provided', async () => {
        await expect(crmClient.removeCompanyFromDeal()).rejects.toThrow();
        await expect(crmClient.removeCompanyFromDeal({ dealId: 1 })).rejects.toThrow();
        await expect(crmClient.removeCompanyFromDeal({ companyId: 1 })).rejects.toThrow();
        expect(loggerMock.error).toHaveBeenCalledTimes(3);
      });

      it('should throw an error if the request fails', async () => {
        // This test fails when using the real hubspot API, because first
        // runs will create records that are found and returned by subsequent runs.
        if (crmClient.isMocked) {
          hubspotMock.deals.removeAssociation.mockRejectedValueOnce(new Error(ERROR_MESSAGE));
          await expect(crmClient.removeCompanyFromDeal({ dealId: TEST_DEAL_ID, companyId: TEST_COMPANY_ID })).rejects.toThrow();
          expect(loggerMock.error).toHaveBeenCalledTimes(1);
        }
      });

      it('should remove the specified company from the deal', async () => {
        hubspotMock.deals.removeAssociation.mockResolvedValueOnce({ success: true });
        const result = await crmClient.removeCompanyFromDeal({ dealId: TEST_DEAL_ID, companyId: TEST_COMPANY_ID });
        expect(result.success).toBe(true);
        expect(loggerMock.error).toHaveBeenCalledTimes(0);
      });
    });

    describe('#deleteDeal', () => {
      it('should throw an error if required input is not provided', async () => {
        await expect(crmClient.deleteDeal({})).rejects.toThrow();
        expect(loggerMock.error).toHaveBeenCalledTimes(1);
      });

      it('should throw an error if the request fails', async () => {
        // This test fails when using the real hubspot API, because first
        // runs will create records that are found and returned by subsequent runs.
        if (crmClient.isMocked) {
          hubspotMock.deals.deleteById.mockRejectedValueOnce(new Error(ERROR_MESSAGE));
          await expect(crmClient.deleteDeal({ id: TEST_DEAL_ID })).rejects.toThrow();
          expect(loggerMock.error).toHaveBeenCalledTimes(1);
        }
      });

      it('should delete a deal and all associated companies and contacts', async () => {
        hubspotMock.deals.deleteById.mockResolvedValueOnce({ success: true });
        const result = await crmClient.deleteDeal({ id: TEST_DEAL_ID });
        expect(result.success).toBe(true);
        expect(hubspotMock.deals.deleteById).toHaveBeenCalledTimes(1);
        expect(loggerMock.error).toHaveBeenCalledTimes(0);
      });
    });

    describe('#purgeDeal', () => {
      it('should throw an error if required input is not provided', async () => {
        await expect(crmClient.purgeDeal()).rejects.toThrow();
        expect(loggerMock.error).toHaveBeenCalledTimes(1);
      });

      it('should throw an error if the request fails', async () => {
        // This test fails when using the real hubspot API, because first
        // runs will create records that are found and returned by subsequent runs.
        if (crmClient.isMocked) {
          hubspotMock.deals.getById.mockRejectedValueOnce(new Error(ERROR_MESSAGE));
          await expect(crmClient.purgeDeal(TEST_DEAL_ID)).rejects.toThrow();
          expect(loggerMock.error).toHaveBeenCalledTimes(1);
        }
      });

      it('should delete a deal and all associated companies and contacts', async () => {
        const dealId = '3257305336';

        hubspotMock.deals.getById.mockResolvedValueOnce(getDealResponse({ id: dealId }));
        hubspotMock.companies.delete.mockResolvedValueOnce({
          companyId: 4512375702,
          deleted: true,
        });
        hubspotMock.companies.delete.mockResolvedValueOnce({
          companyId: 444444444,
          deleted: true,
        });
        hubspotMock.contacts.delete.mockResolvedValueOnce({
          vid: 132,
          deleted: true,
          reason: 'OK',
        });
        hubspotMock.contacts.delete.mockResolvedValueOnce({
          vid: 456,
          deleted: true,
          reason: 'OK',
        });
        hubspotMock.deals.deleteById.mockResolvedValueOnce({ success: true });

        const result = await crmClient.purgeDeal(dealId);
        expect(result.success).toBe(true);
        expect(hubspotMock.companies.delete).toHaveBeenCalledTimes(2);
        expect(hubspotMock.contacts.delete).toHaveBeenCalledTimes(2);
        expect(hubspotMock.deals.deleteById).toHaveBeenCalledTimes(1);
        expect(loggerMock.error).toHaveBeenCalledTimes(0);
      });
    });
  });

  describe('SRM Search', () => {
    describe('#searchCrm()', () => {
      it('should throw an error if required input is not provided', async () => {
        await expect(crmClient.searchCrm({ crmObjectType: 'foo' })).rejects.toThrow();
        expect(loggerMock.error).toHaveBeenCalledTimes(1);
      });

      it('should throw an error if the request fails', async () => {
        // This test fails when using the real hubspot API, because first
        // runs will create records that are found and returned by subsequent runs.
        if (crmClient.isMocked) {
          hubspotMock.apiRequest.mockRejectedValueOnce(new Error(ERROR_MESSAGE));
          await expect(crmClient.searchCrm({ crmObjectType: 'contacts', searchText: 'test' })).rejects.toThrow();
          expect(loggerMock.error).toHaveBeenCalledTimes(1);
        }
      });

      it('should return an array of normalized results when the search succeeds', async () => {
        const ixcAccountId = '15f64b4d-cf8c-470b-a282-0e9d48f4d83d';
        hubspotMock.apiRequest.mockResolvedValueOnce(
          getSearchCrmResponse({
            crmObjectType: 'companies',
            returnProps: ['name', 'domain', 'createdate', 'hs_lastmodifieddate', 'ixc_account_id'],
            id: TEST_COMPANY_ID,
            createdAt: '2021-07-15T19:38:13.365Z',
            updatedAt: '2021-08-02T23:05:41.693Z',
            archived: false,
            properties: {
              hs_object_id: '6581530659',
              name: "Mr meenan's toolbox Co",
              domain: 'inspectionxpert.com',
              ixc_account_id: ixcAccountId,
              createdate: '2021-07-15T19:38:13.365Z',
              hs_lastmodifieddate: '2021-08-02T23:05:41.693Z',
            },
          })
        );
        const results = await crmClient.searchCrm({
          crmObjectType: 'companies',
          filterValues: {
            ixc_account_id: { value: ixcAccountId },
          },
        });
        expect(results.length).toBe(1);
        const result = results[0];
        expect(result.crm).toBe('hubspot');
        expect(result.crmType).toBe('company');
        expect(result.crmId).toBe(TEST_COMPANY_ID);
        expect(result.name).toBe("Mr meenan's toolbox Co");
        expect(result.domain).toBe('inspectionxpert.com');
        expect(result.ixc_account_id).toBe(ixcAccountId);
        expect(loggerMock.error).toHaveBeenCalledTimes(0);
      });
    });
  });

  describe('Associations', () => {
    describe('#getAssociations()', () => {
      it('should throw an error if required input is not provided', async () => {
        await expect(crmClient.getAssociations()).rejects.toThrow();
        await expect(crmClient.getAssociations({ objectId: 1 })).rejects.toThrow();
        await expect(crmClient.getAssociations({ associationId: 1 })).rejects.toThrow();
        expect(loggerMock.error).toHaveBeenCalledTimes(3);
      });

      it('should throw an error if the request fails', async () => {
        // This test fails when using the real hubspot API, because first
        // runs will create records that are found and returned by subsequent runs.
        if (crmClient.isMocked) {
          hubspotMock.apiRequest.mockRejectedValueOnce(new Error(ERROR_MESSAGE));
          await expect(crmClient.getAssociations({ objectId: TEST_PARENT_COMPANY_ID, associationId: 13 })).rejects.toThrow();
          expect(loggerMock.error).toHaveBeenCalledTimes(1);
        }
      });

      it('should remove the specified company from the deal', async () => {
        hubspotMock.apiRequest.mockResolvedValueOnce({
          results: [4715665000],
          hasMore: false,
          offset: 4715665000,
        });
        const result = await crmClient.getAssociations({ objectId: TEST_PARENT_COMPANY_ID, associationId: 13 });
        expect(result.length).toBe(1);
        expect(result[0]).toBe(4715665000);
        expect(loggerMock.error).toHaveBeenCalledTimes(0);
      });
    });

    describe('#getChildCompanyIds()', () => {
      it('should throw an error if required input is not provided', async () => {
        await expect(crmClient.getChildCompanyIds()).rejects.toThrow();
      });

      it('should throw an error if the request fails', async () => {
        // This test fails when using the real hubspot API, because first
        // runs will create records that are found and returned by subsequent runs.
        if (crmClient.isMocked) {
          hubspotMock.apiRequest.mockRejectedValueOnce(new Error(ERROR_MESSAGE));
          await expect(crmClient.getChildCompanyIds({ companyId: TEST_PARENT_COMPANY_ID })).rejects.toThrow();
          expect(loggerMock.error).toHaveBeenCalledTimes(1);
        }
      });

      it('should remove the specified company from the deal', async () => {
        hubspotMock.apiRequest.mockResolvedValueOnce({
          results: [TEST_CHILD_COMPANY_ID],
          hasMore: false,
          offset: TEST_CHILD_COMPANY_ID,
        });
        const result = await crmClient.getChildCompanyIds({ companyId: TEST_PARENT_COMPANY_ID });
        expect(result.length).toBe(1);
        expect(result[0]).toBe(TEST_CHILD_COMPANY_ID);
        expect(loggerMock.error).toHaveBeenCalledTimes(0);
      });
    });

    describe('#getParentCompanyIds()', () => {
      it('should throw an error if required input is not provided', async () => {
        await expect(crmClient.getParentCompanyIds()).rejects.toThrow();
      });

      it('should throw an error if the request fails', async () => {
        // This test fails when using the real hubspot API, because first
        // runs will create records that are found and returned by subsequent runs.
        if (crmClient.isMocked) {
          hubspotMock.apiRequest.mockRejectedValueOnce(new Error(ERROR_MESSAGE));
          await expect(crmClient.getParentCompanyIds({ companyId: TEST_CHILD_COMPANY_ID })).rejects.toThrow();
          expect(loggerMock.error).toHaveBeenCalledTimes(1);
        }
      });

      it('should remove the specified company from the deal', async () => {
        hubspotMock.apiRequest.mockResolvedValueOnce({
          results: [TEST_PARENT_COMPANY_ID],
          hasMore: false,
          offset: TEST_PARENT_COMPANY_ID,
        });
        const result = await crmClient.getParentCompanyIds({ companyId: TEST_CHILD_COMPANY_ID });
        expect(result.length).toBe(1);
        expect(result[0]).toBe(TEST_PARENT_COMPANY_ID);
        expect(loggerMock.error).toHaveBeenCalledTimes(0);
      });
    });

    describe('#getCompanysAssociatedContactIds()', () => {
      it('should throw an error if required input is not provided', async () => {
        await expect(crmClient.getCompanysAssociatedContactIds()).rejects.toThrow();
      });

      it('should throw an error if the request fails', async () => {
        // This test fails when using the real hubspot API, because first
        // runs will create records that are found and returned by subsequent runs.
        if (crmClient.isMocked) {
          hubspotMock.apiRequest.mockRejectedValueOnce(new Error(ERROR_MESSAGE));
          await expect(crmClient.getCompanysAssociatedContactIds({ companyId: TEST_CHILD_COMPANY_ID })).rejects.toThrow();
          expect(loggerMock.error).toHaveBeenCalledTimes(1);
        }
      });

      it('should remove the specified company from the deal', async () => {
        hubspotMock.apiRequest.mockResolvedValueOnce({
          results: [TEST_PARENT_COMPANY_ID],
          hasMore: false,
          offset: TEST_PARENT_COMPANY_ID,
        });
        const result = await crmClient.getCompanysAssociatedContactIds({ companyId: TEST_CHILD_COMPANY_ID });
        expect(result.length).toBe(1);
        expect(result[0]).toBe(TEST_PARENT_COMPANY_ID);
        expect(loggerMock.error).toHaveBeenCalledTimes(0);
      });
    });

    describe('#getCompanysAssociatedDealIds()', () => {
      it('should throw an error if required input is not provided', async () => {
        await expect(crmClient.getCompanysAssociatedDealIds()).rejects.toThrow();
      });

      it('should throw an error if the request fails', async () => {
        // This test fails when using the real hubspot API, because first
        // runs will create records that are found and returned by subsequent runs.
        if (crmClient.isMocked) {
          hubspotMock.apiRequest.mockRejectedValueOnce(new Error(ERROR_MESSAGE));
          await expect(crmClient.getCompanysAssociatedDealIds({ companyId: TEST_CHILD_COMPANY_ID })).rejects.toThrow();
          expect(loggerMock.error).toHaveBeenCalledTimes(1);
        }
      });

      it('should remove the specified company from the deal', async () => {
        hubspotMock.apiRequest.mockResolvedValueOnce({
          results: [TEST_PARENT_COMPANY_ID],
          hasMore: false,
          offset: TEST_PARENT_COMPANY_ID,
        });
        const result = await crmClient.getCompanysAssociatedDealIds({ companyId: TEST_CHILD_COMPANY_ID });
        expect(result.length).toBe(1);
        expect(result[0]).toBe(TEST_PARENT_COMPANY_ID);
        expect(loggerMock.error).toHaveBeenCalledTimes(0);
      });
    });
  });

  describe('Timeline Events', () => {
    describe('#loadLocalTimelineEventTypes()', () => {
      it('should build the JSON objects in into a reference object', async () => {
        const result = await crmClient.loadLocalTimelineEventTypes();
        expect(result).toBeDefined();
        expect(result.contacts).toBeDefined();
        expect(result.companies).toBeDefined();
        expect(result.deals).toBeDefined();
        expect(result.tickets).toBeDefined();
        // Verify an event
        expect(result.contacts['Drawing Uploaded']).toBeDefined();
        const event = result.contacts['Drawing Uploaded'];
        expect(event.name).toBe('Drawing Uploaded');
        expect(event.objectType).toBe('contacts');
        expect(loggerMock.error).toHaveBeenCalledTimes(0);
      });
    });

    describe('#buildTimelineEventReference()', () => {
      it('should map local events to events that are already registered', async () => {
        hubspotMock.apiRequest.mockResolvedValueOnce(TIMELINE_EVENT_TYPES_LIST_RESPONSE);
        const result = await crmClient.buildTimelineEventReference({ localTimelineEvents: TIMELINE_EVENTS_IN_SOURCE });
        expect(result).toBeDefined();
        expect(result.timelineEventTypes.contacts).toBeDefined();
        expect(result.timelineEventTypes.companies).toBeDefined();
        expect(result.timelineEventTypes.deals).toBeDefined();
        expect(result.timelineEventTypes.tickets).toBeDefined();
        // Verify an event
        expect(result.timelineEventTypes.contacts['Drawing Uploaded']).toBeDefined();
        const event = result.timelineEventTypes.contacts['Drawing Uploaded'];
        expect(event.name).toBe('Drawing Uploaded');
        expect(event.headerTemplate).toBe('Drawing uploaded');
        expect(event.objectType).toBe('contacts');
        expect(loggerMock.error).toHaveBeenCalledTimes(0);
      });

      it('should update registered events if local version change', async () => {
        const newHeaderTemplate = 'Drawing uploaded UPDATE';
        const updatedTimelineEventsInSource = JSON.parse(JSON.stringify(TIMELINE_EVENTS_IN_SOURCE));
        updatedTimelineEventsInSource.contacts['Drawing Uploaded'].headerTemplate = newHeaderTemplate;

        hubspotMock.apiRequest.mockResolvedValueOnce(TIMELINE_EVENT_TYPES_LIST_RESPONSE);
        hubspotMock.updateTimelineEventType.mockResolvedValueOnce({
          name: 'Drawing Uploaded',
          headerTemplate: 'Drawing uploaded UPDATE',
          detailTemplate: 'Drawing uploaded at {{#formatDate timestamp}}{{/formatDate}}',
          tokens: [
            {
              label: 'Drawing Name',
              objectPropertyName: null,
              options: [],
              name: 'drawingName',
              type: 'string',
              createdAt: '2021-06-24T21:59:49.751Z',
              updatedAt: '2021-06-24T21:59:49.751Z',
            },
          ],
          id: '1061693',
          objectType: 'contacts',
          createdAt: '2021-06-24T21:59:49.744Z',
          updatedAt: '2021-06-24T21:59:49.744Z',
        });

        const result = await crmClient.buildTimelineEventReference({ localTimelineEvents: updatedTimelineEventsInSource });
        expect(result).toBeDefined();
        expect(result.timelineEventTypes.contacts).toBeDefined();
        expect(result.timelineEventTypes.companies).toBeDefined();
        expect(result.timelineEventTypes.deals).toBeDefined();
        expect(result.timelineEventTypes.tickets).toBeDefined();
        // Verify an event
        expect(result.timelineEventTypes.contacts['Drawing Uploaded']).toBeDefined();
        const event = result.timelineEventTypes.contacts['Drawing Uploaded'];
        expect(event.name).toBe('Drawing Uploaded');
        expect(event.headerTemplate).toBe(newHeaderTemplate);
        expect(event.objectType).toBe('contacts');
        expect(loggerMock.error).toHaveBeenCalledTimes(0);
      });

      it('should register new events if local events are not found', async () => {
        const newEventType = {
          name: 'Registration Finished',
          headerTemplate: 'Registration Finished',
          detailTemplate: 'Registration Finished at {{#formatDate timestamp}}{{/formatDate}}',
          tokens: [
            {
              name: 'userName',
              type: 'string',
              label: 'User Name',
            },
          ],
        };
        const updatedTimelineEventsInSource = JSON.parse(JSON.stringify(TIMELINE_EVENTS_IN_SOURCE));
        updatedTimelineEventsInSource.contacts['Registration Finished'] = {
          objectType: 'contacts',
          ...newEventType,
        };

        hubspotMock.apiRequest.mockResolvedValueOnce(TIMELINE_EVENT_TYPES_LIST_RESPONSE);
        hubspotMock.apiRequest.mockResolvedValueOnce({
          name: 'Registration Finished',
          headerTemplate: 'Registration Finished',
          detailTemplate: 'Registration Finished at {{#formatDate timestamp}}{{/formatDate}}',
          tokens: [
            {
              label: 'User Name',
              objectPropertyName: null,
              options: [],
              name: 'userName',
              type: 'string',
              createdAt: '2021-06-24T21:59:49.751Z',
              updatedAt: '2021-06-24T21:59:49.751Z',
            },
          ],
          id: '1061693',
          objectType: 'contacts',
          createdAt: '2021-06-24T21:59:49.744Z',
          updatedAt: '2021-06-24T21:59:49.744Z',
        });

        const result = await crmClient.buildTimelineEventReference({ localTimelineEvents: updatedTimelineEventsInSource });
        expect(result).toBeDefined();
        expect(result.timelineEventTypes.contacts).toBeDefined();
        expect(result.timelineEventTypes.companies).toBeDefined();
        expect(result.timelineEventTypes.deals).toBeDefined();
        expect(result.timelineEventTypes.tickets).toBeDefined();
        // Verify an event
        expect(result.timelineEventTypes.contacts['Registration Finished']).toBeDefined();
        const event = result.timelineEventTypes.contacts['Registration Finished'];
        expect(event.name).toBe('Registration Finished');
        expect(event.objectType).toBe('contacts');
        expect(loggerMock.error).toHaveBeenCalledTimes(0);
      });
    });

    describe('#recordTimelineEvent()', () => {
      it('should record an event in each of the associated object timelines', async () => {
        hubspotMock.apiRequest.mockResolvedValueOnce(TIMELINE_EVENT_TYPES_LIST_RESPONSE);
        hubspotMock.apiRequest.mockResolvedValueOnce({
          status: 'COMPLETE',
          results: [
            {
              id: '9a31ab30-8346-447f-8da6-b012d1bee824',
              eventTemplateId: '1061693',
              email: null,
              objectId: '2551',
              utk: null,
              domain: null,
              timestamp: '2021-06-25T02:32:58.662Z',
              tokens: { drawingName: 'Test Drawing Name' },
              extraData: { firstName: 'John', secondName: 'Doe' },
              timelineIFrame: null,
              objectType: 'contacts',
              createdAt: null,
            },
            {
              id: '25aa1e82-9eda-479f-aae7-e64e214e2123',
              eventTemplateId: '1061696',
              email: null,
              objectId: '4781384174',
              utk: null,
              domain: null,
              timestamp: '2021-06-25T02:32:58.662Z',
              tokens: { drawingName: 'Test Drawing Name' },
              extraData: { firstName: 'John', secondName: 'Doe' },
              timelineIFrame: null,
              objectType: 'companies',
              createdAt: null,
            },
          ],
          startedAt: '2021-06-24T04:37:48.937Z',
          completedAt: '2021-06-24T04:37:49.159Z',
        });

        const recordEventParams = {
          eventName: 'Drawing Uploaded',
          contactId: 2551, // https://app.hubspot.com/contacts/8445733/contact/2551
          companyId: 4781384174, // https://app.hubspot.com/contacts/8445733/company/4781384174
          // dealId: 3312224416, // https://app.hubspot.com/contacts/8445733/deal/3312224416/
          tokens: {
            drawingName: 'Test Drawing Name',
          },
          extraData: {
            firstName: 'John',
            secondName: 'Doe',
          },
        };

        // Only used when mocking
        if (crmClient.isMocked) recordEventParams.localTimelineEvents = TIMELINE_EVENTS_IN_SOURCE;

        const result = await crmClient.recordTimelineEvent(recordEventParams);
        expect(result.results.length).toBe(2);
        expect(result.status).toBe('COMPLETE');
        const event = result.results[0];
        expect(event.id).toBeDefined();
        expect(event.eventTemplateId).toBeDefined();
        expect(event.timestamp).toBeDefined();
        expect(event.tokens.drawingName).toBe('Test Drawing Name');
        expect(event.extraData.firstName).toBe('John');
        expect(loggerMock.error).toHaveBeenCalledTimes(0);
      });
    });

    describe('#listTimelineEventTypes()', () => {
      it('should throw an error if the request fails', async () => {
        // This test fails when using the real hubspot API, because first
        // runs will create records that are found and returned by subsequent runs.
        if (crmClient.isMocked) {
          hubspotMock.apiRequest.mockRejectedValueOnce(new Error(ERROR_MESSAGE));
          await expect(crmClient.listTimelineEventTypes()).rejects.toThrow();
          expect(loggerMock.error).toHaveBeenCalledTimes(1);
        }
      });

      it('should return an empty array when no event types exist', async () => {
        hubspotMock.apiRequest.mockResolvedValueOnce({
          results: [],
        });
        const result = await crmClient.listTimelineEventTypes();
        expect(result.length).toBe(0);
        expect(loggerMock.error).toHaveBeenCalledTimes(0);
      });

      it('should return an array of event types', async () => {
        hubspotMock.apiRequest.mockResolvedValueOnce({
          results: [
            {
              name: 'Report Generated',
              headerTemplate: 'Report generated',
              detailTemplate: 'Report generated at {{#formatDate timestamp}}{{/formatDate}}',
              tokens: [
                {
                  label: 'Report Name',
                  objectPropertyName: null,
                  options: [],
                  name: 'reportName',
                  type: 'string',
                  createdAt: '2021-06-23T17:51:41.079Z',
                  updatedAt: '2021-06-23T17:51:41.079Z',
                },
              ],
              id: '1061669',
              objectType: 'companies',
              createdAt: '2021-06-23T17:51:41.072Z',
              updatedAt: '2021-06-23T17:51:41.072Z',
            },
            {
              name: 'Report Generated',
              headerTemplate: 'Report generated',
              detailTemplate: 'Report generated at {{#formatDate timestamp}}{{/formatDate}}',
              tokens: [
                {
                  label: 'Report Name',
                  objectPropertyName: null,
                  options: [],
                  name: 'reportName',
                  type: 'string',
                  createdAt: '2021-06-23T17:51:41.079Z',
                  updatedAt: '2021-06-23T17:51:41.079Z',
                },
              ],
              id: '1061671',
              objectType: 'deals',
              createdAt: '2021-06-23T17:51:41.350Z',
              updatedAt: '2021-06-23T17:51:41.350Z',
            },
          ],
        });
        const result = await crmClient.listTimelineEventTypes();
        expect(result.length).toBe(2);
        expect(result[0].name).toBeDefined();
        expect(result[0].headerTemplate).toBeDefined();
        expect(result[0].detailTemplate).toBeDefined();
        expect(result[0].tokens).toBeDefined();
        expect(loggerMock.error).toHaveBeenCalledTimes(0);
      });
    });

    describe('#createTimelineEventType()', () => {
      let params;

      beforeEach(() => {
        params = {
          objectType: 'contacts',
          name: 'Drawing Upload',
          headerTemplate: 'Drawing uploaded',
          detailTemplate: 'Drawing uploaded at {{#formatDate timestamp}}{{/formatDate}}',
          tokens: [
            {
              name: 'drawingName',
              type: 'string',
              label: 'Drawing Name',
            },
          ],
        };
      });

      it('should throw an error if required input is not provided', async () => {
        await expect(crmClient.createTimelineEventType({})).rejects.toThrow();
        expect(loggerMock.error).toHaveBeenCalledTimes(1);
      });

      it('should throw an error if the request fails', async () => {
        // This test fails when using the real hubspot API, because the record is created successfully
        if (crmClient.isMocked) {
          hubspotMock.apiRequest.mockRejectedValueOnce(new Error(ERROR_MESSAGE));
          await expect(crmClient.createTimelineEventType(params)).rejects.toThrow();
          expect(loggerMock.error).toHaveBeenCalledTimes(1);
        }
      });

      it('should create and return an event template record', async () => {
        hubspotMock.apiRequest.mockResolvedValueOnce({
          id: '1056457',
          objectType: 'contacts',
          name: 'Drawing Upload',
          headerTemplate: 'Drawing uploaded',
          detailTemplate: 'Drawing uploaded at {{#formatDate timestamp}}{{/formatDate}}',
          tokens: [
            {
              label: 'Drawing Name',
              objectPropertyName: null,
              options: [],
              name: 'drawingName',
              type: 'string',
              createdAt: '2021-06-03T03:13:25.789Z',
              updatedAt: '2021-06-03T03:13:25.789Z',
            },
          ],
          createdAt: '2021-06-03T03:13:25.780Z',
          updatedAt: '2021-06-03T03:13:25.780Z',
        });
        const result = await crmClient.createTimelineEventType(params);
        expect(result).toBeDefined();
        expect(result.dealname).toBe(params.dealname);
        expect(result.dealtype).toBe(params.dealtype);
        expect(result.amount).toBe(params.amount);
        expect(loggerMock.error).toHaveBeenCalledTimes(0);
      });
    });

    describe('#getTimelineEventType()', () => {
      it('should throw an error if required input is not provided', async () => {
        await expect(crmClient.getTimelineEventType({})).rejects.toThrow();
        expect(loggerMock.error).toHaveBeenCalledTimes(1);
      });

      it('should throw an error if the request fails', async () => {
        // This test fails when using the real hubspot API, because the record is created successfully
        if (crmClient.isMocked) {
          hubspotMock.apiRequest.mockRejectedValueOnce(new Error(ERROR_MESSAGE));
          await expect(crmClient.getTimelineEventType({ eventTemplateId: 12345 })).rejects.toThrow();
          expect(loggerMock.error).toHaveBeenCalledTimes(1);
        }
      });

      it('should create and return an event template record', async () => {
        hubspotMock.apiRequest.mockResolvedValueOnce(TIMELINE_EVENT_SINGLE_RESPONSE);
        const result = await crmClient.getTimelineEventType({ eventTemplateId: '1056457' });
        expect(result).toBeDefined();
        expect(result.name).toBeDefined();
        expect(result.headerTemplate).toBeDefined();
        expect(result.detailTemplate).toBeDefined();
        expect(result.objectType).toBeDefined();
        expect(result.tokens).toBeDefined();
        expect(loggerMock.error).toHaveBeenCalledTimes(0);
      });
    });

    describe('#updateTimelineEventType()', () => {
      let params;

      beforeEach(() => {
        params = {
          eventTemplateId: '1056457',
          objectType: 'contacts',
          name: 'Drawing Upload Updated',
          headerTemplate: 'Drawing uploaded Updated',
          detailTemplate: 'Drawing uploaded updated at {{#formatDate timestamp}}{{/formatDate}}',
          tokens: [
            {
              name: 'drawingName',
              type: 'string',
              label: 'Drawing Name',
            },
            {
              name: 'uploadedBy',
              type: 'string',
              label: 'Uploaded By',
            },
          ],
        };
      });

      it('should throw an error if required input is not provided', async () => {
        await expect(crmClient.updateTimelineEventType({})).rejects.toThrow();
        expect(loggerMock.error).toHaveBeenCalledTimes(1);
      });

      it('should throw an error if the request fails', async () => {
        // This test fails when using the real hubspot API, because the record is created successfully
        if (crmClient.isMocked) {
          hubspotMock.apiRequest.mockRejectedValueOnce(new Error(ERROR_MESSAGE));
          await expect(crmClient.updateTimelineEventType(params)).rejects.toThrow();
          expect(loggerMock.error).toHaveBeenCalledTimes(1);
        }
      });

      it('should create and return an event template record', async () => {
        hubspotMock.apiRequest.mockResolvedValueOnce(TIMELINE_EVENT_SINGLE_RESPONSE);
        const result = await crmClient.updateTimelineEventType(params);
        expect(result).toBeDefined();
        expect(result.name).toBeDefined();
        expect(result.headerTemplate).toBeDefined();
        expect(result.detailTemplate).toBeDefined();
        expect(result.objectType).toBeDefined();
        expect(result.tokens).toBeDefined();
        expect(loggerMock.error).toHaveBeenCalledTimes(0);
      });
    });

    describe('#createTimelineEvent()', () => {
      let params;

      beforeEach(() => {
        params = {
          eventTemplateId: '1056457',
          objectId: '32051',
          tokens: {
            drawingName: 'Test Drawing',
            uploadedBy: 'User name who uploaded',
          },
          extraData: {
            keyA: 'Value A',
            keyB: {
              nestedKeyC: 'Nested Value C',
              nestedKeyD: ['array item A', 'array item zb', 'array item C'],
            },
          },
        };
      });

      it('should throw an error if required input is not provided', async () => {
        await expect(crmClient.createTimelineEvent({})).rejects.toThrow();
        expect(loggerMock.error).toHaveBeenCalledTimes(1);
      });

      it('should throw an error if the request fails', async () => {
        // This test fails when using the real hubspot API, because the record is created successfully
        if (crmClient.isMocked) {
          hubspotMock.apiRequest.mockRejectedValueOnce(new Error(ERROR_MESSAGE));
          await expect(crmClient.createTimelineEvent(params)).rejects.toThrow();
          expect(loggerMock.error).toHaveBeenCalledTimes(1);
        }
      });

      it('should create and return an event template record', async () => {
        hubspotMock.apiRequest.mockResolvedValueOnce({
          id: 'e6dead43-153b-4874-b38a-06ba67f51b94',
          eventTemplateId: '1056457',
          email: null,
          objectId: '32351',
          utk: null,
          domain: null,
          timestamp: '2021-06-04T02:39:56.357Z',
          tokens: {
            uploadedBy: 'User name who uploaded',
            drawingName: 'Test Drawing',
          },
          extraData: {
            keyA: 'Value A',
            keyB: {
              nestedKeyC: 'Nested Value C',
              nestedKeyD: ['array item A', 'array item zb', 'array item C'],
            },
          },
          timelineIFrame: null,
          objectType: 'contacts',
          createdAt: null,
        });
        const result = await crmClient.createTimelineEvent(params);
        expect(result).toBeDefined();
        expect(result.eventTemplateId).toBeDefined();
        expect(result.timestamp).toBeDefined();
        expect(result.objectId).toBeDefined();
        expect(result.objectType).toBeDefined();
        expect(result.tokens).toBeDefined();
        expect(result.extraData).toBeDefined();
        expect(loggerMock.error).toHaveBeenCalledTimes(0);
      });
    });

    describe('#createTimelineEvents()', () => {
      let params;

      beforeEach(() => {
        params = {
          inputs: [
            {
              eventTemplateId: '1056457',
              objectId: '32051',
              tokens: {
                drawingName: 'Test Drawing',
                uploadedBy: 'User name who uploaded',
              },
              extraData: {
                keyA: 'Value A',
                keyB: {
                  nestedKeyC: 'Nested Value C',
                  nestedKeyD: ['array item A', 'array item zb', 'array item C'],
                },
              },
            },
            // {
            //   eventTemplateId: '1056457',
            //   objectId: '6139060607',
            //   tokens: {
            //     drawingName: 'Test Drawing',
            //     uploadedBy: 'User name who uploaded',
            //   },
            //   extraData: {
            //     keyA: 'Value A',
            //     keyB: {
            //       nestedKeyC: 'Nested Value C',
            //       nestedKeyD: ['array item A', 'array item zb', 'array item C'],
            //     },
            //   },
            // },
            // {
            //   eventTemplateId: '1056457',
            //   objectId: '5401105950',
            //   tokens: {
            //     drawingName: 'Test Drawing',
            //     uploadedBy: 'User name who uploaded',
            //   },
            //   extraData: {
            //     keyA: 'Value A',
            //     keyB: {
            //       nestedKeyC: 'Nested Value C',
            //       nestedKeyD: ['array item A', 'array item zb', 'array item C'],
            //     },
            //   },
            // },
          ],
        };
      });

      it('should throw an error if required input is not provided', async () => {
        await expect(crmClient.createTimelineEvents()).rejects.toThrow();
        expect(loggerMock.error).toHaveBeenCalledTimes(1);
      });

      it('should throw an error if the request fails', async () => {
        // This test fails when using the real hubspot API, because the record is created successfully
        if (crmClient.isMocked) {
          hubspotMock.apiRequest.mockRejectedValueOnce(new Error(ERROR_MESSAGE));
          await expect(crmClient.createTimelineEvents(params)).rejects.toThrow();
          expect(loggerMock.error).toHaveBeenCalledTimes(1);
        }
      });

      it('should create and return an event template record', async () => {
        hubspotMock.apiRequest.mockResolvedValueOnce([
          {
            id: 'e6dead43-153b-4874-b38a-06ba67f51b94',
            eventTemplateId: '1056457',
            email: null,
            objectId: '32351',
            utk: null,
            domain: null,
            timestamp: '2021-06-04T02:39:56.357Z',
            tokens: {
              uploadedBy: 'User name who uploaded',
              drawingName: 'Test Drawing',
            },
            extraData: {
              keyA: 'Value A',
              keyB: {
                nestedKeyC: 'Nested Value C',
                nestedKeyD: ['array item A', 'array item zb', 'array item C'],
              },
            },
            timelineIFrame: null,
            objectType: 'contacts',
            createdAt: null,
          },
        ]);
        const result = await crmClient.createTimelineEvents(params);
        expect(result).toBeDefined();
        // expect(result.length).toBe(3);
        expect(result[0].eventTemplateId).toBeDefined();
        expect(result[0].timestamp).toBeDefined();
        expect(result[0].objectId).toBeDefined();
        expect(result[0].objectType).toBeDefined();
        expect(result[0].tokens).toBeDefined();
        expect(result[0].extraData).toBeDefined();
        expect(loggerMock.error).toHaveBeenCalledTimes(0);
      });
    });
  });

  describe('Helpers', () => {
    describe('#transformToAppSchema()', () => {
      it('should return a flattened Hubspot record', async () => {
        const crmType = 'foo_type';
        const crmId = '123';
        const hubspotRecord = getCompanyResponse();
        const result = HubspotCRMAdapter.transformToAppSchema(crmType, crmId, hubspotRecord);
        expect(loggerMock.error).toHaveBeenCalledTimes(0);
        // Adapter metadata
        expect(result.crm).toBe('hubspot');
        expect(result.crmType).toBe(crmType);
        expect(result.crmId).toBe(crmId);
        // Object properties
        expect(result.name).toBeDefined();
        expect(result.domain).toBeDefined();
        expect(result.phone).toBeDefined();
        expect(result.address).toBeDefined();
        expect(result.address2).toBeDefined();
        expect(result.city).toBeDefined();
        expect(result.state).toBeDefined();
        expect(result.zip).toBeDefined();
        expect(result.country).toBeDefined();
        // Record metadata
        expect(result.createdate).toBeDefined();
        expect(result.hs_lastmodifieddate).toBeDefined(); // Question should we format property names?
      });
    });

    describe('#buildCreateOrUpdatePayloadForCompany()', () => {
      it('should return the object expected by create methods in hubspot', async () => {
        const payload = HubspotCRMAdapter.buildPayloadWithName({ foo: 'bar', baz: 5 });
        expect(payload.properties).toBeDefined();
        expect(payload.properties[0].name).toBe('foo');
        expect(payload.properties[0].value).toBe('bar');
        expect(payload.properties[1].name).toBe('baz');
        expect(payload.properties[1].value).toBe(5);
        expect(loggerMock.error).toHaveBeenCalledTimes(0);
      });
    });

    describe('#buildCreateOrUpdatePayloadForContact()', () => {
      it('should return the object expected by create methods in hubspot', async () => {
        const payload = HubspotCRMAdapter.buildPayloadWithProperty({ foo: 'bar', baz: 5 });
        expect(payload.properties).toBeDefined();
        expect(payload.properties[0].property).toBe('foo');
        expect(payload.properties[0].value).toBe('bar');
        expect(payload.properties[1].property).toBe('baz');
        expect(payload.properties[1].value).toBe(5);
        expect(loggerMock.error).toHaveBeenCalledTimes(0);
      });
    });
  });
});
