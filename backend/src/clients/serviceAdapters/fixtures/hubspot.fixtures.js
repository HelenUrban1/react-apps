// prettier-ignore

// TODO: Remove hardcoded test values. Replace with fixture setup file or nock
export const ERROR_MESSAGE = 'An error message.';
export const TEST_CONTACT_ID = 51;
export const TEST_CONTACT_UPDATE_ID = 101;
export const TEST_COMPANY_ID = 4512375702;
export const TEST_PARENT_COMPANY_ID = 4715713142;
export const TEST_CHILD_COMPANY_ID = 4715665000;
export const TEST_COMPANY_DOMAIN = 'halligan.com';
export const TEST_COMPANY_UPDATE_ID = 4583884606;
export const TEST_DEAL_ID = 3002860068;

/*
Custom properties that need to be added to default Hubspot objects:

Company
  Information Group
    IXC Account ID     ixc_account_id
    IX Chargify ID     ix_chargify_id
    IX Chargify Link   ix_chargify_link

Contact
  Information Group 
    IXC User ID        ixc_user_id

Deals
  Information Group
    IXC Account ID      ix_account_id
    IX Chargify ID      ix_chargify_id
    IX Chargify Link    ix_chargify_link     

  Activity Group
    Trial Started Date   trial_tarted_date
    Trial End Date       trial_end_date
*/

export const PIPELINES = [
  {
    pipelineId: '6510422',
    label: 'Freemium',
    active: true,
    displayOrder: 2,
    staticDefault: false,
    stages: [
      { stageId: '6510423', label: 'Trial Requested', probability: 0.2, active: true, displayOrder: 0, closedWon: false },
      { stageId: '6510424', label: 'First Login', probability: 0.3, active: true, displayOrder: 1, closedWon: false },
      { stageId: '6510425', label: 'Uploaded Drawing 1', probability: 0.4, active: true, displayOrder: 2, closedWon: false },
      { stageId: '6510426', label: 'Uploaded Drawing 2', probability: 0.5, active: true, displayOrder: 3, closedWon: false },
      { stageId: '6510427', label: 'Uploaded Drawing 3', probability: 0.6, active: true, displayOrder: 4, closedWon: false },
      { stageId: '6510535', label: 'Uploaded Drawing 4', probability: 0.7, active: true, displayOrder: 5, closedWon: false },
      { stageId: '6510536', label: 'Uploaded Drawing 5', probability: 0.8, active: true, displayOrder: 6, closedWon: false },
      { stageId: '6510537', label: 'Attempted Uploaded Drawing 6', probability: 0.9, active: true, displayOrder: 7, closedWon: false },
      { stageId: '6510428', label: 'Closed Won', probability: 1, active: true, displayOrder: 8, closedWon: true },
      { stageId: '6510429', label: 'Closed Lost', probability: 0, active: true, displayOrder: 9, closedWon: false },
    ],
  },
  {
    pipelineId: '5743684',
    label: 'Online Trial',
    active: true,
    displayOrder: 1,
    staticDefault: false,
    stages: [
      { stageId: '5743685', label: 'Trial Requested', probability: 0.2, active: true, displayOrder: 0, closedWon: false },
      { stageId: '5743686', label: 'First Login', probability: 0.4, active: true, displayOrder: 1, closedWon: false },
      { stageId: '5743687', label: 'Drawing Ballooned', probability: 0.6, active: true, displayOrder: 2, closedWon: false },
      { stageId: '5743688', label: 'Report Created', probability: 0.8, active: true, displayOrder: 3, closedWon: false },
      { stageId: '5743690', label: 'Closed Won', probability: 1, active: true, displayOrder: 4, closedWon: true },
      { stageId: '5743691', label: 'Closed Lost', probability: 0, active: true, displayOrder: 5, closedWon: false },
    ],
  },
  {
    pipelineId: '5743684',
    label: 'IX3 New Sales Pipeline',
    active: true,
    displayOrder: 1,
    staticDefault: false,
    stages: [
      { stageId: '5743685', label: '1. New IX3', probability: 0.2, active: true, displayOrder: 1, closedWon: false },
      { stageId: '5743690', label: 'Closed (Won) IX3', probability: 1, active: true, displayOrder: 4, closedWon: true },
      { stageId: '5743691', label: 'Closed (Lost) IX3', probability: 0, active: true, displayOrder: 5, closedWon: false },
    ],
  },
];

// export const CRM_SEARCH_RESPONSE = {
//   total: 1,
//   results: [
//     {
//       id: '6581530659',
//       properties: {
//         createdate: '2021-07-15T19:38:13.365Z',
//         domain: 'inspectionxpert.com',
//         hs_lastmodifieddate: '2021-08-02T23:05:41.693Z',
//         hs_object_id: '6581530659',
//         name: "Mr meenan's toolbox Co",
//       },
//       createdAt: '2021-07-15T19:38:13.365Z',
//       updatedAt: '2021-08-02T23:05:41.693Z',
//       archived: false,
//     },
//   ],
// };

export const COMPANY_SINGLE_RESPONSE = {
  portalId: 8445733,
  companyId: 4512375702,
  isDeleted: false,
  properties: {
    domain: {
      value: 'halligan.com',
      timestamp: 1600806236464,
      source: 'BIDEN',
      sourceId: 'jordan@inspectionxpert.com',
      versions: [],
    },
    name: {
      value: 'H+A International',
      timestamp: 1600806236464,
      source: 'BIDEN',
      sourceId: 'jordan@inspectionxpert.com',
      versions: [],
    },
    ix_account_name: {
      value: 'H+A International',
      timestamp: 1600806236464,
      source: 'BIDEN',
      sourceId: 'jordan@inspectionxpert.com',
      versions: [],
    },
    ixc_account_id: {
      value: 'The IXC Account UUID',
      timestamp: 1600806236464,
      source: 'BIDEN',
      sourceId: 'jordan@inspectionxpert.com',
      versions: [],
    },
    ix_chargify_id: {
      value: 'The Charfify Customer UUID',
      timestamp: 1600806236464,
      source: 'BIDEN',
      sourceId: 'jordan@inspectionxpert.com',
      versions: [],
    },
    ix_chargify_link: {
      value: 'United States',
      timestamp: 1600806236464,
      source: 'BIDEN',
      sourceId: 'jordan@inspectionxpert.com',
      versions: [],
    },
    country: {
      value: 'United States',
      timestamp: 1600806236464,
      source: 'BIDEN',
      sourceId: 'jordan@inspectionxpert.com',
      versions: [],
    },
    city: {
      value: 'Chicago',
      timestamp: 1600806236464,
      source: 'BIDEN',
      sourceId: 'jordan@inspectionxpert.com',
      versions: [],
    },
    num_associated_contacts: {
      value: '1',
      timestamp: 1600806237862,
      source: 'CALCULATED',
      sourceId: 'RollupProperties',
      versions: [],
    },
    timezone: {
      value: 'America/Chicago',
      timestamp: 1600806236464,
      source: 'BIDEN',
      sourceId: 'jordan@inspectionxpert.com',
      versions: [],
    },
    facebook_company_page: {
      value: 'https://facebook.com/ha-international-inc-marketing-communications',
      timestamp: 1600806236464,
      source: 'BIDEN',
      sourceId: 'jordan@inspectionxpert.com',
      versions: [],
    },
    createdate: {
      value: '1600806236464',
      timestamp: 1600806236464,
      source: 'CONTACTS',
      sourceId: 'CRM_UI',
      versions: [],
    },
    description: {
      value: 'H+A International is a full service integrated marketing communications company dedicated to helping its clients achieve the most effective use of resources.',
      timestamp: 1600806236464,
      source: 'BIDEN',
      sourceId: 'jordan@inspectionxpert.com',
      versions: [],
    },
    hs_num_blockers: {
      value: '0',
      timestamp: 1600806237376,
      source: 'SALES',
      sourceId: 'AssociationsPropertyWorker',
      versions: [],
    },
    industry: {
      value: 'MARKETING_AND_ADVERTISING',
      timestamp: 1600806236464,
      source: 'BIDEN',
      sourceId: 'jordan@inspectionxpert.com',
      versions: [],
    },
    web_technologies: {
      value: 'google_cloud;go_squared;facebook_connect;google_analytics;google_apps;double_click;google_tag_manager',
      timestamp: 1600806236464,
      source: 'BIDEN',
      sourceId: 'jordan@inspectionxpert.com',
      versions: [],
    },
    numberofemployees: {
      value: '10',
      timestamp: 1600806236464,
      source: 'BIDEN',
      sourceId: 'jordan@inspectionxpert.com',
      versions: [],
    },
    hs_analytics_num_visits: {
      value: '0',
      timestamp: 1600806238589,
      source: 'CALCULATED',
      sourceId: 'company sync triggered by vid=51',
      versions: [],
    },
    linkedin_company_page: {
      value: 'https://www.linkedin.com/company/h-a-international',
      timestamp: 1600806236464,
      source: 'BIDEN',
      sourceId: 'jordan@inspectionxpert.com',
      versions: [],
    },
    hubspot_owner_id: {
      value: '50963116',
      timestamp: 1600806258372,
      source: 'CRM_UI',
      sourceId: 'jordan@inspectionxpert.com',
      versions: [],
    },
    hs_analytics_source: {
      value: 'OFFLINE',
      timestamp: 1600806238589,
      source: 'CALCULATED',
      sourceId: 'company sync triggered by vid=51',
      versions: [],
    },
    annualrevenue: {
      value: '1000000',
      timestamp: 1600806236464,
      source: 'BIDEN',
      sourceId: 'jordan@inspectionxpert.com',
      versions: [],
    },
    hs_created_by_user_id: {
      value: '9283091',
      timestamp: 1600806236464,
      source: 'CONTACTS',
      sourceId: 'CRM_UI',
      versions: [],
    },
    founded_year: {
      value: '1984',
      timestamp: 1600806236464,
      source: 'BIDEN',
      sourceId: 'jordan@inspectionxpert.com',
      versions: [],
    },
    hs_analytics_num_page_views: {
      value: '0',
      timestamp: 1600806238589,
      source: 'CALCULATED',
      sourceId: 'company sync triggered by vid=51',
      versions: [],
    },
    state: {
      value: 'IL',
      timestamp: 1600806236464,
      source: 'BIDEN',
      sourceId: 'jordan@inspectionxpert.com',
      versions: [],
    },
    hs_all_owner_ids: {
      value: '50963116',
      timestamp: 1600806258389,
      source: 'CRM_UI',
      sourceId: 'jordan@inspectionxpert.com',
      versions: [],
    },
    linkedinbio: {
      value: 'H+A International is a full service integrated marketing communications company dedicated to helping its clients achieve the most effective use of resources.',
      timestamp: 1600806236464,
      source: 'BIDEN',
      sourceId: 'jordan@inspectionxpert.com',
      versions: [],
    },
    zip: {
      value: '60601',
      timestamp: 1600806236464,
      source: 'BIDEN',
      sourceId: 'jordan@inspectionxpert.com',
      versions: [],
    },
    website: {
      value: 'halligan.com',
      timestamp: 1600806236464,
      source: 'CALCULATED',
      sourceId: null,
      versions: [],
    },
    address: {
      value: '1195 Mustang Dr',
      timestamp: 1600806236464,
      source: 'BIDEN',
      sourceId: 'jordan@inspectionxpert.com',
      versions: [],
    },
    address2: {
      value: 'STE 1220',
      timestamp: 1600806236464,
      source: 'BIDEN',
      sourceId: 'jordan@inspectionxpert.com',
      versions: [],
    },
    hs_analytics_first_timestamp: {
      value: '1599845113548',
      timestamp: 1600806238589,
      source: 'CALCULATED',
      sourceId: 'company sync triggered by vid=51',
      versions: [],
    },
    hs_user_ids_of_all_owners: {
      value: '9283091',
      timestamp: 1600806258389,
      source: 'CRM_UI',
      sourceId: 'jordan@inspectionxpert.com',
      versions: [],
    },
    first_contact_createdate: {
      value: '1599845113883',
      timestamp: 1600806238589,
      source: 'CALCULATED',
      sourceId: 'company sync triggered by vid=51',
      versions: [],
    },
    twitterhandle: {
      value: 'MarComPros',
      timestamp: 1600806236464,
      source: 'BIDEN',
      sourceId: 'jordan@inspectionxpert.com',
      versions: [],
    },
    hs_target_account_probability: {
      value: '0.38625118136405945',
      timestamp: 1600806240578,
      source: 'SALES',
      sourceId: null,
      versions: [],
    },
    hs_lastmodifieddate: {
      value: '1600806298864',
      timestamp: 1600806298864,
      source: 'CALCULATED',
      sourceId: null,
      versions: [],
    },
    hs_num_decision_makers: {
      value: '0',
      timestamp: 1600806237376,
      source: 'SALES',
      sourceId: 'AssociationsPropertyWorker',
      versions: [],
    },
    hs_predictivecontactscore_v2: {
      value: '2.79',
      timestamp: 1600806238589,
      source: 'CALCULATED',
      sourceId: 'company sync triggered by vid=51',
      versions: [],
    },
    hubspot_owner_assigneddate: {
      value: '1600806258372',
      timestamp: 1600806258372,
      source: 'CRM_UI',
      sourceId: 'jordan@inspectionxpert.com',
      versions: [],
    },
    phone: {
      value: '3123324650',
      timestamp: 1600806236464,
      source: 'BIDEN',
      sourceId: 'jordan@inspectionxpert.com',
      versions: [],
    },
    hs_num_child_companies: {
      value: '0',
      timestamp: 1600806298661,
      source: 'CALCULATED',
      sourceId: 'RollupProperties',
      versions: [],
    },
    hs_num_contacts_with_buying_roles: {
      value: '0',
      timestamp: 1600806237376,
      source: 'SALES',
      sourceId: 'AssociationsPropertyWorker',
      versions: [],
    },
    hs_object_id: {
      value: '4512375702',
      timestamp: 1600806236464,
      source: 'CONTACTS',
      sourceId: 'CRM_UI',
      versions: [],
    },
    is_public: {
      value: 'false',
      timestamp: 1600806236464,
      source: 'BIDEN',
      sourceId: 'jordan@inspectionxpert.com',
      versions: [],
    },
    hs_analytics_source_data_2: {
      value: 'sample-contact',
      timestamp: 1600806238589,
      source: 'CALCULATED',
      sourceId: 'company sync triggered by vid=51',
      versions: [],
    },
    hs_analytics_source_data_1: {
      value: 'API',
      timestamp: 1600806238589,
      source: 'CALCULATED',
      sourceId: 'company sync triggered by vid=51',
      versions: [],
    },
    hs_updated_by_user_id: {
      value: '9283091',
      timestamp: 1600806236464,
      source: 'CONTACTS',
      sourceId: 'CRM_UI',
      versions: [],
    },
  },
  additionalDomains: [],
  stateChanges: [],
  mergeAudits: [],
};

export const CONTACT_SINGLE_RESPONSE = {
  vid: 51,
  'canonical-vid': 51,
  'merged-vids': [],
  'portal-id': 8445733,
  'is-contact': true,
  'profile-token': 'AO_T-mPt3NQo8bQTZNv2e-A-fQoLQZcOc7ZQpRY3k0JuyxmeyMo0Z89oUmjlj_PypfOPUIQeyv-k3EIGsUA3iX-C_qO23ieMtkK8-lCC9nOhFwYegjCxG5tqg2Qz5wCrhQ0DPq5BIg22',
  'profile-url': 'https://app.hubspot.com/contacts/8445733/contact/51',
  properties: {
    ixc_user_id: { value: '12345678-abcd-1234-123f-12345a678901', versions: [] },
    ixc_account_id: { value: 'IXC Account Id Here', versions: [] },
    ix_amplitude_link: { value: 'https://analytics.amplitude.com/ixc/project/123456789/search/user_id=12345678-abcd-1234-123f-12345a678901', versions: [] },
    ixc_role: { value: 'Owner,Admin', versions: [] },
    createdate: { value: '1599845113883', versions: [] },
    lastmodifieddate: { value: '1600806238336', versions: [] },
    email: { value: 'bh@hubspot.com', versions: [] },
    hs_email_domain: { value: 'hubspot.com', versions: [] },
    firstname: { value: 'Brian', versions: [] },
    lastname: { value: 'Halligan (Sample Contact)', versions: [] },
    jobtitle: { value: 'CEO', versions: [] },
    company: { value: 'HubSpot', versions: [] },
    associatedcompanyid: { value: '4512375702', versions: [] },
    city: { value: 'Cambridge', versions: [] },
    state: { value: 'MA', versions: [] },
    website: { value: 'http://www.HubSpot.com', versions: [] },
    twitterhandle: { value: 'bhalligan', versions: [] },
    twitterprofilephoto: { value: 'https://pbs.twimg.com/profile_images/3491742741/212e42c07d3348251da10872e85aa6b0.jpeg', versions: [] },
    lifecyclestage: { value: 'lead', versions: [] },
    hs_is_unworked: { value: 'true', versions: [] },
    num_unique_conversion_events: { value: '0', versions: [] },
    num_conversion_events: { value: '0', versions: [] },
    hs_is_contact: { value: 'true', versions: [] },
    hs_object_id: { value: '51', versions: [] },
    hs_lifecyclestage_lead_date: { value: '1599845113548', versions: [] },
    hs_marketable_reason_id: { value: 'Sample Contact', versions: [] },
    hs_marketable_reason_type: { value: 'SAMPLE_CONTACT', versions: [] },
    hs_marketable_status: { value: 'true', versions: [] },
    hs_marketable_until_renewal: { value: 'true', versions: [] },
    hs_social_facebook_clicks: { value: '0', versions: [] },
    hs_social_google_plus_clicks: { value: '0', versions: [] },
    hs_social_linkedin_clicks: { value: '0', versions: [] },
    hs_social_num_broadcast_clicks: { value: '0', versions: [] },
    hs_social_twitter_clicks: { value: '0', versions: [] },
    hs_analytics_average_page_views: { value: '0', versions: [] },
    hs_analytics_first_timestamp: { value: '1599845113548', versions: [] },
    hs_analytics_num_event_completions: { value: '0', versions: [] },
    hs_analytics_num_page_views: { value: '0', versions: [] },
    hs_analytics_num_visits: { value: '0', versions: [] },
    hs_analytics_revenue: { value: '0.0', versions: [] },
    hs_analytics_source_data_1: { value: 'API', versions: [] },
    hs_analytics_source_data_2: { value: 'sample-contact', versions: [] },
    hs_analytics_source: { value: 'OFFLINE', versions: [] },
    hs_predictivecontactscore_v2: { value: '2.79', versions: [] },
  },
  'associated-company': {
    'company-id': 4512375702,
    'portal-id': 8445733,
    properties: {
      country: {},
      num_associated_contacts: {},
      city: {},
      timezone: {},
      facebook_company_page: {},
      description: {},
      createdate: {},
      web_technologies: {},
      industry: {},
      hs_num_blockers: {},
      numberofemployees: {},
      hs_analytics_num_visits: {},
      linkedin_company_page: {},
      hubspot_owner_id: {},
      hs_analytics_source: {},
      hs_created_by_user_id: {},
      annualrevenue: {},
      founded_year: {},
      hs_analytics_num_page_views: {},
      state: {},
      hs_all_owner_ids: {},
      linkedinbio: {},
      zip: {},
      website: {},
      address: {},
      address2: {},
      hs_analytics_first_timestamp: {},
      hs_user_ids_of_all_owners: {},
      twitterhandle: {},
      first_contact_createdate: {},
      hs_target_account_probability: {},
      hs_lastmodifieddate: {},
      phone: {},
      hubspot_owner_assigneddate: {},
      hs_predictivecontactscore_v2: {},
      hs_num_decision_makers: {},
      domain: {},
      name: {},
      is_public: {},
      hs_object_id: {},
      hs_num_contacts_with_buying_roles: {},
      hs_num_child_companies: {},
      hs_analytics_source_data_2: {},
      hs_analytics_source_data_1: {},
      hs_updated_by_user_id: {},
    },
  },
  'form-submissions': [],
  'list-memberships': [],
  'identity-profiles': [{ vid: 51, 'saved-at-timestamp': 1599845113906, 'deleted-changed-timestamp': 0, identities: [] }],
  'merge-audits': [],
};

export const DEAL_SINGLE_RESPONSE = {
  portalId: 8445733,
  dealId: 3002860068,
  isDeleted: false,
  associations: {
    associatedVids: [132, 456],
    associatedCompanyIds: [4512375702, 444444444],
    associatedDealIds: [],
    associatedTicketIds: [],
  },
  properties: {
    ixc_account_id: {
      value: 'The IXC Account UUID',
      timestamp: 1600806236464,
      source: 'BIDEN',
      sourceId: 'jordan@inspectionxpert.com',
      versions: [],
    },
    ix_chargify_id: {
      value: 'The Charfify Customer UUID',
      timestamp: 1600806236464,
      source: 'BIDEN',
      sourceId: 'jordan@inspectionxpert.com',
      versions: [],
    },
    ix_chargify_link: {
      value: 'United States',
      timestamp: 1600806236464,
      source: 'BIDEN',
      sourceId: 'jordan@inspectionxpert.com',
      versions: [],
    },
    dealname: { value: 'Test Deal', timestamp: 1601132094514, source: 'CRM_UI', sourceId: 'jordan@inspectionxpert.com', versions: [] },
    dealstage: { value: 'appointmentscheduled', timestamp: 1601132094514, source: 'CRM_UI', sourceId: 'jordan@inspectionxpert.com', versions: [] },
    dealtype: { value: 'newbusiness', timestamp: 1601132094514, source: 'CRM_UI', sourceId: 'jordan@inspectionxpert.com', versions: [] },
    pipeline: { value: 'default', timestamp: 1601132094514, source: 'CRM_UI', sourceId: 'jordan@inspectionxpert.com', versions: [] },
    amount: { value: '1200', timestamp: 1601132133161, source: 'CRM_UI', sourceId: 'jordan@inspectionxpert.com', versions: [] },
    amount_in_home_currency: { value: '1200', timestamp: 1601132133161, source: 'CALCULATED', sourceId: null, versions: [] },
    hs_is_closed: { value: 'false', timestamp: 1601132094514, source: 'CALCULATED', sourceId: null, versions: [] },
    closedate: { value: '1601477560133', timestamp: 1601132094514, source: 'CRM_UI', sourceId: 'jordan@inspectionxpert.com', versions: [] },
    createdate: { value: '1601131960134', timestamp: 1601132094514, source: 'CRM_UI', sourceId: 'jordan@inspectionxpert.com', versions: [] },
    days_to_close: { value: '4', timestamp: 1601132094514, source: 'CALCULATED', sourceId: null, versions: [] },
    hs_acv: { value: '1200.000', timestamp: 1601132134537, source: 'CALCULATED', sourceId: 'RollupProperties', versions: [] },
    hs_all_owner_ids: { value: '50963116', timestamp: 1601132094514, source: 'CONTACTS', sourceId: 'CRM_UI', versions: [] },
    hs_analytics_source_data_1: { value: 'API', timestamp: 1601132094965, source: 'DEALS', sourceId: 'deal sync triggered by vid=51', versions: [] },
    hs_analytics_source_data_2: { value: 'sample-contact', timestamp: 1601132094965, source: 'DEALS', sourceId: 'deal sync triggered by vid=51', versions: [] },
    hs_analytics_source: { value: 'OFFLINE', timestamp: 1601132094965, source: 'DEALS', sourceId: 'deal sync triggered by vid=51', versions: [] },
    hs_arr: { value: '1200.000', timestamp: 1601132134537, source: 'CALCULATED', sourceId: 'RollupProperties', versions: [] },
    hs_closed_amount_in_home_currency: { value: '0', timestamp: 1601132094514, source: 'CALCULATED', sourceId: null, versions: [] },
    hs_closed_amount: { value: '0', timestamp: 1601132094514, source: 'CALCULATED', sourceId: null, versions: [] },
    hs_created_by_user_id: { value: '9283091', timestamp: 1601132094514, source: 'CONTACTS', sourceId: 'CRM_UI', versions: [] },
    hs_createdate: { value: '1601132094514', timestamp: 1601132094514, source: 'CONTACTS', sourceId: 'CRM_UI', versions: [] },
    hs_deal_stage_probability: { value: '0.200000000000000011102230246251565404236316680908203125', timestamp: 1601132094514, source: 'CALCULATED', sourceId: null, versions: [] },
    hs_lastmodifieddate: { value: '1601132134846', timestamp: 1601132134846, source: 'CALCULATED', sourceId: null, versions: [] },
    hs_mrr: { value: '100.000', timestamp: 1601132134537, source: 'CALCULATED', sourceId: 'RollupProperties', versions: [] },
    hs_object_id: { value: '3002860068', timestamp: 1601132094514, source: 'CONTACTS', sourceId: 'CRM_UI', versions: [] },
    hs_projected_amount_in_home_currency: { value: '240.000000000000013322676295501878485083580017089843750000', timestamp: 1601132133161, source: 'CALCULATED', sourceId: null, versions: [] },
    hs_projected_amount: { value: '240.000000000000013322676295501878485083580017089843750000', timestamp: 1601132133161, source: 'CALCULATED', sourceId: null, versions: [] },
    hs_tcv: { value: '1200.000', timestamp: 1601132134537, source: 'CALCULATED', sourceId: 'RollupProperties', versions: [] },
    hs_updated_by_user_id: { value: '9283091', timestamp: 1601132094514, source: 'CONTACTS', sourceId: 'CRM_UI', versions: [] },
    hs_user_ids_of_all_owners: { value: '9283091', timestamp: 1601132094514, source: 'CONTACTS', sourceId: 'CRM_UI', versions: [] },
    hubspot_owner_assigneddate: { value: '1601132094514', timestamp: 1601132094514, source: 'CRM_UI', sourceId: 'jordan@inspectionxpert.com', versions: [] },
    hubspot_owner_id: { value: '50963116', timestamp: 1601132094514, source: 'CRM_UI', sourceId: 'jordan@inspectionxpert.com', versions: [] },
    num_associated_contacts: { value: '1', timestamp: 1601132094919, source: 'CALCULATED', sourceId: 'RollupProperties', versions: [] },
  },
  imports: [],
  stateChanges: [],
};

export const TIMELINE_EVENT_SINGLE_RESPONSE = {
  name: 'Drawing Upload Updated',
  headerTemplate: 'Drawing uploaded Updated',
  detailTemplate: 'Drawing uploaded updated at {{#formatDate timestamp}}{{/formatDate}}',
  tokens: [
    {
      label: 'Drawing Name',
      objectPropertyName: null,
      options: [],
      name: 'drawingName',
      type: 'string',
      createdAt: '2021-06-03T03:13:25.789Z',
      updatedAt: '2021-06-03T03:23:14.766Z',
    },
    {
      label: 'Uploaded By',
      objectPropertyName: null,
      options: [],
      name: 'uploadedBy',
      type: 'string',
      createdAt: '2021-06-03T03:23:14.773Z',
      updatedAt: '2021-06-03T03:23:14.773Z',
    },
  ],
  id: '1056457',
  objectType: 'contacts',
  createdAt: '2021-06-03T03:13:25.780Z',
  updatedAt: '2021-06-03T03:23:14.754Z',
};

export const TIMELINE_EVENT_TYPES_LIST_RESPONSE = {
  results: [
    {
      name: 'Drawing Uploaded',
      headerTemplate: 'Drawing uploaded',
      detailTemplate: 'Drawing uploaded at {{#formatDate timestamp}}{{/formatDate}}',
      tokens: [
        {
          label: 'Drawing Name',
          objectPropertyName: null,
          options: [],
          name: 'drawingName',
          type: 'string',
          createdAt: '2021-06-24T21:59:49.744Z',
          updatedAt: '2021-06-24T21:59:49.744Z',
        },
      ],
      id: '1061693',
      objectType: 'contacts',
      createdAt: '2021-06-24T21:59:49.744Z',
      updatedAt: '2021-06-24T21:59:49.744Z',
    },
    {
      name: 'Report Generated',
      headerTemplate: 'Report generated',
      detailTemplate: 'Report generated at {{#formatDate timestamp}}{{/formatDate}}',
      tokens: [
        {
          label: 'Report Name',
          objectPropertyName: null,
          options: [],
          name: 'reportName',
          type: 'string',
          createdAt: '2021-06-24T21:59:49.744Z',
          updatedAt: '2021-06-24T21:59:49.744Z',
        },
      ],
      id: '1061695',
      objectType: 'contacts',
      createdAt: '2021-06-24T21:59:50.262Z',
      updatedAt: '2021-06-24T21:59:50.262Z',
    },
    {
      name: 'Drawing Uploaded',
      headerTemplate: 'Drawing uploaded',
      detailTemplate: 'Drawing uploaded at {{#formatDate timestamp}}{{/formatDate}}',
      tokens: [
        {
          label: 'Drawing Name',
          objectPropertyName: null,
          options: [],
          name: 'drawingName',
          type: 'string',
          createdAt: '2021-06-24T21:59:49.744Z',
          updatedAt: '2021-06-24T21:59:49.744Z',
        },
      ],
      id: '1061696',
      objectType: 'companies',
      createdAt: '2021-06-24T21:59:50.423Z',
      updatedAt: '2021-06-24T21:59:50.423Z',
    },
    {
      name: 'Report Generated',
      headerTemplate: 'Report generated',
      detailTemplate: 'Report generated at {{#formatDate timestamp}}{{/formatDate}}',
      tokens: [
        {
          label: 'Report Name',
          objectPropertyName: null,
          options: [],
          name: 'reportName',
          type: 'string',
          createdAt: '2021-06-24T21:59:49.744Z',
          updatedAt: '2021-06-24T21:59:49.744Z',
        },
      ],
      id: '1061698',
      objectType: 'companies',
      createdAt: '2021-06-24T21:59:50.837Z',
      updatedAt: '2021-06-24T21:59:50.837Z',
    },
    {
      name: 'Drawing Uploaded',
      headerTemplate: 'Drawing uploaded',
      detailTemplate: 'Drawing uploaded at {{#formatDate timestamp}}{{/formatDate}}',
      tokens: [
        {
          label: 'Drawing Name',
          objectPropertyName: null,
          options: [],
          name: 'drawingName',
          type: 'string',
          createdAt: '2021-06-24T21:59:49.744Z',
          updatedAt: '2021-06-24T21:59:49.744Z',
        },
      ],
      id: '1061699',
      objectType: 'deals',
      createdAt: '2021-06-24T21:59:51.029Z',
      updatedAt: '2021-06-24T21:59:51.029Z',
    },
    {
      name: 'Report Generated',
      headerTemplate: 'Report generated',
      detailTemplate: 'Report generated at {{#formatDate timestamp}}{{/formatDate}}',
      tokens: [
        {
          label: 'Report Name',
          objectPropertyName: null,
          options: [],
          name: 'reportName',
          type: 'string',
          createdAt: '2021-06-24T21:59:49.744Z',
          updatedAt: '2021-06-24T21:59:49.744Z',
        },
      ],
      id: '1061700',
      objectType: 'deals',
      createdAt: '2021-06-24T21:59:51.184Z',
      updatedAt: '2021-06-24T21:59:51.184Z',
    },
  ],
};

export const TIMELINE_EVENTS_IN_SOURCE = {
  contacts: {
    'Drawing Uploaded': {
      name: 'Drawing Uploaded',
      headerTemplate: 'Drawing uploaded',
      detailTemplate: 'Drawing uploaded at {{#formatDate timestamp}}{{/formatDate}}',
      tokens: [
        {
          name: 'drawingName',
          type: 'string',
          label: 'Drawing Name',
        },
      ],
      objectType: 'contacts',
    },
    'Report Generated': {
      name: 'Report Generated',
      headerTemplate: 'Report generated',
      detailTemplate: 'Report generated at {{#formatDate timestamp}}{{/formatDate}}',
      tokens: [
        {
          name: 'reportName',
          type: 'string',
          label: 'Report Name',
        },
      ],
      objectType: 'contacts',
    },
  },
  companies: {
    'Drawing Uploaded': {
      name: 'Drawing Uploaded',
      headerTemplate: 'Drawing uploaded',
      detailTemplate: 'Drawing uploaded at {{#formatDate timestamp}}{{/formatDate}}',
      tokens: [
        {
          name: 'drawingName',
          type: 'string',
          label: 'Drawing Name',
        },
      ],
      objectType: 'companies',
    },
    'Report Generated': {
      name: 'Report Generated',
      headerTemplate: 'Report generated',
      detailTemplate: 'Report generated at {{#formatDate timestamp}}{{/formatDate}}',
      tokens: [
        {
          name: 'reportName',
          type: 'string',
          label: 'Report Name',
        },
      ],
      objectType: 'companies',
    },
  },
  deals: {
    'Drawing Uploaded': {
      name: 'Drawing Uploaded',
      headerTemplate: 'Drawing uploaded',
      detailTemplate: 'Drawing uploaded at {{#formatDate timestamp}}{{/formatDate}}',
      tokens: [
        {
          name: 'drawingName',
          type: 'string',
          label: 'Drawing Name',
        },
      ],
      objectType: 'deals',
    },
    'Report Generated': {
      name: 'Report Generated',
      headerTemplate: 'Report generated',
      detailTemplate: 'Report generated at {{#formatDate timestamp}}{{/formatDate}}',
      tokens: [
        {
          name: 'reportName',
          type: 'string',
          label: 'Report Name',
        },
      ],
      objectType: 'deals',
    },
  },
  tickets: {},
};

export const getHubspotMock = (params = {}) => {
  return {
    accessToken: jest.fn(),
    accessTokenUpdatedAt: jest.fn(),
    accessTokenExpiresIn: jest.fn(),
    refreshAccessToken: jest.fn(),
    apiRequest: jest.fn(),
    updateTimelineEventType: jest.fn(),
    createTimelineEventType: jest.fn(),
    pipelines: {
      get: jest.fn(),
    },
    companies: {
      get: jest.fn(),
      getById: jest.fn(),
      getByDomain: jest.fn(),
      create: jest.fn(),
      addContactToCompany: jest.fn(),
      update: jest.fn(),
      delete: jest.fn(),
    },
    contacts: {
      search: jest.fn(),
      get: jest.fn(),
      getAll: jest.fn(),
      getByEmail: jest.fn(),
      getById: jest.fn(),
      update: jest.fn(),
      create: jest.fn(),
      createOrUpdate: jest.fn(),
      createOrUpdateBatch: jest.fn(),
      updateByEmail: jest.fn(),
      delete: jest.fn(),
      merge: jest.fn(),
    },
    deals: {
      create: jest.fn(),
      get: jest.fn(),
      getById: jest.fn(),
      getAssociated: jest.fn(),
      deleteById: jest.fn(),
      updateById: jest.fn(),
      updateBatch: jest.fn(),
      associate: jest.fn(),
      removeAssociation: jest.fn(),
    },
    crm: {
      associations: {
        create: jest.fn(),
        // createBatch: jest.fn(),
        delete: jest.fn(),
        // deleteBatch: jest.fn(),
      },
    },
  };
};

// Helper function that can fabricate hubspot crm search responses
export const getSearchCrmResponse = (params = {}) => {
  const { crmObjectType } = params;

  const record = {
    id: params.id || '6581530659',
    createdAt: params.createdAt || '2021-07-15T19:38:13.365Z',
    updatedAt: params.updatedAt || '2021-08-02T23:05:41.693Z',
    archived: params.archived || false,
  };

  let refObject;
  if (crmObjectType === 'companies') {
    refObject = { ...COMPANY_SINGLE_RESPONSE };
  } else if (crmObjectType === 'contacts') {
    refObject = { ...CONTACT_SINGLE_RESPONSE };
  } else if (crmObjectType === 'deals') {
    refObject = { ...DEAL_SINGLE_RESPONSE };
  } // else {  'products', 'tickets', 'line_items', 'quotes', 'custom objects' }

  // Determine if all or only some properties should be returned
  const keepProps = params.returnProps || [];
  const keepAllProps = keepProps.length === 0;

  // Add properties
  record.properties = Object.entries(refObject.properties).reduce((obj, [prop, propData]) => {
    if (keepAllProps || keepProps.includes(prop)) {
      if (params.properties && Object.prototype.hasOwnProperty.call(params.properties, prop)) {
        obj[prop] = params.properties[prop];
      } else {
        obj[prop] = propData.value;
      }
    }
    return obj;
  }, {});

  // Return one or many
  return {
    total: 1,
    results: [record],
  };
};

// Helper function that can fabricate hubspot Company lookup responses
export const getCompanyResponse = (params = {}) => {
  let company = { ...COMPANY_SINGLE_RESPONSE };

  // Override top level attributes if provided
  company = Object.entries(company).reduce((obj, [prop, propData]) => {
    if (prop !== 'properties' && Object.prototype.hasOwnProperty.call(params, prop)) {
      obj[prop] = params[prop];
    }
    return obj;
  }, company);

  // Determine if all or only some properties should be returned
  const keepProps = params.returnProps || [];
  const keepAllProps = keepProps.length === 0;

  // Add properties
  company.properties = Object.entries(company.properties).reduce((obj, [prop, propData]) => {
    if (keepAllProps || keepProps.includes(prop)) {
      if (params.properties && Object.prototype.hasOwnProperty.call(params.properties, prop)) {
        obj[prop] = {
          value: params.properties[prop],
          timestamp: 1600806236464,
          source: 'SOME_SOURCE',
          sourceId: 'john@doe.com',
          versions: [],
        };
      } else {
        obj[prop] = propData;
      }
    }
    return obj;
  }, {});

  // Return one or many
  if (params.returnMany) {
    return {
      results: [company],
      hasMore: false,
      offset: { companyId: company.companyId, isPrimary: true },
    };
  }
  return company;
};

// Helper function that can fabricate hubspot Contact lookup responses
export const getContactResponse = (params = {}) => {
  let contact = { ...CONTACT_SINGLE_RESPONSE };

  // Override top level attributes if provided
  contact = Object.entries(contact).reduce((obj, [prop, propData]) => {
    if (prop !== 'properties' && Object.prototype.hasOwnProperty.call(params, prop)) {
      // console.log(`${prop} : ${obj[prop]} => ${params[prop]}`);
      // NOTE: id should up in a few places
      // id, canonical - vid, 'identity-profiles': [{
      obj[prop] = params[prop];
    }
    return obj;
  }, contact);

  // Determine if all or only some properties should be returned
  const keepProps = params.returnProps || [];
  const keepAllProps = keepProps.length === 0;

  // Add properties
  contact.properties = Object.entries(contact.properties).reduce((obj, [prop, propData]) => {
    if (keepAllProps || keepProps.includes(prop)) {
      if (params.properties && Object.prototype.hasOwnProperty.call(params.properties, prop)) {
        obj[prop] = {
          value: params.properties[prop],
          timestamp: 1600806236464,
          source: 'SOME_SOURCE',
          sourceId: 'john@doe.com',
          versions: [],
        };
      } else {
        obj[prop] = propData;
      }
    }
    return obj;
  }, {});

  // Return one or many
  if (params.returnMany) {
    return {
      results: [contact],
      hasMore: false,
      offset: { vid: contact.vid, isPrimary: true },
    };
  }
  return contact;
};

// Helper function that can fabricate hubspot deal lookup responses
export const getDealResponse = (params = {}) => {
  let deal = { ...DEAL_SINGLE_RESPONSE };

  // Override top level attributes if provided
  deal = Object.entries(deal).reduce((obj, [prop, propData]) => {
    if (prop !== 'properties' && Object.prototype.hasOwnProperty.call(params, prop)) {
      obj[prop] = params[prop];
    }
    return obj;
  }, deal);

  // Determine if all or only some properties should be returned
  const keepProps = params.returnProps || [];
  const keepAllProps = keepProps.length === 0;

  // Add properties
  deal.properties = Object.entries(deal.properties).reduce((obj, [prop, propData]) => {
    if (keepAllProps || keepProps.includes(prop)) {
      if (params.properties && Object.prototype.hasOwnProperty.call(params.properties, prop)) {
        obj[prop] = {
          value: params.properties[prop],
          timestamp: 1600806236464,
          source: 'SOME_SOURCE',
          sourceId: 'john@doe.com',
          versions: [],
        };
      } else {
        obj[prop] = propData;
      }
    }
    return obj;
  }, {});

  // Return one or many
  if (params.returnMany) {
    return {
      results: [deal],
      hasMore: false,
      offset: { id: deal.dealId, isPrimary: true },
    };
  }
  return deal;
};
