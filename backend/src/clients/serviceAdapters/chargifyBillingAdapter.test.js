const dotenv = require('dotenv');

dotenv.config({ path: '../.env' });

const ChargifyBillingAdapter = require('./chargifyBillingAdapter');

const USE_MOCKS = true;

// prettier-ignore
const {
  INVOICES_RESULTS_PER_PAGE_MAX_SUPPORTED,
  INVOICES_REQUESTS_COUNT_MAX,
  ACCOUNT_ID, //
  API_KEY,
  PUBLIC_KEY,
  PRIVATE_KEY,
  SITE_NAME,
  ERROR_MESSAGE,
  USER_EMAIL,
  MOCK_SUBSCRIPTION,
  GET_SUBSCRIPTIONS_SINGLE_RESPONSE,
  GET_SUBSCRIPTIONS_MULTIPLE_RESPONSE,
  GET_INVOICE_RESPONSE,
  getHttpClientMock,
  createGetInvoicesResponse,
  getCustomerResponse,
  MOCK_PRODUCT_FAMILY_SET,
  getProductFamilyResponse,
  MOCK_PRODUCT_SET,
  MOCK_PRODUCT,
  getProductsResponse,
  MOCK_PRODUCT_PRICES,
  getProductPricesResponse,
  MOCK_PRODUCT_COMPONENT_SET,
  getProductComponentsResponse,
  MOCK_COMPONENT_PRICES,
  getComponentResponse,
  MOCK_PRODUCT_CATALOG,
  MOCK_PAYMENT_PROFILE,
} = require('./fixtures/chargify.fixtures');

const fakeInvoice = {
  uid: 123,
  subtotal_amount: '3750.0',
  discount_amount: '0.0',
  tax_amount: '271.88',
  total_amount: '4021.88',
  credit_amount: '0.0',
  paid_amount: '0.0',
  refund_amount: '0.0',
  due_amount: '4021.88',
};

describe('ChargifyBillingAdapter', () => {
  let billingClient;
  const httpClientMock = getHttpClientMock();
  const loggerMock = {
    trace: jest.fn(),
    debug: jest.fn(),
    log: jest.fn(),
    info: jest.fn(),
    warn: jest.fn(),
    error: jest.fn(),
  };

  beforeAll(async () => {
    if (USE_MOCKS) {
      // console.log('Using mock objects during test');
      billingClient = new ChargifyBillingAdapter({
        apiKey: API_KEY,
        publicKey: PUBLIC_KEY,
        privateKey: PRIVATE_KEY,
        siteName: SITE_NAME,
        httpClient: httpClientMock,
        logger: loggerMock,
      });
    } else {
      // console.log('Using REAL APIs during test');
      billingClient = new ChargifyBillingAdapter({
        apiKey: process.env.IXC_CHARGIFY_API_KEY,
        publicKey: process.env.IXC_CHARGIFY_PUBLIC_KEY,
        privateKey: process.env.IXC_CHARGIFY_PRIVATE_KEY,
        siteName: 'inspectionxpert-test',
        logger: loggerMock,
      });
    }
  });

  beforeEach(() => {
    jest.resetAllMocks();
  });

  describe('Instantiation', () => {
    it('should populate the `siteUrlBase` property', () => {
      expect(billingClient.siteUrlBase.includes(SITE_NAME)).toBe(true);
      expect(loggerMock.error).toHaveBeenCalledTimes(0);
    });

    it('should populate the `basicAuth` property', () => {
      const expected = { username: API_KEY, password: 'x' };
      expect(billingClient.basicAuth).toEqual(expected);
      expect(loggerMock.error).toHaveBeenCalledTimes(0);
    });
  });

  describe('#getMetadata', () => {
    it('should return name and mock status of adapter', () => {
      const md = billingClient.getMetadata();
      expect(md).toBeDefined();
      expect(md.name).toBe('chargify');
      expect(md.siteName).toBe('a-test-site-name');
      expect(md.isMocked).toBeDefined();
      expect(loggerMock.error).toHaveBeenCalledTimes(0);
    });
  });

  describe('#getVendorUrlForCustomerWithId', () => {
    it('should return URL of customer record', () => {
      const result = billingClient.getVendorUrlForCustomerWithId(1234);
      expect(result).toBe('https://a-test-site-name.chargify.com/customers/1234');
      expect(loggerMock.error).toHaveBeenCalledTimes(0);
    });
  });

  describe('#getVendorUrlForSubscriptionWithId', () => {
    it('should return URL of subscription record', () => {
      const result = billingClient.getVendorUrlForSubscriptionWithId(1234);
      expect(result).toBe('https://a-test-site-name.chargify.com/subscriptions/1234');
      expect(loggerMock.error).toHaveBeenCalledTimes(0);
    });
  });

  describe('#getInitData()', () => {
    it('should throw an error if required input is not provided', () => {
      expect(() => {
        billingClient.getInitData();
      }).toThrow();

      expect(loggerMock.error).toHaveBeenCalledTimes(1);
    });

    it('should populate the result properly', () => {
      const result = billingClient.getInitData(ACCOUNT_ID);
      expect(Object.keys(result).length).toBe(3);
      expect(result.publicKey).toBe(PUBLIC_KEY);
      expect(result.siteUrl).toBe(billingClient.siteUrlBase);
      expect(result.securityToken.split('.').length).toBe(3);
      expect(loggerMock.error).toHaveBeenCalledTimes(0);
    });
  });

  describe('#logError()', () => {
    it('should log errors formatted in the axios manner', () => {
      const error = new Error();
      const message = ERROR_MESSAGE;
      const dataErrorA = 'this is wrong';
      const dataErrorB = 'this is also  wrong';
      error.response = {};
      error.response.data = {
        errors: [dataErrorA, dataErrorB],
      };

      billingClient.logError(error, message);

      expect(loggerMock.error).toHaveBeenCalledTimes(1);
      expect(loggerMock.error.mock.calls[0][0]).toBe(`${message} ${dataErrorA},${dataErrorB}`);
    });

    it('should log errors with a standard format as they are', () => {
      const error = new Error('fail');
      billingClient.logError(error, ERROR_MESSAGE);

      expect(loggerMock.error).toHaveBeenCalledTimes(1);
      expect(loggerMock.error.mock.calls[0][0]).toBe(`${ERROR_MESSAGE} Error: ${error.message}`);
    });
  });

  describe('Catalog', () => {
    describe('#getCatalog()', () => {
      it('should throw an error if the request fails', async () => {
        if (USE_MOCKS) {
          httpClientMock.get.mockRejectedValue(new Error(ERROR_MESSAGE));
          await expect(billingClient.getCatalog()).rejects.toThrow();
          expect(loggerMock.error).toHaveBeenCalledTimes(2);
        }
      });

      it('should return a catalog with just the products in the product family requested when the request succeeds', async () => {
        const productFamilyId = 1526431;
        httpClientMock.get.mockResolvedValueOnce(getProductsResponse({ productFamilyId }));
        httpClientMock.get.mockResolvedValueOnce({
          data: {
            price_points: [],
          },
        }); // Product prices
        httpClientMock.get.mockResolvedValueOnce(getProductComponentsResponse({ productFamilyId }));
        // component prices
        httpClientMock.get.mockResolvedValueOnce({
          data: {
            price_points: [
              {
                handle: '123_monthly',
                prices: [
                  {
                    id: 2030660,
                    component_id: 1108426,
                    starting_quantity: 1,
                    ending_quantity: 5,
                    unit_price: '0.0',
                    price_point_id: 1067356,
                    formatted_unit_price: '$0.00',
                    segment_id: null,
                  },
                  {
                    id: 2030661,
                    component_id: 1108426,
                    starting_quantity: 6,
                    ending_quantity: 100,
                    unit_price: '125.0',
                    price_point_id: 1067356,
                    formatted_unit_price: '$125.00',
                    segment_id: null,
                  },
                  {
                    id: 2030662,
                    component_id: 1108426,
                    starting_quantity: 101,
                    ending_quantity: 250,
                    unit_price: '225.0',
                    price_point_id: 1067356,
                    formatted_unit_price: '$225.00',
                    segment_id: null,
                  },
                  {
                    id: 2030663,
                    component_id: 1108426,
                    starting_quantity: 251,
                    ending_quantity: 500,
                    unit_price: '325.0',
                    price_point_id: 1067356,
                    formatted_unit_price: '$325.00',
                    segment_id: null,
                  },
                  {
                    id: 2030664,
                    component_id: 1108426,
                    starting_quantity: 501,
                    ending_quantity: 1000,
                    unit_price: '425.0',
                    price_point_id: 1067356,
                    formatted_unit_price: '$425.00',
                    segment_id: null,
                  },
                  {
                    id: 2030665,
                    component_id: 1108426,
                    starting_quantity: 1001,
                    ending_quantity: null,
                    unit_price: '525.0',
                    price_point_id: 1067356,
                    formatted_unit_price: '$525.00',
                    segment_id: null,
                  },
                ],
              },
            ],
          },
        });
        httpClientMock.get.mockResolvedValueOnce({
          data: {
            price_points: [
              {
                handle: '123_monthly',
                prices: [
                  {
                    id: 1886525,
                    component_id: 1028184,
                    starting_quantity: 1,
                    ending_quantity: null,
                    unit_price: '125.0',
                    price_point_id: 961586,
                    formatted_unit_price: '$125.00',
                    segment_id: null,
                  },
                ],
              },
            ],
          },
        });
        httpClientMock.get.mockResolvedValueOnce({
          data: {
            price_points: [
              {
                handle: '123_monthly',
                prices: [
                  {
                    id: 2221756,
                    component_id: 1213884,
                    starting_quantity: 1,
                    ending_quantity: 1,
                    unit_price: '125.0',
                    price_point_id: 1202165,
                    formatted_unit_price: '$125.00',
                    segment_id: null,
                  },
                  {
                    id: 2221757,
                    component_id: 1213884,
                    starting_quantity: 2,
                    ending_quantity: 3,
                    unit_price: '250.0',
                    price_point_id: 1202165,
                    formatted_unit_price: '$250.00',
                    segment_id: null,
                  },
                  {
                    id: 2221758,
                    component_id: 1213884,
                    starting_quantity: 4,
                    ending_quantity: 5,
                    unit_price: '375.0',
                    price_point_id: 1202165,
                    formatted_unit_price: '$375.00',
                    segment_id: null,
                  },
                ],
              },
            ],
          },
        });

        const result = await billingClient.getCatalog({ productFamilyId });
        expect(result).toEqual(MOCK_PRODUCT_CATALOG);
        expect(loggerMock.error).toHaveBeenCalledTimes(0);
      });
    });

    describe('#getProductFamilies()', () => {
      it('should throw an error if the request fails', async () => {
        if (USE_MOCKS) {
          httpClientMock.get.mockRejectedValue(new Error(ERROR_MESSAGE));
          await expect(billingClient.getProductFamilies()).rejects.toThrow();
          expect(loggerMock.error).toHaveBeenCalledTimes(1);
        }
      });

      it('should return the list of product families when the request succeeds', async () => {
        httpClientMock.get.mockResolvedValue(getProductFamilyResponse());
        const result = await billingClient.getProductFamilies();
        expect(result).toEqual(MOCK_PRODUCT_FAMILY_SET);
        expect(loggerMock.error).toHaveBeenCalledTimes(0);
      });
    });

    describe('#getProducts()', () => {
      it('should throw an error if the request fails', async () => {
        if (USE_MOCKS) {
          httpClientMock.get.mockRejectedValue(new Error(ERROR_MESSAGE));
          await expect(billingClient.getProducts()).rejects.toThrow();
          expect(loggerMock.error).toHaveBeenCalledTimes(1);
        }
      });

      it('should return all products in all families when productFamilyId is not specified and the request succeeds', async () => {
        httpClientMock.get.mockResolvedValue(getProductsResponse());
        const result = await billingClient.getProducts();
        expect(MOCK_PRODUCT_SET.length).toEqual(8);
        expect(result).toEqual(MOCK_PRODUCT_SET);
        expect(result.length).toEqual(8);
        expect(loggerMock.error).toHaveBeenCalledTimes(0);
      });

      it('should return only products in the family with productFamilyId when it is specified and the request succeeds', async () => {
        httpClientMock.get.mockResolvedValue(getProductsResponse({ productFamilyId: 1526432 }));
        expect(MOCK_PRODUCT_SET.length).toEqual(8);
        const result = await billingClient.getProducts({ productFamilyId: 1526432 });
        expect(result.length).toEqual(6);
        expect(loggerMock.error).toHaveBeenCalledTimes(0);
      });
    });

    describe('#getProductPrices()', () => {
      it('should throw an error if required input is not provided', async () => {
        await expect(billingClient.getProductPrices()).rejects.toThrow();
        expect(loggerMock.error).toHaveBeenCalledTimes(1);
      });

      it('should throw an error if the request fails', async () => {
        if (USE_MOCKS) {
          httpClientMock.get.mockRejectedValue(new Error(ERROR_MESSAGE));
          await expect(billingClient.getProductPrices()).rejects.toThrow();
          expect(loggerMock.error).toHaveBeenCalledTimes(1);
        }
      });

      it('should return the price data for the specified product when the request succeeds', async () => {
        httpClientMock.get.mockResolvedValue(getProductPricesResponse());
        const result = await billingClient.getProductPrices({ productId: MOCK_PRODUCT.id });
        expect(result).toEqual(MOCK_PRODUCT_PRICES);
        expect(loggerMock.error).toHaveBeenCalledTimes(0);
      });
    });

    describe('#getProductComponents()', () => {
      it('should throw an error if required input is not provided', async () => {
        await expect(billingClient.getProductComponents()).rejects.toThrow();
        expect(loggerMock.error).toHaveBeenCalledTimes(1);
      });

      it('should throw an error if the request fails', async () => {
        if (USE_MOCKS) {
          httpClientMock.get.mockRejectedValue(new Error(ERROR_MESSAGE));
          await expect(billingClient.getProductComponents()).rejects.toThrow();
          expect(loggerMock.error).toHaveBeenCalledTimes(1);
        }
      });

      it('should return available product components for the product family with productFamilyId when the request succeeds', async () => {
        const productFamilyId = 1526431;
        httpClientMock.get.mockResolvedValue(getProductComponentsResponse({ productFamilyId }));
        // expect(MOCK_PRODUCT_SET.length).toEqual(8);
        const result = await billingClient.getProductComponents({ productFamilyId });
        expect(result).toEqual(MOCK_PRODUCT_COMPONENT_SET);
        expect(loggerMock.error).toHaveBeenCalledTimes(0);
      });
    });

    describe('#getComponent()', () => {
      it('should throw an error if required input is not provided', async () => {
        await expect(billingClient.getComponent()).rejects.toThrow();
        expect(loggerMock.error).toHaveBeenCalledTimes(1);
      });

      it('should throw an error if the request fails', async () => {
        if (USE_MOCKS) {
          httpClientMock.get.mockRejectedValue(new Error(ERROR_MESSAGE));
          await expect(billingClient.getComponent({ handle: 1108426 })).rejects.toThrow();
          expect(loggerMock.error).toHaveBeenCalledTimes(1);
        }
      });

      it('should return product component with the specified componentId when the request succeeds', async () => {
        httpClientMock.get.mockResolvedValue(getComponentResponse({ handle: 'paid_user_seat_ss' }));
        // expect(MOCK_PRODUCT_SET.length).toEqual(8);
        const result = await billingClient.getComponent({ handle: 'paid_user_seat_ss' });
        expect(result).toEqual(MOCK_PRODUCT_COMPONENT_SET[2].component);
        expect(loggerMock.error).toHaveBeenCalledTimes(0);
      });
    });

    describe('getCurrentPrice', () => {
      it('warns if no price point is found', async () => {
        const price = await billingClient.getCurrentPrice('FAKE', MOCK_COMPONENT_PRICES, 1);
        expect(loggerMock.warn).toHaveBeenCalledTimes(1);
        expect(price).toBe(null);
      });

      it('warns if the quantity does no exist on the available price point', async () => {
        const price = await billingClient.getCurrentPrice('Annually', MOCK_COMPONENT_PRICES, 0);
        expect(loggerMock.warn).toHaveBeenCalledTimes(1);
        expect(price).toBe(null);
      });
      it('returns current annual price for product', async () => {
        const price = await billingClient.getCurrentPrice('Annually', MOCK_PRODUCT_PRICES);
        expect(loggerMock.warn).toHaveBeenCalledTimes(0);
        expect(price).not.toBe(null);
      });
      it('returns current annual price for component', async () => {
        const price = await billingClient.getCurrentPrice('Annually', MOCK_COMPONENT_PRICES);
        expect(loggerMock.warn).toHaveBeenCalledTimes(0);
        expect(price).not.toBe(null);
      });
      it('returns current annual price for component at specific quantity', async () => {
        const price = await billingClient.getCurrentPrice('Annually', MOCK_COMPONENT_PRICES, 1);
        expect(loggerMock.warn).toHaveBeenCalledTimes(0);
        expect(price).not.toBe(null);
      });
    });
  });

  describe('Customers', () => {
    describe('#createOrUpdateCustomer()', () => {
      let params;

      beforeEach(() => {
        params = {
          email: 'some-email',
          firstName: 'some-first-name',
          lastName: 'some-last-name',
          reference: 'some-reference',
        };
      });

      it('should throw an error if required input is not provided', async () => {
        await expect(billingClient.createOrUpdateCustomer({})).rejects.toThrow();
        expect(loggerMock.error).toHaveBeenCalledTimes(2);
      });

      it('should throw an error if the getCustomer request fails', async () => {
        httpClientMock.get.mockRejectedValue(new Error(ERROR_MESSAGE)); // getCustomer
        // httpClientMock.post.mockRejectedValue(new Error(ERROR_MESSAGE)); // createCustomer
        // httpClientMock.put.mockRejectedValue(new Error(ERROR_MESSAGE)); // updateCustomer
        await expect(billingClient.createOrUpdateCustomer(params)).rejects.toThrow();
        // getCustomer fires error once inside itself, and throws to createOrUpdateCustomer where it gets logged a second time
        expect(loggerMock.error).toHaveBeenCalledTimes(2);
      });

      it('should create and return a customer record when customer does not already exist', async () => {
        httpClientMock.get.mockResolvedValue({ data: [] }); // getCustomer
        httpClientMock.post.mockResolvedValue(
          getCustomerResponse({
            id: 'an-id',
            ...params,
            returnSingleRecord: true,
          })
        );
        const result = await billingClient.createOrUpdateCustomer(params);
        expect(result).toBeDefined();
        expect(result.id).toBe('an-id');
        expect(result.email).toBe(params.email);
        expect(loggerMock.error).toHaveBeenCalledTimes(0);
      });

      it('should find and update customer record when customer already exists', async () => {
        httpClientMock.get.mockResolvedValue({
          data: [
            {
              id: 'an-id',
              ...params,
              email: 'original-value',
              returnSingleRecord: true,
            },
          ],
        }); // getCustomer
        httpClientMock.put.mockResolvedValue(
          getCustomerResponse({
            id: 'an-id',
            ...params,
            returnSingleRecord: true,
          })
        );
        const result = await billingClient.createOrUpdateCustomer(params);
        expect(result).toBeDefined();
        expect(result.id).toBe('an-id');
        expect(result.email).toBe(params.email);
        expect(loggerMock.error).toHaveBeenCalledTimes(0);
      });
    });

    describe('#createCustomer()', () => {
      let params;

      beforeEach(() => {
        params = {
          reference: 'some-reference',
          email: 'some-email',
          firstName: 'some-first-name',
          lastName: 'some-last-name',
        };
      });

      it('should throw an error if required input is not provided', async () => {
        await expect(billingClient.createCustomer({})).rejects.toThrow();
        expect(loggerMock.error).toHaveBeenCalledTimes(1);
      });

      it('should throw an error if the request fails', async () => {
        httpClientMock.post.mockRejectedValue(new Error(ERROR_MESSAGE));
        await expect(billingClient.createCustomer(params)).rejects.toThrow();
        expect(loggerMock.error).toHaveBeenCalledTimes(1);
      });

      it('should create and return a customer record when the request succeeds', async () => {
        httpClientMock.post.mockResolvedValue(getCustomerResponse({ id: 'an-id', email: params.email, returnSingleRecord: true }));
        const result = await billingClient.createCustomer(params);
        expect(result).toBeDefined();
        expect(result.email).toBe(params.email);
        expect(loggerMock.error).toHaveBeenCalledTimes(0);
      });

      it('should have custom properties', async () => {
        httpClientMock.post.mockResolvedValue(
          getCustomerResponse({
            id: 'an-id',
            hubSpot_company_id: 'HubSpot Company ID Here',
            ixc_account_id: 'IXC Account UUID Here',
            returnSingleRecord: true,
          })
        );
        const result = await billingClient.createCustomer(params);
        expect(result).toBeDefined();
        expect(result.hubSpot_company_id).toBeDefined();
        expect(result.ixc_account_id).toBeDefined();
        expect(loggerMock.error).toHaveBeenCalledTimes(0);
      });
    });

    describe('#getCustomerbyId()', () => {
      it('should throw an error if the request fails', async () => {
        httpClientMock.get.mockRejectedValue(new Error(ERROR_MESSAGE));
        await expect(billingClient.getCustomerById()).rejects.toThrow();
        expect(loggerMock.error).toHaveBeenCalledTimes(1);
      });

      it('should return the customer record when the request succeeds', async () => {
        const customer = { id: 'an-id' };
        httpClientMock.get.mockResolvedValue(
          getCustomerResponse({
            id: customer.id,
            returnSingleRecord: true,
          })
        );
        const result = await billingClient.getCustomerById(customer.id);
        expect(result).toEqual(customer);
        expect(loggerMock.error).toHaveBeenCalledTimes(0);
      });
    });

    describe('#getCustomerByReference()', () => {
      it('should throw an error if the request fails', async () => {
        httpClientMock.get.mockRejectedValue(new Error(ERROR_MESSAGE));
        await expect(billingClient.getCustomerByReference()).rejects.toThrow();
        expect(loggerMock.error).toHaveBeenCalledTimes(1);
      });

      it('should return the customer record when the request succeeds', async () => {
        const customer = { id: 'an-id', reference: 'some-reference' };
        httpClientMock.get.mockResolvedValue(
          getCustomerResponse({
            id: customer.id,
            reference: customer.reference,
            returnSingleRecord: true,
          })
        );
        const result = await billingClient.getCustomerByReference(customer.reference);
        expect(result.id).toEqual(customer.id);
        expect(result.reference).toEqual(customer.reference);
        expect(loggerMock.error).toHaveBeenCalledTimes(0);
      });
    });

    describe('#getMetadataForCustomerWithId()', () => {
      it('should throw an error if the request fails', async () => {
        httpClientMock.get.mockRejectedValue(new Error(ERROR_MESSAGE));
        await expect(billingClient.getMetadataForCustomerWithId()).rejects.toThrow();
        expect(loggerMock.error).toHaveBeenCalledTimes(1);
      });

      it('should return the metadata for the customer record when the request succeeds', async () => {
        const customer = { id: 42644027 };
        httpClientMock.get.mockResolvedValue({
          data: {
            total_count: 2,
            current_page: 1,
            total_pages: 1,
            per_page: 20,
            metadata: [
              {
                id: 16427577,
                value: '0e09f42a-98d0-45fd-883c-43a37a861fe8',
                resource_id: 42644027,
                deleted_at: null,
                name: 'ixc_account_id',
              },
              {
                id: 16427578,
                value: '5923613467',
                resource_id: 42644027,
                deleted_at: null,
                name: 'hubspot_company_id',
              },
            ],
          },
        });
        const result = await billingClient.getMetadataForCustomerWithId(customer.id);
        // console.log('getMetadataForCustomerWithId.response', result);
        expect(result.ixc_account_id).toEqual('0e09f42a-98d0-45fd-883c-43a37a861fe8');
        expect(result.hubspot_company_id).toEqual('5923613467');
        expect(loggerMock.error).toHaveBeenCalledTimes(0);
      });
    });

    describe('#findCustomer()', () => {
      it('should throw an error if the request fails', async () => {
        httpClientMock.get.mockRejectedValue(new Error(ERROR_MESSAGE));
        await expect(billingClient.findCustomer()).rejects.toThrow();
        expect(loggerMock.error).toHaveBeenCalledTimes(1);
      });

      it('should return an array of customer records when the request succeeds', async () => {
        const customer = {
          id: 'an-id',
          email: 'foo@bar.com',
          reference: 'HubSpot Company ID Here',
        };
        httpClientMock.get.mockResolvedValue({ data: [customer] }); // getCustomer
        const result = await billingClient.findCustomer(customer.id);
        expect(result.length).toEqual(1); // Result should be an array
        expect(result[0].id).toEqual(customer.id);
        expect(result[0].email).toEqual(customer.email);
        expect(loggerMock.error).toHaveBeenCalledTimes(0);
      });
    });

    describe('#updateCustomer()', () => {
      it('should throw an error if required input is not provided', async () => {
        await expect(billingClient.updateCustomer({})).rejects.toThrow();
        expect(loggerMock.error).toHaveBeenCalledTimes(1);
      });

      it('should throw an error if the request fails', async () => {
        httpClientMock.put.mockRejectedValue(new Error(ERROR_MESSAGE));
        await expect(billingClient.updateCustomer({ id: 'an-id' })).rejects.toThrow();
        expect(loggerMock.error).toHaveBeenCalledTimes(1);
      });

      it('should return the customer record that reflects the update when the request succeeds', async () => {
        const customer = { id: 'an-id', email: 'a-new-email-address@customer.org' };
        httpClientMock.put.mockResolvedValue(
          getCustomerResponse({
            id: customer.id,
            email: customer.email,
            returnSingleRecord: true,
          })
        );
        const result = await billingClient.updateCustomer({
          id: customer.id,
          email: customer.email,
        });

        expect(result).toEqual(customer);
        expect(loggerMock.error).toHaveBeenCalledTimes(0);

        const apiUrl = httpClientMock.put.mock.calls[0][0];
        expect(apiUrl.includes(customer.id));
      });
    });
  });

  describe('Subscriptions', () => {
    describe('#createSubscription()', () => {
      it('should throw an error if required input is not provided', async () => {
        await expect(billingClient.createSubscription({})).rejects.toThrow();
        expect(loggerMock.error).toHaveBeenCalledTimes(1);
      });

      // The customer lookup was removed, so this test is no longer valid
      // it('should throw an error if the get customer record request fails', async () => {
      //   httpClientMock.get.mockRejectedValue(new Error(ERROR_MESSAGE));
      //   await expect(billingClient.createSubscription({ billingToken: 'a-toekn' })).rejects.toThrow();

      //   expect(loggerMock.error).toHaveBeenCalledTimes(2); // One call for getCustomer() and one for createSubscription()
      // });

      it('should throw an error if an invalid parameter is passed', async () => {
        httpClientMock.post.mockResolvedValue({ data: { subscription: { id: 'an-id' } } });
        await expect(
          billingClient.createSubscription({
            catalogKey: 'an-invalid-key',
            customerId: 'chargify_customer_reference',
          })
        ).rejects.toThrow();
        expect(loggerMock.error).toHaveBeenCalledTimes(1);
      });

      it('should throw an error if the create subscription request fails', async () => {
        httpClientMock.post.mockRejectedValue(new Error(ERROR_MESSAGE));
        await expect(billingClient.createSubscription({})).rejects.toThrow();

        expect(loggerMock.error).toHaveBeenCalledTimes(1);
      });

      it('should return the subscription record when the create trial subscription request succeeds', async () => {
        const subscription = { id: 'an-id' };
        httpClientMock.post.mockResolvedValue({ data: { subscription } });
        const result = await billingClient.createSubscription({
          product: MOCK_PRODUCT_CATALOG['inspectionxpert-cloud'].products.ix_standard,
          customerId: 'chargify_customer_reference',
          userEmail: 'a@a.org',
        });

        expect(result).toEqual(subscription);
        expect(loggerMock.error).toHaveBeenCalledTimes(0);
      });

      it('should return the subscription record when the create paid subscription request succeeds', async () => {
        const subscription = { id: 'an-id' };
        httpClientMock.post.mockResolvedValue({ data: { subscription } });
        const result = await billingClient.createSubscription({
          catalogKey: 'ix_standard',
          product: MOCK_PRODUCT_CATALOG['inspectionxpert-cloud'].products.ix_standard,
          customerId: 'chargify_customer_reference',
          billingToken: 'a-token',
          userEmail: 'a@a.org',
        });

        expect(result).toEqual(subscription);
        expect(loggerMock.error).toHaveBeenCalledTimes(0);
      });
    });

    describe('#getSubscriptions()', () => {
      it('should throw an error if the request fails', async () => {
        httpClientMock.get.mockRejectedValue(new Error(ERROR_MESSAGE));
        await expect(billingClient.getSubscriptions()).rejects.toThrow();
        expect(loggerMock.error).toHaveBeenCalledTimes(1);
      });

      it('should throw an error if per_page is greater than 200', async () => {
        await expect(billingClient.getSubscriptions({ per_page: 201 })).rejects.toThrow();
        expect(loggerMock.error).toHaveBeenCalledTimes(1);
      });

      it('should throw an error if state value is invalid', async () => {
        await expect(billingClient.getSubscriptions({ state: 'bacon' })).rejects.toThrow();
        expect(loggerMock.error).toHaveBeenCalledTimes(1);
      });

      it('should throw an error if a start_date or end_date is provided and its value is not YYYY-MM-DD', async () => {
        await expect(billingClient.getSubscriptions({ start_date: '21-55-65' })).rejects.toThrow();
        await expect(billingClient.getSubscriptions({ end_date: '1111-55-65' })).rejects.toThrow();
        expect(loggerMock.error).toHaveBeenCalledTimes(2);
      });

      it('should throw an error if a start_datetime or end_datetime is provided and its value is not YYYY-MM-DD', async () => {
        await expect(billingClient.getSubscriptions({ start_datetime: '2021-09-29 55:12:89' })).rejects.toThrow();
        await expect(billingClient.getSubscriptions({ end_datetime: '1111-55-63 55:12' })).rejects.toThrow();
        expect(loggerMock.error).toHaveBeenCalledTimes(2);
      });

      it('should throw an error if sort value is invalid', async () => {
        await expect(billingClient.getSubscriptions({ sort: 'bacon' })).rejects.toThrow();
        expect(loggerMock.error).toHaveBeenCalledTimes(1);
      });

      it('should throw an error if direction value is invalid', async () => {
        await expect(billingClient.getSubscriptions({ direction: 'bacon' })).rejects.toThrow();
        expect(loggerMock.error).toHaveBeenCalledTimes(1);
      });

      it('should return the subscription record when the request is valid and succeeds', async () => {
        const subscriptions = [
          {
            subscription: { id: 'an-id' },
          },
          {
            subscription: { id: 'an-id' },
          },
        ];
        httpClientMock.get.mockResolvedValue({
          data: subscriptions,
        });
        const result = await billingClient.getSubscriptions({
          state: 'active',
          start_date: '2021-09-29',
          end_date: '2021-10-29',
          sort: 'signup_date',
          direction: 'asc',
          per_page: 2,
        });
        expect(result).toEqual(subscriptions);
        expect(loggerMock.error).toHaveBeenCalledTimes(0);
      });
    });

    describe('#getSubscriptionById()', () => {
      it('should throw an error if required input is not provided', async () => {
        await expect(billingClient.getSubscriptionById()).rejects.toThrow();

        expect(loggerMock.error).toHaveBeenCalledTimes(1);
      });

      it('should throw an error if the request fails', async () => {
        httpClientMock.get.mockRejectedValue(new Error(ERROR_MESSAGE));
        await expect(billingClient.getSubscriptionById('an-id')).rejects.toThrow();

        expect(loggerMock.error).toHaveBeenCalledTimes(1);
      });

      it('should return the subscription record when the request succeeds', async () => {
        const subscriptions = [{ id: 'an-id' }, { id: 'another-id' }];
        httpClientMock.get.mockResolvedValue({ data: { subscription: subscriptions } });
        const result = await billingClient.getSubscriptionById(subscriptions[0].id);

        expect(result).toEqual(subscriptions);
        expect(loggerMock.error).toHaveBeenCalledTimes(0);
      });
    });

    describe('#getSubscriptionByReference()', () => {
      it('should throw an error if required input is not provided', async () => {
        await expect(billingClient.getSubscriptionByReference()).rejects.toThrow();

        expect(loggerMock.error).toHaveBeenCalledTimes(1);
      });

      it('should throw an error if the request fails', async () => {
        httpClientMock.get.mockRejectedValue(new Error(ERROR_MESSAGE));
        await expect(billingClient.getSubscriptionByReference('an-id')).rejects.toThrow();

        expect(loggerMock.error).toHaveBeenCalledTimes(1);
      });

      it('should return the customer record when the request succeeds', async () => {
        const subscriptions = [{ id: 'an-id' }, { id: 'another-id' }];
        httpClientMock.get.mockResolvedValue({ data: subscriptions });
        const result = await billingClient.getSubscriptionByReference(subscriptions[0].id);

        expect(result).toEqual(subscriptions);
        expect(loggerMock.error).toHaveBeenCalledTimes(0);
      });
    });

    describe('#getSubscriptionsForCustomer()', () => {
      it('should throw an error if required input is not provided', async () => {
        await expect(billingClient.getSubscriptionsForCustomer()).rejects.toThrow();

        expect(loggerMock.error).toHaveBeenCalledTimes(1);
      });

      it('should throw an error if the request fails', async () => {
        httpClientMock.get.mockRejectedValue(new Error(ERROR_MESSAGE));
        await expect(billingClient.getSubscriptionsForCustomer('an-id')).rejects.toThrow();

        expect(loggerMock.error).toHaveBeenCalledTimes(1);
      });

      it('should return the customer record when the request succeeds', async () => {
        httpClientMock.get.mockResolvedValue(GET_SUBSCRIPTIONS_MULTIPLE_RESPONSE);
        const result = await billingClient.getSubscriptionsForCustomer(GET_SUBSCRIPTIONS_MULTIPLE_RESPONSE.data[0].id);
        expect(result[0]).toEqual(GET_SUBSCRIPTIONS_MULTIPLE_RESPONSE.data[0].subscription);
        expect(result[1]).toEqual(GET_SUBSCRIPTIONS_MULTIPLE_RESPONSE.data[1].subscription);
        expect(loggerMock.error).toHaveBeenCalledTimes(0);
      });
    });

    describe('#updateSubscription()', () => {
      it('should throw an error if required input is not provided', async () => {
        await expect(billingClient.updateSubscription({})).rejects.toThrow();
        expect(loggerMock.error).toHaveBeenCalledTimes(1);
      });

      it('should throw an error if the request fails', async () => {
        httpClientMock.put.mockRejectedValue(new Error(ERROR_MESSAGE));
        await expect(billingClient.updateSubscription({ id: 'an-id', billingToken: 'some-token' })).rejects.toThrow();
        expect(loggerMock.error).toHaveBeenCalledTimes(1);
      });

      it('should return the subscription record that reflects the update when the request succeeds', async () => {
        const subscription = { id: 'an-id', catalogKey: 'ix_standard' };
        httpClientMock.put.mockResolvedValue({
          data: {
            subscription: {
              id: subscription.id,
              product_handle: 'ix_standard',
              product_price_point_handle: 'ix_standard',
              components: [
                {
                  component_id: 'handle:ix_standard_trial_user_seat',
                  price_point_id: 'handle:standard',
                  allocated_quantity: 1,
                },
              ],
            },
          },
        });
        const result = await billingClient.updateSubscription({
          id: subscription.id,
          catalogKey: subscription.catalogKey,
        });

        expect(result.id).toEqual(subscription.id);
        expect(result.product_handle).toEqual(subscription.catalogKey);
        // TODO: Add subscription
        expect(loggerMock.error).toHaveBeenCalledTimes(0);

        const apiUrl = httpClientMock.put.mock.calls[0][0];
        expect(apiUrl.includes(subscription.id));
      });

      it('should return the subscription record that reflects an update to attributes that require an override when the request succeeds', async () => {
        // activated_at, canceled_at, cancellation_message, expires_at
        const newDate = new Date('2021-01-01');
        const subscription = {
          id: 'an-id',
          activated_at: newDate.toISOString(),
        };
        httpClientMock.get.mockResolvedValue({
          data: {
            subscription: {
              id: subscription.id,
              activated_at: newDate.toISOString(),
              product_handle: 'ix_standard',
              product_price_point_handle: 'ix_standard',
              components: [
                {
                  component_id: 'handle:ix_standard_trial_user_seat',
                  price_point_id: 'handle:standard',
                  allocated_quantity: 1,
                },
              ],
            },
          },
        });

        httpClientMock.put.mockResolvedValue({
          data: {
            subscription: {
              id: subscription.id,
              activated_at: newDate.toISOString(),
              product_handle: 'ix_standard',
              product_price_point_handle: 'ix_standard',
              components: [
                {
                  component_id: 'handle:ix_standard_trial_user_seat',
                  price_point_id: 'handle:standard',
                  allocated_quantity: 1,
                },
              ],
            },
          },
        });

        const result = await billingClient.updateSubscription({
          id: subscription.id,
          activated_at: newDate.toISOString(),
        });

        expect(result.id).toEqual(subscription.id);
        expect(result.activated_at).toEqual(subscription.activated_at);
        // TODO: Add subscription
        expect(loggerMock.error).toHaveBeenCalledTimes(0);

        const apiUrl = httpClientMock.put.mock.calls[0][0];
        expect(apiUrl.includes(subscription.id));
      });
    });

    describe('#updateSubscriptionQuantity()', () => {
      it('should throw an error if required input is not provided', async () => {
        await expect(billingClient.updateSubscriptionQuantity({})).rejects.toThrow();
        expect(loggerMock.error).toHaveBeenCalledTimes(1);
      });

      it('should throw an error if the request fails', async () => {
        httpClientMock.post.mockRejectedValue(new Error(ERROR_MESSAGE));
        await expect(billingClient.updateSubscriptionQuantity({ id: 'an-id' })).rejects.toThrow();
        expect(loggerMock.error).toHaveBeenCalledTimes(1);
      });

      it('should update the quantity of the specified item in the subscription when the request succeeds', async () => {
        const component = {
          id: 'an-id',
          quantity: 0,
        };
        httpClientMock.post.mockResolvedValue({
          data: {
            allocation: {
              allocation_id: 587878036,
              component_id: 984732,
              subscription_id: 36504178,
              quantity: 1,
              previous_quantity: 0,
              memo: 'Trial converted to annual subscription',
              timestamp: '2020-10-12T02:32:38Z',
              proration_upgrade_scheme: 'prorate-attempt-capture',
              proration_downgrade_scheme: 'no-prorate',
              price_point_id: 866444,
              previous_price_point_id: 866444,
              component_handle: 'ix_standard_annual_user_seat',
              accrue_charge: false,
              upgrade_charge: 'prorated',
              downgrade_credit: 'none',
              created_at: '2020-10-11T22:32:38-04:00',
              payment: null,
            },
          },
        });
        const result = await billingClient.updateSubscriptionQuantity({
          id: 36504178,
          componentId: component.id,
          newQuantity: 1,
          memo: 'Trial converted to annual subscription',
        });

        expect(result.quantity).toEqual(1);
        expect(result.previous_quantity).toEqual(0);
        expect(loggerMock.error).toHaveBeenCalledTimes(0);

        const apiUrl = httpClientMock.post.mock.calls[0][0];
        expect(apiUrl.includes(component.id));
      });
    });

    describe('#delayedCancelSubscription()', () => {
      it('should throw an error if required input is not provided', async () => {
        await expect(billingClient.delayedCancelSubscription()).rejects.toThrow();

        expect(loggerMock.error).toHaveBeenCalledTimes(1);
      });

      it('should throw an error if the request fails', async () => {
        httpClientMock.post.mockRejectedValue(new Error(ERROR_MESSAGE));
        await expect(billingClient.delayedCancelSubscription('an-id', 'another-id')).rejects.toThrow();

        expect(loggerMock.error).toHaveBeenCalledTimes(1);
      });

      it('should return false if the response has a non-2XX status', async () => {
        httpClientMock.post.mockResolvedValue({ status: 422 });
        const result = await billingClient.delayedCancelSubscription('an-id', 'another-id');

        expect(result).toBe(false);
        expect(loggerMock.error).toHaveBeenCalledTimes(0);
      });

      it('should return true if the response has a 200 status', async () => {
        httpClientMock.post.mockResolvedValue({ status: 200 });
        const result = await billingClient.delayedCancelSubscription('an-id', 'another-id');

        expect(result).toBe(true);
        expect(loggerMock.error).toHaveBeenCalledTimes(0);
      });

      it('should return true if the response has a 201 status', async () => {
        httpClientMock.post.mockResolvedValue({ status: 201 });
        const result = await billingClient.delayedCancelSubscription('an-id', 'another-id');

        expect(result).toBe(true);
        expect(loggerMock.error).toHaveBeenCalledTimes(0);
      });
    });

    describe('#immediatelyCancelSubscription()', () => {
      it('should throw an error if required input is not provided', async () => {
        await expect(billingClient.immediatelyCancelSubscription()).rejects.toThrow();

        expect(loggerMock.error).toHaveBeenCalledTimes(1);
      });

      it('should throw an error if the request fails', async () => {
        httpClientMock.delete.mockRejectedValue(new Error(ERROR_MESSAGE));
        await expect(billingClient.immediatelyCancelSubscription('an-id', 'another-id')).rejects.toThrow();

        expect(loggerMock.error).toHaveBeenCalledTimes(1);
      });

      it('should return false if the response has a non-2XX status', async () => {
        httpClientMock.delete.mockResolvedValue({ status: 422 });
        const result = await billingClient.immediatelyCancelSubscription('an-id', 'another-id');

        expect(result).toBe(false);
        expect(loggerMock.error).toHaveBeenCalledTimes(0);
      });

      it('should return true if the response has a 200 status', async () => {
        httpClientMock.delete.mockResolvedValue({ status: 200 });
        const result = await billingClient.immediatelyCancelSubscription('an-id', 'another-id');

        expect(result).toBe(true);
        expect(loggerMock.error).toHaveBeenCalledTimes(0);
      });

      it('should return true if the response has a 201 status', async () => {
        httpClientMock.delete.mockResolvedValue({ status: 201 });
        const result = await billingClient.immediatelyCancelSubscription('an-id', 'another-id');

        expect(result).toBe(true);
        expect(loggerMock.error).toHaveBeenCalledTimes(0);
      });
    });

    describe('#reactivateSubscription()', () => {
      it('should throw an error if required input is not provided', async () => {
        await expect(billingClient.reactivateSubscription()).rejects.toThrow();

        expect(loggerMock.error).toHaveBeenCalledTimes(1);
      });

      it('should throw an error if the request fails', async () => {
        httpClientMock.put.mockRejectedValue(new Error(ERROR_MESSAGE));
        await expect(billingClient.reactivateSubscription('an-id', 'another-id')).rejects.toThrow();

        expect(loggerMock.error).toHaveBeenCalledTimes(1);
      });

      it('should return false if the response has a non-2XX status', async () => {
        httpClientMock.put.mockResolvedValue({ status: 422 });
        const result = await billingClient.reactivateSubscription('an-id', 'another-id');

        expect(result).toBe(false);
        expect(loggerMock.error).toHaveBeenCalledTimes(0);
      });

      it('should return true if the response has a 200 status', async () => {
        httpClientMock.put.mockResolvedValue({ status: 200 });
        const result = await billingClient.reactivateSubscription('an-id', 'another-id');

        expect(result).toBe(true);
        expect(loggerMock.error).toHaveBeenCalledTimes(0);
      });

      it('should return true if the response has a 201 status', async () => {
        httpClientMock.put.mockResolvedValue({ status: 201 });
        const result = await billingClient.reactivateSubscription('an-id', 'another-id');

        expect(result).toBe(true);
        expect(loggerMock.error).toHaveBeenCalledTimes(0);
      });
    });

    describe('#removeDelayedCancel()', () => {
      it('should throw an error if required input is not provided', async () => {
        await expect(billingClient.removeDelayedCancel()).rejects.toThrow();

        expect(loggerMock.error).toHaveBeenCalledTimes(1);
      });

      it('should throw an error if the request fails', async () => {
        httpClientMock.delete.mockRejectedValue(new Error(ERROR_MESSAGE));
        await expect(billingClient.removeDelayedCancel('an-id', 'another-id')).rejects.toThrow();

        expect(loggerMock.error).toHaveBeenCalledTimes(1);
      });

      it('should return false if the response has a non-2XX status', async () => {
        httpClientMock.delete.mockResolvedValue({ status: 422 });
        const result = await billingClient.removeDelayedCancel('an-id', 'another-id');

        expect(result).toBe(false);
        expect(loggerMock.error).toHaveBeenCalledTimes(0);
      });

      it('should return true if the response has a 200 status', async () => {
        httpClientMock.delete.mockResolvedValue({ status: 200 });
        const result = await billingClient.removeDelayedCancel('an-id', 'another-id');

        expect(result).toBe(true);
        expect(loggerMock.error).toHaveBeenCalledTimes(0);
      });

      it('should return true if the response has a 201 status', async () => {
        httpClientMock.delete.mockResolvedValue({ status: 201 });
        const result = await billingClient.removeDelayedCancel('an-id', 'another-id');

        expect(result).toBe(true);
        expect(loggerMock.error).toHaveBeenCalledTimes(0);
      });
    });

    describe('#purgeSubscription()', () => {
      beforeEach(() => {
        if (USE_MOCKS) {
          billingClient = new ChargifyBillingAdapter({
            apiKey: API_KEY,
            publicKey: PUBLIC_KEY,
            privateKey: PRIVATE_KEY,
            siteName: SITE_NAME,
            httpClient: httpClientMock,
            logger: loggerMock,
          });
        }
      });

      it('should throw an error called against a site name that does not have the word test in it', async () => {
        if (USE_MOCKS) {
          billingClient = new ChargifyBillingAdapter({
            apiKey: API_KEY,
            publicKey: PUBLIC_KEY,
            privateKey: PRIVATE_KEY,
            siteName: 'a-prod-site',
            httpClient: httpClientMock,
            logger: loggerMock,
          });
          await expect(billingClient.purgeSubscription({ subscriptionId: 123, customerId: 123 })).rejects.toThrow();
          expect(loggerMock.error).toHaveBeenCalledTimes(1);
        }
      });

      it('should throw an error if required input is not provided', async () => {
        await expect(billingClient.purgeSubscription({})).rejects.toThrow();
        await expect(billingClient.purgeSubscription({ subscriptionId: 123 })).rejects.toThrow();
        await expect(billingClient.purgeSubscription({ customerId: 123 })).rejects.toThrow();
        expect(loggerMock.error).toHaveBeenCalledTimes(3);
      });

      it('should throw an error if the request fails', async () => {
        httpClientMock.post.mockRejectedValue(new Error(ERROR_MESSAGE));
        await expect(billingClient.purgeSubscription({ subscriptionId: 123, customerId: 123 })).rejects.toThrow();
        expect(loggerMock.error).toHaveBeenCalledTimes(1);
      });

      it('should return the subscription record that reflects the update when the request succeeds', async () => {
        httpClientMock.post.mockResolvedValue({
          data: {
            subscription: MOCK_SUBSCRIPTION,
          },
        });
        const result = await billingClient.purgeSubscription({ subscriptionId: 123, customerId: 123 });

        expect(result.id).toEqual(MOCK_SUBSCRIPTION.id);
        expect(result.customer.id).toEqual(MOCK_SUBSCRIPTION.customer.id);
        expect(loggerMock.error).toHaveBeenCalledTimes(0);
      });
    });
  });

  describe('Custom Fields', () => {
    describe('#getSubscriptionCustomFields()', () => {
      it('should throw an error if required input is not provided', async () => {
        await expect(billingClient.getSubscriptionCustomFields()).rejects.toThrow();

        expect(loggerMock.error).toHaveBeenCalledTimes(1);
      });

      it('should throw an error if the request fails', async () => {
        httpClientMock.get.mockRejectedValue(new Error(ERROR_MESSAGE));
        await expect(billingClient.getSubscriptionCustomFields('an-id')).rejects.toThrow();
        expect(loggerMock.error).toHaveBeenCalledTimes(1);
      });

      it('should return the metadata records when the request succeeds', async () => {
        const customFields = [
          {
            id: 15750346,
            value: 'Annually',
            resource_id: 37003269,
            deleted_at: null,
            name: 'billing_interval',
          },
          {
            id: 15750898,
            value: 'test',
            resource_id: 37003269,
            deleted_at: null,
            name: 'Test',
          },
        ];
        httpClientMock.get.mockResolvedValue({
          data: {
            total_count: 2,
            current_page: 1,
            total_pages: 1,
            per_page: 20,
            metadata: customFields,
          },
        });
        const result = await billingClient.getSubscriptionCustomFields(customFields[0].resource_id);

        expect(result).toEqual(customFields);
        expect(loggerMock.error).toHaveBeenCalledTimes(0);
      });
    });

    describe('#updateSubscriptionCustomFields()', () => {
      it('should throw an error if required input is not provided', async () => {
        await expect(billingClient.updateSubscriptionCustomFields({})).rejects.toThrow();
        expect(loggerMock.error).toHaveBeenCalledTimes(1);
      });

      it('should throw an error if the request fails', async () => {
        httpClientMock.put.mockRejectedValue(new Error(ERROR_MESSAGE));
        await expect(
          billingClient.updateSubscriptionCustomFields({
            subscriptionId: 37003269,
            changes: [],
          })
        ).rejects.toThrow();
        expect(loggerMock.error).toHaveBeenCalledTimes(1);
      });

      it('should return the metadata records that reflects the update when the request succeeds', async () => {
        const changes = [{ name: 'Test', value: 'changed!' }];
        httpClientMock.put.mockResolvedValue({
          data: [
            {
              value: 'changed!',
              deleted_at: null,
              id: 15750898,
              resource_id: 37003269,
              name: 'Test',
            },
          ],
        });
        const result = await billingClient.updateSubscriptionCustomFields({
          subscriptionId: 37003269,
          changes,
        });
        expect(result).toHaveLength(1);
        expect(result[0].name).toEqual(changes[0].name);
        expect(result[0].value).toEqual(changes[0].value);
        expect(loggerMock.error).toHaveBeenCalledTimes(0);

        const apiUrl = httpClientMock.put.mock.calls[0][0];
        expect(apiUrl.includes(37003269));
      });
    });

    describe('#deleteSubscriptionCustomFields()', () => {
      it('should throw an error if required input is not provided', async () => {
        await expect(billingClient.deleteSubscriptionCustomFields({})).rejects.toThrow();
        expect(loggerMock.error).toHaveBeenCalledTimes(1);
      });

      it('should throw an error if the request fails', async () => {
        httpClientMock.delete.mockRejectedValue(new Error(ERROR_MESSAGE));
        await expect(billingClient.deleteSubscriptionCustomFields({ subscriptionId: 'id', changes: ['fake'] })).rejects.toThrow();
        expect(loggerMock.error).toHaveBeenCalledTimes(1);
      });

      it('should return the metadata records that reflects the update when the request succeeds', async () => {
        const changes = ['Test'];
        httpClientMock.delete.mockResolvedValue(true);
        const result = await billingClient.deleteSubscriptionCustomFields({
          subscriptionId: 37003269,
          changes,
        });
        expect(result).toBeTruthy();
        expect(loggerMock.error).toHaveBeenCalledTimes(0);

        const apiUrl = httpClientMock.delete.mock.calls[0][0];
        expect(apiUrl.includes(37003269));
      });
    });
  });

  describe('Payment Methods', () => {
    // TODO: Write addPaymentMethod() tests once probable flow updates regarding being provided with the list of customers is done

    describe('#changeDefaultPaymentMethod()', () => {
      it('should throw an error if required input is not provided', async () => {
        await expect(billingClient.changeDefaultPaymentMethod()).rejects.toThrow();

        expect(loggerMock.error).toHaveBeenCalledTimes(1);
      });

      it('should throw an error if the request fails', async () => {
        httpClientMock.post.mockRejectedValue(new Error(ERROR_MESSAGE));
        await expect(billingClient.changeDefaultPaymentMethod('an-id', 'another-id')).rejects.toThrow();

        expect(loggerMock.error).toHaveBeenCalledTimes(1);
      });

      it('should return the payment profile on success', async () => {
        httpClientMock.post.mockResolvedValue({ data: MOCK_PAYMENT_PROFILE });
        const result = await billingClient.changeDefaultPaymentMethod('an-id', 'another-id');

        expect(result).toMatchObject(MOCK_PAYMENT_PROFILE.payment_profile);
        expect(loggerMock.error).toHaveBeenCalledTimes(0);
      });
    });

    describe('chargeSubscriptionPrePayment', () => {
      it('should throw an error if the required input is not provided', async () => {
        await expect(billingClient.chargeSubscriptionPrePayment()).rejects.toThrow();

        expect(loggerMock.error).toHaveBeenCalledTimes(1);
      });

      it('should throw an error if the request fails', async () => {
        httpClientMock.post.mockRejectedValue(new Error(ERROR_MESSAGE));
        await expect(billingClient.chargeSubscriptionPrePayment('123', 123, '123', '123')).rejects.toThrow();

        expect(loggerMock.error).toHaveBeenCalledTimes(1);
      });

      it('should return the payment information on success', async () => {
        const prepayment = {
          id: 1234,
          subscription_id: 1234,
          amount_in_cents: 1234,
          memo: 'Prepayment',
          created_at: '2021-03-24T11:32:45-04:00',
          starting_balance_in_cents: 1234,
          ending_balance_in_cents: 0,
        };
        httpClientMock.post.mockResolvedValue({
          status: 201,
          data: { prepayment },
        });
        const result = await billingClient.chargeSubscriptionPrePayment('1234', 1234, '1234', 'Prepayment');

        expect(result).toBe(prepayment);
        expect(loggerMock.error).toHaveBeenCalledTimes(0);
      });
    });

    describe('getBillingPortalLink', () => {
      it('should throw an error if the required input is not provided', async () => {
        await expect(billingClient.getBillingPortalLink()).rejects.toThrow();

        expect(loggerMock.error).toHaveBeenCalledTimes(1);
      });

      it('should throw an error if the request fails', async () => {
        httpClientMock.post.mockRejectedValue({
          status: 422,
          errors: ['Billing Portal is not enabled for this customer.'],
        });
        await expect(billingClient.getBillingPortalLink('123')).rejects.toThrow();

        expect(loggerMock.error).toHaveBeenCalledTimes(1);
      });

      it('should return the payment information on success', async () => {
        const billingPortal = {
          url: 'https://www.billingportal.com/manage/19804639/1517596469/bd16498719a7d3e6',
          fetch_count: 1,
          created_at: '2018-02-02T18:34:29Z',
          new_link_available_at: '2018-02-17T18:34:29Z',
          expires_at: '2018-04-08T17:34:29Z',
          last_invite_sent_at: '2018-02-02T18:34:29Z',
        };
        httpClientMock.get.mockResolvedValue({
          status: 200,
          data: billingPortal,
        });
        const result = await billingClient.getBillingPortalLink('1234');

        expect(result).toBe(billingPortal);
        expect(loggerMock.error).toHaveBeenCalledTimes(0);
      });
    });
  });

  describe('Invoices', () => {
    describe('#getInvoices()', () => {
      it('should throw an error if the request fails', async () => {
        httpClientMock.get.mockRejectedValue(new Error(ERROR_MESSAGE));
        await expect(billingClient.getInvoices()).rejects.toThrow();

        expect(loggerMock.error).toHaveBeenCalledTimes(2); // One call for getCustomer() and one for getInvoices()
      });

      it('should handle no invoices found without erroring or infinitely looping', async () => {
        httpClientMock.get.mockResolvedValueOnce(GET_SUBSCRIPTIONS_SINGLE_RESPONSE);

        const result = await billingClient.getInvoices(USER_EMAIL);
        expect(result).toEqual([]);

        expect(loggerMock.error).toHaveBeenCalledTimes(0);
      });

      it('should not infinitely loop even if every request for invoices results in invoices retrieved', async () => {
        httpClientMock.get.mockResolvedValueOnce(GET_SUBSCRIPTIONS_SINGLE_RESPONSE);
        httpClientMock.get.mockResolvedValue(createGetInvoicesResponse());

        const result = await billingClient.getInvoices(USER_EMAIL);
        expect(result.length).toEqual(INVOICES_REQUESTS_COUNT_MAX);

        const getSubscriptionsForCustomerCallCount = 1;
        const expectedHttpGetCallCount = getSubscriptionsForCustomerCallCount + result.length;
        expect(httpClientMock.get).toHaveBeenCalledTimes(expectedHttpGetCallCount);

        expect(loggerMock.error).toHaveBeenCalledTimes(0);
      });

      it('should correctly capture invoice data for a single invoice', async () => {
        httpClientMock.get.mockResolvedValueOnce(GET_SUBSCRIPTIONS_MULTIPLE_RESPONSE);
        httpClientMock.get.mockResolvedValueOnce(createGetInvoicesResponse());

        const result = await billingClient.getInvoices(USER_EMAIL);

        expect(result.length).toBe(1);
        const actualInvoice = result[0];
        const expectedInvoice = createGetInvoicesResponse().data.invoices[0];
        expect(actualInvoice.id).toEqual(expectedInvoice.id);
        expect(actualInvoice.status).toEqual(expectedInvoice.status);
        expect(actualInvoice.issue_date).toEqual(expectedInvoice.issue_date);
        expect(actualInvoice.product_name).toEqual(expectedInvoice.product_name);
        expect(actualInvoice.total_amount).toEqual(expectedInvoice.total_amount);

        expect(loggerMock.error).toHaveBeenCalledTimes(0);
      });

      it('should correctly capture invoice data for multiple invoices', async () => {
        httpClientMock.get.mockResolvedValueOnce(GET_SUBSCRIPTIONS_MULTIPLE_RESPONSE);
        httpClientMock.get.mockResolvedValueOnce(createGetInvoicesResponse());
        httpClientMock.get.mockResolvedValueOnce(createGetInvoicesResponse());

        const result = await billingClient.getInvoices(USER_EMAIL);

        expect(result.length).toBe(2);
        const actualInvoice = result[1];
        const expectedInvoice = createGetInvoicesResponse().data.invoices[0];
        expect(actualInvoice.id).toEqual(expectedInvoice.id);
        expect(actualInvoice.status).toEqual(expectedInvoice.status);
        expect(actualInvoice.issue_date).toEqual(expectedInvoice.issue_date);
        expect(actualInvoice.product_name).toEqual(expectedInvoice.product_name);
        expect(actualInvoice.total_amount).toEqual(expectedInvoice.total_amount);

        expect(loggerMock.error).toHaveBeenCalledTimes(0);
      });

      it('should paginate when there is a single subscription', async () => {
        const responses = [];
        const invoicesCount = 3;
        for (let i = 0; i < invoicesCount; i++) {
          const response = createGetInvoicesResponse();
          const invoice = response.data.invoices[0];
          invoice.uid = i;
          responses.push(response);
        }

        httpClientMock.get.mockResolvedValueOnce(GET_SUBSCRIPTIONS_SINGLE_RESPONSE);
        httpClientMock.get.mockResolvedValueOnce(responses[0]);
        httpClientMock.get.mockResolvedValueOnce(responses[1]);
        httpClientMock.get.mockResolvedValueOnce(responses[2]);

        const results = await billingClient.getInvoices(USER_EMAIL, 1);
        expect(results.length).toBe(invoicesCount);

        const getSubscriptionsForCustomerCallCount = 1;
        const finalGetInvoicesCallWithNoResultsCount = 1;
        const expectedHttpGetCallCount = getSubscriptionsForCustomerCallCount + invoicesCount + finalGetInvoicesCallWithNoResultsCount;
        expect(httpClientMock.get).toHaveBeenCalledTimes(expectedHttpGetCallCount);

        const getInvoicesCalls = httpClientMock.get.mock.calls.filter((call) => call[0].endsWith('invoices.json'));
        getInvoicesCalls.forEach((call, index) => {
          expect(call[1].params.page).toBe(index + 1);
        });

        results.forEach((actualInvoice, index) => {
          const expectedInvoice = createGetInvoicesResponse().data.invoices[0];
          const expectedId = index; // The id values were set to match the counter in the for loop above
          expect(actualInvoice.uid).toEqual(expectedId);
          expect(actualInvoice.status).toEqual(expectedInvoice.status);
          expect(actualInvoice.issue_date).toEqual(expectedInvoice.issue_date);
          expect(actualInvoice.product_name).toEqual(expectedInvoice.product_name);
          expect(actualInvoice.total_amount).toEqual(expectedInvoice.total_amount);
        });

        expect(loggerMock.error).toHaveBeenCalledTimes(0);
      });

      it('should paginate when there are multiple subscriptions', async () => {
        httpClientMock.get.mockResolvedValueOnce(GET_SUBSCRIPTIONS_MULTIPLE_RESPONSE);

        const responses = [];
        const invoicesPerSubscription = 3;
        const subscriptionsCount = 2;
        for (let i = 0; i < subscriptionsCount; i++) {
          for (let ii = 0; ii < invoicesPerSubscription; ii++) {
            const response = createGetInvoicesResponse();
            const invoice = response.data.invoices[0];
            invoice.uid = `subscription${i + 1}-invoice${ii + 1}`;
            responses.push(response);
            httpClientMock.get.mockResolvedValueOnce(response);
            if (ii == invoicesPerSubscription - 1) {
              httpClientMock.get.mockResolvedValueOnce({ data: [] });
            }
          }
        }

        const results = await billingClient.getInvoices(USER_EMAIL, 1);

        const expectedGetInvoicesCallCount = subscriptionsCount * invoicesPerSubscription;
        expect(results.length).toBe(expectedGetInvoicesCallCount);

        const getSubscriptionsForCustomerCallCount = 1;
        const finalGetInvoicesCallWithNoResultsCount = subscriptionsCount;
        const expectedHttpGetCallCount = getSubscriptionsForCustomerCallCount + expectedGetInvoicesCallCount + finalGetInvoicesCallWithNoResultsCount;
        expect(httpClientMock.get).toHaveBeenCalledTimes(expectedHttpGetCallCount);

        const getInvoicesCalls = httpClientMock.get.mock.calls.filter((call) => call[0].endsWith('invoices.json'));
        const getInvoicesCallsSubscriptionOne = getInvoicesCalls.filter((call) => call[1].params.subscription_id === '01');
        getInvoicesCallsSubscriptionOne.forEach((call, index) => {
          expect(call[1].params.page).toBe(index + 1);
        });
        const getInvoicesCallsSubscriptionTwo = getInvoicesCalls.filter((call) => call[1].params.subscription_id === '02');
        getInvoicesCallsSubscriptionTwo.forEach((call, index) => {
          expect(call[1].params.page).toBe(index + 1);
        });

        const resultsSubscriptionOne = results.filter((result) => result.uid.startsWith('subscription1'));
        resultsSubscriptionOne.forEach((actualInvoice, index) => {
          const expectedInvoice = createGetInvoicesResponse().data.invoices[0];
          const expectedId = `subscription1-invoice${index + 1}`;

          expect(actualInvoice.uid).toEqual(expectedId);
          expect(actualInvoice.status).toEqual(expectedInvoice.status);
          expect(actualInvoice.issue_date).toEqual(expectedInvoice.issue_date);
          expect(actualInvoice.product_name).toEqual(expectedInvoice.product_name);
          expect(actualInvoice.total_amount).toEqual(expectedInvoice.total_amount);
        });

        const resultsSubscriptionTwo = results.filter((result) => result.uid.startsWith('subscription2'));
        resultsSubscriptionTwo.forEach((actualInvoice, index) => {
          const expectedInvoice = createGetInvoicesResponse().data.invoices[0];
          const expectedId = `subscription2-invoice${index + 1}`;

          expect(actualInvoice.uid).toEqual(expectedId);
          expect(actualInvoice.status).toEqual(expectedInvoice.status);
          expect(actualInvoice.issue_date).toEqual(expectedInvoice.issue_date);
          expect(actualInvoice.product_name).toEqual(expectedInvoice.product_name);
          expect(actualInvoice.total_amount).toEqual(expectedInvoice.total_amount);
        });

        expect(loggerMock.error).toHaveBeenCalledTimes(0);
      });
    });

    describe('getInvoicesForSubscription', () => {
      it('should throw an error if the required input is not provided', async () => {
        await expect(billingClient.getInvoicesForSubscription()).rejects.toThrow();

        expect(loggerMock.error).toHaveBeenCalledTimes(1);
      });

      it('should throw an error if the request fails', async () => {
        httpClientMock.post.mockRejectedValue(new Error(ERROR_MESSAGE));
        await expect(billingClient.getInvoicesForSubscription('123')).rejects.toThrow();

        expect(loggerMock.error).toHaveBeenCalledTimes(1);
      });

      it('should return the payment information on success', async () => {
        httpClientMock.get.mockResolvedValue({
          status: 201,
          data: {
            invoices: [fakeInvoice],
          },
        });
        const result = await billingClient.getInvoicesForSubscription('1234');

        expect(result).toHaveLength(1);
        expect(result[0]).toBe(fakeInvoice);
        expect(loggerMock.error).toHaveBeenCalledTimes(0);
      });
    });

    describe('#downloadInvoice()', () => {
      it('should throw an error if the request fails', async () => {
        httpClientMock.get.mockRejectedValue(new Error(ERROR_MESSAGE));
        await expect(billingClient.downloadInvoice()).rejects.toThrow();

        expect(loggerMock.error).toHaveBeenCalledTimes(1);
      });

      it('should return the invoice id and payload when the request succeeds', async () => {
        const id = 'an-id';
        httpClientMock.get.mockResolvedValue(GET_INVOICE_RESPONSE);
        const result = await billingClient.downloadInvoice(id);

        expect(result.id).toEqual(id);
        expect(result.base64Payload).toEqual(Buffer.from(GET_INVOICE_RESPONSE.data, 'binary').toString('base64'));
        expect(loggerMock.error).toHaveBeenCalledTimes(0);
      });
    });

    describe('issueInvoice', () => {
      it('should throw an error if the required input is not provided', async () => {
        await expect(billingClient.issueInvoice()).rejects.toThrow();

        expect(loggerMock.error).toHaveBeenCalledTimes(1);
      });

      it('should throw an error if the request fails', async () => {
        httpClientMock.post.mockRejectedValue(new Error(ERROR_MESSAGE));
        await expect(billingClient.issueInvoice('123', [{ component_id: 123, allocated_quantity: 1, start: '2021-03-25', end: '2021-04-25' }])).rejects.toThrow();

        expect(loggerMock.error).toHaveBeenCalledTimes(1);
      });

      it('should return the payment information on success', async () => {
        httpClientMock.post.mockResolvedValue({
          status: 201,
          data: {
            invoice: fakeInvoice,
          },
        });
        const result = await billingClient.issueInvoice('1234', [{ component_id: 123, allocated_quantity: 1, start: '2021-03-25', end: '2021-04-25' }]);

        expect(result).toBe(fakeInvoice);
        expect(loggerMock.error).toHaveBeenCalledTimes(0);
      });
    });

    describe('payInvoice', () => {
      it('should throw an error if the required input is not provided', async () => {
        await expect(billingClient.payInvoice()).rejects.toThrow();

        expect(loggerMock.error).toHaveBeenCalledTimes(1);
      });

      it('should throw an error if the request fails', async () => {
        httpClientMock.post.mockRejectedValue(new Error(ERROR_MESSAGE));
        await expect(billingClient.payInvoice('123', 10.0)).rejects.toThrow();

        expect(loggerMock.error).toHaveBeenCalledTimes(1);
      });

      it('should return the payment information on success', async () => {
        httpClientMock.post.mockResolvedValue({
          status: 201,
          data: fakeInvoice,
        });
        const result = await billingClient.payInvoice('1234', 10.0);

        expect(result).toBe(fakeInvoice);
        expect(loggerMock.warn).toHaveBeenCalledTimes(1);
        expect(loggerMock.error).toHaveBeenCalledTimes(0);
      });
    });

    describe('voidInvoice', () => {
      it('should throw an error if the required input is not provided', async () => {
        await expect(billingClient.voidInvoice()).rejects.toThrow();

        expect(loggerMock.error).toHaveBeenCalledTimes(1);
      });

      it('should throw an error if the request fails', async () => {
        httpClientMock.post.mockRejectedValue(new Error(ERROR_MESSAGE));
        await expect(billingClient.voidInvoice('123')).rejects.toThrow();

        expect(loggerMock.error).toHaveBeenCalledTimes(1);
      });

      it('should return the payment information on success', async () => {
        httpClientMock.post.mockResolvedValue({
          status: 201,
          data: fakeInvoice,
        });
        const result = await billingClient.voidInvoice('1234');

        expect(result).toBe(fakeInvoice);
        expect(loggerMock.error).toHaveBeenCalledTimes(0);
      });
    });
  });
});
