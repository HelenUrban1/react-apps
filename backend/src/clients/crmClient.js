const HubspotCRMAdapter = require('./serviceAdapters/hubspotCRMAdapter');
// const SalesforceCRMAdapter = require('./serviceAdapters/salesforceCRMAdapter');

/**
 * crmClient provides the application with a standard interface to any CRM
 *
 * @param {Object} params - All required config data for the adapter
 * @prop  {String} useAdapter - The ID of the CRM adapter to load (default is 'hubspot')
 * @prop  {Object} [logger] - The logging utility. If not provided, it defaults to console.
 * @return {crmClient}
 */
export default function makeCrmClient(params = {}) {
  // Extract adapter
  const { useAdapter = 'hubspot', logger = console } = params;
  try {
    // Initialize the requested adapter
    let crmAdapter;
    if (useAdapter === 'hubspot') {
      console.log('makeCrmClient params:', params);
      crmAdapter = new HubspotCRMAdapter(params);
      // This is where other adapter would be registered
      // if (useAdapter === 'salesforce') {
      //   crmAdapter = new SalesforceCRMAdapter(params);
    } else {
      throw new Error(`Unrecognized CRM adapter ${useAdapter}`);
    }
    return crmAdapter;
  } catch (error) {
    logger.error(`crmClient.makeCrmClient failed. ${error}`);
    throw error;
  }
}
