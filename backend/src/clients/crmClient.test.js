const dotenv = require('dotenv');

dotenv.config({ path: '../.env' });

const TEST_PRIVATE_ACCESS_TOKEN = process.env.IXC_HUBSPOT_PRIVATE_ACCESS_TOKEN;
const TEST_API_KEY = process.env.IXC_HUBSPOT_API_KEY;

const makeCrmClient = require('./crmClient').default;

// prettier-ignore
const {
  getHubspotMock,
} = require('./serviceAdapters/fixtures/hubspot.fixtures');

describe('crmClient', () => {
  let crmClient;

  const hubspotConfigMock = {
    privateAccessToken: TEST_PRIVATE_ACCESS_TOKEN,
    apiKey: TEST_API_KEY,
    appDevApiKey: 'Some Developer API Key',
    appId: 123456,
  };

  const hubspotMock = getHubspotMock();

  const loggerMock = {
    trace: jest.fn(),
    debug: jest.fn(),
    log: jest.fn(),
    info: jest.fn(),
    warn: jest.fn(),
    error: jest.fn(),
  };

  beforeEach(() => {
    jest.resetAllMocks();
  });

  describe('makeCrmClient', () => {
    it('should throw an error if required input is not provided', async () => {
      expect(() => {
        makeCrmClient({ logger: loggerMock });
      }).toThrow();
      expect(loggerMock.error).toHaveBeenCalledTimes(1);
    });

    it('should throw an error if an unrecognized adapter is requested', async () => {
      expect(() => {
        makeCrmClient({
          useAdapter: 'foo',
          logger: loggerMock,
          hubspotMock,
          ...hubspotConfigMock,
        });
      }).toThrow();
      expect(loggerMock.error).toHaveBeenCalledTimes(1);
    });

    it('should populate the result properly if given a valid adapter', async () => {
      crmClient = makeCrmClient({
        useAdapter: 'hubspot',
        logger: loggerMock,
        hubspotMock,
        ...hubspotConfigMock,
      });
      // Adapter metadata
      expect(crmClient.adapter).toBeDefined();
      expect(crmClient.adapter.name).toBe('hubspot');
      expect(crmClient.adapter.isMocked).toBeDefined();
      // Comapanies
      expect(crmClient.getCompanyById).toBeDefined();
      expect(crmClient.getCompanyByDomain).toBeDefined();
      expect(crmClient.createCompany).toBeDefined();
      expect(crmClient.updateCompany).toBeDefined();
      expect(crmClient.createOrUpdateCompany).toBeDefined();
      expect(crmClient.addContactToCompany).toBeDefined();
      // Contacts
      expect(crmClient.getContactById).toBeDefined();
      expect(crmClient.createOrUpdateContact).toBeDefined();
      expect(crmClient.updateContact).toBeDefined();
      // Deals
      expect(crmClient.getDealById).toBeDefined();
      expect(crmClient.createDeal).toBeDefined();
      expect(crmClient.updateDeal).toBeDefined();
      expect(crmClient.addContactToDeal).toBeDefined();
      expect(crmClient.addCompanyToDeal).toBeDefined();
      expect(crmClient.removeContactFromDeal).toBeDefined();
      expect(crmClient.removeCompanyFromDeal).toBeDefined();
    });

    it('should fall back to a valid adapter if one is not specified', async () => {
      crmClient = await makeCrmClient({
        logger: loggerMock,
        hubspotMock,
        ...hubspotConfigMock,
      });

      // Adapter metadata
      expect(crmClient.adapter).toBeDefined();
      expect(crmClient.adapter.name).toBe('hubspot');
      expect(crmClient.adapter.isMocked).toBeDefined();
      // Comapanies
      expect(crmClient.getCompanyById).toBeDefined();
      expect(crmClient.getCompanyByDomain).toBeDefined();
      expect(crmClient.createCompany).toBeDefined();
      expect(crmClient.updateCompany).toBeDefined();
      expect(crmClient.createOrUpdateCompany).toBeDefined();
      expect(crmClient.addContactToCompany).toBeDefined();
      // Contacts
      expect(crmClient.getContactById).toBeDefined();
      expect(crmClient.createOrUpdateContact).toBeDefined();
      expect(crmClient.updateContact).toBeDefined();
      // Deals
      expect(crmClient.getDealById).toBeDefined();
      expect(crmClient.createDeal).toBeDefined();
      expect(crmClient.updateDeal).toBeDefined();
      expect(crmClient.addContactToDeal).toBeDefined();
      expect(crmClient.addCompanyToDeal).toBeDefined();
      expect(crmClient.removeContactFromDeal).toBeDefined();
      expect(crmClient.removeCompanyFromDeal).toBeDefined();
    });
  });
});
