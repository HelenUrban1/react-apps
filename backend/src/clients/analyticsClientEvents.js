module.exports = {
  errorsNonspecific: {
    name: 'Backend Error',
    version: '1.0.0',
  },

  userFileUpload: {
    name: 'user uploaded file ',
    version: '1.0.0',
  },

  email: {
    sent: {
      name: 'Email Sent',
      version: '1.0.0',
    },

    processed: {
      name: 'Email Processed',
      version: '1.0.0',
    },

    deferred: {
      name: 'Email Deferred',
      version: '1.0.0',
    },

    delivered: {
      name: 'Email Delivered',
      version: '1.0.0',
    },

    open: {
      name: 'Email Opened',
      version: '1.0.0',
    },

    click: {
      name: 'Email Clicked',
      version: '1.0.0',
    },

    bounce: {
      name: 'Email Bounced',
      version: '1.0.0',
    },

    dropped: {
      name: 'Email Dropped',
      version: '1.0.0',
    },

    unsubscribe: {
      name: 'Email Unsubscribe',
      version: '1.0.0',
    },

    group_unsubscribe: {
      name: 'Email Group Unsubscribe',
      version: '1.0.0',
    },

    group_resubscribe: {
      name: 'Email Group Resubscribe',
      version: '1.0.0',
    },

    spamreport: {
      name: 'Email Spam Report',
      version: '1.0.0',
    },

    error: {
      name: 'Email Event Processing Error',
      version: '1.0.0',
    },
  },

  chargify: {
    payment_failure: {
      name: 'Payment Failed',
      version: '1.0.0',
    },
    error: {
      name: 'Chargify Event Processing Error',
      version: '1.0.0',
    },
  },
};
