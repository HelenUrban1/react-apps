const Analytics = require('analytics-node');
const { log } = require('../logger');
const config = require('../../config')();
const events = require('./analyticsClientEvents');
const constants = require('./analyticsClientConstants');
// const { log } = require('../logger');

/**
 * Product instrumentation client that sends data to Segment.io using
 * the Segment.io Node SDK.
 */
module.exports = class AnalyticsClient {
  /**
   * @param {Object} params
   * @prop  {String} writeKey - The Segment.io source write key
   * @prop  {Object} [ClientSdk] - The Segment.io SDK. Used by tests
   * @return {AnalyticsClient}
   */
  constructor(params) {
    const { writeKey, ClientSdk = Analytics } = params;
    this.client = new ClientSdk(writeKey);
    this.events = events;
  }

  /**
   * Links a user to a specific user id.
   * Note that frontend/src/modules/shared/analytics/analytics.js also has an identify method.
   *
   * @see {@link https://segment.com/docs/connections/sources/catalog/libraries/server/node/#identify}
   *
   * @param {Object} params - See docs linked to above for all params except the custom ones listed below
   * @prop  {Object} params.req - The request object
   * @prop  {Object} params.ixcEvent - The event. Named this way to distinguish from the `event` param in the SDK call
   */
  identify(params = {}) {
    try {
      // const event = params.ixcEvent || {};
      const event = { name: 'Identify', version: '1.0.0' };
      params.event = event.name;
      params.traits = params.traits || {};

      this.ensureUserIdentification(params);
      this.populateContext(params);
      this.populateEventMetadata(params.traits, event);
      this.deleteCustomParams(params);
      if (config.isNotTestEnv) log.debug('Analytics.identify', params);
      this.client.identify(params);
    } catch (error) {
      this.onError(error, 'identify');
    }
  }

  /**
   * Ensures that either the `userId` or `anonymousId` param is set. If neither is
   * set, the SDK will throw an exception.
   *
   * @private
   * @param {Object} params
   * @prop  {Object} params.req
   */
  ensureUserIdentification(params) {
    if (params.userId || params.anonymousId) {
      return;
    }

    const req = params.req || { headers: {} };
    params.anonymousId = req.headers[constants.ANONYMOUS_ID_HEADER] || constants.UNDEFINED_ANONYMOUS_ID;
  }

  /**
   * Emits a tracking event.
   *
   * @see {@link https://segment.com/docs/connections/sources/catalog/libraries/server/node/#track}
   *
   * @param {Object} params - See docs linked to above for all params except the custom ones listed below
   * @prop  {Object} params.req - The request object
   * @prop  {Object} params.ixcEvent - The event. Named this way to distinguish from the `event` param in the SDK call
   */
  track(params) {
    try {
      const event = params.ixcEvent || {};
      params.event = event.name;
      params.properties = params.properties || {};
      this.ensureUserIdentification(params);
      this.populateContext(params);
      this.generateUserMetadata(params);
      this.populateEventMetadata(params.properties, event);
      this.generateObjectMetadata(params);
      this.deleteCustomParams(params);
      if (config.isNotTestEnv) log.debug('Analytics.track', params);
      this.client.track(params);
    } catch (error) {
      this.onError(error, 'track');
    }
  }

  /**
   * Associates an identified user with a group.
   *
   * @see {@link https://segment.com/docs/connections/sources/catalog/libraries/server/node/#group}
   *
   * @param {Object} params - See docs linked to above for all params except the custom ones listed below
   * @prop  {Object} params.req - The request object
   * @prop  {Object} params.ixcEvent - The event. Named this way to distinguish from the `event` param in the SDK call
   */
  group(params) {
    try {
      const event = params.ixcEvent || {};
      params.event = event.name;
      params.traits = params.traits || {};

      this.ensureUserIdentification(params);
      this.populateContext(params);
      this.populateEventMetadata(params.traits, event);
      this.deleteCustomParams(params);
      if (config.isNotTestEnv) log.debug('Analytics.group', params);
      this.client.group(params);
    } catch (error) {
      this.onError(error, 'group');
    }
  }

  /**
   * Handles a caught exception.
   *
   * @private
   * @param {Error} error - The error
   * @param {String} method - The name of the calling method
   */
  onError(error, method) {
    if (process.env.NODE_ENV !== 'test') {
      console.error(`AnalyticsClient ${method} error`, error);
    }
  }

  /**
   * Populates context object passed to segment.io
   *
   * @see {@link https://segment.com/docs/connections/spec/common/}
   *
   * @private
   * @param {Object} params
   */
  populateContext(params) {
    params.context = params.context || {};

    // Release information
    params.context.app = {
      name: 'IXC',
      namespace: 'com.inspectionxpert.app.backend',
      // version: 'n.n.n',
      // build: '<GIT HASH HERE>',
    };
    if (config.package && config.package.version) params.context.app.version = config.package.version;
    if (config.scm && config.scm.commit) params.context.app.build = config.scm.commit;
  }

  /**
   * Sets metadata on the object provided by reference.
   *
   * @private
   * @param {Object} container
   * @param {Object} event
   */
  populateEventMetadata(container, event) {
    container = container || {};
    container.source = constants.EVENT_SOURCE_BACKEND;
    container.eventVersion = event.version;
  }

  /**
   * Generate metadata for Identify event, such as user and plan info.
   *
   * @private
   * @param {Object} params
   */
  generateUserMetadata(params) {
    // Extract user from request or from params
    let user;
    if (params.req && params.req.currentUser) {
      user = params.req.currentUser;
    } else if (params.user) {
      user = params.user;
    } else {
      return;
    }

    // See https://segment.com/docs/connections/spec/identify/
    if (user.id) params.userId = user.id;

    params.properties.user = { id: user.id };
    if (user.firstName) params.properties.user.firstName = user.firstName;
    if (user.lastName) params.properties.user.lastName = user.lastName;
    if (user.email) params.properties.user.email = user.email;

    // Plan / Subscription
    // params.subscription = account.subscription;
    // params.isTrial = account.isTrial;
    if (user.paid) params.properties.isPaid = user.paid;

    // Account / Site Details
    if (user.activeAccountMemberId) params.properties.activeAccountMemberId = user.activeAccountMemberId;
    if (user.roles) params.properties.roles = user.roles;

    params.properties.account = {};
    if (user.accountId) params.properties.account.id = user.accountId;
    if (user.accountName) params.properties.account.name = user.accountName;
    if (user.account) {
      if (user.account.id) params.properties.account.id = user.account.id;
      if (user.account.orgName) params.properties.account.name = user.account.orgName;
    }

    if (user.siteId) {
      params.properties.site = {
        id: user.siteId,
        name: user.siteName,
      };
    }

    if (user.subscription) {
      params.properties.subscription = user.subscription;
    }
  }

  /**
   * Returns default attributes for key objects, such as Parts, Drawings, Characteristic
   *
   * @private
   * @param {object} params - The params passed to track or page
   */
  generateObjectMetadata(params) {
    let properties = params.properties || {};
    let { drawing, part } = params;
    const { characteristic, subscription, user, config, reviewCycle, marker, preset } = params;

    // TODO: Standardize unpacking of common objects
    if (params.error) {
      let { error, graphql = false } = params;

      if (error.originalError) {
        error = error.originalError;
        graphql = true;
      }

      properties = {
        ...properties,
        'error.fromGraphql': graphql,
        'error.message': error.message,
        'error.name': error.name,
        'error.fileName': error.fileName,
        'error.lineNumber': error.lineNumber,
        'error.columnNumber': error.columnNumber,
        'error.stack': error?.stack,
        'error.string': error.toString(),
      };

      if (error.details) properties['error.details'] = error.details;
      if (error.errors) properties['error.errors'] = error.errors;
    }

    // TODO: Standardize unpacking of common objects
    if (characteristic) {
      properties = {
        ...properties,
        'char.id': characteristic.id,
        'char.fullType': `${characteristic.notationType} : ${characteristic.notationSubtype}`,
        'char.type': characteristic.notationType,
        'char.subtype': characteristic.notationSubtype,
        'char.fullSpecification': characteristic.fullSpecification,
        'char.tolerances': characteristic.toleranceSource,
        'drawing.sheet': characteristic.sheet,
      };
      drawing = characteristic.drawing;
    }

    if (drawing) {
      properties = {
        ...properties,
        'drawing.id': drawing.id,
        'drawing.name': drawing.name,
        'drawing.revision': drawing.revision,
      };
      part = drawing.part;
    }

    if (part) {
      properties = {
        ...properties,
        'part.id': part.id,
        'part.number': part.number,
        'part.revision': part.revision,
        'part.accessControl': part.accessControl,
      };
    }

    if (config) {
      properties = {
        ...properties,
        'config.list': config.listType,
        'config.entry.id': config.id,
        'config.entry.name': config.name,
        'config.entry.metadata': config.meta,
        'config.entry.status': config.status,
      };
    }

    // if (user) {
    //   properties = {
    //     ...properties,
    //     'currentUser.id': user.currentUser.id,
    //     'targetUser.id': user.targetUser.id,
    //     'targetUser.roles': user.targetUser.roles,
    //     'targetUser.accountMember.status': user.targetUser.status,
    //   };
    // }

    if (subscription) {
      properties = {
        ...properties,
        'ixc.subscription.id': subscription.paymentSystemId,
        'chargify.subscription.id': subscription.providerProductId,
        'ixc.customer.id': subscription.accountId,
        'ixc.billing.id': subscription.billingId,
        'chargify.customer.id': subscription.customerId,
        'chargify.product.id': subscription.accessControl,
        'renewal.date': subscription.renewalDate,
      };
    }

    if (reviewCycle) {
      properties = {
        ...properties,
        'ixc.review.event': reviewCycle.event,
        'ixc.review.elapsed': reviewCycle.elapsed,
        'ixc.review.capture': reviewCycle.capture,
        'ixc.review.correction': reviewCycle.correction,
        'ixc.review.confidence': reviewCycle.confidence,
        'ixc.review.issue': reviewCycle.issue,
        'ixc.review.userId': reviewCycle.user.id,
        'ixc.review.userEmail': reviewCycle.user.email,
      };
    }

    if (marker) {
      properties = {
        ...properties,
        'marker.id': marker.id,
        'marker.shape': marker.shape,
        'marker.fillColor': marker.fillColor,
        'marker.borderColor': marker.borderColor,
        'marker.borderStyle': marker.borderStyle,
        'marker.fontColor': marker.fontColor,
      };
    }

    if (preset) {
      properties = {
        ...properties,
        'preset.name': preset.name,
        'preset.customers': preset.customers,
        'preset.styles': preset.assignedStyles,
      };
    }

    params.properties = properties;
    return params;
  }

  /**
   * Removes custom params so they cannot interfere with the SDK method execution.
   * The SDK call will error if for example the `req` param is included because
   * `JSON.parse(req)` fails.
   *
   * @private
   * @param {Object} params
   */
  deleteCustomParams(params) {
    delete params.req;
    delete params.ixcEvent;
  }
};
