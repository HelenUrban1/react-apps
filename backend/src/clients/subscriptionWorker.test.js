// const dotenv = require('dotenv');
// dotenv.config({ path: '../../.env' });

const moment = require('moment');
const makeCrmClient = require('./crmClient').default;
const makeBillingClient = require('./billingClient').default;
const SubscriptionWorker = require('./subscriptionWorker');
const UserRepository = require('../database/repositories/userRepository');
const { MOCK_PRODUCT_CATALOG, MOCK_PRODUCT_PRICES, MOCK_SUBSCRIPTION } = require('./serviceAdapters/fixtures/chargify.fixtures.js');

const USE_MOCKS = true;

describe('SubscriptionWorker', () => {
  const mockS3 = {
    getObject: jest.fn(),
    upload: jest.fn(),
  };

  const loggerMock = {
    trace: jest.fn(),
    debug: jest.fn(),
    log: jest.fn(),
    info: jest.fn(),
    warn: jest.fn(),
    error: jest.fn(),
  };

  const crmClientMock = {
    getMetadata: jest.fn(),
    // Companies
    getCompanyById: jest.fn(),
    getCompanyByDomain: jest.fn(),
    getCompanyByAccountId: jest.fn(),
    createCompany: jest.fn(),
    updateCompany: jest.fn(),
    createOrUpdateCompany: jest.fn(),
    addContactToCompany: jest.fn(),
    // Contacts
    getContactById: jest.fn(),
    getContactByEmail: jest.fn(),
    createOrUpdateContact: jest.fn(),
    updateContact: jest.fn(),
    updateContacts: jest.fn(),
    // Deals
    getDealById: jest.fn(),
    searchDeals: jest.fn(),
    createDeal: jest.fn(),
    updateDeal: jest.fn(),
    addContactToDeal: jest.fn(),
    addCompanyToDeal: jest.fn(),
    removeContactFromDeal: jest.fn(),
    removeContactFromCompany: jest.fn(),
    removeCompanyFromDeal: jest.fn(),
    // Associations
    // getAssociations
    getChildCompanyIds: jest.fn(),
    getParentCompanyIds: jest.fn(),
    getCompanysAssociatedContactIds: jest.fn(),
    getCompanysAssociatedDealIds: jest.fn(),
    // Timeline Event Templates
    buildTimelineEventReference: jest.fn(),
    loadLocalTimelineEventTypes: jest.fn(),
    extendTimelineEventReference: jest.fn(),
    timelineEventsAreDifferent: jest.fn(),
    listTimelineEventTypes: jest.fn(),
    createTimelineEventType: jest.fn(),
    updateTimelineEventType: jest.fn(),
    // Timeline Events
    ensureAccessToken: jest.fn(),
    getTimelineEventType: jest.fn(),
    createTimelineEvent: jest.fn(),
    createTimelineEvents: jest.fn(),
    getTimelineEvent: jest.fn(),
    recordEvent: jest.fn(),
    buildTimelineEventPayload: jest.fn(),
    // Helper Functions
    dateInCrmFormat: jest.fn(),
  };

  const billingClientMock = {
    adapter: { name: 'chargify', isMocked: true },
    trace: jest.fn(),
    getMetadata: jest.fn(),
    getVendorUrlForCustomerWithId: jest.fn(),
    getVendorUrlForSubscriptionWithId: jest.fn(),
    getInitData: jest.fn(),
    // Customers
    createOrUpdateCustomer: jest.fn(),
    createCustomer: jest.fn(),
    getCustomerById: jest.fn(),
    getCustomerByReference: jest.fn(),
    getMetadataForCustomerWithId: jest.fn(),
    findCustomer: jest.fn(),
    updateCustomer: jest.fn(),
    // Product Catalog
    getCatalog: jest.fn(),
    getProductFamilies: jest.fn(),
    getProducts: jest.fn(),
    getProductPrices: jest.fn(),
    getProductComponents: jest.fn(),
    getComponent: jest.fn(),
    getComponentPrices: jest.fn(),
    getSubscriptionComponents: jest.fn(),
    deleteSubscriptionCustomFields: jest.fn(),
    getCurrentPrice: jest.fn(),
    // Subscriptions
    createSubscription: jest.fn(),
    getSubscriptionById: jest.fn(),
    getSubscriptionByReference: jest.fn(),
    getSubscriptionsForCustomer: jest.fn(),
    updateSubscription: jest.fn(),
    updateSubscriptionQuantity: jest.fn(),
    updateComponentPrice: jest.fn(),
    delayedCancelSubscription: jest.fn(),
    immediatelyCancelSubscription: jest.fn(),
    removeDelayedCancel: jest.fn(),
    reactivateSubscription: jest.fn(),
    getBillingPortalLink: jest.fn(),
    // Custom Fields
    getSubscriptionCustomFields: jest.fn(),
    updateSubscriptionCustomFields: jest.fn(),
    // Payment Methods
    addPaymentMethod: jest.fn(),
    changeDefaultPaymentMethod: jest.fn(),
    chargeSubscriptionPrePayment: jest.fn(),
    // Payment History
    getInvoices: jest.fn(),
    getInvoicesForSubscription: jest.fn(),
    downloadInvoice: jest.fn(),
    issueInvoice: jest.fn(),
    payInvoice: jest.fn(),
    voidInvoice: jest.fn(),
    hasActivePaymentMethod: jest.fn(),
  };

  // Data submitted by form on SignupPage.jsx to doRegisterAccountAndUser
  const signupPageFormData = {
    user: {
      id: 'e0a01779-c8ed-4090-bd44-9c1b2242fb75',
      firstName: 'Dev',
      lastName: 'Monkey',
      fullName: null,
      email: 'dev.mail.monkey@gmail.com',
      emailVerified: true,
      profile: {
        phoneNumber: '123-456-7890',
      },
      phoneNumber: '123-456-7890',
      avatars: null,
      paid: true,
      accountId: 'e0a01769-c8ed-4090-bd24-9c1b2241fb75',
      accountName: 'Acme Corp',
      activeAccountMemberId: '007516cc-3168-40e4-8ad3-d42c92f66caa',
      siteId: '31ef1d56-1b86-4ffa-b886-507ed8f021c5',
      siteName: null,
      roles: ['Owner'],
      accountMemberships: [
        {
          id: '007516cc-3168-40e4-8ad3-d42c92f66caa',
          accountId: 'e0a01769-c8ed-4090-bd24-9c1b2241fb75',
          roles: ['Owner'],
          status: 'Active',
        },
      ],
      authenticationUid: 'c7bf4eca-cd00-4d65-81a8-5d4da86ea824',
    },
    account: {
      orgName: 'Acme Corp',
      orgPhone: '123-456-7890',
      orgEmail: 'dev.mail.monkey@gmail.com',
      orgStreet: '1 Glendwood Ave',
      orgStreet2: 'WeWork Floor 5',
      orgCity: 'Raleigh',
      orgRegion: 'NC',
      orgPostalcode: '27603',
      orgCountry: 'US',
      // Added by doRegisterAccountAndUser before calling AccountService.create
      status: 'Active',
      settings: '{}',
      dbHost: '',
      dbName: '',
      // Available only after doRegisterAccountAndUser
      id: 'e0a01769-c8ed-4090-bd24-9c1b2241fb75',
    },
    // Created by doRegisterAccountAndUser after AccountService.create
    site: {
      id: '1769e0a0-4090-c8ed-bd24-41fb759c1b22',
      accountId: 'e0a01769-c8ed-4090-bd24-9c1b2241fb75', // newAccount.id,
      status: 'Active',
      settings: '{}',
      siteName: 'Acme Corp', // account.orgName,
      sitePhone: '123-456-7890', // account.orgPhone,
      siteEmail: 'dev.mail.monkey@gmail.com', // account.orgEmail,
      siteStreet: '1 Glendwood Ave', // account.orgStreet,
      siteStreet2: 'WeWork Floor 5', // account.orgStreet2,
      siteCity: 'Raleigh', // account.orgCity,
      siteRegion: 'NC', // account.orgRegion,
      sitePostalcode: '27603', // account.orgPostalcode,
      siteCountry: 'US', // account.orgCountry,
    },
  };

  // const currentUserMock = {
  //   id: 'e0a01779-c8ed-4090-bd44-9c1b2242fb75',
  //   firstName: 'Dev',
  //   lastName: 'Monkey',
  //   fullName: null,
  //   email: 'dev.mail.monkey@gmail.com',
  //   emailVerified: true,
  //   phoneNumber: '123-456-7890',
  //   avatars: null,
  //   paid: true,
  //   accountId: 'e0a01769-c8ed-4090-bd24-9c1b2241fb75',
  //   accountName: 'Acme Corp',
  //   activeAccountMemberId: '007516cc-3168-40e4-8ad3-d42c92f66caa',
  //   siteId: '31ef1d56-1b86-4ffa-b886-507ed8f021c5',
  //   siteName: null,
  //   roles: ['Owner'],
  //   accountMemberships: [
  //     {
  //       id: '007516cc-3168-40e4-8ad3-d42c92f66caa',
  //       accountId: 'e0a01769-c8ed-4090-bd24-9c1b2241fb75',
  //       roles: ['Owner'],
  //       status: 'Active',
  //     },
  //   ],
  //   authenticationUid: 'c7bf4eca-cd00-4d65-81a8-5d4da86ea824',
  // };

  let subWorker;

  beforeAll(async () => {
    if (USE_MOCKS) {
      // Use the mock APIs
      console.log('Using mock objects during subscriptionWorker test');
      subWorker = new SubscriptionWorker({
        s3: mockS3,
        crmClient: crmClientMock,
        billingClient: billingClientMock,
        logger: loggerMock,
      });
    } else {
      // console.log('Using REAL APIs during subscriptionWorker test');
      // Create Real CRM Connection
      console.log('subscriptionWorker.test');
      const crmClient = await makeCrmClient({
        privateAccessToken: process.env.IXC_HUBSPOT_PRIVATE_ACCESS_TOKEN,
        logger: loggerMock,
      });

      // Create Real Billing Connection
      const billingClient = await makeBillingClient({
        useAdapter: 'chargify',
        siteName: 'inspectionxpert-test',
        publicKey: process.env.IXC_CHARGIFY_PUBLIC_KEY,
        privateKey: process.env.IXC_CHARGIFY_PRIVATE_KEY,
        apiKey: process.env.IXC_CHARGIFY_API_KEY,
        logger: loggerMock,
      });

      // Use the actual APIs
      subWorker = new SubscriptionWorker({
        crmClient,
        billingClient,
        logger: loggerMock,
      });
    }
  });

  beforeEach(() => {
    jest.resetAllMocks();
  });

  describe('Initialization', () => {
    it('should throw an error if required input is not provided', async () => {
      expect(() => {
        const sw = new SubscriptionWorker({ logger: loggerMock });
      }).toThrow();
      expect(loggerMock.error).toHaveBeenCalledTimes(1);
    });

    it('should populate the result properly if given a valid adapter', async () => {
      const sw = new SubscriptionWorker({
        s3: mockS3,
        crmClient: crmClientMock,
        billingClient: billingClientMock,
        logger: loggerMock,
      });
      // Adapter metadata
      expect(sw.crmClient).toBeDefined();
      expect(sw.billingClient).toBeDefined();
      expect(sw.isMocked).toBeDefined();
      // Functions
      // expect(subWorker.getCompanyById).toBeDefined();
    });
  });

  describe('Catalog Management', () => {
    describe('#inspectCatalog()', () => {
      it('should return a string that summarizes the loaded catalog when the request succeeds', async () => {
        billingClientMock.getCatalog.mockResolvedValueOnce(MOCK_PRODUCT_CATALOG);
        let metadata = subWorker.inspectCatalog();
        expect(metadata).toBe('catalog_source=chargify&product_family_id=1526431&product_family_handle=inspectionxpert-cloud');
        await subWorker.loadCatalog();
        metadata = subWorker.inspectCatalog();
        expect(metadata).toBe('catalog_source=chargify&product_family_id=1526431&product_family_handle=inspectionxpert-cloud&product_family_name=inspectionxpert-cloud&product_ids=ix_standard');
      });
    });

    describe('#loadCatalog()', () => {
      it('should save the catalog in memory when the request succeeds', async () => {
        billingClientMock.getCatalog.mockResolvedValueOnce(MOCK_PRODUCT_CATALOG);
        const result = await subWorker.loadCatalog();
        expect(result).toMatchObject(MOCK_PRODUCT_CATALOG);
        expect(subWorker.catalog).toMatchObject(MOCK_PRODUCT_CATALOG);
      });

      it('should save the catalog in memory and to S3 when updateCache is true and the request succeeds', async () => {
        billingClientMock.getCatalog.mockResolvedValueOnce(MOCK_PRODUCT_CATALOG);
        const s3Response = {
          ETag: '"6805f2cfc46c0f04559748bb039d69ae"',
          ServerSideEncryption: 'AES256',
          VersionId: 'Ri.vC6qVlA4dEnjgRV4ZHsHoFIjqEMNt',
        };
        mockS3.upload.mockReturnValueOnce({
          promise: () => {
            return Promise.resolve(s3Response);
          },
        });
        const result = await subWorker.loadCatalog({ updateCache: true });
        expect(result).toMatchObject(MOCK_PRODUCT_CATALOG);
        expect(subWorker.catalog).toMatchObject(MOCK_PRODUCT_CATALOG);
        expect(mockS3.upload).toHaveBeenCalledTimes(1);
      });
    });

    describe('#fetchCatalog()', () => {
      it('should return a catalog from S3 when useCache=true', async () => {
        // Native AWS mock
        // mockS3.getObject.mockReturnValueOnce({
        //   promise: () => {
        //     return Promise.resolve({ Body: JSON.stringify(MOCK_PRODUCT_CATALOG) });
        //   },
        // });
        // AWS-Wrapper mock
        mockS3.getObject.mockReturnValueOnce(
          // TODO: Update aws-wrapper, so that mocking it is identical to mocking production AWS
          Promise.resolve({ Body: JSON.stringify(MOCK_PRODUCT_CATALOG) })
        );
        const result = await subWorker.fetchCatalog({ useCache: true });
        expect(result).toMatchObject(MOCK_PRODUCT_CATALOG);
      });

      it('should return a catalog when useCache=false and the billingClient can reach the vendor', async () => {
        billingClientMock.getCatalog.mockResolvedValueOnce(MOCK_PRODUCT_CATALOG);
        const result = await subWorker.fetchCatalog({ useCache: false });
        expect(result).toMatchObject(MOCK_PRODUCT_CATALOG);
      });

      it('should return a catalog from S3 when useCache=false and the billingClient is unable to reach the vendor', async () => {
        billingClientMock.getCatalog.mockRejectedValueOnce(new Error('An error message.'));
        // Native AWS mock
        // mockS3.getObject.mockReturnValueOnce({
        //   promise: () => {
        //     return Promise.resolve({ Body: JSON.stringify(MOCK_PRODUCT_CATALOG) });
        //   },
        // });
        // AWS-Wrapper mock
        mockS3.getObject.mockReturnValueOnce(Promise.resolve({ Body: JSON.stringify(MOCK_PRODUCT_CATALOG) }));
        const result = await subWorker.fetchCatalog({ useCache: false });
        expect(result).toMatchObject(MOCK_PRODUCT_CATALOG);
      });

      // BUG: This test is firing (node:300) UnhandledPromiseRejectionWarning
      it('should throw an error when the billingClient is unable to reach the vendor and S3 does not respond', async () => {
        billingClientMock.getCatalog.mockRejectedValueOnce(new Error('An error message.'));
        mockS3.getObject.mockRejectedValueOnce(new Error('An error message.'));
        await expect(subWorker.fetchCatalog({ useCache: false })).rejects.toThrow();
        expect(loggerMock.error).toHaveBeenCalledTimes(2);
      });
    });

    describe('#cacheCatalog()', () => {
      it('should save the catalog object to S3', async () => {
        const s3Response = {
          ETag: '"6805f2cfc46c0f04559748bb039d69ae"',
          ServerSideEncryption: 'AES256',
          VersionId: 'Ri.vC6qVlA4dEnjgRV4ZHsHoFIjqEMNt',
        };
        // Native AWS mock
        // mockS3.upload.mockReturnValueOnce({
        //   promise: () => {
        //     return Promise.resolve(s3Response);
        //   },
        // });
        // AWS-Wrapper mock
        mockS3.upload.mockReturnValueOnce(Promise.resolve(s3Response));
        const result = await subWorker.cacheCatalog({ catalog: MOCK_PRODUCT_CATALOG });
        expect(result).toMatchObject(s3Response);
      });
    });

    describe('#loadCatalogFromCache()', () => {
      it('should load the catalog object to S3', async () => {
        // Native AWS mock
        // mockS3.getObject.mockReturnValueOnce({
        //   promise: () => {
        //     return Promise.resolve({ Body: JSON.stringify(MOCK_PRODUCT_CATALOG) });
        //   },
        // });
        // AWS-Wrapper mock
        mockS3.getObject.mockReturnValueOnce(Promise.resolve({ Body: JSON.stringify(MOCK_PRODUCT_CATALOG) }));
        const result = await subWorker.loadCatalogFromCache();
        expect(result).toMatchObject(MOCK_PRODUCT_CATALOG);
      });
    });
  });

  describe('Freemium Trial', () => {
    describe('#createFreemium()', () => {
      it('should throw an error if required input is not provided', async () => {
        await expect(subWorker.createFreemium({})).rejects.toThrow();
        expect(loggerMock.error).toHaveBeenCalledTimes(1);
      });

      it('should throw an error if the request fails', async () => {
        crmClientMock.createOrUpdateCompany.mockRejectedValueOnce(new Error('An error message.'));
        await expect(
          subWorker.createFreemium({
            // reference: 'some-reference',
            name: 'Foo Company',
            domain: 'foo.com',
          })
        ).rejects.toThrow();
        expect(loggerMock.error).toHaveBeenCalledTimes(1);
      });

      it('should create a company, contact, and deal in CRM and a customer and subscription in billing system when the request succeeds', async () => {
        crmClientMock.createOrUpdateCompany.mockResolvedValueOnce({
          crm: 'hubspot',
          crmType: 'company',
          crmId: 4721081532,
          ix_chargify_id: '37711536',
          hs_lastmodifieddate: '1603837461096',
          hs_object_id: '4721081532',
          createdate: '1603837457283',
          ix_chargify_link: 'https://inspectionxpert-test.chargify.com/customers/37711536',
        });

        crmClientMock.createOrUpdateContact.mockResolvedValueOnce({
          hs_is_unworked: 'true',
          website: 'http://gmail.com',
          firstname: 'Dev',
          city: 'Raleigh',
          lastmodifieddate: '1603837457616',
          num_unique_conversion_events: '0',
          createdate: '1603837457485',
          hs_lifecyclestage_subscriber_date: '1603837457485',
          hs_all_contact_vids: '1951',
          lastname: 'Monkey',
          hs_marketable_until_renewal: 'false',
          hs_is_contact: 'true',
          num_conversion_events: '0',
          hs_object_id: '1951',
          hs_email_domain: 'gmail.com',
          company: 'Acme Corp',
          state: 'NC',
          lifecyclestage: 'subscriber',
          email: 'dev.mail.monkey@gmail.com',
          ixc_user_id: 'e0a01779-c8ed-4090-bd44-9c1b2242fb75',
          crm: 'hubspot',
          crmType: 'contact',
          crmId: 1951,
        });

        crmClientMock.addContactToCompany.mockResolvedValueOnce({ success: true });

        crmClientMock.createDeal.mockResolvedValueOnce({
          hs_closed_amount_in_home_currency: '0',
          dealname: 'Acme Corp : Dev Monkey',
          amount: '',
          num_associated_contacts: '1',
          createdate: '1603837458220',
          amount_in_home_currency: '',
          hs_is_closed: 'false',
          ixc_account_id: 'e0a01769-c8ed-4090-bd24-9c1b2241fb75',
          days_to_close: '0',
          hs_deal_stage_probability: '0.200000000000000011102230246251565404236316680908203125',
          ix_chargify_link: 'https://inspectionxpert-test.chargify.com/subscriptions/36827260',
          pipeline: '6510422',
          ix_chargify_id: '36827260',
          hs_closed_amount: '0',
          hs_analytics_source: 'OFFLINE',
          hs_lastmodifieddate: '1603837461271',
          dealstage: '6510423',
          trial_type: 'Freemium',
          hs_createdate: '1603837458220',
          hs_object_id: '3252998005',
          hs_projected_amount: '',
          hs_analytics_source_data_1: 'API',
          hs_projected_amount_in_home_currency: '',
          dealtype: 'newbusiness',
          crm: 'hubspot',
          crmType: 'deal',
          crmId: 3252998005,
        });

        crmClientMock.addContactToDeal.mockResolvedValueOnce({ success: true });

        crmClientMock.addCompanyToDeal.mockResolvedValueOnce({ success: true });

        billingClientMock.createOrUpdateCustomer.mockResolvedValueOnce({
          first_name: 'Dev',
          last_name: 'Monkey',
          email: 'dev.mail.monkey@gmail.com',
          cc_emails: null,
          organization: null,
          reference: 'e0a01769-c8ed-4090-bd24-9c1b2241fb75',
          id: 37711536,
          created_at: '2020-10-27T18:24:19-04:00',
          updated_at: '2020-10-27T18:24:19-04:00',
          address: null,
          address_2: null,
          city: null,
          state: null,
          zip: null,
          country: null,
          phone: '',
          verified: false,
          portal_customer_created_at: null,
          portal_invite_last_sent_at: null,
          portal_invite_last_accepted_at: null,
          tax_exempt: false,
          vat_number: null,
          parent_id: null,
          locale: null,
          default_subscription_group_uid: null,
        });

        billingClientMock.getCurrentPrice.mockResolvedValueOnce(MOCK_PRODUCT_PRICES[0]);

        billingClientMock.createSubscription.mockResolvedValueOnce({
          id: 36827260,
          state: 'active',
          trial_started_at: null,
          trial_ended_at: null,
          activated_at: '2020-10-27T18:24:20-04:00',
          created_at: '2020-10-27T18:24:19-04:00',
          updated_at: '2020-10-27T18:24:20-04:00',
          expires_at: null,
          balance_in_cents: 0,
          current_period_ends_at: '2020-11-27T17:24:19-05:00',
          next_assessment_at: '2020-11-27T17:24:19-05:00',
          canceled_at: null,
          cancellation_message: null,
          next_product_id: null,
          next_product_handle: null,
          cancel_at_end_of_period: false,
          payment_collection_method: 'remittance',
          snap_day: null,
          cancellation_method: null,
          current_period_started_at: '2020-10-27T18:24:19-04:00',
          previous_state: 'active',
          signup_payment_id: 433830000,
          signup_revenue: '0.00',
          delayed_cancel_at: null,
          coupon_code: null,
          total_revenue_in_cents: 0,
          product_price_in_cents: 0,
          product_version_number: 1,
          payment_type: null,
          referral_code: null,
          coupon_use_count: null,
          coupon_uses_allowed: null,
          reason_code: null,
          automatically_resume_at: null,
          coupon_codes: [],
          offer_id: null,
          payer_id: 37711536,
          receives_invoice_emails: null,
          product_price_point_id: 969460,
          next_product_price_point_id: null,
          credit_balance_in_cents: 0,
          prepayment_balance_in_cents: 0,
          net_terms: null,
          stored_credential_transaction_id: null,
          locale: null,
          reference: '3252998005',
          currency: 'USD',
          on_hold_at: null,
          scheduled_cancellation_at: null,
          customer: {
            id: 37711536,
            first_name: 'Dev',
            last_name: 'Monkey',
            organization: null,
            email: 'dev.mail.monkey@gmail.com',
            created_at: '2020-10-27T18:24:19-04:00',
            updated_at: '2020-10-27T18:24:19-04:00',
            reference: 'e0a01769-c8ed-4090-bd24-9c1b2241fb75',
            address: null,
            address_2: null,
            city: null,
            state: null,
            zip: null,
            country: null,
            phone: '',
            portal_invite_last_sent_at: null,
            portal_invite_last_accepted_at: null,
            verified: false,
            portal_customer_created_at: null,
            vat_number: null,
            cc_emails: null,
            tax_exempt: false,
            parent_id: null,
            locale: null,
          },
          product: {
            id: 5233891,
            name: 'Standard',
            handle: 'ix_standard',
            description: '',
            accounting_code: '',
            request_credit_card: true,
            expiration_interval: null,
            expiration_interval_unit: 'never',
            created_at: '2020-05-21T14:21:34-04:00',
            updated_at: '2020-10-24T15:08:33-04:00',
            price_in_cents: 0,
            interval: 1,
            interval_unit: 'month',
            initial_charge_in_cents: null,
            trial_price_in_cents: null,
            trial_interval: null,
            trial_interval_unit: 'month',
            archived_at: null,
            require_credit_card: false,
            return_params: '',
            taxable: true,
            update_return_url: '',
            tax_code: 'D0000000',
            initial_charge_after_trial: false,
            version_number: 1,
            update_return_params: '',
            default_product_price_point_id: 969460,
            request_billing_address: false,
            require_billing_address: false,
            require_shipping_address: false,
            product_price_point_id: 969460,
            product_price_point_name: 'Standard',
            product_price_point_handle: 'ix_standard',
            product_family: { handle: 'inspectionxpert-cloud' },
            public_signup_pages: [],
          },
          group: null,
        });

        billingClientMock.getCurrentPrice.mockResolvedValueOnce({
          id: 1067356,
          handle: 'v10102020_annually',
        });

        billingClientMock.updateSubscriptionQuantity.mockResolvedValueOnce({
          allocation_id: 589424613,
          component_id: 1108426,
          subscription_id: 36827260,
          quantity: 1,
          previous_quantity: 0,
          memo: 'Initial seat component for new trial',
          timestamp: '2020-10-27T22:59:51Z',
          proration_upgrade_scheme: 'prorate-attempt-capture',
          proration_downgrade_scheme: 'no-prorate',
          price_point_id: 1067356,
          previous_price_point_id: 1067356,
          component_handle: 'paid_user_seat_pu',
          accrue_charge: false,
          upgrade_charge: 'prorated',
          downgrade_credit: 'none',
          created_at: '2020-10-27T18:59:51-04:00',
          payment: null,
        });

        billingClientMock.updateComponentPrice.mockResolvedValueOnce({
          allocation_id: 589424613,
          component_id: 1108426,
          subscription_id: 36827260,
          quantity: 1,
          previous_quantity: 0,
          memo: 'Initial seat component for new trial',
          timestamp: '2020-10-27T22:59:51Z',
          proration_upgrade_scheme: 'prorate-attempt-capture',
          proration_downgrade_scheme: 'no-prorate',
          price_point_id: 1067356,
          previous_price_point_id: 1067356,
          component_handle: 'paid_user_seat_pu',
          accrue_charge: false,
          upgrade_charge: 'prorated',
          downgrade_credit: 'none',
          created_at: '2020-10-27T18:59:51-04:00',
          payment: null,
        });

        crmClientMock.updateCompany.mockResolvedValueOnce({
          ix_chargify_id: '37451677',
          hs_lastmodifieddate: '1602731075555',
          hs_object_id: '4634689283',
          createdate: '1602731070793',
          ix_chargify_link: 'https://inspectionxpert-test.chargify.com/customers/37451677',
          crm: 'hubspot',
          crmType: 'company',
          crmId: 4634689283,
        });

        crmClientMock.updateDeal.mockResolvedValueOnce({
          hs_closed_amount_in_home_currency: '0',
          dealname: 'Acme Corp : Dev Monkey',
          amount: '',
          num_associated_contacts: '1',
          createdate: '1603837458220',
          amount_in_home_currency: '',
          hs_is_closed: 'false',
          ixc_account_id: 'e0a01769-c8ed-4090-bd24-9c1b2241fb75',
          days_to_close: '0',
          hs_deal_stage_probability: '0.200000000000000011102230246251565404236316680908203125',
          ix_chargify_link: 'https://inspectionxpert-test.chargify.com/subscriptions/36827260',
          pipeline: '6510422',
          ix_chargify_id: '36827260',
          hs_closed_amount: '0',
          hs_analytics_source: 'OFFLINE',
          hs_lastmodifieddate: '1603837461271',
          dealstage: '6510423',
          trial_type: 'Freemium',
          hs_createdate: '1603837458220',
          hs_object_id: '3252998005',
          hs_projected_amount: '',
          hs_analytics_source_data_1: 'API',
          hs_projected_amount_in_home_currency: '',
          dealtype: 'newbusiness',
          crm: 'hubspot',
          crmType: 'deal',
          crmId: 3252998005,
        });

        const result = await subWorker.createFreemium(signupPageFormData);
        // console.log('createFreemium result', result);

        expect(result).toBeDefined();
        expect(result.success).toBe(true);

        expect(result.crmCompany).toBeDefined();
        expect(result.crmCompany.ix_chargify_id).toBe('37451677');

        expect(result.crmContact).toBeDefined();
        expect(result.crmContact.email).toBe('dev.mail.monkey@gmail.com');
        expect(result.crmContact.ixc_user_id).toBe('e0a01779-c8ed-4090-bd44-9c1b2242fb75');

        // Make sure the subscription is a freemium trial
        expect(result.bsSubscription).toBeDefined();
        expect(result.bsSubscription.state).toBe('active');
        expect(result.bsSubscription.created_at).toBeDefined();
        expect(result.bsSubscription.trial_started_at).toBe(null);
        expect(result.bsSubscription.trial_ended_at).toBe(null);
        expect(result.bsSubscription.activated_at).toBe('2020-10-27T18:24:20-04:00');
        expect(result.bsSubscription.expires_at).toBe(null);

        expect(loggerMock.error).toHaveBeenCalledTimes(0);
      }, 30000); // Wait 30 seconds for asyn timeouts
    });

    describe('#updateFreemiumQuantity()', () => {
      it('should throw an error if required input is not provided', async () => {
        await expect(subWorker.updateFreemiumQuantity({})).rejects.toThrow();
        expect(loggerMock.error).toHaveBeenCalledTimes(1);
      });

      it('should throw an error if the request fails', async () => {
        crmClientMock.updateDeal.mockRejectedValueOnce(new Error('An error message.'));
        await expect(
          subWorker.updateFreemiumQuantity({
            name: 'Foo Company',
            domain: 'foo.com',
          })
        ).rejects.toThrow();
        expect(loggerMock.error).toHaveBeenCalledTimes(1);
      });

      it('should update the billing system subscription and CRM deal when the request succeeds', async () => {
        billingClientMock.getCustomerByReference.mockResolvedValueOnce({
          first_name: 'Dev',
          last_name: 'Monkey',
          email: 'dev.mail.monkey@gmail.com',
          cc_emails: null,
          organization: null,
          reference: 'e0a01769-c8ed-4090-bd24-9c1b2241fb75',
          id: 37711536,
          created_at: '2020-10-27T18:24:19-04:00',
          updated_at: '2020-10-27T18:24:19-04:00',
          address: null,
          address_2: null,
          city: null,
          state: null,
          zip: null,
          country: null,
          phone: '',
          verified: false,
          portal_customer_created_at: null,
          portal_invite_last_sent_at: null,
          portal_invite_last_accepted_at: null,
          tax_exempt: false,
          vat_number: null,
          parent_id: null,
          locale: null,
          default_subscription_group_uid: null,
        });

        billingClientMock.getSubscriptionsForCustomer.mockResolvedValueOnce([
          {
            id: 36827260,
            state: 'active',
            trial_started_at: null,
            trial_ended_at: null,
            activated_at: '2020-10-27T18:24:20-04:00',
            created_at: '2020-10-27T18:24:19-04:00',
            updated_at: '2020-10-27T18:59:09-04:00',
            expires_at: null,
            balance_in_cents: 24991,
            current_period_ends_at: '2020-11-27T17:24:19-05:00',
            next_assessment_at: '2020-11-27T17:24:19-05:00',
            canceled_at: null,
            cancellation_message: null,
            next_product_id: null,
            next_product_handle: null,
            cancel_at_end_of_period: false,
            payment_collection_method: 'remittance',
            snap_day: null,
            cancellation_method: null,
            current_period_started_at: '2020-10-27T18:24:19-04:00',
            previous_state: 'active',
            signup_payment_id: 433830000,
            signup_revenue: '0.00',
            delayed_cancel_at: null,
            coupon_code: null,
            total_revenue_in_cents: 0,
            product_price_in_cents: 0,
            product_version_number: 1,
            payment_type: null,
            referral_code: null,
            coupon_use_count: null,
            coupon_uses_allowed: null,
            reason_code: null,
            automatically_resume_at: null,
            coupon_codes: [],
            offer_id: null,
            payer_id: 37711536,
            receives_invoice_emails: null,
            product_price_point_id: 969460,
            next_product_price_point_id: null,
            credit_balance_in_cents: 0,
            prepayment_balance_in_cents: 0,
            net_terms: null,
            stored_credential_transaction_id: null,
            locale: null,
            reference: '3252998005',
            currency: 'USD',
            on_hold_at: null,
            scheduled_cancellation_at: null,
            customer: {
              id: 37711536,
              first_name: 'Dev',
              last_name: 'Monkey',
              organization: null,
              email: 'dev.mail.monkey@gmail.com',
              created_at: '2020-10-27T18:24:19-04:00',
              updated_at: '2020-10-27T18:24:19-04:00',
              reference: 'e0a01769-c8ed-4090-bd24-9c1b2241fb75',
              address: null,
              address_2: null,
              city: null,
              state: null,
              zip: null,
              country: null,
              phone: '',
              portal_invite_last_sent_at: null,
              portal_invite_last_accepted_at: null,
              verified: false,
              portal_customer_created_at: null,
              vat_number: null,
              cc_emails: null,
              tax_exempt: false,
              parent_id: null,
              locale: null,
            },
            product: {
              id: 5233891,
              name: 'Standard',
              handle: 'ix_standard',
              description: '',
              accounting_code: '',
              request_credit_card: true,
              expiration_interval: null,
              expiration_interval_unit: 'never',
              created_at: '2020-05-21T14:21:34-04:00',
              updated_at: '2020-10-24T15:08:33-04:00',
              price_in_cents: 0,
              interval: 1,
              interval_unit: 'month',
              initial_charge_in_cents: null,
              trial_price_in_cents: null,
              trial_interval: null,
              trial_interval_unit: 'month',
              archived_at: null,
              require_credit_card: false,
              return_params: '',
              taxable: true,
              update_return_url: '',
              tax_code: 'D0000000',
              initial_charge_after_trial: false,
              version_number: 1,
              update_return_params: '',
              default_product_price_point_id: 969460,
              request_billing_address: false,
              require_billing_address: false,
              require_shipping_address: false,
              product_price_point_id: 969460,
              product_price_point_name: 'Standard',
              product_price_point_handle: 'ix_standard',
              product_family: { handle: 'inspectionxpert-cloud' },
              public_signup_pages: [],
            },
            group: null,
          },
        ]);

        billingClientMock.getSubscriptionById.mockResolvedValueOnce({
          id: 36827260,
          state: 'active',
          trial_started_at: null,
          trial_ended_at: null,
          activated_at: '2020-10-27T18:24:20-04:00',
          created_at: '2020-10-27T18:24:19-04:00',
          updated_at: '2020-10-27T18:59:09-04:00',
          expires_at: null,
          balance_in_cents: 24991,
          current_period_ends_at: '2020-11-27T17:24:19-05:00',
          next_assessment_at: '2020-11-27T17:24:19-05:00',
          canceled_at: null,
          cancellation_message: null,
          next_product_id: null,
          next_product_handle: null,
          cancel_at_end_of_period: false,
          payment_collection_method: 'remittance',
          snap_day: null,
          cancellation_method: null,
          current_period_started_at: '2020-10-27T18:24:19-04:00',
          previous_state: 'active',
          signup_payment_id: 433830000,
          signup_revenue: '0.00',
          delayed_cancel_at: null,
          coupon_code: null,
          total_revenue_in_cents: 0,
          product_price_in_cents: 0,
          product_version_number: 1,
          payment_type: null,
          referral_code: null,
          coupon_use_count: null,
          coupon_uses_allowed: null,
          reason_code: null,
          automatically_resume_at: null,
          coupon_codes: [],
          offer_id: null,
          payer_id: 37711536,
          current_billing_amount_in_cents: 12500,
          receives_invoice_emails: null,
          product_price_point_id: 969460,
          next_product_price_point_id: null,
          credit_balance_in_cents: 0,
          prepayment_balance_in_cents: 0,
          net_terms: null,
          stored_credential_transaction_id: null,
          locale: null,
          reference: '3252998005',
          currency: 'USD',
          on_hold_at: null,
          scheduled_cancellation_at: null,
          customer: {
            id: 37711536,
            first_name: 'Dev',
            last_name: 'Monkey',
            organization: null,
            email: 'dev.mail.monkey@gmail.com',
            created_at: '2020-10-27T18:24:19-04:00',
            updated_at: '2020-10-27T18:24:19-04:00',
            reference: 'e0a01769-c8ed-4090-bd24-9c1b2241fb75',
            address: null,
            address_2: null,
            city: null,
            state: null,
            zip: null,
            country: null,
            phone: '',
            portal_invite_last_sent_at: null,
            portal_invite_last_accepted_at: null,
            verified: false,
            portal_customer_created_at: null,
            vat_number: null,
            cc_emails: null,
            tax_exempt: false,
            parent_id: null,
            locale: null,
          },
          product: {
            id: 5233891,
            name: 'Standard',
            handle: 'ix_standard',
            description: '',
            accounting_code: '',
            request_credit_card: true,
            expiration_interval: null,
            expiration_interval_unit: 'never',
            created_at: '2020-05-21T14:21:34-04:00',
            updated_at: '2020-10-24T15:08:33-04:00',
            price_in_cents: 0,
            interval: 1,
            interval_unit: 'month',
            initial_charge_in_cents: null,
            trial_price_in_cents: null,
            trial_interval: null,
            trial_interval_unit: 'month',
            archived_at: null,
            require_credit_card: false,
            return_params: '',
            taxable: true,
            update_return_url: '',
            tax_code: 'D0000000',
            initial_charge_after_trial: false,
            version_number: 1,
            update_return_params: '',
            default_product_price_point_id: 969460,
            request_billing_address: false,
            require_billing_address: false,
            require_shipping_address: false,
            product_price_point_id: 969460,
            product_price_point_name: 'Standard',
            product_price_point_handle: 'ix_standard',
            product_family: { handle: 'inspectionxpert-cloud' },
            public_signup_pages: [],
          },
          group: null,
        });

        billingClientMock.updateSubscriptionQuantity.mockResolvedValueOnce({
          allocation_id: 589424613,
          component_id: 1108426,
          subscription_id: 36827260,
          quantity: 6,
          previous_quantity: 1,
          memo: 'Freemium subscription updated to 6',
          timestamp: '2020-10-27T22:59:51Z',
          proration_upgrade_scheme: 'prorate-attempt-capture',
          proration_downgrade_scheme: 'no-prorate',
          price_point_id: 1067356,
          previous_price_point_id: 1067356,
          component_handle: 'drawings',
          accrue_charge: false,
          upgrade_charge: 'prorated',
          downgrade_credit: 'none',
          created_at: '2020-10-27T18:59:51-04:00',
          payment: null,
        });

        billingClientMock.getSubscriptionById.mockResolvedValueOnce({
          id: 36827260,
          state: 'active',
          trial_started_at: null,
          trial_ended_at: null,
          activated_at: '2020-10-27T18:24:20-04:00',
          created_at: '2020-10-27T18:24:19-04:00',
          updated_at: '2020-10-27T18:59:51-04:00',
          expires_at: null,
          balance_in_cents: 37481,
          current_period_ends_at: '2020-11-27T17:24:19-05:00',
          next_assessment_at: '2020-11-27T17:24:19-05:00',
          canceled_at: null,
          cancellation_message: null,
          next_product_id: null,
          next_product_handle: null,
          cancel_at_end_of_period: false,
          payment_collection_method: 'remittance',
          snap_day: null,
          cancellation_method: null,
          current_period_started_at: '2020-10-27T18:24:19-04:00',
          previous_state: 'active',
          signup_payment_id: 433830000,
          signup_revenue: '0.00',
          delayed_cancel_at: null,
          coupon_code: null,
          total_revenue_in_cents: 0,
          product_price_in_cents: 0,
          product_version_number: 1,
          payment_type: null,
          referral_code: null,
          coupon_use_count: null,
          coupon_uses_allowed: null,
          reason_code: null,
          automatically_resume_at: null,
          coupon_codes: [],
          offer_id: null,
          payer_id: 37711536,
          current_billing_amount_in_cents: 12500,
          receives_invoice_emails: null,
          product_price_point_id: 969460,
          next_product_price_point_id: null,
          credit_balance_in_cents: 0,
          prepayment_balance_in_cents: 0,
          net_terms: null,
          stored_credential_transaction_id: null,
          locale: null,
          reference: '3252998005',
          currency: 'USD',
          on_hold_at: null,
          scheduled_cancellation_at: null,
          customer: {
            id: 37711536,
            first_name: 'Dev',
            last_name: 'Monkey',
            organization: null,
            email: 'dev.mail.monkey@gmail.com',
            created_at: '2020-10-27T18:24:19-04:00',
            updated_at: '2020-10-27T18:24:19-04:00',
            reference: 'e0a01769-c8ed-4090-bd24-9c1b2241fb75',
            address: null,
            address_2: null,
            city: null,
            state: null,
            zip: null,
            country: null,
            phone: '',
            portal_invite_last_sent_at: null,
            portal_invite_last_accepted_at: null,
            verified: false,
            portal_customer_created_at: null,
            vat_number: null,
            cc_emails: null,
            tax_exempt: false,
            parent_id: null,
            locale: null,
          },
          product: {
            id: 5233891,
            name: 'Standard',
            handle: 'ix_standard',
            description: '',
            accounting_code: '',
            request_credit_card: true,
            expiration_interval: null,
            expiration_interval_unit: 'never',
            created_at: '2020-05-21T14:21:34-04:00',
            updated_at: '2020-10-24T15:08:33-04:00',
            price_in_cents: 0,
            interval: 1,
            interval_unit: 'month',
            initial_charge_in_cents: null,
            trial_price_in_cents: null,
            trial_interval: null,
            trial_interval_unit: 'month',
            archived_at: null,
            require_credit_card: false,
            return_params: '',
            taxable: true,
            update_return_url: '',
            tax_code: 'D0000000',
            initial_charge_after_trial: false,
            version_number: 1,
            update_return_params: '',
            default_product_price_point_id: 969460,
            request_billing_address: false,
            require_billing_address: false,
            require_shipping_address: false,
            product_price_point_id: 969460,
            product_price_point_name: 'Standard',
            product_price_point_handle: 'ix_standard',
            product_family: { handle: 'inspectionxpert-cloud' },
            public_signup_pages: [],
          },
          group: null,
        });

        crmClientMock.updateDeal.mockResolvedValueOnce({
          hs_closed_amount_in_home_currency: '1500',
          dealname: 'Acme Corp : Dev Monkey',
          num_associated_contacts: '1',
          createdate: '1603837458220',
          amount_in_home_currency: '1500',
          hs_is_closed: 'true',
          days_to_close: '0',
          hs_deal_stage_probability: '1',
          ix_chargify_id: '36827260',
          hs_closed_amount: '1500',
          hs_analytics_source: 'OFFLINE',
          trial_type: 'Freemium',
          hs_createdate: '1603837458220',
          hs_projected_amount: '1500',
          hs_projected_amount_in_home_currency: '1500',
          dealtype: 'newbusiness',
          amount: '1500',
          closedate: '1603839592532',
          ixc_account_id: 'e0a01769-c8ed-4090-bd24-9c1b2241fb75',
          ix_chargify_link: 'https://inspectionxpert-test.chargify.com/subscriptions/36827260',
          pipeline: '6510422',
          hs_lastmodifieddate: '1603839592878',
          dealstage: '6510428',
          hs_object_id: '3252998005',
          hs_analytics_source_data_1: 'API',
          crm: 'hubspot',
          crmType: 'deal',
          crmId: 3252998005,
        });

        const accountId = 'e0a01769-c8ed-4090-bd24-9c1b2241fb75';
        const result = await subWorker.updateFreemiumQuantity({ accountId, quantity: 6 });

        expect(result).toBeDefined();
        expect(result.success).toBe(true);

        // Make sure this looks like a non-time bound trial
        expect(result.bsSubscription).toBeDefined();
        expect(result.bsSubscription.state).toBe('active');

        // Make sure the subscription is a time bound trial
        expect(result.bsSubscription).toBeDefined();

        expect(loggerMock.error).toHaveBeenCalledTimes(0);
      }, 30000); // Wait 30 seconds for asyn timeouts
    });
  });

  describe('Time Limited Trial', () => {
    describe('#createTrial()', () => {
      it('should throw an error if required input is not provided', async () => {
        await expect(subWorker.createTrial({})).rejects.toThrow();
        expect(loggerMock.error).toHaveBeenCalledTimes(1);
      });

      it('should throw an error if the request fails', async () => {
        crmClientMock.createOrUpdateCompany.mockRejectedValueOnce(new Error('An error message.'));
        await expect(
          subWorker.createTrial({
            name: 'Foo Company',
            domain: 'foo.com',
          })
        ).rejects.toThrow();
        expect(loggerMock.error).toHaveBeenCalledTimes(1);
      });

      it('should create a company, contact, and deal in CRM and a customer and subscription in billing system when the request succeeds', async () => {
        crmClientMock.createOrUpdateCompany.mockResolvedValueOnce({
          ix_chargify_id: '37714720',
          hs_lastmodifieddate: '1603850186791',
          hs_object_id: '4721389588',
          createdate: '1603850182431',
          ix_chargify_link: 'https://inspectionxpert-test.chargify.com/customers/37714720',
          crm: 'hubspot',
          crmType: 'company',
          crmId: 4721389588,
        });

        crmClientMock.createOrUpdateContact.mockResolvedValueOnce({
          hs_is_unworked: 'true',
          website: 'http://gmail.com',
          firstname: 'Dev',
          city: 'Raleigh',
          lastmodifieddate: '1603850182796',
          num_unique_conversion_events: '0',
          createdate: '1603850182632',
          hs_lifecyclestage_subscriber_date: '1603850182632',
          hs_all_contact_vids: '2001',
          lastname: 'Monkey',
          hs_marketable_until_renewal: 'false',
          hs_is_contact: 'true',
          num_conversion_events: '0',
          hs_object_id: '2001',
          hs_email_domain: 'gmail.com',
          company: 'Acme Corp',
          state: 'NC',
          lifecyclestage: 'subscriber',
          email: 'dev.mail.monkey@gmail.com',
          ixc_user_id: 'e0a01779-c8ed-4090-bd44-9c1b2242fb75',
          crm: 'hubspot',
          crmType: 'contact',
          crmId: 2001,
        });

        crmClientMock.addContactToCompany.mockResolvedValueOnce({ success: true });

        crmClientMock.createDeal.mockResolvedValueOnce({
          hs_closed_amount_in_home_currency: '0',
          dealname: 'Acme Corp : Dev Monkey',
          createdate: '1602731071850',
          ixc_account_id: 'e0a01769-c8ed-4090-bd24-9c1b2241fb75',
          hs_is_closed: 'false',
          days_to_close: '0',
          hs_deal_stage_probability: '0.200000000000000011102230246251565404236316680908203125',
          pipeline: '5743684',
          hs_closed_amount: '0',
          hs_lastmodifieddate: '1602731071850',
          dealstage: '5743685',
          hs_createdate: '1602731071850',
          hs_object_id: '3159749714',
          dealtype: 'newbusiness',
          crm: 'hubspot',
          crmType: 'deal',
          crmId: 3159749714,
        });

        crmClientMock.addContactToDeal.mockResolvedValueOnce({ success: true });

        crmClientMock.addCompanyToDeal.mockResolvedValueOnce({ success: true });

        billingClientMock.createOrUpdateCustomer.mockResolvedValueOnce({
          first_name: 'Dev',
          last_name: 'Monkey',
          email: 'dev.mail.monkey@gmail.com',
          cc_emails: null,
          organization: null,
          reference: 'e0a01769-c8ed-4090-bd24-9c1b2241fb75',
          id: 37714720,
          created_at: '2020-10-27T21:56:24-04:00',
          updated_at: '2020-10-27T21:56:24-04:00',
          address: null,
          address_2: null,
          city: null,
          state: null,
          zip: null,
          country: null,
          phone: '',
          verified: false,
          portal_customer_created_at: null,
          portal_invite_last_sent_at: null,
          portal_invite_last_accepted_at: null,
          tax_exempt: false,
          vat_number: null,
          parent_id: null,
          locale: null,
          default_subscription_group_uid: null,
        });

        billingClientMock.getCurrentPrice.mockResolvedValueOnce(MOCK_PRODUCT_PRICES[0]);

        billingClientMock.createSubscription.mockResolvedValueOnce({
          id: 36830644,
          state: 'trialing',
          trial_started_at: '2020-10-27T21:00:00-04:00',
          trial_ended_at: '2024-10-27T21:00:00-04:00',
          activated_at: null,
          created_at: '2020-10-27T21:00:00-04:00',
          updated_at: '2020-10-27T21:00:00-04:00',
          expires_at: null,
          balance_in_cents: 0,
          current_period_ends_at: '2024-10-27T21:56:24-04:00',
          next_assessment_at: '2024-10-27T21:56:24-04:00',
          canceled_at: null,
          cancellation_message: null,
          next_product_id: null,
          next_product_handle: null,
          cancel_at_end_of_period: false,
          payment_collection_method: 'remittance',
          snap_day: null,
          cancellation_method: null,
          current_period_started_at: '2020-10-27T21:56:24-04:00',
          previous_state: 'trialing',
          signup_payment_id: 433878556,
          signup_revenue: '0.00',
          delayed_cancel_at: null,
          coupon_code: null,
          total_revenue_in_cents: 0,
          product_price_in_cents: 0,
          product_version_number: 1,
          payment_type: null,
          referral_code: null,
          coupon_use_count: null,
          coupon_uses_allowed: null,
          reason_code: null,
          automatically_resume_at: null,
          coupon_codes: [],
          offer_id: null,
          payer_id: 37714720,
          receives_invoice_emails: null,
          product_price_point_id: 969487,
          next_product_price_point_id: null,
          credit_balance_in_cents: 0,
          prepayment_balance_in_cents: 0,
          net_terms: null,
          stored_credential_transaction_id: null,
          locale: null,
          reference: '3255468599',
          currency: 'USD',
          on_hold_at: null,
          scheduled_cancellation_at: null,
          customer: {
            id: 37714720,
            first_name: 'Dev',
            last_name: 'Monkey',
            organization: null,
            email: 'dev.mail.monkey@gmail.com',
            created_at: '2020-10-27T21:56:24-04:00',
            updated_at: '2020-10-27T21:56:24-04:00',
            reference: 'e0a01769-c8ed-4090-bd24-9c1b2241fb75',
            address: null,
            address_2: null,
            city: null,
            state: null,
            zip: null,
            country: null,
            phone: '',
            portal_invite_last_sent_at: null,
            portal_invite_last_accepted_at: null,
            verified: false,
            portal_customer_created_at: null,
            vat_number: null,
            cc_emails: null,
            tax_exempt: false,
            parent_id: null,
            locale: null,
          },
          product: {
            id: 5256309,
            name: 'Standard',
            handle: 'ix_standard',
            description: '',
            accounting_code: '',
            request_credit_card: true,
            expiration_interval: null,
            expiration_interval_unit: 'never',
            created_at: '2020-09-01T20:56:13-04:00',
            updated_at: '2020-10-25T16:28:06-04:00',
            price_in_cents: 0,
            interval: 1,
            interval_unit: 'month',
            initial_charge_in_cents: null,
            trial_price_in_cents: 0,
            trial_interval: 48,
            trial_interval_unit: 'month',
            archived_at: null,
            require_credit_card: false,
            return_params: '',
            taxable: false,
            update_return_url: '',
            tax_code: '',
            initial_charge_after_trial: false,
            version_number: 1,
            update_return_params: '',
            default_product_price_point_id: 969487,
            request_billing_address: false,
            require_billing_address: false,
            require_shipping_address: false,
            product_price_point_id: 969487,
            product_price_point_name: 'Standard',
            product_price_point_handle: 'ix_standard',
            product_family: { handle: 'inspectionxpert-cloud' },
            public_signup_pages: [],
          },
          group: null,
        });

        billingClientMock.getCurrentPrice.mockResolvedValueOnce({
          id: 1067356,
          handle: 'v10102020_annually',
        });

        billingClientMock.updateSubscriptionQuantity.mockResolvedValueOnce({
          allocation_id: 589424613,
          component_id: 1108426,
          subscription_id: 36827260,
          quantity: 1,
          previous_quantity: 0,
          memo: 'Initial seat component for new trial',
          timestamp: '2020-10-27T22:59:51Z',
          proration_upgrade_scheme: 'prorate-attempt-capture',
          proration_downgrade_scheme: 'no-prorate',
          price_point_id: 1067356,
          previous_price_point_id: 1067356,
          component_handle: 'paid_user_seat_pu',
          accrue_charge: false,
          upgrade_charge: 'prorated',
          downgrade_credit: 'none',
          created_at: '2020-10-27T18:59:51-04:00',
          payment: null,
        });

        billingClientMock.updateComponentPrice.mockResolvedValueOnce({
          allocation_id: 589424613,
          component_id: 1108426,
          subscription_id: 36827260,
          quantity: 1,
          previous_quantity: 0,
          memo: 'Initial seat component for new trial',
          timestamp: '2020-10-27T22:59:51Z',
          proration_upgrade_scheme: 'prorate-attempt-capture',
          proration_downgrade_scheme: 'no-prorate',
          price_point_id: 1067356,
          previous_price_point_id: 1067356,
          component_handle: 'paid_user_seat_pu',
          accrue_charge: false,
          upgrade_charge: 'prorated',
          downgrade_credit: 'none',
          created_at: '2020-10-27T18:59:51-04:00',
          payment: null,
        });

        crmClientMock.updateCompany.mockResolvedValueOnce({
          ix_chargify_id: '37451677',
          hs_lastmodifieddate: '1602731075555',
          hs_object_id: '4634689283',
          createdate: '1602731070793',
          ix_chargify_link: 'https://inspectionxpert-test.chargify.com/customers/37451677',
          crm: 'hubspot',
          crmType: 'company',
          crmId: 4634689283,
        });

        crmClientMock.updateDeal.mockResolvedValueOnce({
          hs_closed_amount_in_home_currency: '0',
          dealname: 'Acme Corp : Dev Monkey',
          num_associated_contacts: '1',
          createdate: '1603850183406',
          amount_in_home_currency: '',
          hs_is_closed: 'false',
          days_to_close: '1460',
          hs_deal_stage_probability: '0.200000000000000011102230246251565404236316680908203125',
          trial_end_date: '1730073600000',
          ix_chargify_id: '36830644',
          hs_closed_amount: '0',
          hs_analytics_source: 'OFFLINE',
          trial_type: 'Time Limited',
          hs_createdate: '1603850183406',
          hs_projected_amount: '',
          hs_projected_amount_in_home_currency: '',
          dealtype: 'newbusiness',
          amount: '',
          closedate: '1730073600000',
          ixc_account_id: 'e0a01769-c8ed-4090-bd24-9c1b2241fb75',
          ix_chargify_link: 'https://inspectionxpert-test.chargify.com/subscriptions/36830644',
          pipeline: '5743684',
          hs_lastmodifieddate: '1603850186954',
          dealstage: '5743685',
          hs_object_id: '3255468599',
          trial_started_date: '1603843200000',
          hs_analytics_source_data_1: 'API',
          crm: 'hubspot',
          crmType: 'deal',
          crmId: 3255468599,
        });

        const result = await subWorker.createTrial(signupPageFormData);
        // console.log('createTrial result', result);
        /*
        { 
          success: true, 
          input:
            { userId: 'e0a01779-c8ed-4090-bd44-9c1b2242fb75',
              accountId: 'e0a01769-c8ed-4090-bd24-9c1b2241fb75',
              firstName: 'Dev',
              lastName: 'Monkey',
              email: 'dev.mail.monkey@gmail.com',
              phone: '',
              domain: 'gmail.com',
              orgName: 'Acme Corp',
              orgStreet: '1 Glendwood Ave',
              orgStreet2: 'WeWork Floor 5',
              orgCity: 'Raleigh',
              orgRegion: 'NC',
              orgPostalcode: '27603',
              orgCountry: 'US' },
          crmCompany:
            { ix_chargify_id: '37451677',
              hs_lastmodifieddate: '1602731075555',
              hs_object_id: '4634689283',
              createdate: '1602731070793',
              ix_chargify_link:
              'https://inspectionxpert-test.chargify.com/customers/37451677',
              crm: 'hubspot',
              crmType: 'company',
              crmId: 4634689283 },
          crmContact:
            { hs_is_unworked: 'true',
              website: 'http://gmail.com',
              firstname: 'Dev',
              city: 'Raleigh',
              lastmodifieddate: '1602731071314',
              num_unique_conversion_events: '0',
              createdate: '1602731071055',
              hs_lifecyclestage_subscriber_date: '1602731071055',
              lastname: 'Monkey',
              hs_is_contact: 'true',
              num_conversion_events: '0',
              hs_object_id: '1251',
              hs_email_domain: 'gmail.com',
              company: 'Acme Corp',
              state: 'NC',
              lifecyclestage: 'subscriber',
              email: 'dev.mail.monkey@gmail.com',
              ixc_user_id: 'e0a01779-c8ed-4090-bd44-9c1b2242fb75',
              crm: 'hubspot',
              crmType: 'contact',
              crmId: 1251 },
          crmDeal:
            { hs_closed_amount_in_home_currency: '0',
              dealname: 'Acme Corp : Dev Monkey',
              closedate: '1603335873000',
              num_associated_contacts: '1',
              createdate: '1602731071850',
              hs_is_closed: 'false',
              ixc_account_id: 'e0a01769-c8ed-4090-bd24-9c1b2241fb75',
              days_to_close: '7',
              hs_deal_stage_probability: '0.200000000000000011102230246251565404236316680908203125',
              ix_chargify_link:
              'https://inspectionxpert-test.chargify.com/subscriptions/36574344',
              pipeline: '5743684',
              ix_chargify_id: '36574344',
              hs_closed_amount: '0',
              hs_analytics_source: 'OFFLINE',
              hs_lastmodifieddate: '1602731075727',
              dealstage: '5743685',
              hs_createdate: '1602731071850',
              hs_object_id: '3159749714',
              hs_analytics_source_data_1: 'API',
              dealtype: 'newbusiness',
              crm: 'hubspot',
              crmType: 'deal',
              crmId: 3159749714 },
          bsSubscription:
            { id: 36574344,
              state: 'active',
              trial_started_at: null,
              trial_ended_at: null,
              activated_at: '2020-10-14T23:04:35-04:00',
              created_at: '2020-10-14T23:04:33-04:00',
              updated_at: '2020-10-14T23:04:35-04:00',
              expires_at: '2020-10-21T23:04:33-04:00',
              balance_in_cents: 0,
              current_period_ends_at: '2020-10-21T23:04:33-04:00',
              next_assessment_at: '2020-10-21T23:04:33-04:00',
              canceled_at: null,
              cancellation_message: null,
              next_product_id: null,
              next_product_handle: null,
              cancel_at_end_of_period: false,
              payment_collection_method: 'remittance',
              snap_day: null,
              cancellation_method: null,
              current_period_started_at: '2020-10-14T23:04:33-04:00',
              previous_state: 'active',
              signup_payment_id: 430337209,
              signup_revenue: '0.00',
              delayed_cancel_at: null,
              coupon_code: null,
              total_revenue_in_cents: 0,
              product_price_in_cents: 0,
              product_version_number: 1,
              payment_type: null,
              referral_code: null,
              coupon_use_count: null,
              coupon_uses_allowed: null,
              reason_code: null,
              automatically_resume_at: null,
              coupon_codes: [],
              offer_id: null,
              payer_id: 37451677,
              receives_invoice_emails: null,
              product_price_point_id: 969487,
              next_product_price_point_id: null,
              credit_balance_in_cents: 0,
              prepayment_balance_in_cents: 0,
              net_terms: null,
              stored_credential_transaction_id: null,
              locale: null,
              reference: '3159749714',
              currency: 'USD',
              on_hold_at: null,
              customer: [Object],
              product: [Object],
              group: null } } }
        */
        expect(result).toBeDefined();
        expect(result.success).toBe(true);

        expect(result.crmCompany).toBeDefined();
        expect(result.crmCompany.ix_chargify_id).toBe('37451677');

        expect(result.crmContact).toBeDefined();
        expect(result.crmContact.email).toBe('dev.mail.monkey@gmail.com');
        expect(result.crmContact.ixc_user_id).toBe('e0a01779-c8ed-4090-bd44-9c1b2242fb75');

        // Make sure the subscription is a time bound trial
        expect(result.bsSubscription).toBeDefined();
        expect(result.bsSubscription.state).toBe('trialing');
        expect(result.bsSubscription.created_at).toBeDefined(); // 2020-10-24T13:25:07-04:00
        expect(result.bsSubscription.trial_started_at).toBeDefined(); // 2020-10-24T13:25:07-04:00
        expect(result.bsSubscription.trial_ended_at).toBeDefined(); // 2024-10-24T13:25:07-04:00
        expect(result.bsSubscription.activated_at).toBe(null);
        expect(result.bsSubscription.expires_at).toBe(null);
        const trialStartedAt = Date.parse(result.bsSubscription.trial_started_at);
        const trialEndedAt = Date.parse(result.bsSubscription.trial_ended_at);
        const differenceInDays = (Math.round(trialEndedAt - trialStartedAt) / (1000 * 60 * 60 * 24)).toFixed(0) - 1;

        expect(loggerMock.error).toHaveBeenCalledTimes(0);
      }, 30000); // Wait 30 seconds for asyn timeouts
    });

    describe('#extendTrial()', () => {
      it('should throw an error if required input is not provided', async () => {
        await expect(subWorker.extendTrial({})).rejects.toThrow();
        expect(loggerMock.error).toHaveBeenCalledTimes(1);
      });

      it('should throw an error if the request fails', async () => {
        // This test fails when using the real hubspot API, because the record is created successfully
        if (subWorker.isMocked) {
          // crmClientMock.updateDeal.mockRejectedValueOnce(new Error('An error message.'));
          await expect(
            subWorker.extendTrial({
              accountId: 'e0a01769-c8ed-4090-bd24-9c1b2241fb75',
            })
          ).rejects.toThrow();
          expect(loggerMock.error).toHaveBeenCalledTimes(1);
        }
      });

      it('should update the trial subscriptions expiration date in the billing and CRM systems when the request succeeds', async () => {
        billingClientMock.getCustomerByReference.mockResolvedValueOnce({
          first_name: 'Dev',
          last_name: 'Monkey',
          email: 'dev.mail.monkey@gmail.com',
          cc_emails: null,
          organization: null,
          reference: 'e0a01769-c8ed-4090-bd24-9c1b2241fb75',
          id: 37451677,
          created_at: '2020-10-14T23:04:33-04:00',
          updated_at: '2020-10-14T23:04:33-04:00',
          address: null,
          address_2: null,
          city: null,
          state: null,
          zip: null,
          country: null,
          phone: '',
          verified: false,
          portal_customer_created_at: null,
          portal_invite_last_sent_at: null,
          portal_invite_last_accepted_at: null,
          tax_exempt: false,
          vat_number: null,
          parent_id: null,
          locale: null,
          default_subscription_group_uid: null,
        });

        billingClientMock.getSubscriptionsForCustomer.mockResolvedValueOnce([
          {
            id: 36574344,
            state: 'active',
            trial_started_at: null,
            trial_ended_at: null,
            activated_at: '2020-10-14T23:04:35-04:00',
            created_at: '2020-10-14T23:04:33-04:00',
            updated_at: '2020-10-14T23:04:35-04:00',
            expires_at: '2020-10-21T23:04:33-04:00',
            balance_in_cents: 0,
            current_period_ends_at: '2020-10-21T23:04:33-04:00',
            next_assessment_at: '2020-10-21T23:04:33-04:00',
            canceled_at: null,
            cancellation_message: null,
            next_product_id: null,
            next_product_handle: null,
            cancel_at_end_of_period: false,
            payment_collection_method: 'remittance',
            snap_day: null,
            cancellation_method: null,
            current_period_started_at: '2020-10-14T23:04:33-04:00',
            previous_state: 'active',
            signup_payment_id: 430337209,
            signup_revenue: '0.00',
            delayed_cancel_at: null,
            coupon_code: null,
            total_revenue_in_cents: 0,
            product_price_in_cents: 0,
            product_version_number: 1,
            payment_type: null,
            referral_code: null,
            coupon_use_count: null,
            coupon_uses_allowed: null,
            reason_code: null,
            automatically_resume_at: null,
            coupon_codes: [],
            offer_id: null,
            payer_id: 37451677,
            receives_invoice_emails: null,
            product_price_point_id: 969487,
            next_product_price_point_id: null,
            credit_balance_in_cents: 0,
            prepayment_balance_in_cents: 0,
            net_terms: null,
            stored_credential_transaction_id: null,
            locale: null,
            reference: '3159749714',
            currency: 'USD',
            on_hold_at: null,
            customer: {
              id: 37451444,
              first_name: 'Dev',
              last_name: 'Monkey',
              organization: null,
              email: 'dev.mail.monkey@gmail.com',
              created_at: '2020-10-14T22:41:32-04:00',
              updated_at: '2020-10-14T22:41:32-04:00',
              reference: 'e0a01769-c8ed-4090-bd24-9c1b2241fb75',
              address: null,
              address_2: null,
              city: null,
              state: null,
              zip: null,
              country: null,
              phone: '',
              portal_invite_last_sent_at: null,
              portal_invite_last_accepted_at: null,
              verified: false,
              portal_customer_created_at: null,
              vat_number: null,
              cc_emails: null,
              tax_exempt: false,
              parent_id: null,
              locale: null,
            },
            product: {
              id: 5256309,
              name: 'Standard',
              handle: 'ix_standard',
              description: '',
              accounting_code: '',
              request_credit_card: true,
              expiration_interval: 7,
              expiration_interval_unit: 'day',
              created_at: '2020-09-01T20:56:13-04:00',
              updated_at: '2020-10-11T23:03:57-04:00',
              price_in_cents: 0,
              interval: 7,
              interval_unit: 'day',
              initial_charge_in_cents: null,
              trial_price_in_cents: null,
              trial_interval: null,
              trial_interval_unit: 'month',
              archived_at: null,
              require_credit_card: false,
              return_params: '',
              taxable: false,
              update_return_url: '',
              tax_code: '',
              initial_charge_after_trial: false,
              version_number: 1,
              update_return_params: '',
              default_product_price_point_id: 969487,
              request_billing_address: false,
              require_billing_address: false,
              require_shipping_address: false,
              product_price_point_id: 969487,
              product_price_point_name: 'Original',
              product_price_point_handle: 'ix_standard',
              product_family: { id: 1526431, name: 'IX3', description: '', handle: 'inspectionxpert-cloud', accounting_code: null, created_at: '2020-05-21T14:21:33-04:00', updated_at: '2020-09-01T16:25:44-04:00' },
              public_signup_pages: [],
            },
            group: null,
          },
        ]);

        billingClientMock.updateSubscription.mockResolvedValueOnce({
          id: 36574344,
          state: 'active',
          trial_started_at: null,
          trial_ended_at: null,
          activated_at: '2020-10-14T23:04:35-04:00',
          created_at: '2020-10-14T23:04:33-04:00',
          updated_at: '2020-10-15T00:00:36-04:00',
          expires_at: '2020-12-01T19:00:00-05:00',
          balance_in_cents: 0,
          current_period_ends_at: '2020-10-21T23:04:33-04:00',
          next_assessment_at: '2020-10-21T23:04:33-04:00',
          canceled_at: null,
          cancellation_message: null,
          next_product_id: null,
          next_product_handle: null,
          cancel_at_end_of_period: false,
          payment_collection_method: 'remittance',
          snap_day: null,
          cancellation_method: null,
          current_period_started_at: '2020-10-14T23:04:33-04:00',
          previous_state: 'active',
          signup_payment_id: 430337209,
          signup_revenue: '0.00',
          delayed_cancel_at: null,
          coupon_code: null,
          total_revenue_in_cents: 0,
          product_price_in_cents: 0,
          product_version_number: 1,
          payment_type: null,
          referral_code: null,
          coupon_use_count: null,
          coupon_uses_allowed: null,
          reason_code: null,
          automatically_resume_at: null,
          coupon_codes: [],
          offer_id: null,
          payer_id: 37451677,
          current_billing_amount_in_cents: 0,
          receives_invoice_emails: null,
          product_price_point_id: 969487,
          next_product_price_point_id: null,
          credit_balance_in_cents: 0,
          prepayment_balance_in_cents: 0,
          net_terms: null,
          stored_credential_transaction_id: null,
          locale: null,
          reference: '3159749714',
          currency: 'USD',
          on_hold_at: null,
          customer: {
            id: 37451677,
            first_name: 'Dev',
            last_name: 'Monkey',
            organization: null,
            email: 'dev.mail.monkey@gmail.com',
            created_at: '2020-10-14T23:04:33-04:00',
            updated_at: '2020-10-14T23:04:33-04:00',
            reference: 'e0a01769-c8ed-4090-bd24-9c1b2241fb75',
            address: null,
            address_2: null,
            city: null,
            state: null,
            zip: null,
            country: null,
            phone: '',
            portal_invite_last_sent_at: null,
            portal_invite_last_accepted_at: null,
            verified: false,
            portal_customer_created_at: null,
            vat_number: null,
            cc_emails: null,
            tax_exempt: false,
            parent_id: null,
            locale: null,
          },
          product: {
            id: 5256309,
            name: 'Standard',
            handle: 'ix_standard',
            description: '',
            accounting_code: '',
            request_credit_card: true,
            expiration_interval: 7,
            expiration_interval_unit: 'day',
            created_at: '2020-09-01T20:56:13-04:00',
            updated_at: '2020-10-11T23:03:57-04:00',
            price_in_cents: 0,
            interval: 7,
            interval_unit: 'day',
            initial_charge_in_cents: null,
            trial_price_in_cents: null,
            trial_interval: null,
            trial_interval_unit: 'month',
            archived_at: null,
            require_credit_card: false,
            return_params: '',
            taxable: false,
            update_return_url: '',
            tax_code: '',
            initial_charge_after_trial: false,
            version_number: 1,
            update_return_params: '',
            default_product_price_point_id: 969487,
            request_billing_address: false,
            require_billing_address: false,
            require_shipping_address: false,
            product_price_point_id: 969487,
            product_price_point_name: 'Original',
            product_price_point_handle: 'ix_standard',
            product_family: { id: 1526431, name: 'IX3', description: '', handle: 'inspectionxpert-cloud', accounting_code: null, created_at: '2020-05-21T14:21:33-04:00', updated_at: '2020-09-01T16:25:44-04:00' },
            public_signup_pages: [],
          },
          group: null,
        });

        crmClientMock.updateDeal.mockResolvedValueOnce({
          hs_closed_amount_in_home_currency: '0',
          dealname: 'Acme Corp : Dev Monkey',
          closedate: '1606867200000',
          num_associated_contacts: '1',
          createdate: '1602731071850',
          hs_is_closed: 'false',
          ixc_account_id: 'e0a01769-c8ed-4090-bd24-9c1b2241fb75',
          days_to_close: '47',
          hs_deal_stage_probability: '0.40000000000000002220446049250313080847263336181640625',
          ix_chargify_link: 'https://inspectionxpert-test.chargify.com/subscriptions/36574344',
          pipeline: '5743684',
          ix_chargify_id: '36574344',
          hs_closed_amount: '0',
          hs_analytics_source: 'OFFLINE',
          hs_lastmodifieddate: '1602734437158',
          dealstage: '5743686',
          hs_createdate: '1602731071850',
          hs_object_id: '3159749714',
          hs_analytics_source_data_1: 'API',
          dealtype: 'newbusiness',
          crm: 'hubspot',
          crmType: 'deal',
          crmId: 3159749714,
        });

        const result = await subWorker.extendTrial({
          accountId: 'e0a01769-c8ed-4090-bd24-9c1b2241fb75',
          newEndDate: '2020-12-02', // YYYY-MM-DD
        });
        expect(result).toBeDefined();
        expect(result.success).toBe(true);

        expect(result.bsSubscription).toBeDefined();
        expect(result.bsSubscription.expires_at).toBe('2020-12-01T19:00:00-05:00');

        expect(loggerMock.error).toHaveBeenCalledTimes(0);
      }, 30000); // Wait 30 seconds for asyn timeouts
    });

    describe('#rollbackTrialConversion()', () => {
      const original = {
        ...MOCK_SUBSCRIPTION,
        state: 'trialing',
        billing_interval: 'monthly',
        components: [
          {
            component_id: 1028184,
            product_family_key: 'inspectionxpert-cloud',
            allocated_quantity: 1,
            price_point_id: 1886525,
            price_point_handle: 'v20210415_monthly',
            name: 'User Seats (PU)',
            type: 'per_unit',
            unit_price: '125',
          },
        ],
      };
      const changed = {
        ...MOCK_SUBSCRIPTION,
        billing_interval: 'annually',
        terms_accepted_on: 'now',
        components: [
          {
            component_id: 1028184,
            product_family_key: 'inspectionxpert-cloud',
            allocated_quantity: 3,
            price_point_id: 1886525,
            price_point_handle: 'v20210415_annually',
            name: 'User Seats (PU)',
            type: 'per_unit',
            unit_price: '1500',
          },
        ],
      };
      const metaData = [
        { name: 'billing_interval', value: 'annually' },
        { name: 'terms_accepted_on', value: 'now' },
      ];
      it('should rollback next billing change', async () => {
        billingClientMock.updateSubscription.mockResolvedValueOnce();

        await subWorker.rollbackTrialConversion({ subscriptionId: 36574117, original, changed, metaData, rollback: { subscription: true } });

        expect(billingClientMock.updateSubscription).toHaveBeenCalledTimes(1);
        expect(billingClientMock.updateSubscription).toHaveBeenCalledWith({
          id: changed.id,
          product: original.product,
          next_billing_at: original.next_billing_at,
        });

        expect(loggerMock.error).toHaveBeenCalledTimes(0);
      }, 30000); // Wait 30 seconds for asyn timeouts

      it('should rollback product if trial was expired', async () => {
        billingClientMock.updateSubscription.mockResolvedValueOnce();

        await subWorker.rollbackTrialConversion({ subscriptionId: 36574117, original, changed: { ...changed, state: 'trial_expired' }, metaData, rollback: { subscription: true } });

        expect(billingClientMock.updateSubscription).toHaveBeenCalledTimes(1);
        expect(billingClientMock.updateSubscription).toHaveBeenCalledWith({
          id: changed.id,
          product: original.product,
        });

        expect(loggerMock.error).toHaveBeenCalledTimes(0);
      }, 30000); // Wait 30 seconds for asyn timeouts

      it('should void generated invoices', async () => {
        const voidAllOpenInvoicesMock = jest.spyOn(subWorker, 'voidAllOpenInvoices').mockImplementation(() => true);

        await subWorker.rollbackTrialConversion({ subscriptionId: 36574117, original, changed, metaData, rollback: { invoices: true } });

        expect(voidAllOpenInvoicesMock).toHaveBeenCalledTimes(1);
        expect(loggerMock.error).toHaveBeenCalledTimes(0);

        voidAllOpenInvoicesMock.mockRestore();
      }, 30000); // Wait 30 seconds for asyn timeouts

      it('should rollback components', async () => {
        const updateComponentsMock = jest.spyOn(subWorker, 'updateComponents').mockImplementation(() => true);

        await subWorker.rollbackTrialConversion({ subscriptionId: 36574117, original, changed, metaData, rollback: { components: true } });

        expect(updateComponentsMock).toHaveBeenCalledTimes(1);
        expect(loggerMock.error).toHaveBeenCalledTimes(0);

        updateComponentsMock.mockRestore();
      }, 30000); // Wait 30 seconds for asyn timeouts

      it('should rollback custom field changes', async () => {
        billingClientMock.updateSubscriptionCustomFields.mockResolvedValueOnce();

        await subWorker.rollbackTrialConversion({ subscriptionId: 36574117, original, changed, metaData, rollback: { metadata: true } });

        expect(billingClientMock.updateSubscriptionCustomFields).toHaveBeenCalledTimes(1);
        expect(billingClientMock.updateSubscriptionCustomFields).toHaveBeenCalledWith({
          changes: [
            {
              name: 'billing_interval',
              value: 'monthly',
            },
          ],
          subscriptionId: 36574117,
        });

        expect(loggerMock.error).toHaveBeenCalledTimes(0);
      }, 30000); // Wait 30 seconds for asyn timeouts

      it('should delete new custom fields', async () => {
        billingClientMock.deleteSubscriptionCustomFields.mockResolvedValueOnce();

        await subWorker.rollbackTrialConversion({ subscriptionId: 36574117, original, changed, metaData, rollback: { metadata: true } });

        expect(billingClientMock.deleteSubscriptionCustomFields).toHaveBeenCalledTimes(1);
        expect(billingClientMock.deleteSubscriptionCustomFields).toHaveBeenCalledWith({ changes: ['terms_accepted_on'], subscriptionId: 36574117 });

        expect(loggerMock.error).toHaveBeenCalledTimes(0);
      }, 30000); // Wait 30 seconds for asyn timeouts

      it('logs an error if it fails to rollback', async () => {
        await expect(subWorker.rollbackTrialConversion({})).rejects.toThrow();
        expect(loggerMock.error).toHaveBeenCalledTimes(0);
      });
    });

    describe('#convertTrialToSubscription()', () => {
      it('should throw an error if required input is not provided', async () => {
        const rollbackTrialConversionMock = jest.spyOn(subWorker, 'rollbackTrialConversion').mockImplementation(() => true);

        await expect(subWorker.convertTrialToSubscription({})).rejects.toThrow();
        expect(loggerMock.error).toHaveBeenCalledTimes(1);

        rollbackTrialConversionMock.mockRestore();
      });

      it('should throw an error if the request fails', async () => {
        const rollbackTrialConversionMock = jest.spyOn(subWorker, 'rollbackTrialConversion').mockImplementation(() => true);

        // This test fails when using the real hubspot API, because the record is created successfully
        if (subWorker.isMocked) {
          // hubspotMock.companies.create.mockRejectedValueOnce(new Error('An error message.'));
          await expect(
            subWorker.convertTrialToSubscription({
              subscriptionId: '1234',
              data: {},
            })
          ).rejects.toThrow();
          expect(loggerMock.error).toHaveBeenCalledTimes(1);
        }

        rollbackTrialConversionMock.mockRestore();
      });

      it('should throw an error if component is not in the catalog', async () => {
        const mockComponents = [
          {
            component_id: 1241907,
            handle: 'internal_account_ss',
            product_family_key: 'ix_standard',
            quantity: 1,
          },
          {
            component_id: 1213884,
            handle: 'paid_user_seat_ss',
            product_family_key: 'ix_standard',
            quantity: 1,
          },
        ];
        jest.spyOn(subWorker, 'getSubscriptionForId').mockImplementation(() => ({ ...MOCK_SUBSCRIPTION, state: 'trialing' }));
        // Default Component
        billingClientMock.getCurrentPrice.mockResolvedValueOnce({
          id: 1241559,
          default: false,
          name: '20210419_annually',
          pricing_scheme: 'stairstep',
          component_id: 1213884,
          handle: 'v20210419_annually',
          archived_at: null,
          created_at: '2021-04-19T10:39:20-04:00',
          updated_at: '2021-04-19T10:39:20-04:00',
          prices: [
            {
              id: 2275421,
              component_id: 1213884,
              starting_quantity: 1,
              ending_quantity: 1,
              unit_price: '1500.0',
              price_point_id: 1241559,
              formatted_unit_price: '$1,500.00',
              segment_id: null,
            },
            {
              id: 2275422,
              component_id: 1213884,
              starting_quantity: 2,
              ending_quantity: 3,
              unit_price: '3000.0',
              price_point_id: 1241559,
              formatted_unit_price: '$3,000.00',
              segment_id: null,
            },
            {
              id: 2275423,
              component_id: 1213884,
              starting_quantity: 4,
              ending_quantity: 5,
              unit_price: '4500.0',
              price_point_id: 1241559,
              formatted_unit_price: '$4,500.00',
              segment_id: null,
            },
          ],
        });
        // Product Price
        billingClientMock.getCurrentPrice.mockResolvedValueOnce(MOCK_PRODUCT_PRICES[0]);

        // Update Components
        jest.spyOn(subWorker, 'updateComponents').mockImplementation(() => true);

        await expect(
          subWorker.convertTrialToSubscription({
            subscriptionId: '36831983',
            data: { termsAcceptedOn: null },
            components: mockComponents,
          })
        ).rejects.toThrow();
        expect(loggerMock.error).toHaveBeenCalledTimes(1);
      }); // Wait 30 seconds for async timeouts

      it('should convert the trial to a paid subscription in the billing system and close the CRM deal as won', async () => {
        const mockComponents = [
          {
            component_id: 1028184,
            handle: 'paid_user_seat_pu',
            product_family_key: 'ix_standard',
            quantity: 1,
          },
          {
            component_id: 1213884,
            handle: 'paid_user_seat_ss',
            product_family_key: 'ix_standard',
            quantity: 1,
          },
        ];
        const getSubscriptionForIdMock = jest.spyOn(subWorker, 'getSubscriptionForId').mockImplementation(() => ({ ...MOCK_SUBSCRIPTION, state: 'trialing' }));
        // Default Component
        billingClientMock.getCurrentPrice.mockResolvedValueOnce({
          id: 1241559,
          default: false,
          name: '20210419_annually',
          pricing_scheme: 'stairstep',
          component_id: 1213884,
          handle: 'v20210419_annually',
          archived_at: null,
          created_at: '2021-04-19T10:39:20-04:00',
          updated_at: '2021-04-19T10:39:20-04:00',
          prices: [
            {
              id: 2275421,
              component_id: 1213884,
              starting_quantity: 1,
              ending_quantity: 1,
              unit_price: '1500.0',
              price_point_id: 1241559,
              formatted_unit_price: '$1,500.00',
              segment_id: null,
            },
            {
              id: 2275422,
              component_id: 1213884,
              starting_quantity: 2,
              ending_quantity: 3,
              unit_price: '3000.0',
              price_point_id: 1241559,
              formatted_unit_price: '$3,000.00',
              segment_id: null,
            },
            {
              id: 2275423,
              component_id: 1213884,
              starting_quantity: 4,
              ending_quantity: 5,
              unit_price: '4500.0',
              price_point_id: 1241559,
              formatted_unit_price: '$4,500.00',
              segment_id: null,
            },
          ],
        });
        billingClientMock.getCurrentPrice.mockResolvedValueOnce({
          id: 1241559,
          default: false,
          name: '20210419_annually',
          pricing_scheme: 'stairstep',
          component_id: 1213884,
          handle: 'v20210419_annually',
          archived_at: null,
          created_at: '2021-04-19T10:39:20-04:00',
          updated_at: '2021-04-19T10:39:20-04:00',
          prices: [
            {
              id: 2275421,
              component_id: 1213884,
              starting_quantity: 1,
              ending_quantity: 1,
              unit_price: '1500.0',
              price_point_id: 1241559,
              formatted_unit_price: '$1,500.00',
              segment_id: null,
            },
            {
              id: 2275422,
              component_id: 1213884,
              starting_quantity: 2,
              ending_quantity: 3,
              unit_price: '3000.0',
              price_point_id: 1241559,
              formatted_unit_price: '$3,000.00',
              segment_id: null,
            },
            {
              id: 2275423,
              component_id: 1213884,
              starting_quantity: 4,
              ending_quantity: 5,
              unit_price: '4500.0',
              price_point_id: 1241559,
              formatted_unit_price: '$4,500.00',
              segment_id: null,
            },
          ],
        });
        // Product Price
        billingClientMock.getCurrentPrice.mockResolvedValueOnce(MOCK_PRODUCT_PRICES[0]);

        // Update Components
        const updateComponentsMock = jest.spyOn(subWorker, 'updateComponents').mockImplementation(() => true);

        billingClientMock.updateSubscription.mockResolvedValueOnce({
          id: 36831983,
          state: 'trialing',
          trial_started_at: '2020-10-27T23:28:34-04:00',
          trial_ended_at: '2020-10-27T23:29:10-04:00',
          activated_at: null,
          created_at: '2020-10-27T23:28:34-04:00',
          updated_at: '2020-10-27T23:29:10-04:00',
          expires_at: '2021-10-27T00:00:00-04:00',
          balance_in_cents: 0,
          current_period_ends_at: '2020-10-27T23:29:10-04:00',
          next_assessment_at: '2020-10-27T23:29:10-04:00',
          canceled_at: null,
          cancellation_message: null,
          next_product_id: null,
          next_product_handle: null,
          cancel_at_end_of_period: false,
          payment_collection_method: 'remittance',
          snap_day: null,
          cancellation_method: null,
          current_period_started_at: '2020-10-27T23:28:34-04:00',
          previous_state: 'trialing',
          signup_payment_id: 433903604,
          signup_revenue: '0.00',
          delayed_cancel_at: null,
          coupon_code: null,
          total_revenue_in_cents: 0,
          product_price_in_cents: 0,
          product_version_number: 1,
          payment_type: null,
          referral_code: null,
          coupon_use_count: null,
          coupon_uses_allowed: null,
          reason_code: null,
          automatically_resume_at: null,
          coupon_codes: [],
          offer_id: null,
          payer_id: 37716139,
          current_billing_amount_in_cents: 0,
          receives_invoice_emails: null,
          product_price_point_id: 969460,
          next_product_price_point_id: null,
          credit_balance_in_cents: 0,
          prepayment_balance_in_cents: 0,
          net_terms: null,
          stored_credential_transaction_id: null,
          locale: null,
          reference: '3257618779',
          currency: 'USD',
          on_hold_at: null,
          scheduled_cancellation_at: null,
          customer: {
            id: 37716139,
            first_name: 'Dev',
            last_name: 'Monkey',
            organization: null,
            email: 'dev.mail.monkey@gmail.com',
            created_at: '2020-10-27T23:28:34-04:00',
            updated_at: '2020-10-27T23:28:34-04:00',
            reference: 'e0a01769-c8ed-4090-bd24-9c1b2241fb75',
            address: null,
            address_2: null,
            city: null,
            state: null,
            zip: null,
            country: null,
            phone: '',
            portal_invite_last_sent_at: null,
            portal_invite_last_accepted_at: null,
            verified: false,
            portal_customer_created_at: null,
            vat_number: null,
            cc_emails: null,
            tax_exempt: false,
            parent_id: null,
            locale: null,
          },
          product: {
            id: 5233891,
            name: 'Standard',
            handle: 'ix_standard',
            description: '',
            accounting_code: '',
            request_credit_card: true,
            expiration_interval: null,
            expiration_interval_unit: 'never',
            created_at: '2020-05-21T14:21:34-04:00',
            updated_at: '2020-10-24T15:08:33-04:00',
            price_in_cents: 0,
            interval: 1,
            interval_unit: 'month',
            initial_charge_in_cents: null,
            trial_price_in_cents: null,
            trial_interval: null,
            trial_interval_unit: 'month',
            archived_at: null,
            require_credit_card: false,
            return_params: '',
            taxable: true,
            update_return_url: '',
            tax_code: 'D0000000',
            initial_charge_after_trial: false,
            version_number: 1,
            update_return_params: '',
            default_product_price_point_id: 969460,
            request_billing_address: false,
            require_billing_address: false,
            require_shipping_address: false,
            product_price_point_id: 969460,
            product_price_point_name: 'Standard',
            product_price_point_handle: 'ix_standard',
            product_family: { id: 1526431, name: 'IX3', description: '', handle: 'inspectionxpert-cloud', accounting_code: null, created_at: '2020-05-21T14:21:33-04:00', updated_at: '2020-09-01T16:25:44-04:00' },
            public_signup_pages: [],
          },
          group: null,
        });

        crmClientMock.updateDeal.mockResolvedValueOnce({
          hs_closed_amount_in_home_currency: '0',
          dealname: 'Acme Corp : Dev Monkey',
          num_associated_contacts: '1',
          createdate: '1603855713105',
          amount_in_home_currency: '0',
          hs_is_closed: 'true',
          days_to_close: '0',
          hs_deal_stage_probability: '1',
          trial_end_date: '1730073600000',
          ix_chargify_id: '36831983',
          hs_closed_amount: '0',
          hs_analytics_source: 'OFFLINE',
          trial_type: 'Time Limited',
          hs_createdate: '1603855713105',
          hs_projected_amount: '0',
          hs_projected_amount_in_home_currency: '0',
          dealtype: 'newbusiness',
          amount: '0',
          closedate: '1603855750089',
          ixc_account_id: 'e0a01769-c8ed-4090-bd24-9c1b2241fb75',
          ix_chargify_link: 'https://inspectionxpert-test.chargify.com/subscriptions/36831983',
          pipeline: '5743684',
          hs_lastmodifieddate: '1603855752585',
          dealstage: '5743690',
          hs_object_id: '3257618779',
          trial_started_date: '1603843200000',
          hs_analytics_source_data_1: 'API',
          crm: 'hubspot',
          crmType: 'deal',
          crmId: 3257618779,
        });

        const result = await subWorker.convertTrialToSubscription({
          subscriptionId: '36831983',
          data: { termsAcceptedOn: null },
          components: mockComponents,
        });

        expect(result).toBeDefined();
        expect(result.success).toBe(true);
        expect(result.bsSubscription).toBeDefined();
        expect(result.crmDeal).toBeDefined();

        expect(billingClientMock.updateSubscription).toHaveBeenCalledTimes(1);
        expect(crmClientMock.updateDeal).toHaveBeenCalledTimes(1);

        expect(loggerMock.error).toHaveBeenCalledTimes(0);

        getSubscriptionForIdMock.mockRestore();
        updateComponentsMock.mockRestore();
      }, 30000); // Wait 30 seconds for async timeouts

      it('should set the correct defaults when no new values are passed', async () => {
        const getSubscriptionForIdMock = jest.spyOn(subWorker, 'getSubscriptionForId').mockImplementation(() => ({ ...MOCK_SUBSCRIPTION, state: 'trialing' }));
        // Default Component
        billingClientMock.getCurrentPrice.mockResolvedValueOnce({
          id: 1241559,
          default: false,
          name: '20210419_annually',
          pricing_scheme: 'stairstep',
          component_id: 1213884,
          handle: 'v20210419_annually',
          archived_at: null,
          created_at: '2021-04-19T10:39:20-04:00',
          updated_at: '2021-04-19T10:39:20-04:00',
          prices: [
            {
              id: 2275421,
              component_id: 1213884,
              starting_quantity: 1,
              ending_quantity: 1,
              unit_price: '1500.0',
              price_point_id: 1241559,
              formatted_unit_price: '$1,500.00',
              segment_id: null,
            },
            {
              id: 2275422,
              component_id: 1213884,
              starting_quantity: 2,
              ending_quantity: 3,
              unit_price: '3000.0',
              price_point_id: 1241559,
              formatted_unit_price: '$3,000.00',
              segment_id: null,
            },
            {
              id: 2275423,
              component_id: 1213884,
              starting_quantity: 4,
              ending_quantity: 5,
              unit_price: '4500.0',
              price_point_id: 1241559,
              formatted_unit_price: '$4,500.00',
              segment_id: null,
            },
          ],
        });
        // Product Price
        billingClientMock.getCurrentPrice.mockResolvedValueOnce(MOCK_PRODUCT_PRICES[0]);

        // Update Components
        const updateComponentsMock = jest.spyOn(subWorker, 'updateComponents').mockImplementation(() => true);

        // Updates
        billingClientMock.updateSubscription.mockResolvedValueOnce({
          id: 36831983,
          state: 'trialing',
          trial_started_at: '2020-10-27T23:28:34-04:00',
          trial_ended_at: '2020-10-27T23:29:10-04:00',
          activated_at: null,
          created_at: '2020-10-27T23:28:34-04:00',
          updated_at: '2020-10-27T23:29:10-04:00',
          expires_at: '2021-10-27T00:00:00-04:00',
          balance_in_cents: 0,
          current_period_ends_at: '2020-10-27T23:29:10-04:00',
          next_assessment_at: '2020-10-27T23:29:10-04:00',
          canceled_at: null,
          cancellation_message: null,
          next_product_id: null,
          next_product_handle: null,
          cancel_at_end_of_period: false,
          payment_collection_method: 'remittance',
          snap_day: null,
          cancellation_method: null,
          current_period_started_at: '2020-10-27T23:28:34-04:00',
          previous_state: 'trialing',
          signup_payment_id: 433903604,
          signup_revenue: '0.00',
          delayed_cancel_at: null,
          coupon_code: null,
          total_revenue_in_cents: 0,
          product_price_in_cents: 0,
          product_version_number: 1,
          payment_type: null,
          referral_code: null,
          coupon_use_count: null,
          coupon_uses_allowed: null,
          reason_code: null,
          automatically_resume_at: null,
          coupon_codes: [],
          offer_id: null,
          payer_id: 37716139,
          current_billing_amount_in_cents: 0,
          receives_invoice_emails: null,
          product_price_point_id: 969460,
          next_product_price_point_id: null,
          credit_balance_in_cents: 0,
          prepayment_balance_in_cents: 0,
          net_terms: null,
          stored_credential_transaction_id: null,
          locale: null,
          reference: '3257618779',
          currency: 'USD',
          on_hold_at: null,
          scheduled_cancellation_at: null,
          customer: {
            id: 37716139,
            first_name: 'Dev',
            last_name: 'Monkey',
            organization: null,
            email: 'dev.mail.monkey@gmail.com',
            created_at: '2020-10-27T23:28:34-04:00',
            updated_at: '2020-10-27T23:28:34-04:00',
            reference: 'e0a01769-c8ed-4090-bd24-9c1b2241fb75',
            address: null,
            address_2: null,
            city: null,
            state: null,
            zip: null,
            country: null,
            phone: '',
            portal_invite_last_sent_at: null,
            portal_invite_last_accepted_at: null,
            verified: false,
            portal_customer_created_at: null,
            vat_number: null,
            cc_emails: null,
            tax_exempt: false,
            parent_id: null,
            locale: null,
          },
          product: {
            id: 5233891,
            name: 'Standard',
            handle: 'ix_standard',
            description: '',
            accounting_code: '',
            request_credit_card: true,
            expiration_interval: null,
            expiration_interval_unit: 'never',
            created_at: '2020-05-21T14:21:34-04:00',
            updated_at: '2020-10-24T15:08:33-04:00',
            price_in_cents: 0,
            interval: 1,
            interval_unit: 'month',
            initial_charge_in_cents: null,
            trial_price_in_cents: null,
            trial_interval: null,
            trial_interval_unit: 'month',
            archived_at: null,
            require_credit_card: false,
            return_params: '',
            taxable: true,
            update_return_url: '',
            tax_code: 'D0000000',
            initial_charge_after_trial: false,
            version_number: 1,
            update_return_params: '',
            default_product_price_point_id: 969460,
            request_billing_address: false,
            require_billing_address: false,
            require_shipping_address: false,
            product_price_point_id: 969460,
            product_price_point_name: 'Standard',
            product_price_point_handle: 'ix_standard',
            product_family: { id: 1526431, name: 'IX3', description: '', handle: 'inspectionxpert-cloud', accounting_code: null, created_at: '2020-05-21T14:21:33-04:00', updated_at: '2020-09-01T16:25:44-04:00' },
            public_signup_pages: [],
          },
          group: null,
        });
        crmClientMock.updateDeal.mockResolvedValueOnce({
          hs_closed_amount_in_home_currency: '0',
          dealname: 'Acme Corp : Dev Monkey',
          num_associated_contacts: '1',
          createdate: '1603855713105',
          amount_in_home_currency: '0',
          hs_is_closed: 'true',
          days_to_close: '0',
          hs_deal_stage_probability: '1',
          trial_end_date: '1730073600000',
          ix_chargify_id: '36831983',
          hs_closed_amount: '0',
          hs_analytics_source: 'OFFLINE',
          trial_type: 'Time Limited',
          hs_createdate: '1603855713105',
          hs_projected_amount: '0',
          hs_projected_amount_in_home_currency: '0',
          dealtype: 'newbusiness',
          amount: '0',
          closedate: '1603855750089',
          ixc_account_id: 'e0a01769-c8ed-4090-bd24-9c1b2241fb75',
          ix_chargify_link: 'https://inspectionxpert-test.chargify.com/subscriptions/36831983',
          pipeline: '5743684',
          hs_lastmodifieddate: '1603855752585',
          dealstage: '5743690',
          hs_object_id: '3257618779',
          trial_started_date: '1603843200000',
          hs_analytics_source_data_1: 'API',
          crm: 'hubspot',
          crmType: 'deal',
          crmId: 3257618779,
        });

        // Call monthly
        let result = await subWorker.convertTrialToSubscription({
          subscriptionId: '36831983',
          interval: 'Monthly',
          data: { termsAcceptedOn: null },
        });

        expect(result).toBeDefined();
        expect(result.success).toBe(true);
        expect(result.bsSubscription).toBeDefined();
        expect(result.crmDeal).toBeDefined();

        expect(billingClientMock.getCurrentPrice).toHaveBeenCalledTimes(2);
        expect(billingClientMock.getCurrentPrice).toHaveBeenCalledWith('Monthly', MOCK_PRODUCT_CATALOG['inspectionxpert-cloud'].components.paid_user_seat_ss.prices);

        expect(billingClientMock.updateSubscription).toHaveBeenCalledTimes(1);
        expect(crmClientMock.updateDeal).toHaveBeenCalledTimes(1);

        expect(loggerMock.error).toHaveBeenCalledTimes(0);

        // Second set of mock returns
        // Default Component
        billingClientMock.getCurrentPrice.mockResolvedValueOnce({
          id: 1241559,
          default: false,
          name: '20210419_annually',
          pricing_scheme: 'stairstep',
          component_id: 1213884,
          handle: 'v20210419_annually',
          archived_at: null,
          created_at: '2021-04-19T10:39:20-04:00',
          updated_at: '2021-04-19T10:39:20-04:00',
          prices: [
            {
              id: 2275421,
              component_id: 1213884,
              starting_quantity: 1,
              ending_quantity: 1,
              unit_price: '1500.0',
              price_point_id: 1241559,
              formatted_unit_price: '$1,500.00',
              segment_id: null,
            },
            {
              id: 2275422,
              component_id: 1213884,
              starting_quantity: 2,
              ending_quantity: 3,
              unit_price: '3000.0',
              price_point_id: 1241559,
              formatted_unit_price: '$3,000.00',
              segment_id: null,
            },
            {
              id: 2275423,
              component_id: 1213884,
              starting_quantity: 4,
              ending_quantity: 5,
              unit_price: '4500.0',
              price_point_id: 1241559,
              formatted_unit_price: '$4,500.00',
              segment_id: null,
            },
          ],
        });
        // Product Price
        billingClientMock.getCurrentPrice.mockResolvedValueOnce(MOCK_PRODUCT_PRICES[0]);
        // Updates
        billingClientMock.updateSubscription.mockResolvedValueOnce({
          id: 36831983,
          state: 'trialing',
          trial_started_at: '2020-10-27T23:28:34-04:00',
          trial_ended_at: '2020-10-27T23:29:10-04:00',
          activated_at: null,
          created_at: '2020-10-27T23:28:34-04:00',
          updated_at: '2020-10-27T23:29:10-04:00',
          expires_at: '2021-10-27T00:00:00-04:00',
          balance_in_cents: 0,
          current_period_ends_at: '2020-10-27T23:29:10-04:00',
          next_assessment_at: '2020-10-27T23:29:10-04:00',
          canceled_at: null,
          cancellation_message: null,
          next_product_id: null,
          next_product_handle: null,
          cancel_at_end_of_period: false,
          payment_collection_method: 'remittance',
          snap_day: null,
          cancellation_method: null,
          current_period_started_at: '2020-10-27T23:28:34-04:00',
          previous_state: 'trialing',
          signup_payment_id: 433903604,
          signup_revenue: '0.00',
          delayed_cancel_at: null,
          coupon_code: null,
          total_revenue_in_cents: 0,
          product_price_in_cents: 0,
          product_version_number: 1,
          payment_type: null,
          referral_code: null,
          coupon_use_count: null,
          coupon_uses_allowed: null,
          reason_code: null,
          automatically_resume_at: null,
          coupon_codes: [],
          offer_id: null,
          payer_id: 37716139,
          current_billing_amount_in_cents: 0,
          receives_invoice_emails: null,
          product_price_point_id: 969460,
          next_product_price_point_id: null,
          credit_balance_in_cents: 0,
          prepayment_balance_in_cents: 0,
          net_terms: null,
          stored_credential_transaction_id: null,
          locale: null,
          reference: '3257618779',
          currency: 'USD',
          on_hold_at: null,
          scheduled_cancellation_at: null,
          customer: {
            id: 37716139,
            first_name: 'Dev',
            last_name: 'Monkey',
            organization: null,
            email: 'dev.mail.monkey@gmail.com',
            created_at: '2020-10-27T23:28:34-04:00',
            updated_at: '2020-10-27T23:28:34-04:00',
            reference: 'e0a01769-c8ed-4090-bd24-9c1b2241fb75',
            address: null,
            address_2: null,
            city: null,
            state: null,
            zip: null,
            country: null,
            phone: '',
            portal_invite_last_sent_at: null,
            portal_invite_last_accepted_at: null,
            verified: false,
            portal_customer_created_at: null,
            vat_number: null,
            cc_emails: null,
            tax_exempt: false,
            parent_id: null,
            locale: null,
          },
          product: {
            id: 5233891,
            name: 'Standard',
            handle: 'ix_standard',
            description: '',
            accounting_code: '',
            request_credit_card: true,
            expiration_interval: null,
            expiration_interval_unit: 'never',
            created_at: '2020-05-21T14:21:34-04:00',
            updated_at: '2020-10-24T15:08:33-04:00',
            price_in_cents: 0,
            interval: 1,
            interval_unit: 'month',
            initial_charge_in_cents: null,
            trial_price_in_cents: null,
            trial_interval: null,
            trial_interval_unit: 'month',
            archived_at: null,
            require_credit_card: false,
            return_params: '',
            taxable: true,
            update_return_url: '',
            tax_code: 'D0000000',
            initial_charge_after_trial: false,
            version_number: 1,
            update_return_params: '',
            default_product_price_point_id: 969460,
            request_billing_address: false,
            require_billing_address: false,
            require_shipping_address: false,
            product_price_point_id: 969460,
            product_price_point_name: 'Standard',
            product_price_point_handle: 'ix_standard',
            product_family: { id: 1526431, name: 'IX3', description: '', handle: 'inspectionxpert-cloud', accounting_code: null, created_at: '2020-05-21T14:21:33-04:00', updated_at: '2020-09-01T16:25:44-04:00' },
            public_signup_pages: [],
          },
          group: null,
        });
        crmClientMock.updateDeal.mockResolvedValueOnce({
          hs_closed_amount_in_home_currency: '0',
          dealname: 'Acme Corp : Dev Monkey',
          num_associated_contacts: '1',
          createdate: '1603855713105',
          amount_in_home_currency: '0',
          hs_is_closed: 'true',
          days_to_close: '0',
          hs_deal_stage_probability: '1',
          trial_end_date: '1730073600000',
          ix_chargify_id: '36831983',
          hs_closed_amount: '0',
          hs_analytics_source: 'OFFLINE',
          trial_type: 'Time Limited',
          hs_createdate: '1603855713105',
          hs_projected_amount: '0',
          hs_projected_amount_in_home_currency: '0',
          dealtype: 'newbusiness',
          amount: '0',
          closedate: '1603855750089',
          ixc_account_id: 'e0a01769-c8ed-4090-bd24-9c1b2241fb75',
          ix_chargify_link: 'https://inspectionxpert-test.chargify.com/subscriptions/36831983',
          pipeline: '5743684',
          hs_lastmodifieddate: '1603855752585',
          dealstage: '5743690',
          hs_object_id: '3257618779',
          trial_started_date: '1603843200000',
          hs_analytics_source_data_1: 'API',
          crm: 'hubspot',
          crmType: 'deal',
          crmId: 3257618779,
        });

        // Call without inteval specified
        result = await subWorker.convertTrialToSubscription({
          subscriptionId: '36831983',
          data: { termsAcceptedOn: null },
        });

        expect(result).toBeDefined();
        expect(result.success).toBe(true);
        expect(result.bsSubscription).toBeDefined();
        expect(result.crmDeal).toBeDefined();

        expect(billingClientMock.getCurrentPrice).toHaveBeenCalledTimes(4);
        expect(billingClientMock.getCurrentPrice).toHaveBeenCalledWith('Annually', MOCK_PRODUCT_CATALOG['inspectionxpert-cloud'].components.paid_user_seat_ss.prices);

        expect(billingClientMock.updateSubscription).toHaveBeenCalledTimes(2);
        expect(crmClientMock.updateDeal).toHaveBeenCalledTimes(2);

        expect(loggerMock.error).toHaveBeenCalledTimes(0);

        getSubscriptionForIdMock.mockRestore();
        updateComponentsMock.mockRestore();
      }, 30000); // Wait 30 seconds for async timeouts
    });

    describe('#expireTrial()', () => {
      it('should throw an error if required input is not provided', async () => {
        await expect(subWorker.expireTrial({})).rejects.toThrow();
        expect(loggerMock.error).toHaveBeenCalledTimes(1);
      });

      it('should throw an error if the request fails', async () => {
        // This test fails when using the real hubspot API, because the record is created successfully
        if (subWorker.isMocked) {
          // hubspotMock.companies.create.mockRejectedValueOnce(new Error('An error message.'));
          await expect(subWorker.expireTrial({ name: 'foo' })).rejects.toThrow();
          expect(loggerMock.error).toHaveBeenCalledTimes(1);
        }
      });

      it('should set the CRM deal for the trial to closed lost when the request succeeds', async () => {
        billingClientMock.getCustomerByReference.mockResolvedValueOnce({
          first_name: 'Dev',
          last_name: 'Monkey',
          email: 'dev.mail.monkey@gmail.com',
          cc_emails: null,
          organization: null,
          reference: 'e0a01769-c8ed-4090-bd24-9c1b2241fb75',
          id: 37451677,
          created_at: '2020-10-14T23:04:33-04:00',
          updated_at: '2020-10-14T23:04:33-04:00',
          address: null,
          address_2: null,
          city: null,
          state: null,
          zip: null,
          country: null,
          phone: '',
          verified: false,
          portal_customer_created_at: null,
          portal_invite_last_sent_at: null,
          portal_invite_last_accepted_at: null,
          tax_exempt: false,
          vat_number: null,
          parent_id: null,
          locale: null,
          default_subscription_group_uid: null,
        });

        billingClientMock.getSubscriptionCustomFields.mockResolvedValueOnce([
          {
            id: 15809160,
            value: '2f84e6ec-2615-4c55-b19b-b454cc6fa1bc',
            resource_id: 36574344,
            deleted_at: null,
            name: 'ixc_subscription_id',
          },
        ]);

        billingClientMock.getSubscriptionsForCustomer.mockResolvedValueOnce([
          {
            id: 36574344,
            state: 'active',
            trial_started_at: null,
            trial_ended_at: null,
            activated_at: '2020-10-14T23:04:35-04:00',
            created_at: '2020-10-14T23:04:33-04:00',
            updated_at: '2020-10-14T23:04:35-04:00',
            expires_at: '2020-10-21T23:04:33-04:00',
            balance_in_cents: 0,
            current_period_ends_at: '2020-10-21T23:04:33-04:00',
            next_assessment_at: '2020-10-21T23:04:33-04:00',
            canceled_at: null,
            cancellation_message: null,
            next_product_id: null,
            next_product_handle: null,
            cancel_at_end_of_period: false,
            payment_collection_method: 'remittance',
            snap_day: null,
            cancellation_method: null,
            current_period_started_at: '2020-10-14T23:04:33-04:00',
            previous_state: 'active',
            signup_payment_id: 430337209,
            signup_revenue: '0.00',
            delayed_cancel_at: null,
            coupon_code: null,
            total_revenue_in_cents: 0,
            product_price_in_cents: 0,
            product_version_number: 1,
            payment_type: null,
            referral_code: null,
            coupon_use_count: null,
            coupon_uses_allowed: null,
            reason_code: null,
            automatically_resume_at: null,
            coupon_codes: [],
            offer_id: null,
            payer_id: 37451677,
            receives_invoice_emails: null,
            product_price_point_id: 969487,
            next_product_price_point_id: null,
            credit_balance_in_cents: 0,
            prepayment_balance_in_cents: 0,
            net_terms: null,
            stored_credential_transaction_id: null,
            locale: null,
            reference: '3159749714',
            currency: 'USD',
            on_hold_at: null,
            customer: {
              id: 37451444,
              first_name: 'Dev',
              last_name: 'Monkey',
              organization: null,
              email: 'dev.mail.monkey@gmail.com',
              created_at: '2020-10-14T22:41:32-04:00',
              updated_at: '2020-10-14T22:41:32-04:00',
              reference: 'e0a01769-c8ed-4090-bd24-9c1b2241fb75',
              address: null,
              address_2: null,
              city: null,
              state: null,
              zip: null,
              country: null,
              phone: '',
              portal_invite_last_sent_at: null,
              portal_invite_last_accepted_at: null,
              verified: false,
              portal_customer_created_at: null,
              vat_number: null,
              cc_emails: null,
              tax_exempt: false,
              parent_id: null,
              locale: null,
            },
            product: {
              id: 5256309,
              name: 'Standard',
              handle: 'ix_standard',
              description: '',
              accounting_code: '',
              request_credit_card: true,
              expiration_interval: 7,
              expiration_interval_unit: 'day',
              created_at: '2020-09-01T20:56:13-04:00',
              updated_at: '2020-10-11T23:03:57-04:00',
              price_in_cents: 0,
              interval: 7,
              interval_unit: 'day',
              initial_charge_in_cents: null,
              trial_price_in_cents: null,
              trial_interval: null,
              trial_interval_unit: 'month',
              archived_at: null,
              require_credit_card: false,
              return_params: '',
              taxable: false,
              update_return_url: '',
              tax_code: '',
              initial_charge_after_trial: false,
              version_number: 1,
              update_return_params: '',
              default_product_price_point_id: 969487,
              request_billing_address: false,
              require_billing_address: false,
              require_shipping_address: false,
              product_price_point_id: 969487,
              product_price_point_name: 'Original',
              product_price_point_handle: 'ix_standard',
              product_family: { id: 1526431, name: 'IX3', description: '', handle: 'inspectionxpert-cloud', accounting_code: null, created_at: '2020-05-21T14:21:33-04:00', updated_at: '2020-09-01T16:25:44-04:00' },
              public_signup_pages: [],
            },
            group: null,
          },
        ]);

        crmClientMock.updateDeal.mockResolvedValueOnce({
          hs_closed_amount_in_home_currency: '0',
          dealname: 'Acme Corp : Dev Monkey',
          pipeline: '5743684',
          dealstage: '5743691',
          closedate: '1606867200000',
          num_associated_contacts: '1',
          createdate: '1602731071850',
          hs_is_closed: 'true',
          ixc_account_id: 'e0a01769-c8ed-4090-bd24-9c1b2241fb75',
          days_to_close: '47',
          hs_deal_stage_probability: '0',
          ix_chargify_link: 'https://inspectionxpert-test.chargify.com/subscriptions/36574344',
          ix_chargify_id: '36574344',
          hs_closed_amount: '0',
          hs_analytics_source: 'OFFLINE',
          hs_lastmodifieddate: '1602735319155',
          hs_createdate: '1602731071850',
          hs_object_id: '3159749714',
          hs_analytics_source_data_1: 'API',
          dealtype: 'newbusiness',
          crm: 'hubspot',
          crmType: 'deal',
          crmId: 3159749714,
        });

        const result = await subWorker.expireTrial({
          accountId: 'e0a01769-c8ed-4090-bd24-9c1b2241fb75',
        });
        expect(result).toBeDefined();
        expect(result.success).toBe(true);
        expect(result.crmDeal.dealstage).toBe('5743691'); // 'CLOSED LOST': 5743691
        expect(loggerMock.error).toHaveBeenCalledTimes(0);

        expect(loggerMock.error).toHaveBeenCalledTimes(0);
      }, 30000); // Wait 30 seconds for asyn timeouts
    });
  });

  describe('Universal Functions', () => {
    describe('#setIxcDbId', () => {
      it('throws an error if missing required parameter', async () => {
        await expect(subWorker.setIxcDbId({})).rejects.toThrow();
        expect(loggerMock.error).toHaveBeenCalledTimes(1);
      });

      it('returns the updated custom field', async () => {
        billingClientMock.updateSubscriptionCustomFields.mockResolvedValueOnce('something');
        const result = await subWorker.setIxcDbId({ dbId: 'some', subscriptionId: 'some' });
        expect(result).toBe('something');
      });
    });

    describe('#updateComponents', () => {
      it('throws an error if missing required parameter', async () => {
        await expect(subWorker.updateComponents({})).rejects.toThrow();
        expect(loggerMock.error).toHaveBeenCalledTimes(1);
      });

      it('throws an error if new component is missing required parameter', async () => {
        await expect(
          subWorker.updateComponents({
            components: [{}],
            subscriptionComponents: [],
            subscriptionId: 'some',
          })
        ).rejects.toThrow();
        expect(loggerMock.error).toHaveBeenCalledTimes(1);
      });

      it('throws an error if original component is missing required parameter', async () => {
        await expect(
          subWorker.updateComponents({
            components: [{ component_id: 'some', price_point: 'some', quantity: 2 }],
            subscriptionComponents: [{ component_id: 'some' }],
            subscriptionId: 'some',
          })
        ).rejects.toThrow();
        expect(loggerMock.error).toHaveBeenCalledTimes(1);
      });

      it('calls update quantity component on the billing client when quantity is different', async () => {
        billingClientMock.updateSubscriptionQuantity.mockResolvedValueOnce('something');
        await subWorker.updateComponents({
          components: [{ component_id: 'some', price_point: 'some', quantity: 2 }],
          subscriptionComponents: [{ component_id: 'some', price_point_id: 'some', allocated_quantity: 1 }],
          subscriptionId: 'some',
        });
        expect(billingClientMock.updateSubscriptionQuantity).toHaveBeenCalledTimes(1);
        expect(billingClientMock.updateComponentPrice).toHaveBeenCalledTimes(0);
        expect(loggerMock.error).toHaveBeenCalledTimes(0);
        expect(loggerMock.debug).toHaveBeenCalledTimes(3);
      });

      it('calls update price component on the billing client when price point is different', async () => {
        billingClientMock.updateComponentPrice.mockResolvedValueOnce('something');
        await subWorker.updateComponents({
          components: [{ component_id: 'some', price_point: 'some', quantity: 2 }],
          subscriptionComponents: [{ component_id: 'some', price_point_id: 'else', allocated_quantity: 2 }],
          subscriptionId: 'some',
        });
        expect(billingClientMock.updateSubscriptionQuantity).toHaveBeenCalledTimes(0);
        expect(billingClientMock.updateComponentPrice).toHaveBeenCalledTimes(1);
        expect(loggerMock.error).toHaveBeenCalledTimes(0);
        expect(loggerMock.debug).toHaveBeenCalledTimes(3);
      });

      it('logs before and after completion', async () => {
        await subWorker.updateComponents({
          components: [{ component_id: 'some', price_point: 'some', quantity: 1 }],
          subscriptionComponents: [{ component_id: 'some', price_point_id: 'some', allocated_quantity: 1 }],
          subscriptionId: 'some',
        });
        expect(billingClientMock.updateSubscriptionQuantity).toHaveBeenCalledTimes(0);
        expect(billingClientMock.updateComponentPrice).toHaveBeenCalledTimes(0);
        expect(loggerMock.error).toHaveBeenCalledTimes(0);
        expect(loggerMock.debug).toHaveBeenCalledTimes(3);
      });
    });

    describe('#generateInvoice', () => {
      it('throws an error if missing required parameter', async () => {
        await expect(subWorker.generateInvoice({})).rejects.toThrow();
        expect(loggerMock.error).toHaveBeenCalledTimes(1);
      });

      it('logs twice when successfully running', async () => {
        await subWorker.generateInvoice({
          components: [{ component_id: 'some', price_point: 'some', quantity: 2 }],
          bsSubscription: { id: 'id', customer: { email: 'test@email.com' } },
          memo: 'memo',
          startDate: moment.utc('2021-05-28T23:59:59.999+00:00'),
          endDate: moment.utc('2021-05-28T23:59:59.999+00:00'),
          interval: 'Annual',
        });
        expect(loggerMock.debug).toHaveBeenCalledTimes(2);
      });
    });

    describe('#voidAllOpenInvoices', () => {
      it('throws an error if missing required parameter', async () => {
        await expect(subWorker.voidAllOpenInvoices({})).rejects.toThrow();
        expect(loggerMock.error).toHaveBeenCalledTimes(1);
      });

      it('logs before and after completion', async () => {
        billingClientMock.getInvoicesForSubscription.mockResolvedValueOnce([]);
        await subWorker.voidAllOpenInvoices({ subscriptionId: 'some' });
        expect(loggerMock.error).toHaveBeenCalledTimes(0);
        expect(loggerMock.debug).toHaveBeenCalledTimes(2);
      });

      it('throws an error if invoice is missing required parameter', async () => {
        billingClientMock.getInvoicesForSubscription.mockResolvedValueOnce([{}]);
        await expect(subWorker.voidAllOpenInvoices({ subscriptionId: 'some' })).rejects.toThrow();
        expect(loggerMock.error).toHaveBeenCalledTimes(1);
      });

      it('logs each invoice voided', async () => {
        billingClientMock.getInvoicesForSubscription.mockResolvedValueOnce([{ uid: 'some' }]);
        billingClientMock.voidInvoice.mockResolvedValueOnce('some');
        await subWorker.voidAllOpenInvoices({ subscriptionId: 'some' });
        expect(loggerMock.error).toHaveBeenCalledTimes(0);
        expect(billingClientMock.voidInvoice).toHaveBeenCalledTimes(1);
        expect(loggerMock.debug).toHaveBeenCalledTimes(3);
      });
    });

    describe('#updateSubscription', () => {
      it('throws an error if missing required parameter', async () => {
        await expect(subWorker.updateSubscription({})).rejects.toThrow();
        expect(loggerMock.error).toHaveBeenCalledTimes(1);
      });

      it('updates billing period', async () => {
        const getSubscriptionForIdMock = jest.spyOn(subWorker, 'getSubscriptionForId').mockImplementation(() => ({
          id: 'some',
          billing_interval: 'Annually',
          activated_at: '2025-01-01',
          trial_ended_at: '2025-01-01',
          current_period_started_at: '2025-01-01',
          product: { product_family: { handle: 'inspectionxpert-cloud' }, handle: 'ix_standard' },
        }));

        billingClientMock.getCatalog.mockResolvedValueOnce({ 'inspectionxpert-cloud': { products: { ix_standard: { prices: [] } } } });
        await subWorker.loadCatalog();
        billingClientMock.updateSubscriptionCustomFields.mockResolvedValueOnce(true);
        billingClientMock.getCurrentPrice.mockResolvedValueOnce({ handle: 'some' });
        billingClientMock.updateSubscription.mockResolvedValueOnce(true);

        await subWorker.updateSubscription({ subscriptionId: 'some', billingPeriod: 'Monthly' });
        expect(billingClientMock.updateSubscriptionCustomFields).toHaveBeenCalledTimes(1);
        expect(billingClientMock.updateSubscription).toHaveBeenCalledTimes(1);

        getSubscriptionForIdMock.mockRestore();
      });

      it('updates product', async () => {
        const getSubscriptionForIdMock = jest.spyOn(subWorker, 'getSubscriptionForId').mockImplementation(() => ({ id: 'some', product: { id: 'some' } }));
        billingClientMock.updateSubscription.mockResolvedValueOnce(true);

        await subWorker.updateSubscription({ subscriptionId: 'some', providerProductId: 'new' });
        expect(billingClientMock.updateSubscription).toHaveBeenCalledTimes(1);

        getSubscriptionForIdMock.mockRestore();
      });

      it('sets subscription to remittance', async () => {
        const getSubscriptionForIdMock = jest.spyOn(subWorker, 'getSubscriptionForId').mockImplementation(() => ({ id: 'some', payment_collection_method: 'automatic' }));
        billingClientMock.updateSubscription.mockResolvedValueOnce(true);

        await subWorker.updateSubscription({ subscriptionId: 'some', remittance: true });
        expect(billingClientMock.updateSubscription).toHaveBeenCalledTimes(1);

        getSubscriptionForIdMock.mockRestore();
      });

      it('updates components', async () => {
        const getSubscriptionForIdMock = jest.spyOn(subWorker, 'getSubscriptionForId').mockImplementation(() => ({ id: 'some', components: [{ component_id: 'some', price_point_id: 'some', allocated_quantity: 1 }] }));
        billingClientMock.updateSubscriptionQuantity.mockResolvedValueOnce('something');

        await subWorker.updateSubscription({ subscriptionId: 'some', components: [{ component_id: 'some', price_point: 'some', quantity: 2 }] });
        expect(billingClientMock.updateSubscriptionQuantity).toHaveBeenCalledTimes(1);

        getSubscriptionForIdMock.mockRestore();
      });

      it('updates the CRM', async () => {
        const getSubscriptionForIdMock = jest.spyOn(subWorker, 'getSubscriptionForId').mockImplementation(() => ({ id: 'some', reference: 'some' }));
        const calculateDealValueMock = jest.spyOn(subWorker, 'calculateDealValue').mockImplementation(() => 5);
        crmClientMock.updateDeal.mockResolvedValueOnce('some');

        await subWorker.updateSubscription({ subscriptionId: 'some' });
        expect(crmClientMock.updateDeal).toHaveBeenCalledTimes(1);

        getSubscriptionForIdMock.mockRestore();
        calculateDealValueMock.mockRestore();
      });

      it('returns the updated subscription', async () => {
        const getSubscriptionForIdMock = jest.spyOn(subWorker, 'getSubscriptionForId').mockImplementation(() => ({ id: 'some' }));
        const result = await subWorker.updateSubscription({ subscriptionId: 'some' });
        expect(result.update).toMatchObject({ id: 'some' });
        expect(result.success).toBeTruthy();

        getSubscriptionForIdMock.mockRestore();
      });
    });

    describe('#addContactToAccount()', () => {
      it('should throw an error if required input is not provided', async () => {
        await expect(subWorker.addContactToAccount()).rejects.toThrow();
        await expect(subWorker.addContactToAccount({ account: { domain: 'foo' } })).rejects.toThrow();
        await expect(subWorker.addContactToAccount({ user: { name: 'foo' } })).rejects.toThrow();
        expect(loggerMock.error).toHaveBeenCalledTimes(3);
      });

      it('should throw an error if the request fails', async () => {
        // This test fails when using the real hubspot API, because first
        // runs will create records that are found and returned by subsequent runs.
        if (subWorker.isMocked) {
          // hubspotMock.deals.associate.mockRejectedValueOnce(new Error('An error message.'));
          await expect(subWorker.addContactToAccount({ account: TEST_ACCOUNT, user: TEST_USER })).rejects.toThrow();
          expect(loggerMock.error).toHaveBeenCalledTimes(1);
        }
      });

      it('should associate the specified contact with the deal', async () => {
        // hubspotMock.deals.associate.mockResolvedValueOnce({ success: true });
        billingClientMock.getCustomerByReference.mockResolvedValueOnce({
          first_name: 'Dev',
          last_name: 'Monkey',
          email: 'dev.mail.monkey@gmail.com',
          cc_emails: null,
          organization: null,
          reference: 'e0a01769-c8ed-4090-bd24-9c1b2241fb75',
          id: 37711536,
          created_at: '2020-10-27T18:24:19-04:00',
          updated_at: '2020-10-27T18:24:19-04:00',
          address: null,
          address_2: null,
          city: null,
          state: null,
          zip: null,
          country: null,
          phone: '',
          verified: false,
          portal_customer_created_at: null,
          portal_invite_last_sent_at: null,
          portal_invite_last_accepted_at: null,
          tax_exempt: false,
          vat_number: null,
          parent_id: null,
          locale: null,
          default_subscription_group_uid: null,
        });

        billingClientMock.getMetadataForCustomerWithId.mockResolvedValue({
          ixc_account_id: '83215576-359c-42e6-895f-75aa1b2af61a',
          hubspot_company_id: '5929423486',
        });

        crmClientMock.getCompanyById.mockResolvedValueOnce({
          hs_num_open_deals: '0',
          website: 'qualityxpert.com',
          num_associated_contacts: '1',
          hs_analytics_first_timestamp: '1619189627933',
          description: 'Please enter your company URL below.',
          createdate: '1619189627568',
          ixc_account_id: '83215576-359c-42e6-895f-75aa1b2af61a',
          hs_num_blockers: '0',
          ix_chargify_link: 'https://inspectionxpert-test.chargify.com/customers/42658762',
          hs_analytics_num_visits: '0',
          ix_chargify_id: '42658762',
          first_contact_createdate: '1619189627933',
          hs_lastmodifieddate: '1619189676923',
          hs_analytics_source: 'OFFLINE',
          hs_predictivecontactscore_v2: '2.11',
          hs_num_decision_makers: '0',
          domain: 'qualityxpert.com',
          name: 'JDixA Org',
          hs_object_id: '5929423486',
          hs_num_contacts_with_buying_roles: '0',
          hs_num_child_companies: '0',
          hs_analytics_num_page_views: '0',
          hs_analytics_source_data_1: 'API',
          crm: 'hubspot',
          crmType: 'company',
          crmId: 5929423486,
        });

        billingClientMock.getSubscriptionsForCustomer.mockResolvedValueOnce([
          {
            id: 36827260,
            state: 'active',
            trial_started_at: null,
            trial_ended_at: null,
            activated_at: '2020-10-27T18:24:20-04:00',
            created_at: '2020-10-27T18:24:19-04:00',
            updated_at: '2020-10-27T18:59:09-04:00',
            expires_at: null,
            balance_in_cents: 24991,
            current_period_ends_at: '2020-11-27T17:24:19-05:00',
            next_assessment_at: '2020-11-27T17:24:19-05:00',
            canceled_at: null,
            cancellation_message: null,
            next_product_id: null,
            next_product_handle: null,
            cancel_at_end_of_period: false,
            payment_collection_method: 'remittance',
            snap_day: null,
            cancellation_method: null,
            current_period_started_at: '2020-10-27T18:24:19-04:00',
            previous_state: 'active',
            signup_payment_id: 433830000,
            signup_revenue: '0.00',
            delayed_cancel_at: null,
            coupon_code: null,
            total_revenue_in_cents: 0,
            product_price_in_cents: 0,
            product_version_number: 1,
            payment_type: null,
            referral_code: null,
            coupon_use_count: null,
            coupon_uses_allowed: null,
            reason_code: null,
            automatically_resume_at: null,
            coupon_codes: [],
            offer_id: null,
            payer_id: 37711536,
            receives_invoice_emails: null,
            product_price_point_id: 969460,
            next_product_price_point_id: null,
            credit_balance_in_cents: 0,
            prepayment_balance_in_cents: 0,
            net_terms: null,
            stored_credential_transaction_id: null,
            locale: null,
            reference: '3252998005',
            currency: 'USD',
            on_hold_at: null,
            scheduled_cancellation_at: null,
            customer: {
              id: 37711536,
              first_name: 'Dev',
              last_name: 'Monkey',
              organization: null,
              email: 'dev.mail.monkey@gmail.com',
              created_at: '2020-10-27T18:24:19-04:00',
              updated_at: '2020-10-27T18:24:19-04:00',
              reference: 'e0a01769-c8ed-4090-bd24-9c1b2241fb75',
              address: null,
              address_2: null,
              city: null,
              state: null,
              zip: null,
              country: null,
              phone: '',
              portal_invite_last_sent_at: null,
              portal_invite_last_accepted_at: null,
              verified: false,
              portal_customer_created_at: null,
              vat_number: null,
              cc_emails: null,
              tax_exempt: false,
              parent_id: null,
              locale: null,
            },
            product: {
              id: 5233891,
              name: 'Standard',
              handle: 'ix_standard',
              description: '',
              accounting_code: '',
              request_credit_card: true,
              expiration_interval: null,
              expiration_interval_unit: 'never',
              created_at: '2020-05-21T14:21:34-04:00',
              updated_at: '2020-10-24T15:08:33-04:00',
              price_in_cents: 0,
              interval: 1,
              interval_unit: 'month',
              initial_charge_in_cents: null,
              trial_price_in_cents: null,
              trial_interval: null,
              trial_interval_unit: 'month',
              archived_at: null,
              require_credit_card: false,
              return_params: '',
              taxable: true,
              update_return_url: '',
              tax_code: 'D0000000',
              initial_charge_after_trial: false,
              version_number: 1,
              update_return_params: '',
              default_product_price_point_id: 969460,
              request_billing_address: false,
              require_billing_address: false,
              require_shipping_address: false,
              product_price_point_id: 969460,
              product_price_point_name: 'Standard',
              product_price_point_handle: 'ix_standard',
              product_family: { handle: 'inspectionxpert-cloud' },
              public_signup_pages: [],
            },
            group: null,
          },
        ]);

        billingClientMock.getSubscriptionById.mockResolvedValueOnce({
          id: 36827260,
          state: 'active',
          trial_started_at: null,
          trial_ended_at: null,
          activated_at: '2020-10-27T18:24:20-04:00',
          created_at: '2020-10-27T18:24:19-04:00',
          updated_at: '2020-10-27T18:59:09-04:00',
          expires_at: null,
          balance_in_cents: 24991,
          current_period_ends_at: '2020-11-27T17:24:19-05:00',
          next_assessment_at: '2020-11-27T17:24:19-05:00',
          canceled_at: null,
          cancellation_message: null,
          next_product_id: null,
          next_product_handle: null,
          cancel_at_end_of_period: false,
          payment_collection_method: 'remittance',
          snap_day: null,
          cancellation_method: null,
          current_period_started_at: '2020-10-27T18:24:19-04:00',
          previous_state: 'active',
          signup_payment_id: 433830000,
          signup_revenue: '0.00',
          delayed_cancel_at: null,
          coupon_code: null,
          total_revenue_in_cents: 0,
          product_price_in_cents: 0,
          product_version_number: 1,
          payment_type: null,
          referral_code: null,
          coupon_use_count: null,
          coupon_uses_allowed: null,
          reason_code: null,
          automatically_resume_at: null,
          coupon_codes: [],
          offer_id: null,
          payer_id: 37711536,
          current_billing_amount_in_cents: 12500,
          receives_invoice_emails: null,
          product_price_point_id: 969460,
          next_product_price_point_id: null,
          credit_balance_in_cents: 0,
          prepayment_balance_in_cents: 0,
          net_terms: null,
          stored_credential_transaction_id: null,
          locale: null,
          reference: '3252998005',
          currency: 'USD',
          on_hold_at: null,
          scheduled_cancellation_at: null,
          customer: {
            id: 37711536,
            first_name: 'Dev',
            last_name: 'Monkey',
            organization: null,
            email: 'dev.mail.monkey@gmail.com',
            created_at: '2020-10-27T18:24:19-04:00',
            updated_at: '2020-10-27T18:24:19-04:00',
            reference: 'e0a01769-c8ed-4090-bd24-9c1b2241fb75',
            address: null,
            address_2: null,
            city: null,
            state: null,
            zip: null,
            country: null,
            phone: '',
            portal_invite_last_sent_at: null,
            portal_invite_last_accepted_at: null,
            verified: false,
            portal_customer_created_at: null,
            vat_number: null,
            cc_emails: null,
            tax_exempt: false,
            parent_id: null,
            locale: null,
          },
          product: {
            id: 5233891,
            name: 'Standard',
            handle: 'ix_standard',
            description: '',
            accounting_code: '',
            request_credit_card: true,
            expiration_interval: null,
            expiration_interval_unit: 'never',
            created_at: '2020-05-21T14:21:34-04:00',
            updated_at: '2020-10-24T15:08:33-04:00',
            price_in_cents: 0,
            interval: 1,
            interval_unit: 'month',
            initial_charge_in_cents: null,
            trial_price_in_cents: null,
            trial_interval: null,
            trial_interval_unit: 'month',
            archived_at: null,
            require_credit_card: false,
            return_params: '',
            taxable: true,
            update_return_url: '',
            tax_code: 'D0000000',
            initial_charge_after_trial: false,
            version_number: 1,
            update_return_params: '',
            default_product_price_point_id: 969460,
            request_billing_address: false,
            require_billing_address: false,
            require_shipping_address: false,
            product_price_point_id: 969460,
            product_price_point_name: 'Standard',
            product_price_point_handle: 'ix_standard',
            product_family: { handle: 'inspectionxpert-cloud' },
            public_signup_pages: [],
          },
          group: null,
        });

        crmClientMock.getDealById.mockResolvedValueOnce({
          ixc_account_id: 'e0a01769-c8ed-4090-bd24-9c1b2241fb75',
          dealname: 'Acme Corp : Dev Monkey',
          pipeline: '5743684', // 'ONLINE TRIAL'
          dealstage: '5743686', // 'TRIAL REQUESTED': 5743685,
          dealtype: 'newbusiness',
          ix_chargify_link: 'https://inspectionxpert-test.chargify.com/subscriptions/36574344',
          ix_chargify_id: '36574344',
          crm: 'hubspot',
          crmType: 'deal',
          crmId: 3159749714,
          createdate: '1602731071850',
          hs_createdate: '1602731071850',
          hs_lastmodifieddate: '1602731075727',
          hs_is_closed: 'false',
          days_to_close: '7',
          closedate: '1603335873000',
          num_associated_contacts: '1',
          hs_deal_stage_probability: '0.2',
          hs_closed_amount: '0',
          hs_closed_amount_in_home_currency: '0',
          hs_analytics_source: 'OFFLINE',
          hs_object_id: '3159749714',
          hs_analytics_source_data_1: 'API',
          associations: {
            associatedCompanyIds: ['3252998005'],
          },
        });

        crmClientMock.createOrUpdateContact.mockResolvedValueOnce({
          hs_is_unworked: 'true',
          website: 'http://gmail.com',
          firstname: 'Dev',
          city: 'Raleigh',
          lastmodifieddate: '1603837457616',
          num_unique_conversion_events: '0',
          createdate: '1603837457485',
          hs_lifecyclestage_subscriber_date: '1603837457485',
          hs_all_contact_vids: '1951',
          lastname: 'Monkey',
          hs_marketable_until_renewal: 'false',
          hs_is_contact: 'true',
          num_conversion_events: '0',
          hs_object_id: '1951',
          hs_email_domain: 'gmail.com',
          company: 'Acme Corp',
          state: 'NC',
          lifecyclestage: 'subscriber',
          email: 'dev.mail.monkey@gmail.com',
          ixc_user_id: 'e0a01779-c8ed-4090-bd44-9c1b2242fb75',
          crm: 'hubspot',
          crmType: 'contact',
          crmId: 1951,
        });

        crmClientMock.addContactToCompany.mockResolvedValueOnce({ success: true });

        crmClientMock.addContactToDeal.mockResolvedValueOnce({ success: true });

        billingClientMock.updateCustomer.mockResolvedValueOnce({
          first_name: 'Dev',
          last_name: 'Monkey',
          email: 'dev.mail.monkey@gmail.com',
          cc_emails: null,
          organization: null,
          reference: 'e0a01769-c8ed-4090-bd24-9c1b2241fb75',
          id: 37711536,
          created_at: '2020-10-27T18:24:19-04:00',
          updated_at: '2020-10-27T18:24:19-04:00',
          address: null,
          address_2: null,
          city: null,
          state: null,
          zip: null,
          country: null,
          phone: '',
          verified: false,
          portal_customer_created_at: null,
          portal_invite_last_sent_at: null,
          portal_invite_last_accepted_at: null,
          tax_exempt: false,
          vat_number: null,
          parent_id: null,
          locale: null,
          default_subscription_group_uid: null,
        });

        const result = await subWorker.addContactToAccount({ account: signupPageFormData.account, user: signupPageFormData.user });
        expect(result.success).toBe(true);
        expect(loggerMock.error).toHaveBeenCalledTimes(0);
      });
    });

    describe('#removeContactFromAccount()', () => {
      it('should throw an error if required input is not provided', async () => {
        await expect(subWorker.removeContactFromAccount()).rejects.toThrow();
        await expect(subWorker.removeContactFromAccount({ account: signupPageFormData.account })).rejects.toThrow();
        await expect(subWorker.removeContactFromAccount({ user: signupPageFormData.user })).rejects.toThrow();
        expect(loggerMock.error).toHaveBeenCalledTimes(3);
      });

      it('should throw an error if the request fails', async () => {
        // This test fails when using the real hubspot API, because first
        // runs will create records that are found and returned by subsequent runs.
        if (subWorker.isMocked) {
          // hubspotMock.deals.removeAssociation.mockRejectedValueOnce(new Error('An error message.'));
          await expect(subWorker.removeContactFromAccount({ account: TEST_ACCOUNT, user: TEST_USER })).rejects.toThrow();
          expect(loggerMock.error).toHaveBeenCalledTimes(1);
        }
      });

      it('should remove the specified contact from the deal', async () => {
        // hubspotMock.deals.removeAssociation.mockResolvedValueOnce({ success: true });
        billingClientMock.getCustomerByReference.mockResolvedValueOnce({
          first_name: 'Dev',
          last_name: 'Monkey',
          email: 'dev.mail.monkey@gmail.com',
          cc_emails: null,
          organization: null,
          reference: 'e0a01769-c8ed-4090-bd24-9c1b2241fb75',
          id: 37711536,
          created_at: '2020-10-27T18:24:19-04:00',
          updated_at: '2020-10-27T18:24:19-04:00',
          address: null,
          address_2: null,
          city: null,
          state: null,
          zip: null,
          country: null,
          phone: '',
          verified: false,
          portal_customer_created_at: null,
          portal_invite_last_sent_at: null,
          portal_invite_last_accepted_at: null,
          tax_exempt: false,
          vat_number: null,
          parent_id: null,
          locale: null,
          default_subscription_group_uid: null,
        });

        billingClientMock.getSubscriptionsForCustomer.mockResolvedValueOnce([
          {
            id: 36827260,
            state: 'active',
            trial_started_at: null,
            trial_ended_at: null,
            activated_at: '2020-10-27T18:24:20-04:00',
            created_at: '2020-10-27T18:24:19-04:00',
            updated_at: '2020-10-27T18:59:09-04:00',
            expires_at: null,
            balance_in_cents: 24991,
            current_period_ends_at: '2020-11-27T17:24:19-05:00',
            next_assessment_at: '2020-11-27T17:24:19-05:00',
            canceled_at: null,
            cancellation_message: null,
            next_product_id: null,
            next_product_handle: null,
            cancel_at_end_of_period: false,
            payment_collection_method: 'remittance',
            snap_day: null,
            cancellation_method: null,
            current_period_started_at: '2020-10-27T18:24:19-04:00',
            previous_state: 'active',
            signup_payment_id: 433830000,
            signup_revenue: '0.00',
            delayed_cancel_at: null,
            coupon_code: null,
            total_revenue_in_cents: 0,
            product_price_in_cents: 0,
            product_version_number: 1,
            payment_type: null,
            referral_code: null,
            coupon_use_count: null,
            coupon_uses_allowed: null,
            reason_code: null,
            automatically_resume_at: null,
            coupon_codes: [],
            offer_id: null,
            payer_id: 37711536,
            receives_invoice_emails: null,
            product_price_point_id: 969460,
            next_product_price_point_id: null,
            credit_balance_in_cents: 0,
            prepayment_balance_in_cents: 0,
            net_terms: null,
            stored_credential_transaction_id: null,
            locale: null,
            reference: '3252998005',
            currency: 'USD',
            on_hold_at: null,
            scheduled_cancellation_at: null,
            customer: {
              id: 37711536,
              first_name: 'Dev',
              last_name: 'Monkey',
              organization: null,
              email: 'dev.mail.monkey@gmail.com',
              created_at: '2020-10-27T18:24:19-04:00',
              updated_at: '2020-10-27T18:24:19-04:00',
              reference: 'e0a01769-c8ed-4090-bd24-9c1b2241fb75',
              address: null,
              address_2: null,
              city: null,
              state: null,
              zip: null,
              country: null,
              phone: '',
              portal_invite_last_sent_at: null,
              portal_invite_last_accepted_at: null,
              verified: false,
              portal_customer_created_at: null,
              vat_number: null,
              cc_emails: null,
              tax_exempt: false,
              parent_id: null,
              locale: null,
            },
            product: {
              id: 5233891,
              name: 'Standard',
              handle: 'ix_standard',
              description: '',
              accounting_code: '',
              request_credit_card: true,
              expiration_interval: null,
              expiration_interval_unit: 'never',
              created_at: '2020-05-21T14:21:34-04:00',
              updated_at: '2020-10-24T15:08:33-04:00',
              price_in_cents: 0,
              interval: 1,
              interval_unit: 'month',
              initial_charge_in_cents: null,
              trial_price_in_cents: null,
              trial_interval: null,
              trial_interval_unit: 'month',
              archived_at: null,
              require_credit_card: false,
              return_params: '',
              taxable: true,
              update_return_url: '',
              tax_code: 'D0000000',
              initial_charge_after_trial: false,
              version_number: 1,
              update_return_params: '',
              default_product_price_point_id: 969460,
              request_billing_address: false,
              require_billing_address: false,
              require_shipping_address: false,
              product_price_point_id: 969460,
              product_price_point_name: 'Standard',
              product_price_point_handle: 'ix_standard',
              product_family: { handle: 'inspectionxpert-cloud' },
              public_signup_pages: [],
            },
            group: null,
          },
        ]);

        billingClientMock.getSubscriptionById.mockResolvedValueOnce({
          id: 36827260,
          state: 'active',
          trial_started_at: null,
          trial_ended_at: null,
          activated_at: '2020-10-27T18:24:20-04:00',
          created_at: '2020-10-27T18:24:19-04:00',
          updated_at: '2020-10-27T18:59:09-04:00',
          expires_at: null,
          balance_in_cents: 24991,
          current_period_ends_at: '2020-11-27T17:24:19-05:00',
          next_assessment_at: '2020-11-27T17:24:19-05:00',
          canceled_at: null,
          cancellation_message: null,
          next_product_id: null,
          next_product_handle: null,
          cancel_at_end_of_period: false,
          payment_collection_method: 'remittance',
          snap_day: null,
          cancellation_method: null,
          current_period_started_at: '2020-10-27T18:24:19-04:00',
          previous_state: 'active',
          signup_payment_id: 433830000,
          signup_revenue: '0.00',
          delayed_cancel_at: null,
          coupon_code: null,
          total_revenue_in_cents: 0,
          product_price_in_cents: 0,
          product_version_number: 1,
          payment_type: null,
          referral_code: null,
          coupon_use_count: null,
          coupon_uses_allowed: null,
          reason_code: null,
          automatically_resume_at: null,
          coupon_codes: [],
          offer_id: null,
          payer_id: 37711536,
          current_billing_amount_in_cents: 12500,
          receives_invoice_emails: null,
          product_price_point_id: 969460,
          next_product_price_point_id: null,
          credit_balance_in_cents: 0,
          prepayment_balance_in_cents: 0,
          net_terms: null,
          stored_credential_transaction_id: null,
          locale: null,
          reference: '3252998005',
          currency: 'USD',
          on_hold_at: null,
          scheduled_cancellation_at: null,
          customer: {
            id: 37711536,
            first_name: 'Dev',
            last_name: 'Monkey',
            organization: null,
            email: 'dev.mail.monkey@gmail.com',
            created_at: '2020-10-27T18:24:19-04:00',
            updated_at: '2020-10-27T18:24:19-04:00',
            reference: 'e0a01769-c8ed-4090-bd24-9c1b2241fb75',
            address: null,
            address_2: null,
            city: null,
            state: null,
            zip: null,
            country: null,
            phone: '',
            portal_invite_last_sent_at: null,
            portal_invite_last_accepted_at: null,
            verified: false,
            portal_customer_created_at: null,
            vat_number: null,
            cc_emails: null,
            tax_exempt: false,
            parent_id: null,
            locale: null,
          },
          product: {
            id: 5233891,
            name: 'Standard',
            handle: 'ix_standard',
            description: '',
            accounting_code: '',
            request_credit_card: true,
            expiration_interval: null,
            expiration_interval_unit: 'never',
            created_at: '2020-05-21T14:21:34-04:00',
            updated_at: '2020-10-24T15:08:33-04:00',
            price_in_cents: 0,
            interval: 1,
            interval_unit: 'month',
            initial_charge_in_cents: null,
            trial_price_in_cents: null,
            trial_interval: null,
            trial_interval_unit: 'month',
            archived_at: null,
            require_credit_card: false,
            return_params: '',
            taxable: true,
            update_return_url: '',
            tax_code: 'D0000000',
            initial_charge_after_trial: false,
            version_number: 1,
            update_return_params: '',
            default_product_price_point_id: 969460,
            request_billing_address: false,
            require_billing_address: false,
            require_shipping_address: false,
            product_price_point_id: 969460,
            product_price_point_name: 'Standard',
            product_price_point_handle: 'ix_standard',
            product_family: { handle: 'inspectionxpert-cloud' },
            public_signup_pages: [],
          },
          group: null,
        });

        crmClientMock.getDealById.mockResolvedValueOnce({
          ixc_account_id: 'e0a01769-c8ed-4090-bd24-9c1b2241fb75',
          dealname: 'Acme Corp : Dev Monkey',
          pipeline: '5743684', // 'ONLINE TRIAL'
          dealstage: '5743686', // 'TRIAL REQUESTED': 5743685,
          dealtype: 'newbusiness',
          ix_chargify_link: 'https://inspectionxpert-test.chargify.com/subscriptions/36574344',
          ix_chargify_id: '36574344',
          crm: 'hubspot',
          crmType: 'deal',
          crmId: 3159749714,
          createdate: '1602731071850',
          hs_createdate: '1602731071850',
          hs_lastmodifieddate: '1602731075727',
          hs_is_closed: 'false',
          days_to_close: '7',
          closedate: '1603335873000',
          num_associated_contacts: '1',
          hs_deal_stage_probability: '0.2',
          hs_closed_amount: '0',
          hs_closed_amount_in_home_currency: '0',
          hs_analytics_source: 'OFFLINE',
          hs_object_id: '3159749714',
          hs_analytics_source_data_1: 'API',
          associations: {
            associatedCompanyIds: ['3252998005'],
          },
        });

        billingClientMock.getMetadataForCustomerWithId.mockResolvedValue({
          ixc_account_id: '83215576-359c-42e6-895f-75aa1b2af61a',
          hubspot_company_id: '5929423486',
        });

        crmClientMock.getCompanyById.mockResolvedValueOnce({
          hs_num_open_deals: '0',
          website: 'qualityxpert.com',
          num_associated_contacts: '1',
          hs_analytics_first_timestamp: '1619189627933',
          description: 'Please enter your company URL below.',
          createdate: '1619189627568',
          ixc_account_id: '83215576-359c-42e6-895f-75aa1b2af61a',
          hs_num_blockers: '0',
          ix_chargify_link: 'https://inspectionxpert-test.chargify.com/customers/42658762',
          hs_analytics_num_visits: '0',
          ix_chargify_id: '42658762',
          first_contact_createdate: '1619189627933',
          hs_lastmodifieddate: '1619189676923',
          hs_analytics_source: 'OFFLINE',
          hs_predictivecontactscore_v2: '2.11',
          hs_num_decision_makers: '0',
          domain: 'qualityxpert.com',
          name: 'JDixA Org',
          hs_object_id: '5929423486',
          hs_num_contacts_with_buying_roles: '0',
          hs_num_child_companies: '0',
          hs_analytics_num_page_views: '0',
          hs_analytics_source_data_1: 'API',
          crm: 'hubspot',
          crmType: 'company',
          crmId: 5929423486,
        });

        crmClientMock.getContactByEmail.mockResolvedValueOnce({
          hs_is_unworked: 'true',
          website: 'http://gmail.com',
          firstname: 'Dev',
          city: 'Raleigh',
          lastmodifieddate: '1603837457616',
          num_unique_conversion_events: '0',
          createdate: '1603837457485',
          hs_lifecyclestage_subscriber_date: '1603837457485',
          hs_all_contact_vids: '1951',
          lastname: 'Monkey',
          hs_marketable_until_renewal: 'false',
          hs_is_contact: 'true',
          num_conversion_events: '0',
          hs_object_id: '1951',
          hs_email_domain: 'gmail.com',
          company: 'Acme Corp',
          state: 'NC',
          lifecyclestage: 'subscriber',
          email: 'dev.mail.monkey@gmail.com',
          ixc_user_id: 'e0a01779-c8ed-4090-bd44-9c1b2242fb75',
          crm: 'hubspot',
          crmType: 'contact',
          crmId: 1951,
        });

        crmClientMock.removeContactFromCompany.mockResolvedValueOnce(true);
        crmClientMock.removeContactFromDeal.mockResolvedValueOnce(true);
        const result = await subWorker.removeContactFromAccount({ account: signupPageFormData.account, user: signupPageFormData.user });
        expect(result.success).toBe(true);
        expect(loggerMock.error).toHaveBeenCalledTimes(0);
      });
    });

    describe('#lookupBackOfficeIDs()', () => {
      it('should throw an error if required input is not provided', async () => {
        await expect(subWorker.lookupBackOfficeIDs()).rejects.toThrow();
        expect(loggerMock.error).toHaveBeenCalledTimes(0);
      });

      it('should throw an error if the request fails', async () => {
        // This test fails when using the real hubspot API, because first
        // runs will create records that are found and returned by subsequent runs.
        if (subWorker.isMocked) {
          // hubspotMock.deals.removeAssociation.mockRejectedValueOnce(new Error('An error message.'));
          await expect(subWorker.lookupBackOfficeIDs({ id: 'an-id' })).rejects.toThrow();
          expect(loggerMock.error).toHaveBeenCalledTimes(1);
        }
      });

      it('should extract all of the backoffice and system record IDs from currentUser if they are present', async () => {
        const currentUser = {
          id: 'e0a01779-c8ed-4090-bd44-9c1b2242fb75',
          accountId: 'some-accountId',
          crmContactId: 'some-contactId',
          subscription: {
            paymentSystemId: 'some-subscriptionId',
            customerId: 'some-customerId',
            companyId: 'some-companyId',
            crmDealId: 'some-dealId',
          },
        };
        const result = await subWorker.lookupBackOfficeIDs(currentUser);
        expect(result).toEqual({
          ixcUserId: 'e0a01779-c8ed-4090-bd44-9c1b2242fb75',
          ixcAccountId: 'some-accountId',
          bsSubscriptionId: 'some-subscriptionId',
          bsCustomerId: 'some-customerId',
          crmContactId: 'some-contactId',
          crmCompanyId: 'some-companyId',
          crmDealId: 'some-dealId',
        });
        expect(loggerMock.error).toHaveBeenCalledTimes(0);
      });

      it('should lookup the crmContactId when its not present', async () => {
        const currentUser = {
          id: 'e0a01779-c8ed-4090-bd44-9c1b2242fb75',
          accountId: 'some-accountId',
          // crmContactId: 'some-contactId',
          subscription: {
            paymentSystemId: 'some-subscriptionId',
            customerId: 'some-customerId',
            companyId: 'some-companyId',
            crmDealId: 'some-dealId',
          },
        };
        crmClientMock.getContactByEmail.mockResolvedValueOnce({ crmId: 'remote-contactId' });
        const getSubscriptionForIdMock = jest.spyOn(UserRepository, 'updateUserOnly').mockImplementation(() => ({ id: 'e0a01779-c8ed-4090-bd44-9c1b2242fb75', crmContactId: 'remote-contactId' }));
        const result = await subWorker.lookupBackOfficeIDs(currentUser);
        expect(result).toEqual({
          ixcUserId: 'e0a01779-c8ed-4090-bd44-9c1b2242fb75',
          ixcAccountId: 'some-accountId',
          bsSubscriptionId: 'some-subscriptionId',
          bsCustomerId: 'some-customerId',
          crmContactId: 'remote-contactId',
          crmCompanyId: 'some-companyId',
          crmDealId: 'some-dealId',
        });
        expect(loggerMock.error).toHaveBeenCalledTimes(0);
        getSubscriptionForIdMock.mockRestore();
      });

      it('should lookup the subscription when its not present', async () => {
        const currentUser = {
          id: 'e0a01779-c8ed-4090-bd44-9c1b2242fb75',
          accountId: 'some-accountId',
          crmContactId: 'some-contactId',
        };
        const getSubscriptionForIdMock = jest.spyOn(subWorker, 'getSubscriptionForAccountId').mockImplementation(() => ({
          id: 'remote-subscriptionId',
          reference: 'remote-dealId',
          customer: {
            id: 'remote-customerId',
          },
        }));
        billingClientMock.getMetadataForCustomerWithId.mockResolvedValue({
          ixc_account_id: 'some-accountId',
          hubspot_company_id: 'remote-companyId',
        });
        const result = await subWorker.lookupBackOfficeIDs(currentUser);
        expect(result).toEqual({
          ixcUserId: 'e0a01779-c8ed-4090-bd44-9c1b2242fb75',
          ixcAccountId: 'some-accountId',
          bsSubscriptionId: 'remote-subscriptionId',
          bsCustomerId: 'remote-customerId',
          crmContactId: 'some-contactId',
          crmCompanyId: 'remote-companyId',
          crmDealId: 'remote-dealId',
        });
        expect(loggerMock.error).toHaveBeenCalledTimes(0);
        getSubscriptionForIdMock.mockRestore();
      });
    });

    describe('#registerSubscription', () => {
      it('throws an error if missing required parameter', async () => {
        await expect(subWorker.registerSubscription({})).rejects.toThrow();
        expect(loggerMock.error).toHaveBeenCalledTimes(1);
      });

      it('creates a subscription', async () => {
        const extractAppContextSpy = jest.spyOn(SubscriptionWorker, 'extractAppContext');
        // Set Mock Returns
        crmClientMock.createOrUpdateCompany.mockResolvedValueOnce({ crmId: 'crmCompany' });
        crmClientMock.createOrUpdateContact.mockResolvedValueOnce('crmContact');

        // billingClientMock.getVendorUrlForSubscriptionWithId.mockResolvedValueOnce({ crmId: 'some' });
        crmClientMock.createDeal.mockResolvedValueOnce({ crmId: 'some-crmDealId' });
        crmClientMock.addContactToDeal.mockResolvedValueOnce(true);
        crmClientMock.addCompanyToDeal.mockResolvedValueOnce(true);
        // billingClientMock.updateSubscription.mockResolvedValueOnce({ id: 'some' });
        // billingClientMock.updateSubscriptionCustomFields.mockResolvedValueOnce(true);

        billingClientMock.createOrUpdateCustomer.mockResolvedValueOnce('billingCustomer');

        billingClientMock.getCurrentPrice.mockResolvedValueOnce(MOCK_PRODUCT_PRICES[0]);
        billingClientMock.createSubscription.mockResolvedValueOnce({ id: 'billingSubscription' });

        billingClientMock.getCurrentPrice.mockResolvedValueOnce({
          id: 'some',
          handle: 'some',
        });
        billingClientMock.updateSubscriptionQuantity.mockResolvedValueOnce({
          subscription_id: 'billingSubscription',
          price_point_id: 'some',
          price_point_handle: 'some',
        });
        billingClientMock.updateComponentPrice.mockResolvedValueOnce({
          subscription_id: 'billingSubscription',
          price_point_id: 'some',
          price_point_handle: 'some',
        });
        crmClientMock.updateCompany.mockResolvedValueOnce('crmCompany');

        // Set Catalog
        billingClientMock.getCatalog.mockResolvedValueOnce({
          'inspectionxpert-cloud': {
            products: {
              ix_standard: { prices: [] },
            },
            components: {
              paid_user_seat_ss: {
                id: 'some',
                handle: 'paid_user_seat_ss',
                name: 'some',
                kind: 'some',
                unit_name: 'some',
                pricing_scheme: 'some',
              },
            },
          },
        });
        await subWorker.loadCatalog();

        const result = await subWorker.registerSubscription({
          user: {
            id: 'some',
            firstName: 'some',
            lastName: 'some',
            email: 'some@some.com',
            profile: { phoneNumber: 'some' },
            roles: ['some'],
          },
          account: {
            //
            id: 'some',
            orgName: 'some',
            orgStreet: 'some',
            orgStreet2: 'some',
            orgCity: 'some',
            orgRegion: 'some',
            orgPostalcode: 'some',
            orgCountry: 'some',
          },
          product: 'some',
          crmCustomerType: 'PROSPECT',
          crmPipelineName: 'ONLINE TRIAL',
          crmPipelineStage: 'TRIAL REQUESTED',
          crmDealType: 'newbusiness',
          crmDealTrialType: 'Time Limited',
        });

        // Standardizes Input
        expect(extractAppContextSpy).toHaveBeenCalledTimes(1);

        // Updates CRM Company
        expect(crmClientMock.createOrUpdateCompany).toHaveBeenCalledTimes(1);

        // Updates CRM Contact
        expect(crmClientMock.createOrUpdateContact).toHaveBeenCalledTimes(1);

        // Updates Billing Customer
        expect(billingClientMock.createOrUpdateCustomer).toHaveBeenCalledTimes(1);

        // Creates Billing Subscription
        expect(billingClientMock.createSubscription).toHaveBeenCalledTimes(1);

        // Sets Component
        expect(billingClientMock.updateSubscriptionQuantity).toHaveBeenCalledTimes(1);

        // Adds Billing IDs to CRM Company
        expect(crmClientMock.updateCompany).toHaveBeenCalledTimes(1);

        // Returns Data
        expect(result).toMatchObject({
          bsSubscription: {
            id: 'billingSubscription',
            companyId: 'crmCompany',
            components: [
              {
                component_id: 'some',
                component_handle: 'paid_user_seat_ss',
                name: 'some',
                kind: 'some',
                unit_name: 'some',
                pricing_scheme: 'some',
                subscription_id: 'billingSubscription',
                price_point_id: 'some',
                price_point_handle: 'some',
              },
            ],
          },
          crmCompany: 'crmCompany',
          bsCustomer: 'billingCustomer',
          crmContact: 'crmContact',
          input: {
            userId: 'some',
            accountId: 'some',
            firstName: 'some',
            lastName: 'some',
            email: 'some@some.com',
            phone: 'some',
            roles: 'some',
            orgName: 'some',
            orgStreet: 'some',
            orgStreet2: 'some',
            orgCity: 'some',
            orgRegion: 'some',
            orgPostalcode: 'some',
            orgCountry: 'some',
            domain: 'some.com',
          },
          success: true,
        });
        extractAppContextSpy.mockRestore();
      });

      it('uses the internal $0 seat for inspectionxpert domains', async () => {
        const extractAppContextSpy = jest.spyOn(SubscriptionWorker, 'extractAppContext');
        // Set Mock Returns
        crmClientMock.createOrUpdateCompany.mockResolvedValueOnce({ crmId: 'crmCompany' });
        crmClientMock.createOrUpdateContact.mockResolvedValueOnce('crmContact');

        // billingClientMock.getVendorUrlForSubscriptionWithId.mockResolvedValueOnce({ crmId: 'some' });
        crmClientMock.createDeal.mockResolvedValueOnce({ crmId: 'some-crmDealId' });
        crmClientMock.addContactToDeal.mockResolvedValueOnce(true);
        crmClientMock.addCompanyToDeal.mockResolvedValueOnce(true);
        // billingClientMock.updateSubscription.mockResolvedValueOnce({ id: 'some' });
        // billingClientMock.updateSubscriptionCustomFields.mockResolvedValueOnce(true);

        billingClientMock.createOrUpdateCustomer.mockResolvedValueOnce('billingCustomer');

        billingClientMock.getCurrentPrice.mockResolvedValueOnce(MOCK_PRODUCT_PRICES[0]);
        billingClientMock.createSubscription.mockResolvedValueOnce({ id: 'billingSubscription' });

        billingClientMock.getCurrentPrice.mockResolvedValueOnce({
          id: 'some',
          handle: 'some',
        });
        billingClientMock.updateSubscriptionQuantity.mockResolvedValueOnce({
          subscription_id: 'billingSubscription',
          price_point_id: 'some',
          price_point_handle: 'some',
        });
        billingClientMock.updateComponentPrice.mockResolvedValueOnce({
          subscription_id: 'billingSubscription',
          price_point_id: 'some',
          price_point_handle: 'some',
        });
        crmClientMock.updateCompany.mockResolvedValueOnce('crmCompany');

        // Set Catalog
        billingClientMock.getCatalog.mockResolvedValueOnce({
          'inspectionxpert-cloud': {
            products: {
              ix_standard: { prices: [] },
            },
            components: {
              internal_account_ss: {
                id: 'some',
                handle: 'internal_account_ss',
                name: 'some',
                kind: 'some',
                unit_name: 'some',
                pricing_scheme: 'some',
              },
            },
          },
        });
        await subWorker.loadCatalog();

        const result = await subWorker.registerSubscription({
          user: {
            id: 'some',
            firstName: 'some',
            lastName: 'some',
            email: 'some@inspectionxpert.com',
            profile: { phoneNumber: 'some' },
            roles: ['some'],
          },
          account: {
            //
            id: 'some',
            orgName: 'some',
            orgStreet: 'some',
            orgStreet2: 'some',
            orgCity: 'some',
            orgRegion: 'some',
            orgPostalcode: 'some',
            orgCountry: 'some',
          },
          product: 'some',
          crmPipelineName: '',
          crmCustomerType: '',
          crmPipelineStage: '',
          crmDealType: '',
          crmDealTrialType: '',
        });

        // Standardizes Input
        expect(extractAppContextSpy).toHaveBeenCalledTimes(1);

        // Updates CRM Company
        expect(crmClientMock.createOrUpdateCompany).toHaveBeenCalledTimes(1);

        // Updates CRM Contact
        expect(crmClientMock.createOrUpdateContact).toHaveBeenCalledTimes(1);

        // Updates Billing Customer
        expect(billingClientMock.createOrUpdateCustomer).toHaveBeenCalledTimes(1);

        // Creates Billing Subscription
        expect(billingClientMock.createSubscription).toHaveBeenCalledTimes(1);

        // Sets Component
        expect(billingClientMock.updateSubscriptionQuantity).toHaveBeenCalledTimes(1);

        // Adds Billing IDs to CRM Company
        expect(crmClientMock.updateCompany).toHaveBeenCalledTimes(1);

        // Returns Data
        expect(result).toMatchObject({
          bsSubscription: {
            id: 'billingSubscription',
            companyId: 'crmCompany',
            components: [
              {
                component_id: 'some',
                component_handle: 'internal_account_ss',
                name: 'some',
                kind: 'some',
                unit_name: 'some',
                pricing_scheme: 'some',
                subscription_id: 'billingSubscription',
                price_point_id: 'some',
                price_point_handle: 'some',
              },
            ],
          },
          crmCompany: 'crmCompany',
          bsCustomer: 'billingCustomer',
          crmContact: 'crmContact',
          input: {
            userId: 'some',
            accountId: 'some',
            firstName: 'some',
            lastName: 'some',
            email: 'some@inspectionxpert.com',
            phone: 'some',
            roles: 'some',
            orgName: 'some',
            orgStreet: 'some',
            orgStreet2: 'some',
            orgCity: 'some',
            orgRegion: 'some',
            orgPostalcode: 'some',
            orgCountry: 'some',
            domain: 'inspectionxpert.com',
          },
          success: true,
        });

        extractAppContextSpy.mockRestore();
      });
    });

    describe('#registerSalesAcceptedLead', () => {
      it('throws an error if missing required parameter', async () => {
        await expect(subWorker.registerSalesAcceptedLead({})).rejects.toThrow();
        expect(loggerMock.error).toHaveBeenCalledTimes(1);
      });

      it('creates deal if it does not exist', async () => {
        // Set up mocks
        const getSubscriptionForIdMock = jest.spyOn(subWorker, 'getSubscriptionForAccountId').mockImplementation(() => ({ id: 'some', customer: { organization: 'some', first_name: 'some', last_name: 'some' } }));
        billingClientMock.getMetadataForCustomerWithId.mockResolvedValueOnce({ hubspot_company_id: 'some' });
        crmClientMock.getCompanyById.mockResolvedValueOnce(true);
        crmClientMock.getContactByEmail.mockResolvedValueOnce(true);

        billingClientMock.getVendorUrlForSubscriptionWithId.mockResolvedValueOnce({ crmId: 'some' });
        crmClientMock.createDeal.mockResolvedValueOnce({ crmId: 'some' });
        crmClientMock.addContactToDeal.mockResolvedValueOnce(true);
        crmClientMock.addCompanyToDeal.mockResolvedValueOnce(true);
        billingClientMock.updateSubscription.mockResolvedValueOnce({ id: 'some' });
        billingClientMock.updateSubscriptionCustomFields.mockResolvedValueOnce(true);

        await subWorker.registerSalesAcceptedLead({ accountId: 'some', email: 'some', crmPipelineStage: 'some', crmPipelineName: 'some', crmDealTrialType: 'some', crmDealType: 'some' });
        expect(billingClientMock.getVendorUrlForSubscriptionWithId).toHaveBeenCalledTimes(1);
        expect(crmClientMock.createDeal).toHaveBeenCalledTimes(1);

        getSubscriptionForIdMock.mockRestore();
      });

      it('updates and returns data', async () => {
        // Set up mocks
        const getSubscriptionForIdMock = jest.spyOn(subWorker, 'getSubscriptionForAccountId').mockImplementation(() => ({ id: 'some', customer: { id: 'some' }, reference: 'some' }));
        billingClientMock.getMetadataForCustomerWithId.mockResolvedValueOnce({ hubspot_company_id: 'some' });
        crmClientMock.getCompanyById.mockResolvedValueOnce(true);
        crmClientMock.getContactByEmail.mockResolvedValueOnce(true);

        crmClientMock.getDealById.mockResolvedValueOnce({ crmId: 'some' });
        crmClientMock.addContactToDeal.mockResolvedValueOnce(true);
        crmClientMock.addCompanyToDeal.mockResolvedValueOnce(true);
        billingClientMock.updateSubscription.mockResolvedValueOnce({ id: 'some' });
        billingClientMock.updateSubscriptionCustomFields.mockResolvedValueOnce(true);

        const result = await subWorker.registerSalesAcceptedLead({ accountId: 'some', email: 'some', crmPipelineStage: 'some', crmPipelineName: 'some', crmDealTrialType: 'some', crmDealType: 'some' });

        // gets deal
        expect(crmClientMock.getDealById).toHaveBeenCalledTimes(1);

        // adds contact to deal
        expect(crmClientMock.addContactToDeal).toHaveBeenCalledTimes(1);

        // adds company to deal
        expect(crmClientMock.addCompanyToDeal).toHaveBeenCalledTimes(1);

        // adds deal ID to billing subscription reference
        expect(billingClientMock.updateSubscription).toHaveBeenCalledTimes(1);

        // adds deal ID to billing subscription custom fields
        expect(billingClientMock.updateSubscriptionCustomFields).toHaveBeenCalledTimes(1);

        // returns data
        expect(result).toMatchObject({
          crmDeal: { crmId: 'some' },
          success: true,
        });

        getSubscriptionForIdMock.mockRestore();
      });
    });

    describe('#updateDealStage()', () => {
      it('should throw an error if required input is not provided', async () => {
        await expect(subWorker.updateDealStage({})).rejects.toThrow();
        expect(loggerMock.error).toHaveBeenCalledTimes(1);
      });

      it('should throw an error if the request fails', async () => {
        // This test fails when using the real hubspot API, because the record is created successfully
        if (subWorker.isMocked) {
          // hubspotMock.deals.update.mockRejectedValueOnce(new Error('An error message.'));
          await expect(
            subWorker.updateDealStage({
              accountId: 'e0a01769-c8ed-4090-bd24-9c1b2241fb75',
            })
          ).rejects.toThrow();
          expect(loggerMock.error).toHaveBeenCalledTimes(1);
        }
      });

      it('should update the crm deal stage of time bound trials when the request succeeds', async () => {
        crmClientMock.getDealById.mockResolvedValueOnce({
          ixc_account_id: 'e0a01769-c8ed-4090-bd24-9c1b2241fb75',
          dealname: 'Acme Corp : Dev Monkey',
          pipeline: '5743684', // 'ONLINE TRIAL'
          dealstage: '5743686', // 'TRIAL REQUESTED': 5743685,
          dealtype: 'newbusiness',
          ix_chargify_link: 'https://inspectionxpert-test.chargify.com/subscriptions/36574344',
          ix_chargify_id: '36574344',
          crm: 'hubspot',
          crmType: 'deal',
          crmId: 3159749714,
          createdate: '1602731071850',
          hs_createdate: '1602731071850',
          hs_lastmodifieddate: '1602731075727',
          hs_is_closed: 'false',
          days_to_close: '7',
          closedate: '1603335873000',
          num_associated_contacts: '1',
          hs_deal_stage_probability: '0.2',
          hs_closed_amount: '0',
          hs_closed_amount_in_home_currency: '0',
          hs_analytics_source: 'OFFLINE',
          hs_object_id: '3159749714',
          hs_analytics_source_data_1: 'API',
        });

        crmClientMock.updateDeal.mockResolvedValueOnce({
          ixc_account_id: 'e0a01769-c8ed-4090-bd24-9c1b2241fb75',
          dealname: 'Acme Corp : Dev Monkey',
          pipeline: '5743684', // 'ONLINE TRIAL'
          dealstage: '5743686', // 'FIRST LOGIN'
          dealtype: 'newbusiness',
          ix_chargify_link: 'https://inspectionxpert-test.chargify.com/subscriptions/36574344',
          ix_chargify_id: '36574344',
          crm: 'hubspot',
          crmType: 'deal',
          crmId: 3159749714,
          createdate: '1602731071850',
          hs_createdate: '1602731071850',
          hs_lastmodifieddate: '1602731075727',
          hs_is_closed: 'false',
          days_to_close: '7',
          closedate: '1603335873000',
          num_associated_contacts: '1',
          hs_deal_stage_probability: '0.2',
          hs_closed_amount: '0',
          hs_closed_amount_in_home_currency: '0',
          hs_analytics_source: 'OFFLINE',
          hs_object_id: '3159749714',
          hs_analytics_source_data_1: 'API',
        });

        billingClientMock.getCustomerByReference.mockResolvedValueOnce({
          first_name: 'Dev',
          last_name: 'Monkey',
          email: 'dev.mail.monkey@gmail.com',
          cc_emails: null,
          organization: null,
          reference: 'e0a01769-c8ed-4090-bd24-9c1b2241fb75',
          id: 37451677,
          created_at: '2020-10-14T23:04:33-04:00',
          updated_at: '2020-10-14T23:04:33-04:00',
          address: null,
          address_2: null,
          city: null,
          state: null,
          zip: null,
          country: null,
          phone: '',
          verified: false,
          portal_customer_created_at: null,
          portal_invite_last_sent_at: null,
          portal_invite_last_accepted_at: null,
          tax_exempt: false,
          vat_number: null,
          parent_id: null,
          locale: null,
          default_subscription_group_uid: null,
        });

        billingClientMock.getSubscriptionCustomFields.mockResolvedValueOnce([
          {
            id: 15809160,
            value: '2f84e6ec-2615-4c55-b19b-b454cc6fa1bc',
            resource_id: 36574344,
            deleted_at: null,
            name: 'ixc_subscription_id',
          },
        ]);

        billingClientMock.getSubscriptionsForCustomer.mockResolvedValueOnce([
          {
            id: 36574344,
            state: 'active',
            trial_started_at: null,
            trial_ended_at: null,
            activated_at: '2020-10-14T23:04:35-04:00',
            created_at: '2020-10-14T23:04:33-04:00',
            updated_at: '2020-10-14T23:04:35-04:00',
            expires_at: '2020-10-21T23:04:33-04:00',
            balance_in_cents: 0,
            current_period_ends_at: '2020-10-21T23:04:33-04:00',
            next_assessment_at: '2020-10-21T23:04:33-04:00',
            canceled_at: null,
            cancellation_message: null,
            next_product_id: null,
            next_product_handle: null,
            cancel_at_end_of_period: false,
            payment_collection_method: 'remittance',
            snap_day: null,
            cancellation_method: null,
            current_period_started_at: '2020-10-14T23:04:33-04:00',
            previous_state: 'active',
            signup_payment_id: 430337209,
            signup_revenue: '0.00',
            delayed_cancel_at: null,
            coupon_code: null,
            total_revenue_in_cents: 0,
            product_price_in_cents: 0,
            product_version_number: 1,
            payment_type: null,
            referral_code: null,
            coupon_use_count: null,
            coupon_uses_allowed: null,
            reason_code: null,
            automatically_resume_at: null,
            coupon_codes: [],
            offer_id: null,
            payer_id: 37451677,
            receives_invoice_emails: null,
            product_price_point_id: 969487,
            next_product_price_point_id: null,
            credit_balance_in_cents: 0,
            prepayment_balance_in_cents: 0,
            net_terms: null,
            stored_credential_transaction_id: null,
            locale: null,
            reference: '3159749714',
            currency: 'USD',
            on_hold_at: null,
            customer: {
              id: 37451444,
              first_name: 'Dev',
              last_name: 'Monkey',
              organization: null,
              email: 'dev.mail.monkey@gmail.com',
              created_at: '2020-10-14T22:41:32-04:00',
              updated_at: '2020-10-14T22:41:32-04:00',
              reference: 'e0a01769-c8ed-4090-bd24-9c1b2241fb75',
              address: null,
              address_2: null,
              city: null,
              state: null,
              zip: null,
              country: null,
              phone: '',
              portal_invite_last_sent_at: null,
              portal_invite_last_accepted_at: null,
              verified: false,
              portal_customer_created_at: null,
              vat_number: null,
              cc_emails: null,
              tax_exempt: false,
              parent_id: null,
              locale: null,
            },
            product: {
              id: 5256309,
              name: 'Standard',
              handle: 'ix_standard',
              description: '',
              accounting_code: '',
              request_credit_card: true,
              expiration_interval: 7,
              expiration_interval_unit: 'day',
              created_at: '2020-09-01T20:56:13-04:00',
              updated_at: '2020-10-11T23:03:57-04:00',
              price_in_cents: 0,
              interval: 7,
              interval_unit: 'day',
              initial_charge_in_cents: null,
              trial_price_in_cents: null,
              trial_interval: null,
              trial_interval_unit: 'month',
              archived_at: null,
              require_credit_card: false,
              return_params: '',
              taxable: false,
              update_return_url: '',
              tax_code: '',
              initial_charge_after_trial: false,
              version_number: 1,
              update_return_params: '',
              default_product_price_point_id: 969487,
              request_billing_address: false,
              require_billing_address: false,
              require_shipping_address: false,
              product_price_point_id: 969487,
              product_price_point_name: 'Original',
              product_price_point_handle: 'ix_standard',
              product_family: { id: 1526431, name: 'IX3', description: '', handle: 'inspectionxpert-cloud', accounting_code: null, created_at: '2020-05-21T14:21:33-04:00', updated_at: '2020-09-01T16:25:44-04:00' },
              public_signup_pages: [],
            },
            group: null,
          },
        ]);

        const result = await subWorker.updateDealStage({
          accountId: 'e0a01769-c8ed-4090-bd24-9c1b2241fb75',
          // stageName: 'TRIAL REQUESTED' // 5743685,
          stageName: 'FIRST LOGIN', // 5743686,
          // stageName: 'DRAWING BALLOONED' // 5743687,
          // stageName: 'REPORT CREATED' // 5743688,
          // stageName: 'CLOSED WON' // 5743690,
          // stageName: 'CLOSED LOST' // 5743691,
        });
        expect(result).toBeDefined();
        expect(result.success).toBe(true);
        expect(result.crmDeal.dealstage).toBe('5743686'); // 'FIRST LOGIN': 5743686,
        expect(loggerMock.error).toHaveBeenCalledTimes(0);
      }, 30000); // Wait 30 seconds for asyn timeouts

      it('should update the crm deal stage of freemium subscriptions when the request succeeds', async () => {
        crmClientMock.getDealById.mockResolvedValueOnce({
          ixc_account_id: 'e0a01769-c8ed-4090-bd24-9c1b2241fb75',
          dealname: 'Acme Corp : Dev Monkey',
          pipeline: '6510422', // 'FREEMIUM'
          dealstage: '6510423', // 'TRIAL REQUESTED': 6510423,
          dealtype: 'newbusiness',
          ix_chargify_link: 'https://inspectionxpert-test.chargify.com/subscriptions/36574344',
          ix_chargify_id: '36574344',
          crm: 'hubspot',
          crmType: 'deal',
          crmId: 3159749714,
          createdate: '1602731071850',
          hs_createdate: '1602731071850',
          hs_lastmodifieddate: '1602731075727',
          hs_is_closed: 'false',
          days_to_close: '7',
          closedate: '1603335873000',
          num_associated_contacts: '1',
          hs_deal_stage_probability: '0.2',
          hs_closed_amount: '0',
          hs_closed_amount_in_home_currency: '0',
          hs_analytics_source: 'OFFLINE',
          hs_object_id: '3159749714',
          hs_analytics_source_data_1: 'API',
        });

        crmClientMock.updateDeal.mockResolvedValueOnce({
          ixc_account_id: 'e0a01769-c8ed-4090-bd24-9c1b2241fb75',
          dealname: 'Acme Corp : Dev Monkey',
          pipeline: '6510422', // 'FREEMIUM'
          dealstage: '6510424', // 'FIRST LOGIN'
          dealtype: 'newbusiness',
          ix_chargify_link: 'https://inspectionxpert-test.chargify.com/subscriptions/36574344',
          ix_chargify_id: '36574344',
          crm: 'hubspot',
          crmType: 'deal',
          crmId: 3159749714,
          createdate: '1602731071850',
          hs_createdate: '1602731071850',
          hs_lastmodifieddate: '1602731075727',
          hs_is_closed: 'false',
          days_to_close: '7',
          closedate: '1603335873000',
          num_associated_contacts: '1',
          hs_deal_stage_probability: '0.2',
          hs_closed_amount: '0',
          hs_closed_amount_in_home_currency: '0',
          hs_analytics_source: 'OFFLINE',
          hs_object_id: '3159749714',
          hs_analytics_source_data_1: 'API',
        });

        billingClientMock.getCustomerByReference.mockResolvedValueOnce({
          first_name: 'Dev',
          last_name: 'Monkey',
          email: 'dev.mail.monkey@gmail.com',
          cc_emails: null,
          organization: null,
          reference: 'e0a01769-c8ed-4090-bd24-9c1b2241fb75',
          id: 37451677,
          created_at: '2020-10-14T23:04:33-04:00',
          updated_at: '2020-10-14T23:04:33-04:00',
          address: null,
          address_2: null,
          city: null,
          state: null,
          zip: null,
          country: null,
          phone: '',
          verified: false,
          portal_customer_created_at: null,
          portal_invite_last_sent_at: null,
          portal_invite_last_accepted_at: null,
          tax_exempt: false,
          vat_number: null,
          parent_id: null,
          locale: null,
          default_subscription_group_uid: null,
        });

        billingClientMock.getSubscriptionCustomFields.mockResolvedValueOnce([
          {
            id: 15809160,
            value: '2f84e6ec-2615-4c55-b19b-b454cc6fa1bc',
            resource_id: 36574344,
            deleted_at: null,
            name: 'ixc_subscription_id',
          },
        ]);

        billingClientMock.getSubscriptionsForCustomer.mockResolvedValueOnce([
          {
            id: 36574344,
            state: 'active',
            trial_started_at: null,
            trial_ended_at: null,
            activated_at: '2020-10-14T23:04:35-04:00',
            created_at: '2020-10-14T23:04:33-04:00',
            updated_at: '2020-10-14T23:04:35-04:00',
            expires_at: '2020-10-21T23:04:33-04:00',
            balance_in_cents: 0,
            current_period_ends_at: '2020-10-21T23:04:33-04:00',
            next_assessment_at: '2020-10-21T23:04:33-04:00',
            canceled_at: null,
            cancellation_message: null,
            next_product_id: null,
            next_product_handle: null,
            cancel_at_end_of_period: false,
            payment_collection_method: 'remittance',
            snap_day: null,
            cancellation_method: null,
            current_period_started_at: '2020-10-14T23:04:33-04:00',
            previous_state: 'active',
            signup_payment_id: 430337209,
            signup_revenue: '0.00',
            delayed_cancel_at: null,
            coupon_code: null,
            total_revenue_in_cents: 0,
            product_price_in_cents: 0,
            product_version_number: 1,
            payment_type: null,
            referral_code: null,
            coupon_use_count: null,
            coupon_uses_allowed: null,
            reason_code: null,
            automatically_resume_at: null,
            coupon_codes: [],
            offer_id: null,
            payer_id: 37451677,
            receives_invoice_emails: null,
            product_price_point_id: 969487,
            next_product_price_point_id: null,
            credit_balance_in_cents: 0,
            prepayment_balance_in_cents: 0,
            net_terms: null,
            stored_credential_transaction_id: null,
            locale: null,
            reference: '3159749714',
            currency: 'USD',
            on_hold_at: null,
            customer: {
              id: 37451444,
              first_name: 'Dev',
              last_name: 'Monkey',
              organization: null,
              email: 'dev.mail.monkey@gmail.com',
              created_at: '2020-10-14T22:41:32-04:00',
              updated_at: '2020-10-14T22:41:32-04:00',
              reference: 'e0a01769-c8ed-4090-bd24-9c1b2241fb75',
              address: null,
              address_2: null,
              city: null,
              state: null,
              zip: null,
              country: null,
              phone: '',
              portal_invite_last_sent_at: null,
              portal_invite_last_accepted_at: null,
              verified: false,
              portal_customer_created_at: null,
              vat_number: null,
              cc_emails: null,
              tax_exempt: false,
              parent_id: null,
              locale: null,
            },
            product: {
              id: 5256309,
              name: 'Standard',
              handle: 'ix_standard',
              description: '',
              accounting_code: '',
              request_credit_card: true,
              expiration_interval: 7,
              expiration_interval_unit: 'day',
              created_at: '2020-09-01T20:56:13-04:00',
              updated_at: '2020-10-11T23:03:57-04:00',
              price_in_cents: 0,
              interval: 7,
              interval_unit: 'day',
              initial_charge_in_cents: null,
              trial_price_in_cents: null,
              trial_interval: null,
              trial_interval_unit: 'month',
              archived_at: null,
              require_credit_card: false,
              return_params: '',
              taxable: false,
              update_return_url: '',
              tax_code: '',
              initial_charge_after_trial: false,
              version_number: 1,
              update_return_params: '',
              default_product_price_point_id: 969487,
              request_billing_address: false,
              require_billing_address: false,
              require_shipping_address: false,
              product_price_point_id: 969487,
              product_price_point_name: 'Original',
              product_price_point_handle: 'ix_standard',
              product_family: { id: 1526431, name: 'IX3', description: '', handle: 'inspectionxpert-cloud', accounting_code: null, created_at: '2020-05-21T14:21:33-04:00', updated_at: '2020-09-01T16:25:44-04:00' },
              public_signup_pages: [],
            },
            group: null,
          },
        ]);

        const result = await subWorker.updateDealStage({
          accountId: 'e0a01769-c8ed-4090-bd24-9c1b2241fb75',
          stageName: 'FIRST LOGIN',
          // stageName: 'TRIAL REQUESTED' // 6510423
          // stageName: 'FIRST LOGIN' // 6510424,
          // stageName: 'UPLOADED DRAWING 1' // 6510425,
          // stageName: 'UPLOADED DRAWING 2' // 6510426,
          // stageName: 'UPLOADED DRAWING 3' // 6510427,
          // stageName: 'UPLOADED DRAWING 4' // 6510535,
          // stageName: 'UPLOADED DRAWING 5' // 6510536,
          // stageName: 'ATTEMPTED UPLOAD DRAWING 6' // 6510537,
          // stageName: 'CLOSED WON' // 6510428,
          // stageName: 'CLOSED LOST' // 6510429,
        });
        expect(result).toBeDefined();
        expect(result.success).toBe(true);
        expect(result.crmDeal.dealstage).toBe('6510424'); // 'FIRST LOGIN': 5743686,
        expect(loggerMock.error).toHaveBeenCalledTimes(0);
      }, 30000); // Wait 30 seconds for asyn timeouts
    });

    describe('#updateRegistrationDetails()', () => {
      it('should throw an error if required input is not provided', async () => {
        await expect(subWorker.updateRegistrationDetails({})).rejects.toThrow();
        expect(loggerMock.error).toHaveBeenCalledTimes(1);
      });

      it('should throw an error if the request fails', async () => {
        // This test fails when using the real hubspot API, because the record is created successfully
        if (subWorker.isMocked) {
          // hubspotMock.deals.update.mockRejectedValueOnce(new Error('An error message.'));
          await expect(
            subWorker.updateRegistrationDetails({
              accountId: 'e0a01769-c8ed-4090-bd24-9c1b2241fb75',
              email: 'foo@bar.com',
              firstName: 'John',
              lastName: 'Doe',
              orgName: 'MTI',
            })
          ).rejects.toThrow();
          expect(loggerMock.error).toHaveBeenCalledTimes(1);
        }
      });

      it('should update the crm company and contact names when it succeeds', async () => {
        billingClientMock.getSubscriptionsForCustomer.mockResolvedValueOnce([
          {
            id: 41722182,
            state: 'trialing',
            trial_started_at: '2021-04-23T10:53:49-04:00',
            trial_ended_at: '2021-04-24T10:53:49-04:00',
            activated_at: null,
            created_at: '2021-04-23T10:53:49-04:00',
            updated_at: '2021-04-23T10:53:50-04:00',
            expires_at: null,
            balance_in_cents: 0,
            current_period_ends_at: '2021-04-24T10:53:49-04:00',
            next_assessment_at: '2021-04-24T10:53:49-04:00',
            canceled_at: null,
            cancellation_message: null,
            next_product_id: null,
            next_product_handle: null,
            cancel_at_end_of_period: false,
            payment_collection_method: 'remittance',
            snap_day: null,
            cancellation_method: null,
            current_period_started_at: '2021-04-23T10:53:49-04:00',
            previous_state: 'trialing',
            signup_payment_id: 492587390,
            signup_revenue: '0.00',
            delayed_cancel_at: null,
            coupon_code: null,
            total_revenue_in_cents: 0,
            product_price_in_cents: 0,
            product_version_number: 1,
            payment_type: null,
            referral_code: null,
            coupon_use_count: null,
            coupon_uses_allowed: null,
            reason_code: null,
            automatically_resume_at: null,
            coupon_codes: [],
            offer_id: null,
            payer_id: 42658762,
            receives_invoice_emails: null,
            product_price_point_id: 1094304,
            next_product_price_point_id: null,
            credit_balance_in_cents: 0,
            prepayment_balance_in_cents: 0,
            net_terms: null,
            stored_credential_transaction_id: null,
            locale: null,
            reference: null,
            currency: 'USD',
            on_hold_at: null,
            scheduled_cancellation_at: null,
            customer: {
              id: 42658762,
              first_name: 'JDixA',
              last_name: '.',
              organization: null,
              email: 'jordan+ixA@qualityxpert.com',
              created_at: '2021-04-23T10:53:49-04:00',
              updated_at: '2021-04-23T10:53:49-04:00',
              reference: '83215576-359c-42e6-895f-75aa1b2af61a',
              address: null,
              address_2: null,
              city: null,
              state: null,
              state_name: null,
              zip: null,
              country: null,
              country_name: null,
              phone: '',
              portal_invite_last_sent_at: null,
              portal_invite_last_accepted_at: null,
              verified: false,
              portal_customer_created_at: null,
              vat_number: null,
              cc_emails: null,
              tax_exempt: false,
              parent_id: null,
              locale: null,
            },
            product: {
              id: 5371107,
              name: 'Standard',
              handle: 'ix_standard',
              description: '',
              accounting_code: '',
              request_credit_card: true,
              expiration_interval: null,
              expiration_interval_unit: 'never',
              created_at: '2020-11-02T14:44:43-05:00',
              updated_at: '2021-04-19T11:40:31-04:00',
              price_in_cents: 0,
              interval: 1,
              interval_unit: 'month',
              initial_charge_in_cents: null,
              trial_price_in_cents: 0,
              trial_interval: 1,
              trial_interval_unit: 'day',
              archived_at: null,
              require_credit_card: false,
              return_params: '',
              taxable: false,
              update_return_url: '',
              tax_code: '',
              initial_charge_after_trial: false,
              version_number: 1,
              update_return_params: '',
              default_product_price_point_id: 1094304,
              request_billing_address: false,
              require_billing_address: false,
              require_shipping_address: false,
              product_price_point_id: 1094304,
              product_price_point_name: '20210101',
              product_price_point_handle: 'uuid:c6f1cdd0-ff71-0138-1e94-02b7af0a7112',
              product_family: { handle: 'inspectionxpert-cloud' },
              public_signup_pages: [],
            },
            group: null,
          },
        ]);

        billingClientMock.getSubscriptionById.mockResolvedValueOnce({
          id: 41722182,
          state: 'trialing',
          trial_started_at: '2021-04-23T10:53:49-04:00',
          trial_ended_at: '2021-04-24T10:53:49-04:00',
          activated_at: null,
          created_at: '2021-04-23T10:53:49-04:00',
          updated_at: '2021-04-23T10:53:50-04:00',
          expires_at: null,
          balance_in_cents: 0,
          current_period_ends_at: '2021-04-24T10:53:49-04:00',
          next_assessment_at: '2021-04-24T10:53:49-04:00',
          canceled_at: null,
          cancellation_message: null,
          next_product_id: null,
          next_product_handle: null,
          cancel_at_end_of_period: false,
          payment_collection_method: 'remittance',
          snap_day: null,
          cancellation_method: null,
          current_period_started_at: '2021-04-23T10:53:49-04:00',
          previous_state: 'trialing',
          signup_payment_id: 492587390,
          signup_revenue: '0.00',
          delayed_cancel_at: null,
          coupon_code: null,
          total_revenue_in_cents: 0,
          product_price_in_cents: 0,
          product_version_number: 1,
          payment_type: null,
          referral_code: null,
          coupon_use_count: null,
          coupon_uses_allowed: null,
          reason_code: null,
          automatically_resume_at: null,
          coupon_codes: [],
          offer_id: null,
          payer_id: 42658762,
          current_billing_amount_in_cents: 0,
          receives_invoice_emails: null,
          product_price_point_id: 1094304,
          next_product_price_point_id: null,
          credit_balance_in_cents: 0,
          prepayment_balance_in_cents: 0,
          net_terms: null,
          stored_credential_transaction_id: null,
          locale: null,
          reference: null,
          currency: 'USD',
          on_hold_at: null,
          scheduled_cancellation_at: null,
          customer: {
            id: 42658762,
            first_name: 'JDixA',
            last_name: '.',
            organization: null,
            email: 'jordan+ixA@qualityxpert.com',
            created_at: '2021-04-23T10:53:49-04:00',
            updated_at: '2021-04-23T10:53:49-04:00',
            reference: '83215576-359c-42e6-895f-75aa1b2af61a',
            address: null,
            address_2: null,
            city: null,
            state: null,
            state_name: null,
            zip: null,
            country: null,
            country_name: null,
            phone: '',
            portal_invite_last_sent_at: null,
            portal_invite_last_accepted_at: null,
            verified: false,
            portal_customer_created_at: null,
            vat_number: null,
            cc_emails: null,
            tax_exempt: false,
            parent_id: null,
            locale: null,
          },
          product: {
            id: 5371107,
            name: 'Standard',
            handle: 'ix_standard',
            description: '',
            accounting_code: '',
            request_credit_card: true,
            expiration_interval: null,
            expiration_interval_unit: 'never',
            created_at: '2020-11-02T14:44:43-05:00',
            updated_at: '2021-04-19T11:40:31-04:00',
            price_in_cents: 0,
            interval: 1,
            interval_unit: 'month',
            initial_charge_in_cents: null,
            trial_price_in_cents: 0,
            trial_interval: 1,
            trial_interval_unit: 'day',
            archived_at: null,
            require_credit_card: false,
            return_params: '',
            taxable: false,
            update_return_url: '',
            tax_code: '',
            initial_charge_after_trial: false,
            version_number: 1,
            update_return_params: '',
            default_product_price_point_id: 1094304,
            request_billing_address: false,
            require_billing_address: false,
            require_shipping_address: false,
            product_price_point_id: 1094304,
            product_price_point_name: '20210101',
            product_price_point_handle: 'uuid:c6f1cdd0-ff71-0138-1e94-02b7af0a7112',
            product_family: {
              id: 1526431,
              name: 'IX3',
              description: '',
              handle: 'inspectionxpert-cloud',
              accounting_code: null,
              created_at: '2020-05-21T14:21:33-04:00',
              updated_at: '2020-09-01T16:25:44-04:00',
            },
            public_signup_pages: [],
          },
          group: null,
        });

        billingClientMock.getCustomerByReference.mockResolvedValue({
          first_name: 'JDixA',
          last_name: '.',
          email: 'jordan+ixA@qualityxpert.com',
          cc_emails: null,
          organization: null,
          reference: '83215576-359c-42e6-895f-75aa1b2af61a',
          id: 42658762,
          created_at: '2021-04-23T10:53:49-04:00',
          updated_at: '2021-04-23T10:53:49-04:00',
          address: null,
          address_2: null,
          city: null,
          state: null,
          state_name: null,
          zip: null,
          country: null,
          country_name: null,
          phone: '',
          verified: false,
          portal_customer_created_at: null,
          portal_invite_last_sent_at: null,
          portal_invite_last_accepted_at: null,
          tax_exempt: false,
          vat_number: null,
          parent_id: null,
          locale: null,
          default_subscription_group_uid: null,
        });

        billingClientMock.getSubscriptionsForCustomer.mockResolvedValue([
          {
            id: 41722182,
            state: 'trialing',
            trial_started_at: '2021-04-23T10:53:49-04:00',
            trial_ended_at: '2021-04-24T10:53:49-04:00',
            activated_at: null,
            created_at: '2021-04-23T10:53:49-04:00',
            updated_at: '2021-04-23T10:53:50-04:00',
            expires_at: null,
            balance_in_cents: 0,
            current_period_ends_at: '2021-04-24T10:53:49-04:00',
            next_assessment_at: '2021-04-24T10:53:49-04:00',
            canceled_at: null,
            cancellation_message: null,
            next_product_id: null,
            next_product_handle: null,
            cancel_at_end_of_period: false,
            payment_collection_method: 'remittance',
            snap_day: null,
            cancellation_method: null,
            current_period_started_at: '2021-04-23T10:53:49-04:00',
            previous_state: 'trialing',
            signup_payment_id: 492587390,
            signup_revenue: '0.00',
            delayed_cancel_at: null,
            coupon_code: null,
            total_revenue_in_cents: 0,
            product_price_in_cents: 0,
            product_version_number: 1,
            payment_type: null,
            referral_code: null,
            coupon_use_count: null,
            coupon_uses_allowed: null,
            reason_code: null,
            automatically_resume_at: null,
            coupon_codes: [],
            offer_id: null,
            payer_id: 42658762,
            receives_invoice_emails: null,
            product_price_point_id: 1094304,
            next_product_price_point_id: null,
            credit_balance_in_cents: 0,
            prepayment_balance_in_cents: 0,
            net_terms: null,
            stored_credential_transaction_id: null,
            locale: null,
            reference: null,
            currency: 'USD',
            on_hold_at: null,
            scheduled_cancellation_at: null,
            customer: {
              id: 42658762,
              first_name: 'JDixA',
              last_name: '.',
              organization: null,
              email: 'jordan+ixA@qualityxpert.com',
              created_at: '2021-04-23T10:53:49-04:00',
              updated_at: '2021-04-23T10:53:49-04:00',
              reference: '83215576-359c-42e6-895f-75aa1b2af61a',
              address: null,
              address_2: null,
              city: null,
              state: null,
              state_name: null,
              zip: null,
              country: null,
              country_name: null,
              phone: '',
              portal_invite_last_sent_at: null,
              portal_invite_last_accepted_at: null,
              verified: false,
              portal_customer_created_at: null,
              vat_number: null,
              cc_emails: null,
              tax_exempt: false,
              parent_id: null,
              locale: null,
            },
            product: {
              id: 5371107,
              name: 'Standard',
              handle: 'ix_standard',
              description: '',
              accounting_code: '',
              request_credit_card: true,
              expiration_interval: null,
              expiration_interval_unit: 'never',
              created_at: '2020-11-02T14:44:43-05:00',
              updated_at: '2021-04-19T11:40:31-04:00',
              price_in_cents: 0,
              interval: 1,
              interval_unit: 'month',
              initial_charge_in_cents: null,
              trial_price_in_cents: 0,
              trial_interval: 1,
              trial_interval_unit: 'day',
              archived_at: null,
              require_credit_card: false,
              return_params: '',
              taxable: false,
              update_return_url: '',
              tax_code: '',
              initial_charge_after_trial: false,
              version_number: 1,
              update_return_params: '',
              default_product_price_point_id: 1094304,
              request_billing_address: false,
              require_billing_address: false,
              require_shipping_address: false,
              product_price_point_id: 1094304,
              product_price_point_name: '20210101',
              product_price_point_handle: 'uuid:c6f1cdd0-ff71-0138-1e94-02b7af0a7112',
              product_family: { handle: 'inspectionxpert-cloud' },
              public_signup_pages: [],
            },
            group: null,
          },
        ]);

        billingClientMock.getMetadataForCustomerWithId.mockResolvedValue({
          ixc_account_id: '83215576-359c-42e6-895f-75aa1b2af61a',
          hubspot_company_id: '5929423486',
        });

        billingClientMock.getSubscriptionById.mockResolvedValue({
          id: 41722182,
          state: 'trialing',
          trial_started_at: '2021-04-23T10:53:49-04:00',
          trial_ended_at: '2021-04-24T10:53:49-04:00',
          activated_at: null,
          created_at: '2021-04-23T10:53:49-04:00',
          updated_at: '2021-04-23T10:53:50-04:00',
          expires_at: null,
          balance_in_cents: 0,
          current_period_ends_at: '2021-04-24T10:53:49-04:00',
          next_assessment_at: '2021-04-24T10:53:49-04:00',
          canceled_at: null,
          cancellation_message: null,
          next_product_id: null,
          next_product_handle: null,
          cancel_at_end_of_period: false,
          payment_collection_method: 'remittance',
          snap_day: null,
          cancellation_method: null,
          current_period_started_at: '2021-04-23T10:53:49-04:00',
          previous_state: 'trialing',
          signup_payment_id: 492587390,
          signup_revenue: '0.00',
          delayed_cancel_at: null,
          coupon_code: null,
          total_revenue_in_cents: 0,
          product_price_in_cents: 0,
          product_version_number: 1,
          payment_type: null,
          referral_code: null,
          coupon_use_count: null,
          coupon_uses_allowed: null,
          reason_code: null,
          automatically_resume_at: null,
          coupon_codes: [],
          offer_id: null,
          payer_id: 42658762,
          current_billing_amount_in_cents: 0,
          receives_invoice_emails: null,
          product_price_point_id: 1094304,
          next_product_price_point_id: null,
          credit_balance_in_cents: 0,
          prepayment_balance_in_cents: 0,
          net_terms: null,
          stored_credential_transaction_id: null,
          locale: null,
          reference: null,
          currency: 'USD',
          on_hold_at: null,
          scheduled_cancellation_at: null,
          customer: {
            id: 42658762,
            first_name: 'JDixA',
            last_name: '.',
            organization: null,
            email: 'jordan+ixA@qualityxpert.com',
            created_at: '2021-04-23T10:53:49-04:00',
            updated_at: '2021-04-23T10:53:49-04:00',
            reference: '83215576-359c-42e6-895f-75aa1b2af61a',
            address: null,
            address_2: null,
            city: null,
            state: null,
            state_name: null,
            zip: null,
            country: null,
            country_name: null,
            phone: '',
            portal_invite_last_sent_at: null,
            portal_invite_last_accepted_at: null,
            verified: false,
            portal_customer_created_at: null,
            vat_number: null,
            cc_emails: null,
            tax_exempt: false,
            parent_id: null,
            locale: null,
          },
          product: {
            id: 5371107,
            name: 'Standard',
            handle: 'ix_standard',
            description: '',
            accounting_code: '',
            request_credit_card: true,
            expiration_interval: null,
            expiration_interval_unit: 'never',
            created_at: '2020-11-02T14:44:43-05:00',
            updated_at: '2021-04-19T11:40:31-04:00',
            price_in_cents: 0,
            interval: 1,
            interval_unit: 'month',
            initial_charge_in_cents: null,
            trial_price_in_cents: 0,
            trial_interval: 1,
            trial_interval_unit: 'day',
            archived_at: null,
            require_credit_card: false,
            return_params: '',
            taxable: false,
            update_return_url: '',
            tax_code: '',
            initial_charge_after_trial: false,
            version_number: 1,
            update_return_params: '',
            default_product_price_point_id: 1094304,
            request_billing_address: false,
            require_billing_address: false,
            require_shipping_address: false,
            product_price_point_id: 1094304,
            product_price_point_name: '20210101',
            product_price_point_handle: 'uuid:c6f1cdd0-ff71-0138-1e94-02b7af0a7112',
            product_family: {
              id: 1526431,
              name: 'IX3',
              description: '',
              handle: 'inspectionxpert-cloud',
              accounting_code: null,
              created_at: '2020-05-21T14:21:33-04:00',
              updated_at: '2020-09-01T16:25:44-04:00',
            },
            public_signup_pages: [],
          },
          group: null,
        });

        crmClientMock.getCompanyById.mockResolvedValueOnce({
          hs_num_open_deals: '0',
          website: 'qualityxpert.com',
          num_associated_contacts: '1',
          hs_analytics_first_timestamp: '1619189627933',
          description: 'Please enter your company URL below.',
          createdate: '1619189627568',
          ixc_account_id: '83215576-359c-42e6-895f-75aa1b2af61a',
          hs_num_blockers: '0',
          ix_chargify_link: 'https://inspectionxpert-test.chargify.com/customers/42658762',
          hs_analytics_num_visits: '0',
          ix_chargify_id: '42658762',
          first_contact_createdate: '1619189627933',
          hs_lastmodifieddate: '1619189676923',
          hs_analytics_source: 'OFFLINE',
          hs_predictivecontactscore_v2: '2.11',
          hs_num_decision_makers: '0',
          domain: 'qualityxpert.com',
          name: 'JDixA Org',
          hs_object_id: '5929423486',
          hs_num_contacts_with_buying_roles: '0',
          hs_num_child_companies: '0',
          hs_analytics_num_page_views: '0',
          hs_analytics_source_data_1: 'API',
          crm: 'hubspot',
          crmType: 'company',
          crmId: 5929423486,
        });

        crmClientMock.getContactByEmail.mockResolvedValueOnce({
          hs_is_unworked: 'true',
          firstname: 'JDixA',
          associatedcompanyid: '5929423486',
          num_unique_conversion_events: '0',
          hs_analytics_revenue: '0.0',
          hs_social_num_broadcast_clicks: '0',
          createdate: '1619189627933',
          hs_analytics_num_visits: '0',
          hs_social_linkedin_clicks: '0',
          hs_marketable_until_renewal: 'false',
          hs_marketable_status: 'false',
          hs_analytics_source: 'OFFLINE',
          hs_email_domain: 'qualityxpert.com',
          hs_analytics_num_page_views: '0',
          company: 'JDixA Org',
          email: 'jordan+ixa@qualityxpert.com',
          ixc_user_id: 'cd3ae7f4-8986-48d8-90a4-6820e58472a8',
          website: 'http://qualityxpert.com',
          lastmodifieddate: '1619189675257',
          hs_analytics_first_timestamp: '1619189627933',
          hs_social_google_plus_clicks: '0',
          hs_lifecyclestage_subscriber_date: '1619189627933',
          hs_analytics_average_page_views: '0',
          hs_all_contact_vids: '12501',
          hs_social_facebook_clicks: '0',
          hs_predictivescoringtier: 'tier_4',
          hs_is_contact: 'true',
          hs_predictivecontactscore_v2: '2.11',
          num_conversion_events: '0',
          hs_object_id: '12501',
          hs_analytics_num_event_completions: '0',
          hs_social_twitter_clicks: '0',
          hs_analytics_source_data_1: 'API',
          lifecyclestage: 'subscriber',
          crm: 'hubspot',
          crmType: 'contact',
          crmId: 12501,
        });

        billingClientMock.updateCustomer.mockResolvedValueOnce({
          first_name: 'JDixA',
          last_name: 'Duggan',
          email: 'jordan+ixA@qualityxpert.com',
          cc_emails: null,
          organization: 'RealOrgName',
          reference: '24e3bd20-8554-4548-af12-61ecf9894025',
          id: 42658998,
          created_at: '2021-04-23T11:07:56-04:00',
          updated_at: '2021-04-23T11:12:14-04:00',
          address: null,
          address_2: null,
          city: null,
          state: null,
          state_name: null,
          zip: null,
          country: null,
          country_name: null,
          phone: '',
          verified: false,
          portal_customer_created_at: null,
          portal_invite_last_sent_at: null,
          portal_invite_last_accepted_at: null,
          tax_exempt: false,
          vat_number: null,
          parent_id: null,
          locale: null,
          default_subscription_group_uid: null,
        });

        crmClientMock.updateContact.mockResolvedValueOnce({
          hs_is_unworked: 'true',
          firstname: 'JDixA',
          associatedcompanyid: '5929693496',
          num_unique_conversion_events: '0',
          hs_analytics_revenue: '0.0',
          hs_social_num_broadcast_clicks: '0',
          createdate: '1619190475416',
          hs_analytics_num_visits: '0',
          hs_social_linkedin_clicks: '0',
          hs_marketable_until_renewal: 'false',
          hs_marketable_status: 'false',
          hs_analytics_source: 'OFFLINE',
          hs_email_domain: 'qualityxpert.com',
          hs_analytics_num_page_views: '0',
          company: 'JDixA Org',
          email: 'jordan+ixa@qualityxpert.com',
          ixc_user_id: '9a9e9d34-b702-46aa-b9e1-6fc61e5d56fc',
          website: 'http://qualityxpert.com',
          lastmodifieddate: '1619190735119',
          hs_analytics_first_timestamp: '1619190475416',
          hs_social_google_plus_clicks: '0',
          hs_lifecyclestage_subscriber_date: '1619190475416',
          hs_analytics_average_page_views: '0',
          hs_all_contact_vids: '12551',
          lastname: 'Duggan',
          hs_social_facebook_clicks: '0',
          hs_predictivescoringtier: 'tier_4',
          hs_is_contact: 'true',
          hs_predictivecontactscore_v2: '2.11',
          num_conversion_events: '0',
          hs_object_id: '12551',
          hs_analytics_num_event_completions: '0',
          hs_social_twitter_clicks: '0',
          hs_analytics_source_data_1: 'API',
          lifecyclestage: 'subscriber',
          crm: 'hubspot',
          crmType: 'contact',
          crmId: 12551,
        });

        crmClientMock.updateCompany.mockResolvedValueOnce({
          hs_lastmodifieddate: '1619190735535',
          hs_object_id: '5929693496',
          name: 'RealOrgName',
          createdate: '1619190475003',
          crm: 'hubspot',
          crmType: 'company',
          crmId: 5929693496,
        });

        const result = await subWorker.updateRegistrationDetails({
          user: signupPageFormData.user,
          accountId: 'e0a01769-c8ed-4090-bd24-9c1b2241fb75',
          email: 'jordan+ixa@qualityxpert.com',
          firstName: 'JDixA',
          lastName: 'Duggan',
          orgName: 'RealOrgName',
        });
        expect(result).toBeDefined();
        expect(result.success).toBe(true);
        expect(loggerMock.error).toHaveBeenCalledTimes(0);
      }, 30000); // Wait 30 seconds for asyn timeouts
    });

    describe('#updateAccountDetails()', () => {
      it('should throw an error if required input is not provided', async () => {
        await expect(subWorker.updateAccountDetails({})).rejects.toThrow();
        expect(loggerMock.error).toHaveBeenCalledTimes(1);
      });

      it('should throw an error if the request fails', async () => {
        // This test fails when using the real hubspot API, because the record is created successfully
        if (subWorker.isMocked) {
          // hubspotMock.deals.update.mockRejectedValueOnce(new Error('An error message.'));
          await expect(
            subWorker.updateAccountDetails({
              accountId: 'e0a01769-c8ed-4090-bd24-9c1b2241fb75',
              orgName: 'MTI',
              orgPhone: '555-555-5555',
              orgStreet: 'Backer',
              orgStreet2: 'Suite 2',
              orgCity: 'Apex',
              orgPostalcode: '27539',
              orgRegion: 'North_Carolina',
              orgCountry: 'United States',
            })
          ).rejects.toThrow();
          expect(loggerMock.error).toHaveBeenCalledTimes(1);
        }
      });

      // TODO Will be tested in Cody's Account Profile update code
      it.skip('should update the crm company and deal name when it succeeds', async () => {
        billingClientMock.getSubscriptionsForCustomer.mockResolvedValueOnce([
          {
            id: 41722182,
            state: 'trialing',
            trial_started_at: '2021-04-23T10:53:49-04:00',
            trial_ended_at: '2021-04-24T10:53:49-04:00',
            activated_at: null,
            created_at: '2021-04-23T10:53:49-04:00',
            updated_at: '2021-04-23T10:53:50-04:00',
            expires_at: null,
            balance_in_cents: 0,
            current_period_ends_at: '2021-04-24T10:53:49-04:00',
            next_assessment_at: '2021-04-24T10:53:49-04:00',
            canceled_at: null,
            cancellation_message: null,
            next_product_id: null,
            next_product_handle: null,
            cancel_at_end_of_period: false,
            payment_collection_method: 'remittance',
            snap_day: null,
            cancellation_method: null,
            current_period_started_at: '2021-04-23T10:53:49-04:00',
            previous_state: 'trialing',
            signup_payment_id: 492587390,
            signup_revenue: '0.00',
            delayed_cancel_at: null,
            coupon_code: null,
            total_revenue_in_cents: 0,
            product_price_in_cents: 0,
            product_version_number: 1,
            payment_type: null,
            referral_code: null,
            coupon_use_count: null,
            coupon_uses_allowed: null,
            reason_code: null,
            automatically_resume_at: null,
            coupon_codes: [],
            offer_id: null,
            payer_id: 42658762,
            receives_invoice_emails: null,
            product_price_point_id: 1094304,
            next_product_price_point_id: null,
            credit_balance_in_cents: 0,
            prepayment_balance_in_cents: 0,
            net_terms: null,
            stored_credential_transaction_id: null,
            locale: null,
            reference: null,
            currency: 'USD',
            on_hold_at: null,
            scheduled_cancellation_at: null,
            customer: {
              id: 42658762,
              first_name: 'JDixA',
              last_name: '.',
              organization: null,
              email: 'jordan+ixA@qualityxpert.com',
              created_at: '2021-04-23T10:53:49-04:00',
              updated_at: '2021-04-23T10:53:49-04:00',
              reference: '83215576-359c-42e6-895f-75aa1b2af61a',
              address: null,
              address_2: null,
              city: null,
              state: null,
              state_name: null,
              zip: null,
              country: null,
              country_name: null,
              phone: '',
              portal_invite_last_sent_at: null,
              portal_invite_last_accepted_at: null,
              verified: false,
              portal_customer_created_at: null,
              vat_number: null,
              cc_emails: null,
              tax_exempt: false,
              parent_id: null,
              locale: null,
            },
            product: {
              id: 5371107,
              name: 'Standard',
              handle: 'ix_standard',
              description: '',
              accounting_code: '',
              request_credit_card: true,
              expiration_interval: null,
              expiration_interval_unit: 'never',
              created_at: '2020-11-02T14:44:43-05:00',
              updated_at: '2021-04-19T11:40:31-04:00',
              price_in_cents: 0,
              interval: 1,
              interval_unit: 'month',
              initial_charge_in_cents: null,
              trial_price_in_cents: 0,
              trial_interval: 1,
              trial_interval_unit: 'day',
              archived_at: null,
              require_credit_card: false,
              return_params: '',
              taxable: false,
              update_return_url: '',
              tax_code: '',
              initial_charge_after_trial: false,
              version_number: 1,
              update_return_params: '',
              default_product_price_point_id: 1094304,
              request_billing_address: false,
              require_billing_address: false,
              require_shipping_address: false,
              product_price_point_id: 1094304,
              product_price_point_name: '20210101',
              product_price_point_handle: 'uuid:c6f1cdd0-ff71-0138-1e94-02b7af0a7112',
              product_family: { handle: 'inspectionxpert-cloud' },
              public_signup_pages: [],
            },
            group: null,
          },
        ]);

        billingClientMock.getSubscriptionById.mockResolvedValueOnce({
          id: 41722182,
          state: 'trialing',
          trial_started_at: '2021-04-23T10:53:49-04:00',
          trial_ended_at: '2021-04-24T10:53:49-04:00',
          activated_at: null,
          created_at: '2021-04-23T10:53:49-04:00',
          updated_at: '2021-04-23T10:53:50-04:00',
          expires_at: null,
          balance_in_cents: 0,
          current_period_ends_at: '2021-04-24T10:53:49-04:00',
          next_assessment_at: '2021-04-24T10:53:49-04:00',
          canceled_at: null,
          cancellation_message: null,
          next_product_id: null,
          next_product_handle: null,
          cancel_at_end_of_period: false,
          payment_collection_method: 'remittance',
          snap_day: null,
          cancellation_method: null,
          current_period_started_at: '2021-04-23T10:53:49-04:00',
          previous_state: 'trialing',
          signup_payment_id: 492587390,
          signup_revenue: '0.00',
          delayed_cancel_at: null,
          coupon_code: null,
          total_revenue_in_cents: 0,
          product_price_in_cents: 0,
          product_version_number: 1,
          payment_type: null,
          referral_code: null,
          coupon_use_count: null,
          coupon_uses_allowed: null,
          reason_code: null,
          automatically_resume_at: null,
          coupon_codes: [],
          offer_id: null,
          payer_id: 42658762,
          current_billing_amount_in_cents: 0,
          receives_invoice_emails: null,
          product_price_point_id: 1094304,
          next_product_price_point_id: null,
          credit_balance_in_cents: 0,
          prepayment_balance_in_cents: 0,
          net_terms: null,
          stored_credential_transaction_id: null,
          locale: null,
          reference: null,
          currency: 'USD',
          on_hold_at: null,
          scheduled_cancellation_at: null,
          customer: {
            id: 42658762,
            first_name: 'JDixA',
            last_name: '.',
            organization: null,
            email: 'jordan+ixA@qualityxpert.com',
            created_at: '2021-04-23T10:53:49-04:00',
            updated_at: '2021-04-23T10:53:49-04:00',
            reference: '83215576-359c-42e6-895f-75aa1b2af61a',
            address: null,
            address_2: null,
            city: null,
            state: null,
            state_name: null,
            zip: null,
            country: null,
            country_name: null,
            phone: '',
            portal_invite_last_sent_at: null,
            portal_invite_last_accepted_at: null,
            verified: false,
            portal_customer_created_at: null,
            vat_number: null,
            cc_emails: null,
            tax_exempt: false,
            parent_id: null,
            locale: null,
          },
          product: {
            id: 5371107,
            name: 'Standard',
            handle: 'ix_standard',
            description: '',
            accounting_code: '',
            request_credit_card: true,
            expiration_interval: null,
            expiration_interval_unit: 'never',
            created_at: '2020-11-02T14:44:43-05:00',
            updated_at: '2021-04-19T11:40:31-04:00',
            price_in_cents: 0,
            interval: 1,
            interval_unit: 'month',
            initial_charge_in_cents: null,
            trial_price_in_cents: 0,
            trial_interval: 1,
            trial_interval_unit: 'day',
            archived_at: null,
            require_credit_card: false,
            return_params: '',
            taxable: false,
            update_return_url: '',
            tax_code: '',
            initial_charge_after_trial: false,
            version_number: 1,
            update_return_params: '',
            default_product_price_point_id: 1094304,
            request_billing_address: false,
            require_billing_address: false,
            require_shipping_address: false,
            product_price_point_id: 1094304,
            product_price_point_name: '20210101',
            product_price_point_handle: 'uuid:c6f1cdd0-ff71-0138-1e94-02b7af0a7112',
            product_family: {
              id: 1526431,
              name: 'IX3',
              description: '',
              handle: 'inspectionxpert-cloud',
              accounting_code: null,
              created_at: '2020-05-21T14:21:33-04:00',
              updated_at: '2020-09-01T16:25:44-04:00',
            },
            public_signup_pages: [],
          },
          group: null,
        });

        billingClientMock.getCustomerByReference.mockResolvedValue({
          first_name: 'JDixA',
          last_name: '.',
          email: 'jordan+ixA@qualityxpert.com',
          cc_emails: null,
          organization: null,
          reference: '83215576-359c-42e6-895f-75aa1b2af61a',
          id: 42658762,
          created_at: '2021-04-23T10:53:49-04:00',
          updated_at: '2021-04-23T10:53:49-04:00',
          address: null,
          address_2: null,
          city: null,
          state: null,
          state_name: null,
          zip: null,
          country: null,
          country_name: null,
          phone: '',
          verified: false,
          portal_customer_created_at: null,
          portal_invite_last_sent_at: null,
          portal_invite_last_accepted_at: null,
          tax_exempt: false,
          vat_number: null,
          parent_id: null,
          locale: null,
          default_subscription_group_uid: null,
        });

        billingClientMock.getSubscriptionsForCustomer.mockResolvedValue([
          {
            id: 41722182,
            state: 'trialing',
            trial_started_at: '2021-04-23T10:53:49-04:00',
            trial_ended_at: '2021-04-24T10:53:49-04:00',
            activated_at: null,
            created_at: '2021-04-23T10:53:49-04:00',
            updated_at: '2021-04-23T10:53:50-04:00',
            expires_at: null,
            balance_in_cents: 0,
            current_period_ends_at: '2021-04-24T10:53:49-04:00',
            next_assessment_at: '2021-04-24T10:53:49-04:00',
            canceled_at: null,
            cancellation_message: null,
            next_product_id: null,
            next_product_handle: null,
            cancel_at_end_of_period: false,
            payment_collection_method: 'remittance',
            snap_day: null,
            cancellation_method: null,
            current_period_started_at: '2021-04-23T10:53:49-04:00',
            previous_state: 'trialing',
            signup_payment_id: 492587390,
            signup_revenue: '0.00',
            delayed_cancel_at: null,
            coupon_code: null,
            total_revenue_in_cents: 0,
            product_price_in_cents: 0,
            product_version_number: 1,
            payment_type: null,
            referral_code: null,
            coupon_use_count: null,
            coupon_uses_allowed: null,
            reason_code: null,
            automatically_resume_at: null,
            coupon_codes: [],
            offer_id: null,
            payer_id: 42658762,
            receives_invoice_emails: null,
            product_price_point_id: 1094304,
            next_product_price_point_id: null,
            credit_balance_in_cents: 0,
            prepayment_balance_in_cents: 0,
            net_terms: null,
            stored_credential_transaction_id: null,
            locale: null,
            reference: null,
            currency: 'USD',
            on_hold_at: null,
            scheduled_cancellation_at: null,
            customer: {
              id: 42658762,
              first_name: 'JDixA',
              last_name: '.',
              organization: null,
              email: 'jordan+ixA@qualityxpert.com',
              created_at: '2021-04-23T10:53:49-04:00',
              updated_at: '2021-04-23T10:53:49-04:00',
              reference: '83215576-359c-42e6-895f-75aa1b2af61a',
              address: null,
              address_2: null,
              city: null,
              state: null,
              state_name: null,
              zip: null,
              country: null,
              country_name: null,
              phone: '',
              portal_invite_last_sent_at: null,
              portal_invite_last_accepted_at: null,
              verified: false,
              portal_customer_created_at: null,
              vat_number: null,
              cc_emails: null,
              tax_exempt: false,
              parent_id: null,
              locale: null,
            },
            product: {
              id: 5371107,
              name: 'Standard',
              handle: 'ix_standard',
              description: '',
              accounting_code: '',
              request_credit_card: true,
              expiration_interval: null,
              expiration_interval_unit: 'never',
              created_at: '2020-11-02T14:44:43-05:00',
              updated_at: '2021-04-19T11:40:31-04:00',
              price_in_cents: 0,
              interval: 1,
              interval_unit: 'month',
              initial_charge_in_cents: null,
              trial_price_in_cents: 0,
              trial_interval: 1,
              trial_interval_unit: 'day',
              archived_at: null,
              require_credit_card: false,
              return_params: '',
              taxable: false,
              update_return_url: '',
              tax_code: '',
              initial_charge_after_trial: false,
              version_number: 1,
              update_return_params: '',
              default_product_price_point_id: 1094304,
              request_billing_address: false,
              require_billing_address: false,
              require_shipping_address: false,
              product_price_point_id: 1094304,
              product_price_point_name: '20210101',
              product_price_point_handle: 'uuid:c6f1cdd0-ff71-0138-1e94-02b7af0a7112',
              product_family: { handle: 'inspectionxpert-cloud' },
              public_signup_pages: [],
            },
            group: null,
          },
        ]);

        billingClientMock.getMetadataForCustomerWithId.mockResolvedValue({
          ixc_account_id: '83215576-359c-42e6-895f-75aa1b2af61a',
          hubspot_company_id: '5929423486',
        });

        billingClientMock.getSubscriptionById.mockResolvedValue({
          id: 41722182,
          state: 'trialing',
          trial_started_at: '2021-04-23T10:53:49-04:00',
          trial_ended_at: '2021-04-24T10:53:49-04:00',
          activated_at: null,
          created_at: '2021-04-23T10:53:49-04:00',
          updated_at: '2021-04-23T10:53:50-04:00',
          expires_at: null,
          balance_in_cents: 0,
          current_period_ends_at: '2021-04-24T10:53:49-04:00',
          next_assessment_at: '2021-04-24T10:53:49-04:00',
          canceled_at: null,
          cancellation_message: null,
          next_product_id: null,
          next_product_handle: null,
          cancel_at_end_of_period: false,
          payment_collection_method: 'remittance',
          snap_day: null,
          cancellation_method: null,
          current_period_started_at: '2021-04-23T10:53:49-04:00',
          previous_state: 'trialing',
          signup_payment_id: 492587390,
          signup_revenue: '0.00',
          delayed_cancel_at: null,
          coupon_code: null,
          total_revenue_in_cents: 0,
          product_price_in_cents: 0,
          product_version_number: 1,
          payment_type: null,
          referral_code: null,
          coupon_use_count: null,
          coupon_uses_allowed: null,
          reason_code: null,
          automatically_resume_at: null,
          coupon_codes: [],
          offer_id: null,
          payer_id: 42658762,
          current_billing_amount_in_cents: 0,
          receives_invoice_emails: null,
          product_price_point_id: 1094304,
          next_product_price_point_id: null,
          credit_balance_in_cents: 0,
          prepayment_balance_in_cents: 0,
          net_terms: null,
          stored_credential_transaction_id: null,
          locale: null,
          reference: null,
          currency: 'USD',
          on_hold_at: null,
          scheduled_cancellation_at: null,
          customer: {
            id: 42658762,
            first_name: 'JDixA',
            last_name: '.',
            organization: null,
            email: 'jordan+ixA@qualityxpert.com',
            created_at: '2021-04-23T10:53:49-04:00',
            updated_at: '2021-04-23T10:53:49-04:00',
            reference: '83215576-359c-42e6-895f-75aa1b2af61a',
            address: null,
            address_2: null,
            city: null,
            state: null,
            state_name: null,
            zip: null,
            country: null,
            country_name: null,
            phone: '',
            portal_invite_last_sent_at: null,
            portal_invite_last_accepted_at: null,
            verified: false,
            portal_customer_created_at: null,
            vat_number: null,
            cc_emails: null,
            tax_exempt: false,
            parent_id: null,
            locale: null,
          },
          product: {
            id: 5371107,
            name: 'Standard',
            handle: 'ix_standard',
            description: '',
            accounting_code: '',
            request_credit_card: true,
            expiration_interval: null,
            expiration_interval_unit: 'never',
            created_at: '2020-11-02T14:44:43-05:00',
            updated_at: '2021-04-19T11:40:31-04:00',
            price_in_cents: 0,
            interval: 1,
            interval_unit: 'month',
            initial_charge_in_cents: null,
            trial_price_in_cents: 0,
            trial_interval: 1,
            trial_interval_unit: 'day',
            archived_at: null,
            require_credit_card: false,
            return_params: '',
            taxable: false,
            update_return_url: '',
            tax_code: '',
            initial_charge_after_trial: false,
            version_number: 1,
            update_return_params: '',
            default_product_price_point_id: 1094304,
            request_billing_address: false,
            require_billing_address: false,
            require_shipping_address: false,
            product_price_point_id: 1094304,
            product_price_point_name: '20210101',
            product_price_point_handle: 'uuid:c6f1cdd0-ff71-0138-1e94-02b7af0a7112',
            product_family: {
              id: 1526431,
              name: 'IX3',
              description: '',
              handle: 'inspectionxpert-cloud',
              accounting_code: null,
              created_at: '2020-05-21T14:21:33-04:00',
              updated_at: '2020-09-01T16:25:44-04:00',
            },
            public_signup_pages: [],
          },
          group: null,
        });

        crmClientMock.getCompanyById.mockResolvedValueOnce({
          hs_num_open_deals: '0',
          website: 'qualityxpert.com',
          num_associated_contacts: '1',
          hs_analytics_first_timestamp: '1619189627933',
          description: 'Please enter your company URL below.',
          createdate: '1619189627568',
          ixc_account_id: '83215576-359c-42e6-895f-75aa1b2af61a',
          hs_num_blockers: '0',
          ix_chargify_link: 'https://inspectionxpert-test.chargify.com/customers/42658762',
          hs_analytics_num_visits: '0',
          ix_chargify_id: '42658762',
          first_contact_createdate: '1619189627933',
          hs_lastmodifieddate: '1619189676923',
          hs_analytics_source: 'OFFLINE',
          hs_predictivecontactscore_v2: '2.11',
          hs_num_decision_makers: '0',
          domain: 'qualityxpert.com',
          name: 'JDixA Org',
          hs_object_id: '5929423486',
          hs_num_contacts_with_buying_roles: '0',
          hs_num_child_companies: '0',
          hs_analytics_num_page_views: '0',
          hs_analytics_source_data_1: 'API',
          crm: 'hubspot',
          crmType: 'company',
          crmId: 5929423486,
        });

        crmClientMock.getContactByEmail.mockResolvedValueOnce({
          hs_is_unworked: 'true',
          firstname: 'JDixA',
          associatedcompanyid: '5929423486',
          num_unique_conversion_events: '0',
          hs_analytics_revenue: '0.0',
          hs_social_num_broadcast_clicks: '0',
          createdate: '1619189627933',
          hs_analytics_num_visits: '0',
          hs_social_linkedin_clicks: '0',
          hs_marketable_until_renewal: 'false',
          hs_marketable_status: 'false',
          hs_analytics_source: 'OFFLINE',
          hs_email_domain: 'qualityxpert.com',
          hs_analytics_num_page_views: '0',
          company: 'JDixA Org',
          email: 'jordan+ixa@qualityxpert.com',
          ixc_user_id: 'cd3ae7f4-8986-48d8-90a4-6820e58472a8',
          website: 'http://qualityxpert.com',
          lastmodifieddate: '1619189675257',
          hs_analytics_first_timestamp: '1619189627933',
          hs_social_google_plus_clicks: '0',
          hs_lifecyclestage_subscriber_date: '1619189627933',
          hs_analytics_average_page_views: '0',
          hs_all_contact_vids: '12501',
          hs_social_facebook_clicks: '0',
          hs_predictivescoringtier: 'tier_4',
          hs_is_contact: 'true',
          hs_predictivecontactscore_v2: '2.11',
          num_conversion_events: '0',
          hs_object_id: '12501',
          hs_analytics_num_event_completions: '0',
          hs_social_twitter_clicks: '0',
          hs_analytics_source_data_1: 'API',
          lifecyclestage: 'subscriber',
          crm: 'hubspot',
          crmType: 'contact',
          crmId: 12501,
        });

        billingClientMock.updateCustomer.mockResolvedValueOnce({
          first_name: 'JDixA',
          last_name: 'Duggan',
          email: 'jordan+ixA@qualityxpert.com',
          cc_emails: null,
          organization: 'RealOrgName',
          reference: '24e3bd20-8554-4548-af12-61ecf9894025',
          id: 42658998,
          created_at: '2021-04-23T11:07:56-04:00',
          updated_at: '2021-04-23T11:12:14-04:00',
          address: null,
          address_2: null,
          city: null,
          state: null,
          state_name: null,
          zip: null,
          country: null,
          country_name: null,
          phone: '',
          verified: false,
          portal_customer_created_at: null,
          portal_invite_last_sent_at: null,
          portal_invite_last_accepted_at: null,
          tax_exempt: false,
          vat_number: null,
          parent_id: null,
          locale: null,
          default_subscription_group_uid: null,
        });

        crmClientMock.updateContact.mockResolvedValueOnce({
          hs_is_unworked: 'true',
          firstname: 'JDixA',
          associatedcompanyid: '5929693496',
          num_unique_conversion_events: '0',
          hs_analytics_revenue: '0.0',
          hs_social_num_broadcast_clicks: '0',
          createdate: '1619190475416',
          hs_analytics_num_visits: '0',
          hs_social_linkedin_clicks: '0',
          hs_marketable_until_renewal: 'false',
          hs_marketable_status: 'false',
          hs_analytics_source: 'OFFLINE',
          hs_email_domain: 'qualityxpert.com',
          hs_analytics_num_page_views: '0',
          company: 'JDixA Org',
          email: 'jordan+ixa@qualityxpert.com',
          ixc_user_id: '9a9e9d34-b702-46aa-b9e1-6fc61e5d56fc',
          website: 'http://qualityxpert.com',
          lastmodifieddate: '1619190735119',
          hs_analytics_first_timestamp: '1619190475416',
          hs_social_google_plus_clicks: '0',
          hs_lifecyclestage_subscriber_date: '1619190475416',
          hs_analytics_average_page_views: '0',
          hs_all_contact_vids: '12551',
          lastname: 'Duggan',
          hs_social_facebook_clicks: '0',
          hs_predictivescoringtier: 'tier_4',
          hs_is_contact: 'true',
          hs_predictivecontactscore_v2: '2.11',
          num_conversion_events: '0',
          hs_object_id: '12551',
          hs_analytics_num_event_completions: '0',
          hs_social_twitter_clicks: '0',
          hs_analytics_source_data_1: 'API',
          lifecyclestage: 'subscriber',
          crm: 'hubspot',
          crmType: 'contact',
          crmId: 12551,
        });

        crmClientMock.updateCompany.mockResolvedValueOnce({
          hs_lastmodifieddate: '1619190735535',
          hs_object_id: '5929693496',
          name: 'RealOrgName',
          createdate: '1619190475003',
          crm: 'hubspot',
          crmType: 'company',
          crmId: 5929693496,
        });

        const result = await subWorker.updateRegistrationDetails({
          accountId: 'e0a01769-c8ed-4090-bd24-9c1b2241fb75',
          email: 'jordan+ixa@qualityxpert.com',
          firstName: 'JDixA',
          lastName: 'Duggan',
          orgName: 'RealOrgName',
        });
        expect(result).toBeDefined();
        expect(result.success).toBe(true);
        expect(loggerMock.error).toHaveBeenCalledTimes(0);
      }, 30000); // Wait 30 seconds for asyn timeouts
    });

    describe('#extractAppContext', () => {
      it('converts data to required form', () => {
        const data = SubscriptionWorker.extractAppContext({
          user: {
            id: 'some',
            firstName: 'some',
            lastName: 'some',
            email: 'some@inspectionxpert.com',
            profile: { phoneNumber: 'some' },
            roles: ['some'],
          },
          account: {
            //
            id: 'some',
            orgName: 'some',
            orgStreet: 'some',
            orgStreet2: 'some',
            orgCity: 'some',
            orgRegion: 'some',
            orgPostalcode: 'some',
            orgCountry: 'some',
          },
        });
        expect(data).toMatchObject({
          userId: 'some',
          accountId: 'some',
          firstName: 'some',
          lastName: 'some',
          email: 'some@inspectionxpert.com',
          phone: 'some',
          roles: 'some',
          orgName: 'some',
          orgStreet: 'some',
          orgStreet2: 'some',
          orgCity: 'some',
          orgRegion: 'some',
          orgPostalcode: 'some',
          orgCountry: 'some',
          domain: 'inspectionxpert.com',
        });
      });
    });

    describe('#getSubscriptionForId', () => {
      it('warns if no subscription is found', async () => {
        billingClientMock.getSubscriptionById.mockResolvedValueOnce(null);
        const sub = await subWorker.getSubscriptionForId('none');
        expect(loggerMock.warn).toHaveBeenCalledTimes(1);
        expect(sub).toBeNull();
      });

      it('assigns custom fields if any are found', async () => {
        billingClientMock.getSubscriptionById.mockResolvedValueOnce({});
        billingClientMock.getSubscriptionComponents.mockResolvedValueOnce('found');
        billingClientMock.getSubscriptionCustomFields.mockResolvedValueOnce([{ name: 'name', value: 'value' }]);

        const sub = await subWorker.getSubscriptionForId('none');
        expect(billingClientMock.getSubscriptionCustomFields).toHaveBeenCalledTimes(1);
        expect(sub.name).toBe('value');
      });

      it('returns the subscription', async () => {
        billingClientMock.getSubscriptionById.mockResolvedValueOnce({});
        billingClientMock.getSubscriptionComponents.mockResolvedValueOnce('found');
        billingClientMock.getSubscriptionCustomFields.mockResolvedValueOnce([{ name: 'name', value: 'value' }]);

        const sub = await subWorker.getSubscriptionForId('none');
        expect(billingClientMock.getSubscriptionById).toHaveBeenCalledTimes(1);
        expect(billingClientMock.getSubscriptionComponents).toHaveBeenCalledTimes(1);
        expect(billingClientMock.getSubscriptionCustomFields).toHaveBeenCalledTimes(1);
        expect(sub).toMatchObject({ components: 'found', name: 'value' });
      });
    });

    describe('#getSubscriptionForAccountId', () => {
      it('warns if no subscriptions are found', async () => {
        const getSubscriptionForIdMock = jest.spyOn(subWorker, 'getSubscriptionsForAccountId').mockImplementation(() => []);

        const sub = await subWorker.getSubscriptionForAccountId('none');
        expect(loggerMock.warn).toHaveBeenCalledTimes(1);
        expect(sub).toBeNull();

        getSubscriptionForIdMock.mockRestore();
      });

      it('filters subscriptions by passed handle', async () => {
        const getSubscriptionForIdMock = jest.spyOn(subWorker, 'getSubscriptionsForAccountId').mockImplementation(() => [
          {
            //
            id: 'some',
            customer: { organization: 'some', first_name: 'some', last_name: 'some' },
            product: { handle: 'keep', product_family: { handle: 'inspectionxpert-cloud' } },
          },
          {
            //
            id: 'some',
            customer: { organization: 'some', first_name: 'some', last_name: 'some' },
            product: { handle: 'lose', product_family: { handle: 'inspectionxpert-cloud' } },
          },
        ]);
        billingClientMock.getSubscriptionComponents.mockResolvedValueOnce('found');
        billingClientMock.getSubscriptionCustomFields.mockResolvedValueOnce([]);

        const sub = await subWorker.getSubscriptionForAccountId('none', 'keep');
        expect(sub).toMatchObject({
          id: 'some',
          customer: { organization: 'some', first_name: 'some', last_name: 'some' },
          product: { handle: 'keep', product_family: { handle: 'inspectionxpert-cloud' } },
          components: 'found',
        });

        getSubscriptionForIdMock.mockRestore();
      });

      it('warns if filtering returns no subscriptions', async () => {
        const getSubscriptionForIdMock = jest.spyOn(subWorker, 'getSubscriptionsForAccountId').mockImplementation(() => [
          {
            id: 'some',
            customer: { organization: 'some', first_name: 'some', last_name: 'some' },
            product: { product_family: { handle: 'inspectionxpert-cloud' } },
          },
        ]);
        billingClientMock.getSubscriptionComponents.mockResolvedValueOnce('found');
        billingClientMock.getSubscriptionCustomFields.mockResolvedValueOnce([]);

        const sub = await subWorker.getSubscriptionForAccountId('none', 'none');
        expect(loggerMock.warn).toHaveBeenCalledTimes(1);
        expect(sub).toBeNull();

        getSubscriptionForIdMock.mockRestore();
      });

      it('warns if more than one subscription was found', async () => {
        const getSubscriptionForIdMock = jest.spyOn(subWorker, 'getSubscriptionsForAccountId').mockImplementation(() => [
          {
            //
            id: 'some',
            customer: { organization: 'some', first_name: 'some', last_name: 'some' },
            product: { product_family: { handle: 'inspectionxpert-cloud' } },
          },
          {
            //
            id: 'some',
            customer: { organization: 'some', first_name: 'some', last_name: 'some' },
            product: { product_family: { handle: 'inspectionxpert-cloud' } },
          },
          {
            //
            id: 'some',
            customer: { organization: 'some', first_name: 'some', last_name: 'some' },
            product: { product_family: { handle: 'inspectionxpert-cloud' } },
          },
        ]);
        billingClientMock.getSubscriptionComponents.mockResolvedValueOnce('found');
        billingClientMock.getSubscriptionCustomFields.mockResolvedValueOnce([]);

        const sub = await subWorker.getSubscriptionForAccountId('none');
        expect(loggerMock.warn).toHaveBeenCalledTimes(1);
        expect(sub).toMatchObject({
          id: 'some',
          customer: { organization: 'some', first_name: 'some', last_name: 'some' },
          product: { product_family: { handle: 'inspectionxpert-cloud' } },
          components: 'found',
        });

        getSubscriptionForIdMock.mockRestore();
      });

      it('assigns custom fields if found', async () => {
        const getSubscriptionForIdMock = jest.spyOn(subWorker, 'getSubscriptionsForAccountId').mockImplementation(() => [
          {
            //
            id: 'some',
            customer: { organization: 'some', first_name: 'some', last_name: 'some' },
            product: { product_family: { handle: 'inspectionxpert-cloud' } },
          },
        ]);
        billingClientMock.getSubscriptionComponents.mockResolvedValueOnce('found');
        billingClientMock.getSubscriptionCustomFields.mockResolvedValueOnce([{ name: 'name', value: 'value' }]);

        const sub = await subWorker.getSubscriptionForAccountId('none');
        expect(sub).toMatchObject({
          id: 'some',
          customer: { organization: 'some', first_name: 'some', last_name: 'some' },
          product: { product_family: { handle: 'inspectionxpert-cloud' } },
          components: 'found',
          name: 'value',
        });

        getSubscriptionForIdMock.mockRestore();
      });
    });

    describe('#getSubscriptionsForAccountId', () => {
      it('warns if no customer is found', async () => {
        billingClientMock.getCustomerByReference.mockResolvedValueOnce(null);
        const subs = await subWorker.getSubscriptionsForAccountId('none');
        expect(loggerMock.warn).toHaveBeenCalledTimes(1);
        expect(subs).toBeNull();
      });

      it('warns if no subscriptions are found', async () => {
        billingClientMock.getCustomerByReference.mockResolvedValueOnce({ id: 'some' });
        billingClientMock.getSubscriptionsForCustomer.mockResolvedValueOnce(null);
        const subs = await subWorker.getSubscriptionsForAccountId('none');
        expect(loggerMock.warn).toHaveBeenCalledTimes(1);
        expect(subs).toBeNull();
      });

      it('filters the subscriptions and collects extra data', async () => {
        billingClientMock.getCustomerByReference.mockResolvedValueOnce({ id: 'some' });
        billingClientMock.getSubscriptionsForCustomer.mockResolvedValueOnce([{ id: 'some', product: { product_family: { handle: 'inspectionxpert-cloud' } } }, { id: 'some2' }]);
        billingClientMock.getSubscriptionById.mockResolvedValueOnce({ current_billing_amount_in_cents: 'some' });
        const subs = await subWorker.getSubscriptionsForAccountId('none');
        expect(subs).toMatchObject([{ id: 'some', current_billing_amount_in_cents: 'some', product: { product_family: { handle: 'inspectionxpert-cloud' } } }]);
      });
    });

    describe('#getBillingInitData', () => {
      it('returns data', async () => {
        const cat = {
          'inspectionxpert-cloud': {
            products: {
              ix_standard: { prices: [] },
            },
            components: {
              paid_user_seat_ss: {
                id: 'some',
                handle: 'paid_user_seat_ss',
                name: 'some',
                kind: 'some',
                unit_name: 'some',
                pricing_scheme: 'some',
              },
            },
          },
        };
        billingClientMock.getInitData.mockReturnValueOnce({ auth: 'billing_permissions' });
        const fetchCatalogMock = jest.spyOn(subWorker, 'fetchCatalog').mockImplementation(() => cat);
        const getSubscriptionForIdMock = jest.spyOn(subWorker, 'getSubscriptionForAccountId').mockImplementation(() => ({
          id: 'some',
          customer: { organization: 'some', first_name: 'some', last_name: 'some' },
        }));

        const result = await subWorker.getBillingInitData('some');
        expect(result).toMatchObject({ auth: 'billing_permissions', catalog: JSON.stringify(cat), subscription: { id: 'some', customer: { organization: 'some', first_name: 'some', last_name: 'some' } } });
        fetchCatalogMock.mockRestore();
        getSubscriptionForIdMock.mockRestore();
      });
    });

    describe('#calculateDealValue', () => {
      it('should populate the result properly if given a valid adapter', async () => {
        // When crm=ARR and billing=MRR
        crmClientMock.deal_value_is_arr = true;
        billingClientMock.subscription_value_is_arr = false;
        let sw = new SubscriptionWorker({
          s3: mockS3,
          crmClient: crmClientMock,
          billingClient: billingClientMock,
          logger: loggerMock,
        });
        expect(sw.calculateDealValue(12)).toBe(144);

        // When crm=MRR and billing=ARR
        crmClientMock.deal_value_is_arr = false;
        billingClientMock.subscription_value_is_arr = true;
        sw = new SubscriptionWorker({
          s3: mockS3,
          crmClient: crmClientMock,
          billingClient: billingClientMock,
          logger: loggerMock,
        });
        expect(sw.calculateDealValue(12)).toBe(1);

        // When crm=ARR and billing=ARR
        crmClientMock.deal_value_is_arr = true;
        billingClientMock.subscription_value_is_arr = true;
        sw = new SubscriptionWorker({
          s3: mockS3,
          crmClient: crmClientMock,
          billingClient: billingClientMock,
          logger: loggerMock,
        });
        expect(sw.calculateDealValue(12)).toBe(12);

        // When crm=MRR and billing=MRR
        crmClientMock.deal_value_is_arr = false;
        billingClientMock.subscription_value_is_arr = false;
        sw = new SubscriptionWorker({
          s3: mockS3,
          crmClient: crmClientMock,
          billingClient: billingClientMock,
          logger: loggerMock,
        });
        expect(sw.calculateDealValue(12)).toBe(12);
      });
    });

    describe('#paymentFailure', () => {
      it('should throw error if missing subscription', async () => {
        await expect(subWorker.paymentFailure({}, { isTest: true })).rejects.toThrow();
        expect(loggerMock.error).toHaveBeenCalledTimes(1);
      });

      it('should return true on success', async () => {
        const fakeSub = { customer: { id: 1234, email: 'fake@gmail.com' } };
        const fakeTrans = { created_at: '2021-07-07T00:00:00-5:00' };
        billingClientMock.getBillingPortalLink.mockResolvedValueOnce(null);
        const subs = await subWorker.paymentFailure({ subscription: fakeSub, transaction: fakeTrans }, { isTest: true });
        expect(subs).toBeTruthy();
      });
    });
  });

  describe('Helpers', () => {
    describe('#logError', () => {
      it('logs an axios error', () => {
        subWorker.logError({ response: { data: { errors: 'Error' } } }, 'Message');
        expect(loggerMock.error).toHaveBeenCalledTimes(1);
      });

      it('logs a non-axios error', () => {
        subWorker.logError('Error', 'Message');
        expect(loggerMock.error).toHaveBeenCalledTimes(1);
      });
    });

    describe('#validateCatalogKey', () => {
      it('throws error if product family does not exist', async () => {
        billingClientMock.getCatalog.mockResolvedValueOnce({ prod_fam: {} });
        await subWorker.loadCatalog();
        expect(() => subWorker.validateCatalogKey('none')).toThrow(Error);
      });

      it('does not throw error if product family exists', async () => {
        billingClientMock.getCatalog.mockResolvedValueOnce({ prod_fam: {} });
        await subWorker.loadCatalog();
        expect(() => subWorker.validateCatalogKey('prod_fam')).not.toThrow(Error);
      });
    });

    describe('#validateComponentId', () => {
      it('throws error if component does not exist', async () => {
        billingClientMock.getCatalog.mockResolvedValueOnce({ prod_fam: { components: { comp_handle: { id: 'some' } } } });
        await subWorker.loadCatalog();
        expect(() => subWorker.validateComponentId('prod_fam', 'none')).toThrow(Error);
      });

      it('does not throw error if component exists', async () => {
        billingClientMock.getCatalog.mockResolvedValueOnce({ prod_fam: { components: { comp_handle: { id: 'some' } } } });
        await subWorker.loadCatalog();
        expect(() => subWorker.validateComponentId('prod_fam', 'some')).not.toThrow(Error);
      });
    });

    describe('#validateComponentHandle', () => {
      it('throws error if component does not exist', async () => {
        billingClientMock.getCatalog.mockResolvedValueOnce({ prod_fam: { components: { comp_handle: { id: 'some' } } } });
        await subWorker.loadCatalog();
        expect(() => subWorker.validateComponentHandle('prod_fam', 'none')).toThrow(Error);
      });

      it('does not throw error if component exists', async () => {
        billingClientMock.getCatalog.mockResolvedValueOnce({ prod_fam: { components: { comp_handle: { id: 'some' } } } });
        await subWorker.loadCatalog();
        expect(() => subWorker.validateComponentHandle('prod_fam', 'comp_handle')).not.toThrow(Error);
      });
    });

    describe('#validateProductHandle', () => {
      it('throws error if product does not exist', async () => {
        billingClientMock.getCatalog.mockResolvedValueOnce({ prod_fam: { products: { prod_handle: {} } } });
        await subWorker.loadCatalog();
        expect(() => subWorker.validateProductHandle('prod_fam', 'none')).toThrow(Error);
      });

      it('does not throw error if family exists', async () => {
        billingClientMock.getCatalog.mockResolvedValueOnce({ prod_fam: { products: { prod_handle: {} } } });
        await subWorker.loadCatalog();
        expect(() => subWorker.validateProductHandle('prod_fam', 'prod_handle')).not.toThrow(Error);
      });
    });

    describe('#hasActivePaymentMethod', () => {
      it('returns false if paymentMethod is undefined', () => {
        const paymentMethod = undefined;
        expect(subWorker.hasActivePaymentMethod(paymentMethod)).toBeFalsy();
      });

      it('returns true if the paymentMethod is valid after update', () => {
        const paymentMethod = {
          update: {
            id: 52811996,
            state: 'trialing',
            trial_started_at: '2022-03-17T08:36:40-04:00',
            trial_ended_at: '2022-03-24T08:36:40-04:00',
            activated_at: null,
            created_at: '2022-03-17T08:36:40-04:00',
            updated_at: '2022-03-17T08:36:44-04:00',
            expires_at: null,
            balance_in_cents: 0,
            current_period_ends_at: '2022-03-24T08:36:40-04:00',
            next_assessment_at: '2022-03-24T08:36:40-04:00',
            canceled_at: null,
            cancellation_message: null,
            next_product_id: null,
            next_product_handle: null,
            cancel_at_end_of_period: false,
            payment_collection_method: 'automatic',
            snap_day: null,
            cancellation_method: null,
            current_period_started_at: '2022-03-17T08:36:40-04:00',
            previous_state: 'trialing',
            signup_payment_id: 631716071,
            signup_revenue: '0.00',
            delayed_cancel_at: null,
            coupon_code: null,
            total_revenue_in_cents: 0,
            product_price_in_cents: 0,
            product_version_number: 1,
            payment_type: null,
            referral_code: null,
            coupon_use_count: null,
            coupon_uses_allowed: null,
            reason_code: null,
            automatically_resume_at: null,
            coupon_codes: [],
            offer_id: null,
            payer_id: 53497215,
            receives_invoice_emails: null,
            product_price_point_id: 1256407,
            next_product_price_point_id: null,
            credit_balance_in_cents: 0,
            prepayment_balance_in_cents: 0,
            net_terms: null,
            stored_credential_transaction_id: null,
            locale: null,
            reference: '8247967249',
            currency: 'USD',
            on_hold_at: null,
            scheduled_cancellation_at: null,
            product_price_point_type: 'catalog',
            current_billing_amount_in_cents: 150000,
            customer: {
              id: 53497215,
              first_name: 'Ryan QX',
              last_name: 'Mee QX',
              organization: null,
              email: 'ryan+LT17D@qualityxpert.com',
              created_at: '2022-03-17T08:36:39-04:00',
              updated_at: '2022-03-17T08:36:41-04:00',
              reference: '8f9733af-0cc8-4796-b958-f0289edf03d0',
              address: null,
              address_2: null,
              city: null,
              state: null,
              state_name: null,
              zip: null,
              country: null,
              country_name: null,
              phone: '91991991919',
              portal_invite_last_sent_at: null,
              portal_invite_last_accepted_at: null,
              verified: false,
              portal_customer_created_at: '2022-03-17T08:36:41-04:00',
              vat_number: null,
              cc_emails: null,
              tax_exempt: false,
              parent_id: null,
              locale: null,
            },
            product: {
              id: 5371107,
              name: 'Standard',
              handle: 'ix_standard',
              description: '',
              accounting_code: '',
              request_credit_card: true,
              expiration_interval: null,
              expiration_interval_unit: 'never',
              created_at: '2020-11-02T14:44:43-05:00',
              updated_at: '2022-03-02T14:44:58-05:00',
              price_in_cents: 0,
              interval: 12,
              interval_unit: 'month',
              initial_charge_in_cents: null,
              trial_price_in_cents: 0,
              trial_interval: 7,
              trial_interval_unit: 'day',
              archived_at: null,
              require_credit_card: false,
              return_params: '',
              taxable: false,
              update_return_url: '',
              tax_code: '',
              initial_charge_after_trial: false,
              version_number: 1,
              update_return_params: '',
              default_product_price_point_id: 1094304,
              request_billing_address: false,
              require_billing_address: false,
              require_shipping_address: false,
              product_price_point_id: 1256407,
              product_price_point_name: '20210415_annually',
              product_price_point_handle: 'v20210415_annually',
              product_family: [Object],
              public_signup_pages: [],
            },
            group: null,
            components: [],
            hubspot_deal_id: '8247967249',
            billing_interval: 'Annually',
            ixc_subscription_id: '2c111fb1-fb40-49df-9780-57cb8c968402',
            credit_card: {
              id: 35967803,
              first_name: 'Ryan',
              last_name: 'Mee',
              masked_card_number: 'XXXX-XXXX-XXXX-1111',
              card_type: 'visa',
              expiration_month: 12,
              expiration_year: 2023,
              customer_id: 53497215,
              current_vault: 'bogus',
              vault_token: '1',
              billing_address: '1 Test St.',
              billing_city: 'City',
              billing_state: 'AL',
              billing_zip: '12345',
              billing_country: 'US',
              customer_vault_token: null,
              billing_address_2: '',
              site_gateway_setting_id: null,
              payment_type: 'credit_card',
              disabled: false,
              gateway_handle: null,
            },
          },
          previousPayment: null,
        };
        expect(subWorker.hasActivePaymentMethod(paymentMethod)).toBeTruthy();
      });
    });
  });
});
