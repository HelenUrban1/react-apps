const ChargifyBillingAdapter = require('./serviceAdapters/chargifyBillingAdapter');
// const StripeBillingAdapter = require('./serviceAdapters/stripeBillingAdapter');

/**
 * billingClient provides the application with a standard interface to any billing provider
 *
 * @param {Object} params
 * @prop  {String} useAdapter - The ID of the Billing adapter to load (default is 'chargify')
 * @prop  {String} apiKey - The provider's API key
 * @prop  {String} [publicKey] - The provider's public key
 * @prop  {String} [privateKey] - The provider's private key
 * @prop  {String} [siteName] - The provider's site name
 * @prop  {Object} [httpClient] - The HTTP client
 * @prop  {Object} [logger] - The logging utility
 * @return {billingClient}
 */
export default function makeBillingClient(params = {}) {
  // Extract adapter
  const { useAdapter = 'chargify', logger = console } = params;
  try {
    // Initialize the requested adapter
    let billingAdapter;
    if (useAdapter === 'chargify') {
      billingAdapter = new ChargifyBillingAdapter(params);
      // This is where other adapter would be registered
      // if (useAdapter === 'stripe') {
      //   billingAdapter = new StripeBillingAdapter(params);
    } else {
      throw new Error(`Unrecognized Billing adapter ${useAdapter}`);
    }
    return billingAdapter;
  } catch (error) {
    logger.error(`billingClient.makeBillingClient failed. ${error}`);
    throw error;
  }
}
