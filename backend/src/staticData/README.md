# IX Products

## Overview

The JSON file in this directory stores data representing IX products that are defined in the billing/subscription vendor's system (currently Chargify). For details about Chargify and the IXC integration, see [these docs](https://inspectionxpert.atlassian.net/wiki/spaces/IC/pages/152698893/Subscriptions+Billing).

Product data is manually replicated here to:

- Allow for subscription -> product mappings
- Serve as the source for product level feature flag configuration

The data file is manually maintained because product data is expected to rarely change.

### Changing product.json

#### Adding a Product

- Create the product in Chargify's web app
- Make a Chargify API request to fetch the new product's data: `curl -u <api_key>:x https://inspectionxpert.chargify.com/products/<product_id>.json`
  - See the team Keybase chat for the API key
  - The product id is shown on Chargify's site or URL when selecting a product to view its details
- Add a new object to products.json that is a child of the outermost `{}`
- Name the new entry after the new product
- Paste in the output of the Chargify API response
- Add a `features` array as a top level item in the new product data
  - Define feature flag configuration if desired
- Save and commit the file
