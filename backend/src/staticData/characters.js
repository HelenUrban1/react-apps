/*
This file is meant to provide a single source of truth for characters attributes.
It exposes constants for common symbols and a character registry which exposes
a method for retriving relevant information about the chracter.

In order for the unicode symbols to be displayed in an IDE or terminal, you must install
the InspectionXpert GDT font on your system and update your IDE or terminal settings to use it. 

Download  'InspectionXpert GDT' font:
  Windows 
    https://s3.amazonaws.com/ondemandtest.inspectionxpert.com/GDT_Font/InspectionXpert+GDT+Font.msi
  Mac
    https://ondemand.inspectionxpert.com/GDT_Font/InspectionXpertGDT.ttf

*/

// There appears to be two sizes of each of the icons. There could be more???
export const TYPE_LABEL_GDT = 'Geometric Tolerance';
export const GDT_SYMBOLS = ['', '', '', '', '', '', '', '', '', '', '', '', '', ''];
export const GDT_SYMBOLS_SML = ['⏤', '⏥', '○', '⌭', '⌒', '⌓', '⊥', '∠', '⑊', '⌖', '⌾', '⌯', '↗', '⌰'];

// TODO: Lookup the larger versions of M, P, CF, ST and the smaller versions of CR, SR, 
export const GDT_MODIFIERS = ['Ⓕ', 'Ⓘ', 'Ⓛ', 'Ⓜ', '', 'Ⓢ', 'Ⓣ', 'Ⓤ', '', '', '', ''];
export const GDT_MODIFIERS_SML = ['', '', '', '', 'Ⓟ', '', '', '', '', '', '', ''];

// Character reference
export const CAD_CHARACTERS = [
  // Alphanumerics
  { char: '0', framed_char: '' },
  { char: '1', framed_char: '' },
  { char: '2', framed_char: '' },
  { char: '3', framed_char: '' },
  { char: '4', framed_char: '' },
  { char: '5', framed_char: '' },
  { char: '6', framed_char: '' },
  { char: '7', framed_char: '' },
  { char: '8', framed_char: '' },
  { char: '9', framed_char: '' },
  { char: 'A', framed_char: '' },
  { char: 'B', framed_char: '' },
  { char: 'C', framed_char: '' },
  { char: 'D', framed_char: '' },
  { char: 'E', framed_char: '' },
  { char: 'F', framed_char: '' },
  { char: 'G', framed_char: '' },
  { char: 'H', framed_char: '' },
  { char: 'I', framed_char: '' },
  { char: 'J', framed_char: '' },
  { char: 'K', framed_char: '' },
  { char: 'L', framed_char: '' },
  { char: 'M', framed_char: '' },
  { char: 'N', framed_char: '' },
  { char: 'O', framed_char: '' },
  { char: 'P', framed_char: '' },
  { char: 'Q', framed_char: '' },
  { char: 'R', framed_char: '' },
  { char: 'S', framed_char: '' },
  { char: 'T', framed_char: '' },
  { char: 'U', framed_char: '' },
  { char: 'V', framed_char: '' },
  { char: 'W', framed_char: '' },
  { char: 'X', framed_char: '' },
  { char: 'Y', framed_char: '' },
  { char: 'Z', framed_char: '' },

  // GD&T Frames
  // NOTE: Potential framed alternatives to standard brackets (I couldn't visually distinguish between them)
  { char: '[', framed_char: '', name: 'Left Bracket', escape: true }, // '',''
  { char: '|', framed_char: '', name: 'Pipe', escape: true }, // ''
  { char: ']', framed_char: '', name: 'Right Bracket', escape: true }, //  '','',
  { char: ' ', framed_char: '', name: 'Space' },

  // GD&T Type Symbols
  {
    char: '',
    char_sml: '⏤',
    framed_char: '',
    name: 'Straightness',
    type: TYPE_LABEL_GDT,
    group: 'Form',
  },
  {
    char: '',
    char_sml: '⏥',
    framed_char: '',
    name: 'Flatness',
    type: TYPE_LABEL_GDT,
    group: 'Form',
  },
  {
    char: '',
    char_sml: '○',
    framed_char: '',
    name: 'Circularity',
    type: TYPE_LABEL_GDT,
    group: 'Form',
  },
  {
    char: '',
    char_sml: '⌭',
    framed_char: '',
    name: 'Cylindricity',
    type: TYPE_LABEL_GDT,
    group: 'Form',
  },
  {
    char: '',
    char_sml: '⌒',
    framed_char: '',
    name: 'Profile of a Line',
    type: TYPE_LABEL_GDT,
    group: 'Profile',
  },
  {
    char: '',
    char_sml: '⌓',
    framed_char: '',
    name: 'Profile of a Surface',
    type: TYPE_LABEL_GDT,
    group: 'Profile',
  },
  {
    char: '',
    char_sml: '⊥',
    framed_char: '',
    name: 'Perpendicularity',
    type: TYPE_LABEL_GDT,
    group: 'Orientation',
  },
  {
    char: '',
    char_sml: '∠',
    framed_char: '',
    name: 'Angularity',
    type: TYPE_LABEL_GDT,
    group: 'Orientation',
  },
  {
    char: '',
    char_sml: '⑊',
    framed_char: '',
    name: 'Parallelism',
    type: TYPE_LABEL_GDT,
    group: 'Orientation',
  },
  {
    char: '',
    char_sml: '⌖',
    framed_char: '',
    name: 'Position',
    type: TYPE_LABEL_GDT,
    group: 'Location',
  },
  {
    char: '',
    char_sml: '⌾',
    framed_char: '',
    name: 'Concentricity',
    type: TYPE_LABEL_GDT,
    group: 'Location',
  },
  {
    char: '',
    char_sml: '⌯',
    framed_char: '',
    name: 'Symmetry',
    type: TYPE_LABEL_GDT,
    group: 'Location',
  },
  {
    char: '',
    char_sml: '↗',
    framed_char: '',
    name: 'Runout',
    type: TYPE_LABEL_GDT,
    group: 'Runout',
  },
  {
    char: '',
    char_sml: '⌰',
    framed_char: '',
    name: 'Total Runout',
    type: TYPE_LABEL_GDT,
    group: 'Runout',
  },

  // GD&T Modifiers
  { char: 'Ⓔ', char_sml: '', framed_char: '', name: 'NOT SET', desc: 'Circle E' }, // TODO: Is this character valid?
  { char: 'Ⓕ', char_sml: '', framed_char: '', name: 'Free State', desc: 'Circle F' },
  { char: 'Ⓘ', char_sml: '', framed_char: '', name: 'Independency', desc: 'Circle I' },
  {
    char: 'Ⓛ',
    char_sml: '',
    framed_char: '',
    name: 'Least Material Condition (LMC/LMB)',
    desc: 'Circle L',
  },
  {
    char: 'Ⓜ',
    char_sml: '',
    framed_char: '',
    name: 'Maximum Material Condition (MMC/MMB)',
    desc: 'Circle M',
  },
  { char: 'Ⓟ', char_sml: '', framed_char: '', name: 'Projected Tolerance', desc: 'Circle P' },
  { char: 'Ⓡ', char_sml: '', framed_char: '', name: 'NOT SET', desc: 'Circle R' }, // TODO: Is this character valid?
  {
    char: 'Ⓢ',
    char_sml: '',
    framed_char: '',
    name: 'Regardless of Feature Size (RFS)',
    desc: 'Circle S',
  },
  { char: 'Ⓣ', char_sml: '', framed_char: '', name: 'Tangent Plane', desc: 'Circle T' },
  { char: 'Ⓤ', char_sml: '', framed_char: '', name: 'Unequally Disposed', desc: 'Circle U' },
  { char: '', char_sml: '', framed_char: '', name: 'Continuous Feature', desc: 'Hexframed CF' },
  {
    char: '',
    char_sml: '',
    framed_char: '',
    name: 'Statistical Tolerance',
    desc: 'Hexframed ST',
  }, // Character renders the same, but is a different unicode
  // TODO: Why does controlled radius have a left side to its frame?
  {
    char: '',
    char_sml: 'CR',
    framed_char: '',
    name: 'Controlled Radius',
    desc: 'Uni CR',
    notes_abrv: ['CR'],
  },
  {
    char: '',
    char_sml: 'SR',
    framed_char: '',
    name: 'Spherical Radius',
    desc: 'Uni SR',
    notes_abrv: ['SR'],
  },
  {
    char: '',
    char_sml: 'S',
    framed_char: '',
    name: 'Spherical Diameter',
    desc: 'Uni S diameter',
    notes_abrv: ['SPHER DIA'],
  },

  // Unicode CAD Symbols
  { char: 'μ', framed_char: '', name: 'Mu' },
  { char: '°', framed_char: '', name: 'Degrees', notes_abrv: 'DEG' },
  { char: '', char_sml: '⌀', framed_char: '', name: 'Diameter', notes_abrv: 'DIA' },
  // { char: 'Ø', name: 'Diameter Zero', desc: 'zero with slash', framed_char: '' },
  { char: '', framed_char: '', name: 'Square', desc: 'square', notes_abrv: ['SQ'] },
  { char: '', char_sml: '⌴', name: 'Counterbore', notes_abrv: ['CBORE'] },
  { char: '', char_sml: '⌴⌀', name: 'Counterbore Diameter', notes_abrv: ['CBOREDIA'] },
  { char: '', char_sml: '⌴↧', name: 'Counterbore Depth', notes_abrv: ['CBOREDP'] },
  { char: '', name: 'Spotface', desc: 'bucketed SF', notes_abrv: ['SF', 'SFACE'] },
  { char: '', char_sml: '⌵', name: 'Countersink', notes_abrv: ['CSK'] },
  { char: '', name: 'Slope' },
  { char: '', name: 'Conical Taper' },
  { char: '', framed_char: '', name: 'Translation' },
  { char: '', framed_char: '', name: 'Between' },
  { char: '', name: 'Distance Left of Position' },
  { char: '', name: 'Distance Right of Position' },
  { char: '', name: 'Distance from Position' },
  { char: '', name: 'Up Arrow' },
  { char: '', name: 'Down Arrow' },
  { char: '', name: 'Left Arrow' },
  { char: '', name: 'Right Arrow' },
  { char: '', char_sml: '↧', name: 'Depth', notes_abrv: ['DP'] },
  { char: '', name: 'Weld' },
  { char: '', name: 'Datum Extend Left' },
  { char: '', name: 'Datum Extend Right' },
  { char: '', name: 'Surface Finish' },
  { char: '', name: 'Surface Finish Removal Process' },
  { char: '', name: 'Surface Finish Removal Not Permitted' },
  { char: '', name: 'Centerline', desc: 'C over L' },
  { char: '', name: 'Plate/Property Line', desc: 'P over L' },

  // Standard characters
  // NOT IMPLEMENTED: !@#$&*={}:"<>?;'\
  // Regex characters that need to be escaped   .^$*+?()[]{\|
  // Regex characters that need to be escaped when in character set  ^-]\
  { char: '.', framed_char: '', name: 'Decimal', desc: 'decimal', escape: true },
  { char: ',', framed_char: '', name: 'Comma', desc: 'comma' },
  { char: '+', framed_char: '', name: 'Plus', desc: 'plus', escape: true },
  { char: '-', framed_char: '', name: 'Minus', desc: 'minus', escape: true },
  { char: '±', framed_char: '', name: 'Tolerance', desc: 'plus over minus' },
  { char: '_', framed_char: '', name: 'Underscore' },
  { char: '~', framed_char: '', name: 'Tilde' },
  { char: '/', framed_char: '', name: 'Forward Slash' },
  { char: '%', framed_char: '', name: 'Percent' },
  { char: '(', framed_char: '', name: 'Left Parentheses', escape: true },
  { char: ')', framed_char: '', name: 'Right Parentheses', escape: true },
  { char: '^', framed_char: '', name: 'Caret', escape: true },
  { char: '`', framed_char: '', name: 'Grave accent' },
];

// Build the lookup registry only if it hasn't already been built
let registry;
function buildReference() {
  if (!registry) {
    registry = {};
    CAD_CHARACTERS.forEach((c) => {
      // Provide lookup by char
      registry[c.char] = c;
      // Provide lookup by name, framed, and unframed character if they exist
      ['name', 'char_sml', 'framed_char'].forEach((att) => {
        if (Object.prototype.hasOwnProperty.call(c, att)) {
          registry[c[att]] = c;
        }
      });
    });
  }
}
buildReference();

// Lookup metadata for a provided character
export function lookup(value) {
  const val = registry[value];
  if (!val) {
    // log.warn(`No value found for ${value}`);
  }
  return val;
}

// Builds a bar delimited list of semanticly equivalent characters
// and returns it as a string for use in a RegExp
export function matchPatternFor(character, prefix = '', suffix = '') {
  const element = lookup(character);
  const parts = [];
  let symbol;
  if (element) {
    ['char', 'char_sml', 'framed_char'].forEach((att) => {
      if (Object.prototype.hasOwnProperty.call(element, att)) {
        symbol = element[att];
        // Escape reserved regex characters
        if (att === 'char' && element.escape) {
          symbol = `\\${symbol}`;
        }
        parts.push(symbol);
      }
    });
  } else {
    // Note: Don't need to escape here, because all character that need escapting should be in the registry
    parts.push(character);
  }
  return new RegExp(`${prefix}(${parts.join('|')})${suffix}`);
}

// TODO add this to new module when created
// Given a bar delimitted string of framed GD&T characters return the bar delimitted equavalent
// '' -> '|⌓|0.020|A|B|C|'
export function unFrameText(text) {
  const unFramedText = [];
  [...text].forEach((char) => {
    const charData = lookup(char);
    if (charData) {
      // If character is a leading GDT icon, add a leading bar b/c it has its own
      if (charData.type === TYPE_LABEL_GDT && charData.framed_char === char) {
        unFramedText.push('|');
        unFramedText.push(charData.char);
      } else {
        unFramedText.push(charData.char);
      }
    } else {
      unFramedText.push(char);
    }
  });
  return unFramedText.join('');
}

// TODO: Consolidate these in frontend/src/utils/textOperations.ts
// Given a bar delimitted string of unframed GD&T characters return the framed equavalent
// '|⌓|0.020|A|B|C|' -> ''
export function frameText(text) {
  const framedText = [];
  [...text].forEach((char) => {
    const charData = lookup(char);
    // If character is a leading GDT icon, remove leading bar b/c it has its own
    if (charData && charData.type === TYPE_LABEL_GDT) {
      framedText.pop();
    }
    if (!charData) {
      framedText.push(char);
    } else {
      framedText.push(charData.framed_char);
    }
  });
  return framedText.join('');
}

// GD&T Type / Subtype Relationships
// TODO: This could be moved to a module dedicated to structures
// prettier-ignore
export const TYPE_MAP = { 
  'Geometric Tolerance': [
    lookup('Straightness'), 
    lookup('Flatness'), 
    lookup('Circularity'), 
    lookup('Cylindricity'),
    lookup('Profile of a Line'), 
    lookup('Profile of a Surface'),
    lookup('Position'), 
    lookup('Concentricity'), 
    lookup('Symmetry'),
    lookup('Parallelism'), 
    lookup('Perpendicularity'), 
    lookup('Angularity'),
    lookup('Runout'), 
    lookup('Total Runout')
  ],
  'Dimension' :[
    'Angle',
    'Length',
    'Diameter',
    'Radius',
    'Depth',
    'Thread',
    'Counterbore Depth', // TODO: Why is this different than Depth
    'Counterbore Diameter', // TODO: Why is this different than Diameter
    'Countersink Angle'
  ],
  'Note':[
    'Note'
  ]
}
