const rTracer = require('cls-rtracer');
const morgan = require('morgan');
const { log } = require('./index');

const REQUEST_ID_HEADER = 'x-request-id';

/*
const CORRELATION_ID_HEADER = 'x-correlation-id';
const correlator = require('express-correlation-id');
// Use express-correlation-id to add x-correlation-id header to track request across services
app.use(correlator());
function middleware(req, res, next) {
  var correlationId = req.get('X-Correlation-Id') || uuid.v4();
  res.set('X-Correlation-Id', correlationId);
  next();
}
*/

// Exclude heartbeat and healthchecker from request logging
// "user_agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/61.0.3163.100 Chrome/61.0.3163.100 Safari/537.36 PingdomPageSpeed/1.0 (pingbot/2.0; +http://www.pingdom.com/)"
// "user_agent": "ELB-HealthChecker/2.0"
const RE_USERAGENT_FILTER = new RegExp('(pingbot|elb-healthchecker)', 'i');

module.exports = function appendLogger(app) {
  // NOTE: This middleware is used to collect CORs data during requests to help debug CORS failures
  // app.use((req, res, next) => {
  //   log.debug({
  //     purpose: 'CORS-LOGGING',
  //     path: `${req.protocol}://${req.hostname}/${req.originalUrl}`,
  //     origin: req.header('origin'),
  //     contentType: req.header('Content-Type'), // "application/json"
  //     userAgent: req.header('user-agent'), // "Mozilla/5.0 (Macintosh Intel Mac OS X 10_8_5) AppleWebKi..."
  //   });
  //   next();
  // });

  // Ensure the Request ID is appended to all requests
  // Add a Request-Id to all log events https://github.com/puzpuzpuz/cls-rtracer
  app.use(
    rTracer.expressMiddleware({
      useHeader: true,
      headerName: REQUEST_ID_HEADER,
    })
  );

  // Ensure the Request ID is appended to all requests
  app.use((req, res, next) => {
    req.id = rTracer.id();
    req.headers[REQUEST_ID_HEADER] = req.id;
    log.trace(`Setting response header ${REQUEST_ID_HEADER}=${req.id}`);
    res.header(REQUEST_ID_HEADER, [req.id]);
    next();
  });

  // Use Morgan for access logging and include the request-id
  morgan.token('request-id', (req, res) => {
    return req.headers[REQUEST_ID_HEADER];
  });
  morgan.token('host', function (req, res) {
    return req.hostname;
  });

  app.use(
    morgan(
      '{"request-id": ":request-id", "host": ":host", "remote_addr": ":remote-addr", "remote_user": ":remote-user", "date": ":date[clf]", "method": ":method", "url": ":url", "http_version": ":http-version", "status": ":status", "result_length": ":res[content-length]", "referrer": ":referrer", "user_agent": ":user-agent", "response_time": ":response-time"}',
      {
        stream: log.stream,
        skip(req, res) {
          // return res.statusCode < 400;
          return RE_USERAGENT_FILTER.test(req.header('user-agent'));
        },
      }
    )
  );
};
