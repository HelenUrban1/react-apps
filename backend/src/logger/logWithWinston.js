const winston = require(`winston`);
const rTracer = require(`cls-rtracer`);
const { info } = require('winston');

const LEVELS = {
  error: 0,
  warn: 1,
  info: 2,
  debug: 3,
  trace: 4,
};

winston.addColors({
  error: 'bold red',
  warn: 'yellow',
  info: 'white',
  debug: 'cyan',
  trace: 'gray',
});

const TRANSPORTS = [];

const formatMeta = (meta) => {
  // You can format the splat yourself
  const splat = meta[Symbol.for('splat')];
  if (splat && splat.length) {
    return splat.length === 1 ? JSON.stringify(splat[0], null, 1) : JSON.stringify(splat, null, 1);
  }
  return '';
};

const prettyJsonFormat = winston.format.printf((info) => {
  // Get the request tracing Id
  const rid = rTracer.id();
  // Winston 3+ removed the ability to stringify objects, so extra additional data into a metadata object to stringify
  const { timestamp, level, message, label = '', ...metadata } = info;
  let metadataString;
  try {
    metadataString = metadata ? formatMeta(metadata) : '';
  } catch (error) {
    // BUG: JS objects with circular references throw and err
    metadataString = `ERROR FORMATTING LOG METADATA : ${error.name} - ${error.message}`;
  }
  return rid ? `[${label}] ${timestamp} [request-id:${rid}] : ${level} : ${message} ${metadataString}` : `[${label}] ${timestamp} : ${level} : ${message} ${metadataString}`;
});

function createTransport(transportConfig) {
  // Synchronize log levels across all transports
  const useTransportConfig = { ...transportConfig, levels: LEVELS };
  let transport;
  if (transportConfig.type === 'console') {
    transport = new winston.transports.Console(useTransportConfig);
  } else if (transportConfig.type === 'file') {
    transport = new winston.transports.File(useTransportConfig);
  } else if (transportConfig.type === 'http') {
    transport = new winston.transports.Http(useTransportConfig);
    /*
  // Add other transports here
  // https://github.com/winstonjs/winston/blob/master/docs/transports.md
  } else if (transportConfig.type == '...') {
    transport = new winston.transports.Stream({stream: fs.createWriteStream('/dev/null') // other options }
  */
  } else {
    throw new Error(`An unrecognized Winston transport was encountered during logger initialization ${useTransportConfig.type}`);
  }
  return transport;
}

function createConfig(label, options) {
  let useColors = false;
  options.transports.forEach((transportConfig) => {
    if (transportConfig.colorize) {
      useColors = true;
    }
  });

  let format;
  if (useColors) {
    format = winston.format.combine(
      winston.format.label({ label }),
      winston.format.colorize({
        all: true,
      }),
      winston.format.timestamp(), // .timestamp({format: 'YY-MM-DD HH:MM:SS'})
      prettyJsonFormat
    );
  } else {
    format = winston.format.combine(
      winston.format.label({ label }),
      winston.format.timestamp(), // .timestamp({format: 'YY-MM-DD HH:MM:SS'})
      prettyJsonFormat
    );
  }

  return {
    ...options,
    levels: LEVELS,
    transports: TRANSPORTS,
    format,
  };
}

function createLogger(label, options) {
  const winstonLoggerConfig = createConfig(label, options);
  const loggerInstance = winston.loggers.add(label, winstonLoggerConfig);
  // Create a stream to accept access events from Morgan
  loggerInstance.stream = {
    write: (message) => {
      loggerInstance.info(message.trim());
    },
  };
  loggerInstance.trace(`logger.logWithWinston.createLogger ${label} finished`);
  return loggerInstance;
}

function initLogging(options) {
  if (options.transports) {
    options.transports.forEach((transportConfig) => {
      TRANSPORTS.push(createTransport(transportConfig));
    });
  }
}

module.exports = { initLogging, createLogger, createConfig };
