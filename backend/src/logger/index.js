const config = require('../../config')();
const { initLogging, createLogger } = require('./logWithWinston');
const { loggers } = require('winston');

initLogging(config.logging);

createLogger('SERVER', config.logging);
createLogger('CLIENT', config.logging);

// Export the logger
const log = loggers.get('SERVER');
const clientLogger = loggers.get('CLIENT');

/*
log.trace('trace message');
log.debug('debug message');
log.info('info message');
log.warn('warn message');
log.error('error message');

clientLogger.trace('trace client message');
clientLogger.debug('debug client message');
clientLogger.info('info client message');
clientLogger.warn('warn client message');
clientLogger.error('error client message');
*/

module.exports = { log, clientLogger };
