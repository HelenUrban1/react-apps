const config = require('../../config')();
const USE_MOCKS = config.useMocks;

if (USE_MOCKS) {
  jest.doMock('../database/models');
  jest.doMock('../database/repositories/reviewTaskRepository');
  jest.doMock('../database/repositories/characteristicRepository');
  jest.doMock('../database/repositories/partRepository');
  jest.doMock('../database/repositories/listItemRepository');
  jest.doMock('../database/repositories/notificationRepository');
  jest.doMock('../database/repositories/drawingSheetRepository');
  jest.doMock('../external/parseNotation');
  const interpretNotationText = (require('../external/parseNotation').default = () => {
    return {
      parsed: {
        notation: { type: 'nominalWithTolerances', nominal: { text: '1.45', value: '1.45', units: '' }, plus: { text: '0.2', value: '0.2', units: '' }, minus: { text: '0.2', value: '0.2', units: '' }, text: '1.45±0.2' },
        quantity: { value: '1', units: '', text: '' },
        words: '',
        text: '1.45±0.2',
        input: '1.45 +/- 0.2',
      },
      ocrRes: '1.45 +/- 0.2',
    };
  });
}

const models = require('../database/models');
const ReviewTaskRepository = require('../database/repositories/reviewTaskRepository');
const CharacteristicRepository = require('../database/repositories/characteristicRepository');
const PartRepository = require('../database/repositories/partRepository');
const ListItemRepository = require('../database/repositories/listItemRepository');
const DrawingSheetRepository = require('../database/repositories/drawingSheetRepository');

const SequelizeRepository = require('../database/repositories/sequelizeRepository');
const CharacteristicService = require('./characteristicService');
const PartService = require('./partService');
const ReviewTaskService = require('./reviewTaskService');
const { data: characteristicData } = require('../__fixtures__').characteristic;
const { data: partData } = require('../__fixtures__').part;
const { data: drawingSheetData } = require('../__fixtures__').drawingSheet;

describe('ReviewTaskService', () => {
  const context = { currentUser: { accountId: config.testAccountId } };
  const data = {
    status: 'Waiting',
    type: 'Capture',
    metadata: JSON.stringify({
      ocrText: '1.25 +/- 0.1',
      correctedText: '',
      characteristicId: 'e0a01769-c8ed-4090-bd24-9c1b2241f123',
    }),
    accountId: config.testAccountId,
    siteId: '31ef1d56-1b86-4ffa-b886-507ed8f021c5',
    partId: 'd6c5bc49-9fb2-4fbb-8a61-77bc8d18036f',
    drawingId: 'c200ce70-72b8-44f7-8d3f-4b93f6d1b491',
    reviewerId: null,
    processingStart: null,
    processingEnd: null,
  };

  const characteristicMock = {
    createMock: jest.fn().mockReturnValue(characteristicData[0]),
    createInAccountMock: jest.fn().mockReturnValue(characteristicData[0]),
    updateMock: jest.fn().mockReturnValue(characteristicData[0]),
    findAndCountAllMock: jest.fn().mockReturnValue({
      rows: [characteristicData[0]],
      count: 1,
    }),
    findByIdMock: jest.fn().mockReturnValue(characteristicData[0]),
    destroyMock: jest.fn(),
    updateFromReviewMock: jest.fn().mockReturnValue(characteristicData[0]),
    findByIdInAccountMock: jest.fn().mockReturnValue({
      ...characteristicData[0],
      part: {
        dataValues: {
          id: 'd6c5bc49-9fb2-4fbb-8a61-77bc8d18036f',
        },
      },
    }),
    findAndCountAllForReviewMock: jest.fn().mockReturnValue({
      rows: [characteristicData[0]],
      count: 1,
    }),
    destroyFromReviewMock: jest.fn().mockReturnValue(characteristicData[0]),
  };

  const partMock = {
    createMock: jest.fn().mockReturnValue(partData[0]),
    createInAccountMock: jest.fn().mockReturnValue(partData[0]),
    updateMock: jest.fn().mockReturnValue(partData[0]),
    findAndCountAllMock: jest.fn().mockReturnValue({
      rows: [partData[0]],
      count: 1,
    }),
    findByIdMock: jest.fn().mockReturnValue(partData[0]),
    destroyMock: jest.fn(),
    findByIdInAccountMock: jest.fn().mockReturnValue(partData[0]),
  };

  const reviewTaskMock = {
    createMock: jest.fn().mockReturnValue(data),
    createInAccountMock: jest.fn().mockReturnValue(data),
    updateMock: jest.fn().mockReturnValue(data),
    findAndCountAllMock: jest.fn().mockReturnValue({
      rows: [data],
      count: 1,
    }),
    findByIdMock: jest.fn().mockReturnValue(data),
    destroyMock: jest.fn(),
    findNextQueuedTask: jest.fn().mockReturnValue(data),
    returnTaskToQueue: jest.fn(),
    count: jest.fn().mockReturnValue(0),
    elapsedTime: jest.fn().mockReturnValue(10),
  };

  const drawingSheetMock = {
    findOne: jest.fn().mockReturnValue(drawingSheetData[0]),
    incrementInAccountMock: jest.fn().mockReturnValue({
      ...data[0], //
      foundCaptureCount: 1,
      acceptedCaptureCount: 1,
      rejectedCaptureCount: 0,
      acceptedTimeoutCaptureCount: 0,
      reviewedCaptureCount: 1,
      reviewedTimeoutCaptureCount: 0,
      autoballoonProcessingComplete: true,
      autoballoonReviewComplete: true,
    }),
  };

  beforeEach(async () => {
    if (USE_MOCKS) {
      models.getTenant = () =>
        Promise.resolve({
          sequelize: {
            transaction: jest.fn().mockReturnValue({
              rollback: jest.fn(),
              commit: jest.fn(),
            }),
          },
        });

      models.sequelizeAdmin = {
        query: jest.fn().mockReturnValue(Promise.resolve({})),
      };

      PartRepository.mockImplementation(() => {
        return {
          create: partMock.createMock,
          createInAccount: partMock.createInAccountMock,
          update: partMock.updateMock,
          findAndCountAll: partMock.findAndCountAllMock,
          findById: partMock.findByIdMock,
          destroy: partMock.destroyMock,
          findByIdInAccount: partMock.findByIdInAccountMock,
        };
      });

      ListItemRepository.findByType = jest.fn().mockReturnValue([]);

      CharacteristicRepository.mockImplementation(() => {
        return {
          create: characteristicMock.createMock,
          createInAccount: characteristicMock.createInAccountMock,
          update: characteristicMock.updateMock,
          findAndCountAll: characteristicMock.findAndCountAllMock,
          findById: characteristicMock.findByIdMock,
          destroy: characteristicMock.destroyMock,
          updateFromReview: characteristicMock.updateFromReviewMock,
          findByIdInAccount: characteristicMock.findByIdInAccountMock,
          findAndCountAllForReview: characteristicMock.findAndCountAllForReviewMock,
          destroyFromReview: characteristicMock.destroyFromReviewMock,
        };
      });

      DrawingSheetRepository.mockImplementation(() => {
        return {
          findOne: drawingSheetMock.findOneMock,
          incrementInAccount: drawingSheetMock.incrementInAccountMock,
        };
      });

      ReviewTaskRepository.create = reviewTaskMock.createMock;
      ReviewTaskRepository.createInAccount = reviewTaskMock.createInAccountMock;
      ReviewTaskRepository.update = reviewTaskMock.updateMock;
      ReviewTaskRepository.findAndCountAll = reviewTaskMock.findAndCountAllMock;
      ReviewTaskRepository.findById = reviewTaskMock.findByIdMock;
      ReviewTaskRepository.destroy = reviewTaskMock.destroyMock;
      ReviewTaskRepository.findNextQueuedTask = reviewTaskMock.findNextQueuedTask;
      ReviewTaskRepository.returnTaskToQueue = reviewTaskMock.returnTaskToQueue;
      ReviewTaskRepository.count = reviewTaskMock.count;
      ReviewTaskRepository.elapsedTime = reviewTaskMock.elapsedTime;

      SequelizeRepository.commitTransaction = jest.fn();
    } else {
      await Promise.all([
        SequelizeRepository.cleanDatabase(config.reviewAccountId), //
        SequelizeRepository.cleanDatabase(config.testAccountId),
      ]);
    }
  });

  afterAll(async () => {
    if (!USE_MOCKS) {
      await Promise.all([
        SequelizeRepository.closeConnections(config.testAccountId), //
        SequelizeRepository.closeConnections(config.reviewAccountId),
        SequelizeRepository.closeConnections(),
      ]);
    }
  });

  describe('create()', () => {
    it('Should return reviewTask data', async () => {
      try {
        const reviewTask = await new ReviewTaskService(context).create(data);
        expect(reviewTask).toBeTruthy();
      } catch (exception) {
        // eslint-disable-next-line no-console
        console.error(exception);
        expect(exception).toBeFalsy();
      }
    });
  });

  describe('update()', () => {
    it('Should update reviewTask data', async () => {
      const reviewTask = await new ReviewTaskService(context).create(data);
      reviewTask.status = 'Processing';
      const updatedReviewTask = await new ReviewTaskService(context).update(reviewTask.id, reviewTask);
      expect(updatedReviewTask.status).toBe('Processing');
    });
  });

  describe('destroyAll()', () => {
    it('Should delete a reviewItem by id', async () => {
      if (USE_MOCKS) {
        reviewTaskMock.findAndCountAllMock = jest
          .fn()
          .mockReturnValueOnce({ rows: [{}], count: 1 })
          .mockReturnValueOnce({ rows: [], count: 0 });
        ReviewTaskRepository.findAndCountAll = reviewTaskMock.findAndCountAllMock;
      }
      const reviewTask = await new ReviewTaskService(context).create(data);
      const all = await new ReviewTaskService(context).findAndCountAll();
      expect(all.count).toBe(1);
      expect(all.rows).toBeTruthy();

      const context2 = { currentUser: { accountId: config.testAccountId } };
      await new ReviewTaskService(context2).destroyAll([reviewTask.id]);
      const none = await new ReviewTaskService(context2).findAndCountAll();
      expect(none.count).toBe(0);
      expect(none.rows).toStrictEqual([]);
    });
  });

  describe('findById()', () => {
    it('Should find reviewTask data by id', async () => {
      const reviewTask = await new ReviewTaskService(context).create(data);
      const foundReviewTask = await new ReviewTaskService(context).findById(reviewTask.id);
      expect(foundReviewTask.id).toBe(reviewTask.id);
    });
  });

  describe('addToQueue()', () => {
    it('Should add an item to the database', async () => {
      const reviewTaskService = new ReviewTaskService(context);
      await reviewTaskService.addToQueue([data]);
      const reviewTask = await reviewTaskService.pullFromQueue();
      expect(reviewTask).toBeTruthy();
    });
  });

  describe('returnToQueue()', () => {
    it('Should return reviewTask to the Waiting status', async () => {
      if (USE_MOCKS) {
        reviewTaskMock.findByIdMock = jest.fn().mockReturnValue({ ...data, status: 'Waiting' });
        ReviewTaskRepository.findById = reviewTaskMock.findByIdMock;
      }

      const reviewTaskService = new ReviewTaskService(context);
      await reviewTaskService.addToQueue([data]);
      const reviewTask = await reviewTaskService.pullFromQueue();
      await reviewTaskService.returnToQueue(reviewTask.id);
      const checkReviewTask = await reviewTaskService.findById(reviewTask.id);
      expect(checkReviewTask.status).toBe('Waiting');
    });
  });

  describe('acceptCapture()', () => {
    it('Should set a reviewTask to accepted status', async () => {
      if (USE_MOCKS) {
        reviewTaskMock.findByIdMock = jest.fn().mockReturnValue({ ...data, status: 'Accepted' });
        ReviewTaskRepository.findById = reviewTaskMock.findByIdMock;
        characteristicMock.findByIdMock = jest.fn().mockReturnValue({
          ...characteristicData[0],
          fullSpecification: '1.45 +/- 0.2',
          reviewTaskId: 'dbc9f506-fa24-48e2-a7b1-88b8e7ab4dbd',
        });
        CharacteristicRepository.mockImplementation(() => {
          return {
            create: characteristicMock.createMock,
            findById: characteristicMock.findByIdMock,
            updateFromReview: characteristicMock.updateFromReviewMock,
          };
        });
      }

      await new CharacteristicService(context).create(characteristicData[0]);
      const reviewTaskService = new ReviewTaskService(context);
      await reviewTaskService.addToQueue([data]);
      const reviewTask = await reviewTaskService.pullFromQueue();
      await reviewTaskService.acceptCapture(reviewTask.id, {
        ...reviewTask,
        metadata: JSON.stringify({
          ocrText: '1.25 +/- 0.1',
          correctedText: '',
          characteristicId: 'e0a01769-c8ed-4090-bd24-9c1b2241f123',
        }),
      });

      const checkCharacteristic = await new CharacteristicService(context).findById(characteristicData[0].id);
      expect(checkCharacteristic.reviewTaskId).toBeTruthy();

      const checkReviewTask = await reviewTaskService.findById(reviewTask.id);
      expect(checkReviewTask.status).toBe('Accepted');
    });
  });

  describe('correctCapture()', () => {
    it('Should set a reviewTask to corrected status when part has tolerances', async () => {
      if (USE_MOCKS) {
        reviewTaskMock.findByIdMock = jest.fn().mockReturnValue({ ...data, status: 'Corrected' });
        ReviewTaskRepository.findById = reviewTaskMock.findByIdMock;

        characteristicMock.findByIdMock = jest.fn().mockReturnValue({
          ...characteristicData[0],
          fullSpecification: '1.45 +/- 0.2',
          reviewTaskId: 'dbc9f506-fa24-48e2-a7b1-88b8e7ab4dbd',
        });
        CharacteristicRepository.mockImplementation(() => {
          return {
            create: characteristicMock.createMock,
            findById: characteristicMock.findByIdMock,
            updateFromReview: characteristicMock.updateFromReviewMock,
            findAndCountAllForReview: characteristicMock.findAndCountAllForReviewMock,
            findByIdInAccount: characteristicMock.findByIdInAccountMock,
          };
        });
      }

      await new PartService(context).create(partData[0]);
      const characteristic = { ...characteristicData[0], part: partData[0].id };
      await new CharacteristicService(context).create(characteristic);
      const reviewTaskService = new ReviewTaskService(context);
      await reviewTaskService.addToQueue([data]);
      const reviewTask = await reviewTaskService.pullFromQueue();
      await reviewTaskService.correctCapture(reviewTask.id, {
        ...reviewTask,
        metadata: JSON.stringify({
          ocrText: '1.25 +/- 0.1',
          correctedText: '1.45 +/- 0.2',
          characteristicId: 'e0a01769-c8ed-4090-bd24-9c1b2241f123',
        }),
      });
      const checkCharacteristic = await new CharacteristicService(context).findById(characteristicData[0].id);
      expect(checkCharacteristic.reviewTaskId).toBeTruthy();
      expect(checkCharacteristic.fullSpecification).toBe('1.45 +/- 0.2');

      const checkReviewTask = await reviewTaskService.findById(reviewTask.id);
      expect(checkReviewTask.status).toBe('Corrected');
    });

    it('Should set a reviewTask to corrected status when part has no tolerances', async () => {
      if (USE_MOCKS) {
        reviewTaskMock.findByIdMock = jest.fn().mockReturnValue({ ...data, status: 'Corrected' });
        ReviewTaskRepository.findById = reviewTaskMock.findByIdMock;

        characteristicMock.findByIdMock = jest.fn().mockReturnValue({
          ...characteristicData[0],
          fullSpecification: '1.45',
          upperSpecLimit: '1.46',
          lowerSpecLimit: '1.44',
          reviewTaskId: 'dbc9f506-fa24-48e2-a7b1-88b8e7ab4dbd',
        });
        CharacteristicRepository.mockImplementation(() => {
          return {
            create: characteristicMock.createMock,
            findById: characteristicMock.findByIdMock,
            updateFromReview: characteristicMock.updateFromReviewMock,
            findAndCountAllForReview: characteristicMock.findAndCountAllForReviewMock,
            findByIdInAccount: characteristicMock.findByIdInAccountMock,
          };
        });
      }

      await new PartService(context).create({ ...partData[0], defaultLinearTolerances: null, defaultAngularTolerances: null });
      const characteristic = { ...characteristicData[0], part: partData[0].id };
      await new CharacteristicService(context).create(characteristic);
      const reviewTaskService = new ReviewTaskService(context);
      await reviewTaskService.addToQueue([data]);
      const reviewTask = await reviewTaskService.pullFromQueue();
      await reviewTaskService.correctCapture(reviewTask.id, {
        ...reviewTask,
        metadata: JSON.stringify({
          ocrText: '1.25',
          correctedText: '1.45',
          characteristicId: 'e0a01769-c8ed-4090-bd24-9c1b2241f123',
        }),
      });
      const checkCharacteristic = await new CharacteristicService(context).findById(characteristicData[0].id);
      expect(checkCharacteristic.reviewTaskId).toBeTruthy();
      expect(checkCharacteristic.fullSpecification).toBe('1.45');
      expect(checkCharacteristic.upperSpecLimit).toBe('1.46');
      expect(checkCharacteristic.lowerSpecLimit).toBe('1.44');

      const checkReviewTask = await reviewTaskService.findById(reviewTask.id);
      expect(checkReviewTask.status).toBe('Corrected');
    });
  });

  describe('rejectCapture()', () => {
    it('Should set a reviewTask to rejected status', async () => {
      if (USE_MOCKS) {
        reviewTaskMock.findByIdMock = jest.fn().mockReturnValue({ ...data, status: 'Rejected' });
        ReviewTaskRepository.findById = reviewTaskMock.findByIdMock;
        characteristicMock.findByIdMock = jest.fn().mockReturnValue(null);
        CharacteristicRepository.mockImplementation(() => {
          return {
            create: characteristicMock.createMock,
            findById: characteristicMock.findByIdMock,
            updateFromReview: characteristicMock.updateFromReviewMock,
            findAndCountAllForReview: characteristicMock.findAndCountAllForReviewMock,
            destroyFromReview: characteristicMock.destroyFromReviewMock,
          };
        });
      }

      await new PartService(context).create(partData[0]);
      const characteristic = { ...characteristicData[0], part: partData[0].id };
      await new CharacteristicService(context).create(characteristic);
      const reviewTaskService = new ReviewTaskService(context);
      await reviewTaskService.addToQueue([data]);
      const reviewTask = await reviewTaskService.pullFromQueue();
      await reviewTaskService.rejectCapture(reviewTask.id, {
        ...reviewTask,
        metadata: JSON.stringify({
          ocrText: '1.25 +/- 0.1',
          correctedText: '',
          characteristicId: 'e0a01769-c8ed-4090-bd24-9c1b2241f123',
        }),
      });
      const checkCharacteristic = await new CharacteristicService(context).findById(characteristicData[0].id);
      expect(checkCharacteristic).toBe(null);

      const checkReviewTask = await reviewTaskService.findById(reviewTask.id);
      expect(checkReviewTask.status).toBe('Rejected');
    });
  });

  describe('partQueuedCaptureCount()', () => {
    it('Should return a count of tasks queued for the part', async () => {
      if (USE_MOCKS) {
        reviewTaskMock.countMock = jest.fn().mockReturnValue(1);
        ReviewTaskRepository.count = reviewTaskMock.countMock;
      }

      await new PartService(context).create(partData[0]);
      const characteristic = { ...characteristicData[0], part: partData[0].id };
      await new CharacteristicService(context).create(characteristic);
      const reviewTaskService = new ReviewTaskService(context);
      await reviewTaskService.addToQueue([data]);
      const count = await reviewTaskService.partQueuedCaptureCount(partData[0].id);
      expect(count.total).toBe(1);
      expect(count.waiting).toBe(1);
    });
  });

  describe('waitingTasksCount()', () => {
    it('Should return a count of tasks waiting to be processed', async () => {
      if (USE_MOCKS) {
        reviewTaskMock.countMock = jest.fn().mockReturnValue(1);
        ReviewTaskRepository.count = reviewTaskMock.countMock;
      }

      await new PartService(context).create(partData[0]);
      const characteristic = { ...characteristicData[0], part: partData[0].id };
      await new CharacteristicService(context).create(characteristic);
      const reviewTaskService = new ReviewTaskService(context);
      await reviewTaskService.addToQueue([data]);
      const count = await reviewTaskService.waitingTasksCount(partData[0].id);
      expect(count).toBe(1);
    });
  });

  describe('processedTasksCount()', () => {
    it('Should return a count of tasks that have been processed', async () => {
      await new PartService(context).create(partData[0]);
      const characteristic = { ...characteristicData[0], part: partData[0].id };
      await new CharacteristicService(context).create(characteristic);
      const reviewTaskService = new ReviewTaskService(context);
      await reviewTaskService.addToQueue([data]);
      const count = await reviewTaskService.processedTasksCount(partData[0].id);
      expect(count).toBe(0);
    });
  });

  describe('totalTasksCount()', () => {
    it('Should return a count of total tasks', async () => {
      await new PartService(context).create(partData[0]);
      const characteristic = { ...characteristicData[0], part: partData[0].id };
      await new CharacteristicService(context).create(characteristic);
      const reviewTaskService = new ReviewTaskService(context);
      await reviewTaskService.addToQueue([data]);
      const count = await reviewTaskService.processedTasksCount(partData[0].id);
      expect(count).toBe(0);
    });
  });

  describe('averageTimeInQueue()', () => {
    it('Should return an average for the amount of time tasks spend in the queue', async () => {
      await new CharacteristicService(context).create(characteristicData[0]);
      const reviewTaskService = new ReviewTaskService(context);
      await reviewTaskService.addToQueue([data]);
      const reviewTask = await reviewTaskService.pullFromQueue();
      await reviewTaskService.acceptCapture(reviewTask.id, {
        ...reviewTask,
        metadata: JSON.stringify({
          ocrText: '1.25 +/- 0.1',
          correctedText: '',
          characteristicId: 'e0a01769-c8ed-4090-bd24-9c1b2241f123',
        }),
      });
      const time = await reviewTaskService.averageTimeInQueue();
      expect(time).toBeTruthy();
    });
  });

  describe('averageTimeToProcess()', () => {
    it('Should return an average for the amount of time tasks take to process', async () => {
      await new CharacteristicService(context).create(characteristicData[0]);
      const reviewTaskService = new ReviewTaskService(context);
      await reviewTaskService.addToQueue([data]);
      const reviewTask = await reviewTaskService.pullFromQueue();
      await reviewTaskService.acceptCapture(reviewTask.id, {
        ...reviewTask,
        metadata: JSON.stringify({
          ocrText: '1.25 +/- 0.1',
          correctedText: '',
          characteristicId: 'e0a01769-c8ed-4090-bd24-9c1b2241f123',
        }),
      });
      const time = await reviewTaskService.averageTimeToProcess();
      expect(time).toBeTruthy();
    });
  });

  describe('agingTasksCount()', () => {
    it('Should return a count of tasks waiting to be processed over specified minutes old', async () => {
      await new PartService(context).create(partData[0]);
      const characteristic = { ...characteristicData[0], part: partData[0].id };
      await new CharacteristicService(context).create(characteristic);
      const reviewTaskService = new ReviewTaskService(context);
      await reviewTaskService.addToQueue([data]);

      const count = await ReviewTaskService.agingTasksCount(10);
      expect(count).toBe(0);
    });
  });
});
