const config = require('../../config')();
const USE_MOCKS = config.useMocks;

if (USE_MOCKS) {
  jest.doMock('../database/models');
  jest.doMock('../database/repositories/organizationRepository');
}

const models = require('../database/models');
const OrganizationRepository = require('../database/repositories/organizationRepository');
const SequelizeRepository = require('../database/repositories/sequelizeRepository');
const OrganizationService = require('./organizationService');

describe('OrganizationService', () => {
  const data = {
    status: 'Active',
    type: 'Customer',
    settings: '',
    name: 'Boeing',
    phone: '(919) 555-5555',
  };
  const createMock = jest.fn().mockReturnValue(data);

  beforeEach(async () => {
    if (USE_MOCKS) {
      models.getTenant = () =>
        Promise.resolve({
          sequelize: {
            transaction: jest.fn().mockReturnValue({
              rollback: jest.fn(),
              commit: jest.fn(),
            }),
          },
        });

      models.sequelizeAdmin = {
        query: jest.fn().mockReturnValue(Promise.resolve({})),
      };

      OrganizationRepository.mockImplementation(() => {
        return {
          create: createMock,
        };
      });

      SequelizeRepository.commitTransaction = jest.fn();
    } else {
      await Promise.all([
        SequelizeRepository.cleanDatabase(config.testAccountId), //
      ]);
    }
  });

  describe('CreateOrganization', () => {
    let organization;

    const context = { currentUser: { accountId: config.testAccountId } };

    it('Should return organization data', async () => {
      try {
        organization = await new OrganizationService(context).create(data);
        if (USE_MOCKS) {
          expect(createMock).toBeCalledTimes(1);
        }
        expect(organization).toBeTruthy();
      } catch (exception) {
        console.error(exception);
        expect(exception).toBeFalsy();
      }
    });
  });
  afterAll(async () => {
    if (!USE_MOCKS) {
      await SequelizeRepository.closeConnections(config.testAccountId);
    }
  });
});
