const { Op } = require('sequelize');
const ReviewTaskRepository = require('../database/repositories/reviewTaskRepository');
const CharacteristicRepository = require('../database/repositories/characteristicRepository');
const ListItemRepository = require('../database/repositories/listItemRepository');
const SequelizeRepository = require('../database/repositories/sequelizeRepository');
const DrawingSheetService = require('./drawingSheetService');
const interpretNotationText = require('../external/parseNotation').default;
const { updateExistingItem, classifyCharacteristic } = require('./shared/extraction/extractUtil');
const { getListIdByValue, convertToListObject } = require('./shared/extraction/lists');
const renumberAfterDelete = require('./shared/extraction/renumber').default;
const config = require('../../config')();
const defaults = require('../staticData/defaults');

/**
 * Handles ReviewTask operations
 */
module.exports = class ReviewTaskService {
  constructor({ currentUser, language, eventEmitter }) {
    this.currentUser = currentUser;
    this.language = language;
    this.eventEmitter = eventEmitter;
  }

  /**
   * Adds an array of reviewTasks to the database
   * @param {*} data
   */
  async addToQueue(data) {
    const transaction = await SequelizeRepository.createTransaction(config.reviewAccountId);

    try {
      const tasks = [];
      data.forEach((reviewTask) => {
        tasks.push(ReviewTaskRepository.create(reviewTask, { transaction, currentUser: this.currentUser }));
      });

      await Promise.all(tasks);

      await SequelizeRepository.commitTransaction(transaction);

      if (this.eventEmitter) {
        this.eventEmitter.emit('reviewTask.addToQueue', { accountId: this.currentUser.accountId, siteId: this.currentUser.siteId, data });
      }
    } catch (error) {
      await SequelizeRepository.rollbackTransaction(transaction);
      throw error;
    }
  }

  /**
   * Returns the oldest item in the 'Waiting' state, updates state to 'Processing' and sets processingStart to now
   */
  async pullFromQueue() {
    return ReviewTaskRepository.findNextQueuedTask({
      currentUser: this.currentUser,
    });
  }

  /**
   * Puts item back into the queue by setting state to 'Waiting' and processingStart to null
   * @param {*} id
   */
  async returnToQueue(id) {
    const transaction = await SequelizeRepository.createTransaction(config.reviewAccountId);
    try {
      await ReviewTaskRepository.returnTaskToQueue(id, {
        transaction,
        currentUser: this.currentUser,
      });

      await SequelizeRepository.commitTransaction(transaction);
    } catch (error) {
      await SequelizeRepository.rollbackTransaction(transaction);
      throw error;
    }
  }

  async incrementDrawingSheetCount(data, characteristic) {
    const drawingSheetService = new DrawingSheetService({ currentUser: this.currentUser, language: this.language, eventEmitter: this.eventEmitter });
    const drawingSheet = {
      accountId: data.accountId,
      siteId: data.siteId,
      part: data.partId,
      drawing: data.drawingId,
      pageIndex: characteristic.drawingSheetIndex - 1, // Char index is 1 based, sheet is 0 based
      reviewedCaptureCount: 1,
    };
    await drawingSheetService.incrementInAccount(drawingSheet);
  }

  /**
   * Mark a reviewTask as accepted
   * @param {*} id
   * @param {*} data
   */
  async acceptCapture(id, data) {
    const reviewTransaction = await SequelizeRepository.createTransaction(config.reviewAccountId);
    const accountTransaction = await SequelizeRepository.createTransaction(data.accountId);

    try {
      const reviewTask = { ...data };
      reviewTask.status = 'Accepted';
      reviewTask.processingEnd = new Date();
      await ReviewTaskRepository.update(id, reviewTask, {
        reviewTransaction,
        currentUser: this.currentUser,
      });
      const metadata = JSON.parse(data.metadata);
      const characteristic = await new CharacteristicRepository().updateFromReview(
        metadata.characteristicId,
        { reviewTaskId: id, accountId: data.accountId },
        {
          transaction: accountTransaction,
          currentUser: this.currentUser,
        }
      );

      await SequelizeRepository.commitTransaction(accountTransaction);
      await SequelizeRepository.commitTransaction(reviewTransaction);

      await this.incrementDrawingSheetCount(data, characteristic);
    } catch (error) {
      await SequelizeRepository.rollbackTransaction(accountTransaction);
      await SequelizeRepository.rollbackTransaction(reviewTransaction);
      throw error;
    }
  }

  /**
   * Update a characteristic based on text corrected in a reviewTask
   * @param {*} id
   * @param {*} data
   */
  async correctCapture(id, data) {
    const reviewTransaction = await SequelizeRepository.createTransaction(config.reviewAccountId);
    const accountTransaction = await SequelizeRepository.createTransaction(data.accountId);

    try {
      // Load types
      const listTypes = await ListItemRepository.findByType('Type', {
        currentUser: {
          accountId: data.accountId,
          siteId: data.siteId,
        },
      });
      const types = convertToListObject(listTypes);

      // Load existing characteristic
      const characteristicRepository = new CharacteristicRepository();
      const metadata = JSON.parse(data.metadata);
      const existingItem = await characteristicRepository.findByIdInAccount(metadata.characteristicId, data.accountId, {
        transaction: accountTransaction,
        currentUser: this.currentUser,
      });
      const part = existingItem.part.dataValues;
      let tolerances = {
        linear: defaults.DefaultTolerances.linear,
        angular: defaults.DefaultTolerances.angular,
      };
      let assignedStyles = defaults.defaultMarkerOptions;
      if (part) {
        if (part.defaultAngularTolerances && part.defaultLinearTolerances) {
          tolerances = {
            linear: JSON.parse(part.defaultLinearTolerances),
            angular: JSON.parse(part.defaultAngularTolerances),
          };
        }
        if (part.defaultMarkerOptions) {
          assignedStyles = JSON.parse(part.defaultMarkerOptions);
        }
      }
      const sanitized = metadata.correctedText.replace(/^\n|\n$/g, '');
      // Convert updated text to characteristic
      const result = { parsed: await interpretNotationText(sanitized), ocrRes: metadata.correctedText };
      const { classified, type, subType, notationClass } = classifyCharacteristic(result, tolerances);

      const typeId = getListIdByValue({
        defaultValue: defaults.defaultTypeIDs.Note,
        list: types,
        value: type,
      });
      const subtypeId = getListIdByValue({
        defaultValue: defaults.defaultSubtypeIDs.Note,
        list: types ? types[typeId]?.children : {},
        value: subType,
      });
      const characteristicPropertiesToUpdate = updateExistingItem(result, part, assignedStyles, existingItem, typeId, subtypeId, notationClass, classified, tolerances);
      if (characteristicPropertiesToUpdate) {
        characteristicPropertiesToUpdate.reviewTaskId = id;
        characteristicPropertiesToUpdate.accountId = data.accountId;

        // Write changes to characteristic and reviewTask to database
        const updates = [];
        updates.push(
          ReviewTaskRepository.update(
            id,
            { ...data, status: 'Corrected', processingEnd: new Date() },
            {
              transaction: reviewTransaction,
              currentUser: this.currentUser,
            }
          )
        );
        updates.push(
          characteristicRepository.updateFromReview(
            metadata.characteristicId, //
            characteristicPropertiesToUpdate,
            {
              transaction: accountTransaction,
              currentUser: this.currentUser,
            }
          )
        );
        await Promise.all(updates);
        await SequelizeRepository.commitTransaction(accountTransaction);
        await SequelizeRepository.commitTransaction(reviewTransaction);

        if (this.eventEmitter) {
          this.eventEmitter.emit('characteristic.update', { accountId: data.accountId, siteId: data.siteId, characteristicPropertiesToUpdate });
        }

        await this.incrementDrawingSheetCount(data, existingItem);
      } else {
        throw new Error('Could not update characteristic text.');
      }
    } catch (error) {
      await SequelizeRepository.rollbackTransaction(accountTransaction);
      await SequelizeRepository.rollbackTransaction(reviewTransaction);
      throw error;
    }
  }

  /**
   * Mark task as rejected, soft delete the associated characteristic, renumber the remaining characteristics
   * @param {*} id
   * @param {*} data
   */
  async rejectCapture(id, data) {
    const reviewTransaction = await SequelizeRepository.createTransaction(config.reviewAccountId);
    const accountTransaction = await SequelizeRepository.createTransaction(data.accountId);

    try {
      const characteristicRepository = new CharacteristicRepository();
      const partCharacteristics = await characteristicRepository.findAndCountAllForReview(data.partId, data.accountId);
      const renumber = renumberAfterDelete([id], partCharacteristics.rows);

      const updates = renumber.itemsToUpdate.map((item) => characteristicRepository.updateFromReview(item.id, { ...item, accountId: data.accountId }, { transaction: accountTransaction, currentUser: this.currentUser }));
      updates.push(
        ReviewTaskRepository.update(
          id,
          {
            ...data,

            status: 'Rejected',
            processingEnd: new Date(),
          },
          {
            reviewTransaction,
            currentUser: this.currentUser,
          }
        )
      );
      const metadata = JSON.parse(data.metadata);
      updates.push(
        characteristicRepository.updateFromReview(
          metadata.characteristicId,
          {
            reviewTaskId: id, //
            accountId: data.accountId,
          },
          {
            transaction: accountTransaction,
            currentUser: this.currentUser,
          }
        )
      );
      await Promise.all(updates);
      const characteristic = await characteristicRepository.destroyFromReview(
        metadata.characteristicId,
        {
          reviewTaskId: id, //
          deletedAt: new Date(),
          accountId: data.accountId,
        },
        {
          transaction: accountTransaction,
          currentUser: this.currentUser,
        }
      );

      await SequelizeRepository.commitTransaction(accountTransaction);
      await SequelizeRepository.commitTransaction(reviewTransaction);

      if (this.eventEmitter) {
        this.eventEmitter.emit('characteristic.destroy', { accountId: data.accountId, siteId: data.siteId, characteristic });
      }

      await this.incrementDrawingSheetCount(data, characteristic);
    } catch (error) {
      await SequelizeRepository.rollbackTransaction(accountTransaction);
      await SequelizeRepository.rollbackTransaction(reviewTransaction);
      throw error;
    }
  }

  /**
   * Returns a count of reviewTasks in "Waiting" or "Processing" status and total reviewTasks for a part
   * @param {*} partId
   */
  async partQueuedCaptureCount(partId) {
    if (!partId) {
      return null;
    }

    const filter = {
      status: {
        [Op.or]: ['Waiting', 'Processing'],
      },
      partId,
    };

    const counts = [];
    counts.push(
      ReviewTaskRepository.count(filter, {
        currentUser: this.currentUser,
      })
    );
    counts.push(
      ReviewTaskRepository.count(
        { partId },
        {
          currentUser: this.currentUser,
        }
      )
    );
    const result = await Promise.all(counts);
    const [waiting, total] = result;
    return { waiting, total };
  }

  /**
   * Shows number of waiting tasks for a part or all time if partId is null
   * @param {*} partId
   */
  async waitingTasksCount(partId) {
    const filter = {
      status: 'Waiting',
    };

    if (partId) {
      filter.partId = partId;
    }

    return ReviewTaskRepository.count(filter, {
      currentUser: this.currentUser,
    });
  }

  /**
   * Shows number of processed tasks for a part or all time if partId is null
   * @param {*} partId
   */
  async processedTasksCount(partId) {
    const filter = {
      status: {
        [Op.or]: ['Accepted', 'Corrected', 'Rejected'],
      },
    };

    if (partId) {
      filter.partId = partId;
    }

    return ReviewTaskRepository.count(filter, {
      currentUser: this.currentUser,
    });
  }

  /**
   * Shows total number of tasks for a part or all time if partId is null
   * @param {*} partId
   */
  async totalTasksCount(partId) {
    const filter = {};

    if (partId) {
      filter.partId = partId;
    }

    return ReviewTaskRepository.count(filter, {
      currentUser: this.currentUser,
    });
  }

  /**
   * Returns the average elapsed time that reviewTasks are waiting before being processed
   */
  async averageTimeInQueue() {
    const filter = {};

    const result = await ReviewTaskRepository.elapsedTime(filter, 'createdAt', 'processingStart', {
      currentUser: this.currentUser,
    });
    if (result && result.length > 0) {
      return JSON.stringify(result[0].dataValues);
    }
    return '0';
  }

  /**
   * Returns the average elapsed time that reviewTasks take to processed
   */
  async averageTimeToProcess() {
    const filter = {};

    const result = await ReviewTaskRepository.elapsedTime(filter, 'processingStart', 'processingEnd', {
      currentUser: this.currentUser,
    });
    if (result && result.length > 0) {
      return JSON.stringify(result[0].dataValues);
    }
    return '0';
  }

  /**
   * Returns the number of tasks waiting that are the given number of minutes old
   * @param {*} minutesOld
   */
  static async agingTasksCount(minutesOld) {
    const filter = {
      status: 'Waiting',
      createdAt: {
        [Op.lte]: new Date(Date.now() - minutesOld * 60 * 1000),
      },
    };
    const result = await ReviewTaskRepository.count(filter);
    if (result) {
      return result;
    }
    return 0;
  }

  /**
   * Removes items in Waiting or Processing status from the Review Queue by soft deleting them.
   *
   * @returns Number of items removed from queue
   */
  async clearQueue() {
    return ReviewTaskRepository.clearQueue();
  }

  /**
   * Creates a ReviewTask.
   *
   * @param {*} data
   */
  async create(data) {
    const transaction = await SequelizeRepository.createTransaction(config.reviewAccountId);

    try {
      const record = await ReviewTaskRepository.create(data, {
        transaction,
        currentUser: this.currentUser,
      });

      await SequelizeRepository.commitTransaction(transaction);

      return record;
    } catch (error) {
      await SequelizeRepository.rollbackTransaction(transaction);
      throw error;
    }
  }

  /**
   * Updates a ReviewTask.
   *
   * @param {*} id
   * @param {*} data
   */
  async update(id, data) {
    const transaction = await SequelizeRepository.createTransaction(config.reviewAccountId);

    try {
      const record = await ReviewTaskRepository.update(id, data, {
        transaction,
        currentUser: this.currentUser,
      });

      await SequelizeRepository.commitTransaction(transaction);

      return record;
    } catch (error) {
      await SequelizeRepository.rollbackTransaction(transaction);
      throw error;
    }
  }

  /**
   * Destroy all ReviewTasks with those ids.
   *
   * @param {*} ids
   */
  async destroyAll(ids) {
    const transaction = await SequelizeRepository.createTransaction(config.reviewAccountId);

    try {
      let idsToDelete = ids;
      if (!Array.isArray(ids) && typeof ids === 'string') {
        idsToDelete = [ids];
      }
      await Promise.all(idsToDelete.map((id) => ReviewTaskRepository.destroy(id, { transaction, currentUser: this.currentUser })));

      await SequelizeRepository.commitTransaction(transaction);
    } catch (error) {
      await SequelizeRepository.rollbackTransaction(transaction);
      throw error;
    }
  }

  /**
   * Finds the ReviewTask by Id.
   *
   * @param {*} id
   */
  async findById(id) {
    return ReviewTaskRepository.findById(id, {
      currentUser: this.currentUser,
    });
  }

  /**
   * Finds ReviewTasks based on the query.
   *
   * @param {*} args
   */
  async findAndCountAll(args) {
    return ReviewTaskRepository.findAndCountAll(args, {
      currentUser: this.currentUser,
    });
  }
};
