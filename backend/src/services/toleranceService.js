const { defaultSubtypeIDs } = require('../staticData/defaults');

module.exports = class ToleranceService {
  /**
   * Converts a text to snake_case format.
   * @param {string} text
   * @returns {string} The text converted to snake_case format.
   */
  toSnakeCase(text) {
    return text.toLowerCase().replace(/ /g, '_');
  }

  /**
   * Extracts numeric characters from a string and returns them as a string.
   * @param {string} text
   * @returns {string} The extracted numeric characters as a string.
   */
  toNumericString(text) {
    return text.replace(/[^0-9.,]/g, '');
  }

  /**
   * Determines the precision of a numeric value.
   * @param {string} value
   * @returns {number} The precision of the numeric value.
   */
  getPrecision(value) {
    if (!value) {
      return 0;
    }
    const parts = value.trim().split('.');
    return parts[1] ? parts[1].length : 0;
  }

  /**
   * Determines the greatest precision among the provided numeric values.
   * @param {Object} options
   * @param {string} options.nominal
   * @param {string} [options.plus]
   * @param {string} [options.minus]
   * @returns {number} The greatest precision among the provided values.
   */
  getGreatestPrecision({ nominal, plus, minus }) {
    const all = [this.getPrecision(nominal)];
    if (plus) {
      all.push(this.getPrecision(plus));
    }
    if (minus) {
      all.push(this.getPrecision(minus));
    }
    all.sort();
    return all[all.length - 1];
  }

  /**
   * Retrieves the appropriate tolerances based on the subtype.
   * @param {Object} options
   * @param {string} options.subType
   * @param {Tolerance} options.tolerances
   * @returns {ToleranceType} The appropriate tolerances based on the subtype.
   */
  getTolerancesForSubtype({ subType, tolerances }) {
    return subType === defaultSubtypeIDs.Angle || (subType && subType.toLowerCase() === 'angle') ? tolerances.angular : tolerances.linear;
  }

  /**
   * Determines the ranged tolerance values for a given nominal value
   * @param {RangeTolerance[]} tolerance
   * @param {string} nominal
   * @returns {{ plus: string, minus: string }} The ranged tolerance values.
   */
  getRangedTolerance(tolerance, nominal) {
    let plus = '';
    let minus = '';
    for (let index = 0; index < tolerance.length; index++) {
      const range = tolerance[index];
      const nominalNum = parseFloat(nominal);
      if (nominalNum >= parseFloat(range.rangeLow) && nominalNum <= parseFloat(range.rangeHigh)) {
        plus = range.plus;
        minus = range.minus;
        break;
      }
    }
    return { plus, minus };
  }

  /**
   * Determines the precision tolerance values for a given nominal value
   * @param {PrecisionTolerance[]} tolerance
   * @param {string} nominal
   * @returns {{ plus: string, minus: string }} The precision tolerance values.
   */
  getPrecisionTolerance(tolerance, nominal) {
    const precision = this.getPrecision(nominal);
    let plus = '';
    let minus = '';
    for (let index = 0; index < tolerance.length; index++) {
      const tol = tolerance[index];
      const tolPrecision = this.getPrecision(tol.level);
      if (precision === tolPrecision) {
        plus = tol.plus;
        minus = tol.minus;
        break;
      }
    }
    return { plus, minus };
  }

  /**
   * Calculates the upper and lower specification limits from nominal and tolerance values.
   * @param {Object} options
   * @param {string} options.nominal
   * @param {string} [options.plus]
   * @param {string} [options.minus]
   * @returns {{ upper: string, lower: string }} The upper and lower specification limits.
   */
  calculateSpecLimitFromTols({ nominal, plus, minus }) {
    let upper = '';
    let lower = '';
    const precision = this.getGreatestPrecision({ nominal, plus, minus });
    if (plus) {
      upper = (parseFloat(nominal) + parseFloat(plus)).toFixed(precision).toString();
    }
    if (minus) {
      lower = (parseFloat(nominal) + parseFloat(minus)).toFixed(precision).toString(); // plus plus, minus, minus ???
    }
    return { upper, lower };
  }

  /**
   * Calculates the geometric tolerance tolerances from the total tolerance value.
   * @param {string} totalTol
   * @returns {{ plus: string, minus: string }} The plus and minus tolerances for geometric tolerance.
   */
  calculateGDTTolerances(totalTol) {
    // if the totalTol is odd then we need to add an extra precision so a tolerance of 0.25 becomes 0.125 instead of 0.13
    const buffer = parseInt(totalTol.charAt(totalTol.length - 1), 10) % 2 === 1 ? 1 : 0;
    const precision = this.getPrecision(totalTol) + buffer;
    const plus = (parseFloat(totalTol) / 2).toFixed(precision);
    const minus = ((parseFloat(totalTol) * -1) / 2).toFixed(precision); // plus plus, minus, minus ???
    return {
      plus,
      minus,
    };
  }

  /**
   * Calculates the dimension tolerances based on the nominal value and the provided tolerance section.
   * @param {Object} options
   * @param {string} options.nominal
   * @param {TolSection} options.tolerances
   * @returns {{ plus: string, minus: string }} The plus and minus tolerances for the dimension.
   */
  calculateDimensionTolerances({ nominal, tolerances }) {
    if (tolerances.type === 'PrecisionTolerance') {
      return this.getPrecisionTolerance(tolerances.precisionTols, nominal);
    }
    return this.getRangedTolerance(tolerances.rangeTols, nominal);
  }

  /**
   * Calculates the specification limits for a geometric tolerance based on the provided measurement and primary tolerance zone.
   * @param {ParsedCharacteristic} feature
   * @param {string} measurement
   * @param {string} [primary]
   * @returns {ParsedCharacteristic} The geometric tolerance feature with updated specification limits.
   * @throws {Error} Throws an error if no primary tolerance zone is provided.
   */
  calculateGDTSpecLimits(feature, measurement, primary) {
    if (!primary) {
      throw new Error('No Primary Tolerance Zone');
    }
    const primaryVal = this.toNumericString(primary);
    const gdtWithLimits = { ...feature };
    gdtWithLimits.toleranceSource = 'Document_Defined';
    if (measurement.toLocaleLowerCase().includes('profile')) {
      const { plus, minus } = this.calculateGDTTolerances(primaryVal);
      const { upper, lower } = this.calculateSpecLimitFromTols({
        nominal: '0',
        plus,
        minus,
      });
      gdtWithLimits.upperSpecLimit = upper;
      gdtWithLimits.lowerSpecLimit = lower;
    } else {
      gdtWithLimits.upperSpecLimit = primaryVal;
      gdtWithLimits.lowerSpecLimit = '';
    }
    return gdtWithLimits;
  }

  /**
   * Calculates the specification limits for a dimension feature based on the provided tolerances and measurement type.
   * @param {Object} options
   * @param {ParsedCharacteristic} options.feature
   * @param {Tolerance} options.tolerances
   * @param {string} options.measurement
   * @returns {ParsedCharacteristic} The dimension feature with updated specification limits.
   */
  calculateDimensionSpecLimits({ feature, tolerances, measurement }) {
    const dimWithLimits = { ...feature };
    if (!dimWithLimits.nominal) {
      return dimWithLimits;
    }
    const { nominal } = dimWithLimits;

    if (dimWithLimits.upperSpecLimit && dimWithLimits.lowerSpecLimit) {
      return dimWithLimits;
    }

    const tols = this.getTolerancesForSubtype({
      subType: measurement,
      tolerances,
    });
    let plus = dimWithLimits.plusTol || '';
    let minus = dimWithLimits.minusTol || '';

    // Get defaults if either tolerance is missing
    if (plus === '' && minus === '') {
      dimWithLimits.toleranceSource = 'Default_Tolerance';
    }
    if (plus === '' || minus === '') {
      const tol = this.calculateDimensionTolerances({
        nominal: nominal,
        tolerances: tols,
      });
      plus = tol.plus;
      minus = tol.minus;
      dimWithLimits.plusTol = dimWithLimits.plusTol || plus;
      dimWithLimits.minusTol = dimWithLimits.minusTol || minus;
    }

    // Calculate spec limits using tolerances
    const { upper, lower } = this.calculateSpecLimitFromTols({
      nominal: nominal,
      plus,
      minus,
    });
    dimWithLimits.upperSpecLimit = upper;
    dimWithLimits.lowerSpecLimit = lower;
    if (!dimWithLimits.unit || dimWithLimits.unit === '') {
      // Don't replace with tolerance unit unless feature has none
      dimWithLimits.unit = tols.unit;
    }

    return dimWithLimits;
  }

  /**
   * Assigns tolerances to a feature based on the provided tolerances and measurement type, updating the feature's specification limits accordingly.
   * @param {Object} options
   * @param {ParsedCharacteristic} options.feature
   * @param {Tolerance} options.tolerances
   * @param {string} options.measurement
   * @param {boolean} options.isGDT
   * @returns {ParsedCharacteristic} The feature with updated specification limits based on the assigned tolerances.
   * @throws {Error} If the feature or tolerances are null.
   */
  assignFeatureTolerances({
    feature,
    tolerances,
    measurement, // subtype or subtype measurement for dimensions and GDT
    isGDT,
  }) {
    if (!feature) {
      throw new Error('Cannot assign tolerances to null feature');
    }
    if (!tolerances) {
      throw new Error('Cannot assign tolerances without part defaults');
    }
    let tolerancedFeature = { ...feature };
    if (measurement) {
      if (isGDT) {
        tolerancedFeature = this.calculateGDTSpecLimits(tolerancedFeature, measurement, tolerancedFeature.gdtPrimaryToleranceZone);
      } else {
        tolerancedFeature = this.calculateDimensionSpecLimits({
          feature: tolerancedFeature,
          tolerances,
          measurement,
        });
      }
    }
    // no measurement = note
    return tolerancedFeature;
  }
};
