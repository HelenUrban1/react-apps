const config = require('../../config')();

const USE_MOCKS = config.useMocks;
const { part, sheet, feature, revisedPart, revisedDrawing, revisedSheet, revisedFeature, partInput, drawingInput, sheetInput, featureInput, revisions } = require('../__fixtures__/revisionFixture');

if (USE_MOCKS) {
  jest.doMock('../database/models');
  jest.doMock('../database/repositories/partRepository');
  jest.doMock('../database/repositories/drawingRepository');
  jest.doMock('../database/repositories/drawingSheetRepository');
  jest.doMock('../database/repositories/characteristicRepository');
}

const models = require('../database/models');
const PartRepository = require('../database/repositories/partRepository');
const DrawingRepository = require('../database/repositories/drawingRepository');
const DrawingSheetRepository = require('../database/repositories/drawingSheetRepository');
const CharacteristicRepository = require('../database/repositories/characteristicRepository');
const RevisionService = require('./revisionService');
const SequelizeRepository = require('../database/repositories/sequelizeRepository');
const { convertToInput: convertToPartInput } = require('../database/repositories/partRepository');
const { convertToInput: convertToDrawingInput } = require('../database/repositories/drawingRepository');
const { convertToInput: convertToDrawingSheetInput } = require('../database/repositories/drawingSheetRepository');
const { convertToInput: convertToCharacteristicInput } = require('../database/repositories/characteristicRepository');

describe('RevisionService', () => {
  const accountId = config.testAccountId;
  const findPartMock = jest.fn().mockReturnValue({ ...part });
  const createPartMock = jest.fn().mockReturnValue({ ...revisedPart });
  const partInputMock = jest.fn().mockReturnValue({ ...partInput });
  const updatePartMock = jest.fn().mockReturnValue({ ...part });
  const findAndCountAllMock = jest.fn().mockReturnValue({ ...revisions });
  const markPartAsDeletedMock = jest.fn();
  const findDrawingMock = jest.fn().mockReturnValue({ ...revisedDrawing });
  const createDrawingMock = jest.fn().mockReturnValue({ ...revisedDrawing });
  const drawingInputMock = jest.fn().mockReturnValue({ ...drawingInput });
  const findDrawingSheetMock = jest.fn().mockReturnValue({ ...sheet });
  const createDrawingSheetMock = jest.fn().mockReturnValue({ ...revisedSheet });
  const sheetInputMock = jest.fn().mockReturnValue({ ...sheetInput });
  const markDrawingAsDeletedMock = jest.fn();
  const findCharacteristicMock = jest.fn().mockReturnValue({ ...feature });
  const createCharacteristicMock = jest.fn().mockReturnValue({ ...revisedFeature });
  const characteristicInputMock = jest.fn().mockReturnValue({ ...featureInput });

  beforeEach(async () => {
    if (USE_MOCKS) {
      models.getTenant = () =>
        Promise.resolve({
          sequelize: {
            transaction: jest.fn().mockReturnValue({
              rollback: jest.fn(),
              commit: jest.fn(),
            }),
          },
        });

      models.sequelizeAdmin = {
        query: jest.fn().mockReturnValue(Promise.resolve({})),
      };

      PartRepository.mockImplementation(() => {
        return {
          findById: findPartMock,
          create: createPartMock,
          update: updatePartMock,
          findAndCountAll: findAndCountAllMock,
          markAsDeleted: markPartAsDeletedMock,
        };
      });

      convertToPartInput.mockImplementation(() => {
        return partInputMock;
      });

      DrawingRepository.mockImplementation(() => {
        return {
          findById: findDrawingMock,
          create: createDrawingMock,
          markAsDeleted: markDrawingAsDeletedMock,
        };
      });

      convertToDrawingInput.mockImplementation(() => {
        return drawingInputMock;
      });

      DrawingSheetRepository.mockImplementation(() => {
        return {
          findById: findDrawingSheetMock,
          create: createDrawingSheetMock,
        };
      });

      convertToDrawingSheetInput.mockImplementation(() => {
        return sheetInputMock;
      });

      CharacteristicRepository.mockImplementation(() => {
        return {
          findById: findCharacteristicMock,
          create: createCharacteristicMock,
        };
      });

      convertToCharacteristicInput.mockImplementation(() => {
        return characteristicInputMock;
      });

      SequelizeRepository.commitTransaction = jest.fn();
    }
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  afterAll(async () => {
    if (!USE_MOCKS) {
      await SequelizeRepository.closeConnections();
      await SequelizeRepository.closeConnections(accountId);
    }
  });

  describe('Create Part Revision', () => {
    const context = { currentUser: { accountId } };

    it('Should return a revised part', async () => {
      try {
        const revision = await new RevisionService(context).createPartRevision(part.id);
        if (USE_MOCKS) {
          expect(findPartMock).toBeCalledTimes(1);
          expect(createPartMock).toBeCalledTimes(1);
          expect(createDrawingMock).toBeCalledTimes(1);
          expect(createDrawingSheetMock).toBeCalledTimes(1);
          expect(createCharacteristicMock).toBeCalledTimes(1);
        }
        expect(revision.part.id).toBe(revisedPart.id);
        expect(revision.drawings[0].id).toBe(revisedDrawing.id);
        expect(revision.sheets[0].id).toBe(revisedSheet.id);
        expect(revision.characteristics[0].id).toBe(revisedFeature.id);
      } catch (exception) {
        console.error(exception);
        expect(exception).toBeFalsy();
      }
    }, 10000);
  });

  describe('Create Drawing Revision', () => {
    const context = { currentUser: { accountId } };

    it('Should return a revised drawing', async () => {
      try {
        const revision = await new RevisionService(context).createDrawingRevision(part.id);
        if (USE_MOCKS) {
          expect(findPartMock).toBeCalledTimes(1);
          expect(findDrawingMock).toBeCalledTimes(3);
          expect(updatePartMock).toBeCalledTimes(1);
          expect(createCharacteristicMock).toBeCalledTimes(1);
        }
        expect(revision.part.id).toBe(part.id);
        expect(revision.drawing.id).toBe(revisedDrawing.id);
        expect(revision.characteristics[0].id).toBe(revisedFeature.id);
      } catch (exception) {
        console.error(exception);
        expect(exception).toBeFalsy();
      }
    }, 10000);
  });

  describe('deletePart', () => {
    const context = { currentUser: { accountId } };

    it('should call findById on the part repository', async () => {
      await new RevisionService(context).deletePart(part.id);

      expect(findPartMock).toBeCalledTimes(1);
    });

    it('should call findAndCountAll on the part repository', async () => {
      await new RevisionService(context).deletePart(part.id);

      expect(findAndCountAllMock).toBeCalledTimes(1);
    });

    it('should call markAsDeleted on the part repository for each revision returned', async () => {
      await new RevisionService(context).deletePart(part.id);

      expect(markPartAsDeletedMock).toBeCalledTimes(2);
    });

    describe('and there are child drawings', () => {
      it('should call markAsDeleted on the drawing repository for each drawing', async () => {
        await new RevisionService(context).deletePart(part.id);

        expect(markDrawingAsDeletedMock).toBeCalledTimes(2);
      });
    });
  });
});
