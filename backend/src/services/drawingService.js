const path = require('path');
const DrawingRepository = require('../database/repositories/drawingRepository');
const ValidationError = require('../errors/validationError');
const SequelizeRepository = require('../database/repositories/sequelizeRepository');
const { log } = require('../logger');

/**
 * Handles Drawing operations
 */
module.exports = class DrawingService {
  constructor({ currentUser, language }) {
    this.repository = new DrawingRepository();
    this.currentUser = currentUser;
    this.language = language;
  }

  /**
   * Creates a Drawing.
   *
   * @param {*} data
   */
  async create(data) {
    const transaction = await SequelizeRepository.createTransaction(this.currentUser.accountId);

    try {
      data.file.forEach((file) => {
        if (path.extname(file.name).toLocaleLowerCase() !== '.pdf') {
          throw new ValidationError(this.language, 'importer.errors.invalidFilePdf');
        }
      });

      const record = await this.repository.create(data, {
        transaction,
        currentUser: this.currentUser,
      });

      await SequelizeRepository.commitTransaction(transaction);

      return record;
    } catch (error) {
      log.error(error);
      await SequelizeRepository.rollbackTransaction(transaction);
      throw error;
    }
  }

  /**
   * Updates a Drawing.
   *
   * @param {*} id
   * @param {*} data
   */
  async update(id, data) {
    const transaction = await SequelizeRepository.createTransaction(this.currentUser.accountId);

    try {
      const record = await this.repository.update(id, data, {
        transaction,
        currentUser: this.currentUser,
      });

      await SequelizeRepository.commitTransaction(transaction);

      return record;
    } catch (error) {
      await SequelizeRepository.rollbackTransaction(transaction);
      throw error;
    }
  }

  /**
   * Destroy all Drawings with those ids.
   *
   * @param {*} ids
   */
  async destroyAll(ids) {
    const transaction = await SequelizeRepository.createTransaction(this.currentUser.accountId);

    try {
      for (const id of ids) {
        await this.repository.update(
          id,
          { status: 'Deleted' },
          {
            transaction,
            currentUser: this.currentUser,
          }
        );
        await this.repository.destroy(id, {
          transaction,
          currentUser: this.currentUser,
        });
      }

      await SequelizeRepository.commitTransaction(transaction);
    } catch (error) {
      await SequelizeRepository.rollbackTransaction(transaction);
      throw error;
    }
  }

  /**
   * Finds the Drawing by Id.
   *
   * @param {*} id
   */
  async findById(id) {
    return this.repository.findById(id, {
      currentUser: this.currentUser,
    });
  }

  /**
   * Finds Drawings for Autocomplete.
   *
   * @param {*} search
   * @param {*} limit
   */
  async findAllAutocomplete(search, limit) {
    return this.repository.findAllAutocomplete(search, limit, {
      currentUser: this.currentUser,
    });
  }

  /**
   * Finds Drawings based on the query.
   *
   * @param {*} args
   */
  async findAndCountAll(args) {
    return this.repository.findAndCountAll(args, {
      currentUser: this.currentUser,
    });
  }

  /**
   * Imports a list of Drawings.
   *
   * @param {*} data
   * @param {*} importHash
   */
  async import(data, importHash) {
    if (!importHash) {
      throw new ValidationError(this.language, 'importer.errors.importHashRequired');
    }

    if (await this._isImportHashExistent(importHash)) {
      throw new ValidationError(this.language, 'importer.errors.importHashExistent');
    }

    const dataToCreate = {
      ...data,
      importHash,
    };

    return this.create(dataToCreate);
  }

  /**
   * Checks if the import hash already exists.
   * Every item imported has a unique hash.
   *
   * @param {*} importHash
   */
  async _isImportHashExistent(importHash) {
    const count = await this.repository.count({
      importHash,
    });

    return count > 0;
  }
};
