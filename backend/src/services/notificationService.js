const NotificationRepository = require('../database/repositories/notificationRepository');
const ValidationError = require('../errors/validationError');
const SequelizeRepository = require('../database/repositories/sequelizeRepository');
const { log } = require('../logger');

/**
 * Handles Notification operations
 */
module.exports = class NotificationService {
  constructor({ currentUser, language, eventEmitter }) {
    this.repository = new NotificationRepository();
    this.currentUser = currentUser;
    this.language = language;
    this.eventEmitter = eventEmitter;
  }

  /**
   * Creates a Notification.
   *
   * @param {*} data
   */
  async create(data) {
    log.debug(`notificationService.create: title: ${data.title} content: ${data.content}`);
    const transaction = await SequelizeRepository.createTransaction(this.currentUser.accountId);

    try {
      const record = await this.repository.create(data, {
        transaction,
        currentUser: this.currentUser,
      });

      await SequelizeRepository.commitTransaction(transaction);

      if (this.eventEmitter) {
        log.debug('Emitting notification.create', this.currentUser.accountId, record.id);
        this.eventEmitter.emit('notification.create', { accountId: this.currentUser.accountId, siteId: this.currentUser.siteId, data: record });
      }

      return record;
    } catch (error) {
      await SequelizeRepository.rollbackTransaction(transaction);
      throw error;
    }
  }

  async createInAccount(data) {
    log.debug(`notificationService.createInAccount: accountId: ${data.accountId} siteId: ${data.siteId} title: ${data.title} content: ${data.content}`);
    const transaction = await SequelizeRepository.createTransaction(data.accountId);

    try {
      const record = await this.repository.createInAccount(data, {
        transaction,
        currentUser: this.currentUser,
      });

      await SequelizeRepository.commitTransaction(transaction);
      log.debug(`notificationService.createInAccount eventEmitter: ${!!this.eventEmitter}`);
      if (this.eventEmitter) {
        log.debug(`Emitting notification.create id: ${record.id} accountId: ${data.accountId} siteId: ${data.siteId}`);
        this.eventEmitter.emit('notification.create', { accountId: data.accountId, siteId: data.siteId, data: record });
      }

      return record;
    } catch (error) {
      await SequelizeRepository.rollbackTransaction(transaction);
      throw error;
    }
  }

  /**
   * Updates a Notification.
   *
   * @param {*} id
   * @param {*} data
   */
  async update(id, data) {
    const transaction = await SequelizeRepository.createTransaction(this.currentUser.accountId);

    try {
      const record = await this.repository.update(id, data, {
        transaction,
        currentUser: this.currentUser,
      });

      await SequelizeRepository.commitTransaction(transaction);

      if (this.eventEmitter) {
        this.eventEmitter.emit('notification.update', { accountId: this.currentUser.accountId, siteId: this.currentUser.siteId, data: record });
      }

      return record;
    } catch (error) {
      await SequelizeRepository.rollbackTransaction(transaction);
      throw error;
    }
  }

  /**
   * Destroy all Notifications with those ids.
   *
   * @param {*} ids
   */
  async destroyAll(ids) {
    const transaction = await SequelizeRepository.createTransaction(this.currentUser.accountId);

    try {
      let idsToDelete = ids;
      if (!Array.isArray(ids) && typeof ids === 'string') {
        idsToDelete = [ids];
      }
      await Promise.all(idsToDelete.map((id) => this.repository.destroy(id, { transaction, currentUser: this.currentUser })));

      await SequelizeRepository.commitTransaction(transaction);

      if (this.eventEmitter) {
        this.eventEmitter.emit('notification.destroyAll', { accountId: this.currentUser.accountId, siteId: this.currentUser.siteId, data: ids });
      }
    } catch (error) {
      await SequelizeRepository.rollbackTransaction(transaction);
      throw error;
    }
  }

  /**
   * Finds the Notification by Id.
   *
   * @param {*} id
   */
  async findById(id) {
    return this.repository.findById(id, {
      currentUser: this.currentUser,
    });
  }

  /**
   * Counts Notifications based on the query.
   *
   * @param {*} args
   */
  async count(args) {
    return this.repository.count(args, {
      currentUser: this.currentUser,
    });
  }

  /**
   * Finds Notifications based on the query.
   *
   * @param {*} args
   */
  async findAndCountAll(args) {
    return this.repository.findAndCountAll(args, {
      currentUser: this.currentUser,
    });
  }

  /**
   * Imports a list of Notifications.
   *
   * @param {*} data
   * @param {*} importHash
   */
  async import(data, importHash) {
    if (!importHash) {
      throw new ValidationError(this.language, 'importer.errors.importHashRequired');
    }

    if (await this._isImportHashExistent(importHash)) {
      throw new ValidationError(this.language, 'importer.errors.importHashExistent');
    }

    const dataToCreate = {
      ...data,
      importHash,
    };

    return this.create(dataToCreate);
  }

  /**
   * Checks if the import hash already exists.
   * Every item imported has a unique hash.
   *
   * @param {*} importHash
   */
  async _isImportHashExistent(importHash) {
    const count = await this.repository.count({
      importHash,
    });

    return count > 0;
  }
};
