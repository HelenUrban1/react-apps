const OcrService = require('./ocrService');
const axios = require('axios');
const _ = require('lodash');
const config = require('../../config')();

/**
 * A helper function to convert the FormData fields of a form into an Object.
 * @param {Array} array The FormData array of form fields.
 * @returns {Object} An object containing the form fields and values as an Object.
 */
function convertFormFieldsToObject(array) {
  const object = {};
  let currentField = null;

  for (const item of array) {
    if (typeof item === 'string' && !item.includes('Content-Disposition')) {
      object[currentField] = item.trim();
    } else if (typeof item === 'function' && item.name === 'bound') {
      continue;
    } else if (typeof item === 'string') {
      const match = item.match(/name="([^"]+)"/);
      if (match) {
        currentField = match[1];
      }
    }
  }

  return object;
}

jest.mock('axios');
jest.useFakeTimers();

describe('OcrService', () => {
  let ocrService;

  const assignedStyles = {
    default: {
      style: 'f12c5566-7612-48e9-9534-ef90b276ebaa',
      assign: ['Default'],
    },
  };

  const dimension = {
    status: 'Active',
    verified: false,
    notationType: 'db687ac1-7bf8-4ada-be2b-979f8921e1a0', // Dimension
    notationSubtype: 'edc8ecda-5f88-4b81-871f-6c02f1c50afd', // Length
    notationClass: 'Tolerance',
    fullSpecification: '1.25±.01',
    quantity: 1,
    nominal: '1.25',
    upperSpecLimit: '1.26',
    lowerSpecLimit: '1.24',
    plusTol: '.01',
    minusTol: '-.01',
    toleranceSource: 'Document_Defined',
    drawingRotation: 0.0,
    drawingScale: 1,
    boxLocationY: 120,
    boxLocationX: 220,
    boxWidth: 250,
    boxHeight: 100,
    boxRotation: 0,
    markerLocationX: 198,
    markerLocationY: 180,
    markerFontSize: 40,
    markerSize: 80,
    markerGroup: '1',
    markerGroupShared: false,
    markerIndex: 0,
    markerSubIndex: -1,
    markerLabel: '1',
    markerStyle: 'f12c5566-7612-48e9-9534-ef90b276ebaa',
    gridCoordinates: '3B',
    balloonGridCoordinates: '4B',
    connectionPointGridCoordinates: 'External',
  };

  const newDimension = {
    toleranceSource: 'Document_Defined',
    notationType: 'db687ac1-7bf8-4ada-be2b-979f8921e1a0',
    notationSubtype: 'edc8ecda-5f88-4b81-871f-6c02f1c50afd',
    fullSpecification: '1.25±.01',
    nominal: '1.25',
    plusTol: '.01',
    minusTol: '-.01',
    upperSpecLimit: '1.26',
    lowerSpecLimit: '1.24',
    quantity: 1,
    notationClass: 'Tolerance',
    unit: 'placeholder',
    captureError: '',
  };

  const GDT = {
    status: 'Active',
    verified: false,
    notationType: 'f3c4d8e0-fb79-4c72-a3f8-3f584ea075a7', // Geometric Tolerance
    notationSubtype: '5ce784e8-f614-4e6d-a79b-237b4dc751dd', // Angularity
    notationClass: 'Tolerance',
    fullSpecification: '<', // |<|1.5|A|B|C|
    quantity: 1,
    upperSpecLimit: '1.5',
    lowerSpecLimit: '',
    gdtSymbol: '',
    gdtPrimaryToleranceZone: '1.5',
    gdtSecondaryToleranceZone: '',
    gdtPrimaryDatum: 'A',
    gdtSecondaryDatum: 'B',
    gdtTertiaryDatum: 'C',
    toleranceSource: 'Document_Defined',
    drawingRotation: 0.0,
    drawingScale: 1,
    boxLocationY: 120,
    boxLocationX: 220,
    boxWidth: 250,
    boxHeight: 100,
    boxRotation: 0,
    markerLocationX: 198,
    markerLocationY: 180,
    markerFontSize: 40,
    markerSize: 80,
    markerGroup: '1',
    markerGroupShared: false,
    markerIndex: 0,
    markerSubIndex: -1,
    markerLabel: '1',
    markerStyle: 'f12c5566-7612-48e9-9534-ef90b276ebaa',
    gridCoordinates: '3B',
    balloonGridCoordinates: '4B',
    connectionPointGridCoordinates: 'External',
  };

  const newGDT = {
    toleranceSource: 'Document_Defined',
    notationType: 'f3c4d8e0-fb79-4c72-a3f8-3f584ea075a7',
    fullSpecification: '<',
    gdtSymbol: '',
    gdtPrimaryToleranceZone: '1.5',
    gdtSecondaryToleranceZone: '',
    gdtPrimaryDatum: 'A',
    gdtSecondaryDatum: 'B',
    gdtTertiaryDatum: 'C',
    upperSpecLimit: '1.5',
    lowerSpecLimit: '',
    notationSubtype: '5ce784e8-f614-4e6d-a79b-237b4dc751dd',
    quantity: 1,
    notationClass: 'Tolerance',
    captureError: '',
  };

  const note = {
    status: 'Active',
    verified: false,
    notationType: 'fa3284ed-ad3e-43c9-b051-87bd2e95e927', // Note
    notationSubtype: '805fd539-8063-4158-8361-509e10a5d371', // Note
    notationClass: 'N_A',
    fullSpecification: 'This is a Note We Captured',
    quantity: 1,
    nominal: 'This is a Note We Captured',
    toleranceSource: 'N_A',
    drawingRotation: 0.0,
    drawingScale: 1,
    boxLocationY: 120,
    boxLocationX: 220,
    boxWidth: 250,
    boxHeight: 100,
    boxRotation: 0,
    markerLocationX: 198,
    markerLocationY: 180,
    markerFontSize: 40,
    markerSize: 80,
    markerGroup: '1',
    markerGroupShared: false,
    markerIndex: 0,
    markerSubIndex: -1,
    markerLabel: '1',
    markerStyle: 'f12c5566-7612-48e9-9534-ef90b276ebaa',
    gridCoordinates: '3B',
    balloonGridCoordinates: '4B',
    connectionPointGridCoordinates: 'External',
  };

  const newNote = {
    toleranceSource: 'N_A',
    notationType: 'fa3284ed-ad3e-43c9-b051-87bd2e95e927',
    notationSubtype: '805fd539-8063-4158-8361-509e10a5d371',
    fullSpecification: 'This is a Note We Captured',
    nominal: 'This is a Note We Captured',
    unit: '',
    quantity: 1,
    notationClass: 'N_A',
    captureError: '',
  };

  const mockGDT = {
    input: '|<|1.5|A|B|C|',
    notation: {
      datums: [{ id: 'A' }, { id: 'B' }, { id: 'C' }],
      subtype: 'Angularity',
      text: '|<|1.5|A|B|C|',
      type: 'gdtControlFrame',
      zones: { primary: { modifier: '', postModifierValue: '', prefix: '', text: '1.5', value: '1.5' } },
    },
    quantity: {
      text: '',
      units: '',
      value: '1',
    },
    text: '|<|1.5|A|B|C|',
    words: '',
  };

  const mockDimension = {
    input: '1.25±.01',
    notation: {
      nominal: { text: '1.25', value: '1.25', units: '' },
      upper_tol: { text: '.01', value: '.01', units: '', sign: '+' },
      lower_tol: { text: '-.01', value: '.01', units: '', sign: '-' },
      text: '1.25±.01',
      type: 'nominalWithTolerances',
    },
    quantity: {
      text: '',
      units: '',
      value: '1',
    },
    text: '1.25±.01',
    words: '',
  };

  const mockNote = {
    input: 'This is a Note We Captured',
    notation: {
      type: 'unknown',
    },
    quantity: {
      text: '',
      units: '',
      value: '1',
    },
    text: 'This is a Note We Captured',
    words: 'This is a Note We Captured',
  };

  const types = {
    'db687ac1-7bf8-4ada-be2b-979f8921e1a0': {
      id: 'db687ac1-7bf8-4ada-be2b-979f8921e1a0',
      value: 'Dimension',
      default: true,
      meta: '',
      status: 'Active',
      listType: 'Type',
      index: 0,
      children: {
        'edc8ecda-5f88-4b81-871f-6c02f1c50afd': {
          id: 'edc8ecda-5f88-4b81-871f-6c02f1c50afd',
          value: 'Length',
          default: true,
          meta: { measurement: 'Length' },
          status: 'Active',
          listType: 'Type',
          index: 0,
          parentId: 'db687ac1-7bf8-4ada-be2b-979f8921e1a0',
        },
        'b2e1e0aa-0da4-4844-853a-f20971b013a4': {
          id: 'b2e1e0aa-0da4-4844-853a-f20971b013a4',
          value: 'Angle',
          default: true,
          meta: { measurement: 'Plane Angle' },
          status: 'Active',
          listType: 'Type',
          index: 1,
          parentId: 'db687ac1-7bf8-4ada-be2b-979f8921e1a0',
        },
        '49d45a6c-1073-4c6e-805f-8f4105641bf1': {
          id: '49d45a6c-1073-4c6e-805f-8f4105641bf1',
          value: 'Radius',
          default: true,
          meta: { measurement: 'Length' },
          status: 'Active',
          listType: 'Type',
          index: 2,
          parentId: 'db687ac1-7bf8-4ada-be2b-979f8921e1a0',
        },
        '4396f3c2-a915-4458-8cff-488b6d301ecd': {
          id: '4396f3c2-a915-4458-8cff-488b6d301ecd',
          value: 'Diameter',
          default: true,
          meta: { measurement: 'Length' },
          status: 'Active',
          listType: 'Type',
          index: 3,
          parentId: 'db687ac1-7bf8-4ada-be2b-979f8921e1a0',
        },
      },
    },
    'f3c4d8e0-fb79-4c72-a3f8-3f584ea075a7': {
      id: 'f3c4d8e0-fb79-4c72-a3f8-3f584ea075a7',
      value: 'Geometric Tolerance',
      default: true,
      meta: '',
      status: 'Active',
      listType: 'Type',
      index: 1,
      children: {
        'ea0e747f-9e70-4e2e-9a34-4491f6b63593': {
          id: 'ea0e747f-9e70-4e2e-9a34-4491f6b63593',
          value: 'Profile of a Surface',
          default: true,
          meta: { measurement: 'Length' },
          status: 'Active',
          listType: 'Type',
          index: 0,
          parentId: 'f3c4d8e0-fb79-4c72-a3f8-3f584ea075a7',
        },
        '19a7a9a1-76c5-45ef-8cf9-611ebe7cf207': {
          id: '19a7a9a1-76c5-45ef-8cf9-611ebe7cf207',
          value: 'Position',
          default: true,
          meta: { measurement: 'Length' },
          status: 'Active',
          listType: 'Type',
          index: 1,
          parentId: 'f3c4d8e0-fb79-4c72-a3f8-3f584ea075a7',
        },
        '5ce784e8-f614-4e6d-a79b-237b4dc751dd': {
          id: '5ce784e8-f614-4e6d-a79b-237b4dc751dd',
          value: 'Angularity',
          default: true,
          meta: { measurement: 'Plane Angle' },
          status: 'Active',
          listType: 'Type',
          index: 2,
          parentId: 'f3c4d8e0-fb79-4c72-a3f8-3f584ea075a7',
        },
      },
    },
    'fa3284ed-ad3e-43c9-b051-87bd2e95e927': {
      id: 'fa3284ed-ad3e-43c9-b051-87bd2e95e927',
      value: 'Note',
      default: true,
      meta: '',
      status: 'Active',
      listType: 'Type',
      index: 1,
      children: {
        '805fd539-8063-4158-8361-509e10a5d371': {
          id: '805fd539-8063-4158-8361-509e10a5d371',
          value: 'Note',
          default: true,
          meta: '',
          status: 'Active',
          listType: 'Type',
          index: 0,
          parentId: 'fa3284ed-ad3e-43c9-b051-87bd2e95e927',
        },
      },
    },
    'b8599a29-7560-48e2-807a-6f3c3c0f2366': {
      id: 'b8599a29-7560-48e2-807a-6f3c3c0f2366',
      value: 'Other',
      default: true,
      meta: '',
      status: 'Active',
      listType: 'Type',
      index: 3,
      children: {
        'd02aa7bc-b791-413d-be10-fe5359bb8385': {
          id: 'd02aa7bc-b791-413d-be10-fe5359bb8385',
          value: 'Electric Power',
          default: true,
          meta: { measurement: 'Power' },
          status: 'Active',
          listType: 'Type',
          index: 0,
          parentId: 'b8599a29-7560-48e2-807a-6f3c3c0f2366',
        },
      },
    },
  };
  const tolerances = {
    linear: {
      type: 'PrecisionTolerance',
      rangeTols: [
        { rangeLow: '1', rangeHigh: '9', plus: '1', minus: '-1' },
        { rangeLow: '0.1', rangeHigh: '0.9', plus: '0.1', minus: '-0.1' },
        { rangeLow: '0.01', rangeHigh: '0.09', plus: '0.01', minus: '-0.01' },
        { rangeLow: '0.001', rangeHigh: '0.009', plus: '0.001', minus: '-0.001' },
        { rangeLow: '0.0001', rangeHigh: '0.0009', plus: '0.0001', minus: '-0.0001' },
      ],
      precisionTols: [
        { level: 'X', plus: '1', minus: '-1' },
        { level: 'X.X', plus: '0.1', minus: '-0.1' },
        { level: 'X.XX', plus: '0.01', minus: '-0.01' },
        { level: 'X.XXX', plus: '0.001', minus: '-0.001' },
        { level: 'X.XXXX', plus: '0.0001', minus: '-0.0001' },
      ],
      unit: 'placeholder',
    },
    angular: {
      type: 'PrecisionTolerance',
      rangeTols: [
        { rangeLow: '1', rangeHigh: '9', plus: '1', minus: '-1' },
        { rangeLow: '0.1', rangeHigh: '0.9', plus: '0.1', minus: '-0.1' },
        { rangeLow: '0.01', rangeHigh: '0.09', plus: '0.01', minus: '-0.01' },
        { rangeLow: '0.001', rangeHigh: '0.009', plus: '0.001', minus: '-0.001' },
        { rangeLow: '0.0001', rangeHigh: '0.0009', plus: '0.0001', minus: '-0.0001' },
      ],
      precisionTols: [
        { level: 'X', plus: '1', minus: '-1' },
        { level: 'X.X', plus: '0.1', minus: '-0.1' },
        { level: 'X.XX', plus: '0.01', minus: '-0.01' },
        { level: 'X.XXX', plus: '0.001', minus: '-0.001' },
        { level: 'X.XXXX', plus: '0.0001', minus: '-0.0001' },
      ],
      unit: 'placeholder',
    },
  };

  beforeAll(() => {
    ocrService = new OcrService();
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  describe('sendToOcr', () => {
    it('should send image data to EDA for OCR extraction', async () => {
      const imageData = Buffer.from('fakeImageData');
      const expectedParams = { api_key: '6', thresh: '165', type: '0', name: 'capture.png' };

      axios.mockResolvedValue({ data: 'fakeOcrData' });

      const response = await ocrService.sendToOcr(imageData, false, 'dimension');
      const formData = axios.mock.calls[0][0].data;
      const calledParams = convertFormFieldsToObject(formData._streams);

      expect(calledParams).toEqual(expectedParams);
      expect(response).toEqual('fakeOcrData');
    });

    it('should handle isGdt flag properly', async () => {
      const imageData = Buffer.from('fakeImageData');
      const expectedParams = { api_key: '6', thresh: '165', type: '1', name: 'capture.png' };

      axios.mockResolvedValue({ data: 'fakeOcrData' });

      const response = await ocrService.sendToOcr(imageData, true, 'dimension');
      const formData = axios.mock.calls[0][0].data;
      const calledParams = convertFormFieldsToObject(formData._streams);

      expect(calledParams).toEqual(expectedParams);
      expect(response).toEqual('fakeOcrData');
    });

    it('should handle mode properly', async () => {
      const imageData = Buffer.from('fakeImageData');
      const expectedParams = { api_key: '6', thresh: '165', type: '2', name: 'capture.png' };

      axios.mockResolvedValue({ data: 'fakeOcrData' });

      const response = await ocrService.sendToOcr(imageData, false, 'text');
      const formData = axios.mock.calls[0][0].data;
      const calledParams = convertFormFieldsToObject(formData._streams);

      expect(calledParams).toEqual(expectedParams);
      expect(response).toEqual('fakeOcrData');
    });

    it('should handle errors', async () => {
      const imageData = Buffer.from('fakeImageData');
      const errorMessage = 'Network Error';
      axios.mockRejectedValue({ message: errorMessage });

      const result = await ocrService.sendToOcr(imageData, false, 'dimension');

      expect(result).toEqual({ error: { message: 'Network Error' } });
    });

    it('should handle a request timeout', async () => {
      const imageData = Buffer.from('fakeImageData');
      const errorMessage = 'Request Timeout';
      axios.mockRejectedValue({ message: errorMessage });

      const promise = ocrService.sendToOcr(imageData, false, 'dimension');

      const exceedTimeout = parseInt(config.edaTimeout) + 1;
      jest.advanceTimersByTime(exceedTimeout);

      const result = await promise;

      expect(result).toEqual({ error: { message: 'Request Timeout' } });
    });
  });

  describe('compareOCRParseResults', () => {
    it('should handle successful OCR parse results', async () => {
      const ocrRes = {
        tess: 'Sample tess result',
        ocr: 'Sample ocr result',
        error: null,
      };
      const textRes = {
        tess: 'Sample tess text result',
        ocr: 'Sample ocr text result',
        error: null,
      };

      const result = await ocrService.compareOCRParseResults(ocrRes, textRes);

      expect(result.parsed).toBeDefined();
      expect(result.captureError).toBeNull();
    });

    it('should handle OCR failure with no text results', async () => {
      const ocrRes = {
        tess: '',
        ocr: '',
        error: new Error('Empty Capture'),
      };

      const result = await ocrService.compareOCRParseResults(ocrRes);

      expect(result.parsed.notation).toEqual({ type: 'note', text: undefined });
      expect(result.captureError).toBeDefined();
    });

    it('should handle OCR failure with text results', async () => {
      const ocrRes = {
        tess: '.',
        ocr: '.',
        error: null,
      };
      const textRes = {
        tess: 'Sample tess text result',
        ocr: 'Sample ocr text result',
        error: null,
      };

      const result = await ocrService.compareOCRParseResults(ocrRes, textRes);

      expect(result.parsed).toEqual({ input: 'Sample ocr text result', notation: { text: 'Sample ocr text result', type: 'unknown' }, quantity: { text: '', units: '', value: '1' }, text: 'Sample ocr text result', words: 'Sample ocr text result' });
    });

    it('should handle network errors', async () => {
      const ocrRes = {
        tess: '',
        ocr: '',
        error: new Error('Network Error'),
      };

      const result = await ocrService.compareOCRParseResults(ocrRes);

      expect(result.parsed.error.message).toEqual('Network Error');
    });

    it('should handle unrecognized feature type', async () => {
      const ocrRes = {
        tess: '',
        ocr: '',
        error: new Error('Saved as a Note'),
      };

      const result = await ocrService.compareOCRParseResults(ocrRes);

      expect(result.parsed.notation).toEqual({ text: undefined, type: 'note' });
      expect(result.captureError).toBeDefined();
    });

    it('should handle unknown errors', async () => {
      const ocrRes = {
        tess: '',
        ocr: '',
        error: new Error('Unknown Error'),
      };

      const result = await ocrService.compareOCRParseResults(ocrRes);

      expect(result.parsed.error.message).toEqual('Unknown Error');
      expect(result.parsed.notation).toEqual({ text: undefined, type: 'note' });
      expect(result.captureError).toBeDefined();
    });
  });

  describe('shouldUpdateToleranceSource', () => {
    it('should return true if feature is not provided', () => {
      const change = { plusTol: 1, minusTol: 1, upperSpecLimit: 10, lowerSpecLimit: 0 };
      expect(ocrService.shouldUpdateToleranceSource(change)).toBe(true);
    });

    it('should return true if any property of change is different from feature', () => {
      const change = { plusTol: 1, minusTol: 1, upperSpecLimit: 10, lowerSpecLimit: 0 };
      const feature = { plusTol: 2, minusTol: 1, upperSpecLimit: 10, lowerSpecLimit: 0 };
      expect(ocrService.shouldUpdateToleranceSource(change, feature)).toBe(true);
    });

    it('should return false if all properties of change are the same as feature', () => {
      const change = { plusTol: 1, minusTol: 1, upperSpecLimit: 10, lowerSpecLimit: 0 };
      const feature = { plusTol: 1, minusTol: 1, upperSpecLimit: 10, lowerSpecLimit: 0 };
      expect(ocrService.shouldUpdateToleranceSource(change, feature)).toBe(false);
    });

    it('should return true if feature is not provided and change is empty', () => {
      const change = {};
      expect(ocrService.shouldUpdateToleranceSource(change)).toBe(true);
    });

    it('should return true if feature is empty and change has properties', () => {
      const change = { plusTol: 1, minusTol: 1, upperSpecLimit: 10, lowerSpecLimit: 0 };
      const feature = {};
      expect(ocrService.shouldUpdateToleranceSource(change, feature)).toBe(true);
    });
  });

  describe('parseFeatureFromFullSpec', () => {
    const service = new OcrService();
    const mockInterpretNotationText = jest.spyOn(service, 'interpretNotationText');
    it('should parse and assign a new full specification for an existing feature', async () => {
      mockInterpretNotationText.mockReturnValueOnce(mockDimension);

      let res = await service.parseFeatureFromFullSpec({
        assignedStyles,
        fullSpecification: dimension.fullSpecification,
        oldFeature: GDT,
        tolerances,
        types,
      });

      expect(res.feature).toMatchObject(dimension);

      mockInterpretNotationText.mockReturnValueOnce(mockGDT);
      res = await service.parseFeatureFromFullSpec({
        assignedStyles,
        fullSpecification: GDT.fullSpecification,
        oldFeature: dimension,
        tolerances,
        types,
      });

      expect(res.feature).toMatchObject(GDT);
    });

    it('should parse and assign OCR for a new capture', async () => {
      mockInterpretNotationText.mockReturnValueOnce(mockDimension);
      let res = await service.parseFeatureFromFullSpec({
        assignedStyles,
        ocrRes: { ocr: dimension.fullSpecification, tess: dimension.fullSpecification },
        tolerances,
        types,
      });

      expect(res.feature).toMatchObject(newDimension);
      mockInterpretNotationText.mockReturnValueOnce(mockGDT);
      res = await service.parseFeatureFromFullSpec({
        assignedStyles,
        ocrRes: { ocr: GDT.fullSpecification, tess: GDT.fullSpecification },
        tolerances,
        types,
      });

      expect(res.feature).toMatchObject(newGDT);
      mockInterpretNotationText.mockReturnValueOnce(mockNote);
      res = await service.parseFeatureFromFullSpec({
        assignedStyles,
        ocrRes: { ocr: note.fullSpecification, tess: note.fullSpecification },
        tolerances,
        types,
      });

      expect(res.feature).toMatchObject(newNote);
    });

    it('should return null if no OCR or full specification are passed to the parser', async () => {
      const res = await service.parseFeatureFromFullSpec({
        assignedStyles,
        tolerances,
        types,
      });
      expect(res).toBeNull();
    });
  });

  describe('createCharacteristicInput function', () => {
    it('should return a characteristic object with default values if no feature is provided', () => {
      const result = ocrService.createCharacteristicInput(undefined);
      expect(result).toEqual({
        drawingSheetIndex: 0,
        previousRevision: null,
        originalCharacteristic: null,
        status: 'Active',
        captureMethod: 'Manual',
        captureError: '',
        notationType: 'fa3284ed-ad3e-43c9-b051-87bd2e95e927',
        notationSubtype: '805fd539-8063-4158-8361-509e10a5d371',
        notationClass: 'Tolerance',
        fullSpecification: '',
        quantity: 1,
        nominal: '',
        upperSpecLimit: '',
        lowerSpecLimit: '',
        plusTol: '',
        minusTol: '',
        unit: '',
        gdtSymbol: undefined,
        gdtPrimaryToleranceZone: undefined,
        gdtSecondaryToleranceZone: undefined,
        gdtPrimaryDatum: undefined,
        gdtSecondaryDatum: undefined,
        gdtTertiaryDatum: undefined,
        inspectionMethod: '',
        criticality: '',
        notes: '',
        drawingRotation: 0,
        drawingScale: 1,
        boxLocationY: 0,
        boxLocationX: 0,
        boxWidth: 0,
        boxHeight: 0,
        boxRotation: 0,
        markerGroup: '',
        markerGroupShared: false,
        markerIndex: 0,
        markerSubIndex: 0,
        markerLabel: '',
        markerStyle: 'f12c5566-7612-48e9-9534-ef90b276ebaa',
        markerLocationX: 0,
        markerLocationY: 0,
        markerFontSize: 0,
        markerSize: 0,
        markerRotation: 0,
        gridCoordinates: '',
        balloonGridCoordinates: '',
        connectionPointGridCoordinates: '',
        toleranceSource: 'Default_Tolerance',
        operation: '',
        part: undefined,
        drawing: undefined,
        drawingSheet: undefined,
        verified: false,
        fullSpecificationFromOCR: '',
        confidence: -1,
        connectionPointLocationX: 0,
        connectionPointLocationY: 0,
        connectionPointIsFloating: false,
        displayLeaderLine: true,
        leaderLineDistance: 0,
      });
    });

    it('should return a characteristic object with provided feature values merged with default values', () => {
      const feature = {
        drawingSheetIndex: 'A',
        previousRevision: '1',
        originalCharacteristic: 'original',
        status: 'status',
        captureMethod: 'method',
      };
      const result = ocrService.createCharacteristicInput(feature);

      expect(result).toEqual({
        drawingSheetIndex: 'A',
        previousRevision: '1',
        originalCharacteristic: 'original',
        status: 'status',
        captureMethod: 'method',
        captureError: '',
        notationType: 'fa3284ed-ad3e-43c9-b051-87bd2e95e927',
        notationSubtype: '805fd539-8063-4158-8361-509e10a5d371',
        notationClass: 'Tolerance',
        fullSpecification: '',
        quantity: 1,
        nominal: '',
        upperSpecLimit: '',
        lowerSpecLimit: '',
        plusTol: '',
        minusTol: '',
        unit: '',
        gdtSymbol: undefined,
        gdtPrimaryToleranceZone: undefined,
        gdtSecondaryToleranceZone: undefined,
        gdtPrimaryDatum: undefined,
        gdtSecondaryDatum: undefined,
        gdtTertiaryDatum: undefined,
        inspectionMethod: '',
        criticality: '',
        notes: '',
        drawingRotation: 0,
        drawingScale: 1,
        boxLocationY: 0,
        boxLocationX: 0,
        boxWidth: 0,
        boxHeight: 0,
        boxRotation: 0,
        markerGroup: '',
        markerGroupShared: false,
        markerIndex: 0,
        markerSubIndex: 0,
        markerLabel: '',
        markerStyle: 'f12c5566-7612-48e9-9534-ef90b276ebaa',
        markerLocationX: 0,
        markerLocationY: 0,
        markerFontSize: 0,
        markerSize: 0,
        markerRotation: 0,
        gridCoordinates: '',
        balloonGridCoordinates: '',
        connectionPointGridCoordinates: '',
        toleranceSource: 'Default_Tolerance',
        operation: '',
        part: undefined,
        drawing: undefined,
        drawingSheet: undefined,
        verified: false,
        fullSpecificationFromOCR: '',
        confidence: -1,
        connectionPointLocationX: 0,
        connectionPointLocationY: 0,
        connectionPointIsFloating: false,
        displayLeaderLine: true,
        leaderLineDistance: 0,
      });
    });
  });
});
