const config = require('../../config')();

const USE_MOCKS = config.useMocks;

if (USE_MOCKS) {
  jest.doMock('../database/models');
  jest.doMock('../database/repositories/drawingSheetRepository');
  jest.doMock('../database/repositories/partRepository');
  jest.doMock('../database/repositories/notificationRepository');
}

const models = require('../database/models');
const DrawingSheetRepository = require('../database/repositories/drawingSheetRepository');
const PartRepository = require('../database/repositories/partRepository');
const NotificationRepository = require('../database/repositories/notificationRepository');
const SequelizeRepository = require('../database/repositories/sequelizeRepository');
const DrawingSheetService = require('./drawingSheetService');
const PartService = require('./partService');
const { data } = require('../__fixtures__').drawingSheet;
const { data: partData } = require('../__fixtures__').part;
const { data: notificationData } = require('../__fixtures__').notification;
const NotificationService = require('./notificationService');

describe('DrawingSheetService', () => {
  const context = { currentUser: { accountId: config.testAccountId } };

  const drawingSheetMock = {
    createMock: jest.fn().mockReturnValue(data[0]),
    incrementInAccountMock: jest.fn().mockReturnValue({
      ...data[0], //
      foundCaptureCount: 1,
      acceptedCaptureCount: 1,
      rejectedCaptureCount: 0,
      acceptedTimeoutCaptureCount: 0,
      reviewedCaptureCount: 1,
      reviewedTimeoutCaptureCount: 0,
      autoballoonProcessingComplete: true,
      autoballoonReviewComplete: true,
    }),
    updateMock: jest.fn().mockReturnValue(data[0]),
    findAndCountAllMock: jest
      .fn()
      .mockReturnValueOnce({
        rows: [data[0]],
        count: 1,
      })
      .mockReturnValueOnce({ rows: [], count: 0 }),
    findByIdMock: jest.fn().mockReturnValue(data[0]),
    destroyMock: jest.fn(),

    statusUpdateInAccountMock: jest.fn().mockReturnValue({
      ...data[0], //
      foundCaptureCount: 1,
      acceptedCaptureCount: 1,
      rejectedCaptureCount: 0,
      acceptedTimeoutCaptureCount: 0,
      reviewedCaptureCount: 1,
      reviewedTimeoutCaptureCount: 0,
      autoballoonProcessingComplete: true,
      autoballoonReviewComplete: true,
      autoMarkupStatus: 'Error',
    }),
  };

  const partMock = {
    createMock: jest.fn().mockReturnValue(partData[0]),
    createInAccountMock: jest.fn().mockReturnValue(partData[0]),
    updateMock: jest.fn().mockReturnValue(partData[0]),
    findAndCountAllMock: jest.fn().mockReturnValue({
      rows: [partData[0]],
      count: 1,
    }),
    findByIdMock: jest.fn().mockReturnValue(partData[0]),
    destroyMock: jest.fn(),
    findByIdInAccountMock: jest.fn().mockReturnValue(partData[0]),
  };

  const notificationMock = {
    createMock: jest.fn().mockReturnValue(notificationData[0]),
    createInAccountMock: jest.fn().mockReturnValue(notificationData[0]),
    updateMock: jest.fn().mockReturnValue(notificationData[0]),
    findAndCountAllMock: jest.fn().mockReturnValueOnce({
      rows: [notificationData[0]],
      count: 1,
    }),
    findByIdMock: jest.fn().mockReturnValue(notificationData[0]),
    destroyMock: jest.fn(),
  };

  beforeEach(async () => {
    if (USE_MOCKS) {
      models.getTenant = () =>
        Promise.resolve({
          sequelize: {
            transaction: jest.fn().mockReturnValue({
              rollback: jest.fn(),
              commit: jest.fn(),
            }),
          },
        });

      models.sequelizeAdmin = {
        query: jest.fn().mockReturnValue(Promise.resolve({})),
      };

      DrawingSheetRepository.mockImplementation(() => {
        return {
          create: drawingSheetMock.createMock,
          update: drawingSheetMock.updateMock,
          findAndCountAll: drawingSheetMock.findAndCountAllMock,
          findById: drawingSheetMock.findByIdMock,
          destroy: drawingSheetMock.destroyMock,
          incrementInAccount: drawingSheetMock.incrementInAccountMock,
        };
      });

      PartRepository.mockImplementation(() => {
        return {
          create: partMock.createMock,
          createInAccount: partMock.createInAccountMock,
          update: partMock.updateMock,
          findAndCountAll: partMock.findAndCountAllMock,
          findById: partMock.findByIdMock,
          destroy: partMock.destroyMock,
          findByIdInAccount: partMock.findByIdInAccountMock,
        };
      });
      NotificationRepository.mockImplementation(() => {
        return {
          create: notificationMock.createMock,
          createInAccount: notificationMock.createInAccountMock,
          update: notificationMock.updateMock,
          findAndCountAll: notificationMock.findAndCountAllMock,
          findById: notificationMock.findByIdMock,
          destroy: notificationMock.destroyMock,
        };
      });

      SequelizeRepository.commitTransaction = jest.fn();
    } else {
      await Promise.all([
        SequelizeRepository.cleanDatabase(config.testAccountId), //
      ]);
    }
  });

  afterAll(async () => {
    if (!USE_MOCKS) {
      await Promise.all([
        SequelizeRepository.closeConnections(config.testAccountId), //
        SequelizeRepository.closeConnections(),
      ]);
    }
  });

  describe('incrementInAccount()', () => {
    it('Should create a notification when drawingSheet data shows autoballooning is complete', async () => {
      await new PartService(context).create(partData[0]);
      const drawingSheet = await new DrawingSheetService(context).create(data[0]);
      drawingSheet.foundCaptureCount = 1;
      drawingSheet.acceptedCaptureCount = 1;
      drawingSheet.reviewedCaptureCount = 1;

      drawingSheet.accountId = config.testAccountId;
      drawingSheet.siteId = '31ef1d56-1b86-4ffa-b886-507ed8f021c5';
      drawingSheet.drawing = drawingSheet.drawingId;
      drawingSheet.part = drawingSheet.partId;

      const updatedDrawingSheet = await new DrawingSheetService(context).incrementInAccount(drawingSheet);
      if (USE_MOCKS) {
        expect(drawingSheetMock.incrementInAccountMock).toBeCalledTimes(1);
        expect(notificationMock.createInAccountMock).toBeCalledTimes(1);
      } else {
        const notifications = await new NotificationService(context).count();
        expect(notifications).toBe(1);
      }
      expect(updatedDrawingSheet.foundCaptureCount).toBe(1);
      expect(updatedDrawingSheet.acceptedCaptureCount).toBe(1);
      expect(updatedDrawingSheet.reviewedCaptureCount).toBe(1);
    });
  });

  describe('statusUpdateInAccount()', () => {
    it('Should create a notification when drawingSheet have run through lambda find-image-captures with an error', async () => {
      await new PartService(context).create(partData[0]);
      const drawingSheet = await new DrawingSheetService(context).create(data[0]);
      drawingSheet.autoMarkupStatus = 'Error';

      drawingSheet.accountId = config.testAccountId;
      drawingSheet.siteId = '31ef1d56-1b86-4ffa-b886-507ed8f021c5';
      drawingSheet.drawing = drawingSheet.drawingId;
      drawingSheet.part = drawingSheet.partId;

      const updatedDrawingSheet = await new DrawingSheetService(context).statusUpdateInAccount(drawingSheet);
      if (USE_MOCKS) {
        expect(drawingSheetMock.statusUpdateInAccountMock).toBeCalledTimes(1);
        expect(notificationMock.createInAccountMock).toBeCalledTimes(1);
      } else {
        const notifications = await new NotificationService(context).count();
        expect(notifications).toBe(1);
      }
      expect(updatedDrawingSheet.autoMarkupStatus).toBe('Error');
    });
  });
});
