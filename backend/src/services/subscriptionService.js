const SubscriptionRepository = require('../database/repositories/subscriptionRepository');
const ValidationError = require('../errors/validationError');
const SequelizeRepository = require('../database/repositories/sequelizeRepository');
const { log } = require('../logger');

const DATA_OUT_OF_SYNC_MESSAGE = 'Billing/Subscription data is out of sync between the billing provider and IXC.';

/**
 * Handles Subscription operations
 */
module.exports = class SubscriptionService {
  constructor(context) {
    const { currentUser, language, billingClient, subscriptionWorker, logger } = context;
    this.repository = new SubscriptionRepository();
    this.currentUser = currentUser;
    this.language = language;
    this.billingClient = billingClient;
    this.subscriptionWorker = subscriptionWorker;
    this.logger = logger;
  }

  /**
   * Creates a Subscription.
   *
   * @param {*} data
   */
  async create(data) {
    const subscription = await this.billingClient.createSubscription(data);

    return this.createSubscription(subscription);
  }

  async createSubscription(subscription) {
    const transaction = await SequelizeRepository.createTransaction();

    try {
      const record = await this.repository.create(subscription, {
        transaction,
        currentUser: this.currentUser,
      });

      await SequelizeRepository.commitTransaction(transaction);

      return record;
    } catch (error) {
      // An error means that IXC and the subscription provider are now out of
      // sync -- a new subscription exists with the provider but not in the IXC DB.
      // This needs to be remedied to get the systems back in sync.
      this.logger.error(`${DATA_OUT_OF_SYNC_MESSAGE} Error: ${error}`);

      await SequelizeRepository.rollbackTransaction(transaction);
      throw error;
    }
  }

  /**
   * Updates a Subscription.
   *
   * @param {*} id
   * @param {*} data
   * @param {*} type - determines how to interact with billingClient (update, cancel, reactivate, etc.)
   */
  async update(id, data, type, components, billingToken, componentHandle) {
    let chargifySuccess = false;
    let dataToChange = { ...data };

    switch (type) {
      case 'Delayed_Cancel':
        // converts an 'Active' subscription to 'Active (Pending Cancellation)'
        chargifySuccess = await this.billingClient.delayedCancelSubscription(data.paymentSystemId);
        break;
      case 'Reactivate_Pending':
        // converts an 'Active (Pending Cancellation)' subscription to 'Active'
        chargifySuccess = await this.billingClient.removeDelayedCancel(data.paymentSystemId);
        break;
      case 'Immediate_Cancel':
        // immediately cancels a Subscription
        chargifySuccess = await this.billingClient.immediatelyCancelSubscription(data.paymentSystemId);
        break;
      case 'Add_Payment':
        // Add a credit card payment to the account
        chargifySuccess = await this.billingClient.addPaymentMethod(data);
        // dataToChange.credit_card = chargifySuccess.update.credit_card;
        dataToChange = chargifySuccess.update;
        // Void Previously Generated Invoices
        if ((dataToChange.state?.toLowerCase() === 'trialing' || dataToChange.state?.toLowerCase() === 'trial_ended') && dataToChange.terms_accepted_on) {
          log.debug('Voiding Previously Generated Invoices for Remittance Subscription');
          await this.subscriptionWorker.voidAllOpenInvoices({ subscriptionId: dataToChange.paymentSystemId });
        }
        break;
      case 'Remove_Payment':
        // remove a credit card payment from the account and switch subscription to remittance
        chargifySuccess = await this.billingClient.removePaymentMethod(data);
        dataToChange = chargifySuccess.update;
        break;
      // DEAL creation moved to registerSubscription.
      // case 'Register_Deal':
      //   chargifySuccess = await this.subscriptionWorker.registerSalesAcceptedLead(data);
      //   break;
      case 'Update_Subscription':
        chargifySuccess = await this.subscriptionWorker.updateSubscription({
          subscriptionId: data.paymentSystemId,
          components,
          componentHandle,
          billingPeriod: data.billingPeriod,
          providerProductId: data.providerProductId,
        });
        dataToChange = {
          //
          ...chargifySuccess.update,
          billingId: data.billingId,
          billingPeriod: data.billingPeriod,
          pendingComponents: data?.pendingComponents,
          pendingTier: data?.pendingTier,
          pendingBillingPeriod: data?.pendingBillingPeriod,
        };
        break;
      case 'Trial_To_Subscription':
        chargifySuccess = await this.subscriptionWorker.convertTrialToSubscription({
          subscriptionId: data.paymentSystemId,
          components: components || [],
          componentHandle,
          interval: data.billingPeriod,
          billingToken,
          data,
        });
        dataToChange = {
          ...chargifySuccess.bsSubscription,
          billingPeriod: data.billingPeriod,
          billingId: this.currentUser.id || data.billingId,
        };
        break;
      case 'Update_Usage':
        chargifySuccess = true; // no change to chargify
        break;
      default:
        break;
    }

    if (!chargifySuccess) throw new Error(`Chargify ${type} failed.`);

    return this.updateSubscription(id, dataToChange);
  }

  async updateSubscription(id, dataToChange) {
    const transaction = await SequelizeRepository.createTransaction();

    try {
      const record = await this.repository.update(id, dataToChange, {
        transaction,
        currentUser: this.currentUser,
      });

      await SequelizeRepository.commitTransaction(transaction);

      return record;
    } catch (error) {
      this.logger.error(`${DATA_OUT_OF_SYNC_MESSAGE} Error: ${error}`);

      await SequelizeRepository.rollbackTransaction(transaction);
      throw error;
    }
  }

  /**
   * Destroy all Subscriptions with those ids.
   *
   * @param {*} ids
   */
  async destroyAll(ids) {
    const transaction = await SequelizeRepository.createTransaction();

    try {
      for (const id of ids) {
        await this.repository.destroy(id, {
          transaction,
          currentUser: this.currentUser,
        });
      }

      await SequelizeRepository.commitTransaction(transaction);
    } catch (error) {
      // NOTE: An error means that IXC and the subscription provider are now out of
      // sync -- subscriptions were deleted with the provider but not in the IXC DB.
      // This needs to be remedied to get the systems back in sync.

      await SequelizeRepository.rollbackTransaction(transaction);
      throw error;
    }
  }

  /**
   * Finds the Subscription by Id.
   *
   * @param {*} id
   */
  async findById(id, context = {}) {
    return this.repository.findById(id, context);
  }

  /**
   * Finds the Subscription by Account Id.
   *
   * @param {*} accountId
   */
  async findByAccountId(accountId, context = {}) {
    return this.repository.findByAccountId(accountId, context);
  }

  /**
   * Finds Subscriptions for Autocomplete.
   *
   * @param {*} search
   * @param {*} limit
   */
  async findAllAutocomplete(search, limit) {
    return this.repository.findAllAutocomplete(search, limit);
  }

  /**
   * Finds Subscriptions based on the query.
   *
   * @param {*} args
   */
  async findAndCountAll(args) {
    console.log('findAndCountAll.args', args);
    return this.repository.findAndCountAll(args);
  }

  /**
   * Imports a list of Subscriptions.
   *
   * @param {*} data
   * @param {*} importHash
   */
  async import(data, importHash) {
    if (!importHash) {
      throw new ValidationError(this.language, 'importer.errors.importHashRequired');
    }

    if (await this._isImportHashExistent(importHash)) {
      throw new ValidationError(this.language, 'importer.errors.importHashExistent');
    }

    const dataToCreate = {
      ...data,
      importHash,
    };

    return this.create(dataToCreate);
  }

  /**
   * See comment block for chargifyBillingAdapter::getInitData() in backend/src/clients/serviceAdapters/chargifyBillingAdapter.js
   */
  async getBillingInitData(accountId) {
    // return this.billingClient.getInitData(accountId);
    const data = await this.subscriptionWorker.getBillingInitData(accountId);
    if (data.subscription) {
      if (data.subscription?.ixc_subscription_id) {
        const DbSubscription = await this.updateSubscription(data.subscription.ixc_subscription_id, data.subscription);
        data.subscription = DbSubscription;
      } else {
        // Chargify is missing the DB entry ID
        const exists = await this.repository.findOne({ where: { paymentSystemId: data.subscription.id } }, { currentUser: this.currentUser });
        let DbSubscription;
        if (exists) {
          DbSubscription = await this.updateSubscription(exists.id, data.subscription);
        } else {
          // The DB entry doesn't exist, create it
          DbSubscription = await this.createSubscription(data.subscription);
        }
        // Add the DB entry's ID to the subscription model
        await this.billingClient.updateSubscriptionCustomFields({
          subscriptionId: data.subscription.id,
          changes: [{ name: 'ixc_subscription_id', value: DbSubscription.id }],
        });
        data.subscription = DbSubscription;
      }
    }
    return data;
  }

  /**
   * See comment block for BillingClient::getInvoices().
   */
  getInvoices(customer) {
    return this.billingClient.getInvoices(customer);
  }

  /**
   * See comment block for BillingClient::downloadInvoice().
   */
  downloadInvoice(id) {
    return this.billingClient.downloadInvoice(id);
  }

  /**
   * See comment block for BillingClient::getActiveSubscription().
   */
  getActiveSubscription(id) {
    return this.billingClient.getActiveSubscription(id);
  }

  /**
   * Checks if the import hash already exists.
   * Every item imported has a unique hash.
   *
   * @param {*} importHash
   */
  async _isImportHashExistent(importHash) {
    const count = await this.repository.count({
      importHash,
    });

    return count > 0;
  }
};
