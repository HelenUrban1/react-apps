const ClassifierService = require('./classifierService');
const service = new ClassifierService();

const dimension = {
  id: 'e0a01769-c8ed-4090-bd24-9c1b2241f123',
  drawingSheetIndex: 1,
  captureMethod: 'Automated',
  status: 'Active',
  verified: false,
  notationType: 'placeholder', // Dimension
  notationSubtype: 'placeholder', // Curvilinear
  notationClass: 'Tolerance',
  fullSpecification: '1.25 +/- 0.1',
  quantity: 3,
  nominal: '1.25',
  upperSpecLimit: '1.26',
  lowerSpecLimit: '1.24',
  plusTol: '0.1',
  minusTol: '0.1',
  unit: 'placeholder',
  toleranceSource: 'Document_Defined',
  inspectionMethod: 'placeholder', // Caliper
  criticality: null,
  notes: '',
  drawingRotation: 0.0,
  drawingScale: 1.0,
  boxLocationY: 510.7251086456913,
  boxLocationX: 202.30217210136476,
  boxWidth: 33.53615354307212,
  boxHeight: 15.562144712486088,
  boxRotation: 0.0,
  markerGroup: '1',
  markerGroupShared: false,
  markerIndex: 0,
  markerSubIndex: null,
  markerLabel: '1',
  markerStyle: 'aad19683-337a-4039-a910-98b0bdd5ee57',
  markerLocationX: 45,
  markerLocationY: 180,
  markerFontSize: 18,
  markerSize: 36,
  gridCoordinates: null,
  balloonGridCoordinates: null,
  connectionPointGridCoordinates: null,
  createdAt: 'placeholder',
  updatedAt: 'placeholder',
  partId: 'd6c5bc49-9fb2-4fbb-8a61-77bc8d28036f', // belongsTo
  drawingId: 'c200ce70-72b8-44f7-8d3f-4b93f6d1b491', // belongsTo
  drawingSheetId: 'd6c5bc49-9fb2-4fbb-8a61-77bc8d380352',
};

const note = {
  id: 'e0a01769-c8ed-40c0-bd24-9c1b2241f135',
  drawingSheetIndex: 2,
  captureMethod: 'Automated',
  status: 'Active',
  verified: false,
  notationType: 'placeholder', // Note
  notationSubtype: 'placeholder', // Note
  notationClass: 'Tolerance',
  fullSpecification: 'This is a Note We Captured',
  quantity: 1,
  nominal: '',
  upperSpecLimit: null,
  lowerSpecLimit: null,
  plusTol: null,
  minusTol: null,
  unit: 'placeholder',
  toleranceSource: 'Document_Defined',
  inspectionMethod: 'placeholder', // Caliper
  criticality: null,
  notes: '',
  drawingRotation: 0.0,
  drawingScale: 1.0,
  boxLocationY: 642,
  boxLocationX: 91,
  boxWidth: 357,
  boxHeight: 76,
  boxRotation: 0.0,
  markerGroup: '5',
  markerGroupShared: false,
  markerIndex: 5,
  markerSubIndex: null,
  markerLabel: '5',
  markerStyle: 'f1386f09-9c03-4c59-ada9-acb6a16bfbab',
  markerLocationX: 222,
  markerLocationY: -180,
  markerFontSize: 18,
  markerSize: 36,
  gridCoordinates: null,
  balloonGridCoordinates: null,
  connectionPointGridCoordinates: null,
  createdAt: 'placeholder',
  updatedAt: 'placeholder',
  partId: 'd6c5bc49-9fb2-4fbb-8a61-77bc8d28036f', // belongsTo
  drawingId: 'c200ce70-72b8-44f7-8d3f-4b93f6d1b491', // belongsTo
  drawingSheetId: 'd6c5bc49-9fb2-4fbb-8a61-77bc8d380352',
};

const GDT = {
  id: 'e36abada-2a27-40e0-9dd2-e58fea53bf02',
  captureMethod: 'Automated',
  toleranceSource: 'Document_Defined',
  status: 'Active',
  verified: false,
  drawingSheetIndex: 1,
  notationType: 'placeholder', // Geometric Tolerance
  notationSubtype: 'placeholder', // Angularity
  notationClass: 'Tolerance',
  fullSpecification: '|<|1.5|A|B|C|',
  quantity: 1,
  nominal: '1.5',
  plusTol: '0.1',
  minusTol: '0.1',
  upperSpecLimit: '1.5',
  lowerSpecLimit: '',
  gdtSymbol: 'Angularity',
  gdtPrimaryToleranceZone: '1.5',
  gdtSecondaryToleranceZone: 'Zone2',
  gdtPrimaryDatum: 'A',
  gdtSecondaryDatum: 'B',
  gdtTertiaryDatum: 'C',
  unit: 'placeholder',
  drawingRotation: 0,
  drawingScale: 1,
  boxLocationY: 120,
  boxLocationX: 220,
  boxWidth: 250,
  boxHeight: 100,
  boxRotation: 0,
  markerIndex: 2,
  markerLabel: '3',
  markerGroup: '3',
  markerStyle: 'aad19683-337a-4039-a910-98b0bdd5ee57',
  markerLocationX: 190,
  markerLocationY: 100.5,
  markerFontSize: 18,
  markerSize: 36,
  partId: 'c48e082a-ed09-4885-ae15-e45c4f58b628',
  drawingId: 'bf76c82f-4c45-4bf7-be51-34c401c49d85',
  drawingSheetId: '4f53a001-9e39-4c7a-aaf0-d769d3c9a425',
  createdAt: 'placeholder',
  updatedAt: 'placeholder',
};

const types = {
  'db687ac1-7bf8-4ada-be2b-979f8921e1a0': {
    id: 'db687ac1-7bf8-4ada-be2b-979f8921e1a0',
    value: 'Dimension',
    default: true,
    meta: '',
    status: 'Active',
    listType: 'Type',
    index: 0,
    children: {
      'edc8ecda-5f88-4b81-871f-6c02f1c50afd': {
        id: 'edc8ecda-5f88-4b81-871f-6c02f1c50afd',
        value: 'Length',
        default: true,
        meta: { measurement: 'Length' },
        status: 'Active',
        listType: 'Type',
        index: 0,
        parentId: 'db687ac1-7bf8-4ada-be2b-979f8921e1a0',
      },
      'b2e1e0aa-0da4-4844-853a-f20971b013a4': {
        id: 'b2e1e0aa-0da4-4844-853a-f20971b013a4',
        value: 'Angle',
        default: true,
        meta: { measurement: 'Plane Angle' },
        status: 'Active',
        listType: 'Type',
        index: 1,
        parentId: 'db687ac1-7bf8-4ada-be2b-979f8921e1a0',
      },
      '49d45a6c-1073-4c6e-805f-8f4105641bf1': {
        id: '49d45a6c-1073-4c6e-805f-8f4105641bf1',
        value: 'Radius',
        default: true,
        meta: { measurement: 'Length' },
        status: 'Active',
        listType: 'Type',
        index: 2,
        parentId: 'db687ac1-7bf8-4ada-be2b-979f8921e1a0',
      },
      '4396f3c2-a915-4458-8cff-488b6d301ecd': {
        id: '4396f3c2-a915-4458-8cff-488b6d301ecd',
        value: 'Diameter',
        default: true,
        meta: { measurement: 'Length' },
        status: 'Active',
        listType: 'Type',
        index: 3,
        parentId: 'db687ac1-7bf8-4ada-be2b-979f8921e1a0',
      },
    },
  },
  'f3c4d8e0-fb79-4c72-a3f8-3f584ea075a7': {
    id: 'f3c4d8e0-fb79-4c72-a3f8-3f584ea075a7',
    value: 'Geometric Tolerance',
    default: true,
    meta: '',
    status: 'Active',
    listType: 'Type',
    index: 1,
    children: {
      'ea0e747f-9e70-4e2e-9a34-4491f6b63593': {
        id: 'ea0e747f-9e70-4e2e-9a34-4491f6b63593',
        value: 'Profile of a Surface',
        default: true,
        meta: { measurement: 'Length' },
        status: 'Active',
        listType: 'Type',
        index: 0,
        parentId: 'f3c4d8e0-fb79-4c72-a3f8-3f584ea075a7',
      },
      '19a7a9a1-76c5-45ef-8cf9-611ebe7cf207': {
        id: '19a7a9a1-76c5-45ef-8cf9-611ebe7cf207',
        value: 'Position',
        default: true,
        meta: { measurement: 'Length' },
        status: 'Active',
        listType: 'Type',
        index: 1,
        parentId: 'f3c4d8e0-fb79-4c72-a3f8-3f584ea075a7',
      },
      '5ce784e8-f614-4e6d-a79b-237b4dc751dd': {
        id: '5ce784e8-f614-4e6d-a79b-237b4dc751dd',
        value: 'Angularity',
        default: true,
        meta: { measurement: 'Plane Angle' },
        status: 'Active',
        listType: 'Type',
        index: 2,
        parentId: 'f3c4d8e0-fb79-4c72-a3f8-3f584ea075a7',
      },
    },
  },
  'fa3284ed-ad3e-43c9-b051-87bd2e95e927': {
    id: 'fa3284ed-ad3e-43c9-b051-87bd2e95e927',
    value: 'Note',
    default: true,
    meta: '',
    status: 'Active',
    listType: 'Type',
    index: 1,
    children: {
      '805fd539-8063-4158-8361-509e10a5d371': {
        id: '805fd539-8063-4158-8361-509e10a5d371',
        value: 'Note',
        default: true,
        meta: '',
        status: 'Active',
        listType: 'Type',
        index: 0,
        parentId: 'fa3284ed-ad3e-43c9-b051-87bd2e95e927',
      },
    },
  },
};

describe('Classifier', () => {
  describe('Controller', () => {
    it('should parse a Note given the notation Classifier return value', async () => {
      const interpreted = {
        ocrRes: 'THIS IS A NOTE',
        input: 'THIS IS A NOTE',
        parsed: {
          input: 'THIS IS A NOTE',
        },
        notation: {
          type: 'note',
          text: 'THIS IS A NOTE',
          input: 'THIS IS A NOTE',
        },
      };
      const feature = await service.parseInterpretedNotation({
        interpreted,
        types,
      });
      expect(feature).toMatchObject({
        notationType: 'fa3284ed-ad3e-43c9-b051-87bd2e95e927', // Note
        notationSubtype: '805fd539-8063-4158-8361-509e10a5d371', // Note
        fullSpecification: 'THIS IS A NOTE',
        nominal: 'THIS IS A NOTE',
        unit: '',
      });
    });

    it('should parse a GDT given the notation Classifier return value', async () => {
      const interpreted = {
        input: '|⌖|⌀.01Ⓢ|A|B|C|',
        text: '|⌖|⌀.01Ⓢ|A|B|C|',
        words: '',
        quantity: {
          text: '',
          units: '',
          value: '1',
        },
        notation: {
          datums: [{ id: 'A' }, { id: 'B' }, { id: 'C' }],
          subtype: 'Position',
          text: '|⌖|⌀.01Ⓢ|A|B|C|',
          type: 'gdtControlFrame',
          zones: {
            primary: {
              modifier: 'Ⓢ',
              postModifierValue: '',
              prefix: '⌀',
              text: '⌀.01Ⓢ',
              value: '.01',
            },
          },
        },
      };
      const feature = await service.parseInterpretedNotation({
        interpreted,
        types,
      });
      expect(feature).toMatchObject({
        notationType: 'f3c4d8e0-fb79-4c72-a3f8-3f584ea075a7', // Geometric Tolerance
        notationSubtype: '19a7a9a1-76c5-45ef-8cf9-611ebe7cf207', // position
        fullSpecification: '', // framed
        gdtSymbol: '',
        gdtPrimaryToleranceZone: '⌀.01Ⓢ',
        gdtPrimaryDatum: 'A',
        gdtSecondaryDatum: 'B',
        gdtTertiaryDatum: 'C',
      });
    });

    it('should parse a Dimension given the notation Classifier return value', async () => {
      const interpreted = {
        input: '2.99',
        text: '2.99',
        words: '',
        quantity: {
          text: '',
          units: '',
          value: '1',
        },
        notation: {
          text: '2.99',
          type: 'length',
          nominal: {
            text: '2.99',
            units: '',
            value: '2.99',
          },
        },
      };
      const feature = await service.parseInterpretedNotation({
        interpreted,
        types,
      });
      expect(feature).toMatchObject({
        notationType: 'db687ac1-7bf8-4ada-be2b-979f8921e1a0', // Dimension
        notationSubtype: 'edc8ecda-5f88-4b81-871f-6c02f1c50afd', // length
        fullSpecification: '2.99',
        nominal: '2.99',
      });
    });

    it('should parse a Special Dimension given the notation Classifier return value', async () => {
      const interpreted = {
        input: '2X ⌀.12 THRU',
        text: '2X .12 THRU',
        words: '',
        quantity: {
          text: '2X',
          units: 'X',
          value: '2',
        },
        notation: {
          text: '.12 THRU',
          type: 'hole',
          symbol: '',
          modifier: '',
          width: {
            text: '.12',
            units: '',
            value: '.12',
          },
          depth: {
            text: 'THRU',
            symbol: '',
            word: 'THRU',
            type: 'depth',
            amount: {
              text: '',
              value: '',
            },
          },
        },
      };
      const feature = await service.parseInterpretedNotation({
        interpreted,
        types,
      });
      expect(feature).toMatchObject({
        notationType: 'db687ac1-7bf8-4ada-be2b-979f8921e1a0', // Dimension
        notationSubtype: '4396f3c2-a915-4458-8cff-488b6d301ecd', // diameter
        fullSpecification: '2X ⌀.12 THRU',
        nominal: '.12',
      });
    });

    it('should store unknown features with framed characters as notes', async () => {
      const interpreted = {
        error: {
          errors: [
            {
              name: 'NoViableAltException',
              message:
                "Expecting: one of these possible Token sequences:↵  1. [CatGDTSymbol]↵  2. [VPipe, CatGDTSymbol]↵  3. [ThreadMetric]↵  4. [Multiple]↵  5. [ValueWithOptionalUnits, By, CatNumericValue]↵  6. [Integer, By, CatNumericValue]↵  7. [CatNumericValue, By, ValueWithOptionalUnits]↵  8. [CatNumericValue, By, Integer]↵  9. [ValueWithOptionalUnits, By, CatNumericValue, CatWhiteSpace]↵  10. [ValueWithOptionalUnits, By, CatNumericValue, Tolerance]↵  11. [ValueWithOptionalUnits, By, CatNumericValue, Plus]↵  12. [ValueWithOptionalUnits, By, CatNumericValue, CatNumericValue]↵  13. [ValueWithOptionalUnits, By, CatNumericValue, ForwardSlash]↵  14. [ValueWithOptionalUnits, By, CatNumericValue, Range]↵  15. [ValueWithOptionalUnits, By, CatNumericValue, Minus]↵  16. [Integer, By, CatNumericValue, CatWhiteSpace]↵  17. [Integer, By, CatNumericValue, Tolerance]↵  18. [Integer, By, CatNumericValue, Plus]↵  19. [Integer, By, CatNumericValue, CatNumericValue]↵  20. [Integer, By, CatNumericValue, ForwardSlash]↵  21. [Integer, By, CatNumericValue, Range]↵  22. [Integer, By, CatNumericValue, Minus]↵  23. [CatNumericValue, CatWhiteSpace, Tolerance, CatWhiteSpace]↵  24. [CatNumericValue, CatWhiteSpace, Tolerance, CatNumericValue]↵  25. [CatNumericValue, CatWhiteSpace, Plus, CatNumericValue]↵  26. [CatNumericValue, CatWhiteSpace, CatNumericValue, CatWhiteSpace]↵  27. [CatNumericValue, CatWhiteSpace, CatNumericValue, ForwardSlash]↵  28. [CatNumericValue, CatWhiteSpace, CatNumericValue, Minus]↵  29. [CatNumericValue, CatWhiteSpace, CatNumericValue, CatNumericValue]↵  30. [CatNumericValue, CatWhiteSpace, CatWhiteSpace, Plus]↵  31. [CatNumericValue, CatWhiteSpace, CatWhiteSpace, CatNumericValue]↵  32. [CatNumericValue, Tolerance, CatWhiteSpace, CatNumericValue]↵  33. [CatNumericValue, Tolerance, CatNumericValue, By]↵  34. [CatNumericValue, Plus, CatNumericValue, CatWhiteSpace]↵  35. [CatNumericValue, Plus, CatNumericValue, ForwardSlash]↵  36. [CatNumericValue, Plus, CatNumericValue, Minus]↵  37. [CatNumericValue, Plus, CatNumericValue, CatNumericValue]↵  38. [CatNumericValue, CatNumericValue, CatWhiteSpace, ForwardSlash]↵  39. [CatNumericValue, CatNumericValue, CatWhiteSpace, CatWhiteSpace]↵  40. [CatNumericValue, CatNumericValue, CatWhiteSpace, Minus]↵  41. [CatNumericValue, CatNumericValue, CatWhiteSpace, CatNumericValue]↵  42. [CatNumericValue, CatNumericValue, ForwardSlash, CatWhiteSpace]↵  43. [CatNumericValue, CatNumericValue, ForwardSlash, Minus]↵  44. [CatNumericValue, CatNumericValue, ForwardSlash, CatNumericValue]↵  45. [CatNumericValue, CatNumericValue, Minus, CatNumericValue]↵  46. [CatNumericValue, CatNumericValue, CatNumericValue, By]↵  47. [CatNumericValue, By, CatNumericValue]↵  48. [CatNumericValue, Range, Integer, By]↵  49. [CatNumericValue, Minus, Integer, By]↵  50. [CatNumericValue, Minus, StandardThreadFormSeries]↵  51. [CatNumericValue, Minus, Integer, StandardThreadFormSeries]↵  52. [LeftParentheses]↵  53. [VPipe, ValueWithOptionalUnits]↵  54. [VPipe, WordThru]↵  55. [VPipe, By]↵  56. [VPipe, CatNumericValue]↵  57. [VPipe, Depth]↵  58. [VPipe, HolePrefix]↵  59. [LeftBracket]↵  60. [WordThru]↵  61. [By]↵  62. [Depth]↵  63. [HolePrefix]↵  64. [ValueWithOptionalUnits]↵  65. [Integer]↵  66. [ValueWithOptionalUnits, CatRangeWord]↵  67. [CatNumericValue, WordDeep]↵  68. [CatNumericValue, WordDia]↵  69. [CatNumericValue, ForwardSlash]↵  70. [CatNumericValue, CatWhiteSpace, WordDia]↵  71. [CatNumericValue, Tolerance, CatNumericValue]↵  72. [CatNumericValue, CatNumericValue, CatNumericValue]↵  73. [CatNumericValue, CatWhiteSpace, CatNumericValue]↵  74. [CatNumericValue, Range, CatNumericValue]↵  75. [CatNumericValue, Minus, CatNumericValue]↵  76. [CatNumericValue, CatWhiteSpace, Tolerance, CatWhiteSpace]↵  77. [CatNumericValue, CatWhiteSpace, Tolerance, CatNumericValue]↵  78. [CatNumericValue, CatWhiteSpace, Plus, CatNumericValue]↵  79. [CatNumericValue, CatWhiteSpace, CatNumericValue, CatWhiteSpace]↵  80. [CatNumericValue, CatWhiteSpace, CatNumericValue, ForwardSlash]↵  81. [CatNumericValue, CatWhiteSpace, CatNumericValue, Minus]↵  82. [CatNumericValue, CatWhiteSpace, CatNumericValue, CatNumericValue]↵  83. [CatNumericValue, CatWhiteSpace, CatWhiteSpace, Plus]↵  84. [CatNumericValue, CatWhiteSpace, CatWhiteSpace, CatNumericValue]↵  85. [CatNumericValue, Tolerance, CatWhiteSpace, CatNumericValue]↵  86. [CatNumericValue, Plus, CatNumericValue, CatWhiteSpace]↵  87. [CatNumericValue, Plus, CatNumericValue, ForwardSlash]↵  88. [CatNumericValue, Plus, CatNumericValue, Minus]↵  89. [CatNumericValue, Plus, CatNumericValue, CatNumericValue]↵  90. [CatNumericValue, CatNumericValue, CatWhiteSpace, ForwardSlash]↵  91. [CatNumericValue, CatNumericValue, CatWhiteSpace, CatWhiteSpace]↵  92. [CatNumericValue, CatNumericValue, CatWhiteSpace, Minus]↵  93. [CatNumericValue, CatNumericValue, CatWhiteSpace, CatNumericValue]↵  94. [CatNumericValue, CatNumericValue, ForwardSlash, CatWhiteSpace]↵  95. [CatNumericValue, CatNumericValue, ForwardSlash, Minus]↵  96. [CatNumericValue, CatNumericValue, ForwardSlash, CatNumericValue]↵  97. [CatNumericValue, CatNumericValue, Minus, CatNumericValue]↵  98. [CatWhiteSpace]↵  99. [ClassBasic]↵  100. [ClassReference]↵  101. [Words]↵but found: '|'",
            },
          ],
          name: 'Parsing Errors',
        },
        input: 'This is a note |C| with a framed character',
        notation: { type: 'unknown' },
      };
      const feature = await service.parseInterpretedNotation({
        interpreted,
        types,
      });
      expect(feature).toMatchObject({
        notationType: 'fa3284ed-ad3e-43c9-b051-87bd2e95e927', // Note
        notationSubtype: '805fd539-8063-4158-8361-509e10a5d371', // Note
        fullSpecification: 'This is a note  with a framed character',
        nominal: 'This is a note |C| with a framed character',
      });
    });

    it('should store unknown features as notes', async () => {
      const interpreted = {
        error: {
          errors: [
            {
              name: 'NoViableAltException',
              message:
                "Expecting: one of these possible Token sequences:↵  1. [CatGDTSymbol]↵  2. [VPipe, CatGDTSymbol]↵  3. [ThreadMetric]↵  4. [Multiple]↵  5. [ValueWithOptionalUnits, By, CatNumericValue]↵  6. [Integer, By, CatNumericValue]↵  7. [CatNumericValue, By, ValueWithOptionalUnits]↵  8. [CatNumericValue, By, Integer]↵  9. [ValueWithOptionalUnits, By, CatNumericValue, CatWhiteSpace]↵  10. [ValueWithOptionalUnits, By, CatNumericValue, Tolerance]↵  11. [ValueWithOptionalUnits, By, CatNumericValue, Plus]↵  12. [ValueWithOptionalUnits, By, CatNumericValue, CatNumericValue]↵  13. [ValueWithOptionalUnits, By, CatNumericValue, ForwardSlash]↵  14. [ValueWithOptionalUnits, By, CatNumericValue, Range]↵  15. [ValueWithOptionalUnits, By, CatNumericValue, Minus]↵  16. [Integer, By, CatNumericValue, CatWhiteSpace]↵  17. [Integer, By, CatNumericValue, Tolerance]↵  18. [Integer, By, CatNumericValue, Plus]↵  19. [Integer, By, CatNumericValue, CatNumericValue]↵  20. [Integer, By, CatNumericValue, ForwardSlash]↵  21. [Integer, By, CatNumericValue, Range]↵  22. [Integer, By, CatNumericValue, Minus]↵  23. [CatNumericValue, CatWhiteSpace, Tolerance, CatWhiteSpace]↵  24. [CatNumericValue, CatWhiteSpace, Tolerance, CatNumericValue]↵  25. [CatNumericValue, CatWhiteSpace, Plus, CatNumericValue]↵  26. [CatNumericValue, CatWhiteSpace, CatNumericValue, CatWhiteSpace]↵  27. [CatNumericValue, CatWhiteSpace, CatNumericValue, ForwardSlash]↵  28. [CatNumericValue, CatWhiteSpace, CatNumericValue, Minus]↵  29. [CatNumericValue, CatWhiteSpace, CatNumericValue, CatNumericValue]↵  30. [CatNumericValue, CatWhiteSpace, CatWhiteSpace, Plus]↵  31. [CatNumericValue, CatWhiteSpace, CatWhiteSpace, CatNumericValue]↵  32. [CatNumericValue, Tolerance, CatWhiteSpace, CatNumericValue]↵  33. [CatNumericValue, Tolerance, CatNumericValue, By]↵  34. [CatNumericValue, Plus, CatNumericValue, CatWhiteSpace]↵  35. [CatNumericValue, Plus, CatNumericValue, ForwardSlash]↵  36. [CatNumericValue, Plus, CatNumericValue, Minus]↵  37. [CatNumericValue, Plus, CatNumericValue, CatNumericValue]↵  38. [CatNumericValue, CatNumericValue, CatWhiteSpace, ForwardSlash]↵  39. [CatNumericValue, CatNumericValue, CatWhiteSpace, CatWhiteSpace]↵  40. [CatNumericValue, CatNumericValue, CatWhiteSpace, Minus]↵  41. [CatNumericValue, CatNumericValue, CatWhiteSpace, CatNumericValue]↵  42. [CatNumericValue, CatNumericValue, ForwardSlash, CatWhiteSpace]↵  43. [CatNumericValue, CatNumericValue, ForwardSlash, Minus]↵  44. [CatNumericValue, CatNumericValue, ForwardSlash, CatNumericValue]↵  45. [CatNumericValue, CatNumericValue, Minus, CatNumericValue]↵  46. [CatNumericValue, CatNumericValue, CatNumericValue, By]↵  47. [CatNumericValue, By, CatNumericValue]↵  48. [CatNumericValue, Range, Integer, By]↵  49. [CatNumericValue, Minus, Integer, By]↵  50. [CatNumericValue, Minus, StandardThreadFormSeries]↵  51. [CatNumericValue, Minus, Integer, StandardThreadFormSeries]↵  52. [LeftParentheses]↵  53. [VPipe, ValueWithOptionalUnits]↵  54. [VPipe, WordThru]↵  55. [VPipe, By]↵  56. [VPipe, CatNumericValue]↵  57. [VPipe, Depth]↵  58. [VPipe, HolePrefix]↵  59. [LeftBracket]↵  60. [WordThru]↵  61. [By]↵  62. [Depth]↵  63. [HolePrefix]↵  64. [ValueWithOptionalUnits]↵  65. [Integer]↵  66. [ValueWithOptionalUnits, CatRangeWord]↵  67. [CatNumericValue, WordDeep]↵  68. [CatNumericValue, WordDia]↵  69. [CatNumericValue, ForwardSlash]↵  70. [CatNumericValue, CatWhiteSpace, WordDia]↵  71. [CatNumericValue, Tolerance, CatNumericValue]↵  72. [CatNumericValue, CatNumericValue, CatNumericValue]↵  73. [CatNumericValue, CatWhiteSpace, CatNumericValue]↵  74. [CatNumericValue, Range, CatNumericValue]↵  75. [CatNumericValue, Minus, CatNumericValue]↵  76. [CatNumericValue, CatWhiteSpace, Tolerance, CatWhiteSpace]↵  77. [CatNumericValue, CatWhiteSpace, Tolerance, CatNumericValue]↵  78. [CatNumericValue, CatWhiteSpace, Plus, CatNumericValue]↵  79. [CatNumericValue, CatWhiteSpace, CatNumericValue, CatWhiteSpace]↵  80. [CatNumericValue, CatWhiteSpace, CatNumericValue, ForwardSlash]↵  81. [CatNumericValue, CatWhiteSpace, CatNumericValue, Minus]↵  82. [CatNumericValue, CatWhiteSpace, CatNumericValue, CatNumericValue]↵  83. [CatNumericValue, CatWhiteSpace, CatWhiteSpace, Plus]↵  84. [CatNumericValue, CatWhiteSpace, CatWhiteSpace, CatNumericValue]↵  85. [CatNumericValue, Tolerance, CatWhiteSpace, CatNumericValue]↵  86. [CatNumericValue, Plus, CatNumericValue, CatWhiteSpace]↵  87. [CatNumericValue, Plus, CatNumericValue, ForwardSlash]↵  88. [CatNumericValue, Plus, CatNumericValue, Minus]↵  89. [CatNumericValue, Plus, CatNumericValue, CatNumericValue]↵  90. [CatNumericValue, CatNumericValue, CatWhiteSpace, ForwardSlash]↵  91. [CatNumericValue, CatNumericValue, CatWhiteSpace, CatWhiteSpace]↵  92. [CatNumericValue, CatNumericValue, CatWhiteSpace, Minus]↵  93. [CatNumericValue, CatNumericValue, CatWhiteSpace, CatNumericValue]↵  94. [CatNumericValue, CatNumericValue, ForwardSlash, CatWhiteSpace]↵  95. [CatNumericValue, CatNumericValue, ForwardSlash, Minus]↵  96. [CatNumericValue, CatNumericValue, ForwardSlash, CatNumericValue]↵  97. [CatNumericValue, CatNumericValue, Minus, CatNumericValue]↵  98. [CatWhiteSpace]↵  99. [ClassBasic]↵  100. [ClassReference]↵  101. [Words]↵but found: '|'",
            },
          ],
          name: 'Parsing Errors',
        },
        input: '|C|',
        notation: { type: 'unknown' },
      };
      const feature = await service.parseInterpretedNotation({
        interpreted,
        types,
      });
      expect(feature).toMatchObject({
        notationType: 'fa3284ed-ad3e-43c9-b051-87bd2e95e927', // Note
        notationSubtype: '805fd539-8063-4158-8361-509e10a5d371', // Note
        fullSpecification: '',
        nominal: '|C|',
      });
    });

    it('should throw an error if not passed results', () => {
      const interpreted = null;
      expect(() =>
        service.parseInterpretedNotation({
          interpreted,
          types,
        }),
      ).toThrow();
    });
  });

  describe('Assignments', () => {
    it('should correctly assign a Note characteristic', () => {
      const noteFeature = service.assignNote({
        feature: {},
        interpreted: {
          ocrRes: 'THIS IS A NOTE',
          input: 'THIS IS A NOTE',
          parsed: {
            input: 'THIS IS A NOTE',
          },
          notation: {
            type: 'note',
            text: 'THIS IS A NOTE',
            input: 'THIS IS A NOTE',
          },
        },
      });
      expect(noteFeature).toMatchObject({
        notationType: 'Note',
        notationSubtype: 'Note',
        fullSpecification: 'THIS IS A NOTE',
        nominal: 'THIS IS A NOTE',
        unit: '',
      });
    });

    it('should correctly assign a GDT characteristic', () => {
      const GDTFeature = service.assignGDT({
        feature: {},
        interpreted: {
          input: '|⌖|⌀.01Ⓢ|A|B|C|',
          text: '|⌖|⌀.01Ⓢ|A|B|C|',
          words: '',
          quantity: {
            text: '',
            units: '',
            value: '1',
          },
          notation: {
            datums: [{ id: 'A' }, { id: 'B' }, { id: 'C' }],
            subtype: 'Position',
            text: '|⌖|⌀.01Ⓢ|A|B|C|',
            type: 'gdtControlFrame',
            zones: {
              primary: {
                modifier: 'Ⓢ',
                postModifierValue: '',
                prefix: '⌀',
                text: '⌀.01Ⓢ',
                value: '.01',
              },
            },
          },
        },
      });
      expect(GDTFeature).toMatchObject({
        notationType: 'Geometric_Tolerance',
        notationSubtype: 'position',
        fullSpecification: '', // framed
        gdtSymbol: '',
        gdtPrimaryToleranceZone: '⌀.01Ⓢ',
        gdtPrimaryDatum: 'A',
        gdtSecondaryDatum: 'B',
        gdtTertiaryDatum: 'C',
      });
    });

    it('should correctly assign a Dimension characteristic', () => {
      const dimensionFeature = service.assignDimension({
        feature: {},
        interpreted: {
          input: '2.99',
          text: '2.99',
          words: '',
          quantity: {
            text: '',
            units: '',
            value: '1',
          },
          notation: {
            text: '2.99',
            type: 'length',
            nominal: {
              text: '2.99',
              units: '',
              value: '2.99',
            },
          },
        },
      });
      expect(dimensionFeature).toMatchObject({
        notationType: 'Dimension',
        notationSubtype: 'length',
        fullSpecification: '2.99',
        nominal: '2.99',
      });
    });

    it('should correctly assign a Split Dimension characteristic', () => {
      const dimensionFeature = service.assignDimension({
        feature: {},
        interpreted: {
          input: '3.39/3.36',
          text: '3.39/3.36',
          words: '',
          quantity: {
            text: '',
            units: '',
            value: '1',
          },
          notation: {
            text: '3.39/3.36',
            type: 'splitLimits',
            upper_limit: {
              text: '3.39',
              units: '',
              value: '3.39',
            },
            lower_limit: {
              text: '3.36',
              units: '',
              value: '3.36',
            },
          },
        },
      });
      expect(dimensionFeature).toMatchObject({
        notationType: 'Dimension',
        notationSubtype: 'length',
        fullSpecification: '3.39/3.36',
        nominal: '3.39/3.36',
        upperSpecLimit: '3.39',
        lowerSpecLimit: '3.36',
      });
    });

    it('should correctly assign a Dimension characteristic with tolerances', () => {
      const dimensionFeature = service.assignDimension({
        feature: {},
        interpreted: {
          input: '2.77±.04',
          text: '2.77±.04',
          words: '',
          quantity: {
            text: '',
            units: '',
            value: '1',
          },
          notation: {
            text: '2.77±.04',
            type: 'nominalWithTolerances',
            nominal: {
              text: '2.77',
              units: '',
              value: '2.77',
            },
            upper_tol: {
              text: '.04',
              units: '',
              value: '.04',
              sign: '+',
            },
            lower_tol: {
              text: '-.04',
              units: '',
              value: '.04',
              sign: '-',
            },
          },
        },
      });
      expect(dimensionFeature).toMatchObject({
        notationType: 'Dimension',
        notationSubtype: 'length',
        fullSpecification: '2.77±.04',
        nominal: '2.77',
        plusTol: '.04',
        minusTol: '-.04',
      });
    });

    it('should correctly assign a Dimension characteristic with Width', () => {
      const dimensionFeature = service.assignDimension({
        feature: {},
        interpreted: {
          input: '2X ⌀.12 THRU',
          text: '2X .12 THRU',
          words: '',
          quantity: {
            text: '2X',
            units: 'X',
            value: '2',
          },
          notation: {
            text: '.12 THRU',
            type: 'hole',
            symbol: '',
            modifier: '',
            width: {
              text: '.12',
              units: '',
              value: '.12',
            },
            depth: {
              text: 'THRU',
              symbol: '',
              word: 'THRU',
              type: 'depth',
              amount: {
                text: '',
                value: '',
              },
            },
          },
        },
      });
      expect(dimensionFeature).toMatchObject({
        notationType: 'Dimension',
        notationSubtype: 'diameter',
        fullSpecification: '2X ⌀.12 THRU',
        nominal: '.12',
      });
    });

    it('should correctly assign a Dimension characteristic with Limits', () => {
      const dimensionFeature = service.assignDimension({
        feature: {},
        interpreted: {
          input: '1.25 MIN',
          text: '1.25 MIN',
          words: '',
          quantity: {
            text: '',
            units: '',
            value: '1',
          },
          notation: {
            text: '1.25 MIN',
            type: 'extreme',
            edge: 'MIN',
            limit: {
              text: '1.25',
              units: '',
              value: '1.25',
            },
          },
        },
      });
      expect(dimensionFeature).toMatchObject({
        notationType: 'Dimension',
        notationSubtype: 'length',
        fullSpecification: '1.25 MIN',
        nominal: '1.25',
        upperSpecLimit: '',
        lowerSpecLimit: '1.25',
      });
    });
  });

  describe('Updates', () => {
    it('updates shared fields', () => {
      const changed = service.updateGeneral({
        feature: note,
        change: { notationType: 'some.uuid', quantity: 3 },
      });
      expect(changed.notationType).toBe('some.uuid');
      expect(changed.quantity).toBe(3);
      expect(changed.id).toBe(note.id);
    });

    it('updates a GDT', () => {
      const changed = service.updateGDT({
        feature: GDT,
        change: { gdtPrimaryToleranceZone: '123.456', gdtPrimaryDatum: 'Z' },
      });
      expect(changed.gdtPrimaryToleranceZone).toBe('123.456');
      expect(changed.gdtPrimaryDatum).toBe('Z');
      expect(changed.id).toBe(GDT.id);
    });

    it('updates a Dimension', () => {
      const changed = service.updateDimension({
        feature: dimension,
        change: { nominal: '123.456', notationClass: 'Basic' },
      });
      expect(changed.nominal).toBe('123.456');
      expect(changed.notationClass).toBe('Basic');
      expect(changed.id).toBe(dimension.id);
    });

    it('updates to basic', () => {
      const changed = service.updateDimension({
        feature: dimension,
        change: { notationClass: 'Basic' },
      });
      expect(changed.notationClass).toBe('Basic');
      expect(changed.fullSpecification).toBe('');
    });

    it('updates to reference', () => {
      const changed = service.updateDimension({
        feature: dimension,
        change: { notationClass: 'Reference' },
      });
      expect(changed.notationClass).toBe('Reference');
      expect(changed.fullSpecification).toBe(`(${dimension.fullSpecification})`);
    });

    it('updates inspection fields', () => {
      const changed = service.updateGeneral({
        feature: dimension,
        change: { criticality: 'testCrit', operation: 'testOp', inspectionMethod: 'testMethod', notes: 'testing notes' },
      });
      expect(changed.criticality).toBe('testCrit');
      expect(changed.operation).toBe('testOp');
      expect(changed.inspectionMethod).toBe('testMethod');
      expect(changed.notes).toBe('testing notes');
    });
  });

  describe('Utilities', () => {
    it('converts interpreted GDT subtype to database subtype', () => {
      const text = 'Profile of a Surface';
      const subtype = service.toSnakeCase(text);
      expect(subtype).toBe('profile_of_a_surface');
    });

    it('determines if a dimension is basic given the interpreter notation modifier', () => {
      let modifier = 'basic';
      let basic = service.isBasic(modifier);
      expect(basic).toBeTruthy();
      modifier = 'reference';
      basic = service.isBasic(modifier);
      expect(basic).toBeFalsy();
    });

    it('determines if a dimension is a reference given the interpreter notation modifier', () => {
      let modifier = 'basic';
      let reference = service.isReference(modifier);
      expect(reference).toBeFalsy();
      modifier = 'reference';
      reference = service.isReference(modifier);
      expect(reference).toBeTruthy();
    });

    it('determines the class of the feature given the interpreter notation modifier', () => {
      let modifier = 'basic';
      let notationClass = service.getClassForNotation(modifier);
      expect(notationClass).toBe('Basic');
      modifier = 'reference';
      notationClass = service.getClassForNotation(modifier);
      expect(notationClass).toBe('Reference');
      modifier = '';
      notationClass = service.getClassForNotation(modifier);
      expect(notationClass).toBe('Tolerance');
    });

    it('adds pipes where needed to a basic dimension', () => {
      expect(service.updateToBasic('1.25')).toBe('');
      expect(service.updateToBasic('1.25 BASIC')).toBe('1.25 BASIC');
      expect(service.updateToBasic('1.25 BSC')).toBe('1.25 BSC');
      expect(service.updateToBasic('|1.25')).toBe('');
      expect(service.updateToBasic('1.25|')).toBe('');
      expect(service.updateToBasic('(1.25)')).toBe('');
    });

    it('adds parentheses where needed to a reference dimension', () => {
      expect(service.updateToRef('1.25')).toBe('(1.25)');
      expect(service.updateToRef('1.25 Reference')).toBe('1.25 Reference');
      expect(service.updateToRef('1.25 REF')).toBe('1.25 REF');
      expect(service.updateToRef('1.25 Ref.')).toBe('1.25 Ref.');
      expect(service.updateToRef('(1.25')).toBe('(1.25)');
      expect(service.updateToRef('1.25)')).toBe('(1.25)');
      expect(service.updateToRef('|1.25|')).toBe('(1.25)');
      expect(service.updateToRef('')).toBe('(1.25)');
    });

    it('removes pipes and parentheses when switching to tolerance', () => {
      expect(service.updateToTol('', 'Basic')).toBe('1.25');
      expect(service.updateToTol('1.25', 'Basic')).toBe('1.25');
      expect(service.updateToTol('(1.25)', 'Reference')).toBe('1.25');
      expect(service.updateToTol('1.25)', 'Reference')).toBe('1.25');
      expect(service.updateToTol('|1.25', 'Basic')).toBe('1.25');
    });
  });
});
