const JobRepository = require('../database/repositories/jobRepository');
const SequelizeRepository = require('../database/repositories/sequelizeRepository');

/**
 * Handles Job operations
 */
module.exports = class JobService {
  constructor({ currentUser, language }) {
    this.repository = new JobRepository();
    this.currentUser = currentUser;
    this.language = language;
  }

  /**
   * Creates a Job.
   *
   * @param {*} data
   */
  async create(data) {
    const transaction = await SequelizeRepository.createTransaction(this.currentUser.accountId);

    try {
      const record = await this.repository.create(data, {
        transaction,
        currentUser: this.currentUser,
      });

      await SequelizeRepository.commitTransaction(transaction);

      return record;
    } catch (error) {
      await SequelizeRepository.rollbackTransaction(transaction);
      throw error;
    }
  }

  /**
   * Updates a Job.
   *
   * @param {*} id
   * @param {*} data
   */
  async update(id, data) {
    const transaction = await SequelizeRepository.createTransaction(this.currentUser.accountId);

    try {
      const record = await this.repository.update(id, data, {
        transaction,
        currentUser: this.currentUser,
      });

      await SequelizeRepository.commitTransaction(transaction);

      return record;
    } catch (error) {
      await SequelizeRepository.rollbackTransaction(transaction);
      throw error;
    }
  }

  /**
   * Destroy all Jobs with those ids.
   *
   * @param {*} ids
   */
  async destroyAll(ids) {
    const transaction = await SequelizeRepository.createTransaction(this.currentUser.accountId);

    try {
      for (const id of ids) {
        await this.repository.destroy(id, {
          transaction,
          currentUser: this.currentUser,
        });
      }

      await SequelizeRepository.commitTransaction(transaction);
    } catch (error) {
      await SequelizeRepository.rollbackTransaction(transaction);
      throw error;
    }
  }

  /**
   * Finds the Job by Id.
   *
   * @param {*} id
   */
  async findById(id) {
    return this.repository.findById(id, {
      currentUser: this.currentUser,
    });
  }

  /**
   * Finds Jobs based on the query.
   *
   * @param {*} args
   */
  async findAndCountAll(args) {
    return this.repository.findAndCountAll(args, {
      currentUser: this.currentUser,
    });
  }

  /**
   * Finds Jobs based on the query.
   *
   * @param {*} args
   */
  async findAndCountAll(args) {
    return this.repository.findAndCountAll(args, {
      currentUser: this.currentUser,
    });
  }
};
