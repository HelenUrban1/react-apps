const config = require('../../config')();
const USE_MOCKS = config.useMocks;

if (USE_MOCKS) {
  jest.doMock('../database/models');
  jest.doMock('../database/repositories/userRepository');
}

const models = require('../database/models');
const UserRepository = require('../database/repositories/userRepository');
const UserService = require('./userService');
const SequelizeRepository = require('../database/repositories/sequelizeRepository');

describe('User Service', () => {
  const accountId = config.testAccountId;
  const data = {
    email: 'test@user.com',
  };

  const createMock = jest.fn().mockReturnValue(data);

  beforeEach(async () => {
    if (USE_MOCKS) {
      models.getTenant = () =>
        Promise.resolve({
          sequelize: {
            transaction: jest.fn().mockReturnValue({
              rollback: jest.fn(),
              commit: jest.fn(),
            }),
          },
        });

      models.sequelizeAdmin = {
        query: jest.fn().mockReturnValue(Promise.resolve({})),
      };

      UserRepository.create = createMock;

      SequelizeRepository.commitTransaction = jest.fn();
    }
  });

  afterAll(async () => {
    if (!USE_MOCKS) {
      await SequelizeRepository.closeConnections();
      await SequelizeRepository.closeConnections(accountId);
    }
  });

  describe('Create', () => {
    const context = { currentUser: { accountId } };

    it('Should return user data', async () => {
      try {
        const user = await new UserService(context).create(data);
        if (USE_MOCKS) {
          expect(createMock).toBeCalledTimes(1);
        }
        expect(user).toBeTruthy();
      } catch (exception) {
        console.error(exception);
        expect(exception).toBeFalsy();
      }
    }, 10000);
  });
});
