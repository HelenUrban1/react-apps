const CharacteristicRepository = require('../database/repositories/characteristicRepository');
const ValidationError = require('../errors/validationError');
const SequelizeRepository = require('../database/repositories/sequelizeRepository');
const DrawingSheetService = require('./drawingSheetService');
const PartService = require('./partService');
const OcrService = require('./ocrService');

/**
 * Handles Characteristic operations
 */
module.exports = class CharacteristicService {
  constructor({ currentUser, language, eventEmitter }) {
    this.repository = new CharacteristicRepository();
    this.drawingSheetService = new DrawingSheetService({ currentUser, language, eventEmitter });
    this.partService = new PartService({ currentUser, language, eventEmitter });
    this.ocrService = new OcrService();
    this.currentUser = currentUser;
    this.language = language;
    this.eventEmitter = eventEmitter;
  }

  /**
   * Creates a Characteristic.
   *
   * @param {*} data
   */
  async create(data) {
    const transaction = await SequelizeRepository.createTransaction(this.currentUser.accountId);

    try {
      const record = await this.repository.create(data, {
        transaction,
        currentUser: this.currentUser,
      });

      await SequelizeRepository.commitTransaction(transaction);

      return record;
    } catch (error) {
      await SequelizeRepository.rollbackTransaction(transaction);
      throw error;
    }
  }

  /**
   * Creates a characteristic in a specific account.
   * Used for auto-ballooning
   * @param {*} data
   */
  async createInAccount(data) {
    const transaction = await SequelizeRepository.createTransaction(data.accountId);

    try {
      const record = await this.repository.createInAccount(data, {
        transaction,
        currentUser: this.currentUser,
      });

      await SequelizeRepository.commitTransaction(transaction);

      return record;
    } catch (error) {
      await SequelizeRepository.rollbackTransaction(transaction);
      throw error;
    }
  }

  /**
   * Creates multiple characteristics in a specific account.
   * Used for auto-ballooning
   * @param {*} data
   */
  async createManyInAccount(data) {
    const characteristics = [];
    const accountId = data[0].accountId;
    const siteId = data[0].siteId;
    if (!accountId || !siteId) throw new Error('Account ID or Site ID not specified');

    const transaction = await SequelizeRepository.createTransaction(accountId);
    try {
      await Promise.all(
        data.map(async (characteristic) => {
          const record = await this.repository.createInAccount(characteristic, {
            transaction,
            currentUser: this.currentUser,
          });
          characteristics.push(record);
        }),
      );
    } catch (error) {
      await SequelizeRepository.rollbackTransaction(transaction);
      throw error;
    }
    await SequelizeRepository.commitTransaction(transaction);

    const records = await this.drawingSheetService.findDrawingSheetsWithNullFoundCaptureCountInAccount(data[0].drawing, accountId);

    let autoMarkupComplete = true;

    records.forEach((sheet) => {
      const foundCaptureCount = sheet.dataValues.foundCaptureCount;
      if (!foundCaptureCount) {
        autoMarkupComplete = false;
        return;
      }
    });

    if (autoMarkupComplete) {
      const updatedPart = await this.partService.updateForUserAndTentant(data[0].part, accountId, siteId, { autoMarkupStatus: 'Completed' });
      if (this.eventEmitter) {
        this.eventEmitter.emit('part.autoMarkupCompleted', { accountId, siteId, data: updatedPart });
      }
    }

    return characteristics;
  }

  /**
   * Updates a Characteristic.
   *
   * @param {*} id
   * @param {*} data
   */
  async update(id, data) {
    const transaction = await SequelizeRepository.createTransaction(this.currentUser.accountId);

    try {
      const record = await this.repository.update(id, data, {
        transaction,
        currentUser: this.currentUser,
      });

      await SequelizeRepository.commitTransaction(transaction);

      return record;
    } catch (error) {
      await SequelizeRepository.rollbackTransaction(transaction);
      throw error;
    }
  }

  /**
   * Destroy all Characteristics with those ids.
   *
   * @param {*} ids
   */
  async destroyAll(ids) {
    const transaction = await SequelizeRepository.createTransaction(this.currentUser.accountId);

    try {
      let idsToDelete = ids;
      if (!Array.isArray(ids) && typeof ids === 'string') {
        idsToDelete = [ids];
      }
      await Promise.all(idsToDelete.map((id) => this.repository.destroy(id, { transaction, currentUser: this.currentUser })));

      await SequelizeRepository.commitTransaction(transaction);
    } catch (error) {
      await SequelizeRepository.rollbackTransaction(transaction);
      throw error;
    }
  }

  /**
   * Finds the Characteristic by Id.
   *
   * @param {*} id
   */
  async findById(id) {
    return this.repository.findById(id, {
      currentUser: this.currentUser,
    });
  }

  /**
   * Finds Characteristics for Autocomplete.
   *
   * @param {*} search
   * @param {*} limit
   */
  async findAllAutocomplete(search, limit) {
    return this.repository.findAllAutocomplete(search, limit, {
      currentUser: this.currentUser,
    });
  }

  /**
   * Finds Characteristics based on the query.
   *
   * @param {*} args
   */
  async findAndCountAll(args) {
    return this.repository.findAndCountAll(args, {
      currentUser: this.currentUser,
    });
  }

  /**
   * Imports a list of Characteristics.
   *
   * @param {*} data
   * @param {*} importHash
   */
  async import(data, importHash) {
    if (!importHash) {
      throw new ValidationError(this.language, 'importer.errors.importHashRequired');
    }

    if (await this._isImportHashExistent(importHash)) {
      throw new ValidationError(this.language, 'importer.errors.importHashExistent');
    }

    const dataToCreate = {
      ...data,
      importHash,
    };

    return this.create(dataToCreate);
  }

  /**
   * Checks if the import hash already exists.
   * Every item imported has a unique hash.
   *
   * @param {*} importHash
   */
  async _isImportHashExistent(importHash) {
    const count = await this.repository.count({
      importHash,
    });

    return count > 0;
  }

  /**
   * Extracts the OCR from an image, updates the characteristic saves it to the database.
   * @param {Buffer} imageData
   * @param {Boolean} isGdt
   * @param {String} characteristicId
   * @returns {characteristic} The parsed characteristic & emits the characteristic.update event.
   */
  async parseAndSaveAutomarkup(imageData, isGdt, characteristicId, tolerances, types) {
    const [ocrRes, textRes] = await Promise.all([this.ocrService.sendToOcr(imageData, isGdt, 'dimension'), this.ocrService.sendToOcr(imageData, isGdt, 'text')]);

    const characteristicToUpdate = await this.findById(characteristicId);

    const parsed = await this.ocrService.parseFeatureFromFullSpec({ ocrRes, textRes, characteristicToUpdate, tolerances, types });

    const updated = await this.update(characteristicId, {
      notationClass: parsed.input.notationClass,
      fullSpecification: parsed.input.fullSpecification,
      quantity: parsed.input.quantity,
      nominal: parsed.input.nominal,
      upperSpecLimit: parsed.input.upperSpecLimit,
      lowerSpecLimit: parsed.input.lowerSpecLimit,
      plusTol: parsed.input.plusTol,
      minusTol: parsed.input.minusTol,
      unit: parsed.input.unit,
      captureError: parsed.input.captureError,
      verified: parsed.input.verified,
      fullSpecificationFromOCR: parsed.input.fullSpecificationFromOCR,
      confidence: parsed.input.confidence,
      connectionPointIsFloating: parsed.input.connectionPointIsFloating,
      toleranceSource: parsed.input.toleranceSource,
      notationType: parsed.input.notationType,
      notationSubtype: parsed.input.notationSubtype,
      part: characteristicToUpdate.part,
      drawing: characteristicToUpdate.drawing,
      drawingSheet: characteristicToUpdate.drawingSheet,
    });

    this.eventEmitter.emit('characteristic.update', { accountId: this.currentUser.accountId, siteId: this.currentUser.siteId, data: updated });
    return updated;
  }
};
