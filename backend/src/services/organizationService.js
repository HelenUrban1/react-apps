const OrganizationRepository = require('../database/repositories/organizationRepository');
const AccountRepository = require('../database/repositories/accountRepository');
const ValidationError = require('../errors/validationError');
const SequelizeRepository = require('../database/repositories/sequelizeRepository');

/**
 * Handles Organization operations
 */
module.exports = class OrganizationService {
  constructor({ currentUser, language, crmClient }) {
    this.repository = new OrganizationRepository();
    this.currentUser = currentUser;
    this.language = language;
    this.crmClient = crmClient;
  }

  /**
   * Creates a Organization.
   *
   * @param {*} data
   */
  async create(data) {
    const transaction = await SequelizeRepository.createTransaction(this.currentUser.accountId);

    try {
      const record = await this.repository.create(data, {
        transaction,
        currentUser: this.currentUser,
      });

      await SequelizeRepository.commitTransaction(transaction);

      return record;
    } catch (error) {
      await SequelizeRepository.rollbackTransaction(transaction);
      throw error;
    }
  }

  /**
   * Creates a default Organization based on info of a new Account.
   *
   * @param {*} data
   */
  async createDefault(data) {
    const transaction = await SequelizeRepository.createTransaction(data.accountId);

    try {
      const accountUser = {
        accountId: data.accountId,
      };

      const account = await new AccountRepository().findById(data.accountId);
      const organization = {
        status: 'Active',
        type: 'Account',
        settings: '',
        name: account.orgName,
        phone: account.orgPhone,
        email: account.orgEmail,
        street1: account.orgStreet,
        street2: account.orgStreet2,
        city: account.orgCity,
        region: account.orgRegion,
        postalCode: account.orgPostalcode,
        country: account.orgCountry,
        siteId: null,
      };

      const record = await this.repository.create(organization, {
        transaction,
        currentUser: accountUser,
      });

      await SequelizeRepository.commitTransaction(transaction);

      return record;
    } catch (error) {
      await SequelizeRepository.rollbackTransaction(transaction);
      throw error;
    }
  }

  /**
   * Updates a Organization.
   *
   * @param {*} id
   * @param {*} data
   */
  async update(id, data) {
    const transaction = await SequelizeRepository.createTransaction(this.currentUser.accountId);

    try {
      const record = await this.repository.update(id, data, {
        transaction,
        currentUser: this.currentUser,
      });

      await SequelizeRepository.commitTransaction(transaction);
      return record;
    } catch (error) {
      await SequelizeRepository.rollbackTransaction(transaction);
      throw error;
    }
  }

  /**
   * Destroy all Organizations with those ids.
   *
   * @param {*} ids
   */
  async destroyAll(ids) {
    const transaction = await SequelizeRepository.createTransaction(this.currentUser.accountId);

    try {
      for (const id of ids) {
        await this.repository.destroy(id, {
          transaction,
          currentUser: this.currentUser,
        });
      }

      await SequelizeRepository.commitTransaction(transaction);
    } catch (error) {
      await SequelizeRepository.rollbackTransaction(transaction);
      throw error;
    }
  }

  /**
   * Finds the Organization by Id.
   *
   * @param {*} id
   */
  async findById(id) {
    return this.repository.findById(id, {
      currentUser: this.currentUser,
    });
  }

  /**
   * Finds Organizations for Autocomplete.
   *
   * @param {*} search
   * @param {*} limit
   */
  async findAllAutocomplete(search, limit) {
    return this.repository.findAllAutocomplete(search, limit, {
      currentUser: this.currentUser,
    });
  }

  /**
   * Finds Organizations based on the query.
   *
   * @param {*} args
   */
  async findAndCountAll(args) {
    return this.repository.findAndCountAll(args, {
      currentUser: this.currentUser,
    });
  }

  /**
   * Imports a list of Organizations.
   *
   * @param {*} data
   * @param {*} importHash
   */
  async import(data, importHash) {
    if (!importHash) {
      throw new ValidationError(this.language, 'importer.errors.importHashRequired');
    }

    if (await this._isImportHashExistent(importHash)) {
      throw new ValidationError(this.language, 'importer.errors.importHashExistent');
    }

    const dataToCreate = {
      ...data,
      importHash,
    };

    return this.create(dataToCreate);
  }

  /**
   * Checks if the import hash already exists.
   * Every item imported has a unique hash.
   *
   * @param {*} importHash
   */
  async _isImportHashExistent(importHash) {
    const count = await this.repository.count({
      importHash,
    });

    return count > 0;
  }

  /**
   * Checks if an organization already exists.
   *
   * @param {string} newOrganizationName 
   */
  async doesOrganizationExist(newOrganizationName) {
    return await this.repository.getDoesOrganizationExist(newOrganizationName, {
      currentUser: this.currentUser,
    })
  }
};