const config = require('../../config')();

jest.doMock('../database/models');
jest.doMock('../database/repositories/userRepository');
jest.doMock('../database/repositories/accountMemberRepository');

const models = require('../database/models');
const UserRepository = require('../database/repositories/userRepository');
const AccountMemberRepository = require('../database/repositories/accountMemberRepository');
const SequelizeRepository = require('../database/repositories/sequelizeRepository');
const ExternalEventService = require('./externalEventService');
const analyticsClientEvents = require('../clients/analyticsClientEvents.js');
// const accountMemberFixtures = require('../__fixtures__').accountMemberFixture;
const userFixtures = require('../__fixtures__/userFixture');

const {
  sendgridEvents, //
  sqsPostBodyForSendgridEvent,
  sqsPostForSendgridEvent,
} = require('../__fixtures__/externalEvents/sendgridEventFixture');
const {
  chargifyEvents, //
  sqsPostBodyForChargifyEvent,
  sqsPostForChargifyEvent,
} = require('../__fixtures__/externalEvents/chargifyEventFixture');

const SubscriptionWorker = require('../clients/subscriptionWorker');

describe('ExternalEventService', () => {
  const mockS3 = {
    getObject: jest.fn(),
    upload: jest.fn(),
  };

  const loggerMock = {
    trace: jest.fn(),
    debug: jest.fn(),
    log: jest.fn(),
    info: jest.fn(),
    warn: jest.fn(),
    error: jest.fn(),
  };

  const crmClientMock = {
    getMetadata: jest.fn(),
    // Companies
    getCompanyById: jest.fn(),
    getCompanyByDomain: jest.fn(),
    createCompany: jest.fn(),
    updateCompany: jest.fn(),
    createOrUpdateCompany: jest.fn(),
    addContactToCompany: jest.fn(),
    // Contacts
    getContactById: jest.fn(),
    getContactByEmail: jest.fn(),
    createOrUpdateContact: jest.fn(),
    updateContact: jest.fn(),
    updateContacts: jest.fn(),
    // Deals
    getDealById: jest.fn(),
    searchDeals: jest.fn(),
    createDeal: jest.fn(),
    updateDeal: jest.fn(),
    addContactToDeal: jest.fn(),
    addCompanyToDeal: jest.fn(),
    removeContactFromDeal: jest.fn(),
    removeContactFromCompany: jest.fn(),
    removeCompanyFromDeal: jest.fn(),
    // Associations
    // getAssociations
    getChildCompanyIds: jest.fn(),
    getParentCompanyIds: jest.fn(),
    getCompanysAssociatedContactIds: jest.fn(),
    getCompanysAssociatedDealIds: jest.fn(),
    // Timeline Event Templates
    buildTimelineEventReference: jest.fn(),
    loadLocalTimelineEventTypes: jest.fn(),
    extendTimelineEventReference: jest.fn(),
    timelineEventsAreDifferent: jest.fn(),
    listTimelineEventTypes: jest.fn(),
    createTimelineEventType: jest.fn(),
    updateTimelineEventType: jest.fn(),
    // Timeline Events
    ensureAccessToken: jest.fn(),
    getTimelineEventType: jest.fn(),
    createTimelineEvent: jest.fn(),
    createTimelineEvents: jest.fn(),
    getTimelineEvent: jest.fn(),
    recordEvent: jest.fn(),
    buildTimelineEventPayload: jest.fn(),
    // Helper Functions
    dateInCrmFormat: jest.fn(),
  };

  const billingClientMock = {
    adapter: { name: 'chargify', isMocked: true },
    trace: jest.fn(),
    getMetadata: jest.fn(),
    getVendorUrlForCustomerWithId: jest.fn(),
    getVendorUrlForSubscriptionWithId: jest.fn(),
    getInitData: jest.fn(),
    // Customers
    createOrUpdateCustomer: jest.fn(),
    createCustomer: jest.fn(),
    getCustomerById: jest.fn(),
    getCustomerByReference: jest.fn(),
    getMetadataForCustomerWithId: jest.fn(),
    findCustomer: jest.fn(),
    updateCustomer: jest.fn(),
    // Product Catalog
    getCatalog: jest.fn(),
    getProductFamilies: jest.fn(),
    getProducts: jest.fn(),
    getProductPrices: jest.fn(),
    getProductComponents: jest.fn(),
    getComponent: jest.fn(),
    getComponentPrices: jest.fn(),
    getSubscriptionComponents: jest.fn(),
    deleteSubscriptionCustomFields: jest.fn(),
    getCurrentPrice: jest.fn(),
    // Subscriptions
    createSubscription: jest.fn(),
    getSubscriptionById: jest.fn(),
    getSubscriptionByReference: jest.fn(),
    getSubscriptionsForCustomer: jest.fn(),
    updateSubscription: jest.fn(),
    updateSubscriptionQuantity: jest.fn(),
    updateComponentPrice: jest.fn(),
    delayedCancelSubscription: jest.fn(),
    immediatelyCancelSubscription: jest.fn(),
    removeDelayedCancel: jest.fn(),
    reactivateSubscription: jest.fn(),
    getBillingPortalLink: jest.fn(),
    // Custom Fields
    getSubscriptionCustomFields: jest.fn(),
    updateSubscriptionCustomFields: jest.fn(),
    // Payment Methods
    addPaymentMethod: jest.fn(),
    changeDefaultPaymentMethod: jest.fn(),
    chargeSubscriptionPrePayment: jest.fn(),
    // Payment History
    getInvoices: jest.fn(),
    getInvoicesForSubscription: jest.fn(),
    downloadInvoice: jest.fn(),
    issueInvoice: jest.fn(),
    payInvoice: jest.fn(),
    voidInvoice: jest.fn(),
  };

  const analyticsClientMock = {
    track: jest.fn(),
    events: analyticsClientEvents,
  };

  const mockUser = userFixtures.findByEmailWithoutAvatarMock;

  const subWorker = new SubscriptionWorker({
    s3: mockS3,
    crmClient: crmClientMock,
    billingClient: billingClientMock,
    logger: loggerMock,
  });

  const context = {
    isTest: true,
    logger: loggerMock,
    analyticsClient: analyticsClientMock,
    subscriptionWorker: subWorker,
    crmClient: crmClientMock,
    billingClient: billingClientMock,
  };

  beforeEach(async () => {
    jest.resetAllMocks();

    // models.getTenant = () =>
    //   Promise.resolve({
    //     sequelize: {
    //       transaction: jest.fn().mockReturnValue({
    //         rollback: jest.fn(),
    //         commit: jest.fn(),
    //       }),
    //     },
    //   });

    models.sequelizeAdmin = {
      query: jest.fn().mockReturnValue(Promise.resolve({})),
    };

    const updateMock = jest.fn().mockReturnValue({ ...mockUser });

    UserRepository.findByEmailWithoutAvatar.mockResolvedValue({ ...mockUser });

    AccountMemberRepository.update.mockResolvedValue({
      ...mockUser.accountMemberships[0],
      inviteStatus: { ...sendgridEvents[-1] },
    });

    SequelizeRepository.commitTransaction = jest.fn();
  });

  // afterAll(async (done) => {
  //   await Promise.all([
  //     SequelizeRepository.closeConnections(config.testAccountId), //
  //     SequelizeRepository.closeConnections(),
  //   ]);
  //   done();
  // });

  describe('processEvent()', () => {
    it('Should return an array containing a result record for each event passed in', async () => {
      const result = await ExternalEventService.processEvent({
        analyticsClient: analyticsClientMock,
        event: { ...sqsPostBodyForSendgridEvent },
      });
      const eventCount = sendgridEvents.length;
      expect(result.results.length).toBe(eventCount);
      expect(result.message).toBe(`Processed ${eventCount} of ${eventCount} SendGrid events successfully`);
      expect(UserRepository.findByEmailWithoutAvatar).toBeCalledTimes(1);
      expect(AccountMemberRepository.update).toBeCalledTimes(eventCount);
      expect(analyticsClientMock.track).toBeCalledTimes(eventCount);
    });

    it('Should return 501 if an event handler is not availble for the event source', async () => {
      const result = await ExternalEventService.processEvent({
        event: {
          data: [{ email: 'dev.mail.monkey@gmail.com' }],
          attributes: {
            webhookSource: 'FOO',
          },
        },
      });
      expect(result.status).toBe(501);
      expect(result.message).toBe(`No handler matched the event type FOO`);
    });

    it('Should return an processing count that reflects whether an error occured', async () => {
      AccountMemberRepository.update.mockRejectedValueOnce(new Error('This is a mock error'));
      const result = await ExternalEventService.processEvent({
        analyticsClient: analyticsClientMock,
        event: { ...sqsPostBodyForSendgridEvent },
      });
      const eventCount = sendgridEvents.length;
      expect(result.results.length).toBe(eventCount);
      expect(result.message).toBe(`Processed ${eventCount - 1} of ${eventCount} SendGrid events successfully`);
      expect(UserRepository.findByEmailWithoutAvatar).toBeCalledTimes(1);
      expect(AccountMemberRepository.update).toBeCalledTimes(eventCount);
      expect(analyticsClientMock.track).toBeCalledTimes(eventCount + 1); // Add one for the error event
      // expect(loggerMock.error).toHaveBeenCalledTimes(0);
    });
  });

  describe('handleSendgridEvent()', () => {
    it('Should return an array containing a result record for each event passed in', async () => {
      const result = await ExternalEventService.handleSendgridEvent(sendgridEvents, { analyticsClient: analyticsClientMock });
      const eventCount = sendgridEvents.length;
      expect(result.results.length).toBe(eventCount);
      expect(result.message).toBe(`Processed ${eventCount} of ${eventCount} SendGrid events successfully`);
      expect(UserRepository.findByEmailWithoutAvatar).toBeCalledTimes(1);
      expect(AccountMemberRepository.update).toBeCalledTimes(eventCount);
      expect(analyticsClientMock.track).toBeCalledTimes(eventCount);
      // expect(loggerMock.error).toHaveBeenCalledTimes(0);
    });

    it('Should return an processing count that reflects whether an error occured', async () => {
      AccountMemberRepository.update.mockRejectedValueOnce(new Error('This is a mock error'));
      const result = await ExternalEventService.handleSendgridEvent(sendgridEvents, { analyticsClient: analyticsClientMock });
      const eventCount = sendgridEvents.length;
      expect(result.results.length).toBe(eventCount);
      expect(result.message).toBe(`Processed ${eventCount - 1} of ${eventCount} SendGrid events successfully`);
      expect(UserRepository.findByEmailWithoutAvatar).toBeCalledTimes(1);
      expect(AccountMemberRepository.update).toBeCalledTimes(eventCount);
      expect(analyticsClientMock.track).toBeCalledTimes(eventCount + 1); // Add one for the error event
      // expect(loggerMock.error).toHaveBeenCalledTimes(0);
    });
  });

  describe('handleChargifyEvent()', () => {
    it('Should return an array containing a result record for each event passed in', async () => {
      const paymentFailureMock = jest.spyOn(subWorker, 'paymentFailure').mockImplementation(() => true);
      const result = await ExternalEventService.handleChargifyEvent(chargifyEvents, context);
      const eventCount = chargifyEvents.length;
      expect(result.results.length).toBe(eventCount);
      expect(result.message).toBe(`Processed ${eventCount} of ${eventCount} Chargify events successfully`);
      expect(paymentFailureMock).toBeCalledTimes(eventCount);
      expect(analyticsClientMock.track).toBeCalledTimes(eventCount);
      // expect(loggerMock.error).toHaveBeenCalledTimes(0);
    });

    it('Should return an processing count that reflects whether an error occurred', async () => {
      const paymentFailureMock = jest.spyOn(subWorker, 'paymentFailure').mockImplementation(() => true);
      paymentFailureMock.mockRejectedValueOnce(new Error('This is a mock error'));
      const result = await ExternalEventService.handleChargifyEvent(chargifyEvents, context);
      const eventCount = chargifyEvents.length;
      expect(result.results.length).toBe(eventCount);
      expect(result.message).toBe(`Processed ${eventCount - 1} of ${eventCount} Chargify events successfully`);
      expect(paymentFailureMock).toBeCalledTimes(eventCount);
      expect(analyticsClientMock.track).toBeCalledTimes(eventCount + 1); // Add one for the error event
      // expect(loggerMock.error).toHaveBeenCalledTimes(0);
    });
  });

  describe('findAccountMemberByEmail()', () => {
    it('Should return the user and membership when successful', async () => {
      const lookupEmail = 'dev.mail.monkey@gmail.com';
      const user = await ExternalEventService.findAccountMemberByEmail(lookupEmail);
      // Verify structure
      expect(user.userId).toBe('e0a01779-c8ed-4090-bd44-9c1b2242fb75');
      expect(user.account).toBeDefined();
      expect(user.membership).toBeDefined();
      // Verify that membership is determined
      expect(user.activeAccountId).toBe('e0a01769-c8ed-4090-bd24-9c1b2241fb75');
      expect(user.account.id).toBe(user.activeAccountId);
      expect(user.membership.accountId).toBe(user.activeAccountId);
      expect(user.membership.id).toBe('007516cc-3168-40e4-8ad3-d42c92f66caa');
      // Verify that profile is merged in
      expect(user.email).toBe(lookupEmail);
      expect(user.firstName).toBe('Dev');
      expect(user.lastName).toBe('Monkey');
      expect(user.accountName).toBe('Acme Corp');
      expect(UserRepository.findByEmailWithoutAvatar).toBeCalledTimes(1);
      // expect(loggerMock.error).toHaveBeenCalledTimes(0);
    });

    it('Should return null if the user is not found', async () => {
      const lookupEmail = 'FOO';
      UserRepository.findByEmailWithoutAvatar.mockResolvedValueOnce(undefined);
      const user = await ExternalEventService.findAccountMemberByEmail(lookupEmail);
      // Verify structure
      expect(user).toBe(undefined);
      expect(UserRepository.findByEmailWithoutAvatar).toBeCalledTimes(1);
      // expect(loggerMock.error).toHaveBeenCalledTimes(0);
    });
  });
});
