const config = require('../../config')();

const USE_MOCKS = config.useMocks;

if (USE_MOCKS) {
  jest.doMock('../database/models');
  jest.doMock('../database/repositories/annotationRepository');
}

const models = require('../database/models');
const AnnotationRepository = require('../database/repositories/annotationRepository');
const SequelizeRepository = require('../database/repositories/sequelizeRepository');
const AnnotationService = require('./annotationService');
const { data } = require('../__fixtures__').annotation;

describe('AnnotationService', () => {
  const context = { currentUser: { accountId: config.testAccountId } };

  const createMock = jest.fn().mockReturnValue(data[0]);
  const updateMock = jest.fn().mockReturnValue(data[0]);
  const findAndCountAllMock = jest
    .fn()
    .mockReturnValueOnce({
      rows: [data[0]],
      count: 1,
    })
    .mockReturnValueOnce({ rows: [], count: 0 });
  const findByIdMock = jest.fn().mockReturnValue(data[0]);
  const destroyMock = jest.fn();

  beforeEach(async () => {
    if (USE_MOCKS) {
      models.getTenant = () =>
        Promise.resolve({
          sequelize: {
            transaction: jest.fn().mockReturnValue({
              rollback: jest.fn(),
              commit: jest.fn(),
            }),
          },
        });

      models.sequelizeAdmin = {
        query: jest.fn().mockReturnValue(Promise.resolve({})),
      };

      AnnotationRepository.mockImplementation(() => {
        return {
          create: createMock,
          update: updateMock,
          findAndCountAll: findAndCountAllMock,
          findById: findByIdMock,
          destroy: destroyMock,
        };
      });

      SequelizeRepository.commitTransaction = jest.fn();
    } else {
      await Promise.all([
        SequelizeRepository.cleanDatabase(config.testAccountId), //
      ]);
    }
  });

  afterAll(async () => {
    if (!USE_MOCKS) {
      await Promise.all([
        SequelizeRepository.closeConnections(config.testAccountId), //
        SequelizeRepository.closeConnections(),
      ]);
    }
  });

  describe('create()', () => {
    it('Should return annotation data', async () => {
      try {
        const annotation = await new AnnotationService(context).create(data[0]);
        if (USE_MOCKS) {
          expect(createMock).toBeCalledTimes(1);
        }
        expect(annotation).toBeTruthy();
      } catch (exception) {
        // eslint-disable-next-line no-console
        console.error(exception);
        expect(exception).toBeFalsy();
      }
    });
  });

  describe('update()', () => {
    it('Should update annotation data', async () => {
      const annotation = await new AnnotationService(context).create(data[0]);
      annotation.annotation = 'Update';
      const updatedAnnotation = await new AnnotationService(context).update(annotation.id, annotation);
      if (USE_MOCKS) {
        expect(updateMock).toBeCalledTimes(1);
      }
      expect(updatedAnnotation.annotation).toBe('Update');
    });
  });

  describe('destroyAll()', () => {
    it('Should delete a reviewItem by id', async () => {
      const annotation = await new AnnotationService(context).create(data[0]);
      const all = await new AnnotationService(context).findAndCountAll();
      expect(all.count).toBe(1);
      expect(all.rows).toBeTruthy();

      const context2 = { currentUser: { accountId: config.testAccountId } };
      await new AnnotationService(context2).destroyAll([annotation.id]);
      const none = await new AnnotationService(context2).findAndCountAll();
      if (USE_MOCKS) {
        expect(destroyMock).toBeCalledTimes(1);
      }
      expect(none.count).toBe(0);
      expect(none.rows).toStrictEqual([]);
    });
  });

  describe('findById()', () => {
    it('Should find annotation data by id', async () => {
      const annotation = await new AnnotationService(context).create(data[0]);
      const foundAnnotation = await new AnnotationService(context).findById(annotation.id);
      if (USE_MOCKS) {
        expect(findByIdMock).toBeCalledTimes(1);
      }
      expect(foundAnnotation.id).toBe(annotation.id);
    });
  });
});
