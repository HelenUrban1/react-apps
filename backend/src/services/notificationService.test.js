const config = require('../../config')();

const USE_MOCKS = config.useMocks;

if (USE_MOCKS) {
  jest.doMock('../database/models');
  jest.doMock('../database/repositories/notificationRepository');
}

const models = require('../database/models');
const NotificationRepository = require('../database/repositories/notificationRepository');
const SequelizeRepository = require('../database/repositories/sequelizeRepository');
const NotificationService = require('./notificationService');
const { data } = require('../__fixtures__').notification;

describe('NotificationService', () => {
  const context = { currentUser: { accountId: config.testAccountId } };

  const createMock = jest.fn().mockReturnValue(data[0]);
  const createInAccountMock = jest.fn().mockReturnValue(data[0]);
  const updateMock = jest.fn().mockReturnValue(data[0]);
  const findAndCountAllMock = jest
    .fn()
    .mockReturnValueOnce({
      rows: [data[0]],
      count: 1,
    })
    .mockReturnValueOnce({ rows: [], count: 0 });
  const countMock = jest.fn().mockReturnValueOnce(1).mockReturnValueOnce(0);
  const findByIdMock = jest.fn().mockReturnValue(data[0]);
  const destroyMock = jest.fn();

  beforeEach(async () => {
    if (USE_MOCKS) {
      models.getTenant = () =>
        Promise.resolve({
          sequelize: {
            transaction: jest.fn().mockReturnValue({
              rollback: jest.fn(),
              commit: jest.fn(),
            }),
          },
        });

      models.sequelizeAdmin = {
        query: jest.fn().mockReturnValue(Promise.resolve({})),
      };

      NotificationRepository.mockImplementation(() => {
        return {
          create: createMock,
          createInAccount: createInAccountMock,
          update: updateMock,
          findAndCountAll: findAndCountAllMock,
          count: countMock,
          findById: findByIdMock,
          destroy: destroyMock,
        };
      });

      SequelizeRepository.commitTransaction = jest.fn();
    } else {
      await Promise.all([
        SequelizeRepository.cleanDatabase(config.testAccountId), //
      ]);
    }
  });

  afterAll(async () => {
    if (!USE_MOCKS) {
      await Promise.all([
        SequelizeRepository.closeConnections(config.testAccountId), //
        SequelizeRepository.closeConnections(),
      ]);
    }
  });

  describe('create()', () => {
    it('Should return notification data', async () => {
      try {
        const notification = await new NotificationService(context).create(data[0]);
        if (USE_MOCKS) {
          expect(createMock).toBeCalledTimes(1);
        }
        expect(notification).toBeTruthy();
      } catch (exception) {
        // eslint-disable-next-line no-console
        console.error(exception);
        expect(exception).toBeFalsy();
      }
    });
  });

  describe('update()', () => {
    it('Should update notification data', async () => {
      const notification = await new NotificationService(context).create(data[0]);
      notification.title = 'New Title';
      const updatedNotification = await new NotificationService(context).update(notification.id, notification);
      if (USE_MOCKS) {
        expect(updateMock).toBeCalledTimes(1);
      }
      expect(updatedNotification.title).toBe('New Title');
    });
  });

  describe('destroyAll()', () => {
    it('Should delete a reviewItem by id', async () => {
      const notification = await new NotificationService(context).create(data[0]);
      const all = await new NotificationService(context).count();
      expect(all).toBe(1);

      const context2 = { currentUser: { accountId: config.testAccountId } };
      await new NotificationService(context2).destroyAll([notification.id]);
      const none = await new NotificationService(context2).count();
      if (USE_MOCKS) {
        expect(destroyMock).toBeCalledTimes(1);
      }
      expect(none).toBe(0);
    });
  });

  describe('findById()', () => {
    it('Should find notification data by id', async () => {
      const notification = await new NotificationService(context).create(data[0]);
      const foundNotification = await new NotificationService(context).findById(notification.id);
      if (USE_MOCKS) {
        expect(findByIdMock).toBeCalledTimes(1);
      }
      expect(foundNotification.id).toBe(notification.id);
    });
  });
});
