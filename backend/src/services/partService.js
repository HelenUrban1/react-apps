const moment = require('moment');
const PartRepository = require('../database/repositories/partRepository');
const ValidationError = require('../errors/validationError');
const SequelizeRepository = require('../database/repositories/sequelizeRepository');
const ListItemService = require('./listItemService');

/**
 * Handles Part operations
 */
module.exports = class PartService {
  constructor({ currentUser, language, eventEmitter }) {
    this.repository = new PartRepository();
    this.currentUser = currentUser;
    this.language = language;
    this.eventEmitter = eventEmitter;
  }

  /**
   * Creates a Part.
   *
   * @param {*} data
   */
  async create(data) {
    const transaction = await SequelizeRepository.createTransaction(this.currentUser.accountId);

    try {
      const record = await this.repository.create(data, {
        transaction,
        currentUser: this.currentUser,
      });

      await SequelizeRepository.commitTransaction(transaction);

      if (this.eventEmitter) {
        this.eventEmitter.emit('part.create', { accountId: this.currentUser.accountId, siteId: this.currentUser.siteId, data: record });
      }

      return record;
    } catch (error) {
      await SequelizeRepository.rollbackTransaction(transaction);
      throw error;
    }
  }

  /**
   * Updates a Part.
   *
   * @param {*} id
   * @param {*} data
   */
  async update(id, data) {
    const transaction = await SequelizeRepository.createTransaction(this.currentUser.accountId);

    try {
      const record = await this.repository.update(id, data, {
        transaction,
        currentUser: this.currentUser,
      });

      await SequelizeRepository.commitTransaction(transaction);

      if (this.eventEmitter) {
        this.eventEmitter.emit('part.update', { accountId: this.currentUser.accountId, siteId: this.currentUser.siteId, data: record });
      }

      return record;
    } catch (error) {
      await SequelizeRepository.rollbackTransaction(transaction);
      throw error;
    }
  }

  /**
   * Updates a Part for a tenant using the specified account id.
   * @param {string} id
   * @param {string} accountId
   * @param {Object} data
   */
  async updateForUserAndTentant(id, accountId, siteId, data) {
    const transaction = await SequelizeRepository.createTransaction(accountId);

    try {
      const record = await this.repository.updateForUserAndTenant(id, accountId, data, {
        transaction,
        currentUser: this.currentUser,
      });

      await SequelizeRepository.commitTransaction(transaction);

      if (this.eventEmitter) {
        this.eventEmitter.emit('part.update', { accountId, siteId, data: record });
      }

      return record;
    } catch (error) {
      await SequelizeRepository.rollbackTransaction(transaction);
      throw error;
    }
  }

  /**
   * Destroy all Parts with those ids.
   *
   * @param {*} ids
   */
  async destroyAll(ids) {
    const transaction = await SequelizeRepository.createTransaction(this.currentUser.accountId);

    try {
      for (const id of ids) {
        await this.repository.destroy(id, {
          transaction,
          currentUser: this.currentUser,
        });
      }

      await SequelizeRepository.commitTransaction(transaction);

      if (this.eventEmitter) {
        this.eventEmitter.emit('part.destroyAll', { accountId: this.currentUser.accountId, siteId: this.currentUser.siteId, data: ids });
      }
    } catch (error) {
      await SequelizeRepository.rollbackTransaction(transaction);
      throw error;
    }
  }

  /**
   * Finds the Part by Id.
   *
   * @param {*} id
   */
  async findById(id) {
    return this.repository.findById(id, {
      currentUser: this.currentUser,
    });
  }

  /**
   * Finds Parts for Autocomplete.
   *
   * @param {*} search
   * @param {*} limit
   */
  async findAllAutocomplete(search, limit) {
    return this.repository.findAllAutocomplete(search, limit, {
      currentUser: this.currentUser,
    });
  }

  /**
   * Finds Parts based on the query.
   *
   * @param {*} args
   */
  async findAndCountAll(args) {
    return this.repository.findAndCountAll(args, {
      currentUser: this.currentUser,
    });
  }

  /**
   * Imports a list of Parts.
   *
   * @param {*} data
   * @param {*} importHash
   */
  async import(data, importHash) {
    if (!importHash) {
      throw new ValidationError(this.language, 'importer.errors.importHashRequired');
    }

    if (await this._isImportHashExistent(importHash)) {
      throw new ValidationError(this.language, 'importer.errors.importHashExistent');
    }

    const dataToCreate = {
      ...data,
      importHash,
    };

    return this.create(dataToCreate);
  }

  /**
   * Checks if the import hash already exists.
   * Every item imported has a unique hash.
   *
   * @param {*} importHash
   */
  async _isImportHashExistent(importHash) {
    const count = await this.repository.count({
      importHash,
    });

    return count > 0;
  }

  static getCurrentDrawings(drawings, partDrawingId) {
    const currentDrawings = [];
    drawings.forEach((drawing) => {
      // filter out old revisions from drawing list unless its the partDrawing
      const rev = currentDrawings.findIndex((draw) => draw.originalDrawing === drawing.originalDrawing);
      if (rev >= 0) {
        if (drawing.id === partDrawingId || (currentDrawings[rev].id !== partDrawingId && moment(drawing.createdAt).isAfter(moment(currentDrawings[rev].createdAt)))) {
          currentDrawings[rev] = drawing;
        }
      } else {
        currentDrawings.push(drawing);
      }
    });
    return currentDrawings.sort((a, b) => (moment(a.createdAt).isAfter(moment(b.createdAt)) ? -1 : 1));
  }

  async findByIdForExport(id) {
    const listItemService = new ListItemService({ currentUser: this.currentUser });

    const listItems = await listItemService.findAndCountAll();

    const listItemStore = {};
    for (let i = 0; i < listItems.rows.length; i++) {
      const listItem = listItems.rows[i];
      listItemStore[listItem.id] = listItem.name;
    }
    const part = await this.findById(id);
    if (part && part.characteristics) {
      const loadedDrawings = part && part.drawings ? this.getCurrentDrawings(part.drawings).map((draw) => draw.id) : [part.primaryDrawing.id];

      const loadedFeatures = [...part.characteristics].filter((feat) => loadedDrawings.includes(feat.drawing.id));

      part.characteristics = loadedFeatures.map((item) => {
        item.notationType = listItemStore[item.notationType];
        item.notationSubtype = listItemStore[item.notationSubtype];
        item.unit = listItemStore[item.unit];
        return item;
      });
    }
    return part;
  }
};
