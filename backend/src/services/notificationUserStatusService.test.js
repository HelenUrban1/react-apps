const config = require('../../config')();

const USE_MOCKS = config.useMocks;

if (USE_MOCKS) {
  jest.doMock('../database/models');
  jest.doMock('../database/repositories/notificationUserStatusRepository');
}

const models = require('../database/models');
const NotificationUserStatusRepository = require('../database/repositories/notificationUserStatusRepository');
const SequelizeRepository = require('../database/repositories/sequelizeRepository');
const NotificationUserStatusService = require('./notificationUserStatusService');
const { data } = require('../__fixtures__').notificationUserStatus;
const NotificationService = require('./notificationService');
const { data: notificationData } = require('../__fixtures__').notification;

describe('NotificationUserStatusService', () => {
  const context = { currentUser: { accountId: config.testAccountId } };

  const createMock = jest.fn().mockReturnValue(data[0]);
  const createInAccountMock = jest.fn().mockReturnValue(data[0]);
  const updateMock = jest.fn().mockReturnValue(data[0]);
  const findAndCountAllMock = jest
    .fn()
    .mockReturnValueOnce({
      rows: [data[0]],
      count: 1,
    })
    .mockReturnValueOnce({ rows: [], count: 0 });
  const findByIdMock = jest.fn().mockReturnValue(data[0]);
  const destroyMock = jest.fn();

  beforeEach(async () => {
    if (USE_MOCKS) {
      models.getTenant = () =>
        Promise.resolve({
          sequelize: {
            transaction: jest.fn().mockReturnValue({
              rollback: jest.fn(),
              commit: jest.fn(),
            }),
          },
        });

      models.sequelizeAdmin = {
        query: jest.fn().mockReturnValue(Promise.resolve({})),
      };

      NotificationUserStatusRepository.mockImplementation(() => {
        return {
          create: createMock,
          createInAccount: createInAccountMock,
          update: updateMock,
          findAndCountAll: findAndCountAllMock,
          findById: findByIdMock,
          destroy: destroyMock,
        };
      });

      SequelizeRepository.commitTransaction = jest.fn();
    } else {
      await Promise.all([
        SequelizeRepository.cleanDatabase(config.testAccountId), //
      ]);

      // Need a notifiation to hook the UserStatuses to
      await new NotificationService(context).create(notificationData[0]);
    }
  });

  afterAll(async () => {
    if (!USE_MOCKS) {
      await Promise.all([
        SequelizeRepository.closeConnections(config.testAccountId), //
        SequelizeRepository.closeConnections(),
      ]);
    }
  });

  describe('create()', () => {
    it('Should return notificationUserStatus data', async () => {
      try {
        const notificationUserStatus = await new NotificationUserStatusService(context).create(data[0]);
        if (USE_MOCKS) {
          expect(createMock).toBeCalledTimes(1);
        }
        expect(notificationUserStatus).toBeTruthy();
      } catch (exception) {
        // eslint-disable-next-line no-console
        console.error(exception);
        expect(exception).toBeFalsy();
      }
    });
  });

  describe('update()', () => {
    it('Should update notificationUserStatus data', async () => {
      const notificationUserStatus = await new NotificationUserStatusService(context).create(data[0]);
      const readDate = new Date();
      notificationUserStatus.readAt = readDate;
      const updatedNotificationUserStatus = await new NotificationUserStatusService(context).update(notificationUserStatus.id, notificationUserStatus);
      if (USE_MOCKS) {
        expect(updateMock).toBeCalledTimes(1);
      }
      expect(updatedNotificationUserStatus.readAt).toStrictEqual(readDate);
    });
  });

  describe('destroyAll()', () => {
    it('Should delete a reviewItem by id', async () => {
      const notificationUserStatus = await new NotificationUserStatusService(context).create(data[0]);
      const all = await new NotificationUserStatusService(context).findAndCountAll();
      expect(all.count).toBe(1);
      expect(all.rows).toBeTruthy();

      const context2 = { currentUser: { accountId: config.testAccountId } };
      await new NotificationUserStatusService(context2).destroyAll([notificationUserStatus.id]);
      const none = await new NotificationUserStatusService(context2).findAndCountAll();
      if (USE_MOCKS) {
        expect(destroyMock).toBeCalledTimes(1);
      }
      expect(none.count).toBe(0);
      expect(none.rows).toStrictEqual([]);
    });
  });

  describe('findById()', () => {
    it('Should find notificationUserStatus data by id', async () => {
      const notificationUserStatus = await new NotificationUserStatusService(context).create(data[0]);
      const foundNotificationUserStatus = await new NotificationUserStatusService(context).findById(notificationUserStatus.id);
      if (USE_MOCKS) {
        expect(findByIdMock).toBeCalledTimes(1);
      }
      expect(foundNotificationUserStatus.id).toBe(notificationUserStatus.id);
    });
  });
});
