const config = require('../../config')();
const USE_MOCKS = config.useMocks;

if (USE_MOCKS) {
  jest.doMock('../database/models');
  jest.doMock('../database/repositories/measurementRepository');
}

const models = require('../database/models');
const MeasurementRepository = require('../database/repositories/measurementRepository');
const MeasurementService = require('./measurementService');
const SequelizeRepository = require('../database/repositories/sequelizeRepository');

describe('MeasurementService', () => {
  const accountId = config.testAccountId;
  const data = {
    value: 'Test Measurement',
    status: 'Unmeasured',
    createdBy: 'e0a01769-c8ed-4090-bd24-9c1b2241fb75',
    updatedBy: 'e0a01769-c8ed-4090-bd24-9c1b2241fb75',
  };

  const createMock = jest.fn().mockReturnValue(data);
  const updateMock = jest.fn().mockReturnValue({ ...data, value: 'foo' });

  beforeEach(async () => {
    if (USE_MOCKS) {
      models.getTenant = () =>
        Promise.resolve({
          sequelize: {
            transaction: jest.fn().mockReturnValue({
              rollback: jest.fn(),
              commit: jest.fn(),
            }),
          },
        });

      models.sequelizeAdmin = {
        query: jest.fn().mockReturnValue(Promise.resolve({})),
      };

      MeasurementRepository.mockImplementation(() => {
        return {
          create: createMock,
          update: updateMock,
        };
      });

      SequelizeRepository.commitTransaction = jest.fn();
    }
  });

  afterAll(async () => {
    if (!USE_MOCKS) {
      await SequelizeRepository.closeConnections();
      await SequelizeRepository.closeConnections(accountId);
    }
  });

  describe('Create Measurement', () => {
    const context = { currentUser: { accountId } };

    it('Should return measurement data', async () => {
      try {
        const measurement = await new MeasurementService(context).create(data);
        if (USE_MOCKS) {
          expect(createMock).toBeCalledTimes(1);
        }
        expect(measurement).toBeTruthy();
      } catch (exception) {
        console.error(exception);
        expect(exception).toBeFalsy();
      }
    }, 10000);
  });

  describe('Update Measurement', () => {
    const context = { currentUser: { accountId } };

    it('Should return updated measurement data', async () => {
      try {
        const measurement = await new MeasurementService(context).create(data);
        expect(measurement).toBeTruthy();

        const measurementAgain = await new MeasurementService(context).update(measurement.id, { value: 'foo' });

        if (USE_MOCKS) {
          expect(updateMock).toBeCalledTimes(1);
        }
        expect(measurementAgain.value).toEqual('foo');
      } catch (exception) {
        console.error(exception);
        expect(exception).toBeFalsy();
      }
    }, 10000);
  });
});
