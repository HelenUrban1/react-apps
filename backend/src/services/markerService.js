const MarkerRepository = require('../database/repositories/markerRepository');
const ValidationError = require('../errors/validationError');
const SequelizeRepository = require('../database/repositories/sequelizeRepository');

/**
 * Handles Marker operations
 */
module.exports = class MarkerService {
  constructor({ currentUser, language }) {
    this.repository = new MarkerRepository();
    this.currentUser = currentUser;
    this.language = language;
  }

  /**
   * Creates a Marker.
   *
   * @param {*} data
   */
  async create(data) {
    const transaction = await SequelizeRepository.createTransaction(this.currentUser.accountId);

    try {
      const record = await this.repository.create(data, {
        transaction,
        currentUser: this.currentUser,
      });

      await SequelizeRepository.commitTransaction(transaction);

      return record;
    } catch (error) {
      await SequelizeRepository.rollbackTransaction(transaction);
      throw error;
    }
  }

  async createDefaults(accountId) {
    const transaction = await SequelizeRepository.createTransaction(accountId);

    try {
      const ids = [
        '607eab44-b7d9-4d19-9ad7-78d0f5425944',
        'f12c5566-7612-48e9-9534-ef90b276ebaa',
        'ca02e6c9-646c-49b4-a998-7c2c7d6292d3',
        '3d3f1c44-d73e-456f-8f35-384a52cf7fb4',
        'c623219f-4466-4be3-a95b-2bd8e9ffbea6',
        '52a9b677-58ff-43ea-8258-79ff5aec89dd',
        '79b34eb7-fa0e-4f2f-bcdd-2b15f37d3c56',
        '56a15ab5-1679-4cf5-851b-038e78f74515',
        'b28ce349-8be7-4c42-b771-fa3206cb2cdb',
        '0501de9d-b041-426a-b17a-2d8ce85c7b26',
        'd276af65-0938-4e25-aa28-6f12273574ce',
        'c35ad2d2-c045-4430-9740-5041ea309b12',
        '9e45a1ae-49b0-4421-977a-36ba9870ea3d',
        'a2e7799c-8675-4f2e-a5e8-7b3628c08f7b',
        '5b48ed02-76cb-46c0-b017-da8060636817',
        'aad19683-337a-4039-a910-98b0bdd5ee57',
        '4a14b684-6b0e-48bf-8109-d21cb63942cf',
        '9c115936-b81a-42ef-b5d8-54b2e4d7e036',
        '8d1a8859-c1bc-4c90-b7c6-2bf1640856ac',
        '7886bc31-b8cf-4453-8a6a-e0eed8fcf27b',
        '25750225-6d8b-4469-acad-a99c217fa8ba',
        '2d69348a-f26b-4775-bb63-5b54b6b31d3c',
        '6554ebf7-ae05-4527-adc1-6edd38b8974d',
        'c858d5fb-5fc2-41f3-b4a6-5aa09ec27307',
        'e0370cfc-a975-49e9-a17a-ba946f2ba7d3',
        '2da1eab7-608a-4d8a-bfc3-fe874261b6f5',
        'f1386f09-9c03-4c59-ada9-acb6a16bfbab',
        '87a7dbdf-282d-429e-aa56-df38a60a966b',
        '8ccdc6b1-842a-432c-b84d-47862863de9b',
        '7b0b3d84-cd81-4c39-bd77-af2c3c34f7ce',
      ];
      const currentDate = Date.now();
      const defaultProps = {
        markerStyleName: 'Default',
        defaultPosition: 'Left',
        hasLeaderLine: false,
        fontSize: 10,
        borderWidth: 1,
        leaderLineWidth: 1,
        createdAt: currentDate,
        updatedAt: currentDate,
      };
      const shapes = ['Circle', 'Triangle', 'Diamond'];
      const themes = [
        {
          fontColor: 'White',
          fillColor: 'Purple',
          borderColor: 'Purple',
          leaderLineColor: 'Purple',
        },
        {
          fontColor: 'White',
          fillColor: 'Coral',
          borderColor: 'Coral',
          leaderLineColor: 'Coral',
        },
        {
          fontColor: 'White',
          fillColor: 'Seafoam',
          borderColor: 'Seafoam',
          leaderLineColor: 'Seafoam',
        },
        {
          fontColor: 'White',
          fillColor: 'Sunshine',
          borderColor: 'Sunshine',
          leaderLineColor: 'Sunshine',
        },
        {
          fontColor: 'White',
          fillColor: 'Black',
          borderColor: 'Black',
          leaderLineColor: 'Black',
        },
        {
          fontColor: 'Purple',
          fillColor: 'White',
          borderColor: 'Purple',
          leaderLineColor: 'Purple',
        },
        {
          fontColor: 'Coral',
          fillColor: 'White',
          borderColor: 'Coral',
          leaderLineColor: 'Coral',
        },
        {
          fontColor: 'Seafoam',
          fillColor: 'White',
          borderColor: 'Seafoam',
          leaderLineColor: 'Seafoam',
        },
        {
          fontColor: 'Sunshine',
          fillColor: 'White',
          borderColor: 'Sunshine',
          leaderLineColor: 'Sunshine',
        },
        {
          fontColor: 'Black',
          fillColor: 'White',
          borderColor: 'Black',
          leaderLineColor: 'Black',
        },
      ];

      const accountUser = {
        accountId,
      };

      let i = 0;
      const markers = [];
      for (let s = 0; s < shapes.length; s++) {
        const shape = shapes[s];
        for (let t = 0; t < themes.length; t++) {
          const theme = themes[t];
          const markerId = ids[i];
          const newMarker = { ...defaultProps };
          newMarker.shape = shape;
          newMarker.fontColor = theme.fontColor;
          newMarker.fillColor = theme.fillColor;
          newMarker.borderColor = theme.borderColor;
          newMarker.leaderLineColor = theme.leaderLineColor;
          newMarker.id = markerId;
          markers.push(
            this.repository.create(newMarker, {
              transaction,
              currentUser: accountUser,
            })
          );
          i += 1;
        }
      }
      await Promise.all(markers);

      await SequelizeRepository.commitTransaction(transaction);
    } catch (error) {
      await SequelizeRepository.rollbackTransaction(transaction);
      throw error;
    }
  }

  /**
   * Updates a Marker.
   *
   * @param {*} id
   * @param {*} data
   */
  async update(id, data) {
    const transaction = await SequelizeRepository.createTransaction(this.currentUser.accountId);

    try {
      const record = await this.repository.update(id, data, {
        transaction,
        currentUser: this.currentUser,
      });

      await SequelizeRepository.commitTransaction(transaction);

      return record;
    } catch (error) {
      await SequelizeRepository.rollbackTransaction(transaction);
      throw error;
    }
  }

  /**
   * Destroy all Markers with those ids.
   *
   * @param {*} ids
   */
  async destroyAll(ids) {
    const transaction = await SequelizeRepository.createTransaction(this.currentUser.accountId);

    try {
      for (const id of ids) {
        await this.repository.destroy(id, {
          transaction,
          currentUser: this.currentUser,
        });
      }

      await SequelizeRepository.commitTransaction(transaction);
    } catch (error) {
      await SequelizeRepository.rollbackTransaction(transaction);
      throw error;
    }
  }

  /**
   * Finds the Marker by Id.
   *
   * @param {*} id
   */
  async findById(id) {
    return this.repository.findById(id, {
      currentUser: this.currentUser,
    });
  }

  /**
   * Finds Markers for Autocomplete.
   *
   * @param {*} search
   * @param {*} limit
   */
  async findAllAutocomplete(search, limit) {
    return this.repository.findAllAutocomplete(search, limit, {
      currentUser: this.currentUser,
    });
  }

  /**
   * Finds Markers based on the query.
   *
   * @param {*} args
   */
  async findAndCountAll(args) {
    return this.repository.findAndCountAll(args, {
      currentUser: this.currentUser,
    });
  }

  /**
   * Imports a list of Markers.
   *
   * @param {*} data
   * @param {*} importHash
   */
  async import(data, importHash) {
    if (!importHash) {
      throw new ValidationError(this.language, 'importer.errors.importHashRequired');
    }

    if (await this._isImportHashExistent(importHash)) {
      throw new ValidationError(this.language, 'importer.errors.importHashExistent');
    }

    const dataToCreate = {
      ...data,
      importHash,
    };

    return this.create(dataToCreate);
  }

  /**
   * Checks if the import hash already exists.
   * Every item imported has a unique hash.
   *
   * @param {*} importHash
   */
  async _isImportHashExistent(importHash) {
    const count = await this.repository.count({
      importHash,
    });

    return count > 0;
  }
};
