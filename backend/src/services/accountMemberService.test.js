const SequelizeRepository = require('../database/repositories/sequelizeRepository');
const UserRepository = require('../database/repositories/userRepository');
const AccountMemberService = require('./accountMemberService');
const { data } = require('../__fixtures__').accountMember;
const userData = require('../__fixtures__').user.data;
const { createUserWithAccountAssociation } = require('../database/repositories/repositoryTestUtils');
const constants = require('../database/constants').accountMember;
const config = require('../../config')();

const roles = {};
constants.ROLES.forEach((role) => (roles[role] = role));
const options = { currentUser: { accountId: config.testAccountId } };

describe('AccountMemberService', () => {
  let accountMemberService;
  let billingClientMock;

  beforeEach(async () => {
    await SequelizeRepository.cleanDatabase();

    billingClientMock = {
      getCustomer: jest.fn(),
      updateCustomer: jest.fn(),
      createCustomer: jest.fn(),
    };
    billingClientMock.getCustomer.mockResolvedValue({
      id: '123',
    });
    billingClientMock.createCustomer.mockResolvedValue({
      id: '123',
      email: 'a@a.org',
    });

    accountMemberService = new AccountMemberService({
      billingClient: billingClientMock,
    });
  });

  afterAll(async () => {
    await SequelizeRepository.closeConnections();
  });

  describe('#create()', () => {
    it('should create a record that is associated with the specified user', async () => {
      const user = await UserRepository.create(userData[0], options);

      const fixture = JSON.parse(JSON.stringify(data.accountAUserCOwner));
      fixture.user = user.id;

      const accountMember = await accountMemberService.create(fixture);
      expect(accountMember).toBeDefined();
      expect(accountMember.userId).toBe(user.id);
    });

    it('should create a Chargify cutomer when creating the first member of an account', async () => {
      const user = await UserRepository.create(userData[0], options);

      const fixture = JSON.parse(JSON.stringify(data.accountAUserCOwner));
      fixture.user = user.id;

      await accountMemberService.create(fixture);

      const chargifyCall = billingClientMock.createCustomer.mock.calls[0][0];
      expect(chargifyCall.reference).toBe(user.id);
      expect(chargifyCall.email).toBe(user.email);
      // TODO: get first and last name from userProfile
      // expect(chargifyCall.firstName).toBe(user.firstName);
      // expect(chargifyCall.lastName).toBe(user.lastName);
    });
  });

  // handling of account ownership transfer goes through IamEditor.js
  describe.skip('#changeAccountOwnerFromCurrentUser()', () => {
    it('should change the Account Owner from the current user to a new user', async () => {
      let result = await createUserWithAccountAssociation(
        {
          accountMemberFixture: data.accountAUserCOwner,
          userFixture: userData[0],
        },
        options
      );
      const currentOwner = result.user;
      const currentOwnerAccountMember = result.accountMember;
      expect(currentOwnerAccountMember.roles.includes(roles.Owner)).toBe(true);

      result = await createUserWithAccountAssociation(
        {
          accountMemberFixture: data.accountAUserAPaidRoles,
          userFixture: userData[1],
        },
        options
      );
      const newOwner = result.user;
      const newOwnerAccountMember = result.accountMember;
      expect(newOwnerAccountMember.roles.includes(roles.Owner)).toBe(false);

      await accountMemberService.changeAccountOwnerFromCurrentUser(
        {
          newOwnerAccountMemberId: newOwnerAccountMember.id,
        },
        {
          currentUser: currentOwner,
        }
      );

      const updatedOriginalOwnerAccountMember = await accountMemberService.findById(currentOwnerAccountMember.id);
      expect(updatedOriginalOwnerAccountMember.roles.includes(roles.Owner)).toBe(false);

      const updatedNewOwnerAccountMember = await accountMemberService.findById(newOwnerAccountMember.id);
      expect(updatedNewOwnerAccountMember.roles.includes(roles.Owner)).toBe(true);

      const chargifyCall = billingClientMock.updateCustomer.mock.calls[0][0];
      expect(chargifyCall.reference).toBe(newOwner.id);
      expect(chargifyCall.email).toBe(newOwner.email);
      // TODO: Get first and last name from userProfile
      // expect(chargifyCall.firstName).toBe(newOwner.firstName);
      // expect(chargifyCall.lastName).toBe(newOwner.lastName);
    });
  });
});
