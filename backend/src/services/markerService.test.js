const config = require('../../config')();
const USE_MOCKS = config.useMocks;

if (USE_MOCKS) {
  jest.doMock('../database/models');
  jest.doMock('../database/repositories/markerRepository');
}

const models = require('../database/models');
const MarkerRepository = require('../database/repositories/markerRepository');

const MarkerService = require('./markerService');
const SequelizeRepository = require('../database/repositories/sequelizeRepository');

const createMock = jest.fn().mockReturnValue({});
const findAndCountAllMock = jest.fn().mockReturnValueOnce({ rows: [], count: 0 }).mockReturnValueOnce({ rows: [], count: 30 });

describe('MarkerService', () => {
  const accountId = config.testAccountId;

  beforeEach(async () => {
    if (USE_MOCKS) {
      models.getTenant = () =>
        Promise.resolve({
          sequelize: {
            transaction: jest.fn().mockReturnValue({
              rollback: jest.fn(),
              commit: jest.fn(),
            }),
          },
        });

      models.sequelizeAdmin = {
        query: jest.fn().mockReturnValue(Promise.resolve({})),
      };

      MarkerRepository.mockImplementation(() => {
        return {
          create: createMock,
          findAndCountAll: findAndCountAllMock,
        };
      });

      SequelizeRepository.commitTransaction = jest.fn();
    }
  });

  describe('#createDefaults', () => {
    afterAll(async () => {
      if (!USE_MOCKS) {
        await SequelizeRepository.closeConnections();
        await SequelizeRepository.closeConnections(accountId);
      }
    });

    const context = { currentUser: { accountId } };
    it('Should create 30 default markers', async () => {
      try {
        const markerService = new MarkerService(context);
        const check = await markerService.findAndCountAll({});
        expect(check.count).toBe(0);
        await markerService.createDefaults(accountId);
        const markers = await markerService.findAndCountAll({});
        if (USE_MOCKS) {
          expect(createMock).toBeCalledTimes(30);
        }
        expect(markers.count).toBe(30);
      } catch (exception) {
        console.error(exception);
        expect(exception).toBeFalsy();
      }
    });
  });
});
