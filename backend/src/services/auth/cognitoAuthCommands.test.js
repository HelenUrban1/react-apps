const CognitoAuthCommands = require('./cognitoAuthCommands');

describe('CognitoAuthCommands', () => {
  const cognitoClient = {
    send: jest.fn(),
  };
  const email = 'fake@fake.com';
  const password = 'Fakeword2@';
  const cognitoEmail = 'fake__at__fake__dot__com';
  const accessToken = { AccessToken: '123456' };
  const friendlyDeviceName = 'device';
  const code = '654321';
  const refreshToken = 'refresh-token';
  const deviceKey = 'abcdef';

  describe('Get User Command', () => {
    it('catches an error if cognito throws one', async () => {
      cognitoClient.send.mockImplementation(() => {
        throw new Error('Test');
      });

      expect(CognitoAuthCommands.cognitoGetUser('fake@fake.com', { cognitoClient })).rejects.toThrow();
    });
  });

  describe('Enable User Command', () => {
    it('catches an error if cognito throws one', async () => {
      const mockGetUser = jest.spyOn(CognitoAuthCommands, 'cognitoGetUser');
      mockGetUser.mockReturnValue({ enabled: false });

      cognitoClient.send.mockImplementation(() => {
        throw new Error('Test');
      });

      expect(CognitoAuthCommands.cognitoEnableUser(email, { cognitoClient })).rejects.toThrow();
    });

    it('returns true if user is already enabled', async () => {
      const mockGetUser = jest.spyOn(CognitoAuthCommands, 'cognitoGetUser');
      mockGetUser.mockReturnValue({ enabled: true });

      cognitoClient.send.mockImplementation(() => {
        return true;
      });

      const res = await CognitoAuthCommands.cognitoEnableUser(email, { cognitoClient });
      expect(res).toBeTruthy();
    });

    it('returns a response if user was disabled', async () => {
      const mockGetUser = jest.spyOn(CognitoAuthCommands, 'cognitoGetUser');
      mockGetUser.mockReturnValue({ enabled: true });

      cognitoClient.send.mockImplementation(() => {
        return { username: cognitoEmail };
      });

      const res = await CognitoAuthCommands.cognitoEnableUser(email, { cognitoClient });
      expect(res.username).toBe(cognitoEmail);
    });
  });

  describe('Disable User Command', () => {
    it('catches an error if cognito throws one', async () => {
      cognitoClient.send.mockImplementation(() => {
        throw new Error('Test');
      });

      expect(CognitoAuthCommands.cognitoDisableUser(email, { cognitoClient })).rejects.toThrow();
    });

    it('returns a response when successfully disabled', async () => {
      cognitoClient.send.mockImplementation(() => {
        return { enabled: false };
      });

      const res = await CognitoAuthCommands.cognitoDisableUser(email, { cognitoClient });
      expect(res.enabled).toBeFalsy();
    });
  });

  describe('Update User Command', () => {
    it('catches an error if cognito throws one', async () => {
      cognitoClient.send.mockImplementation(() => {
        throw new Error('Test');
      });

      expect(CognitoAuthCommands.cognitoUpdateUserAttributes(email, [], { cognitoClient })).rejects.toThrow();
    });

    it('returns a response when successfully disabled', async () => {
      cognitoClient.send.mockImplementation(() => {
        return { UserAttributes: { 'custom:accountId': 'account-id' } };
      });

      const res = await CognitoAuthCommands.cognitoUpdateUserAttributes(email, [{ Name: 'custom:accountId', Value: 'account-id' }], { cognitoClient });
      expect(res.UserAttributes['custom:accountId']).toBe('account-id');
    });
  });

  describe('Associate Software Token Command', () => {
    it('catches an error if cognito throws one', async () => {
      cognitoClient.send.mockImplementation(() => {
        throw new Error('Test');
      });

      expect(CognitoAuthCommands.cognitoAssociateSoftwareToken(accessToken, { cognitoClient })).rejects.toThrow();
    });

    it('returns a response when successfully disabled', async () => {
      cognitoClient.send.mockImplementation(() => {
        return true;
      });

      const res = await CognitoAuthCommands.cognitoAssociateSoftwareToken(accessToken, { cognitoClient });
      expect(res).toBeTruthy();
    });
  });

  describe('Verify Software Token Command', () => {
    it('catches an error if cognito throws one', async () => {
      cognitoClient.send.mockImplementation(() => {
        throw new Error('Test');
      });

      expect(CognitoAuthCommands.cognitoVerifySoftwareToken(accessToken, friendlyDeviceName, code, { cognitoClient })).rejects.toThrow();
    });

    it('returns a response when successfully disabled', async () => {
      cognitoClient.send.mockImplementation(() => {
        return true;
      });

      const res = await CognitoAuthCommands.cognitoVerifySoftwareToken(accessToken, friendlyDeviceName, code, { cognitoClient });
      expect(res).toBeTruthy();
    });
  });

  describe('Set User MFA Preference Command', () => {
    it('catches an error if cognito throws one', async () => {
      cognitoClient.send.mockImplementation(() => {
        throw new Error('Test');
      });

      expect(CognitoAuthCommands.cognitoSetUserMFAPreference(email, true, { cognitoClient })).rejects.toThrow();
    });

    it('returns a response when successfully disabled', async () => {
      cognitoClient.send.mockImplementation(() => {
        return { SoftwareTokenMfaSettings: { Enabled: true, PreferredMfa: true } };
      });

      const res = await CognitoAuthCommands.cognitoSetUserMFAPreference(email, true, { cognitoClient });
      expect(res.SoftwareTokenMfaSettings.Enabled).toBeTruthy();
    });
  });

  describe('Admin Initiate Auth Command', () => {
    it('catches an error if cognito throws one', async () => {
      cognitoClient.send.mockImplementation(() => {
        throw new Error('Test');
      });

      expect(CognitoAuthCommands.cognitoAdminInitiateAuth(refreshToken, deviceKey, { cognitoClient })).rejects.toThrow();
    });

    it('returns a response when successfully disabled', async () => {
      cognitoClient.send.mockImplementation(() => {
        return {
          AuthenticationResult: { AccessToken: accessToken },
          ChallengeName: 'challenge-name',
          ChallengeParameters: { parameter: 'param' },
        };
      });

      const res = await CognitoAuthCommands.cognitoAdminInitiateAuth(refreshToken, deviceKey, { cognitoClient });
      expect(res.AuthenticationResult.AccessToken).toBe(accessToken);
    });
  });

  describe('Initiate Auth Command', () => {
    it('catches an error if cognito throws one', async () => {
      cognitoClient.send.mockImplementation(() => {
        throw new Error('Test');
      });

      expect(CognitoAuthCommands.cognitoInitiateAuth(email, password, { cognitoClient })).rejects.toThrow();
    });

    it('returns a response when successfully disabled', async () => {
      cognitoClient.send.mockImplementation(() => {
        return true;
      });

      const res = await CognitoAuthCommands.cognitoInitiateAuth(refreshToken, password, { cognitoClient });
      expect(res).toBeTruthy();
    });
  });

  describe('Respond to Auth Challenge Command', () => {
    it('catches an error if cognito throws one', async () => {
      cognitoClient.send.mockImplementation(() => {
        throw new Error('Test');
      });

      expect(CognitoAuthCommands.cognitoRespondToAuthChallenge(email, code, {}, { cognitoClient })).rejects.toThrow();
    });

    it('returns a response when successfully disabled', async () => {
      cognitoClient.send.mockImplementation(() => {
        return true;
      });

      const res = await CognitoAuthCommands.cognitoRespondToAuthChallenge(email, code, {}, { cognitoClient });
      expect(res).toBeTruthy();
    });
  });

  describe('Update Device Status Command', () => {
    it('catches an error if cognito throws one', async () => {
      cognitoClient.send.mockImplementation(() => {
        throw new Error('Test');
      });

      expect(CognitoAuthCommands.cognitoUpdateDeviceStatus(email, deviceKey, true, { cognitoClient })).rejects.toThrow();
    });

    it('returns a response when successfully disabled', async () => {
      cognitoClient.send.mockImplementation(() => {
        return true;
      });

      const res = await CognitoAuthCommands.cognitoUpdateDeviceStatus(email, deviceKey, true, { cognitoClient });
      expect(res).toBeTruthy();
    });
  });

  describe('Confirm Device Command', () => {
    it('catches an error if cognito throws one', async () => {
      cognitoClient.send.mockImplementation(() => {
        throw new Error('Test');
      });

      expect(CognitoAuthCommands.cognitoConfirmDevice(deviceKey, accessToken, {}, { cognitoClient })).rejects.toThrow();
    });

    it('returns a response when successfully disabled', async () => {
      cognitoClient.send.mockImplementation(() => {
        return true;
      });

      const res = await CognitoAuthCommands.cognitoConfirmDevice(deviceKey, accessToken, {}, { cognitoClient });
      expect(res).toBeTruthy();
    });
  });

  describe('Forgot Password Command', () => {
    it('catches an error if cognito throws one', async () => {
      cognitoClient.send.mockImplementation(() => {
        throw new Error('Test');
      });

      expect(CognitoAuthCommands.cognitoForgotPassword('test@email.com', 'Test', { cognitoClient })).rejects.toThrow();
    });

    it('returns a response when forgot password is successfully triggered', async () => {
      cognitoClient.send.mockImplementation(() => {
        return true;
      });

      const res = await CognitoAuthCommands.cognitoForgotPassword('test@email.com', 'TestName', { cognitoClient });
      expect(res).toBeTruthy();
    });
  });

  describe('Confirm Forgot Password Command', () => {
    it('catches an error if cognito throws one', async () => {
      cognitoClient.send.mockImplementation(() => {
        throw new Error('Test');
      });

      expect(CognitoAuthCommands.cognitoConfirmForgotPassword('123456', 'test@email.com', 'NewPassword1!', 'TestName', { cognitoClient })).rejects.toThrow();
    });

    it('returns a response when new password is successfully confirmed', async () => {
      cognitoClient.send.mockImplementation(() => {
        return true;
      });

      const res = await CognitoAuthCommands.cognitoConfirmForgotPassword('123456', 'test@email.com', 'NewPassword1!', 'TestName', { cognitoClient });
      expect(res).toBeTruthy();
    });
  });
});
