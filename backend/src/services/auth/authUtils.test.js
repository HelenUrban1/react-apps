const { checkComplexity, validateDomain, convertEmailToCognitoUsername, convertCognitoUsernameToEmail } = require('./authUtils');
const ValidationError = require('../../errors/validationError');

describe('Auth Utils', () => {
  describe('Password Complexity Check', () => {
    it('enforces password complexity against requirements', () => {
      const fail1 = checkComplexity('Testings!!'); // fail number
      const fail2 = checkComplexity('Testings11'); // fail special char
      const fail3 = checkComplexity('testings1!'); // fail upper case
      const fail4 = checkComplexity('TESTINGS1!'); // fail lower case
      const fail5 = checkComplexity('Test1!'); // fail length
      const fail6 = checkComplexity('testing@inspectionxpert.com'); // fail different
      const pass = checkComplexity('Testings1!'); // passing password
      expect(fail1).toBeFalsy();
      expect(fail2).toBeFalsy();
      expect(fail3).toBeFalsy();
      expect(fail4).toBeFalsy();
      expect(fail5).toBeFalsy();
      expect(fail6).toBeFalsy();
      expect(pass).toBeTruthy();
    });
  });

  describe('Email domain validation', () => {
    it('Throws an error if the email is the wrong format', async () => {
      await expect(validateDomain('not an email', {})).rejects.toThrow(ValidationError);
    });

    it('Throws an error if no domain is found', async () => {
      await expect(validateDomain('missing.domain.com', {})).rejects.toThrow(ValidationError);
    });

    it('Returns Freemail if its a free email service', async () => {
      const policy = await validateDomain('test@gmail.com', {});
      expect(policy).toBe('Freemail');
    });
  });

  it('Converts email addresses into cognito usernames', () => {
    const username = convertEmailToCognitoUsername('test+1@qualityxpert.com');
    expect(username).toBe('test__plus__1__at__qualityxpert__dot__com');

    const username2 = convertEmailToCognitoUsername('dev.mail.monkey+1@gmail.com');
    expect(username2).toBe('dev__dot__mail__dot__monkey__plus__1__at__gmail__dot__com');
  });

  it('Converts cognito usernames into email addresses', () => {
    let email = convertCognitoUsernameToEmail('test__plus__1__at__qualityxpert__dot__com');
    expect(email).toBe('test+1@qualityxpert.com');

    email = convertCognitoUsernameToEmail('dev__dot__mail__dot__monkey__plus__1__at__gmail__dot__com');
    expect(email).toBe('dev.mail.monkey+1@gmail.com');
  });
});
