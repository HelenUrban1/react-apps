const assert = require('assert');
const UserRepository = require('../../database/repositories/userRepository');
const SequelizeRepository = require('../../database/repositories/sequelizeRepository');
const AccountService = require('../accountService');

/**
 * Handles the update of the user profile.
 */
module.exports = class AuthProfileEditor {
  constructor(currentUser, language) {
    this.currentUser = currentUser;
    this.language = language;

    this.transaction = null;
  }

  /**
   * Executes the user update.
   *
   * @param {*} data
   */
  async execute(data, options = {}) {
    this.data = data;

    await this._validate();

    try {
      this.transaction = await SequelizeRepository.createTransaction();

      await this._loadUser();
      await this._updateAtDatabase(options);

      await SequelizeRepository.commitTransaction(this.transaction);
    } catch (error) {
      await SequelizeRepository.rollbackTransaction(this.transaction);
      throw error;
    }
  }

  /**
   * Loads the user.
   */
  async _loadUser() {
    this.user = await UserRepository.findById(this.currentUser.id, { transaction: this.transaction });
  }

  /**
   * Updates the user at the database.
   */
  async _updateAtDatabase(options = {}) {
    this.user = await UserRepository.updateProfile(this.currentUser.id, this.data, {
      currentUser: this.currentUser,
      transaction: this.transaction,
    });

    if (this.user.roles && (this.user.roles.includes('Owner') || this.user.roles.includes('Admin') || this.user.roles.includes('Billing')) && this.user.status === 'Active') {
      // update CRM info
      const account = await new AccountService({ currentUser: this.user, language: options.language }).findById(this.user.accountId, {
        currentUser: this.user,
        transaction: this.transaction,
      });

      // create or update the contact in hubspot
      // Note: Contact should already exist at this point, but this function will Update the contact if it exists
      // TODO: Update to use the function for only updating the Contact (and Chargify Customer if Owner) when that is split out from updateAccountDetails
      options.subscriptionWorker.addContactToAccount({ user: this.user, account });
    }
  }

  /**
   * Validates the user info.
   */
  async _validate() {
    assert(this.currentUser, 'currentUser is required');
    assert(this.currentUser.id, 'currentUser.id is required');
    assert(this.currentUser.email, 'currentUser.email is required');

    assert(this.data, 'profile is required');
  }
};
