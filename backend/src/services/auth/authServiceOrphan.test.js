const config = require('../../../config')();

const USE_MOCKS = config.useMocks;

if (USE_MOCKS) {
  jest.doMock('../../database/models');
  jest.doMock('../../database/repositories/accountRepository');
  jest.doMock('../../database/repositories/accountMemberRepository');
  jest.doMock('../../database/repositories/userRepository');
  jest.doMock('../../database/repositories/siteRepository');
  jest.doMock('../../database/repositories/markerRepository');
  jest.doMock('../../database/repositories/organizationRepository');
}

const AuthService = require('./authService');
const AccountRepository = require('../../database/repositories/accountRepository');
const AccountMemberRepository = require('../../database/repositories/accountMemberRepository');
const UserRepository = require('../../database/repositories/userRepository');
const SiteRepository = require('../../database/repositories/siteRepository');
const MarkerRepository = require('../../database/repositories/markerRepository');
const OrganizationRepository = require('../../database/repositories/organizationRepository');
const models = require('../../database/models');
const SequelizeRepository = require('../../database/repositories/sequelizeRepository');
const AuthEmails = require('./authEmails');

const sendgridApiKeyFromConfig = config.email.sendgrid_api_key;

describe('orphan users', () => {
  const options = {
    currentUser: {
      accountId: config.testAccountId,
    },
  };

  const bsSubscription = {
    id: 41107825,
    state: 'trialing',
    trial_started_at: '2021-04-06T14:09:16-04:00',
    trial_ended_at: '2025-04-06T14:09:16-04:00',
    activated_at: null,
    created_at: '2021-04-06T14:09:16-04:00',
    updated_at: '2021-04-06T14:09:17-04:00',
    expires_at: null,
    balance_in_cents: 0,
    current_period_ends_at: '2025-04-06T14:09:16-04:00',
    next_assessment_at: '2025-04-06T14:09:16-04:00',
    canceled_at: null,
    cancellation_message: null,
    next_product_id: null,
    next_product_handle: null,
    cancel_at_end_of_period: false,
    payment_collection_method: 'remittance',
    snap_day: null,
    cancellation_method: null,
    current_period_started_at: '2021-04-06T14:09:16-04:00',
    previous_state: 'trialing',
    signup_payment_id: 487093353,
    signup_revenue: '0.00',
    delayed_cancel_at: null,
    coupon_code: null,
    total_revenue_in_cents: 0,
    product_price_in_cents: 0,
    product_version_number: 1,
    payment_type: null,
    referral_code: null,
    coupon_use_count: null,
    coupon_uses_allowed: null,
    reason_code: null,
    automatically_resume_at: null,
    coupon_codes: [],
    offer_id: null,
    payer_id: 42063190,
    receives_invoice_emails: null,
    product_price_point_id: 969487,
    next_product_price_point_id: null,
    credit_balance_in_cents: 0,
    prepayment_balance_in_cents: 0,
    net_terms: null,
    stored_credential_transaction_id: null,
    locale: null,
    reference: '4943213356',
    currency: 'USD',
    on_hold_at: null,
    scheduled_cancellation_at: null,
    customer: {
      id: 42063190,
      first_name: 'Test',
      last_name: 'User',
      organization: null,
      email: 'test@testing.com',
      created_at: '2021-04-06T14:09:16-04:00',
      updated_at: '2021-04-06T14:09:16-04:00',
      reference: 'ef1839a3-9693-4e8e-89a2-f55173df1e61',
      address: null,
      address_2: null,
      city: null,
      state: null,
      state_name: null,
      zip: null,
      country: null,
      country_name: null,
      phone: '',
      portal_invite_last_sent_at: null,
      portal_invite_last_accepted_at: null,
      verified: false,
      portal_customer_created_at: null,
      vat_number: null,
      cc_emails: null,
      tax_exempt: false,
      parent_id: null,
      locale: null,
    },
    product: {
      id: 5256309,
      name: 'IX3 Trial',
      handle: 'ix_standard',
      description: '',
      accounting_code: '',
      request_credit_card: true,
      expiration_interval: null,
      expiration_interval_unit: 'never',
      created_at: '2020-09-01T20:56:13-04:00',
      updated_at: '2020-10-25T16:28:06-04:00',
      price_in_cents: 0,
      interval: 1,
      interval_unit: 'month',
      initial_charge_in_cents: null,
      trial_price_in_cents: 0,
      trial_interval: 48,
      trial_interval_unit: 'month',
      archived_at: null,
      require_credit_card: false,
      default_product_price_point_id: 969487,
      request_billing_address: false,
      require_billing_address: false,
      require_shipping_address: false,
      product_price_point_id: 969487,
      product_price_point_name: 'Standard',
      product_price_point_handle: 'standard',
      product_family: [Object],
      public_signup_pages: [],
    },
    group: null,
  };

  const subWorkerMock = {
    createTrial: jest.fn(),
  };

  const orphanEmail = 'someUniqueEmail@testing.com';
  const orphanEmail2 = 'someUniqueEmail2@testing.com';
  const orphanEmail3 = 'someUniqueEmail3@testing.com';
  const orphanEmail4 = 'someUniqueEmail4@testing.com';
  const orphanEmail5 = 'someUniqueEmail5@testing.com';
  const orphanEmail6 = 'someUniqueEmail6@testing.com';
  const firstName = 'Test';
  const mockSendNextStepEmail = jest.spyOn(AuthEmails, 'sendNextStepEmail');
  const mockFindOwner = jest.spyOn(AccountMemberRepository, 'findOwnerByAccountId');
  const mockCreateSubscription = jest.spyOn(AuthService, 'createSubscription');
  const mockSendAccessRequestEmail = jest.spyOn(AuthEmails, 'sendAccessRequestEmail');
  const mockSendApprovedRequestEmail = jest.spyOn(AuthEmails, 'sendRequestApprovedEmail');
  const mockSendRequestRejectedEmail = jest.spyOn(AuthEmails, 'sendRequestRejectedEmail');

  const accountData = {
    id: 'dc9beb03-71a1-4939-88b5-41f6660c9c31',
    status: 'Active',
    settings: '{"allowRequestPolicy":true}',
    orgName: 'Admin Org',
    orgPhone: null,
    orgEmail: 'org@testing.com',
    orgStreet: null,
    orgStreet2: null,
    orgCity: null,
    orgRegion: null,
    orgPostalcode: null,
    orgCountry: null,
    orgUnitName: null,
    orgUnitId: null,
    parentAccountId: null,
    importHash: null,
    dbHost: 'localhost',
    dbName: 'ixc_account_test_dc9beb03-71a1-4939-88b5-41f6660c9c31',
    createdAt: '2021-05-12T14:36:19.669Z',
    updatedAt: '2021-05-12T14:36:19.669Z',
    deletedAt: null,
    createdById: null,
    updatedById: null,
    members: [
      {
        paid: true,
        id: '681544ec-d799-4a3f-a6bd-199f9071f981',
        settings: '{}',
        roles: ['Owner', 'Admin'],
        status: 'Active',
        accessControl: false,
        importHash: null,
        createdAt: '2021-05-12T15:01:25.795Z',
        updatedAt: '2021-05-12T15:01:25.808Z',
        deletedAt: null,
        accountId: '95d05d48-082d-49d1-bf35-4ba6c543f990',
        siteId: 'affaa7c4-f877-4c39-81c7-890f6ffb574a',
        userId: 'afd6e2ab-74ea-4947-9612-58696b44a21c',
        createdById: null,
        updatedById: null,
      },
    ],
  };

  const accountMock = {
    createMock: jest.fn().mockReturnValue(accountData),
    updateMock: jest.fn().mockReturnValue(accountData),
    findAndCountAllMock: jest.fn().mockReturnValue({
      rows: [],
      count: 0,
    }),
    findByIdMock: jest.fn().mockReturnValue(accountData),
    findByDomainMock: jest.fn().mockReturnValue(accountData),
    destroyMock: jest.fn(),
  };

  const accountMemberData = {
    paid: true,
    id: 'd7f97786-c0a4-4d1f-99ee-669c72ef5945',
    settings: '{}',
    roles: ['Owner', 'Admin'],
    status: 'Requesting',
    accessControl: true,
    importHash: null,
    createdAt: '2021-05-12T19:09:53.388Z',
    updatedAt: '2021-05-12T19:09:53.400Z',
    deletedAt: null,
    accountId: config.testAccountId,
    siteId: '608e76c3-95a8-4c5f-9a70-cebb85916e35',
    userId: 'e204c0b1-48dc-47e7-bf60-f65dbedf4b58',
    createdById: null,
    updatedById: null,
    account: {
      id: config.testAccountId,
      status: 'Active',
      settings: '{"allowRequestPolicy":true}',
      orgName: 'Admin Org',
      orgPhone: null,
      orgEmail: 'org@testing.com',
      orgStreet: null,
      orgStreet2: null,
      orgCity: null,
      orgRegion: null,
      orgPostalcode: null,
      orgCountry: null,
      orgUnitName: null,
      orgUnitId: null,
      parentAccountId: null,
      importHash: null,
      dbHost: 'localhost',
      dbName: 'ixc_account_test_3f1e4fa4-e6df-4ad2-8e4d-ecb04a075159',
      createdAt: '2021-05-12T19:09:52.192Z',
      updatedAt: '2021-05-12T19:09:52.192Z',
      deletedAt: null,
      createdById: null,
      updatedById: null,
    },
    user: {
      id: 'e204c0b1-48dc-47e7-bf60-f65dbedf4b58',
      password: null,
      emailVerified: false,
      emailVerificationToken: null,
      emailVerificationTokenExpiresAt: null,
      passwordResetToken: null,
      passwordResetTokenExpiresAt: null,
      email: 'org@testing.com',
      signupMeta: '{"firstName":"Admin","email":"org@testing.com"}',
      activeAccountMemberId: 'd7f97786-c0a4-4d1f-99ee-669c72ef5945',
      authenticationUid: 'e204c0b1-48dc-47e7-bf60-f65dbedf4b58',
      disabled: false,
      importHash: null,
      createdAt: '2021-05-12T19:09:53.367Z',
      updatedAt: '2021-05-12T19:09:53.416Z',
      deletedAt: null,
      createdById: null,
      updatedById: null,
    },
  };

  const accountMemberMock = {
    findOwnerByAccountIdMock: jest.fn().mockReturnValue(accountMemberData),
    findByUserAndAccountMock: jest.fn().mockReturnValue(accountMemberData),
    findByUserMock: jest.fn().mockReturnValue([]),
    updateMock: jest.fn().mockReturnValue({ ...accountMemberData, status: 'Active' }),
    findAndCountAllMock: jest.fn().mockReturnValue({
      rows: [],
      count: 0,
    }),
  };

  const userData = {
    dataValues: {
      id: 'e204c0b1-48dc-47e7-bf60-f65dbedf4b58',
      password: null,
      emailVerified: true,
      emailVerificationToken: '1234567',
      emailVerificationTokenExpiresAt: null,
      passwordResetToken: null,
      passwordResetTokenExpiresAt: null,
      email: 'someUniqueEmail@testing.com',
      signupMeta: '{"firstName":"Test","email":"someUniqueEmail@testing.com"}',
      activeAccountMemberId: null,
      authenticationUid: null,
      disabled: false,
      importHash: null,
      createdAt: '2021-05-12T14:36:21.017Z',
      updatedAt: '2021-05-12T14:36:21.017Z',
      deletedAt: null,
      createdById: null,
      updatedById: null,
      siteId: '31ef1d56-1b86-4ffa-b886-507ed8f021c5',
      accountId: config.testAccountId,
    },
  };
  const userMock = {
    createOrphanFromAuthMock: jest.fn().mockReturnValue(userData),
    findByEmailMock: jest.fn().mockReturnValueOnce(null).mockReturnValue(userData),
    createFromAuthMock: jest.fn().mockReturnValue(userData),
    adoptOrphanToAccountMock: jest.fn().mockReturnValue(userData),
    findByIdMock: jest.fn().mockReturnValue(userData),
    findByEmailWithoutAvatarMock: jest.fn().mockReturnValue(userData),
  };

  const siteMock = {
    createMock: jest.fn().mockReturnValue({}),
  };

  let testAccountId;
  let testSiteId;

  const mockRepositoryFunctions = () => {
    models.getTenant = () => Promise.resolve({});

    models.sequelizeAdmin = {
      query: jest.fn().mockReturnValue(Promise.resolve({})),
    };
    models.user.findOne = () => Promise.resolve(userData.dataValues);

    SequelizeRepository.commitTransaction = jest.fn();
    SequelizeRepository.createTransaction = jest.fn();
    AccountRepository.mockImplementation(() => {
      return {
        create: accountMock.createMock,
        update: accountMock.updateMock,
        findAndCountAll: accountMock.findAndCountAllMock,
        findById: accountMock.findByIdMock,
        findByDomain: accountMock.findByDomainMock,
        destroy: accountMock.destroyMock,
      };
    });
    AccountMemberRepository.findOwnerByAccountId = accountMemberMock.findOwnerByAccountIdMock;
    AccountMemberRepository.findByUserAndAccount = accountMemberMock.findByUserAndAccountMock;
    AccountMemberRepository.findByUser = accountMemberMock.findByUserMock;
    AccountMemberRepository.update = accountMemberMock.updateMock;
    AccountMemberRepository.findAndCountAll = accountMemberMock.findAndCountAllMock;
    UserRepository.createOrphanFromAuth = userMock.createOrphanFromAuthMock;
    UserRepository.findByEmail = userMock.findByEmailMock;
    UserRepository.createFromAuth = userMock.createFromAuthMock;
    UserRepository.adoptOrphanToAccount = userMock.adoptOrphanToAccountMock;
    UserRepository.findById = userMock.findByIdMock;
    UserRepository.findByEmailWithoutAvatar = userMock.findByEmailWithoutAvatarMock;
    SiteRepository.mockImplementation(() => {
      return {
        create: siteMock.createMock,
      };
    });
    MarkerRepository.mockImplementation(() => {
      return {
        create: jest.fn().mockReturnValue({}),
      };
    });
    OrganizationRepository.mockImplementation(() => {
      return {
        create: jest.fn().mockReturnValue({}),
      };
    });
  };

  beforeAll(async () => {
    // Workaround for EmailSender running and erroring if its env var is set
    config.email.sendgrid_api_key = undefined;
    if (USE_MOCKS) {
      mockRepositoryFunctions();
      UserRepository.findByEmail = userMock.findByEmailMock;
    } else {
      jest.setTimeout(10000);
      await SequelizeRepository.cleanDatabase(options.currentUser.accountId);
      await SequelizeRepository.cleanDatabase();
    }
    subWorkerMock.createTrial.mockResolvedValueOnce({ bsSubscription });
    mockCreateSubscription.mockReturnValue(true);
    await AuthService.signupSubmit('org@testing.com', 'Admin', options);
    const user = await UserRepository.findByEmail('org@testing.com', options);

    const membership = await AccountMemberRepository.findByUserAndAccount(user.id, user.accountId, options);

    if (USE_MOCKS) {
      testAccountId = config.testAccountId;
      testSiteId = '31ef1d56-1b86-4ffa-b886-507ed8f021c5';
    } else {
      testAccountId = user.accountId;
      testSiteId = membership.siteId;
    }
  });

  beforeEach(() => {
    jest.clearAllMocks();
    mockFindOwner.mockReturnValue({ email: 'mockOwner@testing.com' });
    mockSendNextStepEmail.mockReturnValue(true);
    subWorkerMock.createTrial.mockResolvedValueOnce({ bsSubscription });
    mockCreateSubscription.mockReturnValue(true);
    mockSendApprovedRequestEmail.mockReturnValue(true);
    mockSendAccessRequestEmail.mockReturnValue(true);
    mockSendRequestRejectedEmail.mockReturnValue(true);

    if (USE_MOCKS) {
      mockRepositoryFunctions();
      UserRepository.findByEmail = jest.fn().mockReturnValue(userData.dataValues);
    }
  });

  afterAll(async () => {
    config.email.sendgrid_api_key = sendgridApiKeyFromConfig;

    if (!USE_MOCKS) {
      await SequelizeRepository.cleanDatabase(options.currentUser.accountId);
      await SequelizeRepository.cleanDatabase(testAccountId);
      await SequelizeRepository.cleanDatabase();
      await SequelizeRepository.closeConnections(options.currentUser.accountId);
      await SequelizeRepository.closeConnections(testAccountId);
      await SequelizeRepository.closeConnections();
    }
  });

  it('creates an orphan user', async () => {
    if (USE_MOCKS) {
      UserRepository.findByEmail = jest.fn().mockReturnValue({ ...userData.dataValues, emailVerified: false });
    }
    const success = await AuthService.createOrphanUser(orphanEmail, firstName, 'Open', options);
    expect(success).toBeTruthy();
    const user = await UserRepository.findByEmail(orphanEmail, options);
    expect(user).not.toBe(null);
    expect(user.emailVerificationToken).toBeDefined();
    expect(user.emailVerified).toBeFalsy();
    expect(user.signupMeta).toBe('{"firstName":"Test","email":"someUniqueEmail@testing.com"}');
    const memberships = await AccountMemberRepository.findByUser(user.id, options);
    expect(memberships.length).toBe(0);
  });

  it('adopts an orphan to an account', async () => {
    await AuthService.createOrphanUser(orphanEmail2, firstName, 'Open', options);
    const user = await UserRepository.findByEmail(orphanEmail2, options);
    const newOpts = { currentUser: { accountId: config.testAccountId, id: user.id } };
    expect(user).not.toBe(null);
    await UserRepository.adoptOrphanToAccount(user, { id: user.id, accountId: testAccountId, siteId: testSiteId, roles: ['Collaborator'] }, { ...newOpts, status: 'Active' });
    const membership = await AccountMemberRepository.findByUserAndAccount(user.id, testAccountId, options);
    expect(membership).not.toBe(null);
    expect(membership.userId).toBe(user.id);
    expect(membership.accountId).toBe(testAccountId);
    expect(membership.accessControl).toBeTruthy();
  });

  it('approves an orphan access request', async () => {
    mockFindOwner.mockRestore();
    if (USE_MOCKS) {
      AccountMemberRepository.findByUser = jest.fn().mockReturnValue([accountMemberData]);
    }
    await AuthService.createOrphanUser(orphanEmail3, firstName, 'Open', options);
    let user = await UserRepository.findByEmail(orphanEmail3, options);
    expect(user).not.toBe(null);
    const newOpts = { currentUser: { accountId: config.testAccountId, id: user.id } };
    await AuthService.sendAccessRequest(user.email, undefined, user.emailVerificationToken, ['Collaborator'], newOpts);
    let membership = await AccountMemberRepository.findByUserAndAccount(user.id, testAccountId, newOpts);
    expect(membership.status).toBe('Requesting');
    user = await UserRepository.findByEmail(orphanEmail3, newOpts);
    await AuthService.acceptAccessRequest('org@testing.com', user.emailVerificationToken, user.id, newOpts);
    if (USE_MOCKS) {
      expect(userMock.findByIdMock).toBeCalledTimes(1);
      expect(accountMemberMock.updateMock).toBeCalledTimes(1);
      expect(mockSendApprovedRequestEmail).toBeCalledTimes(1);
    } else {
      membership = await AccountMemberRepository.findByUserAndAccount(user.id, testAccountId, newOpts);
      expect(membership.status).toBe('Active');
    }
  });

  it('rejects an access request and destroys the orphan if that was their only membership', async () => {
    if (USE_MOCKS) {
      AccountMemberRepository.findByUser = jest.fn().mockReturnValue([accountMemberData]);
    }
    await AuthService.createOrphanUser(orphanEmail4, firstName, 'Open', options);
    const user = await UserRepository.findByEmail(orphanEmail4, options);
    expect(user).not.toBe(null);
    const token = await AuthEmails.generateEmailToken(user.email, options); // skipped by mocks
    await AuthService.sendAccessRequest(user.email, undefined, token, ['Collaborator'], options);
    const membership = await AccountMemberRepository.findByUserAndAccount(user.id, testAccountId, options);
    expect(membership.status).toBe('Requesting');
    const adminUser = await UserRepository.findByEmail('org@testing.com', options);
    const rejectOpts = { currentUser: { accountId: testAccountId, id: adminUser.id } };
    const mockFindByUserAndAccount = jest.spyOn(AccountMemberRepository, 'findByUserAndAccount');
    mockFindByUserAndAccount.mockReturnValue(membership);
    await AuthService.rejectAccessRequest('org@testing.com', token, rejectOpts);
    mockFindByUserAndAccount.mockRestore();
    if (USE_MOCKS) {
      expect(models.user.destroy).toBeCalledTimes(1);
      expect(models.account.destroy).toBeCalledTimes(1);
      expect(models.accountMember.destroy).toBeCalledTimes(1);
      expect(models.site.destroy).toBeCalledTimes(1);
      expect(mockSendRequestRejectedEmail).toBeCalledTimes(1);
    } else {
      const endUser = await UserRepository.findByEmail(orphanEmail4, options);
      expect(endUser).toBe(null);
    }
  });

  it('creates a new account for an orphan', async () => {
    mockFindOwner.mockRestore();
    const success = await AuthService.createOrphanUser(orphanEmail5, 'Orphan', 'Open', options);
    expect(success).toBeTruthy();
    const user = await UserRepository.findByEmail(orphanEmail5, options);
    expect(user).not.toBe(null);
    await AuthService.createAccountForOrphan(user, 'FakePassword1!', 'Orphan Org', 'Orphan', 'User', options);
    if (USE_MOCKS) {
      expect(userMock.adoptOrphanToAccountMock).toBeCalledTimes(1);
    } else {
      const memberships = await AccountMemberRepository.findByUser(user.id, options);
      expect(memberships.length).toBe(1);
      expect(memberships[0].userId).toBe(user.id);
      expect(memberships[0].roles.includes('Owner')).toBeTruthy();
    }
  });

  it('creates a new account for an orphan and deletes it', async () => {
    if (USE_MOCKS) {
      AccountMemberRepository.findByUser = jest.fn().mockReturnValue([accountMemberData]);
      accountMock.findByIdMock = jest.fn().mockReturnValueOnce(accountData).mockReturnValue(null);
      AccountRepository.mockImplementation(() => {
        return {
          create: accountMock.createMock,
          update: accountMock.updateMock,
          findAndCountAll: accountMock.findAndCountAllMock,
          findById: accountMock.findByIdMock,
          findByDomain: accountMock.findByDomainMock,
          destroy: accountMock.destroyMock,
        };
      });
    }
    mockFindOwner.mockRestore();
    const success = await AuthService.createOrphanUser(orphanEmail6, 'Orphan', 'Open', options);
    expect(success).toBeTruthy();
    let user = await UserRepository.findByEmail(orphanEmail6, options);
    expect(user).not.toBe(null);
    await AuthService.createAccountForOrphan(user, 'FakePassword1!', 'Orphan Org', 'Orphan', 'User', options);
    const memberships = await AccountMemberRepository.findByUser(user.id, options);
    const accountId = memberships[0].accountId;
    expect(memberships.length).toBe(1);
    expect(memberships[0].userId).toBe(user.id);
    expect(memberships[0].roles.includes('Owner')).toBeTruthy();

    //delete account
    await AuthService.convertAccountToOrphan(accountId, options);

    const account = await new AccountRepository().findById(accountId, options);
    expect(account).toBe(null);
    user = await UserRepository.findByEmail(orphanEmail6, options);
    expect(user).not.toBe(null);
    expect(user.activeAccountMemberId).toBe(null);
  });
});
