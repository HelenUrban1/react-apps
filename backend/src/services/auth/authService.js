// const bcrypt = require('bcrypt');
const bcrypt = require('bcryptjs');
const Sequelize = require('sequelize');
const jwt = require('jsonwebtoken');
const models = require('../../database/models');
const UserRepository = require('../../database/repositories/userRepository');
const AccountRepository = require('../../database/repositories/accountRepository');
const AccountService = require('../accountService');
const SiteService = require('../siteService');
const OrganizationService = require('../organizationService');
const MarkerService = require('../markerService');
const AccountMemberRepository = require('../../database/repositories/accountMemberRepository');
const ValidationError = require('../../errors/validationError');
const EmailSender = require('../shared/email/emailSender');
const AuthEmails = require('./authEmails');
const AuthProfileEditor = require('./authProfileEditor');
const SequelizeRepository = require('../../database/repositories/sequelizeRepository');
const AuditLogRepository = require('../../database/repositories/auditLogRepository');
const config = require('../../../config')();
const { checkComplexity, validateDomain, deleteUserAndAccountRecords, convertToOrphan } = require('./authUtils');
const SettingRepository = require('../../database/repositories/settingRepository');
const UserProfileRepository = require('../../database/repositories/userProfileRepository');
const { log } = require('../../logger');
const { i18n } = require('../../i18n');
const SubscriptionRepository = require('../../database/repositories/subscriptionRepository');
const CognitoAuthEmails = require('./cognitoAuthEmails');

const BCRYPT_SALT_ROUNDS = 12;
const { Op } = Sequelize;

/**
 * Handles all the Auth operations of the user.
 */
class AuthService {
  /**
   * Creates a Subscription for an Account
   * @param {*} user
   * @param {*} account
   * @param {*} options (includes subscriptionWorker)
   * @returns
   */
  static async createSubscription({ user, account, forceCreateCompany }, options) {
    try {
      // NOTE: createTrial creates the CRM deal when the trial is requested (before registration form is submitted)
      // const trialCreatedReturn = await options.subscriptionWorker.createTrial({ user, account });
      // const subscription = await new SubscriptionRepository({}, options).create({ ...trialCreatedReturn.bsSubscription, accountId: account.id, billingId: user.id }, { ...options, currentUser: user });
      // options.subscriptionWorker.setIxcDbId({ dbId: subscription.id, subscriptionId: subscription.paymentSystemId });
      // NOTE: beginRegistration & completeRegistration creates the CRM deal when the registration form is completed
      return await options.subscriptionWorker.beginRegistration({ user, account, forceCreateCompany });
    } catch (error) {
      log.error(error);
      throw new ValidationError(options.language, 'iam.errors.failedRegister');
    }
  }

  /**
   * Creates the first Site for an Account
   * @param {Account} account
   * @param {Object} options
   */
  static async createFirstSiteForAccount(account, options = {}) {
    // First site in an account uses the account info
    const site = {
      status: 'Active',
      settings: '{}',
      siteName: account.orgName,
      sitePhone: account.orgPhone,
      siteEmail: account.orgEmail,
      siteStreet: account.orgStreet,
      siteStreet2: account.orgStreet2,
      siteCity: account.orgCity,
      siteRegion: account.orgRegion,
      sitePostalcode: account.orgPostalcode,
      siteCountry: account.orgCountry,
      accountId: account.id,
    };

    return new SiteService({}, options).create(site);
  }

  /**
   * Creates a New Account from the required information
   * @param {String} orgName
   * @param {String} orgEmail
   * @param {Boolean} policy
   * @param {Object} options
   * @returns
   */
  static async createNewAccount({ orgName, orgEmail, orgPhone, policy }, options = {}) {
    const account = {
      orgName,
      orgEmail,
      orgPhone,
    };

    // Init default account values
    account.status = 'Active';
    account.settings = JSON.stringify({ allowRequestPolicy: policy });
    account.dbHost = '';
    account.dbName = '';

    let newAccount;
    let newSite;

    try {
      newAccount = await new AccountService({}, options).create(account);
      newSite = await this.createFirstSiteForAccount(newAccount, options);
      await new MarkerService({}, options).createDefaults(newAccount.id);
      await new OrganizationService({}, options).createDefault({ accountId: newAccount.id, siteId: newSite.id });

      return { newAccount, newSite };
    } catch (error) {
      log.error(error);
      await deleteUserAndAccountRecords({ account: newAccount, site: newSite }, true, options);
      throw new ValidationError(options.language, '');
    }
  }

  /**
   * Entry point for User signup whether from App or Hubspot
   * @param {String} email
   * @param {String} password
   * @param {String} firstName
   * @param {String} lastName
   * @param {String} orgName
   * @param {Boolean} overrideDomainCheck
   * @param {Object} options
   * @returns
   */
  static async signupSubmit(email, firstName, options = {}) {
    log.debug('AuthService.signupSubmit');
    // Check for Existing User
    const existingUser = await UserRepository.findByEmail(email, options);

    log.debug('AuthService.signupSubmit : existingUser');
    log.debug(config.jwt.authStrategy);
    if (existingUser && existingUser.activeAccountMemberId) {
      log.debug('AuthService.signupSubmit : existingUser.activeAccountMemberId');
      if (config.jwt.authStrategy === 'cognito') {
        log.debug('AuthService.signupSubmit : existingUser cognito');
        return CognitoAuthEmails.cognitoResendVerificationEmail(existingUser.email, options);
      }
      const messageId = this.resendVerificationEmail(existingUser.email, options);
      return messageId !== false; // Graphql expects boolean return value
    }

    // Check for Existing Domain and Blacklisted Domains
    log.debug('AuthService.signupSubmit : validateDomain');
    const requestPolicy = await validateDomain(email, options);

    if (requestPolicy === 'Open') {
      log.debug('AuthService.signupSubmit : createOrphanUser');
      // create user, send access request email with button to request access or start trial
      await this.createOrphanUser(email, firstName, requestPolicy, options);
      return config.jwt.authStrategy !== 'cognito' ? true : 'orphanRequestOpen';
    }
    // create new account for user
    log.debug('AuthService.signupSubmit : accountService.create');
    // NOTE: This is where the initial orgName for ALL new Accounts is first declared
    // TODO: If the user is coming through a HS form and their HS account exists, we could just use the company name from it
    // For now, extract the domain name from the user's email address and make it the company name
    const orgName = email.toLowerCase().split('@').pop(); // Used to be `${firstName} Org`;
    const { newAccount, newSite } = await this.createNewAccount({ orgName, orgEmail: email, orgPhone: undefined, policy: 'New' }, options);
    await this.signup(email, firstName, newAccount, newSite, options);

    return config.jwt.authStrategy !== 'cognito' ? true : 'emailAddressVerification';
  }

  /**
   * Signs up with the email and password and returns a JWT token.
   *
   * @param {String} email
   * @param {String} password
   * @param {Object} [options]
   * @return {String} Auth token in JWT format
   */
  static async signup(email, firstName, account, site, options = {}) {
    try {
      log.debug('AuthService.signup signupNewUser');
      return this.signupNewUser(email, firstName, account, site, options);
    } catch (error) {
      log.error(error);
      await deleteUserAndAccountRecords({ account, site }, true, options);
      throw new ValidationError(options.language, '');
    }
  }

  /**
   * Sign up flow for an existing user. This flow is for a user who
   * was invited but has not previously used the app.
   *
   * @param {String} hashedPassword
   * @param {Boolean} existingUser
   * @param {Object} [options]
   * @param {String} password
   * @return {String} Auth token in JWT format
   */
  static async signupExistingUser(hashedPassword, existingUser, options = {}, password) {
    log.debug('AuthService.signupExistingUser');
    // If the user already has an authenticationUid,
    // it means that it has already signed up
    if (existingUser.authenticationUid) {
      throw new ValidationError(options.language, 'auth.emailAlreadyInUse');
    }

    if (existingUser.disabled) {
      throw new ValidationError(options.language, 'auth.userDisabled');
    }

    // Check the password complexity
    if (!checkComplexity(password)) {
      throw new ValidationError(options.language, 'auth.weakPassword');
    }

    /**
     * In the case of the user exists on the database (was invited)
     * it only creates the new password
     */
    await UserRepository.updatePassword(existingUser.id, hashedPassword, options);

    if (EmailSender.isConfigured) {
      await AuthEmails.sendEmailAddressVerificationEmail(options.language, existingUser.email, existingUser.firstName, options);
    }

    const jwt = await this.jwt(existingUser);
    const refresh = await this.refreshToken(existingUser);

    return [jwt, refresh];
  }

  /**
   * Sign up flow for a new user.
   *
   * @param {String} hashedPassword
   * @param {String} email
   * @param {Object} [options]
   * @return {String} Auth token in JWT format
   */
  static async signupNewUser(email, firstName, account, site, options = {}) {
    log.debug('AuthService.signupNewUser');
    const newOptions = { ...options };
    if (!newOptions.accountId) {
      newOptions.accountId = account.id;
    }
    if (!newOptions.siteId) {
      newOptions.siteId = site.id;
    }

    let newUser;

    try {
      // creates a user and associates them to the created account and site
      newUser = await UserRepository.createFromAuth(
        {
          email,
          firstName,
          lastName: '',
          password: null,
          accountId: account.id,
          siteId: site.id,
          signupMeta: JSON.stringify({ firstName, email }),
        },
        newOptions
      );
      // create a new subscription
      await this.createSubscription({ user: newUser, account, forceCreateCompany: false }, options);
    } catch (error) {
      log.error(error);
      if (newUser) await deleteUserAndAccountRecords({ user: newUser, account, site }, true, options);
      throw new ValidationError(options.language, 'iam.errors.failedRegister');
    }

    if (config.jwt.authStrategy !== 'cognito' && (EmailSender.isConfigured || options.isTest)) {
      await AuthEmails.sendEmailAddressVerificationEmail(newOptions.language, newUser.email, newUser.firstName, newOptions);
    } else {
      await UserRepository.generateEmailVerificationToken(newUser.email, options);
    }

    const jwt = await this.jwt(newUser);
    const refresh = await this.refreshToken(newUser);

    return config.jwt.authStrategy === 'cognito' ? newUser : [jwt, refresh];
  }

  /**
   * Flow for inviting a new user.
   *
   * @param {String} email
   * @param {Array} roles
   * @param {String} accountId
   * @param {String} siteId
   * @param {Object} [options]
   * @return {Boolean}
   */
  static async inviteNewUser(email, roles, control, accountId, siteId, sender, options = {}) {
    log.debug('AuthService.inviteNewUser');
    const currentOptions = { ...options, status: 'Pending' };

    if (!currentOptions.accountId) {
      currentOptions.accountId = accountId;
    }
    if (!currentOptions.siteId) {
      currentOptions.siteId = siteId;
    }
    let newUser;
    const exists = (await UserRepository.count({ email: { [Op.iLike]: `%${email}%` } }, { transaction: this.transaction })) > 0;
    if (exists) {
      newUser = await this.reactivateInvitedUser(email, options);
    } else {
      // Create new user
      newUser = await UserRepository.createFromAuth(
        {
          email,
          roles,
          password: null,
          accountId,
          siteId,
          control,
        },
        currentOptions
      );
      if (!newUser) {
        throw new Error('Could not register new user. Please check provided information.');
      }
    }
    if (!newUser) {
      throw new Error('Could not register new user. Please check provided information.');
    }
    if (!newUser.activeAccountMemberId) {
      // adopt this orphan because we love love
      newUser = await UserRepository.adoptOrphanToAccount(newUser, { id: newUser.id, accountId, siteId, roles }, currentOptions);
    }

    // Send user invitation
    let messageId;
    if (config.jwt.authStrategy !== 'cognito') {
      messageId = await AuthEmails.sendUserInvitationEmail(email, sender, currentOptions);
    }

    if (!messageId && config.jwt.authStrategy !== 'cognito') {
      throw new Error('Failed to send email. Please check your internet connection and contact support if the issue persists.');
    }
    return true;
  }

  static async resendUserInvite(email, firstName, options = {}) {
    return AuthEmails.sendUserInvitationEmail(email, firstName, options);
  }

  static async resendExpiredInvite(email, options = {}) {
    const user = await UserRepository.findByEmailWithoutAvatar(email, options);
    return this.resendUserInvite(user.email, user.accountName || user.account?.orgEmail, options);
  }

  /**
   * Flow for requesting access to an account
   * @param {String} email
   * @param {String} adminEmail
   * @param {String[]} roles
   * @param {String} accountId
   * @param {*} sender
   * @param {*} options
   * @returns
   */
  static async sendAccessRequest(email, adminEmail, token, roles, options = {}) {
    // Finds orphan user
    log.debug('AuthService.sendAccessRequest');
    const user = await UserRepository.findByEmail(email, { ...options, userOnly: true });
    const newPassword = user.password;

    const currentOptions = { ...options };
    let adminUser;
    let accountMembership;

    if (adminEmail) {
      // access is being requested to a specific admin email
      adminUser = await UserRepository.findByEmailWithoutAvatar(adminEmail, currentOptions);
    } else {
      // access is being automatically requested to the account owner
      const account = await new AccountRepository().findByDomain(email, currentOptions);
      adminUser = await UserRepository.findByEmail(account.orgEmail, currentOptions);
    }

    if (!adminUser || !adminUser.id) {
      throw new ValidationError(options.language, 'errors.validation.invalidAdmin');
    }

    const memberships = await AccountMemberRepository.findByUser(adminUser.id, options);
    const userDomain = email.split('@').pop();
    memberships
      .filter((membership) => membership.roles.includes('Admin') || membership.roles.includes('Owner'))
      .forEach((membership) => {
        const accountDomain = membership.account.orgEmail.split('@').pop();
        if (accountDomain === userDomain) {
          if (accountMembership) throw new ValidationError(options.language, 'errors.validation.multipleAccounts');
          accountMembership = membership;
        }
      });

    if (!accountMembership || !user) {
      throw new ValidationError(options.language, 'errors.validation.adminEmail');
    }

    currentOptions.status = 'Requesting';

    const updatedUser = await UserRepository.adoptOrphanToAccount(
      user,
      { id: user.id, password: null, accountId: accountMembership.accountId, siteId: accountMembership.siteId, roles, firstName: JSON.parse(user.signupMeta || '{}').firstName || '', lastName: '' },
      currentOptions
    );

    if (!updatedUser) {
      throw new ValidationError(options.language, 'iam.errors.failedRegister');
    }

    // Send user invitation to Admin
    const messageId = AuthEmails.sendAccessRequestEmail(email, adminUser.firstName, adminUser.email, currentOptions);
    if (!messageId) {
      throw new ValidationError(options.language, 'emails.error.failed');
    }

    return messageId !== false; // sendAccessRequest expects boolean response
  }

  /**
   * Creates an Account for an Orphaned User
   * @param {String} user
   * @param {String} orgName
   * @param {Object} options
   * @returns
   */
  static async createAccountForOrphan(user, hashedPassword, orgName, orgPhone, firstName, lastName, options = {}) {
    log.debug('AuthService.createAccountForOrphan');
    const { newAccount, newSite } = await this.createNewAccount({ orgName, orgEmail: user.email, orgPhone, policy: false }, options);

    try {
      const updatedUser = await UserRepository.adoptOrphanToAccount(user, { id: user.id, password: hashedPassword, accountId: newAccount.id, siteId: newSite.id, firstName, lastName, accessControl: false }, options);
      const sub = await this.createSubscription({ user: updatedUser, account: newAccount, forceCreateCompany: true }, options);

      return { account: updatedUser.account, user: updatedUser, crmCompany: sub.crmCompany };
    } catch (error) {
      log.debug('bad account');
      log.error(error);
      await deleteUserAndAccountRecords({ account: newAccount, site: newSite }, true, options);
      throw new ValidationError(options.language, 'iam.errors.failedRegister');
    }
  }

  // creates a user without an account when a user signs up with a duplicate email domain
  static async createOrphanUser(email, firstName, requestPolicy, options = {}) {
    log.debug('AuthService.createOrphanUser');
    let newUser;
    try {
      const currentOptions = { ...options };
      currentOptions.status = 'Requesting';
      const accountRepo = new AccountRepository();
      const account = await accountRepo.findByDomain(email, currentOptions);
      const adminUser = await AccountMemberRepository.findOwnerByAccountId(account.id, currentOptions);
      newUser = await UserRepository.createOrphanFromAuth(
        {
          email,
          password: null,
          accountId: null,
          siteId: null,
          signupMeta: JSON.stringify({ firstName, email }),
        },
        currentOptions
      );
      if (!newUser) {
        throw new ValidationError(options.language, 'iam.errors.failedRegister');
      }

      return config.jwt.authStrategy !== 'cognito'
        ? await AuthEmails.sendNextStepEmail({ email, firstName, adminEmail: adminUser.email, adminFirstName: adminUser.firstName }, requestPolicy, currentOptions)
        : !!(await UserRepository.generateEmailVerificationToken(newUser.email, options));
    } catch (error) {
      log.error(error);
      await deleteUserAndAccountRecords({ user: newUser }, true, options);
      throw new ValidationError(options.language, 'errors.account.genericCreate');
    }
  }

  /**
   * Accepts a users Request for account access
   * @param {*} email
   * @param {*} sender
   * @param {*} options
   * @returns
   */
  static async acceptAccessRequest(adminEmail, token, userId, options = {}) {
    log.debug('AuthService.acceptAccessRequest');
    const adminUser = await UserRepository.findByEmailWithoutAvatar(adminEmail, options);

    let requestingUser;
    if (userId) {
      requestingUser = await UserRepository.findById(userId, options);
    } else {
      requestingUser = await UserRepository.findValidTokenOrCanceledInvite(token, options);
      if (!requestingUser) {
        const expiredUser = await models.user.findOne({
          where: { emailVerificationToken: token },
        });
        // Send user invitation to Admin
        const messageId = await AuthEmails.sendAccessRequestEmail(expiredUser.email, adminUser.firstName, adminEmail, options);
        if (!messageId) {
          throw new ValidationError(options.language, 'emails.error.failed');
        }

        return ['resent', 'resent'];
      }
    }
    if (!requestingUser) throw new ValidationError(options.language, 'iam.error.userNotFound');

    const membership = await AccountMemberRepository.findByUserAndAccount(requestingUser.id, adminUser.accountId, options);
    if (!membership) throw new ValidationError(options.language, 'auth.userMembershipError');

    const activeMembership = await AccountMemberRepository.update(membership.id, { status: 'Active' }, options);
    if (!activeMembership) throw new ValidationError(options.language, 'auth.userMembershipError');

    const firstName = requestingUser.firstName || JSON.parse(requestingUser.signupMeta || '{}').firstName;

    const messageId = await AuthEmails.sendRequestApprovedEmail(requestingUser.email, firstName, adminUser.firstName || adminUser.email, membership.account.orgName, options);
    if (!messageId) throw new ValidationError(options.language, 'emails.error.failed');

    const jwt = await this.jwt(adminUser);
    const refresh = await this.refreshToken(adminUser);

    return [jwt, refresh];
  }

  /**
   * Rejects a user's request for Account Access (through email link)
   * @param {String!} adminEmail
   * @param {String!} token
   * @param {String} userId
   * @param {Object} options
   * @returns
   */
  static async rejectAccessRequest(adminEmail, token, options = {}) {
    const transaction = SequelizeRepository.getTransaction(options);
    const adminUser = await UserRepository.findByEmailWithoutAvatar(adminEmail, options);

    const user = await models.user.findOne(
      {
        where: { emailVerificationToken: token },
      },
      {
        transaction,
      }
    );

    if (!user) throw new ValidationError(options.language, 'iam.error.userNotFound');

    const membership = await AccountMemberRepository.findByUserAndAccount(user.id, adminUser.accountId, options);

    if (!membership) throw new ValidationError(options.language, 'auth.userMembershipError');

    await this.rejectRequestingUser(user, membership, options);

    return !!convertToOrphan(user.email, options, true, UserRepository);
  }

  /**
   * Converts an account and all users of it to orphans
   * @param {String!} email
   * @param {Object} options
   * @returns
   */
  static async convertAccountToOrphan(accountId, options = {}) {
    // update account to status = archived deletedAt = timestamp
    // find all accountMembers with accountId
    // loop through those and convert users to orphans and delete accountMember

    // archive the account
    await new AccountRepository().update(
      accountId,
      {
        status: 'Archived',
      },
      { ...options, skipMembersUpdate: true }
    );

    // get all of the accountMembers for that account
    const memberships = await AccountMemberRepository.findAndCountAll(
      {
        filter: {
          account: accountId,
        },
      },
      options
    );
    if (memberships.rows) {
      for (let i = 0; i < memberships.rows.length; i++) {
        const userId = memberships.rows[i].user.id;
        const userEmail = memberships.rows[i].user.email;
        if (userId) {
          // update the user record
          await UserRepository.updateUserOnly(
            userId,
            {
              authenticationUid: null,
              activeAccountMemberId: null,
              disabled: false,
              emailVerified: false,
              password: null,
            },
            { ...options, userOnly: true }
          );
        }
        const userProfile = await UserProfileRepository.findByUserId(userId, options);

        // send an email to this bitch
        await AuthEmails.sendOrphanedUserEmail(userEmail, userProfile ? userProfile.firstName : 'there', options);
      }
    }

    // this destroys account members as well
    await new AccountRepository().destroy(accountId, options);

    return true;
  }

  static async startTrialForOrphan(user, options = {}) {
    log.debug('AuthService.startTrialForOrphan');
    // Check email domain against other accounts, disable other account's request policy if needed
    await validateDomain(user.email, options);

    // Verify user's email, generate new token
    // await UserRepository.updateUserOnly(user.id, { emailVerified: true }, { ...options, userOnly: true });
    const token = await AuthEmails.generateEmailToken(user.email, options);

    // Return URL for user to finish registration
    return `/auth/verify-email?email=${encodeURIComponent(user.email)}&token=${token}&form=new`;
  }

  /**
   * Finishes Registration of a User (setting PW coming from hubspot, new account for trial override, profile info for Orphans, PW+Profile info for invited users)
   * @param {String} email
   * @param {String} password
   * @param {String} firstName
   * @param {String} lastName
   * @param {String} orgName
   * @param {String} token
   * @param {Object} options
   * @returns {String} jwt token
   */
  static async finishRegistration(email, password, firstName, lastName, orgName, phoneNumber, token, options = {}) {
    log.debug('AuthService.finishRegistration');

    // finish user registration;
    let user = await UserRepository.findByEmail(email, { ...options, userOnly: false });
    if (!user) throw new ValidationError(options.language, 'auth.userNotFound');

    // Validate password
    let hashedPassword = user.password;
    if (password) {
      // user had to set their password
      if (!checkComplexity(password)) throw new ValidationError(options.language, 'auth.weakPassword');
      hashedPassword = await bcrypt.hash(password, BCRYPT_SALT_ROUNDS);
    }

    let membership = await AccountMemberRepository.findById(user.activeAccountMemberId, options);

    if (!membership) {
      // create an account for the user (user selected Free Trial from Request E-Mail)
      // handles email verification and profile update and returns new accountMembership
      membership = await this.createAccountForOrphan(user, hashedPassword, orgName, phoneNumber, firstName, lastName, { ...options, override: true });
      user = await UserRepository.findByEmail(email, { ...options, userOnly: false });
    } else if (orgName) {
      log.debug('AuthService.finishRegistration update orgName');
      // Update organization name for Account
      // We can't call accountService here because currentUser and accountId aren't fully hydrated until after login
      const accountRepo = new AccountRepository();
      await accountRepo.update(membership.accountId, { orgName, orgPhone: phoneNumber }, { ...options, skipMembersUpdate: true });
    }

    // No need to await, because response object isn't required
    // NOTE: createTrial creates the CRM deal when the trial is requested (before registration form is submitted)
    // log.debug('AuthService.finishRegistration call options.context.subscriptionWorker.updateAccountDetails');
    // await options.subscriptionWorker.updateRegistrationDetails({ user, accountId: membership.accountId, email, firstName, lastName, orgName, phoneNumber });

    if (orgName) {
      // NOTE: beginRegistration & completeRegistration creates the CRM deal when the registration form is completed
      log.debug('options.subscriptionWorker.completeRegistration', { user, account: membership.account, email, firstName, lastName, orgName, phoneNumber });
      const trialCreatedReturn = await options.subscriptionWorker.completeRegistration({ user, account: membership.account, email, firstName, lastName, orgName, phoneNumber, crmCompany: membership.crmCompany });
      const subscription = await new SubscriptionRepository({}, options).create({ ...trialCreatedReturn.bsSubscription, accountId: membership.account.id, billingId: user.id }, { ...options, currentUser: user });
      options.subscriptionWorker.setIxcDbId({ dbId: subscription.id, subscriptionId: subscription.paymentSystemId });
    }

    await UserRepository.updatePassword(user.id, hashedPassword, options);
    await UserRepository.markEmailVerified(user.id, options);
    // update user profile
    if (firstName || lastName) {
      await UserProfileRepository.update(user.id, { firstName, lastName, phoneNumber: phoneNumber || user.profile?.phoneNumber }, { ...options, currentUser: user });
    }

    return true;
  }

  /**
   * Flow for Register a new user from an invitation link.
   *
   * @param {String} token
   * @param {String} email
   * @param {String} password
   * @param {Object} profile
   * @param {Object} [options]
   * @return {Boolean}
   */
  static async registerInvitedUser(token, email, password, profile, options = {}) {
    log.debug('AuthService.registerInvitedUser');
    const existingUser = await UserRepository.findByEmailWithoutAvatar(email, options);
    const hashedPassword = await bcrypt.hash(password, BCRYPT_SALT_ROUNDS);
    const currentOptions = { ...options };

    if (!existingUser) {
      throw new ValidationError(options.language, 'auth.userNotFound');
    }

    if (existingUser.disabled) {
      throw new ValidationError(options.language, 'auth.inviteCancelled.invalidToken');
    }

    // Check the password complexity
    if (!checkComplexity(password)) {
      throw new ValidationError(options.language, 'auth.weakPassword');
    }

    const membership = await AccountMemberRepository.findByUserAndAccount(existingUser.id, existingUser.accountId, options);
    if (!membership) {
      throw new ValidationError(options.language, 'auth.userMembershipError');
    }

    if (membership.status === 'Active') {
      throw new ValidationError(options.language, 'auth.userMembershipActive');
    }

    if (!currentOptions.accountId) {
      currentOptions.accountId = existingUser.accountId;
    }
    if (!currentOptions.currentUser) {
      currentOptions.currentUser = existingUser;
    }
    if (!currentOptions.siteId) {
      currentOptions.siteId = existingUser.siteId;
    }
    await UserRepository.updatePassword(existingUser.id, hashedPassword, currentOptions);
    const verified = await this.verifyEmail(token, currentOptions, true);
    if (!verified) {
      throw new Error('test');
    }

    await AccountMemberRepository.update(membership.id, { status: 'Active' });

    const editor = new AuthProfileEditor(existingUser, currentOptions.language);
    await editor.execute(profile, currentOptions).catch((err) => {
      throw err;
    });

    if (membership.roles.includes('Admin') || membership.roles.includes('Billing')) {
      const account = await new AccountService({ currentUser: existingUser, language: currentOptions.language }).findById(existingUser.accountId, currentOptions);
      const user = await UserRepository.findByEmailWithoutAvatar(existingUser.email, currentOptions);
      await options.subscriptionWorker.addContactToAccount({ user, account });
    }

    return true;
  }

  /**
   * Disables a User whose Invite was Cancelled
   * @param {String} currentUser
   * @param {String} user
   * @param {String} options
   * @returns
   */
  static async disableUserCancelledInvite(currentUser, user, options = {}) {
    const emailVerificationTokenExpiresAt = Date.now();
    const success = await UserRepository.updateUserOnly(user.id, { emailVerificationTokenExpiresAt, disabled: true, updatedById: currentUser.id }, options);
    await AuditLogRepository.log(
      {
        entityName: 'user',
        entityId: user.id,
        action: AuditLogRepository.UPDATE,
        values: {
          id: user.id,
          disabled: true,
          emailVerificationTokenExpiresAt,
        },
      },
      options
    );
    return !!success;
  }

  /**
   * Rejects the access request of a user and hard deletes the user if they are orphaned
   * @param {String} user
   * @param {AccountMember} membership
   * @param {Object} options
   * @returns
   */
  static async rejectRequestingUser(user, membership, options = {}) {
    const currentUser = SequelizeRepository.getCurrentUser(options);
    await deleteUserAndAccountRecords({ accountMember: membership }, true, options);
    const count = await AccountMemberRepository.count({ userId: user.id }, options);
    if (count === 0) await deleteUserAndAccountRecords({ user }, true, options);
    const firstName = user.firstName || JSON.parse(user.signupMeta, '{}').firstName;
    await AuthEmails.sendRequestRejectedEmail(user.email, firstName, currentUser.email, options);
    return true;
  }

  /**
   * Cancel a registration invitation and invalidate the link
   *
   * @param {String} email
   * @param {Object} options
   */
  static async cancelInvitedUser(email, options = {}) {
    const user = await UserRepository.findByEmail(email, options);
    if (!user) {
      throw new ValidationError(options.language, 'auth.userNotFound');
    }

    // convert user to orphan
    return !!convertToOrphan(email, options, true, UserRepository);
  }

  /**
   * Enable a user whose invitation was previously cancelled
   *
   * @param {String} email
   * @param {Object} options
   */
  static async reactivateInvitedUser(email, options = {}) {
    log.debug('AuthService.reactivateInvitedUser');
    const currentUser = SequelizeRepository.getCurrentUser(options);
    const transaction = SequelizeRepository.getTransaction(options);
    const user = await models.user.findOne(
      {
        where: { email: { [Op.iLike]: `%${email}%` } },
      },
      {
        transaction,
      }
    );
    if (!user) {
      throw new ValidationError(options.language, 'auth.userNotFound');
    }

    if (user?.disabled) {
      const newUser = await user.update(
        {
          disabled: false,
          updatedById: currentUser.id,
        },
        { transaction }
      );

      await AuditLogRepository.log(
        {
          entityName: 'user',
          entityId: user.id,
          action: AuditLogRepository.UPDATE,
          values: {
            id: user.id,
            disabled: false,
          },
        },
        options
      );
      return newUser;
    }

    if (!user?.activeAccountMemberId) {
      return user;
    }
    throw new ValidationError(this.language, 'iam.errors.userAlreadyExists');
  }

  /**
   * Signs in a user with the email and password and returns a JWT token.
   * @param {String} email
   * @param {String} password
   * @param {Object} [options]
   */
  static async signin(email, password, options = {}) {
    const user = await UserRepository.findByEmailWithoutAvatar(email, options);
    if (!user || user.orphan) {
      throw new ValidationError(options.language, 'auth.userNotFound');
    }

    const activeAccountMember = user.accountMemberships.find((accountMember) => accountMember.id === user.activeAccountMemberId || (accountMember.dataValues && accountMember.dataValues.id === user.activeAccountMemberId));
    if (!activeAccountMember || activeAccountMember.status === 'Archived') {
      throw new ValidationError(options.language, 'auth.userMembershipDisabled');
    }

    if (activeAccountMember.status === 'Pending') {
      throw new ValidationError(options.language, 'auth.userMembershipPending');
    }

    if (activeAccountMember.status === 'Requesting') {
      throw new ValidationError(options.language, 'auth.userMembershipRequesting');
    }

    if (user.disabled) {
      throw new ValidationError(options.language, 'auth.userDisabled');
    }

    if (!user.password) {
      throw new ValidationError(options.language, 'auth.wrongPassword');
    }

    const passwordsMatch = await bcrypt.compare(password, user.password);

    if (!passwordsMatch) {
      throw new ValidationError(options.language, 'auth.wrongPassword');
    }

    const mfaEnabled = await SettingRepository.findByName('mfaEnabled', {
      currentUser: user,
    });
    if (mfaEnabled) {
      return { mfaEnabled: 'true' };
    }

    const token = await this.jwt(user);
    const refresh = await this.refreshToken(user);

    return [token, refresh];
  }

  /**
   * Signs in the service account user via device token.
   * @param {String} deviceToken
   */
  static async signinWithDevice(deviceToken, account) {
    // TODO do something to validate the device token
    const [user, member] = await this.findServiceAccountMember(account);

    const jwt = await this.jwt(user);
    const refresh = await this.refreshToken(user);

    return [jwt, refresh];
  }

  static async refreshToken(user) {
    log.debug('AuthService.refreshToken');
    return jwt.sign({ id: user.id, accountId: user.accountId, siteId: user.siteId }, config.jwt.refreshJwtSecret, { expiresIn: config.jwt.refreshExpiry });
  }

  static async jwt(user) {
    log.debug('AuthService.jwt');
    return jwt.sign({ id: user.id, accountId: user.accountId, siteId: user.siteId }, config.jwt.authJwtSecret, { expiresIn: config.jwt.authExpiry });
  }

  /**
   * Finds the user based on the JWT token.
   *
   * @param {String} token
   */
  static async findByToken(token) {
    return new Promise((resolve, reject) => {
      jwt.verify(token, config.jwt.authJwtSecret, (err, decoded) => {
        if (err) {
          reject(err);
          return;
        }

        const { id } = decoded;
        this.findById(id)
          .then((user) => resolve(user))
          .catch((error) => reject(error));
      });
    });
  }

  static async findByRefreshToken(token) {
    log.debug('AuthService.findByRefreshToken');
    return new Promise((resolve, reject) => {
      jwt.verify(token, config.jwt.refreshJwtSecret, (err, decoded) => {
        if (err) {
          reject(err);
          return;
        }

        const { id } = decoded;
        this.findById(id)
          .then((user) => resolve(user))
          .catch((error) => reject(error));
      });
    });
  }

  // TODO: This appears to just wrap a repo method and adjust an attribute. This should probably happen inside the model itself.
  /**
   * Finds the user by id.
   *
   * @param {String} id
   */
  static async findById(id) {
    const user = await UserRepository.findByIdWithoutAvatar(id);

    if (user && !EmailSender.isConfigured) {
      user.emailVerified = true;
    }

    return user;
  }

  /**
   * Finds the service account member and user.
   *
   * @param {Object} account
   * @param {Object} [options]
   */
  static async findServiceAccountMember(account, options = {}) {
    const user = await UserRepository.findByEmailWithoutAvatar(`service+${account.id}@inspectionxpert.com`, options);
    const member = await AccountMemberRepository.findByUser(user.id, options);

    return [user, member];
  }

  /**
   * Finds the account by domain.
   *
   * @param {String} email
   * @param {Object} [options]
   */
  static async findByDomain(email, options = {}) {
    const accountRepo = new AccountRepository();
    const account = await accountRepo.findByDomain(email, options);

    return account;
  }

  /**
   * Finds Accounts based on the query.
   *
   * @param {*} args
   */
  static async findAndCountAll(args) {
    const users = await UserRepository.findAllByRole(args);
    return users;
  }

  /**
   * Sends an email address verification email to an orphan account
   *
   * @param {String} language
   * @param {String} email
   * @param {Object} [options]
   */
  static async resendVerificationEmail(email, options) {
    log.debug('AuthService.resendVerificationEmail');

    if (!EmailSender.isConfigured) {
      throw new Error(`Email provider is not configured. Please configure it at backend/config/<environment>.json.`);
    }

    const user = await UserRepository.findByEmail(email, options);
    if (!user) return true;

    log.debug('AuthService.resendVerificationEmail : user.email', user.email);
    const firstName = user.firstName || JSON.parse(user.signupMeta || '{}').firstName;

    if (user.emailVerified) {
      return AuthEmails.sendVerifiedUserEmail(user.email, firstName, options);
    }

    log.debug('AuthService.resendVerificationEmail : user');
    if (user && user.accountId) {
      const membership = await AccountMemberRepository.findByUserAndAccount(user.id, user.accountId, options);

      if (membership && membership.status !== 'Active') {
        log.debug('AuthService.resendVerificationEmail : membership', membership.status);
        const notExpired = await UserRepository.findByEmailVerificationToken(user.emailVerificationToken, options);
        if (notExpired) {
          // user was invited and the invite was cancelled, or they were set to inactive by the administrator
          let msg = i18n(options.language, 'auth.userMembershipDisabled', membership.account.orgName);
          switch (membership.status) {
            case 'Pending':
              // user was invited, may have lost their invitation email
              return this.resendUserInvite(user.email, membership.account.orgName, options);
            case 'Requesting':
              // user requested access, waiting on admin action
              msg = i18n(options.language, 'auth.userMembershipRequesting', membership.account.orgName);
              break;
            default:
              break;
          }
          log.debug('AuthService.resendVerificationEmail : AuthEmails.sendPendingEmail');
          return AuthEmails.sendPendingEmail(email, firstName, msg, options);
        }
        // expired invitation, generate a new token and resend the email
        const admin = await AccountMemberRepository.findOwnerByAccountId(membership.account.id, options);
        return AuthEmails.sendUserInvitationEmail(user.email, admin.firstName || admin.email, options);
      }
      if (membership.roles.includes('Owner')) {
        // this was a new account for a new user, they need the normal finish registration
        log.debug('AuthService.resendVerificationEmail : sendEmailAddressVerificationEmail');
        return AuthEmails.sendEmailAddressVerificationEmail(options.language, user.email, firstName, options);
      }
      // this was an existing account for a new user, the admin approved their request, but the token expired
      log.debug('AuthService.resendVerificationEmail : AccountMemberRepository.findOwnerByAccountId');
      const admin = await AccountMemberRepository.findOwnerByAccountId(membership.account.id, options);
      return AuthEmails.sendRequestApprovedEmail(user.email, firstName, admin.firstName || admin.email, membership.account.orgName, options);
    }
    try {
      log.debug('AuthService.resendVerificationEmail : validateDomain');
      const type = await validateDomain(email, { ...options, skipAccountUpdate: true });
      const acct = await new AccountRepository().findByDomain(email, options);
      const admin = await AccountMemberRepository.findOwnerByAccountId(acct.id, options);
      return AuthEmails.sendNextStepEmail({ email, firstName, adminEmail: admin.email, adminFirstName: admin.firstName }, type === 'Open' ? type : 'resendClosed', options);
    } catch (error) {
      log.error(error);
      // if we fail to find an account here the user is an orphan and should get the deleted email
      return AuthEmails.sendOrphanedUserEmail(email, firstName, options);
    }
  }

  /**
   * Verifies the user email based on the token.
   *
   * @param {String} token
   * @param {Object} [options]
   */
  static async verifyEmail(token, options = {}, invite = false, request = false) {
    const user = await UserRepository.findByEmailVerificationToken(token, options);
    if (!user) {
      if (invite) {
        throw new ValidationError(options.language, 'auth.inviteExpired.invalidToken');
      } else {
        throw new ValidationError(options.language, 'auth.emailAddressVerificationEmail.invalidToken');
      }
    }

    const verified = await UserRepository.markEmailVerified(user.id, options);

    if (verified && !request) {
      const jwt = await this.jwt(user);
      const refresh = await this.refreshToken(user);

      return [jwt, refresh];
    }
    return null;
  }

  /**
   * Resets the password, validating the password reset token.
   *
   * @param {String} token
   * @param {String} password
   * @param {Object} [options]
   */
  static async passwordReset(token, password, options = {}) {
    const user = await UserRepository.findByPasswordResetToken(token, options);

    if (!user || !user.emailVerified) {
      throw new ValidationError(options.language, 'auth.passwordReset.invalidToken');
    }

    // Check the password complexity
    if (!checkComplexity(password)) {
      throw new ValidationError(options.language, 'auth.weakPassword');
    }

    const hashedPassword = await bcrypt.hash(password, BCRYPT_SALT_ROUNDS);

    return !!UserRepository.updatePassword(user.id, hashedPassword, options);
  }
}

module.exports = AuthService;
