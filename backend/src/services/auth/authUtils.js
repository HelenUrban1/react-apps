const { invalidDomains } = require('../../staticData/defaults');
const { log } = require('../../logger');
const SequelizeRepository = require('../../database/repositories/sequelizeRepository');
const models = require('../../database/models');
const ValidationError = require('../../errors/validationError');
const AccountRepository = require('../../database/repositories/accountRepository');
const AccountMemberRepository = require('../../database/repositories/accountMemberRepository');
const SettingRepository = require('../../database/repositories/settingRepository');
const UserProfileRepository = require('../../database/repositories/userProfileRepository');

/**
 *
 * @param {String} password
 * @return {Boolean} Whether the password meets complexity requirements
 */
export const checkComplexity = (password) => {
  const hasLength = password.length >= 10;
  const hasUpperCase = /[A-Z]/.test(password);
  const hasLowerCase = /[a-z]/.test(password);
  const hasSpecialChar = /[!@#$%^&*()]/.test(password);
  const hasNumber = /[0-9]/.test(password);
  const isEmail = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(password);
  return hasLength && hasUpperCase && hasLowerCase && hasSpecialChar && hasNumber && !isEmail;
};

/**
 *
 * @param {*} email
 * @returns
 */
export const convertEmailToCognitoUsername = (email) => {
  const regDot = /\./gi;
  const regAt = /@/gi;
  const regPlus = /\+/gi;

  return email.replace(regAt, '__at__').replace(regDot, '__dot__').replace(regPlus, '__plus__');
};

/**
 *
 * @param {*} username
 * @returns
 */
export const convertCognitoUsernameToEmail = (username) => {
  const regDot = /(__dot__)/gi;
  const regAt = /(__at__)/gi;
  const regPlus = /(__plus__)/gi;

  return username.replace(regAt, '@').replace(regDot, '.').replace(regPlus, '+');
};

/**
 * Validates the email domain against invalid domains and returns number of accounts with domain
 * @param {String} email
 * @param {*} options
 * @returns
 */
export const validateDomain = async (email, options) => {
  log.debug('AuthUtils.validateDomain');
  if (!email.includes('@') || !email.includes('.')) {
    log.error(`Email of "${email}" is invalid`);
    throw new ValidationError('en', 'Email is invalid');
  }
  const domain = email.split('@').pop();
  if (!domain) {
    log.error(`No domain found for "${email}"`);
    throw new ValidationError('en', 'Email is invalid');
  }

  // check if domain is blacklisted
  if (invalidDomains.includes(domain)) {
    log.debug(`Domain for email "${email}" is blacklisted.`);
    return 'Freemail';
  }

  try {
    // Check if account already exists for this domain
    const accountRepo = new AccountRepository();
    const { rows } = await accountRepo.findAndCountAll();
    if (rows) {
      const accts = [];
      rows.forEach((acct) => {
        if (acct.orgEmail) {
          const acctDomain = acct.orgEmail.split('@').pop();
          if (!acctDomain) {
            log.warning(`Account of email "${acct.orgEmail}" does not have a recognizable domain`);
          }
          if (acctDomain && acctDomain === domain) accts.push(acct);
        }
      });
      if (accts.length === 0) {
        log.debug('No account exists for the domain. Create a new account.');
        return 'New'; // no accounts share domain
      }
      if (accts.length === 1) {
        log.debug('One account exists for the domain. Offer to be added to existing account.');
        // one account shares domain, get account's request policy setting and return
        const owner = await AccountMemberRepository.findOwnerByAccountId(accts[0].id, options);
        const membership = await AccountMemberRepository.findByUserAndAccount(owner.id, accts[0].id, options);
        const newOptions = { ...options, currentUser: { id: owner.id, accountId: membership.accountId, siteId: membership.siteId, userId: owner.id } };
        const accountRequestSetting = await SettingRepository.findByName('request', newOptions);

        if ((!accountRequestSetting || !accountRequestSetting.value || accountRequestSetting.value === 'Closed' || options.override) && !options.skipAccountUpdate) {
          log.debug('Updating request settings for account...');
          // single account has Closed policy, need to update request setting flag on Account
          const currentSetting = JSON.parse(accts[0].settings || '{}');
          currentSetting.allowRequestPolicy = false;
          // TODO: This update was erasing accountId from the account's associated accountMember
          // ...not sure why, using flag to skip members update in accountRepo.update
          newOptions.skipMembersUpdate = true;
          await accountRepo.update(accts[0].id, { settings: JSON.stringify(currentSetting) }, newOptions);
        }
        if (accountRequestSetting && accountRequestSetting.value) {
          log.debug(`Account request setting found. "${accountRequestSetting.value}" will be returned.`);
          return accountRequestSetting.value;
        }
        return 'Closed';
      }
      log.debug('More than one account exists for the domain. Create a new account.');
    }
    log.warn('No accounts were found.');
    return 'Closed'; // Unexpected results? Validation was unsuccessful, prevent access requests
  } catch (error) {
    log.debug(`Error validating email "${email}" of domain "${domain}"`);
    log.error(error);
    throw new ValidationError('en', 'account.errors.genericDomain');
  }
};

/**
 * Deletes User and Account Associations
 * @param {Object} user
 * @param {Object} account
 * @param {Object} accountMember
 * @param {Object} site
 * @param {Boolean} force - true for hard delete, false for soft
 * @param {Object} options
 */
export const deleteUserAndAccountRecords = async ({ user, account, accountMember, site }, force = false, options) => {
  const transaction = SequelizeRepository.getTransaction(options);

  if (user) {
    log.debug(`deleting user ${user.email} ${user.id} hard=${force}`);
    await models.user.destroy({ where: { id: user.id }, force, transaction });
  }

  if (account) {
    log.debug(`deleting account ${account.orgName} ${account.id} hard=${force}`);
    await models.account.destroy({ where: { id: account.id }, force, transaction });
  }

  if (accountMember) {
    log.debug(`deleting accountMember ${accountMember.userId} ${accountMember.accountId} ${accountMember.siteId} hard=${force}`);
    await models.accountMember.destroy({ where: { id: accountMember.id }, force, transaction });
  }

  if (site) {
    log.debug(`deleting site ${site.id} hard=${force}`);
    await models.site.destroy({ where: { id: site.id }, force, transaction });
  }
};

/**
 * Converts a User who had an expired Invite or Request to a Trial Account
 * @param {String!} email
 * @param {Object} options
 * @returns
 */
export const convertToOrphan = async (email, options = {}, disabled = false, UserRepository) => {
  log.debug('AuthUtils.convertToOrphan');
  const transaction = SequelizeRepository.getTransaction(options);
  const user = await UserRepository.findByEmail(email, { ...options, userOnly: true });
  const membership = await models.accountMember.findOne(
    {
      where: { userId: user.id },
    },
    {
      transaction,
    }
  );
  if (membership) {
    await AccountMemberRepository.destroy(membership.id, { ...options });
    const profile = await UserProfileRepository.findByUserId(user.id, { ...options, accountId: membership.accountId });
    if (profile) {
      log.debug('deleting user profile');
      await UserProfileRepository.destroy(profile.id, { ...options, accountId: membership.accountId });
    }
  }
  const success = await UserRepository.updateUserOnly(
    user.id,
    {
      authenticationUid: null,
      activeAccountMemberId: null,
      disabled,
      emailVerified: false,
      password: null,
      signupMeta: JSON.stringify({ firstName: user.firstName || '', email }),
    },
    { ...options, userOnly: true }
  );
  return success;
};
