const Amplify = require('aws-amplify');
const EmailSender = require('../shared/email/emailSender');
const UserRepository = require('../../database/repositories/userRepository');
const ValidationError = require('../../errors/validationError');
const { log } = require('../../logger');
const AccountMemberRepository = require('../../database/repositories/accountMemberRepository');
const { i18n } = require('../../i18n');
const AuthEmails = require('./authEmails');
const CognitoAuthCommands = require('./cognitoAuthCommands');
const { convertEmailToCognitoUsername, validateDomain } = require('./authUtils');

const { Auth } = Amplify;

class CognitoAuthEmails {
  /**
   *
   * @param {*} email
   * @param {*} options
   * @returns
   */
  static async cognitoSendEmailAddressVerificationEmail(email, options = {}) {
    log.debug('CognitoAuthEmails.cognitoSendEmailAddressVerificationEmail');
    const username = convertEmailToCognitoUsername(email);
    const user = await UserRepository.findByEmailWithoutAvatar(email, options);

    log.debug('send verification email user');
    log.debug(user);

    // update user data
    const attributes = [
      { Name: 'custom:account', Value: user.account.id },
      { Name: 'custom:status', Value: options.isReminder ? 'emailAddressVerificationReminder' : 'emailAddressVerification' },
    ];

    log.debug('CognitoAuthEmails.cognitoSendEmailAddressVerificationEmail : attributes', { email, attributes, options });
    await CognitoAuthCommands.cognitoUpdateUserAttributes(email, attributes, options);
    await UserRepository.generateEmailVerificationToken(email, options);

    const signupMeta = user.signupMeta ? JSON.parse(user.signupMeta) : null;

    await Auth.resendSignUp(username, {
      source: options.isReminder ? 'emailAddressVerificationReminder' : 'emailAddressVerification',
      firstName: signupMeta ? signupMeta.firstName : user.firstName || '',
    });

    return true;
  }

  /**
   *
   * @param {*} email
   * @param {*} password
   * @param {*} sender
   * @param {*} accountId
   * @param {*} options
   * @returns
   */
  static async cognitoSendUserInvitationEmail(email, password, sender, accountId, options = {}) {
    log.debug('CognitoAuthEmails.cognitoSendUserInvitationEmail');
    const username = convertEmailToCognitoUsername(email);
    await UserRepository.generateEmailVerificationToken(email, options);

    await Auth.signUp({
      username,
      password,
      clientMetadata: {
        source: 'invitation',
        sender,
      },
      attributes: {
        email,
        'custom:account': accountId,
        'custom:status': 'invitation',
      },
    });

    return true;
  }

  /**
   *
   * @param {*} email
   * @param {*} firstName
   * @param {*} options
   * @returns
   */
  static async cognitoResendUserInvite(email, firstName, options = {}) {
    log.debug('CognitoAuthEmails.cognitoResentUserInvite');
    const username = convertEmailToCognitoUsername(email);
    const user = await UserRepository.findByEmailWithoutAvatar(email, options);

    // update user data
    const attributes = [
      { Name: 'custom:account', Value: user.account.id },
      { Name: 'custom:status', Value: 'invitation' },
    ];
    await CognitoAuthCommands.cognitoUpdateUserAttributes(email, attributes, options);
    await UserRepository.generateEmailVerificationToken(email, options);

    await Auth.resendSignUp(username, {
      source: 'invitation',
      sender: firstName,
    });

    return true;
  }

  /**
   *
   * @param {*} email
   * @param {*} firstName
   * @param {*} options
   * @returns
   */
  static async cognitoSendRequestApproved(email, sender, orgName, options = {}) {
    log.debug('cognitoSendRequestApproved');
    const { currentUser } = options;
    const username = convertEmailToCognitoUsername(email);
    await CognitoAuthCommands.cognitoEnableUser(email, options);

    // update user data
    const attributes = [
      { Name: 'custom:account', Value: currentUser.accountId },
      { Name: 'custom:status', Value: 'requestApproved' },
    ];
    await CognitoAuthCommands.cognitoUpdateUserAttributes(email, attributes, options);
    await UserRepository.generateEmailVerificationToken(email, options);

    await Auth.resendSignUp(username, {
      source: 'requestApproved',
      sender,
      orgName,
    });

    return true;
  }

  /**
   *
   * @param {*} email
   * @param {*} firstName
   * @param {*} source
   * @param {*} options
   * @returns
   */
  static async cognitoSendOrphanEmail(email, firstName, source, options = {}) {
    log.debug('CognitoAuthEmails.cognitoSendOrphanEmail');
    const { currentUser } = options;
    const username = convertEmailToCognitoUsername(email);

    // update user data
    const attributes = [
      { Name: 'custom:account', Value: currentUser.accountId },
      { Name: 'custom:status', Value: source },
    ];
    await CognitoAuthCommands.cognitoUpdateUserAttributes(email, attributes, options);

    // refresh verification token in our DB
    await UserRepository.generateEmailVerificationToken(email, options);

    await Auth.resendSignUp(username, {
      source,
      firstName,
    });

    return true;
  }

  /**
   *
   * @param {email, firstName}
   * @param {*} type
   * @param {*} options
   * @returns
   */
  static async cognitoSendNextStepEmail({ email, firstName }, type, options = {}) {
    log.debug('CognitoAuthService.cognitoSendNextStepEmail');

    // const { currentUser } = options;
    // await CognitoAuthCommands.cognitoEnableUser(email, options);

    // // update user data
    // const attributes = [
    //   { Name: 'custom:account', Value: currentUser.accountId },
    //   { Name: 'custom:status', Value: type },
    // ];
    // await CognitoAuthCommands.cognitoUpdateUserAttributes(email, attributes, options);

    // Refresh verification token in our DB
    await UserRepository.generateEmailVerificationToken(email, options);

    switch (type) {
      case 'Open':
        // send an email with link to request access
        return this.cognitoSendOrphanEmail(email, firstName, 'orphanRequestOpen', options);
      case 'resendClosed':
        // resends the closed request orphan email
        return this.cognitoSendOrphanEmail(email, firstName, 'resendClosed', options);
      default:
        throw new ValidationError(options.langauge, 'auth.cognito.cognitoError');
    }
  }

  /**
   * Sends an email address verification email to an orphan account
   *
   * @param {String} language
   * @param {String} email
   * @param {Object} [options]
   */
  static async cognitoResendVerificationEmail(email, options) {
    log.debug('CognitoAuthEmails.cognitoResendVerificationEmail');
    if (!EmailSender.isConfigured) {
      throw new Error(`Email provider is not configured. Please configure it at backend/config/<environment>.json.`);
    }

    const user = await UserRepository.findByEmail(email, options);
    if (!user) return true;

    const firstName = user.firstName || JSON.parse(user.signupMeta || '{}').firstName;

    if (user.emailVerified) {
      // User is already verified, no need to send through Cognito
      return AuthEmails.sendVerifiedUserEmail(user.email, firstName, options);
    }

    if (user && user.accountId) {
      const membership = await AccountMemberRepository.findByUserAndAccount(user.id, user.accountId, options);
      if (membership && membership.status !== 'Active') {
        const notExpired = await UserRepository.findByEmailVerificationToken(user.emailVerificationToken, options);
        if (notExpired) {
          // user was invited and the invite was cancelled, or they were set to inactive by the administrator
          let msg = i18n(options.language, 'auth.userMembershipDisabled', membership.account.orgName);
          switch (membership.status) {
            case 'Pending':
              // user was invited, may have lost their invitation email
              return this.cognitoResendUserInvite(user.email, membership.account.orgName, options);
            case 'Requesting':
              // user requested access, waiting on admin action
              msg = i18n(options.language, 'auth.userMembershipRequesting', membership.account.orgName);
              break;
            default:
              break;
          }
          // just informational, no need to send through cognito
          return AuthEmails.sendPendingEmail(email, firstName, msg, options);
        }
        // expired invitation, generate a new token and resend the email
        const admin = await AccountMemberRepository.findOwnerByAccountId(membership.account.id, options);
        return this.cognitoResendUserInvite(user.email, admin.firstName || admin.email, options);
      }
      if (membership.roles.includes('Owner')) {
        // this was a new account for a new user, they need the normal finish registration
        return this.cognitoSendEmailAddressVerificationEmail(user.email, options);
      }
      // this was an existing account for a new user, the admin approved their request, but the token expired
      const admin = await AccountMemberRepository.findOwnerByAccountId(membership.account.id, options);
      return this.cognitoSendRequestApproved(user.email, admin.firstName || admin.email, membership.account.orgName, options);
    }
    try {
      const type = await validateDomain(email, { ...options, skipAccountUpdate: true });
      return this.cognitoSendNextStepEmail({ email, firstName }, type === 'Open' ? type : 'resendClosed', options);
    } catch (error) {
      log.error(error);
      // if we fail to find an account here the user is an orphan and should get the deleted email
      // This email contains no link, no need to run through Cognito
      return AuthEmails.sendOrphanedUserEmail(email, firstName, options);
    }
  }
}

module.exports = CognitoAuthEmails;
