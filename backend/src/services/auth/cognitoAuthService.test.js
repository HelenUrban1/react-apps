const Amplify = require('aws-amplify');
const config = require('../../../config')();

const USE_MOCKS = config.useMocks;

if (USE_MOCKS) {
  jest.doMock('../../database/models');
  jest.doMock('../../database/repositories/accountRepository');
  jest.doMock('../../database/repositories/accountMemberRepository');
  jest.doMock('../../database/repositories/userRepository');
  jest.doMock('../../database/repositories/siteRepository');
  jest.doMock('../../database/repositories/markerRepository');
  jest.doMock('../../database/repositories/organizationRepository');
}

const AccountRepository = require('../../database/repositories/accountRepository');
const AccountMemberRepository = require('../../database/repositories/accountMemberRepository');
const UserRepository = require('../../database/repositories/userRepository');
const SiteRepository = require('../../database/repositories/siteRepository');
const MarkerRepository = require('../../database/repositories/markerRepository');
const OrganizationRepository = require('../../database/repositories/organizationRepository');
const models = require('../../database/models');

const AuthService = require('./authService');
const CognitoAuthService = require('./cognitoAuthService');
const SequelizeRepository = require('../../database/repositories/sequelizeRepository');
const ValidationError = require('../../errors/validationError');
const accountMemberData = require('../../__fixtures__').accountMember.data;
const userData = require('../../__fixtures__').user.data;
const AuthEmails = require('./authEmails');
const CognitoAuthEmails = require('./cognitoAuthEmails');
const CognitoAuthCommands = require('./cognitoAuthCommands');

const jwt = require('jsonwebtoken');
const axios = require('axios');
jest.mock('axios');

const { Auth } = Amplify;

const sendgridApiKeyFromConfig = config.email.sendgrid_api_key;

describe('AuthService', () => {
  let signedUpUserFixture;
  let invitedUserFixture;
  let accountMemberFixture;
  const options = {
    currentUser: {
      accountId: config.testAccountId,
    },
  };

  const bsSubscription = {
    id: 41107825,
    state: 'trialing',
    trial_started_at: '2021-04-06T14:09:16-04:00',
    trial_ended_at: '2025-04-06T14:09:16-04:00',
    activated_at: null,
    created_at: '2021-04-06T14:09:16-04:00',
    updated_at: '2021-04-06T14:09:17-04:00',
    expires_at: null,
    balance_in_cents: 0,
    current_period_ends_at: '2025-04-06T14:09:16-04:00',
    next_assessment_at: '2025-04-06T14:09:16-04:00',
    canceled_at: null,
    cancellation_message: null,
    next_product_id: null,
    next_product_handle: null,
    cancel_at_end_of_period: false,
    payment_collection_method: 'remittance',
    snap_day: null,
    cancellation_method: null,
    current_period_started_at: '2021-04-06T14:09:16-04:00',
    previous_state: 'trialing',
    signup_payment_id: 487093353,
    signup_revenue: '0.00',
    delayed_cancel_at: null,
    coupon_code: null,
    total_revenue_in_cents: 0,
    product_price_in_cents: 0,
    product_version_number: 1,
    payment_type: null,
    referral_code: null,
    coupon_use_count: null,
    coupon_uses_allowed: null,
    reason_code: null,
    automatically_resume_at: null,
    coupon_codes: [],
    offer_id: null,
    payer_id: 42063190,
    receives_invoice_emails: null,
    product_price_point_id: 969487,
    next_product_price_point_id: null,
    credit_balance_in_cents: 0,
    prepayment_balance_in_cents: 0,
    net_terms: null,
    stored_credential_transaction_id: null,
    locale: null,
    reference: '4943213356',
    currency: 'USD',
    on_hold_at: null,
    scheduled_cancellation_at: null,
    customer: {
      id: 42063190,
      first_name: 'Test',
      last_name: 'User',
      organization: null,
      email: 'test@testing.com',
      created_at: '2021-04-06T14:09:16-04:00',
      updated_at: '2021-04-06T14:09:16-04:00',
      reference: 'ef1839a3-9693-4e8e-89a2-f55173df1e61',
      address: null,
      address_2: null,
      city: null,
      state: null,
      state_name: null,
      zip: null,
      country: null,
      country_name: null,
      phone: '',
      portal_invite_last_sent_at: null,
      portal_invite_last_accepted_at: null,
      verified: false,
      portal_customer_created_at: null,
      vat_number: null,
      cc_emails: null,
      tax_exempt: false,
      parent_id: null,
      locale: null,
    },
    product: {
      id: 5256309,
      name: 'IX3 Trial',
      handle: 'ix_standard',
      description: '',
      accounting_code: '',
      request_credit_card: true,
      expiration_interval: null,
      expiration_interval_unit: 'never',
      created_at: '2020-09-01T20:56:13-04:00',
      updated_at: '2020-10-25T16:28:06-04:00',
      price_in_cents: 0,
      interval: 1,
      interval_unit: 'month',
      initial_charge_in_cents: null,
      trial_price_in_cents: 0,
      trial_interval: 48,
      trial_interval_unit: 'month',
      archived_at: null,
      require_credit_card: false,
      default_product_price_point_id: 969487,
      request_billing_address: false,
      require_billing_address: false,
      require_shipping_address: false,
      product_price_point_id: 969487,
      product_price_point_name: 'Standard',
      product_price_point_handle: 'standard',
      product_family: [Object],
      public_signup_pages: [],
    },
    group: null,
  };

  const subWorkerMock = {
    createTrial: jest.fn(),
    addContactToAccount: jest.fn(),
    updateAccountDetails: jest.fn(),
  };

  const accountData = {
    id: 'dc9beb03-71a1-4939-88b5-41f6660c9c31',
    status: 'Active',
    settings: '{"allowRequestPolicy":true}',
    orgName: 'Admin Org',
    orgPhone: null,
    orgEmail: 'org@testing.com',
    orgStreet: null,
    orgStreet2: null,
    orgCity: null,
    orgRegion: null,
    orgPostalcode: null,
    orgCountry: null,
    orgUnitName: null,
    orgUnitId: null,
    parentAccountId: null,
    importHash: null,
    dbHost: 'localhost',
    dbName: 'ixc_account_test_dc9beb03-71a1-4939-88b5-41f6660c9c31',
    createdAt: '2021-05-12T14:36:19.669Z',
    updatedAt: '2021-05-12T14:36:19.669Z',
    deletedAt: null,
    createdById: null,
    updatedById: null,
    members: [
      {
        paid: true,
        id: '681544ec-d799-4a3f-a6bd-199f9071f981',
        settings: '{}',
        roles: ['Owner', 'Admin'],
        status: 'Active',
        accessControl: false,
        importHash: null,
        createdAt: '2021-05-12T15:01:25.795Z',
        updatedAt: '2021-05-12T15:01:25.808Z',
        deletedAt: null,
        accountId: '95d05d48-082d-49d1-bf35-4ba6c543f990',
        siteId: 'affaa7c4-f877-4c39-81c7-890f6ffb574a',
        userId: 'afd6e2ab-74ea-4947-9612-58696b44a21c',
        createdById: null,
        updatedById: null,
      },
    ],
  };

  const accountMock = {
    createMock: jest.fn().mockReturnValue(accountData),
    updateMock: jest.fn().mockReturnValue(accountData),
    findAndCountAllMock: jest.fn().mockReturnValue({
      rows: [],
      count: 0,
    }),
    findByIdMock: jest.fn().mockReturnValue(accountData),
    findByDomainMock: jest.fn().mockReturnValue(accountData),
    destroyMock: jest.fn(),
  };

  const accountMemberMock = {
    findOwnerByAccountIdMock: jest.fn().mockReturnValue(accountMemberData.accountAUserAPaidRoles),
    findByUserAndAccountMock: jest.fn().mockReturnValue(accountMemberData.accountAUserAPaidRoles),
    findByIdMock: jest.fn().mockReturnValue(accountMemberData.accountAUserAPaidRoles),
    findByUserMock: jest.fn().mockReturnValue([]),
    updateMock: jest.fn().mockReturnValue({ ...accountMemberData.accountAUserAPaidRoles, status: 'Active' }),
  };

  const userMock = {
    createOrphanFromAuthMock: jest.fn().mockReturnValue(userData[0]),
    findByEmailMock: jest.fn().mockReturnValueOnce(null).mockReturnValue(userData[3]),
    createFromAuthMock: jest.fn().mockReturnValue(userData[0]),
    adoptOrphanToAccountMock: jest.fn().mockReturnValue(userData[0]),
    findByIdMock: jest.fn().mockReturnValue(userData[0]),
    findByEmailWithoutAvatarMock: jest.fn().mockReturnValue(userData[0]),
    updatePasswordMock: jest.fn().mockReturnValue(userData[0]),
    findByEmailVerificationTokenMock: jest.fn().mockReturnValue(userData[3]),
    markEmailVerifiedMock: jest.fn().mockReturnValue(true),
    updateProfileMock: jest.fn().mockReturnValue(userData[0]),
    generateEmailVerificationToken: jest.fn().mockReturnValue(userData[0].emailVerificationToken),
  };

  const siteMock = {
    createMock: jest.fn().mockReturnValue({}),
  };

  const mockRepositoryFunctions = () => {
    models.getTenant = () => Promise.resolve({});

    models.sequelizeAdmin = {
      query: jest.fn().mockReturnValue(Promise.resolve({})),
    };
    models.user.findOne = () => Promise.resolve(userData[0]);

    SequelizeRepository.commitTransaction = jest.fn();
    SequelizeRepository.createTransaction = jest.fn();
    AccountRepository.mockImplementation(() => {
      return {
        create: accountMock.createMock,
        update: accountMock.updateMock,
        findAndCountAll: accountMock.findAndCountAllMock,
        findById: accountMock.findByIdMock,
        findByDomain: accountMock.findByDomainMock,
        destroy: accountMock.destroyMock,
      };
    });
    AccountMemberRepository.findOwnerByAccountId = accountMemberMock.findOwnerByAccountIdMock;
    AccountMemberRepository.findByUserAndAccount = accountMemberMock.findByUserAndAccountMock;
    AccountMemberRepository.findByUser = accountMemberMock.findByUserMock;
    AccountMemberRepository.update = accountMemberMock.updateMock;
    AccountMemberRepository.findById = accountMemberMock.findByIdMock;
    UserRepository.createOrphanFromAuth = userMock.createOrphanFromAuthMock;
    UserRepository.findByEmail = userMock.findByEmailMock;
    UserRepository.findByEmailWithoutAvatar = userMock.findByEmailWithoutAvatarMock;
    UserRepository.generateEmailVerificationToken = userMock.generateEmailVerificationToken;
    UserRepository.createFromAuth = userMock.createFromAuthMock;
    UserRepository.adoptOrphanToAccount = userMock.adoptOrphanToAccountMock;
    UserRepository.findById = userMock.findByIdMock;
    UserRepository.findByEmailWithoutAvatar = userMock.findByEmailWithoutAvatarMock;
    UserRepository.updatePassword = userMock.updatePasswordMock;
    UserRepository.findByEmailVerificationToken = userMock.findByEmailVerificationTokenMock;
    UserRepository.markEmailVerified = userMock.markEmailVerifiedMock;
    UserRepository.updateProfile = userMock.updateProfileMock;
    SiteRepository.mockImplementation(() => {
      return {
        create: siteMock.createMock,
      };
    });
    MarkerRepository.mockImplementation(() => {
      return {
        create: jest.fn().mockReturnValue({}),
      };
    });
    OrganizationRepository.mockImplementation(() => {
      return {
        create: jest.fn().mockReturnValue({}),
      };
    });
  };

  beforeAll(async () => {
    // Workaround for EmailSender running and erroring if its env var is set
    config.email.sendgrid_api_key = undefined;

    if (USE_MOCKS) {
      mockRepositoryFunctions();
    } else {
      await SequelizeRepository.cleanDatabase(options.currentUser.accountId);
      await SequelizeRepository.cleanDatabase();
    }
    signedUpUserFixture = { ...userData[0] };
    expect(signedUpUserFixture.authenticationUid).toBeDefined();
    invitedUserFixture = { ...userData[3] };
    expect(invitedUserFixture.authenticationUid).not.toBeDefined();

    accountMemberFixture = { ...accountMemberData.accountAUserAPaidRoles, user: signedUpUserFixture.id };

    if (!USE_MOCKS) {
      await UserRepository.create(signedUpUserFixture, options);
      await AccountMemberRepository.create(accountMemberFixture, options);
    }

    const mockSendInviteEmail = jest.spyOn(AuthEmails, 'sendUserInvitationEmail');
    mockSendInviteEmail.mockReturnValue(true);

    // TODO: All functions should take named params, so we don't get inputs confused
    // 3rd param "control" was missing from this call
    // The siteId is '31ef1d56-1b86-4ffa-b886-507ed8f021c5'
    await AuthService.inviteNewUser(invitedUserFixture.email, ['Collaborator'], false, config.testAccountId, '31ef1d56-1b86-4ffa-b886-507ed8f021c5', 'any', options);
  });

  beforeEach(() => {
    jest.clearAllMocks();

    if (USE_MOCKS) {
      mockRepositoryFunctions();
    }
  });

  afterAll(async () => {
    config.email.sendgrid_api_key = sendgridApiKeyFromConfig;

    if (!USE_MOCKS) {
      await SequelizeRepository.cleanDatabase(options.currentUser.accountId);
      await SequelizeRepository.cleanDatabase();
      await SequelizeRepository.closeConnections(options.currentUser.accountId);
      await SequelizeRepository.closeConnections();
    }
  });

  describe('#signup()', () => {
    it('should update the password of an invited user that has not previously signed up', async () => {
      if (USE_MOCKS) {
        const findByEmailMock = jest.fn().mockReturnValueOnce(userData[3]).mockReturnValue(userData[5]);
        UserRepository.findByEmail = findByEmailMock;
      }
      const rawPassword = 'Somevalue1!';
      const firstName = 'user';
      const lastName = 'c';
      let user = await UserRepository.findByEmail(invitedUserFixture.email, options);
      expect(user).not.toBe(null);
      const originalPassword = user.password;
      subWorkerMock.createTrial.mockResolvedValueOnce({ bsSubscription });
      subWorkerMock.addContactToAccount.mockResolvedValueOnce(true);
      subWorkerMock.updateAccountDetails.mockResolvedValueOnce(true);
      const mockFindUserByToken = jest.spyOn(UserRepository, 'findByEmailVerificationToken');
      mockFindUserByToken.mockResolvedValueOnce(user);
      const mockFindByUserAndAccount = jest.spyOn(AccountMemberRepository, 'findByUserAndAccount');
      mockFindByUserAndAccount.mockReturnValue(userData[5].accountMemberships[0].dataValues);
      await CognitoAuthService.cognitoRegisterInvitedUser(user.email, rawPassword, { firstName, lastName }, { ...options, subscriptionWorker: subWorkerMock });

      user = await UserRepository.findByEmail(invitedUserFixture.email, options);
      expect(user).not.toBe(null);
      expect(user.password).not.toBe(originalPassword);
      expect(user.password).not.toBe(rawPassword);
      expect(user.authenticationUid).toBeDefined();
    });
  });

  describe('invite users', () => {
    const roles = ['Viewer'];
    const control = false;
    const accountId = config.testAccountId;
    const siteId = '31ef1d56-1b86-4ffa-b886-507ed8f021c5';
    const sender = 'Somebody';

    describe('cognitoInviteNewUser', () => {
      it('creates a new unverified user', async () => {
        if (USE_MOCKS) {
          const findByEmailMock = jest.fn().mockReturnValueOnce(userData[3]);
          UserRepository.findByEmail = findByEmailMock;
        }
        expect.assertions(4);
        const mockSendUserInvitation = jest.spyOn(CognitoAuthEmails, 'cognitoSendUserInvitationEmail');
        mockSendUserInvitation.mockReturnValue(true);
        const email = 'someUniqueEmail@gmail.com';
        const success = await CognitoAuthService.cognitoInviteNewUser(email, roles, control, accountId, siteId, sender, options);
        expect(success).toBeTruthy();
        const user = await UserRepository.findByEmail(email, options);
        expect(user).not.toBe(null);
        expect(user.emailVerificationToken).toBeDefined();
        expect(user.emailVerified).toBeFalsy();
        mockSendUserInvitation.mockRestore();
      });

      it('throws error if email fails', async () => {
        expect.assertions(1);
        const mockSendUserInvitation = jest.spyOn(CognitoAuthEmails, 'cognitoSendUserInvitationEmail');
        mockSendUserInvitation.mockReturnValue(false);
        const email = 'THROWEMAILERROR@gmail.com';
        await expect(CognitoAuthService.cognitoInviteNewUser(email, roles, control, accountId, siteId, {})).rejects.toThrow(Error);
        mockSendUserInvitation.mockRestore();
      });

      it('rejects existing user', async () => {
        if (USE_MOCKS) {
          models.user.findOne = () => Promise.resolve({ ...userData[0], activeAccountMemberId: '7a788809-8673-49ce-884b-2759b53b3a57' });
        }
        expect.assertions(1);
        const mockUserRepositoryCount = jest.spyOn(UserRepository, 'count');
        mockUserRepositoryCount.mockReturnValue(1);
        const email = 'dev.mail.monkey@gmail.com';
        await expect(CognitoAuthService.cognitoInviteNewUser(email, roles, control, accountId, siteId, {})).rejects.toThrow(ValidationError);
        mockUserRepositoryCount.mockRestore();
      });

      it('is case insensitive when searching for users', async () => {
        if (USE_MOCKS) {
          models.user.findOne = () => Promise.resolve({ ...userData[0], activeAccountMemberId: '7a788809-8673-49ce-884b-2759b53b3a57' });
        }
        expect.assertions(1);
        const mockUserRepositoryCount = jest.spyOn(UserRepository, 'count');
        mockUserRepositoryCount.mockReturnValue(1);
        const email = 'DeV.mAiL.mOnKeY@GmAiL.com';
        await expect(CognitoAuthService.cognitoInviteNewUser(email, roles, control, accountId, siteId, {})).rejects.toThrow(ValidationError);
        mockUserRepositoryCount.mockRestore();
      });
    });

    describe('cognitoRegisterInvitedUser', () => {
      it('does not register cancelled invitation', async () => {
        expect.assertions(1);
        const mockFindByEmail = jest.spyOn(UserRepository, 'findByEmailWithoutAvatar');
        mockFindByEmail.mockImplementation(() => {
          return { disabled: true };
        });
        const email = 'dev.mail.monkey@gmail.com';
        const password = 'Somevalue1!';
        const profile = { firstName: 'Fake', lastName: 'Human' };
        await expect(CognitoAuthService.cognitoRegisterInvitedUser(email, password, profile, options)).rejects.toThrow(ValidationError);
        mockFindByEmail.mockRestore();
      });
      it('returns true when registering an invited user', async () => {
        expect.assertions(2);
        const email = 'someUniqueEmail123@gmail.com';
        jest.restoreAllMocks();
        const mockSendUserInvitation = jest.spyOn(CognitoAuthEmails, 'cognitoSendUserInvitationEmail');
        mockSendUserInvitation.mockReturnValue(true);
        userMock.findByEmailWithoutAvatarMock.mockReturnValue(userData[0]);
        const success = await CognitoAuthService.cognitoInviteNewUser(email, roles, control, accountId, siteId, sender, options);
        expect(success).toBeTruthy();

        const password = 'Somevalue1!';
        const profile = { firstName: 'Fake', lastName: 'Human' };
        const mockFindByUserAndAccount = jest.spyOn(AccountMemberRepository, 'findByUserAndAccount');
        mockFindByUserAndAccount.mockReturnValue(userData[5].accountMemberships[0].dataValues);
        const res = await CognitoAuthService.cognitoRegisterInvitedUser(email, password, profile, { ...options, subscriptionWorker: subWorkerMock });

        expect(res).toBeTruthy();
        mockSendUserInvitation.mockRestore();
      });
      it('verifies and sets password when registering by invitation', async () => {
        if (USE_MOCKS) {
          const findByEmailMock = jest.fn().mockReturnValueOnce(userData[3]).mockReturnValue(userData[4]);
          UserRepository.findByEmail = findByEmailMock;
        }
        expect.assertions(7);
        const email = 'someUniqueEmail456@gmail.com';
        const mockSendUserInvitation = jest.spyOn(AuthEmails, 'sendUserInvitationEmail');
        mockSendUserInvitation.mockReturnValue(true);
        const mockAuthSignUp = jest.spyOn(Auth, 'signUp');
        mockAuthSignUp.mockReturnValue(true);
        const success = await CognitoAuthService.cognitoInviteNewUser(email, roles, control, accountId, siteId, sender, options);
        expect(success).toBeTruthy();
        let user = await UserRepository.findByEmail(email, options);
        expect(user).not.toBe(null);
        const originalPassword = user.password;

        const password = 'Somevalue1!';
        const profile = { firstName: 'Fake', lastName: 'Human' };
        const mockFindByUserAndAccount = jest.spyOn(AccountMemberRepository, 'findByUserAndAccount');
        mockFindByUserAndAccount.mockReturnValue(userData[5].accountMemberships[0].dataValues);
        const res = await CognitoAuthService.cognitoRegisterInvitedUser(email, password, profile, { ...options, subscriptionWorker: subWorkerMock });
        expect(res).toBeTruthy();

        user = await UserRepository.findByEmail(email, options);
        expect(user).not.toBe(null);
        expect(user.password).not.toBe(originalPassword);
        expect(user.password).not.toBe(password); // don't save raw passwords
        expect(user.emailVerified).toBeTruthy();
        mockSendUserInvitation.mockRestore();
      });
      it('does not register invited users with a weak password', async () => {
        if (USE_MOCKS) {
          const findByEmailMock = jest.fn().mockReturnValue(userData[3]);
          UserRepository.findByEmail = findByEmailMock;
        }
        const email = 'someUniqueEmail457@gmail.com';
        const mockSendUserInvitation = jest.spyOn(CognitoAuthEmails, 'cognitoSendUserInvitationEmail');
        mockSendUserInvitation.mockReturnValue(true);
        const mockAuthSignUp = jest.spyOn(Auth, 'signUp');
        mockAuthSignUp.mockReturnValue(true);
        const success = await CognitoAuthService.cognitoInviteNewUser(email, roles, control, accountId, siteId, sender, options);
        expect(success).toBeTruthy();
        let user = await UserRepository.findByEmail(email, options);
        expect(user).not.toBe(null);
        const originalPassword = user.password;

        const password = 'simple';
        const profile = { firstName: 'Fake', lastName: 'Human' };
        await expect(CognitoAuthService.cognitoRegisterInvitedUser(email, password, profile, { ...options, subscriptionWorker: subWorkerMock })).rejects.toThrow(ValidationError);

        user = await UserRepository.findByEmail(email, options);
        expect(user).not.toBe(null);
        expect(user.password).toBe(originalPassword);
        expect(user.emailVerified).toBeFalsy();
        mockSendUserInvitation.mockRestore();
      });
    });

    describe('cognitoCancelInvitedUser', () => {
      it('orphans user on cancel', async () => {
        if (USE_MOCKS) {
          const findByEmailMock = jest
            .fn()
            .mockReturnValueOnce(userData[3])
            .mockReturnValue({ ...userData[3], disabled: true, activeAccountMemberId: null });
          UserRepository.findByEmail = findByEmailMock;
        }
        expect.assertions(3);
        const mockSendUserInvitation = jest.spyOn(CognitoAuthEmails, 'cognitoSendUserInvitationEmail');
        mockSendUserInvitation.mockReturnValue(true);
        const mockAuthSignUp = jest.spyOn(Auth, 'signUp');
        mockAuthSignUp.mockReturnValue(true);
        const email = 'someUniqueEmailabc@gmail.com';
        const success = await CognitoAuthService.cognitoInviteNewUser(email, roles, control, accountId, siteId, sender, options);
        expect(success).toBeTruthy();
        const mockAuthUpdateUser = jest.spyOn(CognitoAuthCommands, 'cognitoUpdateUserAttributes');
        mockAuthUpdateUser.mockReturnValue(true);
        const mockAuthDisableUser = jest.spyOn(CognitoAuthCommands, 'cognitoDisableUser');
        mockAuthDisableUser.mockReturnValue(true);
        await CognitoAuthService.cognitoCancelInvitedUser(email, options);
        const user = await UserRepository.findByEmail(email, options);
        expect(user).not.toBe(null);
        expect(user.activeAccountMemberId).toBe(null);
        mockSendUserInvitation.mockRestore();
      });

      it('throws error if no user found', async () => {
        expect.assertions(1);
        const mockUserRepositoryCount = jest.spyOn(UserRepository, 'count');
        mockUserRepositoryCount.mockReturnValue(1);
        const email = 'neverExisted@gmail.com';
        await expect(CognitoAuthService.cognitoCancelInvitedUser(email, options)).rejects.toThrow(ValidationError);
        mockUserRepositoryCount.mockRestore();
      });

      it('is case insensitive when searching for users', async () => {
        if (USE_MOCKS) {
          const findByEmailMock = jest.fn().mockReturnValue({ ...userData[3], disabled: true, activeAccountMemberId: null });
          UserRepository.findByEmail = findByEmailMock;
        }
        expect.assertions(5);
        const mockSendUserInvitation = jest.spyOn(CognitoAuthEmails, 'cognitoSendUserInvitationEmail');
        mockSendUserInvitation.mockReturnValue(true);
        const mockAuthSignUp = jest.spyOn(Auth, 'signUp');
        mockAuthSignUp.mockReturnValue(true);
        const email = 'someUniqueEmaildef@gmail.com';
        const emailWonky = 'sOmEuNiQuEeMaIlDeF@gmail.com';
        const success = await CognitoAuthService.cognitoInviteNewUser(email, roles, control, accountId, siteId, sender, options);
        expect(success).toBeTruthy();
        const mockAuthUpdateUser = jest.spyOn(CognitoAuthCommands, 'cognitoUpdateUserAttributes');
        mockAuthUpdateUser.mockReturnValue(true);
        const mockAuthDisableUser = jest.spyOn(CognitoAuthCommands, 'cognitoDisableUser');
        mockAuthDisableUser.mockReturnValue(true);
        await CognitoAuthService.cognitoCancelInvitedUser(emailWonky, options);
        let user = await UserRepository.findByEmail(email, options);
        expect(user).not.toBe(null);
        expect(user.activeAccountMemberId).toBe(null);
        user = await UserRepository.findByEmail(emailWonky, options);
        expect(user).not.toBe(null);
        expect(user.activeAccountMemberId).toBe(null);
        mockSendUserInvitation.mockRestore();
      });
    });

    describe('cognitoReactivateInvitedUser', () => {
      it('reactivates cancelled user', async () => {
        if (USE_MOCKS) {
          const findByEmailMock = jest
            .fn()
            .mockReturnValueOnce({ ...userData[3], disabled: true, activeAccountMemberId: null })
            .mockReturnValueOnce({ ...userData[3], disabled: true, activeAccountMemberId: null })
            .mockReturnValueOnce({ ...userData[3], disabled: true, activeAccountMemberId: null })
            .mockReturnValueOnce({ ...userData[3], disabled: false, activeAccountMemberId: '7a788809-8673-49ce-884b-2759b53b3a57' });
          UserRepository.findByEmail = findByEmailMock;
        }
        expect.assertions(6);
        const mockSendUserInvitation = jest.spyOn(CognitoAuthEmails, 'cognitoSendUserInvitationEmail');
        mockSendUserInvitation.mockReturnValue(true);
        const mockAuthSignUp = jest.spyOn(Auth, 'signUp');
        mockAuthSignUp.mockReturnValue(true);
        const email = 'someUniqueEmailxyz@gmail.com';
        let success = await CognitoAuthService.cognitoInviteNewUser(email, roles, control, accountId, siteId, sender, options);
        expect(success).toBeTruthy();
        const mockAuthUpdateUser = jest.spyOn(CognitoAuthCommands, 'cognitoUpdateUserAttributes');
        mockAuthUpdateUser.mockReturnValue(true);
        const mockAuthDisableUser = jest.spyOn(CognitoAuthCommands, 'cognitoDisableUser');
        mockAuthDisableUser.mockReturnValue(true);
        await CognitoAuthService.cognitoCancelInvitedUser(email, options);
        let user = await UserRepository.findByEmail(email, options);
        expect(user).not.toBe(null);
        expect(user.activeAccountMemberId).toBe(null);
        mockAuthSignUp.mockReturnValue(true);
        success = await CognitoAuthService.cognitoInviteNewUser(email, roles, control, accountId, siteId, sender, options);
        expect(success).toBeTruthy();
        user = await UserRepository.findByEmail(email, options);
        expect(user).not.toBe(null);
        expect(user.activeAccountMemberId).not.toBeNull();
        mockSendUserInvitation.mockRestore();
      });

      it('is case insensitive when searching for users', async () => {
        if (USE_MOCKS) {
          const findByEmailMock = jest
            .fn()
            .mockReturnValueOnce({ ...userData[3], disabled: true, activeAccountMemberId: null })
            .mockReturnValueOnce({ ...userData[3], disabled: true, activeAccountMemberId: null })
            .mockReturnValueOnce({ ...userData[3], disabled: true, activeAccountMemberId: null })
            .mockReturnValueOnce({ ...userData[3], disabled: false, activeAccountMemberId: '7a788809-8673-49ce-884b-2759b53b3a57' })
            .mockReturnValueOnce({ ...userData[3], disabled: false, activeAccountMemberId: '7a788809-8673-49ce-884b-2759b53b3a57' });
          UserRepository.findByEmail = findByEmailMock;
        }
        expect.assertions(8);
        const mockSendUserInvitation = jest.spyOn(CognitoAuthEmails, 'cognitoSendUserInvitationEmail');
        mockSendUserInvitation.mockReturnValue(true);
        const email = 'someUniqueEmailuvw@gmail.com';
        const emailWonky = 'sOmEuNiQuEeMaIlUvW@gmail.com';
        const mockAuthSignUp = jest.spyOn(Auth, 'signUp');
        mockAuthSignUp.mockReturnValue(true);
        let success = await CognitoAuthService.cognitoInviteNewUser(email, roles, control, accountId, siteId, sender, options);
        expect(success).toBeTruthy();
        const mockAuthUpdateUser = jest.spyOn(CognitoAuthCommands, 'cognitoUpdateUserAttributes');
        mockAuthUpdateUser.mockReturnValue(true);
        const mockAuthDisableUser = jest.spyOn(CognitoAuthCommands, 'cognitoDisableUser');
        mockAuthDisableUser.mockReturnValue(true);
        await CognitoAuthService.cognitoCancelInvitedUser(email, options);
        let user = await UserRepository.findByEmail(email, options);
        expect(user).not.toBe(null);
        expect(user.activeAccountMemberId).toBe(null);
        mockAuthSignUp.mockReturnValue(true);
        success = await CognitoAuthService.cognitoInviteNewUser(emailWonky, roles, control, accountId, siteId, sender, options);
        expect(success).toBeTruthy();
        user = await UserRepository.findByEmail(emailWonky, options);
        expect(user).not.toBe(null);
        expect(user.activeAccountMemberId).not.toBeNull();
        user = await UserRepository.findByEmail(email, options);
        expect(user).not.toBe(null);
        expect(user.activeAccountMemberId).not.toBeNull();

        mockSendUserInvitation.mockRestore();
      });
    });
  });

  describe('Validate user JWT token', () => {
    afterEach(() => {
      jest.clearAllMocks();
    });

    it('should return current user when token is valid', async () => {
      const accessToken = 'mockAccessToken';
      const decodedJwt = { header: { kid: 'mockKid' } };
      const verifiedToken = { username: 'mock_example_dot_com' };
      const username = 'mock_example_dot_com';
      const currentUser = { email: 'mock@example.com' };

      jwt.decode = jest.fn().mockReturnValue(decodedJwt);
      jwt.verify = jest.fn().mockReturnValue(verifiedToken);
      axios.mockResolvedValue({
        status: 200,
        data: { keys: [{ kid: 'mockKid', n: 'mockN', kty: 'RSA', e: 'mockE' }] },
      });
      UserRepository.findByEmailWithoutAvatar = jest.fn().mockResolvedValue(currentUser);

      const result = await CognitoAuthService.validateUserToken(`Bearer ${accessToken}`);

      expect(result).toEqual(currentUser);

      expect(jwt.decode).toHaveBeenCalledWith(accessToken, { complete: true });
      expect(jwt.verify).toHaveBeenCalledWith(accessToken, expect.any(String), { issuer: expect.any(String), algorithms: ['RS256'] });
      expect(axios).toHaveBeenCalledWith({
        method: 'GET',
        url: expect.any(String),
      });
      expect(UserRepository.findByEmailWithoutAvatar).toHaveBeenCalledWith(username);
    });

    it('should throw error when token is invalid', async () => {
      const accessToken = 'invalidToken';

      jwt.decode = jest.fn().mockReturnValue(null);

      await expect(CognitoAuthService.validateUserToken(`Bearer ${accessToken}`)).rejects.toThrow('Invalid JWT, could not decode');

      expect(jwt.decode).toHaveBeenCalledWith(accessToken, { complete: true });
    });

    it('should throw error when failed to recover JWKs', async () => {
      const accessToken = 'mockAccessToken';

      jwt.decode = jest.fn().mockReturnValue({ header: { kid: 'mockKid' } });
      axios.mockResolvedValue({ status: 500 });

      await expect(CognitoAuthService.validateUserToken(`Bearer ${accessToken}`)).rejects.toThrow('Failed to recover JWKs in Cognito Auth Strategy with statusCode 500');

      expect(jwt.decode).toHaveBeenCalledWith(accessToken, { complete: true });
      expect(axios).toHaveBeenCalledWith({
        method: 'GET',
        url: expect.any(String),
      });
    });

    it('should throw error when any other error occurs', async () => {
      const accessToken = 'mockAccessToken';
      const errorMessage = 'Some error occurred';

      jwt.decode = jest.fn().mockReturnValue({ header: { kid: 'mockKid' } });
      axios.mockRejectedValue(new Error(errorMessage));

      await expect(CognitoAuthService.validateUserToken(`Bearer ${accessToken}`)).rejects.toThrow(errorMessage);

      expect(jwt.decode).toHaveBeenCalledWith(accessToken, { complete: true });
      expect(axios).toHaveBeenCalledWith({
        method: 'GET',
        url: expect.any(String),
      });
    });
  });
});
