const EmailSender = require('../shared/email/emailSender');
const EmailTemplate = require('../../emails/template');
const UserRepository = require('../../database/repositories/userRepository');
const ValidationError = require('../../errors/validationError');
const config = require('../../../config')();
const { log } = require('../../logger');

class AuthEmails {
  static async generateEmailToken(email, options) {
    try {
      return UserRepository.generateEmailVerificationToken(email, options);
    } catch (error) {
      throw new ValidationError(options.language, 'auth.emailAddressVerificationEmail.error');
    }
  }

  /**
   * Sends an email address verification email.
   *
   * @param {String} language
   * @param {String} email
   * @param {Object} [options]
   */
  static async sendEmailAddressVerificationEmail(language, email, name, options) {
    if (!EmailSender.isConfigured && !options.isTest) {
      throw new Error(`Email provider is not configured. Please configure it at backend/config/<environment>.json.`);
    }

    let link;
    try {
      const token = await this.generateEmailToken(email, options);
      link = `${config.clientUrl}/auth/verify-email?token=${token}&email=${encodeURIComponent(email)}&form=new`;
    } catch (error) {
      log.error(error);
      throw new ValidationError(language, 'auth.emailAddressVerificationEmail.error');
    }

    const emailAddressVerificationEmail = new EmailTemplate(language, options.isReminder ? 'emailAddressVerificationReminder' : 'emailAddressVerification', email, link);
    emailAddressVerificationEmail.setRecipient = name || email;
    if (options.isTest || config.jwt.authStrategy === 'cognito') return true;
    const messageId = new EmailSender(emailAddressVerificationEmail).send();
    return messageId !== false; // sendEmailAddressVerificationEmail expects boolean response
  }

  /**
   * Send a registration invitation email.
   *
   * @param {String} email
   * @param {Object} [options]
   * @return {Boolean}
   */
  static async sendUserInvitationEmail(email, admin, options = {}) {
    log.debug('AuthService.sendUserInvitation');
    const token = await this.generateEmailToken(email, options);
    const link = `${config.clientUrl}/auth/verify-email?token=${token}&email=${encodeURIComponent(email)}&form=invite`;
    const invitationEmail = new EmailTemplate(options.language, options.isReminder ? 'invitationReminder' : 'invitation', email, link);
    invitationEmail.setRecipient = email;
    invitationEmail.setSender = admin;
    if (options.isTest || config.jwt.authStrategy === 'cognito') return true;
    const messageId = new EmailSender(invitationEmail).send();
    return messageId !== false; // sendUserInvitationEmail expects boolean response
  }

  /**
   * Sends an access request email to an Admin from a User
   * @param {String} requestingEmail
   * @parma {String} adminFirstName
   * @param {String} adminEmail
   * @param {Object} [options]
   * @return {Boolean}
   */
  static async sendAccessRequestEmail(requestingEmail, adminFirstName, adminEmail, options = {}) {
    log.debug('AuthService.sendAccessRequestEmail');
    const token = await this.generateEmailToken(requestingEmail, options);
    const link = `${config.clientUrl}/auth/request-response?token=${token}&email=${encodeURIComponent(adminEmail)}&response=approve`;
    const alternateLink = `${config.clientUrl}/auth/request-response?token=${token}&email=${encodeURIComponent(adminEmail)}&response=reject`;
    const accessRequestEmail = new EmailTemplate(options.language, 'request', adminEmail, link);
    accessRequestEmail.setRecipient = adminFirstName || adminEmail;
    accessRequestEmail.setSender = requestingEmail;
    accessRequestEmail.setAlternateLink = alternateLink;
    const messageId = new EmailSender(accessRequestEmail).send();
    return messageId !== false; // sendAccessRequestEmail expects boolean response
  }

  /**
   * Sends an email to a new user giving them an option to Request Access or Start Trial
   * @param {String} email
   * @param {String} sender
   * @param {String} type
   * @param {Object} [options]
   * @return {Boolean}
   */
  static async sendOrphanRequestEmail(email, recipientName, type, options = {}) {
    const token = await this.generateEmailToken(email, options);
    const link = `${config.clientUrl}/auth/verify-email?token=${token}&email=${encodeURIComponent(email)}&form=new`;
    const alternateLink = `${config.clientUrl}/auth/request-access?token=${token}&email=${encodeURIComponent(email)}&form=request`;
    const orphanRequestEmail = new EmailTemplate(options.language, type, email, link, alternateLink);
    orphanRequestEmail.setRecipient = recipientName;
    orphanRequestEmail.setSender = email;
    log.debug('sending orphan request email');
    const messageId = new EmailSender(orphanRequestEmail).send();
    return messageId !== false; // sendOrphanRequestEmail expects boolean response
  }

  static async sendOrphanResendClosedEmail(email, firstName, type, options = {}) {
    const token = await this.generateEmailToken(email, options);
    const link = `${config.clientUrl}/auth/verify-email?token=${token}&email=${encodeURIComponent(email)}&form=new`;
    const orphanResendClosedEmail = new EmailTemplate(options.language, type, email, link);
    orphanResendClosedEmail.setRecipient = firstName || email;
    orphanResendClosedEmail.setSender = email;
    const messageId = new EmailSender(orphanResendClosedEmail).send();
    return messageId !== false; // sendOrphanResendClosedEmail expects boolean response
  }

  /**
   * Sends an email to a User notifying them that their Access Request was Approved
   * @param {String} requesterEmail
   * @param {String} sender
   * @param {Object} options
   * @returns
   */
  static async sendRequestApprovedEmail(requestingEmail, requestingFirstName, adminFirstName, orgName, options) {
    const token = await this.generateEmailToken(requestingEmail, options);
    const link = `${config.clientUrl}/auth/verify-email?token=${token}&email=${encodeURIComponent(requestingEmail)}&form=register`;
    const requestApprovedEmail = new EmailTemplate(options.language, 'requestApproved', requestingEmail, link);
    requestApprovedEmail.setRecipient = requestingFirstName;
    requestApprovedEmail.setSender = adminFirstName;
    requestApprovedEmail.setOrganization = orgName;
    const messageId = new EmailSender(requestApprovedEmail).send();
    return messageId !== false; // sendRequestApprovedEmail expects boolean response
  }

  /**
   * Sends an email to a User notifying them that their Access Request was Rejected
   * @param {String} requesterEmail
   * @param {String} sender
   * @param {Object} options
   * @returns
   */
  static async sendRequestRejectedEmail(requestingEmail, requestingFirstName, sender, options) {
    const requestApprovedEmail = new EmailTemplate(options.language, 'requestRejected', requestingEmail);
    requestApprovedEmail.setRecipient = requestingFirstName || requestingEmail;
    requestApprovedEmail.setSender = sender;
    const messageId = new EmailSender(requestApprovedEmail).send();
    return messageId !== false; // sendRequestRejectedEmail expects boolean response
  }

  /**
   * Sends a password reset email.
   *
   * @param {String} language
   * @param {String} email
   * @param {Object} [options]
   */
  static async sendPasswordResetEmail(language, email, options = {}) {
    if (!EmailSender.isConfigured) {
      throw new Error(`Email provider is not configured. Please configure it at backend/config/<environment>.json.`);
    }

    let link;
    let recipient;

    try {
      const { token, user } = await UserRepository.generatePasswordResetToken(email, options);
      if (!token) return true;
      const firstName = user.firstName || JSON.parse(user.signupMeta || '{}').firstName;
      recipient = firstName || user.email;
      link = `${config.clientUrl}/auth/password-reset?token=${token}&email=${encodeURIComponent(email)}`;
    } catch (error) {
      log.error(error);
      throw new ValidationError(language, 'auth.passwordReset.error');
    }

    const passwordResetEmail = new EmailTemplate(language, 'passwordReset', email, link);
    passwordResetEmail.setRecipient = recipient;

    const messageId = new EmailSender(passwordResetEmail).send();
    return messageId !== false; // sendPasswordResetEmail expects boolean response
  }

  static async sendVerifiedUserEmail(email, firstName, options = {}) {
    const link = `${config.clientUrl}/auth/signin`;
    const verifiedUserEmail = new EmailTemplate(options.language, 'verifiedUser', email, link);
    verifiedUserEmail.setRecipient = firstName || email;
    const messageId = new EmailSender(verifiedUserEmail).send();
    return messageId !== false; // sendVerifiedUserEmail expects boolean response
  }

  static async sendOrphanedUserEmail(email, firstName, options = {}) {
    const orphanedUserEmail = new EmailTemplate(options.language, 'orphanedUser', email);
    orphanedUserEmail.setRecipient = firstName || email;
    const messageId = new EmailSender(orphanedUserEmail).send();
    return messageId !== false; // sendOrphanedUserEmail expects boolean response
  }

  static async sendPendingEmail(email, firstName, message, options = {}) {
    const pendingEmail = new EmailTemplate(options.language, 'pendingAction', email);
    pendingEmail.setRecipient = firstName || email;
    pendingEmail.setMessage = message;
    const messageId = new EmailSender(pendingEmail).send();
    return messageId !== false; // sendPendingEmail expects boolean response
  }

  static async sendNextStepEmail({ email, firstName, adminEmail, adminFirstName }, type, options = {}) {
    log.debug('AuthService.sendNextStepEmail');
    let messageId;
    switch (type) {
      case 'Open':
        // send an email with link to request access
        messageId = AuthEmails.sendOrphanRequestEmail(email, firstName, 'orphanRequestOpen', options);
        break;
      case 'Email':
        // send an email with link to request access by email
        messageId = AuthEmails.sendOrphanRequestEmail(email, adminEmail, 'orphanRequestEmail', 'request-access', { ...options, policy: '&policy=Email' });
        break;
      case 'resendClosed':
        messageId = AuthEmails.sendOrphanResendClosedEmail(email, firstName, type, options);
        break;
      default:
        throw new ValidationError(options.language, 'emails.error.failed');
    }
    if (!messageId) {
      throw new ValidationError(options.language, 'emails.error.failed');
    }
    log.debug('AuthService.sendNextStepEmail end');
    return messageId !== false; // sendNextStepEmail expects boolean response
  }
}

module.exports = AuthEmails;
