const {
  AdminDisableUserCommand,
  AdminUpdateUserAttributesCommand,
  AdminGetUserCommand,
  AdminConfirmSignUpCommand,
  AdminEnableUserCommand,
  AssociateSoftwareTokenCommand,
  VerifySoftwareTokenCommand,
  AdminSetUserMFAPreferenceCommand,
  AdminInitiateAuthCommand,
  AdminRespondToAuthChallengeCommand,
  InitiateAuthCommand,
  AdminUpdateDeviceStatusCommand,
  ConfirmDeviceCommand,
  ForgotPasswordCommand,
  ConfirmForgotPasswordCommand,
  AdminListDevicesCommand,
} = require('@aws-sdk/client-cognito-identity-provider');
const { convertEmailToCognitoUsername } = require('./authUtils');
const config = require('../../../config')();
const ValidationError = require('../../errors/validationError');
const { log } = require('../../logger');

class CognitoAuthCommands {
  static async cognitoSendCommand(command, options) {
    log.debug('CognitoAuthCommands.cognitoSendCommand');
    log.debug(command);
    const { cognitoClient, language } = options;
    try {
      const response = await cognitoClient.send(command);
      return response;
    } catch (err) {
      log.debug(err.name);
      const lang = language || 'en';
      err.type = 'cognito';
      switch (err.name) {
        case 'UserNotFoundException':
          // do nothing with this, expected for a user not to exist sometimes (e.g. signup)
          // if its not expected it will be handled elsewhere
          log.info('auth.cognito.UserNotFoundException');
          return undefined;
        case 'LimitExceededException':
        case 'CodeMismatchException':
        case 'ExpiredCodeException':
        case 'NotAuthorizedException':
          // results in obfuscated error message for faulty login attempt
          throw new ValidationError(lang, `auth.cognito.${err.name}`);
        default:
          // Generic cognito error message to handle unknown cases
          throw new ValidationError(lang, 'auth.cognito.error', `: ${err.name}`);
      }
    }
  }

  /**
   *
   * @param {*} email
   * @param {*} options
   * @returns
   */
  static async cognitoGetUser(email, options = {}) {
    log.debug('CognitoAuthCommands.cognitoGetUser');
    const username = convertEmailToCognitoUsername(email);

    // Get User Information
    // https://docs.aws.amazon.com/cognito-user-identity-pools/latest/APIReference/API_AdminGetUser.html
    const getUserCommand = new AdminGetUserCommand({ UserPoolId: config.cognito.USER_POOL_ID, Username: username });
    return this.cognitoSendCommand(getUserCommand, options);
  }

  /**
   *
   * @param {*} email
   * @param {*} options
   * @returns
   */
  static async cognitoAdminConfirmSignup(email, options = {}) {
    log.debug('CognitoAuthCommands.cognitoAdminConfirmSignup');
    const username = convertEmailToCognitoUsername(email);
    const cognitoUser = await this.cognitoGetUser(email, options);

    if (cognitoUser) {
      // https://docs.aws.amazon.com/cognito-user-identity-pools/latest/APIReference/API_AdminConfirmSignUp.html
      const confirmSignupCommand = new AdminConfirmSignUpCommand({ UserPoolId: config.cognito.USER_POOL_ID, Username: username });
      return this.cognitoSendCommand(confirmSignupCommand, options);
    }

    return true;
  }

  /**
   *
   * @param {*} email
   * @param {*} options
   * @returns
   */
  static async cognitoEnableUser(email, options = {}) {
    log.debug('CognitoAuthCommands.cognitoEnableUser');
    const username = convertEmailToCognitoUsername(email);
    const cognitoUser = await this.cognitoGetUser(email, options);

    if (cognitoUser && !cognitoUser.Enabled) {
      // https://docs.aws.amazon.com/cognito-user-identity-pools/latest/APIReference/API_AdminEnableUser.html
      const enableUserCommand = new AdminEnableUserCommand({ UserPoolId: config.cognito.USER_POOL_ID, Username: username });
      return this.cognitoSendCommand(enableUserCommand, options);
    }

    return true;
  }

  /**
   *
   * @param {*} email
   * @param {*} options
   * @returns
   */
  static async cognitoDisableUser(email, options = {}) {
    log.debug('CognitoAuthCommands.cognitoDisableUser');
    const username = convertEmailToCognitoUsername(email);

    // Disable User
    // https://docs.aws.amazon.com/cognito-user-identity-pools/latest/APIReference/API_AdminDisableUser.html
    const disableUserCommand = new AdminDisableUserCommand({ UserPoolId: config.cognito.USER_POOL_ID, Username: username });
    return this.cognitoSendCommand(disableUserCommand, options);
  }

  /**
   *
   * @param {} email
   * @param {*} attributes
   * @param {*} options
   */
  static async cognitoUpdateUserAttributes(email, attributes, options = {}) {
    log.debug('CognitoAuthCommands.cognitoUpdateUserAttributes');
    const username = convertEmailToCognitoUsername(email);
    log.debug(`CognitoAuthCommands.cognitoUpdateUserAttributes : username ${username}`);
    log.debug(`CognitoAuthCommands.cognitoUpdateUserAttributes : UserPoolId ${config.cognito.USER_POOL_ID}`);
    log.debug(attributes);
    // Update User Attributes
    // https://docs.aws.amazon.com/cognito-user-identity-pools/latest/APIReference/API_AdminUpdateUserAttributes.html
    const updateUserCommand = new AdminUpdateUserAttributesCommand({ UserPoolId: config.cognito.USER_POOL_ID, Username: username, UserAttributes: attributes });
    log.debug('CognitoAuthCommands.cognitoUpdateUserAttributes : updateUserCommand');
    return this.cognitoSendCommand(updateUserCommand, options);
  }

  /**
   *
   * @param {*} accessToken
   * @param {*} options
   * @returns
   */
  static async cognitoAssociateSoftwareToken(accessToken, options = {}) {
    log.debug('CognitoAuthCommands.cognitoAssociateSoftwareToken');
    const associateSoftwareTokenCommand = new AssociateSoftwareTokenCommand({ AccessToken: accessToken });
    return this.cognitoSendCommand(associateSoftwareTokenCommand, options);
  }

  /**
   *
   * @param {*} accessToken
   * @param {*} friendlyDeviceName
   * @param {*} code
   * @param {*} options
   * @returns
   */
  static async cognitoVerifySoftwareToken(accessToken, friendlyDeviceName, code, options = {}) {
    log.debug('CognitoAuthCommands.cognitoVerifySoftwareToken');
    const verifyTokenCommand = new VerifySoftwareTokenCommand({
      AccessToken: accessToken,
      FriendlyDeviceName: friendlyDeviceName,
      UserCode: code,
    });

    return this.cognitoSendCommand(verifyTokenCommand, options);
  }

  /**
   *
   * @param {String} email
   * @param {Boolean} enabled
   * @param {*} options
   * @returns
   */
  static async cognitoSetUserMFAPreference(email, enabled, options = {}) {
    log.debug('CognitoAuthCommands.cognitoSetUserMFAPreference');
    const username = convertEmailToCognitoUsername(email);

    const setMfaPrefCommand = new AdminSetUserMFAPreferenceCommand({
      UserPoolId: config.cognito.USER_POOL_ID,
      Username: username,
      SoftwareTokenMfaSettings: { Enabled: enabled, PreferredMfa: enabled },
    });

    return this.cognitoSendCommand(setMfaPrefCommand, options);
  }

  /**
   *
   * @param {*} refreshToken
   * @param {*} deviceKey
   * @param {*} options
   * @returns
   */
  static async cognitoAdminInitiateAuth(refreshToken, deviceKey, options = {}) {
    log.debug('CognitoAuthCommands.cognitoAdminInitiateAuth');
    const userPoolId = config.cognito.USER_POOL_ID;
    const clientId = config.cognito.APP_CLIENT_ID;

    const input = {
      AuthFlow: 'REFRESH_TOKEN_AUTH',
      AuthParameters: {
        REFRESH_TOKEN: refreshToken,
        DEVICE_KEY: deviceKey,
      },
      ClientId: clientId,
      UserPoolId: userPoolId,
    };

    const adminInitAuthCommand = new AdminInitiateAuthCommand(input);

    return this.cognitoSendCommand(adminInitAuthCommand, options);
  }

  /**
   *
   * @param {*} email
   * @param {*} password
   * @param {*} options
   * @returns
   */
  static async cognitoInitiateAuth(email, password, options = {}) {
    log.debug('CognitoAuthCommands.cognitoInitiateAuth');
    const username = convertEmailToCognitoUsername(email);

    const userPoolId = config.cognito.USER_POOL_ID;
    const clientId = config.cognito.APP_CLIENT_ID;

    const input = {
      AuthFlow: 'USER_PASSWORD_AUTH',
      AuthParameters: {
        USERNAME: username,
        PASSWORD: password,
        // SRP_A: aValue.toString(16),
      },
      ClientId: clientId,
      UserPoolId: userPoolId,
    };

    // Begin Auth flow
    // https://docs.aws.amazon.com/cognito-user-identity-pools/latest/APIReference/API_InitiateAuth.html
    const initAuthCommand = new InitiateAuthCommand(input);
    return this.cognitoSendCommand(initAuthCommand, options);
  }

  /**
   *
   * @param {*} email
   * @param {*} mfaCode
   * @param {*} Session
   * @param {*} options
   * @returns
   */
  static async cognitoRespondToAuthChallenge(email, mfaCode, Session, options = {}) {
    log.debug('CognitoAuthCommands.cognitoRespondToAuthChallenge');
    const username = convertEmailToCognitoUsername(email);
    const userPoolId = config.cognito.USER_POOL_ID;
    const clientId = config.cognito.APP_CLIENT_ID;

    // Respond to an authentication challenge, as an administrator
    // Validate provided MFA code and continue signin
    // https://docs.aws.amazon.com/cognito-user-identity-pools/latest/APIReference/API_AdminRespondToAuthChallenge.html
    const respondToChallengeCommand = new AdminRespondToAuthChallengeCommand({
      UserPoolId: userPoolId,
      ClientId: clientId,
      ChallengeName: 'SOFTWARE_TOKEN_MFA',
      ChallengeResponses: {
        USERNAME: username,
        SOFTWARE_TOKEN_MFA_CODE: mfaCode.toString(),
      },
      Session,
    });

    return this.cognitoSendCommand(respondToChallengeCommand, options);
  }

  /**
   *
   * @param {*} email
   * @param {*} deviceKey
   * @param {*} rememberStatus
   * @param {*} options
   * @returns
   */
  static async cognitoUpdateDeviceStatus(email, deviceKey, rememberStatus, options = {}) {
    log.debug('CognitoAuthCommands.cognitoUpdateDeviceStatus');
    const username = convertEmailToCognitoUsername(email);

    // Register user device for cognito to remember
    // https://docs.aws.amazon.com/AWSJavaScriptSDK/v3/latest/clients/client-cognito-identity-provider/interfaces/adminupdatedevicestatuscommandinput.html
    const updateDeviceStatusCommand = new AdminUpdateDeviceStatusCommand({
      UserPoolId: config.cognito.USER_POOL_ID,
      Username: username,
      DeviceKey: deviceKey,
      DeviceRememberedStatus: rememberStatus,
    });

    return this.cognitoSendCommand(updateDeviceStatusCommand, options);
  }

  /**
   *
   * @param {*} deviceKey
   * @param {*} accessToken
   * @param {*} deviceSecretVerifierConfig
   * @param {*} options
   * @returns
   */
  static async cognitoConfirmDevice(deviceKey, accessToken, deviceSecretVerifierConfig, options = {}) {
    log.debug('CognitoAuthCommands.cognitoConfirmDevice');

    // Confirm user device
    // https://docs.aws.amazon.com/AWSJavaScriptSDK/v3/latest/clients/client-cognito-identity-provider/interfaces/confirmdevicecommandinput.html
    const confirmDeviceCommand = new ConfirmDeviceCommand({
      DeviceKey: deviceKey,
      AccessToken: accessToken || undefined,
      DeviceSecretVerifierConfig: deviceSecretVerifierConfig,
      DeviceName: 'NodeJS SDK',
    });

    return this.cognitoSendCommand(confirmDeviceCommand, options);
  }

  /**
   *
   * @param {String} email
   * @param {String} firstName
   * @param {*} options
   * @returns
   */
  static async cognitoForgotPassword(email, firstName, options = {}) {
    log.debug('CognitoAuthCommands.cognitoForgotPassword');
    const Username = convertEmailToCognitoUsername(email);
    const ClientId = config.cognito.APP_CLIENT_ID;

    // Trigger the forgot password message
    // https://docs.aws.amazon.com/AWSJavaScriptSDK/v3/latest/clients/client-cognito-identity-provider/classes/forgotpasswordcommand.html
    const forgotPasswordCommand = new ForgotPasswordCommand({
      ClientId,
      Username,
      ClientMetadata: { source: 'passwordReset', firstName },
    });

    return this.cognitoSendCommand(forgotPasswordCommand, options);
  }

  /**
   *
   * @param {*} email
   * @param {*} sender
   * @param {*} options
   * @returns
   */
  static async cognitoConfirmForgotPassword(token, email, password, firstName, options = {}) {
    log.debug('CognitoAuthCommands.cognitoConfirmForgotPassword');
    const Username = convertEmailToCognitoUsername(email);
    const ClientId = config.cognito.APP_CLIENT_ID;

    // Password change confirmation
    // https://docs.aws.amazon.com/AWSJavaScriptSDK/v3/latest/clients/client-cognito-identity-provider/classes/confirmforgotpasswordcommand.html
    const confirmForgotPasswordCommand = new ConfirmForgotPasswordCommand({
      ClientId,
      Username,
      Password: password,
      ConfirmationCode: token,
      ClientMetadata: { source: 'passwordReset', firstName },
    });

    return this.cognitoSendCommand(confirmForgotPasswordCommand, options);
  }

  /**
   *
   * @param {*} email
   * @param {*} options
   * @returns
   */
  static async cognitoListDevices(email, options = {}) {
    log.debug('CognitoAuthCommands.cognitoListDevices');
    const Username = convertEmailToCognitoUsername(email);
    const UserPoolId = config.cognito.USER_POOL_ID;

    // List Devices
    // https://docs.aws.amazon.com/AWSJavaScriptSDK/v3/latest/clients/client-cognito-identity-provider/classes/adminlistdevicescommand.html
    const listDevicesCommand = new AdminListDevicesCommand({
      Username,
      UserPoolId,
    });

    return this.cognitoSendCommand(listDevicesCommand, options);
  }
}

module.exports = CognitoAuthCommands;
