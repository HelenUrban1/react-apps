// const bcrypt = require('bcrypt');
const bcrypt = require('bcryptjs');
const Sequelize = require('sequelize');
const Amplify = require('aws-amplify');
const { checkComplexity, convertEmailToCognitoUsername, validateDomain, deleteUserAndAccountRecords, convertToOrphan } = require('./authUtils');
const models = require('../../database/models');
const UserRepository = require('../../database/repositories/userRepository');
const AccountRepository = require('../../database/repositories/accountRepository');
const AccountService = require('../accountService');
const AccountMemberRepository = require('../../database/repositories/accountMemberRepository');
const SettingRepository = require('../../database/repositories/settingRepository');
const ValidationError = require('../../errors/validationError');
const AuthProfileEditor = require('./authProfileEditor');
const SequelizeRepository = require('../../database/repositories/sequelizeRepository');
const AuditLogRepository = require('../../database/repositories/auditLogRepository');
const UserProfileRepository = require('../../database/repositories/userProfileRepository');
const { log } = require('../../logger');
const SubscriptionRepository = require('../../database/repositories/subscriptionRepository');
const AuthService = require('./authService');
const AuthEmails = require('./authEmails');
const CognitoAuthEmails = require('./cognitoAuthEmails');
const CognitoAuthCommands = require('./cognitoAuthCommands');
const jwt = require('jsonwebtoken');
const axios = require('axios');
const jwkToPem = require('jwk-to-pem');
const config = require('../../../config')();
const { convertCognitoUsernameToEmail } = require('./authUtils');

const { Auth } = Amplify;

const BCRYPT_SALT_ROUNDS = 12;
const { Op } = Sequelize;

class CognitoAuthService {
  /**
   *
   * @param {*} email
   * @param {*} firstName
   * @param {*} lastName
   * @param {*} password
   * @param {*} orgName
   * @param {*} orgPhone
   * @param {*} options
   * @returns
   */
  static async cognitoFinishRegistration(email, firstName, lastName, password, orgName = undefined, orgPhone = undefined, options = {}) {
    log.debug('CognitoAuthService.cognitoFinishRegistration');

    // finish user registration;
    let user = await UserRepository.findByEmail(email, { ...options, userOnly: false });
    if (!user) throw new ValidationError(options.language, 'auth.userNotFound');

    let hashedPassword = password;
    if (password) {
      hashedPassword = await bcrypt.hash(password, BCRYPT_SALT_ROUNDS);
    }

    let membership = await AccountMemberRepository.findById(user.activeAccountMemberId, options);

    if (!membership) {
      // create an account for the user (user selected Free Trial from Request E-Mail)
      // handles email verification and profile update and returns new accountMembership
      membership = await AuthService.createAccountForOrphan(user, hashedPassword, orgName, orgPhone, firstName, lastName, { ...options, override: true });
      user = await UserRepository.findByEmail(email, { ...options, userOnly: false });
    } else if (orgName) {
      log.debug('CognitoAuthService.cognitoFinishRegistration update orgName');
      // Update organization name for Account
      // We can't call accountService here because currentUser and accountId aren't fully hydrated until after login
      const accountRepo = new AccountRepository();
      await accountRepo.update(membership.accountId, { orgName, orgPhone }, { ...options, skipMembersUpdate: true });
    }

    if (orgName) {
      // NOTE: beginRegistration & completeRegistration creates the CRM deal when the registration form is completed
      log.debug('options.subscriptionWorker.completeRegistration', { user, account: membership.account, email, firstName, lastName, orgName, orgPhone });
      const trialCreatedReturn = await options.subscriptionWorker.completeRegistration({ user, account: membership.account, email, firstName, lastName, orgName, phoneNumber: orgPhone, crmCompany: membership.crmCompany });
      const subscription = await new SubscriptionRepository({}, options).create({ ...trialCreatedReturn.bsSubscription, accountId: membership.account.id, billingId: user.id }, { ...options, currentUser: user });
      options.subscriptionWorker.setIxcDbId({ dbId: subscription.id, subscriptionId: subscription.paymentSystemId });
    }

    await UserRepository.updatePassword(user.id, hashedPassword, options);
    await UserRepository.markEmailVerified(user.id, options);
    // update user profile
    if (firstName || lastName) {
      await UserProfileRepository.update(user.id, { firstName, lastName, phoneNumber: orgPhone || user.profile?.phoneNumber }, { ...options, currentUser: user });
    }

    return user;
  }

  /**
   *
   * @param {*} email
   * @param {*} password
   * @param {*} mfaCode
   * @param {*} rememberStatus
   * @param {*} authHelper
   * @param {*} options
   * @returns
   */
  static async cognitoFinishSignin(email, password, mfaCode, rememberStatus, authHelper, options = {}) {
    const { cognitoClient } = options;
    // Get the initate response
    const initResponse = await CognitoAuthCommands.cognitoInitiateAuth(email, password, { cognitoClient });

    // Get User Information
    const user = await CognitoAuthCommands.cognitoGetUser(email, { cognitoClient });

    // Determine if MFA is enabled for the account and validate it if a code was provided
    let mfaResponse = false;
    let mfaEnabled;
    const currentUser = await UserRepository.findByEmailWithoutAvatar(email);
    if (initResponse.ChallengeName === 'SOFTWARE_TOKEN_MFA') {
      mfaEnabled = { value: 'true' };
      // Validate MFA code if enabled for the account
      if (mfaCode) {
        mfaResponse = await CognitoAuthCommands.cognitoRespondToAuthChallenge(email, mfaCode, initResponse.Session, { cognitoClient });
      }
    } else {
      // Check settings to see if MFA is enabled
      mfaEnabled = await SettingRepository.findByName('mfaEnabled', { currentUser });
    }

    if (currentUser && currentUser.status === 'Archived') {
      return { status: 403, payload: { error: 'User is disabled' } };
    }

    const { AuthenticationResult } = mfaResponse || initResponse;

    let deviceKey;
    let refreshToken;

    if (AuthenticationResult) {
      refreshToken = AuthenticationResult.RefreshToken;
      deviceKey = await CognitoAuthService.deviceKeyHandler(email, AuthenticationResult, rememberStatus, initResponse, authHelper, { cognitoClient });

      if (deviceKey.errGenHash) {
        return { status: 500, payload: deviceKey.errGenHash };
      }
    }

    // Tell frontend to sign user up for MFA or request MFA Token if enabled
    // Pass authentication token if MFA is not enabled
    const initResult = {
      authSignIn: AuthenticationResult ? AuthenticationResult.AccessToken : undefined,
      challengeName: initResponse.ChallengeName,
      challengeParameters: initResponse.ChallengeParameters,
      session: initResponse.Session,
      user,
      mfaEnabled,
    };

    return { status: 200, payload: initResult, deviceKey, refreshToken };
  }

  /**
   *
   * @param {*} email
   * @param {*} password
   * @param {*} mfaCode
   * @param {*} rememberStatus
   * @param {*} authHelper
   * @param {*} options
   * @returns
   */
  static async cognitoMigrateUser(email, password, mfaCode, rememberStatus, authHelper, options = {}) {
    log.debug('cognitoAuthService.cognitoMigrateUser');

    // generate a random password using the user email address
    function generateRandomPasswordFromEmail(emailAddress) {
      let randomPassword = emailAddress.split('').sort().join('');
      const { index } = /[a-zA-Z]/i.exec(randomPassword);
      randomPassword = randomPassword.charAt(index).toUpperCase() + randomPassword + index;
      randomPassword = randomPassword
        .split('')
        .sort(function () {
          return 0.5 - Math.random();
        })
        .join('')
        .padEnd(10, randomPassword.charAt(index - 1));
      return randomPassword;
    }

    const { cognitoClient } = options;
    const user = await UserRepository.findByEmail(email, {});

    const passwordsMatch = await bcrypt.compare(password, user.password);
    if (passwordsMatch) {
      // check that password is valid
      let pass = password;
      const validPassword = checkComplexity(password);
      if (!validPassword) pass = generateRandomPasswordFromEmail(email);

      const username = convertEmailToCognitoUsername(email);

      await Auth.signUp({
        username,
        password: pass,
        clientMetadata: {
          sender: user.profile.firstName || email,
          source: 'migration',
        },
        attributes: {
          email,
          given_name: user.profile.firstName || '',
          family_name: user.profile.lastName || '',
          'custom:account': user.accountId,
          'custom:status': 'migration',
        },
      }).then(async () => {
        const attributes = [{ Name: 'email_verified', Value: 'true' }];
        await CognitoAuthCommands.cognitoUpdateUserAttributes(email, attributes, { cognitoClient });
        await CognitoAuthCommands.cognitoAdminConfirmSignup(email, { cognitoClient });
      });

      if (validPassword) return this.cognitoFinishSignin(email, password, mfaCode, rememberStatus, authHelper, options);

      // Password did not meet complexity requirements, reset it
      await CognitoAuthEmails.cognitoSendPasswordResetEmail(email, options);

      return { status: 200, payload: 'reset' };
    }

    if (!passwordsMatch) {
      // if passwords do not match, migrate the user to cognito and send password reset email
      // this resolution reacts to when a user has tried to login but their password wasn't correct.
      const username = convertEmailToCognitoUsername(email);

      await Auth.signUp({
        username,
        password: generateRandomPasswordFromEmail(email),
        clientMetadata: {
          sender: user.profile.firstName || email,
          source: 'migration',
        },
        attributes: {
          email,
          given_name: user.profile.firstName || '',
          family_name: user.profile.lastName || '',
          'custom:account': user.accountId,
          'custom:status': 'migration',
        },
      }).then(async () => {
        const attributes = [{ Name: 'email_verified', Value: 'true' }];
        await CognitoAuthCommands.cognitoUpdateUserAttributes(email, attributes, { cognitoClient });
        await CognitoAuthCommands.cognitoAdminConfirmSignup(email, { cognitoClient });
        await CognitoAuthEmails.cognitoSendPasswordResetEmail(email, options);

        throw new ValidationError(options.language || 'en', 'auth.userPasswordResetRequired');
      });
    }

    throw new ValidationError(options.language, 'auth.cognito.NotAuthorizedException');
  }

  /**
   *
   * @param {*} email
   * @param {*} options
   * @returns
   */
  static async cognitoCheckUserStatus(email, options) {
    log.debug('CognitoAuthService.cognitoCheckUserStatus');
    const user = await UserRepository.findByEmailWithoutAvatar(email, options);
    const userFromToken = await UserRepository.findValidTokenOrCanceledInvite(user.emailVerificationToken, options);
    if (!userFromToken) {
      CognitoAuthEmails.cognitoResendVerificationEmail(email, options);
      return 'Expired';
    }
    if (user.disabled) return 'Canceled';
    if (user.email !== email) return 'Requested';
    return 'Valid';
  }

  /**
   * Accepts a users Request for account access
   * @param {*} email
   * @param {*} sender
   * @param {*} options
   * @returns
   */
  static async cognitoAcceptAccessRequest(adminEmail, token, userId, options = {}) {
    try {
      log.debug('CognitoAuthService.cognitoAcceptAccessRequest');
      const adminUser = await UserRepository.findByEmailWithoutAvatar(adminEmail, options);

      let requestingUser;
      if (userId) {
        requestingUser = await UserRepository.findById(userId, options);
      } else {
        requestingUser = await UserRepository.findValidTokenOrCanceledInvite(token, options);
        if (!requestingUser) {
          const expiredUser = await models.user.findOne({
            where: { emailVerificationToken: token },
          });
          // Send user invitation to Admin
          const messageId = await AuthEmails.sendAccessRequestEmail(expiredUser.email, adminUser.firstName, adminEmail, options);
          if (!messageId) {
            throw new ValidationError(options.language, 'emails.error.failed');
          }

          return 'resent';
        }
      }
      if (!requestingUser) throw new ValidationError(options.language, 'iam.error.userNotFound');

      const membership = await AccountMemberRepository.findByUserAndAccount(requestingUser.id, adminUser.accountId, options);
      if (!membership) throw new ValidationError(options.language, 'auth.userMembershipError');

      const activeMembership = await AccountMemberRepository.update(membership.id, { status: 'Active' }, options);
      if (!activeMembership) throw new ValidationError(options.language, 'auth.userMembershipError');

      await CognitoAuthEmails.cognitoSendRequestApproved(requestingUser.email, adminUser.firstName || adminUser.email, membership.account.orgName, options);
      return 'true';
    } catch (err) {
      log.error(err);
      throw err;
    }
  }

  /**
   *
   * @param {*} email
   * @param {*} roles
   * @param {*} control
   * @param {*} accountId
   * @param {*} siteId
   * @param {*} sender
   * @param {*} options
   * @returns
   */
  static async cognitoInviteNewUser(email, roles, control, accountId, siteId, sender, options = {}) {
    log.debug('CognitoAuthService.cognitoInviteNewUserCognito');
    const currentOptions = { ...options, status: 'Pending' };

    if (!currentOptions.accountId) {
      currentOptions.accountId = accountId;
    }
    if (!currentOptions.siteId) {
      currentOptions.siteId = siteId;
    }
    let newUser;
    const exists = (await UserRepository.count({ email: { [Op.iLike]: `%${email}%` } }, { transaction: this.transaction })) > 0;
    if (exists) {
      // handle reactivate invited user
      log.debug('AuthService.inviteNewUserCognito reactivateInvitedUser');
      newUser = await this.cognitoReactivateInvitedUser(email, options);
    } else {
      // Create new user
      let password = email.split('').sort().join('');
      const { index } = /[a-zA-Z]/i.exec(password);
      password = password.charAt(index).toUpperCase() + password + index;

      newUser = await UserRepository.createFromAuth(
        {
          email,
          roles,
          password: null,
          accountId,
          siteId,
          control,
        },
        currentOptions,
      );
      if (!newUser) {
        throw new Error('Could not register new user. Please check provided information.');
      }
      await UserRepository.generateEmailVerificationToken(email, options);

      const emailRes = await CognitoAuthEmails.cognitoSendUserInvitationEmail(email, password, sender, accountId, options);

      if (!emailRes) {
        throw new Error('Could not register user. Please check provided information.');
      }
    }
    if (!newUser) {
      throw new Error('Could not register new user. Please check provided information.');
    }
    if (!newUser.activeAccountMemberId) {
      newUser = await UserRepository.adoptOrphanToAccount(newUser, { id: newUser.id, accountId, siteId, roles }, currentOptions);
    }

    await UserRepository.generateEmailVerificationToken(newUser.email, options);

    return true;
  }

  /**
   * Flow for Register a new user from an invitation link.
   *
   * @param {String} token
   * @param {String} email
   * @param {String} password
   * @param {Object} profile
   * @param {Object} [options]
   * @return {Boolean}
   */
  static async cognitoRegisterInvitedUser(email, password, profile, options = {}) {
    log.debug('CognitoAuthService.cognitoRegisterInvitedUser');
    const existingUser = await UserRepository.findByEmailWithoutAvatar(email, options);
    const hashedPassword = await bcrypt.hash(password, BCRYPT_SALT_ROUNDS);
    const currentOptions = { ...options };

    if (!existingUser) {
      throw new ValidationError(options.language, 'auth.userNotFound');
    }

    if (existingUser.disabled) {
      throw new ValidationError(options.language, 'auth.inviteCancelled.invalidToken');
    }

    // Check the password complexity
    if (!checkComplexity(password)) {
      throw new ValidationError(options.language, 'auth.weakPassword');
    }

    const membership = await AccountMemberRepository.findByUserAndAccount(existingUser.id, existingUser.accountId, options);
    if (!membership) {
      throw new ValidationError(options.language, 'auth.userMembershipError');
    }

    if (membership.status === 'Active') {
      throw new ValidationError(options.language, 'auth.userMembershipActive');
    }

    if (!currentOptions.accountId) {
      currentOptions.accountId = existingUser.accountId;
    }
    if (!currentOptions.currentUser) {
      currentOptions.currentUser = existingUser;
    }
    if (!currentOptions.siteId) {
      currentOptions.siteId = existingUser.siteId;
    }
    await UserRepository.updatePassword(existingUser.id, hashedPassword, currentOptions);
    let user = await UserRepository.markEmailVerified(existingUser.id, options);

    await AccountMemberRepository.update(membership.id, { status: 'Active' });

    const editor = new AuthProfileEditor(existingUser, currentOptions.language);

    await editor.execute(profile, currentOptions).catch((err) => {
      throw err;
    });

    if (membership.roles.includes('Admin') || membership.roles.includes('Billing')) {
      const account = await new AccountService({ currentUser: existingUser, language: currentOptions.language }).findById(existingUser.accountId, currentOptions);
      user = await UserRepository.findByEmailWithoutAvatar(existingUser.email, currentOptions);
      await options.subscriptionWorker.addContactToAccount({ user, account });
    }

    return user;
  }

  /**
   *
   * @param {*} email
   * @param {*} options
   * @returns
   */
  static async cognitoCancelInvitedUser(email, options = {}) {
    log.debug('CognitoAuthService.cognitoCancelInvitedUser');
    const user = await UserRepository.findByEmail(email, options);

    if (!user) {
      throw new ValidationError(options.language, 'auth.userNotFound');
    }

    // convert user to orphan
    return !!convertToOrphan(email, options, true, UserRepository);
  }

  /**
   *
   * @param {*} email
   * @param {*} options
   * @returns
   */
  static async cognitoReactivateInvitedUser(email, options = {}) {
    log.debug('CognitoAuthService.cognitoReactivateInvitedUser');
    const currentUser = SequelizeRepository.getCurrentUser(options);
    const transaction = SequelizeRepository.getTransaction(options);
    const user = await models.user.findOne(
      {
        where: { email: { [Op.iLike]: `%${email}%` } },
      },
      {
        transaction,
      },
    );
    if (!user) {
      throw new ValidationError(options.language, 'auth.userNotFound');
    }

    if (user?.disabled) {
      const newUser = await user.update(
        {
          disabled: false,
          updatedById: currentUser.id,
        },
        { transaction },
      );

      await AuditLogRepository.log(
        {
          entityName: 'user',
          entityId: user.id,
          action: AuditLogRepository.UPDATE,
          values: {
            id: user.id,
            disabled: false,
          },
        },
        options,
      );

      // reactivate the user in cognito and resend the invite
      await CognitoAuthCommands.cognitoEnableUser(email, options);
      await CognitoAuthEmails.cognitoResendUserInvite(email, currentUser.profile?.firstName || 'Admin', options);

      return newUser;
    }

    if (!user?.activeAccountMemberId) {
      // Not sure what case triggers this
      return user;
    }

    throw new ValidationError(options.language, 'iam.errors.userAlreadyExists');
  }

  /**
   * Rejects the access request of a user and hard deletes the user if they are orphaned
   * @param {String} user
   * @param {AccountMember} membership
   * @param {Object} options
   * @returns
   */
  static async cognitoRejectRequestingUser(user, membership, status, options = {}) {
    log.debug('CognitoAuthService.congitoRejectRequestingUser');
    const currentUser = SequelizeRepository.getCurrentUser(options);
    await deleteUserAndAccountRecords({ accountMember: membership }, true, options);
    const count = await AccountMemberRepository.count({ userId: user.id }, options);
    if (count === 0) await deleteUserAndAccountRecords({ user }, true, options);

    if (status === 'rejected') {
      const firstName = user.firstName || user.signupMeta ? JSON.parse(user.signupMeta, '{}').firstName : user.email;
      await AuthEmails.sendRequestRejectedEmail(user.email, firstName, currentUser.email, options);
    }

    try {
      // update user data
      const attributes = [
        { Name: 'custom:account', Value: 'orphan' },
        { Name: 'custom:status', Value: status },
      ];
      await CognitoAuthCommands.cognitoUpdateUserAttributes(user.email, attributes, options);

      // disable user in Cognito
      await CognitoAuthCommands.cognitoDisableUser(user.email, options);
    } catch (err) {
      log.debug('error disabling cognito user');
      log.error(err);
    }

    return true;
  }

  /**
   * @param {String!} adminEmail
   * @param {String!} token
   * @param {String} userId
   * @param {Object} options
   * @returns
   */
  static async cognitoRejectAccessRequest(adminEmail, token, options = {}) {
    log.debug('CognitoAuthService.cognitoRejectAccessRequest');
    const transaction = SequelizeRepository.getTransaction(options);
    const adminUser = await UserRepository.findByEmailWithoutAvatar(adminEmail, options);
    const user = await models.user.findOne(
      {
        where: { emailVerificationToken: token },
      },
      {
        transaction,
      },
    );
    if (!user) throw new ValidationError(options.language, 'iam.error.userNotFound');
    const membership = await AccountMemberRepository.findByUserAndAccount(user.id, adminUser.accountId, options);
    if (!membership) throw new ValidationError(options.language, 'auth.userMembershipError');
    await this.cognitoRejectRequestingUser(user, membership, 'rejected', options);
    return !!convertToOrphan(user.email, options, true, UserRepository);
  }

  /**
   *
   * @param {*} user
   * @param {*} options
   * @returns
   */
  static async cognitoStartTrialForOrphan(user, options = {}) {
    log.debug('CognitoAuthService.cognitoStartTrialForOrphan');
    // Check email domain against other accounts, disable other account's request policy if needed
    await validateDomain(user.email, options);

    // Send a new verification email for a new account
    const username = convertEmailToCognitoUsername(user.email);

    // Make sure user is enabled
    await CognitoAuthCommands.cognitoEnableUser(user.email, options);

    await Auth.resendSignUp(username, {
      source: 'emailAddressVerification',
      sender: user.firstName,
    });

    await UserRepository.generateEmailVerificationToken(user.email, options);

    return `/auth/email-unverified/?email=${encodeURIComponent(user.email)}&sent=true`;
  }

  /**
   * Resets the password, validating the password reset token.
   *
   * @param {String} token (Cognto)
   * @param {String} email
   * @param {String} password
   * @param {Object} [options]
   */
  static async cognitoPasswordReset(token, email, password, options = {}) {
    const user = await UserRepository.findByEmail(email, { ...options, userOnly: false });
    if (!user) {
      throw new ValidationError(options.language, 'auth.passwordReset.error');
    }

    // token passed in is the Cognito token, need to get the passwordResetToken from our DB
    const u = await UserRepository.findByPasswordResetToken(user.passwordResetToken, options);
    if (!u) {
      if (!user.emailVerified) {
        // User has an account but hasn't verified, resend the verification email
        await CognitoAuthEmails.cognitoResendVerificationEmail(email, options);
        throw new ValidationError(options.language, 'auth.passwordReset.unverified');
      }
      if (user.passwordResetToken) {
        // User has a reset token but it was expired
        await CognitoAuthEmails.cognitoSendPasswordResetEmail(email, options);
        throw new ValidationError(options.language, 'auth.passwordReset.expiredToken');
      }
    }

    // Check the password complexity
    if (!checkComplexity(password)) {
      throw new ValidationError(options.language, 'auth.weakPassword');
    }

    const hashedPassword = await bcrypt.hash(password, BCRYPT_SALT_ROUNDS);

    await CognitoAuthCommands.cognitoConfirmForgotPassword(token, email, password, user.firstName, options);

    return !!UserRepository.updatePassword(user.id, hashedPassword, options);
  }

  /**
   *
   * @param {*} email
   * @param {*} options
   * @returns
   */
  static async cognitoResetMfa(email, options = {}) {
    log.debug('CognitoAuthService.cognitoResetMfa');
    const enableMfa = false;
    return CognitoAuthCommands.cognitoSetUserMFAPreference(email, enableMfa, options);
  }

  /**
   *
   * @param {String} email
   * @param {*} authResult
   * @param {String} rememberStatus
   * @param {*} initResponse
   * @param {*} authHelper
   * @param {*} options
   * @returns
   */
  static async deviceKeyHandler(email, authResult, rememberStatus, initResponse, authHelper, options = {}) {
    log.debug('CognitoAuthService.deviceKeyHandler');
    const { cognitoClient } = options;
    const deviceKey = authResult.NewDeviceMetadata ? authResult.NewDeviceMetadata.DeviceKey : initResponse.AuthenticationResult.NewDeviceMetadata.DeviceKey;
    const deviceGroupKey = authResult.NewDeviceMetadata ? authResult.NewDeviceMetadata.DeviceKey : initResponse.AuthenticationResult.NewDeviceMetadata.DeviceGroupKey;

    // NOTE: Devin 12/20/2021 - This seems to be generating some kind of encrypted password for the cognito remembered device.
    // Since timeout refresh doesn't work and Dan is gone, it's possible something in here is meant to be passed back by the frontend
    // But it's unclear what or where it should be called
    await authHelper.generateHashDevice(deviceGroupKey, deviceKey, async (errGenHash) => {
      if (errGenHash) {
        log.error(errGenHash);
        return { errGenHash };
      }
      const deviceSecretVerifierConfig = {
        Salt: Buffer.from(authHelper.getSaltDevices(), 'hex').toString('base64'),
        PasswordVerifier: Buffer.from(authHelper.getVerifierDevices(), 'hex').toString('base64'),
      };
      // Commented out - not being used but not sure why they were originally added
      // const verifierDevices = deviceSecretVerifierConfig.PasswordVerifier;
      // const randomPassword = authHelper.getRandomPassword();

      const confirmRes = await CognitoAuthCommands.cognitoConfirmDevice(deviceKey, authResult.AccessToken, deviceSecretVerifierConfig, { cognitoClient });
      log.debug(confirmRes);

      const statusRes = await CognitoAuthCommands.cognitoUpdateDeviceStatus(email, deviceKey, rememberStatus, { cognitoClient });
      log.debug(statusRes);
    });

    return deviceKey;
  }

  /**
   * A method to validate a Cognito JWT token and return the current user using that token.
   * @param {string} token
   * @returns {Object} The current user, obtained from the JWT Token passed in.
   */
  static async validateUserToken(token) {

    const accessToken = token.split('Bearer ')[1];
    const decodedJwt = jwt.decode(accessToken, { complete: true });
    const issuer = `https://cognito-idp.${config.cognito.REGION}.amazonaws.com/${config.cognito.USER_POOL_ID}`;
    let pems = {};

    const response = await axios({
      method: 'GET',
      url: `${issuer}/.well-known/jwks.json`,
    });
    if (response.status !== 200) throw new Error(`Failed to recover JWKs in Cognito Auth Strategy with statusCode ${response.status}`);

    const { keys } = response.data;
    keys.forEach((key) => {
      const pem = {
        n: key.n,
        kty: key.kty,
        e: key.e,
      };
      pems[key.kid] = jwkToPem(pem);
    });

    if (!decodedJwt) {
      throw new Error('Invalid JWT, could not decode');
    }

    const pem = pems[decodedJwt.header.kid];
    const verifiedToken = jwt.verify(accessToken, pem, { issuer, algorithms: ['RS256'] });

    const email = convertCognitoUsernameToEmail(verifiedToken.username);
    const currentUser = await UserRepository.findByEmailWithoutAvatar(email);
    return currentUser;
  }
}

module.exports = CognitoAuthService;
