const DrawingSheetRepository = require('../database/repositories/drawingSheetRepository');
const PartRepository = require('../database/repositories/partRepository');
const NotificationService = require('./notificationService');
const ValidationError = require('../errors/validationError');
const SequelizeRepository = require('../database/repositories/sequelizeRepository');
const { i18n } = require('../i18n');
const { log } = require('../logger');
const config = require('../../config')();

/**
 * Handles DrawingSheet operations
 */
module.exports = class DrawingSheetService {
  constructor({ currentUser, language, eventEmitter }) {
    this.repository = new DrawingSheetRepository();
    this.currentUser = currentUser;
    this.language = language;
    this.eventEmitter = eventEmitter;
  }

  /**
   * Creates a DrawingSheet.
   *
   * @param {*} data
   */
  async create(data) {
    const transaction = await SequelizeRepository.createTransaction(this.currentUser.accountId);

    try {
      const record = await this.repository.create(data, {
        transaction,
        currentUser: this.currentUser,
      });

      await SequelizeRepository.commitTransaction(transaction);

      return record;
    } catch (error) {
      await SequelizeRepository.rollbackTransaction(transaction);
      throw error;
    }
  }

  /**
   * Updates a DrawingSheet.
   *
   * @param {*} id
   * @param {*} data
   */
  async update(id, data) {
    const transaction = await SequelizeRepository.createTransaction(this.currentUser.accountId);

    try {
      const record = await this.repository.update(id, data, {
        transaction,
        currentUser: this.currentUser,
      });

      await SequelizeRepository.commitTransaction(transaction);

      if (this.eventEmitter) {
        this.eventEmitter.emit('drawingSheet.update', { accountId: this.currentUser.accountId, siteId: this.currentUser.siteId, data: record });
      }

      return record;
    } catch (error) {
      await SequelizeRepository.rollbackTransaction(transaction);
      throw error;
    }
  }

  /**
   * Find every DrawingSheet that has a null FoundCaptureCount for given drawingId.
   *
   * @param {Object} drawingId
   * @param {string} accountId
   */
  async findDrawingSheetsWithNullFoundCaptureCountInAccount(drawingId, accountId) {
    try {
      const records = await this.repository.findAllByDrawingIdInAccount(drawingId, accountId);

      return records;
    } catch (error) {
      throw error;
    }
  }

  /**
   * Increments *CaptureCount fields for a DrawingSheet in a specified account.
   * Used for auto-ballooning.
   *
   * @param {*} data
   */
  async incrementInAccount(data) {
    log.debug(`drawingSheetService.incrementInAccount: accountId: ${data.accountId}, siteId: ${data.siteId}`);
    log.debug(`drawingSheetService.incrementInAccount: data: ${JSON.stringify(data)}`);
    const record = await this.repository.incrementInAccount(data, {
      currentUser: {
        ...this.currentUser,
        accountId: data.accountId,
      },
    });
    log.debug(`record id: ${record.id}`);
    if (this.eventEmitter) {
      this.eventEmitter.emit('drawingSheet.update', { accountId: data.accountId, siteId: data.siteId, data: record });
    }

    // If autoballooning is done, create a notification for the part
    const complete = config.autoBallooning.useHumanReview ? record.autoballoonReviewComplete : record.autoballoonProcessingComplete;
    log.debug(`autoballooning complete: ${complete}`);
    if (record && complete) {
      const part = await new PartRepository().findByIdInAccount(data.part, data.accountId, {
        currentUser: this.currentUser,
      });
      if (part) {
        const featureCount = part.characteristics ? part.characteristics.filter((c) => c.captureMethod === 'Automated' && (c.reviewTaskId !== null || !config.autoBallooning.useHumanReview)).length : 0;
        await new NotificationService({ currentUser: this.currentUser, language: this.language, eventEmitter: this.eventEmitter }).createInAccount({
          title: i18n(this.language, 'parts.notifications.readyForReview.title', part.name),
          content: i18n(this.language, 'parts.notifications.readyForReview.content', featureCount),
          priority: 1,
          relationId: part.id,
          relationType: 'part',
          action: i18n(this.language, 'parts.notifications.readyForReview.buttons'),
          accountId: data.accountId,
          siteId: data.siteId,
        });
      }
    }

    return record;
  }

  /**
   * Updates a DrawingSheet Error Status for a specified account when lambda fails.
   * Used when EDA auto-markup failure in lambda find imge captures.
   * If any sheet has failed part type automarkupstatus is updated as error and event transmitted to front end.
   *
   * @param {*} data
   * @output Boolean
   */
  async statusUpdateInAccount(data) {
    const retrievedRecord = await this.repository.findByPartDrawingPage(data);
    log.debug(`drawingSheetService.statusUpdateInAccount retrievedRecord id: ${retrievedRecord.id} data: ${JSON.stringify(data)}`);

    const isUpdateSuccess = await this.repository.updateDrawingSheetAutomarkupStatus(retrievedRecord.id, data);
    if (!isUpdateSuccess) {
      return false;
    }

    // Check if all sheets have processed through lambda, autoMarkupStatus will be null while processing
    const isAllProcessed = await this.repository.allSheetsProcessed(data);
    if (!isAllProcessed) {
      return true;
    }

    // All sheets have processed; now check for and autoMarkupStatus of errors
    const isAnySheetOfErrorStatus = await this.repository.sheetWithErrorStatus(data);
    if (isAnySheetOfErrorStatus) {
      // update Part: automarkupstatus to Error if any sheet has errored
      const updatedPart = await this.repository.updatePartAutomarkupStatus(data, 'Error');
      if (this.eventEmitter) {
        this.eventEmitter.emit('part.autoMarkupCompleted', { accountId: data.accountId, siteId: data.siteId, data: updatedPart });
      }
    }
    return true;
  }

  /**
   * Destroy all DrawingSheets with those ids.
   *
   * @param {*} ids
   */
  async destroyAll(ids) {
    const transaction = await SequelizeRepository.createTransaction(this.currentUser.accountId);

    try {
      for (const id of ids) {
        await this.repository.destroy(id, {
          transaction,
          currentUser: this.currentUser,
        });
      }

      await SequelizeRepository.commitTransaction(transaction);
    } catch (error) {
      await SequelizeRepository.rollbackTransaction(transaction);
      throw error;
    }
  }

  /**
   * Finds the DrawingSheet by Id.
   *
   * @param {*} id
   */
  async findById(id) {
    return this.repository.findById(
      id,

      {
        currentUser: this.currentUser,
      },
    );
  }

  /**
   * Finds DrawingSheets for Autocomplete.
   *
   * @param {*} search
   * @param {*} limit
   */
  async findAllAutocomplete(search, limit) {
    return this.repository.findAllAutocomplete(search, limit, {
      currentUser: this.currentUser,
    });
  }

  /**
   * Finds DrawingSheets based on the query.
   *
   * @param {*} args
   */
  async findAndCountAll(args) {
    return this.repository.findAndCountAll(args, {
      currentUser: this.currentUser,
    });
  }

  /**
   * Imports a list of DrawingSheets.
   *
   * @param {*} data
   * @param {*} importHash
   */
  async import(data, importHash) {
    if (!importHash) {
      throw new ValidationError(this.language, 'importer.errors.importHashRequired');
    }

    if (await this._isImportHashExistent(importHash)) {
      throw new ValidationError(this.language, 'importer.errors.importHashExistent');
    }

    const dataToCreate = {
      ...data,
      importHash,
    };

    return this.create(dataToCreate);
  }

  /**
   * Checks if the import hash already exists.
   * Every item imported has a unique hash.
   *
   * @param {*} importHash
   */
  async _isImportHashExistent(importHash) {
    const count = await this.repository.count({
      importHash,
    });

    return count > 0;
  }
};
