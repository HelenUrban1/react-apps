const { secretsManager } = require('aws-wrapper');
const { v4: uuid } = require('uuid');
const AccountRepository = require('../database/repositories/accountRepository');
const OrganizationRepository = require('../database/repositories/organizationRepository');
const SiteRepository = require('../database/repositories/siteRepository');
const ValidationError = require('../errors/validationError');
const SequelizeRepository = require('../database/repositories/sequelizeRepository');
const config = require('../../config')();
const models = require('../database/models');

/**
 * Handles Account operations
 */
module.exports = class AccountService {
  constructor(context) {
    const { currentUser, language, subscriptionWorker } = context;
    this.repository = new AccountRepository();
    this.organizationRepository = new OrganizationRepository();
    this.currentUser = currentUser;
    this.language = language;
    this.subscriptionWorker = subscriptionWorker; // pull subscriptionWorker from passed in context
    this.context = context; // Extract context, so that subscriptionWorker is available
  }

  /**
   * Creates a Account.
   *
   * @param {*} data
   */
  async create(data) {
    const transaction = await SequelizeRepository.createTransaction();

    try {
      await AccountService.createClientResources(data);

      const record = await this.repository.create(data, {
        transaction,
        currentUser: this.currentUser,
      });
      await SequelizeRepository.commitTransaction(transaction);

      await AccountService.initializeDatabase(data.id);

      return record;
    } catch (error) {
      await SequelizeRepository.rollbackTransaction(transaction);
      throw error;
    }
  }

  static async createClientResources(data) {
    AccountService.setId(data);
    const tenantId = AccountService.getTenantIdFromData(data);
    const password = await secretsManager.createRandomPassword();
    AccountService.updateDbConnectionFields(data, tenantId);
    await secretsManager.createDatabaseSecret(data.id, tenantId, password);
    await AccountService.createDatabaseUser(tenantId, password);
    await AccountService.createDatabase(tenantId);
  }

  // We use the id before it gets created by the data layer,
  // so add here if it isn't set
  static setId(data) {
    if (!data.id) {
      data.id = uuid();
    }
  }

  static getTenantIdFromData(data) {
    return `${config.accountDbPrefix}${config.env}_${data.id}`;
  }

  static updateDbConnectionFields(data, tenantId) {
    data.dbName = tenantId;
    data.dbHost = config.database.host;
  }

  static async createDatabaseUser(tenantId, password) {
    await models.sequelizeAdmin.query(`CREATE USER "${tenantId}" WITH PASSWORD :password;`, { replacements: { password }, type: models.sequelize.QueryTypes.RAW });
  }

  static async createDatabase(tenantId) {
    // On RDS, need to grant the admin user the role of the owner of the database being created
    await models.sequelizeAdmin.query(`GRANT "${tenantId}" TO "${config.databaseAdmin.username}";`, { type: models.sequelize.QueryTypes.RAW });
    await models.sequelizeAdmin.query(`CREATE DATABASE "${tenantId}" OWNER "${tenantId}";`, { type: models.sequelize.QueryTypes.RAW });
    // Grant can be revoked after the database is created
    await models.sequelizeAdmin.query(`REVOKE "${tenantId}" FROM "${config.databaseAdmin.username}";`, { type: models.sequelize.QueryTypes.RAW });
  }

  static async initializeDatabase(tenantId) {
    // Set up database with initial tables
    await models.getTenant(tenantId, 'syncDatabase');
  }

  /**
   * Updates a Account.
   *
   * @param {*} id
   * @param {*} data
   */
  async update(id, data) {
    const organizationTransaction = await SequelizeRepository.createTransaction(this.currentUser.accountId);
    const accountTransaction = await SequelizeRepository.createTransaction();
    try {
      if (data.organizationId)
        await new OrganizationRepository().update(
          data.organizationId,
          {
            status: 'Active',
            type: 'Account',
            name: data.orgName,
            phone: data.orgPhone,
            street1: data.orgStreet,
            street2: data.orgStreet2,
            city: data.orgCity,
            region: data.orgRegion,
            postalCode: data.orgPostalcode,
            country: data.orgCountry,
          },
          {
            organizationTransaction,
            currentUser: this.currentUser,
          }
        );
      if (this.currentUser.siteId)
        await new SiteRepository().update(
          this.currentUser.siteId,
          {
            status: 'Active',
            siteName: data.orgName,
            sitePhone: data.orgPhone,
            siteStreet: data.orgStreet,
            siteStreet2: data.orgStreet2,
            siteCity: data.orgCity,
            siteRegion: data.orgRegion,
            sitePostalcode: data.orgPostalcode,
            siteCountry: data.orgCountry,
          },
          {
            organizationTransaction,
            currentUser: this.currentUser,
          }
        );

      await SequelizeRepository.commitTransaction(organizationTransaction);

      if (this.context.subscriptionWorker) this.context.subscriptionWorker.updateAccountDetails({ accountId: id, ...data });

      delete data.organizationId;
      const record = await this.repository.update(id, data, {
        accountTransaction,
        currentUser: this.currentUser,
        skipMembersUpdate: true,
      });

      await SequelizeRepository.commitTransaction(accountTransaction);

      return record;
    } catch (error) {
      console.error(error);
      await SequelizeRepository.rollbackTransaction(organizationTransaction);
      await SequelizeRepository.rollbackTransaction(accountTransaction);
      throw error;
    }
  }

  /**
   * Destroy all Accounts with those ids.
   *
   * @param {*} ids
   */
  async destroyAll(ids) {
    const transaction = await SequelizeRepository.createTransaction();

    try {
      const results = [];
      for (let index = 0; index < ids.length; index++) {
        const id = ids[index];
        results.push(
          this.repository.destroy(id, {
            transaction,
            currentUser: this.currentUser,
          })
        );
      }

      await Promise.all(results);

      await SequelizeRepository.commitTransaction(transaction);
    } catch (error) {
      await SequelizeRepository.rollbackTransaction(transaction);
      throw error;
    }
  }

  /**
   * Finds the Account by Id.
   *
   * @param {*} id
   */
  async findById(id, options = {}) {
    return this.repository.findById(id, options);
  }

  /**
   * Finds the first Account by Domain.
   *
   * @param {*} domain
   */
  async findByDomain(email) {
    const { rows } = await this.repository.findAndCountAll();

    if (rows) {
      const domain = email.split('@').pop();
      rows.forEach((acct) => {
        if (acct.orgEmail) {
          const acctDomain = acct.orgEmail.split('@').pop();
          if (acctDomain && acctDomain === domain) return acct;
        }
      });
    }
  }

  /**
   * Finds Accounts for Autocomplete.
   *
   * @param {*} search
   * @param {*} limit
   */
  async findAllAutocomplete(search, limit) {
    return this.repository.findAllAutocomplete(search, limit);
  }

  /**
   * Finds Accounts based on the query.
   *
   * @param {*} args
   */
  async findAndCountAll(args) {
    return this.repository.findAndCountAll(args);
  }

  /**
   * Imports a list of Accounts.
   *
   * @param {*} data
   * @param {*} importHash
   */
  async import(data, importHash) {
    if (!importHash) {
      throw new ValidationError(this.language, 'importer.errors.importHashRequired');
    }

    if (await this.isImportHashExistent(importHash)) {
      throw new ValidationError(this.language, 'importer.errors.importHashExistent');
    }

    const dataToCreate = {
      ...data,
      importHash,
    };

    return this.create(dataToCreate);
  }

  /**
   * Checks if the import hash already exists.
   * Every item imported has a unique hash.
   *
   * @param {*} importHash
   */
  async isImportHashExistent(importHash) {
    const count = await this.repository.count({
      importHash,
    });

    return count > 0;
  }
};
