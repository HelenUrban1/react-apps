const config = require('../../config')();
const USE_MOCKS = config.useMocks;

if (USE_MOCKS) {
  jest.doMock('../database/models');
  jest.doMock('../database/repositories/characteristicRepository');
}

const models = require('../database/models');
const CharacteristicRepository = require('../database/repositories/characteristicRepository');
const SequelizeRepository = require('../database/repositories/sequelizeRepository');
const CharacteristicService = require('./characteristicService');
const { data } = require('../__fixtures__').characteristic;

describe('CharacteristicService', () => {
  const context = { currentUser: { accountId: config.testAccountId } };

  const createMock = jest.fn().mockReturnValue(data[0]);
  const createInAccountMock = jest.fn().mockReturnValue(data[0]);
  const updateMock = jest.fn().mockReturnValue(data[0]);
  const findAndCountAllMock = jest
    .fn()
    .mockReturnValueOnce({
      rows: [data[0]],
      count: 1,
    })
    .mockReturnValueOnce({ rows: [], count: 0 });
  const findByIdMock = jest.fn().mockReturnValue(data[0]);
  const destroyMock = jest.fn();

  beforeEach(async () => {
    if (USE_MOCKS) {
      models.getTenant = () =>
        Promise.resolve({
          sequelize: {
            transaction: jest.fn().mockReturnValue({
              rollback: jest.fn(),
              commit: jest.fn(),
            }),
          },
        });

      models.sequelizeAdmin = {
        query: jest.fn().mockReturnValue(Promise.resolve({})),
      };

      CharacteristicRepository.mockImplementation(() => {
        return {
          create: createMock,
          createInAccount: createInAccountMock,
          update: updateMock,
          findAndCountAll: findAndCountAllMock,
          findById: findByIdMock,
          destroy: destroyMock,
        };
      });

      SequelizeRepository.commitTransaction = jest.fn();
    } else {
      await Promise.all([
        SequelizeRepository.cleanDatabase(config.testAccountId), //
      ]);
    }
  });

  afterAll(async () => {
    if (!USE_MOCKS) {
      await Promise.all([
        SequelizeRepository.closeConnections(config.testAccountId), //
        SequelizeRepository.closeConnections(),
      ]);
    }
  });

  describe('create()', () => {
    it('Should return characteristic data', async () => {
      try {
        const characteristic = await new CharacteristicService(context).create(data[0]);
        if (USE_MOCKS) {
          expect(createMock).toBeCalledTimes(1);
        }
        expect(characteristic).toBeTruthy();
      } catch (exception) {
        // eslint-disable-next-line no-console
        console.error(exception);
        expect(exception).toBeFalsy();
      }
    });
  });

  describe('createInAccount()', () => {
    it('Should return characteristic data', async () => {
      try {
        const characteristic = await new CharacteristicService(context).createInAccount({ ...data[0], accountId: config.testAccountId });
        if (USE_MOCKS) {
          expect(createInAccountMock).toBeCalledTimes(1);
        }
        expect(characteristic).toBeTruthy();
      } catch (exception) {
        // eslint-disable-next-line no-console
        console.error(exception);
        expect(exception).toBeFalsy();
      }
    });
  });

  describe('createManyInAccount()', () => {
    it('Should return characteristic data', async () => {
      try {
        const characteristic = await new CharacteristicService(context).createManyInAccount(data);
        if (USE_MOCKS) {
          expect(createInAccountMock).toBeCalledTimes(3);
        }
        expect(characteristic).toBeTruthy();
      } catch (exception) {
        // eslint-disable-next-line no-console
        console.error(exception);
        expect(exception).toBeFalsy();
      }
    });
  });

  describe('update()', () => {
    it('Should update characteristic data', async () => {
      const characteristic = await new CharacteristicService(context).create(data[0]);
      characteristic.nominal = '1.25';
      const updatedCharacteristic = await new CharacteristicService(context).update(characteristic.id, characteristic);
      if (USE_MOCKS) {
        expect(updateMock).toBeCalledTimes(1);
      }
      expect(updatedCharacteristic.nominal).toBe('1.25');
    });
  });

  describe('destroyAll()', () => {
    it('Should delete a reviewItem by id', async () => {
      const characteristic = await new CharacteristicService(context).create(data[0]);
      const all = await new CharacteristicService(context).findAndCountAll();
      expect(all.count).toBe(1);
      expect(all.rows).toBeTruthy();

      const context2 = { currentUser: { accountId: config.testAccountId } };
      await new CharacteristicService(context2).destroyAll([characteristic.id]);
      const none = await new CharacteristicService(context2).findAndCountAll();
      if (USE_MOCKS) {
        expect(destroyMock).toBeCalledTimes(1);
      }
      expect(none.count).toBe(0);
      expect(none.rows).toStrictEqual([]);
    });
  });

  describe('findById()', () => {
    it('Should find characteristic data by id', async () => {
      const characteristic = await new CharacteristicService(context).create(data[0]);
      const foundCharacteristic = await new CharacteristicService(context).findById(characteristic.id);
      if (USE_MOCKS) {
        expect(findByIdMock).toBeCalledTimes(1);
      }
      expect(foundCharacteristic.id).toBe(characteristic.id);
    });
  });
});
