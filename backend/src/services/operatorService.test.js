const config = require('../../config')();
const USE_MOCKS = config.useMocks;

if (USE_MOCKS) {
  jest.doMock('../database/models');
  jest.doMock('../database/repositories/operatorRepository');
}

const models = require('../database/models');
const OperatorRepository = require('../database/repositories/operatorRepository');
const OperatorService = require('./operatorService');
const SequelizeRepository = require('../database/repositories/sequelizeRepository');

describe('OperatorService', () => {
  const accountId = config.testAccountId;
  const data = {
    name: 'Test Operator',
    disabled: false,
    isMaster: false,
  };

  const createMock = jest.fn().mockReturnValue(data);
  const updateMock = jest.fn().mockReturnValue({ ...data, name: 'foo' });

  beforeEach(async () => {
    if (USE_MOCKS) {
      models.getTenant = () =>
        Promise.resolve({
          sequelize: {
            transaction: jest.fn().mockReturnValue({
              rollback: jest.fn(),
              commit: jest.fn(),
            }),
          },
        });

      models.sequelizeAdmin = {
        query: jest.fn().mockReturnValue(Promise.resolve({})),
      };

      OperatorRepository.mockImplementation(() => {
        return {
          create: createMock,
          update: updateMock,
        };
      });

      SequelizeRepository.commitTransaction = jest.fn();
    }
  });

  afterAll(async () => {
    if (!USE_MOCKS) {
      await SequelizeRepository.closeConnections();
      await SequelizeRepository.closeConnections(accountId);
    }
  });

  describe('CreateOperator', () => {
    const context = { currentUser: { accountId } };

    it('Should return operator data', async () => {
      try {
        const operator = await new OperatorService(context).create(data);
        if (USE_MOCKS) {
          expect(createMock).toBeCalledTimes(1);
        }
        expect(operator).toBeTruthy();
      } catch (exception) {
        console.error(exception);
        expect(exception).toBeFalsy();
      }
    }, 10000);
  });

  describe('Update Operator', () => {
    const context = { currentUser: { accountId } };

    it('Should return updated operator data', async () => {
      try {
        const operator = await new OperatorService(context).create(data);
        expect(operator).toBeTruthy();

        const operatorAgain = await new OperatorService(context).update(operator.id, { name: 'foo' });

        if (USE_MOCKS) {
          expect(updateMock).toBeCalledTimes(1);
        }
        expect(operatorAgain.name).toEqual('foo');
      } catch (exception) {
        console.error(exception);
        expect(exception).toBeFalsy();
      }
    }, 10000);
  });
});
