const IamEditor = require('./iamEditor');

describe('PermissionChecker', () => {
  describe('#validateHas()', () => {
    it('should throw an error if a non-billing admin tries to give billing permission', async () => {
      const currentUser = {
        roles: ['Admin'],
        id: 'abc01779-c8ed-4090-bd44-9c1b2242fcba',
        email: 'dev.mail.monkey@gmail.com',
      };
      const iamEditor = new IamEditor(currentUser, 'en');

      const data1 = {
        id: '2614adcc-b4cf-46bb-8ae5-5e4c834fdab4',
        roles: ['Admin', 'Billing'],
        accessControl: false,
        status: 'Active',
      };
      try {
        await iamEditor.update(data1);
        // if update doesn't throw, fail test
        expect(true).toBe(false);
      } catch (error) {
        expect(error.message).toContain(`You can't give permissions higher than your own.`);
      }
    });

    it('should not throw an error when a billing admin tries to give billing permission', async () => {
      const currentUser = {
        roles: ['Admin', 'Billing'],
        id: '43fe236b-b4e0-4184-826e-a1ba122181da',
        accountId: '2c56f6d1-993f-444f-9c9b-adcd5b6e7c76',
        email: 'dev.mail.monkey@gmail.com',
      };
      const iamEditor = new IamEditor(currentUser, 'en');
      const loadUserMock = jest.fn(() => null);
      const updateAtDatabaseMock = jest.fn(() => null);
      iamEditor._loadUser = loadUserMock;
      iamEditor._updateAtDatabase = updateAtDatabaseMock;
      const data1 = {
        id: 'b3a5ecd8-7513-499b-bd60-2f0da71d8147',
        roles: ['Admin', 'Billing'],
        accessControl: false,
        status: 'Active',
      };
      const result = await iamEditor.update(data1);
      expect(result).toBeUndefined();
      expect(loadUserMock).toBeCalled();
      expect(updateAtDatabaseMock).toBeCalled();
    });

    it('should not throw an error when an owner tries to give billing permission', async () => {
      const currentUser = {
        roles: ['Owner'],
        id: '43fe236b-b4e0-4184-826e-a1ba122181da',
        accountId: '2c56f6d1-993f-444f-9c9b-adcd5b6e7c76',
        email: 'dev.mail.monkey@gmail.com',
      };
      const iamEditor = new IamEditor(currentUser, 'en');
      const loadUserMock = jest.fn(() => null);
      const updateAtDatabaseMock = jest.fn(() => null);
      iamEditor._loadUser = loadUserMock;
      iamEditor._updateAtDatabase = updateAtDatabaseMock;
      const data1 = {
        id: 'a9e9a037-b16a-4da5-ada0-e4243b78bf8c',
        roles: ['Admin', 'Billing'],
        accessControl: false,
        status: 'Active',
      };
      const result = await iamEditor.update(data1);
      expect(result).toBeUndefined();
      expect(loadUserMock).toBeCalled();
      expect(updateAtDatabaseMock).toBeCalled();
    });

    it('should not throw an error when an admin tries to give reviewer permission', async () => {
      const currentUser = {
        roles: ['Admin'],
        id: '43fe236b-b4e0-4184-826e-a1ba122181da',
        accountId: '2c56f6d1-993f-444f-9c9b-adcd5b6e7c76',
        email: 'dev.mail.monkey@gmail.com',
      };
      const iamEditor = new IamEditor(currentUser, 'en');
      const loadUserMock = jest.fn(() => null);
      const updateAtDatabaseMock = jest.fn(() => null);
      iamEditor._loadUser = loadUserMock;
      iamEditor._updateAtDatabase = updateAtDatabaseMock;
      const data1 = {
        id: 'a9e9a037-b16a-4da5-ada0-e4243b78bf8c',
        roles: ['Collaborator'],
        accessControl: false,
        status: 'Active',
      };
      const result = await iamEditor.update(data1);
      expect(result).toBeUndefined();
      expect(loadUserMock).toBeCalled();
      expect(updateAtDatabaseMock).toBeCalled();
    });
  });
});
