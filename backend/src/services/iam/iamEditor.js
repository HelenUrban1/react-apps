const assert = require('assert');
const pick = require('lodash/pick');
const Roles = require('../../security/roles');
const { log } = require('../../logger');
const ValidationError = require('../../errors/validationError');
const SequelizeRepository = require('../../database/repositories/sequelizeRepository');
const UserRepository = require('../../database/repositories/userRepository');
const AccountMemberRepository = require('../../database/repositories/accountMemberRepository');
const AccountService = require('../accountService');

/**
 * Handles the edition of the user(s) via the IAM page.
 */
module.exports = class IamEditor {
  constructor(currentUser, language) {
    this.language = language;
    this.data = null;
    this.transaction = null;

    // the current user
    this.currentUser = currentUser;
    this.currentProfile = currentUser.profile;

    // target user for changes if not currentUser
    this.targetUser = null;
  }

  /**
   * Updates a user via the IAM page.
   *
   * @param {*} data
   */
  async update(data, options = {}) {
    this.data = data;
    await this._validate().catch((err) => {
      log.error(err);
      throw err;
    });
    try {
      this.transaction = await SequelizeRepository.createTransaction();
      await this._loadUser(options);
      await this._updateAtDatabase(options);

      await SequelizeRepository.commitTransaction(this.transaction);
    } catch (error) {
      await SequelizeRepository.rollbackTransaction(this.transaction);
      log.error(error);
      throw error;
    }
  }

  async transferOwner(data, options = {}) {
    this.data = data;

    await this._validateTransferOwner();

    try {
      this.transaction = await SequelizeRepository.createTransaction();

      await this._loadUser(options);
      await this._transferOwnership(options);

      await SequelizeRepository.commitTransaction(this.transaction);
    } catch (error) {
      await SequelizeRepository.rollbackTransaction(this.transaction);

      throw error;
    }
  }

  async archiveOverflow(paid) {
    const allUsers = await UserRepository.findAndCountAll({ filter: { activeAccountId: this.currentUser.accountId } }, {}, this.currentUser);
    if (allUsers?.rows && allUsers.rows.length > 0) {
      const paidUsers = allUsers.rows
        .filter((user) => user.paid) // Only Paid
        .filter((user) => user.status !== 'Archived') // Only Active
        .filter((user) => !user.roles.includes('Owner')) // Don't Archive Owner
        .sort((a, b) => new Date(b.createdAt) - new Date(a.createdAt)); // Newest First
      const overflow = paidUsers.length + 1 - paid;
      const overflowList = paidUsers.slice(0, overflow);
      // await Promise.all(
      for (let index = 0; index < overflowList.length; index++) {
        const user = overflowList[index];

        await this.update({
          ...pick(user, ['id', 'firstName', 'lastName', 'phoneNumber', 'avatars', 'roles', 'accessControl', 'status']),
          status: 'Archived',
          updatedById: this.currentUser.id,
        });
      }
      // overflowList.forEach(async (user) => {
      //   await this.update({
      //     ...pick(user, ['id', 'firstName', 'lastName', 'phoneNumber', 'avatars', 'roles', 'accessControl', 'status']),
      //     status: 'Archived',
      //     updatedById: this.currentUser.id,
      //   });
      // });
      // );
    }
  }

  get _roles() {
    if (this.data.roles && !Array.isArray(this.data.roles)) {
      return [this.data.roles];
    }
    const uniqueRoles = [...new Set(this.data.roles)];
    return uniqueRoles;
  }

  /**
   * Current user is already loaded
   * Load target user
   */
  async _loadUser(options) {
    // set targetUser to current user if the current user is changing themselves, or get the target user
    // const options = {
    //   currentUser: { accountId: this.currentUser.accountId, siteId: this.currentUser.siteId },
    // };
    this.targetUser = await UserRepository.findById(this.data.id, options).catch((err) => {
      log.error(err);
      throw err;
    });

    if (!this.targetUser) {
      log.error('Target user was not returned');
      throw new ValidationError(this.language, 'iam.errors.userNotFound');
    }
  }

  /**
   * Updates the user at the database.
   */
  async _updateAtDatabase(options = {}) {
    if (!this.data.activeAccountMemberId) this.data.activeAccountMemberId = this.targetUser.activeAccountMemberId;

    // update the account member
    this.accountMember = await AccountMemberRepository.update(this.data.activeAccountMemberId, this.data, {
      ...options,
      currentUser: this.currentUser,
      targetUser: this.targetUser,
      transaction: this.transaction,
      language: this.language,
    });

    // update the user
    this.targetUser = await UserRepository.update(this.targetUser.id, this.data, {
      ...options,
      currentUser: this.currentUser,
      targetUser: this.targetUser,
      transaction: this.transaction,
      language: this.language,
    });

    if ((this.data.roles && this.data.roles.length > 0) || this.data.status) {
      await this._updateCrmRecord(this.targetUser, this.currentUser, this.data.roles || this.currentUser.roles, this.data.status || this.currentUser.status, options);
    }
  }

  /**
   * Updates the Hubspot Record for a Contact and Company
   *
   * @param {User} targetUser
   * @param {User} currentUser
   * @param {String[]} roles
   * @param {Object} [options]
   */
  async _updateCrmRecord(targetUser, currentUser, roles, status, options = {}) {
    try {
      const account = await new AccountService({ currentUser, language: options.language }).findById(currentUser.accountId);

      if (roles && (roles.includes('Owner') || roles.includes('Admin') || roles.includes('Billing')) && status === 'Active') {
        // create or update the contact in hubspot
        await options.subscriptionWorker.addContactToAccount({ user: targetUser, account });
        if (roles.includes('Owner')) {
          // update customer info in chargify
          await options.subscriptionWorker.updateAccountDetails({ accountId: account.id, orgName: account.orgName, email: targetUser.email, firstName: targetUser.firstName, lastName: targetUser.lastName });
        }
      } else {
        // remove contact from company in hubspot if not an Admin, Owner, or Billing, or status is not Active
        await options.subscriptionWorker.removeContactFromAccount({ user: targetUser, account });
      }
    } catch (error) {
      log.error(error);
    }
  }

  /**
   * Updates the currentUser and the new owner to transfer ownership, updates CRM record
   */
  async _transferOwnership(options = {}) {
    this.accountMember = await AccountMemberRepository.changeAccountOwnerFromCurrentUser(this.targetUser, this.currentUser, { ...options, transaction: this.transaction });
    const previousOwner = { ...this.currentUser, roles: this.targetUser.roles };
    const newOwner = { ...this.targetUser, roles: this.currentUser.roles };
    await this._updateCrmRecord(previousOwner, previousOwner, this.targetUser.roles, 'Active', options);
    await this._updateCrmRecord(newOwner, previousOwner, this.currentUser.roles, 'Active', options);
  }

  /**
   * Checks if the user is removing it's own owner role
   */
  async _isRemovingOwnOwnerRole() {
    if (this._roles.includes(Roles.values.owner)) {
      return false;
    }

    if (this.data.id !== this.currentUser.id) {
      return false;
    }

    const accountMember = await UserRepository.getActiveAccountMemberRecord(this.currentUser);

    const roles = accountMember ? accountMember.roles : [];

    return roles.includes(Roles.values.owner);
  }

  async _isEscalatingRole() {
    if (this.currentUser.roles.includes('Owner') || this.currentUser.roles.includes('Billing')) {
      return false;
    }

    if (this.currentUser.roles.includes('Collaborator')) {
      return true;
    }

    return this._roles.includes('Billing');
  }

  async _validate() {
    assert(this.currentUser, 'currentUser is required');
    assert(this.currentUser.id, 'currentUser.id is required');
    assert(this.currentUser.email, 'currentUser.email is required');

    assert(this.data.id, 'id is required');
    assert(this._roles, 'roles is required (can be empty)');

    if (await this._isRemovingOwnOwnerRole()) {
      log.error('Owner attempted to remove own owner role');
      throw new ValidationError(this.language, 'iam.errors.revokingOwnPermission');
    }

    if (await this._isEscalatingRole()) {
      log.error('User tried to give permissions higher than their own');
      throw new ValidationError(this.language, 'iam.errors.escalationOfPrivilege');
    }

    const accountMember = await UserRepository.getActiveAccountMemberRecord(this.currentUser);
    const roles = accountMember ? accountMember.roles : [];
    return roles.includes(Roles.values.owner);
  }

  async _validateTransferOwner() {
    assert(this.currentUser, 'currentUser is required');
    assert(this.currentUser.id, 'currentUser.id is required');
    assert(this.data.id, 'new owner id is required');
  }
};
