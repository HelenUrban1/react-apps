const PermissionChecker = require('./permissionChecker');
const ForbiddenError = require('../../errors/forbiddenError');
const Permissions = require('../../security/permissions').values;

describe('PermissionChecker', () => {
  let permissionChecker;
  let currentUser;
  let rolesMapper;

  beforeEach(() => {
    currentUser = {
      roles: [],
    };
    rolesMapper = {
      hasMatchingRole: jest.fn(),
    };
    permissionChecker = new PermissionChecker({
      currentUser,
      rolesMapper,
      language: 'en',
    });
  });

  describe('#validateHas()', () => {
    it('should throw an assertion error for a permission that does not exist', () => {
      expect(() => {
         permissionChecker.validateHas(Permissions.godMode);
      }).toThrow('permission is required');
    });

    it('should throw a ForbiddenError when the user does not have the provided permission', () => {
      expect(() => {
         permissionChecker.validateHas(Permissions.iamEdit);
      }).toThrow(ForbiddenError);
    });

    it('should not throw an error when the rolesMapper check returns true', () => {
      rolesMapper.hasMatchingRole.mockReturnValue(false);
      expect(() => {
        permissionChecker.validateHas(Permissions.iamEdit);
      }).toThrow(ForbiddenError);
    });

    it('should not throw an error when the rolesMapper check returns true', () => {
      rolesMapper.hasMatchingRole.mockReturnValue(true);
      const result = permissionChecker.validateHas(Permissions.iamEdit);
      expect(result).toBeUndefined();
    });
  });
});
