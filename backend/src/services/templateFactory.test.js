const fs = require('fs');
const path = require('path');
const TemplateFactory = require('./templateFactory');

const template = path.resolve('src/templates/All Data.xlsx');
const templateData = fs.readFileSync(template);

describe('TemplateFactory', () => {
  describe('Font Styles to CSS Font', () => {
    it('parses size', () => {
      const font = { size: 12 };
      const res = TemplateFactory.fontStyleString(font);
      expect(res).toBe('font-size:12pt; ');
    });
    it('parses weight', () => {
      const font = { bold: 1 };
      const res = TemplateFactory.fontStyleString(font);
      expect(res).toBe('font-weight:bold; ');
    });
    it('parses underline', () => {
      const font = { underline: 1 };
      const res = TemplateFactory.fontStyleString(font);
      expect(res).toBe('text-decoration:underline; ');
    });
    it('parses strike through', () => {
      const font = { strike: 1 };
      const res = TemplateFactory.fontStyleString(font);
      expect(res).toBe('text-decoration:line-through; ');
    });
    it('parses style', () => {
      const font = { italic: 1 };
      const res = TemplateFactory.fontStyleString(font);
      expect(res).toBe('font-style:italic; ');
    });
    it('parses family', () => {
      const font = { name: 'Calibri' };
      const res = TemplateFactory.fontStyleString(font);
      expect(res).toBe('font-family:Calibri; ');
    });
    it('parses rgb color', () => {
      const font = { color: { argb: '000000' } };
      const res = TemplateFactory.fontStyleString(font);
      expect(res).toBe('color:#000000;');
    });
    it('parses argb color', () => {
      const font = { color: { argb: 'FF000000' } };
      const res = TemplateFactory.fontStyleString(font);
      expect(res).toBe('color:#000000;');
    });
  });
  describe('Rich Text to HTML String', () => {
    it('parses a rich text array', () => {
      const richText = [{ text: 'BR' }, { text: 'Newline' }];
      const val = TemplateFactory.richTextToString(richText);
      expect(val).toBe('<span>BR</span><span>Newline</span>');
    });
    it('parses a rich text array with line breaks', () => {
      const richText = [{ text: 'BR' }, { text: '\n' }, { text: 'Newline' }];
      const val = TemplateFactory.richTextToString(richText);
      expect(val).toBe('<span>BR</span><br /><span>Newline</span>');
    });
    it('parses a rich text array with font styles', () => {
      const richText = [
        { font: { size: 12 }, text: 'BR' },
        { font: { size: 12 }, text: 'Newline' },
      ];
      const val = TemplateFactory.richTextToString(richText);
      expect(val).toBe('<span style="font-size:12pt; ">BR</span><span style="font-size:12pt; ">Newline</span>');
    });
  });
  describe('Assign Cell Data', () => {
    it('returns the proper cell value if passed', () => {
      const cell = { value: 'string' };
      const val = TemplateFactory.assignData({}, cell);
      expect(val.data).toBe('string');
    });
    it('returns an empty string if no value', () => {
      const cell = { value: null };
      const val = TemplateFactory.assignData({}, cell);
      expect(val.data).toBe('');
    });
    it('returns rich text and parsed text if passed rich text array', () => {
      const cell = {
        value: {
          richText: [
            { font: { size: 12 }, text: 'BR' },
            { font: {}, text: '\n' },
            { font: { size: 12 }, text: 'Newline' },
          ],
        },
      };
      const val = TemplateFactory.assignData({}, cell);
      expect(val.rich).toBe(cell.value.richText);
    });
  });
  describe('Excel Styles to CSS Object', () => {
    it('parses font styles', () => {
      const style = { font: { size: 12, italic: 1, bold: 1, underline: 1, name: 'Arial', color: { argb: '000000' } } };
      const res = TemplateFactory.excelStylesToCSS(style);
      expect(res.font).toBe('italic bold 12pt Arial');
      expect(res.color).toBe('#000000');
      expect(res.textDecoration).toBe('underline');
    });
    it('parses cell alignment', () => {
      const style = { alignment: { horizontal: 'center', vertical: 'middle' } };
      const res = TemplateFactory.excelStylesToCSS(style);
      expect(res.textAlign).toBe('center');
      expect(res.verticalAlign).toBe('middle');
    });
    it('parses background color', () => {
      const style = { fill: { fgColor: { argb: '000000' } } };
      const res = TemplateFactory.excelStylesToCSS(style);
      expect(res.backgroundColor).toBe('#000000');
    });
  });
  describe('Border Styles to CSS Object', () => {
    it('thin border', () => {
      const style = { style: 'thin' };
      const border = TemplateFactory.borderStyle(style);
      expect(border).toBe('solid 1px black');
    });
    it('medium border', () => {
      const style = { style: 'medium' };
      const border = TemplateFactory.borderStyle(style);
      expect(border).toBe('solid 2px black');
    });
    it('thick border', () => {
      const style = { style: 'thick' };
      const border = TemplateFactory.borderStyle(style);
      expect(border).toBe('solid 3px black');
    });
    it('double border', () => {
      const style = { style: 'double' };
      const border = TemplateFactory.borderStyle(style);
      expect(border).toBe('double 3px black');
    });
    it('dashed border', () => {
      const style = { style: 'dashed' };
      const border = TemplateFactory.borderStyle(style);
      expect(border).toBe('dashed 2px black');
    });
    it('dotted border', () => {
      const style = { style: 'dotted' };
      const border = TemplateFactory.borderStyle(style);
      expect(border).toBe('dotted 2px black');
    });
    it('border color', () => {
      const style = { style: 'slantDashDot', color: { rgb: 'ffffff' } };
      const border = TemplateFactory.borderStyle(style);
      expect(border).toBe('dashed 2px #ffffff');
    });
  });
  describe('Assign Cell Style', () => {
    it('converts style object to css object', () => {
      const cell = {
        style: {
          font: {
            size: 12,
            italic: 1,
            bold: 1,
            underline: 1,
            name: 'Arial',
            color: { argb: '000000' },
          },
          alignment: { horizontal: 'center', vertical: 'middle' },
          fill: { fgColor: { argb: '000000' } },
        },
        border: {
          top: { style: 'medium' },
          left: { style: 'medium' },
          right: { style: 'medium' },
          bottom: { style: 'medium' },
        },
      };
      const val = TemplateFactory.assignStyle({ style: {} }, cell);
      expect(val.style).toMatchObject({
        font: 'italic bold 12pt Arial',
        color: '#000000',
        textDecoration: 'underline',
        textAlign: 'center',
        verticalAlign: 'middle',
        backgroundColor: '#000000',
        borderLeft: 'solid 2px black',
        borderRight: 'solid 2px black',
        borderTop: 'solid 2px black',
        borderBottom: 'solid 2px black',
      });
    });
  });
  describe('Return Data for UI', () => {
    let UI;
    beforeAll(async () => {
      UI = await TemplateFactory.returnUI(templateData);
    });
    it('parses sheets', () => {
      expect(UI.length).toBeGreaterThan(0);
    });
    it('parses sheet styles', () => {
      expect(UI[0]).toHaveProperty('theme', expect.anything());
    });
    it('creates array of column widths', () => {
      expect(UI[0].columns.length).toBeGreaterThan(0);
    });
    it('creates array of merged cells', () => {
      expect(UI[0].merges.length).toBeGreaterThan(0);
      expect(UI[0].merges[0]).toHaveProperty('row');
      expect(UI[0].merges[0]).toHaveProperty('col');
      expect(UI[0].merges[0]).toHaveProperty('rowspan');
      expect(UI[0].merges[0].rowspan).toBeGreaterThan(0);
      expect(UI[0].merges[0]).toHaveProperty('colspan');
      expect(UI[0].merges[0].colspan).toBeGreaterThan(0);
    });
    it('parses rows and cells', () => {
      expect(UI[0].rows.length).toBeGreaterThan(0);
      expect(UI[0].rows[1][0]).toHaveProperty('address');
      expect(UI[0].rows[1][0]).toHaveProperty('data');
      expect(UI[0].rows[1][0]).toHaveProperty('style');
    });
  });
  it('parses formulas', async () => {
    const formulas = fs.readFileSync(path.resolve('src/services/shared/testFiles/MergeTestPopulated.xlsx'));
    const UI = await TemplateFactory.returnUI(formulas);
    expect(UI[0].rows.length).toBeGreaterThan(0);
    expect(UI[0].rows[10][2].data).toBe(6);
    expect(UI[0].rows[10][4].data).toBe(7);
    expect(UI[0].rows[7][10].data).toBe(6);
    expect(UI[0].rows[9][10].data).toBe(7);
  });
});
