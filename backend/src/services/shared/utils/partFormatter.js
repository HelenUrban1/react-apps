/* eslint-disable no-restricted-globals */
import xmlConverter from 'xml-js';
import { unFrameText } from '../exceljs/characters';

const notationSubtypeMap = (subtype) => {
  switch (subtype) {
    // Dimension
    case 'Length':
      return 'LinearDimension';
    case 'Angle':
      return 'AngleDimension';
    case 'Radius':
      return 'Radius';
    case 'Diameter':
      return 'Diameter';
    case 'Curvilinear':
      return '';
    case 'Chamfer Size':
      return 'ChamferSize';
    case 'Chamfer Angle':
      return 'ChamferAngle';
    case 'Bend Radius':
      return 'BendRadius';
    case 'Counterbore Depth':
      return 'CounterBoreDepth';
    case 'Counterbore Diameter':
      return 'CounterBoreDiameter';
    case 'Counterbore Angle':
      return 'CounterBoreAngle';
    case 'Countersink Diameter':
      return 'CounterSinkDiameter';
    case 'Countersink Angle':
      return 'CounterSinkAngle';
    case 'Depth':
      return 'Depth';
    case 'Edge Radius':
      return 'EdgeRadius';
    case 'Thickness':
      return 'Thickness';
    case 'Edge Burr Size':
      return '';
    case 'Edge Undercut Size':
      return '';
    case 'Edge Passing Size':
      return '';
    case 'Spherical Radius':
      return '';
    case 'Spherical Diameter':
      return 'SphericalDiameter';

    // GDT
    // these aren't used because I had to switch everything to Dimension but I'll
    // leave them here just in case
    // case 'Profile of a Surface':
    //   return 'ProfileOfSurface';
    // case 'Position':
    //   return 'Position';
    // case 'Flatness':
    //   return 'Flatness';
    // case 'Perpendicularity':
    //   return 'Perpendicularity';
    // case 'Angularity':
    //   return 'Angularity';
    // case 'Circularity':
    //   return 'Circularity';
    // case 'Runout':
    //   return 'CircularRunout'; // ???
    // case 'Total Runout':
    //   return 'TotalRunout';
    // case 'Cylindricity':
    //   return 'Cylindricity';
    // case 'Concentricity':
    //   return 'Concentricity';
    // case 'Symmetry':
    //   return 'Symmetry';
    // case 'Parallelism':
    //   return 'Parallelism';
    // case 'Straightness':
    //   return 'Straightness';
    // case 'Profile of a Line':
    //   return 'ProfileOfLine';
    default:
      return '';
  }
};

const specificationMap = (specification) => {
  switch (specification) {
    case 'Dimension':
      return 'Dimension';
    case 'Geometric Tolerance':
      return 'GeometricTolerance';
    case 'Note':
      return 'Note';
    default:
      return '';
  }
};
// if you are updating one update the other.
// Could be merged into one function if you add attributes for xml format after it is made

export const netInspectJSON = (part) => {
  const formattedFAI = {
    FirstArticleReportType: {
      FormOne: {
        // ResponseCode: 0,
        // FairNumber: 18675,
        PartNumber: part.number,
        PartName: part.name,
        // SerialNumber: null,
        PartRevisionLevel: part.revision,
        DrawingNumber: part.primaryDrawing.number,
        // DrawingRevLevel: 'A',
        // AdditionalChanges: null,
        // ManufacturingProcessReference: null,
        // SupplierCode: null,
        // PoNumber: 1078700,
        // BaseLinePartNumber: null,
        // ReasonForPartial: null,
        // CompletionStatus: 'Complete',
        ApprovalStatus: 'Created',
        DrawingRev: part.primaryDrawing.revision,
        // CustomerReportNumber: null,
        // CustomerPartNumber: null,
        IsPass: true,
        IsAog: false,
        IsFaaApproved: false,
        // UserComments: true,
        InternalFairNumber: true,
        CustomerWorkflowProgress: true,
        InternalWorkflowProgress: true,
        Qpid: true,
        // Subassemblies: {
        //   ResponseCode: 0,
        //   PageInfo: {
        //     Limit: 6,
        //     Offset: 0,
        //   },
        //   HasMore: false,
        //   RecordCount: 6,
        //   PageCount: 1,
        //   CurrentPageNumber: 1,
        //   Items: [
        //     {
        //       FirstArticleSubassemblyType: {
        //         ResponseCode: 0,
        //         PartNumber: 'T7471',
        //         PartName: 'ADHSV PRMR LOCTITE #',
        //         SerialNumber: null,
        //         SupplierName: 'MCMASTER CARR SUPPLY',
        //         FirstArticleNumber: 'COTS ITEM',
        //       },
        //     },
        //   ],
        // },
        Signature: true,
        ReviewedBySignature: true,
        ApprovedBySignature: true,
        CustomerWorkflowSteps: {
          Items: null,
        },
      },
      FormTwo: {
        Materials: {
          // Items: [
          //   {
          //     FirstArticleMaterialType: {
          //       ResponseCode: 0,
          //       SpecificationNumber: 'spec number mat1',
          //       Code: 'code mat1',
          //       CustomerApprovalVerification: 'No',
          //       Material: 'mat1',
          //     },
          //   },
          // ],
        },
        Processes: {
          // Items: [
          //   {
          //     FirstArticleMaterialType: {
          //       ResponseCode: 0,
          //       SpecificationNumber: 'spec number proc1',
          //       Code: 'code proc1',
          //       CustomerApprovalVerification: 'No',
          //       Material: 'proc1',
          //     },
          //   },
          // ],
        },
      },
      FormThree: {
        Characteristics: {
          Items: part.characteristics.map((item) => {
            const specificationType = specificationMap(item.notationType);
            const specification = {};
            const unframedFullSpecification = unFrameText(item.fullSpecification);
            if (specificationType === 'Dimension') {
              specification.Dimension = {
                DimensionType: notationSubtypeMap(item.notationSubtype),
                Description: unframedFullSpecification,
                Units: item.unit,
                MeasurementType: 'Variable',
              };
              let nominal;
              if (item.nominal && !isNaN(item.nominal)) {
                nominal = item.nominal;
              } else if (item.upperSpecLimit && !isNaN(item.upperSpecLimit)) {
                nominal = item.upperSpecLimit;
              } else if (item.lowerSpecLimit && !isNaN(item.lowerSpecLimit)) {
                nominal = item.lowerSpecLimit;
              } else if (item.minusTol && !isNaN(item.minusTol)) {
                nominal = item.minusTol;
              } else if (item.plusTol && !isNaN(item.plusTol)) {
                nominal = item.plusTol;
              }
              const lowTolerance = item.minusTol && !isNaN(item.minusTol) ? item.minusTol : undefined;
              const highTolerance = item.plusTol && !isNaN(item.plusTol) ? item.plusTol : undefined;
              const upperSpecLimit = item.upperSpecLimit && !isNaN(item.upperSpecLimit) ? item.upperSpecLimit : undefined;
              const lowerSpecLimit = item.lowerSpecLimit && !isNaN(item.lowerSpecLimit) ? item.lowerSpecLimit : undefined;
              if (nominal) {
                specification.Dimension.Nominal = {
                  Value: nominal,
                };
              }
              if (lowTolerance) {
                // must be negative, apparently
                specification.Dimension.LowTolerance = {
                  Value: lowTolerance > 0 ? parseFloat(lowTolerance) * -1 : lowTolerance,
                };
              }
              if (highTolerance) {
                specification.Dimension.HighTolerance = {
                  Value: highTolerance,
                };
              }
              if (upperSpecLimit) {
                specification.Dimension.UpperSpecLimit = {
                  Value: upperSpecLimit,
                };
              }
              if (lowerSpecLimit) {
                specification.Dimension.LowerSpecLimit = {
                  Value: lowerSpecLimit,
                };
              }

              // set toleranceType
              if (item.notationClass === 'Basic') {
                specification.Dimension.ToleranceType = 'Basic Dimension';
              } else if (upperSpecLimit && lowerSpecLimit && !lowTolerance && !highTolerance) {
                specification.Dimension.ToleranceType = 'Bilateral';
              } else if (upperSpecLimit && !lowTolerance && !highTolerance && !lowerSpecLimit) {
                specification.Dimension.ToleranceType = 'Unilateral Upper';
              } else if (lowerSpecLimit && !lowTolerance && !highTolerance && !upperSpecLimit) {
                specification.Dimension.ToleranceType = 'Unilateral Lower';
              } else {
                specification.Dimension.ToleranceType = 'Symmetrical';
              }
              specification.SpecificationType = specificationType;
            } else if (specificationType === 'GeometricTolerance') {
              specification.GeometricTolerance = {
                ToleranceType: notationSubtypeMap(item.notationSubtype),
                PrimaryDatum: {
                  Datum: item.gdtPrimaryDatum,
                },
                SecondaryDatum: {
                  Datum: item.gdtSecondaryDatum,
                },
                TertiaryDatum: {
                  Datum: item.gdtTertiaryDatum,
                },
                Units: item.unit,
                Description: item.notationSubtype,
                Callout: {
                  Value: item.fullSpecification,
                  FontId: 'InspectionXpert',
                },
              };
              const lowTolerance = item.minusTol && !isNaN(item.minusTol) ? item.minusTol : undefined;
              const highTolerance = item.plusTol && !isNaN(item.plusTol) ? item.plusTol : undefined;
              const upperSpecLimit = item.upperSpecLimit && !isNaN(item.upperSpecLimit) ? item.upperSpecLimit : undefined;
              const lowerSpecLimit = item.lowerSpecLimit && !isNaN(item.lowerSpecLimit) ? item.lowerSpecLimit : undefined;

              let tolerance = 0;
              if (!isNaN(item.nominal)) {
                tolerance = item.nominal;
              } else if (!isNaN(item.upperSpecLimit)) {
                tolerance = item.upperSpecLimit;
              } else if (!isNaN(item.lowerSpecLimit)) {
                tolerance = item.lowerSpecLimit;
              } else if (!isNaN(item.minusTol)) {
                tolerance = item.minusTol;
              } else if (!isNaN(item.plusTol)) {
                tolerance = item.plusTol;
              }
              if (tolerance) {
                specification.GeometricTolerance.ToleranceValue = {
                  Tolerance: tolerance,
                };
              }

              if (lowTolerance) {
                // must be negative, apparently
                specification.GeometricTolerance.LowTolerance = {
                  Value: lowTolerance > 0 ? parseFloat(lowTolerance) * -1 : lowTolerance,
                };
              }
              if (highTolerance) {
                specification.GeometricTolerance.HighTolerance = {
                  Value: highTolerance,
                };
              }
              if (upperSpecLimit) {
                specification.GeometricTolerance.UpperSpecLimit = {
                  Value: upperSpecLimit,
                };
              }
              if (lowerSpecLimit) {
                specification.GeometricTolerance.LowerSpecLimit = {
                  Value: lowerSpecLimit,
                };
              }

              // set toleranceType
              if (upperSpecLimit && !lowTolerance && !highTolerance && !lowerSpecLimit) {
                specification.GeometricTolerance.ToleranceType = 'Unilateral Upper';
              } else if (lowerSpecLimit && !lowTolerance && !highTolerance && !upperSpecLimit) {
                specification.GeometricTolerance.ToleranceType = 'Unilateral Lower';
              } else {
                specification.GeometricTolerance.ToleranceType = 'Range Inclusive';
              }

              specification.SpecificationType = specificationType;
            } else if (specificationType === 'Note') {
              specification.Note = {
                Value: unframedFullSpecification,
                Units: item.unit,
              };
              specification.SpecificationType = specificationType;
            } else {
              // if it isn't something net-inspect has values for throw it in a dimension
              specification.Dimension = {
                Description: `${item.notationSubtype}: ${unframedFullSpecification}`,
                Units: item.unit,
              };
              let nominal = 0;
              if (item.nominal && !isNaN(item.nominal)) {
                nominal = item.nominal;
              } else if (item.upperSpecLimit && !isNaN(item.upperSpecLimit)) {
                nominal = item.upperSpecLimit;
              } else if (item.lowerSpecLimit && !isNaN(item.lowerSpecLimit)) {
                nominal = item.lowerSpecLimit;
              } else if (item.minusTol && !isNaN(item.minusTol)) {
                nominal = item.minusTol;
              } else if (item.plusTol && !isNaN(item.plusTol)) {
                nominal = item.plusTol;
              }
              if (nominal) {
                specification.Dimension.Nominal = {
                  Value: nominal,
                };
              }
            }
            return {
              FirstArticleCharacteristicType: {
                FeatureNumber: {
                  Value: item.markerLabel,
                },
                OperationNumber: null,
                OperationDescription: null,
                Specification: specification,
                ReferenceLocation: item.gridCoordinates,
                BalloonNumber: item.markerSubIndex ? parseFloat(item.markerLabel) : parseInt(item.markerLabel, 10),
                Comments: item.notes,
                NumericResults: {
                  NumericMeasurement: {
                    OrderNumber: 1,
                  },
                },
              },
            };
          }),
        },
      },
    },
  };
  return formattedFAI;
};

export const netInspectXML = (part) => {
  const formattedFAI = {
    // declaration is required for XML document
    _declaration: { _attributes: { version: '1.0', encoding: 'utf-8' } },
    FirstArticleReportType: {
      // attributes is required from net-inspect. I am unsure if this url will change
      // this is the one listed in the documents for valid uploads
      _attributes: {
        xmlns: 'http://schemas.net-inspect.com/2014/05/01/public',
      },
      FormOne: {
        _attributes: {
          IsDetail: false,
          IsFull: true,
        },
        // ResponseCode: 0,
        // FairNumber: 18675,
        PartNumber: part.number,
        PartName: part.name,
        // SerialNumber: null,
        PartRevisionLevel: part.revision,
        DrawingNumber: part.primaryDrawing.number,
        // DrawingRevLevel: 'A',
        // AdditionalChanges: null,
        // ManufacturingProcessReference: null,
        // SupplierCode: null,
        // PoNumber: 1078700,
        // BaseLinePartNumber: null,
        // ReasonForPartial: null,
        // CompletionStatus: 'Complete',
        ApprovalStatus: 'Created',
        DrawingRev: part.primaryDrawing.revision,
        // CustomerReportNumber: null,
        // CustomerPartNumber: null,
        IsPass: true,
        IsAog: false,
        IsFaaApproved: false,
        // UserComments: true,
        InternalFairNumber: true,
        CustomerWorkflowProgress: true,
        InternalWorkflowProgress: true,
        Qpid: true,
        // Subassemblies: {
        //   ResponseCode: 0,
        //   PageInfo: {
        //     Limit: 6,
        //     Offset: 0,
        //   },
        //   HasMore: false,
        //   RecordCount: 6,
        //   PageCount: 1,
        //   CurrentPageNumber: 1,
        //   Items: [
        //     {
        //       FirstArticleSubassemblyType: {
        //         ResponseCode: 0,
        //         PartNumber: 'T7471',
        //         PartName: 'ADHSV PRMR LOCTITE #',
        //         SerialNumber: null,
        //         SupplierName: 'MCMASTER CARR SUPPLY',
        //         FirstArticleNumber: 'COTS ITEM',
        //       },
        //     },
        //   ],
        // },
        Signature: true,
        ReviewedBySignature: true,
        ApprovedBySignature: true,
        CustomerWorkflowSteps: {
          Items: null,
        },
      },
      FormTwo: {
        Materials: {
          // Items: [
          //   {
          //     FirstArticleMaterialType: {
          //       ResponseCode: 0,
          //       SpecificationNumber: 'spec number mat1',
          //       Code: 'code mat1',
          //       CustomerApprovalVerification: 'No',
          //       Material: 'mat1',
          //     },
          //   },
          // ],
        },
        Processes: {
          // Items: [
          //   {
          //     FirstArticleMaterialType: {
          //       ResponseCode: 0,
          //       SpecificationNumber: 'spec number proc1',
          //       Code: 'code proc1',
          //       CustomerApprovalVerification: 'No',
          //       Material: 'proc1',
          //     },
          //   },
          // ],
        },
      },
      FormThree: {
        Characteristics: {
          Items: part.characteristics.map((item) => {
            const specificationType = specificationMap(item.notationType);
            const specification = {};
            const unframedFullSpecification = unFrameText(item.fullSpecification);
            const notationSubtype = notationSubtypeMap(item.notationSubtype);

            // doing both here because it doesn't let you update tolerance type for GDT
            if (specificationType === 'Dimension' || specificationType === 'GeometricTolerance') {
              specification.Dimension = {
                Description: item.notationSubtype,
                Units: item.unit,
                MeasurementType: 'Variable',
              };
              // add callout for GDT
              if (specificationType === 'GeometricTolerance') {
                specification.Dimension.Callout = {
                  Value: item.fullSpecification,
                  FontId: 'InspectionXpert',
                };
              }
              if (notationSubtype) {
                specification.Dimension.DimensionType = notationSubtype;
              }
              let nominal;
              if (item.nominal && !isNaN(item.nominal)) {
                nominal = item.nominal;
              } else if (item.upperSpecLimit && !isNaN(item.upperSpecLimit)) {
                nominal = item.upperSpecLimit;
              } else if (item.lowerSpecLimit && !isNaN(item.lowerSpecLimit)) {
                nominal = item.lowerSpecLimit;
              } else if (item.minusTol && !isNaN(item.minusTol)) {
                nominal = item.minusTol;
              } else if (item.plusTol && !isNaN(item.plusTol)) {
                nominal = item.plusTol;
              }
              const lowTolerance = item.minusTol && !isNaN(item.minusTol) ? item.minusTol : undefined;
              const highTolerance = item.plusTol && !isNaN(item.plusTol) ? item.plusTol : undefined;
              const upperSpecLimit = item.upperSpecLimit && !isNaN(item.upperSpecLimit) ? item.upperSpecLimit : undefined;
              const lowerSpecLimit = item.lowerSpecLimit && !isNaN(item.lowerSpecLimit) ? item.lowerSpecLimit : undefined;
              if (nominal) {
                specification.Dimension.Nominal = {
                  Value: nominal,
                };
                if (item.notationClass === 'Basic') {
                  specification.Dimension.Basic = {
                    Value: nominal,
                  };
                }
              }
              if (lowTolerance) {
                // must be negative, apparently
                specification.Dimension.LowTolerance = {
                  Value: lowTolerance > 0 ? parseFloat(lowTolerance) * -1 : lowTolerance,
                };
              }
              if (highTolerance) {
                specification.Dimension.HighTolerance = {
                  Value: highTolerance,
                };
              }
              if (upperSpecLimit) {
                specification.Dimension.UpperSpecLimit = {
                  Value: upperSpecLimit,
                };
              }
              if (lowerSpecLimit) {
                specification.Dimension.LowerSpecLimit = {
                  Value: lowerSpecLimit,
                };
              }

              // set toleranceType
              if (item.notationClass === 'Basic') {
                specification.Dimension.ToleranceType = 'EqualTo';
              } else if (upperSpecLimit && lowerSpecLimit && !lowTolerance && !highTolerance) {
                specification.Dimension.ToleranceType = 'RangeInclusive';
              } else if (upperSpecLimit && !lowTolerance && !highTolerance && !lowerSpecLimit) {
                specification.Dimension.ToleranceType = 'LessThanEqual';
              } else if (lowerSpecLimit && !lowTolerance && !highTolerance && !upperSpecLimit) {
                specification.Dimension.ToleranceType = 'MoreThanEqual';
              } else {
                specification.Dimension.ToleranceType = 'Symmetrical';
              }

              specification.SpecificationType = specificationType;
            } else if (specificationType === 'Note') {
              specification.Note = {
                Value: unframedFullSpecification,
                Units: item.unit,
              };
              specification.SpecificationType = specificationType;
            } else {
              // if it isn't something net-inspect has values for throw it in a dimension
              specification.Dimension = {
                Description: `${item.notationSubtype}: ${unframedFullSpecification}`,
                Units: item.unit,
              };
              let nominal = 0;
              if (item.nominal && !isNaN(item.nominal)) {
                nominal = item.nominal;
              } else if (item.upperSpecLimit && !isNaN(item.upperSpecLimit)) {
                nominal = item.upperSpecLimit;
              } else if (item.lowerSpecLimit && !isNaN(item.lowerSpecLimit)) {
                nominal = item.lowerSpecLimit;
              } else if (item.minusTol && !isNaN(item.minusTol)) {
                nominal = item.minusTol;
              } else if (item.plusTol && !isNaN(item.plusTol)) {
                nominal = item.plusTol;
              }
              if (nominal) {
                specification.Dimension.Nominal = {
                  Value: nominal,
                };
              }
            }
            return {
              FirstArticleCharacteristicType: {
                _attributes: {
                  IsKey: false,
                },
                FeatureNumber: {
                  Value: item.markerLabel,
                },
                OperationNumber: null,
                OperationDescription: null,
                Specification: specification,
                ReferenceLocation: item.gridCoordinates,
                BalloonNumber: item.markerSubIndex ? parseFloat(item.markerLabel) : parseInt(item.markerLabel, 10),
                Comments: item.notes,
                NumericResults: {
                  NumericMeasurement: {
                    OrderNumber: 1,
                  },
                },
              },
            };
          }),
        },
      },
    },
  };

  const xmlPart = xmlConverter.js2xml(formattedFAI, { compact: true, spaces: 4 });
  return xmlPart;
};
