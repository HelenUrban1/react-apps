import { testPart, testXML } from './partFormatterTestData';
import { netInspectJSON, netInspectXML } from './partFormatter';

describe('The Part Formatter Utilities', () => {
  it('Returns the part in a JSON format that is valid for Net-Inspect', () => {
    const resultItem = netInspectJSON(testPart);
    expect(resultItem.FirstArticleReportType.FormOne.PartNumber).toBe(testPart.number);
  });

  it('Returns the part in a XML format that is valid for Net-Inspect', () => {
    const resultItem = netInspectXML(testPart);
    expect(resultItem).toBe(testXML);
  });
});
