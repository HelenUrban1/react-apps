const { getTypeIdFromNames } = require('./lists');
const { testTypes, testClassifications, testIDs } = require('./listTestData');

describe('The List utilities', () => {
  describe('gets the type ID from names', () => {
    it('gets the correct IDs', () => {
      let ret = getTypeIdFromNames('Dimension', 'Diameter', testTypes);
      expect(ret.typeId).toBe(testIDs.Dimension);
      expect(ret.subtypeId).toBe(testIDs.Diameter);
      ret = getTypeIdFromNames('Geometric Tolerance', 'Position', testTypes);
      expect(ret.typeId).toBe(testIDs['Geometric Tolerance']);
      expect(ret.subtypeId).toBe(testIDs.Position);
    });
    it('defaults to Note without a match', () => {
      const ret = getTypeIdFromNames('Dim', 'Dia', testTypes);
      expect(ret.typeId).toBe(testIDs.Note);
      expect(ret.subtypeId).toBe(testIDs.subNote);
    });
    it('defaults to Note with the wrong list', () => {
      const ret = getTypeIdFromNames('Dimension', 'Diameter', testClassifications);
      expect(ret.typeId).toBe(testIDs.Note);
      expect(ret.subtypeId).toBe(testIDs.subNote);
    });
  });
});
