import { sanitizedTextComparison, cleanLowercaseString } from './textOperations';

describe('Text Operations can be Used To', () => {
  it('Convert to Comparable Sanitized String', () => {
    expect(cleanLowercaseString('He  ll o W oRl d')).toBe('helloworld');
    expect(cleanLowercaseString('Ge oMetr ic_to lEranc e')).toBe('geometrictolerance');
  });

  it('returns passed value if invalid', () => {
    expect(cleanLowercaseString(1234)).toStrictEqual(1234);
    expect(cleanLowercaseString(['1234'])).toStrictEqual(['1234']);
    expect(cleanLowercaseString({ 12: 34 })).toStrictEqual({ 12: 34 });
  });

  it('Compares Strings in Different Formats', () => {
    expect(sanitizedTextComparison('geometric_tolerance', 'Geometric Tolerance')).toEqual(true);
    expect(sanitizedTextComparison('angle', 'angles')).toEqual(false);
    expect(sanitizedTextComparison('profile of a surface', 'Profile_ofA Surface')).toEqual(true);
  });
});
