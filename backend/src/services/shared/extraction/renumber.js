// Reduce a non-grouped numbers Index by some amount
const reduceWhole = (item, decByGroup, decByInd) => {
  const newItem = { ...item };
  newItem.markerIndex -= decByInd;
  newItem.markerLabel = (parseInt(newItem.markerGroup, 10) - decByGroup).toString();
  newItem.markerGroup = newItem.markerLabel;
  return newItem;
};

// Reduce the Index and Sub-Indices of Grouped Items by some amount, or ungroup a single item
const reduceGroup = (groupItems, decByGroup, decByInd, newList, updateList) => {
  const items = newList;
  const updates = updateList;

  if (groupItems.length === 1) {
    // revert lone grouped item to a whole index
    const newItem = { ...groupItems[0] };
    newItem.markerIndex = newItem.markerIndex - decByInd - groupItems[0].markerSubIndex;
    newItem.markerGroup = (parseInt(newItem.markerGroup, 10) - decByGroup).toString();
    newItem.markerLabel = newItem.markerGroup;
    newItem.markerSubIndex = -1;
    newItem.markerGroupShared = false;
    items.push(newItem);
    updates.push(newItem);
  } else {
    for (let i = 0; i < groupItems.length; i++) {
      // revert group items by index and sub-index
      const newItem = { ...groupItems[i] };
      newItem.markerSubIndex = i;
      const subIndexDiff = groupItems[i].markerSubIndex - i;
      newItem.markerIndex -= decByInd + subIndexDiff;
      newItem.markerGroup = (parseInt(newItem.markerGroup, 10) - decByGroup).toString();
      newItem.markerLabel = `${newItem.markerGroup}.${newItem.markerSubIndex + 1}`;
      items.push(newItem);
      updates.push(newItem);
    }
  }

  return { items, updates };
};

const renumberAfterDelete = (deletedIDs, itemList) => {
  let decByGroup = 0;
  let decByInd = 0;
  let inGroup = false;
  let groupItems = [];
  let currentGroup = 0;
  let itemsToUpdate = [];
  let newList = [];

  for (let i = 0; i < itemList.length; i++) {
    // item was before any deletions and isn't in a group and wasn't deleted, nothing to change
    if (decByInd === 0 && (itemList[i].markerSubIndex < 0 || itemList[i].markerSubIndex === null) && !deletedIDs.includes(itemList[i].id)) {
      newList.push(itemList[i]);
    }
    // item was deleted and wasn't in a group, increment the reducer value
    else if (deletedIDs.includes(itemList[i].id) && (itemList[i].markerSubIndex < 0 || itemList[i].markerSubIndex === null)) {
      decByGroup += 1;
      decByInd += 1;
    }
    // item is in a group
    else if (itemList[i].markerSubIndex >= 0 && itemList[i].markerSubIndex !== null) {
      inGroup = true;
      let deletedCount = 0;
      currentGroup = parseInt(itemList[i].markerGroup, 10);
      if (!deletedIDs.includes(itemList[i].id)) {
        // first item in the group wasn't deleted, add it to the list
        groupItems.push(itemList[i]);
      } else {
        // first item was deleted, decrement the index reducer
        deletedCount += 1;
      }
      while (inGroup) {
        if (itemList[i + 1] && parseInt(itemList[i + 1].markerGroup, 10) === currentGroup) {
          // still in item group
          if (!deletedIDs.includes(itemList[i + 1].id)) {
            // next item in group wasn't deleted
            groupItems.push(itemList[i + 1]);
          } else {
            // next item is deleted, decrement the index reducer
            deletedCount += 1;
          }
          i += 1;
        } else {
          // reached the end of the group
          inGroup = false;
          currentGroup = 0;
        }
      }
      if (groupItems.length === 0) {
        // whole group was deleted, increase the reducer value
        decByGroup += 1;
      } else {
        // reduce group of items
        const returnedLists = reduceGroup(groupItems, decByGroup, decByInd, newList, itemsToUpdate);
        newList = returnedLists.items;
        itemsToUpdate = returnedLists.updates;
      }
      // reset item group array to empty
      decByInd += deletedCount;
      groupItems = [];
    } else {
      // item isn't in a group and wasn't deleted and it needs to be decremented
      const newItem = reduceWhole(itemList[i], decByGroup, decByInd);
      newList.push(newItem);
      itemsToUpdate.push(newItem);
    }
  }

  // updateDatabase(itemsToUpdate, edit, part);
  return { newList, itemsToUpdate };
};

export { renumberAfterDelete as default };
