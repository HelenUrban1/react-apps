import { sanitizedTextComparison } from './textOperations';

const getTypeIdFromNames = (type, subtype, list) => {
  const def = {
    typeId: 'fa3284ed-ad3e-43c9-b051-87bd2e95e927', // default to Note
    subtypeId: '805fd539-8063-4158-8361-509e10a5d371', // default to Note
  };

  const res = {
    typeId: '',
    subtypeId: '',
  };

  if (!type || !list) return def;

  const typeKeys = Object.keys(list);
  for (let i = 0; i < typeKeys.length; i++) {
    if (sanitizedTextComparison(list[typeKeys[i]].value, type)) {
      res.typeId = typeKeys[i];
      break;
    }
  }
  if (!list[res.typeId]) return def;
  const subtypes = list[res.typeId].children;
  if (subtypes) {
    for (let i = 0; i < subtypes.length; i++) {
      if (sanitizedTextComparison(subtypes[i].value, subtype)) {
        res.subtypeId = subtypes[i].id;
        break;
      }
    }
  }

  return res.subtypeId ? res : def;
};

export const getListIdByValue = ({ list, value, defaultValue }) => {
  if (!value) {
    throw new Error('Type not provided');
  }
  if (!list) {
    return defaultValue;
  }
  const ID = Object.keys(list).find((key) => sanitizedTextComparison(list[key].value, value));
  return ID || defaultValue;
};

// Creates key-value object of a given list type
const convertToListObject = (entries) => {
  const typeObject = {};
  const children = {};

  entries.forEach((entry) => {
    if (!entry.parentId || entry.parentId === null) {
      // create keys from the top level items (e.g. types) that don't have a parentId
      typeObject[`${entry.id}`] = {
        id: entry.id,
        value: entry.name,
        default: entry.default,
        meta: entry.metadata ? JSON.parse(entry.metadata) : '',
        status: entry.status,
        listType: entry.listType,
        index: entry.itemIndex,
        children: {},
      };
    } else if (entry.parentId) {
      // populates the children (e.g. subtypes) of parent items
      children[entry.parentId] = children[entry.parentId] ? children[entry.parentId] : {}; // ensure not undefined or null
      children[entry.parentId][`${entry.id}`] = {
        id: entry.id,
        value: entry.name,
        default: entry.default,
        meta: entry.metadata ? JSON.parse(entry.metadata) : '',
        status: entry.status,
        listType: entry.listType,
        index: entry.itemIndex,
        parentId: entry.parentId,
      };
    }
  });
  // ensures the parent exists before assigning the children
  Object.keys(children).forEach((parentId) => {
    typeObject[parentId].children = children[parentId];
  });
  return typeObject;
};

export { convertToListObject, getTypeIdFromNames };
