import { defaultTypeIDs, defaultSubtypeIDs, DefaultTolerances } from '../../../staticData/defaults';
import interpretNotationText from '../../../external/parseNotation';
import { testPart, testCharacteristic, testAssignedStyles, noteClassified, gdtClassified, dimClassified, testTypes } from './extractUtilTestData';
import { toEnumFormat, updateExistingItem } from './extractUtil';

describe('The Extract Utilities', () => {
  it('Returns the Enum format of a String', () => {
    const en = toEnumFormat('geometric_tolerance');
    expect(en).toEqual('Geometric_Tolerance');
  });

  describe('Handles a Failed Call to the Parse', () => {
    it('Received Empty/Undefined Text', async () => {
      await expect(interpretNotationText(undefined)).rejects.toThrow('Notation Interpreter requires text as input');
    });
  });

  it('Updates an existing characteristic to split limits', () => {
    const result = {
      parsed: {
        notation: {
          type: 'splitLimits',
          upper_limit: { text: '3.39', value: '3.39', units: '' },
          lower_limit: { text: '3.36', value: '3.36', units: '' },
          modifier: '',
          text: '3.39/3.36',
        },
        quantity: { value: '1', units: '', text: '' },
        words: '',
        text: '3.39/3.36',
        input: '3.39\n3.36',
      },
      ocrRes: '3.39\n3.36',
    };

    const resultItem = updateExistingItem(result, testPart, testAssignedStyles, testCharacteristic, defaultTypeIDs.Dimension, defaultSubtypeIDs.Length, 'Tolerance', dimClassified, DefaultTolerances);

    expect(resultItem.nominal).toBe(result.parsed.text);
    expect(resultItem.upperSpecLimit).toBe(result.parsed.notation.upper_limit.value);
    expect(resultItem.lowerSpecLimit).toBe(result.parsed.notation.lower_limit.value);
    expect(resultItem.notationType).toBe(defaultTypeIDs.Dimension);
    expect(resultItem.notationSubtype).toBe(defaultSubtypeIDs.Length);
  });

  it('Updates an existing characteristic to GD&T', () => {
    const result = {
      parsed: {
        notation: {
          type: 'gdtControlFrame',
          subtype: 'Position',
          zones: {
            primary: {
              text: '.01Ⓢ', //
              prefix: '',
              value: '.01',
              modifier: 'Ⓢ',
              postModifierValue: '',
            },
          },
          datums: [
            { id: 'A' }, //
            { id: 'B' },
            { id: 'C' },
          ],
        },
        quantity: { value: '1', units: '', text: '' },
        words: '',
        input: '|⌖|.01Ⓢ|A|B|C|',
      },
      ocrRes: '|⌖|.01Ⓢ|A|B|C|',
    };

    const resultItem = updateExistingItem(result, testPart, testAssignedStyles, testCharacteristic, defaultTypeIDs['Geometric Tolerance'], defaultSubtypeIDs.Position, 'Tolerance', gdtClassified, testTypes, DefaultTolerances);

    expect(resultItem.nominal).toBe(''); // Framed version of |⌖|.01Ⓢ|A|B|C|, view with IX GDT font
    expect(resultItem.upperSpecLimit).toBe('.01');
    expect(resultItem.lowerSpecLimit).toBe('');
    expect(resultItem.gdtPrimaryToleranceZone).toBe('.01Ⓢ');
    expect(resultItem.gdtSecondaryToleranceZone).toBe('');
    expect(resultItem.gdtPrimaryDatum).toBe('A');
    expect(resultItem.gdtSecondaryDatum).toBe('B');
    expect(resultItem.gdtTertiaryDatum).toBe('C');
    expect(resultItem.notationType).toBe(defaultTypeIDs['Geometric Tolerance']);
    expect(resultItem.notationSubtype).toBe(defaultSubtypeIDs.Position);
  });

  it('Updates an existing characteristic to note', () => {
    const result = {
      parsed: {
        type: 'note',
        input: 'BREAK ALL SHARP EDGES',
      },
      ocrRes: 'BREAK ALL SHARP EDGES',
    };

    const resultItem = updateExistingItem(result, testPart, testAssignedStyles, testCharacteristic, defaultTypeIDs.Note, defaultSubtypeIDs.Note, 'Tolerance', noteClassified, testTypes, DefaultTolerances);

    expect(resultItem.nominal).toBe(result.parsed.input);
    expect(resultItem.upperSpecLimit).toBe('');
    expect(resultItem.lowerSpecLimit).toBe('');
    expect(resultItem.notationType).toBe(defaultTypeIDs.Note);
    expect(resultItem.notationSubtype).toBe(defaultSubtypeIDs.Note);
  });
});
