const assignFeatureTolerances = require('./tolerancer').default;

const tolerances = {
  linear: {
    type: 'PrecisionTolerance',
    rangeTols: [
      { rangeLow: '1', rangeHigh: '9', plus: '1', minus: '1' },
      { rangeLow: '0.1', rangeHigh: '0.9', plus: '0.1', minus: '0.1' },
      { rangeLow: '0.01', rangeHigh: '0.09', plus: '0.01', minus: '0.01' },
      { rangeLow: '0.001', rangeHigh: '0.009', plus: '0.001', minus: '0.001' },
      { rangeLow: '0.0001', rangeHigh: '0.0009', plus: '0.0001', minus: '0.0001' },
    ],
    precisionTols: [
      { level: 'X', plus: '1', minus: '1' },
      { level: 'X.X', plus: '0.1', minus: '0.1' },
      { level: 'X.XX', plus: '0.01', minus: '0.01' },
      { level: 'X.XXX', plus: '0.001', minus: '0.001' },
      { level: 'X.XXXX', plus: '0.0001', minus: '0.0001' },
    ],
    unit: 'placeholder',
  },
  angular: {
    type: 'PrecisionTolerance',
    rangeTols: [
      { rangeLow: '1', rangeHigh: '9', plus: '1', minus: '1' },
      { rangeLow: '0.1', rangeHigh: '0.9', plus: '0.1', minus: '0.1' },
      { rangeLow: '0.01', rangeHigh: '0.09', plus: '0.01', minus: '0.01' },
      { rangeLow: '0.001', rangeHigh: '0.009', plus: '0.001', minus: '0.001' },
      { rangeLow: '0.0001', rangeHigh: '0.0009', plus: '0.0001', minus: '0.0001' },
    ],
    precisionTols: [
      { level: 'X', plus: '1', minus: '1' },
      { level: 'X.X', plus: '0.1', minus: '0.1' },
      { level: 'X.XX', plus: '0.01', minus: '0.01' },
      { level: 'X.XXX', plus: '0.001', minus: '0.001' },
      { level: 'X.XXXX', plus: '0.0001', minus: '0.0001' },
    ],
    unit: 'placeholder',
  },
};
const UUID = 'e0a01769-c8ed-4090-bd24-9c1b2241f123';

const dimension = {
  drawingSheetIndex: 1,
  captureMethod: 'Automated',
  status: 'Active',
  verified: false,
  notationType: 'placeholder', // Dimension
  notationSubtype: 'placeholder', // Curvilinear
  notationClass: 'Tolerance',
  fullSpecification: '1.25 +/- 0.1',
  quantity: 3,
  nominal: '1.25',
  upperSpecLimit: '1.26',
  lowerSpecLimit: '1.24',
  plusTol: '0.1',
  minusTol: '0.1',
  unit: 'placeholder',
  toleranceSource: 'Captured',
  inspectionMethod: 'placeholder', // Caliper
  criticality: null,
  notes: '',
  drawingRotation: 0.0,
  drawingScale: 1.0,
  boxLocationY: 510.7251086456913,
  boxLocationX: 202.30217210136476,
  boxWidth: 33.53615354307212,
  boxHeight: 15.562144712486088,
  boxRotation: 0.0,
  markerGroup: '1',
  markerGroupShared: false,
  markerIndex: 0,
  markerSubIndex: null,
  markerLabel: '1',
  markerStyle: 'aad19683-337a-4039-a910-98b0bdd5ee57',
  markerLocationX: 45,
  markerLocationY: 180,
  markerFontSize: 18,
  markerSize: 36,
  gridCoordinates: null,
  balloonGridCoordinates: null,
  connectionPointGridCoordinates: null,
  createdAt: 'placeholder',
  updatedAt: 'placeholder',
  partId: 'd6c5bc49-9fb2-4fbb-8a61-77bc8d28036f', // belongsTo
  drawingId: 'c200ce70-72b8-44f7-8d3f-4b93f6d1b491', // belongsTo
  drawingSheetId: 'd6c5bc49-9fb2-4fbb-8a61-77bc8d380352',
};

const note = {
  drawingSheetIndex: 2,
  captureMethod: 'Automated',
  status: 'Active',
  verified: false,
  notationType: 'placeholder', // Note
  notationSubtype: 'placeholder', // Note
  notationClass: 'Tolerance',
  fullSpecification: 'This is a Note We Captured',
  quantity: 1,
  nominal: '',
  upperSpecLimit: null,
  lowerSpecLimit: null,
  plusTol: null,
  minusTol: null,
  unit: 'placeholder',
  toleranceSource: 'Captured',
  inspectionMethod: 'placeholder', // Caliper
  criticality: null,
  notes: '',
  drawingRotation: 0.0,
  drawingScale: 1.0,
  boxLocationY: 642,
  boxLocationX: 91,
  boxWidth: 357,
  boxHeight: 76,
  boxRotation: 0.0,
  markerGroup: '5',
  markerGroupShared: false,
  markerIndex: 5,
  markerSubIndex: null,
  markerLabel: '5',
  markerStyle: 'f1386f09-9c03-4c59-ada9-acb6a16bfbab',
  markerLocationX: 222,
  markerLocationY: -180,
  markerFontSize: 18,
  markerSize: 36,
  gridCoordinates: null,
  balloonGridCoordinates: null,
  connectionPointGridCoordinates: null,
  createdAt: 'placeholder',
  updatedAt: 'placeholder',
  partId: 'd6c5bc49-9fb2-4fbb-8a61-77bc8d28036f', // belongsTo
  drawingId: 'c200ce70-72b8-44f7-8d3f-4b93f6d1b491', // belongsTo
  drawingSheetId: 'd6c5bc49-9fb2-4fbb-8a61-77bc8d380352',
};

const GDT = {
  captureMethod: 'Automated',
  toleranceSource: 'Captured',
  status: 'Active',
  verified: false,
  drawingSheetIndex: 1,
  notationType: 'placeholder', // Geometric Tolerance
  notationSubtype: 'placeholder', // Angularity
  notationClass: 'Tolerance',
  fullSpecification: '|<|1.5|A|B|C|',
  quantity: 1,
  nominal: '1.5',
  plusTol: '',
  minusTol: '',
  upperSpecLimit: '1.5',
  lowerSpecLimit: '',
  gdtSymbol: 'Angularity',
  gdtPrimaryToleranceZone: '1.5',
  gdtSecondaryToleranceZone: 'Zone2',
  gdtPrimaryDatum: 'A',
  gdtSecondaryDatum: 'B',
  gdtTertiaryDatum: 'C',
  unit: 'placeholder',
  drawingRotation: 0,
  drawingScale: 1,
  boxLocationY: 120,
  boxLocationX: 220,
  boxWidth: 250,
  boxHeight: 100,
  boxRotation: 0,
  markerIndex: 2,
  markerLabel: '3',
  markerGroup: '3',
  markerStyle: 'aad19683-337a-4039-a910-98b0bdd5ee57',
  markerLocationX: 190,
  markerLocationY: 100.5,
  markerFontSize: 18,
  markerSize: 36,
  partId: 'c48e082a-ed09-4885-ae15-e45c4f58b628',
  drawingId: 'bf76c82f-4c45-4bf7-be51-34c401c49d85',
  drawingSheetId: '4f53a001-9e39-4c7a-aaf0-d769d3c9a425',
  createdAt: 'placeholder',
  updatedAt: 'placeholder',
};

const measurement = null;

describe('Feature Review', () => {
  describe('Controller', () => {
    it('should not add tolerances or spec limits to a note', async () => {
      const newFeature = await assignFeatureTolerances({
        feature: note,
        tolerances,
        measurement,
        isGDT: false
      });
      expect(newFeature).toMatchObject(note);
    });

    it('should calculate tolerances and spec limits for a GDT', async () => {
      const feature = {
        ...GDT,
        fullSpecification: '|⌖|.01Ⓢ|A|B|C|',
        gdtPrimaryToleranceZone: '.01Ⓢ',
        upperSpecLimit: null,
        lowerSpecLimit: null,
      };
      const newFeature = await assignFeatureTolerances({
        feature,
        tolerances,
        measurement: 'Position',
        isGDT: true
      });
      expect(newFeature).toMatchObject({ ...feature, upperSpecLimit: '.01', lowerSpecLimit: '' });
    });

    it('should calculate tolerances and spec limits for a Dimension', async () => {
      const feature = {
        ...dimension,
        fullSpecification: '1.2',
        nominal: '1.2',
        plusTol: null,
        minusTol: null,
        upperSpecLimit: null,
        lowerSpecLimit: null,
      };
      const newFeature = await assignFeatureTolerances({
        feature,
        tolerances,
        measurement: 'length',
        isGDT: false,
      });
      expect(newFeature).toMatchObject({
        ...feature,
        upperSpecLimit: '1.3',
        lowerSpecLimit: '1.1',
        plusTol: '0.1',
        minusTol: '0.1',
        toleranceSource: 'Inherited',
      });
    });

    it('should update spec limits for a GDT', async () => {
      const feature = {
        ...GDT,
        id: UUID,
        fullSpecification: '|<|3.4|A|B|C|',
        gdtPrimaryToleranceZone: '3.4',
      };
      const newFeature = await assignFeatureTolerances({
        feature,
        tolerances,
        measurement: 'angularity',
        isGDT: true,
      });
      expect(newFeature).toMatchObject({ ...feature, upperSpecLimit: '3.4', lowerSpecLimit: '' });
    });

    it('should update spec limits for a Dimension', async () => {
      const feature = {
        ...dimension,
        id: UUID,
        fullSpecification: '1.25 +/- 0.12',
        plusTol: '0.12',
        minusTol: '0.12',
      };
      const newFeature = await assignFeatureTolerances({
        feature,
        tolerances,
        measurement: 'length',
        isGDT: false,
      });
      expect(newFeature).toMatchObject({
        ...feature,
        upperSpecLimit: '1.37',
        lowerSpecLimit: '1.13',
      });
    });

    it('should throw an error if not passed results', async () => {
      const feature = null;
      try {
        await assignFeatureTolerances({
          feature,
          tolerances,
          measurement,
          isGDT: false,
        });
        expect(true).toBe(false);
      } catch (error) {
        expect(error).toBeTruthy();
      }
    });
  });
});
