// Utils
function toSnakeCase(text) {
  return text.toLowerCase().replace(/ /g, '_');
}

function toNumericString(text) {
  return text.replace(/[^0-9.,]/g, '');
}

function getPrecision(value) {
  if (!value) {
    return 0;
  }
  const parts = value.trim().split('.');
  return parts[1] ? parts[1].length : 0;
}

function getGreatestPrecision({ nominal, plus, minus }) {
  const all = [getPrecision(nominal)];
  if (plus) {
    all.push(getPrecision(plus));
  }
  if (minus) {
    all.push(getPrecision(minus));
  }
  all.sort();
  return all[all.length - 1];
}

function getTolerancesForSubtype({ subType, tolerances }) {
  return (subType && subType.toLowerCase() === 'angle') ? tolerances.angular : tolerances.linear;
}

function getRangedTolerance(tolerance, nominal) {
  let plus = '';
  let minus = '';
  for (let index = 0; index < tolerance.length; index++) {
    const range = tolerance[index];
    const nominalNum = parseFloat(nominal);
    if (nominalNum >= parseFloat(range.rangeLow) && nominalNum <= parseFloat(range.rangeHigh)) {
      plus = range.plus;
      minus = range.minus;
      break;
    }
  }
  return { plus, minus };
}

function getPrecisionTolerance(tolerance, nominal) {
  const precision = getPrecision(nominal);
  let plus = '';
  let minus = '';
  for (let index = 0; index < tolerance.length; index++) {
    const tol = tolerance[index];
    const tolPrecision = getPrecision(tol.level);
    if (precision === tolPrecision) {
      plus = tol.plus;
      minus = tol.minus;
      break;
    }
  }
  return { plus, minus };
}

// Calculations
function calculateSpecLimitFromTols({ nominal, plus, minus }) {
  let upper = '';
  let lower = '';
  const precision = getGreatestPrecision({ nominal, plus, minus });
  if (plus) {
    upper = (parseFloat(nominal) + parseFloat(plus)).toFixed(precision).toString();
  }
  if (minus) {
    lower = (parseFloat(nominal) - parseFloat(minus)).toFixed(precision).toString();
  }
  return { upper, lower };
}

function calculateGDTTolerances(totalTol) {
  const precision = getPrecision(totalTol);
  const plus = (parseFloat(totalTol) / 2).toFixed(precision);
  return { plus, minus: plus };
}

function calculateDimensionTolerances({ nominal, tolerances }) {
  if (tolerances.type === 'PrecisionTolerance') {
    return getPrecisionTolerance(tolerances.precisionTols, nominal);
  }
  return getRangedTolerance(tolerances.rangeTols, nominal);
}

function calculateGDTSpecLimits(feature, measurement, primary) {
  if (!primary) {
    throw new Error('No Primary Tolerance Zone');
  }
  const primaryVal = toNumericString(primary);
  const gdtWithLimits = { ...feature };
  if (toSnakeCase(measurement) === 'profile_of_a_surface' || toSnakeCase(measurement) === 'profile_of_a_line') {
    const { plus, minus } = calculateGDTTolerances(primaryVal);
    const { upper, lower } = calculateSpecLimitFromTols({
      nominal: '0',
      plus,
      minus,
    });
    gdtWithLimits.upperSpecLimit = upper;
    gdtWithLimits.lowerSpecLimit = lower;
  } else {
    gdtWithLimits.upperSpecLimit = primaryVal;
    gdtWithLimits.lowerSpecLimit = '';
  }
  return gdtWithLimits;
}

function calculateDimensionSpecLimits({ feature, tolerances, measurement }) {
  const dimWithLimits = { ...feature };

  if (dimWithLimits.plusTol || dimWithLimits.minusTol) {
    const { upper, lower } = calculateSpecLimitFromTols({
      nominal: dimWithLimits.nominal,
      plus: dimWithLimits.plusTol,
      minus: dimWithLimits.minusTol,
    });
    dimWithLimits.upperSpecLimit = upper;
    dimWithLimits.lowerSpecLimit = lower;
    dimWithLimits.toleranceSource = 'Captured';
  } else {
    const tols = getTolerancesForSubtype({
      subType: measurement,
      tolerances,
    });
    const { plus, minus } = calculateDimensionTolerances({
      nominal: feature.nominal,
      tolerances: tols,
    });
    dimWithLimits.plusTol = plus;
    dimWithLimits.minusTol = minus;
    const { upper, lower } = calculateSpecLimitFromTols({
      nominal: dimWithLimits.nominal,
      plus: dimWithLimits.plusTol,
      minus: dimWithLimits.minusTol,
    });
    dimWithLimits.upperSpecLimit = upper;
    dimWithLimits.lowerSpecLimit = lower;
    dimWithLimits.toleranceSource = 'Inherited';
  }

  return dimWithLimits;
}

const assignFeatureTolerances = ({
  feature,
  tolerances,
  measurement, // subtype or subtype measurement for dimensions and GDT
}) => {
  if (!feature) {
    throw new Error('Cannot assign tolerances to null feature');
  }
  if (!tolerances) {
    throw new Error('Cannot assign tolerances without part defaults');
  }
  let tolerancedFeature = { ...feature };
  const isGDT = !!tolerancedFeature.gdtPrimaryToleranceZone;
  if (measurement && (tolerancedFeature.id || (!tolerancedFeature.upperSpecLimit && !tolerancedFeature.lowerSpecLimit))) {
    // Feature has changed or is a new feature without spec limit
    if (isGDT) {
      tolerancedFeature = calculateGDTSpecLimits(tolerancedFeature, measurement, tolerancedFeature.gdtPrimaryToleranceZone);
    } else {
      tolerancedFeature = calculateDimensionSpecLimits({
        feature: tolerancedFeature,
        tolerances,
        measurement,
      });
    }
  }
  // no measurement = note
  return tolerancedFeature;
};

export { assignFeatureTolerances as default };
