import { frameText, lookup } from '../exceljs/characters';
import { log } from '../../../logger';

/* Given the nominal return the precision for the character
 * Return: (number) precision
 */
function getPrecision(nominal) {
  if (!nominal) return '';
  const parts = nominal.toString().trim().split('.');
  return parts[1] ? parts[1].length : 0;
}

function getGreatestPrecision(nominal, plus, minus) {
  const all = [getPrecision(plus), getPrecision(minus), getPrecision(nominal)].sort();
  return all[all.length - 1];
}

function getSpecLimits(nominal, plus, minus) {
  const precision = getGreatestPrecision(nominal, plus, minus);
  const upperSpecLimit = (parseFloat(nominal) + parseFloat(plus)).toFixed(precision);
  const lowerSpecLimit = (parseFloat(nominal) + parseFloat(minus)).toFixed(precision);
  return { upperSpecLimit, lowerSpecLimit };
}

function getPlusMinusFromTotalTol(totalTol) {
  // if the totalTol is odd then we need to add an extra precision so something like 0.25 becomes 0.125 instead of 0.13
  const buffer = parseInt(totalTol.charAt(totalTol.length - 1), 10) % 2 === 1 ? 1 : 0;
  const precision = getPrecision(totalTol) + buffer;
  const plus = (parseFloat(totalTol) / 2).toFixed(precision);
  return {
    plus,
    minus: -1 * plus,
  };
}

/* Given the Full Spec
 * Return (object) FullSpecification, IsBasic
 * (optional) Nominal, MinusTol, PlusTol, UpperSpecLimit, LowerSpecLimit
 */
function isBasic(obj) {
  let result = false;
  if (obj.notation.modifier) {
    result = obj.notation.modifier === 'basic' || obj.notation.modifier === 'framed';
  }
  return result;
}

/* Given the Full Spec
 * Return (object) FullSpecification, IsReference
 */
function isReference(obj) {
  let result = false;
  if (obj.notation.modifier) {
    result = obj.notation.modifier === 'reference';
  }
  return result;
}

function getTolsForNom(nominal, SubType, AngularTolerances, LinearTolerances) {
  let precision = getPrecision(nominal);
  const tols = SubType === 'angle' ? AngularTolerances : LinearTolerances;
  if (nominal === '' || !tols || !tols.precisionTols[precision]) {
    return {
      usl: '',
      lsl: '',
      plusTol: '',
      minusTol: '',
    };
  }

  const { plus, minus } = tols.precisionTols[precision];

  // Handle the case where the tol precision may be more than the nominal precision
  precision = getGreatestPrecision(nominal, plus, minus);
  const upperSpecLimit = (parseFloat(nominal) + parseFloat(plus)).toFixed(precision).toString();
  const lowerSpecLimit = (parseFloat(nominal) + parseFloat(minus)).toFixed(precision).toString();

  return {
    upperSpecLimit,
    lowerSpecLimit,
    plus,
    minus,
  };
}

// Convert proper case to snake case
// 'Geometric Tolerance' -> 'geometric_tolerance'
function toSnakeCase(text) {
  return text.toLowerCase().replace(/ /g, '_');
}

const emptyClassify = {
  Type: 'Note',
  SubType: 'Note',
  Gdt: null,
  Quantity: 1,
  FullSpecification: '',
  Nominal: '',
  PlusTol: '',
  MinusTol: '',
  UpperSpecLimit: '',
  LowerSpecLimit: '',
  IsBasic: false,
  IsReference: false,
};

// Shim function to convert new parser to old data format
const classify = (obj, AngularTolerances, LinearTolerances) => {
  if (!obj || !obj.input) {
    return emptyClassify;
  }

  const Quantity = obj.quantity && obj.quantity.value ? parseInt(obj.quantity.value, 10) : 1;

  let Type = obj.type;
  let SubType = obj.subtype === 'extreme' ? 'Length' : obj.subtype;
  let FullSpecification = obj.text; // obj.input

  let Nominal;
  // TODO: Tests expect these to be empty strings, but they should be null or undefined
  let PlusTol = '';
  let MinusTol = '';
  let UpperSpecLimit = '';
  let LowerSpecLimit = '';
  let IsBasic = false;
  let IsReference = false;

  let GdtSymbol;
  let GdtTol;
  let GdtTolZone2;
  let GdtPrimaryDatum;
  let GdtSecondDatum;
  let GdtTertiaryDatum;

  // Classify based on Type
  if (obj.notation) {
    // Classify based on Type
    if (obj.notation.type === 'note' || obj.notation.type === 'unknown') {
      Type = 'Note';
      SubType = 'Note';
      FullSpecification = obj.notation.text;
    } else if (obj.notation.type === 'gdtControlFrame') {
      Type = 'Geometric_Tolerance';
      SubType = toSnakeCase(obj.notation.subtype);
      // TODO: Seems like framing could be moved into the interpreter
      FullSpecification = frameText(obj.input);
      Nominal = FullSpecification;

      GdtSymbol = SubType;
      GdtTol = obj.notation.zones.primary.text;
      GdtTolZone2 = obj.notation.zones.secondary ? obj.notation.zones.secondary.value : '';
      GdtPrimaryDatum = obj.notation.datums[0] ? obj.notation.datums[0].id : '';
      GdtSecondDatum = obj.notation.datums[1] ? obj.notation.datums[1].id : '';
      GdtTertiaryDatum = obj.notation.datums[2] ? obj.notation.datums[2].id : '';

      if (SubType.toLowerCase().includes('profile')) {
        const { plus, minus } = getPlusMinusFromTotalTol(obj.notation.zones.primary.value);
        const { upperSpecLimit, lowerSpecLimit } = getSpecLimits(0, plus, minus);
        UpperSpecLimit = upperSpecLimit;
        LowerSpecLimit = lowerSpecLimit;
      } else {
        UpperSpecLimit = obj.notation.zones.primary.value;
        LowerSpecLimit = '';
      }
    } else {
      // console.log(obj);
      Type = 'Dimension';
      IsBasic = isBasic(obj);
      IsReference = isReference(obj);
      if (obj.notation.type === 'length' || obj.notation.type === 'angle') {
        /* 
                {quantity: '1',
                input: '.325',
                value: '.325',
                units: '',
                text: '.325',
                type: 'length',
                words: ''}

                { text: '(1.000°)',
                nominal: { text: '1.000°', value: '1.000', units: '°' },
                type: 'angle',
                modifier: 'reference',
                quantity: '1',
                words: '',
                input: '(1.000°)' }
                */
        SubType = obj.notation.type;
        Nominal = obj.notation.nominal.value;
        const { upperSpecLimit, lowerSpecLimit, plus, minus } = getTolsForNom(Nominal, SubType, AngularTolerances, LinearTolerances);
        PlusTol = plus;
        MinusTol = minus;
        UpperSpecLimit = upperSpecLimit;
        LowerSpecLimit = lowerSpecLimit;
      } else if (obj.notation.type === 'splitLimits') {
        /*
        { "type": "splitLimits",
          "upper": {
              "value": "3.39",
              "units": ""},
          "lower": {
              "value": "3.36",
              "units": ""},
          "modifier": "basic",
          "quantity": "1",
          "words": "",
          "input": "3.39/3.36BASIC"}
        */
        SubType = 'length';
        Nominal = obj.notation.text;
        PlusTol = '';
        MinusTol = '';
        UpperSpecLimit = obj.notation.upper_limit.value;
        LowerSpecLimit = obj.notation.lower_limit.value;
      } else if (obj.notation.type === 'extreme') {
        /*
        { type: 'extreme',
        limit: { text: '1.000', value: '1.000', units: '' },
        edge: 'MAX',
        text: '1.000MAX',
        quantity: '1',
        words: '',
        input: '1.000 MAX' }
        */
        SubType = obj.notation.type;
        Nominal = obj.notation.limit.value;
        UpperSpecLimit = obj.notation.edge === 'MAX' ? Nominal : '';
        LowerSpecLimit = obj.notation.edge === 'MIN' ? Nominal : '';
      } else if (obj.notation.type === 'depth') {
        /*
        { 
          type: 'depth',
          symbol: '',
          distance: { 
            type: 'nominalWithTolerances',
            nominal: { value: '1', units: '', text: '1'}, 
            plus: { value: '.1', units: '', text: '.1'},
            minus: { value: '.1', units: '', text: '.1'},
            text: '1±.1' 
          },
          word: '',
          text: '1±.1' 
        }
        */
        SubType = obj.notation.type;
        if (obj.notation.amount) {
          if (obj.notation.amount.nominal) {
            Nominal = obj.notation.amount.nominal.text;
          } else {
            Nominal = obj.notation.amount.text;
          }
        } else {
          Nominal = obj.notation.text;
        }
      } else if (obj.notation.type === 'hole') {
        /* 
        { type: 'hole',
        symbol: '',
        size: { text: '1.000', value: '1.000', units: '' },
        depth: '',
        text: '1.000',
        quantity: '1',
        words: '',
        input: '1.000' }
        */
        if (obj.notation.symbol === 'R') {
          SubType = 'radius';
        } else {
          const symbol = lookup(obj.notation.symbol);
          SubType = toSnakeCase(symbol.name);
        }
        if (obj.notation.width.type === 'splitLimits') {
          Nominal = obj.notation.width.text;
          UpperSpecLimit = obj.notation.width.upper_limit.value;
          LowerSpecLimit = obj.notation.width.lower_limit.value;
        } else if (obj.notation.width.type === 'nominalWithTolerances') {
          Nominal = obj.notation.width.nominal.value;
          PlusTol = obj.notation.width.upper_tol.sign === '+' ? obj.notation.width.upper_tol.value : `${obj.notation.width.upper_tol.sign}${obj.notation.width.upper_tol.value}`;
          MinusTol = obj.notation.width.lower_tol.sign === '+' ? obj.notation.width.lower_tol.value : `${obj.notation.width.lower_tol.sign}${obj.notation.width.lower_tol.value}`;
          const { upperSpecLimit, lowerSpecLimit } = getSpecLimits(Nominal, PlusTol, MinusTol);
          UpperSpecLimit = upperSpecLimit;
          LowerSpecLimit = lowerSpecLimit;
        } else if (obj.notation.width.type === 'extreme') {
          Nominal = obj.notation.width.limit.value;
          UpperSpecLimit = obj.notation.width.edge === 'MAX' ? Nominal : '';
          LowerSpecLimit = obj.notation.width.edge === 'MIN' ? Nominal : '';
        } else if (obj.notation.width.value) {
          // Nominal with tolerances
          Nominal = obj.notation.width.text;
          const { upperSpecLimit, lowerSpecLimit, plus, minus } = getTolsForNom(Nominal, SubType, AngularTolerances, LinearTolerances);
          PlusTol = plus;
          MinusTol = minus;
          UpperSpecLimit = upperSpecLimit;
          LowerSpecLimit = lowerSpecLimit;
        }
      } else if (obj.notation.type === 'chamfer') {
        // TODO: Chamfers may have two inspectable values which need to be broken up into separate features
        /*
        text: "0.25 X 45°"
        type: "chamfer"
        width: {text: "0.25", value: "0.25", units: ""}
        angle: {text: "45°", value: "45", units: "°"}
        byWidth: ""
        */
        SubType = 'chamfer';
        Nominal = obj.notation.text;
      } else if (obj.notation.type === 'countersink') {
        SubType = 'countersink_diameter';
        Nominal = obj.notation.width.text; // TODO: Should be just nominal value
      } else if (obj.notation.type === 'counterbore') {
        SubType = 'counterbore_diameter';
        Nominal = obj.notation.width.text; // TODO: Should be just nominal value
      } else if (obj.notation.type === 'holeWithCounterbore') {
        // TODO: These need to be broken up into separate features
        if (obj.notation.counterbore.depth.amount) {
          SubType = 'counterbore_depth';
        } else {
          SubType = 'counterbore_diameter';
        }
        Nominal = obj.notation.text;
      } else if (obj.notation.type === 'holeWithCountersink') {
        // TODO: These need to be broken up into separate features
        if (obj.notation.countersink.angle.amount) {
          SubType = 'countersink_angle';
        } else {
          SubType = 'countersink_diameter';
        }
        Nominal = obj.notation.text;
      } else if (obj.notation.type === 'metricThreading' || obj.notation.type === 'metricThreadingInternal' || obj.notation.type === 'metricThreadingExternal') {
        SubType = 'thread';
        Nominal = obj.notation.text;
      } else if (obj.notation.type === 'standardThreadedFastener') {
        SubType = 'thread';
        Nominal = obj.notation.text;
      } else if (obj.notation.type === 'standardThreadingInternal') {
        SubType = 'thread';
        Nominal = obj.notation.text;
      } else if (obj.notation.type === 'valueWithOptionalUnits') {
        log.warn('Unimplemented notation type "valueWithOptionalUnits"');
      } else if (obj.notation.type === 'nominalWithTolerances') {
        /*
        { type: 'nominalWithTolerances',
          nominal: { value: '.325', units: '' },
          plus: { value: '.020', units: '' },
          minus: { value: '.020', units: '' },
          quantity: '1',
          words: '',
          input: '.325±.020' }
          */
        SubType = obj.notation.nominal.units === '°' ? 'angle' : 'length';
        Nominal = obj.notation.nominal.value;
        PlusTol = obj.notation.upper_tol.sign === '+' ? obj.notation.upper_tol.value : `${obj.notation.upper_tol.sign}${obj.notation.upper_tol.value}`;
        MinusTol = obj.notation.lower_tol.sign === '+' ? obj.notation.lower_tol.value : `${obj.notation.lower_tol.sign}${obj.notation.lower_tol.value}`;
        const { upperSpecLimit, lowerSpecLimit } = getSpecLimits(Nominal, PlusTol, MinusTol);
        UpperSpecLimit = upperSpecLimit;
        LowerSpecLimit = lowerSpecLimit;
      }
    }
  }

  return {
    Type,
    SubType,
    Gdt: {
      GdtSymbol,
      GdtTol,
      GdtTolZone2,
      GdtPrimaryDatum,
      GdtSecondDatum,
      GdtTertiaryDatum,
    },
    Quantity,
    FullSpecification,
    Nominal,
    PlusTol,
    MinusTol,
    UpperSpecLimit,
    LowerSpecLimit,
    IsBasic,
    IsReference,
  };
};

export { classify as default };
