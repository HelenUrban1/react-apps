const { defaultTypeIDs, defaultUnitIDs } = require('../../../staticData/defaults');
const classify = require('./classifier').default;
const assignFeatureTolerances = require('./tolerancer').default;

export const getTypeMarkerStyle = (styleOptions, type, subtype) => {
  let styles = styleOptions;
  if (typeof styleOptions === 'string') {
    styles = JSON.parse(styleOptions);
  }
  let markerStyle = styles.default.style;
  Object.keys(styles).forEach((key) => {
    if (styles[key].assign[1] === type && styles[key].assign[2] === subtype) markerStyle = styles[key].style;
  });
  return { style: markerStyle, matched: markerStyle !== styles.default.style };
};

const getTrimmed = (str) => {
  if (str) {
    return str.trim();
  }
  return '';
};

const parseDefaultUnitFromTolerances = (tolerances) => {
  if (tolerances && tolerances !== 'none') {
    const thing = JSON.parse(tolerances);
    if (thing && thing.unit) {
      return thing.unit;
    }
  }
  return null;
};

const getUnit = (type, part) => {
  if (!type || !part) {
    return null;
  }
  if (type.includes('Angle')) {
    const result = parseDefaultUnitFromTolerances(part.defaultAngularTolerances);
    if (result) {
      return result;
    }
    return defaultUnitIDs.degree; // Default to degrees if can't get from setting
  }

  const result = parseDefaultUnitFromTolerances(part.defaultLinearTolerances);
  if (result) {
    return result;
  }

  if (part.measurement === 'Metric') {
    return defaultUnitIDs.millimeter; // Default to millimeters if can't get from setting
  }
  return defaultUnitIDs.inch; // Default to inches if can't get from setting
};

export const toEnumFormat = (text) => {
  if (text === 'extreme' || text === undefined) return 'Note';
  const words = text.split('_');
  for (let i = 0; i < words.length; i++) {
    if (words[i] !== 'a' && words[i] !== 'of') {
      words[i] = words[i].charAt(0).toUpperCase() + words[i].substring(1);
    }
  }
  if (words.length > 1) {
    let enumText = words[0];
    for (let i = 1; i < words.length; i++) {
      enumText += `_${words[i]}`;
    }

    return enumText;
  }
  return words[0];
};

export const updateExistingItem = (result, part, assignedStyles, existingItem, type, subtype, notationClass, classified, tolerances) => {
  if (!part || !part.id || !existingItem) return null;
  // Prepare the UI object from the characteristic
  const returnItem = { ...existingItem, plusTol: classified.PlusTol, minusTol: classified.MinusTol, upperSpecLimit: classified.UpperSpecLimit, lowerSpecLimit: classified.LowerSpecLimit };
  returnItem.confidence = -1; // No confidence is returned from current service
  returnItem.fullSpecificationFromOCR = result.ocrRes;
  returnItem.notationType = type;
  returnItem.notationSubtype = subtype;
  returnItem.notationClass = notationClass;
  returnItem.markerStyle = getTypeMarkerStyle(assignedStyles, type, subtype).style;
  returnItem.captureMethod = 'Automated';
  returnItem.verified = false;
  returnItem.toleranceSource = classified.LowerSpecLimit || classified.UpperSpecLimit ? 'Document_Defined' : 'Default_Tolerance';

  // get Type name from UUID
  if (type === defaultTypeIDs.Note) {
    // need to check properties, but still want empty strings to pass
    if (Object.prototype.hasOwnProperty.call(result, 'parsed') && Object.prototype.hasOwnProperty.call(result.parsed, 'input') && result.parsed.input !== null) {
      returnItem.fullSpecification = getTrimmed(result.parsed.input);
      returnItem.nominal = getTrimmed(result.parsed.input);
    } else {
      returnItem.fullSpecification = result;
      returnItem.nominal = result;
    }
    returnItem.unit = '';
  } else {
    // All items besides Notes
    returnItem.fullSpecification = classified.FullSpecification;
    returnItem.nominal = classified.Nominal;
    returnItem.unit = getUnit(toEnumFormat(classified.SubType), part);
  }

  // If the item is a GD&T
  if (type === defaultTypeIDs['Geometric Tolerance']) {
    returnItem.fullSpecification = classified.FullSpecification;
    returnItem.gdtPrimaryToleranceZone = classified.Gdt?.GdtTol;
    returnItem.gdtSecondaryToleranceZone = classified.Gdt?.GdtTolZone2;
    returnItem.gdtPrimaryDatum = classified.Gdt?.GdtPrimaryDatum;
    returnItem.gdtSecondaryDatum = classified.Gdt?.GdtSecondDatum;
    returnItem.gdtTertiaryDatum = classified.Gdt?.GdtTertiaryDatum;
  } else if (type !== defaultTypeIDs.Note) {
    // feature is a dimension, recalculate the tolerances
    const newTolerances = assignFeatureTolerances({ feature: returnItem, tolerances, measurement: returnItem.notationSubtype });
    returnItem.upperSpecLimit = newTolerances.upperSpecLimit || '';
    returnItem.lowerSpecLimit = newTolerances.lowerSpecLimit || '';
    returnItem.plusTol = newTolerances.plusTol || '';
    returnItem.minusTol = newTolerances.minusTol || '';
  }

  // All items INCLUDING Notes
  // These are required fields on the CharacteristicInput type
  returnItem.quantity = classified.Quantity;
  returnItem.upperSpecLimit = classified.UpperSpecLimit;
  returnItem.lowerSpecLimit = classified.LowerSpecLimit;
  returnItem.plusTol = classified.PlusTol;
  returnItem.minusTol = classified.MinusTol;

  return returnItem;
};

export const classifyCharacteristic = (result, tolerances) => {
  // Cast the result from the engineering notation parser to a characteristic
  const classified = classify(result.parsed, tolerances.linear, tolerances.angular);
  const type = classified.Type;
  const subType = classified.SubType;

  let notationClass = 'Tolerance';
  if (classified.IsBasic) {
    notationClass = 'Basic';
  } else if (classified.IsReference) {
    notationClass = 'Reference';
  }
  return { classified, type, subType, notationClass };
};
