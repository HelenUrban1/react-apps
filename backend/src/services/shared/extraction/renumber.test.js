const renumberAfterDelete = require('./renumber').default;
const { characteristics, listAfterDelete } = require('./renumberTestData');

describe('Characteristic List Renumbering when', () => {
  describe('Deleting an Feature', () => {
    it('Should delete an Feature from the List', () => {
      const feature = characteristics[1];
      const newArr = renumberAfterDelete([feature.id], characteristics);
      expect(newArr.newList.length).toEqual(characteristics.length - 1);
      expect(newArr.newList[1]).not.toEqual(feature);
    });

    it('Should Renumber the Features After the Deletion Point when Deleting Ungrouped', () => {
      const feature = characteristics[0];
      const newArr = renumberAfterDelete([feature.id], characteristics);
      expect(newArr.newList.length).toEqual(characteristics.length - 1);
      for (let i = 0; i < newArr.length; i++) {
        expect(newArr.newList[i].markerGroup).toEqual((parseInt(characteristics[i + 1].markerGroup, 10) - 1).toString());
        expect(newArr.newList[i].markerIndex).toEqual(characteristics[i + 1].markerIndex - 1);
      }
    });

    it('Should only decrease the index of following features if a group member was deleted', () => {
      const feature = characteristics[1]; // 2.1
      const newArr = renumberAfterDelete([feature.id], characteristics);
      expect(newArr.newList.length).toEqual(characteristics.length - 1);
      for (let i = 1; i < newArr.length; i++) {
        expect(newArr.newList[i].markerGroup).toEqual(characteristics[i + 1].markerGroup);
        expect(newArr.newList[i].markerIndex).toEqual(characteristics[i + 1].markerIndex - 1);
      }
    });

    it('Should decrease the sub-index of the following group members', () => {
      const feature = characteristics[1]; // 2.1
      const newArr = renumberAfterDelete([feature.id], characteristics);
      expect(newArr.newList.length).toEqual(characteristics.length - 1);
      expect(newArr.newList[1].markerSubIndex).toEqual(0);
      expect(newArr.newList[1].markerLabel).toEqual('2.1');
      expect(newArr.newList[2].markerSubIndex).toEqual(1);
      expect(newArr.newList[2].markerLabel).toEqual('2.2');
    });

    it('Should decrease following features by 1 marker group and 3 indexes when deleting an entire 3 feature group', () => {
      const newArr = renumberAfterDelete([characteristics[1].id, characteristics[2].id, characteristics[3].id], characteristics);
      expect(newArr.newList.length).toEqual(10);
      for (let i = 1; i < newArr.length; i++) {
        expect(newArr[i].markerGroup).toEqual((parseInt(characteristics[i + 3].markerGroup, 10) - 1).toString());
        expect(newArr[i].markerIndex).toEqual(characteristics[i + 3].markerIndex - 3);
      }
    });

    it('Should renumber as needed when deleting an arbitrary set of features', () => {
      const newArr = renumberAfterDelete([characteristics[1].id, characteristics[7].id, characteristics[8].id, characteristics[11].id], characteristics);
      expect(newArr.newList.length).toEqual(9);
      expect(newArr.newList).toEqual(listAfterDelete);
    });
  });
});
