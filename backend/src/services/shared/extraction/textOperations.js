export const cleanLowercaseString = (text) => {
  if (!text || typeof text !== 'string') {
    return text;
  }
  return text.replace(/ /g, '').replace(/_/g, '').toLowerCase();
};

export const sanitizedTextComparison = (textA, textB) => {
  if (!textA || typeof textA !== 'string' || !textB || typeof textB !== 'string') {
    return false;
  }
  const cleanedA = cleanLowercaseString(textA);
  const cleanedB = cleanLowercaseString(textB);
  if (cleanedA === cleanedB) {
    return true;
  }
  return false;
};
