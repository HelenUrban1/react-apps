const { v4: uuidv4 } = require('uuid');
const nodemailerSendgrid = require('nodemailer-sendgrid');
const validate = require('../../../clients/validate');
const nodemailer = require('../../../external/nodemailer')();
const config = require('../../../../config')();
const { log } = require('../../../logger');

/**
 * Handles Email sending
 */
module.exports = class EmailSender {
  constructor(email) {
    // Validate transport config
    if (!EmailSender.isConfigured) {
      throw new Error(`Email provider is not configured. Please configure it at backend/config/<environment>.json.`);
    }

    // Sendgrid nodemailer transport
    this.transporter = nodemailer.createTransport(
      nodemailerSendgrid({
        apiKey: config.email.sendgrid_api_key, // process.env.SENDGRID_API_KEY
      })
    );

    this.email = email;
  }

  static get isConfigured() {
    // return !!config.email && !!config.email.host;
    return !!config.email && !!config.email.sendgrid_api_key;
  }

  async send(params = {}) {
    // Extract customizations or use default config
    const {
      messageId = uuidv4(), //
      from = config.email.from,
      subject = this.email.subject,
      to = this.email.to,
      html = this.email.html,
      ...metadata
    } = params;

    const mailOptions = {
      messageId,
      from,
      to,
      subject,
      html,
      ...metadata, // Append any custom attributes
    };

    // Validate email template
    validate.required(mailOptions, ['to', 'subject', 'html']);

    log.debug(`EmailSender sending message with subject "${subject}" being sent to ${to} with messageId "${messageId}"`);

    const result = await this.transporter
      .sendMail(mailOptions)
      .then(([res]) => {
        log.debug(`EmailSender sentm message with subject "${subject}" and messageId "${messageId}" delivered to ${to} with response code ${res.statusCode} ${res.statusMessage}`);
        return true;
      })
      .catch((err) => {
        log.debug(`EmailSender encountered an error. Failed to deliver message with subject "${subject}" to ${to} with messageId "${messageId}"`);

        if (err.response && err.response.body && err.response.body.errors) {
          log.debug(err.response.headers);
          err.response.body.errors.forEach((error) => log.error(`${error.field} : ${error.message}`));
        } else {
          log.error(err);
        }
        return false;
      });

    return result ? messageId : false;
  }

  // get transportConfig() {
  //   return config.email;
  // }
};
