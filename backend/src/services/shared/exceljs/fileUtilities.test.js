const fs = require('fs');
const path = require('path');
const Excel = require('exceljs');
const { PassThrough } = require('stream');
const { readFile, loadFile, saveBuffer, writeFile, writeStream, writeCSV } = require('./fileUtilities');

describe('Excel File Utilities', () => {
  afterEach(() => {});
  const templateFilePath = 'src/templates/All Data.xlsx';

  describe('readFile', () => {
    it('should load default template file to excel workbook', async () => {
      const workbook = await readFile(templateFilePath);

      const templateSheet = workbook.worksheets[0]; // the first one;
      const customerToken = templateSheet.getCell('B3');

      expect(customerToken.value).toBe('  {{Customer.Name}}');
    });
  });

  describe('loadFile', () => {
    it('should load file data to excel workbook', async () => {
      fs.readFile(path.resolve(templateFilePath), async (err, data) => {
        if (err) {
          throw err;
        }
        const workbook = await loadFile(data);

        const templateSheet = workbook.worksheets[0]; // the first one;
        const customerToken = templateSheet.getCell('B3');

        expect(customerToken.value).toBe('  {{Customer.Name}}');
      });
    });
  });

  describe('saveBuffer', () => {
    it('should save a workbook to a buffer', async () => {
      const workbook = new Excel.Workbook();
      const buffer = await saveBuffer(workbook);
      expect(Buffer.isBuffer(buffer)).toBeTruthy();
    });
  });

  describe('writeFile', () => {
    it('write a workbook to a file', async () => {
      const workbook = new Excel.Workbook();
      await writeFile(workbook, 'holder');
      expect(fs.existsSync(path.resolve('holder'))).toBeTruthy();
      fs.unlinkSync(path.resolve('holder'));
    });
  });

  describe('writeStream', () => {
    it('write a workbook to an xlsx stream', async () => {
      const mockReadable = new PassThrough();
      const workbook = new Excel.Workbook();
      await writeStream(workbook, mockReadable);
      const mockWritable = fs.createWriteStream('stream');
      mockReadable.pipe(mockWritable);
      mockReadable.on('end', () => {
        expect(fs.existsSync(path.resolve('stream'))).toBeTruthy();
        fs.unlinkSync(path.resolve('stream'));
      });
    });
  });

  describe('writeCSV', () => {
    it('write a workbook to a csv stream', async () => {
      const mockReadable = new PassThrough();
      const workbook = new Excel.Workbook();
      workbook.addWorksheet('My Sheet');
      await writeCSV(workbook, 'My Sheet', mockReadable);
      const mockWritable = fs.createWriteStream('csv');
      mockReadable.pipe(mockWritable);
      mockReadable.on('end', () => {
        expect(fs.existsSync(path.resolve('csv'))).toBeTruthy();
        fs.unlinkSync(path.resolve('csv'));
      });
    });
  });
});
