const Excel = require('exceljs');
const fs = require('fs');
const { validateNumber, toFixed10, formatNumericData, formatCellData, getNumberFormat, getFormulaResult, getColumnLettersFromNumber, getColumnNumberFromLetters, getRowNumberFromAddress, getCellsFromFormula } = require('./dataValidation');

// read or create workbook and return the first cell
const workbookHelper = async (old, file) => {
  let worksheet;
  if (file) {
    await old.xlsx.writeFile(file);
    const workbook = new Excel.Workbook();
    await workbook.xlsx.readFile(file);
    worksheet = workbook.getWorksheet('My Sheet');
  } else {
    worksheet = old.addWorksheet('My Sheet');
  }
  const row = worksheet.getRow(1);
  const cell = row.getCell(1);
  return cell;
};

describe('Excel Data Validation', () => {
  describe('valid number', () => {
    it('returns true if string is a number', () => {
      const isNumber = validateNumber('.04');
      expect(isNumber).toBeTruthy();
      const isNumber2 = validateNumber('2.70');
      expect(isNumber2).toBeTruthy();
    });
    it('returns false if string is a not number', () => {
      const isNumber = validateNumber('2.70±.04');
      expect(isNumber).toBeFalsy();
    });
    it('returns true if passed value is a number but not a string', () => {
      const isNumber = validateNumber(0.04);
      expect(isNumber).toBeTruthy();
      const isNumber2 = validateNumber(2.7);
      expect(isNumber2).toBeTruthy();
    });
  });
  describe('significant digits', () => {
    it('should properly round when setting precision', async () => {
      expect(toFixed10('.014', 2)).toBe('0.01');
      expect(toFixed10('.0145', 2)).toBe('0.01');
      expect(toFixed10('.015', 2)).toBe('0.02');
      expect(toFixed10('0.016', 2)).toBe('0.02');
      expect(toFixed10('5.015', 2)).toBe('5.02');
    });
    it('cells should maintain significant digits of characteristic data', async () => {
      const text = '.040000';
      const value = 0.04;
      const format = '#.??????';
      const file = 'sigDigits.xlsx';

      const workbook = new Excel.Workbook();
      const cell = await workbookHelper(workbook);

      const numFmt = getNumberFormat(text);
      expect(numFmt).toBe(format);

      cell.numFmt = numFmt;
      cell.value = formatCellData(text);

      expect(cell.value).toBe(value);
      expect(cell.numFmt).toBe(format);

      const cell2 = await workbookHelper(workbook, file);

      expect(cell2.value).toBe(value);
      expect(cell2.numFmt).toBe(format);
      const formattedCell = formatNumericData(cell2.value, cell2.numFmt);
      expect(formattedCell).toBe(text);

      fs.unlinkSync(file);
    });
  });
  describe('data type', () => {
    it('numeric data should set the cell type to be numeric', async () => {
      const data = '2.44';
      const file = 'numericType.xlsx';

      const workbook = new Excel.Workbook();
      const cell = await workbookHelper(workbook);

      cell.value = formatCellData(data);
      expect(cell.type).toBe(Excel.ValueType.Number);

      const cell2 = await workbookHelper(workbook, file);
      expect(cell2.type).toBe(Excel.ValueType.Number);

      fs.unlinkSync(file);
    });
    it('textual data should set the cell type to be text', async () => {
      const data = 'Some Note: 34.55 with numbers';
      const file = 'textType.xlsx';

      const workbook = new Excel.Workbook();
      const cell = await workbookHelper(workbook);

      cell.value = formatCellData(data);
      expect(cell.type).toBe(Excel.ValueType.String);

      const cell2 = await workbookHelper(workbook, file);
      expect(cell2.type).toBe(Excel.ValueType.String);

      fs.unlinkSync(file);
    });
    it('formulas should remain formulas', async () => {
      const data = { formula: 'A1+A2', result: 7 };
      const file = 'textType.xlsx';

      const workbook = new Excel.Workbook();
      const cell = await workbookHelper(workbook);

      cell.value = formatCellData(data);
      expect(cell.type).toBe(Excel.ValueType.Formula);

      const cell2 = await workbookHelper(workbook, file);
      expect(cell2.type).toBe(Excel.ValueType.Formula);

      fs.unlinkSync(file);
    });
  });
  describe('formulas', () => {
    it('formulas should be able to read numeric characteristic data as numbers', async () => {
      const A1 = '2.77';
      const A2 = '.04';
      const formula = '$A$1+A2';
      const file = 'formula.xlsx';

      const workbook = new Excel.Workbook();
      const worksheet = workbook.addWorksheet('My Sheet');
      const cell1 = worksheet.getCell('A1');
      cell1.value = formatCellData(A1);
      const cell2 = worksheet.getCell('A2');
      cell2.value = formatCellData(A2);
      const cell3 = worksheet.getCell('A3');
      const result = getFormulaResult(formula, worksheet);
      expect(result).not.toBeNull();
      cell3.value = { formula, result };

      await workbook.xlsx.writeFile(file);
      const workbook2 = new Excel.Workbook();
      await workbook2.xlsx.readFile(file);
      const worksheet2 = workbook.getWorksheet('My Sheet');
      const formulaCell = worksheet2.getCell('A3');
      expect(formulaCell.value).toMatchObject({ formula, result: 2.81 });

      fs.unlinkSync(file);
    });
    it('should get all cell references in a forumla', () => {
      const formula = '$A$1+H6';
      const cells = getCellsFromFormula(formula);

      expect(cells[0].address).toBe('A$1');
      expect(cells[0].row).toEqual(1);
      expect(cells[0].col).toEqual(1);

      expect(cells[1].address).toBe('H6');
      expect(cells[1].row).toEqual(6);
      expect(cells[1].col).toEqual(8);
    });
  });
  describe('conditional formatting', () => {
    it('preserves conditional formatting rules', async () => {
      const format = {
        ref: 'A1:E7',
        rules: [
          {
            type: 'expression',
            formulae: ['MOD(ROW()+COLUMN(),2)=0'],
            style: { fill: { type: 'pattern', pattern: 'solid', bgColor: { argb: 'FF00FF00' } } },
          },
        ],
      };
      const file = 'formatting.xlsx';

      const workbook = new Excel.Workbook();
      const worksheet = workbook.addWorksheet('My Sheet');
      worksheet.addConditionalFormatting(format);
      await workbook.xlsx.writeFile(file);
      const workbook2 = new Excel.Workbook();
      await workbook2.xlsx.readFile(file);
      const worksheet2 = workbook.getWorksheet('My Sheet');
      expect(worksheet2.conditionalFormattings).toEqual(expect.arrayContaining([expect.objectContaining(format)]));

      fs.unlinkSync(file);
    });
  });
  describe('extracts row and column info from an address', () => {
    it('converts a number to letters', () => {
      expect(getColumnLettersFromNumber(0)).toEqual('A');
      expect(getColumnLettersFromNumber(3)).toEqual('D');
      expect(getColumnLettersFromNumber(28)).toEqual('AC');
      expect(getColumnLettersFromNumber(1003)).toEqual('ALP');
    });

    it('converts letters to a number', () => {
      expect(getColumnNumberFromLetters('A')).toEqual(0);
      expect(getColumnNumberFromLetters('D')).toEqual(3);
      expect(getColumnNumberFromLetters('AC')).toEqual(28);
      expect(getColumnNumberFromLetters('ALP')).toEqual(1003);
    });

    it('extracts the row number', () => {
      expect(getRowNumberFromAddress('A12')).toEqual(12);
      expect(getRowNumberFromAddress('AB323')).toEqual(323);
    });
  });
});
