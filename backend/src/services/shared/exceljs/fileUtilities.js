const Excel = require('exceljs');
const csv = require('@fast-csv/format');

const { log } = require('../../../logger');

export const loadFile = async (file) => {
  log.debug(`Loading File`);
  const workbook = new Excel.Workbook();
  await workbook.xlsx.load(file);
  return workbook;
};

export const readFile = async (file) => {
  log.debug(`Reading ${file}`);
  const workbook = new Excel.Workbook();
  await workbook.xlsx.readFile(file);
  return workbook;
};

// Saves an exceljs workbook to a buffer
export const saveBuffer = async (workbook) => {
  if (!workbook) {
    log.error('no workbook loaded');
    return false;
  }
  const buffer = await workbook.xlsx.writeBuffer();
  if (!buffer) {
    log.error('buffer was not saved');
  }
  return buffer;
};

export const writeFile = async (workbook, outFilePath) => {
  log.debug(`Writing ${outFilePath}`);
  await workbook.xlsx.writeFile(outFilePath);
  log.debug('Write complete');
  return outFilePath;
};

export const writeStream = async (workbook, stream) => {
  await workbook.xlsx.write(stream);
};

const convertToPlain = (str) => {
  const basicRtfPattern = /\{\*?\\[^{}]+;}|[{}]|\\[A-Za-z]+\n?(?:-?\d+)?[ ]?/g;
  const newLineSlashesPattern = /\\\n/g;
  const ctrlCharPattern = /\n\\f[0-9]\s/g;

  // Remove RTF Formatting, replace RTF new lines with real line breaks, and remove whitespace
  return str
    .replace(ctrlCharPattern, '')
    .replace(basicRtfPattern, '')
    .replace(newLineSlashesPattern, '\n')
    .replace(/[\u2018\u2019]/g, "'")
    .replace(/[\u201C\u201D]/g, '"')
    .trim();
};

export const writeCSV = async (workbook, sheet, stream) => {
  return new Promise((resolve, reject) => {
    const worksheet = workbook.getWorksheet(sheet);
    const csvStream = csv.format({ writeBOM: true });
    stream.on('finish', () => {
      resolve();
    });
    csvStream.on('error', reject);
    csvStream.pipe(stream);
    let lastRow = 1;
    if (worksheet) {
      worksheet.eachRow((row, rowNumber) => {
        while (lastRow + 1 < rowNumber - 1) {
          csvStream.write([]);
          lastRow += 1;
        }
        // const { values } = row;
        const values = [];
        row.eachCell((cell) => {
          if (cell.type === Excel.ValueType.Merge) {
            values.push('');
          } else if (cell.value.richText) {
            let val = '';
            cell.value.richText.forEach((rich) => {
              if (rich.text) val += convertToPlain(rich.text);
            });
            values.push(val);
          } else {
            values.push(cell.value);
          }
        });
        csvStream.write(values);
        lastRow = rowNumber;
      });
    }
    csvStream.end();
  });
};
