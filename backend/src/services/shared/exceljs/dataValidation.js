const FormulaParser = require('hot-formula-parser').Parser;
const { log } = require('../../../logger');

const isValidNumber = new RegExp(/^-?\d*\.?\d*$/);

const validateNumber = (number) => {
  return number !== null && number !== undefined && number !== '' && number !== ' ' && !!isValidNumber.test(number);
};

const getPrecision = (number) => {
  if (!number) {
    log.warn('cannot get precision of null');
    return null;
  }
  const split = number.toString().split('.');
  if (!split || split.length <= 1) {
    return 0;
  }
  const places = split[1].length;
  if (!places || places > number.length) {
    log.error('Number precision could not be determined');
    return null;
  }
  return places;
};

const getDigits = (number) => {
  const split = number.toString().split('.');
  if (!split || split[0].length === number.length) {
    return number.length;
  }
  const places = split[0].length;
  if (!places) {
    return 0;
  }
  return places;
};

// Convert number into excel numfmt
const getNumberFormat = (value) => {
  const isNumber = validateNumber(value);
  if (!isNumber) {
    return null;
  }
  const precision = getPrecision(value);
  const digits = getDigits(value);
  let numFmt = '';
  if (digits && digits > 0) {
    for (let i = 0; i < digits; i++) {
      // the ? is a digit placeholder that preserves leading zeros
      numFmt += '?';
    }
  } else {
    // the # is a digit placeholder that ignores leading zeros
    numFmt += '#';
  }
  if (precision && precision > 0) {
    numFmt += '.';
    for (let i = 0; i < precision; i++) {
      numFmt += '?';
    }
  }
  return numFmt;
};

// Parse formula results. XLSX does this automatically, but this allows us to test the results in code
const getFormulaResult = (formula, worksheet) => {
  if (!formula || !worksheet) {
    log.error('getFormula called with missing parameter');
    return null;
  }
  const parser = new FormulaParser();
  parser.on('callCellValue', (cellCoord, done) => {
    const row = worksheet.getRow(cellCoord.row.index + 1);
    const cell = row.getCell(cellCoord.column.index + 1);
    done(cell.value);
  });
  const parsed = parser.parse(formula);
  if (parsed.error) {
    // log.warn(`Formula error: ${parsed.error}`);
    return null;
  }
  return parsed.result;
};

// Format a string value to the proper cell value type
const formatCellData = (data) => {
  if (typeof data === 'object') {
    return data;
  }
  const isNumber = validateNumber(data);
  if (isNumber) {
    return parseFloat(data);
  }
  return data.toString();
};

const toFixed10 = (number, precision) => {
  const num = Number.parseFloat(number);
  return (+`${Math.round(+`${num}e${precision}`)}e${-precision}`).toFixed(precision);
};

// Format numeric data into a string, based on the numfmt
const formatNumericData = (value, format) => {
  const isNumber = validateNumber(value);
  if (isNumber) {
    const numString = value.toString();
    // Strip out ? placeholder used to trim leading zero
    const formatString = format.replace('#.', '.');
    let text = '';
    const split = numString.split('.');

    const formatPrecision = getPrecision(formatString);
    const formatDigits = getDigits(formatString);
    const valuePrecision = getPrecision(numString);
    const valueDigits = getDigits(numString);

    // Format Before .
    if (valueDigits !== formatDigits && formatDigits > valueDigits) {
      if (formatPrecision > valuePrecision) {
        const leadingZeros = formatDigits - valueDigits;
        for (let i = 0; i < leadingZeros; i++) {
          text += '0';
        }
      }
    }

    // If No decimal
    if (!split || split[0].length === numString.length) {
      text += numString;
      return text;
    }
    if (!formatPrecision || formatPrecision === 0) {
      text += Math.round(split[0]);
      return text;
    }

    if (formatDigits !== 0) {
      text += split[0];
    }
    text += '.';

    // Format After .
    if (valuePrecision !== formatPrecision) {
      if (formatPrecision > valuePrecision) {
        text += split[1];
        const trailingZeros = formatPrecision - valuePrecision;
        for (let i = 0; i < trailingZeros; i++) {
          text += '0';
        }
        return text;
      }
      const roundedDecimal = toFixed10(`.${split[1]}`, formatPrecision);
      text += roundedDecimal.substring(2);
      return text;
    }
    text += split[1];
    return text;
  }
  return value;
};

const alphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';

// Converts an Integer inter an excel address column string
const getColumnLettersFromNumber = (num) => {
  let res = '';
  let charIndex = (num + 1) % alphabet.length;
  let quotient = (num + 1) / alphabet.length;
  if (charIndex - 1 === -1) {
    charIndex = alphabet.length;
    quotient -= 1;
  }
  res += alphabet.charAt(charIndex - 1) + res;
  if (quotient >= 1) {
    res = getColumnLettersFromNumber(parseInt(quotient - 1, 10)) + res;
  }
  return res;
};

// Converts excel address format strings (e.g. AZ5) to column numbers, 0 indexed
const getColumnNumberFromLetters = (address) => {
  const letters = address.replace(/[0-9]/g, '');
  let number = 0;
  for (let i = 0, j = letters.length - 1; i < letters.length; i += 1, j -= 1) {
    number += alphabet.length ** j * (alphabet.indexOf(letters[i]) + 1);
  }
  return number - 1;
};

// Extracts the row number from an Address string
const getRowNumberFromAddress = (address) => {
  return parseInt(address.replace(/[a-zA-Z]/g, ''), 10);
};

// Parse a formula and return an array of every cell referenced in the formula
const getCellsFromFormula = (formula) => {
  const regex = new RegExp(/([a-zA-Z]+)+(\$?)?(\d+)+/g);
  const cells = [];
  let match = regex.exec(formula);
  const addresses = [match];
  while (match != null) {
    match = regex.exec(formula); // returns [match, column, $, row]
    addresses.push(match);
  }

  for (let index = 0; index < addresses.length; index++) {
    const ref = addresses[index];
    if (ref) {
      const col = parseInt(getColumnNumberFromLetters(ref[1]), 10) + 1;
      const row = parseInt(ref[3], 10);
      const replaceText = ref[0];
      cells.push({ address: replaceText, col, row });
    }
  }
  return cells;
};

exports.validateNumber = validateNumber;
exports.getPrecision = getPrecision;
exports.getDigits = getDigits;
exports.getNumberFormat = getNumberFormat;
exports.getCellsFromFormula = getCellsFromFormula;
exports.getFormulaResult = getFormulaResult;
exports.formatCellData = formatCellData;
exports.toFixed10 = toFixed10;
exports.formatNumericData = formatNumericData;
exports.getColumnLettersFromNumber = getColumnLettersFromNumber;
exports.getColumnNumberFromLetters = getColumnNumberFromLetters;
exports.getRowNumberFromAddress = getRowNumberFromAddress;
