const config = require('../../config')();
const USE_MOCKS = config.useMocks;

if (USE_MOCKS) {
  jest.doMock('../database/models');
  jest.doMock('../database/repositories/sampleRepository');
}

const models = require('../database/models');
const SampleRepository = require('../database/repositories/sampleRepository');
const SampleService = require('./sampleService');
const SequelizeRepository = require('../database/repositories/sequelizeRepository');

describe('SampleService', () => {
  const accountId = config.testAccountId;
  const data = {
    serial: 'Test Sample',
    sampleIndex: 0,
    featureCoverage: '100%',
    status: 'Unmeasured',
  };

  const createMock = jest.fn().mockReturnValue(data);
  const updateMock = jest.fn().mockReturnValue({ ...data, serial: 'foo' });

  beforeEach(async () => {
    if (USE_MOCKS) {
      models.getTenant = () =>
        Promise.resolve({
          sequelize: {
            transaction: jest.fn().mockReturnValue({
              rollback: jest.fn(),
              commit: jest.fn(),
            }),
          },
        });

      models.sequelizeAdmin = {
        query: jest.fn().mockReturnValue(Promise.resolve({})),
      };

      SampleRepository.mockImplementation(() => {
        return {
          create: createMock,
          update: updateMock,
        };
      });

      SequelizeRepository.commitTransaction = jest.fn();
    }
  });

  afterAll(async () => {
    if (!USE_MOCKS) {
      await SequelizeRepository.closeConnections();
      await SequelizeRepository.closeConnections(accountId);
    }
  });

  describe('CreateSample', () => {
    const context = { currentUser: { accountId } };

    it('Should return sample data', async () => {
      try {
        const sample = await new SampleService(context).create(data);
        if (USE_MOCKS) {
          expect(createMock).toBeCalledTimes(1);
        }
        expect(sample).toBeTruthy();
      } catch (exception) {
        console.error(exception);
        expect(exception).toBeFalsy();
      }
    }, 10000);
  });

  describe('Update Sample', () => {
    const context = { currentUser: { accountId } };

    it('Should return updated sample data', async () => {
      try {
        const sample = await new SampleService(context).create(data);
        expect(sample).toBeTruthy();

        const sampleAgain = await new SampleService(context).update(sample.id, { serial: 'foo' });

        if (USE_MOCKS) {
          expect(updateMock).toBeCalledTimes(1);
        }
        expect(sampleAgain.serial).toEqual('foo');
      } catch (exception) {
        console.error(exception);
        expect(exception).toBeFalsy();
      }
    }, 10000);
  });
});
