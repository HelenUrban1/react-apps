const config = require('../../config')();
const USE_MOCKS = config.useMocks;

if (USE_MOCKS) {
  jest.doMock('../database/models');
  jest.doMock('../database/repositories/accountRepository');
}

const { secretsManager } = require('aws-wrapper');
const AccountService = require('./accountService');
const models = require('../database/models');
const AccountRepository = require('../database/repositories/accountRepository');
const SequelizeRepository = require('../database/repositories/sequelizeRepository');

describe('AccountService', () => {
  describe('CreateAccount', () => {
    let account;
    const createMock = jest.fn().mockReturnValue({
      id: '6c1b1e15-873a-488f-bd43-c10ce567f91e',
      settings: '1',
      status: 'Active',
      orgName: 'test',
      orgEmail: 'test@example.com',
      dbHost: '',
      dbName: '6c1b1e15-873a-488f-bd43-c10ce567f91e',
    });

    afterAll(async () => {
      if (!USE_MOCKS) {
        await deleteDatabase(account.dbName);
        await deleteDatabaseUser(account.dbName);
        await SequelizeRepository.closeConnections(account.id);
        await SequelizeRepository.closeConnections();
      }
    });

    beforeEach(() => {
      if (USE_MOCKS) {
        models.getTenant = () => Promise.resolve({});

        models.sequelizeAdmin = {
          query: jest.fn().mockReturnValue(Promise.resolve({})),
        };

        AccountRepository.mockImplementation(() => {
          return {
            create: createMock,
          };
        });

        SequelizeRepository.commitTransaction = jest.fn();
      }
      jest.spyOn(secretsManager, 'createDatabaseSecret');
      jest.spyOn(secretsManager, 'createRandomPassword');
    });

    const context = {};
    const data = {
      settings: '1',
      status: 'Active',
      orgName: 'test',
      orgEmail: 'test@example.com',
      dbHost: '',
      dbName: '',
    };

    it('Should return account data', async () => {
      try {
        account = await new AccountService(context).create(data);
        if (USE_MOCKS) {
          expect(createMock).toBeCalledTimes(1);
          expect(models.sequelizeAdmin.query).toBeCalledTimes(4);
          expect(account.id).toBe('6c1b1e15-873a-488f-bd43-c10ce567f91e');
        } else {
          expect(account.id.length).toBe(36);
          const userCheckResult = await models.sequelize.query(`SELECT COUNT(u.usename) FROM pg_catalog.pg_user u WHERE u.usename = '${account.dbName}';`, { type: models.sequelize.QueryTypes.RAW });
          expect(userCheckResult[0][0].count).toBe('1');
          const databaseCheckResult = await models.sequelize.query(`SELECT COUNT(datname) FROM pg_catalog.pg_database WHERE datname = '${account.dbName}';`, { type: models.sequelize.QueryTypes.RAW });
          expect(databaseCheckResult[0][0].count).toBe('1');
        }
        expect(secretsManager.createRandomPassword).toBeCalledTimes(1);
        expect(secretsManager.createDatabaseSecret).toBeCalledTimes(1);
      } catch (exception) {
        console.error(exception);
        expect(exception).toBeFalsy();
      }
    });
  });
});

/*
 *   Cleanup functions
 */
const deleteDatabase = async (tenantId) => {
  try {
    await models.sequelize.query(`SELECT pg_terminate_backend(pid) FROM pg_stat_activity WHERE datname = '${tenantId}';`, { type: models.sequelize.QueryTypes.RAW });
  } catch (error) {
    console.error('Error closing database connections');
    console.error(error);
  }
  try {
    await models.sequelize.query(`DROP DATABASE IF EXISTS "${tenantId}" `, { type: models.sequelize.QueryTypes.RAW });
  } catch (error) {
    console.error('Error deleting database');
    console.error(error);
  }
};

const deleteDatabaseUser = async (tenantId) => {
  try {
    await models.sequelize.query(`DROP USER IF EXISTS "${tenantId}"`, { type: models.sequelize.QueryTypes.RAW });
  } catch (error) {
    console.error('Error deleting database user');
    console.error(error);
  }
};
