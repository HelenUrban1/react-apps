const UserRepository = require('../database/repositories/userRepository');
const AccountMemberRepository = require('../database/repositories/accountMemberRepository');
const { log } = require('../logger');
const AuditLogRepository = require('../database/repositories/auditLogRepository');

/**
 * Handles Events from external systems (ie from webhook callbacks)
 */
class ExternalEventService {
  /**
   * Primary entry point that validates and routes new events
   *
   * @param {*} params
   * @prop {*} event
   */
  static async processEvent(params) {
    let result = {};
    try {
      // Extract event info
      const { event, ...context } = params;
      const { data, attributes } = event;
      const { webhookSource } = attributes;

      // Route event to correct handler
      if (webhookSource === 'hubspot') {
        log.debug(`Handle hubspot event`);
        result.message = 'HUBSPOT-RESPONSE-WOULD-BE-HERE';
      } else if (webhookSource === 'chargify') {
        result = await this.handleChargifyEvent(data, context);
      } else if (webhookSource === 'sendgrid') {
        result = await this.handleSendgridEvent(data, context);
      } else {
        log.warn(`No handler defined for event type ${webhookSource}`);
        // TODO: Fire an analytics event from here to New Relic
        // 422 Unprocessable Entity (WebDAV)
        // This could also be "501 : Not Implemented", but that may leave the event on the queue and result in re-attempts
        result.status = 501;
        result.message = `No handler matched the event type ${webhookSource}`;
      }
      log.info(`Processing of ${webhookSource} event complete.`, result);
    } catch (error) {
      log.error('ExternalEventService.processEvent.error', error);
      result.error = error;
    }
    return result;
  }

  /**
   * Creates an audit log of the operation.
   *
   * @param {string} action - The action [create, update or delete].
   * @param {string} action - The action [create, update or delete].
   * @param {object} record - The sequelize record
   * @param {object} values - The JSON data passed on the request
   * @param {object} options
   */
  static async _createAuditLog(entityName, action, record, values, options) {
    await AuditLogRepository.log(
      {
        entityName,
        entityId: record.id,
        action,
        values,
      },
      options
    );
  }

  /**
   * Helper method that looks up an account member's details by email address
   * Record includes consolidated data from user, userProfile, account, and accountMember
   *
   * @param {String} email - The email address for the user being looked up
   * @param {Object} options
   */
  // NOTE: We shouldn't be building user records inside of services other than userService or accountService.
  // TODO: The authService and this service both contain logic that should be moved into the user model or the repo.
  static async findAccountMemberByEmail(email, options = {}) {
    const user = await UserRepository.findByEmailWithoutAvatar(email, {
      ...options,
      attributes: ['id', 'emailVerified', 'email', 'activeAccountMemberId', 'disabled', 'createdAt', 'updatedAt', 'deletedAt'],
    });
    if (!user) return;

    // To support isolated multi-tenancy user details were moved into a profile object
    // The block below elevates the profile details to the top level of the currentUser record
    if (user.profile) {
      user.firstName = user.profile.firstName;
      user.lastName = user.profile.lastName;
      user.phoneNumber = user.profile.phoneNumber;
    }

    let membership;
    // TODO: When users can have multiple memberships, this will need to be updated
    if (!user.accountMemberships || user.accountMemberships.length === 0) {
      log.warn(`A membership could not be found for userId: ${user.id} accountId: ${user.accountId}`);
    } else if (user.accountMemberships.length > 1) {
      log.warn(`More than one membership was returned for userId: ${user.id} accountId: ${user.accountId}`);
    } else {
      membership = user.accountMemberships[0].dataValues;
    }

    // Remove uneeded attributes
    delete user.profile;
    delete user.accountMemberships;
    delete user.accountSettings;
    delete user.avatars;

    const record = {
      userId: user.id,
      ...user,
      membership,
    };
    return record;
  }

  /**
   * Event handler for batches of SendGrid events
   *
   * @param {[Object]]} - An array of SendGrid webhook events
   * @param {Object} context
   */
  static async handleSendgridEvent(events, context) {
    log.debug(`Handle SendGrid event batch of ${events.length} events`);

    // Loop over all the events in the batch
    let successes = 0;
    const users = {};
    const missingUsers = {};
    const resolutions = [];
    for (let e = 0; e < events.length; e++) {
      const sendGridEvent = events[e];
      const resolution = { sendGridEvent };
      let member;
      try {
        // Check to see a previous lookup attempt failed for this user
        if (missingUsers[sendGridEvent.email]) throw new Error(`Could not find user for email ${sendGridEvent.email}`);

        // Lookup the userId for the email address (but never more than once per email address)
        if (!users[sendGridEvent.email]) {
          member = await this.findAccountMemberByEmail(sendGridEvent.email);
          if (!member) {
            missingUsers[member] = true;
            throw new Error(`Could not find user for email ${sendGridEvent.email}`);
          }
          users[member.email] = member;
        } else {
          member = users[sendGridEvent.email];
        }

        // Record the event in analytics timeline
        context.analyticsClient.track({
          userId: member.userId,
          ixcEvent: context.analyticsClient.events.email[sendGridEvent.event],
          user: member,
          properties: {
            event: sendGridEvent,
          },
        });

        await this._createAuditLog('sendgrid', sendGridEvent.event, { id: sendGridEvent.sg_message_id }, sendGridEvent, { ...context, currentUser: member });

        // TODO: We could store an array of the events as a string
        // Save the most recent event as the inviteStatus
        await AccountMemberRepository.update(member.membership.id, { inviteStatus: JSON.stringify(sendGridEvent) });

        // TODO: Save to audit log
        resolution.success = true;
        successes += 1;
      } catch (error) {
        log.error(`handleSendgridEvent error: ${error.toString()}`);
        // Record the event in analytics timeline
        if (member && member.userId) {
          context.analyticsClient.track({
            userId: member.userId,
            ixcEvent: context.analyticsClient.events.email.error,
            user: member,
            properties: {
              event: sendGridEvent,
            },
          });
        }
        resolution.error = error.message;
        resolution.success = false;
      }
      resolutions.push(resolution);
    }

    return {
      results: resolutions,
      message: `Processed ${successes} of ${events.length} SendGrid events successfully`,
    };
  }

  /**
   * Event handler for batches of Chargify events
   *
   * @param {[Object]]} - An array of Chargify webhook events
   * @param {Object} context
   */
  static async handleChargifyEvent(events, context) {
    log.debug(`Handle Chargify event batch of ${events.length} events`);
    // Loop over all the events in the batch
    let successes = 0;
    const resolutions = [];
    for (let e = 0; e < events.length; e++) {
      const chargifyEvent = events[e];
      const resolution = { chargifyEvent };
      let member = {};
      if (chargifyEvent.payload && context.subscriptionWorker) {
        try {
          // get user from payload
          if (chargifyEvent.payload.customer) {
            member = chargifyEvent.payload.customer;
          } else if (chargifyEvent.payload.subscription) {
            member = chargifyEvent.payload.subscription.customer;
          } else if (chargifyEvent.payload.transaction) {
            member = { id: chargifyEvent.payload.transaction.customer_id, email: 'na' };
          } else {
            log.warn(`Could not extract member from chargifyEvent`, chargifyEvent);
            throw new Error(`Could not extract member from chargifyEvent`);
          }

          // Lookup the userId for the email address (but never more than once per email address)
          log.debug(`chargifyEvent member`, member);
          const memberEmail = member.email;
          member = await this.findAccountMemberByEmail(member.email);
          if (!member) {
            throw new Error(`Could not find user for member with email ${memberEmail}`);
          }

          // Record the event in analytics timeline
          context.analyticsClient.track({
            userId: member.id,
            ixcEvent: context.analyticsClient.events.chargify[chargifyEvent.event],
            user: member,
            properties: {
              event: chargifyEvent,
            },
          });

          switch (chargifyEvent.event) {
            case 'payment_failure':
            case 'renewal_failure':
              await context.subscriptionWorker.paymentFailure(chargifyEvent.payload, context);
              break;
            default:
              log.warn(`No handler defined for event type ${chargifyEvent.event}, customer: ${member.id}`);
          }

          await this._createAuditLog('chargify', chargifyEvent.event, chargifyEvent, chargifyEvent, { currentUser: member });
          resolution.success = true;
          successes += 1;
        } catch (error) {
          log.error(error);
          // Record the event in analytics timeline
          if (member && member.id) {
            context.analyticsClient.track({
              userId: member.id,
              ixcEvent: context.analyticsClient.events.chargify.error,
              user: member,
              properties: {
                event: chargifyEvent,
              },
            });
          }
          resolution.error = error.message;
          resolution.success = false;
        }
      } else {
        resolution.error = 'No payload';
        resolution.success = false;
      }
      resolutions.push(resolution);
    }

    return {
      results: resolutions,
      message: `Processed ${successes} of ${events.length} Chargify events successfully`,
    };
  }
}

module.exports = ExternalEventService;
