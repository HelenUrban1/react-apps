const AccountMemberRepository = require('../database/repositories/accountMemberRepository');
const ValidationError = require('../errors/validationError');
const SequelizeRepository = require('../database/repositories/sequelizeRepository');

/**
 * Handles AccountMember operations
 */
module.exports = class AccountMemberService {
  constructor({ currentUser, language, billingClient }) {
    this.currentUser = currentUser;
    this.language = language;
    this.billingClient = billingClient;
  }

  /**
   * Creates a AccountMember.
   *
   * @param {*} data
   */
  async create(data) {
    const transaction = await SequelizeRepository.createTransaction();

    let recordData;

    try {
      recordData = await AccountMemberRepository.create(data, {
        transaction,
        currentUser: this.currentUser,
      });

      await SequelizeRepository.commitTransaction(transaction);
    } catch (error) {
      await SequelizeRepository.rollbackTransaction(transaction);
      throw error;
    }

    const accountMembersCount = await AccountMemberRepository.count({
      accountId: recordData.accountId,
    });

    if (accountMembersCount === 1) {
      const record = await this.findById(recordData.id, {
        modelInstance: true,
      });
      await this._createBillingCustomer(record);
    }

    return recordData;
  }

  async changeAccountOwnerFromCurrentUser(data, options = {}) {
    const transaction = await SequelizeRepository.createTransaction();

    let recordData;
    let originalOwnerId;

    try {
      // The `options.currentUser` property is used by tests
      originalOwnerId = options.currentUser ? options.currentUser.id : this.currentUser.id;

      const params = {
        ...data,
        options: {
          transaction,
          currentUser: options.currentUser || this.currentUser,
        },
      };

      recordData = await AccountMemberRepository.changeAccountOwnerFromCurrentUser(params);

      await SequelizeRepository.commitTransaction(transaction);
    } catch (error) {
      await SequelizeRepository.rollbackTransaction(transaction);
      throw error;
    }

    const record = await this.findById(recordData.id, {
      modelInstance: true,
    });
    await this._updateBillingCustomer(record, originalOwnerId);
  }

  /**
   * Updates a AccountMember.
   *
   * @param {*} id
   * @param {*} data
   */
  async update(id, data) {
    const transaction = await SequelizeRepository.createTransaction();

    try {
      const record = await AccountMemberRepository.update(id, data, {
        transaction,
        currentUser: this.currentUser,
      });

      await SequelizeRepository.commitTransaction(transaction);

      return record;
    } catch (error) {
      await SequelizeRepository.rollbackTransaction(transaction);
      throw error;
    }
  }

  /**
   * Destroy all AccountMembers with those ids.
   *
   * @param {*} ids
   */
  async destroyAll(ids) {
    const transaction = await SequelizeRepository.createTransaction();

    try {
      for (const id of ids) {
        await AccountMemberRepository.destroy(id, {
          transaction,
          currentUser: this.currentUser,
        });
      }

      await SequelizeRepository.commitTransaction(transaction);
    } catch (error) {
      await SequelizeRepository.rollbackTransaction(transaction);
      throw error;
    }
  }

  /**
   * Finds the AccountMember by Id.
   *
   * @param {*} id
   * @param {Object} [options]
   */
  async findById(id, options) {
    return AccountMemberRepository.findById(id, options);
  }

  /**
   * Finds AccountMembers for Autocomplete.
   *
   * @param {*} search
   * @param {*} limit
   */
  async findAllAutocomplete(search, limit) {
    return AccountMemberRepository.findAllAutocomplete(search, limit);
  }

  /**
   * Finds AccountMembers based on the query.
   *
   * @param {*} args
   */
  async findAndCountAll(args) {
    return AccountMemberRepository.findAndCountAll(args);
  }

  /**
   * Imports a list of AccountMembers.
   *
   * @param {*} data
   * @param {*} importHash
   */
  async import(data, importHash) {
    if (!importHash) {
      throw new ValidationError(this.language, 'importer.errors.importHashRequired');
    }

    if (await this._isImportHashExistent(importHash)) {
      throw new ValidationError(this.language, 'importer.errors.importHashExistent');
    }

    const dataToCreate = {
      ...data,
      importHash,
    };

    return this.create(dataToCreate);
  }

  /**
   * Checks if the import hash already exists.
   * Every item imported has a unique hash.
   *
   * @param {*} importHash
   */
  async _isImportHashExistent(importHash) {
    const count = await AccountMemberRepository.count({
      importHash,
    });

    return count > 0;
  }

  /**
   * Create a Chartify Customer record
   *
   * @param {AccountMember} record
   */
  async _createBillingCustomer(record) {
    const user = await record.getUser({ raw: true });

    const chargifyCustomer = await this.billingClient.createCustomer({
      reference: user.id,
      email: user.email,
      firstName: user.firstName,
      lastName: user.lastName,
    });

    if (!chargifyCustomer) {
      throw new Error(`Unable to create Chargify customer record for user ${user.id}`);
    }
  }

  /**
   * Update the Chartify Customer record
   *
   * @param {AccountMember} record
   * @param {String} originalOwnerId
   */
  async _updateBillingCustomer(record, originalOwnerId) {
    const user = await record.getUser({ raw: true });

    const chargifyCustomer = await this.billingClient.getCustomer(originalOwnerId);
    if (!chargifyCustomer) {
      throw new Error(`Unable to locate Chargify customer record for User ${record.user}`);
    }

    // TODO: firstName and lastName don't exist on user, need to get info from userProfile
    await this.billingClient.updateCustomer({
      chargifyId: chargifyCustomer.id,
      reference: user.id,
      firstName: user.firstName || chargifyCustomer.firstName,
      lastName: user.lastName || chargifyCustomer.lastName,
      email: user.email,
    });
  }
};
