const UserRepository = require('../database/repositories/userRepository');
const SequelizeRepository = require('../database/repositories/sequelizeRepository');

module.exports = class UserService {
  constructor(context) {
    this.repository = UserRepository;
    this.currentUser = context.currentUser;
  }

  /**
   * Creates a User.
   *
   * @param {*} data
   */
  async create(data, options = {}) {
    const transaction = await SequelizeRepository.createTransaction();

    try {
      const record = await this.repository.create(data, {
        transaction,
        currentUser: this.currentUser,
        ...options,
      });
      await SequelizeRepository.commitTransaction(transaction);

      return record;
    } catch (error) {
      await SequelizeRepository.rollbackTransaction(transaction);
      throw error;
    }
  }

  /**
   * Finds the User by Id.
   *
   * @param {*} id
   */
  async findById(id, options = {}) {
    return this.repository.findById(id, options);
  }
};
