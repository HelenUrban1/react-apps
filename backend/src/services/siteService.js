const { v4: uuid } = require('uuid');
const SiteRepository = require('../database/repositories/siteRepository');
const ValidationError = require('../errors/validationError');
const SequelizeRepository = require('../database/repositories/sequelizeRepository');

/**
 * Handles Site operations
 */
module.exports = class SiteService {
  constructor({ currentUser, language }) {
    this.repository = new SiteRepository();
    this.currentUser = currentUser;
    this.language = language;
  }

  /**
   * Creates a Site.
   *
   * @param {*} data
   */
  async create(data) {
    if (data.accountId) {
      if (this.currentUser) {
        this.currentUser.accountId = data.accountId;
      } else {
        this.currentUser = {
          accountId: data.accountId,
        };
      }
      delete data.accountId;
    }

    const transaction = await SequelizeRepository.createTransaction(this.currentUser.accountId);
    try {
      const record = await this.repository.create(data, {
        transaction,
        currentUser: this.currentUser,
      });
      await SequelizeRepository.commitTransaction(transaction);
      return record;
    } catch (error) {
      await SequelizeRepository.rollbackTransaction(transaction);
      throw error;
    }
  }

  // We use the id before it gets created by the data layer,
  // so add here if it isn't set
  setId(data) {
    if (!data.id) {
      data.id = uuid();
    }
  }

  /**
   * Updates a Site.
   *
   * @param {*} id
   * @param {*} data
   */
  async update(id, data) {
    const transaction = await SequelizeRepository.createTransaction(this.currentUser.accountId);

    try {
      const record = await this.repository.update(id, data, {
        transaction,
        currentUser: this.currentUser,
      });

      await SequelizeRepository.commitTransaction(transaction);

      return record;
    } catch (error) {
      await SequelizeRepository.rollbackTransaction(transaction);
      throw error;
    }
  }

  /**
   * Destroy all Sites with those ids.
   *
   * @param {*} ids
   */
  async destroyAll(ids) {
    const transaction = await SequelizeRepository.createTransaction(this.currentUser.accountId);

    try {
      for (const id of ids) {
        await this.repository.destroy(id, {
          transaction,
          currentUser: this.currentUser,
        });
      }

      await SequelizeRepository.commitTransaction(transaction);
    } catch (error) {
      await SequelizeRepository.rollbackTransaction(transaction);
      throw error;
    }
  }

  /**
   * Finds the Site by Id.
   *
   * @param {*} id
   */
  async findById(id) {
    return this.repository.findById(id);
  }

  /**
   * Finds Sites for Autocomplete.
   *
   * @param {*} search
   * @param {*} limit
   */
  async findAllAutocomplete(search, limit) {
    return this.repository.findAllAutocomplete(search, limit, {
      currentUser: this.currentUser,
    });
  }

  /**
   * Finds Sites based on the query.
   *
   * @param {*} args
   */
  async findAndCountAll(args) {
    return this.repository.findAndCountAll(args, {
      currentUser: this.currentUser,
    });
  }

  /**
   * Imports a list of Sites.
   *
   * @param {*} data
   * @param {*} importHash
   */
  async import(data, importHash) {
    if (!importHash) {
      throw new ValidationError(this.language, 'importer.errors.importHashRequired');
    }

    if (await this._isImportHashExistent(importHash)) {
      throw new ValidationError(this.language, 'importer.errors.importHashExistent');
    }

    const dataToCreate = {
      ...data,
      importHash,
    };

    return this.create(dataToCreate);
  }

  /**
   * Checks if the import hash already exists.
   * Every item imported has a unique hash.
   *
   * @param {*} importHash
   */
  async _isImportHashExistent(importHash) {
    const count = await this.repository.count({
      importHash,
    });

    return count > 0;
  }
};
