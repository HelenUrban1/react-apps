const config = require('../../config')();
const USE_MOCKS = config.useMocks;

if (USE_MOCKS) {
  jest.doMock('../database/models');
  jest.doMock('../database/repositories/jobRepository');
}

const models = require('../database/models');
const JobRepository = require('../database/repositories/jobRepository');
const JobService = require('./jobService');
const SequelizeRepository = require('../database/repositories/sequelizeRepository');

describe('JobService', () => {
  const accountId = config.testAccountId;
  const data = {
    partId: 'd6c5bc49-9fb2-4fbb-8a61-77bc8d28036f',
    name: '867-5309',
    sampling: 'Timed',
    jobStatus: 'Active',
    interval: 5,
    samples: 99,
    passing: 80,
    scrapped: 10,
  };

  const createMock = jest.fn().mockReturnValue(data);
  const updateMock = jest.fn().mockReturnValue({ ...data, name: 'foo' });

  beforeEach(async () => {
    if (USE_MOCKS) {
      models.getTenant = () =>
        Promise.resolve({
          sequelize: {
            transaction: jest.fn().mockReturnValue({
              rollback: jest.fn(),
              commit: jest.fn(),
            }),
          },
        });

      models.sequelizeAdmin = {
        query: jest.fn().mockReturnValue(Promise.resolve({})),
      };

      JobRepository.mockImplementation(() => {
        return {
          create: createMock,
          update: updateMock,
        };
      });

      SequelizeRepository.commitTransaction = jest.fn();
    }
  });

  afterAll(async () => {
    if (!USE_MOCKS) {
      await SequelizeRepository.closeConnections();
      await SequelizeRepository.closeConnections(accountId);
    }
  });

  describe('CreateJob', () => {
    const context = { currentUser: { accountId } };

    it('Should return job data', async () => {
      try {
        const job = await new JobService(context).create(data);
        if (USE_MOCKS) {
          expect(createMock).toBeCalledTimes(1);
        }
        expect(job).toBeTruthy();
      } catch (exception) {
        console.error(exception);
        expect(exception).toBeFalsy();
      }
    }, 10000);
  });

  describe('Update Account', () => {
    const context = { currentUser: { accountId } };

    it('Should return updated job data', async () => {
      try {
        const job = await new JobService(context).create(data);
        expect(job).toBeTruthy();

        const jobAgain = await new JobService(context).update(job.id, { name: 'foo' });
        if (USE_MOCKS) {
          expect(updateMock).toBeCalledTimes(1);
        }
        expect(jobAgain.name).toEqual('foo');
      } catch (exception) {
        console.error(exception);
        expect(exception).toBeFalsy();
      }
    }, 10000);
  });
});
