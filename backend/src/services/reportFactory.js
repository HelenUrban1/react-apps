// A JS copy of the typescript factory

/* eslint no-param-reassign: ["error", { "props": false }] */
const Excel = require('exceljs');
const path = require('path');
const fs = require('fs');
const { validateNumber, formatCellData, getNumberFormat, getColumnLettersFromNumber, getCellsFromFormula } = require('./shared/exceljs/dataValidation');
const { log } = require('../logger');
const { unFrameText } = require('./shared/exceljs/characters');

const TOKEN_RE = new RegExp(/{{(#[a-zA-Z]+ )?([a-zA-Z]+.[a-zA-Z_]*)}}/);

// Helper function to that returns all matches in a string with match subgroups
// TODO: Move this to a utility class or add matchAll typescript def
function findAll(regex, text, aggregator = []) {
  const arr = regex.exec(text);
  if (arr === null) return aggregator;
  const newString = text.slice(arr.index + arr[0].length);
  return findAll(regex, newString, aggregator.concat([arr]));
}

function parseValidJSON(string) {
  if (typeof string !== 'string') {
    log.warn(`Did not parse none string value ${typeof string}`);
    return {};
  }
  let value;
  try {
    value = JSON.parse(string);
    if (typeof value === 'string') {
      value = JSON.parse(value);
    }
  } catch (e) {
    log.error('Backend parseValidJSONString error', e);
  }
  return value;
}

// Factory class of instance methods that merges a dataset with a provided excel file template
class ReportFactory {
  // Helper function to flatten and lowercase data object
  static findTokens(text) {
    return findAll(TOKEN_RE, text);
  }

  // Finds operators like #each and #use
  static findOperator(text) {
    let result = {
      action: null,
      target: null,
    };
    this.findTokens(text).forEach((token) => {
      // If the token isn't an operator, replace it
      if (token[1]) {
        result = {
          action: token[1].trim(),
          target: token[2],
        };
      }
    });
    return result;
  }

  // Replaces all token instances with their corresponding values from the data
  static replaceTokens(text, data, scope = null) {
    let result = text;
    let key;
    let replaceWith;
    this.findTokens(text).forEach((token) => {
      // If the token isn't an operator, replace it
      if (!token[1]) {
        // Calculate the path to the data
        key = token[2].toLowerCase();
        if (scope) key = key.replace(`${scope}.`, '');
        // Get the replacement value from the merge data
        replaceWith = this.getDataAtDotPath(key, data, true);
        // Replace the token with the data or an indicator that the data wasn't found
        if (!replaceWith && replaceWith !== '') replaceWith = ``;
        result = result.replace(token[0], replaceWith);
      }
    });
    return result;
  }

  // Helper function to access deeply nested objects
  static getDataAtDotPath(dotPath, data) {
    let result = '';
    if (!dotPath || !data) return result;
    try {
      result = dotPath.split('.').reduce((o, i) => {
        if (Array.isArray(o[i]) && typeof o[i][0] === 'string') {
          return o[i].join(', ');
        }
        return o[i];
      }, data);
    } catch (error) {
      result = '';
      log.warn(error);
    }
    return result;
  }

  // Helper function to get the current cell address, skipping for merges
  static getCellReference(worksheet, direction, start, col, row) {
    let current = start;
    let letter;
    let address;
    if (direction === 'Vertical') {
      letter = getColumnLettersFromNumber(col);
      address = letter + (current + 1);
      if (worksheet._merges[address] && worksheet._merges[address].model.top === current + 1) {
        current = worksheet._merges[address].model.bottom;
      } else {
        current += 1;
      }
    } else {
      // Horizontal
      letter = getColumnLettersFromNumber(current);
      address = letter + (row + 1);
      if (worksheet._merges[address] && worksheet._merges[address].model.left === current + 1) {
        current = worksheet._merges[address].model.right;
      } else {
        current += 1;
      }
    }
    return { address, current };
  }

  // Replaces all cell references in a formula with updated references, shifted for repeating sections
  static replaceFormulaCellReferences(cells, direction, formula, formulaCoords, updatedCoords) {
    const { row, col } = formulaCoords;
    let newFormula = formula;
    for (let index = 0; index < cells.length; index++) {
      const details = cells[index];
      if (direction === 'Vertical' && details.row === row + 1) {
        // Move the reference to the new row
        const newCol = getColumnLettersFromNumber(details.col - 1);
        const newCellRef = `${newCol}${updatedCoords.row}`;
        newFormula = newFormula.replace(details.address, newCellRef);
      } else if (details.col === col + 1) {
        // Move the reference to the new column
        const newCol = getColumnLettersFromNumber(updatedCoords.col - 1);
        const newCellRef = `${newCol}${details.row}`;
        newFormula = newFormula.replace(details.address, newCellRef);
      }
    }
    return newFormula;
  }

  // Replaces all token instances with their corresponding values from the data
  static handleEach(worksheet, rowID, target, data) {
    // Get the data to merge with
    let targetDotPath = target.toLowerCase();
    if (targetDotPath === 'characteristics' && !data.characteristics) {
      targetDotPath = 'feature';
    }
    // TODO: Remove this once we have all models using "feature" instead of "item"
    if (targetDotPath === 'feature' && !data.feature) {
      targetDotPath = 'item';
    }

    const items = this.getDataAtDotPath(targetDotPath, data);

    // Get the row template
    const rowTemplate = worksheet.getRow(rowID + 1);

    // Preserve value and styles of the row template
    const cellStyles = {};
    rowTemplate.eachCell({ includeEmpty: true }, (cellTemplate, ci) => {
      cellStyles[ci] = {
        font: cellTemplate.style.font,
        alignment: cellTemplate.style.alignment,
        border: cellTemplate.style.border,
        fill: cellTemplate.style.fill,
        value: cellTemplate.value,
        address: cellTemplate.address,
      };
      // Force IXC font inside of iterators
      if (!cellStyles[ci].font) cellStyles[ci].font = {};
      cellStyles[ci].font.name = 'InspectionXpert GDT';
    });

    // Loop over target data set
    let row;

    // log.debug(`RF - I : handleEach.items.forEach`);
    items.forEach((item, i) => {
      // TODO: Logic for footer here, so it used existing rows until it hits the footer,
      // and then splices in new rows or bumps to a new sheet or whatever we want
      worksheet.spliceRows(rowID + i, 1, []);
      row = worksheet.getRow(rowID + i);

      // Build a new row using each cell in the template
      // log.debug(`RF - J : handleEach.items.forEach.eachCell`);
      for (let ci = 1; ci <= Object.keys(cellStyles).length; ci++) {
        const cell = row.getCell(ci);
        if (!cellStyles[ci].value) {
          cell.value = '';
        } else {
          const tokenData = this.replaceTokens(cellStyles[ci].value, item, targetDotPath);
          if (!tokenData || tokenData === '' || tokenData === ' ') {
            cell.value = tokenData;
          } else {
            const value = formatCellData(tokenData);
            const isNumber = validateNumber(tokenData);
            if (isNumber) {
              const numFmt = getNumberFormat(tokenData);
              cell.numFmt = numFmt;
            }
            cell.value = value;
          }
        }
        cell.font = cellStyles[ci].font;
        cell.alignment = cellStyles[ci].alignment;
        cell.border = cellStyles[ci].border;
        cell.fill = cellStyles[ci].fill;
      }
      row.commit();
    });
  }

  // helper function to return the data category and property for a token
  static getTokenDataProperty(tokens) {
    const token = tokens[0];
    const dotPath = tokens[2].split('.');
    let category = dotPath[0].toLowerCase();
    const property = dotPath[1].toLowerCase();
    // TODO: This is a temporary fix required when we changed "items" to "features".
    if (category === 'feature') category = 'item';
    return { token, category, property };
  }

  // replace all tokens in a cell
  // TODO: this function replaces the replaceTokens function above to no longer require a pre-defined category for a section
  // legacy function should be tested for references and removed when possible
  static replaceTokensInCell(tokens, data, value, cellIndex) {
    let newValue = value;
    let stopIterating = false;
    let replaceWith;
    tokens.forEach((tokenObject, index) => {
      if (tokenObject[2]) {
        const { token, category, property } = this.getTokenDataProperty(tokenObject);
        replaceWith = this.getDataAtDotPath(property, data[category] && data[category][cellIndex]);
        // Replace the token with the data or an indicator that the data wasn't found
        if (!replaceWith && replaceWith !== '') replaceWith = ``;
        newValue = newValue.replace(token, replaceWith);
        if (data[category] && index >= data[category].length && index === tokens.length - 1) {
          stopIterating = true;
        }
      }
    });
    return { newValue, stopIterating };
  }

  // helper function to set the value of a cell, providing a number format so that numeric text can be parsed by functions
  static setCellValue(cell, newValue) {
    if (!newValue || newValue === '' || newValue === ' ') {
      cell.value = newValue || '';
    } else {
      const value = formatCellData(newValue);
      const isNumber = validateNumber(newValue);
      if (isNumber) {
        const numFmt = getNumberFormat(newValue);
        // Sometimes accessing numFmt directly throws a read only error
        const style = { ...cell.style };
        style.numFmt = numFmt;
        cell.style = style;
      }
      if (!cell.style) {
        cell.style = { font: {} };
      }
      if (!cell.style.font) {
        cell.style.font = {};
      }
      cell.style.font.name = 'InspectionXpert GDT';
      cell.value = value;
    }
  }

  /**
   * Replace tokens for each cell in repeating section with associated data
   *
   * @param {Worksheet} worksheet - Current worksheet of the excel workbook
   * @param {Array} tokens - Array of tokens in the cell.value identified by findAll
   * @param {Object} details - Footer details for the cell as defined in settings.footers
   * @param {String} direction - Repeat direction for the template (Vertical or Horizontal)
   * @param {Array} data - Part data to populate the template
   *
   */
  // TODO: this function replaces handleEach to no longer require a separate operator token for repeating sections
  // legacy function should be tested for references and removed when possible
  static handleDynamic(worksheet, tokens, details, direction, data) {
    let stopIterating = false;
    const { row, col, length } = details;
    let current = direction === 'Vertical' ? row : col;
    // Loop through the repeating section
    for (let i = 0; i < length; i++) {
      if (stopIterating) {
        // More cells in section than data, so no need to keep iterating
        log.debug('end of data');
        break;
      }
      const reference = this.getCellReference(worksheet, direction, current, col, row);
      current = reference.current;
      const cell = worksheet.getCell(reference.address);
      // Replace each token in the cell
      const replaced = this.replaceTokensInCell(tokens, data, tokens[0].input, i);
      stopIterating = replaced.stopIterating;

      // Set cell.value with replaced string
      this.setCellValue(cell, replaced.newValue);
    }
    return worksheet;
  }

  /**
   * Adjust and add formulas in repeating sections with proper cell references
   *
   * @param {Worksheet} worksheet - Current worksheet of the excel workbook
   * @param {String} formula - Excel formula
   * @param {Number} length - Length of the dynamic section
   * @param {String} direction - Repeat direction for the template (Vertical or Horizontal)
   */
  static handleFormulas(worksheet, formula, length, direction) {
    const stopIterating = false;
    const { row, col, value } = formula;
    let current = direction === 'Vertical' ? row : col;
    // Loop through the repeating section
    for (let i = 0; i < length; i++) {
      if (stopIterating) {
        // More cells in section than data, so no need to keep iterating
        log.debug('end of data');
        break;
      }

      // Get the current cell, skipping for merges
      const reference = this.getCellReference(worksheet, direction, current, col, row);
      current = reference.current;
      const cell = worksheet.getCell(reference.address);
      const newValue = { value: null };

      // get each cell reference in the formula
      const cells = getCellsFromFormula(value.formula);

      // Loop over cell references in the formula and replace them
      newValue.formula = this.replaceFormulaCellReferences(cells, direction, value.formula, { row, col }, { row: cell.row, col: cell.col });

      // Set cell.value with replaced string
      cell.value = newValue;
    }
    return worksheet;
  }

  // helper function to determine if more rows or columns need to be added to a dynamic section
  static countPotentialInserts(tokens, data, sectionLength) {
    let additional = 0;
    for (let t = 0; t < tokens.length; t++) {
      const { category } = this.getTokenDataProperty(tokens[t]);
      if (data[category] && data[category].length > sectionLength && data[category].length - sectionLength > additional) {
        additional = data[category].length - sectionLength;
      }
    }
    return additional;
  }

  // helper function to map the size of the template and the repeating section in preparation for inserting rows or columns
  static repeatingTemplateSize(worksheet, cell, tokens, stop, direction) {
    const template = {
      endRow: cell.row,
      endCol: cell.col - 1,
      colWidths: [],
      rowHeights: [],
      sectionLength: 0,
      sectionWidth: 0,
      sectionHeight: 0,
      newRow: [],
      newColumn: [],
      styles: [],
      formulas: [],
    };

    if (direction === 'Vertical') {
      const rowTemplate = worksheet.getRow(cell.row);
      rowTemplate.eachCell({ includeEmpty: true }, (rowCell) => {
        const dummy = { style: { ...rowCell.style }, col: rowCell.col };
        if (worksheet._merges[rowCell.address]) {
          const merge = { ...worksheet._merges[rowCell.address] };
          dummy.merge = { ...merge.model };
          const colSpan = 1 + dummy.merge.right - dummy.merge.left;
          template.colWidths.push(colSpan);
          template.endCol += colSpan;
          if (tokens.includes(rowCell.address)) {
            template.sectionWidth += 1;
          }
        } else if (tokens.includes(rowCell.address)) {
          template.sectionWidth += 1;
          template.endCol += 1;
          template.colWidths.push(1);
        }
        template.styles.push(dummy);
        template.newRow.push('');
        if (rowCell.value?.formula || rowCell.formula) {
          template.formulas.push({
            row: rowCell.row - 1,
            col: rowCell.col - 1,
            value: rowCell.value,
          });
        }
      });
      const colTemplate = worksheet.getColumn(cell.col);
      colTemplate.eachCell({ includeEmpty: true }, (colCell) => {
        if (colCell.address === colCell.master.address && colCell.row >= cell.row && colCell.row <= stop) {
          template.sectionHeight += 1;
          template.sectionLength += 1;
          if (worksheet._merges[colCell.address]) {
            const merge = worksheet._merges[colCell.master.address].model;
            const rowSpan = 1 + merge.bottom - merge.top;
            template.rowHeights.push(rowSpan);
            template.endRow += rowSpan;
          } else {
            template.rowHeights.push(1);
            template.endRow += 1;
          }
        }
      });
    } else {
      const rowTemplate = worksheet.getRow(cell.row);
      rowTemplate.eachCell({ includeEmpty: true }, (rowCell) => {
        if (rowCell.address === rowCell.master.address && rowCell.col >= cell.col && rowCell.col <= stop) {
          template.sectionWidth += 1;
          template.sectionLength += 1;
          if (worksheet._merges[rowCell.address]) {
            const merge = worksheet._merges[rowCell.master.address].model;
            const colSpan = 1 + merge.right - merge.left;
            template.colWidths.push(colSpan);
            template.endCol += colSpan;
          } else {
            template.colWidths.push(1);
            template.endCol += 1;
          }
        }
      });
      const colTemplate = worksheet.getColumn(cell.col);
      template.columnWidth = colTemplate.width;
      colTemplate.eachCell({ includeEmpty: true }, (colCell) => {
        const dummy = { style: { ...colCell.style }, row: colCell.row };
        if (worksheet._merges[colCell.master.address]) {
          const merge = { ...worksheet._merges[colCell.master.address] };
          dummy.merge = { ...merge.model };
          const rowSpan = 1 + dummy.merge.bottom - dummy.merge.top;
          template.rowHeights.push(rowSpan);
          template.endRow += rowSpan;
          if (tokens.includes(colCell.address)) {
            template.sectionHeight += 1;
          }
        } else if (tokens.includes(colCell.address)) {
          template.sectionHeight += 1;
          template.rowHeights.push(1);
          template.endRow += 1;
        }
        template.styles.push(dummy);
        template.newColumn.push('');
        if (colCell.value?.formula || colCell.formula) {
          template.formulas.push({
            row: colCell.row - 1,
            col: colCell.col - 1,
            value: colCell.value,
          });
        }
      });
    }

    return template;
  }

  // helper function to create adjusted merge array when inserting rows or columns
  static adjustMergesForInserts(worksheet, position, expanded, tokens, cell, direction) {
    log.debug('fixing merges....');
    const newMerges = [];
    const skip = [];
    if (direction === 'Vertical') {
      // Shift merges to account for new rows
      Object.keys(worksheet._merges).forEach((key) => {
        const merge = { ...worksheet._merges[key].model };
        if (merge.top >= position) {
          // Move merges from after the insertion point
          merge.top += expanded;
          merge.bottom += expanded;
          const newCol = getColumnLettersFromNumber(merge.left - 1);
          const endCol = getColumnLettersFromNumber(merge.right - 1);
          newMerges.push({ start: `${newCol}${merge.top}`, end: `${endCol}${merge.bottom}` });
        } else if (merge.top <= cell.row && merge.bottom >= position && !tokens.includes(key)) {
          // Expand merges that cross the insertion point
          merge.bottom += expanded;
          let col = merge.left;
          while (col <= merge.right) {
            skip.push(col);
            col += 1;
          }
          const endCol = getColumnLettersFromNumber(merge.right - 1);
          newMerges.push({ start: key, end: `${endCol}${merge.bottom}` });
        } else {
          // Copy over uneffected merges
          const endCol = getColumnLettersFromNumber(merge.right - 1);
          newMerges.push({ start: key, end: `${endCol}${merge.bottom}` });
        }
        worksheet.unMergeCells(key);
      });
    } else {
      Object.keys(worksheet._merges).forEach((key) => {
        const merge = { ...worksheet._merges[key].model };
        if (merge.left >= position + 1) {
          // Move merges from after the insertion point
          merge.left += expanded;
          merge.right += expanded;
          const newCol = getColumnLettersFromNumber(merge.left - 1);
          const endCol = getColumnLettersFromNumber(merge.right - 1);
          newMerges.push({ start: `${newCol}${merge.top}`, end: `${endCol}${merge.bottom}` });
        } else if (((merge.left <= position + 1 && merge.right > position + 1) || (merge.left < position + 1 && merge.right >= position + 1)) && !tokens.includes(key)) {
          // Expand merges that cross the insertion point
          merge.right += expanded;
          let row = merge.top;
          while (row <= merge.bottom) {
            skip.push(row);
            row += 1;
          }
          const endCol = getColumnLettersFromNumber(merge.right - 1);
          newMerges.push({ start: key, end: `${endCol}${merge.bottom}` });
        } else {
          // Copy over unaffected merges
          const endCol = getColumnLettersFromNumber(merge.right - 1);
          newMerges.push({ start: key, end: `${endCol}${merge.bottom}` });
        }
        // remove old merge
        worksheet.unMergeCells(key);
      });
    }
    return { skip, newMerges };
  }

  // helper function to insert rows and set their styles
  static insertRows(template, worksheet, position, skip, merges) {
    log.debug('inserting rows...');
    const newMerges = [...merges];
    // Insert enough new rows to fit our data after merging
    for (let i = 0; i < template.rowHeights[0]; i++) {
      worksheet.spliceRows(position + i, 0, template.newRow);
      // set styles for merged rows
      for (let colCounter = 0; colCounter < template.styles.length; colCounter++) {
        const { col, style } = template.styles[colCounter];
        if (style) {
          const tempCol = getColumnLettersFromNumber(col - 1);
          const insertCell = worksheet.getCell(`${tempCol}${position + i}`);
          insertCell.style = { ...style };
        }
      }
    }
    // Handle merges and styles now that the rows have shifted
    for (let colCounter = 0; colCounter < template.styles.length; colCounter++) {
      const { merge, col, style } = template.styles[colCounter];
      if (merge && !skip.includes(col)) {
        // new merge
        const startCol = getColumnLettersFromNumber(merge.left - 1);
        const endCol = getColumnLettersFromNumber(merge.right - 1);
        newMerges.push({ start: `${startCol}${position}`, end: `${endCol}${position + template.rowHeights[0] - 1}` });
      }
      // assign styles to new cells
      if (style) {
        const startCol = getColumnLettersFromNumber(col - 1);
        const insertCell = worksheet.getCell(`${startCol}${position}`);
        insertCell.style = { ...style };
      }
    }

    return newMerges;
  }

  // helper function to insert columns and set their styles
  static insertColumns(template, worksheet, position, startCol, skip, merges) {
    log.debug('inserting columns...');
    const newMerges = [...merges];
    for (let i = 0; i < template.colWidths[0]; i++) {
      worksheet.spliceColumns(position + i + 1, 0, template.newColumn);
      const newColumn = worksheet.getColumn(position + i + 1);
      newColumn.width = template.columnWidth;
      // set styles for merged columns
      const tempCol = getColumnLettersFromNumber(position + i);
      for (let rowCounter = 0; rowCounter < template.styles.length; rowCounter++) {
        const { row, style } = template.styles[rowCounter];
        if (style) {
          const insertCell = worksheet.getCell(`${tempCol}${row}`);
          insertCell.style = { ...style };
        }
      }
    }
    // Handle merges and styles now that the columns have shifted
    for (let rowCounter = 0; rowCounter < template.styles.length; rowCounter++) {
      const { merge, row, style } = template.styles[rowCounter];
      if (merge && !skip.includes(row)) {
        const endCol = getColumnLettersFromNumber(position + template.colWidths[0] - 1);
        newMerges.push({ start: `${startCol}${merge.top}`, end: `${endCol}${merge.bottom}` });
      }
      // assign styles to new cells
      if (style) {
        const insertCell = worksheet.getCell(`${startCol}${row}`);
        insertCell.style = { ...style };
      }
    }
    return newMerges;
  }

  /**
   * Insert rows in a vertical repeating section to fit all data and adjust merges accordingly
   *
   * @param {Worksheet} worksheet - Current worksheet of the excel workbook
   * @param {Cell} cell - Worksheet cell containing the repeating token
   * @param {Number} footer - Footer row for the dynamic section
   * @param {Array} tokens - Addresses of all cells with dynamic tokens
   * @param {Array} data - Part data to populate the template
   */
  static handleVerticalInsert(worksheet, cell, footer, tokens, data) {
    let stop = footer + 1;
    if (footer === -1) {
      // no footer set
      stop = cell.row;
    }

    // Determine size of section and template for inserting new rows
    const template = this.repeatingTemplateSize(worksheet, cell, tokens, stop, 'Vertical');

    // Determine how many, if any, rows to insert
    const cellTokens = this.findTokens(cell.value ? cell.value.toString().trim() : cell.value);
    const additional = this.countPotentialInserts(cellTokens, data, template.sectionLength);

    // Insert new rows and style them, if necessary
    if (additional) {
      let position = template.endRow;
      const expanded = additional * template.rowHeights[0];
      const adjusted = this.adjustMergesForInserts(worksheet, position, expanded, tokens, cell, 'Vertical');
      let { newMerges } = adjusted;
      const { skip } = adjusted;
      // Insert and style rows
      for (let y = 0; y < additional; y++) {
        newMerges = this.insertRows(template, worksheet, position, skip, newMerges);
        position += template.rowHeights[0];
      }
      // Assign adjusted merges
      newMerges.forEach((merge) => {
        worksheet.mergeCells(merge.start, merge.end);
      });
      log.debug('new merges assigned for vertical repeat');
      template.sectionLength += additional;
    }

    // Return section info for token replacement
    return template;
  }

  /**
   * Insert columns in a horizontal repeating section to fit all data and adjust merged accordingly
   *
   * @param {Worksheet} worksheet - Current worksheet of the excel workbook
   * @param {Cell} cell- Worksheet cell containing the repeating token
   * @param {Number} footer - Footer row for the dynamic section
   * @param {Array} tokens- Addresses of all cells with dynamic tokens
   * @param {Array} data - Part data to populate the template
   */
  static handleHorizontalInsert(worksheet, cell, footer, tokens, data) {
    let stop = footer + 1;
    if (footer === -1) {
      // no footer set
      stop = cell.col;
    }

    // Determine size of section and template for inserting new columns
    const template = this.repeatingTemplateSize(worksheet, cell, tokens, stop, 'Horizontal');
    // Determine how many, if any, columns to insert
    const cellTokens = this.findTokens(cell.value ? cell.value.toString().trim() : cell.value);
    const additional = this.countPotentialInserts(cellTokens, data, template.sectionLength);

    // Insert new columns and style them, if necessary
    if (additional) {
      let position = template.endCol;
      const expanded = additional * template.colWidths[0];
      // Shift merges to account for new columns
      const adjusted = this.adjustMergesForInserts(worksheet, position, expanded, tokens, cell, 'Horizontal');
      let { newMerges } = adjusted;
      const { skip } = adjusted;
      // Insert and style columns
      for (let y = 0; y < additional; y++) {
        const startCol = getColumnLettersFromNumber(position);
        // Insert enough new columns to fit our data after merging
        newMerges = this.insertColumns(template, worksheet, position, startCol, skip, newMerges);
        position += template.colWidths[0];
      }
      // Assign adjusted merges
      newMerges.forEach((merge) => {
        worksheet.unMergeCells(merge.start); // catch any merged cells that magically got missed
        worksheet.mergeCells(merge.start, merge.end);
      });
      log.debug('new merges assigned for horizontal repeat');
      template.sectionLength += additional;
    }

    // Return section info for token replacement
    return template;
  }

  /**
   * Iterate through cells with repeating tokens and populate the section with data
   *
   * @param {Worksheet} worksheet - Current worksheet of the excel workbook
   * @param {Object} footer - Footer details for the worksheet as defined in settings.footers
   * @param {String} direction - Repeat direction for the template (Vertical or Horizontal)
   * @param {Array} data - Part data to populate the template
   */
  static handleFooter(worksheet, footer, direction, data) {
    const tokenedCells = Object.keys(footer);
    if (!tokenedCells[0]) {
      log.warn('No cell keys available to iterate over');
      return;
    }
    const firstCell = worksheet.getCell(tokenedCells[0]);
    const footerCell = parseInt(footer[tokenedCells[0]].footer, 10);

    // Expand section if needed to fit all data
    let template;
    if (direction === 'Vertical') {
      log.debug(`footer set to row ${footerCell}`);
      template = this.handleVerticalInsert(worksheet, firstCell, footerCell, tokenedCells, data);
    } else {
      log.debug(`footer set to column ${footerCell}`);
      template = this.handleHorizontalInsert(worksheet, firstCell, footerCell, tokenedCells, data);
    }

    // Iterate over tokens and populate section with data
    tokenedCells.forEach((cellStart) => {
      const details = footer[cellStart];
      details.row = parseInt(details.row, 10);
      details.col = parseInt(details.col, 10);
      details.footer = parseInt(details.footer, 10);
      details.length = parseInt(template.sectionLength, 10);
      const cell = worksheet.getCell(cellStart);
      if (!cell.value) {
        cell.value = '';
      }
      const tokens = this.findTokens(cell.value ? cell.value.toString().trim() : '');
      if (!tokens || tokens.length === 0) {
        log.warn('no tokens found');
      } else {
        this.handleDynamic(worksheet, tokens, details, direction, data);
      }
    });

    if (template.formulas.length > 0) {
      template.formulas.forEach((formula) => {
        this.handleFormulas(worksheet, formula, template.sectionLength, direction);
      });
    }
  }

  static handleConditionalFormatting(worksheet) {
    let changed = false;
    log.debug(`checking conditional formatting for ${worksheet.name}`);
    worksheet.conditionalFormattings.forEach((format, formatIndex) => {
      if (format.rules) {
        const newRules = [];
        // find broken rules
        for (let index = 0; index < format.rules.length; index++) {
          const rule = format.rules[index];
          if ((rule.type === 'expression' || rule.type === 'cellIs') && !rule.formulae) {
            log.warn(`Conditional formatting rule ${index + 1} at ${format.ref} removed due to missing formulae`);
            continue;
          }
          newRules.push(rule);
        }
        // remove broken rules
        if (format.rules.length !== newRules.length) {
          changed = true;
          worksheet.conditionalFormattings[formatIndex] = { ...format, rules: newRules };
        }
      }
    });
    log.debug('Format check complete');
    return changed;
  }

  static writeValidatedConditionalFormatting(workbook) {
    let changed = false;
    workbook.eachSheet((worksheet) => {
      if (worksheet.conditionalFormattings) {
        const changedSheet = this.handleConditionalFormatting(worksheet);
        if (changedSheet && !changed) {
          changed = true;
        }
      }
    });
    return changed;
  }

  // Opens an excel template, replaces all tokens with the provided data
  static async merge(data, templateFilePath, outFilePath) {
    let fp = templateFilePath;
    // Validate path to file
    if (typeof templateFilePath === 'string') {
      fp = path.resolve(templateFilePath);
      if (!fs.existsSync(fp)) {
        log.error(`Unable to find report template at ${fp}`);
        return null;
      }
      log.debug(`Loading report template from ${fp}`);
    } else {
      log.debug('Remote template file loaded and passed in');
    }

    const workbook = new Excel.Workbook();

    // Read xlsx file and wait until its loaded to begin processing it
    await workbook.xlsx.readFile(fp);

    // Iterate over all sheets
    workbook.eachSheet((worksheet) => {
      // Iterate over all rows in sheet
      worksheet.eachRow((row, rowID) => {
        // Iterate over all cells in row
        let operator;
        let newValue;

        // row.eachCell(async (cell, colID) => {
        row.eachCell((cell, colID) => {
          // Check to see if there is an operator
          operator = this.findOperator(cell.value);
          if (operator.action) {
            if (operator.action === '#each') {
              this.handleEach(worksheet, rowID, operator.target, data);
            } else {
              // log.error(`RF - ${colID} ${rowID} Unrecognized template operator: ${operator.action}`);
            }
          } else {
            newValue = this.replaceTokens(cell.value, data, '');
            if (cell.value !== newValue) {
              const value = formatCellData(newValue);
              const isNumber = validateNumber(newValue);
              if (isNumber) {
                const numFmt = getNumberFormat(newValue);
                cell.numFmt = numFmt;
              }
              cell.value = value;
            }
          }
        });
      });
    });

    // After processing all sheets,
    await workbook.xlsx.write(outFilePath);

    // If the merge succeeded, return the path of the generated report
    return outFilePath;
  }

  // Replaces excel template tokens with provided data without overwriting template
  // returns an exceljs workbook
  static async preview(data, workbook, templateSettings, direction) {
    if (!workbook) {
      log.error('no workbook loaded');
      return null;
    }
    if (!data) {
      log.error('no data passed');
      return null;
    }
    log.debug('template loaded');
    let sheetIndex = 0; // sheetID doesn't match index if there are null sheets
    const settings = templateSettings ? parseValidJSON(templateSettings) : { footers: {}, tokens: [], saved: '' };
    // Iterate over all sheets
    workbook.eachSheet((worksheet, sheetID) => {
      log.debug(`parsing sheet ${sheetID}, index ${sheetIndex}`);
      const footer = settings.footers ? settings.footers[sheetIndex] : null;
      if (footer && Object.keys(footer).length > 0) {
        this.handleFooter(worksheet, footer, direction, data);
      }
      // Iterate over all rows in sheet
      worksheet.eachRow((row, rowID) => {
        // log.debug(rowID);
        // Iterate over all cells in row
        let operator;
        let newValue;
        row.eachCell((cell, colID) => {
          if (!footer || !Object.keys(footer).includes(cell.master.address)) {
            // Check to see if there is an operator
            operator = this.findOperator(cell.value);
            if (operator.action) {
              if (operator.action === '#each') {
                // log.debug(`each, ${operator.target}`);
                this.handleEach(worksheet, rowID, operator.target, data);
                // Delete the operator and template rows using spliceRows(rowStart, numRowsToRemove)
              } else {
                log.error(`RF - ${colID} ${rowID} Unrecognized template operator: ${operator.action}`);
              }
            } else {
              newValue = this.replaceTokens(cell.value, data, '');
              if (cell.value !== newValue && newValue.length > 0) {
                const value = formatCellData(newValue);
                const isNumber = validateNumber(newValue);
                if (isNumber) {
                  const numFmt = getNumberFormat(newValue);
                  cell.numFmt = numFmt;
                }
                cell.value = value;
              }
            }
          }
        });
      });

      if (worksheet.conditionalFormattings) {
        this.handleConditionalFormatting(worksheet);
      }

      sheetIndex += 1;
    });
    log.debug('returning report');
    return workbook;
  }

  // Overwrites specified cells in workbook with new data
  // returns an exceljs workbook
  static async update(data, workbook) {
    if (!workbook) {
      log.error('no workbook loaded');
      return false;
    }

    for (let i = 0; i < data.length; i++) {
      const change = data[i];
      const sheet = workbook.getWorksheet(change.sheet);
      const cell = sheet.getCell(change.address);
      cell.value = change.data;
    }

    return workbook;
  }

  // Converts framed characters to unframed characters
  // returns an exceljs workbook
  static async unFrame(workbook) {
    if (!workbook) {
      log.error('no workbook loaded');
      return false;
    }

    workbook.eachSheet((worksheet) => {
      worksheet.eachRow((row) => {
        row.eachCell((cell) => {
          if (cell && cell.value && cell.value !== null) {
            const meta = unFrameText(cell.value.toString());
            cell.value = meta;
          }
        });
      });
    });

    return workbook;
  }
}

module.exports = ReportFactory;
