const Excel = require('exceljs');
const fs = require('fs');
const { defaultOperationIDs, defaultMethodIDs } = require('../staticData/defaults');
const ReportFactory = require('./reportFactory');
const { formatNumericData, getFormulaResult } = require('./shared/exceljs/dataValidation');
const { readFile } = require('./shared/exceljs/fileUtilities');
const TemplateFactory = require('./templateFactory');

const mergeData = {
  customer: {
    name: 'Fletch F. Fletcher',
  },
  part: {
    name: 'Fetzer Valve',
    number: '20200101-0001',
    revision: 'B',
  },
  drawing: {
    control: "Boyd Aviation assembly specifications for Mr. Stanwyck's plane.",
    name: 'Fetzer Valve Assembly Detail',
    number: '20200101-0001-B-3',
    revision: '3',
  },
  item: [
    {
      label: '1',
      type: 'Note',
      ocr: 'ANODIZE PER GH52-1',
      nominal: '',
      plus: '',
      minus: '',
      usl: '',
      lsl: '',
      unit: '',
      classification: '',
      method: '',
      operation: null,
      comment: 'LOWER-PLATE-C4 pg.1, Zone 1.A',
      verification: '',
    },
    {
      label: '2',
      type: 'Note',
      ocr: 'BREAK ALL SHARP EDGES TO .02',
      nominal: '',
      plus: '',
      minus: '',
      usl: ' ',
      lsl: ' ',
      unit: ' ',
      classification: '',
      method: '',
      operation: null,
      comment: 'LOWER-PLATE-C4 pg.1, Zone 1.A',
      verification: '',
    },
    {
      label: '3',
      type: 'Note',
      ocr: 'INTERPRET PER ASME Y14.5',
      nominal: '',
      plus: '',
      minus: '',
      usl: ' ',
      lsl: ' ',
      unit: ' ',
      classification: '',
      method: '',
      operation: null,
      comment: 'LOWER-PLATE-C4 pg.1, Zone 1.A',
      verification: '',
    },
    {
      label: '4',
      type: 'Length',
      ocr: '.325±.020',
      nominal: '0.325',
      plus: '0.02',
      minus: '0.02',
      usl: '0.345',
      lsl: '0.305',
      unit: '',
      classification: '',
      method: 'CMM',
      operation: null,
      comment: 'LOWER-PLATE-C4 pg.1, Zone 2.A',
      verification: '',
    },
    {
      label: '5',
      type: 'Length',
      ocr: '.618',
      nominal: '.618',
      plus: '0.02',
      minus: '0.02',
      usl: '0.638',
      lsl: '0.598',
      unit: 'in',
      classification: 'Basic',
      method: 'CMM',
      operation: null,
      comment: 'LOWER-PLATE-C4 pg.1, Zone 2.A',
      verification: '',
    },
    {
      label: '6',
      type: 'Length',
      ocr: '.680',
      nominal: '.680',
      plus: '0.0199999999999999',
      minus: '0.02',
      usl: '0.7',
      lsl: '0.66',
      unit: 'in',
      classification: 'Basic',
      method: 'CMM',
      operation: null,
      comment: 'LOWER-PLATE-C4 pg.1, Zone 2.A',
      verification: '',
    },
    {
      label: '7',
      type: 'Length',
      ocr: '.750',
      nominal: '.750',
      plus: '0.02',
      minus: '0.02',
      usl: '0.77',
      lsl: '0.73',
      unit: 'in',
      classification: 'Basic',
      method: 'CMM',
      operation: null,
      comment: 'LOWER-PLATE-C4 pg.1, Zone 3.B',
      verification: '',
    },
    {
      label: '8',
      type: 'Profile of a Surface',
      ocr: '',
      full_spec: '',
      nominal: '',
      plus: '',
      minus: '',
      usl: '0.01',
      lsl: '-0.01',
      unit: 'in',
      classification: '',
      method: 'CMM',
      operation: null,
      comment: 'LOWER-PLATE-C4 pg.1, Zone 2.C',
      verification: '',
    },
    {
      label: '9.1',
      type: 'Radius',
      ocr: 'R.125',
      nominal: '.125',
      plus: '0.005',
      minus: '0.005',
      usl: '0.13',
      lsl: '0.12',
      unit: 'in',
      classification: '',
      method: 'CMM',
      operation: defaultOperationIDs.Drilling, // Drilling
      comment: 'LOWER-PLATE-C4 pg.1, Zone 3.D',
      verification: '',
    },
    {
      label: '9.2',
      type: 'Radius',
      ocr: 'R.125',
      nominal: '.125',
      plus: '0.005',
      minus: '0.005',
      usl: '0.13',
      lsl: '0.12',
      unit: 'in',
      classification: 'Reference',
      method: 'CMM',
      operation: defaultOperationIDs.Drilling, // Drilling
      comment: 'LOWER-PLATE-C4 pg.1, Zone 3.D',
      verification: '',
    },
    {
      label: '10',
      type: 'Perpendicularity',
      ocr: '',
      nominal: '',
      plus: '',
      minus: '',
      usl: '0.01',
      lsl: '',
      unit: 'in',
      classification: '',
      method: 'CMM',
      operation: null,
      comment: 'LOWER-PLATE-C4 pg.1, Zone 3.D',
      verification: '',
    },
    {
      label: '11',
      type: 'Flatness',
      full_spec: '',
      nominal: '',
      plus: '',
      minus: '',
      usl: '0.002',
      lsl: '',
      unit: 'in',
      classification: '',
      method: 'CMM',
      operation: null,
      comment: 'LOWER-PLATE-C4 pg.1, Zone 3.E',
      verification: '',
    },
    {
      label: '12',
      type: 'Length',
      full_spec: '.250',
      nominal: '.250',
      plus: '0.02',
      minus: '0.02',
      usl: '0.27',
      lsl: '0.23',
      unit: 'in',
      classification: '',
      method: 'CMM',
      operation: null,
      comment: 'LOWER-PLATE-C4 pg.1, Zone 4.E',
      verification: '',
    },
  ],
};
const dynamicSettingsData = `{
  "saved": true, 
  "footers":{
    "0":{
      "B12":{
        "row":"11", "col":"1", "footer":"12"
      }, 
      "C12":{
        "row":"11", "col":"2", "footer":"12"
      }, 
      "D12":{
        "row":"11", "col":"3", "footer":"12"
      }, 
      "E12":{
        "row":"11", "col":"4", "footer":"12"
      },
      "F12":{
        "row":"11", "col":"5", "footer":"12"
      }, 
      "G12":{
        "row":"11", "col":"6", "footer":"12"
      }, 
      "H12":{
        "row":"11", "col":"7", "footer":"12"
      }, 
      "I12":{
        "row":"11", "col":"8", "footer":"12"
      }, 
      "J12":{
        "row":"11", "col":"9", "footer":"12"
      }, 
      "K12":{
        "row":"11", "col":"10", "footer":"12"
      }, 
      "L12":{
        "row":"11", "col":"11", "footer":"12"
      }, 
      "M12":{
        "row":"11", "col":"12", "footer":"12"
      }, 
      "N12":{
        "row":"11", "col":"13", "footer":"12"
      }
    }
  }
}`;
const allDataSettings = `{"saved": true,"direction":"Vertical", "footers":{"0":{ "B12":{"row":"11", "col":"1", "footer":"36"},"C12":{"row":"11", "col":"2", "footer":"36"},"D12":{"row":"11", "col":"3", "footer":"36"},"E12":{"row":"11", "col":"4", "footer":"36"},"F12":{"row":"11", "col":"5", "footer":"36"},"G12":{"row":"11", "col":"6", "footer":"36"},"H12":{"row":"11", "col":"7", "footer":"36"},"I12":{"row":"11", "col":"8", "footer":"36"},"J12":{"row":"11", "col":"9", "footer":"36"},"K12":{"row":"11", "col":"10", "footer":"36"},"L12":{"row":"11", "col":"11", "footer":"36"},"M12":{"row":"11", "col":"12", "footer":"36"},"N12":{"row":"11", "col":"13", "footer":"36"},"O12":{"row":"11", "col":"14", "footer":"36"},"P12":{"row":"11", "col":"15", "footer":"36"},"Q12":{"row":"11", "col":"16", "footer":"36"},"R12":{"row":"11", "col":"17", "footer":"36"},"S12":{"row":"11", "col":"18", "footer":"36"},"T12":{"row":"11", "col":"19", "footer":"36"},"U12":{"row":"11", "col":"20", "footer":"36"},"V12":{"row":"11", "col":"21", "footer":"36"}}}}`;

const templateFilePath = 'src/templates/All Data.xlsx';
const dynamicTemplate = 'data/5350aeaf-31ab-414d-8ac5-450d8a49df16.xlsx';

const as912bSettings =
  '{"saved": true, "direction":"Vertical", "footers":{"2":{"B11":{"row":"10","col":"1","footer":"25"}, "C11":{"row":"10","col":"2","footer":"25"}, "E11":{"row":"10","col":"4","footer":"25"}, "I11":{"row":"10","col":"8","footer":"25"}}}}';
const as912bTemplate = 'src/templates/AS9102B.xlsx';

const mergeTestVertical = {
  G8: { row: 7, col: 6, footer: 9 },
  I8: { row: 7, col: 8, footer: 9 },
};
const mergeTestHorizontal = {
  C7: { row: 6, col: 2, footer: 4 },
  C9: { row: 8, col: 2, footer: 4 },
};
const mergeTestFile = 'src/services/shared/testFiles/MergeTest.xlsx';

describe('ReportFactory', () => {
  describe('findTokens', () => {
    it('Should identify tokens in text', () => {
      const tokens = ReportFactory.findTokens('My name is {{Customer.Name}} the third.');
      expect(tokens.length).toBe(1);
      expect(tokens[0][0]).toBe('{{Customer.Name}}');
      expect(tokens[0][1]).toBe(undefined);
      expect(tokens[0][2]).toBe('Customer.Name');
    });
    it('Should extract multiple tokens in text', () => {
      const tokens = ReportFactory.findTokens('My name is {{Customer.FirstName}} the {{Customer.LastName}}.');
      expect(tokens.length).toBe(2);
      expect(tokens[0][0]).toBe('{{Customer.FirstName}}');
      expect(tokens[0][1]).toBe(undefined);
      expect(tokens[0][2]).toBe('Customer.FirstName');
      expect(tokens[1][0]).toBe('{{Customer.LastName}}');
      expect(tokens[1][1]).toBe(undefined);
      expect(tokens[1][2]).toBe('Customer.LastName');
    });
  });

  describe('findOperators', () => {
    it('Should identify operators in text', () => {
      const operator = ReportFactory.findOperator('{{#each characteristics}}');
      expect(operator.action).toBe('#each');
      expect(operator.target).toBe('characteristics');
    });
  });

  describe('replaceTokens', () => {
    const dummyData = {
      obj: { prop: 'value' },
      arr: ['a', 'b', 'c'],
    };
    it('replaces all tokens', () => {
      const result = ReportFactory.replaceTokens('Data: {{obj.prop}}.', dummyData);
      expect(result).toBe('Data: value.');
    });
    it('does not replace operators', () => {
      const result = ReportFactory.replaceTokens('Data: {{#each arr}}', dummyData);
      expect(result).toBe('Data: {{#each arr}}');
    });
  });

  describe('getDataAtDotPath', () => {
    const dummyData = {
      obj: { prop: 'value' },
      arr: ['a', 'b', 'c'],
      nestArr: { arr: ['d', 'e', 'f'] },
      items: [
        { id: '1', prop: 'val' },
        { id: '2', prop: 'val' },
        { id: '3', prop: 'val' },
      ],
    };
    it('gets nested data based on token dot division', () => {
      const prop = ReportFactory.getDataAtDotPath('obj.prop', dummyData);
      expect(prop).toBe('value');
    });
    it('concats string array values into a single string', () => {
      const arr1 = ReportFactory.getDataAtDotPath('arr', dummyData);
      const arr2 = ReportFactory.getDataAtDotPath('nestArr.arr', dummyData);
      expect(arr1).toBe('a, b, c');
      expect(arr2).toBe('d, e, f');
    });
    it('does not concat object arrays', () => {
      const arr1 = ReportFactory.getDataAtDotPath('items', dummyData);
      expect(arr1).toMatchObject(dummyData.items);
    });
  });

  describe('Significant Digits', () => {
    it('Should maintain significant digits and numeric type for numbers and original data for non-numbers', async () => {
      const templatePath = 'src/templates/All Data.xlsx';

      const workbook = new Excel.Workbook();
      await workbook.xlsx.readFile(templatePath);
      const output = await ReportFactory.preview(mergeData, workbook, allDataSettings, 'Vertical');
      const worksheet = output.getWorksheet('Sheet1');

      const note = worksheet.getCell('U13'); // the 2nd characteristic Comments
      expect(note.value).toBe('LOWER-PLATE-C4 pg.1, Zone 1.A');
      expect(note.type).toBe(Excel.ValueType.String);

      const nominal = worksheet.getCell('L15'); // the 4th characteristic Nominal
      expect(nominal.value).toBe(0.325);
      expect(nominal.numFmt).toBe('?.???');
      expect(nominal.type).toBe(Excel.ValueType.Number);
      const formatted = formatNumericData(nominal.value, nominal.numFmt);
      expect(formatted).toBe('0.325');

      const nominal2 = worksheet.getCell('L16'); // the 5th characteristic Nominal
      expect(nominal2.value).toBe(0.618);
      expect(nominal2.numFmt).toBe('#.???');
      expect(nominal2.type).toBe(Excel.ValueType.Number);
      const formatted2 = formatNumericData(nominal2.value, nominal2.numFmt);
      expect(formatted2).toBe('.618');

      const partName = worksheet.getCell('B5'); // the part name
      expect(partName.value).toBe('  Fetzer Valve');
      expect(partName.type).toBe(Excel.ValueType.String);
    });

    it('should parse formulas and update the results based on the populated data', async () => {
      // We could write a fresh formula sheet for this test, but processing an xlsx saved in another program seemed a closer test to production
      const templatePath = 'src/templates/fai-report-template-formulas.xlsx';
      const file = 'formulaTest.xlsx';

      const workbook = new Excel.Workbook();
      await workbook.xlsx.readFile(templatePath);
      const output = await ReportFactory.preview(mergeData, workbook);
      await output.xlsx.writeFile(file);
      const workbook2 = new Excel.Workbook();
      await workbook2.xlsx.readFile(file);
      const worksheet = workbook2.getWorksheet('Sheet1');

      const B3 = worksheet.getCell('B3');
      const B5 = worksheet.getCell('B5');
      const I5 = worksheet.getCell('I5');
      const B7 = worksheet.getCell('B7');

      const formulaCell = worksheet.getCell('P3'); // B3
      // Formulas come with a result but exceljs can't compute them for us. An xlsx program will do this step automatically for the user
      const result = getFormulaResult(formulaCell.value.formula, worksheet);
      expect(result).toBe(B3.value);

      const formulaCell2 = worksheet.getCell('P5'); // B5&I5
      const result2 = getFormulaResult(formulaCell2.value.formula, worksheet);
      expect(result2).toBe(`${B5.value}${I5.value}`);

      const formulaCell3 = worksheet.getCell('P7'); // B7&" No."
      const result3 = getFormulaResult(formulaCell3.value.formula, worksheet);
      expect(result3).toBe(`${B7.value} No.`);

      fs.unlinkSync(file);
    });

    it('preserves conditional formatting rules', async () => {
      const templatePath = 'src/templates/fai-report-template-formulas.xlsx';
      const file = 'formatting.xlsx';

      const workbook = new Excel.Workbook();
      await workbook.xlsx.readFile(templatePath);
      const worksheet = workbook.getWorksheet('Sheet1');
      const originalFormatting = worksheet.conditionalFormattings;
      const output = await ReportFactory.preview(mergeData, workbook);
      await output.xlsx.writeFile(file);
      const workbook2 = new Excel.Workbook();
      await workbook2.xlsx.readFile(file);
      const worksheet2 = workbook2.getWorksheet('Sheet1');
      expect(worksheet2.conditionalFormattings[0]).toEqual(
        expect.objectContaining({
          ref: originalFormatting[0].ref,
          rules: expect.arrayContaining([
            expect.objectContaining({
              type: originalFormatting[0].rules[0].type,
              operator: originalFormatting[0].rules[0].operator,
              priority: originalFormatting[0].rules[0].priority,
              timePeriod: originalFormatting[0].rules[0].timePeriod,
              percent: originalFormatting[0].rules[0].percent,
              bottom: originalFormatting[0].rules[0].bottom,
              rank: originalFormatting[0].rules[0].rank,
              aboveAverage: originalFormatting[0].rules[0].aboveAverage,
              style: originalFormatting[0].rules[0].style,
              formulae: originalFormatting[0].rules[0].formulae,
            }),
          ]),
        })
      );

      fs.unlinkSync(file);
    });

    it('preserves type for empty dynamic cells', async () => {
      const templatePath = 'src/services/shared/testFiles/OperationCheck.xlsx';
      const footers = JSON.stringify({
        saved: false,
        footers: { 0: { B9: { row: 8, col: 1, footer: 35 }, C9: { row: 8, col: 2, footer: 35 }, D9: { row: 8, col: 3, footer: 35 }, E9: { row: 8, col: 4, footer: 35 }, F9: { row: 8, col: 5, footer: 35 } } },
        tokens: ['{{Part.Number}}', '{{Feature.Label}}', '{{Feature.Operation}}', '{{Feature.Full_Spec}}', '{{Feature.USL}}', '{{Feature.LSL}}'],
      });
      const workbook = new Excel.Workbook();
      await workbook.xlsx.readFile(templatePath);
      const data = {
        ...mergeData,
        item: [
          {
            label: '4',
            type: 'Length',
            full_spec: '.325±.020',
            nominal: '0.325',
            plus: '0.02',
            minus: '0.02',
            usl: '0.345',
            lsl: '0.305',
            unit: '',
            classification: '',
            method: 'CMM',
            operation: null,
            comment: 'LOWER-PLATE-C4 pg.1, Zone 2.A',
            verification: '',
          },
        ],
      };
      const sheet = workbook.getWorksheet('Form3');
      expect(sheet.getCell('D9').value).toBe('  {{Feature.Full_Spec}}');
      expect(sheet.getCell('D12').type).not.toBe(Excel.ValueType.String);

      // Test Report Factory
      const output = await ReportFactory.preview(data, workbook, footers, 'Vertical');
      const worksheet = output.getWorksheet('Form3');

      let D9 = worksheet.getCell('D9');
      expect(D9.value).toBe('.325±.020');
      expect(D9.type).toBe(Excel.ValueType.String);

      let D12 = worksheet.getCell('D12');
      expect(D12.type).toBe(Excel.ValueType.String);

      let D34 = worksheet.getCell('D34');
      expect(D34.type).toBe(Excel.ValueType.String);

      // Test Template Factory
      const template = await TemplateFactory.returnUI(templatePath, output);
      D9 = template[0].rows[8][3];
      expect(D9.data).toBe('.325±.020');
      D12 = template[0].rows[11][3];
      expect(D12.data).toBe('');
      D34 = template[0].rows[33][3];
      expect(D34.data).toBe('');
    });
  });

  describe('.merge', () => {
    it('Should ReportFactory a report', async () => {
      const fileWriteStream = fs.createWriteStream('data/fai-report-final.xlsx');
      let saved = false;
      await ReportFactory.merge(mergeData, templateFilePath, fileWriteStream);
      // Note: Is this the right way to test this?
      fileWriteStream.on('finish', () => {
        saved = true;
        expect(saved).toBeTruthy();
      });
      fileWriteStream.on('error', () => {
        saved = false;
        expect(saved).toBeTruthy();
      });
    });
  });

  describe('preview', () => {
    it('should replace template tokens', async () => {
      const workbook = await readFile(templateFilePath);
      const output = await ReportFactory.preview(mergeData, workbook, allDataSettings, 'Vertical');

      const sheet = output.worksheets[0]; // the first one;
      const customer = sheet.getCell('B3');

      expect(customer.value).toContain(mergeData.customer.name);
    });

    it('should return null if not passed a workbook', async () => {
      const output = await ReportFactory.preview(mergeData, null);
      expect(output).toBe(null);
    });

    it('should repeat dynamic tokens vertically', async () => {
      const workbook = await readFile(dynamicTemplate);
      const startingLength = workbook.worksheets[0].rowCount;
      const output = await ReportFactory.preview(mergeData, workbook, dynamicSettingsData, 'Vertical');
      // adds rows
      expect(startingLength).toBeLessThan(output.worksheets[0].rowCount);

      const sheet = output.worksheets[0]; // the first one;
      const item1 = sheet.getCell('B12');
      const item1OCR = sheet.getCell('D12');
      const item4 = sheet.getCell('B15');
      const item4OCR = sheet.getCell('D15');

      // replaces tokens
      expect(item1.value.toString()).toEqual(mergeData.item[0].label.toString());
      expect(item1OCR.value.toString()).toEqual(mergeData.item[0].ocr.toString());
      expect(item4.value.toString()).toEqual(mergeData.item[3].label.toString());
      expect(item4OCR.value.toString()).toEqual(mergeData.item[3].ocr.toString());
    });

    it('should not repeat fake dynamic tokens', async () => {
      const workbook = await readFile(dynamicTemplate);
      const output = await ReportFactory.preview(mergeData, workbook, dynamicSettingsData, 'Vertical');

      const sheet = output.worksheets[0]; // the first one;
      const fakeToken = sheet.getCell('O12');
      const emptyCell = sheet.getCell('O13');

      expect(fakeToken.value.toString()).toEqual('{{Feature.Verification}}');
      expect(emptyCell.value).toEqual(null);
    });
  });

  describe('update', () => {
    it('update a cell in a report', async () => {
      const workbook = await readFile(templateFilePath);
      const output = await ReportFactory.preview(mergeData, workbook, allDataSettings, 'Vertical');
      const changed = await ReportFactory.update([{ sheet: 'Sheet1', address: 'B3', data: 'SOMETHING NEW' }], output);

      const sheet = changed.worksheets[0]; // the first one;
      const customer = sheet.getCell('B3');

      expect(customer.value).toBe('SOMETHING NEW');
    });
    it('should return null if not passed a workbook', async () => {
      const output = await ReportFactory.preview(mergeData, null);
      expect(output).toBe(null);
    });
  });

  describe('unFrame', () => {
    it('returns the unframed string of a GDT', async () => {
      const workbook = await readFile(as912bTemplate);
      const output = await ReportFactory.preview(mergeData, workbook, as912bSettings, 'Vertical');
      const changed = await ReportFactory.unFrame(output);

      const sheet = changed.getWorksheet('Form3');
      const fullSpec = sheet.getCell('E18'); // The 8th feature

      expect(fullSpec.value).toBe('|⌓|.020|A|B|C|');
    });
    it('does not mutate non framed characters', async () => {
      const workbook = await readFile(as912bTemplate);
      const output = await ReportFactory.preview(mergeData, workbook, as912bSettings, 'Vertical');
      const changed = await ReportFactory.unFrame(output);

      const sheet = changed.getWorksheet('Form3');
      const part = sheet.getCell('F7'); // Part Name

      expect(part.value).toBe('  Fetzer Valve');
    });
  });

  describe('handleDynamic', () => {
    it('populates dynamic data vertically', async () => {
      const workbook = await readFile(mergeTestFile);
      const worksheet = workbook.worksheets[0];
      const details = { ...mergeTestVertical.G8, length: 2, footer: 11 };
      const tokens = [['{{Feature.Label}}', undefined, 'Feature.Label']];
      tokens[0].input = '{{Feature.Label}}';
      const output = ReportFactory.handleDynamic(worksheet, tokens, details, 'Vertical', mergeData);

      const G8 = output.getCell('G8');
      expect(G8.value.toString()).toBe(mergeData.item[0].label);
      const G10 = output.getCell('G10');
      expect(G10.value.toString()).toBe(mergeData.item[1].label);
    });

    it('populates dynamic data horizontally', async () => {
      const workbook = await readFile(mergeTestFile);
      const worksheet = workbook.worksheets[0];
      const details = { ...mergeTestHorizontal.C7, length: 2 };
      const tokens = [['{{Feature.Label}}', undefined, 'Feature.Label']];
      tokens[0].input = '{{Feature.Label}}';
      const output = ReportFactory.handleDynamic(worksheet, tokens, details, 'Horizontal', mergeData);

      const C7 = output.getCell('C7');
      expect(C7.value.toString()).toBe(mergeData.item[0].label.toString());
      const E7 = output.getCell('E7');
      expect(E7.value.toString()).toBe(mergeData.item[1].label.toString());
    });
  });

  describe('handleFormulas', () => {
    it('populates formulas vertically', async () => {
      const workbook = await readFile(mergeTestFile);
      const worksheet = workbook.worksheets[0];
      const formula = { row: 7, col: 10, value: { formula: 'G8+5' } };
      const output = ReportFactory.handleFormulas(worksheet, formula, 2, 'Vertical');

      const K8 = output.getCell('K8');
      expect(K8.value.formula).toBe('G8+5');
      const K10 = output.getCell('K10');
      expect(K10.value.formula).toBe('G10+5');
    });
    it('populates formulas horizontally', async () => {
      const workbook = await readFile(mergeTestFile);
      const worksheet = workbook.worksheets[0];
      const formula = { row: 10, col: 2, value: { formula: 'C7+5' } };
      const output = ReportFactory.handleFormulas(worksheet, formula, 2, 'Horizontal');

      const C11 = output.getCell('C11');
      expect(C11.value.formula).toBe('C7+5');
      const E11 = output.getCell('E11');
      expect(E11.value.formula).toBe('E7+5');
    });
  });

  describe('Handle Insert', () => {
    it('inserts cells vertically', async () => {
      const workbook = await readFile(mergeTestFile);
      const worksheet = workbook.worksheets[0];
      let A14 = worksheet.getCell('A14');
      expect(A14.value).toBe('I NEED TO SPAN THE WHOLE SHEET');

      const G8 = worksheet.getCell('G8');
      const footers = Object.keys(mergeTestVertical);
      ReportFactory.handleVerticalInsert(worksheet, G8, 9, footers, mergeData);

      A14 = worksheet.getCell('A14');
      expect(A14.value).not.toBe('I NEED TO SPAN THE WHOLE SHEET');
      const A36 = worksheet.getCell('A36');
      expect(A36.value).toBe('I NEED TO SPAN THE WHOLE SHEET');
    });

    it('inserts cells horizontally', async () => {
      const workbook = await readFile(mergeTestFile);
      const worksheet = workbook.worksheets[0];
      let G6 = worksheet.getCell('G6');
      expect(G6.value).toBe('Feature Number');

      const C7 = worksheet.getCell('C7');
      const footers = Object.keys(mergeTestHorizontal);
      ReportFactory.handleHorizontalInsert(worksheet, C7, 4, footers, mergeData);

      G6 = worksheet.getCell('G6');
      expect(G6.value).not.toBe('Feature Number');
      const AC6 = worksheet.getCell('AC6');
      expect(AC6.value).toBe('Feature Number');
    });
  });

  describe('handleFooter', () => {
    it('inserts and populates data for a vertical dynamic section', async () => {
      const workbook = await readFile(mergeTestFile);
      const worksheet = workbook.worksheets[0];
      await ReportFactory.handleFooter(worksheet, mergeTestVertical, 'Vertical', mergeData);
      const G8 = worksheet.getCell('G8');
      expect(G8.value.toString()).toBe(mergeData.item[0].label);
      const G10 = worksheet.getCell('G10');
      expect(G10.value.toString()).toBe(mergeData.item[1].label);
      const G30 = worksheet.getCell('G30');
      expect(G30.value.toString()).toBe(mergeData.item[11].label);
      const I8 = worksheet.getCell('I8');
      expect(I8.value.toString()).toBe(''); // No full spec
      const I10 = worksheet.getCell('I10');
      expect(I10.value.toString()).toBe(''); // No full spec
      const I30 = worksheet.getCell('I30');
      expect(I30.value.toString()).toBe(mergeData.item[11].full_spec);
    });
    it('inserts and populates data for a horizontal dynamic section', async () => {
      const workbook = await readFile(mergeTestFile);
      const worksheet = workbook.worksheets[0];
      await ReportFactory.handleFooter(worksheet, mergeTestHorizontal, 'Horizontal', mergeData);
      const C7 = worksheet.getCell('C7');
      expect(C7.value.toString()).toBe(mergeData.item[0].label);
      const E7 = worksheet.getCell('E7');
      expect(E7.value.toString()).toBe(mergeData.item[1].label);
      const Y7 = worksheet.getCell('Y7');
      expect(Y7.value.toString()).toBe(mergeData.item[11].label);
      const C9 = worksheet.getCell('C9');
      expect(C9.value.toString()).toBe(''); // No full spec
      const E9 = worksheet.getCell('E9');
      expect(E9.value.toString()).toBe(''); // No full spec
      const Y9 = worksheet.getCell('Y9');
      expect(Y9.value.toString()).toBe(mergeData.item[11].full_spec);
    });
  });
});
