// A JS copy of the typescript factory

/* eslint no-param-reassign: ["error", { "props": false }] */
/* eslint no-underscore-dangle: ["error", { "allow": ["_merges", "_themes", "_cells"] }] */

const Excel = require('exceljs');
const path = require('path');
const fs = require('fs');
const { log } = require('../logger');
const { formatNumericData, validateNumber, getColumnLettersFromNumber, getColumnNumberFromLetters, getRowNumberFromAddress, getFormulaResult } = require('./shared/exceljs/dataValidation');
const { loadFile, writeFile, readFile } = require('./shared/exceljs/fileUtilities');

// Factory class of instance methods that read and write excel report templates
class TemplateFactory {
  // Set the data string for the cell
  static assignData(val, cell) {
    val.data = cell.value ? cell.value : '';
    if (!cell.value && cell.master?.value) {
      val.data = cell.master.value;
    }
    if (val.data.richText) {
      val.rich = val.data.richText;
      val.data = this.richTextToString(val.data.richText);
    } else {
      const isNumeric = validateNumber(cell.value);
      if (isNumeric && cell.numFmt) {
        val.data = formatNumericData(cell.value, cell.numFmt);
        val.numFmt = cell.numFmt;
      }
    }
    return val;
  }

  // Set the react style object for a cell
  static assignStyle(val, cell) {
    val.style = this.excelStylesToCSS(cell.style);
    if (cell.border) {
      if (cell.border.left) {
        val.style.borderLeft = this.borderStyle(cell.border.left);
      }
      if (cell.border.right) {
        val.style.borderRight = this.borderStyle(cell.border.right);
      }
      if (cell.border.top) {
        val.style.borderTop = this.borderStyle(cell.border.top);
      }
      if (cell.border.bottom) {
        val.style.borderBottom = this.borderStyle(cell.border.bottom);
      }
    }
    return val;
  }

  static cellHasStyle(cell) {
    if ((cell.border && Object.keys(cell.border).length > 0) || (cell.fill && Object.keys(cell.fill).length > 0 && cell.fill.pattern !== 'none')) return true;
    return false;
  }

  static cellHasContent(cell) {
    if (cell) {
      // if any cell has a type that isn't null or string, assume it has content
      // or if it has a type of string, check that it is not an empty string
      if ((cell.type !== Excel.ValueType.Null && cell.type !== Excel.ValueType.String) || (cell.type === Excel.ValueType.String && cell.value && cell.value.trim() !== '') || this.cellHasStyle(cell)) {
        return true;
      }
    }
    return false;
  }

  // Returns true if any cell in the row has values other than null or empty strings
  // Built-in exceljs function row.hasValues only checks that a row only contains null type cells
  static rowHasContent(row) {
    if (!row) {
      return false;
    }

    for (let c = 0; c < row._cells.length; c++) {
      if (this.cellHasContent(row._cells[c])) {
        return true;
      }
    }
    return false;
  }

  // Parse template contents for web UI
  static async returnUI(file, wkbk = null) {
    // const workbook = new Excel.Workbook();
    // await workbook.xlsx.load(file);
    const workbook = wkbk || (await loadFile(file));
    if (!workbook) {
      log.warn('no workbook loaded');
      return false;
    }
    log.debug('template loaded');
    const data = [];
    // let first = true; // Return only one sheet (for faster debugging)
    // eslint-disable-next-line complexity
    workbook.eachSheet((worksheet, sheetID) => {
      // Return only one sheet (for faster debugging)
      // if (!first) {
      //   log.debug('skipped for debugging');
      //   return;
      // }
      // first = false;

      log.debug(`parsing sheet ${sheetID}-${worksheet.name}`);

      // Not rendering any sheets that do not have cells
      if (!worksheet.columns || !worksheet._rows) {
        return;
      }

      const allRows = worksheet._rows;
      let maxRow = 0;
      for (let r = allRows.length; r >= 0; r--) {
        if (this.rowHasContent(allRows[r])) {
          if (r < allRows.length) {
            maxRow = r + 1;
          } else {
            maxRow = r;
          }
          break;
        }
      }

      if (maxRow === 0) {
        return;
      }

      const sheets = {
        name: worksheet.name,
        columns: [],
        rows: [],
        merges: [],
        showGrid: false,
        orientation: worksheet.pageSetup.orientation,
        theme: workbook._themes,
      };

      const BUFFER_ROWS = 5;
      const blankRows = [];
      for (let i = 0; i < BUFFER_ROWS; i++) blankRows.push([]);

      // Set sheet level styles
      if (worksheet.views && worksheet.views.length > 0) {
        for (let i = 0; i < worksheet.views.length; i++) {
          const view = worksheet.views[i];
          if (view.showGridLines) {
            sheets.showGrid = true;
          }
        }
      }

      // Save Merged Array
      Object.keys(worksheet._merges).forEach((key) => {
        const cell = worksheet._merges[key].model;
        sheets.merges.push({
          row: cell.top - 1,
          col: cell.left - 1,
          rowspan: cell.bottom - cell.top + 1,
          colspan: cell.right - cell.left + 1,
        });
      });

      // Set up column width and styles
      for (let col = 0; col < worksheet.columnCount; col++) {
        if (worksheet.columns[col]) {
          sheets.columns[col] = worksheet.columns[col].width ? worksheet.columns[col].width * 7 : 'auto';
          for (let i = 0; i < BUFFER_ROWS; i++) {
            blankRows[i].push({
              data: ' ',
              style: {
                height: `${worksheet.properties.defaultRowHeight ? `${worksheet.properties.defaultRowHeight}pt` : '15pt'}`,
              },
              address: getColumnLettersFromNumber(col) + (worksheet.rowCount + i + 1),
              masterRow: worksheet.rowCount + i + 1,
              masterCol: col + 1,
            });
          }
        }
      }
      // Add blank column
      sheets.columns.push(worksheet.columns && worksheet.columns[worksheet.columnCount] && worksheet.columns[worksheet.columnCount].width ? worksheet.columns[worksheet.columnCount].width * 7 : 'auto');
      for (let i = 0; i < BUFFER_ROWS; i++) {
        blankRows[i].push({
          data: ' ',
          style: {
            height: `${worksheet.properties.defaultRowHeight ? `${worksheet.properties.defaultRowHeight}pt` : '15pt'}`,
          },
          address: getColumnLettersFromNumber(worksheet.columnCount) + (worksheet.rowCount + i + 1),
          masterRow: worksheet.rowCount + i + 1,
          masterCol: worksheet.columnCount + 1,
        });
      }

      const height = worksheet.properties.defaultRowHeight ? worksheet.properties.defaultRowHeight : '15pt';
      const blankColumnLetters = getColumnLettersFromNumber(worksheet.columnCount);
      const addRow = (row, rowID) => {
        if (rowID <= maxRow) {
          const rows = [];
          if (row._cells.length < worksheet.columnCount) {
            for (let c = row._cells.length; c <= worksheet.columnCount; c++) {
              rows[c] = {
                address: `${getColumnLettersFromNumber(c)}${row.number}`,
                data: ' ',
                style: { height: row.height ? `${row.height}pt` : '15pt' },
                colspan: 1,
                rowspan: 1,
                masterCol: c + 1,
                masterRow: row.number,
              };
            }
            sheets.rows[rowID - 1] = rows;
          }

          // Iterate over each cell, saving the data to the sheets object
          // eslint-disable-next-line complexity
          row.eachCell({ includeEmpty: true }, (cell, cellID) => {
            let cellVal = {};

            cellVal.address = cell.address;
            cellVal = this.assignData(cellVal, cell);
            cellVal = this.assignStyle(cellVal, cell);

            if (cell.formula || cell.value?.formula) {
              const result = getFormulaResult(cell.value?.formula || cell.formula, worksheet);
              cellVal.data = result;
              cellVal.formula = cell.value?.formula || cell.formula;
            }

            cellVal.masterRow = cell.master.row || cell.row;
            cellVal.masterCol = cell.master.col || cell.col;

            if (worksheet._merges[cell.master.address]) {
              const merge = worksheet._merges[cell.master.address].model;
              const colspan = merge.right - merge.left + 1;
              const rowspan = merge.bottom - merge.top + 1;
              cellVal.colspan = colspan;
              cellVal.rowspan = rowspan;
            }

            if (row.height) {
              cellVal.style.height = `${row.height}pt`;
            } else {
              cellVal.style.height = height ? `${height}pt` : '15pt';
            }
            rows[cellID - 1] = cellVal;
          });
          if (rows.length <= 0) {
            log.warn('no data in row ', rowID);
          }

          // Add blank column
          const blankColumn = {
            data: ' ',
            style: { height: height ? `${height}pt` : '15pt' },
            address: blankColumnLetters + rowID,
            masterRow: row.number,
            masterCol: worksheet.columnCount + 1,
          };
          rows.push(blankColumn);

          sheets.rows[rowID - 1] = rows;
        }
      };

      worksheet.eachRow({ includeEmpty: true }, addRow);

      if (sheets.rows.length <= 0) {
        log.warn('no rows found for sheet');
      }
      for (let i = 0; i < BUFFER_ROWS; i++) {
        sheets.rows.push(blankRows[i]);
      }
      data.push(sheets);
    });

    log.debug('End of template factory');

    if (!data || data.length <= 0) {
      log.warn('No data found in template file');
      return false;
    }
    return data;
  }

  // Convert a rich text array to an html string
  static richTextToString(value) {
    let data = ``;
    for (let i = 0; i < value.length; i++) {
      if (value[i] && value[i].text) {
        if (value[i].text === '\n') {
          data += `<br />`;
        } else {
          const style = value[i].font ? this.fontStyleString(value[i].font) : null;
          data += `<span${style ? ` style="${style}"` : ''}>${value[i].text}</span>`;
        }
      }
    }
    return data;
  }

  // Convert excel font styles to a css font value
  static fontStyleString(style) {
    if (!style) {
      return false;
    }
    let cell = '';
    if (style.italic) {
      cell += 'font-style:italic; ';
    }
    if (style.bold) {
      cell += 'font-weight:bold; ';
    }
    if (style.underline) {
      cell += 'text-decoration:underline; ';
    }
    if (style.strike) {
      cell += 'text-decoration:line-through; ';
    }
    if (style.size) {
      cell += `font-size:${style.size}pt; `;
    }
    if (style.name) {
      cell += `font-family:${style.name}; `;
    }
    if (style.color && style.color.argb) {
      if (style.color.argb.length > 7) {
        cell += `color:#${style.color.argb.substring(2)};`;
      } else {
        cell += `color:#${style.color.argb};`;
      }
    }
    return cell;
  }

  // Convert excel file styles to a react style object
  static excelStylesToCSS(style) {
    if (!style) {
      return false;
    }
    const keys = Object.keys(style);
    const cell = {};
    for (let i = 0; i < keys.length; i++) {
      const prop = keys[i];
      let font = '';
      switch (prop) {
        case 'font':
          if (style.font.italic) {
            font += 'italic ';
          }
          if (style.font.bold) {
            font += 'bold ';
          }
          if (style.font.underline) {
            cell.textDecoration = 'underline';
          }
          if (style.font.strike) {
            cell.textDecoration = 'line-through';
          }
          if (style.font.size) {
            font += `${style.font.size}pt `;
          }
          if (style.font.name) {
            font += style.font.name;
          }
          if (style.font.color && style.font.color.argb) {
            if (style.font.color.argb.length > 7) {
              cell.color = `#${style.font.color.argb.substring(2)}`;
            } else {
              cell.color = `#${style.font.color.argb}`;
            }
          }
          cell.font = font;
          break;
        case 'alignment':
          if (!style.alignment) {
            break;
          }
          if (style.alignment.horizontal) {
            cell.textAlign = style.alignment.horizontal;
          }
          if (style.alignment.vertical) {
            cell.verticalAlign = style.alignment.vertical;
          }
          break;
        case 'fill':
          if (!style.fill) {
            break;
          }
          if (style.fill.fgColor && style.fill.fgColor.argb) {
            if (style.fill.fgColor.argb.length > 7) {
              cell.backgroundColor = `#${style.fill.fgColor.argb.substring(2)}`;
            } else {
              cell.backgroundColor = `#${style.fill.fgColor.argb}`;
            }
          }
          break;
        default:
          break;
      }
    }
    return cell;
  }

  // convert excel border styles to a react style object
  static borderStyle(style) {
    let border = '';
    switch (style.style) {
      case 'thin':
      case 'hair':
        border += 'solid 1px';
        break;
      case 'medium':
        border += 'solid 2px';
        break;
      case 'thick':
        border += 'solid 3px';
        break;
      case 'double':
        border += 'double 3px';
        break;
      case 'dotted':
      case 'dashDotDot':
      case 'mediumDashDotDot':
        border += 'dotted 2px';
        break;
      case 'dashed':
      case 'dashDot':
      case 'slantDashDot':
      case 'mediumDashed':
      case 'mediumDashDot':
        border += 'dashed 2px';
        break;
      default:
        break;
    }

    if (style.color && style.color.rgb) {
      border += ` #${style.color.rgb}`;
    } else {
      border += ' black';
    }
    return border;
  }

  // write new data into workbook file
  static async overWriteData(data, templateFilePath, outFilePath) {
    log.debug(`write to file. data exists: ${!!data}, template path: ${templateFilePath}, output: ${outFilePath}`);
    const workbook = new Excel.Workbook();
    let fp = templateFilePath;

    // Validate path to file
    if (typeof templateFilePath === 'string') {
      fp = path.resolve(templateFilePath);
      if (!fs.existsSync(fp)) {
        log.error(`Unable to find report template at ${fp}`);
        return false;
      }
      log.debug(`Loading report template from ${fp}`);
    } else {
      log.debug('Remote template file loaded and passed in');
    }

    await workbook.xlsx.readFile(fp);
    log.debug('Template loaded');

    // Iterate over all sheets
    workbook.eachSheet((worksheet, sheetID) => {
      // Changes data is an array of sheet changes
      // each sheet change is an object with the cell address as the key
      // and the changed cell as the value
      const dataSheet = data[sheetID - 1];
      if (!dataSheet) {
        log.debug(`empty sheet ID: ${sheetID}`);
        return;
      }

      const changed = Object.keys(dataSheet);
      for (let c = 0; c < changed.length; c++) {
        if (changed[c]) {
          const newVal = dataSheet[changed[c]];
          if (newVal) {
            let cell = worksheet.getCell(newVal.address);

            // Set value to master for merged cells
            if (cell.master.address !== cell.address) {
              cell = worksheet.getCell(cell.master.address);
            }

            // Save new data
            if (newVal.rich) {
              cell.richText = newVal.rich;
            } else {
              cell.value = newVal.data;
            }
          } else {
            log.debug(`empty cell sheetID: ${sheetID}, cell: ${c}`);
          }
        } else {
          log.debug(`no data at key ${c} in changed`);
        }
      }

      if (worksheet.conditionalFormattings) {
        this.handleConditionalFormatting(worksheet);
      }
    });

    // After processing all sheets,
    writeFile(workbook, outFilePath);
    return outFilePath;
  }
}

module.exports = TemplateFactory;
