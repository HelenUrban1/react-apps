const { frameText, lookup, unFrameText } = require('../staticData/characters');
const { GdtSymbols } = require('../staticData/defaults');
module.exports = class ClassifierService {
  typeMap = {
    champher: 'champher',
    countersink: 'countersink_diameter',
    counterbore: 'counterbore_diameter',
    holeWithCounterbore: { depth: 'counterbore_depth', diameter: 'counterbore_diameter' },
    holeWithCountersink: { angle: 'countersink_angle', diameter: 'countersink_diameter' },
    metricThreading: 'thread',
    metricThreadingInternal: 'thread',
    metricThreadingExternal: 'thread',
    standardThreadedFastner: 'thread',
    standardThreadingInternal: 'thread',
    valueWithOptionalUnits: 'thread',
    nominalWithTolerances: { '°': 'angle', '': 'length' },
    extreme: { '°': 'angle', '': 'length' },
    splitLimits: 'length',
    unknown: 'note',
  };

  /**
   * Parse interpreted notation to determine the engineering notation.
   * @param {Object} options -
   * @param {ParserReturnValue} options.interpreted
   * @param {ListObject} options.types
   * @returns {Object} - The quantity, notation class and full-specification for the parsed result from ENP.
   */
  parseInterpretedNotation({ interpreted, types }) {
    if (!interpreted) {
      throw new Error('Cannot parse blank results');
    }
    let feature = {};

    switch (interpreted?.notation?.type) {
      case 'note':
      case 'unknown':
        feature = this.assignNote({ feature, interpreted });
        break;
      case 'gdtControlFrame':
        feature = this.assignGDT({ feature, interpreted });
        break;
      case undefined:
        throw new Error('Interpretation not parsed');
      default:
        feature = this.assignDimension({ feature, interpreted });
        break;
    }

    feature.quantity = interpreted?.quantity?.value ? parseInt(interpreted.quantity.value, 10) : 1;
    feature = this.assignTypeAndSubtype(feature, types);
    feature.notationClass = this.getClassForNotation(interpreted?.notation?.modifier ? interpreted.notation.modifier : interpreted?.notation?.type);
    if (feature.notationClass === 'Basic') {
      feature.fullSpecification = this.updateToBasic(feature.fullSpecification || '');
    } else if (feature.notationClass === 'Reference') {
      feature.fullSpecification = this.updateToRef(feature.fullSpecification || '');
    }
    return feature;
  }

  /**
   * Assign the relevent properties to a characteristic to classify it as a note..
   * @param {Object} options
   * @param {ParsedCharacteristic} options.feature
   * @param {string} [options.change]
   * @param {ParserReturnValue} [options.interpreted]
   * @returns {ParsedCharacteristic} - The characteristic with note classification.
   */
  assignNote({ feature, change, interpreted }) {
    const note = { ...feature };
    note.notationType = 'Note';
    note.notationSubtype = 'Note';
    if (change) {
      note.fullSpecification = change;
      note.nominal = change;
    } else if (interpreted?.parsed?.input) {
      note.fullSpecification = interpreted.parsed.input.trim();
      note.nominal = interpreted.parsed.input.trim();
    } else {
      // Calling getFramedText ensures that Notes with Basic or GDTs embeded in them get framed
      note.fullSpecification = interpreted?.input ? this.getFramedText(interpreted?.input) : interpreted?.input;
      note.nominal = interpreted?.input;
    }
    note.unit = '';
    return note;
  }

  /**
   * Utility function to to transform text into snake case format.
   * @param {string} text
   * @returns {string} The input text transformed to snake case format.
   */
  toSnakeCase(text) {
    return text.toLowerCase().replace(/ /g, '_');
  }

  /**
   * Updates the full specification to a tolerance string format.
   * @param {string} fullSpec
   * @param {CharacteristicNotationClassEnum} oldClass
   * @returns {string} The updated specification string in tolerance format.
   */
  updateToTol(fullSpec, oldClass) {
    let cleanSpec = this.isFramed(fullSpec) ? this.getUnframedText(fullSpec) : fullSpec;
    if (oldClass === 'Basic') {
      cleanSpec = cleanSpec.replace(/\|/g, '');
    } else {
      cleanSpec = cleanSpec.replace('(', '').replace(')', '');
    }
    return cleanSpec;
  }

  /**
   * Updates the dimension property based on the provided options.
   * @param {Object} options
   * @param {ParsedCharacteristic} options.feature
   * @param {ParsedCharacteristic} options.change
   * @returns {ParsedCharacteristic} The updated characteristic with the new dimension values.
   */
  updateDimension({ feature, change }) {
    const dimension = { ...feature };
    if (change.nominal) {
      dimension.nominal = change.nominal;
    }
    if (change.notationClass) {
      dimension.notationClass = change.notationClass;
      if (change.notationClass === 'Basic') {
        dimension.fullSpecification = this.updateToBasic(dimension.fullSpecification || '');
      } else if (change.notationClass === 'Reference') {
        dimension.fullSpecification = this.updateToRef(dimension.fullSpecification || '');
      } else {
        dimension.fullSpecification = this.updateToTol(dimension.fullSpecification || '', dimension.notationClass);
      }
    }
    if (change.unit) {
      dimension.unit = change.unit;
    }
    if (change.fullSpecification) {
      dimension.fullSpecification = change.fullSpecification;
    } else if (!change.notationClass) {
      dimension.fullSpecification = dimension.nominal;
      if (dimension.plusTol && dimension.minusTol) {
        dimension.fullSpecification += ` ${dimension.plusTol}/${dimension.minusTol}`;
      }
    }
    delete dimension.gdtPrimaryToleranceZone;
    delete dimension.gdtSecondaryToleranceZone;
    delete dimension.gdtPrimaryDatum;
    delete dimension.gdtSecondaryDatum;
    delete dimension.gdtTertiaryDatum;
    delete dimension.gdtSymbol;
    return dimension;
  }

  /**
   * Updates the general properties of a characteristic based on the provided options.
   * @param {Object} options
   * @param {ParsedCharacteristic} options.feature
   * @param {ParsedCharacteristic} options.change
   * @returns {ParsedCharacteristic} The updated characteristic object.
   */
  updateGeneral({ feature, change }) {
    const newFeature = { ...feature };
    if (change.quantity) {
      newFeature.quantity = parseInt(change.quantity.toString(), 10);
    }
    if (change.unit || change.unit === '') {
      newFeature.unit = change.unit;
    }
    if (change.notationType) {
      newFeature.notationType = change.notationType;
    }
    if (change.notationSubtype) {
      newFeature.notationSubtype = change.notationSubtype;
    }
    if (change.status) {
      newFeature.status = change.status;
    }
    if (change.captureMethod) {
      newFeature.captureMethod = change.captureMethod;
    }
    if (change.criticality || change.criticality === '') {
      newFeature.criticality = change.criticality;
    }
    if (change.operation || change.operation === '') {
      newFeature.operation = change.operation;
    }
    if (change.inspectionMethod || change.inspectionMethod === '') {
      newFeature.inspectionMethod = change.inspectionMethod;
    }
    if (change.notes || change.notes === '') {
      newFeature.notes = change.notes;
    }
    if (change.verified) {
      newFeature.verified = change.verified;
    } else {
      newFeature.verified = false;
    }
    return newFeature;
  }

  /**
   * Updates the geometric dimensioning and tolerancing (GD&T) properties of a characteristic based on the provided options.
   * @param {Object} options
   * @param {ParsedCharacteristic} options.feature
   * @param {ParsedCharacteristic} options.change
   * @returns @returns {ParsedCharacteristic} The updated GD&T characteristic object.
   */
  updateGDT({ feature, change }) {
    const gdt = { ...feature };
    if (change.gdtSymbol) {
      gdt.gdtSymbol = change.gdtSymbol;
    }
    if (change.gdtPrimaryToleranceZone) {
      gdt.gdtPrimaryToleranceZone = change.gdtPrimaryToleranceZone;
    }
    if (change.gdtSecondaryToleranceZone) {
      gdt.gdtSecondaryToleranceZone = change.gdtSecondaryToleranceZone;
    }
    if (change.gdtPrimaryDatum) {
      gdt.gdtPrimaryDatum = change.gdtPrimaryDatum;
    }
    if (change.gdtSecondaryDatum) {
      gdt.gdtSecondaryDatum = change.gdtSecondaryDatum;
    }
    if (change.gdtTertiaryDatum) {
      gdt.gdtTertiaryDatum = change.gdtTertiaryDatum;
    }
    if (change.fullSpecification) {
      gdt.fullSpecification = change.fullSpecification;
    }
    delete gdt.nominal;
    delete gdt.plusTol;
    delete gdt.minusTol;
    return gdt;
  }

  /**
   * Retrieves the subtype of the interpreted value based on the provided ParserReturnValue.
   * @param {ParserReturnValue} interpreted
   * @returns {string} The subtype of the interpreted value.
   */
  getSubType(interpreted) {
    let subtype = '';

    if (interpreted?.notation?.symbol === 'R') {
      subtype = 'radius';
    } else if (interpreted?.notation?.symbol) {
      const { name } = lookup(interpreted.notation.symbol);
      subtype = this.toSnakeCase(name);
    } else if (interpreted?.notation?.type) {
      subtype = this.typeMap[interpreted.notation.type];
      if (!subtype) {
        subtype = interpreted.notation.type;
      }
    }
    if (typeof subtype !== 'string') {
      subtype = this.getSubTypeVariant(interpreted);
    }

    return subtype;
  }

  /**
   * Assigns basic dimension properties to the characteristic based on the interpreted value.
   * @param {Object} options
   * @param {ParsedCharacteristic} options.feature
   * @param {ParserReturnValue} options.interpreted
   * @returns {ParsedCharacteristic} The characteristic with assigned basic dimension properties.
   */
  assignBasicDimension({ feature, interpreted }) {
    const dimension = { ...feature };

    if (interpreted?.notation?.upper_limit) {
      dimension.upperSpecLimit = interpreted.notation.upper_limit.value;
      dimension.toleranceSource = 'Document_Defined';
    }

    if (interpreted?.notation?.lower_limit) {
      dimension.lowerSpecLimit = interpreted.notation.lower_limit.value;
      dimension.toleranceSource = 'Document_Defined';
    }
    // plus plus, minus, minus ???
    if (interpreted?.notation?.upper_tol) {
      dimension.plusTol = interpreted.notation.upper_tol.sign === '+' ? interpreted.notation.upper_tol.value : `${interpreted.notation.upper_tol.sign}${interpreted.notation.upper_tol.value}`;
      dimension.toleranceSource = 'Document_Defined';
    }

    if (interpreted?.notation?.lower_tol) {
      dimension.minusTol = interpreted.notation.lower_tol.sign === '+' ? interpreted.notation.lower_tol.value : `${interpreted.notation.lower_tol.sign}${interpreted.notation.lower_tol.value}`;
      dimension.toleranceSource = 'Document_Defined';
    }

    if (interpreted?.notation?.nominal) {
      dimension.nominal = interpreted.notation.nominal.value;
    } else {
      dimension.nominal = interpreted?.notation?.text;
    }

    return dimension;
  }

  /**
   * Assigns geometric dimensioning and tolerancing (GD&T) properties to the characteristic based on the interpreted value.
   * @param {Object} options
   * @param {ParsedCharacteristic} options.feature
   * @param {ParserReturnValue} [options.interpreted]
   * @returns {ParsedCharacteristic} The characteristic with assigned GD&T properties.
   */
  assignGDT({ feature, interpreted }) {
    const GDT = { ...feature };
    GDT.notationType = 'Geometric_Tolerance';
    GDT.fullSpecification = interpreted?.input;
    GDT.gdtSymbol = '';
    GDT.gdtPrimaryToleranceZone = '';
    GDT.gdtSecondaryToleranceZone = '';
    GDT.gdtPrimaryDatum = '';
    GDT.gdtSecondaryDatum = '';
    GDT.gdtTertiaryDatum = '';
    GDT.upperSpecLimit = '';
    GDT.lowerSpecLimit = '';
    if (interpreted?.input) {
      GDT.fullSpecification = frameText(interpreted.input);
    }
    if (interpreted?.notation?.subtype) {
      GDT.notationSubtype = this.toSnakeCase(interpreted.notation.subtype);
      GDT.gdtSymbol = this.getGDTSymbol(GDT.notationSubtype);
    }
    if (interpreted?.notation?.zones) {
      GDT.gdtPrimaryToleranceZone = interpreted.notation.zones.primary.text;
      GDT.gdtSecondaryToleranceZone = interpreted.notation.zones?.secondary?.value || '';
    }
    if (interpreted?.notation?.datums) {
      GDT.gdtPrimaryDatum = interpreted.notation.datums[0]?.id;
      GDT.gdtSecondaryDatum = interpreted.notation.datums[1]?.id;
      GDT.gdtTertiaryDatum = interpreted.notation.datums[2]?.id;
    }
    return GDT;
  }

  /**
   * Assigns dimension properties to the characteristic based on the interpreted value.
   * @param {Object} options
   * @param {ParsedCharacteristic} options.feature
   * @param {ParserReturnValue} [options.interpreted]
   * @returns {ParsedCharacteristic} The characteristic with assigned dimension properties.
   */
  assignDimension({ feature, interpreted }) {
    let dimension = { ...feature };
    dimension.notationType = 'Dimension';
    dimension.notationSubtype = this.getSubType(interpreted);
    dimension.fullSpecification = interpreted?.input;
    dimension.nominal = '';
    dimension.plusTol = '';
    dimension.minusTol = '';
    dimension.upperSpecLimit = '';
    dimension.lowerSpecLimit = '';

    dimension = this.assignBasicDimension({ feature: dimension, interpreted });
    dimension = this.assignSpecialDimension({ feature: dimension, interpreted });
    return dimension;
  }

  /**
   * Retrieves the subtype variant of the interpreted value based on the provided ParserReturnValue.
   * @param {ParserReturnValue} interpreted
   * @returns {string} The subtype variant of the interpreted value.
   */
  getSubTypeVariant(interpreted) {
    let subtype = '';
    if (interpreted?.notation?.type === 'holeWithCounterbore') {
      if (interpreted?.notation?.countersink?.angle?.amount) {
        subtype = 'countersink_angle';
      } else {
        subtype = 'countersink_diameter';
      }
    }
    if (interpreted?.notation?.type === 'holeWithCounterbore') {
      if (interpreted?.notation?.counterbore?.depth?.amount) {
        subtype = 'counterbore_depth';
      } else {
        subtype = 'counterbore_diameter';
      }
    }
    if (interpreted?.notation?.type === 'nominalWithTolerances' || interpreted?.notation?.type === 'extreme') {
      if (interpreted?.notation?.nominal?.units === '°') {
        subtype = 'angle';
      } else {
        subtype = 'length';
      }
    }
    return subtype;
  }

  /**
   * Assigns dimension limit properties to the characteristic based on the provided edge and limit values.
   * @param {Object} options
   * @param {ParsedCharacteristic} options.feature
   * @param {string} options.edge
   * @param {NotationValue} options.limit
   * @returns {ParsedCharacteristic} The characteristic with assigned dimension limit properties.
   */
  assignDimensionLimit({ feature, edge, limit }) {
    const dimension = { ...feature };
    dimension.nominal = limit.value;
    dimension.upperSpecLimit = edge === 'MAX' ? limit.value : '';
    dimension.lowerSpecLimit = edge === 'MIN' ? limit.value : '';
    dimension.toleranceSource = 'Document_Defined';
    return dimension;
  }

  /**
   * Assigns special dimension properties to the characteristic based on the interpreted value.
   * @param {Object} options
   * @param {ParsedCharacteristic} options.feature
   * @param {ParserReturnValue} options.interpreted
   * @returns {ParsedCharacteristic} The characteristic with assigned special dimension properties.
   */
  assignSpecialDimension({ feature, interpreted }) {
    let dimension = { ...feature };
    dimension.notationType = 'Dimension';
    dimension.notationSubtype = this.getSubType(interpreted);
    dimension.fullSpecification = interpreted?.input;

    if (interpreted?.notation?.amount) {
      dimension.nominal = interpreted.notation.amount.value;
    }

    if (interpreted?.notation?.width) {
      dimension = this.assignDimensionWidth({
        feature: dimension,
        width: interpreted.notation.width,
      });
    }

    if (interpreted?.notation?.limit) {
      dimension = this.assignDimensionLimit({
        feature: dimension,
        limit: interpreted.notation.limit,
        edge: interpreted?.notation?.edge,
      });
    }

    return dimension;
  }

  /**
   * Assigns dimension width properties to the characteristic based on the provided width value.
   * @param {Object} options
   * @param {ParsedCharacteristic} options.feature
   * @param {NotationWidth} options.width
   * @returns {ParsedCharacteristic} The characteristic with assigned dimension width properties.
   */
  assignDimensionWidth({ feature, width }) {
    let dimension = { ...feature };

    if (width?.upper_tol) {
      dimension.plusTol = width.upper_tol.sign === '+' ? width.upper_tol.value : `${width.upper_tol.sign}${width.upper_tol.value}`;
      dimension.toleranceSource = 'Document_Defined';
    }

    if (width?.lower_tol) {
      dimension.minusTol = width.lower_tol.sign === '+' ? width.lower_tol.value : `${width.lower_tol.sign}${width.lower_tol.value}`;
      dimension.toleranceSource = 'Document_Defined';
    }

    if (width?.upper_limit) {
      dimension.upperSpecLimit = width.upper_limit.value;
      dimension.toleranceSource = 'Document_Defined';
    }

    if (width?.lower_limit) {
      dimension.lowerSpecLimit = width.lower_limit.value;
      dimension.toleranceSource = 'Document_Defined';
    }

    if (width?.nominal) {
      dimension.nominal = width.nominal.value;
    } else {
      dimension.nominal = width?.text;
    }

    if (width?.limit) {
      dimension = this.assignDimensionLimit({
        feature: dimension,
        limit: width.limit,
        edge: width?.edge,
      });
    }
    return dimension;
  }

  /**
   * Retrieves the GD&T symbol based on the subtype provided.
   * @param {string} subtype
   * @returns {string} The GD&T symbol corresponding to the subtype.
   */
  getGDTSymbol(subtype) {
    if (!subtype) {
      return subtype;
    }
    const type = this.toSnakeCase(subtype);
    const symbol = GdtSymbols[type];
    return symbol?.char || GdtSymbols.position.char;
  }

  /**
   * Assigns the type and subtype IDs to the characteristic based on the provided types list.
   * @param {ParsedCharacteristic} feature
   * @param {ListObject} types
   * @returns {ParsedCharacteristic} The characteristic with assigned type and subtype IDs.
   */
  assignTypeAndSubtype(feature, types) {
    const typeSubType = { ...feature };
    const typeId = this.getListIdByValue({
      list: types,
      value: feature.notationType,
      defaultValue: 'fa3284ed-ad3e-43c9-b051-87bd2e95e927',
    });
    typeSubType.notationType = typeId;
    if (typeId && types[typeId]?.children) {
      const subTypeId = this.getListIdByValue({
        list: types[typeId].children,
        value: feature.notationSubtype,
        defaultValue: '805fd539-8063-4158-8361-509e10a5d371',
      });
      typeSubType.notationSubtype = subTypeId;
    }
    return typeSubType;
  }

  /**
   * Finds the ID of an item in a list object based on its value.
   * @param {Object} options
   * @param {ListObject | null} options.list
   * @param {string} [options.value]
   * @param {string} [options.defaultValue]
   * @returns {string} The ID of the item in the list, or the defaultValue if not found.
   */
  getListIdByValue = ({ list, value, defaultValue = 'Not found' }) => {
    if (!value) {
      throw new Error('Type not provided');
    }
    if (!list) {
      return defaultValue;
    }
    const ID = Object.keys(list).find((key) => this.cleanLowercaseString(list[key].value) === this.cleanLowercaseString(value));
    return ID || defaultValue;
  };

  /**
   * Cleans and converts a string to lowercase.
   * @param {string} text
   * @returns {string} The cleaned and lowercase string.
   */
  cleanLowercaseString = (text) => {
    if (!text || typeof text !== 'string') {
      return text;
    }
    return text.replace(/ /g, '').replace(/_/g, '').toLowerCase();
  };

  /**
   * Retrieves the measurement or value of an item in a list object based on its ID.
   * @param {Object} options
   * @param {ListObject | null} [options.list]
   * @param {string} [options.value]
   * @param {string} [parentId]
   * @returns {string | undefined} The measurement or value of the item, or undefined if not found.
   */
  getListMeasurementById = ({ list, value, parentId }) => {
    if (!value) {
      throw new Error('Type not provided');
    }
    if (!list) {
      return undefined;
    }
    if (parentId && list[parentId]?.children) {
      const children = list[parentId].children;
      return children[value]?.measurement || children[value]?.value;
    }
    return list[value]?.measurement || list[value]?.value;
  };

  /**
   * Retrieves the name of an item in a list object based on its ID.
   * @param {Object} options
   * @param {ListObject | null} [options.list]
   * @param {string} [options.value]
   * @param {string} [parentId]
   * @param {string} options.defaultValue
   * @returns {string} The name of the item, or the defaultValue if not found.
   */
  getListNameById = ({ list, value, parentId, defaultValue = 'Not found' }) => {
    if (!value) {
      throw new Error('Type not provided');
    }
    if (!list) {
      return defaultValue;
    }
    if (parentId && list[parentId]?.children) {
      const children = list[parentId].children;
      return children[value]?.value;
    }
    return list[value]?.value || defaultValue;
  };

  /**
   * Checks if a modifier indicates a reference.
   * @param {string} modifier
   * @returns {boolean} True if the modifier indicates a reference, otherwise false.
   */
  isReference(modifier) {
    let result = false;
    if (modifier) {
      result = modifier === 'reference';
    }
    return result;
  }

  /**
   * Determines the class for a notation based on the provided modifier or type.
   * @param {string} [modifierOrType]
   * @returns {'Tolerance' | 'Basic' | 'Reference' | 'N_A'} The class for the notation.
   */
  getClassForNotation(modifierOrType) {
    let notationClass = 'Tolerance';
    if (this.isBasic(modifierOrType)) {
      notationClass = 'Basic';
    } else if (this.isReference(modifierOrType)) {
      notationClass = 'Reference';
    } else if (modifierOrType === 'note' || modifierOrType === 'unknown') {
      notationClass = 'N_A';
    }
    return notationClass;
  }

  /**
   * Checks if a modifier indicates a basic notation.
   * @param {string} [modifier]
   * @returns {boolean} True if the modifier indicates a basic notation, otherwise false.
   */
  isBasic(modifier) {
    let result = false;
    if (modifier) {
      result = modifier === 'basic' || modifier === 'framed';
    }
    return result;
  }

  /**
   * Updates the specification to a basic format.
   * @param {string} fullSpec
   * @returns {string} The updated specification in basic format.
   */
  updateToBasic(fullSpec) {
    let cleanSpec = this.isFramed(fullSpec) ? this.getUnframedText(fullSpec) : fullSpec;
    cleanSpec = cleanSpec.replace('(', '|').replace(')', '|');
    if (cleanSpec.toLocaleLowerCase().includes('basic') || cleanSpec.toLocaleLowerCase().includes('bsc')) return cleanSpec;
    if (/\|(.*?)\|/.test(cleanSpec)) return this.getFramedText(cleanSpec);
    const singlePipe = cleanSpec.indexOf('|');
    if (singlePipe >= 0) {
      return singlePipe < cleanSpec.length / 2 ? this.getFramedText(`${cleanSpec}|`) : this.getFramedText(`|${cleanSpec}`);
    }
    return this.getFramedText(`|${cleanSpec}|`);
  }

  /**
   * Checks if a string is framed.
   * @param {string} str
   * @returns {boolean} True if the string is framed, otherwise false.
   */
  isFramed = (str) => {
    for (let i = 0; i < str.length; i++) {
      const c = lookup(str.charAt(i));
      if (c && str.charAt(i) === c.framed_char) return true;
    }
    return false;
  };

  /**
   * Gets the text enclosed within framing characters and returns the unframed text.
   * @param {string} str
   * @returns {string} The unframed text.
   */
  getFramedText = (str) => {
    const text = unFrameText(str);
    const frameSubstring = this.getBetweenPipes(text);
    if (frameSubstring) {
      const inFrames = frameText(frameSubstring[0]);
      return text.replace(frameSubstring[0], inFrames);
    }
    return text;
  };

  /**
   * Retrieves the substring enclosed between pipes ('|') in a string.
   * @param {string} str
   * @returns {string | null} The substring enclosed between pipes, or null if not found.
   */
  getBetweenPipes = (str) => {
    return str.match(/\|(.*)\|/);
  };

  /**
   * Updates the specification to a reference format.
   * @param {string} fullSpec
   * @returns {string} The updated specification in reference format.
   */
  updateToRef(fullSpec) {
    let cleanSpec = this.isFramed(fullSpec) ? this.getUnframedText(fullSpec) : fullSpec;
    cleanSpec = cleanSpec.replace('|', '(').replace('|', ')');
    if (cleanSpec.toLocaleLowerCase().includes('ref') || /\((.*?)\)/.test(cleanSpec)) return cleanSpec;
    const start = cleanSpec.indexOf('(');
    if (start >= 0) return `${cleanSpec})`;
    const end = cleanSpec.indexOf(')');
    if (end >= 0) return `(${cleanSpec}`;
    return `(${cleanSpec})`;
  }

  /**
   * Gets the unframed text from a framed string.
   * @param {string} str
   * @returns {string} The unframed text.
   */
  getUnframedText = (str) => {
    const text = unFrameText(str);

    return text;
  };
};
