const config = require('../../config')();
const USE_MOCKS = config.useMocks;

if (USE_MOCKS) {
  jest.doMock('../database/models');
  jest.doMock('../database/repositories/stationRepository');
}

const models = require('../database/models');
const StationRepository = require('../database/repositories/stationRepository');
const StationService = require('./stationService');
const SequelizeRepository = require('../database/repositories/sequelizeRepository');

describe('StationService', () => {
  const accountId = config.testAccountId;
  const data = {
    name: 'Test Station',
    disabled: false,
    isMaster: false,
  };

  const createMock = jest.fn().mockReturnValue(data);
  const updateMock = jest.fn().mockReturnValue({ ...data, name: 'foo' });

  beforeEach(async () => {
    if (USE_MOCKS) {
      models.getTenant = () =>
        Promise.resolve({
          sequelize: {
            transaction: jest.fn().mockReturnValue({
              rollback: jest.fn(),
              commit: jest.fn(),
            }),
          },
        });

      models.sequelizeAdmin = {
        query: jest.fn().mockReturnValue(Promise.resolve({})),
      };

      StationRepository.mockImplementation(() => {
        return {
          create: createMock,
          update: updateMock,
        };
      });

      SequelizeRepository.commitTransaction = jest.fn();
    }
  });

  afterAll(async () => {
    if (!USE_MOCKS) {
      await SequelizeRepository.closeConnections();
      await SequelizeRepository.closeConnections(accountId);
    }
  });

  describe('CreateStation', () => {
    const context = { currentUser: { accountId } };

    it('Should return station data', async () => {
      try {
        const station = await new StationService(context).create(data);
        if (USE_MOCKS) {
          expect(createMock).toBeCalledTimes(1);
        }
        expect(station).toBeTruthy();
      } catch (exception) {
        console.error(exception);
        expect(exception).toBeFalsy();
      }
    }, 10000);
  });

  describe('Update Station', () => {
    const context = { currentUser: { accountId } };

    it('Should return updated station data', async () => {
      try {
        const station = await new StationService(context).create(data);
        expect(station).toBeTruthy();

        const stationAgain = await new StationService(context).update(station.id, { name: 'foo' });

        if (USE_MOCKS) {
          expect(updateMock).toBeCalledTimes(1);
        }
        expect(stationAgain.name).toEqual('foo');
      } catch (exception) {
        console.error(exception);
        expect(exception).toBeFalsy();
      }
    }, 10000);
  });
});
