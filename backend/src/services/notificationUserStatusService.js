const NotificationUserStatusRepository = require('../database/repositories/notificationUserStatusRepository');
const ValidationError = require('../errors/validationError');
const SequelizeRepository = require('../database/repositories/sequelizeRepository');

/**
 * Handles Notification operations
 */
module.exports = class NotificationUserStatusService {
  constructor({ currentUser, language, eventEmitter }) {
    this.repository = new NotificationUserStatusRepository();
    this.currentUser = currentUser;
    this.language = language;
    this.eventEmitter = eventEmitter;
  }

  /**
   * Creates a NotificationUserStatus.
   *
   * @param {*} data
   */
  async create(data) {
    const transaction = await SequelizeRepository.createTransaction(this.currentUser.accountId);

    try {
      const record = await this.repository.create(data, {
        transaction,
        currentUser: this.currentUser,
      });

      await SequelizeRepository.commitTransaction(transaction);

      return record;
    } catch (error) {
      await SequelizeRepository.rollbackTransaction(transaction);
      throw error;
    }
  }

  /**
   * Updates a NotificationUserStatus.
   *
   * @param {*} id
   * @param {*} data
   */
  async update(id, data) {
    const transaction = await SequelizeRepository.createTransaction(this.currentUser.accountId);

    try {
      const record = await this.repository.update(id, data, {
        transaction,
        currentUser: this.currentUser,
      });

      await SequelizeRepository.commitTransaction(transaction);

      if (this.eventEmitter) {
        console.log('Emitting notificationUserStatus.update', this.currentUser.accountId);
        this.eventEmitter.emit('notificationUserStatus.update', { accountId: this.currentUser.accountId, siteId: this.currentUser.siteId, data: record });
      }

      return record;
    } catch (error) {
      await SequelizeRepository.rollbackTransaction(transaction);
      throw error;
    }
  }

  /**
   * Destroy all NotificationUserStatuses with those ids.
   *
   * @param {*} ids
   */
  async destroyAll(ids) {
    const transaction = await SequelizeRepository.createTransaction(this.currentUser.accountId);

    try {
      let idsToDelete = ids;
      if (!Array.isArray(ids) && typeof ids === 'string') {
        idsToDelete = [ids];
      }
      await Promise.all(idsToDelete.map((id) => this.repository.destroy(id, { transaction, currentUser: this.currentUser })));

      await SequelizeRepository.commitTransaction(transaction);
    } catch (error) {
      await SequelizeRepository.rollbackTransaction(transaction);
      throw error;
    }
  }

  /**
   * Finds the NotificationUserStatus by Id.
   *
   * @param {*} id
   */
  async findById(id) {
    return this.repository.findById(id, {
      currentUser: this.currentUser,
    });
  }

  /**
   * Finds NotificationUserStatuses based on the query.
   *
   * @param {*} args
   */
  async findAndCountAll(args) {
    return this.repository.findAndCountAll(args, {
      currentUser: this.currentUser,
    });
  }

  /**
   * Imports a list of NotificationUserStatuses.
   *
   * @param {*} data
   * @param {*} importHash
   */
  async import(data, importHash) {
    if (!importHash) {
      throw new ValidationError(this.language, 'importer.errors.importHashRequired');
    }

    if (await this._isImportHashExistent(importHash)) {
      throw new ValidationError(this.language, 'importer.errors.importHashExistent');
    }

    const dataToCreate = {
      ...data,
      importHash,
    };

    return this.create(dataToCreate);
  }

  /**
   * Checks if the import hash already exists.
   * Every item imported has a unique hash.
   *
   * @param {*} importHash
   */
  async _isImportHashExistent(importHash) {
    const count = await this.repository.count({
      importHash,
    });

    return count > 0;
  }
};
