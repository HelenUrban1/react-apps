const { log } = require('../logger');
const axios = require('axios');
const FormData = require('form-data');
const config = require('../../config')();
const { sanitize, sequence, interpret } = require('@ideagen/engineering-notation-parser');
const { defaultFeature } = require('../staticData/defaults');
const ClassifierService = require('./classifierService');
const ToleranceService = require('./toleranceService');

module.exports = class OcrService {
  constructor() {
    this.classifier = new ClassifierService();
    this.tolerancer = new ToleranceService();
  }
  /**
   * Sends an image to the EDA for OCR extraction.
   * @param {Buffer} imageData
   * @param {Boolean} isGdt
   * @param {"dimension" | "text"} mode
   * @returns {Promise<Object>} The text extracted using both tesseract and pytorch models.
   */
  async sendToOcr(imageData, isGdt, mode) {
    log.debug(`EDA: ${config.edaUrl}`);
    try {
      let type = 0;
      if (isGdt) {
        type = 1;
      }
      if (mode === 'text') {
        type = 2;
      }
      const form = new FormData();
      form.append('api_key', 6);
      form.append('thresh', 165);
      form.append('type', type);
      form.append('name', 'capture.png');
      form.append('image', imageData, 'capture.png');

      const response = await axios({
        method: 'post',
        url: config.edaUrl,
        data: form,
        withCredentials: false,
        timeout: config.edaTimeout,
      });
      return response.data;
    } catch (error) {
      log.error(`Error getting OCR from EDA: ${error.message}`);
      return { error };
    }
  }

  /**
   * Parses both the Pytorch and Tesseract values from Engineering drawing analyzer and compares them to determine the successful values.
   * @param {Object} ocrRes
   * @param {string} ocrRes.tess
   * @param {string} ocrRes.ocr
   * @param {*} ocrRes.error
   * @param {Object} [textRes]
   * @param {string} textRes.tess
   * @param {string} textRes.ocr
   * @param {*} textRes.error
   * @returns {Promise<Object>} The parsed characteristic, the input ocr response and the captureError
   */
  async compareOCRParseResults(ocrRes, textRes) {
    let parsed = null;
    let sanitizedText;
    let responseError;
    let captureError = null;
    let successfulModel = 'ocr';
    let tessTextResult;
    let pytorchTextResult;
    let tessOcrResult;
    let pytorchOcrResult;
    let pytorchTextResponse;
    let tessTextResponse;
    let pytorchOcrResponse;
    let tessOcrResponse;

    try {
      // Handle failure of the OCR service to extract text from the image
      if (!ocrRes || ocrRes.error || ocrRes.ocr.length === 0) {
        if (ocrRes?.error) responseError = ocrRes.error;
        if (textRes?.error) responseError = textRes.error;
        // textRes will come back as success for empty captures so we could use that in the future for error handling
        if (ocrRes.error?.message === 'Network Error' && textRes?.error?.message === 'Network Error') {
          throw new Error('Network Error');
        } else {
          throw new Error('Empty Capture');
        }
      }

      // set up results objects for ocr
      pytorchOcrResult = {
        raw: ocrRes.ocr,
        sanitized: ocrRes.ocr.replace(/^\n|\n$/g, ''),
      };
      tessOcrResult = {
        raw: ocrRes.tess,
        sanitized: ocrRes.tess.replace(/^\n|\n$/g, ''),
      };

      if (textRes) {
        // If text is needed set up those objects and run the notation parser.
        pytorchTextResult = {
          raw: textRes.ocr,
          sanitized: textRes.ocr.replace(/^\n|\n$/g, ''),
        };

        tessTextResult = {
          raw: textRes.tess,
          sanitized: textRes.tess.replace(/^\n|\n$/g, ''),
        };

        [pytorchTextResponse, tessTextResponse, pytorchOcrResponse, tessOcrResponse] = await Promise.all([
          this.interpretNotationText(pytorchTextResult?.sanitized),
          this.interpretNotationText(tessTextResult?.sanitized),
          this.interpretNotationText(pytorchOcrResult.sanitized),
          this.interpretNotationText(tessOcrResult.sanitized),
        ]);
      } else {
        // Run the plain notation parser with just ocr
        [pytorchOcrResponse, tessOcrResponse] = await Promise.all([this.interpretNotationText(pytorchOcrResult.sanitized), this.interpretNotationText(tessOcrResult.sanitized)]);
      }

      // Parse text from tesseract result
      if (pytorchTextResponse && !pytorchTextResponse.error) {
        // If pytorch text response doesn't fail pass it on
        sanitizedText = pytorchTextResult?.sanitized;
        // Parsing succeded, so assign the response to parsed
        parsed = pytorchTextResponse;
        successfulModel = 'ocrText';
      } else if (tessTextResponse && !tessTextResponse.error) {
        // If tess text response doesn't fail pass it on
        sanitizedText = tessTextResult?.sanitized;
        // Parsing succeded, so assign the response to parsed
        parsed = tessTextResponse;
        successfulModel = 'tessText';
      } else if (pytorchOcrResponse && !pytorchOcrResponse.error) {
        // If pytorch dimension response doesn't fail pass it on
        sanitizedText = pytorchOcrResult.sanitized;
        // Parsing succeded, so assign the response to parsed
        parsed = pytorchOcrResponse;
        successfulModel = 'ocrDimension';
      } else {
        // last one to check is tessOcrResponse.
        // If this fails handle the errors
        // Check if both models failed to produce a notation, throw an error so capture will be treated as a Note
        if (tessOcrResponse.error) responseError = tessOcrResponse.error;

        sanitizedText = tessOcrResult.sanitized;
        // Parsing succeded, so assign the response to parsed
        parsed = tessOcrResponse;
        successfulModel = 'tessDimension';
      }
    } catch (err) {
      log.warn('ParseTextController.compareOCRParseResults', responseError, err);

      successfulModel = 'none';

      const errTips = {
        'Empty Capture': 'No text was found, adjust the capture box to try again.',
        'Notation Interpreter requires text as input': 'No text was found, adjust the capture box to try again.',
        'Saved as a Note': "We couldn't recognize this feature type, it's been saved as a Note.",
        'Network Error': 'Our network had an issue, adjust the capture box to try again.',
      };
      captureError = JSON.stringify({
        message: err.message,
        description: errTips[err.message],
      });

      // If an error occurred, send back
      // if sanitized text exists then we can use that. Not sure what edge case this hits but it was here before so I'll leave it.
      if (sanitizedText) {
        parsed = {
          input: sanitizedText,
          notation: {
            type: 'note',
            text: sanitizedText,
          },
          error: responseError,
        };
      }
      // if sanitized text is undefined and the text response was called (!isGdtOrBasic)
      // return the text input. This is usually correct, it just fails parsing.
      else if (!sanitizedText && pytorchTextResponse?.input) {
        parsed = {
          input: pytorchTextResponse.input,
          notation: {
            type: 'note',
            text: pytorchTextResponse.input,
          },
          error: responseError,
        };
      }
      // If the text response doesn't exist then it is gdt or basic so use that model
      else {
        parsed = {
          input: pytorchOcrResponse?.input,
          notation: {
            type: 'note',
            text: pytorchOcrResponse?.input,
          },
          error: responseError,
        };
      }
    }

    return { parsed, ocrRes: ocrRes.ocr, captureError };
  }

  /**
   * Takes a string and uses the Engineeing Notation Parser to try and parse it.
   * @param {string} text
   * @returns {Promise<Object>} The parsed text.
   */
  async interpretNotationText(text) {
    return interpret(sequence(sanitize(text)));
  }

  /**
   * A function to determine if the characteristic tolerance source needs updating.
   * @param {ParsedCharacteristic} change
   * @param {ParsedCharacteristic} [feature]
   * @returns {boolean}
   */
  shouldUpdateToleranceSource(change, feature) {
    if (!feature) {
      return true;
    }
    return change.plusTol !== feature.plusTol || change.minusTol !== feature.minusTol || change.upperSpecLimit !== feature.upperSpecLimit || change.lowerSpecLimit !== feature.lowerSpecLimit;
  }

  /**
   * Parse feature from full specification.
   * @param {Object} options - The options object.
   * @param {OCRResults} [options.ocrRes] - The OCR results.
   * @param {OCRResults} [options.textRes] - The text results.
   * @param {Characteristic} [options.oldFeature] - The old feature.
   * @param {Tolerance} options.tolerances - The tolerances.
   * @param {ListObject} options.types - The types.
   * @returns {Promise<Object>} The feature, a characteristic input object and if it is a new feature.
   */
  async parseFeatureFromFullSpec({ ocrRes, textRes, fullSpecification, oldFeature, tolerances, types }) {
    // Parse Notation
    let parsed;
    if (fullSpecification) {
      // re-parse updated full specification
      parsed = await this.interpretNotationText(fullSpecification);
    } else if (ocrRes) {
      // parse OCR results, choosing the best model
      const obj = await this.compareOCRParseResults(ocrRes, textRes);
      parsed = { ...obj.parsed, captureError: obj.captureError };
    } else {
      log.error('Cannot parse feature without OCR result or new full specification');
      return null;
    }
    let feature = {
      ...oldFeature,
    };
    // reset feature  fields
    delete feature.gdtPrimaryToleranceZone;
    delete feature.gdtSecondaryToleranceZone;
    delete feature.gdtPrimaryDatum;
    delete feature.gdtSecondaryDatum;
    delete feature.gdtTertiaryDatum;
    delete feature.gdtSymbol;
    delete feature.nominal;
    delete feature.plusTol;
    delete feature.minusTol;
    delete feature.upperSpecLimit;
    delete feature.lowerSpecLimit;
    (parsed.notation.type && parsed.notation.type === 'note') || (parsed.notation.type && parsed.notation.type === 'unknown') ? (feature.toleranceSource = 'N_A') : (feature.toleranceSource = 'Default_Tolerance');

    // Classify Notation
    const classified = this.classifier.parseInterpretedNotation({ interpreted: parsed, types });
    feature = { ...feature, ...classified };
    // Apply Tolerances
    const measurement = this.classifier.getListMeasurementById({
      list: types,
      value: feature.notationSubtype,
      parentId: feature.notationType,
    });

    if (measurement !== 'Note') {
      const type = this.classifier.getListNameById({ list: types, value: feature.notationType });
      const isGDT = type === 'Geometric Tolerance';
      feature = this.tolerancer.assignFeatureTolerances({
        feature,
        tolerances,
        measurement,
        isGDT,
      });
    }

    feature.captureError = '';
    if (parsed.error) {
      // Feature was a note with a parsing error
      try {
        feature.toleranceSource = 'N_A';
        feature.notationClass = 'N_A';
        feature.captureError = parsed.captureError;

        if (parsed.error?.name === 'Lexing Errors' || parsed.error?.name === 'Parsing Errors' || parsed?.notation?.type === 'unknown') {
          throw new Error('Saved as a Note');
        }
      } catch (err) {
        // catch err
        const errTips = {
          'Empty Capture': 'No text was found, adjust the capture box to try again.',
          'Notation Interpreter requires text as input': 'No text was found, adjust the capture box to try again.',
          'Saved as a Note': "We couldn't recognize this feature type, it's been saved as a Note.",
          'Network Error': 'Our network had an issue, adjust the capture box to try again.',
        };
        feature.captureError = JSON.stringify({
          message: err.message,
          description: errTips[err.message],
        });
      }
    }

    if (!this.shouldUpdateToleranceSource(feature, oldFeature)) {
      feature.toleranceSource = oldFeature?.toleranceSource;
    }

    const isNewFeature = !feature.id;
    if (!isNewFeature && feature?.markerSubIndex !== undefined && feature.markerSubIndex >= 0) {
      feature.quantity = oldFeature?.quantity || 1;
    }
    if (!isNewFeature && !feature?.nominal) {
      feature.nominal = '';
    }
    const input = this.createCharacteristicInput(feature);

    return { feature, input, isNewFeature };
  }

  /**
   * Creates the characteristic object to insert into the the database.
   * @param {Characteristic} feature
   * @returns {Characteristic} Characteristic object to insert into the database
   */
  createCharacteristicInput(feature) {
    const item = { ...defaultFeature, ...feature, reviewerTaskId: 'none', createdAt: new Date(), updatedAt: new Date(), deletedAt: new Date() };
    const newFeature = {
      drawingSheetIndex: item.drawingSheetIndex,
      previousRevision: item.previousRevision,
      originalCharacteristic: item.originalCharacteristic,
      status: item.status,
      captureMethod: item.captureMethod,
      captureError: item.captureError,
      notationType: item.notationType,
      notationSubtype: item.notationSubtype,
      notationClass: item.notationClass,
      fullSpecification: item.fullSpecification || '',
      quantity: item.quantity,
      nominal: item.nominal || '',
      upperSpecLimit: item.upperSpecLimit,
      lowerSpecLimit: item.lowerSpecLimit,
      plusTol: item.plusTol,
      minusTol: item.minusTol,
      unit: item.unit,
      gdtSymbol: item.gdtSymbol,
      gdtPrimaryToleranceZone: item.gdtPrimaryToleranceZone,
      gdtSecondaryToleranceZone: item.gdtSecondaryToleranceZone,
      gdtPrimaryDatum: item.gdtPrimaryDatum,
      gdtSecondaryDatum: item.gdtSecondaryDatum,
      gdtTertiaryDatum: item.gdtTertiaryDatum,
      inspectionMethod: item.inspectionMethod,
      criticality: item.criticality,
      notes: item.notes,
      drawingRotation: item.drawingRotation,
      drawingScale: item.drawingScale,
      boxLocationY: item.boxLocationY,
      boxLocationX: item.boxLocationX,
      boxWidth: item.boxWidth,
      boxHeight: item.boxHeight,
      boxRotation: item.boxRotation,
      markerGroup: item.markerGroup,
      markerGroupShared: item.markerGroupShared,
      markerIndex: item.markerIndex,
      markerSubIndex: item.markerSubIndex,
      markerLabel: item.markerLabel,
      markerStyle: item.markerStyle,
      markerLocationX: item.markerLocationX,
      markerLocationY: item.markerLocationY,
      markerFontSize: item.markerFontSize,
      markerSize: item.markerSize,
      markerRotation: item.markerRotation || 0,
      gridCoordinates: item.gridCoordinates,
      balloonGridCoordinates: item.balloonGridCoordinates,
      connectionPointGridCoordinates: item.connectionPointGridCoordinates,
      toleranceSource: item.toleranceSource,
      operation: item.operation,
      part: item.part?.id,
      drawing: item.drawing?.id,
      drawingSheet: item.drawingSheet?.id,
      verified: item.verified,
      fullSpecificationFromOCR: item.fullSpecificationFromOCR,
      confidence: item.confidence,
      connectionPointLocationX: item.connectionPointLocationX,
      connectionPointLocationY: item.connectionPointLocationY,
      connectionPointIsFloating: item.connectionPointIsFloating,
      displayLeaderLine: item.displayLeaderLine,
      leaderLineDistance: item.leaderLineDistance,
    };
    return newFeature;
  }
};
