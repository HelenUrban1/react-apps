const SettingRepository = require('../database/repositories/settingRepository');
const SequelizeRepository = require('../database/repositories/sequelizeRepository');

/**
 * Handles Setting operations
 */
module.exports = class SettingService {
  constructor({ currentUser, language }) {
    this.repository = SettingRepository;
    this.currentUser = currentUser;
    this.language = language;
  }

  /**
   * Creates a Setting.
   *
   * @param {*} data
   */
  async create(data) {
    const transaction = await SequelizeRepository.createTransaction(this.currentUser.accountId);

    try {
      const record = await this.repository.create(data, {
        transaction,
        currentUser: this.currentUser,
      });
      await SequelizeRepository.commitTransaction(transaction);

      return record;
    } catch (error) {
      await SequelizeRepository.rollbackTransaction(transaction);
      throw error;
    }
  }

  /**
   * Updates a Setting.
   *
   * @param {*} id
   * @param {*} data
   */
  async update(id, data) {
    const transaction = await SequelizeRepository.createTransaction(this.currentUser.accountId);

    try {
      const record = await this.repository.update(id, data, {
        transaction,
        currentUser: this.currentUser,
      });

      await SequelizeRepository.commitTransaction(transaction);

      return record;
    } catch (error) {
      await SequelizeRepository.rollbackTransaction(transaction);
      throw error;
    }
  }

  /**
   * Destroy all Setting with those ids.
   *
   * @param {*} ids
   */
  async destroyAll(ids) {
    const transaction = await SequelizeRepository.createTransaction(this.currentUser.accountId);

    try {
      const destroys = [];
      for (let i = 0; i < ids.length; i++) {
        const id = ids[i];
        destroys.push(
          this.repository.destroy(id, {
            transaction,
            currentUser: this.currentUser,
          })
        );
      }
      await Promise.all(destroys);

      await SequelizeRepository.commitTransaction(transaction);
    } catch (error) {
      await SequelizeRepository.rollbackTransaction(transaction);
      throw error;
    }
  }

  /**
   * Finds the Setting by Id.
   *
   * @param {*} id
   */
  async findById(id) {
    return this.repository.findById(id);
  }

  /**
   * Finds most specific setting by name.
   * If user is logged in, will look for user setting,
   * then site setting, then account setting, then global setting,
   * returning the first match it finds.
   * @param {String} name
   */
  async findByName(name) {
    return this.repository.findByName(name, {
      currentUser: this.currentUser,
    });
  }

  /**
   * Finds Settings based on the query.
   *
   * @param {*} args
   */
  async findAndCountAll(args) {
    return this.repository.findAndCountAll(args, {
      currentUser: this.currentUser,
    });
  }
};
