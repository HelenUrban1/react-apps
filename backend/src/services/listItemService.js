const ListItemRepository = require('../database/repositories/listItemRepository');
const SequelizeRepository = require('../database/repositories/sequelizeRepository');

/**
 * Handles ListItem operations
 */
module.exports = class ListItemService {
  constructor({ currentUser, language }) {
    this.repository = ListItemRepository;
    this.currentUser = currentUser;
    this.language = language;
  }

  /**
   * Creates a ListItem.
   *
   * @param {*} data
   */
  async create(data) {
    const transaction = await SequelizeRepository.createTransaction(this.currentUser.accountId);

    try {
      const record = await this.repository.create(data, {
        transaction,
        currentUser: this.currentUser,
      });

      await SequelizeRepository.commitTransaction(transaction);

      return record;
    } catch (error) {
      await SequelizeRepository.rollbackTransaction(transaction);
      throw error;
    }
  }

  /**
   * Updates a ListItem.
   *
   * @param {*} id
   * @param {*} data
   */
  async update(id, data) {
    const transaction = await SequelizeRepository.createTransaction(this.currentUser.accountId);

    try {
      const record = await this.repository.update(id, data, {
        transaction,
        currentUser: this.currentUser,
      });

      await SequelizeRepository.commitTransaction(transaction);

      return record;
    } catch (error) {
      await SequelizeRepository.rollbackTransaction(transaction);
      throw error;
    }
  }

  /**
   * Destroy all ListItem with those ids.
   *
   * @param {*} ids
   */
  async destroyAll(ids) {
    const transaction = await SequelizeRepository.createTransaction(this.currentUser.accountId);

    try {
      const destroys = [];
      for (let i = 0; i < ids.length; i++) {
        const id = ids[i];
        destroys.push(
          this.repository.destroy(id, {
            transaction,
            currentUser: this.currentUser,
          })
        );
      }
      await Promise.all(destroys);

      await SequelizeRepository.commitTransaction(transaction);
    } catch (error) {
      await SequelizeRepository.rollbackTransaction(transaction);
      throw error;
    }
  }

  /**
   * Finds the ListItem by Id.
   *
   * @param {*} id
   */
  async findById(id) {
    return this.repository.findById(id);
  }

  /**
   * Finds all list items for a list type.
   * If user is logged in, will include items added by the
   * user and by the user's site and account.
   * @param {String} name
   */
  async findByType(type) {
    return this.repository.findByType(type, {
      currentUser: this.currentUser,
    });
  }

  /**
   * Finds ListItem based on the query.
   *
   * @param {*} args
   */
  async findAndCountAll(args) {
    return this.repository.findAndCountAll(args, {
      currentUser: this.currentUser,
    });
  }
};
