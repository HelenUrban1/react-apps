const moment = require('moment');
const PartRepository = require('../database/repositories/partRepository');
const DrawingRepository = require('../database/repositories/drawingRepository');
const DrawingSheetRepository = require('../database/repositories/drawingSheetRepository');
const CharacteristicRepository = require('../database/repositories/characteristicRepository');
const SequelizeRepository = require('../database/repositories/sequelizeRepository');
const ReportRepository = require('../database/repositories/reportRepository');
const { log } = require('../logger');

/**
 * Handles Revision operations
 */
module.exports = class RevisionService {
  constructor({ currentUser, language }) {
    this.partRepository = new PartRepository();
    this.drawingRepository = new DrawingRepository();
    this.drawingSheetRepository = new DrawingSheetRepository();
    this.characteristicRepository = new CharacteristicRepository();
    this.reportRepository = new ReportRepository();
    this.currentUser = currentUser;
    this.language = language;
  }

  nextLetter(letter) {
    if (letter.toUpperCase() === 'Z') {
      return 'ZA';
    }
    return String.fromCharCode(letter.toUpperCase().charCodeAt(0) + 1);
  }

  guessNextRevision(revision) {
    if (!revision || moment(revision, 'YYYY-MM-DD', true).isValid()) {
      return moment().local().format('YYYY-MM-DD');
    }
    const letterSequence = new RegExp(/[A-Z]+$/g);
    const numberSequence = new RegExp(/[0-9]+$/g);
    if (letterSequence.test(revision)) {
      const current = revision.match(letterSequence);
      if (!current) {
        log.debug('no match found');
        return revision;
      }
      const next = this.nextLetter(current[0][current[0].length - 1]);
      return revision.substring(0, revision.length - 1) + next;
    }
    const current = revision.match(numberSequence);
    if (!current) {
      log.debug('no match found');
      return revision;
    }
    return revision.replace(current[0], '') + (parseFloat(current[0]) + 1).toString();
  }

  async duplicateDrawingSheets(part, originalDrawings, options) {
    log.debug('Cloning drawings sheets...');
    const sheetPromises = [];
    originalDrawings.forEach((originalDrawing) => {
      const copiedDrawing = part.drawings.find((d) => d.dataValues.previousRevision === originalDrawing.id);
      originalDrawing.sheets.forEach((originalSheet) => {
        const sheetInput = DrawingSheetRepository.convertToInput({
          ...originalSheet.dataValues,
          part,
          drawing: copiedDrawing,
          previousRevision: originalSheet.id,
          characteristics: [], // will get associated later
        });
        sheetPromises.push(this.drawingSheetRepository.create(sheetInput, options));
      });
    });

    const sheets = [];
    await Promise.all(sheetPromises).then((results) => {
      results.forEach((r) => sheets.push(r));
    });

    return sheets;
  }

  async duplicateDrawings(drawings, options) {
    log.debug('Cloning drawings...');
    return Promise.all(
      drawings
        .filter((drawing) => !drawings.some((draw) => draw.id === drawing.nextRevision))
        .map(async (drawing) => {
          const d = await this.drawingRepository.findById(drawing.id, options);
          const drawingInput = DrawingRepository.convertToInput({ ...d, part: null, previousRevision: drawing.id, sheets: [], characteristics: [] });
          return this.drawingRepository.create(drawingInput, options);
        })
    );
  }

  async duplicateCharacteristics(characteristics, part, drawings, sheets, options) {
    log.debug('Cloning characteristics...');
    return Promise.all(
      characteristics.map(async (char) => {
        const drawing = drawings.find((d) => d.previousRevision === char.drawing.id);
        const drawingSheets = sheets.filter((s) => s.drawingId === drawing.id).sort((a, b) => a.pageIndex - b.pageIndex);
        const sheet = drawingSheets.find((s) => s.pageIndex === char.drawingSheetIndex - 1) || drawingSheets[drawingSheets.length - 1];

        // Check if the characteristic is off the new page
        // If it is, move it to the edge of the drawing, ignore markerLocation because its polar coords
        const newBoxX = sheet && char.boxLocationX >= sheet.width ? sheet.width - char.boxWidth : char.boxLocationX;
        const newBoxY = sheet && char.boxLocationY >= sheet.height ? sheet.height - char.boxHeight : char.boxLocationY;
        const newBoxDiffX = char.boxLocationX - newBoxX;
        const newBoxDiffY = char.boxLocationY - newBoxY;
        const newConnectionPointX = newBoxDiffX > 0 ? char.connectionPointLocationX - newBoxDiffX : char.connectionPointLocationX;
        const newConnectionPointY = newBoxDiffY > 0 ? char.connectionPointLocationY - newBoxDiffY : char.connectionPointLocationY;
        const moved = newBoxDiffY > 0 || newBoxDiffX > 0 || (sheet && sheet.pageIndex + 1 !== char.drawingSheetIndex);
        const featureInput = CharacteristicRepository.convertToInput({
          ...char.dataValues,
          part,
          drawing: drawing || char.drawing,
          drawingSheet: sheet || char.drawingSheet,
          drawingSheetIndex: sheet && sheet.pageIndex >= 0 ? sheet.pageIndex + 1 : char.drawingSheetIndex,
          previousRevision: char.id,
          boxLocationX: newBoxX,
          boxLocationY: newBoxY,
          connectionPointLocationX: newConnectionPointX,
          connectionPointLocationY: newConnectionPointY,
          verified: moved ? false : char.verified,
          captureError: moved ? JSON.stringify({ message: 'Feature moved', description: 'This feature was outside the boundaries of the revised drawing.' }) : char.captureError,
        });
        return this.characteristicRepository.create(featureInput, options);
      })
    );
  }

  async duplicatePart(id, partRecord, drawings, options) {
    log.debug('Cloning part...');
    const partInput = PartRepository.convertToInput({
      ...partRecord,
      revision: this.guessNextRevision(partRecord.revision),
      originalPart: partRecord.originalPart || id,
      previousRevision: id,
      drawings,
      primaryDrawing: drawings.find((d) => d.previousRevision === partRecord.primaryDrawing.id) || drawings[0],
      characteristics: [], // will get assigned later
      reports: [], // reports shouldn't be copied to the new part
    });

    // add attachments
    let attachedFiles = [];
    if (partRecord.attachments) {
      attachedFiles = partRecord.attachments.map((file) => {
        return {
          id: file.id,
          name: file.name,
          sizeInBytes: file.sizeInBytes,
          publicUrl: file.publicUrl,
          privateUrl: file.privateUrl,
          new: true,
        };
      });
    }

    partInput.attachments = attachedFiles;

    return this.partRepository.create(partInput, options);
  }

  /**
   * Creates a Part Revision
   *
   * @param {string} id
   */
  async createPartRevision(id) {
    const transaction = await SequelizeRepository.createTransaction(this.currentUser.accountId);
    const options = { transaction, currentUser: this.currentUser };

    try {
      const partRecord = await this.partRepository.findById(id, options);
      log.debug(`RevisionService.createPartRevision`);

      if (!partRecord || !partRecord.drawings) {
        throw new Error(`Missing part ${!!partRecord} drawings ${partRecord.drawings}`);
      }

      const currentPartDrawings = partRecord.drawings.filter((d) => d.nextRevision === null);

      // Duplicate Drawings
      const copiedDrawingRecords = await this.duplicateDrawings(currentPartDrawings, options);

      // Duplicate the part
      const copiedPartRecord = await this.duplicatePart(id, partRecord, copiedDrawingRecords, options);

      // Duplicate the sheets
      const copiedDrawingSheetRecords = await this.duplicateDrawingSheets(copiedPartRecord, currentPartDrawings, options);

      // Duplicate Features
      let copiedCharacteristicRecords = [];
      if (partRecord.characteristics.length > 0) {
        copiedCharacteristicRecords = await this.duplicateCharacteristics(
          partRecord.characteristics.filter((c) => c.nextRevision === null),
          copiedPartRecord,
          copiedDrawingRecords,
          copiedDrawingSheetRecords,
          options
        );
      }

      await SequelizeRepository.commitTransaction(transaction);

      return { part: copiedPartRecord, drawings: copiedDrawingRecords, sheets: copiedDrawingSheetRecords, characteristics: copiedCharacteristicRecords };
    } catch (error) {
      await SequelizeRepository.rollbackTransaction(transaction);
      throw error;
    }
  }

  /**
   * Creates a Drawing Revision
   *
   * @param {*} data
   */
  async createDrawingRevision(drawingId, partId) {
    log.debug('RevisionService.createDrawingRevision');
    const transaction = await SequelizeRepository.createTransaction(this.currentUser.accountId);
    const options = { transaction, currentUser: this.currentUser };

    try {
      const originalDrawingRecord = await this.drawingRepository.findById(drawingId, options);
      const revisedDrawingRecord = await this.drawingRepository.findById(originalDrawingRecord.nextRevision, options);
      const partRecord = await this.partRepository.findById(partId, options);

      // Duplicate Features from the Drawing only
      let copiedCharacteristicRecords = [];
      if (originalDrawingRecord.characteristics.length > 0) {
        copiedCharacteristicRecords = await this.duplicateCharacteristics(originalDrawingRecord.characteristics, partRecord, [revisedDrawingRecord], revisedDrawingRecord.sheets, options);
      }

      // Get Updated Drawing record
      const updatedDrawing = await this.drawingRepository.findById(revisedDrawingRecord.id, options);

      // Update Part with new drawings
      const partInput = PartRepository.convertToInput(partRecord);
      partInput.drawings = [...partRecord.drawings.map((d) => d.id), revisedDrawingRecord.id];
      if (revisedDrawingRecord.previousRevision === partInput.primaryDrawing) partInput.primaryDrawing = revisedDrawingRecord.id;
      const updatedPart = await this.partRepository.update(partRecord.id, partInput, options);

      await SequelizeRepository.commitTransaction(transaction);

      return { part: updatedPart, drawing: updatedDrawing, sheets: updatedDrawing.sheets, characteristics: copiedCharacteristicRecords };
    } catch (error) {
      await SequelizeRepository.rollbackTransaction(transaction);
      throw error;
    }
  }

  /**
   * Deletes a Part, including all related revisions,
   * drawings and reports
   *
   * @param {string} id The part id
   */
  async deletePart(id) {
    log.debug('RevisionService.deletePart');
    const transaction = await SequelizeRepository.createTransaction(this.currentUser.accountId);
    const options = { transaction, currentUser: this.currentUser };

    try {
      const part = await this.partRepository.findById(id, options);
      const filter = { originalPart: part.originalPart };
      const revisions = await this.partRepository.findAndCountAll({ filter }, options);

      await Promise.all(
        revisions.rows.map(async (revision) => {
          // Set this revision to deleted
          await this.partRepository.markAsDeleted(revision.id, options);

          // Set all related drawings to deleted
          if (revision.drawings) {
            await Promise.all(
              revision.drawings.map(async (drawing) => {
                await this.drawingRepository.markAsDeleted(drawing.id, options);
              })
            );
          }

          // Set all related reports to deleted
          if (revision.reports) {
            await Promise.all(
              revision.reports.map(async (report) => {
                await this.reportRepository.markAsDeleted(report.id, options);
              })
            );
          }
        })
      );

      await SequelizeRepository.commitTransaction(transaction);
    } catch (error) {
      await SequelizeRepository.rollbackTransaction(transaction);
      throw error;
    }
  }
};
