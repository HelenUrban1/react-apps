/**
 * I18n dictionary for the en.
 */

const en = {
  app: {
    companyName: 'InspectionXpert Corporation a wholly owned subsidiary of Ideagen Inc.',
    title: 'Ideagen Quality Control',
    logoAlt: 'Ideagen Quality Control Logo',
    street: 'PO Box 991',
    street2: '',
    city: 'Blacksburg, VA',
    zip: '24063',
    orderPhone: '(888) 984-3199 ext. 3',
    supportPhone: '(984) 275-3449',
  },

  auth: {
    userDisabled: 'Your account is disabled',
    userNotFound: `Sorry, we don't recognize your credentials`,
    wrongPassword: `Sorry, we don't recognize your credentials`,
    weakPassword: 'This password is too weak',
    emailAlreadyInUse: 'Email is already in use',
    accountAlreadyExists: 'Email domain is already in use',
    invalidEmail: 'Please provide a valid email',
    userMembershipDisabled: 'Your account membership with {0} is disabled, please contact your account administrator to regain access.',
    userMembershipPending: 'Your account membership with {0} is pending, please contact your account administrator to become approved.',
    userMembershipRequesting: 'Your access request to {0} is pending, please contact your account administrator to become approved.',
    userMembershipRequestFailed: 'Unable to accept request',
    userMembershipAcceptFailed: 'Unable to reject request',
    userInviteExpired: 'Your invitation to {0} is expired, please contact your account administrator to receive a new invitation.',
    userMembershipError: 'Unable to find account membership',
    userMembershipActive: 'Membership is already active',
    userPasswordResetRequired: 'Password reset required. check email for link.',
    passwordReset: {
      expiredToken: 'Reset token is expired, please check your email for a new one.',
      invalidToken: 'Password reset link is invalid or has expired',
      unverified: 'Registration incomplete, please check your email to finish.',
      error: `Email not recognized`,
    },
    emailAddressVerificationEmail: {
      invalidToken: 'Email verification link is invalid or has expired',
      error: `Email not recognized`,
    },
    inviteCancelled: {
      invalidToken: 'Invitation to this user has been cancelled.',
      error: `Email not recognized`,
    },
    inviteExpired: {
      invalidToken: 'Invitation link expired. Please ask your organization admin to resend your invitation.',
      error: `Email not recognized`,
    },
    cognito: {
      LimitExceededException: 'Attempt limit exceeded, please try again later.',
      UserNotFoundException: 'User not found',
      CodeMismatchException: "Sorry, we don't recognize your credentials",
      NotAuthorizedException: "Sorry, we don't recognize your credentials",
      ExpiredCodeException: 'Code has expired',
      cognitoError: 'Unexpected error, please contact support.',
    },
  },

  iam: {
    errors: {
      createUser: 'Could not create user',
      userAlreadyExists: 'User with this email already exists',
      userNotFound: 'User not found',
      accountMemberNotFound: 'Account Member not Found',
      profileNotFound: 'User Profile not Found',
      disablingHimself: `You can't disable yourself`,
      revokingOwnPermission: `You can't revoke your own admin permission`,
      failedRegister: 'Could not register new user. Please check provided information.',
      escalationOfPrivilege: `You can't give permissions higher than your own.`,
    },
  },

  importer: {
    errors: {
      invalidFileEmpty: 'The file is empty',
      invalidFileExcel: 'Only excel (.xlsx) files are allowed',
      invalidFilePdf: 'Only pdf files are allowed',
      invalidFileImg: 'Only image files (.png, .jpg, .jpeg) are allowed',
      invalidFileAttachment: 'Only Attachment (.doc, .docx, .xlsx, .pdf, .txt, .csv) files are allowed',
      invalidFileUpload: 'Invalid file. Make sure you are using the last version of the template.',
      importHashRequired: 'Import hash is required',
      importHashExistent: 'Data has already been imported',
    },
  },

  errors: {
    unauthorized: {
      message: 'You are not logged in',
    },
    cors: {
      message: 'Forbidden : CORS policy does not permit origin',
    },
    forbidden: {
      message: 'Forbidden',
    },
    validation: {
      message: 'An error occurred',
      adminEmail: 'Could not find Admin email',
      multipleAccounts: 'Unable to find a unique account, please contact our support team or have your account admin invite you.',
      invalidAdmin: 'Unable to find Administrator.',
      closedPolicy: 'Account does not accept access requests, contact an Admin to request an invite.',
    },
    query: {
      iamUsers: 'Unable to retrieve account members.',
      settings: 'Unable to retrieve account settings.',
      subscription: 'Unable to retrieve subscription information.',
    },
    edit: {
      subscription: 'Failed to update your subscription, refresh and try again our contact our support team.',
      settings: 'Failed to update your settings.',
      iamUsers: 'Failed to update your account member.',
    },
  },

  account: {
    errors: {
      genericDomain: 'Unable to validate email',
      invalidDomain: 'Personal emails are not allowed, a business domain is required.',
      multipleDomain: 'Multiple accounts exist with this email domain, please contact our support team or have an account admin invite you.',
      genericCreate: 'Error creating an account',
    },
  },

  accountMembers: {
    errors: {
      userAndAccountRequired: 'The user and account values are required for creating an account member record.',
      recordExists: 'An account member record already exists for this user and account.',
      noCurrentUser: 'No current user is set.',
      noAccountMembershipsCurrentUser: 'The current user has no account memberships.',
      noActiveMembershipCurrentUser: 'The current user has no active account membership.',
      recordDoesNotExist: 'No account member record found.',
      currentUserMustBeAccountOwner: 'The current user must be the account admin when changing the account admin.',
      newAccountOwnerMustBeAdmin: 'The new account admin must already be an account planner.',
      accountsDoNotMatch: 'The account member to be assigned the admin role must be associated with the same account as the current admin.',
      allowOwnerSet: 'Admin role is not allowed to be set without setting allowOwnerSet option.',
      allowOwnerRemoval: 'Admin role is not allowed to be removed without setting allowOwnerRemoval option.',
      allowOwnerAddition: 'Admin role is not allowed to be added without setting allowOwnerAddition option.',
    },
  },

  parts: {
    notifications: {
      readyForReview: {
        title: `{0} is ready for review.`,
        content: `{0} features identified`,
        buttons: 'Review later,Review features',
      },
    },
  },

  emails: {
    error: {
      failed: 'Failed to send email. Please check your internet connection and contact support if the issue persists.',
    },
    invitation: {
      subject: `[Action Required] {1} invited you to {0}`,
      body: `
      <p>Hi there,</p>
      <p>{2} has invited you to join their {0} account.</p>
      {1}
      <p>This link is active for 30 minutes. </p>
      <p>Thanks,</p>
      <p>{0} Support</p>
      `,
      button: 'Join {0}',
    },
    invitationReminder: {
      subject: `[Action Required] Your Ideagen Quality Control invitation is waiting`,
      body: `
      <p>Hi there,</p>
      <p>{2} has invited you to join their {0} account. Click the link below to set up your profile.</p>
      {1}
      <p>This link is active for 30 minutes. </p>
      <p>Thanks,</p>
      <p>{0} Support</p>
      `,
      button: 'Join {0}',
    },
    request: {
      subject: `{1} has asked to join to your {0} team`,
      body: `
      <p>Hi {3},</p>
      <p>{4} has asked to join your {0} account.</p>
      <p>If you don't recognize {4}, you can reject the request.</p>
      <table align="center" border="0" width="100%" style="table-layout:fixed;">
        <tr>
          <td colspan="1"></td>
          <td colspan="2">{1}</td>
          <td colspan="2">{2}</td>
          <td colspan="1"></td>
        </tr>
      </table>
      <p>These links are active for 30 minutes.</p>
      <p>Thanks,</p>
      <p>{0} Support</p>
      `,
      button: 'Approve access',
      altButton: 'Reject request',
    },
    orphanRequestOpen: {
      subject: `[Action Required] Finish setting up your {0} account`,
      body: `
      <p>Hi {3},</p>
      <p>An {0} account already exists with the domain in your email address.</p>
      <p>Click below to request access to the existing account, or create a new account.</p>
      <table align="center" border="0" width="100%" style="table-layout:fixed;">
        <tr>
          <td colspan="1"></td>
          <td colspan="2">{1}</td>
          <td colspan="2">{2}</td>
          <td colspan="1"></td>
        </tr>
      </table>
      <p>These links are active for 30 minutes.</p>
      <p>If you believe this is an error, please let us know. Reply to this email or call our support team at 888-984-3199 ext 2. Our support hours are Monday-Friday, 8:30am-5:30pm Eastern.</p>
      <p>Thanks,</p>
      <p>{0} Support</p>
      `,
      button: 'Create new account',
      altButton: 'Request access',
    },
    orphanRequestEmail: {
      subject: `[Action Required] Finish setting up your {0} account`,
      body: `
      <p>Hi {3},</p>
      <p>An account already exists with your email domain.</p>
      <p>Click 'Request Access' complete the request form.</p>
      <p>If you believe this is an error, contact our support team.</p>
      <table align="center" border="0" width="100%" style="table-layout:fixed;">
        <tr>
          <td colspan="1"></td>
          <td colspan="2">{1}</td>
          <td colspan="2">{2}</td>
          <td colspan="1"></td>
        </tr>
      </table>
      <p>The link is active for 30 minutes. </p>
      <p>Thanks,</p>
      <p>{0} Support</p>
      `,
      button: 'Request Access',
    },
    resendClosed: {
      subject: `[Action Required] Finish setting up your {0} account`,
      body: `
      <p>Hi {2},</p>
      <p>An account already exists with your email domain and is no longer accepting requests.</p>
      <p>Begin your free access with a new domain, or contact an Administrator to request an invitation.</p>
      {1}
      <p>This link is active for 30 minutes. </p>
      <p>Thanks,</p>
      <p>{0} Support</p>
      `,
      button: 'Create new account',
    },
    requestApproved: {
      subject: `Your request to join {0} was approved`,
      body: `
      <p>Hi {2},</p>
      <p>{3} has approved your request to join the {0} team for {4}.</p>
      <p>If you don't recognize {4}, please ignore this email.</p>
      {1}
      <p>This link is active for 30 minutes.</p>
      <p>Thanks,</p>
      <p>{0} Support</p>
      `,
      button: 'Join {0}',
    },
    requestRejected: {
      subject: `Your request to join {0} was rejected`,
      body: `
      <p>Hi {1},</p>
      <p>The account administrator has rejected your request to join their {0} account.</p>
      <p>If you believe this was an error, please contact your administrator directly.</p>
      <p>Thanks,</p>
      <p>{0} Support</p>
      `,
    },
    emailAddressVerification: {
      subject: `[Action Required] Start your free access to Ideagen Quality Control`,
      body: `
      <p>Hi {2},</p>
      <p>Thanks for requesting free access to {0}. Please click the link below to confirm your email and finish setting up your account.</p>
      {1}
      <p>If you didn’t request access to {0}, please ignore this message.</p>
      <p>Thanks,</p>
      <p>{0} Support</p>
      `,
      button: 'Finish creating my account',
    },
    emailAddressVerificationReminder: {
      subject: `[Action Required] Finish creating your Ideagen Quality Control account`,
      body: `
      <p>Hi {2},</p>
      <p>Your Ideagen Quality Control account is waiting! Click the button below to finish registering and start your free access.</p>
      {1}
      <p>If you didn’t request access to {0}, please ignore this message.</p>
      <p>Thanks,</p>
      <p>{0} Support</p>
      `,
      button: 'Finish creating my account',
    },
    passwordReset: {
      subject: `[Action Required] Reset your password for {0}`,
      body: `
        <p>Hi {3},</p>
        <p>Need to reset your password? Don't worry, we've got you covered.</p>
        {1}
        <p>If the link doesn't work, copy and paste this URL in your browser:</p>
        <p>{2}</p>
        <p>We’re here to help if you need it. Call us at 888-984-3199 ext 2 or reply to this email. Our support hours are Monday-Friday, 8:30am-5:30pm Eastern.</p>
        <p>Thanks,</p>
        <p>{0} Support</p>
      `,
      button: 'Reset my password',
    },
    verifiedUser: {
      subject: `Your {0} account is verified`,
      body: `
        <p>Hi {2},</p>
        <p>Your {0} account is already verified, follow the link to sign in.</p>
        {1}
        <p>We’re here to help if you need it. Call us at 888-984-3199 ext 2 or reply to this email. Our support hours are Monday-Friday, 8:30am-5:30pm Eastern.</p>
        <p>Thanks,</p>
        <p>{0} Support</p>
      `,
      button: 'Sign in',
    },
    orphanedUser: {
      subject: `Your {0} account has been deleted`,
      body: `
        <p>Hi {1},</p>
        <p>Your {0} account has been deleted at the request of the Administrator.</p>
        <p>If you believe this was an error, please contact your administrator directly.</p>

        <p>We’re here to help if you need it. Call us at 888-984-3199 ext 2 or reply to this email. Our support hours are Monday-Friday, 8:30am-5:30pm Eastern.</p>
        <p>Thanks,</p>
        <p>{0} Support</p>
      `,
    },
    pendingAction: {
      subject: `[Action Pending] Contact your {0} Administrator`,
      body: `
      <p>Hi {1},</p>
      <p>{2}</p>
      <p>We’re here to help if you need it. Call us at 888-984-3199 ext 2 or reply to this email. Our support hours are Monday-Friday, 8:30am-5:30pm Eastern.</p>
      <p>Thanks,</p>
      <p>{0} Support</p>
      `,
    },
    paymentFailed: {
      subject: `A Payment has Failed for your {0} Subscription`,
      bodyA: `
      <p>Hi there,</p>
      <p>Looks like there was a problem with your payment on {0}.</p>
      `,
      bodyB: `
      <p>If you have any questions or concerns please feel free to respond to this email or to call us at {0}</p>
      <p>Thanks,</p>
      <p>{1} Support</p>
      `,
      billing: `<p>Please update your billing information, including credit card number, through <a href="{0}">this link</a> or in the <a href="{1}">Billing Settings</a> section of {2}.</p>`,
      settings: `<p>Please update your billing information, including credit card number, in the <a href="{0}">Billing Settings</a> section of {1}.</p>`,
      disclaimer: `This link will expire at {0}.`,
    },
  },

  stations: {
    default: 'Default',
  },

  samples: {
    sample: 'Sample',
    serial: 'Serial',
    serialNumber: 'Serial Number',
    sampleNumber: 'Sample Number',
    coverage: 'Feature Coverage',
    pass: 'Pass',
    fail: 'Fail',
    unmeasured: 'Unmeasured',
    scrap: 'Scrap',
  },
};

module.exports = en;
