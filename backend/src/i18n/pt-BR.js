/**
 * I18n dictionary for the en.
 */

const ptBR = {
  app: {
    title: 'Aplicação',
  },

  auth: {
    userDisabled: 'Sua conta está desativada',
    userNotFound: `Desculpe, não reconhecemos suas credenciais`,
    wrongPassword: `Desculpe, não reconhecemos suas credenciais`,
    weakPassword: 'Esta senha é muito fraca',
    emailAlreadyInUse: 'O email já está sendo usado',
    invalidEmail: 'Por favor forneça um email válido',
    passwordReset: {
      invalidToken: 'Link de redefinição de senha inválido ou expirado',
      error: `Email não encontrado`,
    },
    emailAddressVerificationEmail: {
      invalidToken: 'Link de verificação de email inválido ou expirado',
      error: `Email não encontrado`,
    },
  },

  iam: {
    errors: {
      userAlreadyExists: 'Usuário com este email já existe',
      userNotFound: 'Usuário não encontrado',
      disablingHimself: `Você não pode desativar-se`,
      revokingOwnPermission: `Você não pode revogar sua própria permissão de proprietário`,
      escalationOfPrivilege: `Você não pode dar permissões superiores às suas.`,
    },
  },

  importer: {
    errors: {
      invalidFileEmpty: 'O arquivo está vazio',
      invalidFileExcel: 'Apenas arquivos Excel (.xlsx) são permitidos',
      invalidFileUpload: 'Arquivo inválido. Verifique se você está usando a última versão do modelo.',
      importHashRequired: 'Hash de importação é necessário',
      importHashExistent: 'Dados já foram importados',
    },
  },

  errors: {
    validation: {
      message: 'Ocorreu um erro',
    },
  },

  accountMembers: {
    // These entries need to be localized
    errors: {
      userAndAccountRequired: 'The user and account values are required for creating an account member record.',
      recordExists: 'An account member record already exists for this user and account.',
      noCurrentUser: 'No current user is set.',
      noAccountMembershipsCurrentUser: 'The current user has no account memberships.',
      noActiveMembershipCurrentUser: 'The current user has no active account membership.',
      recordDoesNotExist: 'No account member record found.',
      currentUserMustBeAccountOwner: 'The current user must be the account admin when changing the account admin.',
      newAccountOnwerMustBeAdmin: 'The new account admin must already be an account planner.',
      accountsDoNotMatch: 'The account member to be assigned the admin role must be associated with the same account as the current admin.',
    },
  },

  emails: {
    invitation: {
      subject: `Você foi convidado para o app {0}`,
      body: `
        <p>Olá,</p>
        <p>Você foi convidado para o app {0}.</p>
        <p>Clique neste link para registrar-se.</p>
        <p><a href="{1}">{1}</a></p>
        <p>Obrigado,</p>
        <p>Equipe do app {0}</p>
      `,
    },
    emailAddressVerification: {
      subject: `Verifique seu e-mail do app {0}`,
      body: `
        <p>Olá,</p>
        <p>Clique neste link para verificar seu endereço de e-mail.</p>
        <p><a href='{0}'>{0}</a></p>
        <p>Se você não solicitou a verificação deste endereço, ignore este e-mail.</p>
        <p>Obrigado,</p>
        <p>Equipe do app {1}</p>
      `,
    },
    passwordReset: {
      subject: `Redefinir a senha do app {0}`,
      body: `
        <p>Olá,</p>
        <p>Clique neste link para redefinir a senha de login no app {0} com sua conta {1}.</p>
        <p><a href='{2}'>{2}</a></p>
        <p>Se você não solicitou a redefinição da sua senha, ignore este e-mail.</p>
        <p>Obrigado,</p>
        <p>Equipe do app {0}</p>
      `,
    },
  },
};

module.exports = ptBR;
