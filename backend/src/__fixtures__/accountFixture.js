const genericFixture = require('./genericFixture');
const AccountRepository = require('../database/repositories/accountRepository');
const config = require('../../config')();

const createRepoInstance = () => new AccountRepository();

const USER_ID = 'e0a01779-c8ed-4090-bd44-9c1b2242fb75';
const ACME_CORP_ACCOUNT_ID = 'e0a01769-c8ed-4090-bd24-9c1b2241fb75';

const data = [
  {
    id: ACME_CORP_ACCOUNT_ID,
    dbHost: config.accountDatabase.host,
    dbName: config.accountDatabase.database,
    status: 'Active',
    orgName: 'Acme Corp',
    orgPhone: '987-654-3210',
    orgEmail: 'dev.mail.monkey@gmail.com',
    orgStreet: '1 Glenwood Ave',
    orgStreet2: '',
    orgCity: 'Raleigh',
    orgRegion: 'North_Carolina',
    orgPostalcode: '27603',
    orgCountry: 'United_States',
    settings: '{}',
    createdById: USER_ID,
    updatedById: USER_ID
  },
  {
    id: '8f186c73-8de0-467e-afe9-3e3132a48de1',
    dbHost: config.accountDatabase.host,
    dbName: 'testDB',
    status: 'Active',
    orgName: 'Acme Corp Eng Org Unit',
    orgPhone: '111-000-1111',
    orgEmail: 'dev.mail.monkey@gmail.com',
    orgStreet: '1 Glenwood Ave',
    orgStreet2: '',
    orgCity: 'Raleigh',
    orgRegion: 'North_Carolina',
    orgPostalcode: '27603',
    orgCountry: 'United_States',
    orgUnitName: 'Engineering',
    orgUnitId: 'bdd06007-ac37-4df4-866c-f55f60bfcbb6',
    parentAccountId: ACME_CORP_ACCOUNT_ID,
    settings: '{}',
    createdById: USER_ID,
    updatedById: USER_ID
  }
];

const accountFixture = genericFixture({
  idField: 'id',
  createFn: (data, repoInstance) => repoInstance.create(data),
  data
});

module.exports = {
  createRepoInstance,
  data,
  accountFixture
};
