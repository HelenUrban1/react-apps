const userFixture = require('./userFixture');
const accountFixture = require('./accountFixture');
const accountMemberFixture = require('./accountMemberFixture');
const optionsFixture = require('./optionsFixture');
const partFixture = require('./partFixture');
const drawingFixture = require('./drawingFixture');
const drawingSheetFixture = require('./drawingSheetFixture');
const characteristicFixture = require('./characteristicFixture');
const operatorFixture = require('./operatorFixture');
const jobFixture = require('./jobFixture');
const markerFixture = require('./markerFixture');
const measurementFixture = require('./measurementFixture');
const reportTemplateFixture = require('./reportTemplateFixture');
const subscriptionFixture = require('./subscriptionFixture');
const reportFixture = require('./reportFixture');
const fileFixture = require('./fileFixture');
const settingFixture = require('./settingFixture');
const listItemFixture = require('./listItemFixture');
const siteFixture = require('./siteFixture');
const organizationFixture = require('./organizationFixture');
const reviewTaskFixture = require('./reviewTaskFixture');
const stationFixture = require('./stationFixture');
const sampleFixture = require('./sampleFixture');
const notificationFixture = require('./notificationFixture');
const notificationUserStatusFixture = require('./notificationUserStatusFixture');
const annotationFixture = require('./annotationFixture');

module.exports = {
  user: userFixture,
  account: accountFixture,
  accountMember: accountMemberFixture,
  options: optionsFixture,
  part: partFixture,
  drawing: drawingFixture,
  drawingSheet: drawingSheetFixture,
  characteristic: characteristicFixture,
  marker: markerFixture,
  annotation: annotationFixture,
  measurement: measurementFixture,
  operator: operatorFixture,
  reportTemplate: reportTemplateFixture,
  job: jobFixture,
  subscription: subscriptionFixture,
  report: reportFixture,
  file: fileFixture,
  organization: organizationFixture,
  setting: settingFixture,
  listItem: listItemFixture,
  site: siteFixture,
  reviewTask: reviewTaskFixture,
  station: stationFixture,
  sample: sampleFixture,
  notification: notificationFixture,
  notificationUserStatus: notificationUserStatusFixture,
};
