/**
 * Events from external systems are
 *   Submitted to ALB endpoint /webhook which triggers lambda /services/webhookConsumer
 *   Lambda /services/webhookConsumer validates signature and if valid, stores requests in SQS
 *   SQS triggers Lambda /services/webhookConsumer
 *   Lambda /services/webhookConsumer POSTs event to endpoint /api/event in <ixc-mono>/backend/src/api/index.js
 */

const chargifyEvents = [
  {
    id: 372846073,
    event: 'payment_failure',
    payload: {
      site: { id: 31615, subdomain: 'general-goods' },
      subscription: {
        id: 16090357,
        state: 'past_due',
        trial_started_at: 'Mon, 23 Jan 2017 09:59:26 EST -05:00',
        trial_ended_at: 'Mon, 23 Jan 2017 09:59:49 EST -05:00',
        activated_at: null,
        created_at: 'Mon, 23 Jan 2017 09:59:26 EST -05:00',
        updated_at: 'Mon, 30 Jan 2017 12:07:40 EST -05:00',
        expires_at: null,
        balance_in_cents: 1000,
        current_period_ends_at: 'Thu, 23 Feb 2017 09:59:49 EST -05:00',
        next_assessment_at: 'Tue, 31 Jan 2017 12:07:40 EST -05:00',
        canceled_at: null,
        cancellation_message: null,
        next_product_id: null,
        cancel_at_end_of_period: false,
        payment_collection_method: 'automatic',
        snap_day: null,
        cancellation_method: null,
        current_period_started_at: 'Mon, 23 Jan 2017 09:59:49 EST -05:00',
        previous_state: 'past_due',
        signup_payment_id: 171203419,
        signup_revenue: '0.00',
        delayed_cancel_at: null,
        coupon_code: null,
        total_revenue_in_cents: 0,
        product_price_in_cents: 1000,
        product_version_number: 1,
        payment_type: 'credit_card',
        referral_code: '4nvrr2',
        coupon_use_count: null,
        coupon_uses_allowed: null,
        product_price_point_id: 1,
        next_product_price_point_id: null,
        currency: 'USD',
        customer: {
          id: 15547334,
          first_name: 'No obligation',
          last_name: 'Bad Card',
          organization: '',
          email: 'nobbad@example.com',
          created_at: 'Mon, 23 Jan 2017 09:59:26 EST -05:00',
          updated_at: 'Mon, 23 Jan 2017 09:59:28 EST -05:00',
          reference: null,
          address: '',
          address_2: '',
          city: '',
          state: '',
          zip: '',
          country: '',
          phone: '',
          portal_invite_last_sent_at: 'Mon, 23 Jan 2017 09:59:28 EST -05:00',
          portal_invite_last_accepted_at: null,
          verified: null,
          portal_customer_created_at: 'Mon, 23 Jan 2017 09:59:28 EST -05:00',
          vat_number: '123456789',
          cc_emails: null,
          tax_exempt: false,
          parent_id: null,
        },
        product: {
          id: 4453830,
          name: 'Trial No obligation',
          handle: 'trial-no-obligation',
          description: '',
          accounting_code: '',
          request_credit_card: true,
          expiration_interval: null,
          expiration_interval_unit: 'never',
          created_at: 'Mon, 23 Jan 2017 09:53:23 EST -05:00',
          updated_at: 'Thu, 26 Jan 2017 13:35:29 EST -05:00',
          price_in_cents: 1000,
          interval: 1,
          interval_unit: 'month',
          initial_charge_in_cents: null,
          trial_price_in_cents: 0,
          trial_interval: 1,
          trial_interval_unit: 'day',
          archived_at: null,
          require_credit_card: false,
          return_params: '',
          taxable: false,
          update_return_url: 'http://www.example.com',
          initial_charge_after_trial: false,
          version_number: 1,
          update_return_params: 'id={subscription_id}&ref={customer_reference}',
          default_product_price_point_id: 1,
          product_price_point_id: 1,
          product_price_point_handle: null,
          product_family: { id: 986840, name: 'Acme Products', description: '', handle: 'acme-products', accounting_code: null },
          public_signup_pages: [{ id: 310559, return_url: null, return_params: '', url: 'https://general-goods.chargifypay.com/subscribe/ngbsvxv4hq7q/trial-no-obligation' }],
        },
        credit_card: {
          id: 10734525,
          first_name: 'Chester',
          last_name: 'Tester',
          masked_card_number: 'XXXX-XXXX-XXXX-2',
          card_type: 'bogus',
          expiration_month: 1,
          expiration_year: 2027,
          customer_id: 15547334,
          current_vault: 'bogus',
          vault_token: '2',
          billing_address: '',
          billing_city: '',
          billing_state: '',
          billing_zip: '',
          billing_country: '',
          customer_vault_token: null,
          billing_address_2: '',
          payment_type: 'credit_card',
        },
      },
      transaction: {
        id: 172084191,
        subscription_id: 16090357,
        type: 'Payment',
        kind: null,
        transaction_type: 'payment',
        success: false,
        amount_in_cents: 1000,
        memo: 'Bogus Gateway: Forced failure',
        created_at: 'Mon, 30 Jan 2017 12:07:39 EST -05:00',
        starting_balance_in_cents: 1000,
        ending_balance_in_cents: 1000,
        gateway_used: 'bogus',
        gateway_transaction_id: null,
        gateway_order_id: null,
        payment_id: null,
        product_id: 4453830,
        tax_id: null,
        component_id: null,
        statement_id: 84491989,
        customer_id: 15547334,
        card_number: 'XXXX-XXXX-XXXX-2',
        card_expiration: '01/2027',
        card_type: 'bogus',
        refunded_amount_in_cents: 0,
        invoice_uids: ['inv_9hchn3xc84mgm'],
        invoice_id: null,
        currency: 'USD',
      },
      event_id: 372846073,
    },
  },
  {
    id: 377601653,
    event: 'renewal_failure',
    payload: {
      subscription: {
        id: 16372192,
        state: 'past_due',
        trial_started_at: null,
        trial_ended_at: null,
        activated_at: 'Mon, 13 Feb 2017 11:50:57 EST -05:00',
        created_at: 'Mon, 13 Feb 2017 11:50:55 EST -05:00',
        updated_at: 'Mon, 13 Feb 2017 13:28:06 EST -05:00',
        expires_at: null,
        balance_in_cents: 16000,
        current_period_ends_at: 'Mon, 13 Mar 2017 14:28:05 EDT -04:00',
        next_assessment_at: 'Tue, 14 Feb 2017 13:28:06 EST -05:00',
        canceled_at: null,
        cancellation_message: null,
        next_product_id: null,
        cancel_at_end_of_period: false,
        payment_collection_method: 'automatic',
        snap_day: null,
        cancellation_method: null,
        current_period_started_at: 'Mon, 13 Feb 2017 13:28:05 EST -05:00',
        previous_state: 'active',
        signup_payment_id: 173961106,
        signup_revenue: '60.00',
        delayed_cancel_at: null,
        coupon_code: null,
        total_revenue_in_cents: 6000,
        product_price_in_cents: 5000,
        product_version_number: 1,
        payment_type: null,
        referral_code: 'cz8wdq',
        coupon_use_count: null,
        coupon_uses_allowed: null,
        product_price_point_id: 1,
        next_product_price_point_id: null,
        currency: 'USD',
        customer: {
          id: 15826583,
          first_name: 'Doris',
          last_name: 'Tester',
          organization: 'Acme',
          email: 'doris@example.com',
          created_at: 'Mon, 13 Feb 2017 11:50:55 EST -05:00',
          updated_at: 'Mon, 13 Feb 2017 11:50:58 EST -05:00',
          reference: '123456789',
          address: '123 Anywhere Street',
          address_2: '',
          city: 'Boston',
          state: 'MA',
          zip: '02120',
          country: 'US',
          phone: '555-555-1212',
          portal_invite_last_sent_at: 'Mon, 13 Feb 2017 11:50:58 EST -05:00',
          portal_invite_last_accepted_at: null,
          verified: null,
          portal_customer_created_at: 'Mon, 13 Feb 2017 11:50:58 EST -05:00',
          vat_number: '123456789',
          cc_emails: null,
          tax_exempt: false,
          parent_id: null,
        },
        product: {
          id: 4442358,
          name: 'Gold Product',
          handle: 'gold-product',
          description: '',
          accounting_code: '',
          request_credit_card: true,
          expiration_interval: null,
          expiration_interval_unit: 'never',
          created_at: 'Thu, 15 Dec 2016 09:32:36 EST -05:00',
          updated_at: 'Thu, 15 Dec 2016 09:32:36 EST -05:00',
          price_in_cents: 5000,
          interval: 1,
          interval_unit: 'month',
          initial_charge_in_cents: null,
          trial_price_in_cents: null,
          trial_interval: null,
          trial_interval_unit: 'month',
          archived_at: null,
          require_credit_card: true,
          return_params: '',
          taxable: false,
          update_return_url: 'http://www.example.com',
          initial_charge_after_trial: false,
          version_number: 1,
          update_return_params: 'id={subscription_id}&ref={customer_reference}',
          default_product_price_point_id: 1,
          product_price_point_id: 1,
          product_price_point_handle: null,
          product_family: { id: 986840, name: 'Acme Products', description: '', handle: 'acme-products', accounting_code: null },
          public_signup_pages: [
            { id: 306012, return_url: '', return_params: '', url: 'https://general-goods.chargifypay.com/subscribe/7dbsnjd8t8cx/gold-product' },
            { id: 310598, return_url: '', return_params: '', url: 'https://general-goods.chargifypay.com/subscribe/ksjh9py5fn5h/gold-product' },
            { id: 311132, return_url: '', return_params: '', url: 'https://general-goods.chargifypay.com/subscribe/kjmks49g8d3d/gold-product' },
          ],
        },
      },
      site: { id: 31615, subdomain: 'general-goods' },
      event_id: 377601653,
    },
  },
];

const sqsPostBodyForChargifyEvent = {
  attributes: {
    linuxTimestamp: 1626317953987,
    awsRequestId: 'db5b7e1d-1d28-4249-acc7-ce2fe4c9eb47',
    isoTimestamp: '2021-07-15T02:59:13.987Z',
    webhookSource: 'chargify',
  },
  data: chargifyEvents,
};

const sqsPostForChargifyEvent = {
  headers: {
    accept: 'application/json, text/plain, */*',
    'user-agent': 'axios/0.21.1',
    via: '1.1 4a21175361a1e842a337986b5f7399ab.cloudfront.net (CloudFront)',
    'content-type': 'application/json;charset=utf-8',
    'cloudfront-is-mobile-viewer': 'false',
    'cloudfront-is-tablet-viewer': 'false',
    'cloudfront-is-smarttv-viewer': 'false',
    'cloudfront-is-desktop-viewer': 'true',
    'cloudfront-viewer-country': 'US',
    'cloudfront-forwarded-proto': 'https',
    'x-amzn-trace-id': 'Root=1-60f0e4b0-0511f41612c9cd2f6b01c804',
    'x-amz-cf-id': 'pVrZ94erES9U6RK6V_h9rc-CkbcDNiekehT7aVjIS9oGA2_fhwbMHg==',
    'x-request-id': '7a7e77d0-e5d7-11eb-b3e7-bdbbbdf6b920',
  },
  body: sqsPostBodyForChargifyEvent,
};

module.exports = {
  chargifyEvents,
  sqsPostBodyForChargifyEvent,
  sqsPostForChargifyEvent,
};
