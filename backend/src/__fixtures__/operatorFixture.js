const genericFixture = require('./genericFixture');
const OperatorRepository = require('../database/repositories/operatorRepository');

const createRepoInstance = () => new OperatorRepository();

const data = [
  {
    id: 'b3a5ecd8-7513-499b-bd60-2f0da71d8147',
    fullName: 'John Doe',
    firstName: 'John',
    lastName: 'Doe',
    email: 'john@test.test',
    accessControl: 'None',
  },
];

const operatorFixture = genericFixture({
  idField: 'id',
  createFn: (createData, repoInstance) => repoInstance.create(createData),
  data,
});

module.exports = {
  createRepoInstance,
  data,
  operatorFixture,
};
