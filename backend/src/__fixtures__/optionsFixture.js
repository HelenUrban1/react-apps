const genericFixture = require('./genericFixture');
const OptionsRepository = require('../database/repositories/optionsRepository');

const optionsFixture = genericFixture({
  idField: 'id',
  createFn: (data) => new OptionsRepository().create(data),
  data: [
    {
      id: '1',
      // Add attributes here
    },
  ],
});

module.exports = optionsFixture;
