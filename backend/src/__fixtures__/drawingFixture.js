const genericFixture = require('./genericFixture');
const DrawingRepository = require('../database/repositories/drawingRepository');

const createRepoInstance = () => new DrawingRepository();

const data = [
  {
    id: 'c200ce70-72b8-44f7-8d3f-4b93f6d1b491',
    sheetCount: 1,
    name: 'Graytech Masterpiece',
    documentType: 'Drawing',
    number: '12345',
    revision: 'H',
    linearUnit: 'Inch',
    angularUnit: 'Degrees',
    linearTolerances: `none`,
    angularTolerances: `none`,
    markerOptions: `none`,
    tags: 'none',
    status: 'Active',
    siteId: '31ef1d56-1b86-4ffa-b886-507ed8f021c5',
    partId: 'd6c5bc49-9fb2-4fbb-8a61-77bc8d18036f',
  },
  {
    id: '1110ce70-72b8-44f7-8d3f-4b93f6d1b111',
    sheetCount: 1,
    name: 'Graytech Masterpiece',
    documentType: 'Drawing',
    number: '12345',
    revision: 'H',
    linearUnit: 'Inch',
    angularUnit: 'Degrees',
    linearTolerances: `none`,
    angularTolerances: `none`,
    markerOptions: `none`,
    tags: 'none',
    status: 'Active',
    siteId: '31ef1d56-1b86-4ffa-b886-507ed8f021c5',
    partId: 'd6c5bc49-9fb2-4fbb-8a61-77bc8d18036f',
  },
];

const drawingFixture = genericFixture({
  idField: 'id',
  createFn: (data, repoInstance) => repoInstance.create(data),
  data,
});

module.exports = {
  createRepoInstance,
  data,
  drawingFixture,
};
