const genericFixture = require('./genericFixture');
const SampleRepository = require('../database/repositories/sampleRepository');

const createRepoInstance = () => new SampleRepository();

const data = [
  {
    id: '31ef1d56-1b86-4ffa-b886-507ed8f02222',
    serial: 'Sample Fixture',
    sampleIndex: 0,
    featureCoverage: '',
    status: 'Unmeasured',
    partId: '63ff7d84-fc95-4776-a830-1b9e03d4b201',
    jobId: 'fedcd347-df93-4718-a247-440236206e5e',
    createdById: 'e0a01779-c8ed-4090-bd44-9c1b2242fb75',
    updatedById: 'e0a01779-c8ed-4090-bd44-9c1b2242fb75',
  },
];

const sampleFixture = genericFixture({
  idField: 'id',
  createFn: (data, repoInstance) => repoInstance.create(data),
  data,
});

module.exports = {
  createRepoInstance,
  data,
  sampleFixture,
};
