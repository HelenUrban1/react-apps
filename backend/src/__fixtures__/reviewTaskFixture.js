const ACME_CORP_ACCOUNT_ID = 'e0a01769-c8ed-4090-bd24-9c1b2241fb75';

const data = [
  {
    id: 'df34499f-24d2-4c5e-87ad-899a226e380d',
    status: 'Waiting',
    type: 'Capture',
    metadata: JSON.stringify({
      ocrText: '1.25 +/- 0.1',
      correctedText: '1.45 +/- 0.2',
      characteristicId: 'e0a01769-c8ed-4090-bd24-9c1b2241f123',
    }),
    processingStart: null,
    processingEnd: null,
    reviewerId: null,
    accountId: ACME_CORP_ACCOUNT_ID,
    siteId: '31ef1d56-1b86-4ffa-b886-507ed8f021c5',
    partId: 'd6c5bc49-9fb2-4fbb-8a61-77bc8d18036f',
    drawingId: 'c200ce70-72b8-44f7-8d3f-4b93f6d1b491',
  },
];

module.exports = {
  data,
};
