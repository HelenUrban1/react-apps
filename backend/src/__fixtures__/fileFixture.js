const genericFixture = require('./genericFixture');
const FileRepository = require('../database/repositories/fileRepository');

const createRepoInstance = () => new FileRepository();

const data = [
  {
    id: 'f9cbf870-b67e-4800-b09d-6d2d4d199cbf',
    name: 'Graytech Size D Drawing (1).pdf',
    sizeInBytes: 596942,
    belongsToId: 'c200ce70-72b8-44f7-8d3f-4b93f6d1b491',
    privateUrl: 'TC_06_OCR_10 - LOWER PLATE C.PDF',
    publicUrl: 'http://ixc-local.com:8080/api/download?privateUrl=TC_06_OCR_10 - LOWER PLATE C.PDF',
    belongsTo: 'drawings',
    belongsToColumn: 'file',
  },
];

const fileFixture = genericFixture({
  idField: 'id',
  createFn: (data, repoInstance) => repoInstance.create(data),
  data,
});

module.exports = {
  createRepoInstance,
  data,
  fileFixture,
};
