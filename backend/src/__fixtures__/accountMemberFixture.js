const genericFixture = require('./genericFixture');
const AccountMemberRepository = require('../database/repositories/accountMemberRepository');
const constants = require('../database/constants').accountMember;

const roles = {};
constants.ROLES.forEach((role) => (roles[role] = role));

const createRepoInstance = () => new AccountMemberRepository();

const USER_ID_A = 'a9e9a037-b16a-4da5-ada0-e4243b78bf8c';
const USER_ID_B = '94e6e846-907a-4a69-a8c6-02721b1c7fc8';
const USER_ID_C = '43fe236b-b4e0-4184-826e-a1ba122181da';
const USER_ID_D = '5aabf7ee-4ec8-49a3-8aec-1522932a5f13';
const ACCOUNT_ID_A = '2c56f6d1-993f-444f-9c9b-adcd5b6e7c76';
const ACCOUNT_ID_B = '1d50c86c-c7b6-4c21-8987-290fa3877f39';
const ACCOUNT_ID_C = '4e5bed4c-2f78-4a30-862b-6ae760c8d5d9';

const data = {
  accountAUserAPaidRoles: {
    id: 'b405a995-27b0-45bc-a98b-8188981fe658',
    settings: constants.DEFAULT_SETTINGS,
    roles: [roles.Admin],
    status: 'Active',
    accessControl: false,
    account: ACCOUNT_ID_A,
    user: USER_ID_A,
  },
  accountAUserBUnpaidRoles: {
    id: '01ba2824-2f31-466a-893c-eeec01ffce0a',
    settings: constants.DEFAULT_SETTINGS,
    roles: [roles.Billing],
    status: 'Active',
    accessControl: false,
    account: ACCOUNT_ID_A,
    user: USER_ID_B,
  },
  accountAUserCOwner: {
    id: '477663e5-509f-4418-a362-b498222d2e4b',
    settings: constants.DEFAULT_SETTINGS,
    roles: [roles.Owner],
    status: 'Active',
    accessControl: false,
    account: ACCOUNT_ID_A,
    user: USER_ID_C,
  },
  accountAUserDPaidAndUnpaidRoles: {
    id: '179cf04b-aa83-4f24-a46e-792777557df6',
    settings: constants.DEFAULT_SETTINGS,
    roles: [roles.Admin, roles.Billing],
    status: 'Active',
    accessControl: false,
    account: ACCOUNT_ID_A,
    user: USER_ID_D,
  },
  accountBUserAUnpaidRoles: {
    id: '6d912a1a-3542-407c-93d8-9bb930970c28',
    settings: constants.DEFAULT_SETTINGS,
    roles: [roles.Viewer],
    status: 'Active',
    accessControl: false,
    account: ACCOUNT_ID_B,
    user: USER_ID_A,
  },
  accountBUserBOwner: {
    id: '31343780-6170-4c88-b4bd-ebefd74d4e6c',
    settings: constants.DEFAULT_SETTINGS,
    roles: [roles.Owner],
    status: 'Active',
    accessControl: false,
    account: ACCOUNT_ID_B,
    user: USER_ID_B,
  },
  accountBUserCPaidAndUnpaidRoles: {
    id: '7b07ceaa-a2a4-458a-ab0d-5558ba9d0acf',
    settings: constants.DEFAULT_SETTINGS,
    roles: [roles.Admin, roles.Billing],
    status: 'Active',
    accessControl: false,
    account: ACCOUNT_ID_B,
    user: USER_ID_C,
  },
  accountCUserAPaidRoles: {
    id: '3e3c075b-7fb2-4721-b2ec-51a293027ba0',
    settings: constants.DEFAULT_SETTINGS,
    roles: [roles.Collaborator],
    status: 'Active',
    accessControl: false,
    account: ACCOUNT_ID_C,
    user: USER_ID_A,
  },
  emptyRoles: {
    id: 'cec57564-a30c-4d6a-b430-3b0dbaf66f99',
    settings: constants.DEFAULT_SETTINGS,
    roles: [],
    status: 'Active',
    accessControl: false,
    account: ACCOUNT_ID_C,
    user: USER_ID_B,
  },
  noRoles: {
    id: '15aa0bdd-1c8e-47d6-9925-b419f321fc64',
    settings: constants.DEFAULT_SETTINGS,
    status: 'Active',
    accessControl: false,
    account: ACCOUNT_ID_C,
    user: USER_ID_D,
  },
};

const accountMemberFixture = genericFixture({
  idField: 'id',
  createFn: (data, repoInstance) => repoInstance.create(data),
  data,
});

module.exports = {
  createRepoInstance,
  data,
  accountMemberFixture,
  USER_ID_A,
  USER_ID_B,
  USER_ID_C,
  ACCOUNT_ID_A,
  ACCOUNT_ID_B,
  ACCOUNT_ID_C,
};
