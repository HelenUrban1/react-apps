module.exports = ({ createFn, data, idField = 'id' }) => ({
  buildAll() {
    return JSON.parse(JSON.stringify(data));
  },

  build(id, overrides) {
    const item = this.buildAll().find((i) => i[idField] === id);

    if (!item) {
      throw new Error(`Not found fixture with id: ${id}.`);
    }

    return {
      ...item,
      ...overrides,
    };
  },

  async createAll(repoInstance) {
    const items = [];

    for (const item of this.buildAll()) {
      items.push(await this.create(item.id, undefined, repoInstance));
    }

    return items;
  },

  async create(id, overrides, repoInstance) {
    const item = this.build(id, overrides);

    if (!createFn) {
      return item;
    }

    return createFn(item, repoInstance);
  },
});
