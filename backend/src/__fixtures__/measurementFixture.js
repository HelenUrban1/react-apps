const genericFixture = require('./genericFixture');
const MeasurementRepository = require('../database/repositories/measurementRepository');

const createRepoInstance = () => new MeasurementRepository();

const data = [
  {
    id: '5b7ad336-9dad-4e69-93e7-dddbeef64f84',
    value: 'Value',
    status: 'Pass',
    gage: 'Micrometer',
    machine: 'Miller',
    sampleId: '63ff7d84-fc95-4776-a830-1b9e03d4b201',
    operatorId: '52a45922-2b05-4a32-bd27-7ff319d30111',
    characteristicId: 'fedcd347-df93-4718-a247-440236206e5e',
    stationId: 'abccd347-df93-4718-a247-440236206e5e',
    operationId: 'cbacd347-df93-4718-a247-440236206e5e',
    methodId: '121cd347-df93-4718-a247-440236206e5e',
  },
];

const measurementFixture = genericFixture({
  idField: 'id',
  createFn: (data, repoInstance) => repoInstance.create(data),
  data,
});

module.exports = {
  createRepoInstance,
  data,
  measurementFixture,
};
