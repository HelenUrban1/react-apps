const genericFixture = require('./genericFixture');
const MarkerRepository = require('../database/repositories/markerRepository');

const createRepoInstance = () => new MarkerRepository();

const data = [
  {
    id: '607eab44-b7d9-4d19-9ad7-78d0f5425944',
    markerStyleName: 'Default',
    shape: 'Circle',
    defaultPosition: 'Left',
    hasLeaderLine: false,
    fontSize: 10,
    fontColor: 'White',
    fillColor: 'Purple',
    borderColor: 'Purple',
    borderWidth: 1,
    leaderLineColor: 'Purple',
    leaderLineWidth: 1,
  },
];

const markerFixture = genericFixture({
  idField: 'id',
  createFn: (data, repoInstance) => repoInstance.create(data),
  data,
});

module.exports = {
  createRepoInstance,
  data,
  markerFixture,
};