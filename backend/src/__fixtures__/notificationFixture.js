const genericFixture = require('./genericFixture');
const NotificationRepository = require('../database/repositories/notificationRepository');

const createRepoInstance = () => new NotificationRepository();

const data = [
  {
    id: 'f8ea59ac-e160-4554-943a-5da6bc0ebf4e',
    title: 'PRT-2343 has finished autoballooning',
    content: '54 features identified, PRT-2343 is ready for review',
    action: null,
    actionAt: null,
    priority: 0,
    relationId: 'd6c5bc49-9fb2-4fbb-8a61-77bc8d18036f',
    relationType: 'part',
    actorId: null,
    siteId: '31ef1d56-1b86-4ffa-b886-507ed8f021c5',
    createdById: 'e0a01779-c8ed-4090-bd44-9c1b2242fb75',
    updatedById: 'e0a01779-c8ed-4090-bd44-9c1b2242fb75',
  },
];

const notificationFixture = genericFixture({
  idField: 'id',
  createFn: (data, repoInstance) => repoInstance.create(data),
  data,
});

module.exports = {
  createRepoInstance,
  data,
  notificationFixture,
};
