const { defaultTypeIDs, defaultSubtypeIDs, defaultMethodIDs } = require('../staticData/defaults');

const genericFixture = require('./genericFixture');
const CharacteristicRepository = require('../database/repositories/characteristicRepository');
const { defaultUnitIDs } = require('../staticData/defaults');

const createRepoInstance = () => new CharacteristicRepository();

const data = [
  {
    id: 'e0a01769-c8ed-4090-bd24-9c1b2241f123',
    drawingSheetIndex: 1,
    captureMethod: 'Automated',
    status: 'Active',
    verified: false,
    notationType: defaultTypeIDs.Dimension, // Dimension
    notationSubtype: defaultSubtypeIDs.Curvilinear, // Curvilinear
    notationClass: 'Tolerance',
    fullSpecification: '1.25 +/- 0.1',
    quantity: 3,
    nominal: '1.25',
    upperSpecLimit: '1.26',
    lowerSpecLimit: '1.24',
    plusTol: '0.1',
    minusTol: '0.1',
    unit: defaultUnitIDs.inch,
    toleranceSource: 'Document_Defined',
    inspectionMethod: defaultMethodIDs.Caliper, // Caliper
    criticality: null,
    notes: '',
    drawingRotation: 0.0,
    drawingScale: 1.0,
    boxLocationY: 510.7251086456913,
    boxLocationX: 202.30217210136476,
    boxWidth: 33.53615354307212,
    boxHeight: 15.562144712486088,
    connectionPointLocationY: 510.7251086456913,
    connectionPointLocationX: 202.30217210136476,
    boxRotation: 0.0,
    markerGroup: '1',
    markerGroupShared: false,
    markerIndex: 0,
    markerSubIndex: null,
    markerLabel: '1',
    markerStyle: 'aad19683-337a-4039-a910-98b0bdd5ee57',
    markerLocationX: 45,
    markerLocationY: 180,
    markerFontSize: 18,
    markerSize: 36,
    markerRotation: 0,
    leaderLineDistance: 0,
    gridCoordinates: null,
    balloonGridCoordinates: null,
    connectionPointGridCoordinates: null,
    partId: 'd6c5bc49-9fb2-4fbb-8a61-77bc8d28036f', // belongsTo
    drawingId: 'c200ce70-72b8-44f7-8d3f-4b93f6d1b491', // belongsTo
    drawingSheetId: 'd6c5bc49-9fb2-4fbb-8a61-77bc8d380352',
  },
  {
    id: '11101769-c8ed-4090-bd24-9c1b2241f123',
    drawingSheetIndex: 1,
    captureMethod: 'Automated',
    status: 'Active',
    verified: false,
    notationType: defaultTypeIDs.Dimension, // Dimension
    notationSubtype: defaultSubtypeIDs.Curvilinear, // Curvilinear
    notationClass: 'Tolerance',
    fullSpecification: '1.25 +/- 0.1',
    quantity: 3,
    nominal: '1.25',
    upperSpecLimit: '1.26',
    lowerSpecLimit: '1.24',
    plusTol: '0.1',
    minusTol: '0.1',
    unit: defaultUnitIDs.inch,
    toleranceSource: 'Document_Defined',
    inspectionMethod: defaultMethodIDs.Caliper, // Caliper
    criticality: null,
    notes: '',
    drawingRotation: 0.0,
    drawingScale: 1.0,
    boxLocationY: 510.7251086456913,
    boxLocationX: 202.30217210136476,
    boxWidth: 33.53615354307212,
    boxHeight: 15.562144712486088,
    connectionPointLocationY: 510.7251086456913,
    connectionPointLocationX: 202.30217210136476,
    boxRotation: 0.0,
    markerGroup: '1',
    markerGroupShared: false,
    markerIndex: 0,
    markerSubIndex: null,
    markerLabel: '1',
    markerStyle: 'aad19683-337a-4039-a910-98b0bdd5ee57',
    markerLocationX: 45,
    markerLocationY: 180,
    markerFontSize: 18,
    markerSize: 36,
    markerRotation: 0,
    leaderLineDistance: 0,
    gridCoordinates: null,
    balloonGridCoordinates: null,
    connectionPointGridCoordinates: null,
    partId: 'd6c5bc49-9fb2-4fbb-8a61-77bc8d28036f', // belongsTo
    drawingId: 'c200ce70-72b8-44f7-8d3f-4b93f6d1b491', // belongsTo
    drawingSheetId: 'd6c5bc49-9fb2-4fbb-8a61-77bc8d380352',
  },
];

const characteristicFixture = genericFixture({
  idField: 'id',
  createFn: (data, repoInstance) => repoInstance.create(data),
  data,
});

module.exports = {
  createRepoInstance,
  data,
  characteristicFixture,
};
