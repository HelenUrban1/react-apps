const genericFixture = require('./genericFixture');
const StationRepository = require('../database/repositories/stationRepository');

const createRepoInstance = () => new StationRepository();

const data = [
  {
    id: '31ef1d56-1b86-4ffa-b886-507ed8f01111',
    name: 'Fixture Station',
    isMaster: false,
    disabled: false,
    createdById: 'e0a01779-c8ed-4090-bd44-9c1b2242fb75',
    updatedById: 'e0a01779-c8ed-4090-bd44-9c1b2242fb75',
  },
];

const stationFixture = genericFixture({
  idField: 'id',
  createFn: (data, repoInstance) => repoInstance.create(data),
  data,
});

module.exports = {
  createRepoInstance,
  data,
  stationFixture,
};
