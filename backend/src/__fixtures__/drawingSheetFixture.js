const genericFixture = require('./genericFixture');
const DrawingSheetRepository = require('../database/repositories/drawingSheetRepository');

const createRepoInstance = () => new DrawingSheetRepository();

const data = [
  {
    id: 'd6c5bc49-9fb2-4fbb-8a61-77bc8d380352',
    pageName: 'Sheet 1',
    pageIndex: 0,
    height: 450,
    width: 450,
    rotation: 0,
    grid: '{"initialCanvasWidth":1280,"initialCanvasHeight":720,"lineWidth":5,"labelColor":"#963CBD","lineColor":"#963CBD","columnLeftLabel":"4","columnRightLabel":"1","rowTopLabel":"B","rowBottomLabel":"A","skipLabels":[],"showLabels":true,"rowLineRatios":[0,0.47945,1],"rowLines":[27,342,684],"rowLabels":["B","A"],"colLineRatios":[0,0.23013,0.48675,0.74338,1],"colLines":[32,310,620,930,1240],"colLabels":["4","3","2","1"]}',
    useGrid: true,
    markerSize: 24,
    displayLines: false,
    displayLabels: false,
    importHash: null,
    part: 'd6c5bc49-9fb2-4fbb-8a61-77bc8d18036f',
    drawing: 'c200ce70-72b8-44f7-8d3f-4b93f6d1b491',
    createdById: 'e0a01779-c8ed-4090-bd44-9c1b2242fb75',
    updatedById: 'e0a01779-c8ed-4090-bd44-9c1b2242fb75',
  },
  {
    id: '1115bc49-9fb2-4fbb-8a61-77bc8d380111',
    pageName: 'Sheet 1',
    pageIndex: 0,
    height: 450,
    width: 450,
    rotation: 0,
    grid: '{"initialCanvasWidth":1280,"initialCanvasHeight":720,"lineWidth":5,"labelColor":"#963CBD","lineColor":"#963CBD","columnLeftLabel":"4","columnRightLabel":"1","rowTopLabel":"B","rowBottomLabel":"A","skipLabels":[],"showLabels":true,"rowLineRatios":[0,0.47945,1],"rowLines":[27,342,684],"rowLabels":["B","A"],"colLineRatios":[0,0.23013,0.48675,0.74338,1],"colLines":[32,310,620,930,1240],"colLabels":["4","3","2","1"]}',
    displayLines: false,
    displayLabels: false,
    importHash: null,
    part: 'd6c5bc49-9fb2-4fbb-8a61-77bc8d18036f',
    drawing: 'c200ce70-72b8-44f7-8d3f-4b93f6d1b491',
    createdById: 'e0a01779-c8ed-4090-bd44-9c1b2242fb75',
    updatedById: 'e0a01779-c8ed-4090-bd44-9c1b2242fb75',
  },
];

const drawingSheetFixture = genericFixture({
  idField: 'id',
  createFn: (cdata, repoInstance) => repoInstance.create(cdata),
  data,
});

module.exports = {
  createRepoInstance,
  data,
  drawingSheetFixture,
};
