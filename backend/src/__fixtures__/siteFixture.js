const genericFixture = require('./genericFixture');
const SiteRepository = require('../database/repositories/siteRepository');

const createRepoInstance = () => new SiteRepository();

const data = [
  {
    id: '31ef1d56-1b86-4ffa-b886-507ed8f021c5',
    status: 'Active',
    siteName: 'Acme Corp',
    sitePhone: '987-654-3210',
    siteEmail: 'dev.mail.monkey@gmail.com',
    siteStreet: '1 Glenwood Ave',
    siteStreet2: '',
    siteCity: 'Raleigh',
    siteRegion: 'North_Carolina',
    sitePostalcode: '27603',
    siteCountry: 'United_States',
    settings: 'settings',
    createdById: 'e0a01779-c8ed-4090-bd44-9c1b2242fb75',
    updatedById: 'e0a01779-c8ed-4090-bd44-9c1b2242fb75',
  },
];

const siteFixture = genericFixture({
  idField: 'id',
  createFn: (data, repoInstance) => repoInstance.create(data),
  data,
});

module.exports = {
  createRepoInstance,
  data,
  siteFixture,
};
