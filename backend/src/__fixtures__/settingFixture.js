const genericFixture = require('./genericFixture');

const data = [
  {
    id: 'acafb538-6a9b-4419-afc7-181b33d1c650',
    name: 'theme',
    value: 'default_system',
    metadata: null,
    siteId: null,
    userId: null,
  },
  {
    id: 'd00738fe-bb02-41dc-b9d0-f1a506625610',
    name: 'theme',
    value: 'default_account',
    metadata: null,
    siteId: null,
    userId: null,
  },
  {
    id: '2e17b868-3897-43d5-817b-db411edabee8',
    name: 'theme',
    value: 'default_site',
    metadata: null,
    siteId: '31ef1d56-1b86-4ffa-b886-507ed8f021c5',
    userId: null,
  },
  {
    id: '0eea4e3f-8004-4966-98a6-8e5c90de658b',
    name: 'theme',
    value: 'default_user',
    metadata: null,
    siteId: null,
    userId: 'e0a01779-c8ed-4090-bd44-9c1b2242fb75',
  },
];

const settingFixture = genericFixture({
  idField: 'id',
  createFn: (data, repoInstance) => repoInstance.create(data),
  data,
});

module.exports = {
  data,
  settingFixture,
};
