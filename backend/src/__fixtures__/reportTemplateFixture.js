const genericFixture = require('./genericFixture');
const ReportTemplateRepository = require('../database/repositories/reportTemplateRepository');

const createRepoInstance = () => new ReportTemplateRepository();

const data = [
  {
    id: '361ed4ee-04a8-46ef-9a6d-df2e26786a49',
    title: 'FAI Alpha Template',
    type: 'FAI',
    providerId: '361ed4ee-04a8-46ef-9a6d-df2e26786a49',
    status: 'Active',
    direction: 'Vertical',
    settings: '{}',
    sortIndex: 0,
    filters: null,
  },
];

const reportTemplateFixture = genericFixture({
  idField: 'id',
  createFn: (data, repoInstance) => repoInstance.create(data),
  data,
});

module.exports = {
  createRepoInstance,
  data,
  reportTemplateFixture,
};
