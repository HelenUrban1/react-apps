const genericFixture = require('./genericFixture');
const JobRepository = require('../database/repositories/jobRepository');

const createRepoInstance = () => new JobRepository();

const data = [
  {
    id: 'd6c5bc49-9fb2-4fbb-8a61-77bc8d18036f',
    partId: '480e0437-e26a-4a03-818d-8494b65a0bec',
    name: '0JB-499JF94-FNEW',
    sampling: 'Quantity',
    jobStatus: 'Active',
    interval: 5,
    samples: 50,
    passing: 40,
    scrapped: 8,
  },
];

const jobFixture = genericFixture({
  idField: 'id',
  createFn: (cdata, repoInstance) => repoInstance.create(cdata),
  data,
});

module.exports = {
  createRepoInstance,
  data,
  jobFixture,
};
