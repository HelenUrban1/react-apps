const genericFixture = require('./genericFixture');
const OrganizationRepository = require('../database/repositories/organizationRepository');

const createRepoInstance = () => new OrganizationRepository();

const data = [
  {
    id: '86cbc2c6-7d27-4789-a451-4307f65e9e01',
    status: 'Active',
    type: 'Customer',
    settings: '',
    name: 'Boeing',
    orgLogo: {
      name: 'avatar.jpg',
      sizeInBytes: 100000,
      publicUrl: 'http://path.path/avatar.jpg',
      privateUrl: 'path/avatar.jpg',
      new: true,
    },
  },
];

const organizationFixture = genericFixture({
  idField: 'id',
  createFn: (data, repoInstance) => repoInstance.create(data),
  data,
});

module.exports = {
  createRepoInstance,
  data,
  organizationFixture,
};
