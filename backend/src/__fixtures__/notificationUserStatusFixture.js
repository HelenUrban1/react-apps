const genericFixture = require('./genericFixture');
const NotificationUserStatusRepository = require('../database/repositories/notificationUserStatusRepository');

const createRepoInstance = () => new NotificationUserStatusRepository();

const data = [
  {
    id: '67fe70dd-3819-40a5-92d7-6bde79150838',
    readAt: null,
    archivedAt: null,
    notificationId: 'f8ea59ac-e160-4554-943a-5da6bc0ebf4e',
    userId: 'e0a01779-c8ed-4090-bd44-9c1b2242fb75',
  },
];

const notificationUserStatusFixture = genericFixture({
  idField: 'id',
  createFn: (data, repoInstance) => repoInstance.create(data),
  data,
});

module.exports = {
  createRepoInstance,
  data,
  notificationUserStatusFixture,
};
