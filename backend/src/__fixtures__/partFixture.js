const genericFixture = require('./genericFixture');
const PartRepository = require('../database/repositories/partRepository');
const defaults = require('../staticData/defaults');

const createRepoInstance = () => new PartRepository();

const data = [
  {
    id: 'd6c5bc49-9fb2-4fbb-8a61-77bc8d18036f',
    customerId: '480e0437-e26a-4a03-818d-8494b65a0bec',
    purchaseOrderCode: null,
    purchaseOrderLink: null,
    type: 'Part',
    name: 'Graytech Masterpiece',
    number: '12345',
    revision: 'H',
    reviewStatus: 'Unverified',
    drawingNumber: null,
    drawingRevision: null,
    defaultLinearTolerances: defaults.defaultTolerances.linearInch,
    defaultAngularTolerances: defaults.defaultTolerances.angular,
    defaultMarkerOptions: `{"1":{"style":"25750225-6d8b-4469-acad-a99c217fa8ba","assign":["Type","${defaults.defaultTypeIDs.Dimension}","${defaults.defaultSubtypeIDs.Radius}"]},"2":{"style":"3d3f1c44-d73e-456f-8f35-384a52cf7fb4","assign":["Classification","${defaults.defaultClassificationIDs.Critical}"]},"default":{"style":"f12c5566-7612-48e9-9534-ef90b276ebaa","assign":["Default"]}}`,
    presets: `{"styles":"Purple Diamond"}`,
    status: 'Active',
    workflowStage: 0,
    completedAt: null,
    balloonedAt: null,
    reportGeneratedAt: null,
    notes: null,
    accessControl: 'None',
    measurement: 'Imperial',
    tags: null,
    reports: null,
    primaryDrawingId: 'c200ce70-72b8-44f7-8d3f-4b93f6d1b491',
    siteId: '31ef1d56-1b86-4ffa-b886-507ed8f021c5',
  },
  {
    id: 'd6c5bc49-9fb2-4fbb-8a61-77bc8d180111',
    customerId: '480e0437-e26a-4a03-818d-8494b65a0bec',
    purchaseOrderCode: null,
    purchaseOrderLink: null,
    type: 'Part',
    name: 'Graytech Masterpiece',
    number: '12345',
    revision: 'H',
    previousRevision: '1115bc49-9fb2-4fbb-8a61-77bc8d18036f',
    reviewStatus: 'Unverified',
    drawingNumber: null,
    drawingRevision: null,
    defaultLinearTolerances: defaults.defaultTolerances.linearInch,
    defaultAngularTolerances: defaults.defaultTolerances.angular,
    defaultMarkerOptions: `{"1":{"style":"25750225-6d8b-4469-acad-a99c217fa8ba","assign":["Type","${defaults.defaultTypeIDs.Dimension}","${defaults.defaultSubtypeIDs.Radius}"]},"2":{"style":"3d3f1c44-d73e-456f-8f35-384a52cf7fb4","assign":["Classification","${defaults.defaultClassificationIDs.Critical}"]},"default":{"style":"f12c5566-7612-48e9-9534-ef90b276ebaa","assign":["Default"]}}`,
    presets: `{"styles":"Purple Diamond"}`,
    status: 'Active',
    workflowStage: 0,
    completedAt: null,
    balloonedAt: null,
    reportGeneratedAt: null,
    notes: null,
    accessControl: 'None',
    measurement: 'Imperial',
    tags: null,
    reports: null,
    primaryDrawingId: 'c200ce70-72b8-44f7-8d3f-4b93f6d1b491',
    siteId: '31ef1d56-1b86-4ffa-b886-507ed8f021c5',
  },
];

const partFixture = genericFixture({
  idField: 'id',
  createFn: (data, repoInstance) => repoInstance.create(data),
  data,
});

module.exports = {
  createRepoInstance,
  data,
  partFixture,
};
