const genericFixture = require('./genericFixture');
const ReportRepository = require('../database/repositories/reportRepository');

const createRepoInstance = () => new ReportRepository();

const data = [
  {
    id: '361ed4ee-04a8-46ef-9a6d-df2e26786a49',
    partVersion: 'H',
    title: 'FAI Report',
    templateUrl: 'src/template/fake.xlsx',
    status: 'Active',
  },
];

const reportFixture = genericFixture({
  idField: 'id',
  createFn: (data, repoInstance) => repoInstance.create(data),
  data,
});

module.exports = {
  createRepoInstance,
  data,
  reportFixture,
};

