const config = require('../../config')();
const AuthService = require('../services/auth/authService');
const SubscriptionService = require('../services/subscriptionService');
const { log } = require('../logger');
const UserRepository = require('../database/repositories/userRepository');

/**
 * Authenticates and fills the request with the user if it exists.
 * If no token is passed, it continues the request but without filling the currentUser.
 * If userAutoAuthenticatedEmailForTests exists and no token is passed, it fills with this user for tests.
 *
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
async function authMiddleware(req, res, next) {
  let idToken;
  req.language = req.headers['accept-language'] || 'en';

  const isTokenEmpty = req.headers && req.headers.authorization && !req.headers.authorization.startsWith('Bearer ');

  if (isTokenEmpty) {
    return authenticateWithTestUserIfExists(req, res, next);
  }

  if (req.headers.authorization && req.headers.authorization.startsWith('Bearer ')) {
    // permit services to still access the API
    idToken = req.headers.authorization.split('Bearer ')[1];
  }

  if (!idToken) {
    res.setHeader('session-status', 'user-logged-out'); // Allows web crawlers to determine session status
    return next();
  }

  try {
    const currentUser = await AuthService.findByToken(idToken);

    if (currentUser && currentUser.disabled) {
      throw new Error(`User '${currentUser.email}' is disabled`);
    }

    req.currentUser = currentUser;
    res.setHeader('session-status', 'user-logged-in'); // Allows web crawlers to determine session status

    // Fetch user subscription data
    const subServ = new SubscriptionService(req);
    const subscription = await subServ.findByAccountId(currentUser.accountId, req).catch((err) => {
      // If there is no profile b/c this is an orphan, continue processing
      log.warn(`Failed to find subscription: ${err}`);
      return null;
    });
    req.currentUser.subscription = subscription;

    // Don't identify the user to Analytics here, because sessions are stateless
    // Instead just explicitly pass userId with every analyticsClient.track call
    // req.analyticsClient.identify({ req });

    return next();
  } catch (error) {
    log.error('Error while verifying ID token:', error);
    if (error.name && error.name === 'TokenExpiredError') {
      return res.status(401).send('Expired Token');
    }

    if (error.name && error.name === 'TokenExpiredError') {
      res.status(401).send('TokenExpiredError');
    } else {
      res.status(403).send('Unauthorized');
    }
  }
}

/**
 * Fills the request with the authenticateWithTestUser in case it exists.
 *
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
async function authenticateWithTestUserIfExists(req, res, next) {
  const userAutoAuthenticatedEmailForTests = config && config.userAutoAuthenticatedEmailForTests;

  if (!userAutoAuthenticatedEmailForTests) {
    return next();
  }

  try {
    const currentUser = await UserRepository.findByEmailWithoutAvatar(userAutoAuthenticatedEmailForTests);

    log.debug(`Automatically authenticated user for tests: ${userAutoAuthenticatedEmailForTests}`);

    if (currentUser && currentUser.disabled) {
      throw new Error(`User '${currentUser.email}' is disabled`);
    }

    req.currentUser = currentUser;

    return next();
  } catch (error) {
    log.error(`Error while authenticating with default user: ${userAutoAuthenticatedEmailForTests}:`, error);

    res.status(403).send('Unauthorized');
  }
}

module.exports = authMiddleware;
