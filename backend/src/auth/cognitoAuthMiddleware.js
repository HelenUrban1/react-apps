const jwt = require('jsonwebtoken');
const config = require('../../config')();
const { log } = require('../logger');
const UserRepository = require('../database/repositories/userRepository');
const { convertCognitoUsernameToEmail } = require('../services/auth/authUtils');
const { AliasExistsException } = require('@aws-sdk/client-cognito-identity-provider');

async function authenticateWithTestUserIfExists(req, res, next) {
  const userAutoAuthenticatedEmailForTests = config && config.userAutoAuthenticatedEmailForTests;

  if (!userAutoAuthenticatedEmailForTests) {
    return next();
  }

  try {
    const currentUser = await UserRepository.findByEmailWithoutAvatar(userAutoAuthenticatedEmailForTests);

    log.debug(`Automatically authenticated user for tests: ${userAutoAuthenticatedEmailForTests}`);

    if (currentUser && currentUser.status === 'Archived') {
      throw new Error(`User '${currentUser.email}' is disabled`);
    }

    req.currentUser = currentUser;

    return next();
  } catch (error) {
    log.error(`Error while authenticating with default user: ${userAutoAuthenticatedEmailForTests}:`, error);

    return res.status(403).send('Unauthorized');
  }
}

async function cognitoAuthMiddleware(req, res, next) {
  try {
    if (!req.headers.authorization) {
      res.setHeader('session-status', 'user-logged-out'); // Allows web crawlers to determine session status
      return next();
    }

    const accessToken = req.headers.authorization.split('Bearer ')[1];

    if (!accessToken) {
      res.setHeader('session-status', 'user-logged-out'); // Allows web crawlers to determine session status
      return next();
    }

    const decodedJwt = jwt.decode(accessToken, { complete: true });

    const issuer = `https://cognito-idp.${config.cognito.REGION}.amazonaws.com/${config.cognito.USER_POOL_ID}`;

    if (!decodedJwt) {
      return authenticateWithTestUserIfExists(req, res, next);
    }
    if (decodedJwt.payload.iss !== issuer) return res.status(403).send('Invalid Token Issuer');
    if (decodedJwt.payload.token_use !== 'access') return res.status(403).send('Unauthorized Token');

    const pem = req.pems[decodedJwt.header.kid];
    let email;
    try {
      const verified = jwt.verify(accessToken, pem, { issuer, algorithms: ['RS256'] });
      log.debug(`Verified JWT for ${verified.username}`);
      email = convertCognitoUsernameToEmail(verified.username);
    } catch (e) {
      log.error(e);
      return res.status(401).send('TokenExpiredError');
    }

    try {
      const currentUser = await UserRepository.findByEmailWithoutAvatar(email);
      if (currentUser && currentUser.status === 'Archived') {
        throw new Error(`User '${currentUser.email}' is disabled`);
      }
      req.currentUser = currentUser;
      res.setHeader('session-status', 'user-logged-in'); // Allows web crawlers to determine session status
    } catch (e) {
      log.error(e);
      return res.status(403).send({ error: 'UserDisabledError' });
    }

    return next();
  } catch (error) {
    return res.status(401).send({ error: 'Auth failed' });
  }
}

module.exports = cognitoAuthMiddleware;
