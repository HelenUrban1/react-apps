import { log } from '../logger';
import CharacteristicService from '../services/characteristicService';

const config = require('../../config')();
const api = require('../api');

const setupSocketEvents = () => {
  const io = api.get('io');
  const eventEmitter = api.get('eventEmitter');

  if (config.isBeforeProduction) {
    io.on('connection', (socket) => {
      log.debug(`UserId: ${socket.user.id} AccountId: ${socket.user.accountId} SiteId: ${socket.user.siteId}`);
      socket.send(`Hi from server joined ${socket.user.accountId}-${socket.user.siteId}-${socket.user.id}`);
      socket.join(`${socket.user.accountId}-${socket.user.siteId}-${socket.user.id}`);
      socket.join(`${socket.user.accountId}-${socket.user.siteId}`);
      socket.join(`${socket.user.accountId}`);

      socket.on('message', async (data) => {
        log.warn(`client sent message: ${data}`);
      });

      socket.on('characteristic.getOcr', async (payload) => {
        const base64Image = payload.imageData.split(';base64,').pop();
        const buffer = Buffer.from(base64Image, 'base64');

        const characteristicService = new CharacteristicService({ currentUser: socket.user, language: 'en', eventEmitter: eventEmitter });
        characteristicService.parseAndSaveAutomarkup(buffer, payload.isGdtOrBasic, payload.characteristicId, payload.tolerances, payload.types);
      });
    });

    eventEmitter.on('notification.create', (info) => {
      try {
        log.debug('CREATED A NOTIFICATION', { accountId: info.accountId, siteId: info.siteId});

        const notification = info.data;
        notification.userStatuses.forEach((userStatus) => {
          log.warn(userStatus);
          log.debug(`${info.accountId}-${info.siteId}-${userStatus.userId}`);
          io.to(`${info.accountId}-${info.siteId}-${userStatus.userId}`).emit('notification.create', info.data);
        });
      } catch (error) {
        log.error(error);
      }
    });

    eventEmitter.on('notification.update', (info) => {
    try {
        log.debug('UPDATED A NOTIFICATION', { accountId: info.accountId, siteId: info.siteId });

        const notification = info.data;
        notification.userStatuses.forEach((userStatus) => {
          log.debug(`${info.accountId}-${info.siteId}-${userStatus.userId}`);
          io.to(`${info.accountId}-${info.siteId}-${userStatus.userId}`).emit('notification.create', info.data);
        });
      } catch (error) {
        log.error(error);
      }
    });

    eventEmitter.on('notificationUserStatus.update', (info) => {
      try {
        log.debug('UPDATED A NOTIFICATION USER STATUS', { accountId: info.accountId, siteId: info.siteId, userId: info.data });

        const userStatus = info.data;

        io.to(`${info.accountId}-${info.siteId}-${userStatus.userId}`).emit('notificationUserStatus.update', info.data);
      } catch (error) {
        log.error(error);
      }
    });

    eventEmitter.on('part.update', (info) => {
      try {
        log.debug('UPDATED A PART STATUS', { accountId: info.accountId, siteId: info.siteId });

        io.to(`${info.accountId}-${info.siteId}`).emit('part.update', info.data);
      } catch (error) {
        log.error(error);
      }
    });

    eventEmitter.on('drawingSheet.update', (info) => {
      try {
        log.debug('UPDATED A DRAWING SHEET STATUS', { accountId: info.accountId, siteId: info.siteId });

        io.to(`${info.accountId}-${info.siteId}`).emit('drawingSheet.update', info.data);
      } catch (error) {
        log.error(error);
      }
    });

    eventEmitter.on('characteristic.update', (info) => {
      try {
        log.debug('UPDATED CHARACTERISTIC',  { accountId: info.accountId, siteId: info.siteId });

        io.to(`${info.accountId}-${info.siteId}`).emit('characteristic.update', info.data);
      } catch (error) {
        log.error(error);
      }
    });

    eventEmitter.on('characteristic.destroy', (info) => {
      try {
        log.debug('DESTROY CHARACTERISTIC', { accountId: info.accountId, siteId: info.siteId });

        io.to(`${info.accountId}-${info.siteId}`).emit('characteristic.destroy', info.data);
      } catch (error) {
        log.error(error);
      }
    });

    eventEmitter.on('reviewTask.addToQueue', (info) => {
      try {
        io.to(config.reviewAccountId).emit('reviewTask.addToQueue', info.data);
      } catch (error) {
        log.error(error);
      }
    });

    eventEmitter.on('part.autoMarkupCompleted', (info) => {
      try {
        log.debug('AUTOMARKUPCOMPLETE PART', info);

        io.to(`${info.accountId}-${info.siteId}`).emit('part.autoMarkupCompleted', info.data);
      } catch (error) {
        log.error(error);
      }
    });
  }
};

export { setupSocketEvents as default };
