const config = require('../config')();
const { log } = require('./logger');

module.exports = async () => {
  // Validate ENV variables are setup in config
  require('../config/validateConfig')({ config, log });
};
