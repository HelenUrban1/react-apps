const { i18n } = require('../i18n');

module.exports = class PaymentFailureEmail {
  constructor(language, to, billing, expires) {
    this.language = language.includes('en') ? 'en' : language;
    this.to = to;
    this.billing = billing;
    this.expires = expires;
  }

  get subject() {
    return i18n(this.language, 'emails.paymentFailure.subject', i18n(this.language, 'app.title'));
  }

  get html() {
    let body = '';
    body += i18n(this.language, 'emails.paymentFailure.body', this.billing, i18n(this.language, 'app.phone'), i18n(this.language, 'app.title'));
    body += i18n(this.language, 'emails.paymentFailure.disclaimer', this.expires);
    return body;
  }
};
