const config = require('../../config')();
const { i18n } = require('../i18n');

module.exports = class InvitationEmail {
  constructor(language, to, token = null) {
    this.language = language.includes('en') ? 'en' : language;
    this.to = to;
    this.token = token;
  }

  get subject() {
    return i18n(this.language, 'emails.invitation.subject', i18n(this.language, 'app.title'));
  }

  get html() {
    let url = config.clientUrl;
    url += '/auth/signup?email=';
    url += encodeURIComponent(this.to);
    if (this.token) {
      url += '&token=';
      url += encodeURIComponent(this.token);
    }

    return i18n(this.language, 'emails.invitation.body', i18n(this.language, 'app.title'), url);
  }
};
