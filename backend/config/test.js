const appRoot = require('app-root-path');
require('dotenv').config();

module.exports = {
  env: 'test',

  // Will be ixc-dev.com | ixc-qa.com | ixc-stage.com | app.inspectionxpert.com (prod)
  hostname: process.env.IXC_HOSTNAME || 'ixc-local.com',
  protocol: process.env.PROTOCOL || 'http',
  port: process.env.PORT || ':3000',
  edaUrl: process.env.EDA_URL || `https://www.ixc-dev.com/eda`,
  edaTimeout: process.env.EDA_TIMEOUT,

  database: {
    dialect: 'postgres',
    host: process.env.IXC_DB_HOST,
    username: process.env.IXC_DB_UN,
    password: process.env.IXC_DB_PW,
    database: `${process.env.IXC_DB_CONTAINER}_test`,
    logging: console.log,
  },
  databaseAdmin: {
    dialect: 'postgres',
    host: process.env.IXC_DB_HOST,
    username: process.env.IXC_DB_ADMIN_UN,
    password: process.env.IXC_DB_ADMIN_PW,
    database: `${process.env.IXC_DB_CONTAINER}_test`,
    logging: console.log, // This gets replaced by system logger
  },
  accountDatabase: {
    dialect: 'postgres',
    host: process.env.IXC_DB_HOST,
    username: `${process.env.IXC_ACCOUNT_DB_PREFIX}test_${process.env.IXC_TEST_ACCOUNT_ID}`,
    password: process.env.IXC_ACCOUNT_DB_PW,
    database: `${process.env.IXC_ACCOUNT_DB_PREFIX}test_${process.env.IXC_TEST_ACCOUNT_ID}`,
    logging: console.log, // This gets replaced by system logger
  },
  trialAccountDatabase: {
    dialect: 'postgres',
    host: process.env.IXC_DB_HOST,
    username: `${process.env.IXC_ACCOUNT_DB_PREFIX}test_${process.env.IXC_TRIAL_ACCOUNT_ID}`,
    password: process.env.IXC_ACCOUNT_DB_PW,
    database: `${process.env.IXC_ACCOUNT_DB_PREFIX}test_${process.env.IXC_TRIAL_ACCOUNT_ID}`,
    logging: console.log, // This gets replaced by system logger
  },
  trialExpiredAccountDatabase: {
    dialect: 'postgres',
    host: process.env.IXC_DB_HOST,
    username: `${process.env.IXC_ACCOUNT_DB_PREFIX}test_${process.env.IXC_TRIAL_EXPIRED_ACCOUNT_ID}`,
    password: process.env.IXC_ACCOUNT_DB_PW,
    database: `${process.env.IXC_ACCOUNT_DB_PREFIX}test_${process.env.IXC_TRIAL_EXPIRED_ACCOUNT_ID}`,
    logging: console.log, // This gets replaced by system logger
  },
  freemiumAccountDatabase: {
    dialect: 'postgres',
    host: process.env.IXC_DB_HOST,
    username: `${process.env.IXC_ACCOUNT_DB_PREFIX}test_${process.env.IXC_FREEMIUM_ACCOUNT_ID}`,
    password: process.env.IXC_ACCOUNT_DB_PW,
    database: `${process.env.IXC_ACCOUNT_DB_PREFIX}test_${process.env.IXC_FREEMIUM_ACCOUNT_ID}`,
    logging: console.log, // This gets replaced by system logger
  },
  reviewAccountDatabase: {
    dialect: 'postgres',
    host: process.env.IXC_DB_HOST,
    username: `${process.env.IXC_ACCOUNT_DB_PREFIX}test_${process.env.IXC_REVIEW_ACCOUNT_ID}`,
    password: process.env.IXC_ACCOUNT_DB_PW,
    database: `${process.env.IXC_ACCOUNT_DB_PREFIX}test_${process.env.IXC_REVIEW_ACCOUNT_ID}`,
    logging: console.log, // This gets replaced by system logger
  },

  trialAccountId: process.env.IXC_TRIAL_ACCOUNT_ID,
  trialExpiredAccountId: process.env.IXC_TRIAL_EXPIRED_ACCOUNT_ID,
  freemiumAccountId: process.env.IXC_FREEMIUM_ACCOUNT_ID,
  testAccountId: process.env.IXC_TEST_ACCOUNT_ID,
  reviewAccountId: process.env.IXC_REVIEW_ACCOUNT_ID,
  passwordEncryptionPassphrase: process.env.IXC_PASSWORD_ENCRYPTION_PASSPHRASE,

  new_relic: {
    license_key: process.env.IXC_NEW_RELIC_LICENSE_KEY,
    labels: `Config:Test,Tier:Ixc_Mono_Backend,Domain:${process.env.IXC_MAIL_SENDING_DOMAIN}`,
  },

  // jwt configuration options
  jwt: {
    // Secret used to Sign the JWT (Authentication) tokens.
    authJwtSecret: process.env.IXC_JWT_SECRET,
    refreshJwtSecret: process.env.IXC_REFRESH_SECRET,
    authExpiry: Number.parseInt(process.env.IXC_AUTH_EXPIRY) || 60 * 15,
    refreshExpiry: Number.parseInt(process.env.IXC_REFRESH_EXPIRY) || 60 * 60 * 24,
    authStrategy: process.env.IXC_AUTH_STRATEGY,
  },

  // Should uploads go to remote (s3) or local file system
  uploadRemote: false,

  // Directory where uploaded files are saved. Defaults to src/data.
  uploadDir: `${appRoot}/data`,

  // Logging config for Winston
  logging: {
    exitOnError: false,
    transports: [
      {
        type: 'console',
        level: process.env.LOG_LEVEL || 'error',
        colorize: true,
        handleExceptions: true,
      },
    ],
  },

  // Sendgrid Transport
  email: {
    transport: 'sendgrid',
    sendgrid_api_key: process.env.IXC_MAIL_SENDGRID_API_KEY,
    from: `"InspectionXpert" <${process.env.IXC_MAIL_FROM_ADDRESS}>`,
  },

  // Client URL used when sending emails
  clientUrl: process.env.IXC_MAIL_SENDING_DOMAIN,

  // For testing, when this email is set, all requests will automatically authenticate using this email.
  userAutoAuthenticatedEmailForTests: 'dev.mail.monkey@gmail.com',

  // Allow visitors to browse GraphiQL API https://github.com/graphql/graphiql
  graphiql: false,

  // Bucket for user uploaded files
  s3UploadBucket: process.env.IXC_S3_UPLOAD_BUCKET,

  // Bucket for database backups
  s3DbBackupBucket: process.env.IXC_DB_BACKUP_BUCKET,

  // Value that gets prepended to database and user names for accounts
  accountDbPrefix: process.env.IXC_ACCOUNT_DB_PREFIX,

  // Current AWS Region
  awsRegion: process.env.IXC_AWS_REGION,

  // Product instrumentation
  analytics: {
    segmentSourceWriteKey: process.env.IXC_SEGMENT_SOURCE_WRITE_KEY || 'UYP5cgR91dujOh3Jp7XXleouYsavLC00',
  },

  // App monitoring through Sentry
  appMonitoring: {
    sentryClientKey: process.env.IXC_SENTRY_CLIENT_KEY,
  },

  // ETL / API proxying through SPHINX
  sphinx: {
    baseUrl: process.env.IXC_SPHINX_BASE_URL,
    basePath: process.env.IXC_SPHINX_BASE_PATH,
    sharedKey: process.env.IXC_SPHINX_SHARED_KEY,
  },

  // CRM Integration through Hubspot
  crm: {
    useAdapter: 'hubspot',
    privateAccessToken: process.env.IXC_HUBSPOT_PRIVATE_ACCESS_TOKEN,
    apiKey: process.env.IXC_HUBSPOT_API_KEY,
    appId: process.env.IXC_HUBSPOT_APP_ID || 233345,
    appOauthAccessToken: process.env.IXC_HUBSPOT_APP_OAUTH_ACCESS_TOKEN,
    appOauthRefreshToken: process.env.IXC_HUBSPOT_APP_OAUTH_REFRESH_TOKEN,
    appClientId: process.env.IXC_HUBSPOT_APP_CLIENT_ID,
    appClientSecret: process.env.IXC_HUBSPOT_APP_CLIENT_SECRET,
    appDevApiKey: process.env.IXC_HUBSPOT_APP_DEV_API_KEY,
    dealType: process.env.IXC_DEAL_TYPE || 'newbusiness',
    dealTrialType: process.env.IXC_TRIAL_TYPE || 'Time Limited',
    pipelineName: process.env.IXC_DEAL_PIPELINE || 'IX3 New Sales Pipeline',
    pipelineStage: process.env.IXC_MQA_STAGE || '1. New IX3',
    closedWon: process.env.IXC_CLOSED_WON || 'Closed (Won) IX3',
    closedLost: process.env.IXC_CLOSED_LOST || 'Closed (Lost) IX3',
  },

  // Billing through Chargify / Stripe integration
  billing: {
    useAdapter: 'chargify',
    chargifyPublicKey: process.env.IXC_CHARGIFY_PUBLIC_KEY,
    chargifyPrivateKey: process.env.IXC_CHARGIFY_PRIVATE_KEY,
    chargifySiteName: process.env.IXC_CHARGIFY_SITE_NAME || 'inspectionxpert-test',
    chargifyApiKey: process.env.IXC_CHARGIFY_API_KEY,
  },

  // Product catalog keys
  product: {
    s3Bucket: process.env.IXC_PRODUCT_CATALOG_CACHE_BUCKET || 'inspection-xpert-product-catalog',
    familyId: process.env.IXC_CHARGIFY_PRODUCT_FAMILY_ID || '1526431',
    familyHandle: process.env.IXC_CHARGIFY_PRODUCT_FAMILY_HANDLE || 'inspectionxpert-cloud',
  },

  pdfTronSdkKey: process.env.IXC_PDFTRON_SDK_KEY || 'InspectionXpert Corporation(inspectionxpert.com):OEM:InspectionXpert Cloud::WL:AMS(20240301):0E77D4D11F677AD0D353727860617F9BEF56F50595B50BB4144C588024DABEF5C7',

  enpUrl: process.env.IXC_ENP_URL || 'https://www.ixc-dev.com/api/enp',

  redis: {
    host: process.env.IXC_REDIS_HOST || 'http://ixc-local.com',
    port: process.env.IXC_REDIS_PORT || '6379',
    enabled: process.env.IXC_REDIS_ENABLED || false,
  },

  // For CLI tools that need to move between environments
  awsAssumeRole: {
    username: process.env.IXC_AWS_USERNAME,
    sourceAccountId: process.env.IXC_AWS_SOURCE_ACCOUNT_ID,
    targetAccountId: process.env.IXC_AWS_TARGET_ACCOUNT_ID,
    targetRole: process.env.IXC_AWS_TARGET_ROLE,
  },

  cognito: {
    REGION: process.env.IXC_AWS_REGION,
    USER_POOL_ID: process.env.IXC_COGNITO_USER_POOL_ID,
    APP_CLIENT_ID: process.env.IXC_COGNITO_APP_CLIENT_ID,
    CUSTOM_DOMAIN: process.env.IXC_COGNITO_CUSTOM_DOMAIN,
  },

  // Autoballooning
  autoBallooning: {
    useHumanReview: false, // Disable until human review is staffed
  },

  // Determines if jest tests will use mocks or hit the database
  useMocks: true,
};
