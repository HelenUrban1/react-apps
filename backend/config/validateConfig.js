const { validate } = require('jsonschema');
const configSchema = require('./configSchema.json');

module.exports = function validateConfig(params) {
  const { config, log = console } = params;

  // Validate config
  const validationResult = validate(config, configSchema);
  if (validationResult.errors.length) {
    let errMessage = `IXC Backend configuration is invalid due to ${validationResult.errors.length} errors:`;
    for (let i = 0; i < validationResult.errors.length; i++) {
      const error = validationResult.errors[i];
      // error.instance : The Javascript object being evaluated
      // error.schema : The JSON schema that the instance is being checked against
      // error.path : An array of the parent objects that the property belongs to
      // error.property : The full path of the parent to the property that is missing as a string
      // error.argument : The name of the property that is missing
      // error.name: : The type of error
      // error.message : The text description of the error
      // error.stack : The full type + message error string
      let desc = `${i + 1}) ${error.stack.replace('instance', 'config')}`;
      try {
        if (error.schema && error.schema.properties && error.schema.properties[error.argument] && error.schema.properties[error.argument].description) {
          desc = `${desc}. Description: ${error.schema.properties[error.argument].description}`;
        }
      } catch (err) {
        log.debug('Unable to extract Description from schema', error);
      }
      errMessage = `${errMessage}\n${desc}`;
    }

    // Only exit the process if running in development or test mode
    if (config.isProdEnv) {
      log.warn(errMessage);
    } else {
      log.error(errMessage);
      process.exit(1);
    }
  } else {
    log.info('IXC Backend configuration validation succeeded');
  }
};
