const git = require('git-rev-sync');

module.exports = {
  tag: git.tag(true),
  branch: git.branch(),
  commit: git.short(),
};
