/**
 * This method is responsible for checking the current environment and
 * returning the correct config.
 */
const packageJson = require('../package.json');

// Helper function to read boolean values from ENV variables
// TODO: Move to helper module
const stringToBoolean = (string) => (string === 'false' ? false : !!string);

module.exports = function get() {
  const isMigration = !!process.env.MIGRATION_ENV;
  let config;
  if (isMigration) {
    config = require(`./${process.env.MIGRATION_ENV}`);
  } else {
    // Load the config that corresponds to the value of NODE_ENV
    config = require(`./${process.env.NODE_ENV}`);
  }

  // Updated config with metadata from package.json
  config.package = {
    name: packageJson.name,
    version: packageJson.version,
  };

  // Updated config with metadata from git
  config.scm = {
    tag: process.env.SCM_TAG || '',
    branch: process.env.SCM_BRANCH || '',
    build: process.env.SCM_HASH || '',
  };

  // Cast boolean values
  config.redis.enabled = stringToBoolean(config.redis.enabled);

  // Add build datetime if it exists, otherwise indicate Hot Reloading
  if (config.env === 'development') {
    config.buildDatetime = 'Hot Reloading';
  } else {
    config.buildDatetime = process.env.BUILD_DATETIME || 'Not provided';
  }

  // Helper flags that deterministically return boolean result based on env
  config.isDevEnv = config.env === 'development';
  config.isProdEnv = config.env === 'production';
  config.isTestEnv = config.env === 'test';
  config.isNotDevEnv = config.env !== 'development';
  config.isNotProdEnv = config.env !== 'production';
  config.isNotTestEnv = config.env !== 'test';

  // Helper flags that deterministically return boolean result based on host
  config.isLocalHost = config.hostname.indexOf('ixc-local.com') > -1 || config.hostname.indexOf('localhost') > -1;
  config.isDevHost = config.hostname.indexOf('ixc-dev') > -1;
  config.isQaHost = config.hostname.indexOf('ixc-qa') > -1;
  config.isStageHost = config.hostname.indexOf('ixc-stage') > -1;
  config.isProdHost = config.hostname.indexOf('app.inspectionxpert.com') > -1;
  config.isNotLocalHost = config.hostname.indexOf('ixc-local.com') === -1 && config.hostname.indexOf('localhost') === -1;
  config.isNotDevHost = config.hostname.indexOf('ixc-dev') === -1;
  config.isNotQaHost = config.hostname.indexOf('ixc-qa') === -1;
  config.isNotStageHost = config.hostname.indexOf('ixc-stage') === -1;
  config.isNotProdHost = config.hostname.indexOf('app.inspectionxpert.com') === -1;

  config.protocol = 'https';

  // Compose CORS whitelist
  if (config.isLocalHost) {
    // Localhost does NOT support subdomains but DOES accept ports
    config.corsRegExp = new RegExp(`^(${config.protocol}:\/\/(.+\.)?${config.hostname}(:\\d{1,5})?)$`, 'i'); // eslint-disable-line
  } else {
    // Remote envs DO support subdomains but do NOT accept ports
    config.corsRegExp = new RegExp(`^(${config.protocol}:\/\/(.+\.)?${config.hostname})$`, 'i'); // eslint-disable-line
  }

  // Alias for release progression local > dev > qa, stage > prod
  config.isBeforeQA = config.isNotQaHost && config.isNotStageHost && config.isNotProdHost;
  config.isBeforeStaging = config.isNotStageHost && config.isNotProdHost;
  config.isBeforeProduction = config.isNotProdHost;

  return config;
};
