module.exports = {
  testEnvironment: 'node',
  preset: 'ts-jest/presets/js-with-babel', // https://kulshekhar.github.io/ts-jest/user/config/
  roots: ['<rootDir>/src'],
  transform: {
    '^.+\\.tsx?$': 'ts-jest',
  },
  moduleFileExtensions: ['ts', 'tsx', 'js', 'jsx', 'json', 'node'],
  globalSetup: '<rootDir>/src/jestGlobalSetup.js',
  // globalTeardown: '<rootDir>/src/jestGlobal/testTeardown.js',
  testMatch: ['**/?(*.)+(spec|test).[jt]s?(x)'],
  testPathIgnorePatterns: ['/node_modules/', '/build/', '/coverage/'],
  coverageDirectory: 'coverage',
  coverageReporters: ['text', 'json', 'lcov', 'clover'],
  testResultsProcessor: 'jest-sonar-reporter',
  coverageThreshold: {
    global: {
      statements: 0,
      branches: 0,
      functions: 0,
      lines: 0,
    },
  },
};
