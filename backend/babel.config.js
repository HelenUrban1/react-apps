module.exports = {
  presets: [
    '@babel/preset-typescript',
    [
      '@babel/preset-env',
      {
        targets: {
          node: 'current',
        },
      },
    ],
  ],

  sourceMaps: 'inline',
  env: {
    test: {
      plugins: ['@babel/plugin-transform-runtime'],
    },
  },
};
