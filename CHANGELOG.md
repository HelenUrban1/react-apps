# Changelog

## Ideagen Quality Control Essentials - 2.2.1 2024.01.15

### Bugs

- IXC-3214 Launch product in home is reloading home on first click, going to QC Essentials on second click

### Stories

-IXC-3215 Redirection URL to Ideagen Home should not send parameter sk=1 
-IXC-3236 Update 'Request an Account' redirection URL

## Ideagen Quality Control Essentials - 2.2.0 2023.12.01

### Bugs

- IXC-3101 Encountering an error when trying to expand the Feature Types and Inspection Properties after searching the Sub Types

### Stories

- IXC-3045 User can select menu icon to navigate to Ideagen Home from within Ideagen Quality Control Essentials application
- IXC-3172 Ideagen Home parameters are configurable
- IXC-3174 Redirect QC Essentials user to Ideagen Home

## Ideagen Quality Control Essentials - 2.1.0 2023.11.15

### Bugs

- IXC-1445 [9] Win10,Edge - Portal freezes after changing grid dimensions several times
- IXC-1937 [15] Lots of API requests when updating grid
- IXC-2460 Data retention policy is unavailable
- IXC-2880 On absolute timeout a generic error displays before pushing the user to the signin page
- IXC-3105 Customer Name must be Unique
- IXC-3107 Invite new user modal - Expected error message has changed for a known invalid email \[Automation detected\]
- IXC-3121 Angular captures apply linear default tolerances

### Stories

- IXC-2540 Option to copy Part Number text and paste directly into Drawing Number field
- IXC-2717 Customer's name should not be repeated on add new customer input field.	   
- IXC-3077 Customers' names should not be duplicated on configurations page   
- IXC-3080 Identify the application as Essentials Edition 
- IXC-3118 Update text on data retention modal
- IXC-3123 Use the current document in the PDFViewer during create captures		   
- IXC-3167 Update AS9102C form3 default template

### Engineering Tasks

- IXC-2167 Improve Grid Performance for Large Dimensions
- IXC-2694 Update EDA python packages to most up to date workable versions

## Ideagen Quality Control Essentials - 2.0.2 2023.09.14

### Bugs
- IXC-2988 Drawing number not displaying on report templates
- IXC-3002 Data entered on document page not reported in all data report templates.

### Stories
- IXC-3057 Provide AS9102RevC forms 1, 2, 3 as Default Template

### Engineering Tasks
- IXC-2920 Update frontend to use Node 18
- IXC-2437 HTTP Strict Transport Security (HSTS) Policy Not Enabled
- IXC-2439 Cookie Not Marked as HttpOnly
- IXC-2440 Cookie Not Marked as Secure
- IXC-2442 Missing X-Frame-Options Header

## Ideagen Quality Control - 2.0.1 2023.07.18

### Bugs
- IXC-3015 Update 'sales' email address in application
- IXC-3017 Update 'orders' email address in application
- IXC-3018 Replace text on subscription modal 
- IXC-3019 Replace mailing address on email templates 
- IXC-3030 Update price list on billing page 
- IXC-3031 Update text on email template footer
- IXC-3033 Verification email requires alternative footer
 -IXC-3034 Update Strike through price list on billing page

## Ideagen Quality Control - 2.0.0 2023.06.23

### Stories
- IXC-2664 Rebranding from InspectionXpert3.0 to Ideagen Quality Control
- IXC-2960 Provide links to legally required documents on login screen
- IXC-3000 Apply rebranding where required in email templates

### Bugs
- IXC-2900 Tolerance Type, Tolerance source tokens should return null value for Feature type "Note"
- IXC-2962 GDT check box not behaving as intended 
- IXC-2968 Item list does not update on lose focus

## 1.2.4    2023.05.17

### Bugs
- IXC-2079 [6] Win 10,Chrome- Infinite loading on window drawer when the user cancels to proceed with the part info 
- IXC-2465 QA - Inviting users from seats page and setting yes to review restricted documents are being set to deny ITAR on account activation 
- IXC-2474 QA - Password in plain text in login payload 
- IXC-2570 Version Number / Build information not displaying for IXC-Stage 
- IXC-2593 Tolerance Source value is changed when setting either; Classification/Operation/Inspection Method/Comment 
- IXC-2609 Resend email verification email field removes Plus + symbol 
- IXC-2648 QA - Unable to delete Captured features on data extraction page. 
- IXC-2649 Cant type special character in to part number field but visible on banner 
- IXC-2674 [PROD] - Annotate function not creating sticky note on drawing 
- IXC-2699 Production - Password reset email for users inactive since before 1.2.1 upgrade are not being received 
- IXC-2700 Navigating to large jumps in parts list pages crashes IXC 
- IXC-2808 On user session expiration the currentUser property within the backend becomes undefined 
- IXC-2825 expanding with shared / sub-balloon produces opposite results (sub-balloons/shared balloon on downloaded PDF) 
- IXC-2882 Ballooned PDF displaying annotations from previous part creations

### Stories
- IXC-1699 Redirect back to previous route after sign in after expiration 
- IXC-2537 Allow limited scope special characters to be included in part details - Part Name 
- IXC-2538 Allow limited scope special characters to be included in part details - Part Number 
- IXC-2539 Allow limited scope special characters to be included in part details - Drawing Number 
- IXC-2819 Change text on subscription modal. 
- IXC-2873 Remove pay monthly option on upgrade/confirm subscription modal 
- IXC-2891 Swap privacy policy/terms and conditions hyperlink 
- IXC-2903 InspectionXpert an Ideagen solution text footer added to subscription modals

## 1.2.3    2023.03.22

### Bugs

- IXC-2801 : Verification emails not sent to those enrolled in the trial workflow

## 1.2.2    2023.01.11

### Stories

- IXC-2417 : Make clear to users that GDT font must be installed in order to see engineering symbols

### Bugs

- IXC-889 : [24] Part's last 'Updated at' not updating after ballooning
- IXC-1326 : [20] Token not removed from cell when moved to another cell
- IXC-2115 : [20] W10,Chr- Clicking on Balloon Part icon redirects to the "Choose A Template" tab with blank left menu
- IXC-2178 : [15] - Reports in Part Details Page don't Update Status when Editing Part's Feature Data
- IXC-2179 : [15] - Report Data is Duplicating when Editing a Cell with Text and then Clicking an Empty Cell
- IXC-2206 : [15] - Win10, Chrome - Color settings aren't applied correctly after enabling "Use grid zones on this page"
- IXC-2252 : Deleting a part with revisions is only marking the latest revision as deleted
- IXC-2254 : macOS,Chrome-Warning is not seen when creating empty customer name
- IXC-2262 : [12] - Win 10, Chrome - The part/drawing name isn't updated in the Revision History panel
- IXC-2279 : Jobs side bar Z index needs to be greater than the table titles
- IXC-2280 : [20] Token sometimes being returned to cell when refreshing
- IXC-2295 : Parts field returns 502 Bad gateway
- IXC-2337 : IXC - 2258 Win11, Chrome - Jobs side bar index is not greater than the table titles
- IXC-2338 : To disallow Emojis in the first and last name fields
- IXC-2387 : GDT font download prompt does not function as expected
- IXC-2389 : Add credit card modal - Scroll bars on validation messages 
- IXC-2412 : Invite user and then cancel the invite does not cancel the invitation.
- IXC-2425 : Default access control for all users should be ‘deny ITAR’. From IXC-2242
- IXC-2463 : Multiple full stops in emails causing a http 500 error when registering accounts
- IXC-2466 : QA - Can't delete part on parts page.
- IXC-2507 : Inactive user attempting log on, prevents subsequent active users logging in
- IXC-2517 : Turning off auto ballooning has also turned off custom feature for the QA environment 
- IXC-2577 : QA and Stage - Terms and conditions link returns 404
- IXC-2583 : Operators settings is available on STAGE

## 1.2.1 2022.11.28

### Stories

- IXC-1804 : Remember me button needs to be wired up
- IXC-1969 : [3rd Party] Password Recovery
- IXC-1970 : [3rd Party] MFA Recovery Code
- IXC-1972 : [3rd Party] [Functional Spike] How to Import Existing Email/PW Users into service

### Engineering Tasks

- IXC-2172 : Add SendGrid to Registration Lambda
- IXC-2194 : Cognito Invite User
- IXC-2212 : Cognito Requesting Access
- IXC-2236 : IXC-2245:cognito-refresh-logout
- IXC-2239 : IXC-2239:implement-backend-typescript
- IXC-2408 : Implement Hubspot authentication to using Private Apps
- IXC-2471 : Deploying Cognito to Stage and Prod envs
- IXC-2510 : Hide Auto-ballooning functions for release candidate 1.2.1 for QA, Staging & Production
- IXC-2511 : Hide Net-Inspect functions for release candidate 1.2.1 for QA, Staging & Production

### Bugs

- IXC-1126 : [24] Win 10, Chrome - Broken images are displayed on the forms
- IXC-2036 : [18] Win 10, Chrome- Wrong part number is filed in the Part report field
- IXC-2049 : [25] Cannot delete multi-value template tokens
- IXC-2184 : [25] - Invited User isn't Logged in After Finishing Registration
- IXC-2217 : Error when adding new part
- IXC-2464 : QA - Inactive users are able to login to IXC
- IXC-2476 : QA - Can not disable 2FA
- IXC-2516 : Net inspection features not hidden for a new requested account
- IXC-2533 : Net inspect toggle still present in confirm/ change subscription modal via billing page.

## 1.2.0 2022.02.16

### Stories

- IXC-2105 : Handle Multiple Drawings in Reports
- IXC-1905 : Revision History Panel
- IXC-1904 : Previous Revision View
- IXC-2199 : Drawing Revision Alignment
- IXC-1538 : Hi-light when viewing reports from previous revisions of parts
- IXC-2168 : Enable MFA per account
- IXC-1965 : Login Refactor for MFA
- IXC-2141 : Add Drawing from Part Editor
- IXC-2107 : Net-Inspect Paywall
- IXC-2059 : Create Net-Inspect connection to overwrite FAI report
- IXC-2045 : Export IX3 Part to File
- IXC-1900 : Per Sheet Default Tolerances
- IXC-1897 : Document Information Step
- IXC-1889 : Custom balloon size for each page
- IXC-1887 : Allow removal of grid entirely on a per-sheet basis
- IXC-1885 : GDT Datums should allow more than 1 character

### Engineering Tasks

- IXC-1405 : Remove CharacteristicsContext Entirely
- IXC-1197 : Create an OCR Test Runner
- IXC-1539 : Design Spike - Diff drawing revisions
- IXC-2144 : Design Spike - CMM Sketching Session
- IXC-1876 : Queue-Processor ECS CICD
- IXC-1875 : Backup-Trigger Lambda CICD
- IXC-1354 : [sphinx] Lambda infra with API GW
- IXC-847 : [sphinx] Create CI pipeline
- IXC-846 : [sphinx] Create infrastructure
- IXC-2213 : Create method of clearing review tasks when queues are unstaffed in pre-production environments
- IXC-2189 : Parts list and extraction page not updating in real time for the server
- IXC-2188 : Sheets being duplicated
- IXC-2187 : Human Review corrections not working
- IXC-2146 : Load testing Autoballooning
- IXC-2145 : Test different drawing sizes to find current max page size
- IXC-2136 : Monitoring for Autoballooning
- IXC-2132 : Machine Learning
- IXC-2003 : Fix performance issues so Autoballooning can complete successfully
- IXC-1963 : [3rd Party] Multi-Tenancy Setup
- IXC-1928 : Add call and error data to graphql error trap
- IXC-1768 : Per Client Back Up: Backups - Infra
- IXC-1766 : Updating an item should not update the entire characteristicDetails object in redux
- IXC-1592 : PdfViewerContext writes to Redux
- IXC-1543 : Add IXC role / permission handling
- IXC-1444 : Upgrade PDFTron and benchmark loading time

### Bugs

- IXC-2154 : [40] Win 10, Chrome - Pagination stops working after opening and closing any part
- IXC-2130 : [36] Don't update part for each cell
- IXC-2153 : [30] Grid Zones - The color picker appears for a moment and then immediately disappears
- IXC-2193 : [30] Feature location not matching grid
- IXC-2182 : [30] Unable to Transfer Ownership in Single Seat Accounts
- IXC-2143 : [28] Errors in part wizard navigation
- IXC-2201 : [27] Unable to update Upper/Lower Spec Limits and have the edit persists during Data Extraction
- IXC-2203 : [27] Geometric Tolerances in report have different symbols in the Full Spec cell and Nominal cell
- IXC-2155 : [25] Windows 11 - Chrome - Incorrect 2 Digit "Col left" value handling in Gridzone screen and then stuck
- IXC-2123 : [25] IXC-1903 Win10,Chrome - No part is created with one drawing per uploaded PDF
- IXC-2119 : [24] IXC-1593 W10,Chr- Number 3 on drawing and uncaptured features disappear after pressing Hide captures
- IXC-1983 : [21] Cannot add a space after a period or ¬± symbol in a full spec for a note
- IXC-2157 : [20] macOS,Chrome-Endless Spinning is sen when deleting the uploaded pdf
- IXC-2039 : [18] Win 10, Chrome - Counter of unread notifications doesn't update after marking them as read
- IXC-2005 : [12]W10,Chr- Account Settings - Billing - The date is missing between "Paid with ending in" and "Change"
- IXC-2087 : Win 10, Chrome- Wrong feature index is displayed in the report after ungrouping features
- IXC-2106 : Human review corrections not reflected on part features

## 1.1.4 2021.10.20

### Stories

- IXC-1047 : Upload AS9102 to Net-Inspect
- IXC-1903 : Multiple Document Upload
- IXC-2104 : Create Drawing Revision from Part Toolbar
- IXC-1933 : Frame Characters in Notes
- IXC-1247 : Remove minus from multiples that are negative
- IXC-2012 : Connect export button to SPHINX
- IXC-1964 : [3rd Party] Registration Refactor
- IXC-1899 : Grid Exists and is Right-Sized & Grid Can Be Removed
- IXC-1895 : Toolbar Document Controls
- IXC-1894 : Extraction/PDFTron Toolbar Refactor
- IXC-2060 : Design Spike - Data Connection UI/UX
- IXC-933 : Adding a Station

### Engineering Tasks

- IXC-2102 : Fix memory leak in OCR process for large drawings
- IXC-2056 : Add manually added IAM users to IaC
- IXC-1597 : Delete PdfViewerContext
- IXC-1593 : All uses of pdfViewer read from Redux

### Bugs

- IXC-1203 : [28] Improve support for non-notation words
- IXC-1852 : [27] Notes containing framed characters are re-parsed removing their framed status
- IXC-1992 : [20] Win10/Chrome : Balloon styles are not updated correctly according to the type
- IXC-2038 : [15] Win 10, Chrome- List of notifications becomes empty after checking/unchecking View archived checkbox
- IXC-2037 : [15] Win 10, Chrome- "View Archived" displayed incorrectly when "Hide Read" notifications is selected
- IXC-2004 : [9]Win10,Chrome - Account Settings - Billing - The "Billing Contact" field shows incorrect data
- IXC-2103 : [9] ++ Tolerance with symbol not parsing correctly
- IXC-2125 : [3] W10,Chr- The same button has different names when the same data is entered in "Confirm subscription"
- IXC-2084 : Win 10,Chrome-Entered page names on document info section are not reflected anywhere

## 1.1.3 2021.09.23

### Stories

- IXC-2051 : Delay parsing until 3 secs after last edit in Full Spec
- IXC-1284 : Support for Double Positive or Double Negative Tolerances
- IXC-1372 : Adding spaces to a dimension/length separated by "/" causes it to return as a note
- IXC-1351 : Notes with numbers extracting as dimensions

### Engineering Tasks

- IXC-1995 : NetInspect : Finish part to FAI for NetInspect translation
- IXC-1929 : NetInspect : FUNctional Spike : Generate NetInspect FAI xml file
- IXC-2000 : Autoballooning : Turn on and Test web sockets for autoballooning on Dev server
- IXC-1999 : Autoballooning : Drawing sheets need to exist for autoballooning to function
- IXC-1926 : Autoballooning : Do not auto balloon new drawing revisions
- IXC-1902 : Backend Revision Management
- IXC-1973 : MFA/SSO : Passthrough for FE->API->Cognito
- IXC-1773 : METRO : Cleanup data table logic and CSS
- IXC-1772 : METRO : Cleanup numpad JS and CSS
- IXC-1987 : Generate messageId to improve email tracking
- IXC-2001 : Fix activating autoballooned captures
- IXC-1877 : Lambda Insights and X-Ray for the webhookConsumer lambda in all environments
- IXC-2002 : Document Manual Backup Process with Local Restore
- IXC-1595 : All uses of partEditor (from Context) use Redux instead
- IXC-1594 : PartEditorContext writes to Redux
- IXC-1586 : All changes to SessionContext instead write directly to Redux
- IXC-1585 : All uses of SessionContext instead check Redux

### Bugs

- IXC-2053 : [54] Unable to Convert Trial w/ Components
- IXC-1935 : [30] Purchasing in an expired trial does not correctly update HubSpot and Chargify
- IXC-2050 : [28] Parts List Data not Updating After Progressing through Wizard
- IXC-1921 : [16] Trials created in Wrong Deal Stage
- IXC-2035 : Win 10, Chrome- Wrong results are displayed for "Review status" after reset
- IXC-1950 : 1.1.1 B : Win 10, Chrome - Focus is lost when doing a new capture over the previous capture
- IXC-1943 : 1.1.1 B : Pagination works incorrectly, some pages are displayed without data
- IXC-1940 : 1.1.1 B : W10,Chr-Part Presets- Nothing happens when adding Tolerance and Style Presets, unable to add balloon

## 1.1.2 2021.08.29

### Stories

- IXC-482 : Auto-Ballooning Processing Notification Upload Flow
- IXC-1938 : Feature not updating when type is changed
- IXC-1932 : Push notifications to Human Reviewers that new captures are ready
- IXC-1931 : Update Notification List for AB Actions
- IXC-1930 : Allow Access to Autoballooning on per Client Basis
- IXC-1884 : Remove capture as note modal
- IXC-1856 : Remittance to credit card payment chargify process
- IXC-1855 : Only allow remittance payment for annual subscriptions
- IXC-1813 : Notifications of Autoballoon and Text Processing completion
- IXC-1758 : Architecture Spike: Characteristics Canvas
- IXC-1312 : Template Editor Panel Update
- IXC-1276 : Architecture Spike: Multi-Factor Authentication (MFA)
- IXC-1183 : Logging in : Operator PIN
- IXC-1775 : Data Management : Measurement Data Model
- IXC-951 : UI : Measurements Container
- IXC-1754 : Measurements are saved to the DB
- IXC-1925 : Delete a Measurement

### Engineering Tasks

- IXC-1988 : Check autoballooning on dev server
- IXC-1962 : [3rd Party] Select an Integration
- IXC-1936 : Disable new relic logging
- IXC-1883 : Integrate Net-Inspect client with upload flow
- IXC-1878 : Bump timeout of the webhook-consumer lambda to 1 min
- IXC-1867 : Machine Learning Course
- IXC-1703 : Capture Chargify webhook callbacks
- IXC-1702 : Capture Sendgrid webhook callbacks
- IXC-1534 : For IXC, Add SphinxClient support and GraphQL resources for data exports
- IXC-1100 : Create dashboards in New Relic
- IXC-827 : Spike: Refactoring the Test Suite to just use images rather than entire PDF

### Bugs

- IXC-1927 : [30] Cannot add presets
- IXC-1750 : [30] Feature Cards Selected on Mouse Up instead of Click
- IXC-1879 : [28] Add Webhook Consumer for Failed CC Payments
- IXC-1807 : [28] Circle M and L missing from symbol list
- IXC-1504 : [28] Deleting Features from Second Page Causing Stale State
- IXC-1471 : [28] Switching from Manually Set to Document Defined Tolerances Doesn't Reparse Full Spec
- IXC-1923 : [25] Feature not updating when type is changed
- IXC-1323 : [25] Seeded Custom Template Fails on Report Creation
- IXC-973 : [25] Merged footer unmerging after downloading or saving report
- IXC-1795 : [24] Part & drawing counts differ in subscription object
- IXC-1383 : [21] DrawingName on part information panel errors on null
- IXC-1994 : [21] - Win 10, Chrome - Clicking on the part on any page after the 1st page, redirects me to the 1st page
- IXC-1934 : [21] Quantity For Ballooned Features Defaulting to Quantity of Previous Feature
- IXC-1871 : [20] Template Formatting is Different on Upload vs. Edit
- IXC-1380 : [20] Balloon Style Assignments Disappear on Quick First Step Through

## 1.1.1 2021.08.7

### Stories

- IXC-1870 : Add the Original Filename to the Drawing Upload Analytics Event
- IXC-1865 : Handle email bounces from sendgrid
- IXC-1822 : Send reminder emails if user doesn't activate
- IXC-1798 : When Metro is purchased, a worker will create the service account
- IXC-1753 : Selecting an Operator activates that Operator or User
- IXC-1537 : Design Spike: Upload revised drawing to existing part
- IXC-1406 : Notify user of Autoballoon and Text Processing progress Frontend

### Engineering Tasks

- IXC-1907 : SPIKE: Improve Email Deliverability
- IXC-1892 : Machine Learning Course
- IXC-1880 : Cleanup - set sqs queue visibility timeout to default - re-add environment variables to dev, stage and qa
- IXC-1849 : Machine Learning Course
- IXC-1811 : Validate ENV variables on startup
- IXC-1533 : Create and/or modify Connections endpoints needed to support Destination configuration
- IXC-1532 : Add IXC destination configuration support
- IXC-1531 : Create Lambda harness
- IXC-1465 : P4 : Add security headers & metatags - Codify Deploy
- IXC-1258 : Upgrade to Cypress v7
- IXC-1012 : Event Bus : Error Handling

### Bugs

- IXC-1846 : [28] Add Messaging for Failed CC Payments
- IXC-1839 : [30] "Trial Started" isn't firing as often as it should
- IXC-1704 : [30] Ensure Hubspot Deal is closed won
- IXC-1425 : [24] Correcting feature in review form doesn't clear Note warning

### Infrastructure

- IXC-1881 : Backup-Trigger Step Function Infra
- IXC-1873 : Backup-Trigger Lambda Infra
- IXC-1874 : Queue-Processor ECS Infra

## 1.1.0 2021.07.12

### Stories

- IXC-385 : Parts List Search Functionality
- IXC-1314 : Report Review View
- IXC-1310 : Report Template Gallery
- IXC-1309 : Report Template Card
- IXC-1545 : Report Save & Exit button is misleading
- IXC-1060 : Report template token UI changes
- IXC-1365 : Disable save & exit when report title is changed and not yet saved
- IXC-1844 : Change registration form label 'Password' to 'Create Password'
- IXC-1393 : Auto-Ballooning : Design Spike : Notify user of Autoballoon and Text Processing progress
- IXC-1722 : Auto-Ballooning : Processing Notification Backend
- IXC-1814 : Auto-Ballooning : Notifications Management
- IXC-1547 : Research Spike: Integrate with the Net-Inspect API
- IXC-1633 : Net-Inspect : FAI uploads
- IXC-1572 : Net-Inspect : Change analytics service from Rudder to Segment per IXC change
- IXC-1752 : METRO : Populate Operator Select Screen from DB
- IXC-1776 : METRO : Data Management Sample Data Model
- IXC-1187 : METRO : Operator Assumes Service Account
- IXC-960 : METRO : Feature Tile Block
- IXC-944 : METRO UI : Part Sample Header Info

### Engineering Tasks

- IXC-1589 : PartEditorDrawer updateNavigation uses Redux
- IXC-1588 : All uses of NavigationContext instead read from Redux
- IXC-1590 : Nav updateNavigation usesRedux
- IXC-1587 : NavigationContext writes to Redux
- IXC-1746 : Strip out useLocation in favor of redux
- IXC-1591 : Delete NavigationContext
- IXC-1825 : Suppress Analytics for Bots
- IXC-1872 : Setup Athena for CloudFront Logs in prod
- IXC-1862 : Increase sqs queue visibility timeout to 300 seconds in terraform
- IXC-1860 : Review CloudFront logs in S3
- IXC-1859 : Remove Default Error Pages from CloudFront
- IXC-1853 : Bump access for Dan Crowley
- IXC-1831 : Send UMC a final copy of their QX data
- IXC-1829 : Setup Redis for Socket.io Notification Backend
- IXC-1810 : Build CORs whitelist for backend
- IXC-1803 : Consolidated Billing Access for Tyler Glenski @ Ideagen
- IXC-1801 : Updates for localhost https
- IXC-1791 : Address May 2021 Pen Test Issues
- IXC-1778 : Update Backend & Client Errors
- IXC-1770 : Setup segment.io Cloudfront proxy in QA, Stage, and Prod
- IXC-1747 : Tear down unleash
- IXC-1742 : Fix Reporting and Template Tests
- IXC-1723 : CI/CD for webhook listener and consumer lambda's
- IXC-1649 : Functional Spike : Automated Deletion of Records Created by Tests in Chargify/Hubspot
- IXC-1109 : Setup APM for all backend and EDA

### Bugs

- IXC-1797 : [56] User gets Redirected to Sign-in Page when Following Registration Email Link
- IXC-1794 : [42] Template library not displaying all templates
- IXC-1805 : [40] Verifying an item doesn't persist
- IXC-1757 : [40] Review and Update Handling of Subscription Changes for Trialing Users w/ Pending Payment
- IXC-1741 : [40] Hidden leader lines are appearing in printed PDF
- IXC-1743 : [35] Feature Changes don't Persist if Triggering Certain Actions before Blurring Input
- IXC-1704 : [30] Ensure Hubspot Deal is closed won
- IXC-1832 : [30] Parts with large numbers of features cause an entity.too.large error when entering reporting
- IXC-1359 : [28] Part info caching old data when changed from the part info drawer
- IXC-1262 : [28] Excel Cells Imported with a "Number" Type Fill with a 0 Value in Reports
- IXC-1843 : [28] currentSheet not being properly passed to sheet updates when editing a report
- IXC-1802 : [20] Subscriptions Not changing Price Point correctly after Trial Ended
- IXC-1793 : [27] Can't cancel expired invite
- IXC-1828 : [14] Clicking a feature in the canvas does not change the selected feature in the review drawer
- IXC-1806 : [12] Part Stepping/Ballooning is not working properly when autoballooning is turned off
- IXC-1756 : [10] Long refresh lag when editing user in seats table
- IXC-1842 : [7] Prevent oddly shaped click events from erroring the canvas

## 1.0.11 2021.07.07

### Hotfix

- Increase ECS memory for backend container

## 1.0.10 2021.06.02

### Stories

- IXC-1726 : Hubspot contact links to Amplitude user timeline
- IXC-1677 : Pull account information into company settings page
- IXC-1635 : Allow Orphan Users to Re-Register or Receive Invite
- IXC-1404 : CharacteristicsCanvas Redux Conversion
- IXC-1311 : Report/Template Header Update

### Engineering Tasks

- IXC-1721 : Fix issue with image-capture-to-characteristic lambda on ixc-dev
- IXC-1719 : State of the Quarter
- IXC-1695 : Adjust JWT to separate refresh token out and derive sessionId
- IXC-1686 : Machine Learning Course
- IXC-1419 : Setup segment.io proxy in Cloudfront
- IXC-1338 : Mock Account Service in Jest

### Bugs

- IXC-1744 : [30] UUID's Shown in Review Panel Selects when Re-opening a Part
- IXC-1734 : [20] Google Fonts Currently Blocked by CSP
- IXC-1725 : [24] Hubspot contact's company name not updating
- IXC-1718 : [28] Prevent overwriting of Hubspot company name
- IXC-1717 : [16] Analytics Identify Calls Firing Excessively
- IXC-1692 : Chargify Catalog not loading

## 1.0.9 2021.05.18

### Bugs

- IXC-1745 : Unable to Convert from Trial Ended to Active Subscription

## 1.0.8 2021.05.10

### Stories

- IXC-1694 : Remittance provisional subscription handling
- IXC-1693 : Credit Card Error Handling
- IXC-1680 : Trial Bar UI Update
- IXC-1675 : Add Wistia to CSP Policy
- IXC-1668 : Update Auto-Renewal Opt-in language
- IXC-1667 : Branding Updates - Favicon & Page Name
- IXC-1639 : Adjust roles & privlages
- IXC-1628 : Opt-in language for renewal
- IXC-1122 : Data Management : Research Spike : Middleware Choice for async actions and side effects
- IXC-1120 : Data Management : Add chosen middleware for async actions and side effects
- IXC-685 : Capture : Improve Note Recognition when Ballooning
- IXC-637 : Signup Flow for New User to Existing Account

### Engineering Tasks

- IXC-1697 - New relic analysis in remote envs
- IXC-1696 - Product Instrumentation Updates
- IXC-1650 - Functional Spike : MailSlurp for Testing Emails

### Bugs

- IXC-1698 - [42] Resend Verification Email Form is not Submitting
- IXC-1669 - [54] Planner can Give Self Billing Permission
- IXC-1663 - [42] Error appers when you tried to choose role for new user
- IXC-1654 - [60] Catalog is not being accessed correctly on the frontend
- IXC-1617 - [36] Unregistered user can reset password, "Hi {3}" displayed in reset email

## 1.0.7 2021.05.04

### Bugs

- Resend verification email fix

## 1.0.8 2021.05.10

### Stories

- IXC-1694 : Remittance provisional subscription handling
- IXC-1693 : Credit Card Error Handling
- IXC-1680 : Trial Bar UI Update
- IXC-1675 : Add Wistia to CSP Policy
- IXC-1668 : Update Auto-Renewal Opt-in language
- IXC-1667 : Branding Updates - Favicon & Page Name
- IXC-1639 : Adjust roles & privlages
- IXC-1628 : Opt-in language for renewal
- IXC-1122 : Data Management : Research Spike : Middleware Choice for async actions and side effects
- IXC-1120 : Data Management : Add chosen middleware for async actions and side effects
- IXC-685 : Capture : Improve Note Recognition when Ballooning
- IXC-637 : Signup Flow for New User to Existing Account

### Engineering Tasks

- IXC-1697 - New relic analysis in remote envs
- IXC-1696 - Product Instrumentation Updates
- IXC-1650 - Functional Spike : MailSlurp for Testing Emails

### Bugs

- IXC-1698 - [42] Resend Verification Email Form is not Submitting
- IXC-1669 - [54] Planner can Give Self Billing Permission
- IXC-1663 - [42] Error appers when you tried to choose role for new user
- IXC-1654 - [60] Catalog is not being accessed correctly on the frontend
- IXC-1617 - [36] Unregistered user can reset password, "Hi {3}" displayed in reset email

## 1.0.7 2021.05.04

### Bugs

- Resend verification email fix

## 1.0.6 - 2021.04.27

### Bugs

- Trial bar
- $0 component
- Phone number fix
- Pipeline ENV vars

## 1.0.5 - 2021.04.26

### Bugs

- Pipeline ENV vars fix

## 1.0.4 - 2021.04.26

### Bugs

- Pipeline ENV vars fix

## 1.0.3 - 2021.04.26

### Bugs

- Fixed CRM params in ecs configs

## 1.0.2 - 2021.04.26

### Bugs

- Added new_relic_license key to ECS tasks
- Fixed chargify link to customer in subscriptionWorker
- Updated IXC Sales pipeline name
- Added product familiy filter to subscriptionWorker to filter out IXOD subscriptions

## 1.0.1 - 2021.04.26

### Bugs

- Added chargify env variables to ECS config
- Added subscription analytics to Amplitude events
- Fixed log error in migration

## 1.0.0 - 2021.04.24

### Stories

- IXC-1667 : Branding Updates - Favicon & Page Name
- IXC-1656 : Company settings page
- IXC-1655 : Sync IXC Account Members with Hubspot Company Contacts
- IXC-1652 : Transition Hubspot Record from SLA to MQA on First Part Upload
- IXC-1647 : Manage Reused Domains and Emails in Hubspot and Chargify on Account Creation
- IXC-1639 : Adjust roles & privlages
- IXC-1630 : Provide a way for account owners to delete their trials
- IXC-1628 : Opt-in language for renewal
- IXC-1627 : Allow trial account creation when users domain is used in an active subscription
- IXC-1557 : Create Net-Inspect API client
- IXC-1555 : User registration via Hubspot Form : Profile Setup
- IXC-1552 : Refine Note text
- IXC-1549 : Make hosts file
- IXC-1514 : User registration via Hubspot Form : API Call
- IXC-1507 : Feature flag invite new member
- IXC-1442 : Change localhost to ixc-local.com and add hosts file configuration instructions to readme
- IXC-1403 : ItemPanel Redux Conversion
- IXC-1115 : Restrict Access to ITAR/EAR Documents Based on User Settings
- IXC-877 : [Test Verification] Update Signup Flow for New Account to include Subscription as a Trial
- IXC-637 : Signup Flow for New User to Existing Account
- IXC-462 : Leader Lines from Balloons to Capture Box

### Engineering Tasks

- IXC-1670 : Update Snippets for Appcues and Help Icon
- IXC-1662 : OCR Load Balancing
- IXC-1658 : Setup Johns on-boarding services in dev / qa
- IXC-1608 : Add build version and githash to the page and config
- IXC-1605 : Load testing
- IXC-1535 : Amplitude event refinements
- IXC-1512 : Replace "loading" text with spinners
- IXC-1028 : Add Approval to Deploy Build If Any Migrations Fail
- IXC-1027 : Migrate Database Before Deploy of New Build
- IXC-348 : [monitoring] : ixc-mono Backend

### Bugs

- IXC-1363 : [74] Editing a template while it is writing a previous save throws an error
- IXC-1556 : [60] Deployed app does not clear cookies, even when response header says to do so
- IXC-1666 : [54]Creating a capture errors out the page
- IXC-1629 : [54] Win 10, Firefox - Not all balloons in list displayed when opening part with many balloons
- IXC-1669 : [54] Planner can Give Self Billing Permission
- IXC-1457 : [50] Style Presets Migrations
- IXC-1619 : [48] Add Balloon Style-Second cascade menu option unselect on click other 1st menu option
- IXC-1505 : [45] Owner can't Change their ITAR/Restricted Setting
- IXC-1128 : [42] Win 10, Chrome - Unable to add part information and item information
- IXC-1620 : [42] Settings: Error "errors.query.susbcription" when opening Account Settings pages
- IXC-1621 : [42] Possible to open Restricted - ITAR document, an error occurred when opening
- IXC-1663 : [42] Error appers when you tried to choose role for new user
- IXC-1632 : [42] Email addresses should not be case-sensative
- IXC-1631 : [42] Can't reliably download reports from part detail panel
- IXC-1622 : [42] Account Administrator can't set the Access Control to "Unrestricted access"
- IXC-1580 : [40] Bad filepath for email logo causes invitation failure
- IXC-1617 : [36] Unregistered user can reset password, "Hi {3}" displayed in reset email
- IXC-1377 : [32] Extraction ‚Üí Report ‚Üí Cancel/Back should return to Extraction
- IXC-1623 : [32] Access Control drop-down isn't anchored to its item, page layout becomes broken
- IXC-1420 : [30] Really Wide Captures Cause Balloons To Go Off Page
- IXC-1542 : [30] Clearing Style Assignment Cascader Breaks Style Presets in Configurations
- IXC-1254 : [28] Balloon Placement on Capture doesn't Properly Account for Page Rotation
- IXC-1634 : [9] authSignOut resolver should return string
- IXC-1583 : [3] Save new Tolerance Preset modal has an incorrect header

## 0.1.19 - 2021.04.05

### Stories

- IXC-1271 : Named Presets : Tolerances
- IXC-660 : Balloon: Manually Adjust Size
- IXC-462 : Leader Lines from Balloons to Capture Box
- IXC-1115 : Restrict Access to ITAR/EAR Documents Based on User Settings
- IXC-1552 : Refine Note text
- IXC-1371 : CharacteristicsContext is also writing to Redux
- IXC-1165 : Ext : Production Email Templates
- IXC-1115 : Restrict Access to ITAR/EAR Documents Based on User Settings

### Engineering Tasks

- IXC-1524 : [80] Resolve drawing-analyzer container issues reported by Vanta
- IXC-1523 : [80] Resolve ixc-mono-backend container issues reported by Vanta
- IXC-1512 : Replace "loading" text with spinners
- IXC-1503 : Codify S3 bucket for catalog
- IXC-1502 : AWS kickoff to merge in base branch
- IXC-1025 : Backup Process Can Include Git Tag and Git Hash in File Name
- IXC-916 : Move template settings out of context

### Bugs

- IXC-1556 : [60] Deployed app does not clear cookies, even when response header says to do so
- IXC-1457 : [50] Style Presets Migrations
- IXC-1619 : [48] Add Balloon Style-Second cascade menu option unselect on click other 1st menu option
- IXC-1505 : [45] Owner can't Change their ITAR/Restricted Setting
- IXC-1622 : [42] Account Administrator can't set the Access Control to "Unrestricted access"
- IXC-1621 : [42] Possible to open Restricted - ITAR document, an error occurred when opening
- IXC-1632 : [42] Email addresses should not be case-sensative
- IXC-1631 : [42] Can't reliably download reports from part detail panel
- IXC-1128 : [42] Unable to add part information and item information
- IXC-1580 : [40] Bad filepath for email logo causes invitation failure
- IXC-1320 : [36] Report template Provider can't be changed after adding a new one
- IXC-1364 : [36] User not taken to parts page after confirming email
- IXC-1350 : [35] Sub-balloon locations all show parent balloon location
- IXC-1623 : [32] Access Control drop-down isn't anchored to its item, page layout becomes broken
- IXC-1377 : [32] Extraction ‚Üí Report ‚Üí Cancel/Back should return to Extraction
- IXC-1542 : [30] Clearing Style Assignment Cascader Breaks Style Presets in Configurations
- IXC-1420 : [30] Really Wide Captures Cause Balloons To Go Off Page
- IXC-1254 : [28] Balloon Placement on Capture doesn't Properly Account for Page Rotation
- IXC-1624 : [15] Save button shifts if the Style Preset name is too long
- IXC-1583 : [3] Save new Tolerance Preset modal has an incorrect header

## 0.1.18 - 2021.03.22

### Stories

- IXC-1525 : Make redux devtools have trace enabled
- IXC-1270 : Balloon: Named Style Presets
- IXC-1214 : [BE] Add/Edit Jobs
- IXC-1165 : Ext : Production Email Templates
- IXC-1115 : Restrict Access to ITAR/EAR Documents Based on User Settings
- IXC-1000 : Connect Billing UI to backend's subscriptionService
- IXC-474 : FUNctional Spike: Items List - Drag and Drop

### Engineering Tasks

- IXC-1516 : Backup Process Reports on Result to Calling Service
- IXC-1466 : Terraform Vanta ECR Scanning
- IXC-1400 : P4 : Alerts Multiple
- IXC-1303 : Ext : Annual Contract and Terms & Conditions
- IXC-1287 : Run Sonar scanner during each deploy
- IXC-1180 : Back-office integration failure state Alerts and Queue
- IXC-1024 : Backup Process Can Backup Primary Database
- IXC-916 : Move template settings out of context

### Bugs

- IXC-1542 : [30] Clearing Style Assignment Cascader Breaks Style Presets in Configurations
- IXC-1536 : [12] Edit Horizontal Template Dynamic Footer Error
- IXC-1457 : [50] - Style Presets Migrations
- IXC-1420 : [30] Really Wide Captures Cause Balloons To Go Off Page
- IXC-1382 : [32] Feature capture fails when previous capture resolves
- IXC-1377 : [32] Extraction ‚Üí Report ‚Üí Cancel/Back should return to Extraction
- IXC-1363 : [74] Editing a template while it is writing a previous save throws an error
- IXC-1320 : [36] Report template Provider can't be changed after adding a new one
- IXC-1254 : [28] Balloon Placement on Capture doesn't Properly Account for Page Rotation
- IXC-1253 : [35] Retest - Unable to Expand Features that were Switched from Note to Dimension
- IXC-1128 : [42] Win 10, Chrome - Unable to add part information and item information

## 0.1.17 - 2021.03.10

### Stories

- IXC-1508 : Custom balloons should be editable after creation
- IXC-1435 : Basic dimensions should auto-frame
- IXC-1422 : Verify toggle should lock editing of feature
- IXC-1388 : P2 : Enforce Password Complexity
- IXC-1264 : Symbol Selector Improve UX
- IXC-1214 : [BE] Add/Edit Jobs
- IXC-1115 : Restrict Access to ITAR/EAR Documents Based on User Settings
- IXC-1002 : Add IXC support for managing SPHINX API keys
- IXC-831 : Move Part Details to the right side of the screen
- IXC-810 : Ext : Production Trial Signup Form
- IXC-474 : FUNctional Spike: Items List - Drag and Drop
- IXC-462 : Leader Lines from Balloons to Capture Box
- IXC-298 : Symbol insert and delete from full specification

### Engineering Tasks

- IXC-776 : SOC II Type 2 Audit
- IXC-1389 : P2 : Secure session management
- IXC-1391 : P3 : Investigate user list leakage
- IXC-1390 : P3 : Investigate / mitigate elevation of privileges
- IXC-1400 : P4 : Alerts Multiple
- IXC-1396 : P4 : Validate upload file types
- IXC-1394 : P4 : Update React and resolve npm audit alerts
- IXC-1347 : P4 : Cache busting so hard refresh isn't necessary after deploy
- IXC-1408 : Update production domain name
- IXC-1517 : Update PDFTron License keys
- IXC-1026 : Spike: Backup Process Reports on Result to Calling Service
- IXC-1516 : Backup Process Reports on Result to Calling Service
- IXC-1501 : Lockdown ECS task roles
- IXC-1469 : Setup CNAMES and DNS Lookup for Sendgrid
- IXC-1467 : Fix kickoff source versions
- IXC-1246 : Create endpoint for retrieving API key by account / site / user ids
- IXC-1219 : Product Catalogue Service
- IXC-296 : Integrated test for editing a characteristic

### Bugs

- IXC-1473 : [40] Default Balloons have Dotted Borders Instead of Solid
- IXC-1364 : [36] User not taken to parts page after confirming email
- IXC-1253 : [35] Retest - Unable to Expand Features that were Switched from Note to Dimension

## 0.1.16 - 2021.02.23

### Stories

- IXC-462 : Leader Lines from Balloons to Capture Box
- IXC-1270 : Balloon: Named Style Presets
- IXC-474 : FUNctional Spike: Items List - Drag and Drop
- IXC-1214 : [BE] Add/Edit Jobs
- IXC-1388 : P2 : Enforce Password Complexity
- IXC-1370 : Balloon: Designer
- IXC-1369 : Human Task Lifecycle Orchestration - Update Part View
- IXC-1368 : Human Task Lifecycle Orchestration - No Reviewers Online

### Engineering Tasks

- IXC-1401 : Migration Investigation Tool
- IXC-1455 : Fix unleash CICD
- IXC-1454 : Roll out new OpenVPN AMI across all env's (per Vanta vulnerability finding)
- IXC-1451 : Codify ECS autoscaling policies in Terraform
- IXC-1426 : CICD should fail on scripts exiting non-zero
- IXC-1412 : Reconfigure CI to deploy tags on develop to QA
- IXC-1402 : P4 : Add security headers & metatags
- IXC-1391 : P3 : Investigate user list leakage
- IXC-1367 : Swap rudder to segment.io
- IXC-1321 : Convert reporting Context to Redux Store
- IXC-610 : Auto-Scaling Services
- IXC-283 : Unit Test for Image Capture
- IXC-189 : test for capture rectangle

### Bugs

- IXC-1457 : [50] Style Presets Migrations
- IXC-1424 : [60] Markers Migration Failing
- IXC-1364 : [36] User not taken to parts page after confirming email
- IXC-1353 : [35] Balloon styles not being applied in some cases

## 0.1.15.1 - 2021.02.19

- Removed @pdftron/webviewer-downloader ^1.4.2 from frontend package.json to resolve deploy failure due to use of ssh as dependency protocol

## 0.1.15 - 2021.02.06

### Stories

- IXC-1231 : Human Task Lifecycle Orchestration
- IXC-1232 : Human Task Scheduling for Auto-Ballooning
- IXC-1238 : 3rd party pen test for SOC2
- IXC-1269 : Leader Lines Manually Adjust
- IXC-449 : Extraction Toolbar - Hide/Show
- IXC-672 : Symbol Selector modal should float

### Engineering Tasks

- IXC-1080 : SOC2 : Groups manage employee accounts permissions
- IXC-1081 : SOC2 : Logs retained for 365 days
- IXC-1113 : [oncall] - Setup OpsGenie
- IXC-1143 : Fix failing cypress tests
- IXC-1213 : Ensure we're using TLS 1.1 everywhere
- IXC-1297 : Rudder ECS Task Definitions
- IXC-1298 : Rudder Observability
- IXC-1333 : Setup rudder schema in RDS (all envs)
- IXC-1362 : Upgrade OpenVPN Severs per Vanta Scan
- IXC-1367 : Swap rudder to segment.io
- IXC-1392 : P2 : TLS 1.2 and HTTPS Only
- IXC-350 : Alerting on-call
- IXC-435 : Watch for PR merge that will allow Rudder to be loaded from CDN
- IXC-450 : Integrate Rudder into infrastructure
- IXC-508 : Create ECS Fargate infrastructure for rudder

### Bugs

- IXC-1085 : [40] When you group a characteristic that can be expanded you can no longer expand it
- IXC-1376 : [48] Sub balloons aren't updating correctly
- IXC-1379 : [10] Sub balloons jump after they are moved
- IXC-1385 : [] Features that throw an error are not selected after capture
- IXC-1386 : [72] Loss of access for a user
- IXC-993 : [36] Win10, Chrome - Comments are transferring on its own in Item Review when adding data

## 0.1.14 - 2021.01.26

### Issues

- None

### Bugs

- IXC-1085 [40] When you group a characteristic that can be expanded you can no longer expand it
- Hotfix : Crash on first capture of new part
- Hotfix : Editing card not updating full spec
- Dashed box animation speed set faster

## 0.1.13 - 2021.01.25

### Stories

- IXC-1064 : Resize balloon when first capture box is resized
- IXC-1205 : UI: Show OCR errors associated with captures
- IXC-1209 : UI: Show capture status while OCR is being performed - backend
- IXC-1230 : Human Task Queue Setup
- IXC-1244 : Improve visual cue when clicking "Verify" in Review Mode
- IXC-1342 : Functional Spike: Async Middleware Pattern and Guide for Redux Conversion
- IXC-1361 : Fix distinction between Select vs Text input CSS
- IXC-769 : Complete Ant Design Migration from 3.0 to 4.0

### Engineering Tasks

- IXC-1216 : Create Jira Burndown Charts
- IXC-1345 : Update Cypress Tests for Ant Design V4
- IXC-463 : Mock-up Needed

### Bugs

- IXC-1127 : [40] Win10, Chrome - Numbers don't fit completely in balloon after selecting "with Sub Balloons" option
- IXC-1217 : [30] Changing characteristic Type/Subtype does not change selected unit/available units for most types
- IXC-1332 : [54] Canceling Edit of an Existing Template Deletes the Template
- IXC-1339 : [40] Default AS9102B not Handling the Footer Row on Form 3 Correctly
- IXC-1358 : [45] Can't change Inspection Method
- IXC-733 : [36] [Edge] - Parts - Part does not show in list when adding by using "New Customer" from drop down list
- IXC-803 : [27] Chrome - Item list - No space seen between balloon style and grouped numbers in the item list

## 0.1.12 - 2021.01.11

### Stories

- IXC-1192 : [FE] Add/Edit Jobs
- IXC-758 : For GD&T disallow editing via Full Spec field
- IXC-946 : UI : Data Entry
- IXC-947 : UI : Number Pad for Data Entry

### Engineering Tasks

- IXC-1084 : Add Jest Tests for Review Panel
- IXC-1295 : Rudder ECS Fargate Infra
- IXC-1296 : Rudder CICD Infra in Terraform

### Bugs

- IXC-1290 : [40] - Wrong Precision is Applied for Features with Trailing 0's
- IXC-1325 : [40] - Report fields skipped of filled with "Not Found" for Types, Classifications, Ops, Methods, Units
- IXC-1343 : [42] Win 10, Firefox - Error 403 on clicking Report button on the feature panel
- IXC-792 : [36] Error in PartDetailsDrawer after deleting a part
- IXC-991 : [30] Win10,Firefox - Json Network error is shown on the page

## 0.1.11 - 2020.12.28

### Stories

- IXC-1233 : Human Reviewer Section Setup
- IXC-1234 : Reviewer Engagement Management
- IXC-1235 : UI for Human Review of Capture
- IXC-1243 : Tolerances Setup : Support tabbing auto-select entire tolerance value on focus
- IXC-623 : For Notes don't display Full Spec input
- IXC-766 : Functional Spike: Upgrade to Ant Design 4.0
- IXC-830 : Add Operation and Inspection Method as options for Balloon Styles
- IXC-848 : Integrate IXC as a source for SPHINX

### Engineering Tasks

- IXC-1206 : UI: Refactor OCR to only call the EDA once per capture and look at CNN and Tess model results
- IXC-1222 : Ensure event handlers are cleaned up properly
- IXC-1226 : Copy : UI Text Updates for Clarity
- IXC-1328 : Extract failing cypress tests to separate branch

### Bugs

- IXC-1129 : [36] macOS 10.13.6, Firefox - Report - Blank page shown after selecting custom template
- IXC-1256 : [8] - Duplicate Diameter Symbol in Insert Character Modal
- IXC-1299 : [40] - Different Diameter Symbols being Used by the ENP for Notes/GDT vs. Dimensions

## 0.1.10 - 2020.12.16

### Bugs

- Removed redux-offline that was breaking deploy
- Moved Metro nav options behind feature flag

## 0.1.9 - 2020.12.16

### Stories

- IXC-1210 : UI: Show capture status while OCR is being performed - frontend
- IXC-1252 : Improve Report Generation UX
- IXC-1316 : Preview Part Data when Building Template
- IXC-554 : Improve Error Handling for Failed Extractions
- IXC-648 : Persistent Trial Notification

### Engineering Tasks

- IXC-1133 : Recognize Fremium vs Time Bound Subscription Trial
- IXC-1172 : Reporting : Design Cycle - Footer Row Interaction
- IXC-1301 : Update parseOcr method to use correct dictionary for Notes

### Bugs

- IXC-1123 : [48] Win 10, Chrome - Fatal memory error shown while setting template tokens
- IXC-1125 : [42] Win 10, Chrome - Report generation gets struck at 50%
- IXC-1130 : [36] Win 10, Chrome - Error occurs after edit report and click Save & Exit button
- IXC-1318 : [] Seed Files need to be Updated with Default Tolerances using UUIDs for Units

## 0.1.8 - 2020.12.07

- Commented out check for subscription status which was raising an error

## 0.1.7 - 2020.12.07

### Stories

- IXC-1004 : Keep Horizontal Scrollbar Visible in the Main Content Area on a Vertically Scrollable Page
- IXC-1013 : Setting Lists: Customize Classifications
- IXC-1014 : Setting Lists: Customize Operations
- IXC-1015 : Setting Lists: Customize Inspection Methods
- IXC-1016 : Setting Lists: Customize Types & Sub-Types
- IXC-1059 : Changing Seat Details
- IXC-1062 : Invite New Users - Send Email
- IXC-1063 : Invite New Users Integration
- IXC-1066 : UI : Ensure All Hooks are Being Sent to Store
- IXC-1067 : Add Redux-Offline and Configure Offline Queue
- IXC-1068 : Product instrumentation
- IXC-1096 : Add a Seat Component to a subscription when user hits their limit
- IXC-1132 : Fremium Trial bound by number of drawings (No time limit)
- IXC-1225 : Refine Switch UI in Configurations and Review Panel
- IXC-1263 : Frequently Used Characters for the Insert Characters Modal
- IXC-16 : Research how to measure End-to-End User Acceptance
- IXC-269 : Extract a Tolerance of Position feature control frame with no datum
- IXC-304 : Accurately capture notes next to a dimension
- IXC-338 : Manage team preferences
- IXC-389 : Support for complex notations in GD&T tolerance zones
- IXC-489 : Error Handling w/API
- IXC-490 : Changes to Global Part Settings should Propagate through the Part's Data
- IXC-652 : Seat Management
- IXC-676 : Change 'Save' to 'Next' in PDF Upload
- IXC-705 : Functional Spike: Correctly track marketing source attribution
- IXC-707 : Investigate browser pop up issues with IE
- IXC-728 : Template Manager Lists
- IXC-742 : Template Manager Panel
- IXC-765 : [Archive]Setting Lists
- IXC-808 : Fix failing Cypress tests from IXC-717 branch
- IXC-822 : Report Editor Form: Data Filters
- IXC-938 : UI : Meta Data Block
- IXC-943 : UI : Part Sample Header
- IXC-945 : UI : Measurement Data Table

### Engineering Tasks

- IXC-1050 : Functional Spike: SmartExtract Functionality in IX3
- IXC-1052 : Webhook Handlers for CRM & Billing Systems
- IXC-1082 : SOC2 : Infrastructure dependencies checked for vulnerabilities
- IXC-1087 : Spike : Optimize DB reset and seed scripts in cypress tests
- IXC-1104 : [vanta] - Enable VPC flow logs
- IXC-1106 : [vanta] - Network diagram
- IXC-1117 : Revamp IAM groups to work around 8 per user maximum
- IXC-1137 : Analysis - Default Tolerances cypress tests are failing
- IXC-1141 : Analysis - Wizard cypress tests are failing
- IXC-1149 : Global - Token - Logging out a user cypress tests fail
- IXC-1150 : Parts List - Drawer cypress tests fail
- IXC-1152 : Report - New Report cypress tests fail
- IXC-1159 : Setting - Billing cypress tests fail
- IXC-1161 : Setting - Menu & Preferences cypress tests fail
- IXC-1167 : Create network diagram
- IXC-1169 : Review : Panel and Reporting UI Updates
- IXC-1170 : Review : Improve Handling of Feature Card Verification
- IXC-1171 : Reporting : Design Cycle - Reporting Flow
- IXC-1175 : Reporting : Design Cycle - Token Presentation in Templates
- IXC-1208 : ENP: Extract tolerances from classifier
- IXC-1211 : Invalidate stale assets after successful deploy
- IXC-1212 : Trigger Deploys on push tags to master
- IXC-1215 : Branding Requests for Marketing
- IXC-1293 : Rudder PostgresSQL Aurora DB
- IXC-1294 : Rudder Destination (S3?)
- IXC-195 : Lunch & Learn: Design Systems
- IXC-196 : Lunch & Learn: Front End Testing
- IXC-349 : On-call Management
- IXC-391 : Functional Spike: Connection Quality Monitor
- IXC-401 : Reserve AWS Instances
- IXC-452 : Move new UI components into storybook and create documentation
- IXC-499 : Create tests for an object with Model, Repository, Service, and API
- IXC-500 : Create method of test coverage of backend, frontend, and cypress tests
- IXC-551 : Provide backend app containers access to S3 for document uploads
- IXC-564 : Add CloudWatch -> Elasticsearch logs in Lambda modules
- IXC-657 : [Needs Review] Set up LocalStack to Emulate AWS Locally
- IXC-684 : Better form experience like Typeform
- IXC-807 : Improve up-time reporting accuracy
- IXC-832 : [Needs Review] Research Spike: Get services running on LocalStack & tests passing
- IXC-833 : Reconcile when Part changes should trigger Out of Date for a Report
- IXC-843 : Integrate the implemented source, transformer, and destination
- IXC-854 : Integrate repo with SonarCloud
- IXC-855 : Document how to turn off Auto-Ballooning
- IXC-883 : Integrate with database
- IXC-970 : Create Classifier module
- IXC-979 : Subscription Info UI: Change
- IXC-997 : Create configurations repository

### Bugs

- IXC-1035 : [45] Report page crashes when selected on cell with formula
- IXC-1036 : [54] Formatting for Excel Report Template lost
- IXC-1056 : [15] Excel files exporting with unexpected cell styles
- IXC-1134 : [] [Test bug from Applause testing services]
- IXC-1181 : [50] Walkme smart tip not rendering correctly
- IXC-1302 : [40] Reports and Templates Download and Naming Issues
- IXC-263 : [54] Modifier for Unequally Disposed Profile Tolerance (U) - Not handled correctly
- IXC-553 : [10] Pressing refresh returns 404 error
- IXC-580 : [60] Unit select in review shows UUID
- IXC-581 : [20] Review form inputs overflowing drawer container
- IXC-591 : [48] Balloons Don't Load when Opening an Existing Part
- IXC-595 : [40] User location on Part Analysis changing when browser refreshes
- IXC-598 : [8] Inconsistent drop shadows on the edges of panels
- IXC-599 : [8] Top toolbar should be centered to document viewport when right doc navigator drawer is open
- IXC-605 : [16] Balloon Styles: Condition is referred to in validation message but missing corresponding label in UI
- IXC-606 : [20] Remove buttons with no functionality
- IXC-614 : [36] Manual ballooning providing captures sporadically
- IXC-625 : [54] Items are bouncing back and forth while in review mode
- IXC-681 : [20] Settings Menu detaches relation to Nav Bar when scrolling on the screen
- IXC-688 : [50] Cannot add styles in Ballon Styles page
- IXC-709 : [50] Part info exit button navigates to default tolerances
- IXC-718 : [27] Default unit of measure for Metric Linear should be millimeter
- IXC-721 : [8] CORS error when calling Walkme
- IXC-722 : [40] Deleting part from parts drawer doesn't trigger user confirmation
- IXC-797 : [40] Radius with Minimum not processed correctly
- IXC-873 : [50] Calculation error when adding symbol in a GD&T
- IXC-908 : [50] Download Files (Part Hub Panel) not working
- IXC-921 : [54] Chrome-Error display & unable to open report after editing and 1st item Full spec has special symbol
- IXC-923 : [54] Win 10,Chrome-Error occur while tapping the next icon in Choose a Template.
- IXC-928 : [54] Win 10, Edge - Edited parts Full Specification is not reflecting on the downloaded report
- IXC-929 : [54] Win 10, Chrome - When unit is selected as centimetre, the length data captured is in inches
- IXC-966 : [45] 'Note' showing up as a sub-type under Dimension
- IXC-972 : [40] Footer Row not removing across the entire row
- IXC-995 : [54] Win10,Firefox - Set Template Tokens slowing down a browser & error message shows

## 0.1.6 - 2020.11.04

### Stories

- IXC-1058 : Seats Management UI
- IXC-1061 : Invite New Users Modal
- IXC-1070 : METRO Design Document
- IXC-1072 : Single capture / review stage
- IXC-1075 : Item list does not collapse
- IXC-1076 : Review stage page browser remains closed
- IXC-1079 : Vanta : SSL configuration has no known issues
- IXC-1097 : Admin/Owner Can Manage Pending Invites
- IXC-1118 : Install : Availability of PWA
- IXC-649 : Invite New Users
- IXC-651 : Upgrade Subscription
- IXC-654 : Product Instrumentation
- IXC-666 : Functional Spike: Extraction Review - Zoom in and follow
- IXC-686 : Hide Extraction Toolbar when not in Ballooning or Review
- IXC-755 : Update Chargify through Service for Account Management
- IXC-757 : Copy : Hard code "Default" next to default balloon style
- IXC-768 : Copy : Change the Part Analysis buttons to 'Next" and 'Previous'
- IXC-775 : SOC II Type 1 Corrective Action Plan
- IXC-876 : Reporting : Return to Part List - Part Hub after saving report
- IXC-878 : Item List should auto-scroll when new Items are extracted
- IXC-957 : Hubspot Client
- IXC-959 : Sample Trial Form / Landing Page
- IXC-964 : Install : Installation Authentication
- IXC-978 : Billing Settings

### Engineering Tasks

- IXC-1001 : Create endpoints for Connections CRUD operations
- IXC-1003 : Policy Review
- IXC-1018 : John > Team Knowledge Transfer
- IXC-1033 : Cleanup manually created CICD POC resources in AWS shared account
- IXC-1040 : [Due 10/23] Send UMC backup of data
- IXC-1045 : Setup QA Infrastructure
- IXC-1049 : Production infrastructure
- IXC-1051 : Back-Office System Integration
- IXC-1053 : Push to Staging
- IXC-1054 : Orientation for Cody
- IXC-1073 : Support QA and Production Deploy
- IXC-1074 : Downgrade the QualityXpert Production Environment
- IXC-1078 : Confirm Production Deploy
- IXC-1088 : Vanta Task : Monitoring
- IXC-1098 : Cypress Testing Review and Upgrade
- IXC-1102 : Vanta vulnerability scanner on EC2 instances
- IXC-1105 : [vanta] - Redirect HTTP to HTTPS on load balancers
- IXC-1121 : Upgrade to Cypress 5+
- IXC-1135 : Analysis - Balloon Styles - Wizard Progression cypress tests are failing
- IXC-1136 : Analysis - Balloon Styles - Balloon Styles update on changes cypress test is failing
- IXC-640 : Rework frontend DNS and backend to be behind CloudFront in stage
- IXC-715 : Uniform style and linting forms to be discussed - Typescript
- IXC-866 : Finish Dynamic Tokens
- IXC-962 : Build tests to confirm data encapsulation
- IXC-982 : Payment Info UI
- IXC-985 : Add NewRelic agent to Queue Processor
- IXC-986 : Add CI/Buildspec to Queue Processor

### Bugs

- IXC-1008 : [60] Having a period at the end of a report name prevents downloading to Excel
- IXC-1042 : [63] Part 2 Functional Spike - Memory usage maxing out when zooming in on PDF in Extraction
- IXC-1065 : [] Balloon Jumps after Moving
- IXC-1083 : [54] Lists for new accounts are not populated
- IXC-1086 : [60] Review Form is not Saving Changes to Feature Data
- IXC-729 : [49] [Edge] - Drawing Viewer - Clicking "Parts Info" button prior to PDF loading leads to empty page
- IXC-731 : [50] [Edge] - Parts - Unable to select a different Customer from the drop down menu
- IXC-732 : [54] [Edge] - Parts List - Unable to click the different pages if clicking Page 3 initially
- IXC-737 : [12] [Chrome] - Parts- Entering an invalid character into the Column fields is freezing the page
- IXC-739 : [42] [Chrome] - Parts - Grid Zones page is blank if user does not select a customer first
- IXC-798 : [20] [Mac Chrome] - Dashboard - Error Shown for a second when refreshing or logging into the dashboard
- IXC-799 : [42] [Mac-Firefox] - New Part - White blank screen displayed after uploading multiple pages PDF
- IXC-800 : [42] {Win- FF} -Created Part - Opening already created parts show blank page with error in console
- IXC-802 : [35] [Firefox] - Parts List - Part Number or Customer fields shows empty until page has been refreshed
- IXC-805 : [12] [Edge] - Add Part - Document is cut off in preview window
- IXC-806 : [30] [Edge] - Report Generator - User lands on blank page after using back and forward button
- IXC-917 : [20] Windows 10, Firefox - Custom template blue icon and texts are overlapping
- IXC-924 : [28] Windows 10, Chrome - Adding new part or selecting fields leads to network error
- IXC-926 : [54] Win 10, Firefox - Document control in the report is shown as NaN but is set on ITAR/EAR
- IXC-927 : [54] Windows 10, Chrome - Units Cm and M display as string of numbers and letters when selected
- IXC-992 : [10] Win 10,Chrome - Dynamic Data icon appears truncated/misaligned n Set Template Tokens section,

## 0.1.5 - 2020.09.29

Added DB migrations to dockerfile for backend builds to succeed

### Stories

- IXC-862 : Functional Spike: METRO App

### Engineering Tasks

- IXC-1039 : [Due 9/26] Send UMC Backup of data and PDFs

## 0.1.4 - 2020.09.28

- Critical Defects
- Added QA and Prod to CICD

## 0.1.3 - 2020.09.28

### Stories

- IXC-567 : Choose a Subscription Page
- IXC-613 : Show version number/build on page
- IXC-713 : Downloading a Report w/o GDT Font
- IXC-811 : UX/UI Trial & Subscription: Manage Seats
- IXC-813 : UX/UI Trial and Subscription: Subscribe / Trial Expired
- IXC-909 : Functional Spike: Improve Auto-Balloon Capture Rectangle Accuracy
- IXC-930 : Logging in : Operator Profile
- IXC-932 : UI : Choosing a Station
- IXC-934 : Install : UI : Navigation Header
- IXC-935 : UI : Selecting a Job to Enter Measurements For
- IXC-936 : UI : Selecting a Part to Measure
- IXC-937 : UI : Meta Data Tiles
- IXC-942 : UI : Meta Data Filter Menu
- IXC-965 : Make Cypress tests Feature Flag Aware
- IXC-971 : Balloon: Adjust Size
- IXC-977 : Settings Page and Navigation
- IXC-989 : Setting List: UI & Populate from Types & Sub-Types
- IXC-990 : Add group and tag to Cypress

### Engineering Tasks

- IXC-1010 : CI/CD Codify revamp in Terraform (from the POC)
- IXC-1019 : Subscription Info UI: Cancel
- IXC-1020 : Subscription Info UI: Reactivate
- IXC-1021 : Onboarding with Daniel
- IXC-1041 : Jordan Sign Envirotech NDA
- IXC-402 : Scale back down RDS EC2 instance types in dev (and staging?)
- IXC-633 : Update Account Backup Service to be a Container
- IXC-634 : Service to Trigger Account Backups
- IXC-639 : Research Spike - Make and Document Example Data Migrations
- IXC-812 : UX/UI Trial and Subscription: Billing Navigation and Details Page
- IXC-840 : Create a base source class and one source implementation
- IXC-842 : Create a base destination class and one destination implementation
- IXC-844 : Integrate the remaining sources and destinations
- IXC-863 : Add Heap.io IXC-Stage
- IXC-864 : Research monorepo with CICD and coordinate testing
- IXC-880 : CI/CD w/ Monorepo POC
- IXC-881 : CI/CD monorepo coordinated testing POC
- IXC-976 : Discuss Linting Rules
- IXC-980 : Invoices Table
- IXC-981 : Seats and Data Usage
- IXC-983 : Integrate SonarCloud into QueueProcessor
- IXC-984 : Add structured logging to Queue Processor
- IXC-987 : Queue Processor: Install postgres binaries during install/build

### Bugs

- IXC-1006 : [63] Memory usage maxing out when zooming in on PDF in Extraction
- IXC-1007 : [60] Item List number order getting disrupted
- IXC-1037 : [] Win 10, Chrome - Template file could not be loaded error on selecting few default report templates
- IXC-573 : [60] Nominal not updating correctly when switched from Note to Dimension
- IXC-712 : [50] Can't add Labels to Skip in Grid
- IXC-719 : [50] Backspacing when setting up grid crashes the app
- IXC-816 : [60] Incorrect tolerance & limits displayed when capturing a Radius with a MIN or MAX
- IXC-875 : [60] Dead-end Buttons
- IXC-891 : [60] Footer Row adding extra rows instead of expanding in line with total number of items
- IXC-892 : [60] Custom Templates not displaying or downloading with the GD&T Font
- IXC-968 : [60] Incorrect tolerance & limits displayed when capturing a Radius with a MIN
- IXC-974 : [60] Incorrect Note capture
- IXC-999 : [60] Deleting sub-items with a shared balloon causing an error

## 0.1.2 - 2020.08.26

- Report footer bugfix
- Thumbnail fixes
- Frontend CI pipeline fix of build-args

## 0.1.1 - 2020.08.26

Report template token drag and drop performance improvement

### Stories

- IXC-636 : Add User Invitation Flow
- IXC-826 : Update OCR so Dimensions/Notes are the correct type

### Engineering Tasks

- IXC-963 : Change landing page when clicking the Help (?) button

### Bugs

- IXC-823 : [60] OCR not registering capture when manually ballooning

## 0.1.0 - 2020.08.24

First release of InspectionXpert Cloud on the new base.

### Stories

- IXC-198 : Extract dimensions with leading decimal places (OCR Training)
- IXC-54 : Extract bilateral tolerances with a quantity in front of the nominal
- IXC-635 : Update Signup Flow for New Account
- IXC-664 : Refresh Extraction
- IXC-696 : Report Editor Form
- IXC-698 : Report Editor Actions Bar
- IXC-699 : Downloading a Report
- IXC-701 : Editable Report
- IXC-744 : Report Template Area
- IXC-745 : Template Information Panel
- IXC-746 : Template Editor Panel
- IXC-747 : Token Tree
- IXC-752 : Report Template Action Bar
- IXC-753 : Template Footer Row Interaction
- IXC-774 : Soc II Type 1 Gap Analysis
- IXC-795 : Static Token Functionality
- IXC-796 : Dynamic Token Functionality
- IXC-820 : Research Spike: Create a work plan
- IXC-853 : Vanta Policy Review and Creation
- IXC-858 : IX 3.0 METRO UX/UI Design
- IXC-859 : Settings Data Service
- IXC-860 : Automatically rotate capture rectangle to adjust for vertical dimensions
- IXC-888 : Extract Grid Coordinates so they can be Included in Reports
- IXC-907 : Functional Spike: Reduce UI lag when assigning tokens to custom template

### Engineering Tasks

- IXC-793 : Migrate or Archive all Code Repositories from CloudForge
- IXC-828 : Alert notifications for CI/CD pipelines
- IXC-829 : continue refactoring cypress/integration/app/Extraction/
- IXC-835 : Knowledge Transfer : OCR
- IXC-836 : Create SPHINX application scaffolding
- IXC-837 : Set up SPHINX HTTP request handling
- IXC-838 : Add a SPHINX health check endpoint
- IXC-839 : Define REST API
- IXC-851 : refactor cypress/integration/app/partsList
- IXC-852 : Create a Database Diagram for QX
- IXC-857 : Turn off ability to highlight searchable text
- IXC-874 : Set up application configuration
- IXC-882 : Research Spike: Determine database approach
- IXC-884 : Define database schemas

### Bugs

- IXC-302 : [] Updating the Captured Value is producing inconsistent results
- IXC-552 : [54] Handling of Rotated PDFs for Extraction
- IXC-596 : [60] Adjusting the tolerance drop-down jumps me to a future step
- IXC-612 : [60] Data in the review not updating to corresponding Item List selection
- IXC-615 : [60] Expanding Item in the Item List not functioning properly
- IXC-626 : [60] Classifications are not retaining and Balloon Styles aren't updating
- IXC-673 : [54] Screen crashes and turns white
- IXC-674 : [60] Steps in Navigation during Analysis jump back and forth
- IXC-675 : [60] Can't Expand Notes after adding a quantity greater than 1
- IXC-687 : [60] Should prevent user proceed without valid part number
- IXC-711 : [50] Users cannot change part status
- IXC-730 : [54] [Chrome] - Parts- Toggle button overlaps with text & comment textbox on Item Review panel
- IXC-734 : [70] Chrome - PDF report - Balloons are not in correct positions in the PDF report
- IXC-735 : [60] [Edge]- Item List - Item numbers are added unpredictably if click on "Expand by with Shared Balloon"
- IXC-736 : [63] [Chrome]-Parts- Clicking Customer to sort it by name leading to blank page with 404 error
- IXC-740 : [70] [Chrome] - Parts - Customer name and part number fields are cleared out automatically
- IXC-801 : [60] [Edge]- Item List - Additional quantity of items in the items list are added with the same numbers
- IXC-824 : [60] Nominal displaying strange symbol when OCR of Countersink
- IXC-867 : [60] Unable to Generate Reports with the Default AS9102B or PPAP Templates
- IXC-868 : [60] Unable to Create a New Report
- IXC-890 : [60] Inspection Method token causes Custom Reporting to crash
- IXC-893 : [60] WalkMe not working in Dev/Stage
- IXC-894 : [20] Describe the Template Page - Sheets need wrap text
- IXC-895 : [40] Edit Your Report Page - When exporting to Excel it saves as a 3 OTHER FILE
- IXC-896 : [45] Incidental populates for Classification in the report if none was selected
- IXC-897 : [54] Table is missing buffer row when editing/creating a Template
- IXC-898 : [45] Can't Concatenate Tokens and/or Text
- IXC-899 : [60] Created Template for Customer Provided doesn‚Äôt Show up in Templates List
- IXC-900 : [60] Report gets saved with the first template clicked
- IXC-901 : [24] Template cloning hits an error
- IXC-902 : [] Errors when no Customer is Set for the Part
- IXC-903 : [60] Dynamic data isn‚Äôt populating for custom templates
- IXC-904 : [] Custom Template (Strattec) showing data from 1st sheet in the 3rd sheet
- IXC-905 : [30] User isn‚Äôt guided back to the part if they edit Custom Template
- IXC-906 : [54] Cannot scroll to the bottom of a report to set the Footer Row
- IXC-918 : [] Win 10, Chrome-Item Review-For item with tolerance value, field gets overlapped in description panel
- IXC-919 : [] Windows 10, Chrome - Ballooning - Text not captured while manual ballooning
- IXC-922 : [] Windows 10, Chrome - Balloon Style - Not able to change default balloon style or add balloon style.
- IXC-925 : [] Win10, Firefox - Changing the Part status from Unverified to Ready For Review page shows Error:(
- IXC-950 : [54] Unable to Create Templates and Reports with Double-Lettered Columns
- IXC-955 : [60] Create account form returns error on save

### Stories

- IXC-198 Extract dimensions with leading decimal places (OCR Training)
- IXC-635 Update Signup Flow for New Account
- IXC-646 Trial and Subscription Management UI Mockups
- IXC-664 Refresh Extraction
- IXC-859 Settings Data Service
- IXC-860 Automatically rotate capture rectangle to adjust for vertical dimensions
- IXC-54 Extract bilateral tolerances with a quantity in front of the nominal
- IXC-647 Billing Navigation
- IXC-696 Report Editor Form
- IXC-698 Report Editor Actions Bar
- IXC-699 Downloading a Report
- IXC-701 Editable Report
- IXC-744 Report Template Area
- IXC-745 Template Information Panel
- IXC-746 Template Editor Panel
- IXC-747 Token Tree
- IXC-749 Sheet Navigation Panel
- IXC-752 Report Template Action Bar
- IXC-753 Template Footer Row Interaction
- IXC-774 Soc II Type 1 Gap Analysis
- IXC-795 Static Token Functionality
- IXC-796 Dynamic Token Functionality
- IXC-820 Research Spike: Create a work plan
- IXC-853 Vanta Policy Review and Creation
- IXC-858 IX 3.0 METRO UX/UI Design
- IXC-888 Extract Grid Coordinates so they can be Included in Reports
- IXC-907 Functional Spike: Reduce UI lag when assigning tokens to custom template

### Engineering Tasks

- IXC-793 Migrate or Archive all Code Repositories from CloudForge
- IXC-632 Update API to Connect to Correct Account Database
- IXC-638 Update S3 Uploads to Include AccountId and SiteId
- IXC-782 Data validation for saving, previewing, and in browser editing
- IXC-857 Turn off ability to highlight searchable text
- IXC-347 Continuous monitoring
- IXC-525 Integrate Rapid7 into new IXC infrastructure
- IXC-611 Research platform for performance metrics for backend
- IXC-828 Alert notifications for CI/CD pipelines
- IXC-829 continue refactoring cypress/integration/app/Extraction/
- IXC-835 Knowledge Transfer : OCR
- IXC-836 Create SPHINX application scaffolding
- IXC-837 Set up SPHINX HTTP request handling
- IXC-838 Add a SPHINX health check endpoint
- IXC-839 Define REST API
- IXC-851 refactor cypress/integration/app/partsList
- IXC-852 Create a Database Diagram for QX
- IXC-874 Set up application configuration
- IXC-882 Research Spike: Determine database approach
- IXC-884 Define database schemas

### Bugs

- IXC-734 [70] Chrome - PDF report - Balloons are not in correct positions in the PDF report
- IXC-740 [70] [Chrome] - Parts - Customer name and part number fields are cleared out automatically
- IXC-736 [63] [Chrome]-Parts- Clicking Customer to sort it by name leading to blank page with 404 error
- IXC-955 [60] Create account form returns error on save
- IXC-596 [60] Adjusting the tolerance drop-down jumps me to a future step
- IXC-612 [60] Data in the review not updating to corresponding Item List selection
- IXC-615 [60] Expanding Item in the Item List not functioning properly
- IXC-626 [60] Classifications are not retaining and Balloon Styles aren't updating
- IXC-674 [60] Steps in Navigation during Analysis jump back and forth
- IXC-675 [60] Can't Expand Notes after adding a quantity greater than 1
- IXC-687 [60] Should prevent user proceed without valid part number
- IXC-735 [60] [Edge]- Item List - Item numbers are added unpredictably if click on "Expand by with Shared Balloon"
- IXC-801 [60] [Edge]- Item List - Additional quantity of items in the items list are added with the same numbers
- IXC-824 [60] Nominal displaying strange symbol when OCR of Countersink
- IXC-867 [60] Unable to Generate Reports with the Default AS9102B or PPAP Templates
- IXC-868 [60] Unable to Create a New Report
- IXC-890 [60] Inspection Method token causes Custom Reporting to crash
- IXC-899 [60] Created Template for Customer Provided doesn‚Äôt Show up in Templates List
- IXC-900 [60] Report gets saved with the first template clicked
- IXC-903 [60] Dynamic data isn‚Äôt populating for custom templates
- IXC-893 [60] WalkMe not working in Dev/Stage
- IXC-552 [54] Handling of Rotated PDFs for Extraction
- IXC-673 [54] Screen crashes and turns white
- IXC-730 [54] [Chrome] - Parts- Toggle button overlaps with text & comment textbox on Item Review panel
- IXC-897 [54] Table is missing buffer row when editing/creating a Template
- IXC-906 [54] Cannot scroll to the bottom of a report to set the Footer Row
- IXC-950 [54] Unable to Create Templates and Reports with Double-Lettered Columns
- IXC-711 [50] Users cannot change part status
- IXC-896 [45] Incidental populates for Classification in the report if none was selected
- IXC-898 [45] Can't Concatenate Tokens and/or Text
- IXC-895 [40] Edit Your Report Page - When exporting to Excel it saves as a 3 OTHER FILE
- IXC-905 [30] User isn't guided back to the part if they edit Custom Template
- IXC-901 [24] Template cloning hits an error
- IXC-894 [20] Describe the Template Page - Sheets need wrap text
- IXC-302 [] Updating the Captured Value is producing inconsistent results
- IXC-902 [] Errors when no Customer is Set for the Part
- IXC-904 [] Custom Template (Strattec) showing data from 1st sheet in the 3rd sheet
- IXC-918 [] Win 10, Chrome-Item Review-For item with tolerance value, field gets overlapped in description panel
- IXC-919 [] Windows 10, Chrome - Ballooning - Text not captured while manual ballooning
- IXC-922 [] Windows 10, Chrome - Balloon Style - Not able to change default balloon style or add balloon style.
- IXC-925 [] Win10, Firefox - Changing the Part status from Unverified to Ready For Review page shows Error:(

## 0.0.6 - 2020.07.01

Ensure the application can securely separate and back up accounts/sites/users

### Stories

- IXC-430 : Extraction Instructions
- IXC-471 : User Roles
- IXC-531 : Part Status Options (Part Creation)
- IXC-642 : New Report Template Page
- IXC-647 : Billing Navigation
- IXC-690 : Report Sheet Navigation
- IXC-691 : Reports List in the Part Details Drawer
- IXC-692 : Templates Lists for Choosing a Report Template
- IXC-693 : Previewing Templates with Real Data
- IXC-695 : Documents List in the Part Details Drawer
- IXC-717 : Multiple account support
- IXC-749 : Sheet Navigation Panel
- IXC-751 : Selected Cell Container
- IXC-783 : Migrate OCR calls to OCRv2 (eda)
- IXC-787 : Integrate Cypress Tests with Cypress Dashboard
- IXC-794 : Token Appearance
- IXC-817 : Migrate Autoballoon calls to Autoballoon v2 (eda)

### Engineering Tasks

- IXC-406 : Deploy SonarQube
- IXC-494 : One API Gateway for Region Detection Services
- IXC-616 : Create repo for CI Scripts
- IXC-628 : Template Creation
- IXC-632 : Update API to Connect to Correct Account Database
- IXC-638 : Update S3 Uploads to Include AccountId and SiteId
- IXC-669 : Uniform style and linting forms to be discussed
- IXC-671 : Functional Spike : Setup APM
- IXC-714 : Deploy OCR V2 via CI to dev and staging envs
- IXC-725 : Cypress Tests: Refactoring Locators
- IXC-760 : Jordan: Read this ticket before closing the Sprint
- IXC-761 : Backup or Migrate Legacy Git Repositories by Oct 1, 2020
- IXC-782 : Data validation for saving, previewing, and in browser editing
- IXC-788 : Look into flaky tests
- IXC-791 : app/analysis/PartInformation.spec.js, app/analysis/Wizard.spec.js
- IXC-809 : cypress/integration/app/Extraction/

### Bugs

- IXC-683 : [0] Notation Parser Error Handling Test Failing on Local Test Run
- IXC-727 : [60] npm run storybook:build failed when running in frontend folder
- IXC-781 : [] should show 5 rows but was actually 4 in Linear Section in default tolerances
- IXC-786 : [] Cannot delete report in Parts Drawer view
- IXC-825 : [] Error in /account-member edit flow

## 0.0.5 - 2020.06.01

Internal release of new codebase's foundation. Capabilities will include AWS Deployment Path, AWS IAM accounts for all developers, CI/CD deployment of application scaffolding with automated linting, test execution, code coverage reports, and security audits, User creation (simple object vs full implementation

### Stories

- IXC-336 : Start a new trial
- IXC-339 : Invite team members
- IXC-340 : Trial expiration reminder
- IXC-341 : Extend a trial
- IXC-343 : Provide payment information
- IXC-344 : Archive / delete accounts
- IXC-345 : Cancel plan
- IXC-346 : Change subscription plan
- IXC-358 : Renewal reminders
- IXC-404 : UX/UI Design for Creating a First Article Inspection Report
- IXC-414 : UX/UI Global Settings
- IXC-468 : UX/UI Design for Creating Custom Report Templates
- IXC-511 : UI and UX for changing tolerance type
- IXC-516 : Make Invoices Available in App
- IXC-527 : Open WalkMe Player when clicking IXC Help Button
- IXC-641 : Research Spike: Excel Preview/Editing
- IXC-644 : Create Report Page
- IXC-645 : Trial and Subscription Management User Flows
- IXC-656 : Account Management for Sales and Marketing
- IXC-659 : In-App Alerts
- IXC-661 : Accounting Role
- IXC-662 : Paid and Unpaid Seats
- IXC-694 : Creating a New Report Progression
- IXC-700 : Report Flow Drawers

### Engineering Tasks

- IXC-354 : Setup Applause test-cycles
- IXC-428 : Codify Security Acct: AWS IAM users / groups into Terraform
- IXC-522 : Functional Spike: Connecting New OCR Service
- IXC-529 : Allow for ECS container definitions to be loaded as part of application build/deploy
- IXC-530 : Support multiple subscriptions per account
- IXC-535 : Research: Flask app containerized
- IXC-539 : Deliver UMC QX Data
- IXC-571 : CodePipelines in dev should have a corresponding pipeline to handle stage/prod
- IXC-627 : Teardown Jenkins infra and servers
- IXC-629 : Mock/Wrap AWS
- IXC-630 : Update Data Models for Sites and User Profiles
- IXC-631 : Split Databases by Account
- IXC-670 : Functional Spike: In App Token Assignment
- IXC-678 : Fix Cypress E2E tests
- IXC-708 : Create health status check for engineering and ocr parser
- IXC-710 : Fix remaining Cypress E2E tests
- IXC-716 : Uniform style and linting forms - PR Templates & Git commit messages
- IXC-723 : Temp. teardown resources in prod to save on costs for now

### Bugs

- IXC-301 : [] Only numbers should appear in tolerance box
- IXC-576 : [40] Customer name showing up as UUID
- IXC-577 : [60] New Parts not saving into Parts List
- IXC-588 : [40] Weird menu when adjusting capture box size
- IXC-589 : [60] Difficulty fixing GDT caught as a note
- IXC-590 : [60] Can't delete part or edit from parts list page
- IXC-594 : [] Part Info not persisting when leaving the Flow
- IXC-617 : [60] Degrees missing from default Metric Angle Units
- IXC-618 : [60] Balloon should be created even if text not detected
- IXC-643 : [20] Grid not spacing evenly when generated
- IXC-679 : [30] Grid rotation broken
- IXC-689 : [70] Items disappeared after reordering item to the last one
- IXC-726 : [] [Test bug from Applause testing services]

## 0.0.4 - 2021.05.01

Operations necessary for a product that can launch and scale

### Stories

- IXC-386 : Part Details Action Buttons
- IXC-396 : Research Spike: Trial and Subscription
- IXC-415 : Part Analysis Step 4/4 - Grid Settings Step
- IXC-433 : Manual Ballooning Mode
- IXC-436 : Extraction Toolbar - Expanding
- IXC-437 : Extraction Toolbar - Group/Ungroup
- IXC-441 : Review Drawer - Data Form
- IXC-444 : Insert Special Characters
- IXC-456 : Review Step
- IXC-476 : Autoballooning - Selecting a Region
- IXC-498 : User Role: Manager
- IXC-515 : Multi-Select Items
- IXC-541 : Create a Ballooned PDF
- IXC-546 : Generate Demo Report
- IXC-566 : Update Public Login Image
- IXC-609 : Configure Staging Env
- IXC-619 : Research Spike: Engineering Plan for Multi-Tenancy
- IXC-620 : Research Spike: Engineering Plan for Custom Templates
- IXC-621 : Research Spike: Creating a FAI Report (may want to be merged with Custom Reporting)
- IXC-665 : Add Image Capture to the Add Symbol Modal

### Engineering Tasks

- IXC-321 : Configure CI/CD for new codebase
- IXC-347 : Continuous monitoring
- IXC-525 : Integrate Rapid7 into new IXC infrastructure
- IXC-538 : Extraction page should show auto ballooned characterstics when they are found
- IXC-548 : Rework frontend DNS and backend to be behind CloudFront
- IXC-549 : Uploaded Part Drawings Should Display on Extraction Page
- IXC-550 : Consolidate Part Editor components
- IXC-555 : Setup Lambda for the Engineering-Notation-Parser
- IXC-611 : Research platform for performance metrics for backend

### Bugs

- IXC-295 : [] Post Processing Errors
- IXC-563 : [] PDFTron Thumbnail Rendering
- IXC-568 : [] Grid doesn't render correctly without refreshing
- IXC-569 : [60] Cannot adjust the Capture Value in Item List
- IXC-572 : [] Grid shouldn't be case sensitive
- IXC-574 : [50] Quantity isn't being picked up
- IXC-575 : [] App crashes when deleting an Item
- IXC-578 : [40] Manual Extraction requires pdftron refresh to activate (zoom or page change)
- IXC-579 : [] Insert Character modal doesn't update characteristic
- IXC-582 : [60] Report button on Review step does not generate report
- IXC-583 : [60] Updating anything in a previous workflow step jumps the user to the max visited workflow step
- IXC-584 : [] New balloons ignoring default balloon styles
- IXC-585 : [60] Can't Switch Measurement Types
- IXC-586 : [] Can only create single new balloon style
- IXC-587 : [] Review step should not be available when there are no characteristics
- IXC-592 : [] Page crashes when setting the Document Control field
- IXC-593 : [] TypeError crash due to Classification Select List
- IXC-597 : [] Unit of Measure doesn't stay changed when switching it to Metric (Duplicate IXC-585)
- IXC-602 : [] Wizard navigation buttons at bottom of grid stage touch
- IXC-603 : [] File extension shouldn't be present in Part Name
- IXC-604 : [] Balloon Styles: Hovering over the "Add" button causes it to scale up.
- IXC-607 : [54] User self-registration fails
- IXC-608 : [] test Defect
- IXC-624 : [60] Manual Ballooning not working and producing "Item Assignment Failed" alert

## 0.0.3 - 2020.04.01

Team creation and management. As a Product and Engineering Team, we need to have a process for a prospect to start a trial and eventually convert to a customer without requiring intervention from IX staff.

### Stories

- IXC-10 : Improve image capture quality for standard ANSI drawing sizes (Spike)
- IXC-106 : Ability to move balloon & capture rectangle without changing to 'Select' mode
- IXC-11 : Recognize individual elements in each dimension capture (spike)
- IXC-12 : Fix WalkMe Help Menu
- IXC-13 : Post-extract review design (spike)
- IXC-14 : Auto-Capture ISO drawing in production environment
- IXC-15 : Organized test-part library and test results
- IXC-17 : Adjustable capture rectangles
- IXC-18 : Consistently extract characteristics with bilateral tolerances
- IXC-19 : Post-Processing Accuracy Testing
- IXC-191 : Capture each line of a characteristic group individually
- IXC-192 : Determine cause preventing drawing 333-3646(03)\_BP from being auto-ballooned
- IXC-20 : Run Manual OCR Accuracy Test (including multiple pages)
- IXC-212 : Spike: Can We configure OCR to Recognize '0' Tolerances for Bilateral Dimensions
- IXC-22 : Percent of Autoballoon Accuracy
- IXC-223 : Bootstrap email service for renewal reminders
- IXC-224 : Split OCR Test Data from the Main Project
- IXC-23 : Percent of characteristics autocaptured
- IXC-234 : Ability to drill down to the characteristic sub-types that are failing in sample sets
- IXC-235 : Create a visual of the capture rectangles when testing auto-ballooning
- IXC-24 : Curate sample data set (prepped for testing) - 5 Parts
- IXC-241 : Create a Mechanical Turk Qualification Test for the Customer Captures
- IXC-250 : Run one page ANSI Sample through testing
- IXC-256 : Region Detection Implementation
- IXC-26 : Automatic alert to development when the OCR is down
- IXC-265 : Detect/extract counterbore diameter and depth separately
- IXC-266 : Detect/extract countersink diameter and angle separately
- IXC-268 : Detect/extract chamfer length and angle (or second length) separately
- IXC-27 : Create Mechanical Turk Test of OCR Accuracy on Customer Captures
- IXC-277 : Spike : Prototype parser / lexer solution for chars
- IXC-306 : Improve image capture quality - Gray Tech Drawing
- IXC-309 : Clickable Mock-up: Parts List --> Part Extraction Review
- IXC-31 : Recognize and capture Thread callouts
- IXC-310 : Integrate lexer/parser
- IXC-324 : Register new user
- IXC-328 : Main Navigation bar
- IXC-329 : Password management
- IXC-33 : Update GD&T to account for ISO 1101 symbols and standards
- IXC-35 : Research adding test cases & test plans to Jira
- IXC-364 : Swap Aha Ideas Portal with Productboard's
- IXC-37 : Review and migrate applicable bugs from TP to Jira
- IXC-381 : Parts List Data Table
- IXC-382 : Part Details Drawer
- IXC-383 : New Part Button
- IXC-384 : Drag and Drop Part Drawings on the Parts List page
- IXC-407 : Analysis Wizard
- IXC-409 : PDF Viewing Canvas - Functional Spike
- IXC-410 : Part Analysis Step 1/4 - Part Information
- IXC-411 : Part Analysis Step 2/4 - Default Tolerances
- IXC-412 : Part Analysis Step 3/4 - Balloon Styles
- IXC-42 : Run, analyze, and identify failures from baseline test on cohort 1 of test drawings
- IXC-429 : Show Detected Regions
- IXC-43 : Add OCR reset button & function to Image Capture box
- IXC-431 : Extraction Toolbar - Extract Mode
- IXC-440 : Extraction Toolbar - Delete
- IXC-442 : Review Drawer - Verification Controls
- IXC-443 : Review Drawer - Progression Buttons
- IXC-445 : Data Review - GDT Form
- IXC-45 : Automate auto-balloon / OCR test results straight from Jenkins to Scorecard
- IXC-454 : Functional Spike: Feature Flags for access restriction
- IXC-46 : Pull automated accuracy tests into their own job on Jenkins
- IXC-473 : Items List - Cards Display
- IXC-475 : Autoballooning - Single Extract
- IXC-477 : Items List - Edit and Delete
- IXC-479 : FUNctional Spike: Create Demo Report
- IXC-48 : Add planning poker into Slack
- IXC-483 : Special Character Modal
- IXC-484 : Data Review - Dimensions Form
- IXC-485 : Data Review - Notes Form
- IXC-486 : Data Review - Tolerances Recalculate after Changing Data
- IXC-493 : FUNctional Spike:Develop OCR Service Facade
- IXC-517 : Implement OCR Service Facade
- IXC-523 : Functional Spike: Create a Ballooned PDF
- IXC-533 : Add Font to Capture Symbols & GD&T
- IXC-540 : Setup Jira Resolution workflow
- IXC-57 : Tool Discussion w/ John
- IXC-58 : Customer Workflow Discussion w/ Mike, Rolin
- IXC-59 : Create User Task Flows
- IXC-60 : Page archetypes wireframes
- IXC-61 : Upload an Document Userflow
- IXC-62 : Balloon a drawing userflow
- IXC-646 : Trial and Subscription Management UI Mockups
- IXC-7 : Create Auto-Ballooning Prototype (Spike)
- IXC-73 : Make gdt characteristics viewable in notes section
- IXC-74 : Show list of reports with a table similar to the Parts Page
- IXC-78 : Bar needs a size too to test summation
- IXC-79 : Bar Test
- IXC-9 : Unable to type into GD&T field immediately after inserting character with GD&T builder
- IXC-90 : Improve image capture quality for standard ANSI drawing sizes

### Engineering Tasks

- IXC-102 : Enable AWS Multi-Account Structure
- IXC-104 : Setup clean repo and Atlassian toolchain
- IXC-105 : Create developer reference for SDLC
- IXC-193 : Lunch & Learn: Frontend Framework Conventions
- IXC-194 : Lunch & Learn: Rest vs GraphQL
- IXC-197 : Design: Sketch Session Review
- IXC-204 : Automated OCR testing producing target output datafile
- IXC-205 : Knowledge transfer Amanda > IX Dev Team (OCR Training)
- IXC-206 : Run IXC Time Test: Blaise
- IXC-208 : Run IXC Time Test: Devin
- IXC-214 : Knowledge Transfer: Adjusting the capture rectangle
- IXC-215 : Create Ideal Characteristic Sample Set for GD&T
- IXC-216 : Add OWASP and Eng Accounting fields to Jira
- IXC-218 : Update Second Cohort of Sample Drawings
- IXC-219 : Select 5 drawings for second ANSI sample set
- IXC-220 : Lunch and Learn: Git Bootcamp Part 1
- IXC-221 : Spike: Region detection and categorization
- IXC-229 : Create AWS Accounts for Reference Architecture
- IXC-230 : Reserve Placeholder Domains
- IXC-231 : Setup git repos for reference architecture
- IXC-232 : Create CI Service account
- IXC-236 : Spike: Identify engineering path to OCR library maintenance
- IXC-237 : Select 5 drawings for a third ANSI sample set
- IXC-238 : Create a third ANSI drawing sample set
- IXC-239 : How to break out components - a nominal with bilateral tolerances
- IXC-240 : Investigate and improve the OCR in some meaningful way
- IXC-242 : Improve post processing test to check GDT tolerances and datums
- IXC-243 : Select 2 drawings for a first ISO Sample Set
- IXC-244 : Create an ISO Drawing Sample Set (2 Drawings)
- IXC-249 : Create an ANSI Sample with Tags (<50 Chars.)
- IXC-251 : Test & identify if Mechanical Turk and large sample set are worth pursuing
- IXC-257 : Fix Data Studio Connection to CSV
- IXC-274 : Run IXC Time Test: Mark
- IXC-294 : Audit EC2 instances running in legacy AWS account
- IXC-299 : Fix PDFTron test for accurate reporting
- IXC-305 : Add IXOD vs IXC time savings to OCR Performance Dashboard
- IXC-318 : Deploy Multi-Account AWS
- IXC-319 : Provision IAM User Accounts
- IXC-320 : Authentication toolchain
- IXC-322 : Containerization of API endpoints
- IXC-323 : Centralized logging
- IXC-326 : Per-client data isolation
- IXC-327 : Per-client backup & recovery
- IXC-335 : New-client provisioning
- IXC-365 : Run IXC Time Test: Ryan
- IXC-367 : Correct spacing of depth symbol in IXGDT_STP Font
- IXC-368 : Arch Spike : Backend Service Identification
- IXC-369 : Research Improved OCR to detect leading decimals
- IXC-371 : Arch Spike : Front End Architecture
- IXC-373 : Catalog issues with IX GD&T Font
- IXC-374 : Prototype Front End Testing Lifecycle
- IXC-375 : Front-end architecture mining
- IXC-377 : Investigate costs from Gruntwork reference architecture
- IXC-378 : Create documentation to on-board devs to AWS and EC2
- IXC-379 : Build and deploy ixc-mono app (soup to nuts)
- IXC-387 : Remove OnPremise from IXOD Download Portal
- IXC-39 : UMC Raven DB upgrade to professional license
- IXC-390 : Research Spike: Apollo Front End to Replace Redux
- IXC-392 : Disable DNS Forwarding through ENI's in AWS
- IXC-393 : Use latest generation of AWS instance types
- IXC-394 : Deconstruct Part Analysis
- IXC-395 : Deconstruct Part Extraction
- IXC-397 : Connect Front End Spike to the IXC-Mono repo
- IXC-398 : Remove WalkMe Snippets from MVF
- IXC-399 : Implement product instrumentation
- IXC-400 : Remove Amplitude Snippets from MVF
- IXC-405 : Work through Infrastructure Access Documentation
- IXC-408 : Implement WalkMe
- IXC-420 : Upgrade Elasticsearch from v5.5 to v7.x
- IXC-421 : Functional Spike - Fabric Js or other to handle movable canvas objects
- IXC-422 : Codify VPN to RDS in TF Security Groups
- IXC-423 : Convert from ECS EC2 to ECS Fargate
- IXC-424 : Move US-East-1 QualityXpert instances to Gov-Cloud
- IXC-426 : Create data models for Part
- IXC-427 : Implement application monitoring
- IXC-448 : Shutdown and Backup QualityXpert Accounts
- IXC-451 : Investigate ability to replace storybook unit testing with cypress
- IXC-455 : Part Extraction Ticket Review
- IXC-457 : Proxy analytics events from frontend through backend
- IXC-458 : SSL Certificate on help.inspectionxpert.com expired
- IXC-461 : Update stock image for log-in pages
- IXC-49 : Create Jira flow for tickets (Spike)
- IXC-492 : Add logger to frontend and backend
- IXC-497 : Review CMM Import app with Transformation
- IXC-524 : Pass in environment variables to the running container as well as secrets
- IXC-537 : Part documents should be uploaded and referenced from S3
- IXC-542 : Update xctentitlements.com SSL certificate
- IXC-55 : Setup Mark and Sathish to create Json files
- IXC-56 : Setup individual user accounts on the build server
- IXC-72 : Test IXOD 2.0 SP 10 (prefer on Monday)
- IXC-75 : Create issue type Engineering Task in Jira & set workflow
- IXC-88 : Visual representation of OCR accuracy data
- IXC-89 : Bring data into (Scorecard OCR) Accuracy automatically
- IXC-96 : Gather thread & other dimension data from customer drawings. Update post processing pdf

### Bugs

- IXC-173 : [] Clicking into Note Field causes Balloons to Disappear and Disables Extraction
- IXC-177 : [] Pressing delete key (delete not backspace) deletes the character before insertion point instead of after
- IXC-188 : [] Incorrect parsing when tolerances dimensions contain degree symbol
- IXC-199 : [] Balloons should be automatically placed to the direct left of capture rectangle
- IXC-25 : [] Diameter symbol different in ocr capture vs symbol model
- IXC-254 : [] Changing the Type from Dimension to Note or GD&T doesn't work properly
- IXC-258 : [] Rounding occurs when calculating USL & LSL
- IXC-259 : [] Image capture doesn't update immediately to match what is in the capture value (full spec) box
- IXC-260 : [] Ballooned PDF's Balloons are in different locations than shown while ballooning
- IXC-261 : [] Balloon not retaining location after being repositioned
- IXC-276 : [] Orient captured image for vertical dimension in ANSI drawings
- IXC-284 : [] Identify the cause of drawing 973 test failures
- IXC-300 : [] Expanding multiple quantity characteristics incorrectly placing item in the table
- IXC-303 : [] Incorrectly rotating a characteristic in the image capture
- IXC-38 : [] Fix controlled property warning on Part Settings
- IXC-526 : [14] Validation error text has errant text
- IXC-97 : [] Auto-Extract is not placing capture rectangles properly

## 0.0.2 - 2020.03.01

Part Database

## 0.0.1 - 2021.02.01

Single User Account Management
