# webhookListener

An AWS Lambda function that receives webhook callbacks from other systems, verifies their request
signatures, and when valid converts their bodies to JSON and pushes into an SQS queue for downstream processing.

## Supported Listeners

- Hubspot
- Chargify
- Sendgrid

## Setup & Testing Locally

1 - Add a `.env` file to root folder. Populate based on the following:

| Variable                    | Required? | Example Value                                                             | Notes                                                     |
| --------------------------- | :-------: | ------------------------------------------------------------------------- | --------------------------------------------------------- |
| `NODE_ENV`                  |     Y     | "development"                                                             | Sets the environment we are in. Default value: production |
| `LOG_LEVEL`                 |     N     | "debug"                                                                   | dev=debug, test=error, prod=info                          |
| `IXC_NEW_RELIC_LICENSE_KEY` |     Y     | 40 character alpha numeric uuid                                           | License key for New Relic.                                |
| `IXC_AWS_REGION`            |     Y     | "us-east-1"                                                               | Default region                                            |
| `IXC_SQS_URL`               |     Y     | "https://sqs.us-east-1.amazonaws.com/541855766228/incoming-webhooks.fifo" | Url of SQS queue to write to                              |
| `HUBSPOT_WEBHOOK_SECRET`    |     Y     | 36 character alpha numeric uuid                                           | Chargify Webhook Secret                                   |
| `CHARGIFY_WEBHOOK_SECRET`   |     Y     | 42 character alpha numeric uuid                                           | Hubspot Webhook Secret                                    |
| `SENDGRID_VERIFICATION_KEY` |     Y     | 124 character alpha numeric uuid                                          | Sendgrid Webhook Verification Key                         |
| `SONAR_USER_TOKEN`          |     N     | 40 character alpha numeric uuid                                           | Used to run analyses on code or to invoke web services    |

2 - Run the following commands:

```
cd services/webhookListener
npm install
npm test
```

### Manual Verification of Hubspot

Trigger hubspot webhook by submitting [this test form|https://share.hsforms.com/1w_mgSneDT7G1ecB_TIPhiQ510rp] (using incognito browser)

Inspect the [hubspot workflow event history|https://app.hubspot.com/workflows/8445733/platform/flow/42695059/history/log-events/event/73705546757/1605501275184/details] to see the response returned to hubspot.

- Find the Action called Trigger webhook
- Hover over the Contact name and click Event Details
- Look at the value under “Server Response”. It should be Success (200) with some info about the request.

Look for errors in the [log stream for webhook-listener|https://console.aws.amazon.com/cloudwatch/home?region=us-east-1#logsV2:log-groups/log-group/$252Faws$252Flambda$252Fwebhook-listener].

### Manual Verification of Chargify

Go to [Chargify Test Site > Settings > Webhooks|https://app.chargify.com/login.html#webhooks]

There is 1 webhook setup in the table. Click Actions Button to the right of it and select "Test". Wait for the blue notice bar at the top to flip green.

Go to the [Chargify Test Site > Tools > Webhooks Panel|https://inspectionxpert-test.chargify.com/webhooks] and verify the call response (should be the first row).

Look for errors in the [log stream for webhook-listener|https://console.aws.amazon.com/cloudwatch/home?region=us-east-1#logsV2:log-groups/log-group/$252Faws$252Flambda$252Fwebhook-listener].

### Verify Events in SQS

Go to [webhooks SQS queue|https://console.aws.amazon.com/sqs/v2/home?region=us-east-1#/queues/https%3A%2F%2Fsqs.us-east-1.amazonaws.com%2F541855766228%2Fincoming-webhooks.fifo/send-receive]

- Click "Send and receive messages"
- Scroll to bottom "Poll for messages"
- Verify that events from both Hubspot and Chargify are listed and that their bodies were passed through as JSON

## Manual Deploy

### Windows Users

Installed Gnu on Windows https://github.com/bmatzelle/gow/wiki or the zip commands in the npm scripts will fail.

### All Users

1. Verify that AWS lambda function and SQS queus have been setup in infrastructure.

   - Lambda runtime: Node.js 18.x
   - SQS Queue type: FIFO
   - Verify ENV variables are setup according to this README in lambda (or populated in another way)
   - Ensure your AWS config and credentials allow you to auth to the ix-dev environment

2. Build and zip the function ready to deploy to AWS.
   npm run bundle:all

3. Log in to the AWS Web UI for Lambda, select the appropriate Lambda and upload the newly created function.zip file.

## Future Enhancements

### Structured Logging

Currently the lambda is logging to console. Support for LOG_LEVEL and JSON structured logs should be added before deployment to production.

See: https://theburningmonk.com/2017/08/centralised-logging-for-aws-lambda/

### Lambda Destinations

We may be able to use Lambda Destinations instead of manually posting to SQS. The purpose of the listener lambda would be reduced to verifying the webhook request signature.
https://aws.amazon.com/blogs/compute/introducing-aws-lambda-destinations/

### Lambda Layers

Best practice is to lock the aws-sdk version using lambda layers:

```
# Setup dependencies that will go into the layer
mkdir lambda-layer
cd lambda-layer/
npm install aws-sdk

# Create a zip file of just the dependencies
mkdir nodejs
mv node_modules/ nodejs/
zip -r aws-sdk.zip nodejs/

# Upload the layer
aws lambda publish-layer-version --layer-name aws-sdk --zip-file fileb://aws-sdk.zip --compatible-runtimes nodejs12.x --description "My AWS SDK"
```

Note: This Lambda layer's size should be about 6.1 megabytes vs the 40MB when you include aws-sdk in the production dependencies of a project
