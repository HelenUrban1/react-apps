const qs = require('qs');

const { makeHubspotWebhookValidator } = require('./hubspotWebhookValidator');
const { makeChargifyWebhookValidator } = require('./chargifyWebhookValidator');
const { makeSendGridWebhookValidator } = require('./sendgridWebhookValidator');

/**
 * WebhookListener factory method
 */
function makeWebhookListener(params) {
  // Unpack the config and init the default logger
  const { config, makeLogger } = params;
  let logger = makeLogger({ config, label: 'makeFunction' });
  logger.debug('makeWebhookListener.config', config);

  // Initialize the required aws libs
  const { sqs } = params;

  // Assign or create the request validators
  const validateHubspotWebhookRequest = params.validateHubspotWebhookRequest || makeHubspotWebhookValidator({ logger, config });
  const validateChargifyWebhookRequest = params.validateChargifyWebhookRequest || makeChargifyWebhookValidator({ logger, config });
  const validateSendGridWebhookRequest = params.validateSendGridWebhookRequest || makeSendGridWebhookValidator({ logger, config });

  /**
   * Return a function that can be used to validate Hubspot webhook requests
   *
   * @param  {Object} event - Event object from Lambda call
   * @param  {Object} context - Context object from Lambda call
   * @returns  {Object} Http response object containing statusCode and body
   */
  return async function webhookListener(event, context) {
    try {
      // To simplify log tracing create a logger with the messageId as the label
      logger = makeLogger({ config, label: context.awsRequestId });
      logger.info(`Start processing context.awsRequestId : ${context.awsRequestId}`);

      // TODO: eventName
      let webhookSource;
      let result;

      // Decode body if necessary
      if (event.isBase64Encoded) {
        // eslint-disable-next-line
        event.decodedBody = Buffer.from(event.body, 'base64').toString('ascii');
      }

      // Determine which service is calling then validate its request signature
      logger.debug('event.headers', event.headers);
      if (event.headers['user-agent'] && event.headers['user-agent'].includes('hubspot')) {
        webhookSource = 'hubspot';
        result = validateHubspotWebhookRequest(event);
      } else if (event.headers['x-chargify-webhook-id']) {
        webhookSource = 'chargify';
        result = validateChargifyWebhookRequest(event);
      } else if (event.headers['x-twilio-email-event-webhook-signature']) {
        webhookSource = 'sendgrid';
        result = validateSendGridWebhookRequest(event);
      } else {
        // The request could not associated with a validator, so return 403 Forbidden
        logger.error('The request could not be matched with a handler', { context, event });
        return {
          statusCode: 501, // Not Implemented
          body: 'The request could not be matched with a handler',
        };
      }

      // If the source was identified, but the request signature couldn't be verified, return the error
      if (result.statusCode !== 200) {
        logger.debug(`Webhook call from ${webhookSource} IS NOT VALID`);
        logger.error(result.body);
        return { ...result, headers: { 'Content-Type': 'text/plain' } };
      }

      logger.debug(`Webhook call from ${webhookSource} IS VALID`);
      const now = Date.now();
      const ts = new Date().toISOString(); // 2020-11-03T14:51:06.157Z

      // Standardize event payload to a JS object
      let bodyAsObject;
      logger.debug('event.body content-type', event.headers['content-type']);
      if (event.body !== null && event.body !== undefined) {
        if (event.headers['content-type'].includes('application/json')) {
          // TODO: We don't need to parse the body, because it needs to be passed to SQS as a string
          bodyAsObject = JSON.parse(event.body);
        } else if (event.headers['content-type'] === 'application/x-www-form-urlencoded') {
          bodyAsObject = qs.parse(event.decodedBody);
        } else {
          logger.warn('No parser defined for event.body content-type', event.headers['content-type']);
        }
      } else {
        logger.warn('event.body was null or undefined', event.body);
      }
      logger.debug('event.bodyAsObject', bodyAsObject);

      const resBodyJson = {
        version: now,
        event: { ...event, bodyAsObject },
        context,
        config: {
          env: config.env,
          baseUrl: config.baseUrl,
          region: config.awsRegion,
          s3Bucket: config.webhookS3Bucket,
          sqsUrl: config.webhookSqsUrl,
          hubspot_token_is_set: config.hubspotWebhookSecret.length > 0,
          chargify_token_is_set: config.chargifyWebhookSecret.length > 0,
          sendgrid_key_is_set: config.sendgridVerificationKey.length > 0,
        },
      };
      const resBodyStr = JSON.stringify(resBodyJson, null, 2);
      logger.debug(resBodyStr);

      /*
      // SQS Messages are limited to 256KB, but can be up to 2GB with the extended library
      // https://aws.amazon.com/about-aws/whats-new/2015/10/now-send-payloads-up-to-2gb-with-amazon-sqs/#:~:text=Amazon%20Simple%20Queue%20Service%20(SQS,payloads%20were%20limited%20to%20256KB.
      
      // Upload event body to S3 bucket as JSON file
      const fileName = `${now}-${webhookSource}.json`;
      logger.debug(`Preparing S3 upload: ${fileName}`);

      result = await s3
        .putObject({
          Bucket: config.webhookS3Bucket, // arn:aws:s3:::inspection-xpert-dev-webhook-listener
          Key: fileName,
          Tagging: `webhook_source=${webhookSource}&timestamp=${ts}`,
          Body: event.body, // JSON.stringify(event.body),
          ContentType: 'application/json; charset=utf-8',
        })
        .promise();
      logger.debug('S3 upload result: ', result);

      // s3Result = {
      //   ETag: '"8f1ea7dc3e72a84bedf9ef6e3113c156"',
      //   ServerSideEncryption: 'aws:kms',
      //   VersionId: '4Bg8XCL4kM0OMbLruYY2Q7nPuyTHUwG2',
      //   SSEKMSKeyId: 'arn:aws:kms:us-east-1:541855766228:key/f50398fd-dbf1-4b7c-962c-2af77c61971a'
      // }
      */

      // Push message to SNS or SQS
      logger.debug(`Preparing SQS sendMessage`);
      // result = await sqs.sendMessage(config.sqsUrl, message.body, message.attributes);
      result = await sqs
        .sendMessage({
          QueueUrl: config.webhookSqsUrl, // required STRING_VALUE
          MessageDeduplicationId: `${now}`, // Required for FIFO queues
          MessageGroupId: `${now}`, // Required for FIFO queues
          // DelaySeconds: 'NUMBER_VALUE',
          MessageBody: JSON.stringify(bodyAsObject), // required STRING_VALUE
          MessageAttributes: {
            // s3Bucket: {
            //   DataType: 'String',
            //   StringValue: config.webhookS3Bucket,
            // },
            // s3Key: {
            //   DataType: 'String',
            //   StringValue: fileName,
            // },
            awsRequestId: {
              DataType: 'String',
              StringValue: context.awsRequestId,
            },
            webhookSource: {
              DataType: 'String',
              StringValue: webhookSource,
            },
            linuxTimestamp: {
              DataType: 'Number',
              StringValue: `${now}`,
            },
            isoTimestamp: {
              DataType: 'String',
              StringValue: ts,
            },
          },
          // MessageSystemAttributes: {},
        })
        .promise();
      logger.debug('SQS sendMessage result: ', result);
      /*
      {
        ResponseMetadata: { RequestId: 'a47b1ea3-1cc2-5901-a2ff-468e5151af18' },
        MD5OfMessageBody: 'd7ac12b7a8fb9ce750e2f13bfc11c8f2',
        MD5OfMessageAttributes: '95f2cc52b7e34daabb1ac07dcd02e46c',
        MessageId: 'ba5bc73f-895d-49fc-b995-ade1c97bd856',
        SequenceNumber: '18857774025124848384'
      }
      */

      // Return the success result
      return {
        statusCode: 200,
        headers: { 'Content-Type': 'text/plain' },
        body: resBodyStr,
      };
    } catch (error) {
      logger.error(error);
      return {
        statusCode: 500,
        headers: { 'Content-Type': 'text/plain' },
        body: `An error occured while processing the webhook request: ${error.name} : ${error.message}`,
      };
    }
  };
}

module.exports.makeWebhookListener = makeWebhookListener;
