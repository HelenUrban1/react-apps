// require('dotenv').config();

let logLevel = process.env.LOG_LEVEL || 'debug';
let newRelicConfig = 'Dev';
if (process.env.NODE_ENV === 'production') {
  logLevel = 'debug';
  newRelicConfig = 'Prod';
} else if (process.env.NODE_ENV === 'test') {
  logLevel = 'error';
  newRelicConfig = 'Test';
}

module.exports = {
  env: process.env.NODE_ENV,
  new_relic: {
    license_key: process.env.IXC_NEW_RELIC_LICENSE_KEY,
    labels: `Config:${newRelicConfig},Tier:Ixc_Webhook_Listner`,
  },
  logLevel,
  awsRegion: process.env.IXC_AWS_REGION || 'us-east-1',
  // baseUrl: process.env.SERVER_BASE_URL || 'https://alb.ixc-dev.com',
  webhookSqsUrl: process.env.IXC_SQS_URL || 'https://sqs.us-east-1.amazonaws.com/541855766228/incoming-webhooks.fifo',
  hubspotWebhookSecret: process.env.HUBSPOT_WEBHOOK_SECRET,
  chargifyWebhookSecret: process.env.CHARGIFY_WEBHOOK_SECRET,
  sendgridVerificationKey: process.env.SENDGRID_VERIFICATION_KEY,
};
