const { EventWebhook } = require('@sendgrid/eventwebhook');
// Sendgrid Request validation - https://docs.sendgrid.com/for-developers/tracking-events/getting-started-event-webhook

// Test form
// https://app.sendgrid.com/settings/mail_settings
// https://webhook.site/#!/48f092a0-6b19-4fa9-8b6b-be6f6661f04e/5ec11c2d-05fc-4f2e-81c1-560a49784888/1

/**
 * SendGrid webhook handler
 *
 * @see {@link https://docs.sendgrid.com/for-developers/tracking-events/getting-started-event-webhook-security-features#the-signed-event-webhook}
 */
function makeSendGridWebhookValidator(params) {
  const logger = params.logger || console;
  const config = params.config || {};
  const sendgridWebhookAgent = new EventWebhook();

  /**
   * Return a function that can be used to validate SendGrid webhook requests
   *
   * @see {@link https://legacydocs.SendGrid.com/docs/faq/validating-requests-from-SendGrid}
   *
   * @param  {Object} event - Event object from Lambda call
   * @returns  {String} The URI that the SendGrid webhook is calling
   */
  return function validateSendGridWebhookRequest(event) {
    logger.debug('validateSendGridWebhookRequest', event);

    // Read the verification used to validate requests from ENV
    const verificationKey = config.sendgridVerificationKey;
    if (typeof verificationKey !== 'string') {
      return {
        statusCode: 500, // Internal server error
        body: 'The SENDGRID_VERIFICATION_KEY env variable is undefined and must be set before requests can be accepted',
      };
    }

    // Convert the verification key to the correct value
    let ecPublicKey;
    try {
      ecPublicKey = sendgridWebhookAgent.convertPublicKeyToECDSA(verificationKey);
    } catch (error) {
      return {
        statusCode: 500, // Internal server error
        body: `The SENDGRID_VERIFICATION_KEY was invalid : ${error.message}`,
      };
    }

    // Get the actual signature attached to the inbound request
    let signature;
    if ('x-twilio-email-event-webhook-signature' in event.headers) {
      signature = event.headers['x-twilio-email-event-webhook-signature'];
    } else {
      return {
        statusCode: 401, // Unauthorized
        body: 'No X-Twilio-Email-Event-Webhook-Signature header found on request',
      };
    }

    // Get the timestamp signature attached to the inbound request
    let timestamp;
    if ('x-twilio-email-event-webhook-timestamp' in event.headers) {
      timestamp = event.headers['x-twilio-email-event-webhook-timestamp'];
    } else {
      return {
        statusCode: 401, // Unauthorized
        body: 'No X-Twilio-Email-Event-Webhook-Timestamp header found on request',
      };
    }

    // Calculate the expected signature for this request
    if (!sendgridWebhookAgent.verifySignature(ecPublicKey, event.body, signature, timestamp)) {
      return {
        statusCode: 401, // Unauthorized
        body: `SendGrid webhook signature do not match`,
      };
    }

    // If the request is valid, return 200
    return {
      statusCode: 200,
    };
  };
}

module.exports.makeSendGridWebhookValidator = makeSendGridWebhookValidator;
