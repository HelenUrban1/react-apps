const crypto = require('crypto');

/**
 * Chargify webhook handler
 *
 * @see {@link https://help.chargify.com/webhooks/webhooks-reference.html}
 */

function makeChargifyWebhookValidator(params) {
  const logger = params.logger || console;
  const config = params.config || {};

  /**
   * Return a function that can be used to validate Chargify webhook requests
   *
   * @see {@link https://help.chargify.com/webhooks/webhooks-reference.html}
   *
   * @param  {Object} event - Event object from Lambda call
   * @returns  {String} The URI that the Chargify webhook is calling
   */
  return function validateChargifyWebhookRequest(event) {
    logger.debug('validateChargifyWebhookRequest', event);
    // Read the shared secret used to validate requests from ENV
    const sharedSecret = config.chargifyWebhookSecret;

    // Validate incoming request using shared secret
    if (typeof sharedSecret !== 'string') {
      return {
        statusCode: 500,
        body: 'The CHARGIFY_WEBHOOK_SECRET env variable is undefined and must be set before requests can be accepted',
      };
    }

    // Get the actual signature attached to the inbound request
    let sig;
    if ('x-chargify-webhook-signature-hmac-sha-256' in event.headers) {
      sig = event.headers['x-chargify-webhook-signature-hmac-sha-256'];
    } else {
      return {
        statusCode: 401,
        body: 'No x-chargify-webhook-signature-hmac-sha-256 header found on request',
      };
    }

    // Calculate the expected signature for this request
    const calculatedSig = crypto.createHmac('sha256', sharedSecret).update(event.decodedBody).digest('hex');

    // Verify that the calculate signature matches the provded one
    if (sig !== calculatedSig) {
      return {
        statusCode: 401,
        body: `x-chargify-webhook-signature-hmac-sha-256 incorrect. Chargify webhook signature does not match: ${sig} != ${calculatedSig}`,
      };
    }

    // If the request is valid, return 200
    return {
      statusCode: 200,
    };
  };
}

module.exports.makeChargifyWebhookValidator = makeChargifyWebhookValidator;
