const crypto = require('crypto');

// Hubspot Request validation - https://legacydocs.hubspot.com/docs/faq/v1-request-validation

// Test form with Workflow : IX 3.0 Trial (Test)
// https://app.hubspot.com/forms/4173503/9b7ea834-0134-43d4-ba0c-04cc0732350d/performance

/**
 * Hubspot webhook handler
 *
 * @see {@link https://knowledge.hubspot.com/workflows/how-do-i-use-webhooks-with-hubspot-workflows}
 */
function makeHubspotWebhookValidator(params) {
  const logger = params.logger || console;
  const config = params.config || {};

  /**
   * Construct the URI that the Hubspot webhook is calling, which is required for v2 requests
   *
   * @see {@link https://legacydocs.hubspot.com/docs/faq/validating-requests-from-hubspot}
   *
   * @param  {Object} event - Event object from Lambda call
   * @returns  {String} The URI that the Hubspot webhook is calling
   */
  const getHttpURI = (event) => {
    let httpURI;
    if (event.headers['x-forwarded-proto'] && event.headers.host && event.path) {
      httpURI = `${event.headers['x-forwarded-proto']}://${event.headers.host}${event.path}`;
    } else if (config.baseUrl) {
      httpURI = `${config.baseUrl}${event.path}`;
    } else {
      logger.error('Unable to calculate HttpURI');
    }
    return httpURI;
  };

  /**
   * Return a function that can be used to validate Hubspot webhook requests
   *
   * @see {@link https://legacydocs.hubspot.com/docs/faq/validating-requests-from-hubspot}
   *
   * @param  {Object} event - Event object from Lambda call
   * @returns  {String} The URI that the Hubspot webhook is calling
   */
  return function validateHubspotWebhookRequest(event) {
    logger.debug('validateHubspotWebhookRequest', event);

    // Read the shared secret used to validate requests from ENV
    const sharedSecret = config.hubspotWebhookSecret;

    // Validate incoming request using shared secret
    if (typeof sharedSecret !== 'string') {
      return {
        statusCode: 500, // Internal server error
        body: 'The HUBSPOT_WEBHOOK_SECRET env variable is undefined and must be set before requests can be accepted',
      };
    }

    // Get the actual signature attached to the inbound request
    let sig;
    if ('x-hubspot-signature' in event.headers) {
      sig = event.headers['x-hubspot-signature'];
    } else {
      return {
        statusCode: 401, // Unauthorized
        body: 'No X-HubSpot-Signature header found on request',
      };
    }

    // Determine whether the inbound webhook is using hubspot's v1 or v2 signature
    let sigVersion;
    if ('x-hubspot-signature-version' in event.headers) {
      sigVersion = event.headers['x-hubspot-signature-version'];
    } else {
      return {
        statusCode: 401, // Unauthorized
        body: 'No x-hubspot-signature-version header found on request',
      };
    }

    // Calculate the expected signature for this request
    let calculatedSig;
    if (sigVersion === 'v1') {
      calculatedSig = crypto.createHash('sha256').update(`${sharedSecret}${event.body}`).digest('hex');
    } else if (sigVersion === 'v2') {
      const httpMethod = event.httpMethod || 'POST';
      const httpURI = getHttpURI(event);
      calculatedSig = crypto.createHash('sha256').update(`${sharedSecret}${httpMethod}${httpURI}${event.body}`).digest('hex');
    } else {
      return {
        statusCode: 501, // Not Implemented
        body: `Unrecognized x-hubspot-signature-version found: ${sigVersion}`,
      };
    }

    // Verify that the calculate signature matches the provded one
    if (sig !== calculatedSig) {
      return {
        statusCode: 401, // Unauthorized
        body: `X-Hub-Signature header incorrect. Hubspot webhook signature do not match: ${sig} != ${calculatedSig}`,
      };
    }

    // If the request is valid, return 200
    return {
      statusCode: 200,
    };
  };
}

module.exports.makeHubspotWebhookValidator = makeHubspotWebhookValidator;
