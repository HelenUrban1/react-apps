const AWS = require('aws-sdk');
const { makeWebhookListener } = require('./src/webhookListnerFactory');
const config = require('./src/config');
const { makeLogger } = require('./src/logger');

// Configre real AWS
AWS.config.update({ region: config.awsRegion });
const sqs = new AWS.SQS();
// const s3 = new AWS.S3();

const webhookListener = makeWebhookListener({
  config,
  makeLogger,
  sqs,
});

module.exports.handler = webhookListener;
