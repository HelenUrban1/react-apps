const { makeChargifyWebhookValidator } = require('../src/chargifyWebhookValidator');

const CHARGIFY_TEST_WEBHOOK_SECRET = '5ynS4eeZcRco3h5gfBogJLSied84AlGgTwM0x89GPw';
const chargifyWebhookCallStub = require('./fixtures/chargify/customer_create.json');

const { event } = chargifyWebhookCallStub;
event.body = JSON.stringify(event.body);

describe('validateChargifyWebhookRequest', () => {
  // Prepare the mock webhook event

  const testConfig = {
    chargifyWebhookSecret: CHARGIFY_TEST_WEBHOOK_SECRET,
  };

  const loggerMock = {
    trace: jest.fn(),
    debug: jest.fn(),
    log: jest.fn(),
    info: jest.fn(),
    warn: jest.fn(),
    error: jest.fn(),
  };

  let validateChargifyWebhookRequest;
  beforeEach(() => {
    jest.resetAllMocks();
    validateChargifyWebhookRequest = makeChargifyWebhookValidator({
      logger: loggerMock,
      config: testConfig,
    });
  });

  it('should return 200 when the request is valid', async () => {
    const response = validateChargifyWebhookRequest(event);
    expect(response).toBeTruthy();
    expect(response.statusCode).toBe(200);
    expect(loggerMock.error).toHaveBeenCalledTimes(0);
  });

  it('should return 500 (Server Error) when CHARGIFY_WEBHOOK_SECRET is not available', async () => {
    validateChargifyWebhookRequest = makeChargifyWebhookValidator({
      logger: loggerMock,
      config: {
        chargifyWebhookSecret: undefined,
      },
    });
    const response = validateChargifyWebhookRequest(event);
    expect(response).toBeTruthy();
    expect(response.statusCode).toBe(500);
    expect(response.body).toBe('The CHARGIFY_WEBHOOK_SECRET env variable is undefined and must be set before requests can be accepted');
    expect(loggerMock.error).toHaveBeenCalledTimes(0);
  });

  it('should return 401 (Unauthorized) when the x-chargify-webhook-signature-hmac-sha-256 header is not present', async () => {
    const badEvent = JSON.parse(JSON.stringify(event));
    delete badEvent.headers['x-chargify-webhook-signature-hmac-sha-256'];
    const response = validateChargifyWebhookRequest(badEvent);
    expect(response).toBeTruthy();
    expect(response.statusCode).toBe(401);
    expect(response.body).toBe('No x-chargify-webhook-signature-hmac-sha-256 header found on request');
    expect(loggerMock.error).toHaveBeenCalledTimes(0);
  });

  it('should return 401 (Unauthorized) when the request signature doesnt match', async () => {
    validateChargifyWebhookRequest = makeChargifyWebhookValidator({
      logger: loggerMock,
      config: {
        chargifyWebhookSecret: '123',
      },
    });
    const response = validateChargifyWebhookRequest(event);
    expect(response).toBeTruthy();
    expect(response.statusCode).toBe(401);
    expect(response.body).toBe('x-chargify-webhook-signature-hmac-sha-256 incorrect. Chargify webhook signature does not match: 3a9261f10605b1be48775aa6bdcf369d95837352a057c530506886719f7408f2 != fd793c00742b55f3f7f004549d726b23f86a06800aff78b87b1dae2711b56cf4');
    expect(loggerMock.error).toHaveBeenCalledTimes(0);
  });
});
