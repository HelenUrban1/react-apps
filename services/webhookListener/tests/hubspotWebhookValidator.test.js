const { makeHubspotWebhookValidator } = require('../src/hubspotWebhookValidator');

const HUBSPOT_TEST_WEBHOOK_SECRET = 'fdb0b67d-7999-478e-87ad-47c2e42bba09';

// Prepare the mock webhook event
const hubspotWebhookCallStub = require('./fixtures/hubspot/hubspotWebhookCall.json');

const { event } = hubspotWebhookCallStub;
event.body = JSON.stringify(event.body);

describe('validateHubspotWebhookRequest', () => {
  const testConfig = {
    // env: 'test',
    // baseUrl: 'https://alb.ixc-dev.com',
    // DefaultRegion: 'us-east-1',
    // webhookSqsUrl: 'https://sqs.us-east-1.amazonaws.com/541855766228/incoming-webhooks.fifo',
    hubspotWebhookSecret: HUBSPOT_TEST_WEBHOOK_SECRET,
  };

  const loggerMock = {
    trace: jest.fn(),
    debug: jest.fn(),
    log: jest.fn(),
    info: jest.fn(),
    warn: jest.fn(),
    error: jest.fn(),
  };

  let validateHubspotWebhookRequest;
  beforeEach(() => {
    jest.resetAllMocks();
    validateHubspotWebhookRequest = makeHubspotWebhookValidator({
      logger: loggerMock,
      config: testConfig,
    });
  });

  it('should return 200 when the request is valid', async () => {
    const response = validateHubspotWebhookRequest(event);
    expect(response).toBeTruthy();
    expect(response.statusCode).toBe(200);
    expect(loggerMock.error).toHaveBeenCalledTimes(0);
  });

  it('should return 500 (Server Error) when HUBSPOT_WEBHOOK_SECRET is not available', async () => {
    validateHubspotWebhookRequest = makeHubspotWebhookValidator({
      logger: loggerMock,
      config: {
        hubspotWebhookSecret: undefined,
      },
    });
    const response = validateHubspotWebhookRequest(event);
    expect(response).toBeTruthy();
    expect(response.statusCode).toBe(500);
    expect(response.body).toBe('The HUBSPOT_WEBHOOK_SECRET env variable is undefined and must be set before requests can be accepted');
    expect(loggerMock.error).toHaveBeenCalledTimes(0);
  });

  it('should return 401 (Unauthorized) when the x-hubspot-signature header is not present', async () => {
    const badEvent = JSON.parse(JSON.stringify(event));
    delete badEvent.headers['x-hubspot-signature'];
    const response = validateHubspotWebhookRequest(badEvent);
    expect(response).toBeTruthy();
    expect(response.statusCode).toBe(401);
    expect(response.body).toBe('No X-HubSpot-Signature header found on request');
    expect(loggerMock.error).toHaveBeenCalledTimes(0);
  });

  it('should return 401 (Unauthorized) when the x-hubspot-signature-version header is not present', async () => {
    const badEvent = JSON.parse(JSON.stringify(event));
    delete badEvent.headers['x-hubspot-signature-version'];
    const response = validateHubspotWebhookRequest(badEvent);
    expect(response).toBeTruthy();
    expect(response.statusCode).toBe(401);
    expect(response.body).toBe('No x-hubspot-signature-version header found on request');
    expect(loggerMock.error).toHaveBeenCalledTimes(0);
  });

  it('should return 501 (Not Implemented) when the x-hubspot-signature-version header is present but not recognized', async () => {
    const badEvent = JSON.parse(JSON.stringify(event));
    badEvent.headers['x-hubspot-signature-version'] = 'v3';
    const response = validateHubspotWebhookRequest(badEvent);
    expect(response).toBeTruthy();
    expect(response.statusCode).toBe(501);
    expect(response.body).toBe('Unrecognized x-hubspot-signature-version found: v3');
    expect(loggerMock.error).toHaveBeenCalledTimes(0);
  });

  it('should return 401 (Unauthorized) when the request signature doesnt match', async () => {
    validateHubspotWebhookRequest = makeHubspotWebhookValidator({
      logger: loggerMock,
      config: {
        hubspotWebhookSecret: '123',
      },
    });
    const response = validateHubspotWebhookRequest(event);
    expect(response).toBeTruthy();
    expect(response.statusCode).toBe(401);
    expect(response.body).toBe('X-Hub-Signature header incorrect. Hubspot webhook signature do not match: 5a7694e00f341fe73f624fe2fc80f7b0d4d3dde172007af02577ca92dbf964bc != c8d569a7976e5554d5e2f5ddbe5b6e7c1994ad3857443a26728ff881edf6f669');
    expect(loggerMock.error).toHaveBeenCalledTimes(0);
  });
});
