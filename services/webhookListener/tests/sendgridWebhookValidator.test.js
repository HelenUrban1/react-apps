const { makeSendGridWebhookValidator } = require('../src/sendgridWebhookValidator');

const VALID_SENDGRID_VERIFICATION_KEY = 'MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEwbSaQ6nAAEUVL1krBCZiZ61SijA2Ach5Wpc+UtU5/HlpA4wUN2Wp43OQ9VRp7hJd7PVZk60bCxeWSdRCb9vCTA==';
const BAD_SENDGRID_VERIFICATION_KEY = 'MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAE0hEnyUefd6diWDy5eVsPIdd5lxVN+ldl/Tb4WODRWc+YF4mRofEz7VaIXCMbxPMcbBThBuXX6Nj86TKzZw15TQ==';

// Prepare the mock webhook event
const event = require('./fixtures/sendgrid/sendgridWebhookEvent.json');

describe('validateSendgridWebhookRequest', () => {
  const testConfig = {
    // env: 'test',
    // baseUrl: 'https://alb.ixc-dev.com',
    // DefaultRegion: 'us-east-1',
    // webhookSqsUrl: 'https://sqs.us-east-1.amazonaws.com/541855766228/incoming-webhooks.fifo',
    sendgridVerificationKey: VALID_SENDGRID_VERIFICATION_KEY,
  };

  const loggerMock = {
    trace: jest.fn(),
    debug: jest.fn(),
    log: jest.fn(),
    info: jest.fn(),
    warn: jest.fn(),
    error: jest.fn(),
  };

  let validateSendGridWebhookRequest;
  beforeEach(() => {
    jest.resetAllMocks();
    validateSendGridWebhookRequest = makeSendGridWebhookValidator({
      logger: loggerMock,
      config: testConfig,
    });
  });

  it('should return 200 when the request is valid', async () => {
    const response = validateSendGridWebhookRequest(event);
    expect(response).toBeTruthy();
    expect(response.statusCode).toBe(200);
    expect(loggerMock.error).toHaveBeenCalledTimes(0);
  });

  it('should return 500 (Server Error) when SENDGRID_VERIFICATION_KEY is not available', async () => {
    validateSendGridWebhookRequest = makeSendGridWebhookValidator({
      logger: loggerMock,
      config: {
        sendgridVerificationKey: undefined,
      },
    });
    const response = validateSendGridWebhookRequest(event);
    expect(response).toBeTruthy();
    expect(response.statusCode).toBe(500);
    expect(response.body).toBe('The SENDGRID_VERIFICATION_KEY env variable is undefined and must be set before requests can be accepted');
    expect(loggerMock.error).toHaveBeenCalledTimes(0);
  });

  it('should return 500 (Server Error) when SENDGRID_VERIFICATION_KEY is not valid', async () => {
    validateSendGridWebhookRequest = makeSendGridWebhookValidator({
      logger: loggerMock,
      config: {
        sendgridVerificationKey: '123',
      },
    });
    const response = validateSendGridWebhookRequest(event);
    expect(response).toBeTruthy();
    expect(response.statusCode).toBe(500);
    expect(response.body).toContain('The SENDGRID_VERIFICATION_KEY was invalid');
    expect(loggerMock.error).toHaveBeenCalledTimes(0);
  });

  it('should return 401 (Unauthorized) when the x-twilio-email-event-webhook-signature header is not present', async () => {
    const badEvent = JSON.parse(JSON.stringify(event));
    delete badEvent.headers['x-twilio-email-event-webhook-signature'];
    const response = validateSendGridWebhookRequest(badEvent);
    expect(response).toBeTruthy();
    expect(response.statusCode).toBe(401);
    expect(response.body).toBe('No X-Twilio-Email-Event-Webhook-Signature header found on request');
    expect(loggerMock.error).toHaveBeenCalledTimes(0);
  });

  it('should return 401 (Unauthorized) when the x-twilio-email-event-webhook-timestamp header is not present', async () => {
    const badEvent = JSON.parse(JSON.stringify(event));
    delete badEvent.headers['x-twilio-email-event-webhook-timestamp'];
    const response = validateSendGridWebhookRequest(badEvent);
    expect(response).toBeTruthy();
    expect(response.statusCode).toBe(401);
    expect(response.body).toBe('No X-Twilio-Email-Event-Webhook-Timestamp header found on request');
    expect(loggerMock.error).toHaveBeenCalledTimes(0);
  });

  it('should return 401 (Unauthorized) when the request signature doesnt match', async () => {
    validateSendGridWebhookRequest = makeSendGridWebhookValidator({
      logger: loggerMock,
      config: {
        sendgridVerificationKey: BAD_SENDGRID_VERIFICATION_KEY,
      },
    });
    const response = validateSendGridWebhookRequest(event);
    expect(response).toBeTruthy();
    expect(response.statusCode).toBe(401);
    expect(response.body).toBe('SendGrid webhook signature do not match');
    expect(loggerMock.error).toHaveBeenCalledTimes(0);
  });
});
