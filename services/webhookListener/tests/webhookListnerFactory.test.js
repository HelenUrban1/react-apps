const { makeWebhookListener } = require('../src/webhookListnerFactory');

// Prepare the mock webhook event
const hubspotWebhookCallStub = require('./fixtures/hubspot/hubspotWebhookCall.json');
const chargifyWebhookCallStub = require('./fixtures/chargify/customer_create.json');
const sendgridWebhookCallStub = require('./fixtures/sendgrid/sendgridWebhookLambdaCall.json');

describe('webhookListener', () => {
  const testConfig = {
    env: 'test',
    baseUrl: 'https://alb.ixc-dev.com',
    DefaultRegion: 'us-east-1',
    webhookSqsUrl: 'https://sqs.us-east-1.amazonaws.com/541855766228/incoming-webhooks.fifo',
    hubspotWebhookSecret: '123',
    chargifyWebhookSecret: '123',
    sendgridVerificationKey: '123',
  };

  const loggerMock = {
    trace: jest.fn(),
    debug: jest.fn(),
    log: jest.fn(),
    info: jest.fn(),
    warn: jest.fn(),
    error: jest.fn(),
  };
  const makeLogger = () => loggerMock;

  const mockSqs = {
    sendMessage: jest.fn(),
  };

  const mockValidateHubspotWebhookRequest = jest.fn();
  const mockValidateChargifyWebhookRequest = jest.fn();
  const mockValidateSendGridWebhookRequest = jest.fn();

  const webhookListener = makeWebhookListener({
    config: testConfig,
    makeLogger,
    sqs: mockSqs,
    validateHubspotWebhookRequest: mockValidateHubspotWebhookRequest,
    validateChargifyWebhookRequest: mockValidateChargifyWebhookRequest,
    validateSendGridWebhookRequest: mockValidateSendGridWebhookRequest,
  });

  beforeEach(() => {
    jest.resetAllMocks();
  });

  it('should return 200 when the request is valid', async () => {
    mockValidateHubspotWebhookRequest.mockReturnValueOnce({
      statusCode: 200,
    });
    mockSqs.sendMessage.mockReturnValueOnce({
      promise: () => {
        return Promise.resolve({
          ResponseMetadata: {
            RequestId: 'a47b1ea3-1cc2-5901-a2ff-468e5151af18',
          },
          MD5OfMessageBody: 'd7ac12b7a8fb9ce750e2f13bfc11c8f2',
          MD5OfMessageAttributes: '95f2cc52b7e34daabb1ac07dcd02e46c',
          MessageId: 'ba5bc73f-895d-49fc-b995-ade1c97bd856',
          SequenceNumber: '18857774025124848384',
        });
      },
    });
    const { event, context } = hubspotWebhookCallStub;
    event.body = JSON.stringify(event.body);
    const response = await webhookListener(event, context);
    expect(response.statusCode).toBe(200);
    const body = JSON.parse(response.body);
    expect(body.event.bodyAsObject).toBeDefined();
    expect(body.event.bodyAsObject.vid).toBe(2651);
    expect(loggerMock.error).toHaveBeenCalledTimes(0);
  });

  it('should return 501 (Not Implemented) when the request can not be matched with a handler', async () => {
    const { context } = chargifyWebhookCallStub;
    const unrecognizedEvent = {
      headers: {
        'user-agent': 'foo',
      },
    };
    const response = await webhookListener(unrecognizedEvent, context);
    expect(response.statusCode).toBe(501);
    expect(response.body).toBe('The request could not be matched with a handler');
    expect(loggerMock.error).toHaveBeenCalledTimes(1);
  });

  it('should return 500 when SQS fails', async () => {
    mockValidateHubspotWebhookRequest.mockReturnValueOnce({
      statusCode: 200,
    });
    mockSqs.sendMessage.mockReturnValueOnce({
      promise: () => {
        return Promise.reject(new Error('SQS failed for some reason'));
      },
    });
    const { event, context } = hubspotWebhookCallStub;
    event.body = JSON.stringify(event.body);
    const response = await webhookListener(event, context);
    expect(response.statusCode).toBe(500);
    expect(response.body).toContain('An error occured while processing the webhook request:');
    expect(loggerMock.error).toHaveBeenCalledTimes(1);
  });

  describe('Message Body content-type Handling', () => {
    it('should extract application/json to JS objects', async () => {
      mockValidateSendGridWebhookRequest.mockReturnValue({
        statusCode: 200,
      });
      mockSqs.sendMessage.mockReturnValueOnce({
        promise: () => {
          return Promise.resolve({
            ResponseMetadata: {
              RequestId: 'a47b1ea3-1cc2-5901-a2ff-468e5151af18',
            },
            MD5OfMessageBody: 'd7ac12b7a8fb9ce750e2f13bfc11c8f2',
            MD5OfMessageAttributes: '95f2cc52b7e34daabb1ac07dcd02e46c',
            MessageId: 'ba5bc73f-895d-49fc-b995-ade1c97bd856',
            SequenceNumber: '18857774025124848384',
          });
        },
      });
      const { event, context } = sendgridWebhookCallStub;
      // event.body = JSON.stringify(event.body); // Not necessary, because value is already a string
      const response = await webhookListener(event, context);
      expect(response.statusCode).toBe(200);
      const call = mockSqs.sendMessage.mock.calls[0][0];
      expect(call).toBeDefined();
      expect(call.MessageBody).toContain('"event":"open"');

      const body = JSON.parse(response.body);
      expect(body.event.bodyAsObject).toBeDefined();
      expect(body.event.bodyAsObject.length).toBe(1);
      expect(body.event.bodyAsObject[0].event).toContain('open');
      expect(loggerMock.error).toHaveBeenCalledTimes(0);
    });

    it('should convert base64 encoded bodies and return application/x-www-form-urlencoded as JSON', async () => {
      mockValidateChargifyWebhookRequest.mockReturnValueOnce({
        statusCode: 200,
      });
      mockSqs.sendMessage.mockReturnValueOnce({
        promise: () => {
          return Promise.resolve({
            ResponseMetadata: {
              RequestId: 'a47b1ea3-1cc2-5901-a2ff-468e5151af18',
            },
            MD5OfMessageBody: 'd7ac12b7a8fb9ce750e2f13bfc11c8f2',
            MD5OfMessageAttributes: '95f2cc52b7e34daabb1ac07dcd02e46c',
            MessageId: 'ba5bc73f-895d-49fc-b995-ade1c97bd856',
            SequenceNumber: '18857774025124848384',
          });
        },
      });
      const { event, context } = chargifyWebhookCallStub;
      event.body = JSON.stringify(event.body);
      const response = await webhookListener(event, context);
      expect(response.statusCode).toBe(200);
      const body = JSON.parse(response.body);
      expect(body.event.decodedBody).toBeDefined();
      expect(body.event.decodedBody).toContain('&event=customer_create&');
      expect(body.event.bodyAsObject.event).toBe('customer_create');
      expect(loggerMock.error).toHaveBeenCalledTimes(0);
    });
  });
});
