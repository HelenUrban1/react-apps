module.exports = {
  preset: 'ts-jest',
  testEnvironment: 'node',
  roots: ['<rootDir>/lib'],
  transform: {
    '^.+\\.tsx?$': 'ts-jest',
  },
  moduleFileExtensions: ['ts', 'tsx', 'js', 'jsx', 'json', 'node'],
  testMatch: ['**/?(*.)+(spec|test).[jt]s?(x)'],
  testPathIgnorePatterns: ['<rootDir>/node_modules/', '<rootDir>/bin/', '<rootDir>/coverage/', '<rootDir>/dist/'],
  coverageDirectory: 'coverage',
  coverageReporters: ['text', 'json', 'lcov', 'clover'],
  testResultsProcessor: 'jest-sonar-reporter',
  coverageThreshold: {
    global: {
      statements: 90,
      branches: 70,
      functions: 88,
      lines: 90,
    },
  },
};
