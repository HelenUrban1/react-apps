# IXC-Mono Queue Processor

This project runs code based on items in an SQS Queue for InspectionXpert Cloud.

# Getting Started

## Clone this Repo

```
git clone git@bitbucket.org:ixeng/ixc-mono.git
```

## Running Locally

1 - Install PostgreSQL (https://www.postgresql.org/download/)

2 - Add a `.env` file to 'services/queue-processor' folder. Populate based on [this table](#environment-variables)

3 - Build supporting sub-packages

```
cd packages/aws-wrapper
npm install
npm run build
```

4 - Go to the 'services/queue-processor' folder, install, and build:

```
cd services/queue-processor
npm install
npm run build
```

# Running Tests

## Jest

Jest handles unit tests. To run the jest tests, use `npm run test` from within the services/queue-processor folder.

# Environment Variables

| Variable                                   | Required? | Example Value                                                                            | Notes                                                                                   |
| ------------------------------------------ | :-------: | ---------------------------------------------------------------------------------------- | --------------------------------------------------------------------------------------- |
| `NODE_ENV`                                 |     Y     | development                                                                              | Sets the environment we are in. Default value: production                               |
| `IXC_AWS_REGION`                           |     Y     | us-east-1                                                                                | AWS Region to use when making calls with the AWS SDK.                                   |
| `IXC_SQS_URL`                              |     Y     | https://sqs.us-east-1.amazonaws.com/101010101010/Queue                                   | SQS queue to listen to.                                                                 |
| `IXC_POLLING_TIMEOUT`                      |     Y     | 10000                                                                                    | Number of milliseconds to wait between polling attempts.                                |
| `IXC_DB_BACKUP_BUCKET`                     |     Y     | inspection-xpert-dev-rdc-backup                                                          | S3 bucket that database backups will be saved to.                                       |
| `IXC_ACCOUNT_DB_PREFIX`                    |     Y     | ixc_account_                                                                             | Account database names are created by combining this value with the env and account ID. |
| `IXC_DB_HOST`                              |     Y     | localhost                                                                                | Host name of database to target for backups.                                            |
| `IXC_PG_DUMP_PATH`                         |     Y     | *Nix: /usr/bin/pg_dump      Win: C:\Program Files\PostgreSQL\10\bin\pg_dump.exe          | Path to the Postgres pg_dump binary.                                                    |
| `IXC_PG_RESTORE_PATH`                      |     Y     | *Nix: /usr/bin/pg_restore   Win: C:\Program Files\PostgreSQL\10\bin\pg_restore.exe       | Path to the Postgres pg_restore binary.                                                 |
| `IXC_NEW_RELIC_LICENSE_KEY`                |     Y     |                                                                                          | License key for New Relic.                                                              |
| `LOG_LEVEL`                                |     N     | info                                                                                     | Output log level. Defaults to 'info' for prod, 'error' for test, and 'debug' for dev.   |
| `SONAR_USER_TOKEN`                         |     Y     |                                                                                          | User Token for SonarQube.                                                               |
| `VPC_NAME`                                 |     Y     | dev                                                                                      | Used to set app name during New Relic agent config. Can be: dev, qa, stage, prod.       |


# Restoring a backup

1. Install the postgresql client if you don't already have it
    ```
    sudo apt install postgresql-client-common
    sudo apt install postgresql-client
    ```
 
2. Download .backup file from S3
 
3. Run the restore command below
    - Database must exist before restore
    - Add --cleanup to drop tables before restore, restoring into a non-empty database will likely have errors.
        ```
        pg_restore -h postgresql-cluster-endpoint -p 5432 -U username -d database -v backupfile.backup
        ```