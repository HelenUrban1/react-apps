import * as winston from 'winston';

require('dotenv').config();

let newRelicConfig = 'Dev';
if (process.env.NODE_ENV === 'production') {
  newRelicConfig = 'Prod';
} else if (process.env.NODE_ENV === 'test') {
  newRelicConfig = 'Test';
}

export default class Config {
  env = process.env.NODE_ENV;

  s3Region = process.env.IXC_AWS_REGION;

  queue = {
    sqsUrl: process.env.IXC_SQS_URL || '',
    pollingTimeout: parseInt(process.env.IXC_POLLING_TIMEOUT!, 10),
  };

  backup = {
    s3Bucket: process.env.IXC_DB_BACKUP_BUCKET,
    accountDbPrefix: process.env.IXC_ACCOUNT_DB_PREFIX,
    pgHost: process.env.IXC_DB_HOST,
    pgDatabase: '',
    pgUser: '',
    pgPassword: '',
    pgdumpArgs: [] as string[],
    pgrestoreArgs: [] as string[],
    pgdumpPath: process.env.IXC_PG_DUMP_PATH as string,
    pgrestorePath: process.env.IXC_PG_RESTORE_PATH as string,
    // maximum time allowed to connect to postgres before a timeout occurs
    pgconnectTimeout: 15,
  };

  // Logging config for Winston
  productionLogging = {
    exitOnError: false,
    transports: [
      {
        type: 'console',
        level: process.env.LOG_LEVEL || 'info',
        colorize: false,
        handleExceptions: true,
        format: winston.format.json(),
      },
    ],
  };

  testLogging = {
    exitOnError: false,
    transports: [
      {
        type: 'console',
        level: process.env.LOG_LEVEL || 'error',
        colorize: true,
        handleExceptions: true,
      },
    ],
  };

  developmentLogging = {
    exitOnError: false,
    transports: [
      {
        type: 'console',
        level: process.env.LOG_LEVEL || 'debug',
        colorize: true,
        handleExceptions: true,
      },
    ],
  };

  new_relic = {
    license_key: process.env.IXC_NEW_RELIC_LICENSE_KEY,
    labels: `Config:${newRelicConfig},Tier:Ixc_Queue_Processor`,
  };
}
