import { spawn } from 'child_process';
import through2 from 'through2';
import fs from 'fs';
import Config from '../config';

export default class Pgrestore {
  spawnPgRestore = (pgRestorePath: string, args: string[], env: NodeJS.ProcessEnv) => {
    if (!fs.existsSync(pgRestorePath)) {
      throw new Error(`pg_restore not found at ${pgRestorePath}`);
    }

    return spawn(pgRestorePath, args, {
      env,
    });
  };

  buildArgs = (pgrestoreArgs: string[]) => {
    let args = ['-Fc', '-l'];
    const extraArgs = pgrestoreArgs;
    args = args.concat(extraArgs);
    return args;
  };

  pgrestore = (config: Config, input: string | Uint8Array, pgRestoreSpawnFn = this.spawnPgRestore) => {
    return new Promise((resolve, reject) => {
      let headerChecked = false;
      let stderr = '';

      // spawn pg_restore process
      const args = this.buildArgs(config.backup.pgrestoreArgs);
      const env = {
        S3_REGION: config.s3Region,
        PGDUMP_PATH: config.backup.pgdumpPath,
        PGRESTORE_PATH: config.backup.pgrestorePath,
        LD_LIBRARY_PATH: config.backup.pgrestorePath,
      };
      const pgrestoreProcess = pgRestoreSpawnFn(config.backup.pgrestorePath, args, env);

      pgrestoreProcess.stdin.write(input);

      // hook into the process
      pgrestoreProcess.stderr.on('data', (data: any) => {
        stderr += data.toString('utf8');
      });

      pgrestoreProcess.on('close', (code: number) => {
        // reject our promise if pg_restore had a non-zero exit
        if (code !== 0) {
          return reject(new Error(`pg_restore process failed: ${stderr}`));
        }
        // check that pg_restore actually gave us some data
        if (!headerChecked) {
          return reject(new Error('pg_restore gave us an unexpected response'));
        }
        return null;
      });

      // use through2 to proxy the pg_restore stdout stream
      // so we can check it's valid
      const buffer = through2(function bufferCheck(chunk) {
        this.push(chunk);
        if (!headerChecked) {
          headerChecked = true;

          // Check that there is a greater than 0 number of entries in the TOC and the DB name is in the file
          const tocEntriesCheck = /TOC Entries: [1-9][0-9]*/g;
          const outputString = chunk.toString('utf8');
          if (tocEntriesCheck.test(outputString) && outputString.includes(config.backup.pgDatabase)) {
            // Prevents an EOF error
            pgrestoreProcess.stdin.destroy();
            resolve(buffer);
          } else {
            reject(new Error('pg_restore gave us an unexpected response, check the backups.'));
          }
        }
      });

      // pipe pg_restore to buffer
      pgrestoreProcess.stdout.pipe(buffer);
    });
  };
}
