import moment from 'moment';

export default class Util {
  generateBackupPath = (databaseName: string, rootPath: string | null, now?: moment.Moment | null) => {
    const currentDate = now || moment().utc();
    const timestamp = moment(currentDate).format('YYYY-MM-DD@HH-mm-ss');
    const filename = `${databaseName}-${timestamp}.backup`;
    const key = `${databaseName}/${filename}`;
    return key;
  };
}
