import path from 'path';
import fs from 'fs';
import sinon from 'sinon';
import Pgdump from './pgdump';
import DefaultConfig from '../config';

const mockSpawn = require('mock-spawn');

describe('pgdump', () => {
  it('should export a function', () => {
    return expect(typeof new Pgdump().pgdump).toBe('function');
  });

  it('should throw an error when pg_dump sends invalid data', () => {
    const pgdumpProcess = mockSpawn()();
    const pgDumpFn = () => pgdumpProcess;
    const config = new DefaultConfig();
    const p = new Pgdump().pgdump(config, pgDumpFn);
    pgdumpProcess.stdout.write('asdfasdf');
    pgdumpProcess.emit('close', 0);
    return expect(p).rejects.toEqual(Error('pg_dump gave us an unexpected response'));
  });

  it('should call pg_dump with some default args', async () => {
    const pgdumpProcess = mockSpawn()();
    const pgDumpFn = sinon.fake.returns(pgdumpProcess);
    const config = new DefaultConfig();
    const p = new Pgdump().pgdump(config, pgDumpFn);
    pgdumpProcess.stdout.write('PGDMP - data - data');
    pgdumpProcess.emit('close', 0);
    await p;

    expect(pgDumpFn.calledOnce).toBeTruthy();
    const pgDumpArgs = pgDumpFn.getCall(0).args[1];
    expect(pgDumpArgs).toStrictEqual(['-Fc', '-Z1']);
  });

  it('should call pg_dump with provided extra arguments as array', async () => {
    const pgdumpProcess = mockSpawn()();
    const pgDumpFn = sinon.fake.returns(pgdumpProcess);

    const config = new DefaultConfig();
    config.backup.pgdumpArgs = ['--exclude-table=ignored-table', '-N', 'public'];

    const p = new Pgdump().pgdump(config, pgDumpFn);
    pgdumpProcess.stdout.write('PGDMP - data - data');
    pgdumpProcess.emit('close', 0);
    await p;

    expect(pgDumpFn.calledOnce).toBeTruthy();
    const pgDumpArgs = pgDumpFn.getCall(0).args[1];

    expect(pgDumpArgs).toStrictEqual(['-Fc', '-Z1', '--exclude-table=ignored-table', '-N', 'public']);
  });

  it('should stream correctly', async () => {
    const pgdumpProcess = mockSpawn()();

    const pgDumpFn = () => pgdumpProcess;
    const config = new DefaultConfig();
    const p = new Pgdump().pgdump(config, pgDumpFn);
    pgdumpProcess.stdout.write('PGDMP - data - data');
    pgdumpProcess.emit('close', 0);

    const buffer: any = await p;

    expect(buffer.read().toString('utf8')).toBe('PGDMP - data - data');
  });

  it('should throw an error when the pg_dump binary does not exist', () => {
    const config = new DefaultConfig();
    config.backup.pgdumpPath = path.join('some', 'non-existent', 'path', 'pg_dump');
    const p = new Pgdump().pgdump(config);
    return expect(p).rejects.toEqual(Error(`pg_dump not found at ${config.backup.pgdumpPath}`));
  });

  describe('default pg_dump binary', () => {
    const config = new DefaultConfig();
    const binaryPath = config.backup.pgdumpPath;
    it('should exist', () => {
      if (!fs.existsSync(binaryPath)) {
        throw new Error(`failed to find pg_dump at ${binaryPath}`);
      }
    });
    it('should be +x', () => {
      if (process.platform === 'win32') {
        return;
      }
      fs.access(binaryPath, fs.constants.X_OK, (err) => {
        if (err) throw new Error(`binary ${binaryPath} is not executable`);
      });
    });
  });
});
