import DefaultConfig from '../config';
import Pgdump from './pgdump';
import Pgrestore from './pgrestore';
import Util from './util';
import log from '../logger';

export default class Backup {
  full = async (accountId: string, requestId: string, s3: any, secretsManager: any) => {
    log.info(`Do full backup for ${accountId}`, { rid: requestId });
    return this.backupDatabase(accountId, requestId, s3, secretsManager);
  };

  backupDatabase = async (accountId: string, requestId: string, s3: any, secretsManager: any) => {
    const config = new DefaultConfig();
    try {
      config.backup.pgDatabase = `${config.backup.accountDbPrefix}${config.env}_${accountId}`;
      config.backup.pgUser = `${config.backup.accountDbPrefix}${config.env}_${accountId}`;
      config.backup.pgPassword = await secretsManager.getDatabasePassword(accountId);

      // spawn the pg_dump process
      const stream = await new Pgdump().pgdump(config);

      // stream the backup to S3 bucket: string, key: string, file: AWS.S3.Body, tags: string
      const key = new Util().generateBackupPath(config.backup.pgDatabase, null);
      const tags = '';
      await s3.upload(config.backup.s3Bucket, key, stream, tags);
      return this.checkDatabaseBackup(config, key, requestId, s3);
    } catch (error) {
      log.error(`backupDatabase for accountId ${accountId} in bucket ${config.backup.s3Bucket}`, { rid: requestId });
      log.error(error, { rid: requestId });
      return false;
    }
  };

  checkDatabaseBackup = async (config: DefaultConfig, key: string, requestId: string, s3: any) => {
    try {
      const getFile = await s3.getObject(config.backup.s3Bucket, key);
      // Checks that file can be read, contains db name and a TOC with one or more items
      await new Pgrestore().pgrestore(config, getFile.Body);
      return true;
    } catch (error) {
      log.error(`checkBackupError for key ${key} in bucket ${config.backup.s3Bucket}`, { rid: requestId });
      log.error(error, { rid: requestId });
      return false;
    }
  };

  /**
   * On a successful backup, delete the SQS message and send a
   * success message back to the step function
   * @param message
   * @param taskToken
   */
  successMessageCleanup = async (message: any, taskToken: string | null, queueUrl: string, sqs: any, stepFunctions: any) => {
    const deleteMessageRequest = {
      QueueUrl: queueUrl,
      ReceiptHandle: message.ReceiptHandle,
    };
    sqs.deleteMessage(deleteMessageRequest);
    if (taskToken) {
      const sendTaskSuccessInput = {
        taskToken,
        output: '{}',
      };
      return stepFunctions.sendTaskSuccess(sendTaskSuccessInput);
    }
    return true;
  };

  /**
   * If we fail to backup but we have a taskToken, delete the message
   * and send task failure so retry will be managed by step function
   * @param message
   * @param taskToken
   * @param error
   */
  failureMessageCleanup = async (message: any, taskToken: string | null, queueUrl: string, sqs: any, stepFunctions: any, log: any, error?: Error) => {
    if (taskToken) {
      const deleteMessageRequest = {
        QueueUrl: queueUrl,
        ReceiptHandle: message.ReceiptHandle,
      };
      sqs.deleteMessage(deleteMessageRequest);
      const sendTaskFailureInput = {
        taskToken,
        error: 'BackupError',
        cause: error ? `${error.name}: ${error.message}` : 'Backup or verification failed',
      };
      if (error) {
        log.error('Error', error);
      }
      return stepFunctions.sendTaskFailure(sendTaskFailureInput);
    }
    return false;
  };
}
