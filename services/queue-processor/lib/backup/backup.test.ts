import Backup from './backup';

const { s3, secretsManager, sqs, stepFunctions } = require('aws-wrapper');

describe('Backup', () => {
  describe('full', () => {
    it('backup and verify a database', async () => {
      const accountId = 'e0a01769-c8ed-4090-bd24-9c1b2241fb75';
      const requestId = '123456';
      const result = await new Backup().full(accountId, requestId, s3, secretsManager);
      expect(result).toBe(true);
    });

    it('return false if a database does not exist', async () => {
      const accountId = '12345678-c8ed-4090-bd24-9c1b2241fb75';
      const requestId = '123456';
      // eslint-disable-next-line no-console
      console.info('Trying to back up a database that will not exist, expect two console errors');
      const result = await new Backup().full(accountId, requestId, s3, secretsManager);
      expect(result).toBe(false);
    });
  });

  describe('successMessageCleanup', () => {
    it('should delete message from SQS', async () => {
      const message = {
        ReceiptHandle: 'ZZZZYYYY',
      };
      const taskToken = null;
      const queueUrl = '';

      const sqsSpy = jest.spyOn(sqs, 'deleteMessage');

      await new Backup().successMessageCleanup(message, taskToken, queueUrl, sqs, stepFunctions);
      expect(sqsSpy).toHaveBeenCalled();
    });

    it('should send task success message to step functions if there is a task token', async () => {
      const message = {
        ReceiptHandle: 'ZZZZYYYY',
      };
      const taskToken = 'AAAABBBB';
      const queueUrl = '';
      const stepFunctionsSpy = jest.spyOn(stepFunctions, 'sendTaskSuccess');

      await new Backup().successMessageCleanup(message, taskToken, queueUrl, sqs, stepFunctions);
      expect(stepFunctionsSpy).toHaveBeenCalled();
    });

    it('should return true if there is no task token', async () => {
      const message = {
        ReceiptHandle: 'ZZZZYYYY',
      };
      const taskToken = null;
      const queueUrl = '';

      const result = await new Backup().successMessageCleanup(message, taskToken, queueUrl, sqs, stepFunctions);
      expect(result).toBe(true);
    });
  });

  describe('failureMessageCleanup', () => {
    it('should delete message from SQS if there is a task token', async () => {
      const message = {
        ReceiptHandle: 'ZZZZYYYY',
      };
      const taskToken = 'AAAABBBB';
      const queueUrl = '';
      const log = {
        error: jest.fn(),
      };
      const sqsSpy = jest.spyOn(sqs, 'deleteMessage');

      await new Backup().failureMessageCleanup(message, taskToken, queueUrl, sqs, stepFunctions, log);
      expect(sqsSpy).toBeCalled();
    });

    it('should send task failure message to step functions if there is a task token', async () => {
      const message = {
        ReceiptHandle: 'ZZZZYYYY',
      };
      const taskToken = 'AAAABBBB';
      const queueUrl = '';
      const log = {
        error: jest.fn(),
      };
      const stepFunctionsSpy = jest.spyOn(stepFunctions, 'sendTaskFailure');

      await new Backup().failureMessageCleanup(message, taskToken, queueUrl, sqs, stepFunctions, log);
      expect(stepFunctionsSpy).toBeCalled();
    });

    it('should log and error if there is one', async () => {
      const message = {
        ReceiptHandle: 'ZZZZYYYY',
      };
      const taskToken = 'AAAABBBB';
      const queueUrl = '';
      const log = {
        error: jest.fn(),
      };
      const error = new Error('Test error');

      const logSpy = jest.spyOn(log, 'error');

      await new Backup().failureMessageCleanup(message, taskToken, queueUrl, sqs, stepFunctions, log, error);
      expect(logSpy).toBeCalled();
    });

    it('should return false if there is no task token', async () => {
      const message = {
        ReceiptHandle: 'ZZZZYYYY',
      };
      const taskToken = null;
      const queueUrl = '';
      const log = {
        error: jest.fn(),
      };
      const result = await new Backup().failureMessageCleanup(message, taskToken, queueUrl, sqs, stepFunctions, log);
      expect(result).toBe(false);
    });
  });
});
