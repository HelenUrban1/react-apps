import moment from 'moment';
import Util from './util';

describe('Util', () => {
  describe('generateBackupPath', () => {
    it('should generate a correct path', () => {
      const databaseName = 'test-db';
      const now = moment('2017-04-22 15:01:02');
      const expected = 'test-db/test-db-2017-04-22@15-01-02.backup';
      const result = new Util().generateBackupPath(databaseName, null, now);
      expect(result).toBe(expected);
    });

    it('should generate a path when a date is not passed in', () => {
      const databaseName = 'test-db';
      const result = new Util().generateBackupPath(databaseName, null);
      expect(result).toContain('test-db');
    });
  });
});
