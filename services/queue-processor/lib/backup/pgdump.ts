import { spawn } from 'child_process';
import through2 from 'through2';
import fs from 'fs';
import Config from '../config';

export default class Pgdump {
  spawnPgDump = (pgDumpPath: string, args: string[], env: NodeJS.ProcessEnv) => {
    if (!fs.existsSync(pgDumpPath)) {
      throw new Error(`pg_dump not found at ${pgDumpPath}`);
    }

    return spawn(pgDumpPath, args, {
      env,
    });
  };

  buildArgs = (pgdumpArgs: string[]) => {
    let args = ['-Fc', '-Z1'];
    const extraArgs = pgdumpArgs;
    args = args.concat(extraArgs);

    return args;
  };

  pgdump = (config: Config, pgDumpSpawnFn = this.spawnPgDump) => {
    return new Promise((resolve, reject) => {
      let headerChecked = false;
      let stderr = '';

      // spawn pg_dump process
      const args = this.buildArgs(config.backup.pgdumpArgs);
      const env = {
        S3_REGION: config.s3Region,
        PGDUMP_PATH: config.backup.pgdumpPath,
        PGRESTORE_PATH: config.backup.pgrestorePath,
        LD_LIBRARY_PATH: config.backup.pgdumpPath,
        PGDATABASE: config.backup.pgDatabase,
        PGUSER: config.backup.pgUser,
        PGPASSWORD: config.backup.pgPassword,
        PGHOST: config.backup.pgHost,
      };
      const pgdumpProcess = pgDumpSpawnFn(config.backup.pgdumpPath, args, env);

      // hook into the process
      pgdumpProcess.stderr.on('data', (data: any) => {
        stderr += data.toString('utf8');
      });

      pgdumpProcess.on('close', (code: number) => {
        // reject our promise if pg_dump had a non-zero exit
        if (code !== 0) {
          return reject(new Error(`pg_dump process failed: ${stderr}`));
        }
        // check that pgdump actually gave us some data
        if (!headerChecked) {
          return reject(new Error('pg_dump gave us an unexpected response'));
        }
        return null;
      });

      // use through2 to proxy the pg_dump stdout stream
      // so we can check it's valid
      const buffer = through2(function bufferCheck(chunk, enc, callback) {
        this.push(chunk);
        // if stdout begins with 'PGDMP' then the backup has begun
        // otherwise, we abort
        if (!headerChecked) {
          headerChecked = true;
          if (chunk.toString('utf8').startsWith('PGDMP')) {
            resolve(buffer);
          } else {
            reject(new Error('pg_dump gave us an unexpected response'));
          }
        }
        callback();
      });

      // pipe pg_dump to buffer
      pgdumpProcess.stdout.pipe(buffer);
    });
  };
}
