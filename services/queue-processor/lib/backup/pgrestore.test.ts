import path from 'path';
import fs from 'fs';
import Pgrestore from './pgrestore';
import DefaultConfig from '../config';

const mockSpawn = require('mock-spawn');

describe('pgrestore', () => {
  it('should export a function', () => {
    return expect(typeof new Pgrestore().pgrestore).toBe('function');
  });

  it('should throw an error when the pg_restore binary does not exist', () => {
    const config = new DefaultConfig();
    config.backup.pgrestorePath = path.join('some', 'non-existent', 'path', 'pg_restore');
    const input = '';
    const p = new Pgrestore().pgrestore(config, input);
    return expect(p).rejects.toEqual(Error(`pg_restore not found at ${config.backup.pgrestorePath}`));
  });

  describe('default pg_restore binary', () => {
    const config = new DefaultConfig();
    const binaryPath = config.backup.pgrestorePath;
    it('should exist', () => {
      if (!fs.existsSync(binaryPath)) {
        throw new Error(`failed to find pg_restore at ${binaryPath}`);
      }
    });
    it('should be +x', () => {
      if (process.platform === 'win32') {
        return;
      }
      fs.access(binaryPath, fs.constants.X_OK, (err) => {
        if (err) throw new Error(`binary ${binaryPath} is not executable`);
      });
    });
  });

  it('should validate good backups', async () => {
    const pgrestoreProcess = mockSpawn()();

    const pgRestoreFn = () => pgrestoreProcess;
    const config = new DefaultConfig();
    const input = 'TOC Entries: 10 PGDMP - data - data';
    const p = new Pgrestore().pgrestore(config, input, pgRestoreFn);
    pgrestoreProcess.stdout.write('TOC Entries: 10 PGDMP - data - data');
    pgrestoreProcess.emit('close', 0);

    const buffer: any = await p;

    expect(buffer.read().toString('utf8')).toBe('TOC Entries: 10 PGDMP - data - data');
  });

  it('should warn on bad backups', async () => {
    const pgrestoreProcess = mockSpawn()();

    const pgRestoreFn = () => pgrestoreProcess;
    const config = new DefaultConfig();
    const input = 'PGDMP - data - data';
    const p = new Pgrestore().pgrestore(config, input, pgRestoreFn);
    pgrestoreProcess.stdout.write('PGDMP - data - data');
    pgrestoreProcess.emit('close', 0);

    expect(p).rejects.toEqual(Error(`pg_restore gave us an unexpected response, check the backups.`));
  });
});
