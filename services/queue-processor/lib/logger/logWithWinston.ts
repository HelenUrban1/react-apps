import * as winston from 'winston';

const rTracer = require(`cls-rtracer`);

const LEVELS = {
  error: 0,
  warn: 1,
  info: 2,
  debug: 3,
  trace: 4,
};

winston.addColors({
  error: 'bold red',
  warn: 'yellow',
  info: 'white',
  debug: 'cyan',
  trace: 'gray',
});

const TRANSPORTS: any[] = [];

function createTransport(transportConfig: any) {
  // Synchronize log levels across all transports
  const useTransportConfig = { ...transportConfig, levels: LEVELS };
  let transport;
  if (transportConfig.type === 'console') {
    transport = new winston.transports.Console(useTransportConfig);
  } else if (transportConfig.type === 'file') {
    transport = new winston.transports.File(useTransportConfig);
  } else if (transportConfig.type === 'http') {
    transport = new winston.transports.Http(useTransportConfig);
    /*
  // Add other transports here
  // https://github.com/winstonjs/winston/blob/master/docs/transports.md
  } else if (transportConfig.type == '...') {
    transport = new winston.transports.Stream({stream: fs.createWriteStream('/dev/null') // other options }
  */
  } else {
    throw new Error(`An unrecognized Winston transport was encountered during logger initialization ${useTransportConfig.type}`);
  }
  return transport;
}

function createConfig(label: string, options: any) {
  let useColors = false;
  options.transports.forEach((transportConfig: any) => {
    if (transportConfig.colorize) {
      useColors = true;
    }
  });

  let format;
  if (useColors) {
    format = winston.format.combine(
      winston.format.label({ label }),
      winston.format.colorize({
        all: true,
      }),
      winston.format.timestamp(), // .timestamp({format: 'YY-MM-DD HH:MM:SS'})
      winston.format.printf((info) => {
        const rid = info.rid || rTracer.id();
        return rid ? `[${info.label}] ${info.timestamp} [request-id:${rid}] : ${info.level} : ${info.message}` : `[${info.label}] ${info.timestamp} : ${info.level} : ${info.message}`;
      })
    );
  } else {
    format = winston.format.combine(
      winston.format.label({ label }),
      winston.format.timestamp(), // .timestamp({format: 'YY-MM-DD HH:MM:SS'})
      winston.format.printf((info) => {
        const rid = info.rid || rTracer.id();
        return rid ? `[${info.label}] ${info.timestamp} [request-id:${rid}] : ${info.level} : ${info.message}` : `[${info.label}] ${info.timestamp} : ${info.level} : ${info.message}`;
      })
    );
  }

  return {
    ...options,
    levels: LEVELS,
    transports: TRANSPORTS,
    format,
  };
}

function createLogger(label: string, options: any) {
  const winstonLoggerConfig = createConfig(label, options);
  const loggerInstance = winston.loggers.add(label, winstonLoggerConfig);
  loggerInstance.info(`logger.logWithWinston.createLogger ${label} finished`);
  return loggerInstance;
}

const initLogging = (options: any) => {
  if (options.transports) {
    options.transports.forEach((transportConfig: any) => {
      TRANSPORTS.push(createTransport(transportConfig));
    });
  }
};

export { initLogging, createLogger, createConfig };
