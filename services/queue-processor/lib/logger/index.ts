import DefaultConfig from '../config';

const { loggers } = require('winston');
const { initLogging, createLogger } = require('./logWithWinston');

const getCurrentLoggingConfig = (configuration: DefaultConfig) => {
  if (configuration.env === 'production') {
    return configuration.productionLogging;
  }
  if (configuration.env === 'test') {
    return configuration.testLogging;
  }
  return configuration.developmentLogging;
};

const config = new DefaultConfig();
const loggingConfig = getCurrentLoggingConfig(config);
initLogging(loggingConfig);
createLogger('Q-PROC', loggingConfig);

// Export the logger
const log = loggers.get('Q-PROC');

/*
log.trace('trace message');
log.debug('debug message');
log.info('info message');
log.warn('warn message');
log.error('error message');
*/

export default log;
