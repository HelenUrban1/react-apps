import Backup from './backup';
import DefaultConfig from './config';
import log from './logger';
import 'newrelic';

const { sqs, s3, stepFunctions, secretsManager } = require('aws-wrapper');
// Used to debug locally against live AWS, see comment in poll function
// let sqs: any;
// let s3: any;
// let stepFunctions: any;
// let secretsManager: any;

process.on('unhandledRejection', (reason: any) => {
  log.error('unhandledRejection', reason);
});

process.on('uncaughtException', (error: Error) => {
  log.error('uncaughtException', error);
});

const config = new DefaultConfig();

const queueUrl = config.queue.sqsUrl;
const { pollingTimeout } = config.queue; // time between receiveMessage calls

const backup = new Backup();

const poll = async () => {
  try {
    /**
     * To debug locally against live AWS, comment out the require from aws-wrapper above and uncomment the block of variables below it and
     * the block below this comment, filling in your AWS IAM username, security account Id, ARN of the role to assume and a current MFA code.
     */
    // const { assumeRole } = require('aws-wrapper');
    // const username = '';
    // const sourceAccountId = '';
    // const roleArn = '';
    // const tokenCode = '';
    // if (!sqs) {
    //   const ar = await assumeRole(username, sourceAccountId, roleArn, tokenCode);
    //   sqs = ar.sqs;
    //   s3 = ar.s3;
    //   stepFunctions = ar.stepFunctions;
    //   secretsManager = ar.secretsManager;
    // }

    if (sqs && s3 && stepFunctions && secretsManager) {
      log.info(`Checking for messages from ${queueUrl}...`);
      const receiveMessageRequest = {
        QueueUrl: queueUrl,
        MessageAttributeNames: ['All'],
        AttributeNames: ['All'],
        VisibilityTimeout: 20,
        WaitTimeSeconds: 20,
        MaxNumberOfMessages: 10,
      };
      const data = await sqs.receiveMessage(receiveMessageRequest);
      log.info('Success', data);
      if (data.Messages && data.Messages.length > 0) {
        const processes = [];
        for (let index = 0; index < data.Messages.length; index += 1) {
          const message = data.Messages[index];
          const messageBody = JSON.parse(message.Body);
          const taskToken = messageBody.TaskToken;
          if (message.MessageAttributes?.method.StringValue === 'backup-full') {
            processes.push(
              backup
                .full(message.MessageAttributes?.accountId?.StringValue, message.MessageAttributes?.requestId?.StringValue, s3, secretsManager)
                .then(async (result) => {
                  if (result) {
                    return new Backup().successMessageCleanup(message, taskToken, queueUrl, sqs, stepFunctions);
                  }
                  if (taskToken) {
                    return new Backup().failureMessageCleanup(message, taskToken, queueUrl, sqs, stepFunctions, log);
                  }
                  // If not successful, there is no task token and an exception was not thrown,
                  // just resolve and the message will show back up in the queue to be tried again.
                  return Promise.resolve();
                })
                .catch(async (error) => {
                  return new Backup().failureMessageCleanup(message, taskToken, queueUrl, sqs, stepFunctions, log, error);
                })
            );
          }
        }
        await Promise.all(processes);
      } else {
        log.info('No messages');
      }
    }
  } catch (error) {
    log.error('Error', error);
  }

  setTimeout(poll, pollingTimeout);
};

poll();

export { poll as default };
