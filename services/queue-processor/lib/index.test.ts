import poll from './index';

jest.mock('./index', () => {
  return jest.fn().mockRejectedValue(new Error('mock error'));
});

const mockCall = jest.fn();
process.on('unhandledRejection', mockCall);

describe('index', () => {
  it('Log unhandledRejection', async () => {
    // process.on('unhandledRejection', (err) => fail(err));
    expect(poll()).rejects.toEqual(new Error('mock error'));
    // await poll();
    // expect(mockCall).toBeCalledTimes(1);
  });
});
