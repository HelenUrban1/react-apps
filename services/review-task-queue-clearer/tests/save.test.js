const axios = require('axios').default;

const updateReviewTasks = require('../src/save');

jest.mock('axios');

describe('updateReviewTasks', () => {
  it('should return count of updated items', async () => {
    const response = {
      data: {
        data: {
          authSignIn: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6ImUwYTAxNzc5LWM4ZWQtNDA5MC1iZDQ0LTljMWIyMjQyZmI3NSIsImFjY291bnRJZCI6ImUwYTAxNzY5LWM4ZWQtNDA5MC1iZDI0LTljMWIyMjQxZmI3NSIsInNpdGVJZCI6IjMxZWYxZDU2LTFiODYtNGZmYS1iODg2LTUwN2VkOGYwMjFjNSIsImlhdCI6MTU5OTA4MDM0NH0.a_ZWj5lKUzzaWoLzQ6nxPh3Bn-DUtzHSEsTy8yb1x5E',
          reviewTaskClearQueue: 2,
        },
      },
    };

    axios.post.mockResolvedValue(response);

    const result = await updateReviewTasks();
    expect(result).toBe(2);
  });
});
