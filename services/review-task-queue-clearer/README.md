# Review Task Queue Clearer

An AWS Lambda function that can be triggered manually or with a cloudwatch event .

## Setup & Testing Locally

1 - Add a `.env` file to root folder. Populate based on the following:

| Variable                    | Required? | Example Value                           | Notes                                                     |
| --------------------------- | :-------: | --------------------------------------- | --------------------------------------------------------- |
| `NODE_ENV`                  |     Y     | "development"                           | Sets the environment we are in. Default value: production |
| `LOG_LEVEL`                 |     N     | "debug"                                 | dev=debug, test=error, prod=info                          |
| `IXC_NEW_RELIC_LICENSE_KEY` |     Y     | 40 character alpha numeric uuid         | License key for New Relic.                                |
| `IXC_AWS_REGION`            |     Y     | "us-east-1"                             | Default region                                            |
| `IXC_BACKEND_URL`           |     Y     | "https://www.ixc-dev.com/api"           | Backend API endpoint                                      |
| `IXC_REVIEW_EMAIL`          |     Y     | "ops+ixc.reviewers@inspectionxpert.com" | Email of review admin user                                |
| `IXC_REVIEW_PASSWORD`       |     Y     | string                                  | Password of review admin user                             |
| `SONAR_USER_TOKEN`          |     N     | 40 character alpha numeric uuid         | Used to run analyses on code or to invoke web services    |

2 - Run the following commands:

```
cd services/review-task-queue-clearer
npm install
npm test
```

## Manual Deploy

### Windows Users

Installed Gnu on Windows https://github.com/bmatzelle/gow/wiki or the zip commands in the npm scripts will fail.

### All Users

1. Verify that AWS lambda function has been setup in infrastructure.

   - Lambda runtime: Node.js 18.x
   - Verify ENV variables are setup according to this README in lambda (or populated in another way)
   - Ensure your AWS config and credentials allow you to auth to the ix-dev environment

2. Build and zip the function ready to deploy to AWS.
   npm run bundle:all

3. Log in to the AWS Web UI for Lambda, select the appropriate Lambda and upload the newly created function.zip file.
```

## Future Enhancements

### Structured Logging

Currently the lambda is logging to console. Support for LOG_LEVEL and JSON structured logs should be added before deployment to production.

See: https://theburningmonk.com/2017/08/centralised-logging-for-aws-lambda/

### Lambda Layers

Best practice is to lock the aws-sdk version using lambda layers:

```
# Setup dependencies that will go into the layer
mkdir lambda-layer
cd lambda-layer/
npm install aws-sdk

# Create a zip file of just the dependencies
mkdir nodejs
mv node_modules/ nodejs/
zip -r aws-sdk.zip nodejs/

# Upload the layer
aws lambda publish-layer-version --layer-name aws-sdk --zip-file fileb://aws-sdk.zip --compatible-runtimes nodejs12.x --description "My AWS SDK"
```
