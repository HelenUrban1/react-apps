// require('dotenv').config();

let logLevel = process.env.LOG_LEVEL || 'debug';
let newRelicConfig = 'Dev';
if (process.env.NODE_ENV === 'production') {
  logLevel = 'debug';
  newRelicConfig = 'Prod';
} else if (process.env.NODE_ENV === 'test') {
  logLevel = 'error';
  newRelicConfig = 'Test';
}

module.exports = {
  env: process.env.NODE_ENV,
  new_relic: {
    license_key: process.env.IXC_NEW_RELIC_LICENSE_KEY,
    labels: `Config:${newRelicConfig},Tier:Ixc_Review_Task_Queue_Clearer`,
  },
  logLevel,
  awsRegion: process.env.IXC_AWS_REGION || 'us-east-1',
  backendUrl: process.env.IXC_BACKEND_URL || 'https://www.ixc-dev.com/api',
  reviewEmail: process.env.IXC_REVIEW_EMAIL || 'ops+ixc.reviewers@inspectionxpert.com',
  reviewPassword: process.env.IXC_REVIEW_PASSWORD || 'test',
};
