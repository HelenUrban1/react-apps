const axios = require('axios').default;
const util = require('util');
const config = require('./config');

// Authenticates the admin user and retrieves active accounts from the backend API
const updateReviewTasks = async () => {
  axios.defaults.withCredentials = true;

  const headers = {
    'Content-Type': 'application/json; charset=utf-8',
  };

  // Log in as the admin user to get a bearer token
  const authRequestData = {
    query: 'mutation AUTH_SIGN_IN($email: String!, $password: String!) { authSignIn(email: $email, password: $password) }',
    variables: {
      email: config.reviewEmail,
      password: config.reviewPassword,
    },
  };

  const authResponse = await axios.post(config.backendUrl, JSON.stringify(authRequestData), { headers });
  console.log('authResponse', authResponse);
  if (!authResponse || !authResponse.data || !authResponse.data.data || !authResponse.data.data.authSignIn) {
    throw Error(`Could not authenticate review account ${config.reviewEmail} against service URL: ${config.backendUrl}`);
  }
  const authToken = authResponse.data.data.authSignIn;

  // Add auth cookie to the header of the request
  headers.Cookie = `jwt=${authToken};`;

  // Add bearer token to the header of the request
  headers.Authorization = `Bearer ${authToken}`;

  const requestData = {
    query: `mutation REVIEW_TASK_CLEAR_QUEUE {
        reviewTaskClearQueue
      }`,
  };
  const response = await axios.post(config.backendUrl, JSON.stringify(requestData), { headers });

  if (!response || !response.data || !response.data.data || !(response.data.data.reviewTaskClearQueue !== undefined)) {
    if (response && response.data && response.data.errors) {
      let errors = '';
      for (let index = 0; index < response.data.errors.length; index += 1) {
        const error = response.data.errors[index];
        errors += `${error.message}\n`;
      }
      throw Error(`Error clearing review task queue: ${errors}`);
    }
    throw Error(`Unknown response: ${util.inspect(response)}`);
  }
  return response.data.data.reviewTaskClearQueue;
};

module.exports = updateReviewTasks;
