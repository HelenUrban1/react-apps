// eslint-disable-next-line prefer-const
let updateReviewTasks = require('./save');

const config = require('./config')();

const handler = async () => {
  try {
    await updateReviewTasks(config);
  } catch (error) {
    // log the error and rethrow for Lambda
    if (process.env.NODE_ENV !== 'test') {
      console.error(error);
    }
    throw error;
  }
};

module.exports = handler;
