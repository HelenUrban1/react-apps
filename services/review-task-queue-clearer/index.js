const newrelic = require('newrelic');
require('@newrelic/aws-sdk');

const handler = require('./src/handler');

module.exports.handler = newrelic.setLambdaHandler(handler);
