/* eslint-disable */
const sonarqubeScanner = require('sonarqube-scanner');

sonarqubeScanner(
  {
    serverUrl: 'https://sonarcloud.io',
    token: process.env.SONAR_USER_TOKEN,
    options: {
      // 'sonar.log.level': 'TRACE',
      // 'sonar.verbose': 'true',
    },
  },
  () => {}
);
