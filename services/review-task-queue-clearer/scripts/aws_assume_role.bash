#!/usr/bin/env bash

set -euo pipefail

###
# This file assumes a role and exports the credentials.
# 
# You would generally run the script like:
#   eval $(./aws_assume_role.bash dev ix-deploy-static-website)
#
# It needs two args:
# 1. aws account alias: dev, stage, or prod
# 2. role name to be assumed
#
# It prints out the credential environment variables which is why it's ideal to
# `eval` this.
###

script_name="$(basename $0)"

# shellcheck disable=SC2034
aws_account_id_dev="541855766228"
# shellcheck disable=SC2034
aws_account_id_qa="088527940899"
# shellcheck disable=SC2034
aws_account_id_stage="183375957479"
# shellcheck disable=SC2034
aws_account_id_prod="648538181042"

if [[ -z "${1:-}" || -z "${2:-}" ]]; then
    echo "Must pass in an account alias name and a role name:"
    printf "\tex. ./%s dev ix-deploy-static-website\n" "${script_name}"
    exit 1
fi

aws_alias_name="${1}"
aws_role_name="${2}"

function account_id_from_alias() {
    local alias="${1}" # ex: dev, stage, or prod
    account_id_var="aws_account_id_${alias}"
    account_id="${!account_id_var}"
    if [[ -z ${account_id:-} ]]; then
        echo "Invalid account alias provided."
        exit 1
    fi
    echo "${account_id}"
}

aws_account_id=$(account_id_from_alias "${aws_alias_name}")

credentials=$(aws sts assume-role \
    --role-arn "arn:aws:iam::${aws_account_id}:role/${aws_role_name}" \
    --role-session-name "CodeBuild-${script_name}-$(date +%s)")

# shellcheck disable=SC2046
echo AWS_ACCESS_KEY_ID=$(jq -r '.Credentials.AccessKeyId' <<< "${credentials}")
# shellcheck disable=SC2046
echo AWS_SECRET_ACCESS_KEY=$(jq -r '.Credentials.SecretAccessKey' <<< "${credentials}")
# shellcheck disable=SC2046
echo AWS_SESSION_TOKEN=$(jq -r '.Credentials.SessionToken' <<< "${credentials}")
