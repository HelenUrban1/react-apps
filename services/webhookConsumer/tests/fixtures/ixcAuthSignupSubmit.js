const AUTH_SIGNUP_SUBMIT_POST = {
  operationName: 'AUTH_SIGNUP_SUBMIT',
  query: 'mutation AUTH_SIGNUP_SUBMIT($email: String!, $firstName: String) {authSignUpSubmit(email: $email, firstName: $firstName)}',
  variables: {
    email: 'dev.mail.monkey@gmail.com',
    firstName: 'Dev',
  },
};

const AUTH_SIGNUP_SUBMIT_RESPONSE = { data: { authSignUpSubmit: true } };

module.exports = {
  AUTH_SIGNUP_SUBMIT_POST, //
  AUTH_SIGNUP_SUBMIT_RESPONSE,
};
