const axios = require('axios');
const { AUTH_SIGNUP_SUBMIT_RESPONSE } = require('./fixtures/ixcAuthSignupSubmit');
const { makeWebhookConsumer } = require('../src/webhookConsumerFactory');

// Prepare the mock webhook event
jest.mock('axios');

const hubspotSignupSqsMessageBatch = require('./fixtures/hubspot/hubspotSignupSqsMessageBatch.json');
const sendgridSqsMessageBatch = require('./fixtures/sendgrid/sendgridSqsMessageBatch.json');

describe('webhookConsumer', () => {
  const testConfig = {
    env: 'test',
    baseUrl: 'https://alb.ixc-dev.com',
    awsRegion: 'us-east-1',
    ixc: {
      backendUrl: 'http://ixc-local.com:8080/api',
    },
  };

  const loggerMock = {
    trace: jest.fn(),
    debug: jest.fn(),
    log: jest.fn(),
    info: jest.fn(),
    warn: jest.fn(),
    error: jest.fn(),
  };
  const makeLogger = () => loggerMock;

  const webhookConsumer = makeWebhookConsumer({
    config: testConfig,
    makeLogger,
  });

  beforeEach(() => {
    jest.resetAllMocks();
  });

  it('should return 422 (Unprocessable Entity) when the request can not be matched with a handler', async () => {
    const { message } = JSON.parse(JSON.stringify(hubspotSignupSqsMessageBatch));
    message.Records[0].messageAttributes.webhookSource.stringValue = 'Some unsupported event source';
    const response = await webhookConsumer(message);
    expect(response.statusCode).toBe(422);
    expect(response.body.message).toBe('No handler matched the event type Some unsupported event source messageId: 2c585205-fdc2-44a0-acd3-d25a7da97c1e');
    expect(loggerMock.error).toHaveBeenCalledTimes(0);
  });

  describe('Hubspot Event Handlers', () => {
    describe('#signup', () => {
      it('should return 200 when processing succeeds', async () => {
        axios.post = jest.fn().mockResolvedValueOnce(AUTH_SIGNUP_SUBMIT_RESPONSE);
        const { message } = JSON.parse(JSON.stringify(hubspotSignupSqsMessageBatch));
        const response = await webhookConsumer(message);
        expect(response.statusCode).toBe(200);
        expect(loggerMock.error).toHaveBeenCalledTimes(0);
      });

      it('should return 500 when processing fails', async () => {
        axios.post = jest.fn().mockRejectedValueOnce(new Error('HTTP Post to IXC FAILED!!!'));
        const { message } = JSON.parse(JSON.stringify(hubspotSignupSqsMessageBatch));
        const response = await webhookConsumer(message);
        expect(response.statusCode).toBe(500);
        expect(response.body.message).toBe('Error encountered while processing messageId: 2c585205-fdc2-44a0-acd3-d25a7da97c1e : Error : HTTP Post to IXC FAILED!!!');
        expect(loggerMock.error).toHaveBeenCalledTimes(1);
      });
    });
  });

  describe('External Event Handlers', () => {
    describe('#deliverExternalEvent', () => {
      it('should return 200 when processing succeeds', async () => {
        axios.post = jest.fn().mockResolvedValueOnce({});
        const { message } = JSON.parse(JSON.stringify(sendgridSqsMessageBatch));
        const response = await webhookConsumer(message);
        expect(response.statusCode).toBe(200);
        expect(loggerMock.error).toHaveBeenCalledTimes(0);
      });

      it('should return 500 when processing fails', async () => {
        axios.post = jest.fn().mockRejectedValueOnce(new Error('HTTP Post to IXC FAILED!!!'));
        const { message } = JSON.parse(JSON.stringify(sendgridSqsMessageBatch));
        const response = await webhookConsumer(message);
        expect(response.statusCode).toBe(500);
        expect(response.body.message).toBe('Error encountered while processing messageId: 8919775c-20cd-4d56-a70f-55536131cbe2 : Error : HTTP Post to IXC FAILED!!!');
        expect(loggerMock.error).toHaveBeenCalledTimes(1);
      });
    });
  });
});
