/* eslint-disable */
const sonarqubeScanner = require('sonarqube-scanner');
const { env } = require('./env.js');

sonarqubeScanner(
  {
    serverUrl: 'https://sonarcloud.io',
    token: env.SONAR_USER_TOKEN,
    options: {
      // 'sonar.log.level': 'TRACE',
      // 'sonar.verbose': 'true',
    },
  },
  () => {}
);
