const z = require('zod');

function createEnv(opts) {
  const runtimeEnv = opts.runtimeEnvStrict || opts.runtimeEnv || env;
  const skip = !!opts.skipValidation;

  if (skip) return runtimeEnv;

  const _client = typeof opts.client === 'object' ? opts.client : {};
  const _server = typeof opts.server === 'object' ? opts.server : {};
  const _shared = typeof opts.shared === 'object' ? opts.shared : {};

  const client = z.object(_client);
  const server = z.object(_server);
  const shared = z.object(_shared);

  const isServer = opts.isServer || typeof window === 'undefined';
  const allClient = client.merge(shared);
  const allServer = server.merge(shared).merge(client);

  const parsed = isServer ? allServer.safeParse(runtimeEnv) : allClient.safeParse(runtimeEnv);

  const onValidationError =
    opts.onValidationError ||
    ((error) => {
      console.error('❌ Invalid environment variables:', error.flatten().fieldErrors);
      throw new Error(`❌ Invalid environment variables: ${JSON.stringify(error.flatten().fieldErrors)}`);
    });

  const onInvalidAccess =
    opts.onInvalidAccess ||
    ((_variable) => {
      throw new Error('❌ Attempted to access a server-side environment variable on the client');
    });

  if (parsed.success === false) {
    return onValidationError(parsed.error);
  }

  const env = new Proxy(parsed.data, {
    get(target, prop) {
      if (typeof prop !== 'string' || prop === '__esModule') return undefined;
      if (!isServer && opts.clientPrefix && !prop.startsWith(opts.clientPrefix) && shared.shape[prop] === undefined) {
        return onInvalidAccess(prop);
      }
      return target[prop];
    },
  });

  return env;
}
module.exports = { createEnv };
