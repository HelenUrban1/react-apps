const axios = require('axios').default;

axios.defaults.withCredentials = true;

const signup = async (params = {}) => {
  // Unpack config and data object
  const { logger, config, event } = params;
  // Unpack values from Hubspot record
  // const { body, attributes } = data;
  const { data } = event;
  // logger.debug(body);
  const email = data.properties.email.versions[0].value;
  const firstName = data.properties.firstname.versions[0].value;

  // TODO: If the company already exists in HS we could extract additional details from the record
  // const orgName = body.properties.hs_email_domain.versions[0].value;

  // Post to graphql endpoint
  const headers = {
    'Content-Type': 'application/json; charset=utf-8',
  };
  // Log in as the admin user to get a bearer token
  const authSignUpSubmitData = {
    operationName: 'AUTH_SIGNUP_SUBMIT',
    query: 'mutation AUTH_SIGNUP_SUBMIT($email: String!, $firstName: String) {authSignUpSubmit(email: $email, firstName: $firstName)}',
    variables: {
      email,
      firstName,
      // orgName,
    },
  };

  logger.debug(`ixcApi.signup.authSignUpSubmitData`, authSignUpSubmitData);
  const regDot = /\./gi;
  const regAt = /@/gi;
  const regPlus = /\+/gi;

  const cognitoUsername = email.replace(regAt, '__at__').replace(regDot, '__dot__').replace(regPlus, '__plus__');

  const usingCognito = config.ixc.authStrategy && config.ixc.authStrategy === 'cognito';

  const response = usingCognito ? await axios.post(`${config.ixc.backendUrl}/cognitoSignup`, { email, firstName, username: cognitoUsername }, { headers }) : await axios.post(config.ixc.backendUrl, JSON.stringify(authSignUpSubmitData), { headers });

  // Handle success
  const graphqlData = response.data.authSignUpSubmit;
  logger.debug('Success graphqlData: ', graphqlData);
  return {
    statusCode: 200,
    body: JSON.stringify({
      message: `Successfully submited Hubspot Form signup for ${email} data to Graphql endpoint ${config.ixc.backendUrl}`,
    }),
    headers: {
      'Access-Control-Allow-Origin': '*',
    },
  };
};

const deliverExternalEvent = async (params = {}) => {
  // Unpack config and data object
  const { logger, config, event } = params;

  logger.debug(`ixcApi.deliverExternalEvent.post.event to ${config.ixc.backendUrl}/event`, event);
  const response = await axios.post(`${config.ixc.backendUrl}/event`, event);
  logger.debug(`ixcApi.deliverExternalEvent.response`, response);

  // Handle success
  let message = `External event post response: ${response.status}`;
  if (response.data && response.data.message) message = `${message} ${response.data.message}`;

  return {
    statusCode: 200,
    body: JSON.stringify({
      message,
    }),
    headers: {
      'Access-Control-Allow-Origin': '*',
    },
  };
};

module.exports = {
  signup,
  deliverExternalEvent,
};
