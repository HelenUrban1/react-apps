const { createEnv } = require('./createEnvSchema');
const { z } = require('zod');
const env = createEnv({
  server: {
    // Required variables
    NODE_ENV: z.string().min(1),
    IXC_NEW_RELIC_LICENSE_KEY: z.string().min(1),
    IXC_AWS_REGION: z.string().min(1),
    IXC_BACKEND_URL: z.string().min(1),
    IXC_AUTH_STRATEGY: z.string().min(1),

    // Optional variable
    LOG_LEVEL: z.string().min(1).optional(),
    SONAR_USER_TOKEN: z.string().min(1).optional(),
  },
  runtimeEnv: {
    // Required variables
    NODE_ENV: process.env.NODE_ENV,
    IXC_NEW_RELIC_LICENSE_KEY: process.env.IXC_NEW_RELIC_LICENSE_KEY,
    IXC_AWS_REGION: process.env.IXC_AWS_REGION,
    IXC_BACKEND_URL: process.env.IXC_BACKEND_URL,
    IXC_AUTH_STRATEGY: process.env.IXC_AUTH_STRATEGY,

    // Optional variables
    LOG_LEVEL: process.env.LOG_LEVEL,
    SONAR_USER_TOKEN: process.env.SONAR_USER_TOKEN,
  },
});
module.exports = { env };
