const winston = require('winston');

// const LEVEL = Symbol.for('level');
// const MESSAGE = Symbol.for('message');

function makeLogger(params) {
  const { config, label = 'ROOT' } = params;

  const logger = winston.createLogger({
    exitOnError: false,
    level: config.logLevel || 'debug',
    format: winston.format.combine(
      winston.format.label({ label }),
      winston.format.timestamp(), // .timestamp({format: 'YY-MM-DD HH:MM:SS'})
      winston.format.json()
    ),
    transports: [
      new winston.transports.Console({ handleExceptions: true }),

      // It may be necessary to force the use of console.log so that requestId isn't lost
      // new winston.transports.Console({
      // https://stackoverflow.com/questions/56562442/aws-lambda-using-winston-logging-loses-request-id
      // log(info, callback) {
      //   setImmediate(() => this.emit('logged', info));
      //   if (this.stderrLevels[info[LEVEL]]) {
      //     console.error(info[MESSAGE]);
      //     if (callback) callback();
      //     return;
      //   }
      //   console.log(info[MESSAGE]);
      //   if (callback) callback();
      // },
      // }),
    ],
  });
  return logger;
}

module.exports.makeLogger = makeLogger;
