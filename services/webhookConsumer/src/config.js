const { env } = require('./env.js');

let logLevel = env.LOG_LEVEL || 'debug';
let newRelicConfig = 'Dev';
if (env.NODE_ENV === 'production') {
  logLevel = 'debug';
  newRelicConfig = 'Prod';
} else if (env.NODE_ENV === 'test') {
  logLevel = 'error';
  newRelicConfig = 'Test';
}

module.exports = {
  env: env.NODE_ENV,
  new_relic: {
    license_key: env.IXC_NEW_RELIC_LICENSE_KEY,
    labels: `Config:${newRelicConfig},Tier:Ixc_Webhook_Consumer`,
  },
  logLevel,
  awsRegion: env.IXC_AWS_REGION,
  ixc: {
    backendUrl: env.IXC_BACKEND_URL,
    authStrategy: env.IXC_AUTH_STRATEGY,
  },
};
