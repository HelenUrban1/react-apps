const ixcApi = require('./services/ixcApi');

function httpResponse(status = 200, message = 'Message not set', data = {}) {
  return {
    statusCode: status,
    // body: JSON.stringify({ ...data, message }),
    body: { ...data, message },
    headers: {
      'Access-Control-Allow-Origin': '*',
    },
  };
}

/**
 * WebhookConsumer Factory Method
 */
function makeWebhookConsumer(params) {
  // Unpack the config and init the default logger
  const { config, makeLogger } = params;
  let logger = makeLogger({ config, label: 'makeFunction' });
  logger.debug('makeWebhookConsumer.config', config);

  /**
   * Return a function that can be used to validate Hubspot webhook requests
   *
   * @param  {Object} message - SQS message object from SQS queue
   * @param  {Object} context - Context object from Lambda call
   * @returns  {Object} Http response object containing statusCode and body
   */
  return async function webhookConsumer(messagePack) {
    // Prepare to track request
    const start = Date.now();

    // TODO: Allow processing of multiple message
    logger.debug('webhookConsumer.messagePack', messagePack);
    const message = messagePack.Records[0];

    // To simplify log tracing create a logger with the messageId as the label
    logger = makeLogger({ config, label: `messageId:${message.messageId}` });
    logger.info(`Start processing message.messageId : ${message.messageId}`);

    // Process the message
    let body;
    try {
      body = JSON.parse(message.body);
      logger.debug('webhookConsumer.message.body', body);
    } catch (error) {
      logger.error('webhookConsumer was unable to parse message.body', error);
      throw error;
    }

    // If there are message attributes, unpack them into a flattened js object
    logger.debug('webhookConsumer.message.messageAttributes');
    const attributes = {};
    Object.keys(message.messageAttributes).forEach((attribute) => {
      if (message.messageAttributes[attribute].dataType === 'String') {
        attributes[attribute] = message.messageAttributes[attribute].stringValue;
      } else if (message.messageAttributes[attribute].dataType === 'Number') {
        attributes[attribute] = +message.messageAttributes[attribute].stringValue;
      } else if (message.messageAttributes[attribute].dataType === 'Boolean') {
        attributes[attribute] = message.messageAttributes[attribute].stringValue[0].toLowerCase() === 't';
      }
    });
    logger.debug('webhookConsumer.attributes', attributes);

    // Recreate the logger with the awsRequestId
    logger = makeLogger({ config, label: `messageId:${message.messageId}->awsRequestId:${attributes.awsRequestId}` });

    // Construct the error handler
    const done = (error) => {
      logger.debug('done called');
      // TODO: Push ProcessingTime metrics to CloudWatch, against the queueName
      const processingTime = Date.now() - start;
      // TODO: Send error to error tracking service like Amplitude, new Relic, etc
      if (error) {
        logger.error(error);
        // TODO: If Error message is not a valid body, it shouldn’t be moved to a Dead Queue.
        // TODO: 5xx and 4xx error should be differentiated. 4xx shouldn’t be retried.
        return httpResponse(500, `Error encountered while processing messageId: ${message.messageId} : ${error.name} : ${error.message}`, { processingTime, error });
      }
      return httpResponse(200, `Processing succeeded for messageId: ${message.messageId} : `, { processingTime });
    };

    // Call the service that performs the actual task
    let result;
    if (attributes.webhookSource === 'hubspot') {
      logger.info(`Handle hubspot signup`);
      result = await ixcApi
        .signup({
          config,
          logger,
          event: { data: body, attributes },
        })
        .then(() => done())
        .catch(done);
      logger.info(`hubspot signup result`, result);
    } else if (attributes.webhookSource === 'sendgrid') {
      logger.info(`Handle SendGrid event`);
      // Send Sendgrid event to ixc-backend
      result = await ixcApi
        .deliverExternalEvent({
          config,
          logger,
          event: { data: body, attributes },
        })
        .then(() => done())
        .catch(done);
      logger.info(`SendGrid result`, result);
    } else {
      // TODO: Fire an analytics event from here to Amplitude
      // 422 Unprocessable Entity (WebDAV)
      // This could also be "501 : Not Implemented", but that may leave the event on the queue and result in re-attempts
      result = httpResponse(422, `No handler matched the event type ${attributes.webhookSource} messageId: ${message.messageId}`);
    }
    logger.info(`Handling of messageId: ${message.messageId} complete.`, result);
    return result;
  };
}

module.exports.makeWebhookConsumer = makeWebhookConsumer;
