// const AWS = require('aws-sdk');
const { makeWebhookConsumer } = require('./src/webhookConsumerFactory');
const config = require('./src/config');
const { makeLogger } = require('./src/logger');

// Configre real AWS
// AWS.config.update({ region: config.awsRegion });

// Build the consumer function
const webhookConsumer = makeWebhookConsumer({
  config,
  makeLogger,
});

// Bind the consumer function to the lambda handler
module.exports.handler = webhookConsumer;
