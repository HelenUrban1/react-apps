# webhookConsumer

An AWS Lambda function that consumes and processes events from an SQS queue that were pushed to it by the webhookListener.

## Setup & Testing Locally

1 - Add a `.env` file to root folder. Populate based on the following:

| Variable                    | Required? | Example Value                   | Notes                                                     |
| --------------------------- | :-------: | ------------------------------- | --------------------------------------------------------- |
| `NODE_ENV`                  |     Y     | "development"                   | Sets the environment we are in. Default value: production |
| `LOG_LEVEL`                 |     N     | "debug"                         | dev=debug, test=error, prod=info                          |
| `IXC_NEW_RELIC_LICENSE_KEY` |     Y     | 40 character alpha numeric uuid | License key for New Relic.                                |
| `IXC_AWS_REGION`            |     Y     | "us-east-1"                     | Default region                                            |
| `IXC_BACKEND_URL`           |     Y     | "https://www.ixc-dev.com/api"   | Backend API endpoint                                      |
| `SONAR_USER_TOKEN`          |     N     | 40 character alpha numeric uuid | Used to run analyses on code or to invoke web services    |
| `IXC_AUTH_STRATEGY`         |     Y     | cognito                         | Notifies the current auth strategy (cognito/standard)     |

2 - Run the following commands:

```
cd services/webhookConsumer
npm install
npm test
```

### Manual Verification

Lambda in ixc-dev.com
https://console.aws.amazon.com/lambda/home?region=us-east-1#/functions/webhook-listener?tab=configure

TBD

### Verify Events in SQS

Go to [webhooks SQS queue|https://console.aws.amazon.com/sqs/v2/home?region=us-east-1#/queues/https%3A%2F%2Fsqs.us-east-1.amazonaws.com%2F541855766228%2Fincoming-webhooks.fifo/send-receive]

- Click "Send and receive messages"
- Scroll to bottom "Poll for messages"
- Verify that events from both Hubspot and Chargify are listed and that their bodies were passed through as JSON

## Manual Deploy

### Windows Users

Installed Gnu on Windows https://github.com/bmatzelle/gow/wiki or the zip commands in the npm scripts will fail.

### All Users

1. Verify that AWS lambda function and SQS queus have been setup in infrastructure.

   - Lambda runtime: Node.js 18.x
   - SQS Queue type: FIFO
   - Verify ENV variables are setup according to this README in lambda (or populated in another way)
   - Ensure your AWS config and credentials allow you to auth to the ix-dev environment

2. Build and zip the function ready to deploy to AWS.
   npm run bundle:all

3. Log in to the AWS Web UI for Lambda, select the appropriate Lambda and upload the newly created function.zip file.
