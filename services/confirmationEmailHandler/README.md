# backup-trigger

An AWS Lambda function that gets triggered by a CloudWatch event, checks for accounts in the primary database and creates SQS messages for the queue processor to run backups.

## Testing Locally

1 - Add a `.env` file to root folder. Populate based on the following:

| Variable                       | Required? | Example Value                   | Notes                                                                  |
| ------------------------------ | :-------: | ------------------------------- | ---------------------------------------------------------------------- |
| `NODE_ENV`                     |     Y     | "development"                   | Sets the environment we are in                                         |
| `IXC_AWS_REGION`               |     Y     | "us-east-1"                     | Default region                                                         |
| `IXC_BACKEND_URL`              |     Y     | "http://ixc-local.com:8080/api" | Backend API endpoint                                                   |
| `IXC_MAIL_SENDGRID_API_KEY`    |     Y     | "apikey"                        | Sendgrid API Key                                                       |
| `IXC_MAIL_FROM_ADDRESS`        |     Y     | "support@inspectionxpert.com"   | The address the email will be sent from                                |
| `COGNITO_EMAILER_KEY_ARN`      |     Y     | string                          | Amazon Resource Name (ARN) of the KMS key used to encrypt users' codes |
| `IXC_MAIL_SENDING_DOMAIN`      |     Y     | https://ixc-local.com           | URL directed to when sending email                                     |
| `IXC_MAIL_SENDING_DOMAIN_PORT` |     N     | 3000                            | Port being used if running on local instance                           |

2 - Build supporting sub-packages

```
cd packages/aws-wrapper
npm install
npm run build
```

3 - Run the following commands:

```
cd services/confirmationEmailHandler
npm install
npm test
```

4 - First deploy, or whenever npm packages are updated

```
npm run deploy:all
```

5. When only source is updated:

```
npm run deploy
```

## Manual Deploy

1. Create an AWS lambda function:

   - Author from scratch
   - Runtime: Node.js 12.x

2. Configuration -> Function code:

   - Code Entry Type: Upload a .zip file
   - Save

3. Add a Cognito event trigger
