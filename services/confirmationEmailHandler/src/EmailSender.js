const { v4: uuidv4 } = require('uuid');
const nodemailerSendgrid = require('nodemailer-sendgrid');
const validate = require('./clients/validate');
const nodemailer = require('nodemailer');
const config = require('./config')();

/**
 * Handles Email sending
 */
module.exports = class EmailSender {
  constructor(email) {
    // Validate transport config
    if (!EmailSender.isConfigured) {
      return false;
    }

    // Sendgrid nodemailer transport
    this.transporter = nodemailer.createTransport(
      nodemailerSendgrid({
        apiKey: config.email.sendgrid_api_key, // process.env.SENDGRID_API_KEY
      })
    );

    this.email = email;
  }

  static get isConfigured() {
    // return !!config.email && !!config.email.host;
    return !!config.email && !!config.email.sendgrid_api_key;
  }

  async send(params = {}) {
    // Extract customizations or use default config
    const {
      messageId = uuidv4(), //
      from = config.email.from,
      subject = this.email.subject,
      to = this.email.to,
      html = this.email.html,
      ...metadata
    } = params;

    const mailOptions = {
      messageId,
      from,
      to,
      subject,
      html,
      ...metadata, // Append any custom attributes
    };

    console.log('pre-validate');
    // Validate email template
    validate.required(mailOptions, ['to', 'subject', 'html']);
    console.log('post-validate');

    const result = await this.transporter
      .sendMail(mailOptions)
      .then(([res]) => {
        return true;
      })
      .catch((err) => {
        return false;
      });
  }
};
