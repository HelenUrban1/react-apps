const config = require('../config')();
const { i18n } = require('../i18n');

const brandColor = '#1B838B';
const backgroundColor = '#eef1f7';
const border = '1px solid #c8cbd1';
const borderBottom = '1px solid #a8abb1';

const spacerCol = `<td style="width:30px; padding:5px;" />`;
const spacerRow = `
  <tr style="height:30px;">
    <td style="width:30px; padding:5px;" />
    <td />
    <td />
    <td />
    <td />
    <td style="width:30px; padding:5px;" />
  </tr>
`;
module.exports = class EmailTemplate {
  constructor(language, type, to, link, alternateLink) {
    this.language = language || 'en';
    this.to = to;
    this.link = link;
    this.alternateLink = alternateLink;
    this.type = type;
    this.recipient = '';
    this.organization = '';
    this.sender = '';
    this.destination = 'signup';
    this.option = '';
    this.message = '';
    this.disclaimer = '';
    this.date = '';
  }

  get subject() {
    return i18n(this.language, `emails.${this.type}.subject`, i18n(this.language, 'app.title'), this.sender);
  }

  set setDestination(destination) {
    this.destination = destination;
  }

  set setToken(token) {
    this.token = token;
    let url = config.clientUrl;
    url += `/auth/${this.destination}?email=`;
    url += encodeURIComponent(this.to);
    if (token) {
      url += '&token=';
      url += encodeURIComponent(token);
    }
    this.link = url;
  }

  set setAlternateLink(altLink) {
    this.alternateLink = altLink;
  }

  set setOrganization(orgName) {
    this.organization = orgName;
  }

  set setMessage(message) {
    this.message = message;
  }

  set setOption(option) {
    this.option = option;
    if (option) {
      let url = this.link;
      let alternateLink = this.link;
      url += option;
      alternateLink += option;
      this.link = url;
      this.alternateLink = alternateLink;
    }
  }

  set setRecipient(name) {
    this.recipient = name;
  }

  set setSender(name) {
    this.sender = name;
  }

  set setDisclaimer(disclaimer) {
    this.disclaimer = disclaimer;
  }

  set setDate(date) {
    this.date = date;
  }

  returnTemplateCopy(type) {
    let linkButton = `
    <p style="text-align:center;margin:24px 0;">
        <a style="background-color:${brandColor};color:#fff;padding:10px 16px;text-align:center;border-radius:4px;text-decoration:none;" href="${this.link}">
            ${i18n(this.language, `emails.${this.type}.button`)}
        </a>
    </p>
    `;
    let body = '';
    const alternateLinkButton = `
    <p style="text-align:center;margin:24px 0;">
        <a style="background-color:rgba(0,0,0,0);border:1px solid ${brandColor};color:${brandColor};padding:10px 16px;text-align:center;border-radius:4px;text-decoration:none;" href="${this.alternateLink}">
            ${i18n(this.language, `emails.${this.type}.altButton`)}
        </a>
    </p>
    `;
    switch (type) {
      case 'invitation':
      case 'invitationReminder':
        linkButton = `
    <p style="text-align:center;margin:24px 0;">
        <a style="background-color:${brandColor};color:#fff;padding:10px 16px;text-align:center;border-radius:4px;text-decoration:none;" href="${this.link}">
            ${i18n(this.language, `emails.${this.type}.button`, i18n(this.language, 'app.title'))}
        </a>
    </p>
    `;
        return i18n(this.language, `emails.${this.type}.body`, i18n(this.language, 'app.title'), linkButton, this.sender);
      case 'request':
        linkButton = `
    <p style="text-align:center;margin:24px 0;">
        <a style="background-color:${brandColor};color:#fff;padding:10px 16px;text-align:center;border-radius:4px;text-decoration:none;" href="${this.link}">
            ${i18n(this.language, `emails.${this.type}.button`, i18n(this.language, 'app.title'))}
        </a>
    </p>
    `;
        return i18n(this.language, `emails.${this.type}.body`, i18n(this.language, 'app.title'), linkButton, alternateLinkButton, this.recipient, this.sender);
      case 'orphanRequestOpen':
        linkButton = `
      <p style="text-align:center;margin:24px 0;">
          <a style="background-color:${brandColor};color:#fff;padding:10px 16px;text-align:center;border-radius:4px;text-decoration:none;" href="${this.link}">
              ${i18n(this.language, `emails.${this.type}.button`, i18n(this.language, 'app.title'))}
          </a>
      </p>
      `;
        return i18n(this.language, `emails.${this.type}.body`, i18n(this.language, 'app.title'), linkButton, alternateLinkButton, this.recipient);
      case 'orphanRequestEmail':
        linkButton = `
        <p style="text-align:center;margin:24px 0;">
            <a style="background-color:${brandColor};color:#fff;padding:10px 16px;text-align:center;border-radius:4px;text-decoration:none;" href="${this.link}">
                ${i18n(this.language, `emails.${this.type}.button`, i18n(this.language, 'app.title'))}
            </a>
        </p>
        `;
        return i18n(this.language, `emails.${this.type}.body`, i18n(this.language, 'app.title'), linkButton, alternateLinkButton, this.recipient);
      case 'resendClosed':
        return i18n(this.language, `emails.${this.type}.body`, i18n(this.language, 'app.title'), linkButton, this.recipient);
      case 'requestApproved':
        linkButton = `
      <p style="text-align:center;margin:24px 0;">
          <a style="background-color:${brandColor};color:#fff;padding:10px 16px;text-align:center;border-radius:4px;text-decoration:none;" href="${this.link}">
              ${i18n(this.language, `emails.${this.type}.button`, i18n(this.language, 'app.title'))}
          </a>
      </p>
      `;
        return i18n(this.language, `emails.${this.type}.body`, i18n(this.language, 'app.title'), linkButton, this.recipient, this.sender, this.organization);
      case 'requestRejected':
        return i18n(this.language, `emails.${this.type}.body`, i18n(this.language, 'app.title'), this.recipient);
      case 'emailAddressVerification':
      case 'emailAddressVerificationReminder':
        return i18n(this.language, `emails.${this.type}.body`, i18n(this.language, 'app.title'), linkButton, this.recipient);
      case 'passwordReset':
        return i18n(this.language, `emails.${this.type}.body`, i18n(this.language, 'app.title'), linkButton, this.link, this.recipient);
      case 'verifiedUser':
        return i18n(this.language, `emails.${this.type}.body`, i18n(this.language, 'app.title'), linkButton, this.recipient);
      case 'orphanedUser':
        return i18n(this.language, `emails.${this.type}.body`, i18n(this.language, 'app.title'), this.recipient);
      case 'pendingAction':
        return i18n(this.language, `emails.${this.type}.body`, i18n(this.language, 'app.title'), this.recipient, this.message);
      case 'paymentFailed':
        body += i18n(this.language, 'emails.paymentFailed.bodyA', this.date);
        if (this.alternateLink) {
          body += i18n(this.language, 'emails.paymentFailed.billing', this.alternateLink, this.link, i18n(this.language, 'app.title'));
        } else {
          body += i18n(this.language, 'emails.paymentFailed.settings', this.link, i18n(this.language, 'app.title'));
        }
        body += i18n(this.language, 'emails.paymentFailed.bodyB', i18n(this.language, 'app.orderPhone'), i18n(this.language, 'app.title'));
        if (this.disclaimer) {
          body += i18n(this.language, 'emails.paymentFailed.disclaimer', this.disclaimer);
        }
        return body;
      default:
        return '';
    }
  }

  // Gmail will try to load the image for you, so to make image work locally,
  // we need to load from dev
  static imageUrl(clientUrl) {
    if (clientUrl.includes('local')) {
      return 'https://www.ixc-dev.com';
    }
    return clientUrl;
  }

  get html() {
    const template = `
      <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
      <html xmlns="http://www.w3.org/1999/xhtml" lang="en-GB">
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
            <title>${i18n(this.language, `emails.${this.type}.subject`, i18n(this.language, 'app.title'), this.sender)}</title>
            <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        </head>
        <body style="margin: 0; padding: 0;">
          <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="100%" style="background-color:${backgroundColor};color:black;font-family: Roboto,RobotoDraft,Helvetica,Arial,sans-serif;">
            ${spacerRow}
            <tr>
              ${spacerCol}
              <td colspan="4">
                <table width="600px" align="center" border="0" cellpadding="0" cellspacing="0" style="border:${border};border-bottom:${borderBottom};background-color:white;border-collapse: collapse;">
                  ${spacerRow}
                  <tr>
                    ${spacerCol}
                    <td colspan="2">
                      <div style="width:200px;margin:8px 8px 20px 0px;">
                        <img style="width:100%" src="${EmailTemplate.imageUrl(config.clientUrl)}/images/logo.png" alt="${i18n(this.language, 'app.logoAlt')}" />
                      </div>
                    </td>
                    <td />
                    <td />
                    ${spacerCol}
                  </tr>
                  <tr>
                    ${spacerCol}
                    <td colspan="4">
                      ${this.returnTemplateCopy(this.type)}
                    </td>
                    ${spacerCol}
                  </tr>
                </table>
              </td>
              <td style="width:30px;" />
            </tr>
            ${spacerRow}
            <tr>
              ${spacerCol}
              <td colspan="4">
                <table align="center" border="0" width="600px">
                  <tr style="font-size:14px;color:rgba(0,0,0,0.85);">
                    <td align="center">${i18n(this.language, 'app.companyName')}</td>
                  </tr>
                  <tr style="font-size:14px;color:rgba(0,0,0,0.85);">
                    <td align="center">${i18n(this.language, 'app.street')}, ${i18n(this.language, 'app.city')}, ${i18n(this.language, 'app.zip')}</td>
                  </tr>
                </table>
              </td>
              ${spacerCol}
            </tr>
            ${spacerRow}
          </table>
        </body> 
      </html>
    `;
    return template;
  }
};
