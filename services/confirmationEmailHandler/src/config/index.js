require('dotenv').config();

module.exports = function get() {
  return {
    env: process.env.NODE_ENV,

    // Default AWS Region
    DefaultRegion: process.env.IXC_AWS_REGION || 'us-east-1',
    backendUrl: process.env.IXC_BACKEND_URL || 'https://www.ixc-dev.com/api',
    email: {
      transport: 'sendgrid',
      sendgrid_api_key: process.env.IXC_MAIL_SENDGRID_API_KEY,
      from: `"Ideagen Quality Control" <${process.env.IXC_MAIL_FROM_ADDRESS}>`,
    },

    // Client URL used when sending emails
    clientUrl: process.env.IXC_MAIL_SENDING_DOMAIN_PORT ? `${process.env.IXC_MAIL_SENDING_DOMAIN}:${process.env.IXC_MAIL_SENDING_DOMAIN_PORT}` : process.env.IXC_MAIL_SENDING_DOMAIN,
  };
};
