const b64 = require('base64-js');
const { buildClient, CommitmentPolicy, KmsKeyringNode } = require('@aws-crypto/client-node');
const EmailSender = require('./EmailSender');
const EmailTemplate = require('./template/template');
const config = require('./config')();

const { decrypt } = buildClient(CommitmentPolicy.REQUIRE_ENCRYPT_ALLOW_DECRYPT);

const kmsKeyring = new KmsKeyringNode({
  keyIds: [process.env.COGNITO_EMAILER_KEY_ARN],
});

/* Sample Event
	{
  version: '1',
  region: 'us-east-1',
  userPoolId: 'us-east-1_xxxxxx',
  userName: 'xxxx@xxxx.com',
  callerContext: {
    awsSdkVersion: 'aws-sdk-xxxx-2.22.1',
    clientId: 'xxxxxxxxxxxxxxxxx'
  },
  triggerSource: 'CustomMessage_SignUp',
  request: {
    userAttributes: {
      sub: 'xxxxxxx',
      'cognito:user_status': 'UNCONFIRMED',
      email_verified: 'true',
      phone_number_verified: 'true',
      phone_number: '+1234567890',
      preferred_username: '+1234567890',
      email: 'xxxxxxx@xxxxxxx.com'
    },
    codeParameter: '{####}',
    linkParameter: '{##Click Here##}',
    usernameParameter: 'xxxxx'
  },
  response: { smsMessage: null, emailMessage: null, emailSubject: null }
}
*/

const handler = async (event, context, callback) => {
  console.log('confirmationEmailTrigger event', event);

  if (!event.triggerSource === 'CustomMessage_SignUp') callback(null, event);
  const { userAttributes, code, clientMetadata } = event.request;
  const { email } = userAttributes;
  const { source, sender, orgName, firstName } = clientMetadata;

  console.log('userAttributes, code, clientMetadata', { userAttributes, code, clientMetadata });
  console.log('email, source, sender, orgName, firstName', { email, source, sender, orgName, firstName });

  const { plaintext, messageHeader } = await decrypt(kmsKeyring, b64.toByteArray(code));
  console.log('plaintext, messageHeader', { plaintext, messageHeader });

  if (event.userPoolId !== messageHeader.encryptionContext['userpool-id']) {
    console.error('Encryption context does not match expected values!');
    return;
  }

  const codeParameter = plaintext.toString();

  const language = 'en';
  if (!EmailSender.isConfigured) {
    console.error(`Email provider is not configured. Please configure it at backend/config/<environment>.json.`);
    return;
  }

  let link;
  let generatedEmail;
  let alternateLink;

  switch (source) {
    case 'emailAddressVerification':
      // triggered by user signup or resending the verification email
      link = `${config.clientUrl}/auth/verify-email?token=${encodeURIComponent(codeParameter)}&email=${encodeURIComponent(email)}&form=new`;
      generatedEmail = new EmailTemplate(language, 'emailAddressVerification', email, link);
      generatedEmail.setRecipient = firstName || email;
      break;
    case 'invitation':
      // triggered by inviting a user or resending a user invite
      link = `${config.clientUrl}/auth/verify-email?token=${encodeURIComponent(codeParameter)}&email=${encodeURIComponent(email)}&form=invite`;
      generatedEmail = new EmailTemplate(language, source, email, link);
      generatedEmail.setRecipient = email;
      generatedEmail.setSender = sender || 'Admin';
      break;
    case 'requestApproved':
      // triggered by approving an access request or resending an approved access request
      link = `${config.clientUrl}/auth/verify-email?token=${encodeURIComponent(codeParameter)}&email=${encodeURIComponent(email)}&form=register`;
      generatedEmail = new EmailTemplate(language, 'requestApproved', email, link);
      generatedEmail.setRecipient = firstName || email;
      generatedEmail.setSender = sender;
      generatedEmail.setOrganization = orgName;
      break;
    case 'orphanRequestOpen':
      // triggered by new user who can request access or create new account
      link = `${config.clientUrl}/auth/verify-email?token=${encodeURIComponent(codeParameter)}&email=${encodeURIComponent(email)}&form=new`;
      alternateLink = `${config.clientUrl}/auth/request-access?token=${encodeURIComponent(codeParameter)}&email=${encodeURIComponent(email)}&form=request`;
      generatedEmail = new EmailTemplate(language, source, email, link, alternateLink);
      generatedEmail.setRecipient = firstName || email;
      generatedEmail.setSender = email;
      break;
    case 'resendClosed':
      link = `${config.clientUrl}/auth/verify-email?token=${encodeURIComponent(codeParameter)}&email=${encodeURIComponent(email)}&form=new`;
      generatedEmail = new EmailTemplate(language, source, email, link);
      generatedEmail.setRecipient = firstName || email;
      generatedEmail.setSender = email;
      break;
    case 'passwordReset':
      link = `${config.clientUrl}/auth/password-reset?token=${encodeURIComponent(codeParameter)}&email=${encodeURIComponent(email)}`;
      generatedEmail = new EmailTemplate(language, source, email, link);
      generatedEmail.setRecipient = firstName || email;
      break;
    default:
      console.warn(`Email handler from source ${source} did not trigger an email`);
      break;
  }

  console.log(`Generated email from source ${source}: ${generatedEmail}`);

  if (link && generatedEmail) {
    try {
      const messageId = await new EmailSender(generatedEmail).send();
      console.log(messageId);
    } catch (error) {
      console.error(error);
      return false;
    }
  }

  return true;
};

module.exports = handler;
