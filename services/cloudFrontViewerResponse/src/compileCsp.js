const fs = require('fs');
const cspDefinitions = require('./cspDefinitions.json');

const combineDirectives = (prop, src) => {
  if (prop) {
    prop.forEach((directive) => {
      src += ' ' + directive;
    });
  }
  return src;
};

const qualifyDirectives = (property, value) => {
  if (value.length > 0) {
    return property + value + '; ';
  }
  return value;
};

const saveOutput = (convertOutput) => {
  fs.writeFileSync('./src/compiledCsp.json', JSON.stringify({ csp: convertOutput }));
  console.log('Saved to compiledCsp.json');
};

const convertCspJsonToString = (cspDefinitions) => {
  const directiveOptions = [
    { name: 'scriptSrc', label: 'script-src', value: '' },
    { name: 'objectSrc', label: 'object-src', value: '' },
    { name: 'baseUri', label: 'base-uri', value: '' },
    { name: 'frameSrc', label: 'frame-src', value: '' },
    { name: 'styleSrc', label: 'style-src', value: '' },
    { name: 'imgSrc', label: 'img-src', value: '' },
    { name: 'connectSrc', label: 'connect-src', value: '' },
    { name: 'mediaSrc', label: 'media-src', value: '' },
    { name: 'fontSrc', label: 'font-src', value: '' },
    { name: 'workerSrc', label: 'worker-src', value: '' },
  ];
  cspDefinitions.forEach((element) => {
    const directives = element.directives;
    directiveOptions.forEach((directiveOption) => {
      directiveOption.value = combineDirectives(directives[directiveOption.name], directiveOption.value);
    });
  });

  directiveOptions.forEach((directiveOption) => {
    directiveOption.value = qualifyDirectives(directiveOption.label, directiveOption.value);
  });
  return directiveOptions.map((directiveOption) => directiveOption.value).join('');
};

const compileCsp = () => {
  try {
    console.log('Loading json from cspDefinitions.json');
    saveOutput(convertCspJsonToString(cspDefinitions));
  } catch (error) {
    console.error(error);
  }
};

compileCsp();

module.exports.convertCspJsonToString = convertCspJsonToString;
