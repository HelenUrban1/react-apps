const compiledCsp = require('./compiledCsp.json');

module.exports.handler = (event, context, callback) => {
  try {
    // Get contents of event
    // NOTE: Edge Lambda event structure from https://docs.aws.amazon.com/AmazonCloudFront/latest/DeveloperGuide/lambda-event-structure.html#lambda-event-structure-request
    const { request, response } = event.Records[0].cf;

    // Replace all self-references to the current host throughout the CSP
    // NOTE: Host must evaluate to the root domain,  "ixc-dev.com" or "ixc-qa.com" or "ixc-stage.com" respectively
    // If IXC_HOSTNAME isn't set, default will fail safe in prod
    let host = 'inspectionxpert.com';
    try {
      host = request.headers.host[0].value.replace(/(www|app)\./g, '');
    } catch (error) {
      console.error('Unable to extract host from event', error, event);
    }
    const finalCsp = compiledCsp.csp.replace(/https:\/\/\*\.\{\{IXC_HOSTNAME\}\}/g, `https://*.${host}`);

    // Set new headers
    [
      { key: 'Content-Security-Policy', value: finalCsp },
      { key: 'Referrer-Policy', value: 'same-origin' },
      { key: 'Strict-Transport-Security', value: 'max-age=31536000; includeSubdomains; preload' },
      { key: 'X-Content-Type-Options', value: 'nosniff' },
      { key: 'X-Frame-Options', value: 'SAMEORIGIN' },
      { key: 'X-XSS-Protection', value: '1; mode=block' },
    ].forEach((item) => {
      response.headers[item.key.toLowerCase()] = [item];
    });

    // Return modified response
    callback(null, response);
  } catch (error) {
    callback(Error(error));
  }
};
