const handler = require('../src/handler');
const cfOriginResponse = require('./fixtures/Cloudfront_Origin_Response.json');

describe('handler', () => {
  it('should set headers on response', () => {
    const event = cfOriginResponse;
    const context = null;
    const callback = (error, response) => {
      expect(error).toBe(null);

      expect(response.headers['content-security-policy'][0].key).toBe('Content-Security-Policy');
      const cspHeaderValue = response.headers['content-security-policy'][0].value;
      expect(cspHeaderValue).toContain('script-src');
      expect(cspHeaderValue).toContain('object-src');
      expect(cspHeaderValue).toContain('base-uri');
      expect(cspHeaderValue).toContain('frame-src');
      expect(cspHeaderValue).toContain('style-src');
      expect(cspHeaderValue).toContain('img-src');
      expect(cspHeaderValue).toContain('connect-src');
      expect(cspHeaderValue).toContain('media-src');
      expect(cspHeaderValue).toContain('font-src');
      expect(cspHeaderValue).toContain('worker-src');

      expect(response.headers['referrer-policy'][0].key).toBe('Referrer-Policy');
      expect(response.headers['referrer-policy'][0].value).toBe('same-origin');

      expect(response.headers['strict-transport-security'][0].key).toBe('Strict-Transport-Security');
      expect(response.headers['strict-transport-security'][0].value).toBe('max-age=31536000; includeSubdomains; preload');

      expect(response.headers['x-content-type-options'][0].key).toBe('X-Content-Type-Options');
      expect(response.headers['x-content-type-options'][0].value).toBe('nosniff');

      expect(response.headers['x-frame-options'][0].key).toBe('X-Frame-Options');
      expect(response.headers['x-frame-options'][0].value).toBe('SAMEORIGIN');

      expect(response.headers['x-xss-protection'][0].key).toBe('X-XSS-Protection');
      expect(response.headers['x-xss-protection'][0].value).toBe('1; mode=block');
    };
    handler.handler(event, context, callback);
  });

  it('should error on invalid response', () => {
    const event = {
      Records: [],
    };
    const context = null;
    const callback = (error, response) => {
      expect(error).toBeTruthy();
      expect(response).toBe(undefined);
    };
    handler.handler(event, context, callback);
  });
});
