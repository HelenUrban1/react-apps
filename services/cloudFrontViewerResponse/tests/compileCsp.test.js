const { convertCspJsonToString } = require('../src/compileCsp');

describe('convertCspJsonToString', () => {
  it('should covert json to a string', () => {
    const csp = [
      {
        name: 'inspectionxpert',
        directives: {
          scriptSrc: ["'self'", 'blob:'],
          objectSrc: ["'none'"],
          baseUri: ["'none'"],
          frameSrc: ["'self'"],
          styleSrc: ["'self'", "'unsafe-inline'", 'blob:'],
          imgSrc: ["'self'"],
          connectSrc: ["'self'", 'ws:'],
          workerSrc: ["'self'", 'blob:'],
        },
      },
      {
        name: 'one',
        directives: {
          scriptSrc: ['https://one.com'],
        },
      },
      {
        name: 'two',
        directives: {
          scriptSrc: ['https://two.com'],
          connectSrc: ['https://two.com'],
        },
      },
      {
        name: 'three',
        directives: {
          scriptSrc: ['https://three.com', 'https://three.net'],
          frameSrc: ['https://three.com'],
          styleSrc: ['https://three.com', 'https://three.net'],
          imgSrc: ['https://three.com', 'https://three.com'],
          connectSrc: ['https://three.com', 'https://three.net'],
        },
      },
    ];
    const result = convertCspJsonToString(csp);
    expect(result).toBe("script-src 'self' blob: https://one.com https://two.com https://three.com https://three.net; object-src 'none'; base-uri 'none'; frame-src 'self' https://three.com; style-src 'self' 'unsafe-inline' blob: https://three.com https://three.net; img-src 'self' https://three.com https://three.com; connect-src 'self' ws: https://two.com https://three.com https://three.net; worker-src 'self' blob:; ");
  });
});
