# cloudFrontViewerResponse

An AWS Lambda function that is triggered by Viewer Response CloudFront events to add security headers to frontend responses.


## Defining the Content Security Policy (CSP)

The CSP is defined in src/cspDefinitions.json. The file contains an array of objects, each with a name and directives. The name should be tied to the third party that is being allowed by the policy (chargify, hubspot, segment, etc.), allowing us to easily remove all the directives for a single entity if they are no longer being used. The directives currently available for use are defined in the directiveOptions variable in src/convert.js.

At run time, the directives are pulled from src/compiledCsp.json. To build the compiledCsp.json run:

```
npm run compile:csp
```

The compile command is run automatically as part of the bundle and deploy scripts.

## Supporting Loading Resources from Subdomains

The CSP policy syntax does not support self-references such as `*.self`

Subdomains need to be explicitly added with the fully qualified domains:

`default-src 'self' sub1.example.com sub2.example.com`

Or with wildcards:

`default-src 'self' *.example.com`

To simplify the use of self-references, the script globally replaces all instances of `{{IXC_HOSTNAME}}` in the CSP with the value found in `request.headers.host[0].value` of the inbound request. As such, `IXC_HOSTNAME` must evaluate to "ixc-dev.com" or "ixc-qa.com" or "ixc-stage.com" respectively. The default value used when `IXC_HOSTNAME` is not found is `inspectionxpert.com` so that the code will fail safe in production.

## Setup & Testing Locally

1 - Run the following commands:

```
cd services/cloudFrontViewerResponse
npm install
npm run test
```

## Manual Deploy

### Windows Users

Installed Gnu on Windows https://github.com/bmatzelle/gow/wiki or the zip commands in the npm scripts will fail.

### All Users

1. Verify that AWS lambda function has been setup in infrastructure.

   - Lambda runtime: Node.js 12.x
   - Verify ENV variables are setup according to this README in lambda (or populated in another way)
   - Ensure your AWS config and credentials allow you to auth to the ix-dev environment

2. First deploy, or whenever npm packages are updated

```
npm deploy:all
```

3. When only source is updated:

```
npm deploy
```

4. Publish New Lambda Version

    You won't see changes to the Lambda code reflected on the server until a new version of the Lambda is published and referenced in CloudFront

   - Navigate to Lambda > Functions > ixc-frontend-security-headers
   - Click the Versions and click the "Publish new version" button
   - Note the new version number, it will be used to update CloudFront in the next step

5. Update CloudFront

   - CloudFront Distributions > E3QQMV2N5YSWHU (Distribution for the main site) > Behaviors > Select record with Origin static.ixc-dev.com and click Edit > Lambda Function Associations
   - CloudFront Event: Viewer Response
   - Lambda Function ARN: arn:aws:lambda:us-east-1:541855766228:function:ixc-frontend-security-headers:12
   -  Note: the 12 at the end of the ARN is a version number that will need to update on each deploy. The ARN must have a version number at the end, and cannot just be set to $LATEST.
   - After updating the behavior, a cache invalidation is required for the updates to be seen.
