const axios = require('axios').default;
const config = require('./config')();

// Authenticates the admin user and retrieves active accounts from the backend API
const queryDatabase = async () => {
  axios.defaults.withCredentials = true;
  const headers = {
    'Content-Type': 'application/json; charset=utf-8',
  };

  // Log in as the admin user to get a bearer token
  const authRequestData = {
    query: 'mutation AUTH_SIGN_IN($email: String!, $password: String!) { authSignIn(email: $email, password: $password) }',
    variables: {
      email: config.adminEmail,
      password: config.adminPassword,
    },
  };

  const authResponse = await axios.post(config.backendUrl, JSON.stringify(authRequestData), { headers });
  if (!authResponse || !authResponse.data || !authResponse.data.data || !authResponse.data.data.authSignIn) {
    throw Error(`Could not authenticate admin account ${config.adminEmail} against service URL: ${config.backendUrl}`);
  }
  const authToken = authResponse.data.data.authSignIn;

  // Add auth cookie to the header of the account list request
  headers.Cookie = `jwt=${authToken};`;

  const oldDate = new Date();
  oldDate.setDate(oldDate.getDate() - 3);

  const userListRequestData = {
    query: `query INCOMPLETE_USER_LIST($emailVerificationTokenExpiresAt: String, $emailVerified: Boolean) {
    incompleteUserList(emailVerificationTokenExpiresAt: $emailVerificationTokenExpiresAt, emailVerified: $emailVerified){
      id
      email
    } }`,
    variables: {
      emailVerified: false,
      emailVerificationTokenExpiresAt: oldDate.toISOString(),
    },
  };

  const userListResponse = await axios.post(config.backendUrl, JSON.stringify(userListRequestData), { headers });
  if (!userListResponse || !userListResponse.data || !userListResponse.data.data || !userListResponse.data.data.incompleteUserList) {
    if (userListResponse && userListResponse.data && userListResponse.data.errors) {
      let errors = '';
      for (let index = 0; index < userListResponse.data.errors.length; index += 1) {
        const error = userListResponse.data.errors[index];
        errors += `${error.message}\n`;
      }
      throw Error(`Received no users from service. Error: ${errors}`);
    }
    throw Error(`Received no accounts from service. Unknown response: ${JSON.stringify(userListResponse)}`);
  }

  return userListResponse.data.data.incompleteUserList.rows.map((user) => user.email);
};

module.exports = queryDatabase;
