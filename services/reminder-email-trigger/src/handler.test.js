const axios = require('axios').default;
const { sqs } = require('aws-wrapper');
const config = require('./config')();

const handler = require('./handler');

jest.mock('aws-wrapper');
jest.mock('axios');

describe('handler', () => {
  it('should process a valid response for a non-cloudwatch event', async () => {
    const authResponse = {
      data: {
        data: {
          authSignIn: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6ImUwYTAxNzc5LWM4ZWQtNDA5MC1iZDQ0LTljMWIyMjQyZmI3NSIsImFjY291bnRJZCI6ImUwYTAxNzY5LWM4ZWQtNDA5MC1iZDI0LTljMWIyMjQxZmI3NSIsInNpdGVJZCI6IjMxZWYxZDU2LTFiODYtNGZmYS1iODg2LTUwN2VkOGYwMjFjNSIsImlhdCI6MTU5OTA4MDM0NH0.a_ZWj5lKUzzaWoLzQ6nxPh3Bn-DUtzHSEsTy8yb1x5E',
          incompleteUserList: {
            count: 5,
            rows: [
              { id: '4ddb4b9d-55fd-46b8-8802-c8dd15d0f33b' }, //
              { id: '80cc2d3f-b3bd-482a-9c5c-b3d5c95143d1' },
              { id: 'e0a01769-c8ed-4090-bd24-9c1b2241fb75' },
              { id: '8f186c73-8de0-467e-afe9-3e3132a48de1' },
              { id: 'ddbe20ac-10bd-444e-9552-cef7b929eedb' },
            ],
          },
        },
      },
    };

    const userResponse = {
      data: {
        data: {
          incompleteUserList: {
            count: 5,
            rows: [
              { id: '4ddb4b9d-55fd-46b8-8802-c8dd15d0f33b', email: 'test1@test.com' }, //
              { id: '80cc2d3f-b3bd-482a-9c5c-b3d5c95143d1', email: 'test2@test.com' },
              { id: 'e0a01769-c8ed-4090-bd24-9c1b2241fb75', email: 'test3@test.com' },
              { id: '8f186c73-8de0-467e-afe9-3e3132a48de1', email: 'test4@test.com' },
              { id: 'ddbe20ac-10bd-444e-9552-cef7b929eedb', email: 'test5@test.com' },
            ],
          },
        },
      },
    };

    const sendResponse = {
      data: {
        authSendReminderInvite: true,
      },
    };

    axios.post.mockResolvedValueOnce(authResponse);
    axios.post.mockResolvedValueOnce(userResponse);
    userResponse.data.data.incompleteUserList.rows.forEach(() => {
      axios.post.mockResolvedValueOnce(sendResponse);
    });

    const event = {};
    const result = await handler(event);
    // expect(result.messages.length).toBe(5);
    // expect(result.sqsUrl).toBe(config.sqsUrl);
    expect(result).toBeTruthy();
  });

  it('should notify when not being able to authenticate to the service', async () => {
    const response = {
      data: {
        error: {
          message: 'Invalid username or password',
        },
      },
    };
    axios.post.mockImplementationOnce(() => response);

    const event = {};
    await expect(async () => {
      return handler(event);
    }).rejects.toEqual(new Error(`Could not authenticate admin account ${config.adminEmail} against service URL: ${config.backendUrl}`));
  });

  it('should notify on an invalid response from account service', async () => {
    const response = {
      data: {
        data: {
          authSignIn: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6ImUwYTAxNzc5LWM4ZWQtNDA5MC1iZDQ0LTljMWIyMjQyZmI3NSIsImFjY291bnRJZCI6ImUwYTAxNzY5LWM4ZWQtNDA5MC1iZDI0LTljMWIyMjQxZmI3NSIsInNpdGVJZCI6IjMxZWYxZDU2LTFiODYtNGZmYS1iODg2LTUwN2VkOGYwMjFjNSIsImlhdCI6MTU5OTA4MDM0NH0.a_ZWj5lKUzzaWoLzQ6nxPh3Bn-DUtzHSEsTy8yb1x5E',
        },
      },
    };
    const errorMessage = 'Service not found';
    axios.post.mockImplementationOnce(() => response).mockImplementationOnce(() => new Error(errorMessage));

    const event = {};
    await expect(async () => {
      return handler(event);
    }).rejects.toEqual(new Error('Received no accounts from service. Unknown response: {}'));
  });
});
