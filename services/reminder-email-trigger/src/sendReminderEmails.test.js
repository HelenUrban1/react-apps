const axios = require('axios').default;
const sendReminderEmails = require('./sendReminderEmails');

jest.mock('axios');

const emails = ['test1@email.com', 'test2@email.com'];

describe('sendReminderEmails', () => {
  afterEach(() => {
    jest.resetAllMocks();
  });

  it('should return true if emails are valid', async () => {
    const sendResponse = {
      data: {
        authSendReminderInvite: true,
      },
    };

    emails.forEach(() => {
      axios.post.mockResolvedValueOnce(sendResponse);
    });

    const response = await sendReminderEmails(emails);
    expect(response).toBeTruthy();
  });

  it('should throw errors if response contains errors', async () => {
    const response = {
      data: {
        errors: [{ message: 'Error A' }, { message: 'Error B' }],
      },
    };

    emails.forEach(() => {
      axios.post.mockResolvedValueOnce(response);
    });

    const errs = 'Error A\nError B\n';

    await expect(async () => {
      return sendReminderEmails(emails);
    }).rejects.toEqual(Error(`Reminder email could not be sent. Error: ${errs}`));
  });

  it('should throw an error for an unknown response', async () => {
    const response = 'unknown';

    emails.forEach(() => {
      axios.post.mockResolvedValueOnce(response);
    });

    await expect(async () => {
      return sendReminderEmails(emails);
    }).rejects.toEqual(Error(`Reminder email could not be sent. Unknown response: "unknown"`));
  });
});
