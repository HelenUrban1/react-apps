const queryDatabase = require('./queryDatabase');
const sendReminderEmails = require('./sendReminderEmails');

const handler = async () => {
  try {
    const emails = await queryDatabase();
    const reminder = await sendReminderEmails(emails);
    if (reminder) {
      return reminder;
    }

    throw new Error('Query for user list failed.');
  } catch (error) {
    // log the error and rethrow for Lambda
    if (process.env.NODE_ENV !== 'test') {
      console.error(error);
    }
    throw error;
  }
};

module.exports = handler;
