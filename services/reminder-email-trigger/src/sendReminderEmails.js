const axios = require('axios').default;
const config = require('./config')();

// Authenticates the admin user and retrieves active accounts from the backend API
const sendReminderEmails = async (emails) => {
  axios.defaults.withCredentials = true;
  const headers = {
    'Content-Type': 'application/json; charset=utf-8',
  };

  await Promise.all(
    emails.map(async (email) => {
      const reminderInviteRequestData = {
        query: `mutation authSendReminderInvite($email: String!) {authSendReminderInvite(email: $email)}`,
        variables: { email },
      };

      const reminderResponse = await axios.post(config.backendUrl, JSON.stringify(reminderInviteRequestData), { headers });
      if (!reminderResponse || !reminderResponse.data || !reminderResponse.data.authSendReminderInvite) {
        if (reminderResponse && reminderResponse.data && reminderResponse.data.errors) {
          let errors = '';
          for (let index = 0; index < reminderResponse.data.errors.length; index += 1) {
            const error = reminderResponse.data.errors[index];
            errors += `${error.message}\n`;
          }
          throw Error(`Reminder email could not be sent. Error: ${errors}`);
        }
        throw Error(`Reminder email could not be sent. Unknown response: ${JSON.stringify(reminderResponse)}`);
      }

      return Promise.resolve();
    })
  );

  return true;
};

module.exports = sendReminderEmails;
