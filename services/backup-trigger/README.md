# backup-trigger

An AWS Lambda function that gets triggered by a CloudWatch event, checks for accounts in the primary database and creates SQS messages for the queue processor to run backups.

## Testing Locally

1 - Add a `.env` file to root folder. Populate based on the following:

| Variable             | Required? | Example Value                                                | Notes                                                     |
| -------------------- | :-------: | ------------------------------------------------------------ | -----------------------------------------                 |
| `NODE_ENV`           |     Y     | "development"                                                | Sets the environment we are in. Default value: production |
| `IXC_AWS_REGION`     |     Y     | "us-east-1"                                                  | Default region                                            |
| `IXC_BACKEND_URL`    |     Y     | "http://ixc-local.com:8080/api"                              | Backend API endpoint                                      |
| `IXC_ADMIN_EMAIL`    |     Y     | "dev.mail.monkey@gmail.com"                                  | Email of user that will query accounts                    |
| `IXC_ADMIN_PASSWORD` |     Y     | "test"                                                       | Password of user that will query accounts                 |
| `IXC_SQS_URL`        |     Y     | "https://sqs.us-east-1.amazonaws.com/541855766228/TestQueue" | Url of SQS queue to write to                              |

2 - Build supporting sub-packages

```
cd packages/aws-wrapper
npm install
npm run build
```

3 - Run the following commands:

```
cd services/backup-trigger
npm install
npm test
```

4 - Build and zip the function ready to deploy to AWS.
   npm run bundle:all

5 - Log in to the AWS Web UI for Lambda, select the appropriate Lambda and upload the newly created function.zip file.

## Manual Deploy

1. Create an AWS lambda function:
    - Author from scratch
    - Runtime: Node.js 18.x

2. Configuration -> Function code:
    - Code Entry Type: Upload a .zip file
    - Save

3. Configuration -> Execution role
    - Edit the role and attach the policy "AmazonSQSFullAccess"

4. Add a Cloudwatch event trigger
