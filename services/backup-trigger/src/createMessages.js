const createMessages = (accountIds) => {
  const messages = [];

  const body = { message: 'Process method for account' };
  for (let index = 0; index < accountIds.length; index += 1) {
    const attributes = {
      accountId: {
        DataType: 'String',
        StringValue: accountIds[index],
      },
      method: {
        DataType: 'String',
        StringValue: 'backup-full',
      },
    };
    messages.push({ MessageBody: body, MessageAttributes: attributes });
  }
  return messages;
};

module.exports = createMessages;
