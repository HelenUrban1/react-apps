const queryDatabase = require('./queryDatabase');
const createMessages = require('./createMessages');
const sendMessages = require('./sendMessages');
const config = require('./config')();

const handler = async (event) => {
  try {
    const activeAccounts = await queryDatabase();
    if (activeAccounts && activeAccounts.length > 0) {
      const messages = createMessages(activeAccounts);
      // if triggered by cloudwatch, send the messages and return,
      // otherwise its triggered by a step function and should just return the messages so they can have transaction IDs applied to them
      if (event.source === 'aws.events') {
        const result = await sendMessages(messages);
        if (result.failures.length > 0) {
          console.error(result.message);
          for (let index = 0; index < result.failures.length; index += 1) {
            const failure = result.failures[index];
            console.error(`Failed to send a message for account ${failure} to queue`);
          }
        } else {
          console.log(result.message);
        }
        return result;
      }
      return { sqsUrl: config.sqsUrl, messages };
    }
    throw new Error('Query for accounts returned no results.');
  } catch (error) {
    // log the error and rethrow for Lambda
    if (process.env.NODE_ENV !== 'test') {
      console.error(error);
    }
    throw error;
  }
};

module.exports = handler;
