const { sqs } = require('aws-wrapper');
const config = require('./config')();

const sendMessages = async (messages) => {
  const successes = [];
  const failures = [];
  const doSendMessages = [];
  for (let index = 0; index < messages.length; index += 1) {
    const message = messages[index];
    doSendMessages.push(sqs.sendMessage({ QueueUrl: config.sqsUrl, MessageBody: message.body, MessageAttributes: message.attributes }));
  }
  const results = await Promise.all(doSendMessages);
  for (let index = 0; index < results.length; index += 1) {
    const message = messages[index];
    const result = results[index];
    if (result && result.MessageId && result.MessageId.length === 36) {
      successes.push(message.MessageAttributes.accountId.StringValue);
    } else {
      failures.push(message.MessageAttributes.accountId.StringValue);
    }
  }

  return { message: `${successes.length} messages sent successfully, ${failures.length} failures to queue: ${config.sqsUrl}`, failures, successes };
};

module.exports = sendMessages;
