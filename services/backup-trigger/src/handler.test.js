const axios = require('axios').default;
const { sqs } = require('aws-wrapper');
const config = require('./config')();

const handler = require('./handler');

jest.mock('aws-wrapper');
jest.mock('axios');

describe('handler', () => {
  it('should process a valid response for a cloudwatch event', async () => {
    sqs.sendMessage.mockImplementation(() => {
      return {
        MessageId: '3bd2e3f9-4a3d-4964-85f7-b2770d7c54cf',
      };
    });

    const response = {
      data: {
        data: {
          authSignIn: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6ImUwYTAxNzc5LWM4ZWQtNDA5MC1iZDQ0LTljMWIyMjQyZmI3NSIsImFjY291bnRJZCI6ImUwYTAxNzY5LWM4ZWQtNDA5MC1iZDI0LTljMWIyMjQxZmI3NSIsInNpdGVJZCI6IjMxZWYxZDU2LTFiODYtNGZmYS1iODg2LTUwN2VkOGYwMjFjNSIsImlhdCI6MTU5OTA4MDM0NH0.a_ZWj5lKUzzaWoLzQ6nxPh3Bn-DUtzHSEsTy8yb1x5E',
          accountList: {
            count: 5,
            rows: [
              { id: '4ddb4b9d-55fd-46b8-8802-c8dd15d0f33b' }, //
              { id: '80cc2d3f-b3bd-482a-9c5c-b3d5c95143d1' },
              { id: 'e0a01769-c8ed-4090-bd24-9c1b2241fb75' },
              { id: '8f186c73-8de0-467e-afe9-3e3132a48de1' },
              { id: 'ddbe20ac-10bd-444e-9552-cef7b929eedb' },
            ],
          },
        },
      },
    };
    axios.post.mockImplementation(() => response);

    const event = { source: 'aws.events' };
    const result = await handler(event);
    expect(result.successes.length).toBe(5);
    expect(result.failures.length).toBe(0);
  });

  it('should process a valid response for a non-cloudwatch event', async () => {
    sqs.sendMessage.mockImplementation(() => {
      return {
        MessageId: '3bd2e3f9-4a3d-4964-85f7-b2770d7c54cf',
      };
    });

    const response = {
      data: {
        data: {
          authSignIn: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6ImUwYTAxNzc5LWM4ZWQtNDA5MC1iZDQ0LTljMWIyMjQyZmI3NSIsImFjY291bnRJZCI6ImUwYTAxNzY5LWM4ZWQtNDA5MC1iZDI0LTljMWIyMjQxZmI3NSIsInNpdGVJZCI6IjMxZWYxZDU2LTFiODYtNGZmYS1iODg2LTUwN2VkOGYwMjFjNSIsImlhdCI6MTU5OTA4MDM0NH0.a_ZWj5lKUzzaWoLzQ6nxPh3Bn-DUtzHSEsTy8yb1x5E',
          accountList: {
            count: 5,
            rows: [
              { id: '4ddb4b9d-55fd-46b8-8802-c8dd15d0f33b' }, //
              { id: '80cc2d3f-b3bd-482a-9c5c-b3d5c95143d1' },
              { id: 'e0a01769-c8ed-4090-bd24-9c1b2241fb75' },
              { id: '8f186c73-8de0-467e-afe9-3e3132a48de1' },
              { id: 'ddbe20ac-10bd-444e-9552-cef7b929eedb' },
            ],
          },
        },
      },
    };
    axios.post.mockImplementation(() => response);

    const event = {};
    const result = await handler(event);
    expect(result.messages.length).toBe(5);
    expect(result.sqsUrl).toBe(config.sqsUrl);
  });

  it('should notify when not being able to authenticate to the service', async () => {
    const response = {
      data: {
        error: {
          message: 'Invalid username or password',
        },
      },
    };
    axios.post.mockImplementationOnce(() => response);

    const event = {};
    await expect(async () => {
      return handler(event);
    }).rejects.toEqual(new Error(`Could not authenticate admin account ${config.adminEmail} against service URL: ${config.backendUrl}`));
  });

  it('should notify on an invalid response from account service', async () => {
    const response = {
      data: {
        data: {
          authSignIn: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6ImUwYTAxNzc5LWM4ZWQtNDA5MC1iZDQ0LTljMWIyMjQyZmI3NSIsImFjY291bnRJZCI6ImUwYTAxNzY5LWM4ZWQtNDA5MC1iZDI0LTljMWIyMjQxZmI3NSIsInNpdGVJZCI6IjMxZWYxZDU2LTFiODYtNGZmYS1iODg2LTUwN2VkOGYwMjFjNSIsImlhdCI6MTU5OTA4MDM0NH0.a_ZWj5lKUzzaWoLzQ6nxPh3Bn-DUtzHSEsTy8yb1x5E',
        },
      },
    };
    const errorMessage = 'Service not found';
    axios.post.mockImplementationOnce(() => response).mockImplementationOnce(() => new Error(errorMessage));

    const event = {};
    await expect(async () => {
      return handler(event);
    }).rejects.toEqual(new Error('Received no accounts from service. Unknown response: {}'));
  });

  it('should notify if not all items are added to queue', async () => {
    const response = {
      data: {
        data: {
          authSignIn: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6ImUwYTAxNzc5LWM4ZWQtNDA5MC1iZDQ0LTljMWIyMjQyZmI3NSIsImFjY291bnRJZCI6ImUwYTAxNzY5LWM4ZWQtNDA5MC1iZDI0LTljMWIyMjQxZmI3NSIsInNpdGVJZCI6IjMxZWYxZDU2LTFiODYtNGZmYS1iODg2LTUwN2VkOGYwMjFjNSIsImlhdCI6MTU5OTA4MDM0NH0.a_ZWj5lKUzzaWoLzQ6nxPh3Bn-DUtzHSEsTy8yb1x5E',
          accountList: {
            count: 5,
            rows: [
              { id: '4ddb4b9d-55fd-46b8-8802-c8dd15d0f33b' }, //
              { id: '80cc2d3f-b3bd-482a-9c5c-b3d5c95143d1' },
              { id: 'e0a01769-c8ed-4090-bd24-9c1b2241fb75' },
            ],
          },
        },
      },
    };

    sqs.sendMessage
      .mockImplementationOnce(() => {
        return {
          MessageId: '3bd2e3f9-4a3d-4964-85f7-b2770d7c54cf',
        };
      })
      .mockImplementationOnce(() => {
        return {
          Error: 'Unable to add item to queue',
        };
      })
      .mockImplementationOnce(() => {
        return {
          Error: 'Unable to add item to queue',
        };
      });
    axios.post.mockImplementation(() => response);

    // eslint-disable-next-line no-console
    console.log('Expect console errors for result and 2 messages.');
    const event = { source: 'aws.events' };
    const result = await handler(event);
    expect(result.successes.length).toBe(1);
    expect(result.failures.length).toBe(2);
  });
});
