const createMessages = require('./createMessages');

describe('createMessages', () => {
  it('should format an array of account IDs to SQS messages', () => {
    const accountIds = [
      '4ddb4b9d-55fd-46b8-8802-c8dd15d0f33b', //
      '80cc2d3f-b3bd-482a-9c5c-b3d5c95143d1',
      'e0a01769-c8ed-4090-bd24-9c1b2241fb75',
      '8f186c73-8de0-467e-afe9-3e3132a48de1',
      'ddbe20ac-10bd-444e-9552-cef7b929eedb',
    ];
    const messages = createMessages(accountIds);
    expect(Array.isArray(messages)).toBe(true);
    expect(messages.length).toBe(5);
    expect(messages).toContainEqual({
      MessageAttributes: {
        accountId: {
          DataType: 'String',
          StringValue: 'e0a01769-c8ed-4090-bd24-9c1b2241fb75',
        },
        method: {
          DataType: 'String',
          StringValue: 'backup-full',
        },
      },
      MessageBody: { message: 'Process method for account' },
    });
  });
});
