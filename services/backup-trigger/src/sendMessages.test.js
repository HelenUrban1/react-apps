const { sqs } = require('aws-wrapper');
const sendMessages = require('./sendMessages');
const config = require('./config')();

jest.mock('aws-wrapper');

describe('sendMessages', () => {
  it('should send each message', async () => {
    sqs.sendMessage.mockImplementation(() => {
      return {
        MessageId: '3bd2e3f9-4a3d-4964-85f7-b2770d7c54cf',
      };
    });

    const messages = [
      {
        MessageAttributes: {
          accountId: { DataType: 'String', StringValue: '4ddb4b9d-55fd-46b8-8802-c8dd15d0f33b' },
          method: { DataType: 'String', StringValue: 'backup-full' },
        },
        MessageBody: 'Process method for account',
      },
      {
        MessageAttributes: {
          accountId: { DataType: 'String', StringValue: '80cc2d3f-b3bd-482a-9c5c-b3d5c95143d1' },
          method: { DataType: 'String', StringValue: 'backup-full' },
        },
        MessageBody: 'Process method for account',
      },
      {
        MessageAttributes: {
          accountId: { DataType: 'String', StringValue: 'e0a01769-c8ed-4090-bd24-9c1b2241fb75' },
          method: { DataType: 'String', StringValue: 'backup-full' },
        },
        MessageBody: 'Process method for account',
      },
      {
        MessageAttributes: {
          accountId: { DataType: 'String', StringValue: '8f186c73-8de0-467e-afe9-3e3132a48de1' },
          method: { DataType: 'String', StringValue: 'backup-full' },
        },
        MessageBody: 'Process method for account',
      },
      {
        MessageAttributes: {
          accountId: { DataType: 'String', StringValue: 'ddbe20ac-10bd-444e-9552-cef7b929eedb' },
          method: { DataType: 'String', StringValue: 'backup-full' },
        },
        MessageBody: 'Process method for account',
      },
    ];
    const result = await sendMessages(messages);
    expect(result.message).toBe(`5 messages sent successfully, 0 failures to queue: ${config.sqsUrl}`);
    expect(result.successes.length).toBe(5);
  });

  it('should alert on failed message sending', async () => {
    sqs.sendMessage
      .mockImplementationOnce(() => {
        return {
          MessageId: '3bd2e3f9-4a3d-4964-85f7-b2770d7c54cf',
        };
      })
      .mockImplementationOnce(() => {
        return {
          Error: 'Unable to add item to queue',
        };
      })
      .mockImplementationOnce(() => {
        return {
          MessageId: '3bd2e3f9-4a3d-4964-85f7-b2770d7c54cf',
        };
      });

    const messages = [
      {
        MessageAttributes: {
          accountId: { DataType: 'String', StringValue: '4ddb4b9d-55fd-46b8-8802-c8dd15d0f33b' },
          method: { DataType: 'String', StringValue: 'backup-full' },
        },
        MessageBody: 'Process method for account',
      },
      {
        MessageAttributes: {
          accountId: { DataType: 'String', StringValue: '80cc2d3f-b3bd-482a-9c5c-b3d5c95143d1' },
          method: { DataType: 'String', StringValue: 'backup-full' },
        },
        MessageBody: 'Process method for account',
      },
      {
        MessageAttributes: {
          accountId: { DataType: 'String', StringValue: 'e0a01769-c8ed-4090-bd24-9c1b2241fb75' },
          method: { DataType: 'String', StringValue: 'backup-full' },
        },
        MessageBody: 'Process method for account',
      },
    ];

    const result = await sendMessages(messages);
    expect(result.message).toBe(`2 messages sent successfully, 1 failures to queue: ${config.sqsUrl}`);
    expect(result.successes.length).toBe(2);
    expect(result.failures[0]).toBe('80cc2d3f-b3bd-482a-9c5c-b3d5c95143d1');
  });
});
