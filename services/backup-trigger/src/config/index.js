require('dotenv').config();

module.exports = function get() {
  return {
    env: process.env.NODE_ENV,

    // Default AWS Region
    DefaultRegion: process.env.IXC_AWS_REGION || 'us-east-1',
    sqsUrl: process.env.IXC_SQS_URL || 'https://sqs.us-east-1.amazonaws.com/541855766228/TestQueue',
    backendUrl: process.env.IXC_BACKEND_URL || 'https://www.ixc-dev.com/api',
    adminEmail: process.env.IXC_ADMIN_EMAIL || 'dev.mail.monkey@gmail.com',
    adminPassword: process.env.IXC_ADMIN_PASSWORD || 'test',
  };
};
