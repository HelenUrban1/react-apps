const axios = require('axios').default;

const queryDatabase = require('./queryDatabase');

jest.mock('axios');

describe('queryDatabase', () => {
  it('should return an array of UUIDs', async () => {
    const response = {
      data: {
        data: {
          authSignIn: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6ImUwYTAxNzc5LWM4ZWQtNDA5MC1iZDQ0LTljMWIyMjQyZmI3NSIsImFjY291bnRJZCI6ImUwYTAxNzY5LWM4ZWQtNDA5MC1iZDI0LTljMWIyMjQxZmI3NSIsInNpdGVJZCI6IjMxZWYxZDU2LTFiODYtNGZmYS1iODg2LTUwN2VkOGYwMjFjNSIsImlhdCI6MTU5OTA4MDM0NH0.a_ZWj5lKUzzaWoLzQ6nxPh3Bn-DUtzHSEsTy8yb1x5E',
          accountList: {
            count: 5,
            rows: [
              { id: '4ddb4b9d-55fd-46b8-8802-c8dd15d0f33b' }, //
              { id: '80cc2d3f-b3bd-482a-9c5c-b3d5c95143d1' },
              { id: 'e0a01769-c8ed-4090-bd24-9c1b2241fb75' },
              { id: '8f186c73-8de0-467e-afe9-3e3132a48de1' },
              { id: 'ddbe20ac-10bd-444e-9552-cef7b929eedb' },
            ],
          },
        },
      },
    };
    axios.post.mockResolvedValue(response);

    const accounts = await queryDatabase();
    expect(Array.isArray(accounts)).toBe(true);
    expect(accounts.length).toBe(5);
    expect(accounts).toContain('e0a01769-c8ed-4090-bd24-9c1b2241fb75');
  });
});
