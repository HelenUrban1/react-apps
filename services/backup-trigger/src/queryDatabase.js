const axios = require('axios').default;
const config = require('./config')();

// Authenticates the admin user and retrieves active accounts from the backend API
const queryDatabase = async () => {
  axios.defaults.withCredentials = true;
  const headers = {
    'Content-Type': 'application/json; charset=utf-8',
  };

  // Log in as the admin user to get a bearer token
  const authRequestData = {
    query: 'mutation AUTH_SIGN_IN($email: String!, $password: String!) { authSignIn(email: $email, password: $password) }',
    variables: {
      email: config.adminEmail,
      password: config.adminPassword,
    },
  };

  const authResponse = await axios.post(config.backendUrl, JSON.stringify(authRequestData), { headers });
  if (!authResponse || !authResponse.data || !authResponse.data.data || !authResponse.data.data.authSignIn) {
    throw Error(`Could not authenticate admin account ${config.adminEmail} against service URL: ${config.backendUrl}`);
  }
  const authToken = authResponse.data.data.authSignIn;

  // Add auth cookie to the header of the account list request
  headers.Cookie = `jwt=${authToken};`;

  const filter = {
    status: 'Active',
  };
  const orderBy = null;
  const limit = 0;
  const offset = 0;
  const requestData = {
    query: `query ACCOUNT_LIST($filter: AccountFilterInput, $orderBy: AccountOrderByEnum, $limit: Int, $offset: Int) {
        accountList(filter: $filter, orderBy: $orderBy, limit: $limit, offset: $offset) {
          count
          rows {
            id
          }
        }
      }`,
    variables: { filter, orderBy, limit, offset },
  };
  const response = await axios.post(config.backendUrl, JSON.stringify(requestData), { headers });

  if (!response || !response.data || !response.data.data || !response.data.data.accountList || !response.data.data.accountList.rows) {
    if (response && response.data && response.data.errors) {
      let errors = '';
      for (let index = 0; index < response.data.errors.length; index += 1) {
        const error = response.data.errors[index];
        errors += `${error.message}\n`;
      }
      throw Error(`Received no accounts from service. Error: ${errors}`);
    }
    throw Error(`Received no accounts from service. Unknown response: ${JSON.stringify(response)}`);
  }
  return response.data.data.accountList.rows.map((v) => v.id);
};

module.exports = queryDatabase;
