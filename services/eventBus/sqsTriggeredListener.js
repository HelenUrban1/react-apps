// Require objects.
var express = require('express');
var app = express();
var aws = require('aws-sdk');
var queueUrl = 'https://sqs.us-west-1.amazonaws.com/526262051452/MyFirstQueue';
var receipt = '';

var params = {
  QueueUrl: queueUrl,
  VisibilityTimeout: 600, // 10 min wait time for anyone else to process.
};
sqs.receiveMessage(params, function (err, data) {
  if (err) {
    res.send(err);
  } else {
    res.send(data);
  }
});

var params = {
  QueueUrl: queueUrl,
  ReceiptHandle: receipt,
};

sqs.deleteMessage(params, function (err, data) {
  if (err) {
    res.send(err);
  } else {
    res.send(data);
  }
});
