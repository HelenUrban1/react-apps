// We are assuming we already created a topic in the AWS console.
const sns = new aws.SNS({ region: 'us-east-1' });

// Send events via SNS which will forward messages to all relevant Queues
// Use one queue per task type (e.g. Billing tasks would have a queue separate from Email tasks.)

const params = {
  Message: JSON.stringify({
    type: 'some-event-type',
    data: {
      some: 'data',
    },
  }),
  // Optional : Filter on a message attribute
  MessageAttributes: {
    'event-type': {
      DataType: 'String',
      StringValue: 'some-event-type',
    },
  },
  // Create a topic in the SNS console and get the ARN for this.
  TopicArn: SnsTopic,
};

// Use async await or just use a promise
sns.publish(params).promise();
