/*
Datalake
An event bus lets us store every event received in a document store (could just be S3 or 
logging it to cloudwatch). This allows us to query any event that happened as well as replay them.

Activity feeds
When all events are stored in a document store you can just ask for events of type and give 
them back in order to the user so that they can see their own activity stream.

Incoming webhooks
Handle incoming webhooks with a simple webhook http handler route. The router just puts the 
events in the bus and lets the background workers take care of things from there.

server.route({
    path: "/api/webhooks/incoming/stripe",
    method: "POST",
    options: {
        handler: (req) => {
            server.createEvent("WEBHOOK_CALL_FROM_HUBSPOT", req.payload);
            return true
        }
    }
})
*/

// The listener watches the queue and run workers
// Alternatively setup an AWS lambda to trigger when a new SQS item is added. AWS will scale them up as needed.
// Either, should read tasks from the queue and call the appropriate handler

// Define max number of workers that can run at once
const MAX_WORKERS = 5;
let currentWorkers = 0;

// This function will manage the queue and bring up workers to handle events that show up in a queue
async function createTaskRunner(sqs, queue, server) {
  let running = false;

  // we have a copy of this running async for each task-type
  async function start() {
    running = true;
    while (running) {
      // This is soft target, because we may reenter here
      // while async waiting on the tasks. In other words:
      // - workers == 10
      // - we are good, we poll the task
      // - polling takes longer than a second
      // - and workers == MAX_WORKERS
      // - we poll again
      //
      // So this will overshoot a little.
      if (current_workers <= MAX_WORKERS) {
        const task = await checkQueue(sqs, queue);
        if (task) {
          // this is run async here to allow
          // running multiple workers of the same type
          // in parallel
          runTask(task, queue, sqs, server);
        }
      }
      await wait(1000);
    }
  }

  return {
    start,
    stop: () => {
      running = false;
    },
  };
}

// Function to check if there is a task in the queue which needs to run
async function checkQueue(sqs, queue) {
  const params = {
    QueueUrl: queue.url,
    MaxNumberOfMessages: 1,
    // WaitTimeSeconds is important.
    // The `await` will wait until it gets something from the queue or 20 seconds has passed before returning.
    // This way we don't keep running over and over and over as fast as possible.
    WaitTimeSeconds: 20,
  };

  // Wait until something is returned from the queue or for WaitTimeSeconds (20) seconds
  const res = await sqs.receiveMessage(params).promise();

  // If a message is found, process it
  if (res.Messages && res.Messages.length) {
    // Get the first message (may get more than one)
    const message = res.Messages[0];

    // Extract the message body
    let messageBody;
    try {
      const data = JSON.parse(message.Body);
      messageBody = JSON.parse(data.Message);
    } catch (e) {
      messageBody = message.Body;
    }

    // Prepare and return a task object for processing by a worker
    const task = {
      id: message.MessageId,
      receipt: message.ReceiptHandle,
      queue: queue.url,
      data: messageBody,
      message: message,
    };
    return task;
  } else {
    return null;
  }
}

// Function to manage the lifecycle of a task as a worker processes it
async function runTask(task, queue, sqs, server) {
  workers = workers + 1;
  const taskSummary = {
    type: queue.type,
    id: task.id,
  };

  try {
    // Extract the
    const complete = await queue.handler(task, queue, sqs, server);
    if (complete) {
      await sqs
        .deleteMessage({
          QueueUrl: queue.url,
          ReceiptHandle: task.receipt,
        })
        .promise();
    } else {
      //We even create events in our event handlers.
      server.createEvent(TASK_WORKER_FAILED, {
        taskSummary,
        complete,
      });
    }
  } catch (e) {
    server.createEvent(TASK_WORKER_FAILED, {
      taskSummary,
      e,
    });
  }
  workers = workers - 1;
}

// Register two task runners (one for each type of queue)
// prettier-ignore
const taskQueues = [ 
  { name: "email",
    url: "https://url-to-email-queue.aws.amazon.com/",
    handler: require("./workers/emailHandler")
  },
  { name: "subscription",
    url: "https://url-to-billing-queue.aws.amazon.com",
    handler: require("./workers/subscriptionHandler")
  }
]

// Main method that spins up the task runners
async function init(sqs, server, tasks) {
  const runners = [];
  taskQueues.forEach(async (queue) => {
    const runner = await createTaskRunner(sqs, queue, server);
    runners.push(runner);
    runner.start();
  });
}
