// Example for sending an email
exports.handler = (task, queue, sqs, server) => {
  let to;
  let message;

  // Handle multiple types of events here

  if (task.data.type === 'USER_CREATED') {
    // USER_CREATED
    to = task.data.data.user.email;
    message = 'WELCOME TO THE SITE!';
  } else if (task.data.type === 'USER_COMMENT') {
    // USER_COMMENT
    to = task.data.data.post.creator.email;
    message = 'SOMEONE COMMENTED ON YOUR POST';
  } else {
    // If an event type doesn't match any of the handlers log a warning
    log.warn(`Unrecognized event type ${task.data.type}`);
  }

  // Do the unit of work that is common to all events
  if (to && message) {
    // This is obviously simplified.
    // Sending emails is not part of this article.
    sendEmail(to, message);
  }

  // Let the system know the event was processed correctly
  return true;
};
