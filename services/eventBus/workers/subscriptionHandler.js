const subscriptionWorker = require('subscriptionWorker');

// Example for sending an email
exports.handler = (task, queue, sqs, server) => {
  const event = task.data.type;
  const payload = task.data.data;
  let result;

  try {
    // USER_CREATED
    const { user, account } = task.data.data;
    result = {
      success: true,
      result: subscriptionWorker.handleEvent(event, payload),
    };
  } catch (error) {
    console.error(error);
    result = { success: false };
  }

  // Let the system know the event was processed correctly
  return result;
};
