import { assumeRole } from './index';

describe('AssumeRole', () => {
  it('should return AWS objects authenticated to another role', async () => {
    const result = await assumeRole('username', '0123456789', 'arn:aws:iam::9876543210:role/all_access', '123456');
    expect(result?.kms.kms.config.credentials?.accessKeyId).toBe('ABCDE1FG234HI5JK6L7M');
    expect(result?.s3.s3.config.credentials?.accessKeyId).toBe('ABCDE1FG234HI5JK6L7M');
    expect(result?.secretsManager.secretsManager.config.credentials?.accessKeyId).toBe('ABCDE1FG234HI5JK6L7M');
    expect(result?.stepFunctions.stepFunctions.config.credentials?.accessKeyId).toBe('ABCDE1FG234HI5JK6L7M');
    expect(result?.sqs.sqs.config.credentials?.accessKeyId).toBe('ABCDE1FG234HI5JK6L7M');
  });
});
