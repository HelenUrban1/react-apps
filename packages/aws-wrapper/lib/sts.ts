import type AWS from 'aws-sdk';

export default class STS {
  sts: AWS.STS;

  constructor(sts: AWS.STS) {
    this.sts = sts;
  }

  assumeRole = async (username: string, sourceAccountId: string, roleArn: string, tokenCode: string) => {
    const assumeRoleRequest = {
      RoleArn: roleArn,
      RoleSessionName: username,
      SerialNumber: `arn:aws:iam::${sourceAccountId}:mfa/${username}`,
      TokenCode: tokenCode,
    };
    return this.sts.assumeRole(assumeRoleRequest).promise();
  };
}
