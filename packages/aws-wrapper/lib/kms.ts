import type AWS from 'aws-sdk';

export default class Kms {
  kms: AWS.KMS;

  constructor(kms: AWS.KMS) {
    this.kms = kms;
  }

  createKmsKey = async (tenantId: string) => {
    // https://docs.aws.amazon.com/AWSJavaScriptSDK/latest/AWS/KMS.html#createKey-property
    const data = await this.kms.createKey().promise();
    const keyId = data?.KeyMetadata?.KeyId || '';
    if (!keyId) {
      return null;
    }
    // https://docs.aws.amazon.com/AWSJavaScriptSDK/latest/AWS/KMS.html#createAlias-property
    const createAliasRequest: AWS.KMS.CreateAliasRequest = {
      AliasName: `alias/${tenantId}`, // alias must start with "alias/"
      TargetKeyId: keyId,
    };
    await this.kms.createAlias(createAliasRequest).promise();
    return keyId;
  };

  describeKey = async (tenantId: string) => {
    const describeKeyRequest = {
      KeyId: `alias/${tenantId}`,
    };
    const data = await this.kms.describeKey(describeKeyRequest).promise();
    return data?.KeyMetadata;
  };
}
