import { stepFunctions } from './index';

describe('AWS.StepFunctions', () => {
  it('should send task success', async () => {
    const result = await stepFunctions.sendTaskSuccess({
      taskToken: '',
      output: '',
    });
    expect(result).toStrictEqual({});
  });
  it('should send task success', async () => {
    const result = await stepFunctions.sendTaskFailure({
      taskToken: '',
      cause: '',
      error: '',
    });
    expect(result).toStrictEqual({});
  });
});
