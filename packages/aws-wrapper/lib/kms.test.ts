import { kms } from './index';

describe('AWS.KMS', () => {
  it('should create Aws Kms Key', async () => {
    const newKeyId = await kms.createKmsKey('05eaad91-8d77-49d5-b1b3-18079df70c60');
    expect(newKeyId).toBe('newKeyId');
  });

  it('should fetch key details', async () => {
    const result = await kms.describeKey('05eaad91-8d77-49d5-b1b3-18079df70c60');
    expect(result?.KeyId).toBe('newKeyId');
    expect(result?.Enabled).toBe(true);
  });
});
