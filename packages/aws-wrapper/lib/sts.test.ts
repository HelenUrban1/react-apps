import { sts } from './index';

describe('AWS.STS', () => {
  it('should assume role', async () => {
    const username = 'user_name';
    const sourceAccountId = '001234567890';
    const targetAccountId = '987654321012';
    const targetRole = 'allow-full-access-from-other-accounts';
    const roleArn = `arn:aws:iam::${targetAccountId}:role/${targetRole}`;
    const mfaToken = '123456';
    const result = await sts.assumeRole(username, sourceAccountId, roleArn, mfaToken);

    expect(result.Credentials?.AccessKeyId).toBeTruthy();
    expect(result.AssumedRoleUser?.AssumedRoleId).toBe('ABCDE1FG234HIJKLMNOPQ:user_name');
    expect(result.AssumedRoleUser?.Arn).toBe('arn:aws:sts::987654321012:assumed-role/allow-full-access-from-other-accounts/user_name');
  });
});
