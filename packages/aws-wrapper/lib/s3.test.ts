import { s3 } from './index';
import S3TestData from './s3TestData';

describe('AWS.S3', () => {
  it('should get a file', async () => {
    // Upload a file to get
    const testData = new S3TestData();
    const uploadFile = testData.testPdfFileDataBuffer;
    await s3.upload('bucketName', 'test.pdf', uploadFile, 'account=11111');

    // Get the file
    const file = await s3.getObject('bucketName', 'test.pdf');
    expect((file.Body as Buffer).length).toBe(111699);
  });
  it('should copy a file', async () => {
    const result = await s3.copyObject('bucketName', 'new.pdf', '/BucketName/test.pdf');
    expect(result).not.toBe(null);
  });

  it('should delete an object', async () => {
    const result = await s3.deleteObject('bucketName', 'test.pdf');
    expect(result).not.toBe(null);
  });

  it('should upload a file', async () => {
    const testData = new S3TestData();
    const file = testData.testPdfFileDataBuffer;
    const result = await s3.upload('bucketName', 'test.pdf', file, 'account=11111');
    expect(result).not.toBe(null);
  });

  it('should list files in bucket', async () => {
    // Upload files to list
    const testData = new S3TestData();
    const uploadFile = testData.testPdfFileDataBuffer;
    await s3.upload('listBucketName', 'test.pdf', uploadFile, 'account=11111');
    await s3.copyObject('listBucketName', 'new.pdf', '/listBucketName/test.pdf');

    const list = await s3.listObjects('listBucketName');
    expect(list.KeyCount).toBe(2);
    expect(list.Contents?.length).toBe(2);
  });
});
