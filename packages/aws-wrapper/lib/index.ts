import AWS from 'aws-sdk';
import AWSMock from 'aws-sdk-mock';
import os from 'os';
import fs from 'fs';
import path from 'path';
import Kms from './kms';
import SecretsManager from './secretsManager';
import S3 from './s3';
import S3TestData from './s3TestData';
import SQS from './sqs';
import STS from './sts';
import StepFunctions from './stepFunctions';

const fsPromises = fs.promises;

AWS.config.update({ region: process.env.IXC_AWS_REGION || 'us-east-1' });

// When is IXC_DB_HOST=postgres, the system is running inside the network created by ixc-mono/docker-compose.yml
if (process.env.IXC_DB_HOST === 'postgres') {
  console.log('aws-wrapper IXC_DB_HOST=postgres (running in docker-compose)');
}

if (process.env.NODE_ENV === 'development' || process.env.NODE_ENV === 'test' || process.env.NODE_ENV === undefined || process.env.IXC_DB_HOST === 'postgres') {
  console.log('aws-wrapper using mocks');
  const s3TestData = new S3TestData();
  AWSMock.setSDKInstance(AWS);
  const testPassword = '5yDbqx1r0GeRxHCFqeqVmiSBZxctTFT5';
  AWSMock.mock('KMS', 'createKey', () => {
    return Promise.resolve({ KeyMetadata: { KeyId: 'newKeyId' } });
  });
  AWSMock.mock('KMS', 'createAlias', () => {
    return Promise.resolve();
  });
  AWSMock.mock('KMS', 'describeKey', () => {
    return Promise.resolve({ KeyMetadata: { KeyId: 'newKeyId', Enabled: true } });
  });

  const writeStreamToFile = (filePath: string, stream: fs.ReadStream): Promise<void> => {
    return new Promise((resolve, reject) => {
      const file = fs.createWriteStream(filePath);
      stream.pipe(file);
      file.on('finish', () => {
        resolve();
      });
      file.on('error', reject);
    });
  };

  AWSMock.mock('S3', 'upload', async (params: any) => {
    let result: any;
    // console.log('s3.upload.params', params);
    try {
      const localPath = path.join(os.tmpdir(), 'S3', params.Bucket, params.Key);
      const localDir = path.dirname(localPath);

      // console.log(`S3mock saving file: ${localPath}`);
      await fsPromises.mkdir(localDir, { recursive: true });

      // If a file body is provided, create a file (otherwise we're just creating the folder)
      if (params.Body) {
        // Need to use different method to write if the body is stream instead of a string or buffer (s3 handles all 3)
        if (typeof params.Body.read === 'function') {
          await writeStreamToFile(localPath, params.Body);
        } else {
          await fsPromises.writeFile(localPath, params.Body);
        }
      }
      // TODO: Fake S3 response
      result = {
        ETag: '"6805f2cfc46c0f04559748bb039d69ae"',
        ServerSideEncryption: 'AES256',
        VersionId: 'Ri.vC6qVlA4dEnjgRV4ZHsHoFIjqEMNt',
        mock: true,
        path: localPath,
      };
    } catch (error) {
      console.error(error);
      throw error;
    }
    return Promise.resolve(result);
  });

  AWSMock.mock('S3', 'listObjectsV2', async (params: any) => {
    let result: any;
    try {
      const localPath = path.join(os.tmpdir(), 'S3', params.Bucket);
      result = {
        IsTruncated: false,
        Contents: [],
        Name: params.Bucket,
        Prefix: '',
        MaxKeys: 1000,
        CommonPrefixes: [],
        KeyCount: 0,
        mock: true,
        path: localPath,
      };
      if (fs.existsSync(localPath)) {
        const files = fs.readdirSync(localPath);
        files.forEach((file) => {
          const stats = fs.statSync(path.join(localPath, file));
          if (stats.isFile()) {
            result.Contents.push({
              Key: file,
              LastModified: stats.mtime,
              ETag: `${result.KeyCount}`,
              Size: stats.size,
              StorageClass: 'STANDARD',
            });
            result.KeyCount += 1;
          }
        });
      } else {
        console.log(`S3mock could not find file ${localPath}`);
      }
    } catch (error) {
      console.error(error);
      throw error;
    }
    return Promise.resolve(result);
  });

  AWSMock.mock('S3', 'getObject', async (params: any) => {
    let result: any;
    // console.log(params);
    try {
      const localPath = path.join(os.tmpdir(), 'S3', params.Bucket, params.Key);
      // console.log(`S3mock reading file: ${localPath}`);
      result = {
        ServerSideEncryption: 'aws:kms',
        SSEKMSKeyId: 'newKeyId',
        mock: true,
        path: localPath,
      };
      if (params.Key) {
        if (fs.existsSync(localPath)) {
          result.Body = Buffer.from(fs.readFileSync(localPath));
        } else {
          console.log(`S3mock could not find file ${localPath}`);
        }
      }
    } catch (error) {
      console.error(error);
      throw error;
      // TODO: This is to preserve original static return value, but the test for the PDF should be updated to use a generic path
      // result = {
      //   Body: s3TestData.testPdfFileDataBuffer,
      //   ServerSideEncryption: 'aws:kms',
      //   SSEKMSKeyId: 'newKeyId',
      // };
    }
    return Promise.resolve(result);
  });

  AWSMock.mock('S3', 'copyObject', async (params: any) => {
    let result: any;
    // console.log(params);
    try {
      const localSourcePath = `${os.tmpdir()}/S3/${params.CopySource}`;
      const localTargetPath = `${os.tmpdir()}/S3/${params.Bucket}/${params.Key}`;
      const localTargetDir = path.dirname(localTargetPath);

      // console.log(`S3mock creating dir: ${localTargetDir}`);
      await fsPromises.mkdir(localTargetDir, { recursive: true });

      // console.log(`S3mock copying file: ${localSourcePath} to ${localTargetPath}`);
      result = {
        ServerSideEncryption: 'aws:kms',
        SSEKMSKeyId: 'newKeyId',
        mock: true,
        path: localTargetPath,
      };
      if (fs.existsSync(localSourcePath)) {
        fs.copyFileSync(localSourcePath, localTargetPath);
      } else {
        console.log(`S3mock could not copy file because source file was not found ${localSourcePath}`);
      }
    } catch (error) {
      console.error(error);
      throw error;
    }
    return Promise.resolve(result);
  });

  AWSMock.mock('S3', 'deleteObject', async (params: any) => {
    let result: any;
    // console.log(params);
    try {
      const localPath = `${os.tmpdir()}/S3/${params.Bucket}/${params.Key}`;
      // console.log(`S3mock deleting file: ${localPath}`);
      result = {
        ServerSideEncryption: 'aws:kms',
        SSEKMSKeyId: 'newKeyId',
        mock: true,
        path: localPath,
      };
      if (fs.existsSync(localPath)) {
        fs.unlinkSync(localPath);
      } else {
        console.log(`S3mock could not delete file because file was not found ${localPath}`);
      }
    } catch (error) {
      console.error(error);
      throw error;
    }
    return Promise.resolve(result);
  });

  AWSMock.mock('SecretsManager', 'createSecret', () => {
    return Promise.resolve();
  });

  AWSMock.mock('SecretsManager', 'getRandomPassword', () => {
    return Promise.resolve({ RandomPassword: testPassword });
  });
  AWSMock.mock('SecretsManager', 'getSecretValue', (getSecretValueRequest: AWS.SecretsManager.GetSecretValueRequest) => {
    return Promise.resolve({
      SecretString: JSON.stringify({
        Username: getSecretValueRequest.SecretId,
        Password: testPassword,
      }),
    });
  });
  AWSMock.mock('SecretsManager', 'describeSecret', (describeSecretRequest: AWS.SecretsManager.DescribeSecretRequest) => {
    return Promise.resolve({ Name: describeSecretRequest.SecretId });
  });
  AWSMock.mock('SQS', 'sendMessage', () => {
    return Promise.resolve({
      MessageId: '3bd2e3f9-4a3d-4964-85f7-b2770d7c54cf',
    });
  });
  AWSMock.mock('SQS', 'receiveMessage', () => {
    return Promise.resolve({
      Messages: [
        {
          MessageId: '3bd2e3f9-4a3d-4964-85f7-b2770d7c54cf',
          ReceiptHandle: 'AQEBjjMNBxeBE1mJRK1SUZ+KIjv2JU5S35nW4iP5TIwm0fuMnCvtwmQNndO3YlIAgizEvgUfDw9Jr4w1Fv4SUBnKEHgWr+e2f84ASvSP0M1OgPGWMMQmzTrkUx5Cr3RcGmqdrEbRwFugejmfMnd3I4q+aFsXqiG0pWbxYOpYxbirdYyy9Z23QC0x3apeI2+mDOi02VOir0f9wlNTjp+TnK0MhHW8Tz0mFQDMnBy23poD/ZWFf6QaMX46psojVmK20h+jjjDG5XJ/WUkdHI4LU+1FZeAPpeDuIo8HJRJzLsBooRhH85pfTLTPR3sCFEnPmEOpgI8J/Jj36Ll2rdxFAGlaegDcZVgA5sR27gg3lWy8axfgPKAXIlw0Bri+ILusVhst',
          Body: 'Process method for account',
          MessageAttributes: {
            accountId: {
              DataType: 'String',
              StringValue: 'e0a01769-c8ed-4090-bd24-9c1b2241fb75',
            },
            method: {
              DataType: 'String',
              StringValue: 'backup-full',
            },
          },
        },
      ],
    });
  });
  AWSMock.mock('SQS', 'deleteMessage', () => {
    return Promise.resolve({});
  });
  AWSMock.mock('STS', 'assumeRole', (assumeRoleRequest: AWS.STS.AssumeRoleRequest) => {
    return Promise.resolve({
      ResponseMetadata: { RequestId: '1dd1c920-0eb3-49ed-9491-867d3c6f6566' },
      Credentials: {
        AccessKeyId: 'ABCDE1FG234HI5JK6L7M',
        SecretAccessKey: '1Ab2CdefGhIj3kL/Mno4p56QrS7tuVwxyzA8bC9d',
        SessionToken: 'AbcDEFGhIJklMNO//////////wEaDG+hrtehp0JKDJI1zSKxAY2SAlHOSGi1DshNy41c6nYyuiBIcCGWmDBoFB/gZEfvBCdSa48I7xbkjSBV0NdEfkP16ZN6QF7z1BBpRLjvg9PtWW82ZtPTIsCjyuyzxQXG7F9N0Of02k9BIf4qi1qpGq/9xtxBIVaMM3Z19oqFGNXha1mR2RMJb2Re1n6qFBvqmfQG/LmOTFLMRVmCyfItI5cjhDP234Zu3JJtunZbmr3PHtD5idcQ5e3GRnvxb2yKkSjVg4aBBjItZ7SD8Ttpmi6L7eKvVUH6euSuicCvjpwbUV41RjZ8OzHuBumzGbhTbmrvkB7A',
        Expiration: new Date(Date.now() + 60000),
      },
      AssumedRoleUser: {
        AssumedRoleId: `ABCDE1FG234HIJKLMNOPQ:${assumeRoleRequest.RoleSessionName}`,
        Arn: `${assumeRoleRequest.RoleArn.replace(':iam::', ':sts::').replace(':role/', ':assumed-role/')}/${assumeRoleRequest.RoleSessionName}`,
      },
    });
  });

  AWSMock.mock('StepFunctions', 'sendTaskSuccess', () => {
    return Promise.resolve({});
  });
  AWSMock.mock('StepFunctions', 'sendTaskFailure', () => {
    return Promise.resolve({});
  });
}

const awsKms = new AWS.KMS();
const awsS3 = new AWS.S3();
const awsSecretsManager = new AWS.SecretsManager();
const awsSqs = new AWS.SQS();
const awsSts = new AWS.STS();
const awsStepFunctions = new AWS.StepFunctions();
const kms = new Kms(awsKms);
const s3 = new S3(awsS3);
const secretsManager = new SecretsManager(awsSecretsManager);
const sqs = new SQS(awsSqs);
const sts = new STS(awsSts);
const stepFunctions = new StepFunctions(awsStepFunctions);

const assumeRole = async (username: string, sourceAccountId: string, roleArn: string, tokenCode: string) => {
  const credentials = await sts.assumeRole(username, sourceAccountId, roleArn, tokenCode);
  if (credentials?.Credentials) {
    const arAwsKms = new AWS.KMS({ accessKeyId: credentials.Credentials.AccessKeyId, secretAccessKey: credentials.Credentials.SecretAccessKey, sessionToken: credentials.Credentials.SessionToken });
    const arAwsS3 = new AWS.S3({ accessKeyId: credentials.Credentials.AccessKeyId, secretAccessKey: credentials.Credentials.SecretAccessKey, sessionToken: credentials.Credentials.SessionToken });
    const arAwsSecretsManager = new AWS.SecretsManager({ accessKeyId: credentials.Credentials.AccessKeyId, secretAccessKey: credentials.Credentials.SecretAccessKey, sessionToken: credentials.Credentials.SessionToken });
    const arAwsSqs = new AWS.SQS({ accessKeyId: credentials.Credentials.AccessKeyId, secretAccessKey: credentials.Credentials.SecretAccessKey, sessionToken: credentials.Credentials.SessionToken });
    const arAwsSts = new AWS.STS({ accessKeyId: credentials.Credentials.AccessKeyId, secretAccessKey: credentials.Credentials.SecretAccessKey, sessionToken: credentials.Credentials.SessionToken });
    const arAwsStepFunctions = new AWS.StepFunctions({ accessKeyId: credentials.Credentials.AccessKeyId, secretAccessKey: credentials.Credentials.SecretAccessKey, sessionToken: credentials.Credentials.SessionToken });
    const arKms = new Kms(arAwsKms);
    const arS3 = new S3(arAwsS3);
    const arSecretsManager = new SecretsManager(arAwsSecretsManager);
    const arSqs = new SQS(arAwsSqs);
    const arSts = new STS(arAwsSts);
    const arStepFunctions = new StepFunctions(arAwsStepFunctions);

    return { kms: arKms, s3: arS3, secretsManager: arSecretsManager, sqs: arSqs, sts: arSts, stepFunctions: arStepFunctions };
  }
  return null;
};

export { kms, s3, secretsManager, sqs, sts, assumeRole, stepFunctions };
