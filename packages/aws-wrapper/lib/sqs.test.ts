import { sqs } from './index';

describe('AWS.SQS', () => {
  const queueUrl = '';
  it('should send a message', async () => {
    const body = 'Process method for account';
    const attributes = {
      accountId: {
        DataType: 'String',
        StringValue: 'e0a01769-c8ed-4090-bd24-9c1b2241fb75',
      },
      method: {
        DataType: 'String',
        StringValue: 'backup-full',
      },
    };
    const result = await sqs.sendMessage({ QueueUrl: queueUrl, MessageBody: body, MessageAttributes: attributes });
    expect(result.MessageId).toBe('3bd2e3f9-4a3d-4964-85f7-b2770d7c54cf');
  });

  it('should receive a message', async () => {
    const receiveMessageRequest = {
      QueueUrl: queueUrl,
      MessageAttributeNames: ['All'],
      AttributeNames: ['All'],
      VisibilityTimeout: 20,
      WaitTimeSeconds: 20,
      MaxNumberOfMessages: 1,
    };
    const messages = await sqs.receiveMessage(receiveMessageRequest);
    if (messages.Messages) {
      const message = messages.Messages[0];
      expect(message.MessageId).toBe('3bd2e3f9-4a3d-4964-85f7-b2770d7c54cf');
      expect(message.ReceiptHandle).toBe('AQEBjjMNBxeBE1mJRK1SUZ+KIjv2JU5S35nW4iP5TIwm0fuMnCvtwmQNndO3YlIAgizEvgUfDw9Jr4w1Fv4SUBnKEHgWr+e2f84ASvSP0M1OgPGWMMQmzTrkUx5Cr3RcGmqdrEbRwFugejmfMnd3I4q+aFsXqiG0pWbxYOpYxbirdYyy9Z23QC0x3apeI2+mDOi02VOir0f9wlNTjp+TnK0MhHW8Tz0mFQDMnBy23poD/ZWFf6QaMX46psojVmK20h+jjjDG5XJ/WUkdHI4LU+1FZeAPpeDuIo8HJRJzLsBooRhH85pfTLTPR3sCFEnPmEOpgI8J/Jj36Ll2rdxFAGlaegDcZVgA5sR27gg3lWy8axfgPKAXIlw0Bri+ILusVhst');
      expect(message.Body).toBe('Process method for account');
      expect(message.MessageAttributes?.accountId.StringValue).toBe('e0a01769-c8ed-4090-bd24-9c1b2241fb75');
      expect(message.MessageAttributes?.method.StringValue).toBe('backup-full');
    } else {
      expect(true).toBe(false);
    }
  });

  it('should delete a message', async () => {
    const receiptHandle = 'AQEBjjMNBxeBE1mJRK1SUZ+KIjv2JU5S35nW4iP5TIwm0fuMnCvtwmQNndO3YlIAgizEvgUfDw9Jr4w1Fv4SUBnKEHgWr+e2f84ASvSP0M1OgPGWMMQmzTrkUx5Cr3RcGmqdrEbRwFugejmfMnd3I4q+aFsXqiG0pWbxYOpYxbirdYyy9Z23QC0x3apeI2+mDOi02VOir0f9wlNTjp+TnK0MhHW8Tz0mFQDMnBy23poD/ZWFf6QaMX46psojVmK20h+jjjDG5XJ/WUkdHI4LU+1FZeAPpeDuIo8HJRJzLsBooRhH85pfTLTPR3sCFEnPmEOpgI8J/Jj36Ll2rdxFAGlaegDcZVgA5sR27gg3lWy8axfgPKAXIlw0Bri+ILusVhst';
    const deleteMessageRequest = {
      QueueUrl: queueUrl,
      ReceiptHandle: receiptHandle,
    };
    const result = await sqs.deleteMessage(deleteMessageRequest);
    expect(result).toStrictEqual({});
  });
});
