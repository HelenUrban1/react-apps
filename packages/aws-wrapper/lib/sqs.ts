import type AWS from 'aws-sdk';

export default class SQS {
  sqs: AWS.SQS;

  constructor(sqs: AWS.SQS) {
    this.sqs = sqs;
  }

  sendMessage = async (sendMessageRequest: AWS.SQS.SendMessageRequest) => {
    return this.sqs.sendMessage(sendMessageRequest).promise();
  };

  receiveMessage = async (receiveMessageRequest: AWS.SQS.ReceiveMessageRequest) => {
    return this.sqs.receiveMessage(receiveMessageRequest).promise();
  };

  deleteMessage = async (deleteMessageRequest: AWS.SQS.DeleteMessageRequest) => {
    return this.sqs.deleteMessage(deleteMessageRequest).promise();
  };
}
