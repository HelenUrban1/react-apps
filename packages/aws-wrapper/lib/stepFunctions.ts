import type AWS from 'aws-sdk';
import type { SendTaskSuccessInput, SendTaskFailureInput } from 'aws-sdk/clients/stepfunctions';

export default class StepFunctions {
  stepFunctions: AWS.StepFunctions;

  constructor(stepFunctions: AWS.StepFunctions) {
    this.stepFunctions = stepFunctions;
  }

  sendTaskSuccess = async (sendTaskSuccessInput: SendTaskSuccessInput) => {
    return this.stepFunctions.sendTaskSuccess(sendTaskSuccessInput).promise();
  };

  sendTaskFailure = async (sendTaskFailureInput: SendTaskFailureInput) => {
    return this.stepFunctions.sendTaskFailure(sendTaskFailureInput).promise();
  };
}
