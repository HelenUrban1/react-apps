import type AWS from 'aws-sdk';

export default class S3 {
  s3: AWS.S3;

  constructor(s3: AWS.S3) {
    this.s3 = s3;
  }

  // return a max of 1000 objects by default
  listObjects = async (bucket: string) => {
    const listObjectsV2Request = {
      Bucket: bucket,
    };
    return this.s3.listObjectsV2(listObjectsV2Request).promise();
  };

  getObject = async (bucket: string, key: string) => {
    const getObjectRequest = {
      Bucket: bucket,
      Key: key,
    };
    return this.s3.getObject(getObjectRequest).promise();
  };

  copyObject = async (bucket: string, key: string, source: string) => {
    const getObjectRequest = {
      CopySource: source, //  "/sourcebucket/sourceKeyName"
      Bucket: bucket, // destination bucket
      Key: key, // destination targetKeyName
    };
    return this.s3.copyObject(getObjectRequest).promise();
  };

  upload = async (bucket: string, key: string, file: AWS.S3.Body, tags: string) => {
    // Typescript won't allow these to be undefined, but calling from js can
    const Body = file || '';
    const Tagging = tags || '';
    const putObjectRequest = {
      Key: key,
      Bucket: bucket,
      Body,
      Tagging,
      ServerSideEncryption: 'aws:kms',
    };

    return this.s3.upload(putObjectRequest).promise();
  };

  deleteObject = async (bucket: string, key: string) => {
    const deleteObjectRequest = {
      Key: key,
      Bucket: bucket,
    };
    return this.s3.deleteObject(deleteObjectRequest).promise();
  };
}
