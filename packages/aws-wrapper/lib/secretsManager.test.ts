import { secretsManager } from './index';

describe('AWS.SecretsManger', () => {
  it('should create database secret', async () => {
    await secretsManager.createDatabaseSecret('05eaad91-8d77-49d5-b1b3-18079df70c60', '05eaad91-8d77-49d5-b1b3-18079df70c60', '5yDbqx1r0GeRxHCFqeqVmiSBZxctTFT5');
    // expect(randomPassword).toBe('5yDbqx1r0GeRxHCFqeqVmiSBZxctTFT5');
  });
  it('should generate a random password', async () => {
    const randomPassword = await secretsManager.createRandomPassword();
    expect(randomPassword).toBe('5yDbqx1r0GeRxHCFqeqVmiSBZxctTFT5');
  });
  it('should get a database password', async () => {
    const databasePassword = await secretsManager.getDatabasePassword('05eaad91-8d77-49d5-b1b3-18079df70c60');
    expect(databasePassword).toBe('5yDbqx1r0GeRxHCFqeqVmiSBZxctTFT5');
  });

  it('should fetch secrets details', async () => {
    const result = await secretsManager.describeSecret('05eaad91-8d77-49d5-b1b3-18079df70c60');
    expect(result).toBe('05eaad91-8d77-49d5-b1b3-18079df70c60');
  });
});
