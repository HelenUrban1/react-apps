import type AWS from 'aws-sdk';
import type { GetSecretValueRequest } from 'aws-sdk/clients/secretsmanager';

interface Credentials {
  Username: string;
  Password: string;
}

export default class SecretsManager {
  secretsManager: AWS.SecretsManager;

  constructor(secretsManager: AWS.SecretsManager) {
    this.secretsManager = secretsManager;
  }

  createDatabaseSecret = async (accountId: string, tenantId: string, password: string) => {
    // https://docs.aws.amazon.com/AWSJavaScriptSDK/latest/AWS/SecretsManager.html#createSecret-property
    const credentials: Credentials = {
      Username: tenantId,
      Password: password,
    };

    const createSecretRequest = {
      Name: accountId,
      SecretString: JSON.stringify(credentials),
    };
    await this.secretsManager.createSecret(createSecretRequest).promise();
  };

  createRandomPassword = async () => {
    // https://docs.aws.amazon.com/AWSJavaScriptSDK/latest/AWS/SecretsManager.html#getRandomPassword-property
    // Generates a random password that is 32 characters and contains:
    // Uppercase, lowercase, numbers and punctuation
    const result = await this.secretsManager.getRandomPassword().promise();
    return result.RandomPassword;
  };

  getDatabasePassword = async (accountId: string) => {
    // https://docs.aws.amazon.com/AWSJavaScriptSDK/latest/AWS/SecretsManager.html#getSecretValue-property
    const getSecretValueRequest: GetSecretValueRequest = {
      SecretId: accountId,
    };
    const result = await this.secretsManager.getSecretValue(getSecretValueRequest).promise();
    if (!result || !result.SecretString) {
      return null;
    }
    try {
      const credentials: Credentials = JSON.parse(result.SecretString);
      return credentials.Password;
    } catch (error) {
      throw new Error(`Tried to parse string ${result.SecretString}`);
    }
  };

  describeSecret = async (tenantId: string) => {
    const describeSecretRequest = {
      SecretId: tenantId,
    };
    const secret = await this.secretsManager.describeSecret(describeSecretRequest).promise();
    return secret.Name;
  };
}
