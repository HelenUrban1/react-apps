module.exports = {
  printWidth: 1000,
  singleQuote: true,
  tabWidth: 2,
  jsxBracketSameLine: false,
  semi: true,
  useTabs: false,
  bracketSpacing: true,
  arrowParens: 'always',
  endOfLine: 'lf',
  requirePragma: false,
};
