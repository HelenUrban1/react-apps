#!/usr/bin/env python3

import argparse
import json
import yaml
import sys

"""
Usage:
	./compose_task_def_envs.py -i ../docker-compose.yml -s backend

Convert docker-compose.yml environment vars for a particular service to an ECS task def format

For example it turns:

environment:
  - NODE_ENV=production
  - IXC_DB_HOST=postgres
  [...]

To a format that we can paste into an AWS ECS task definition, such as:

                {
                    "name": "NODE_ENV",
                    "value": "production"
                },
                {
                    "name": "IXC_DB_HOST",
                    "value": "postgres"
                },
"""


def main():
    parser = argparse.ArgumentParser(
        description='Pull environment names from a docker compose service and format it for ECS task defs')
    parser.add_argument(
        "-i",
        "--input",
        default="./docker-compose.yml",
        help="Path to docker-compose.yml file"
    )
    parser.add_argument(
        "-s",
        "--service",
        required=True,
        help="docker compose service name (found under the `services` property)"
    )

    args = parser.parse_args()
    with open(args.input) as y:
        data = yaml.load(y, Loader=yaml.FullLoader)
        task_def = {'containerDefinitions': [{'environment': []}]}
        if isinstance(data['services'][args.service]['environment'], list):
            for i in data['services'][args.service]['environment']:
                dict_el = {'name': '', 'value': ''}
                dict_el['name'], dict_el['value'] = i.split('=')
                task_def['containerDefinitions'][0]['environment'].append(
                    dict_el)
        else:
            for key, val in data['services'][args.service]['environment'].items():
                dict_el = {'name': '', 'value': ''}
                dict_el['name'] = key
                dict_el['value'] = val
                task_def['containerDefinitions'][0]['environment'].append(
                    dict_el)
        print(json.dumps(task_def, indent=4, sort_keys=True))


if __name__ == '__main__':
    sys.exit(main())
