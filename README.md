# IXC Mono
This is a monolithic repository that contains the main applications that run InspectionXpert Cloud. Edit 5.

## Table of Contents
1. [Pre-requisites](#markdown-header-pre-requisites)   
2. [Install qc-essentials](#markdown-header-header-install)
    1. [Clone Repository](#markdown-header-clone-repositories)
    2. [Install CA Certificate](#markdown-header-install-ca-certificate)
    3. [Environment Variables](#markdown-header-environment-variables)
    4. [Install AWS Wrapper](#markdown-header-install-aws-wrapper)
    5. [Install Queue Processor](#markdown-header-install-queue-processor)
    6. [Install Backup Trigger](#markdown-header-install-backup-trigger)
    7. [Install qc-essentials Frontend](#markdown-header-install-qc-essentials-frontend)
    8. [Install qc-essentials Backend](#markdown-header-install-qc-essentials-backend)
        1. [Install Backend](#markdown-header-install-the-backend)
        2. [Install Database](#markdown-header-install-postgres-database)
        3. [Setup Cognito Locally](#markdown-header-setup-cognito-locally)
    9. [Run QC Essentials](#markdown-header-run-qc-essentials)
    10. [Infrastructure CLI Access](#markdown-header-infrastructure-cli-access)
    11. [Connecting to Remote Databases](#markdown-header-connecting-to-remote-databases)
    12. [Additional Optional Services](#markdown-header-additional-optional-services)
        1. [Install Engineering Drawing Analyzer](#markdown-header-install-the-engineering-drawing-analyzer)
3. [Run tests](#markdown-header-running-tests)
    1. [Cypress](#markdown-header-cypress)
        1. [Install Cypress](#markdown-header-installing-cypress)
        2. [Running Cypress](#markdown-header-running-cypress)
        3. [Further Automation Info](#markdown-header-further-cypress-automation-info)
    2. [Jest](#markdown-header-jest)
4. [Release to Production](#markdown-header-releasing-to-production)
5. [Hotfix Production](#markdown-header-hot-fixing-production)
6. [Back Office Integration](#markdown-header-back-office-integration)
    1. [Hubspot](#markdown-header-hubspot)
    2. [Chargify](#markdown-header-chargify)
    3. [Manage Test Data](#markdown-header-managing-test-data-in-back-office-systems)
    4. [Search Back Office Records](#markdown-header-searching-for-back-office-records)
    5. [Purge Back Office Records](#markdown-header-purging-for-back-office-records)
7. [Common Install Errors & Fixes](#markdown-header-common-install-error--fixes)


# Pre-requisites
To install IXC Mono on your machine you will need to have the following installed:

- [NVS Node Switcher](https://github.com/jasongin/nvs/releases) (Add Node 18.13.0 which will install npm 8.19.3) This is the required version to run all QC Essentials services.
Run in command prompt:
```
nvs add node/18.13.0
nvs link 18.13.0
```
- [Docker Desktop](https://www.docker.com/products/docker-desktop/) (Choose WSL instead of hyper-v)
- [Git](https://git-scm.com/downloads) (Install as admin & bash too)
- Git UI ([Fork](https://git-fork.com/) or [Sourcetree](https://www.sourcetreeapp.com/))
- [PgAdmin](https://www.pgadmin.org/)
- [Python 3.10](https://www.python.org/downloads/release/python-3100/).  Ensure checkbox ‘Add python to environment variables’ is selected.

Once python is installed, install the `awsume` package. 
```
pip install awsume
```

- [Visual Studio Code](https://code.visualstudio.com/) (SonarLint, Eslint & Prettier extensions installed)
- [Visual Studio](https://visualstudio.microsoft.com/downloads/) (Requires v2017 and v2022 With the Desktop development C++ workload to account for different version of Node being used)
- [AWS CLI](https://docs.aws.amazon.com/cli/v1/userguide/cli-chap-install.html)

You will also need access to the following services:

- InspectionXpert AWS
- [JFrog](https://ideagen.jfrog.io/ui/repos/tree/General/ix-docker-dev-local)
- [Gruntworks](https://gruntwork.io/) 
- LastPass Shared Folders

## Change npm shell
You will need to change the default shell npm uses as many of the commands are built for bash.

```
npm config set script-shell "%PROGRAMFILES%\\git\\bin\\bash.exe"
```

If you want to change this back then run this command:

```
npm config delete script-shell
```

## Add ixc-local to Hosts
In order to access the ixc-local domain locally you need to add the following lines at the end of your `Hosts` file in Windows. Usually in `%SYSTEMROOT%\System32\drivers\etc`

```
127.0.0.1 ixc-local.com
127.0.0.1 www.ixc-local.com
```

# Install
The following instructions are for installing all services required to run InspectionXpert Cloud locally. For all install commands it is assumed you have a command prompt terminal open at the root of the repo.

## Clone Repository
Prior to cloning the repo, setup a ssh key for Ideagen Bitbucket:
```
ssh-keygen -t ed25519 -C "you@ideagenplc.com"
cd ~/.ssh
more id_rsa.pub
```
Copy the contents and upload to Bitbucket by: 

Settings > Personal Bitbucket Settings > SSH Keys > Add Key > Paste Key

```
git clone git@bitbucket.org:ideagendevelopment/qc-essentials.git
```

## Install CA Certificate
We need to install the CA certificate to allow https on localhost. The CA certificate is located at the root of the repository and instructions to install can be found [here](https://community.spiceworks.com/how_to/1839-installing-self-signed-ca-certificate-in-windows)

> This could be done using the cert manager command `certmgr /add /s /r localMachine ixc-local.com.crt`

## Environment Variables
In LastPass there is an item for environment variables required for qc-essentials. Download each file and move them to the root of the repo. Then run following command:
```
MoveEnvironmentFiles.bat
```

## Install Aws-Wrapper
Aws-Wrapper is a service that allows us to use AWS services locally for development. To install and build run the following:

```
cd packages/aws-wrapper
npm ci
npm run build
cd ../..
```

## Install Queue Processor
Run the following commands to install and build:
 
```
cd services/queue-processor
npm ci
npm run build
cd ../..
```

## Install Backup Trigger
Run the following commands to install:
 
```
cd services/backup-trigger
npm ci
cd ../..
```

## Install qc-essentials Frontend
qc-essentials Frontend is a React application to install run the following commands:

```
cd frontend
npm ci
npm run copy-webviewer
cd ..
```

## Install qc-essentials Backend
qc-essentials Backend is a Node.js application that runs an Express & GraphQL API. The backend also is dependant on you using AWS Cognito to authenticate, so before we can run the backend we need to set Cognito up.

## Install the Backend
To install the backend run this command:

```
cd backend
npm ci
cd ..
```

## Install Postgres Database
To setup the database, ensure Docker Desktop is logged in and running.
You need to run the following commands:

```
cd backend/storage
docker volume create postgres-volume
docker compose up
```

Open new terminal and run:

```
cd backend
npm run db:reset-and-seed:development
```
> IMPORTANT: This command seemingly fails at least once, usually twice before succeeding. Every error is different and erroneous so just re-run the command if this happens.

This will create a Postgres container with the main and tenant databases with seed data.

To view and manage the database you need to use PgAdmin. To connect to the database do the following on PgAdmin:

- Add New Server
- Name: ixc-local
- Host name: localhost
- Password: `BJJqJ22n77}7=usGbs**Y]MXYeLpef`

Everything else can be left as default. 

### Setup Cognito Locally
Create an AWS access key with an associated secret key to access the AWS security account via the aws-cli, Terraform, etc.

Go to IAM - Users -> [select your username] -> Security Credentials (tab) -> Access keys -> Command Line Key -> Create access key.

> IMPORTANT : It is critical that you protect this data and never share it with anyone.

#### Create the config files
Using the AWS CLI we can create the base InspectionXpert profile. To do so run the following command: 
```
aws configure --profile inspectionxpert
```
You will need to populate the fields with:
 - Your access key and secret key
 - Region: us-east-1
 - Output: leave as None

Then download the config file from QC Essentials AWS profiles in LastPass. Substitute `<AWS Username>` with your username. Save the file and and remove the `.txt` extension. Copy the config file to `%HOMEPATH%/.aws` (this will overwrite the config file already there). 

You can now obtain a session token by running the following command:
```
set AWS_PROFILE=inspectionxpert
awsume ix-dev --role-duration 40000
```

### Run QC Essentials

#### Database
Run the Database container from Docker Desktop.

#### Frontend

Run this command from the frontend directory
```
cd frontend
npm run start
```
> NOTE: npm start is complete when you see one of the following in your console: 'Compiled with warnings' or 'Files successfully emitted'. The site will now be available at https://ixc-local.com:3000/. This will be a blank page until the backend is also running

#### Backend
Ensure you are authenticated with AWS as per the cognito install section. If you do not already have a session token, run awsume with the following from the backend directory addtional argument to provide 8 hours of access:
```
awsume ix-dev --role-duration 28000
```
>NOTE: You will have to run awsume every time you start the backend in a new command window.

Then in the same command window, run the following command:
 
```
cd backend
npm start
```
> NOTE: npm start is complete when you see a debug line with 'S3mock saved file' in your console. You will now be able to load the login page

Once both are running you should be able to visit https://ixc-local.com:3000/ and login with the qualitycontrol.essentials@gmail.com from LastPass.  

## Infrastructure CLI Access
To access the AWS Infrastructure through the command line see this documentation on Confluence, [Infrastructure Access](https://ideagen.atlassian.net.mcas.ms/wiki/spaces/IX/pages/39155040257/Infrastructure+Access)

## Connecting to Remote Databases
To connect to a remote database on AWS follow the documentation on Confluence, [Connecting to Remote Databases](https://ideagen.atlassian.net.mcas.ms/wiki/spaces/IX/pages/39206748173/Connect+to+hosted+AWS+databases)

# Additional Optional Services

The EDA can be run locally for development, the steps to do so are detailed below, however the mono repo is able to communicate with the hosted versions of both services so running them locally all the time is not strictly necessary.

## Install the Engineering Drawing Analyzer
### Consuming
The engineering drawing analyzer (EDA) docker image is hosted on JFrog so to install it you just need to pull the latest version and start the container. 

Go to [JFrog](https://ideagen.jfrog.io/ui/repos/tree/General/ix-docker-dev-local/eda) and follow the *Configure* section in the *Set Me Up* instructions to generate a token. 

To pull the image from JFrog you need to run the following commands:

```
docker login ideagen.jfrog.io
```
Enter your email and then use the generated token as the password.

Once authenticated run

```
docker pull ideagen.jfrog.io/ix-docker-dev-local/eda:<EDA_VERSION>
```

EDA_VERSION comes from jfrog, and you can find this by navigating down the tree from *ix-docker-dev-local* to *eda*, and from there you are able to see the EDA_VERSION.

Then run the image from Docker Desktop with these parameters:

- Container Name: EDA
- Host Port: 5000

The container should now be running and ready.

### Developing
If you need to rebuild the docker images after new development then you should run the following command from the root of the repository:

```
docker compose up
```

#### Development server
```
npm ci
npm start
```


# Running Tests

## Cypress

Cypress handles End-to-End and integration tests for the app.

### Installing Cypress
#### Prerequisites
To install the Cypress Framework, you will need the following:

- A local copy of the repository
- An IDE such as Visual Studio Code
- Node.js, the below versions are supported (follow IT recommendation for secure version)
    - 14.x
    - 16.x
    - 18.x and above

For more information on other pre-requisites see https://docs.cypress.io/guides/getting-started/installing-cypress#System-requirements 

#### Installation
To run the cypress Framework you will need to install Cypress. There are multiple ways to do this however it is recommended that this be done via npm as Cypress is versioned and it simplifies running cypress in Continuous Integration. 

To install cypress use

```
npm install cypress 
```

Further install details can be found here: 
https://docs.cypress.io/guides/getting-started/installing-cypress#Installing  


### Running Cypress
There are 2 ways in which to use Cypress automation theses are listed below: 

1.	Within the Cypress Test Runner
2.	Headless
 
Both can be accessed either via the command line if you are in the directory or within a Visual Studio  terminal

Below are the current list of scripts for the Cypress Automation:
To run in the Cypress Test Runner run one of the following for the required environment you will then be able to select the Spec file required:

```
npm run cy:DEV:open
npm run cy:QA:open
npm run cy:Stage:open
```



To run headless run one of the following for the required environment, this will run all of the spec files:
```
npm run cy:DEV
npm run cy:QA
npm run cy:Stage
```

To run selected spec filed headless you will need to use:
```
npx cypress run --config baseUrl=<ENTER EVNIROMENT URL HERE> --spec "<ENTER SPEC FILE PATH HERE>
eg:
npx cypress run --config baseUrl=https://www.ixc-qa.com/ --spec "cypress\e2e\tests\Authentication\Login.cy.js"
```

#### Run Cypress Automation on a local build
Prerequisite 
To be able to run the automation you will be required to create the login data that is used by seeding into the db or by creating it via the UI. 
Alternatively you can create your own users matching the user types required however will need to amend the login details in the cypress.config.js file. 

To run the cypress automation on a local build you will need to update one of following scripts. You will need insert local host URL and remove the // within the \qc-essentials\package.json file
To run headless: 
```
//"cy:Local": "npx cypress run --config baseUrl=insert dev local URL here",
```

To open the Cypress Test Runner:
```
//"cy:Local:open": "npx cypress open --config baseUrl=insert dev local URL here",
```
Run on local build scripts
Once the prerequisites have been done one of the following scripts can be ran:
To open the test runner the Local environment 
```
npm run cy:Local:open
```
To run headless using the Local environment 
```
npm run cy:Local
```

### Further Cypress Automation info
For further information on the Cypress automation see
https://ideagen.atlassian.net/wiki/spaces/IX/pages/39084490753/Test+Automation 

## Jest

Jest handles unit tests for the App Frontend and Backend. To run the jest tests, use `npm run test` from within the associated folder.

# Releasing to Production

```
...release ready...
git flow release start 0.1.0
...update version in all package.json files
...update CHANGELOG.md
git flow release finish 0.1.0
git push origin master
git push origin develop
git push origin --tags
```

# Hot-fixing Production

```
...BUG IN PROD, Need HOTFIX...
git flow hotfix start hotfix_branch
...develop, review, done...
git flow hotfix finish hotfix_branch
```

# Back Office Integration

## Hubspot

In addition to using the standard Hubspot API to create and manage contacts, companies, and deals, InspectionXpert created a Hubspot App that allows this app to post timeline updates to records in Hubspot. The app is called [IX HubspotBridge](https://app.hubspot.com/developer/8445622/application/233345)
and is installed in both the production and test Hubspot accounts.

The events that show up in the timeline, are defined in backend/src/clients/serviceAdapters/timelineEventTypes and synchronized by backend/src/clients/serviceAdapters/hubspotCRMAdapter.js.

## Chargify

InspectionXpert uses [Chargify](https://app.chargify.com/sellers/52480-inspectionxpert-corporation) to manage subscriptions.

# Managing Test Data in Back Office Systems

A pair of backend npm scripts in the backend allow developers to search for and purge test data in IXC's back office systems.

The scripts accept a `-t` flag to accept search text. If search text is not provided, the scripts will use the value
defined in ENV variable `IXC_PERSONAL_TEST_DATA_LABEL` (which can be added to backend/.env). This allows developers to
purge data that contains their personal testing text pattern without having to pass search text.

When run, both commands will list the CRM Companies, Contacts, and Deals, as well as the Billing System's Customer and
Subscriptions associated with CRM records that match the search text.

The purge command will also list the results of the delete request for each object.

## Searching for Back Office Records

The following command will search for records that contain the string defined in `PERSONAL_TEST_DATA_LABEL`:

```
npm run backoffice:search
```

The following command will search for records that contain text "My Other Label":

```
npm run backoffice:search -- -t "My Other Label"
```

## Purging for Back Office Records

The following command will DELETE records that contain the string defined in `PERSONAL_TEST_DATA_LABEL`:

```
npm run backoffice:purge
```

The following command will DELETE records that contain text "My Other Label":

```
npm run backoffice:purge -- -t "My Other Label"
```

# FAQ's
## Database migration commands

```
npm run db:check-migrations:development test primary
```
This will undo all migrations back to `20210105104815-review-tasks.js`, and then run them again:

```
npm run start:clean
```
This command will run the migrations command and also start the development server once migrations are complete.

# Common Install Error & Fixes
#### Valueerror: invalid mode: 'ru' while trying to load binding.gyp 
This error occurs when installing some services and the fix is to uninstall any other Python versions apart from 3.10 and ensure that Python 3.10 is in your `path` system environment variable. 

#### Error with psutil when setting up cognito
Sometimes you might get an error from a package psutil, usually to resolve this error just uninstall and reinstall the package using pip. Eg pip uninstall psutil pip install psutil

#### ERR! find VSfind VS msvs_version not set from command line or npm config
This error occurs when installing the backend and the fix is to ensure that the Desktop development with C++ workload in installed on Visual Studio. 

Further help can be found at [Here](https://stackoverflow.com/questions/57879150/how-can-i-solve-error-gypgyp-errerr-find-vsfind-vs-msvs-version-not-set-from-c)

#### npm ERR! Failed to execute native-metrics install: No pre-built artifacts to download for your OS/architecture.
This error occurs when installing the backend and the fix is to clear npm config and set VCINSTALLDIR to your 2017 installation of Visual Studio.

Further help can be found [here](https://stackoverflow.com/a/75699040).

#### Permission denied for schema public
When applying migrations to the database sometimes you might get this error. The fix is to change the username and password used to access the database in the backend `.env` file. 

```
IXC_DB_UN=postgres
IXC_DB_PW=BJJqJ22n77}7=usGbs**Y]MXYeLpef
IXC_DB_CONTAINER=ixc_mono
IXC_DB_HOST=localhost
IXC_DB_ADMIN_UN=postgres
IXC_DB_ADMIN_PW=BJJqJ22n77}7=usGbs**Y]MXYeLpef
```
This will use the superuser to apply migrations to the database.

#### Dubious ownership
This error occurs when windows detects that the repository of qc-essentials is owned by multiple users on the device. The fix for this is to run this command:

```
git config --global --add safe.directory <PATH TO REPO>
```
This will use the superuser to apply migrations to the database.

#### ENOENT error no 4058
This error occurs when services that npm relies on to pull and install certain packages are not where they are expected to be on your machine. For example in the case listed below, Git is installed and is available on the users PATH, but the terminal is not picking the installation up from C:\Program Files as it expects:

```
npm ERR! code ENOENT
npm ERR! syscall spawn C:\Program Files\git\bin\bash.exe
npm ERR! path C:\Repos\qc-essentials\frontend\node_modules\canvas
npm ERR! errno -4058
```

To resolve this error, simply install the missing service to the location the terminal specifies, or move an existing installtion there.

#### Error : password authentication failed for user 
This Error can occur during database seeding, when a created user doesn't have a viable password set to validate against.

To resolve this error, open pgAdmin -> navigate to Login/Group Roles -> open the properties of ixc_account_development_{account number} -> set password to IXC_ACCOUNT_DB_PW from the .env file -> save

This should assign the created user the password it is meant to already have, and therefore allow validation against it in future.

#### Error cannot find module distutils
This error occurs when a user has multiple versions of python installed and available on their PATH.
By default python will always seek to use the most recent version of itself that is has available, unfortunately a package we depend on (@newrelic/native-metrics) requires that we operate on version 3.10 of Python exclusively, running newer versions of python will throw this error.

To resolve this error, simply remove excess versions of Python from your machine so that only version 3.10 is installed and set on your PATH.

# Common Runtime Errors
#### Part never loads on parts page
Fresh installs might encounter this error where individual parts are unable to be displayed on the parts page and therefore unable to be ballooned.
The fix is as simple as running the following on your frontend:

```
npm run copy-webviewer
```
As was described in the frontend installation instructions.

# Errors when attempting to edit a report template
This occurs when the local machine doesn't have the correct permissions on the backend/data folder.
- Edit the permissions on the data folder to allow the /Users group to create folders and files.