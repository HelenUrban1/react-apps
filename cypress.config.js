const { defineConfig } = require('cypress');

baseUrl: 'https://www.ixc-qa.com/',
  (module.exports = defineConfig({
    projectId: 'zmqqy6',
    chromeWebSecurity: false,
    experimentalStudio: true,
    defaultCommandTimeout: 50000,
    viewportWidth: 1920,
    viewportHeight: 1080,

    retries: {
      runMode: 3,
      openMode: 2,
    },

    e2e: {
      setupNodeEvents(on, config) {},
    
    // The pattern to match the spec files against
    specPattern: 'cypress/e2e/**/*.cy.{js,jsx,ts,tsx}',
    // The pattern to exclude spec files
    //uncomment the below line to exclude the spec files
    excludeSpecPattern: [
      
      // '**/Add New Part/**', //Parent folder
      // '**/**/Balloon Styles.cy.js', //Spec file
      // '**/**/CancelDialogBox.cy.js', //Spec file
      // '**/**/Document Information.cy.js', //Spec file
      // '**/**/Part Creation Wizard.cy.js', //Spec file
      // '**/**/Part Information.cy.js', //Spec file
      // '**/**/Part-Grid.cy.js', //Spec file
      // '**/**/Part-tolerance.cy.js', //Spec file
      // '**/**/Parts.cy.js', //Spec file
      // '**/**/Title bar revisions.cy.js', //Spec file
      // '**/**/Uploads.cy.js', //Spec file

      // '**/Authentication/**', //Parent folder
      // '**/**/Forgotten Password.cy.js', //Spec file
      // '**/**/Login.cy.js', //Spec file
      // '**/**/Request Account.cy.js', //Spec file

      // '**/Configurations/**', //Parent folder
      // '**/**/Balloon Presets.cy.js', //Spec file
      // '**/**/Feature Types.cy.js',  //Spec file
      // '**/**/Inspection Properties.cy.js', //Spec file
      // '**/**/Tolerance Presets.cy.js', //Spec file

      // '**/Parts List/**', //Parent folder
      // '**/**/PartsList.cy.js', //Spec file

      // '**/Permissions/**', //Parent folder
      // '**/**/Admin permissions.cy.js', //Spec file
      // '**/**/Billing User.cy.js', //Spec file
      // '**/**/ITAR Restrictions.cy.js', //Spec file
      // '**/**/LoginAndLogoutJourney.cy.js', //Spec file
      // '**/**/Planner with billing.cy.js', //Spec file
      // '**/**/Planner-Permissions.cy.js', //Spec file
      // '**/**/Reviewer User.cy.js', //Spec file

      // '**/Seats/**', //Parent folder
      // '**/**/Invite Users.cy.js', //Spec file
      // '**/**/Seat Management.cy.js', //Spec file

      // '**/Settings/**', //Parent folder
      // '**/**/Account Settings.cy.js', //Spec file
      // '**/**/Company Settings.cy.js', //Spec file
      // '**/**/Delete Account.cy.js', //Spec file

      // '**/Subscriptions/**',
      // '**/**/Subscription modal.cy.js', //Spec file
      // '**/**/Trial user.cy.js', //Spec file

      // '**/Templates/**', //Parent folder
      // '**/**/Templates.cy.js', //Spec file
    ]
    },
  

    env: {
      adminUsername: 'iqcessentials+qa-admin@gmail.com',
      adminPassword: 'Xd#NjJ9*4QYfOX73',
      plannerUsername: 'iqcessentials+qa-planner@gmail.com',
      plannerPassword: '6y!1TUDkn96qX^1F',
      plannerwithbillingUsername: 'iqcessentials+qa-plannerwithbilling@gmail.com',
      plannerwithbillingPassword: 'eQaf5^XhjMAuhk5v',
      reviewerUsername: 'iqcessentials+qa-reviewer@gmail.com',
      reviewerPassword: 'f8!2@fTRtfYpI6dh',
      billingUsername: 'iqcessentials+qa-billing@gmail.com',
      billingPassword: '9UZ#Wp%*!^9oq5CU',
      inactiveUsername: 'iqcessentials+qa-inactive@gmail.com',
      inactivePassword: '6TLF@w2v%U%q3lYv',

      devAdminUsername: 'iqcessentials+dev-admin@gmail.com',
      devAdminPassword: 'Xd#NjJ9*4QYfOX73',
      devPlannerUsername: 'iqcessentials+dev-planner@gmail.com',
      devPlannerPassword: '6y!1TUDkn96qX^1F',
      devPlannerwithbillingUsername: 'iqcessentials+dev-plannerwithbilling@gmail.com',
      devPlannerwithbillingPassword: 'eQaf5^XhjMAuhk5v',
      devReviewerUsername: 'iqcessentials+dev-reviewer@gmail.com',
      devReviewerPassword: 'f8!2@fTRtfYpI6dh',
      devBillingUsername: 'iqcessentials+dev-billing@gmail.com',
      devBillingPassword: '9UZ#Wp%*!^9oq5CU',
      devInactiveUsername: 'iqcessentials+dev-inactive@gmail.com',
      devInactivePassword: '6TLF@w2v%U%q3lYv',

      stageAdminUsername: 'iqcessentials+stage-admin@gmail.com',
      stageAdminPassword: 'Xd#NjJ9*4QYfOX73',
      stagePlannerUsername: 'iqcessentials+stage-planner@gmail.com',
      stagePlannerPassword: '6y!1TUDkn96qX^1F',
      stagePlannerwithbillingUsername: 'iqcessentials+stage-plannerwithbilling@gmail.com',
      stagePlannerwithbillingPassword: 'eQaf5^XhjMAuhk5v',
      stageReviewerUsername: 'iqcessentials+stage-reviewer@gmail.com',
      stageReviewerPassword: 'f8!2@fTRtfYpI6dh',
      stageBillingUsername: 'iqcessentials+stage-billing@gmail.com',
      stageBillingPassword: '9UZ#Wp%*!^9oq5CU',
      stageInactiveUsername: 'iqcessentials+stage-inactive@gmail.com',
      stageInactivePassword: '6TLF@w2v%U%q3lYv',
    },

    emailTests: {
      plainaddress: 'plainaddress',
    },
  }));
