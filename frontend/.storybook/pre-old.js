module.exports = function ({ config }) {
  config.module.rules.push({
    test: /\.stories\.js?$/,
    loaders: [require.resolve('@storybook/source-loader')],
    enforce: 'pre',
  });

  return config;
};

module.exports = [
  {
    name: '@storybook/preset-create-react-app',
    options: {
      craOverrides: {
        fileLoaderExcludes: ['less'],
      },
    },
  },
  {
    name: '@storybook/addon-docs/preset',
    options: {
      configureJSX: true,
    },
  },
];
