import { configure, addDecorator, addParameters } from '@storybook/react';
import requireContext from 'require-context.macro';
import React from 'react';
import { GlobalStyle } from '../src/styleguide/shared/global';
import { withA11y } from '@storybook/addon-a11y';
import ixTheme from './ixTheme';
import 'antd/dist/antd.less';
// import '../src/overrides.less';
import '../src/styleguide/AntDesignCustomTheme.less';

addDecorator(withA11y);

addDecorator((story) => (
  <>
    <GlobalStyle />
    {story()}
  </>
));

addParameters({
  options: {
    theme: ixTheme,
  },
});

// automatically import all files ending in *.stories.js
configure([requireContext('../src', false, /Intro\.stories\.mdx/), requireContext('../src', true, /\.stories\.js/)], module);
