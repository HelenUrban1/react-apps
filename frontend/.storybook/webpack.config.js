const path = require('path');
const CopyPlugin = require('copy-webpack-plugin');
const fs = require('fs');
const lessToJs = require('less-vars-to-js');
const NodePolyfillPlugin = require('node-polyfill-webpack-plugin');

// override ant-design in storybook using our own less theme
const themeVariables = lessToJs(fs.readFileSync(path.join(__dirname, '../src/styleguide/AntDesignCustomTheme.less'), 'utf8'));

module.exports = {
  resolve: {
    fallback: {
      timers: require.resolve('timers-browserify'),
    },
  },
  module: {
    rules: [
      {
        loader: 'babel-loader',
        exclude: /node_modules/,
        test: /\.js$/,
        options: {
          presets: ['@babel/react'],
          plugins: [['import', { libraryName: 'antd', style: true }]],
        },
      },
      {
        test: /\.less$/,
        loaders: [
          'style-loader',
          'css-loader',
          {
            loader: 'less-loader',
            options: {
              javascriptEnabled: true,
              modifyVars: themeVariables,
            },
          },
          {
            loader: 'style-resources-loader',
            options: {
              patterns: [path.resolve(__dirname, 'src/styles/colors.less')],
            },
          },
        ],
        include: path.resolve(__dirname, '../'),
      },
    ],
  },
  plugins: [
    new CopyPlugin([
      {
        from: path.join(__dirname, '../node_modules/@pdftron/webviewer/public/'),
        to: path.join(__dirname, '../public/pdftronlib/'),
      },
      {
        from: path.join(__dirname, '../node_modules/@pdftron/webviewer/webviewer.min.js'),
        to: path.join(__dirname, '../public/pdftronlib/'),
      },
    ]),
    new NodePolyfillPlugin(),
  ],
};
