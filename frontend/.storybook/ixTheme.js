import { create } from '@storybook/theming';

export default create({
  base: 'light',

  colorPrimary: '#1b62e9',
  colorSecondary: '#606060',

  // UI
  appBg: '#F6F8FA',
  appContentBg: '#ffffff',
  appBorderColor: '#963cbd',
  appBorderRadius: 1,

  brandTitle: 'InspectionXpert',
  brandUrl: 'https://inspectionxpert.com',
  brandImage: '../public/images/Icon Logo.svg',
});
