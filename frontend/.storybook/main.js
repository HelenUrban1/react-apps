const fs = require('fs');
const lessToJs = require('less-vars-to-js');
const path = require('path');

// override ant-design in storybook using our own less theme
const themeVariables = lessToJs(fs.readFileSync(path.join(__dirname, '../src/styleguide/AntDesignCustomTheme.less'), 'utf8'));

module.exports = {
  stories: ['../src/**/*.stories.@(ts|js)'],
  addons: [
    {
      name: '@storybook/preset-create-react-app',
      options: {
        craOverrides: {
          fileLoaderExcludes: ['less'],
        },
      },
    },
    '@storybook/addon-actions',
    '@storybook/addon-links',
    '@storybook/addon-viewport',
    'storybook-addon-specifications',
    '@storybook/addon-storysource',
    '@storybook/addon-knobs',
    '@storybook/addon-a11y',
    '@storybook/addon-jest',
    '@storybook/addon-docs',
  ],

  webpackFinal: async (config) => {
    config.module.rules.push({
      loader: 'babel-loader',
      exclude: /node_modules/,
      test: /\.js$/,
      options: {
        presets: ['@babel/react'],
        plugins: [['import', { libraryName: 'antd', style: true }]],
      },
    });

    config.module.rules.push({
      test: /\.less$/,
      use: [
        'style-loader',
        'css-loader',
        {
          loader: 'less-loader',
          options: {
            javascriptEnabled: true,
            modifyVars: themeVariables,
          },
        },
        {
          loader: 'style-resources-loader',
          options: {
            patterns: [path.resolve(__dirname, 'src/styles/colors.less')],
          },
        },
      ],
      include: [path.resolve(__dirname, '../src/'), /[\\/]node_modules[\\/].*antd/],
    });

    return config;
  },
};
