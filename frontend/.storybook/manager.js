import { addons } from '@storybook/addons';
import ixTheme from './ixTheme';

addons.setConfig({
  theme: ixTheme,
});
