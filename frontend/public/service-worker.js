const CACHE = 'CACHE-0.0.0';

self.addEventListener('install', (event) => {
  console.log('Service Worker Installing...');
  event.waitUntil(
    caches
      .open(CACHE)
      .then(function (cache) {
        return fetch('files-to-cache.json')
          .then(function (response) {
            return response.json();
          })
          .then(function (files) {
            // console.log('[install] Adding files from JSON file: ', files);
            return cache.addAll(files);
          })
          .catch((e) => {
            console.log('Files could not be added to cache');
          });
      })
      .then(function () {
        // console.log('[install] All required resources have been cached;', 'the Service Worker was successfully installed!');
        return self.skipWaiting();
      })
  );
});

self.addEventListener('fetch', function (event) {
  event.respondWith(
    caches.match(event.request).then(function (response) {
      if (response) {
        // console.log('[fetch] Returning from Service Worker cache: ', event.request.url);
        return response;
      }

      // console.log('[fetch] Returning from server: ', event.request.url);
      return fetch(event.request);
    })
  );
});

self.addEventListener('activate', function (event) {
  // console.log('[activate] Activating service worker!');
  // console.log('[activate] Claiming this service worker!');
  event.waitUntil(self.clients.claim());
});
