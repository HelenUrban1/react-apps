// https://eszter.space/antd-theming/#fn-1

const path = require('path');

const CracoLessPlugin = require('craco-less');

module.exports = {
  jest: {
    configure(config) {
      config.transformIgnorePatterns = ['/node_modules/(?!antd|rc-pagination|rc-calendar|rc-tooltip)/.+\\.js$'];
      return config;
    },
  },
  // Adding Craco plugins

  plugins: [
    {
      plugin: CracoLessPlugin,
      options: {
        lessLoaderOptions: {
          lessOptions: {
            modifyVars: path.join(__dirname, 'src/styleguide/AntDesignCustomTheme.less'),
            javascriptEnabled: true,
          },
        },
      },
    },
  ],
};
