module.exports = {
  env: {
    browser: true,
    node: false,
    commonjs: true,
    es6: true,
    jest: true,
  },
  plugins: ['prettier', 'import'],
  extends: [
    'airbnb-typescript', //
    'prettier', // Uses eslint-config-prettier to disable ESLint rules from @typescript-eslint/eslint-plugin that would conflict with prettier
    'plugin:prettier/recommended', // Enables eslint-plugin-prettier and displays prettier errors as ESLint errors. Make sure this is always the last configuration in the extends array.
  ],
  parserOptions: {
    project: './tsconfig.json',
    tsconfigRootDir: __dirname,
    ecmaVersion: 2018, // Allows for the parsing of modern ECMAScript features
    sourceType: 'module', // Allows for the use of imports
    ecmaFeatures: {
      jsx: true, // Allows for the parsing of JSX
    },
  },
  settings: {
    react: {
      version: 'detect', // Tells eslint-plugin-react to automatically detect the version of React to use
    },
    'import/resolver': {
      node: {
        extensions: ['.js', '.ts', '.jsx', '.tsx'],
        moduleDirectory: ['node_modules', 'src/'],
      },
    },
  },
  rules: {
    'prettier/prettier': 'warn',
    'linebreak-style': ['warn', 'windows'],
    'react/prop-types': 0,
    'react/jsx-filename-extension': [1, { extensions: ['.js', '.jsx', 'ts', 'tsx'] }],
    'import/prefer-default-export': 0,
    '@typescript-eslint/no-useless-constructor': 0,
    '@typescript-eslint/no-throw-literal': 0,
    '@typescript-eslint/no-implied-eval': 0,
    '@typescript-eslint/default-param-last': 0,
    '@typescript-eslint/naming-convention': 0,
    '@typescript-eslint/no-unused-vars': 0,
    '@typescript-eslint/no-use-before-define': 0,
    '@typescript-eslint/return-await': 0,
    '@typescript-eslint/no-unused-expressions': 0,
    '@typescript-eslint/no-shadow': 0,
    '@typescript-eslint/no-redeclare': 0,
    '@typescript-eslint/no-loop-func': 0,
    '@typescript-eslint/lines-between-class-members': 0,
    'no-underscore-dangle': 'off',
    // complexity: ['error', 8],
  },
};
