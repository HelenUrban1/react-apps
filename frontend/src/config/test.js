const backendUrl = null;
const edaUrl = `http://ixc-local.com:5001/eda`;
const enpUrl = `http://ixc-local.com:3001/`;
const wsUrl = ``;

const config = {
  env: 'test',
  backendUrl,
  edaUrl,
  enpUrl,
  wsUrl,
  wsPath: '/api/socket.io/',
  autoBallooning: {
    enabled: false, // Whether to poll at all
    delay: 1000, // How long to wait until first check
    interval: 1000, // How long to wait between checks
    maxTotalSteps: 9,
    useHumanReview: false, // Disable human review until its staffed
  },
  remoteLogging: {
    isEnabled: false,
    level: 'trace',
    url: null, // `${backendUrl}/logger`
  },
  advancedCapture: {
    enabled: false, // Whether to show them or not
  },
  pdfTronKey: 'InspectionXpert Corporation (inspectionxpert.com):OEM:InspectionXpert Cloud::B+:AMS(20240301):0AA5E54D0487A60A0360B13AC9A2537860617F9BEF56F50595B50BB4144C588024DABEF5C7',
  authStrategy: 'cognito',
  sphinx: {
    netinspect: {
      enabled: false,
    },
  },
};
console.log('test.js netinspect disabled');
export default config;
