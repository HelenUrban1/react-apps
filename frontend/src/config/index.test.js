import config, {
  isDevEnv, //
  isTestEnv,
  isProdEnv,
  isNotDevEnv,
  isNotTestEnv,
  isNotProdEnv,
  isLocalHost,
  isDevHost,
  isQAHost,
  isStageHost,
  isProdHost,
  isNotLocalHost,
  isNotDevHost,
  isNotQAHost,
  isNotStageHost,
  isNotProdHost,
  isBeforeQA,
  isBeforeStaging,
  isBeforeProduction,
} from './index';

describe('Required Attributes', () => {
  it('Initializes config specified by REACT_APP_ENVIRONMENT', () => {
    expect(config.env).toBeDefined();
    expect(process.env.REACT_APP_ENVIRONMENT).toBe(config.env);
  });
  it('Exposes service urls', () => {
    expect(config.backendUrl).toBeDefined();
    expect(config.edaUrl).toBeDefined();
    expect(config.enpUrl).toBeDefined();
  });
  it('Exposes feature flags', () => {
    expect(config.autoBallooning.enabled).toBeDefined();
  });
});

describe('Helper Functions for Hostname ', () => {
  describe('when window.location.host = localhost', () => {
    it('.isLocalHost => true', () => {
      if (window.location.host === 'ixc-local.com' || window.location.host === 'localhost') {
        expect(isLocalHost).toBe(true);
      } else {
        expect(isLocalHost).toBe(false);
      }
    });
    it('.isNotLocalHost => false', () => {
      if (window.location.host === 'ixc-local.com' || window.location.host === 'localhost') {
        expect(isNotLocalHost).toBe(false);
      } else {
        expect(isNotLocalHost).toBe(true);
      }
    });
    it('.isDevHost => false', () => {
      if (window.location.host === 'www.ixc-dev.com') {
        expect(isDevHost).toBe(true);
      } else {
        expect(isDevHost).toBe(false);
      }
    });
    it('.isNotDevHost => true', () => {
      if (window.location.host === 'www.ixc-dev.com') {
        expect(isNotDevHost).toBe(false);
      } else {
        expect(isNotDevHost).toBe(true);
      }
    });
    it('.isQAHost => false', () => {
      if (window.location.host === 'www.ixc-qa.com') {
        expect(isDevHost).toBe(true);
      } else {
        expect(isDevHost).toBe(false);
      }
    });
    it('.isNotQAHost => true', () => {
      if (window.location.host === 'www.ixc-qa.com') {
        expect(isNotDevHost).toBe(false);
      } else {
        expect(isNotDevHost).toBe(true);
      }
    });
    it('.isStageHost => false', () => {
      if (window.location.host === 'www.ixc-stage.com') {
        expect(isStageHost).toBe(true);
      } else {
        expect(isStageHost).toBe(false);
      }
    });
    it('.isNotStageHost => true', () => {
      if (window.location.host === 'www.ixc-stage.com') {
        expect(isNotStageHost).toBe(false);
      } else {
        expect(isNotStageHost).toBe(true);
      }
    });
    it('.isProdHost => false', () => {
      if (window.location.host === 'app.inspectionxpert.com') {
        expect(isProdHost).toBe(true);
      } else {
        expect(isProdHost).toBe(false);
      }
    });
    it('.isNotProdHost => true', () => {
      if (window.location.host === 'app.inspectionxpert.com') {
        expect(isNotProdHost).toBe(false);
      } else {
        expect(isNotProdHost).toBe(true);
      }
    });
  });
});

describe('Helper Functions for Release Progression ', () => {
  if (window.location.host === 'ixc-local.com') {
    describe('when ixc-local.com', () => {
      it('.isBeforeQA => true', () => {
        expect(isBeforeQA).toBe(true);
      });
      it('.isBeforeStaging => true', () => {
        expect(isBeforeQA).toBe(true);
      });
      it('.isBeforeProduction => true', () => {
        expect(isBeforeProduction).toBe(true);
      });
    });
  } else if (window.location.host === 'www.ixc-dev.com') {
    describe('when www.ixc-dev.com', () => {
      it('window.location.host = www.ixc-dev.com', () => {
        expect(window.location.host).toBe('www.ixc-dev.com');
      });
      it('.isBeforeStaging => true', () => {
        expect(isBeforeStaging).toBe(true);
      });
      it('.isBeforeProduction => true', () => {
        expect(isBeforeProduction).toBe(true);
      });
    });
  } else if (window.location.host === 'www.ixc-stage.com') {
    describe('when www.ixc-stage.com', () => {
      it('window.location.host = www.ixc-stage.com', () => {
        expect(window.location.host).toBe('www.ixc-stage.com');
      });
      it('.isBeforeStaging => false', () => {
        expect(isBeforeStaging).toBe(false);
      });
      it('.isBeforeProduction => true', () => {
        expect(isBeforeProduction).toBe(true);
      });
    });
  } else if (window.location.host === 'app.inspectionxpert.com') {
    describe('when app.inspectionxpert.com', () => {
      it('window.location.host = app.inspectionxpert.com', () => {
        expect(window.location.host).toBe('app.inspectionxpert.com');
      });
      it('.isBeforeStaging => false', () => {
        expect(isBeforeStaging).toBe(false);
      });
      it('.isBeforeProduction => false', () => {
        expect(isBeforeProduction).toBe(false);
      });
    });
  }
});

describe('Helper Functions for REACT_APP_ENVIRONMENT', () => {
  // const OLD_ENV = process.env.REACT_APP_ENVIRONMENT;

  // afterAll(() => {
  //   process.env.REACT_APP_ENVIRONMENT = `${OLD_ENV}`;
  // });

  describe('when = test', () => {
    it('.isDevEnv => false', () => {
      expect(isDevEnv).toBe(false);
    });
    it('.isNotDevEnv => true', () => {
      expect(isNotDevEnv).toBe(true);
    });
    it('.isTestEnv => true', () => {
      expect(isTestEnv).toBe(true);
    });
    it('.isNotTestEnv => false', () => {
      expect(isNotTestEnv).toBe(false);
    });
    it('.isProdEnv => false', () => {
      expect(isProdEnv).toBe(false);
    });
    it('.isNotProdEnv => true', () => {
      expect(isNotProdEnv).toBe(true);
    });
  });

  // TODO: Doesn't work because react ENV vars are compiled and can't be reset at runtime
  // describe('when REACT_APP_ENVIRONMENT = development', () => {
  //   beforeAll(() => {
  //     jest.resetModules();
  //     process.env.REACT_APP_ENVIRONMENT = 'development';
  //     config = require('./index');
  //   });
  //   it('development', () => {
  //     expect(process.env.REACT_APP_ENVIRONMENT).toBe('development');
  //   });
  //   it('.isDevEnv', () => {
  //     expect(isDevEnv).toBe(true);
  //     expect(isNotDevEnv).toBe(false);
  //   });
  //   it('.isTestEnv', () => {
  //     expect(isTestEnv).toBe(false);
  //     expect(isNotTestEnv).toBe(true);
  //   });
  //   it('.isProdEnv', () => {
  //     expect(isProdEnv).toBe(false);
  //     expect(isNotProdEnv).toBe(true);
  //   });
  // });
});
