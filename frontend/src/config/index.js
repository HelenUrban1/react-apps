import axios from 'modules/shared/network/axios';

// TODO: Compile these at build time using webpack, instead of importing at runtime
import packageJson from '../../package.json';

// Load the config that corresponds to the value of REACT_APP_ENVIRONMENT
const config = require(`./${process.env.REACT_APP_ENVIRONMENT || 'test'}`).default; // eslint-disable-line

// Helper flags that deterministically return boolean result based on env
export const isDevEnv = config.env === 'development';
export const isProdEnv = config.env === 'production';
export const isTestEnv = config.env === 'test';
export const isNotDevEnv = config.env !== 'development';
export const isNotProdEnv = config.env !== 'production';
export const isNotTestEnv = config.env !== 'test';

// Helper flags that deterministically return boolean result based on host
export const isLocalHost = window.location.host.indexOf('ixc-local.com') > -1 || window.location.host.indexOf('localhost') > -1;
export const isDevHost = window.location.host.indexOf('ixc-dev') > -1;
export const isQaHost = window.location.host.indexOf('ixc-qa') > -1;
export const isStageHost = window.location.host.indexOf('ixc-stage') > -1;
export const isProdHost = window.location.host.indexOf('app.inspectionxpert.com') > -1;
export const isNotLocalHost = window.location.host.indexOf('ixc-local.com') === -1 && window.location.host.indexOf('localhost') === -1;
export const isNotDevHost = window.location.host.indexOf('ixc-dev') === -1;
export const isNotQaHost = window.location.host.indexOf('ixc-qa') === -1;
export const isNotStageHost = window.location.host.indexOf('ixc-stage') === -1;
export const isNotProdHost = window.location.host.indexOf('app.inspectionxpert.com') === -1;

// Alias for release progression local > dev > stage > prod
export const isBeforeQA = isNotQaHost && isNotStageHost && isNotProdHost;
export const isBeforeStaging = isNotStageHost && isNotProdHost;
export const isBeforeProduction = isNotProdHost;

export const getBackendConfig = async () => {
  const response = await axios({
    method: 'get',
    url: `${config.backendUrl}/config`,
  });
  return response.data;
};

console.log('FE config.authStrategy', config.authStrategy);
export const refreshEndpoint = config.authStrategy !== 'cognito' ? `${config.backendUrl}/refreshToken` : `${config.backendUrl}/cognitoRefreshToken`;

// Updated config with metadata from package.json
config.package = {
  name: packageJson.name,
  version: packageJson.version,
};

// Updated config with metadata from git
/* eslint-disable */
// This is no longer used:
config.scm = {
  tag: process.env.REACT_APP_SCM_TAG,
  branch: process.env.REACT_APP_SCM_BRANCH,
  commit: process.env.REACT_APP_GIT_SHA,
};
/* eslint-enable */

// Updated config with metadata from git
(async () => {
  config.backend = await getBackendConfig();
})().catch((error) => {
  // Deal with the fact the chain failed
  console.error(error);
});

// Enable autoballooning on all environments
config.autoBallooning.enabled = true;

if (isBeforeQA) {
  console.log('index.js netinspect enabled isBeforeQA');
  config.sphinx.netinspect.enabled = true;
}

export default config;
