const backendUrl = `/api`;
let edaUrl = `/eda`;
let enpUrl = `/api/enp`; // May need to be /enp
let wsUrl = `https://${window.location.host}`; // window.location.host;

const authStrategy = process.env.REACT_APP_IXC_AUTH_STRATEGY;
console.log('process.env.REACT_APP_IXC_AUTH_STRATEGY', authStrategy);

const url = new URL(window.location.href);
if (url.hostname === 'ixc-local.com') {
  edaUrl = `https://ixc-local.com:5000/eda`;
  enpUrl = `https://ixc-local.com:3001/`;
  wsUrl = `https://ixc-local.com:8080`;
}

const config = {
  env: 'production',
  backendUrl,
  edaUrl,
  enpUrl,
  wsUrl,
  wsPath: '/api/socket.io/',
  autoBallooning: {
    enabled: false, // Whether to poll at all, NOTE: this value is be overridden in ./index.js to true for Dev and QA
    delay: 1000, // How long to wait until first check
    interval: 1000, // How long to wait between checks
    maxTotalSteps: 9,
    useHumanReview: false, // Disable human review until its staffed
  },
  remoteLogging: {
    isEnabled: false,
    level: 'trace',
    url: `${backendUrl}/logger`,
  },
  advancedCapture: {
    enabled: false, // Whether to show them or not
  },
  authStrategy,
  pdfTronKey: 'InspectionXpert Corporation (inspectionxpert.com):OEM:InspectionXpert Cloud::B+:AMS(20240301):0AA5E54D0487A60A0360B13AC9A2537860617F9BEF56F50595B50BB4144C588024DABEF5C7',
  sphinx: {
    netinspect: {
      enabled: false,
    },
  },
};
console.log('production.js netinspect disabled');
export default config;
