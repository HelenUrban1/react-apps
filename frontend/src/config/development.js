const backendPort = process.env.REACT_APP_IXC_BACKEND_PORT || ':8080';
const backendUrl = `https://ixc-local.com${backendPort}/api`;
const edaUrl = `https://ixc-local.com:5000/eda`;
const enpUrl = `https://ixc-local.com:3001/`;
const wsUrl = `https://ixc-local.com:8080`;

const authStrategy = process.env.REACT_APP_IXC_AUTH_STRATEGY;
console.log('process.env.REACT_APP_IXC_AUTH_STRATEGY', authStrategy);

const config = {
  env: 'development',
  backendUrl,
  edaUrl,
  enpUrl,
  wsUrl,
  wsPath: '/api/socket.io/',
  autoBallooning: {
    enabled: true, // Whether to poll at all
    delay: 1000, // How long to wait until first check
    interval: 5000, // How long to wait between checks
    maxTotalSteps: 9,
    useHumanReview: false, // Disable human review until its staffed
  },
  remoteLogging: {
    isEnabled: false,
    level: 'trace',
    url: `${backendUrl}/logger`,
  },
  advancedCapture: {
    enabled: true, // Whether to show them or not
  },
  authStrategy,
  pdfTronKey: 'InspectionXpert Corporation (inspectionxpert.com):OEM:InspectionXpert Cloud::B+:AMS(20240301):0AA5E54D0487A60A0360B13AC9A2537860617F9BEF56F50595B50BB4144C588024DABEF5C7',
  sphinx: {
    netinspect: {
      enabled: true,
    },
  },
};

console.log('development.js netinspect enabled');
export default config;
