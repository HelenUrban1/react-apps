declare module 'engineering-notation-parser' {
  export function sanitize(text: string): string;
  export function sequence(text: string): string;
  export function interpret(text: string): {} | { type: string; error: any };
}
