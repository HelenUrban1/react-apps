import RolesMapper from './rolesMapper';
import Roles from './roles';

describe('RolesMapper', () => {
  describe('#hasMatchingRole()', () => {
    it('should return false for empty input', () => {
      const result = RolesMapper.hasMatchingRole();
      expect(result).toBe(false);
    });

    it('should return false for no matching user roles', () => {
      const userRoles = ['ThisDoesNotExist'];
      const result = RolesMapper.hasMatchingRole(userRoles, [Roles.values.owner]);
      expect(result).toBe(false);
    });

    it('should return false for no matching authorization roles', () => {
      const result = RolesMapper.hasMatchingRole('Admin', ['ThisDoesNotExist']);
      expect(result).toBe(false);
    });

    it('should return false for no matching user roles and no matching authorization roles', () => {
      const result = RolesMapper.hasMatchingRole(['ThisDoesNotExist'], ['ThisDoesNotExist']);
      expect(result).toBe(false);
    });

    it('should return true when all authorization roles match the mapping for their user roles', () => {
      const result = RolesMapper.hasMatchingRole(['Billing', 'Viewer'], [Roles.values.accountBilling, Roles.values.partViewer]);
      expect(result).toBe(true);
    });

    it('should return true when one authorization roles matches the mapping for its user role', () => {
      const result = RolesMapper.hasMatchingRole(['Owner'], [Roles.values.owner]);
      expect(result).toBe(true);
    });
  });
});
