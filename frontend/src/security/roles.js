import _keys from 'lodash/keys';
import _values from 'lodash/values';

class Roles {
  static get values() {
    if (this.VALUES) {
      return this.VALUES;
    }

    this.VALUES = {};
    _keys(this.DEFINITIONS).forEach((role) => (this.VALUES[role] = role));
    return this.VALUES;
  }

  static get names() {
    return _keys(this.DEFINITIONS);
  }

  static get definitions() {
    return this.DEFINITIONS;
  }

  static DEFINITIONS = {
    accountAdmin: {
      mapsToAccountMemberRole: 'Admin',
    },
    accountBilling: {
      mapsToAccountMemberRole: 'Billing',
    },
    accountEditor: {
      mapsToAccountMemberRole: 'Admin',
    },
    accountMemberEditor: {
      mapsToAccountMemberRole: 'Admin',
    },
    accountMemberViewer: {
      mapsToAccountMemberRole: 'Admin',
    },
    accountViewer: {
      mapsToAccountMemberRole: 'Admin',
    },
    auditLogViewer: {
      mapsToAccountMemberRole: 'Admin',
    },
    featureEditor: {
      mapsToAccountMemberRole: 'Admin',
    },
    featureViewer: {
      mapsToAccountMemberRole: 'Collaborator',
    },
    collaborator: {
      mapsToAccountMemberRole: 'Collaborator',
    },
    customerEditor: {
      mapsToAccountMemberRole: 'Admin',
    },
    customerViewer: {
      mapsToAccountMemberRole: 'Admin',
    },
    drawingEditor: {
      mapsToAccountMemberRole: 'Admin',
    },
    drawingSheetEditor: {
      mapsToAccountMemberRole: 'Admin',
    },
    drawingSheetViewer: {
      mapsToAccountMemberRole: 'Collaborator',
    },
    drawingViewer: {
      mapsToAccountMemberRole: 'Collaborator',
    },
    editor: {
      mapsToAccountMemberRole: 'Admin',
    },
    entityEditor: {
      mapsToAccountMemberRole: 'Admin',
    },
    entityViewer: {
      mapsToAccountMemberRole: 'Collaborator',
    },
    iamSecurityReviewer: {
      mapsToAccountMemberRole: 'Admin',
    },
    markerEditor: {
      mapsToAccountMemberRole: 'Admin',
    },
    markerViewer: {
      mapsToAccountMemberRole: 'Collaborator',
    },
    metro: {
      mapsToAccountMemberRole: 'Metro',
    },
    notificationEditor: {
      mapsToAccountMemberRole: 'Admin',
    },
    notificationViewer: {
      mapsToAccountMemberRole: 'Admin',
    },
    optionsEditor: {
      mapsToAccountMemberRole: 'Admin',
    },
    optionsViewer: {
      mapsToAccountMemberRole: 'Collaborator',
    },
    organizationEditor: {
      mapsToAccountMemberRole: 'Admin',
    },
    organizationViewer: {
      mapsToAccountMemberRole: 'Collaborator',
    },
    owner: {
      mapsToAccountMemberRole: 'Owner',
    },
    partEditor: {
      mapsToAccountMemberRole: 'Admin',
    },
    partViewer: {
      mapsToAccountMemberRole: 'Collaborator',
    },
    reportEditor: {
      mapsToAccountMemberRole: 'Admin',
    },
    reportTemplateEditor: {
      mapsToAccountMemberRole: 'Admin',
    },
    reportTemplateViewer: {
      mapsToAccountMemberRole: 'Collaborator',
    },
    reportViewer: {
      mapsToAccountMemberRole: 'Collaborator',
    },
    reviewer: {
      mapsToAccountMemberRole: 'Reviewer',
    },
    viewer: {
      mapsToAccountMemberRole: 'Viewer',
    },
  };

  static VALUES = null;
}

export default Roles;
