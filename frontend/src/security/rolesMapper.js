import Roles from './roles';

export const ACCOUNT_MEMBER_ROLES = ['Admin', 'Billing', 'Collaborator', 'Owner', 'Viewer', 'Reviewer', 'Metro'];

/**
 * Maps AccountMember roles, which are the roles that users can assign in the app, with
 * authorization roles, which are the roles defined in security/roles.js and used for
 * access control.
 */
class RolesMapper {
  static map;

  /**
   * Determines if there is a match between the authorization roles provided and the
   * provided user's roles, which are the roles in their active AccountMember record,
   * using the mapping between the two sets of roles created in this class.
   *
   * @param {Array<String>} userRoles
   * @param {Array<String>} authorizationRoles
   * @return {Boolean}
   */
  static hasMatchingRole(userRoles = [], authorizationRoles = []) {
    this._buildMap();

    const currentRoles = Array.isArray(userRoles) ? userRoles : [userRoles];

    return currentRoles.some((userRole) => {
      const entries = this.map[userRole.toLowerCase()];
      if (!entries) {
        return false;
      }

      return entries.some((entry) => authorizationRoles.includes(entry));
    });
  }

  /**
   * Builds the roles mapping.
   *
   * @private
   */
  static _buildMap() {
    if (this.map) {
      return;
    }

    this.map = {};

    ACCOUNT_MEMBER_ROLES.forEach((role) => (this.map[role.toLowerCase()] = []));

    Roles.names.forEach((authRole) => {
      ACCOUNT_MEMBER_ROLES.forEach((accountMemberRole) => {
        if (Roles.definitions[authRole].mapsToAccountMemberRole === accountMemberRole) {
          this.map[accountMemberRole.toLowerCase()].push(authRole);
        }
      });
    });
  }
}

export default RolesMapper;
