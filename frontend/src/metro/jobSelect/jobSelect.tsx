import React, { useState, useEffect } from 'react';
import { Select } from 'antd';
import { useSelector, useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { i18n } from 'i18n';
import { useQuery } from '@apollo/client';
import actions, { OperatorState } from 'modules/metro/metroActions';
import { doSetPart } from 'modules/part/partActions';
import { AppState } from 'modules/state';
import MetaDataTile from 'styleguide/METRO/MetaDataTile/MetaDataTile';
import { PdfViewer } from 'styleguide/PdfViewer/PdfViewer';
import Button from 'styleguide/METRO/Button/Button';
import { JobList, Jobs } from 'domain/jobs/jobApi';
import { Job } from 'domain/jobs/jobTypes';
import { SelectValue } from 'antd/lib/select';
import { Part } from 'domain/part/partTypes';
import Analytics from 'modules/shared/analytics/analytics';
import HeaderWrapper from '../common/headerWrapper/headerWrapper';
import './jobSelect.less';

const { Option } = Select;

interface JobSelectProps {
  changePart: (partId: string) => void;
}

const JobSelect = ({ changePart }: JobSelectProps) => {
  const dispatch = useDispatch();
  const history = useHistory<OperatorState>();
  const { initialized, operator, station, job, drawing } = useSelector((state: AppState) => state.metro);
  const { part } = useSelector((state: AppState) => state);
  const { tronInstance } = useSelector((state: AppState) => state.pdfView);
  const [drawingError, setDrawingError] = useState<boolean>(false);

  const [jobsListQueryParams, setJobsListQueryParams] = useState({
    filter: { jobStatus: ['Active', 'Inactive', 'Complete'] },
    orderBy: 'name_ASC',
    limit: 10,
    offset: 0,
  });

  const { data, loading } = useQuery<JobList>(Jobs.query.list, { variables: jobsListQueryParams });

  // when a job or part changes, set samples and fetch drawing
  useEffect(() => {
    if (job?.id && part.part?.id) {
      dispatch(actions.doFetchSamples(job?.id || '', part.part?.id || ''));
    }
  }, [job?.id, part.part?.id]);

  const setPart = async (newPartId: string | null) => {
    if (newPartId) {
      changePart(newPartId);
    } else {
      dispatch(doSetPart(null, null));
    }
  };

  const clearSelection = () => {
    dispatch(actions.doSetJob(null));
    dispatch(doSetPart(null, null));
  };

  const setJob = (newJob: string | null) => {
    if (newJob) {
      const foundJob = newJob ? data?.jobList.rows.find((j: Job) => j.id === newJob) || null : null;
      dispatch(actions.doSetJob(foundJob));
      if (foundJob && foundJob.parts) {
        changePart(foundJob.parts[0].id || '');
      }
    } else {
      clearSelection();
    }
  };

  useEffect(() => {
    if (initialized) {
      if (!station) history.push('/history', history.location.state);
      if (!operator) history.push('/history', history.location.state);
    }
  }, [initialized]);

  const renderJobSection = () => {
    return (
      <>
        <section data-cy="metro_job_selection" style={{ display: !job ? 'block' : 'none' }}>
          <h2 className="ixc-metro-heading dark">{i18n('metro.job.whatJob')}</h2>
          <Select
            className="ixc-metro-select"
            dropdownClassName="job-select-dropdown"
            size="large"
            placeholder={i18n('metro.job.jobPlaceholder')}
            onChange={(value: SelectValue) => setJob(value?.toString() || '')}
            value={job?.id}
            showSearch
            onSearch={() => {}}
            filterOption={(input: any, option: any) => {
              return option.props.children.toLowerCase().includes(input.toLowerCase());
            }}
            loading={loading}
          >
            {data?.jobList.rows.map((j) => (
              <Option key={j.id} value={j.id}>
                {j.name}
              </Option>
            ))}
          </Select>
        </section>
        {job && (
          <section data-cy="metro_job_selection">
            <MetaDataTile active={false} icon={MetaDataTile.IconType.Edit} label={i18n('metro.job.jobNumber')} onClick={() => setJob(null)} value={job.name} />
          </section>
        )}
      </>
    );
  };

  const renderPartSection = () => (
    <>
      <section data-cy="metro_part_selection">
        <h2 className="ixc-metro-heading dark">{i18n('metro.job.partSelect')}</h2>
        <Select className="ixc-metro-select" dropdownClassName="part-select-dropdown" size="large" value={part.part?.id} defaultValue={job?.parts ? job.parts[0]?.id : undefined} onChange={(value: SelectValue) => setPart(value?.toString() || '')}>
          {job?.parts &&
            job.parts.map((p: Part) => (
              <Option key={p.id} value={p.id}>
                {p.name}
              </Option>
            ))}
        </Select>
      </section>
    </>
  );

  const renderPdf = () => {
    if (drawingError) {
      return <h2 className="ixc-metro-heading dark">{i18n('metro.job.drawingError')}</h2>;
    }

    if (drawing) {
      return <PdfViewer filePath={drawing?.file[0]?.publicUrl} tronInstance={tronInstance} />;
    }
    return <h2 className="ixc-metro-heading dark">{i18n('metro.job.loadingDrawing')}</h2>;
  };

  const startMeasure = () => {
    Analytics.track({
      event: Analytics.events.startMeasure,
      properties: {
        job,
        part,
        operator,
      },
    });
    history.push(`/metro/measure/${job?.id}/${part.part?.id}`, { ...history.location.state });
  };

  const renderPdfButtonSection = () => (
    <div data-cy="metro_part_selection_footer">
      <section className="ixc-job-select-pdf-container">{renderPdf()}</section>
      <Button fullWidth onClick={startMeasure}>
        {i18n('metro.job.measureButton')}
      </Button>
    </div>
  );

  return (
    <HeaderWrapper>
      <main className="ixc-job-select-content">
        {renderJobSection()}
        {job && renderPartSection()}
        {job && part.part && renderPdfButtonSection()}
      </main>
    </HeaderWrapper>
  );
};

export default JobSelect;
