import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import actions, { OperatorState, doInitMetro } from 'modules/metro/metroActions';
import { AppState } from 'modules/state';
import { Route, Switch, useHistory } from 'react-router-dom';
import { global } from 'styleguide';
import log from 'modules/shared/logger';
import { useLazyQuery } from '@apollo/client';
import { PartDrawing, Parts } from 'domain/part/partApi';
import { doSetPart } from 'modules/part/partActions';
import InstallForm from './install/InstallForm';
import PreInstallStep from './install/PreInstall';
import OperatorSelect from './operatorSelect/operatorSelect';
import StationSelect from './stationSelect/stationSelect';
import JobSelect from './jobSelect/jobSelect';
import MeasureEntry from './measureEntry/measureEntry';
import './index.less';

const App = () => {
  const { currentUser } = useSelector((state: AppState) => state.auth);
  const dispatch = useDispatch();
  const history = useHistory<OperatorState>();
  const { initialized } = useSelector((state: AppState) => state.metro);
  const [drawingError, setDrawingError] = useState<boolean>(false);

  useEffect(() => {
    if (!initialized && currentUser?.activeAccountMemberId) {
      // initializes metro in redux
      dispatch(doInitMetro(currentUser.activeAccountMemberId, history.location.state));
    }
  }, [currentUser, initialized]);

  // Graphql hook to lookup record by ID
  const [findPartDrawing] = useLazyQuery(PartDrawing.query.find, {
    onCompleted: ({ drawingFind }) => {
      if (drawingFind) {
        dispatch(actions.doSetDrawing(drawingFind));
        // setDrawing(drawingFind);
      }
    },
    onError: (err) => {
      log.error('JobSelect.findPartDrawing error', err);
      setDrawingError(true);
    },
  });

  const [findPart, { data }] = useLazyQuery(Parts.query.find, {
    onCompleted: ({ partFind }) => {
      if (partFind) {
        dispatch(doSetPart(partFind, partFind));
        findPartDrawing({ variables: { id: partFind.primaryDrawing?.id || '' } });
      }
    },
    onError: (err) => {
      log.error('JobSelect.findPart error', err);
      setDrawingError(true);
    },
  });

  const changePart = (partId: string) => {
    if (data?.partFind?.id === partId) {
      dispatch(doSetPart(data.partFind, data.partFind));
      findPartDrawing({ variables: { id: data.partFind.primaryDrawing?.id || '' } });
    } else {
      findPart({ variables: { id: partId } });
    }
    dispatch(actions.doSelectCell({ featureIndex: 0, sampleIndex: 0, measurementIndex: 0, selectedSample: null, selectedFeature: null, selectedMeasurement: null }));
  };

  return (
    <div className="ixc-metro-container">
      <global.GlobalStyle />
      <Switch>
        <Route path="/metro" exact component={PreInstallStep} />
        <Route path="/metro/operator" component={OperatorSelect} />
        <Route path="/metro/installed" component={InstallForm} />
        <Route path="/metro/station" component={StationSelect} />
        <Route path="/metro/job" render={() => <JobSelect changePart={changePart} />} />
        <Route path="/metro/measure/:jobId/:partId" render={() => <MeasureEntry changePart={changePart} />} />
      </Switch>
    </div>
  );
};
export default App;
