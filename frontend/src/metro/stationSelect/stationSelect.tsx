import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { RouteComponentProps, useHistory } from 'react-router-dom';
import { PlusCircleFilled } from '@ant-design/icons';
import { i18n } from 'i18n';
import log from 'modules/shared/logger';
import Analytics from 'modules/shared/analytics/analytics';
import actions, { OperatorState } from 'modules/metro/metroActions';
import { AppState } from 'modules/state';
import Profile from 'styleguide/METRO/Profile/Profile';
import { useMutation } from '@apollo/client';
import { Stations } from 'metro/graphql/station';
import { StationCreate } from 'types/station';
import { Loader } from 'view/global/Loader';
import { AddStationModal } from './addStationModal';
import SelectHeader from '../common/selectHeader/selectHeader';
import './stationSelectStyle.less';

const StationView: React.FC<RouteComponentProps<{}>> = () => {
  const dispatch = useDispatch();
  const history = useHistory<OperatorState>();
  const { initialized, operator, operators, station, stations } = useSelector((state: AppState) => state.metro);

  const [showModal, setShowModal] = useState(false);
  const [error, setError] = useState<string>();

  const [createStation] = useMutation<StationCreate>(Stations.mutate.create, {
    onCompleted: ({ stationCreate }) => {
      Analytics.track({
        event: Analytics.events.stationCreated,
        properties: {
          station: stationCreate,
        },
      });
      dispatch(actions.doSetStations([...stations, stationCreate]));
      dispatch(actions.doSetStation(stationCreate));
    },
  });

  const setStation = (stationId: string) => {
    const matchingStation = stations.find((st) => st.id === stationId);
    if (!matchingStation) {
      log.warn('No Station Matches');
      return;
    }
    Analytics.track({
      event: Analytics.events.stationSelected,
      properties: {
        station: matchingStation,
      },
    });
    dispatch(actions.doSetStation(matchingStation));
  };

  useEffect(() => {
    if (initialized && !operator) {
      history.push('/metro', history.location.state);
    }
  }, [operators]);

  if (station) {
    history.push('/metro/job', { ...history.location.state, station: station?.id });
  }

  const handleCloseModal = () => {
    setShowModal(false);
  };

  const handleOpenModal = () => {
    setShowModal(true);
  };

  const handleSubmit = (name: string) => {
    setShowModal(false);
    createStation({ variables: { data: { name, disabled: false } } });
  };

  const closeError = () => {
    setError(undefined);
  };

  if (!stations || stations.length === 0) {
    return <Loader loading={50} error={error} closeError={closeError} />;
  }

  return (
    <div className="ixc-station-container">
      <SelectHeader headerText={i18n('metro.station.whichStation')} />
      <section className="ixc-user-grid-container">
        {stations.map((st) => (
          <div key={st.id} className="ixc-user-grid-item" data-cy="metro_station_block">
            <Profile key={`${st.id}-profile`} id={st.id} title={st.name} variant={Profile.Variants.Station} onClick={setStation} isMaster={st.isMaster} />
          </div>
        ))}
        <AddStationModal visible={showModal} handleCloseModal={handleCloseModal} handleSubmit={handleSubmit} stations={stations} />
        <button type="button" className="ixc-user-grid-item add-station" data-cy="metro-add-station-btn" onClick={handleOpenModal}>
          <PlusCircleFilled />
          <span className="add-station-label">{i18n('metro.station.addStation')}</span>
        </button>
      </section>
    </div>
  );
};

export default StationView;
