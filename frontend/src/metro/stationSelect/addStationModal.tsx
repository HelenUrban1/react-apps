import React, { ChangeEvent, useState } from 'react';
import { Modal, Button, Input, Form } from 'antd';
import { TabletOutlined } from '@ant-design/icons';
import { i18n } from 'i18n';
import { Station } from 'types/station';

interface AddStationModalProps {
  visible: boolean;
  handleSubmit: (name: string) => void;
  handleCloseModal: () => void;
  stations: Station[];
}

export const AddStationModal = ({ visible, handleCloseModal, handleSubmit, stations }: AddStationModalProps) => {
  const [name, setName] = useState('');
  const [errorMessage, setErrorMessage] = useState('');

  const handleOk = () => {
    if (!name) {
      setErrorMessage(i18n('errors.form.empty', i18n('common.name')));
      return;
    }
    // TODO: Resolve name collisions if multiple devices submit the same station name
    if (stations.find((station: Station) => station.name === name)) {
      setErrorMessage(i18n('errors.form.duplicate', i18n('common.name')));
      return;
    }
    handleSubmit(name);
  };

  const handleCancel = () => {
    setName('');
    setErrorMessage('');
    handleCloseModal();
  };

  const handleChange = (e: ChangeEvent<HTMLInputElement>) => {
    setName(e.target.value);
    setErrorMessage('');
  };

  return (
    <Modal
      width="480px"
      data-cy="add-station-modal"
      visible={visible} //
      title={i18n('metro.station.addStation')}
      onCancel={handleCancel}
      maskClosable
      mask
      footer={[
        <Button key="submit-btn" data-cy="submit-station-btn" className="ixc-metro-btn" onClick={handleOk}>
          {i18n('metro.station.addStation')}
        </Button>,
      ]}
      className="metro-modal"
    >
      <Form.Item label={i18n('metro.station.stationName')} hasFeedback validateStatus={errorMessage ? 'error' : ''} help={errorMessage}>
        <Input data-cy="add-station-name-input" prefix={<TabletOutlined />} onChange={handleChange} placeholder={i18n('metro.station.namePlaceholder')} />
      </Form.Item>
    </Modal>
  );
};
