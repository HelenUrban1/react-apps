import React, { useEffect, useState } from 'react';
import { Select } from 'antd';
import { Redirect, RouteComponentProps } from 'react-router-dom';
import { global } from 'styleguide';
import SelectHeader from '../common/selectHeader/selectHeader';
import authActions from 'modules/auth/authActions';

import './install.less';
import Loader from 'view/global/Loader';
import { useDispatch, useSelector } from 'react-redux';
import { AppState } from 'modules/state';

const { Option } = Select;

const InstallForm: React.FC<RouteComponentProps<{}>> = ({ history }) => {
  const installed = window.localStorage.getItem('installed') === 'true';
  const { account } = useSelector((state: AppState) => state);
  const { loading, errorMessage } = useSelector((state: AppState) => state.auth);
  const dispatch = useDispatch();

  const [metroLoading, setLoading] = useState(false);

  const closeError = () => {};

  const setConnection = () => {
    history.push('/metro');
  };

  useEffect(() => {
    setLoading(true);
    dispatch(authActions.doGetDeviceToken());
    dispatch(authActions.doSignoutInPlace());
    dispatch(authActions.doSigninWithDevice());
  }, []);

  useEffect(() => {
    if (account.settings && account.settings.index) {
      authActions.doSigninWithDevice();
    }
  }, [account.members]);

  useEffect(() => {
    setLoading(loading);
  }, [loading]);

  return (
    <div className="ixc-metro-install-form">
      <global.GlobalStyle />
      <SelectHeader headerText="Install Successful!" />
      <section>
        {!installed && <Loader loading={undefined} queries={[metroLoading]} error={[errorMessage]} closeError={closeError} />}
        {installed && <Redirect to="/metro/operator" />}
        {/* <h2 className="ixc-metro-heading">What kind of connection does this device have?</h2>
        <div data-cy="metro_station_block">
          <Select className="ixc-metro-select-connection" size="large" placeholder="Connection Strength" onChange={setConnection}>
            <Option value="wired">Wired</Option>
            <Option value="wireless-strong">Wireless - Strong Connection</Option>
            <Option value="wireless-medium">Wireless - Intermittent Connection</Option>
            <Option value="wireless-weak">Wireless - Offline Primarily</Option>
          </Select> */
        /* </div> */}
      </section>
    </div>
  );
};

export default InstallForm;
