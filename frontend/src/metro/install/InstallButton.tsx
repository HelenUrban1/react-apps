import React from 'react';
import { register } from './serviceWorker';
import BeforeInstallPromptEvent from './BeforeInstallPromptEvent';
import metroActions from 'modules/metro/metroActions';
import { IxButton } from 'styleguide';
import { useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';

interface Props {
  style?: React.CSSProperties;
}

interface State {
  disabled: boolean;
  event: BeforeInstallPromptEvent | null;
  display: string;
}

const InstallButton: React.FC<Props> = () => {
  const [state, setState] = React.useState<State>({
    disabled: false,
    event: null,
    display: 'none',
  });
  const dispatch = useDispatch();
  const history = useHistory();

  const addInstallEvent = (e: BeforeInstallPromptEvent) => {
    setState({ ...state, event: e, display: 'block' });
  };

  const install = async () => {
    if (!state.event) return;

    state.event.prompt();

    setState({ ...state, disabled: true });

    state.event.userChoice.then(async (choiceResult) => {
      if (choiceResult.outcome === 'accepted') {
        dispatch(metroActions.doSetInstalled(true));
        window.localStorage.setItem('installed', 'true');

        history.push('/metro/installed');
        setState({ ...state, event: null, display: 'none' });
      } else {
        dispatch(metroActions.doSetInstalled(false));
        setState({ ...state, disabled: false });
      }
    });
  };

  React.useEffect(() => {
    register({ addInstallEvent });
  }, []);

  return <IxButton className="btn-primary install-btn" text="Install Metro" onClick={install} disabled={state.disabled} style={{ display: state.display }} />;
};

export default InstallButton;
