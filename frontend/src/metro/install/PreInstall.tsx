import React from 'react';
import { Redirect, RouteComponentProps } from 'react-router';
import { TextLogo } from 'styleguide/Logo/Logo';
import InstallButton from './InstallButton';

const PreInstallView: React.FC<RouteComponentProps<{}>> = () => {
  const installed = window.localStorage.getItem('installed') === 'true';
  return (
    <main className="ixc-install-container">
      <div className="select-header">
        <div className="logo-icon">
          <TextLogo />
        </div>
      </div>
      <div className="ixc-grid-header" data-cy="metro_login_title">
        Please Install to Start Measuring
      </div>
      {!installed && (
        <InstallButton
          style={{
            display: window.matchMedia('(display-mode: standalone)').matches ? 'none' : 'block',
          }}
        />
      )}
      {installed && <Redirect to="/metro/operator" />}
    </main>
  );
};

export default PreInstallView;
