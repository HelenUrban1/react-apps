import gql from 'graphql-tag';
import { Operator, OperatorFilterInput, OperatorInput } from 'types/operator';

export interface OperatorEditVars {
  id: string;
  data: OperatorInput;
}

export interface OperatorCreateVars {
  data: OperatorInput;
}

export interface OperatorEdit {
  operatorUpdate: Operator;
}
export interface OperatorCreate {
  operatorCreate: Operator;
}

export interface OperatorFind {
  operatorFind: Operator;
}

export type OperatorOrderByEnum =
  | 'id_ASC'
  | 'id_DESC'
  | 'value_ASC'
  | 'value_DESC'
  | 'status_ASC'
  | 'status_DESC'
  | 'gage_ASC'
  | 'gage_DESC'
  | 'operation_ASC'
  | 'operation_DESC'
  | 'method_ASC'
  | 'method_DESC'
  | 'machine_ASC'
  | 'machine_DESC'
  | 'createdAt_ASC'
  | 'createdAt_DESC'
  | 'updatedAt_ASC'
  | 'updatedAt_DESC';

export interface OperatorList {
  operatorList?: {
    count: number;
    rows: Operator[];
  };
}

export interface OperatorListVars {
  filter?: OperatorFilterInput;
  orderBy?: OperatorOrderByEnum;
  limit?: number;
  offset?: number;
}

export const Operators = {
  mutate: {
    edit: gql`
      mutation OPERATOR_UPDATE($id: String!, $data: OperatorInput!) {
        operatorUpdate(id: $id, data: $data) {
          id
          fullName
          firstName
          lastName
          email
          accessControl
          createdById
          updatedById
        }
      }
    `,
    create: gql`
      mutation OPERATOR_CREATE($data: OperatorInput!) {
        operatorCreate(data: $data) {
          id
          fullName
          firstName
          lastName
          email
          accessControl
          accountMemberId
          createdBy {
            id
            email
            fullName
          }
          updatedBy {
            id
            email
            fullName
          }
        }
      }
    `,
  },
  query: {
    find: gql`
      query OPERATOR_FIND($id: String!) {
        operatorFind(id: $id) {
          id
          fullName
          firstName
          lastName
          email
          accessControl
          createdById
          updatedById
        }
      }
    `,
    list: gql`
      query OPERATOR_LIST($filter: OperatorFilterInput, $orderBy: OperatorOrderByEnum, $limit: Int, $offset: Int) {
        operatorList(filter: $filter, orderBy: $orderBy, limit: $limit, offset: $offset) {
          count
          rows {
            id
            fullName
            firstName
            lastName
            email
            accessControl
            createdById
            updatedById
          }
        }
      }
    `,
  },
  util: {
    input: (Operator: Operator, accountMemberId: string) => {
      const newOperator: OperatorInput = {
        data: {
          ...Operator,
          accountMemberId: accountMemberId,
        },
      };
      return { id: Operator.id, operator: newOperator };
    },
  },
};

export default Operators;
