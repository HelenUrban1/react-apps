import gql from 'graphql-tag';
import { Measurement, MeasurementFilterInput, MeasurementInput } from 'types/measurement';

export interface MeasurementEditVars {
  id: string;
  data: MeasurementInput;
}

export interface MeasurementCreateVars {
  data: MeasurementInput;
}

export interface MeasurementDestroyVars {
  ids: string[];
}

export interface MeasurementEdit {
  measurementUpdate: Measurement;
}
export interface MeasurementCreate {
  measurementCreate: Measurement;
}

export interface MeasurementDestroy {
  measurementDestroy: string[];
}

export interface MeasurementFind {
  measurementFind: Measurement;
}

export type MeasurementOrderByEnum =
  | 'id_ASC'
  | 'id_DESC'
  | 'value_ASC'
  | 'value_DESC'
  | 'status_ASC'
  | 'status_DESC'
  | 'gage_ASC'
  | 'gage_DESC'
  | 'operation_ASC'
  | 'operation_DESC'
  | 'method_ASC'
  | 'method_DESC'
  | 'machine_ASC'
  | 'machine_DESC'
  | 'createdAt_ASC'
  | 'createdAt_DESC'
  | 'updatedAt_ASC'
  | 'updatedAt_DESC';

export interface MeasurementList {
  measurementList?: {
    count: number;
    rows: Measurement[];
  };
}

export interface MeasurementListVars {
  filter?: MeasurementFilterInput;
  orderBy?: MeasurementOrderByEnum;
  limit?: number;
  offset?: number;
}

export const Measurements = {
  mutate: {
    edit: gql`
      mutation MEASUREMENT_UPDATE($id: String!, $data: MeasurementInput!) {
        measurementUpdate(id: $id, data: $data) {
          id
          value
          gage
          machine
          status
          characteristicId
          characteristic {
            id
            fullSpecification
          }
          sampleId
          sample {
            id
            serial
          }
          stationId
          station {
            id
            name
          }
          operationId
          operation {
            id
            name
          }
          methodId
          method {
            id
            name
          }
          createdById
          updatedById
        }
      }
    `,
    delete: gql`
      mutation MEASUREMENT_DESTROY($ids: [String!]!) {
        measurementDestroy(ids: $ids)
      }
    `,
    create: gql`
      mutation MEASUREMENT_CREATE($data: MeasurementInput!) {
        measurementCreate(data: $data) {
          id
          value
          gage
          machine
          status
          characteristicId
          characteristic {
            id
            fullSpecification
          }
          sampleId
          sample {
            id
            serial
          }
          stationId
          station {
            id
            name
          }
          operationId
          operation {
            id
            name
          }
          methodId
          method {
            id
            name
          }
          createdBy {
            id
            email
            fullName
          }
          updatedBy {
            id
            email
            fullName
          }
        }
      }
    `,
  },
  query: {
    find: gql`
      query MEASUREMENT_FIND($id: String!) {
        measurementFind(id: $id) {
          id
          value
          gage
          machine
          status
          characteristicId
          characteristic {
            id
            fullSpecification
          }
          sampleId
          sample {
            id
            serial
          }
          stationId
          station {
            id
            name
          }
          operationId
          operation {
            id
            name
          }
          methodId
          method {
            id
            name
          }
          createdById
          updatedById
        }
      }
    `,
    list: gql`
      query MEASUREMENT_LIST($filter: MeasurementFilterInput, $orderBy: MeasurementOrderByEnum, $limit: Int, $offset: Int) {
        measurementList(filter: $filter, orderBy: $orderBy, limit: $limit, offset: $offset) {
          count
          rows {
            id
            value
            gage
            machine
            status
            characteristicId
            characteristic {
              id
              fullSpecification
            }
            sampleId
            sample {
              id
              serial
            }
            stationId
            station {
              id
              name
            }
            operationId
            operation {
              id
              name
            }
            methodId
            method {
              id
              name
            }
            createdById
            updatedById
          }
        }
      }
    `,
  },
  util: {
    input: (measurement: Measurement, operatorId: string) => {
      const newMeasurement: MeasurementInput = {
        value: measurement.value,
        gage: measurement.gage,
        machine: measurement.machine,
        status: measurement.status,
        characteristicId: measurement.characteristicId,
        operatorId,
        sampleId: measurement.sampleId,
        stationId: measurement.stationId,
        methodId: measurement.methodId,
        operationId: measurement.operationId,
      };
      return { id: measurement.id, measure: newMeasurement };
    },
  },
};

export default Measurements;
