import gql from 'graphql-tag';
import log from 'modules/shared/logger';
import { Station, StationUpdateInput } from 'types/station';

export const Stations = {
  mutate: {
    edit: gql`
      mutation STATION_UPDATE($id: String!, $data: StationInput!) {
        stationUpdate(id: $id, data: $data) {
          id
          name
          disabled
          isMaster
          createdAt
          updatedAt
        }
      }
    `,
    delete: gql`
      mutation STATION_DESTROY($ids: [String]!) {
        stationDestroy(ids: $ids)
      }
    `,
    create: gql`
      mutation STATION_CREATE($data: StationInput!) {
        stationCreate(data: $data) {
          id
          name
          disabled
          isMaster
          createdAt
          updatedAt
        }
      }
    `,
  },
  query: {
    find: gql`
      query STATION_FIND($id: String!) {
        stationFind(id: $id) {
          id
          name
          disabled
          isMaster
          createdAt
          updatedAt
        }
      }
    `,
    list: gql`
      query STATION_LIST($filter: StationFilterInput, $orderBy: StationOrderByEnum, $limit: Int, $offset: Int) {
        stationList(filter: $filter, orderBy: $orderBy, limit: $limit, offset: $offset) {
          count
          rows {
            id
            name
            disabled
            isMaster
            createdAt
            updatedAt
          }
        }
      }
    `,
  },
  createGraphqlInput: (data: Station) => {
    if (!data) {
      log.error('No data was passed to create an input', data);
      return null;
    }
    const input: StationUpdateInput = {
      name: data.name,
      disabled: data.disabled || false,
    };
    return input;
  },
};
