import gql from 'graphql-tag';
import log from 'modules/shared/logger';
import { Sample, SampleUpdateInput } from 'types/sample';

export const Samples = {
  mutate: {
    edit: gql`
      mutation SAMPLE_UPDATE($id: String!, $data: SampleInput!) {
        sampleUpdate(id: $id, data: $data) {
          id
          serial
          sampleIndex
          featureCoverage
          status
          partId
          part {
            id
          }
          job {
            id
          }
          measurements {
            id
            value
            gage
            machine
            status
            characteristicId
            sampleId
            stationId
            operationId
            methodId
            createdById
            updatedById
          }
          updatingOperatorId
          createdAt
          updatedAt
        }
      }
    `,
    delete: gql`
      mutation SAMPLE_DESTROY($ids: [String]!) {
        sampleDestroy(ids: $ids)
      }
    `,
    create: gql`
      mutation SAMPLE_CREATE($data: SampleInput!) {
        sampleCreate(data: $data) {
          id
          serial
          sampleIndex
          featureCoverage
          status
          part {
            id
          }
          job {
            id
          }
          measurements {
            id
            value
            gage
            machine
            status
            characteristicId
            sampleId
            stationId
            operationId
            methodId
            createdById
            updatedById
          }
          createdAt
          updatedAt
        }
      }
    `,
  },
  query: {
    find: gql`
      query SAMPLE_FIND($id: String!) {
        sampleFind(id: $id) {
          id
          serial
          sampleIndex
          featureCoverage
          status
          part {
            id
          }
          job {
            id
          }
          measurements {
            id
            value
            gage
            machine
            status
            characteristicId
            sampleId
            stationId
            operationId
            methodId
            createdById
            updatedById
          }
          createdAt
          updatedAt
        }
      }
    `,
    list: gql`
      query SAMPLE_LIST($filter: SampleFilterInput, $orderBy: SampleOrderByEnum, $limit: Int, $offset: Int) {
        sampleList(filter: $filter, orderBy: $orderBy, limit: $limit, offset: $offset) {
          count
          rows {
            id
            serial
            sampleIndex
            featureCoverage
            status
            part {
              id
            }
            job {
              id
            }
            measurements {
              id
              value
              gage
              machine
              status
              characteristicId
              sampleId
              stationId
              operationId
              methodId
              createdById
              updatedById
            }
            createdAt
            updatedAt
          }
        }
      }
    `,
  },
  createGraphqlInput: (data: Sample) => {
    if (!data) {
      log.error('No data was passed to create an input', data);
      return null;
    }
    const input: SampleUpdateInput = {
      serial: data.serial,
      sampleIndex: data.sampleIndex,
      featureCoverage: data.featureCoverage || null,
      status: data.status,
    };
    return input;
  },
};
