import React, { useState, useEffect, RefObject, useRef } from 'react';
import { useHistory, useParams } from 'react-router-dom';
import DataTable from 'styleguide/METRO/DataTable/DataTable';
import Number from 'styleguide/METRO/NumPad/Number';
import { Cell } from 'styleguide/METRO/metro-types';
import { useDispatch, useSelector } from 'react-redux';
import { AppState } from 'modules/state';
import { Sample } from 'types/sample';
import { FixedSizeList, VariableSizeGrid } from 'react-window';
import { Characteristic } from 'types/characteristics';
import { Measurements } from 'metro/graphql/measurements';
import { Measurement, MeasurementInput } from 'types/measurement';
import actions, { createMeasurementThunk, OperatorState, updateMeasurementThunk } from 'modules/metro/metroActions';
import { checkSampleStatus, validateMeasurement } from 'utils/DataCalculation';
import Analytics from 'modules/shared/analytics/analytics';
import { COL_BUFFER, COL_WIDTH, ROW_HEIGHT, ROW_OFFSET } from 'styleguide/METRO/DataTable/TableDims';
import { Loader } from 'view/global/Loader';
import { MeasurementContainer } from 'styleguide/METRO/MeasurementContainer/MeasurementContainer';
import HeaderWrapper from '../common/headerWrapper/headerWrapper';
import './measureEntryStyle.less';

interface Params {
  jobId: string;
  partId: string;
}

interface JobSelectProps {
  changePart: (partId: string) => void;
}

const MeasureEntry = ({ changePart }: JobSelectProps) => {
  const { jobId, partId } = useParams<Params>();
  const { part } = useSelector((state: AppState) => state);
  const { initialized, operator, station, selectedCell, job, samples, gage } = useSelector((state: AppState) => state.metro);
  const history = useHistory<OperatorState>();
  const dispatch = useDispatch();

  const [val, setVal] = useState('');
  const [showNumpad, setShowNumpad] = useState<boolean>(false);
  const [animating, setAnimation] = useState('');
  const scrolling = useRef(false);
  const prevTop = useRef(ROW_HEIGHT * ROW_OFFSET);
  const prevLeft = useRef(COL_WIDTH * COL_BUFFER);

  const sampleRef = useRef<HTMLButtonElement>(null);
  const featureRef = useRef<HTMLDivElement>(null);
  const featureDetailRef = useRef<HTMLDivElement>(null);
  const listContainerRef = useRef<HTMLElement | null>(null);
  const measureRef = useRef<HTMLElement>(null);
  const measureContainerRef = useRef<HTMLElement | null>(null);
  const gridRef = useRef<VariableSizeGrid<any> | null>(null);
  const listRef = useRef<FixedSizeList<any>>(null);
  const sampleContainerRef = useRef<Element | null>(null);

  const partSamples = samples?.filter((sample: Sample) => sample.partId === part?.part?.id).sort((a: Sample, b: Sample) => (a.sampleIndex < b.sampleIndex ? -1 : 1)) || null;

  const partFeatures = part.part?.characteristics?.filter((feature: Characteristic) => feature.status === 'Active').sort((a: Characteristic, b: Characteristic) => (a.markerIndex < b.markerIndex ? -1 : 1)) || null;

  useEffect(() => {
    if (initialized && !job) {
      if (jobId && partId) {
        dispatch(actions.doInitMeasure(jobId, partId));
        changePart(partId);
      } else {
        history.push('/metro/job', history.location.state);
      }
    }
  }, [initialized]);

  const clonedFeatureRef = useRef<HTMLElement[]>([]);
  const clonedSampleRef = useRef<HTMLElement[]>([]);

  const removeClonedElements = () => {
    if (clonedFeatureRef.current) {
      clonedFeatureRef.current.forEach((clone) => clone.remove());
      clonedFeatureRef.current = [];
    }
    if (clonedSampleRef.current) {
      clonedSampleRef.current.forEach((clone) => clone.remove());
      clonedSampleRef.current = [];
    }
  };

  const handleCloseNumpad = () => {
    removeClonedElements();
    setShowNumpad(false);
  };

  // Clones elements to place on top of mask when numpad is open
  const cloneElement = (ref: RefObject<any>) => {
    if (ref.current) {
      const { left, top, height, width } = ref.current.getBoundingClientRect();
      const clonedNode = ref.current.cloneNode(true);
      (clonedNode as HTMLElement).style.left = `${left}px`;
      (clonedNode as HTMLElement).style.top = `${top}px`;
      (clonedNode as HTMLElement).style.height = `${height}px`;
      (clonedNode as HTMLElement).style.width = `${width}px`;
      (clonedNode as HTMLElement).style.zIndex = '10000';
      (clonedNode as HTMLElement).style.position = `absolute`;
      document.body.appendChild(clonedNode);
      return clonedNode;
    }
    return null;
  };

  const handleCloneElements = (left: number, top: number) => {
    removeClonedElements();
    if (showNumpad) {
      const maxDistance = Math.max(Math.abs(left - prevLeft.current), top - prevTop.current);
      const setTimeout = require('timers-browserify');
      setTimeout(
        () => {
          clonedFeatureRef.current.push(cloneElement(featureDetailRef.current ? featureDetailRef : featureRef));
          clonedSampleRef.current.push(cloneElement(sampleRef));
          scrolling.current = false;
        },
        Math.max(maxDistance * 1.5, 500),
      ); // TODO: better way to find/estimate time to scroll?
    }
    prevLeft.current = left;
    prevTop.current = top;
  };

  const handleScrollElements = () => {
    if (!sampleContainerRef.current) sampleContainerRef.current = document.getElementsByClassName('ant-table-header')[0] || null;

    const scrollTop = selectedCell.featureIndex * ROW_HEIGHT + ROW_HEIGHT * ROW_OFFSET;
    const scrollLeft = (selectedCell.sampleIndex + COL_BUFFER) * COL_WIDTH + COL_WIDTH;

    if (measureContainerRef.current) {
      measureContainerRef.current.scrollTo({ left: scrollLeft - measureContainerRef.current.getBoundingClientRect().width, top: scrollTop, behavior: 'smooth' });
    }

    if (sampleContainerRef.current) {
      sampleContainerRef.current.scrollTo({ left: scrollLeft - sampleContainerRef.current.getBoundingClientRect().width, behavior: 'smooth' });
    }

    if (listContainerRef.current) {
      listContainerRef.current.scrollTo({ top: scrollTop, behavior: 'smooth' });
    }

    handleCloneElements(scrollLeft, scrollTop);
  };

  useEffect(() => {
    if (initialized && selectedCell.selectedFeature) {
      setVal(selectedCell.selectedMeasurement?.value || '');
      if (showNumpad) scrolling.current = true;
      handleScrollElements();
    }
  }, [showNumpad, selectedCell.selectedMeasurement?.id, selectedCell.selectedFeature?.id, selectedCell.selectedSample?.id]);

  useEffect(() => {
    window.addEventListener('resize', handleScrollElements);

    return function cleanup() {
      window.removeEventListener('resize', handleScrollElements);
    };
  }, []);

  const handleSelectCell = (cell: Cell) => {
    if (!partFeatures || !partSamples) return;
    const featureIndex = partFeatures.findIndex((feat: Characteristic) => feat.id === cell.feature.id);
    const sampleIndex = partSamples.findIndex((samp: Sample) => samp.id === cell.sample.id);
    if (featureIndex >= 0 && sampleIndex >= 0) {
      const measurements = cell.sample?.measurements?.filter((m: Measurement) => m.characteristicId === cell.feature?.id);
      dispatch(actions.doSelectCell({ featureIndex, sampleIndex, selectedFeature: cell.feature, selectedSample: cell.sample, measurementIndex: measurements?.length || 0, selectedMeasurement: null }));
    }
  };

  const handleNavigateCellsForward = (nextFeature: boolean, sampIndex: number, featIndex: number, backToTop = true, skipScrap: boolean): boolean => {
    if (!partFeatures || !partSamples) return false;
    if (nextFeature && featIndex < partFeatures.length - 1) {
      const ind = featIndex + 1;
      const nextFeat = partFeatures[ind];
      const measurements = partSamples[sampIndex]?.measurements.filter((m: Measurement) => m.characteristicId === nextFeat.id) || [];
      dispatch(actions.doSelectCell({ ...selectedCell, featureIndex: ind, selectedFeature: nextFeat, measurementIndex: measurements?.length || 0, selectedMeasurement: measurements[measurements?.length] || null }));
      setAnimation('up');
    } else if (sampIndex < partSamples.length - 1) {
      const ind = sampIndex + 1;
      const nextFeat = partFeatures[backToTop ? 0 : featIndex];
      const measurements = partSamples[ind]?.measurements.filter((m: Measurement) => m.characteristicId === nextFeat.id) || [];
      if (skipScrap && partSamples[ind].status === 'Scrap') return handleNavigateCellsForward(false, ind, featIndex, backToTop, true);
      dispatch(
        actions.doSelectCell({
          ...selectedCell,
          featureIndex: backToTop ? 0 : featIndex,
          selectedFeature: nextFeat,
          sampleIndex: ind,
          selectedSample: partSamples[ind] || null,
          measurementIndex: measurements?.length || 0,
          selectedMeasurement: measurements[measurements?.length] || null,
        }),
      );
      setAnimation(backToTop ? 'down' : '');
    } else if (featIndex < partFeatures.length - 1) {
      const ind = featIndex + 1;
      if (skipScrap && partSamples[0].status === 'Scrap') return handleNavigateCellsForward(false, ind, 0, true, true);
      const nextFeat = partFeatures[ind];
      const measurements = partSamples[0]?.measurements.filter((m: Measurement) => m.characteristicId === nextFeat.id) || [];
      dispatch(
        actions.doSelectCell({
          ...selectedCell,
          featureIndex: ind,
          selectedFeature: nextFeat,
          sampleIndex: 0,
          selectedSample: partSamples[0],
          measurementIndex: measurements?.length || 0,
          selectedMeasurement: measurements[measurements?.length] || null,
        }),
      );
      setAnimation('up');
    } else {
      setShowNumpad(false);
    }
    return true;
  };

  const handleNavigateCellsBackward = (nextFeature: boolean, sampIndex: number, featIndex: number, backToTop = true, skipScrap: boolean): boolean => {
    if (!partFeatures || !partSamples) return false;
    if (nextFeature && featIndex > 0) {
      const ind = featIndex - 1;
      const nextFeat = partFeatures[ind];
      const measurements = partSamples[sampIndex]?.measurements.filter((m: Measurement) => m.characteristicId === nextFeat.id) || [];
      dispatch(actions.doSelectCell({ ...selectedCell, featureIndex: ind, selectedFeature: nextFeat, measurementIndex: measurements?.length || 0, selectedMeasurement: measurements[measurements?.length] || null }));
      setAnimation('down');
    } else if (sampIndex > 0) {
      const ind = sampIndex - 1;
      if (skipScrap && partSamples[ind].status === 'Scrap') return handleNavigateCellsBackward(false, ind, featIndex, backToTop, true);
      const nextFeat = partFeatures[backToTop ? partFeatures.length - 1 : featIndex];
      const measurements = partSamples[ind]?.measurements.filter((m: Measurement) => m.characteristicId === nextFeat.id) || [];
      dispatch(
        actions.doSelectCell({
          ...selectedCell,
          featureIndex: backToTop ? partFeatures.length - 1 : featIndex,
          selectedFeature: nextFeat,
          sampleIndex: ind,
          selectedSample: partSamples[ind],
          measurementIndex: measurements?.length || 0,
          selectedMeasurement: measurements[measurements?.length] || null,
        }),
      );
      setAnimation(backToTop ? 'up' : '');
    } else if (featIndex > 0) {
      const ind = featIndex - 1;
      if (skipScrap && partSamples[partSamples.length - 1].status === 'Scrap') return handleNavigateCellsBackward(true, sampIndex - 1, featIndex, backToTop, true);
      const nextFeat = partFeatures[ind];
      const measurements = partSamples[partSamples.length - 1]?.measurements.filter((m: Measurement) => m.characteristicId === nextFeat.id) || [];
      dispatch(
        actions.doSelectCell({
          ...selectedCell,
          featureIndex: ind,
          selectedFeature: nextFeat,
          sampleIndex: partSamples.length - 1,
          selectedSample: partSamples[partSamples.length - 1],
          measurementIndex: measurements?.length || 0,
          selectedMeasurement: measurements[measurements?.length] || null,
        }),
      );
      setAnimation(backToTop ? 'up' : '');
    }

    return true;
  };

  const handleTableNav = (type: string, skipScrap = true) => {
    if (!scrolling.current) {
      switch (type) {
        case 'next_feature':
          handleNavigateCellsForward(true, selectedCell.sampleIndex, selectedCell.featureIndex, true, skipScrap);
          break;
        case 'next_sample':
          handleNavigateCellsForward(false, selectedCell.sampleIndex, selectedCell.featureIndex, false, skipScrap);
          break;
        case 'prev_feature':
          handleNavigateCellsBackward(true, selectedCell.sampleIndex, selectedCell.featureIndex, true, skipScrap);
          break;
        case 'prev_sample':
          handleNavigateCellsBackward(false, selectedCell.sampleIndex, selectedCell.featureIndex, false, skipScrap);
          break;
        default:
          break;
      }
    }
  };

  const handleSelectMeasurement = (index: number, measurement: Measurement | null) => {
    Analytics.track({
      event: Analytics.events.measurementSelected,
      properties: {
        measurement,
        operator,
        part: part.part,
        characteristic: selectedCell.selectedFeature,
      },
    });
    dispatch(actions.doSelectCell({ ...selectedCell, measurementIndex: index, selectedMeasurement: measurement }));
  };

  const recordMeasurement = (value: string, shiftPressed = false, ctrlPressed = false) => {
    if (!selectedCell.selectedFeature || !selectedCell.selectedSample || !station?.id) return;
    const measurement: MeasurementInput = {
      value,
      gage: gage || '',
      machine: '',
      status: validateMeasurement(value, selectedCell.selectedFeature),
      characteristicId: selectedCell.selectedFeature.id,
      sampleId: selectedCell.selectedSample.id,
      stationId: station.id,
      methodId: selectedCell.selectedFeature.inspectionMethod || null,
      operationId: selectedCell.selectedFeature.operation || null,
      operatorId: operator.id,
    };

    if (!selectedCell.selectedMeasurement) {
      Analytics.track({
        event: Analytics.events.measurementCreated,
        properties: {
          measurement,
          operator,
          part: part.part,
          characteristic: selectedCell.selectedFeature,
        },
      });
      dispatch(createMeasurementThunk(measurement, partFeatures || []));
    } else {
      Analytics.track({
        event: Analytics.events.measurementUpdated,
        properties: {
          measurement,
          operator,
          part: part.part,
          characteristic: selectedCell.selectedFeature,
        },
      });
      dispatch(updateMeasurementThunk(measurement, selectedCell.selectedMeasurement.id, partFeatures || []));
    }

    if (measurement.status === 'Fail' && selectedCell.selectedSample.status !== 'Fail') {
      dispatch(actions.doUpdateSample({ ...selectedCell.selectedSample, status: 'Fail', updatingOperatorId: operator.id }));
    } else if (measurement.status === 'Pass') {
      // Check all features of this sample to check if sample should be flipped to Pass or Unmeasured
      const sampleStatus = checkSampleStatus(selectedCell.selectedSample, partFeatures || []);
      if (sampleStatus !== selectedCell.selectedSample.status) dispatch(actions.doUpdateSample({ ...selectedCell.selectedSample, status: sampleStatus, updatingOperatorId: operator.id }));
    }

    if (shiftPressed) {
      handleTableNav('next_sample');
    } else if (ctrlPressed) {
      handleTableNav('next_feature');
    }
  };

  const destroyMeasurement = (id: string) => {
    if (partFeatures && selectedCell.selectedMeasurement) dispatch(updateMeasurementThunk(Measurements.util.input(selectedCell.selectedMeasurement, operator.id).measure, id, partFeatures, true));
  };

  const handleSetGage = (newGage: string) => {
    dispatch(actions.doSetGage(newGage));
  };

  return (
    <HeaderWrapper>
      <main className="ixc-measure-content">
        {!partSamples || !partFeatures ? (
          <Loader error={undefined} loading={50} closeError={() => {}} className="metro-loader" />
        ) : (
          <DataTable
            measurementMethod="micrometer"
            samples={partSamples}
            features={partFeatures}
            onCellClick={(cell: Cell) => {
              if (cell.feature && cell.sample) {
                handleSelectCell(cell);
                setShowNumpad(true);
              }
            }}
            sampleRef={sampleRef}
            featureRef={featureRef}
            featureDetailRef={featureDetailRef}
            listContainerRef={listContainerRef}
            measureContainerRef={measureContainerRef}
            gridRef={gridRef}
            listRef={listRef}
            handleTableNav={handleTableNav}
            animating={animating}
            setAnimation={setAnimation}
          />
        )}
      </main>
      {showNumpad && (
        <div className="ixc-numpad-overlay">
          <Number
            style={{}}
            onChange={() => {}}
            recordMeasurement={recordMeasurement}
            position="startBottomRight"
            inline
            placeholder=""
            value={val}
            decimal={2}
            keyValidator={() => true}
            cancel={handleCloseNumpad}
            negative
            featureIndex={selectedCell.featureIndex}
            sampleIndex={selectedCell.sampleIndex}
            measurementIndex={selectedCell.measurementIndex}
            measurementId={selectedCell.selectedMeasurement?.id || null}
            handleTableNav={handleTableNav}
            gage={gage || ''}
            setGage={handleSetGage}
            selectedCell={selectedCell}
          >
            <></>
          </Number>
          <MeasurementContainer required={undefined} measureRef={measureRef} top={(measureContainerRef.current?.getBoundingClientRect().top || 0) + ROW_HEIGHT} selectMeasurement={handleSelectMeasurement} deleteMeasurement={destroyMeasurement} />
        </div>
      )}
    </HeaderWrapper>
  );
};

export default MeasureEntry;
