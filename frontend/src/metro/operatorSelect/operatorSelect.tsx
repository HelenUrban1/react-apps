import React, { useEffect, useRef, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { RouteComponentProps, useHistory } from 'react-router-dom';
import { i18n } from 'i18n';
import Analytics from 'modules/shared/analytics/analytics';
import actions, { OperatorState } from 'modules/metro/metroActions';
import { AppState } from 'modules/state';
import Profile from 'styleguide/METRO/Profile/Profile';
import './operatorSelectStyle.less';
import ReactPinField from 'react-pin-field';
import { Operator } from 'types/operator';
import SelectHeader from '../common/selectHeader/selectHeader';

const OperatorView: React.FC<RouteComponentProps<{}>> = () => {
  const { operator, operators } = useSelector((state: AppState) => state.metro);
  const [showPin, setShowPin] = useState<boolean>(false);
  const [currentOperator, setCurrentOperator] = useState<Operator | null>(null);
  const history = useHistory<OperatorState>();
  const dispatch = useDispatch();

  const pinRef = useRef<any>();

  useEffect(() => {
    if (pinRef.current && pinRef.current.inputs && showPin) {
      pinRef.current.inputs[0].focus();
    }
  }, [showPin]);

  const setOperator = (operator: Operator) => {
    setShowPin(false);
    dispatch(actions.doSetOperator(operator));
    Analytics.track({
      event: Analytics.events.operatorSelect,
      properties: {
        operator,
      },
    });
    history.push('/metro/station', { ...history.location.state, operator: operator.id });
  };

  const fetchAndCheckOperator = (operatorId: string) => {
    const matchingOperator = operators.find((op) => op.id === operatorId);
    if (matchingOperator.pin && matchingOperator.pin.length > 0) {
      setCurrentOperator(matchingOperator);
      setShowPin(true);
      return;
    }
    setOperator(matchingOperator);
  };

  if (operator) {
    history.push('/metro/station', { ...history.location.state, operator: operator.id });
  }

  return (
    <main className="ixc-operator-container">
      <SelectHeader headerText={i18n('metro.operator.whoIsInspecting')} />
      {currentOperator && (
        <div style={{ backgroundColor: 'rgba(0,0,0,0.5)', display: showPin ? 'block' : 'none', position: 'absolute', marginLeft: 'auto', marginRight: 'auto', left: '35%', top: '40%', textAlign: 'center', padding: '75px' }}>
          <ReactPinField
            length={currentOperator.pin.length}
            style={{
              backgroundColor: 'rgb(248, 249, 250)',
              border: '1px solid rgb(204, 204, 204)',
              borderRadius: '0.3rem',
              fontSize: '2rem',
              margin: '0.25rem',
              height: '3.5rem',
              outline: 'none',
              textAlign: 'center',
              transitionDuration: '250ms',
              transitionProperty: 'background, color, border, box-shadow, transform',
              width: '3rem',
              color: 'black',
            }}
            validate={() => true}
            onComplete={(test) => {
              if (test === currentOperator.pin) return setOperator(currentOperator);
              pinRef.current.inputs.forEach((input: HTMLInputElement) => {
                input.value = '';
                input.style.borderColor = 'red';
                input.style.borderWidth = '3px';
              });
              pinRef.current.inputs[0].focus();
            }}
            ref={pinRef}
          />
        </div>
      )}
      <section className="ixc-user-grid-container">
        {operators.map((op) => (
          <div key={op.id} className="ixc-user-grid-item" data-cy="metro_operator_block">
            <Profile key={op.fullName} id={op.id} title={op.fullName || op.email} variant={Profile.Variants.User} onClick={fetchAndCheckOperator} />
          </div>
        ))}
      </section>
    </main>
  );
};

export default OperatorView;
