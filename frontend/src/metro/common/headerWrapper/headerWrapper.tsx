import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';
import actions from 'modules/metro/metroActions';
import { doSetPart } from 'modules/part/partActions';
import NavHeader from 'styleguide/METRO/NavHeader/NavHeader';
import './headerWrapper.less';
import { AppState } from 'modules/state';
import { Station } from 'types/station';

const HeaderWrapper: React.FC<any> = ({ children }) => {
  const dispatch = useDispatch();
  const history = useHistory();
  const { operator, station, stations } = useSelector((state: AppState) => state.metro);

  const logout = () => {
    dispatch(actions.doSetOperator(null));
    dispatch(actions.doSetStation(null));
    dispatch(doSetPart(null, null));
    dispatch(actions.doSetJob(null));
    history.push('/metro', { operator: undefined, station: undefined });
  };

  const handleStationChange = (newStation: Station) => {
    if (station?.id !== newStation.id) {
      dispatch(actions.doSetStation(newStation));
      history.push(history.location.pathname, { operator: operator.id, station: newStation.id });
    }
  };

  const handleLogoClick = () => {
    // TODO: check if user/operator has more than metro access, if so send to parts list
    history.push('/metro/job', history.location.state);
  };
  if (!station) {
    return <></>;
  }
  return (
    <div className="ixc-header-wrapper-container" data-cy="metro_header">
      <NavHeader operator={operator} station={station} stations={stations} onLogout={logout} onLogoClick={handleLogoClick} onStationSelect={handleStationChange} />
      {children}
    </div>
  );
};

export default HeaderWrapper;
