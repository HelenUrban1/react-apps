import React, { useState } from 'react';
import { SearchOutlined } from '@ant-design/icons';
import { i18n } from 'i18n';

import { BrandColors } from 'view/global/defaults';
import { TypeLogo } from 'styleguide/shared/svg';
import './selectHeaderStyle.less';

// We can generate alphabet search element using this
// const ALPHABET = 'abcdefghijklmnopqrstuvwxyz'.split('');

interface Props {
  headerText: string;
}

const SelectHeader: React.FC<Props> = ({ headerText }: Props) => {
  const [isSearching, setIsSearching] = useState(false);

  const SearchButton = (
    <button
      className="search-button"
      type="button"
      onClick={() => {
        setIsSearching(true);
      }}
    >
      <SearchOutlined />
    </button>
  );

  const CancelButton = (
    <button
      className="cancel-button"
      type="button"
      onClick={() => {
        setIsSearching(false);
      }}
    >
      {i18n('common.cancel')}
    </button>
  );

  return (
    <main>
      <div className="select-header">
        <div className="logo-icon">
          <TypeLogo color={BrandColors.Purple} />
        </div>
        {isSearching ? CancelButton : SearchButton}
      </div>
      <div className="ixc-grid-header" data-cy="metro_login_title">
        {headerText}
      </div>
    </main>
  );
};

export default SelectHeader;
