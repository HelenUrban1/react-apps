export interface File {
  id: string;
  name: string;
  sizeInBytes?: number;
  privateUrl?: string;
  publicUrl: string;
}

export interface FileInput {
  id?: string;
  name: string;
  sizeInBytes?: number;
  privateUrl?: string;
  publicUrl: string;
  new?: boolean;
}
