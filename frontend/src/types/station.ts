export type Station = {
  id: string;
  name: string;
  disabled: boolean;
  isMaster: boolean;

  // Milestone events
  createdAt: null | Date;
  updatedAt: null | Date;
};

export type StationUpdateInput = {
  name: string;
  disabled: boolean;
};

// Type definitions for the returned api data
export type StationCreate = {
  stationCreate: Station;
};

export type StationFind = {
  stationFind: Station;
};

export type StationList = {
  stationList: {
    count: number;
    rows: Station[];
  };
};

export type StationEdit = {
  stationUpdate: StationUpdateInput;
};
