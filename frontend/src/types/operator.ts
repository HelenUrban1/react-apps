export type AccessControlEnum = 'None' | 'ITAR';
export type StatusEnum = 'Active' | 'Inactive';

export interface Operator {
  id: string;
  fullName: string;
  firstName: string;
  lastName: string;
  pin: string;
  email: string | null;
  accountMemberId: string;
  status: StatusEnum;
  accessControl: AccessControlEnum;
}

export interface OperatorInput {
  data: {
    fullName: string;
    firstName: string;
    lastName: string;
    pin: string;
    email: string | null;
    accountMemberId: string;
    status: StatusEnum;
    accessControl: AccessControlEnum;
  };
}

export interface OperatorCreate {
  operatorCreate: Operator;
}

export interface OperatorFilterInput {
  id?: string;
  fullName: string;
  firstName?: string;
  lastName?: string;
  email?: string;
  accessControl?: AccessControlEnum;
}
