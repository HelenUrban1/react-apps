export type MarkerShape = 'Circle' | 'Triangle' | 'Diamond' | 'Hexagon';

export type MarkerColor = 'Purple' | 'Coral' | 'Seafoam' | 'Sunshine' | 'Black' | 'White';

export type MarkerStrokeStyleEnum = 'Solid' | 'Dashed' | 'Dotted';

export interface Marker {
  id: string;
  markerStyleName: string;
  shape: MarkerShape;
  defaultPosition: string;
  hasLeaderLine: Boolean;
  fontSize: number;
  fontColor: string;
  fillColor: string;
  borderColor: string;
  borderWidth: number;
  borderStyle: MarkerStrokeStyleEnum;
  leaderLineColor: MarkerColor;
  leaderLineWidth: number;
  leaderLineStyle: MarkerStrokeStyleEnum;
  deletedAt: string | null;
}

export interface MarkerStyle {
  style: string;
  assign: string[];
}
