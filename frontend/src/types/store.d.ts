import { User } from './user.ts';

export interface State {
  router: any;
  layout: any;
  auth: {
    currentUser: User;
    loadingInit: boolean;
    loadingEmailConfirmation: boolean;
    loadingPasswordResetEmail: boolean;
    loadingVerifyEmail: boolean;
    loadingPasswordReset: boolean;
    loadingUpdateProfile: boolean;
    loading: boolean;
    errorMessage: null | string;
  };
  iam: any;
  auditLog: any;
  account: any;
  accountMember: any;
  site: any;
  metro: any;
}
