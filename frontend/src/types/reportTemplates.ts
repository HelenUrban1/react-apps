import { CharacteristicData } from 'types/characteristics';
import { File, FileInput } from 'types/file';
import { Organization } from 'graphql/organization';
import { Part } from 'domain/part/partTypes';

// Enums
export type ReportTemplateStatusEnum = 'Active' | 'Inactive' | 'Deleted';
export type ReportTemplateDirectionEnum = 'Vertical' | 'Horizontal';
export type TokenTypeEnum = 'Single' | 'Dynamic';
export type ReportStatusEnum = 'Active' | 'Inactive' | 'Deleted';
export type ReportOrderByEnum = 'id_ASC' | 'id_DESC' | 'partVersion_ASC' | 'partVersion_DESC' | 'title_ASC' | 'title_DESC' | 'status_ASC' | 'status_DESC' | 'createdAt_ASC' | 'createdAt_DESC';

export type ReportTemplateSettings = {
  footers?: { [key: string]: { [key: string]: { row: number; col: number; footer: number } } };
  tokens?: string[];
  saved: boolean;
};

export type ReportTemplateOrderByEnum =
  | 'id_ASC'
  | 'id_DESC'
  | 'provider_ASC'
  | 'provider_DESC'
  | 'title_ASC'
  | 'title_DESC'
  | 'type_ASC'
  | 'type_DESC'
  | 'status_ASC'
  | 'status_DESC'
  | 'direction_ASC'
  | 'direction_DESC'
  | 'sortIndex_ASC'
  | 'sortIndex_DESC'
  | 'createdAt_ASC'
  | 'createdAt_DESC';

// Types
export interface ReportTemplate {
  id: string;
  provider: Organization; // TODO the default template providers should not be 'none', it breaks type checking
  title: string;
  type?: string;
  status: ReportTemplateStatusEnum;
  direction: ReportTemplateDirectionEnum;
  settings?: string;
  file: File[];
  sortIndex?: number;
  filters?: string;
  createdAt?: Date;
  updatedAt?: Date;
  [key: string]: string | Organization | ReportTemplateStatusEnum | ReportTemplateDirectionEnum | File[] | number | Date | undefined;
}

export interface ReportTemplateFilterInput {
  id?: string;
  title?: string;
  provider?: string[];
  type?: string;
  file?: FileInput[];
  status?: ReportTemplateStatusEnum[];
  sortIndexRange?: number[];
  createdAtRange?: Date[];
  deletedAtRange?: Date[];
  [key: string]: string | string[] | ReportTemplateStatusEnum[] | FileInput[] | number[] | Date[] | undefined;
}

export interface ReportTemplateInput {
  title: string;
  provider: string;
  type?: string;
  file: FileInput[];
  status: ReportTemplateStatusEnum;
  direction: ReportTemplateDirectionEnum;
  settings?: string;
  sortIndex?: number;
  filters?: string;
  createdAt?: Date | string;
  [key: string]: string | FileInput[] | Date | ReportTemplateStatusEnum | ReportTemplateDirectionEnum | number | undefined;
}

export type TokenType = {
  group: string;
  name: string;
  token: string;
  example: string;
  type: TokenTypeEnum;
  fullToken?: string;
};

export interface ReportPartial {
  id: string;
  status: ReportStatusEnum;
  partVersion: string;
  title: string;
  templateUrl: string;
  updatedAt: Date;
  data?: string;
  filters?: string | null;
  file: File[];
  [key: string]: string | File[] | ReportStatusEnum | Date | undefined | null;
}

export interface Report {
  id: string;
  customer?: Organization;
  part?: Part;
  partVersion?: string;
  template?: ReportTemplate;
  data?: string;
  title?: string;
  templateUrl: string;
  filters: string | null;
  partUpdatedAt?: string | null;
  file: File[];
  status?: ReportStatusEnum;
  createdAt?: Date;
  updatedAt?: Date;
  [key: string]: string | undefined | Report | Organization | Part | ReportTemplate | null | File[] | ReportStatusEnum | Date;
}

export interface LocationState {
  from: string;
  partId: string;
  part: Part;
  template: ReportTemplate | null;
  source: string;
}

export interface ReportData {
  customer: {
    name: string;
  };
  part: {
    name: string;
    number: string;
    revision: string;
    control: string;
    status: string;
    updated: string;
  };
  primary: {
    name: string;
    number: string;
    revision: string;
    control: string;
  };
  drawings: string[];
  item: CharacteristicData[];
}
export interface Filter {
  category: string[] | undefined;
  value: any;
}
export interface FilterObject {
  [key: string]: Filter;
}

// Inputs
export interface ReportInput {
  customer?: string;
  file: FileInput[];
  part: string;
  partVersion: string;
  template: string;
  data?: string;
  title: string;
  templateUrl: string;
  filters?: string;
  partUpdatedAt?: string | null;
  status: ReportStatusEnum;
  [key: string]: string | undefined | null | FileInput[] | ReportStatusEnum | Date;
}

export interface ReportFilterInput {
  id?: string;
  customer?: string;
  part?: string;
  partVersion?: string;
  template?: string;
  title?: string;
  status?: ReportStatusEnum;
  createdAtRange?: Date[];
  [key: string]: string | undefined | ReportStatusEnum | Date[];
}
