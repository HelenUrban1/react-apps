import { ListItem } from 'domain/setting/listsTypes';
import { Characteristic } from 'types/characteristics';
import { Operator } from './operator';
import { Sample } from './sample';
import { Station } from './station';

export enum MeasurementStatusEnum {
  Pass = 'Pass',
  Fail = 'Fail',
  Unmeasured = 'Unmeasured',
}

export interface Measurement {
  id: string;
  value: string;
  gage: string;
  machine: string;
  status: MeasurementStatusEnum;
  characteristicId: string;
  characteristic: Characteristic;
  sampleId: string;
  sample: Sample;
  stationId: string;
  station: Station;
  methodId: string;
  method: ListItem;
  operationId: string;
  operation: ListItem;
  createdAt: Date;
  updatedAt: Date;
  deletedAt: Date;
  createdById: string;
  createdBy: Operator;
  updatedById: string;
  updatedBy: Operator;
}

export interface MeasurementInput {
  value: string;
  gage: string;
  machine: string;
  status: MeasurementStatusEnum;
  characteristicId: string;
  operatorId: string;
  sampleId: string;
  stationId: string;
  methodId: string | null;
  operationId: string | null;
}

export interface MeasurementFilterInput {
  id?: string;
  value?: string;
  gage?: string;
  machine?: string;
  status?: MeasurementStatusEnum;
  characteristic?: Characteristic;
  operator?: Operator;
  sample?: Sample;
  station?: Station;
  method?: ListItem;
  operation?: ListItem;
}
