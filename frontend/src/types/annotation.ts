import { Part } from 'domain/part/partTypes';
import { Drawing } from 'graphql/drawing';
import { DrawingSheet } from 'graphql/drawing_sheet';
import IxcTheme from 'styleguide/styles/IxcTheme';
import { Characteristic } from 'types/characteristics';

export type AnnotationActionTypes = 'ANNOTATIONS_ERROR' | 'ANNOTATIONS_UPDATE' | 'ANNOTATIONS_EDIT_ANNOTATION' | 'ANNOTATIONS_CREATE_ANNOTATION' | 'ANNOTATIONS_DELETE_ANNOTATION' | 'ANNOTATIONS_SET_REVISION';

export type CreateAnnotationsPayload = AnnotationInput[];

export interface EditAnnotationsPayload {
  annotations: Annotation[];
  updates: Annotation[];
  newAnnotations: AnnotationInput[];
}

export type AnnotationActionPayloads = EditAnnotationsPayload | CreateAnnotationsPayload | string | boolean | undefined;

export interface Annotation {
  id: string;
  annotation: string;
  fontColor: string;
  fontSize: number;
  borderColor: string;
  backgroundColor: string;
  previousRevision: string | null;
  nextRevision: string | null;
  originalAnnotation: string | null;
  drawingSheetIndex: number;
  drawingRotation: number;
  drawingScale: number;
  boxLocationY: number;
  boxLocationX: number;
  boxWidth: number;
  boxHeight: number;
  boxRotation: number;
  part: Part;
  drawing: Drawing;
  drawingSheet: DrawingSheet;
  characteristic: Characteristic;
  createdAt: Date;
  updatedAt: Date;
  deletedAt: Date;
  [key: string]: string | number | boolean | Date | Part | Drawing | DrawingSheet | Characteristic | undefined | null;
}
export interface AnnotationInput {
  annotation: string;
  fontSize?: number;
  fontColor?: string;
  borderColor?: string;
  backgroundColor?: string;
  drawingSheetIndex: number;
  previousRevision?: string | null;
  nextRevision?: string | null;
  originalAnnotation?: string | null;
  drawingRotation: number;
  drawingScale: number;
  boxLocationY: number;
  boxLocationX: number;
  boxWidth: number;
  boxHeight: number;
  boxRotation: number;
  partId: string;
  drawingId: string;
  drawingSheetId: string;
  characteristicId?: string;
  [key: string]: string | number | boolean | undefined | null;
}

export interface AnnotationEditVars {
  id: string;
  data: AnnotationInput;
}

export interface AnnotationCreateVars {
  data: AnnotationInput;
}

export interface AnnotationEdit {
  annotationUpdate: Annotation;
}
export interface AnnotationCreate {
  annotationCreate: Annotation;
}

export interface AnnotationFind {
  annotationFind: Annotation;
}

export interface AnnotationDelete {
  annotationDestroy: boolean;
}
export interface AnnotationDeleteVars {
  ids: string[];
}

export type AnnotationOrderByEnum =
  | 'id_ASC'
  | 'id_DESC'
  | 'annotation_ASC'
  | 'annotation_DESC'
  | 'fontSize_ASC'
  | 'fontSize_DESC'
  | 'fontColor_ASC'
  | 'fontColor_DESC'
  | 'borderColor_ASC'
  | 'borderColor_DESC'
  | 'backgroundColor_ASC'
  | 'backgroundColor_DESC'
  | 'drawingSheetIndex_ASC'
  | 'drawingSheetIndex_DESC'
  | 'drawingRotation_ASC'
  | 'drawingRotation_DESC'
  | 'drawingScale_ASC'
  | 'drawingScale_DESC'
  | 'boxLocationY_ASC'
  | 'boxLocationY_DESC'
  | 'boxLocationX_ASC'
  | 'boxLocationX_DESC'
  | 'boxWidth_ASC'
  | 'boxWidth_DESC'
  | 'boxHeight_ASC'
  | 'boxHeight_DESC'
  | 'boxRotation_ASC'
  | 'boxRotation_DESC'
  | 'createdAt_ASC'
  | 'createdAt_DESC'
  | 'deletedAt_ASC'
  | 'deletedAt_DESC';

export interface AnnotationFilterInput {
  id?: string;
  annotation?: string;
  fontSize?: number;
  fontColor?: string;
  borderColor?: string;
  backgroundColor?: string;
  drawingSheetIndexRange?: number[];
  drawingRotationRange?: number[];
  drawingScaleRange?: number[];
  boxLocationYRange?: number[];
  boxLocationXRange?: number[];
  boxWidthRange?: number[];
  boxHeightRange?: number[];
  boxRotationRange?: number[];
  part?: string;
  drawing?: string;
  drawingSheet?: string;
  characteristic?: string;
  originalAnnotation?: string;
  createdAtRange?: Date[];
  deletedAtRange?: Date[];
}

export interface AnnotationList {
  annotationList?: {
    count: number;
    rows: Annotation[];
  };
}

export interface AnnotationListVars {
  filter?: AnnotationFilterInput;
  orderBy?: AnnotationOrderByEnum;
  limit?: number;
  offset?: number;
}

export const ANNOTATION_TEXT_PADDING = 6;

export const defaultAnnotation = {
  annotation: 'Double click to edit',
  fontSize: 18,
  fontColor: '#000000',
  borderColor: IxcTheme.colors.noteColorDark,
  backgroundColor: IxcTheme.colors.noteColor,
  drawingSheetIndex: 1,
  drawingRotation: 0,
  drawingScale: 1.0,
  boxLocationY: 10,
  boxLocationX: 10,
  boxWidth: 200,
  boxHeight: 100,
  boxRotation: 0,
};
