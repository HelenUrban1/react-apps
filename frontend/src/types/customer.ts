export interface Customer {
  id: string;
  name: string;
  contact: string;
  email: string;
  phone: string;
}
