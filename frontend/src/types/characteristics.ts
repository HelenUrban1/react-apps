import { Part } from 'domain/part/partTypes';
import { Drawing } from 'graphql/drawing';
import { DrawingSheet } from 'graphql/drawing_sheet';
import { Marker, MarkerStyle } from 'types/marker';
import { CaptureDimensions } from 'utils/ParseTextController/types';
import { Annotation } from './annotation';

export interface CapturedItem {
  id: string;
  itemNumber: null | string;
  rectangle: null | fabric.Rect;
  balloon: null | fabric.Group;
  leaderLine: null | fabric.Line;
  connectionPoint: null | fabric.Circle;
  annotation: null | fabric.Group;
}
export interface CharacteristicsInformation {
  characteristics: Characteristic[];
  inactiveCharacteristics: Characteristic[];
  markers: Marker[];
  capturedItems: CapturedItem[][];
  selected: Characteristic[];
  count: null | number;
  mode: string;
  verified: null | number;
  index: number;
}

export type CharacteristicActionTypes =
  | 'CHARACTERISTICS_CLEANUP_STORE'
  | 'CHARACTERISTICS_LOADED'
  | 'CHARACTERISTICS_PARSED'
  | 'CHARACTERISTICS_ERROR'
  | 'CHARACTERISTICS_UPDATE_POLL_INTERVAL'
  | 'CHARACTERISTICS_UPDATE_CHARACTERISTICS'
  | 'CHARACTERISTICS_UPDATE_INACTIVE'
  | 'CHARACTERISTICS_UPDATE_SELECTED'
  | 'CHARACTERISTICS_UPDATE_MARKERS'
  | 'CHARACTERISTICS_UPDATE_CAPTURES'
  | 'CHARACTERISTICS_EDIT_CHARACTERISTICS'
  | 'CHARACTERISTICS_UPDATE_EXTRACT_MODE'
  | 'CHARACTERISTICS_VERIFY_CHARACTERISTICS'
  | 'CHARACTERISTICS_LOAD_CHARACTERISTICS'
  | 'CHARACTERISTICS_UPDATE_INDEX'
  | 'CHARACTERISTICS_UPDATE_LAST'
  | 'CHARACTERISTICS_UPDATE_PART'
  | 'CHARACTERISTICS_CREATE_CHARACTERISTICS'
  | 'CHARACTERISTICS_DELETE_CHARACTERISTICS'
  | 'CHARACTERISTICS_EDIT_MARKERS'
  | 'CHARACTERISTICS_SET_REVISION'
  | 'CHARACTERISTICS_SET_SINGLE_CUSTOM';

export interface IntervalPayload {
  previousChangeTime: Date;
  previousCount: number;
  previousHRCount: number;
}

export interface UpdateSelectedPayload {
  index?: number;
  lastLabel?: number;
  count?: number;
  selected?: Characteristic[];
  characteristics?: Characteristic[];
  annotations?: Annotation[];
  inactiveCharacteristics: Characteristic[];
  capturedItems: CapturedItem[][];
}
export interface Capture {
  id: number;
  rect: {
    rectangle: fabric.Rect;
    anchorCoord: CaptureDimensions;
    boxRotation: number;
    canvas: fabric.Canvas;
  };
  params: any; // TODO isolate shape
  addItem: boolean;
}

export interface CapturePayload {
  captureTally: number;
  captureArray: Capture[];
}

export interface UpdateRectPayload {
  captureArray: Capture[];
}

export interface UpdateDetailsPayload {
  characteristics: Characteristic[];
}

export interface MarkersPayload {
  markers: Marker[];
  updates: Characteristic[];
  assignedMarkers: { [key: string]: MarkerStyle };
}

export interface AfterSavedCharacteristicsPayload {
  selected: Characteristic[];
  index: number;
  count: number;
  capturedItems: CapturedItem[][];
  characteristics: Characteristic[];
}

export interface EditCharacteristicsPayload {
  characteristics: Characteristic[];
  updates: Characteristic[];
  newItems: CharacteristicInput[];
  verify?: boolean;
  newSelected?: Characteristic[];
}

export type CreateCharacteristicsPayload = CharacteristicInput[];

export type DetailsPayload = CharacteristicsInformation;

export type CharacteristicActionPayloads =
  | IntervalPayload
  | UpdateSelectedPayload
  | EditCharacteristicsPayload
  | AfterSavedCharacteristicsPayload
  | CreateCharacteristicsPayload
  | CapturePayload
  | DetailsPayload
  | UpdateDetailsPayload
  | UpdateRectPayload
  | MarkersPayload
  | string
  | boolean
  | undefined;

export interface Classified {
  Type: string;
  SubType: string;
  Gdt: {
    GdtSymbol: string | undefined;
    GdtTol: string | undefined;
    GdtTolZone2: string | undefined;
    GdtPrimaryDatum: string | undefined;
    GdtSecondDatum: string | undefined;
    GdtTertiaryDatum: string | undefined;
  } | null;
  Quantity: number;
  FullSpecification: string;
  Nominal: string;
  PlusTol: string;
  MinusTol: string;
  UpperSpecLimit: string;
  LowerSpecLimit: string;
  IsBasic: boolean;
  IsReference: boolean;
}

export interface PartialInput {
  drawingSheetIndex: number;
  fullSpecificationFromOCR: string;
  confidence: number;
  status: CharacteristicStatusEnum;
  notationType: string;
  notationSubtype: string;
  notationClass: CharacteristicNotationClassEnum;
  drawingRotation: number;
  drawingScale: number;
  boxLocationY: number;
  boxLocationX: number;
  boxWidth: number;
  boxHeight: number;
  boxRotation: number;
  markerGroup: string;
  markerGroupShared: boolean;
  markerIndex: number;
  markerSubIndex: number;
  markerLabel: string;
  markerStyle: string;
  markerLocationX: number;
  markerLocationY: number;
  markerFontSize: number;
  markerSize: number;
  markerRotation: number;
  gridCoordinates: string;
  balloonGridCoordinates: string;
  connectionPointGridCoordinates: string;
  part: string;
  drawing: string;
  captureMethod: CharacteristicCaptureEnum;
  captureError: string;
  drawingSheet: string;
  verified: boolean;
  toleranceSource: CharacteristicToleranceEnum;
  connectionPointLocationX: number;
  connectionPointLocationY: number;
  connectionPointIsFloating: boolean;
  displayLeaderLine: boolean;
  leaderLineDistance: number;
}

export interface ParsedObject {
  notation: {
    type: string;
    nominal: {
      text: string;
      value: string;
      units: string;
    };
    plus: {
      text: string;
      value: string;
      units: string;
    };
    minus: {
      text: string;
      value: string;
      units: string;
    };
    text: string;
  };
  quantity: {
    value: string;
    units: string;
    text: string;
  };
  words: string;
  text: string;
  input: string;
}

export type CharacteristicNotationTypeEnum = 'Dimension' | 'Geometric_Tolerance' | 'Surface_Finish' | 'Weld' | 'Note' | 'Other';

export enum CharacteristicStatus {
  Active = 'Active',
  Inactive = 'Inactive',
  Rejected = 'Rejected',
}

export type CharacteristicCriticalityEnum = '' | 'Incidental' | 'Minor' | 'Major' | 'Key' | 'Critical';
export type CharacteristicToleranceEnum = 'Default_Tolerance' | 'Document_Defined' | 'Manually_Set' | 'N_A';
export type CharacteristicStatusEnum = 'Active' | 'Inactive' | 'Rejected';
export type CharacteristicCaptureEnum = 'Automated' | 'Manual' | 'Custom' | 'Annotation';
export type CharacteristicNotationSubtypeEnum =
  | 'Length'
  | 'Angle'
  | 'Radius'
  | 'Diameter'
  | 'Curvilinear'
  | 'Chamfer_Size'
  | 'Chamfer_Angle'
  | 'Bend_Radius'
  | 'Counterbore_Depth'
  | 'Counterbore_Diameter'
  | 'Counterbore_Angle'
  | 'Countersink_Diameter'
  | 'Countersink_Angle'
  | 'Depth'
  | 'Edge_Radius'
  | 'Thickness'
  | 'Thread'
  | 'Edge_Burr_Size'
  | 'Edge_Undercut_Size'
  | 'Edge_Passing_Size'
  | 'Profile_of_a_Surface'
  | 'Position'
  | 'Flatness'
  | 'Perpendicularity'
  | 'Angularity'
  | 'Circularity'
  | 'Runout'
  | 'Total_Runout'
  | 'Profile_of_a_Line'
  | 'Cylindricity'
  | 'Concentricity'
  | 'Symmetry'
  | 'Parallelism'
  | 'Straightness'
  | 'Roughness'
  | 'Lay'
  | 'Waviness'
  | 'Structure'
  | 'Material_Removal_Allowance'
  | 'Size'
  | 'Convexity'
  | 'Reinforcement'
  | 'Additional_Characteristic'
  | 'Bead_Contour'
  | 'Depth_of_Bevel'
  | 'Penetration'
  | 'Location'
  | 'Pitch'
  | 'Root_Opening'
  | 'Note'
  | 'Flag_Note'
  | 'Temperature'
  | 'Time'
  | 'Torque'
  | 'Voltage'
  | 'Temper_Conductivity'
  | 'Temper_Verification'
  | 'Sound'
  | 'Frequency'
  | 'Capacitance'
  | 'Electric_Current'
  | 'Resistance'
  | 'Electric_Charge'
  | 'Inductance'
  | 'Apparent_Power'
  | 'Reactive_Power'
  | 'Electric_Power'
  | 'Conductance'
  | 'Admittance'
  | 'Electric_Flux'
  | 'Magnetic_Field'
  | 'Magnetic_Flux'
  | 'Area'
  | 'Resistivity'
  | 'Conductivity'
  | 'Electric_Field'
  | 'Datum_Target'
  | 'Production_Method'
  | 'Annotation_Table'
  | 'Table'
  | 'Cell'
  | 'Callout'
  | 'Datum'
  | 'Balloon';
export type CharacteristicNotationClassEnum = 'Tolerance' | 'Basic' | 'Reference' | 'N_A';

export interface Characteristic {
  id: string;
  previousRevision: string | null;
  nextRevision: string | null;
  originalCharacteristic: string | null;
  drawingSheetIndex: number;
  status: CharacteristicStatusEnum;
  captureMethod: CharacteristicCaptureEnum;
  captureError: string;
  notationType: string;
  notationSubtype: string;
  notationClass: CharacteristicNotationClassEnum;
  fullSpecification: string;
  quantity: number;
  nominal: string;
  upperSpecLimit: string;
  lowerSpecLimit: string;
  plusTol: string;
  minusTol: string;
  unit: string;
  gdtSymbol?: string;
  gdtPrimaryToleranceZone?: string;
  gdtSecondaryToleranceZone?: string;
  gdtPrimaryDatum?: string;
  gdtSecondaryDatum?: string;
  gdtTertiaryDatum?: string;
  criticality: string | null;
  inspectionMethod: string | null;
  notes: string;
  drawingRotation: number;
  drawingScale: number;
  boxLocationY: number;
  boxLocationX: number;
  boxWidth: number;
  boxHeight: number;
  boxRotation: number;
  markerGroup: string;
  markerGroupShared: boolean;
  markerIndex: number;
  markerSubIndex: number;
  markerLabel: string;
  markerStyle: string;
  markerLocationX: number;
  markerLocationY: number;
  markerFontSize: number;
  markerSize: number;
  markerRotation: number;
  gridCoordinates: string;
  balloonGridCoordinates: string;
  connectionPointGridCoordinates: string;
  part: Part;
  drawing: Drawing;
  drawingSheet: DrawingSheet;
  verified: boolean;
  fullSpecificationFromOCR: string;
  confidence: number;
  operation: string | null;
  toleranceSource: CharacteristicToleranceEnum;
  reviewerTaskId: string | null;
  createdAt: Date;
  updatedAt: Date;
  deletedAt: Date;
  connectionPointLocationX: number;
  connectionPointLocationY: number;
  connectionPointIsFloating: boolean;
  displayLeaderLine: boolean;
  leaderLineDistance: number;
  [key: string]:
    | string
    | number
    | boolean
    | CharacteristicNotationTypeEnum
    | CharacteristicNotationSubtypeEnum
    | CharacteristicNotationClassEnum
    | CharacteristicCriticalityEnum
    | CharacteristicToleranceEnum
    | CharacteristicStatusEnum
    | CharacteristicCaptureEnum
    | Date
    | Part
    | Drawing
    | DrawingSheet
    | undefined
    | null;
}
export interface CharacteristicInput {
  drawingSheetIndex: number;
  previousRevision?: string | null;
  nextRevision?: string | null;
  originalCharacteristic?: string | null;
  status: CharacteristicStatusEnum;
  captureMethod: CharacteristicCaptureEnum;
  captureError: string;
  notationType: string;
  notationSubtype: string;
  notationClass: CharacteristicNotationClassEnum;
  fullSpecification: string;
  quantity: number;
  nominal: string;
  upperSpecLimit: string;
  lowerSpecLimit: string;
  plusTol: string;
  minusTol: string;
  unit: string;
  gdtSymbol?: string;
  gdtPrimaryToleranceZone?: string;
  gdtSecondaryToleranceZone?: string;
  gdtPrimaryDatum?: string;
  gdtSecondaryDatum?: string;
  gdtTertiaryDatum?: string;
  criticality?: string | null;
  inspectionMethod?: string | null;
  operation?: string | null;
  notes: string;
  drawingRotation: number;
  drawingScale: number;
  boxLocationY: number;
  boxLocationX: number;
  boxWidth: number;
  boxHeight: number;
  boxRotation: number;
  markerGroup: string;
  markerGroupShared: boolean;
  markerIndex: number;
  markerSubIndex: number;
  markerLabel: string;
  markerStyle: string;
  markerLocationX: number;
  markerLocationY: number;
  markerFontSize: number;
  markerSize: number;
  markerRotation: number;
  gridCoordinates: string;
  balloonGridCoordinates: string;
  connectionPointGridCoordinates: string;
  part: string;
  drawing: string;
  drawingSheet: string;
  verified: boolean;
  fullSpecificationFromOCR: string;
  confidence: number;
  toleranceSource: CharacteristicToleranceEnum;
  connectionPointLocationX: number;
  connectionPointLocationY: number;
  connectionPointIsFloating: boolean;
  displayLeaderLine: boolean;
  leaderLineDistance: number;
  reviewTaskId?: string;
  [key: string]:
    | string
    | number
    | boolean
    | CharacteristicNotationTypeEnum
    | CharacteristicNotationSubtypeEnum
    | CharacteristicNotationClassEnum
    | CharacteristicCriticalityEnum
    | CharacteristicToleranceEnum
    | CharacteristicStatusEnum
    | CharacteristicCaptureEnum
    | Date
    | Part
    | Drawing
    | DrawingSheet
    | undefined
    | null;
}

// Data for token replacement when creating a report
export interface CharacteristicData {
  label: string;
  location: string;
  zone: string;
  balloon_zone: string;
  connection_point_zone: string;
  page: number;
  type: string;
  subtype: string;
  quantity: number;
  unit: string;
  full_spec: string;
  tol_type: CharacteristicNotationClassEnum;
  nominal: string;
  plus: string;
  minus: string;
  usl: string;
  lsl: string;
  source: CharacteristicToleranceEnum;
  classification: string | null;
  operation: string | null;
  method: string | null;
  comment: string;
  verification: boolean;
  number: number;
  status: CharacteristicStatusEnum;
  capturemethod: CharacteristicCaptureEnum;
  captureError: string;
  gdtsymbol: string;
  gdtprimarytolerancezone: string;
  gdtsecondarytolerancezone: string;
  gdtprimarydatum: string;
  gdtsecondarydatum: string;
  gdttertiarydatum: string;
  markergroup: string;
  markergroupshared: boolean;
  markersubindex: number;
  markerstyle: string;
  markerfontsize: number;
  markerSize: number;
  ocr: string;
  confidence: number;
}
export interface CharacteristicEditVars {
  id: string;
  data: CharacteristicInput;
}

export interface CharacteristicCreateVars {
  data: CharacteristicInput;
}

export interface CharacteristicEdit {
  characteristicUpdate: Characteristic;
}
export interface CharacteristicCreate {
  characteristicCreate: Characteristic;
}

export interface CharacteristicFind {
  characteristicFind: Characteristic;
}

export interface CharacteristicDelete {
  characteristicDestroy: boolean;
}
export interface CharacteristicDeleteVars {
  ids: string[];
}

export type CharacteristicOrderByEnum =
  | 'id_ASC'
  | 'id_DESC'
  | 'drawingSheetIndex_ASC'
  | 'drawingSheetIndex_DESC'
  | 'notationType_ASC'
  | 'notationType_DESC'
  | 'notationSubtype_ASC'
  | 'notationSubtype_DESC'
  | 'notationClass_ASC'
  | 'notationClass_DESC'
  | 'fullSpecification_ASC'
  | 'fullSpecification_DESC'
  | 'quantity_ASC'
  | 'quantity_DESC'
  | 'nominal_ASC'
  | 'nominal_DESC'
  | 'upperSpecLimit_ASC'
  | 'upperSpecLimit_DESC'
  | 'lowerSpecLimit_ASC'
  | 'lowerSpecLimit_DESC'
  | 'plusTol_ASC'
  | 'plusTol_DESC'
  | 'minusTol_ASC'
  | 'minusTol_DESC'
  | 'unit_ASC'
  | 'unit_DESC'
  | 'gdtSymbol_ASC'
  | 'gdtSymbol_DESC'
  | 'gdtPrimaryToleranceZone_ASC'
  | 'gdtPrimaryToleranceZone_DESC'
  | 'gdtSecondaryToleranceZone_ASC'
  | 'gdtSecondaryToleranceZone_DESC'
  | 'gdtPrimaryDatum_ASC'
  | 'gdtPrimaryDatum_DESC'
  | 'gdtSecondaryDatum_ASC'
  | 'gdtSecondaryDatum_DESC'
  | 'gdtTertiaryDatum_ASC'
  | 'gdtTertiaryDatum_DESC'
  | 'criticality_ASC'
  | 'criticality_DESC'
  | 'inspectionMethod_ASC'
  | 'inspectionMethod_DESC'
  | 'notes_ASC'
  | 'notes_DESC'
  | 'drawingRotation_ASC'
  | 'drawingRotation_DESC'
  | 'drawingScale_ASC'
  | 'drawingScale_DESC'
  | 'boxLocationY_ASC'
  | 'boxLocationY_DESC'
  | 'boxLocationX_ASC'
  | 'boxLocationX_DESC'
  | 'boxWidth_ASC'
  | 'boxWidth_DESC'
  | 'boxHeight_ASC'
  | 'boxHeight_DESC'
  | 'boxRotation_ASC'
  | 'boxRotation_DESC'
  | 'markerGroup_ASC'
  | 'markerGroup_DESC'
  | 'markerIndex_ASC'
  | 'markerIndex_DESC'
  | 'markerSubIndex_ASC'
  | 'markerSubIndex_DESC'
  | 'markerLabel_ASC'
  | 'markerLabel_DESC'
  | 'markerStyle_ASC'
  | 'markerStyle_DESC'
  | 'markerLocationX_ASC'
  | 'markerLocationX_DESC'
  | 'markerLocationY_ASC'
  | 'markerLocationY_DESC'
  | 'gridCoordinates_ASC'
  | 'gridCoordinates_DESC'
  | 'balloonGridCoordinates_ASC'
  | 'balloonGridCoordinates_DESC'
  | 'connectionPointGridCoordinates_ASC'
  | 'connectionPointGridCoordinates_DESC'
  | 'createdAt_ASC'
  | 'createdAt_DESC'
  | 'deletedAt_ASC'
  | 'deletedAt_DESC';

export interface CharacteristicFilterInput {
  id?: string;
  drawingSheetIndexRange?: number[];
  status?: CharacteristicStatusEnum[];
  notationType?: string;
  notationSubtype?: string;
  notationClass?: CharacteristicNotationClassEnum;
  fullSpecification?: string;
  quantityRange?: number[];
  nominal?: string;
  upperSpecLimit?: string;
  lowerSpecLimit?: string;
  plusTol?: string;
  minusTol?: string;
  unit?: string;
  gdtSymbol?: string;
  gdtPrimaryToleranceZone?: string;
  gdtSecondaryToleranceZone?: string;
  gdtPrimaryDatum?: string;
  gdtSecondaryDatum?: string;
  gdtTertiaryDatum?: string;
  inspectionMethod?: string;
  operation?: string;
  notes?: string;
  drawingRotationRange?: number[];
  drawingScaleRange?: number[];
  boxLocationYRange?: number[];
  boxLocationXRange?: number[];
  boxWidthRange?: number[];
  boxHeightRange?: number[];
  boxRotationRange?: number[];
  markerGroup?: string;
  markerGroupShared?: boolean;
  markerIndexRange?: number[];
  markerSubIndexRange?: number[];
  markerLabel?: string;
  markerStyle?: string;
  markerLocationXRange?: number[];
  markerLocationYRange?: number[];
  markerFontSize?: number[];
  markerSize?: number[];
  gridCoordinates?: string;
  balloonGridCoordinates?: string;
  connectionPointGridCoordinates?: string;
  part?: string;
  drawing?: string;
  drawingSheet?: string;
  originalCharacteristic?: string;
  createdAtRange?: Date[];
  deletedAtRange?: Date[];
}

export interface CharacteristicList {
  characteristicList?: {
    count: number;
    rows: Characteristic[];
  };
}

export interface CharacteristicListVars {
  filter?: CharacteristicFilterInput;
  orderBy?: CharacteristicOrderByEnum;
  limit?: number;
  offset?: number;
}
