export interface PrecisionTolerance {
  level: string;
  plus: string;
  minus: string;
  [key: string]: string;
}

export interface RangeTolerance {
  rangeHigh: string;
  rangeLow: string;
  plus: string;
  minus: string;
  [key: string]: string;
}

export interface TolSection {
  type: 'PrecisionTolerance' | 'RangeTolerance';
  rangeTols: RangeTolerance[];
  precisionTols: PrecisionTolerance[];
  unit: string;
  [key: string]: string | PrecisionTolerance[] | RangeTolerance[];
}

export interface Tolerance {
  linear: TolSection;
  angular: TolSection;
  [key: string]: TolSection;
}
