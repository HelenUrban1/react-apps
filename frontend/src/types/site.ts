export type SiteStatusEnum = 'Active' | 'Past_Due' | 'Suspended' | 'Archived';

export type SiteRegionEnum =
  | 'Alabama'
  | 'Alaska'
  | 'American_Samoa'
  | 'Arizona'
  | 'Arkansas'
  | 'California'
  | 'Colorado'
  | 'Connecticut'
  | 'Delaware'
  | 'District_of_Columbia'
  | 'Florida'
  | 'Georgia'
  | 'Guam'
  | 'Hawaii'
  | 'Idaho'
  | 'Illinois'
  | 'Indiana'
  | 'Iowa'
  | 'Kansas'
  | 'Kentucky'
  | 'Louisiana'
  | 'Maine'
  | 'Maryland'
  | 'Massachusetts'
  | 'Michigan'
  | 'Minnesota'
  | 'Minor_Outlying_Islands'
  | 'Mississippi'
  | 'Missouri'
  | 'Montana'
  | 'Nebraska'
  | 'Nevada'
  | 'New_Hampshire'
  | 'New_Jersey'
  | 'New_Mexico'
  | 'New_York'
  | 'North_Carolina'
  | 'North_Dakota'
  | 'Northern_Mariana_Islands'
  | 'Ohio'
  | 'Oklahoma'
  | 'Oregon'
  | 'Pennsylvania'
  | 'Puerto_Rico'
  | 'Rhode_Island'
  | 'South_Carolina'
  | 'South_Dakota'
  | 'Tennessee'
  | 'Texas'
  | 'US_Virgin_Islands'
  | 'Utah'
  | 'Vermont'
  | 'Virginia'
  | 'Washington'
  | 'West_Virginia'
  | 'Wisconsin'
  | 'Wyoming';

export type SiteCountryEnum = 'United_States';

export interface Site {
  id: string;
  status?: SiteStatusEnum;
  settings?: string;
  siteName?: string;
  sitePhone?: string;
  siteEmail?: string;
  siteStreet?: string;
  siteStreet2?: string;
  siteCity?: string;
  siteRegion?: SiteRegionEnum;
  sitePostalcode?: string;
  siteCountry?: SiteCountryEnum;
  createdAt?: Date;
  updatedAt?: Date;
}
