import { Account } from './account';
import { File } from './file';

export type UserRolesEnum = 'Admin' | 'Billing' | 'Collaborator' | 'Owner' | 'Viewer';

export type UserNonOwnerRolesEnum = 'Admin' | 'Billing' | 'Collaborator' | 'Viewer';

export type UserStatusEnum = 'Active' | 'Suspended' | 'Archived' | 'Pending' | 'Requesting';

export type UserOrderByEnum = 'id_ASC' | 'id_DESC' | 'settings_ASC' | 'settings_DESC' | 'roles_ASC' | 'roles_DESC' | 'status_ASC' | 'status_DESC' | 'createdAt_ASC' | 'createdAt_DESC';

export interface UserFilterInput {
  id?: string;
  account?: string;
  site?: string;
  user?: string;
  settings?: string;
  roles?: UserRolesEnum[];
  status?: UserStatusEnum;
  emailVerified?: boolean;
  createdAtRange?: Date[];
}

export interface UserInput {
  account: string;
  user: string;
  settings: string;
  roles: UserRolesEnum[];
  status: UserStatusEnum;
}

export interface UserPage {
  rows: User[];
  count: number;
}

export interface User {
  id: string;
  activeAccountId?: string;
  activeAccountMemberId?: string;
  accountMemberships: { id: string; accountId?: string; siteId?: string; accessControl: boolean; status: UserStatusEnum }[];
  accountSettings?: string;
  roles?: string[];
  paid?: boolean;
  accessControl?: boolean;
  status?: UserStatusEnum;
  fullName?: string;
  firstName?: string;
  lastName?: string;
  phoneNumber?: string;
  email: string;
  avatars: File[];
  authenticationUid?: string;
  emailVerified?: boolean;
  emailVerificationToken?: string;
  emailVerificationTokenExpiresAt?: Date;
  disabled?: boolean;
  mfaRegistered?: boolean;
  isReviewAccount?: boolean;
  account: Account;
  createdAt?: Date;
  updatedAt?: Date;
}

export interface CurrentUser {
  accountId: string;
  accountName?: string;
  accountSettings?: string;
  activeAccountMemberId: string;
  authenticationUid: string;
  avatars?: null;
  email: string;
  emailVerified?: true;
  firstName?: string;
  fullName?: string;
  id: string;
  isReviewAccount?: false;
  isServiceAccount?: true;
  lastName?: string;
  paid?: true;
  phoneNumber?: string;
  siteId?: string;
  siteName?: string;
}
