export type ModalState = {
  title: string;
  visibility: boolean;
  content: any[];
  footer?: any;
};
