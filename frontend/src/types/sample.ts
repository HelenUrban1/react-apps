import { Job } from 'domain/jobs/jobTypes';
import { Part } from 'domain/part/partTypes';
import { Measurement } from './measurement';

export type SampleStatusEnum = 'Unmeasured' | 'Pass' | 'Fail' | 'Scrap';

export type Sample = {
  id: string;
  serial: string;
  sampleIndex: number;
  status: SampleStatusEnum;
  featureCoverage: string | null;
  partId: string;
  part: Part;
  job: Job;
  measurements: Measurement[];
  updatingOperatorId: string | null;
  createdAt: string | null;
  updatedAt: string | null;
  deletedAt: string | null;
};

export type SampleUpdateInput = {
  serial: string;
  sampleIndex: number;
  status: SampleStatusEnum;
  featureCoverage: string | null | undefined;
};
