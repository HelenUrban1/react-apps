import React from 'react';
import { useSelector } from 'react-redux';
import { useMutation, useQuery } from '@apollo/client';
import ContentWrapper from 'view/layout/styles/ContentWrapper';
import PageTitle from 'view/shared/styles/PageTitle';
import Errors from 'modules/shared/error/errors';
import SphinxApi from 'modules/sphinx/sphinxApi';
import Spinner from 'view/shared/Spinner';
import authSelectors from 'modules/auth/authSelectors';
import { getApiKeyFromConnections } from 'modules/sphinx/connectionModel';
import { Upload, message } from 'antd';
import { InboxOutlined } from '@ant-design/icons';

// This is a temporary page for uploading XML files to send through SPHINX
// to Net-Inspect that allows testing the export flow before the export functionality
// is built into IXC. This page is not intended for use by external users.
export default function TemporaryDataExportTester() {
  const { Dragger } = Upload;

  const user = useSelector((state) => authSelectors.selectCurrentUser(state));
  const { accountId, siteId } = user;
  const userId = user.id;
  const findInput = { accountId, siteId, userId };

  const { loading, data } = useQuery(SphinxApi.query.connectionFind, {
    variables: { data: findInput },
  });

  const connections = data?.connectionFind || [];
  const apiKey = getApiKeyFromConnections(connections);

  const [uploadFile] = useMutation(SphinxApi.mutate.upload, {
    onCompleted: (data) => {
      console.log('Net-Inspect upload complete');
      console.log(JSON.stringify(data, null, 2));
    },
    onError: (error) => {
      Errors.handle(error);
    },
  });

  const makeRequest = async (event) => {
    let payload;
    try {
      payload = await event.file.text();
    } catch (error) {
      Errors.handle(error);
      return;
    }

    uploadFile({
      variables: {
        data: {
          apiKey,
          payload,
        },
      },
    });
  };

  const draggerProps = {
    name: 'file',
    multiple: false,
    customRequest: (e) => makeRequest(e),
    onChange(info) {
      console.log(info);
      const { status } = info.file;
      if (status !== 'uploading') {
        console.log(info.file, info.fileList);
      }
      if (status === 'done') {
        message.success(`${info.file.name} file exported successfully.`);
      } else if (status === 'error') {
        message.error(`${info.file.name} file export failed.`);
      }
    },
    onDrop(e) {
      console.log('Dropped files', e.dataTransfer.files);
    },
  };

  if (loading) {
    return <Spinner />;
  }

  return (
    <ContentWrapper>
      <PageTitle>Net-Inspect Export</PageTitle>

      <Dragger {...draggerProps}>
        <p className="ant-upload-drag-icon">
          <InboxOutlined />
        </p>
        <p className="ant-upload-text">Click or drag files to this area to export</p>
      </Dragger>
    </ContentWrapper>
  );
}
