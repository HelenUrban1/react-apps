// For performance testing
import './wdyr';

import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import SettingsService from './modules/setting/settingService';
import { setPrompt } from './metro/install/serviceWorker';
import BeforeInstallPromptEvent from './metro/install/BeforeInstallPromptEvent';
import { i18n } from './i18n';

(async function () {
  document.title = i18n('app.title');
  await SettingsService.fetchAndApply();
  ReactDOM.render(<App />, document.getElementById('root'));
})();

window.addEventListener('beforeinstallprompt', (e) => {
  e.preventDefault();
  window.localStorage.setItem('installed', 'false');

  setPrompt(e as BeforeInstallPromptEvent);
});
