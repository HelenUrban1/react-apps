import { createSelector } from 'reselect';
import { PartEditorState } from '../state.d';

const selectRaw = (state: { partEditor: PartEditorState }) => state.partEditor;
