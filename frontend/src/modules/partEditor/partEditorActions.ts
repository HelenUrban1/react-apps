import { ApolloClient, NormalizedCacheObject } from '@apollo/client';
import { rebuildToleranceSettingObject } from 'domain/part/edit/defaultTolerances/TolerancesLogic';
import { Grid } from 'domain/part/edit/grid/Grid';
import { PartDrawing, PartDrawingSheet } from 'domain/part/partApi';
import { Part } from 'domain/part/partTypes';
import { Drawing, DrawingChange, DrawingInput, DrawingsAPI } from 'graphql/drawing';
import { DrawingSheet, DrawingSheetChange, DrawingSheetInput, DrawingSheets } from 'graphql/drawing_sheet';
import { Tolerance } from 'types/tolerances';
import { NavigationActions, PartStepEnum } from 'modules/navigation/navigationActions';
import { doGetPartRevisions, PartActions } from 'modules/part/partActions';
import { createDrawingSheetMutation, findPartQuery, populateFullPart } from 'modules/part/partApi';
import { PdfViewActions } from 'modules/pdfView/pdfViewActions';
import Analytics from 'modules/shared/analytics/analytics';
import log from 'modules/shared/logger';
import { AppState, PartEditorState } from 'modules/state';
import { Dispatch } from 'redux';
import { DefaultStyles, defaultUnitIDs } from 'view/global/defaults';
import { cloneDeep } from 'lodash';
import { CharacteristicActions, getCapturedItems, loadCharacteristicsThunk } from 'modules/characteristic/characteristicActions';
import { CapturedItem, Characteristic } from 'types/characteristics';
import { updateItemCoordinates } from 'domain/part/edit/characteristics/utils/extractUtil';
import { checkCurrentRevision } from 'utils/PartUtilities';
import { PdfOverlayViewActions, resetPdfOverlayView } from 'modules/pdfOverlayView/pdfViewActions';
import { callDocLoad, loadPdfCompareOverlay } from 'styleguide/PdfViewer/PdfViewerUtil';

const prefix = 'PART_EDITOR';

export const PartEditorActions = {
  SET_PART_EDITOR_STATE: `${prefix}_SET_STATE`,
  SET_PART_EDITOR_LOADED: `${prefix}_SET_LOADED`,
  SET_PART_EDITOR_ERROR: `${prefix}_SET_ERROR`,
  SET_PART_EDITOR_ASSIGNED_STYLES: `${prefix}_SET_ASSIGNED_STYLES`,
  SET_PART_EDITOR_PRESETS: `${prefix}_SET_PRESETS`,
  SET_PART_EDITOR_DRAWING: `${prefix}_SET_DRAWING`,
  SET_PART_EDITOR_TOLERANCES: `${prefix}_SET_TOLERANCES`,
  SET_PART_EDITOR_DRAWING_SHEET: `${prefix}_SET_SHEET`,
  SET_PART_EDITOR_GRID_CACHE: `${prefix}_SET_GRID_CACHE`,
  SET_PART_EDITOR_FOCUS: `${prefix}_SET_FOCUS`,
  SET_PART_EDITOR_CUSTOM: `${prefix}_SET_CUSTOM`,
  RESET_PART_EDITOR: `${prefix}_RESET_STATE`,
};

export enum PartEditorActionTypes {
  SET_PART_EDITOR_STATE = 'PART_EDITOR_SET_STATE',
  SET_PART_EDITOR_LOADED = 'PART_EDITOR_SET_LOADED',
  SET_PART_EDITOR_ERROR = 'PART_EDITOR_SET_ERROR',
  SET_PART_EDITOR_ASSIGNED_STYLES = 'PART_EDITOR_SET_ASSIGNED_STYLES',
  SET_PART_EDITOR_PRESETS = 'PART_EDITOR_SET_PRESETS',
  SET_PART_EDITOR_TOLERANCES = 'PART_EDITOR_SET_TOLERANCES',
  SET_PART_EDITOR_DRAWING = 'PART_EDITOR_SET_DRAWING',
  SET_PART_EDITOR_SHEET = 'PART_EDITOR_SET_SHEET',
  SET_PART_EDITOR_GRID_CACHE = 'PART_EDITOR_SET_GRID_CACHE',
  SET_PART_EDITOR_FOCUS = 'PART_EDITOR_SET_FOCUS',
  SET_PART_EDITOR_CUSTOM = 'PART_EDITOR_SET_CUSTOM',
  RESET_PART_EDITOR = 'PART_EDITOR_RESET_STATE',
}

export const findPartDrawingQuery = async (queryObject: { id: string }, client: ApolloClient<NormalizedCacheObject>) => {
  return client.query({
    query: PartDrawing.query.find,
    variables: queryObject,
  });
};

export const updatePartDrawingMutation = async (queryObject: { id: string; data: DrawingInput }, client: ApolloClient<NormalizedCacheObject>) => {
  return client.mutate({
    mutation: PartDrawing.mutate.edit,
    variables: queryObject,
    refetchQueries: [
      {
        query: PartDrawing.query.find,
        variables: {
          id: queryObject.id,
        },
      },
    ],
  });
};

export const updateDrawingSheetMutation = async (queryObject: { id: string; data: DrawingSheetInput }, client: ApolloClient<NormalizedCacheObject>) => {
  return client.mutate({
    mutation: PartDrawingSheet.mutate.edit,
    variables: queryObject,
  });
};

export const getGrid = (partDrawing: Drawing, partEditor: PartEditorState, sheetIndex = 0) => {
  const gridCache = [];
  const sortedSheets = partDrawing.sheets ? [...partDrawing.sheets].sort((a, b) => (a.pageIndex < b.pageIndex ? -1 : 1)) : [];

  // load the grids for each sheet into the grid cache
  for (let j = 0; j < sortedSheets?.length || 0; j++) {
    const loadedGrid = sortedSheets[j].grid;
    if (loadedGrid && loadedGrid !== '' && loadedGrid !== 'none') {
      const g: Grid = Grid.fromJsonString(loadedGrid);
      gridCache.push(g);
    }
  }
  // check that the drawing had a grid (it won't if the Part hasn't reached the grid step yet) and set the partEditor gridRenderer
  const initGrid = gridCache[sheetIndex];
  const newGridRenderer = partEditor.gridRenderer;
  if (initGrid && initGrid.toJsonString() !== '') newGridRenderer.grid = initGrid;

  return { gridCache, newGridRenderer };
};

export const initializePartEditorThunk = (partId: string) => async (dispatch: Dispatch<any>, getState: () => AppState, clientPromise: Promise<ApolloClient<NormalizedCacheObject>>) => {
  return clientPromise.then(async (client) => {
    dispatch({ type: PartEditorActionTypes.SET_PART_EDITOR_LOADED, payload: false });
    try {
      const state = getState();
      const { partFind, drawingList, reportList } = (await findPartQuery({ id: partId }, client)).data;
      if (!partFind) {
        throw new Error('Unable to set null part.');
      }
      let part: Part = { ...partFind };
      if (part && drawingList.rows && reportList.rows) {
        part = await populateFullPart(part, drawingList.rows, reportList.rows, client);
        dispatch({ type: PartActions.SET_PART_STATE, payload: { part, originalPart: part, previousPart: state.part.part, partLoaded: true } });
      }

      let assignedStyles;
      let presets: { [key: string]: string } = {};
      if (part && part.defaultMarkerOptions) {
        assignedStyles = JSON.parse(part.defaultMarkerOptions);
      }
      if (part && part.presets) {
        presets = JSON.parse(part.presets);
      }
      if (!assignedStyles || assignedStyles === null) {
        assignedStyles = state.session.settings.stylePresets?.metadata.Default?.setting || DefaultStyles;
      }

      let drawing = { ...(part.drawings?.find((draw) => draw.id === state.partEditor.targetDrawing) || part.primaryDrawing) };
      const { gridCache, newGridRenderer } = getGrid(drawing, state.partEditor);

      const lastStep = state.session.settings.autoballoonEnabled?.value === 'true' ? PartStepEnum.REVIEW : PartStepEnum.EXTRACT;
      const setStep = part.workflowStage > lastStep ? lastStep : part.workflowStage;

      dispatch({ type: NavigationActions.SET_MAX_PART_STEP, payload: setStep });
      if (state.navigation.currentPartStep < 0) {
        // If step was clicked in the side nav, it will already be set and shouldn't be overwritten
        const num = state.partEditor.focus?.split(' ')[0];
        const step = num ? Math.min(parseInt(num, 10), part.workflowStage, setStep) : Math.min(setStep, part.workflowStage);
        dispatch({ type: NavigationActions.SET_CURRENT_PART_STEP, payload: state.partEditor.targetDrawing && !state.pdfOverlayView.revisionFiles ? 1 : step });
      }

      let drawingSheet;

      if (!drawing) {
        throw new Error('Missing Primary Drawing');
      }

      // on load the next unverified feature is selected, make sure the right drawing and sheet are loaded
      if (!state.partEditor.targetDrawing && part.characteristics && part.characteristics.length > 0 && !state.partEditor.focus) {
        const sorted = part.characteristics.filter((char) => char.status === 'Active' && char.nextRevision === null).sort((a, b) => (a.markerIndex < b.markerIndex ? -1 : 1));
        for (let i = 0; i < sorted.length; i++) {
          if (!sorted[i].verified) {
            if (sorted[i].drawing.id !== drawing.id) {
              drawing = part.drawings?.find((draw) => draw.id === sorted[i].drawing.id) || { ...part.primaryDrawing };
            }
            drawingSheet = drawing.sheets.find((sheet) => sheet.id === sorted[i].drawingSheet.id);
            break;
          } else if (i === sorted.length - 1) {
            drawing = part.drawings?.find((draw) => draw.id === sorted[i].drawing.id) || { ...part.primaryDrawing };
            drawingSheet = drawing.sheets.find((sheet) => sheet.id === sorted[i].drawingSheet.id);
          }
        }
      }

      const sortedSheets = drawing.sheets ? [...drawing.sheets].sort((a: DrawingSheet, b: DrawingSheet) => a.pageIndex - b.pageIndex) : [];
      const tolpreset = presets && presets.tolerances ? presets.tolerances : 'Default';
      const settingsDefaultTolerance =
        state.session.settings.tolerancePresets?.metadata && state.session.settings.tolerancePresets.metadata[tolpreset] && state.session.settings.tolerancePresets.metadata[tolpreset]?.setting
          ? (state.session.settings.tolerancePresets.metadata[tolpreset]?.setting as Tolerance)
          : undefined;
      const tolerances = rebuildToleranceSettingObject({ currentPreset: 'Default', linear: sortedSheets[0].defaultLinearTolerances, angular: sortedSheets[0].defaultAngularTolerances, settings: settingsDefaultTolerance });

      if (tolerances.linear.unit === defaultUnitIDs.inch && part.measurement === 'Metric') {
        // Fix default unit, since linear defaults to Imperial measurement
        tolerances.linear.unit = defaultUnitIDs.millimeter;
      }
      dispatch(doGetPartRevisions(part.originalPart));
      dispatch({
        type: PartEditorActions.SET_PART_EDITOR_STATE,
        payload: {
          ...state.partEditor,
          newPart: null,
          partDrawing: { ...drawing, sheets: sortedSheets },
          partDrawingSheet: drawingSheet || sortedSheets[0],
          assignedStyles,
          presets,
          gridCache,
          gridRenderer: newGridRenderer,
          tolerances: tolerances || state.partEditor.tolerances,
          partEditorLoaded: true,
        },
      });
    } catch (err) {
      log.error(err);
      Analytics.track({
        event: Analytics.events.partEditorInitError,
        properties: {
          'part.id': partId,
        },
      });
      dispatch({ type: PartEditorActions.SET_PART_EDITOR_ERROR, payload: err });
    }
  });
};
// single sheet
export const updateDrawingSheetThunk = (sheetIndex: number, partDrawingSheetChanges: DrawingSheetChange) => async (dispatch: Dispatch, getState: () => AppState, clientPromise: Promise<ApolloClient<NormalizedCacheObject>>) => {
  return clientPromise.then(async (client) => {
    const state = getState();
    try {
      if (!state.partEditor.partDrawing || !state.part.part || !state.partEditor.partDrawing.sheets) {
        throw new Error('Unable to update part drawing sheet, because part drawing is null.');
      }
      let partDrawing = cloneDeep(state.partEditor.partDrawing);
      if (!state.partEditor.partDrawing.sheets[sheetIndex]) {
        log.warn('Sheet index did not exist on drawing. Attempting to create sheet.');
        let copiedSheet: DrawingSheet | null = null;
        if (state.partEditor.partDrawing.sheets[0]) {
          [copiedSheet] = state.partEditor.partDrawing.sheets;
        }

        if (!copiedSheet) {
          throw new Error('Unable to update part drawing sheet, because part drawing is missing sheets');
        }
        // Duplicate missing sheets
        const createDrawingSheetTasks: any[] = [];
        const newSheetList: DrawingSheet[] = [];
        for (let index = 0; index <= sheetIndex; index++) {
          if (copiedSheet.pageIndex !== index) {
            log.debug(`Creating cloned sheet for index ${index}`);
            const newSheet: DrawingSheet = {
              ...copiedSheet,
              pageIndex: index,
              pageName: `Page ${index + 1}`,
            };
            newSheetList.push(newSheet);
            const sheetInput = DrawingSheets.createGraphqlInput(newSheet);
            createDrawingSheetTasks.push(
              createDrawingSheetMutation(
                {
                  data: sheetInput,
                },
                client,
              ),
            );
          }
        }
        const newSheets = await Promise.all(createDrawingSheetTasks);
        partDrawing.sheets = [
          ...partDrawing.sheets,
          ...newSheets
            .filter((s) => s.data?.drawingSheetCreate)
            .map((s, i) => {
              return { ...newSheetList[i], ...s.data!.drawingSheetCreate };
            }),
        ].sort((a, b) => a.pageIndex - b.pageIndex);
      }
      dispatch({ type: PartEditorActionTypes.SET_PART_EDITOR_LOADED, payload: false });

      partDrawing.sheets = [...partDrawing.sheets].sort((a: DrawingSheet, b: DrawingSheet) => a.pageIndex - b.pageIndex);
      const { presets, partDrawingSheet } = state.partEditor;

      // Convert the nested part object to the flattened version expected by the update endpoint
      const partDrawingSheetInput = DrawingSheets.createGraphqlInput({
        ...partDrawing.sheets[sheetIndex],
        ...partDrawingSheetChanges,
        characteristics: partDrawing?.characteristics?.filter((char) => char.id === partDrawing.sheets[sheetIndex].id) || [],
      });

      if (partDrawingSheetChanges.characteristics) {
        partDrawingSheetInput.characteristics = [...partDrawingSheetChanges.characteristics];
      }

      // Update feature locations if the grid changed
      const chars: Characteristic[] = [];
      const { capturedItems } = state.characteristics;
      if (capturedItems && capturedItems[sheetIndex]) {
        capturedItems[sheetIndex].forEach((capture: CapturedItem) => {
          chars.push(capture.rectangle?.toObject());
        });
        const { layerManager, tronInstance } = state.pdfView;
        const instance = tronInstance;
        if (!instance) return;
        const currentPage = instance.docViewer.getCurrentPage();
        const document = instance.docViewer?.getDocument();
        const canvas = layerManager.getCurrentCanvasFor('markers');
        // view values from the DocumentView context aren't updating, pulling from PDFTron for now
        const scale = instance && document ? instance.docViewer.getZoomLevel() : 1;
        const rotation = instance ? instance.docViewer.getCompleteRotation(sheetIndex + 1) * 90 : 0;
        const updates = updateItemCoordinates(chars, state.partEditor.gridRenderer.grid, canvas, scale, rotation);
        if (updates && updates.length > 0) {
          dispatch({
            type: CharacteristicActions.EDIT_CHARACTERISTICS,
            payload: {
              updates,
            },
          });
        }
      }

      // Apply the changes and make sure Type is correct
      // const partDrawingSheetInput: DrawingSheetInput = { ...flattenedPartDrawingSheet };
      // Send the data to the server and await the update result
      if (partDrawing?.sheets[sheetIndex] && partDrawing.sheets[sheetIndex].id) {
        const drawingSheet = await updateDrawingSheetMutation({ id: partDrawing.sheets[sheetIndex].id, data: partDrawingSheetInput }, client);
        partDrawing.sheets[sheetIndex] = { ...drawingSheet.data.drawingSheetUpdate };
        const drawingGridOptions = JSON.parse(partDrawing.gridOptions || '{}');
        if (drawingGridOptions.status !== true && partDrawingSheetChanges.useGrid) {
          // Convert the nested part object to the flattened version expected by the update endpoint
          const partDrawingInput: DrawingInput = DrawingsAPI.createGraphqlInput({ ...partDrawing, gridOptions: JSON.stringify({ ...drawingGridOptions, status: true }), sheets: partDrawing.sheets, characteristics: partDrawing.characteristics });

          // Send the data to the server and await the update result
          const updatedDrawing = await updatePartDrawingMutation({ id: partDrawing.id, data: partDrawingInput }, client);
          partDrawing = { ...updatedDrawing.data.drawingUpdate, sheets: updatedDrawing.data.drawingUpdate.sheets };
          partDrawing.sheets.sort((a: DrawingSheet, b: DrawingSheet) => a.pageIndex - b.pageIndex);
        }
        if (sheetIndex === partDrawingSheet?.pageIndex) {
          const settingsDefaultTolerance = state.session.settings.tolerancePresets?.metadata[presets?.tolerances || 'Default'].setting as Tolerance | undefined;
          const tolerances = rebuildToleranceSettingObject({
            currentPreset: 'Default',
            linear: drawingSheet.data.drawingSheetUpdate.defaultLinearTolerances,
            angular: drawingSheet.data.drawingSheetUpdate.defaultAngularTolerances,
            settings: settingsDefaultTolerance,
          });
          if (tolerances.linear.unit === defaultUnitIDs.inch && state.part.part.measurement === 'Metric') {
            // Fix default unit, since linear defaults to Imperial measurement
            tolerances.linear.unit = defaultUnitIDs.millimeter;
          }
          dispatch({ type: PartEditorActions.SET_PART_EDITOR_DRAWING_SHEET, payload: { sheet: { ...drawingSheet.data.drawingSheetUpdate }, drawing: partDrawing, tolerances } });
        } else {
          dispatch({ type: PartEditorActions.SET_PART_EDITOR_DRAWING, payload: partDrawing });
        }
        const updatedPart: Part = { ...state.part.part };
        const partDrawings = updatedPart.drawings ? [...updatedPart.drawings] : [partDrawing];
        partDrawings[partDrawings.findIndex((drawing) => drawing.id === partDrawing.id)] = partDrawing;
        updatedPart.drawings = partDrawings;
        dispatch({ type: PartActions.SET_PART, payload: updatedPart });
      } else {
        throw new Error('Cannot update drawing sheet without valid ID');
      }
      dispatch({ type: PartEditorActionTypes.SET_PART_EDITOR_LOADED, payload: true });
    } catch (err) {
      log.error(err);
      dispatch({ type: PartEditorActions.SET_PART_EDITOR_ERROR, payload: err });
    }
  });
};
// all sheets
export const updateDrawingSheetsThunk = (changes: { drawingSheetChanges: DrawingSheetChange; pageIndex: number }[]) => async (dispatch: Dispatch, getState: () => AppState, clientPromise: Promise<ApolloClient<NormalizedCacheObject>>) => {
  return clientPromise.then(async (client) => {
    const state = getState();
    try {
      if (!state.partEditor.partDrawing || !state.part.part) {
        throw new Error('Unable to update part drawing sheet, because part drawing is null.');
      }

      dispatch({ type: PartEditorActionTypes.SET_PART_EDITOR_LOADED, payload: false });

      const partDrawing = { ...state.partEditor.partDrawing };
      const partDrawingSheets = [...partDrawing.sheets].sort((a: DrawingSheet, b: DrawingSheet) => a.pageIndex - b.pageIndex);

      await Promise.all(
        changes.map(async (change) => {
          // Apply the changes and make sure Type is correct
          // const partDrawingSheetInput: DrawingSheetInput = { ...flattenedPartDrawingSheet };
          // Send the data to the server and await the update result
          const { drawingSheetChanges, pageIndex } = change;
          if (partDrawing?.sheets[pageIndex] && partDrawing.sheets[pageIndex].id) {
            // Convert the nested part object to the flattened version expected by the update endpoint
            const partDrawingSheetInput = DrawingSheets.createGraphqlInput({
              ...partDrawing.sheets[pageIndex],
              ...drawingSheetChanges,
              characteristics: partDrawing?.characteristics?.filter((char) => char.drawing.id === partDrawing.sheets[pageIndex].id) || [],
            });

            if (drawingSheetChanges.characteristics) {
              partDrawingSheetInput.characteristics = [...drawingSheetChanges.characteristics!];
            }
            const drawingSheet = await updateDrawingSheetMutation({ id: partDrawing.sheets[pageIndex].id, data: partDrawingSheetInput }, client);

            partDrawingSheets[pageIndex] = { ...drawingSheet.data.drawingSheetUpdate };
            if (state.partEditor.partDrawingSheet && drawingSheet.data.drawingSheetUpdate.id === state.partEditor.partDrawingSheet.id) {
              dispatch({ type: PartEditorActions.SET_PART_EDITOR_DRAWING_SHEET, payload: { sheet: { ...drawingSheet.data.drawingSheetUpdate } } });
            }
          } else {
            throw new Error('Cannot update drawing sheet without valid ID');
          }
        }),
      );

      partDrawing.sheets = partDrawingSheets;

      const updatedPart: Part = { ...state.part.part };
      const partDrawings = updatedPart.drawings ? [...updatedPart.drawings] : [partDrawing];
      partDrawings[partDrawings.findIndex((drawing) => drawing.id === partDrawing.id)] = partDrawing;
      updatedPart.drawings = partDrawings;
      dispatch({ type: PartActions.SET_PART, payload: updatedPart });
      dispatch({ type: PartEditorActions.SET_PART_EDITOR_STATE, payload: { partDrawing, partEditorLoaded: true } });
    } catch (err: any) {
      log.error(err);
      dispatch({ type: PartEditorActions.SET_PART_EDITOR_STATE, payload: { partEditorLoaded: true, partEditorError: err, partEditorMessage: err.message } });
    }
  });
};

export const updateDrawingThunk = (drawingChanges: DrawingChange, sheetIndex?: number) => async (dispatch: Dispatch, getState: () => AppState, clientPromise: Promise<ApolloClient<NormalizedCacheObject>>) => {
  return clientPromise.then(async (client) => {
    const state = getState();
    const { part } = state.part;
    const { partDrawing, presets } = state.partEditor;
    try {
      dispatch({ type: PartEditorActions.SET_PART_EDITOR_LOADED, payload: false });
      if (!part || !partDrawing || !partDrawing.sheets || partDrawing.sheets.length <= 0 || (sheetIndex && !partDrawing.sheets[sheetIndex])) {
        throw new Error('Unable to update part drawing, because part drawing is null.');
      }

      // Convert the nested part object to the flattened version expected by the update endpoint
      const partDrawingInput: DrawingInput = DrawingsAPI.createGraphqlInput({ ...partDrawing, ...drawingChanges, sheets: partDrawing.sheets, characteristics: partDrawing.characteristics, part });

      if (drawingChanges.sheets) {
        partDrawingInput.sheets = [...drawingChanges.sheets];
      }
      if (drawingChanges.characteristics) {
        partDrawingInput.characteristics = [...drawingChanges.characteristics];
      }

      if (drawingChanges.gridOptions && sheetIndex !== undefined) {
        const sheet = partDrawing.sheets[sheetIndex];
        const opts = JSON.parse(drawingChanges.gridOptions);

        if (Object.keys(opts).includes('status') && !!sheet) {
          const drawingSheetInput = DrawingSheets.createGraphqlInput({ ...sheet, useGrid: opts.status });
          await updateDrawingSheetMutation({ id: sheet.id, data: drawingSheetInput }, client);
        }
      }

      // Send the data to the server and await the update result
      const updatedDrawing = await updatePartDrawingMutation({ id: partDrawing.id, data: partDrawingInput }, client);

      const drawing: Drawing = { ...updatedDrawing.data.drawingUpdate, sheets: updatedDrawing.data.drawingUpdate.sheets };
      drawing.sheets.sort((a: DrawingSheet, b: DrawingSheet) => a.pageIndex - b.pageIndex);

      const settingsDefaultTolerance = state.session.settings.tolerancePresets?.metadata[presets?.tolerances || 'Default'].setting as Tolerance | undefined;
      const tolerances = rebuildToleranceSettingObject({
        currentPreset: 'Default',
        linear: drawing.sheets[sheetIndex || 0]?.defaultLinearTolerances,
        angular: drawing.sheets[sheetIndex || 0]?.defaultAngularTolerances,
        settings: settingsDefaultTolerance,
      });
      if (tolerances.linear.unit === defaultUnitIDs.inch && part.measurement === 'Metric') {
        // Fix default unit, since linear defaults to Imperial measurement
        tolerances.linear.unit = defaultUnitIDs.millimeter;
      }
      const newPart = { ...part };
      newPart.drawings = newPart?.drawings?.map((d) => {
        if (d.id === drawing.id) {
          return { ...d, ...drawing };
        }
        return d;
      });
      if (newPart.primaryDrawing.id === drawing.id) {
        newPart.primaryDrawing = drawing;
      }
      const newEditorState = {
        ...state.partEditor,
        partDrawing: drawing,
        partDrawingSheet: drawing.sheets[sheetIndex || 0],
        partEditorLoaded: true,
        tolerances,
      };
      dispatch({ type: PartActions.SET_PART, payload: newPart });
      if (state.partEditor.partDrawing?.id === drawing.id) {
        const { gridCache, newGridRenderer } = getGrid(drawing, newEditorState, sheetIndex);
        dispatch({
          type: PartEditorActions.SET_PART_EDITOR_STATE,
          payload: { ...newEditorState, gridCache, gridRenderer: newGridRenderer },
        });
      } else {
        dispatch({ type: PartEditorActions.SET_PART_EDITOR_STATE, payload: newEditorState });
      }
      const updatedPart = { ...part, drawings: part.drawings ? [...part.drawings] : [] };
      const drawingIndex = updatedPart.drawings.findIndex((draw) => draw.id === drawing.id);
      if (drawingIndex >= 0) updatedPart.drawings[drawingIndex] = drawing;
      dispatch({ type: PartActions.SET_PART, payload: updatedPart });
    } catch (err) {
      log.error(err);
      dispatch({ type: PartEditorActions.SET_PART_EDITOR_ERROR, payload: err });
    }
  });
};

export const changeDrawingThunk = (id: string) => async (dispatch: Dispatch<any>, getState: () => AppState, clientPromise: Promise<ApolloClient<NormalizedCacheObject>>) => {
  return clientPromise.then(async () => {
    const state = getState();
    const { part, revisions } = state.part;
    const { characteristics, markers, defaultMarker, annotations } = state.characteristics;
    const { partDrawing, assignedStyles } = state.partEditor;
    try {
      if (id === partDrawing?.id) {
        log.warn(`Already viewing drawing ${id}`);
        return;
      }
      dispatch({ type: PartEditorActions.SET_PART_EDITOR_LOADED, payload: false });
      dispatch({ type: PdfViewActions.SET_LOADED, payload: false });
      dispatch({ type: PdfViewActions.SET_INDEX, payload: 0 });
      const drawing = part?.drawings?.find((d) => d.id === id) || null;
      if (!drawing) {
        throw new Error('Unable to change part drawing.');
      }
      const newDrawing = { ...drawing, sheets: [...drawing.sheets] };
      newDrawing.sheets.sort((a: DrawingSheet, b: DrawingSheet) => a.pageIndex - b.pageIndex);
      const { gridCache, newGridRenderer } = getGrid(drawing, state.partEditor, 0);
      dispatch({ type: PartEditorActions.SET_PART_EDITOR_STATE, payload: { partDrawing: newDrawing, partDrawingSheet: newDrawing.sheets[0], gridCache, gridRenderer: newGridRenderer, partEditorLoaded: true } });

      if (part && newDrawing.originalDrawing === partDrawing?.originalDrawing) {
        // changing drawing revisions, need to reload characteristics
        dispatch(loadCharacteristicsThunk(part.id, assignedStyles || {}));
      } else {
        // changed to a different drawing that already has characteristics loaded, just need to swap out capturedItems
        const newCaptures = getCapturedItems(characteristics, annotations, markers, defaultMarker || markers[0], newDrawing.id);
        dispatch({ type: CharacteristicActions.UPDATE_CAPTURES, payload: { capturedItems: newCaptures } });
      }
      if (part) {
        const { revisionIndex, revisionType } = checkCurrentRevision(part, revisions, newDrawing);
        dispatch({ type: PartActions.SET_REVISION_INDEX, payload: { revisionIndex, revisionType } });
      }
    } catch (err: any) {
      log.error(err);
      dispatch({ type: PartEditorActions.SET_PART_EDITOR_STATE, payload: { partEditorLoaded: true, partEditorError: err, partEditorMessage: err.message } });
    }
    dispatch({ type: PdfViewActions.SET_LOADED, payload: true });
    dispatch({ type: PartEditorActions.SET_PART_EDITOR_LOADED, payload: true });
  });
};

export const startCompareDrawingThunk = (id: string, compareMode: 'Compare' | 'Align') => async (dispatch: Dispatch<any>, getState: () => AppState, clientPromise: Promise<ApolloClient<NormalizedCacheObject>>) => {
  return clientPromise.then(async () => {
    const state = getState();
    const { part } = state.part;
    const { partDrawing } = state.partEditor;

    try {
      dispatch({ type: PartEditorActions.SET_PART_EDITOR_LOADED, payload: false });
      dispatch({ type: PdfViewActions.SET_LOADED, payload: false });
      dispatch({ type: PdfViewActions.SET_INDEX, payload: 0 });
      const drawing = part?.drawings?.find((d) => d.id === id) || null;
      if (!drawing) {
        throw new Error('Unable to change part drawing.');
      }
      const newDrawing = { ...drawing, sheets: [...drawing.sheets] };
      newDrawing.sheets.sort((a: DrawingSheet, b: DrawingSheet) => a.pageIndex - b.pageIndex);

      Analytics.track({
        event: Analytics.events.partDrawingRevisionCompare,
        part,
        drawing: partDrawing,
        compareDrawing: newDrawing,
        properties: {
          'part.id': part?.id,
        },
      });

      dispatch({ type: PartEditorActions.SET_PART_EDITOR_STATE, payload: { compareDrawing: newDrawing, compareDrawingSheet: newDrawing.sheets[0], compareMode, partEditorLoaded: true } });
    } catch (err: any) {
      log.error(err);
      dispatch({ type: PartEditorActions.SET_PART_EDITOR_STATE, payload: { partEditorError: err, partEditorMessage: err.message } });
    }
    dispatch({ type: PdfViewActions.SET_LOADED, payload: true });
    dispatch({ type: PartEditorActions.SET_PART_EDITOR_LOADED, payload: true });
  });
};

export const startDrawingAlignmentThunk = (files: any, drawingId?: any) => async (dispatch: Dispatch<any>, getState: () => AppState, clientPromise: Promise<ApolloClient<NormalizedCacheObject>>) => {
  return clientPromise.then(async () => {
    try {
      const state = getState();
      const { part } = state.part;
      const { partDrawing } = state.partEditor;
      const { tronInstance, layerManager } = state.pdfView;
      const { jwt } = state.auth;
      const fileObj = files[0].originFileObj;

      const drawing = partDrawing || part?.drawings?.find((d) => d.id === drawingId);

      Analytics.track({
        event: Analytics.events.partDrawingRevisionCompareAndReplaceStarted,
        part,
        drawing,
        compareDrawing: { drawingFile: fileObj },
      });

      dispatch({ type: PartEditorActions.SET_PART_EDITOR_STATE, payload: { compareMode: 'Align', partEditorLoaded: true, targetDrawing: null } });
      dispatch({ type: CharacteristicActions.UPDATE_CAPTURES, payload: { capturedItems: [] } });
      dispatch({ type: PdfOverlayViewActions.SET_STATE, payload: { revisionFile: fileObj, revisionFiles: null } });
      if (jwt && tronInstance && drawing) await loadPdfCompareOverlay(tronInstance, drawing, jwt, null, layerManager);
    } catch (err: any) {
      log.error(err);
      dispatch({ type: PartEditorActions.SET_PART_EDITOR_STATE, payload: { partEditorError: err, partEditorMessage: err.message } });
    }
  });
};

export const endDrawingAlignmentThunk =
  (redirectToDocs = false) =>
  async (dispatch: Dispatch<any>, getState: () => AppState, clientPromise: Promise<ApolloClient<NormalizedCacheObject>>) => {
    return clientPromise.then(async () => {
      const state = getState();
      const { tronInstance } = state.pdfView;
      const { partDrawing, assignedStyles } = state.partEditor;
      const { jwt } = state.auth;
      const { part } = state.part;

      Analytics.track({
        event: redirectToDocs ? Analytics.events.partDrawingRevisionCompareAndReplaceCompleted : Analytics.events.partDrawingRevisionCompareAndReplaceCanceled,
        part,
        drawing: partDrawing,
        properties: {
          'part.id': part?.id,
        },
      });

      try {
        dispatch({ type: PartEditorActions.SET_PART_EDITOR_STATE, payload: { compareDrawing: null, compareSheet: null, compareMode: null } });
        dispatch(resetPdfOverlayView());
        dispatch({ type: CharacteristicActions.SET_REVISION, payload: { compareRevision: null, compareIndex: 0 } });
        if (partDrawing && part && assignedStyles && partDrawing.drawingFile.privateUrl) {
          // reset the current drawing in the viewer
          callDocLoad(tronInstance, partDrawing.drawingFile.privateUrl, jwt, true, () => {}).then(() => {
            dispatch(loadCharacteristicsThunk(part.id, assignedStyles));
            if (redirectToDocs) dispatch({ type: NavigationActions.SET_CURRENT_PART_STEP, payload: PartStepEnum.DOCS });
          });
        }
      } catch (err: any) {
        log.error(err);
        dispatch({ type: PartEditorActions.SET_PART_EDITOR_STATE, payload: { partEditorError: err, partEditorMessage: err.message } });
      }
    });
  };
