import { GridCanvasRenderer } from 'domain/part/edit/grid/GridRenderer';
import { PartEditorAction, PartEditorState } from 'modules/state';
import { DefaultTolerances } from 'view/global/defaults';

const initialState: PartEditorState = {
  partDrawing: null,
  partDrawingSheet: null,
  compareDrawing: null,
  compareDrawingSheet: null,
  compareMode: null,
  targetDrawing: null,
  gridRenderer: new GridCanvasRenderer(),
  gridCache: [],
  tolerances: DefaultTolerances,
  usedStyles: null,
  defaultMarkerOptions: null,
  assignedStyles: null,
  presets: null,
  nextBalloonNumber: null,
  focus: null,
  partEditorError: null,
  partEditorMessage: null,
  partEditorLoaded: true,
};

export default (state: PartEditorState = initialState, { type, payload }: PartEditorAction): PartEditorState => {
  const initialCopy = { ...initialState };
  switch (type) {
    case 'PART_EDITOR_SET_STATE':
      return { ...state, ...payload };

    case 'PART_EDITOR_SET_DRAWING':
      return { ...state, partDrawing: payload };

    case 'PART_EDITOR_SET_SHEET':
      return { ...state, partDrawingSheet: payload.sheet, partDrawing: payload.drawing || state.partDrawing, tolerances: payload.tolerances || state.tolerances };

    case 'PART_EDITOR_SET_GRID_CACHE':
      return { ...state, gridCache: payload };

    case 'PART_EDITOR_SET_ASSIGNED_STYLES':
      return { ...state, assignedStyles: payload };

    case 'PART_EDITOR_SET_PRESETS':
      return { ...state, presets: payload };

    case 'PART_EDITOR_SET_TOLERANCES':
      return { ...state, tolerances: payload };

    case 'PART_EDITOR_SET_FOCUS':
      return { ...state, focus: payload };

    case 'PART_EDITOR_SET_LOADED':
      return { ...state, partEditorLoaded: payload };

    case 'PART_EDITOR_SET_ERROR':
      return { ...state, partEditorLoaded: true, partEditorError: payload, partEditorMessage: payload.message };

    case 'PART_EDITOR_RESET_STATE':
      if (initialCopy.focus === null) delete initialCopy.focus;
      return { ...state, ...initialCopy };

    default:
      return state;
  }
};
