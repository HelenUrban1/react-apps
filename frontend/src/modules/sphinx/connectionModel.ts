import { Connection, DestinationCredential } from 'modules/sphinx/sphinxInterfaces';

export const getApiKeyFromConnections = (connections: Connection[]): string => {
  if (!connections || !connections.length) {
    return '';
  }

  return connections[0].apiKey;
};

export const addDestinationIds = (connection: Connection, newDestinationsIds: string[]): string[] => {
  const existingDestinationIds: string[] = Array.isArray(connection.destinationIds) ? connection.destinationIds : [];
  const ids: string[] = [...existingDestinationIds, ...newDestinationsIds];

  return Array.from(new Set(ids));
};

export const isResourceNotFoundError = (error: Error): boolean => {
  return error?.message?.includes('status code 404');
};

export const isServerError = (error: Error): boolean => {
  return error?.message?.includes('status code 500');
};

export const getCredentialsForDestination = (connection: Connection, destinationId: string): DestinationCredential | undefined => {
  if (!connection.destinationCredentials) {
    return undefined;
  }

  return connection.destinationCredentials.find((item: DestinationCredential) => item.destinationId === destinationId);
};

export const destinationSelectChangeHandler = (selectedIdsState: string[], setSelectedIdsState: (val: string[]) => void, destinationId: string, selected: boolean) => {
  if (selected && !selectedIdsState.includes(destinationId)) {
    setSelectedIdsState([...selectedIdsState, destinationId]);
  } else {
    setSelectedIdsState(selectedIdsState.filter((id: string) => id !== destinationId));
  }
};
