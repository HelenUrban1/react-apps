import { Definition, Destination, DefinitionItemTypeEnum, Source } from 'modules/sphinx/sphinxInterfaces';

export const getSourceDefinitions = (definitions: Definition[]): Source[] => {
  return definitions?.filter((item: Definition) => {
    return item.enabled && item.type === DefinitionItemTypeEnum.source;
  }) as Source[];
};

export const getDestinationDefinitions = (definitions: Definition[]): Destination[] => {
  return definitions?.filter((item: Definition) => {
    return item.enabled && item.type === DefinitionItemTypeEnum.destination;
  }) as Destination[];
};

export const getSourceName = (definitionId: string | undefined, definitions: Definition[]): string => {
  if (!definitionId || !definitions || !definitions.length) {
    return '';
  }

  const definition: Definition | undefined = definitions.find((item: Definition) => {
    return item.id === definitionId;
  });

  return definition ? definition.name : '';
};

export const getDestination = (destinationId: string, definitions: Definition[]): Destination | undefined => {
  const destinations: Destination[] = getDestinationDefinitions(definitions);
  return destinations.find((destination: Destination) => destination.id === destinationId);
};

export const getDestinationName = (destinationId: string, definitions: Definition[]): string => {
  if (!definitions || !definitions.length) {
    return '';
  }

  const definition: Definition | undefined = definitions.find((item: Definition) => {
    return item.id === destinationId;
  });

  return definition ? definition.name : '';
};

export const isNetInspectDestination = (destinationId: string | undefined, destinations: Destination[]): boolean => {
  if (!destinationId) {
    return false;
  }

  const netInspectDestination: Destination | undefined = destinations.find((item: Destination) => item.name === 'Net-Inspect');

  return netInspectDestination !== undefined && netInspectDestination.id === destinationId;
};
