import { v4 as uuid } from 'uuid';
import { Connection } from 'modules/sphinx/sphinxInterfaces';
import { addDestinationIds, getApiKeyFromConnections, isResourceNotFoundError } from './connectionModel';

describe('Connection Model', () => {
  const createFixture = (): Connection => {
    return {
      id: uuid(),
      apiKey: uuid(),
      accountId: uuid(),
      siteId: uuid(),
      userId: uuid(),
      sourceId: uuid(),
      enabled: true,
      created: new Date().toISOString(),
      updated: new Date().toISOString(),
    };
  };

  describe('#getApiKeyFromConnections()', () => {
    it('should return an empty string when provided with no Connections', () => {
      const items: Connection[] = [];
      expect(getApiKeyFromConnections(items)).toBe('');
    });

    it('should return the API key from a Connection', () => {
      const item: Connection = createFixture();
      const items: Connection[] = [item];
      expect(getApiKeyFromConnections(items)).toBe(item.apiKey);
    });
  });

  describe('#addDestinationIds()', () => {
    it('should match the provided ids if none previously were set', () => {
      const item: Connection = createFixture();
      const ids: string[] = ['a', 'b'];
      const result: string[] = addDestinationIds(item, ids);
      expect(result).toEqual(ids);
    });

    it('should retain existing ids when adding the new ones', () => {
      const item: Connection = createFixture();
      item.destinationIds = ['a'];
      const ids: string[] = ['b', 'c'];
      const result: string[] = addDestinationIds(item, ids);
      expect(result).toEqual(['a', 'b', 'c']);
    });

    it('should not return duplicates', () => {
      const item: Connection = createFixture();
      item.destinationIds = ['a', 'b'];
      const ids: string[] = ['a', 'b', 'c', 'd'];
      const result: string[] = addDestinationIds(item, ids);
      expect(result).toEqual(['a', 'b', 'c', 'd']);
    });
  });

  describe('#isResourceNotFoundError()', () => {
    it('should return false if the error has no message', () => {
      const error: Error = new Error();
      expect(isResourceNotFoundError(error)).toBe(false);
    });

    it('should return false if the error message does not match the 404 message', () => {
      const error: Error = new Error('status code 500');
      expect(isResourceNotFoundError(error)).toBe(false);
    });

    it('should return true if the error message matchs the 404 message', () => {
      const error: Error = new Error('status code 404');
      expect(isResourceNotFoundError(error)).toBe(true);
    });
  });
});
