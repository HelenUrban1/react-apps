import gql from 'graphql-tag';
import { DocumentNode } from 'graphql';

type RequestTypeMap = { [key: string]: DocumentNode };

interface GraphQlApi {
  query: RequestTypeMap;
  mutate: RequestTypeMap;
}

export interface UploadResponse {
  upload: {
    succeeded: {
      destination: string;
      data?: string;
    }[];
    failed: string[];
  };
}
export interface UploadVars {
  data: {
    apiKey: string;
    payload?: string;
    connectionId?: string;
    partId?: string;
    firstArticleId?: string;
  };
}

const connection: string = `
  id
  apiKey
  userId
  enabled
  sourceId
  destinationIds
  transformations {
    id
    priority
  }
  destinationCredentials {
    destinationId
    userName
    password
    company
  }
`;

const SphinxApi: GraphQlApi = {
  query: {
    connectionHealth: gql`
      query CONNECTION_HEALTH($data: ApiKeyRotateInput!) {
        connectionHealth(data: $data)
      }
    `,
    connectionFind: gql`
      query CONNECTION_FIND($data: ConnectionFindInput!) {
        connectionFind(data: $data) {
          ${connection}
        }
      }
    `,
    connectionFindById: gql`
      query CONNECTION_FIND_BY_ID($data: ConnectionFindByIdInput!) {
        connectionFindById(data: $data) {
          ${connection}
        }
      }
    `,
    definitionList: gql`
      query DEFINITION_LIST {
        definitionList {
          id
          name
          type
          enabled
          version
          created
          updated
        }
      }
    `,
  },
  mutate: {
    apiKeyRotate: gql`
      mutation API_KEY_ROTATE($data: ApiKeyRotateInput!) {
        apiKeyRotate(data: $data)
      }
    `,
    connectionCreate: gql`
      mutation CONNECTION_CREATE($data: ConnectionCreateInput!) {
        connectionCreate(data: $data) {
          ${connection}
        }
      }
    `,
    connectionUpdate: gql`
      mutation CONNECTION_UPDATE($data: ConnectionUpdateInput!) {
        connectionUpdate(data: $data) {
          ${connection}
        }
      }
    `,
    connectionDelete: gql`
      mutation CONNECTION_DELETE($data: ConnectionDeleteInput!) {
        connectionDelete(data: $data)
      }
    `,
    // https://bitbucket.org/ixeng/api-proxy/src/develop/src/upload/upload.controller.ts
    // links to upload controller in api-proxy. This is where the upload mutation will go
    upload: gql`
      mutation UPLOAD($data: UploadInput!) {
        upload(data: $data) {
          succeeded {
            destination
            data
          }
          failed
        }
      }
    `,
  },
};

export default SphinxApi;
