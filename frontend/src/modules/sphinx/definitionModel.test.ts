import { v4 as uuid } from 'uuid';
import { Definition, DefinitionItemTypeEnum } from 'modules/sphinx/sphinxInterfaces';
import { getSourceDefinitions, getDestinationDefinitions, getSourceName, getDestinationName } from './definitionModel';

describe('Definition Model', () => {
  const createFixture = (type: DefinitionItemTypeEnum, name: string = ''): Definition => {
    return {
      type,
      name,
      id: uuid(),
      enabled: true,
      created: new Date().toISOString(),
      updated: new Date().toISOString(),
    };
  };

  describe('#getSourceDefinitions()', () => {
    it('should return an empty array when no source definitions exist', () => {
      const destination: Definition = createFixture(DefinitionItemTypeEnum.destination);
      const items: Definition[] = [destination];
      expect(getSourceDefinitions(items)).toEqual([]);
    });

    it('should return an array containing only sources', () => {
      const source: Definition = createFixture(DefinitionItemTypeEnum.source);
      const destination: Definition = createFixture(DefinitionItemTypeEnum.destination);
      const items: Definition[] = [source, destination];
      expect(getSourceDefinitions(items)).toEqual([source]);
    });

    it('should not include disabled sources', () => {
      const sourceEnabled: Definition = createFixture(DefinitionItemTypeEnum.source);
      const sourceDisabled: Definition = createFixture(DefinitionItemTypeEnum.source);
      sourceDisabled.enabled = false;
      const items: Definition[] = [sourceEnabled, sourceDisabled];
      expect(getSourceDefinitions(items)).toEqual([sourceEnabled]);
    });
  });

  describe('#getDestinationDefinitions()', () => {
    it('should return an empty array when no destination definitions exist', () => {
      const source: Definition = createFixture(DefinitionItemTypeEnum.source);
      const items: Definition[] = [source];
      expect(getDestinationDefinitions(items)).toEqual([]);
    });

    it('should return an array containing only destinations', () => {
      const source: Definition = createFixture(DefinitionItemTypeEnum.source);
      const destination: Definition = createFixture(DefinitionItemTypeEnum.destination);
      const items: Definition[] = [source, destination];
      expect(getDestinationDefinitions(items)).toEqual([destination]);
    });

    it('should not include disabled destinations', () => {
      const destinationEnabled: Definition = createFixture(DefinitionItemTypeEnum.destination);
      const destinationDisabled: Definition = createFixture(DefinitionItemTypeEnum.destination);
      destinationDisabled.enabled = false;
      const items: Definition[] = [destinationEnabled, destinationDisabled];
      expect(getDestinationDefinitions(items)).toEqual([destinationEnabled]);
    });
  });

  describe('#getSourceName()', () => {
    it('should return an empty string when no definitions exist', () => {
      const source: Definition = createFixture(DefinitionItemTypeEnum.source);
      const items: Definition[] = [];
      expect(getSourceName(source.id, items)).toBe('');
    });

    it('should return an empty string when a match is not found', () => {
      const source: Definition = createFixture(DefinitionItemTypeEnum.source);
      const items: Definition[] = [source];
      expect(getSourceName('some-id-that-does-not-exist', items)).toBe('');
    });

    it('should return the name of a source when a match is found', () => {
      const name: string = 'source name';
      const sourceA: Definition = createFixture(DefinitionItemTypeEnum.source, name);
      const sourceB: Definition = createFixture(DefinitionItemTypeEnum.source, 'some other name');
      const items: Definition[] = [sourceA, sourceB];
      expect(getSourceName(sourceA.id, items)).toBe(name);
    });
  });

  describe('#getDestinationName()', () => {
    it('should return an empty string when no definitions exist', () => {
      const destination: Definition = createFixture(DefinitionItemTypeEnum.destination);
      const items: Definition[] = [];
      expect(getDestinationName(destination.id, items)).toBe('');
    });

    it('should return an empty string when a match is not found', () => {
      const destination: Definition = createFixture(DefinitionItemTypeEnum.destination);
      const items: Definition[] = [destination];
      expect(getDestinationName('some-id-that-does-not-exist', items)).toBe('');
    });

    it('should return the name of a source when a match is found', () => {
      const name: string = 'destination name';
      const destinationA: Definition = createFixture(DefinitionItemTypeEnum.destination, name);
      const destinationB: Definition = createFixture(DefinitionItemTypeEnum.destination, 'some other name');
      const items: Definition[] = [destinationA, destinationB];
      expect(getDestinationName(destinationA.id, items)).toBe(name);
    });
  });
});
