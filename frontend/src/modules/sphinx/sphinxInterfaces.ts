// See connection.interface.ts in the api-proxy repo. The interfaces
// in this file should match the ones there, with the interfaces in
// the api-proxy repo being the source of truth.

export interface DestinationCredential {
  readonly destinationId: string;
  readonly userName: string;
  readonly company: string;
  readonly password: string;
}

interface Transformation {
  // The id of the transformation item in `definitions`
  id: string;

  // Specifies the order of transformations, which are
  // executed based on this value in ascending order.
  priority: number;
}

export interface ConnectionFindInput {
  accountId: string;
  siteId: string;
  userId: string;
}

export interface Connection {
  id: string;
  apiKey: string;
  accountId: string;
  siteId: string;
  userId: string;
  enabled: boolean;
  created: string;
  updated: string;
  sourceId: string;
  destinationIds?: string[];
  destinationCredentials?: DestinationCredential[];
  transformations?: Transformation[];
}

export enum DefinitionItemTypeEnum {
  source = 'source',
  transformation = 'transformation',
  destination = 'destination',
}

export interface Definition {
  id: string;
  name: string;
  type: DefinitionItemTypeEnum;
  enabled: boolean;
  created: string;
  updated: string;
  version?: string;
}

export interface Source extends Definition {
  type: DefinitionItemTypeEnum.source;
}

export interface Destination extends Definition {
  type: DefinitionItemTypeEnum.destination;
}
