import { PdfViewAction, PdfViewState } from 'modules/state';
import { CanvasLayerManager } from 'styleguide/PdfViewer/canvasLayerManager';

export const initialPdfViewState = {
  height: 0,
  width: 0,
  zoom: 0,
  index: 0,
  rotation: 0,
  layerManager: new CanvasLayerManager({ layers: ['grid', 'markers'] }),
  pages: [],
  tronInstance: null,
  tronOverlayInstance: null,
  overlayView: {
    height: 0,
    width: 0,
    zoom: 0,
    index: 0,
    rotation: 0,
  },
  fileUrl: null,
  pdfLoaded: false,
};

export default (state: PdfViewState = initialPdfViewState, { type, payload }: PdfViewAction): PdfViewState => {
  switch (type) {
    case 'PDF_VIEW_SET_STATE':
      return { ...state, ...payload };

    case 'PDF_VIEW_SET_TRON':
      return { ...state, tronInstance: payload };

    case 'PDF_VIEW_SET_ZOOM':
      return { ...state, zoom: payload };

    case 'PDF_VIEW_SET_INDEX':
      return { ...state, index: payload };

    case 'PDF_VIEW_SET_ROTATION':
      return { ...state, rotation: payload };

    case 'PDF_VIEW_SET_DIMS':
      return { ...state, height: payload.height, width: payload.width };

    case 'PDF_VIEW_SET_FILE_URL':
      return { ...state, fileUrl: payload };

    case 'PDF_VIEW_SET_LOADED':
      return { ...state, pdfLoaded: payload };

    case 'PDF_VIEW_RESET':
      return { ...state, ...initialPdfViewState };

    default:
      return state;
  }
};
