import { ApolloClient, NormalizedCacheObject } from '@apollo/client';
import { WebViewerInstance } from '@pdftron/webviewer';
import { fabric } from 'fabric';
import { setPdfOverlayViewState } from 'modules/pdfOverlayView/pdfViewActions';
import log from 'modules/shared/logger';
import { AppState } from 'modules/state';
import { Dispatch } from 'redux';
import { handlePageComplete } from 'styleguide/PdfViewer/PdfViewerUtil';

const prefix = 'PDF_VIEW';

export const PdfViewActions = {
  SET_STATE: `${prefix}_SET_STATE`,
  SET_TRON: `${prefix}_SET_TRON`,
  SET_TRON_OVERLAY: `${prefix}_SET_TRON_OVERLAY`,
  SET_ROTATION: `${prefix}_SET_ROTATION`,
  SET_ZOOM: `${prefix}_SET_ZOOM`,
  SET_INDEX: `${prefix}_SET_INDEX`,
  SET_DIMS: `${prefix}_SET_DIMS`,
  SET_FILE_URL: `${prefix}_SET_FILE_URL`,
  SET_LOADED: `${prefix}_SET_LOADED`,
  RESET: `${prefix}_RESET`,
};

export const setPdfViewState = (payload: any) => ({ type: PdfViewActions.SET_STATE, payload });
export const setPdfViewTronInstance = (payload: WebViewerInstance | null) => ({ type: PdfViewActions.SET_TRON, payload });
export const setPdfViewTronOverlayInstance = (payload: WebViewerInstance | null) => ({ type: PdfViewActions.SET_TRON_OVERLAY, payload });
export const setPdfViewRotation = (payload: number) => ({ type: PdfViewActions.SET_ROTATION, payload });
export const setPdfViewZoom = (payload: number) => ({ type: PdfViewActions.SET_ZOOM, payload });
export const setPdfViewIndex = (payload: number) => ({ type: PdfViewActions.SET_INDEX, payload });
export const setPdfViewDims = (payload: { height: number; width: number }) => ({ type: PdfViewActions.SET_DIMS, payload });
export const setPdfViewLoaded = (payload: boolean) => ({ type: PdfViewActions.SET_LOADED, payload });
export const setPdfViewFilePath = (payload: string) => ({ type: PdfViewActions.SET_FILE_URL, payload });
export const resetPdfView = () => ({ type: PdfViewActions.RESET });

export const setPdfViewStateThunk = (payload: any) => async (dispatch: Dispatch<any>, getState: () => AppState, clientPromise: Promise<ApolloClient<NormalizedCacheObject>>) => {
  return clientPromise.then(async () => {
    try {
      const state = getState();
      const { tronInstance, layerManager } = state.pdfView;
      const { revisionFile } = state.pdfOverlayView;
      const viewer = tronInstance.Core.documentViewer;

      const newView = handlePageComplete(payload.e, viewer, tronInstance, layerManager, payload.canvasesCache, payload.fabricsCache, fabric, !!revisionFile);

      if (revisionFile) {
        dispatch(setPdfOverlayViewState(newView));
      } else {
        dispatch(setPdfViewState(newView));
      }
    } catch (err) {
      log.error(err);
    }
  });
};
