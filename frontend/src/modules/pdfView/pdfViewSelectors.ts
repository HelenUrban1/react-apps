import { PdfViewState } from '../state.d';

const selectRaw = (state: { pdfView: PdfViewState }) => state.pdfView;
