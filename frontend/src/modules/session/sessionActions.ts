import { ApolloClient, NormalizedCacheObject } from '@apollo/client';
import { ListItems, ListItemList, ListItemListVars } from 'domain/setting/listsApi';
import { Settings, SettingEdit, SettingEditVars, SettingList, SettingListVars, SettingCreate, SettingCreateVars } from 'domain/setting/settingApi';
import { SettingInput, SettingState } from 'domain/setting/settingTypes';
import Markers, { MarkerList } from 'graphql/marker';
import OrganizationAPI, { OrganizationCreate, OrganizationCreateVars, OrganizationInput, OrganizationList, OrganizationListVars } from 'graphql/organization';
import log from 'modules/shared/logger';
import { AppState } from 'modules/state';
import { Dispatch } from 'redux';
import { User } from 'types/user';
import { organizeListItems } from 'utils/Lists';
import { symbolDefaults } from 'view/global/defaults';
import PermissionChecker from 'modules/auth/permissionChecker';
import Permissions from 'security/permissions';

const prefix = 'SESSION';

export const SessionActions = {
  UPDATE_SESSION_CONTEXT: `${prefix}_UPDATE_SESSION_CONTEXT`,
  UPDATE_USER: `${prefix}_UPDATE_USER`,
  UPDATE_ACCOUNT_MEMBERSHIP: `${prefix}_UPDATE_ACCOUNT_MEMBERSHIP`,
  UPDATE_TYPES: `${prefix}_UPDATE_TYPES`,
  UPDATE_METHODS: `${prefix}_UPDATE_METHODS`,
  UPDATE_CLASSIFICATIONS: `${prefix}_UPDATE_CLASSIFICATIONS`,
  UPDATE_OPERATIONS: `${prefix}_UPDATE_OPERATIONS`,
  UPDATE_METRIC: `${prefix}_UPDATE_METRIC`,
  UPDATE_IMPERIAL: `${prefix}_UPDATE_IMPERIAL`,
  UPDATE_MARKERS: `${prefix}_UPDATE_MARKERS`,
  UPDATE_GLOBAL_SETTINGS: `${prefix}_UPDATE_GLOBAL_SETTINGS`,
  UPDATE_USER_SETTINGS: `${prefix}_UPDATE_USER_SETTINGS`,
  UPDATE_ORGANIZATIONS: `${prefix}_UPDATE_ORGANIZATIONS`,
  UPDATE_NEW_ORG: `${prefix}_UPDATE_NEW_ORG`,
  UPDATE_LISTS: `${prefix}_UPDATE_LISTS`,
  UPDATE_SESSION_LOADED: `${prefix}_UPDATE_SESSION_LOADED`,
};

export enum SessionActionTypes {
  UPDATE_SESSION_CONTEXT = 'SESSION_UPDATE_SESSION_CONTEXT',
  UPDATE_USER = 'SESSION_UPDATE_USER',
  UPDATE_ACCOUNT_MEMBERSHIP = 'SESSION_UPDATE_ACCOUNT_MEMBERSHIP',
  UPDATE_TYPES = 'SESSION_UPDATE_TYPES',
  UPDATE_METHODS = 'SESSION_UPDATE_METHODS',
  UPDATE_CLASSIFICATIONS = 'SESSION_UPDATE_CLASSIFICATIONS',
  UPDATE_OPERATIONS = 'SESSION_UPDATE_OPERATIONS',
  UPDATE_METRIC = 'SESSION_UPDATE_METRIC',
  UPDATE_IMPERIAL = 'SESSION_UPDATE_IMPERIAL',
  UPDATE_MARKERS = 'SESSION_UPDATE_MARKERS',
  UPDATE_GLOBAL_SETTINGS = 'SESSION_UPDATE_GLOBAL_SETTINGS',
  UPDATE_USER_SETTINGS = 'SESSION_UPDATE_USER_SETTINGS',
  UPDATE_ORGANIZATIONS = 'SESSION_UPDATE_ORGANIZATIONS',
  UPDATE_NEW_ORG = 'SESSION_UPDATE_NEW_ORG',
  UPDATE_LISTS = 'SESSION_UPDATE_LISTS',
  UPDATE_SESSION_LOADED = 'SESSION_UPDATE_SESSION_LOADED',
}

export const getListItems = async (client: ApolloClient<NormalizedCacheObject>) => {
  return client.query<ListItemList, ListItemListVars>({
    query: ListItems.query.list,
    fetchPolicy: 'no-cache',
    variables: { filter: { status: 'Active' }, paranoid: false },
  });
};

export const getMarkers = async (client: ApolloClient<NormalizedCacheObject>) => {
  return client.query<MarkerList>({
    query: Markers.query.list,
    fetchPolicy: 'no-cache',
    variables: { paranoid: false },
  });
};

export const getSettings = async (client: ApolloClient<NormalizedCacheObject>) => {
  return client.query<SettingList, SettingListVars>({
    query: Settings.query.list,
    fetchPolicy: 'no-cache',
  });
};

export const getUserSettings = async (queryObject: { id: string }, client: ApolloClient<NormalizedCacheObject>) => {
  return client.query<SettingList, SettingListVars>({
    query: Settings.query.list,
    fetchPolicy: 'no-cache',
    variables: {
      filter: { userId: queryObject.id },
    },
  });
};

export const getOrganizations = async (client: ApolloClient<NormalizedCacheObject>) => {
  return client.query<OrganizationList, OrganizationListVars>({
    query: OrganizationAPI.query.list,
    variables: {
      filter: { status: 'Active' },
    },
  });
};

export const updateSetting = async (queryObject: { id: string; data: SettingInput }, client: ApolloClient<NormalizedCacheObject>) => {
  return client.mutate<SettingEdit, SettingEditVars>({
    mutation: Settings.mutate.edit,
    variables: queryObject,
    refetchQueries: [
      {
        query: Settings.query.list,
      },
    ],
  });
};

export const updateSettingThunk = (input: SettingInput, id: string) => async (dispatch: Dispatch, getState: () => AppState, clientPromise: Promise<ApolloClient<NormalizedCacheObject>>) => {
  return clientPromise.then(async (client) => {
    dispatch({ type: SessionActions.UPDATE_SESSION_LOADED, payload: false });
    try {
      const updatedSetting = await updateSetting({ id, data: input }, client);

      if (updatedSetting.data?.settingUpdate.userId) {
        dispatch({
          type: SessionActions.UPDATE_USER_SETTINGS,
          payload: { userSettings: { ...getState().session.userSettings, [updatedSetting.data?.settingUpdate.name || '']: { ...updatedSetting.data?.settingUpdate, metadata: JSON.parse(updatedSetting.data?.settingUpdate.metadata || '{}') } } },
        });
      } else {
        const payload = { settings: { ...getState().session.settings, [updatedSetting.data?.settingUpdate.name || '']: { ...updatedSetting.data?.settingUpdate, metadata: JSON.parse(updatedSetting.data?.settingUpdate.metadata || '{}') } } };
        dispatch({
          type: SessionActions.UPDATE_GLOBAL_SETTINGS,
          payload,
        });
      }
      dispatch({ type: SessionActions.UPDATE_SESSION_LOADED, payload: true });
    } catch (err) {
      log.error(err);
      dispatch({ type: SessionActions.UPDATE_SESSION_LOADED, payload: true });
    }
  });
};

export const createSetting = async (queryObject: { name: string; value?: string; metadata?: string; userId?: string }, client: ApolloClient<NormalizedCacheObject>) => {
  return client.mutate<SettingCreate, SettingCreateVars>({
    mutation: Settings.mutate.create,
    fetchPolicy: 'no-cache',
    variables: {
      data: { ...queryObject },
    },
    refetchQueries: [
      {
        query: Settings.query.list,
      },
    ],
  });
};

export const createOrganizationMutation = async (queryObject: OrganizationInput, client: ApolloClient<NormalizedCacheObject>) => {
  return client.mutate<OrganizationCreate, OrganizationCreateVars>({
    mutation: OrganizationAPI.mutate.create,
    fetchPolicy: 'no-cache',
    variables: {
      data: { ...queryObject },
    },
    refetchQueries: [
      {
        query: OrganizationAPI.query.list,
      },
    ],
  });
};

export const createOrganizationThunk = (input: OrganizationInput) => async (dispatch: Dispatch, getState: () => AppState, clientPromise: Promise<ApolloClient<NormalizedCacheObject>>) => {
  return clientPromise.then(async (client) => {
    dispatch({ type: SessionActions.UPDATE_SESSION_LOADED, payload: false });
    try {
      const createdOrg = await createOrganizationMutation(input, client);

      if (createdOrg.data?.organizationCreate) {
        dispatch({
          type: SessionActions.UPDATE_ORGANIZATIONS,
          payload: { all: [...getState().session.organizations.all, createdOrg.data?.organizationCreate] },
        });
        dispatch({
          type: SessionActions.UPDATE_NEW_ORG,
          payload: createdOrg.data?.organizationCreate,
        });
      }

      dispatch({ type: SessionActions.UPDATE_SESSION_LOADED, payload: true });
    } catch (err) {
      log.error(err);
      dispatch({ type: SessionActions.UPDATE_SESSION_LOADED, payload: true });
    }
  });
};

export const createSettingThunk = (input: SettingInput, userId?: string) => async (dispatch: Dispatch, getState: () => AppState, clientPromise: Promise<ApolloClient<NormalizedCacheObject>>) => {
  return clientPromise.then(async (client) => {
    dispatch({ type: SessionActions.UPDATE_SESSION_LOADED, payload: false });
    try {
      const createdSetting = await createSetting({ ...input, userId }, client);

      dispatch({
        type: SessionActions.UPDATE_USER_SETTINGS,
        payload: { userSettings: { ...getState().session.userSettings, [createdSetting.data?.settingCreate.name || '']: { ...createdSetting.data?.settingCreate, metadata: JSON.parse(createdSetting.data?.settingCreate.metadata || '{}') } } },
      });

      dispatch({ type: SessionActions.UPDATE_SESSION_LOADED, payload: true });
    } catch (err) {
      log.error(err);
      dispatch({ type: SessionActions.UPDATE_SESSION_LOADED, payload: true });
    }
  });
};

export const initializeSessionThunk = (user: User) => async (dispatch: Dispatch, getState: () => AppState, clientPromise: Promise<ApolloClient<NormalizedCacheObject>>) => {
  return clientPromise.then(async (client) => {
    dispatch({ type: SessionActions.UPDATE_SESSION_LOADED, payload: false });
    try {
      const listItems = await getListItems(client);
      // console.log('listItems', listItems);
      const filteredItems = listItems.data.listItemList.rows.filter(
        (item) => (item.userId === null && (item.siteId === null || user!.accountMemberships.find((membership: any) => membership.id === user!.activeAccountMemberId)?.siteId)) || item.userId === user?.id,
      );
      const permissionValidator = new PermissionChecker(user);
      const hasPartRead = permissionValidator.match(Permissions.values.partRead);

      const markers = hasPartRead ? (await getMarkers(client))?.data?.markerList?.rows : null;
      const settingsList = (await getSettings(client)).data.settingList.rows;
      const settings: SettingState = { ...getState().session.settings };

      if (settingsList.length > 0) {
        for (let i = 0; i < settingsList.length; i++) {
          const element = settingsList[i];
          settings[element.name] = { ...element, metadata: element.metadata ? JSON.parse(element.metadata) : undefined };
        }
      }

      const organizations = hasPartRead ? (await getOrganizations(client))?.data : null;
      const userSettingsList = (await getUserSettings({ id: user.id }, client)).data.settingList.rows;
      let userSettings;

      if (userSettingsList) {
        const newSettings: SettingState = { ...getState().session.userSettings };
        const userSets = userSettingsList.filter((s) => s.userId === user.id);
        for (let index = 0; index < userSets.length; index += 1) {
          const element = userSettingsList[index];
          newSettings[element.name] = { ...element, metadata: element.metadata ? JSON.parse(element.metadata) : undefined };
        }
        if (!userSettingsList.find((ele) => ele.name === 'symbols')) {
          const symbolsData = { userId: user.id, name: 'symbols', metadata: JSON.stringify(symbolDefaults) };
          const settingCreate = await createSetting({ ...symbolsData }, client);
          if (settingCreate.data) {
            newSettings[settingCreate.data.settingCreate.name] = {
              ...settingCreate,
              metadata: settingCreate.data.settingCreate.metadata ? JSON.parse(settingCreate.data.settingCreate.metadata) : undefined,
            };
          }
        }
        userSettings = newSettings;
      }

      const finalOrganizeListItems = organizeListItems(filteredItems);

      dispatch({
        type: SessionActionTypes.UPDATE_SESSION_CONTEXT,
        payload: {
          ...finalOrganizeListItems,
          markers,
          settings,
          userSettings,
          organizations: { all: organizations?.organizationList?.rows || [], self: organizations?.self?.rows[0] || null },
          user,
          accountMembership: user.accountMemberships.find((membership: any) => membership.id === user.activeAccountMemberId),
          sessionLoaded: true,
        },
      });
    } catch (err) {
      log.error(err);
      dispatch({ type: SessionActions.UPDATE_SESSION_LOADED, payload: true });
    }
  });
};

export default SessionActions;
