import { createSelector } from 'reselect';
import { SessionState } from '../state.d';

const selectRaw = (state: { session: SessionState }) => state.session;

export const selectSettings = createSelector(
  [selectRaw], //
  (session) => session.settings,
);
