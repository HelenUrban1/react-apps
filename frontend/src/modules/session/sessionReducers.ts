import './sessionActions';
import { DefaultStylePresets, symbolDefaults } from 'view/global/defaults';
import { ListObject, ListObjectItem } from 'domain/setting/listsTypes';
import config from 'config';
import { SessionState, SessionAction } from '../state';

const initialData = {
  user: null,
  accountMembership: null,
  classifications: null,
  operations: null,
  methods: null,
  types: null,
  metricUnits: null,
  imperialUnits: null,
  filterList: {},
  markers: null,
  organizations: { all: [], self: undefined },
  newOrg: null,
  settings: {
    promptFont: { value: 'false', name: 'promptFont' },
    stylePresets: { value: '', metadata: DefaultStylePresets, name: 'stylePresets' },
    tolerancePresets: { value: '', metadata: {}, name: 'tolerancePresets' },
    autoballoonEnabled: { value: config.autoBallooning.enabled.toString(), name: 'autoballoonEnabled' },
  },
  userSettings: {
    symbols: { value: '', name: 'symbols', metadata: symbolDefaults },
  },
  sessionLoaded: false,
};

export default (state = initialData, { type, payload }: SessionAction): SessionState => {
  let subTypes: ListObject = {};
  switch (type) {
    // TODO huge update cases like this are an antipattern and should be decomposed via middleware or atomic calls
    case 'SESSION_UPDATE_SESSION_CONTEXT':
      return {
        ...state,
        ...payload,
      };
    case 'SESSION_UPDATE_USER':
      return {
        ...state,
        user: payload.user || state.user,
      };
    case 'SESSION_UPDATE_ACCOUNT_MEMBERSHIP':
      return {
        ...state,
        accountMembership: payload.accountMembership || state.accountMembership,
      };
    case 'SESSION_UPDATE_TYPES':
      if (payload.types) {
        Object.values(payload.types as ListObject).forEach((t: ListObjectItem) => {
          if (t.children) {
            subTypes = { ...subTypes, ...t.children };
          }
        });
      }
      return {
        ...state,
        types: payload.types || state.types,
        filterList: { ...state.filterList, notationType: payload.types || state.types, notationSubtype: subTypes },
      };
    case 'SESSION_UPDATE_METHODS':
      return {
        ...state,
        methods: payload.methods || state.methods,
        filterList: { ...state.filterList, inspectionMethod: payload.methods || state.methods },
      };
    case 'SESSION_UPDATE_CLASSIFICATIONS':
      return {
        ...state,
        classifications: payload.classifications || state.classifications,
        filterList: { ...state.filterList, criticality: payload.classifications || state.classifications },
      };
    case 'SESSION_UPDATE_OPERATIONS':
      return {
        ...state,
        operations: payload.operations || state.operations,
        filterList: { ...state.filterList, operation: payload.operations || state.operations },
      };
    case 'SESSION_UPDATE_METRIC':
      return {
        ...state,
        metricUnits: payload.metricUnits || state.metricUnits,
      };
    case 'SESSION_UPDATE_IMPERIAL':
      return {
        ...state,
        imperialUnits: payload.imperialUnits || state.imperialUnits,
      };
    case 'SESSION_UPDATE_LISTS':
      if (payload.types) {
        Object.values(payload.types as ListObject).forEach((t: ListObjectItem) => {
          if (t.children) {
            subTypes = { ...subTypes, ...t.children };
          }
        });
      }
      return {
        ...state,
        types: payload.types || state.types,
        methods: payload.methods || state.methods,
        classifications: payload.classifications || state.classifications,
        operations: payload.operations || state.operations,
        metricUnits: payload.metricUnits || state.metricUnits,
        imperialUnits: payload.imperialUnits || state.imperialUnits,
        filterList: {
          ...state.filterList,
          operation: payload.operations || state.operations,
          criticality: payload.classifications || state.classifications,
          inspectionMethod: payload.methods || state.methods,
          notationType: payload.types || state.types,
          notationSubtype: subTypes,
        },
      };
    case 'SESSION_UPDATE_MARKERS':
      return {
        ...state,
        markers: payload.markers || state.markers,
      };
    case 'SESSION_UPDATE_GLOBAL_SETTINGS':
      return {
        ...state,
        settings: payload.settings ? { ...state.settings, ...payload.settings } : { ...state.settings },
      };
    case 'SESSION_UPDATE_USER_SETTINGS':
      return {
        ...state,
        userSettings: payload.userSettings ? { ...payload.userSettings } : { ...state.userSettings },
      };
    case 'SESSION_UPDATE_ORGANIZATIONS':
      return {
        ...state,
        organizations: {
          all: payload.all || state.organizations.all,
          self: payload.self || state.organizations.self,
        },
      };
    case 'SESSION_UPDATE_NEW_ORG':
      return {
        ...state,
        newOrg: payload,
      };
    case 'SESSION_UPDATE_SESSION_LOADED':
      return {
        ...state,
        sessionLoaded: payload,
      };
    default:
      return state;
  }
};
