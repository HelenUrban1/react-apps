import { Notification, NotificationTableFilter, NotificationTableSorter } from 'domain/setting/notification/notificationTypes';
import { Dispatch } from 'redux';

const prefix = 'NOTIFICATION';

export const NotificationActions = {
  SET_NOTIFICATION: `${prefix}_SET_NOTIFICATION`,
  SET_SELECTED_NOTIFICATIONS: `${prefix}_SET_SELECTED_NOTIFICATIONS`,
  SET_TABLE_PAGE: `${prefix}_SET_TABLE_PAGE`,
  SET_TABLE_FILTER: `${prefix}_SET_TABLE_FILTER`,
  SET_TABLE_SORTER: `${prefix}_SET_TABLE_SORTER`,
  SET_UNREAD_COUNT: `${prefix}_SET_UNREAD_COUNT`,
  SET_UNREAD_NOTIFICATION_PREVIEW: `${prefix}_SET_UNREAD_NOTIFICATION_PREVIEW`,
};

export enum NotificationActionTypes {
  SET_NOTIFICATION = 'NOTIFICATION_SET_NOTIFICATION',
  SET_SELECTED_NOTIFICATIONS = 'NOTIFICATION_SET_SELECTED_NOTIFICATIONS',
  SET_TABLE_PAGE = 'NOTIFICATION_SET_TABLE_PAGE',
  SET_TABLE_FILTER = 'NOTIFICATION_SET_TABLE_FILTER',
  SET_TABLE_SORTER = 'NOTIFICATION_SET_TABLE_SORTER',
  SET_UNREAD_COUNT = 'NOTIFICATION_SET_UNREAD_COUNT',
  SET_UNREAD_NOTIFICATION_PREVIEW = 'NOTIFICATION_SET_UNREAD_NOTIFICATION_PREVIEW',
}

export const doSetNotification = (notification: Notification | null) => async (dispatch: Dispatch) => {
  dispatch({ type: NotificationActions.SET_NOTIFICATION, payload: notification });
};
export const doSetSelectedNotifications = (selectedNotifications: Notification[]) => async (dispatch: Dispatch) => {
  dispatch({ type: NotificationActions.SET_SELECTED_NOTIFICATIONS, payload: selectedNotifications });
};
export const doSetTablePage = (tablePage: number) => async (dispatch: Dispatch) => {
  dispatch({ type: NotificationActions.SET_TABLE_PAGE, payload: tablePage });
};
export const doSetTableFilter = (tableFilter: NotificationTableFilter) => async (dispatch: Dispatch) => {
  dispatch({ type: NotificationActions.SET_TABLE_FILTER, payload: tableFilter });
};
export const doSetTableSorter = (tableSorter: NotificationTableSorter) => async (dispatch: Dispatch) => {
  dispatch({ type: NotificationActions.SET_TABLE_SORTER, payload: tableSorter });
};
export const doSetUnreadCount = (unreadCount: number) => async (dispatch: Dispatch) => {
  dispatch({ type: NotificationActions.SET_UNREAD_COUNT, payload: unreadCount });
};
export const doSetUnreadNotificationPreview = (unreadNotificationPreview: Notification[]) => async (dispatch: Dispatch) => {
  dispatch({ type: NotificationActions.SET_UNREAD_NOTIFICATION_PREVIEW, payload: unreadNotificationPreview });
};
