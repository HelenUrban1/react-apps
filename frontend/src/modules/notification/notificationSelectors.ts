import { createSelector } from 'reselect';
import { NotificationState } from '../state.d';

const selectRaw = (state: { notification: NotificationState }) => state.notification;
