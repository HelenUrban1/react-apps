import { NotificationAction, NotificationState } from 'modules/state';

const initialState = {
  notification: null,
  selectedNotifications: [],
  tablePage: 1,
  tableFilter: {},
  tableSorter: null,
  unreadCount: 0,
  unreadNotificationPreview: [],
};

export default (state: NotificationState = initialState, { type, payload }: NotificationAction): NotificationState => {
  switch (type) {
    case 'NOTIFICATION_SET_NOTIFICATION':
      return { ...state, notification: payload };

    case 'NOTIFICATION_SET_SELECTED_NOTIFICATIONS':
      return { ...state, selectedNotifications: payload };

    case 'NOTIFICATION_SET_TABLE_PAGE':
      return { ...state, tablePage: payload };

    case 'NOTIFICATION_SET_TABLE_FILTER':
      return { ...state, tableFilter: payload };

    case 'NOTIFICATION_SET_TABLE_SORTER':
      return { ...state, tableSorter: payload };

    case 'NOTIFICATION_SET_UNREAD_COUNT':
      return { ...state, unreadCount: payload };

    case 'NOTIFICATION_SET_UNREAD_NOTIFICATION_PREVIEW':
      return { ...state, unreadNotificationPreview: payload };

    default:
      return state;
  }
};
