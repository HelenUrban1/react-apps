import list from 'modules/accountMember/list/accountMemberListReducers';
import form from 'modules/accountMember/form/accountMemberFormReducers';
import view from 'modules/accountMember/view/accountMemberViewReducers';
import destroy from 'modules/accountMember/destroy/accountMemberDestroyReducers';
import importerReducer from 'modules/accountMember/importer/accountMemberImporterReducers';
import { combineReducers } from 'redux';

export default combineReducers({
  list,
  form,
  view,
  destroy,
  importer: importerReducer,
});
