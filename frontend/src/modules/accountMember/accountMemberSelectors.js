import { createSelector } from 'reselect';
import authSelectors from 'modules/auth/authSelectors';
import PermissionChecker from 'modules/auth/permissionChecker';
import Permissions from 'security/permissions';

const selectPermissionToRead = createSelector([authSelectors.selectCurrentUser], (currentUser) => new PermissionChecker(currentUser).match(Permissions.values.accountMemberRead));

const selectPermissionToEdit = createSelector([authSelectors.selectCurrentUser], (currentUser) => new PermissionChecker(currentUser).match(Permissions.values.accountMemberEdit));

const selectPermissionToCreate = createSelector([authSelectors.selectCurrentUser], (currentUser) => new PermissionChecker(currentUser).match(Permissions.values.accountMemberCreate));

const selectPermissionToImport = createSelector([authSelectors.selectCurrentUser], (currentUser) => new PermissionChecker(currentUser).match(Permissions.values.accountMemberImport));

const selectPermissionToDestroy = createSelector([authSelectors.selectCurrentUser], (currentUser) => new PermissionChecker(currentUser).match(Permissions.values.accountMemberDestroy));

const selectors = {
  selectPermissionToRead,
  selectPermissionToEdit,
  selectPermissionToCreate,
  selectPermissionToDestroy,
  selectPermissionToImport,
};

export default selectors;
