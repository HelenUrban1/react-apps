import IdField from 'modules/shared/fields/idField';
import DateTimeField from 'modules/shared/fields/dateTimeField';
import DateTimeRangeField from 'modules/shared/fields/dateTimeRangeField';
import StringField from 'modules/shared/fields/stringField';
import EnumeratorField from 'modules/shared/fields/enumeratorField';
import RelationToOneField from 'modules/shared/fields/relationToOneField';
import RolesField from 'modules/shared/fields/rolesField';
import { i18n } from '../../i18n';

// The 'Owner' role is not included because it requires special handling
// (e.g., only the current Owner can assign someone else as the new Owner)
// and cannot be assigned by any admin user like the other roles can.
const ROLES = ['Admin', 'Billing', 'Collaborator', 'Viewer', 'Reviewer'];

function labelOfRole(role) {
  if (!ROLES.includes(role)) {
    return role;
  }

  return i18n(`roles.accountMemberRoles.${role}.label`);
}

function descriptionOfRole(role) {
  if (!ROLES.includes(role)) {
    return role;
  }

  return i18n(`roles.accountMemberRoles.${role}.description`);
}

function rolesSelectOptions() {
  return ROLES.map((role) => ({
    id: role,
    value: role,
    title: descriptionOfRole(role),
    label: labelOfRole(role),
  }));
}

function label(name) {
  return i18n(`entities.accountMember.fields.${name}`);
}

function enumeratorLabel(name, value) {
  return i18n(`entities.accountMember.enumerators.${name}.${value}`);
}

const fields = {
  id: new IdField('id', label('id')),
  account: new RelationToOneField('account', label('account'), {
    required: true,
  }),
  user: new RelationToOneField('user', label('user'), {
    required: true,
  }),
  settings: new StringField('settings', label('settings'), {
    required: true,
  }),
  roles: new RolesField('roles', label('roles'), {
    labelOfRole,
    rolesSelectOptions,
  }),
  status: new EnumeratorField(
    'status',
    label('status'),
    [
      { id: 'Active', label: enumeratorLabel('status', 'Active') },
      { id: 'Suspended', label: enumeratorLabel('status', 'Suspended') },
      { id: 'Archived', label: enumeratorLabel('status', 'Archived') },
    ],
    {
      required: true,
    },
  ),
  createdAt: new DateTimeField('createdAt', label('createdAt')),
  updatedAt: new DateTimeField('updatedAt', label('updatedAt')),
  createdAtRange: new DateTimeRangeField('createdAtRange', label('createdAtRange')),
};

export default {
  fields,
  ROLES,
  labelOfRole,
  descriptionOfRole,
  rolesSelectOptions,
};
