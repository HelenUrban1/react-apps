import model from 'modules/accountMember/accountMemberModel';

const { fields } = model;

export default [fields.account, fields.user, fields.settings, fields.roles, fields.status];
