import importerActions from 'modules/shared/importer/importerActions';
import selectors from 'modules/accountMember/importer/accountMemberImporterSelectors';
import AccountMemberService from 'modules/accountMember/accountMemberService';
import fields from 'modules/accountMember/importer/accountMemberImporterFields';
import { i18n } from '../../../i18n';

export default importerActions('ACCOUNTMEMBER_IMPORTER', selectors, AccountMemberService.import, fields, i18n('entities.accountMember.importer.fileName'));
