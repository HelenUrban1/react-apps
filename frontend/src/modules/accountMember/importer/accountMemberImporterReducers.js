import importerReducers from 'modules/shared/importer/importerReducers';
import actions from 'modules/accountMember/importer/accountMemberImporterActions';

export default importerReducers(actions);
