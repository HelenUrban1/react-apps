import gql from 'graphql-tag';
import getClient from 'modules/shared/graphql/graphqlClient';

export default class AccountMemberService {
  static async update(id, data) {
    const graphqlClient = await getClient();
    const response = await graphqlClient.mutate({
      mutation: gql`
        mutation ACCOUNTMEMBER_UPDATE($id: String!, $data: AccountMemberInput!) {
          accountMemberUpdate(id: $id, data: $data) {
            id
          }
        }
      `,

      variables: {
        id,
        data,
      },
    });

    return response.data.accountMemberUpdate;
  }

  static async destroyAll(ids) {
    const graphqlClient = await getClient();
    const response = await graphqlClient.mutate({
      mutation: gql`
        mutation ACCOUNTMEMBER_DESTROY($ids: [String!]!) {
          accountMemberDestroy(ids: $ids)
        }
      `,

      variables: {
        ids,
      },
    });

    return response.data.accountMemberDestroy;
  }

  static async create(data) {
    const graphqlClient = await getClient();
    const response = await graphqlClient.mutate({
      mutation: gql`
        mutation ACCOUNTMEMBER_CREATE($data: AccountMemberInput!) {
          accountMemberCreate(data: $data) {
            id
          }
        }
      `,

      variables: {
        data,
      },
    });

    return response.data.accountMemberCreate;
  }

  static async import(values, importHash) {
    const graphqlClient = await getClient();
    const response = await graphqlClient.mutate({
      mutation: gql`
        mutation ACCOUNTMEMBER_IMPORT($data: AccountMemberInput!, $importHash: String!) {
          accountMemberImport(data: $data, importHash: $importHash)
        }
      `,

      variables: {
        data: values,
        importHash,
      },
    });

    return response.data.accountMemberImport;
  }

  static async find(id) {
    const graphqlClient = await getClient();
    const response = await graphqlClient.query({
      query: gql`
        query ACCOUNTMEMBER_FIND($id: String!) {
          accountMemberFind(id: $id) {
            id
            account {
              id
              orgName
            }
            user {
              id
              fullName
              email
            }
            settings
            roles
            status
            createdAt
            updatedAt
          }
        }
      `,

      variables: {
        id,
      },
    });

    return response.data.accountMemberFind;
  }

  static async list(filter, orderBy, limit, offset) {
    const graphqlClient = await getClient();
    const response = await graphqlClient.query({
      query: gql`
        query ACCOUNTMEMBER_LIST($filter: AccountMemberFilterInput, $orderBy: AccountMemberOrderByEnum, $limit: Int, $offset: Int) {
          accountMemberList(filter: $filter, orderBy: $orderBy, limit: $limit, offset: $offset) {
            count
            rows {
              id
              account {
                id
                orgName
              }
              user {
                id
                fullName
                email
              }
              settings
              roles
              status
              updatedAt
              createdAt
            }
          }
        }
      `,

      variables: {
        filter,
        orderBy,
        limit,
        offset,
      },
    });

    return response.data.accountMemberList;
  }

  static async listAutocomplete(query, limit) {
    const graphqlClient = await getClient();
    const response = await graphqlClient.query({
      query: gql`
        query ACCOUNTMEMBER_AUTOCOMPLETE($query: String, $limit: Int) {
          accountMemberAutocomplete(query: $query, limit: $limit) {
            id
            label
          }
        }
      `,

      variables: {
        query,
        limit,
      },
    });

    return response.data.accountMemberAutocomplete;
  }
}
