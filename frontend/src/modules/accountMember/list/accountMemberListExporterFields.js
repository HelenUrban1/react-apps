import model from 'modules/accountMember/accountMemberModel';

const { fields } = model;

export default [fields.id, fields.account, fields.user, fields.settings, fields.roles, fields.status, fields.createdAt, fields.updatedAt];
