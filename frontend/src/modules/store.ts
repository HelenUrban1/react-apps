import { routerMiddleware } from 'connected-react-router';
import { createBrowserHistory } from 'history';
import { applyMiddleware, createStore, Store, compose } from 'redux';
import { createEpicMiddleware, combineEpics } from 'redux-observable';
import { throttle } from 'lodash';
import thunkMiddleware from 'redux-thunk';
import { saveState } from 'utils/localStorage';
import { refreshEndpoint } from 'config';
import createAuthRefreshInterceptor from 'axios-auth-refresh';
import initializers from './initializers';
import createRootReducer, { resetEnhancer } from './reducers';
import getClient from './shared/graphql/graphqlClient';
import reportEpics from './report/reportEpics';
import { updateTemplateContextEpic, saveTemplateEpic } from './template/templateEpics';
import { deleteCharacteristicsEpic, editCharacteristicsEpic, createCharacteristicsEpic, updateMarkersEpic } from './characteristic/characteristicEpics';
import { createMeasurementEpic } from './metro/metroEpics';
import axios from './shared/network/axios';

const history = createBrowserHistory();

let store: Store;

const rootEpic = combineEpics<any>(reportEpics, saveTemplateEpic, updateTemplateContextEpic, updateMarkersEpic, editCharacteristicsEpic, createCharacteristicsEpic, deleteCharacteristicsEpic, createMeasurementEpic);

declare global {
  interface Window {
    __REDUX_DEVTOOLS_EXTENSION_COMPOSE__?: any;
  }
}

const actionSanitizer = (action: any) => (action.type.includes('PART_') ? { ...action, payload: '<<LONG_BLOB>>' } : action);
export function configureStore() {
  const composeEnhancers =
    (window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ &&
      window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({
        actionSanitizer,
        stateSanitizer: (state: any) => ({ ...state, part: '<< CLEARING MEMORY >>', pdfView: '<< CLEARING MEMORY >>', partEditor: '<< CLEARING MEMORY >>' }),
        // trace: true,
        // traceLimit: 25,
      })) ||
    compose;

  const epicMiddleware = createEpicMiddleware({
    dependencies: {
      clientPromise: getClient(),
    },
  });

  const middlewares = applyMiddleware(...[thunkMiddleware.withExtraArgument(getClient()), routerMiddleware(history), epicMiddleware]);

  store = createStore(resetEnhancer(createRootReducer(history)), composeEnhancers(middlewares /* offline(offlineConfig) */));

  initializers.forEach((initializer) => {
    initializer(store);
  });

  epicMiddleware.run(rootEpic);

  store.subscribe(
    throttle(() => {
      saveState(store.getState());
    }),
  );

  return store;
}

export function getHistory() {
  return history;
}

export default function getStore() {
  return store;
}

const refreshLogic = (failedRequest: any) => {
  const { deviceKey } = getStore().getState().auth;
  const req = axios.post(refreshEndpoint, { deviceKey });
  return req.then((tokenRefreshResponse) => {
    if (tokenRefreshResponse?.data) {
      getStore().getState().auth.jwt = tokenRefreshResponse.data.authSignIn;
      failedRequest.response.config.headers.authorization = `Bearer ${tokenRefreshResponse.data.authSignIn}`;
      failedRequest.response.config.data = JSON.parse(failedRequest.response.config.data);
      return Promise.resolve();
    }
    return Promise.reject();
  });
};
createAuthRefreshInterceptor(axios, refreshLogic, { pauseInstanceWhileRefreshing: true });
