import { WebViewerInstance } from '@pdftron/webviewer';

const prefix = 'PDF_OVERLAY_VIEW';

export const PdfOverlayViewActions = {
  SET_STATE: `${prefix}_SET_STATE`,
  SET_TRON: `${prefix}_SET_TRON`,
  SET_TRON_OVERLAY: `${prefix}_SET_TRON_OVERLAY`,
  SET_ROTATION: `${prefix}_SET_ROTATION`,
  SET_ZOOM: `${prefix}_SET_ZOOM`,
  SET_INDEX: `${prefix}_SET_INDEX`,
  SET_DIMS: `${prefix}_SET_DIMS`,
  SET_FILE: `${prefix}_SET_FILE`,
  SET_LOADED: `${prefix}_SET_LOADED`,
  RESET: `${prefix}_RESET`,
};

export const setPdfOverlayViewState = (payload: any) => ({ type: PdfOverlayViewActions.SET_STATE, payload });
export const setPdfOverlayViewTronInstance = (payload: WebViewerInstance | null) => ({ type: PdfOverlayViewActions.SET_TRON, payload });
export const setPdfOverlayViewTronOverlayInstance = (payload: WebViewerInstance | null) => ({ type: PdfOverlayViewActions.SET_TRON_OVERLAY, payload });
export const setPdfOverlayViewRotation = (payload: number) => ({ type: PdfOverlayViewActions.SET_ROTATION, payload });
export const setPdfOverlayViewZoom = (payload: number) => ({ type: PdfOverlayViewActions.SET_ZOOM, payload });
export const setPdfOverlayViewIndex = (payload: number) => ({ type: PdfOverlayViewActions.SET_INDEX, payload });
export const setPdfOverlayViewDims = (payload: { height: number; width: number }) => ({ type: PdfOverlayViewActions.SET_DIMS, payload });
export const setPdfOverlayViewLoaded = (payload: boolean) => ({ type: PdfOverlayViewActions.SET_LOADED, payload });
export const setPdfOverlayViewFile = (payload: File) => ({ type: PdfOverlayViewActions.SET_FILE, payload });
export const resetPdfOverlayView = () => ({ type: PdfOverlayViewActions.RESET });
