import { PdfOverlayViewAction, PdfOverlayViewState } from 'modules/state';
import { CanvasLayerManager } from 'styleguide/PdfViewer/canvasLayerManager';

export const initialPdfOverlayViewState = {
  height: 0,
  width: 0,
  zoom: 0,
  index: 0,
  rotation: 0,
  layerManager: new CanvasLayerManager({ layers: ['grid', 'markers'] }),
  pages: [],
  tronInstance: null,
  tronOverlayInstance: null,
  overlayView: {
    height: 0,
    width: 0,
    zoom: 0,
    index: 0,
    rotation: 0,
  },
  revisionFile: null,
  revisionFiles: null, // used to store files for overlay view when coming from details drawer
  pdfLoaded: false,
};

export default (state: PdfOverlayViewState = initialPdfOverlayViewState, { type, payload }: PdfOverlayViewAction): PdfOverlayViewState => {
  switch (type) {
    case 'PDF_OVERLAY_VIEW_SET_STATE':
      return { ...state, ...payload };

    case 'PDF_OVERLAY_VIEW_SET_TRON':
      return { ...state, tronInstance: payload };

    case 'PDF_OVERLAY_VIEW_SET_ZOOM':
      return { ...state, zoom: payload };

    case 'PDF_OVERLAY_VIEW_SET_INDEX':
      return { ...state, index: payload };

    case 'PDF_OVERLAY_VIEW_SET_ROTATION':
      return { ...state, rotation: payload };

    case 'PDF_OVERLAY_VIEW_SET_DIMS':
      return { ...state, height: payload.height, width: payload.width };

    case 'PDF_OVERLAY_VIEW_SET_FILE':
      return { ...state, revisionFile: payload };

    case 'PDF_OVERLAY_VIEW_SET_LOADED':
      return { ...state, pdfLoaded: payload };

    case 'PDF_OVERLAY_VIEW_RESET':
      return { ...state, ...initialPdfOverlayViewState };

    default:
      return state;
  }
};
