import { PdfOverlayViewState } from '../state';

const selectRaw = (state: { pdfOverlayView: PdfOverlayViewState }) => state.pdfOverlayView;
