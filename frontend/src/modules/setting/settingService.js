import getClient from 'modules/shared/graphql/graphqlClient';
import gql from 'graphql-tag';
import AuthService from 'modules/auth/authService';

export default class SettingService {
  static async fetchAndApply() {
    let theme = null;
    try {
      theme = await this.find('theme');
    } catch (error) {
      AuthService.signout();
    }
  }

  static async find(name) {
    const graphqlClient = await getClient();
    const response = await graphqlClient.query({
      query: gql`
        query SETTING_FIND($name: String!) {
          settingFind(name: $name) {
            id
            name
            value
            metadata
            userId
            siteId
            createdAt
            updatedAt
          }
        }
      `,

      variables: {
        name,
      },
    });

    return response.data.settingFind;
  }

  static applyTheme(color) {
    let theme = color;
    if (!color) {
      theme = 'default';
    }

    const link = document.createElement('link');
    link.setAttribute('id', 'theme-link');
    link.setAttribute('rel', 'stylesheet');
    link.setAttribute('type', 'text/css');
    link.setAttribute('href', `${process.env.PUBLIC_URL}/theme/dist/${theme}.css`);

    // Theme needs to be updated so it doesn't overwrite valid styles,
    // removing until we add valid themes
    // if (oldLink) {
    //   document.getElementsByTagName('head').item(0).replaceChild(oldLink, link);
    // } else {
    //   document.getElementsByTagName('head').item(0).appendChild(link);
    // }
  }
}
