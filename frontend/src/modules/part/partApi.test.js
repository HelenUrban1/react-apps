import { createDrawingSheets } from './partApi';

describe('Part Utilities', () => {
  it('Can create drawing sheets', async () => {
    // Mocking what we need from PDFtron

    const doc = {
      getPageCount: jest.fn().mockReturnValue(Promise.resolve(2)),
      getPageInfo: jest.fn().mockReturnValue(
        Promise.resolve({
          height: 792,
          width: 1224,
        }),
      ),
      getPageRotation: jest.fn().mockReturnValue(Promise.resolve(0)),
    };

    global.Core = {
      setWorkerPath: jest.fn(),
      createDocument: () => {
        return doc;
      },
    };

    const file = [];
    const partId = '22f3ceb8-4dcd-4f9c-a7bf-2b52ec7f6147';
    const partDrawingId = 'c5d23876-41e1-4c8b-845b-b93a41188b5b';
    const defaultTolerances = { linear: 'linear', angular: 'angular' };
    const client = {
      mutate: jest.fn(),
    };
    await createDrawingSheets(file, partId, partDrawingId, defaultTolerances, client);

    expect(client.mutate).toBeCalledTimes(2);
    expect(doc.getPageInfo).toHaveBeenCalledWith(1);
    expect(doc.getPageRotation).toHaveBeenCalledWith(1);

    const createDrawingSheetCallParameters = client.mutate.mock.calls[0][0];
    const result = {
      data: {
        pageIndex: 0,
        pageName: 'Page 1',
        height: 792,
        width: 1224,
        rotation: `0`,
        grid: '{"initialCanvasWidth":1224,"initialCanvasHeight":792,"pageRotation":0,"lineWidth":5,"labelColor":"#963CBD","lineColor":"#963CBD","columnLeftLabel":"4","columnRightLabel":"1","rowTopLabel":"B","rowBottomLabel":"A","skipLabels":[],"showLabels":true,"rowLineRatios":[0,0.5,1],"rowLines":[20,394,768],"rowLabels":["B","A"],"colLineRatios":[0,0.25,0.5,0.75,1],"colLines":[31,320,609,898,1187],"colLabels":["4","3","2","1"]}',
        displayLines: true,
        displayLabels: true,
        part: partId,
        drawing: partDrawingId,
        defaultLinearTolerances: defaultTolerances.linear,
        defaultAngularTolerances: defaultTolerances.angular,
      },
    };
    expect(createDrawingSheetCallParameters.variables).toStrictEqual(result);
  });
});
