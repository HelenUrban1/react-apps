import { newPartDefaults, Part, PartChange, PartRevision, PartTableFilter, PartTableSorter, PartUpdateInput } from 'domain/part/partTypes';
import { ApolloClient, NormalizedCacheObject } from '@apollo/client';
import { Dispatch } from 'redux';
import log from 'modules/shared/logger';
import { PartEdit, PartEditVars, PartList, PartListVars, Parts } from 'domain/part/partApi';
import { NavigationActions, PartStepEnum } from 'modules/navigation/navigationActions';
import { AppState } from 'modules/state';
import { LoadedPresets } from 'domain/setting/settingTypes';
import FileUploader from 'modules/shared/fileUpload/fileUploader';
import message from 'antd/lib/message';
import Analytics from 'modules/shared/analytics/analytics';
import { v4 as uuid } from 'uuid';
import config from 'config';
import { Drawing, DrawingsAPI, DrawingList, DrawingListVars, reviseDrawingMutation } from 'graphql/drawing';
import { doSetSubscription } from 'modules/subscription/subscriptionActions';
import { i18n } from 'i18n';
import { getFileExtension, isPDF } from 'utils/files';
import { FileObject } from 'styleguide/File/Upload';
import { changeDrawingThunk, endDrawingAlignmentThunk, PartEditorActions } from 'modules/partEditor/partEditorActions';
import { File as FileType, FileInput } from 'types/file';
import { PdfViewActions } from 'modules/pdfView/pdfViewActions';
import { getHistory } from 'modules/store';
import moment from 'moment';
import { checkCurrentRevision } from 'utils/PartUtilities';
import { loadCharacteristicsThunk } from 'modules/characteristic/characteristicActions';
import { createDrawingMutation, createPartMutation, createDrawingSheets, findPartQuery, populateFullPart, updatePartMutation, revisePartMutation } from './partApi';
import { CharacteristicActions } from 'modules/characteristic/characteristicActions';

const prefix = 'PART';

interface File {
  name: string;
  sizeInBytes: number;
  publicUrl: string;
  privateUrl: string;
  new: boolean;
}

export const PartActions = {
  SET_PART_STATE: `${prefix}_SET_PART_STATE`,
  SET_PART: `${prefix}_SET_PART`,
  SET_REVISIONS: `${prefix}_SET_REVISIONS`,
  SET_REVISION_INDEX: `${prefix}_SET_REVISION_INDEX`,
  SET_PART_ERROR: `${prefix}_SET_ERROR`,
  SET_ORIGINAL_PART: `${prefix}_SET_ORIGINAL_PART`,
  SET_PREVIOUS_PART: `${prefix}_SET_PREVIOUS_PART`,
  SET_NEW_PART: `${prefix}_SET_NEW_PART`,
  SET_PARTS: `${prefix}_SET_PARTS`,
  SET_TABLE_PAGE: `${prefix}_SET_TABLE_PAGE`,
  SET_TABLE_FILTER: `${prefix}_SET_TABLE_FILTER`,
  SET_TABLE_SORTER: `${prefix}_SET_TABLE_SORTER`,
  SET_PART_LOADED: `${prefix}_SET_LOADED`,
  PART_RESET: `${prefix}_RESET`,
  RESET_TABLE: `${prefix}_RESET_TABLE`,
};

export enum PartActionTypes {
  SET_PART_STATE = 'PART_SET_PART_STATE',
  SET_PART = 'PART_SET_PART',
  SET_REVISIONS = 'PART_SET_REVISIONS',
  SET_REVISION_INDEX = 'PART_SET_REVISION_INDEX',
  SET_ORIGINAL_PART = `PART_SET_ORIGINAL_PART`,
  SET_PREVIOUS_PART = `PART_SET_PREVIOUS_PART`,
  SET_NEW_PART = 'PART_SET_NEW_PART',
  SET_PARTS = `PART_SET_PARTS`,
  SET_TABLE_PAGE = 'PART_SET_TABLE_PAGE',
  SET_TABLE_FILTER = 'PART_SET_TABLE_FILTER',
  SET_TABLE_SORTER = 'PART_SET_TABLE_SORTER',
  SET_PART_LOADED = 'PART_SET_LOADED',
  SET_PART_ERROR = 'PART_SET_ERROR',
  PART_RESET = 'PART_RESET',
  RESET_TABLE = 'PART_RESET_TABLE',
}

export const doSetPart = (part: Part | null, previousPart: Part | null) => async (dispatch: Dispatch, getState: () => AppState) => {
  const state = getState();
  const currentPart = state.part.part;
  if (!part || currentPart?.id !== part.id) {
    dispatch({ type: NavigationActions.RESET });
  }
  dispatch({ type: PartActions.SET_PART_STATE, payload: { part, originalPart: part, previousPart } });
};

export const doSetTablePage = (tablePage: number) => async (dispatch: Dispatch) => {
  dispatch({ type: PartActions.SET_TABLE_PAGE, payload: tablePage });
};
export const doSetTableFilter = (tableFilter: PartTableFilter) => async (dispatch: Dispatch) => {
  dispatch({ type: PartActions.SET_TABLE_FILTER, payload: tableFilter });
};
export const doSetTableSorter = (tableSorter: PartTableSorter | null) => async (dispatch: Dispatch) => {
  dispatch({ type: PartActions.SET_TABLE_SORTER, payload: tableSorter });
};

export const doDeletePart = (part: Part, refetchParts?: any, partListQueryParams?: any) => async (dispatch: any, getState: any, clientPromise: Promise<ApolloClient<any>>) => {
  return clientPromise.then(async (client) => {
    try {
      dispatch({ type: PartActions.SET_PART_LOADED, payload: false });

      // Delete Part
      log.debug(`Deleting Part ${part.name}...`);

      await client.mutate({
        mutation: Parts.mutate.delete,
        variables: {
          id: part.id,
        },
      });

      dispatch({ type: PartActions.PART_RESET });

      // Part deleted from the parts list page, refetch parts list to update table
      if (refetchParts && partListQueryParams) {
        refetchParts({ variables: { ...partListQueryParams }, fetchPolicy: 'no-cache' });
      }

      // Reload Subscription to update metrics
      const { account } = getState();
      if (account?.id) {
        log.debug(`Updating Subscription Metrics for ${account.id}...`);
        dispatch(doSetSubscription(account.id));
      }
      dispatch({ type: PartActions.SET_PART_LOADED, payload: true });
    } catch (error: any) {
      log.debug('Encountered error while deleting part');
      dispatch({ type: PartActions.SET_PART_ERROR, payload: error.message });
    }
  });
};

export const doGetPartList = (filter?: { status: string[]; searchTerm?: string; customer?: string[]; reviewStatus?: string[] }) => async (dispatch: Dispatch, getState: () => AppState, clientPromise: Promise<ApolloClient<any>>) => {
  return clientPromise.then(async (client) => {
    dispatch({ type: PartActions.SET_PART_LOADED, payload: false });
    const state = getState();
    const searchFilter = filter || state.part.tableFilter;
    const { data } = await client.query<PartList>({
      query: Parts.query.list,
      fetchPolicy: 'network-only',
      variables: {
        filter: searchFilter,
      },
    });
    if (data) {
      dispatch({ type: PartActions.SET_PART_STATE, payload: { ...state.part, parts: data.partList.rows, partLoaded: true } });
    } else {
      dispatch({ type: PartActions.SET_PART_LOADED, payload: true });
    }
  });
};

const getDefaultTolerances = (tolerances: LoadedPresets | undefined) => {
  const tols: { linear: null | string; angular: null | string } = { linear: null, angular: null };
  if (tolerances) {
    const tolerancesMeta = tolerances.metadata;
    if (tolerancesMeta.default && tolerancesMeta.default.setting) {
      tols.linear = JSON.stringify(tolerancesMeta.default.setting.linear);
      tols.angular = JSON.stringify(tolerancesMeta.default.setting.angular);
    }
  }
  return tols;
};

const getDefaultMarkerOptions = (styles: LoadedPresets | undefined) => {
  if (styles) {
    const presets = styles.metadata;
    if (presets.default && presets.default.setting) return JSON.stringify(presets.default.setting);
  }
  return null;
};

export const createNewPartThunk = (drawings: Drawing[], files: { [key: string]: any }) => async (dispatch: Dispatch, getState: () => AppState, clientPromise: Promise<ApolloClient<NormalizedCacheObject>>) => {
  return clientPromise.then(async (client) => {
    log.debug('Creating Part...');
    const state = getState();
    try {
      if (!drawings || drawings.length <= 0) {
        log.warn('No drawings created');
        dispatch({ type: PartActions.SET_PART_LOADED, payload: true });
        return;
      }

      // Create a temporary part object that the drawing will be assigned to
      const defaultTolerances = getDefaultTolerances(state.session.settings.tolerancePresets);
      const newPart = {
        ...newPartDefaults,
        defaultMarkerOptions: getDefaultMarkerOptions(state.session.settings.stylePresets),
        defaultLinearTolerances: defaultTolerances.linear,
        defaultAngularTolerances: defaultTolerances.angular,
        primaryDrawing: drawings[0].id,
        drawings: drawings.map((d) => d.id), // Array of drawing IDs
        name: drawings[0].file[0].name.replace(/[_-]+/g, ' ').replace(/\.PDF/gi, ''),
        revision: moment().local().format('YYYY-MM-DD'),
        autoMarkupStatus: files[drawings[0].id].ocr ? 'In_Progress' : null,
      };
      log.debug(`Creating ${newPart.name}...`);

      // Update the graphql cache so that subsequent requests can find it
      const { data: part } = await createPartMutation({ data: newPart }, client);
      let ocr = false;
      dispatch({ type: CharacteristicActions.UPDATE_EXTRACT_MODE, payload: { mode: 'Manual' } });

      if (part?.partCreate) {
        await Promise.all(
          drawings.map(async (drawing) => {
            // Added as tags when uploading to S3
            const additionalData = [
              {
                key: 'partId',
                value: part?.partCreate.id,
              },
              {
                key: 'drawingId',
                value: drawing.id,
              },
              {
                key: 'uniqueFileName',
                value: files[drawing.id].file.privateUrl,
              },
              {
                key: 'autoballoon',
                value: state.session.settings.autoballoonEnabled?.value === 'true' && files[drawing.id].ocr,
              },
            ];
            if (!ocr && state.session.settings.autoballoonEnabled?.value === 'true' && files[drawing.id].ocr) {
              ocr = true; // Toggles whether to show the wait for autoballooning modal on the list page
              dispatch({ type: CharacteristicActions.UPDATE_EXTRACT_MODE, payload: { mode: 'Auto' } });
            }
            log.debug(`Uploading ${files[drawing.id].fileName}...`);
            await FileUploader.uploadToServer(files[drawing.id].originFileObj, 'drawing/file', files[drawing.id].file.privateUrl, additionalData);
            await createDrawingSheets(files[drawing.id].originFileObj, part.partCreate.id, drawing.id, defaultTolerances, client);
            Analytics.track({
              event: Analytics.events.partDrawingUploaded,
              part: part.partCreate,
              drawing,
              properties: {
                'drawing.filename': files[drawing.id].fileName,
                multipleDocuments: drawings.length > 1,
              },
            });
          }),
        );
        const partFoundResult = (await findPartQuery({ id: part.partCreate.id }, client)).data;
        const { partFind, drawingList, reportList } = partFoundResult;

        const fullPart = await populateFullPart(partFind, drawingList.rows, reportList.rows, client);

        dispatch({
          type: PartActions.SET_PART_STATE,
          payload: {
            ...state.part,
            part: fullPart,
            originalPart: fullPart,
            previousPart: state.part,
            newPart: { ...fullPart, ocr },
            revisions: {
              [fullPart.id]: {
                id: fullPart.id,
                name: fullPart.name,
                number: fullPart.number,
                revision: part.revision || undefined,
                drawings: {},
                createdAt: fullPart.createdAt,
              },
            },
            partLoaded: true,
          },
        });
      } else {
        log.error('partCreateResult.data.partCreate did not exist');
      }
    } catch (err) {
      log.error(err);
      dispatch({ type: PartActions.SET_PART_STATE, payload: { partLoaded: true, partError: err } });
    }
  });
};

export const updatePartThunk = (partChanges: PartChange, id: string, targetPart?: Part) => async (dispatch: Dispatch, getState: () => AppState, clientPromise: Promise<ApolloClient<NormalizedCacheObject>>) => {
  return clientPromise.then(async (client) => {
    try {
      dispatch({ type: PartActions.SET_PART_LOADED, payload: false });
      const part = targetPart || getState().part.part;
      const { revisions } = getState().part;
      if (!part) {
        throw new Error('PEC Unable to update part, because part is null.');
      }

      // Convert the nested part object to the flattened version expected by the update endpoint
      const flattenedPart: any = {
        ...part,
        primaryDrawing: part.primaryDrawing.id,
        drawings: part.drawings ? part.drawings.map((r) => r.id) : [],
        // characteristics: part.characteristics!.map(r => r.id),
        reports: part.reports ? part.reports.map((r) => r.id) : [],
        customer: part.customer ? part.customer.id : null,
        ...partChanges,
      };

      if (!flattenedPart.type) {
        flattenedPart.type = 'Part';
      }

      // The update endpoint's PartInput type excludes these values
      delete flattenedPart.characteristics;
      delete flattenedPart.drawingName;
      delete flattenedPart.createdAt;
      delete flattenedPart.updatedAt;
      delete flattenedPart.id;

      // Apply the changes and make sure Type is correct
      const partInput: PartUpdateInput = { ...flattenedPart };

      const updatedPart = await updatePartMutation({ id, data: partInput }, client);
      if (part && updatedPart.data?.partUpdate && updatedPart.data.partUpdate.status !== 'Deleted') {
        const foundPart = await findPartQuery({ id }, client);
        let fullPart = foundPart.data.partFind;
        if (fullPart && foundPart.data.drawingList && foundPart.data.drawingList && foundPart.data.reportList) {
          fullPart = await populateFullPart(fullPart, foundPart.data.drawingList.rows, foundPart.data.reportList.rows, client);
        }
        const revs = { ...revisions };

        if (revs[part.id] && partChanges.revision) revs[part.id].revision = partChanges.revision;
        if (revs[part.id] && partChanges.name) revs[part.id].name = partChanges.name;

        dispatch({ type: PartActions.SET_PART_STATE, payload: { part: fullPart, originalPart: fullPart, partLoaded: true, revisions: revs } });
      } else {
        // maybe?
        dispatch({ type: PartActions.SET_PART_STATE, payload: { part: null, originalPart: null, previousPart: getState().part.part, partLoaded: true } });
      }
    } catch (err) {
      log.error('PEC updateResult.error', err);
      dispatch({ type: PartActions.SET_PART_STATE, payload: { partLoaded: true, partError: err } });
    }
  });
};

export const uploadAttachmentsThunk = (files: FileObject[]) => async (dispatch: Dispatch, getState: () => AppState, clientPromise: Promise<ApolloClient<NormalizedCacheObject>>) => {
  return clientPromise.then(async (client) => {
    try {
      dispatch({ type: PartActions.SET_PART_LOADED, payload: false });
      const state = getState();
      const { part } = state.part;

      if (!part || !files || files.length === 0) {
        throw new Error(`Missing data needed for attachment: Part ${!!part} Files ${!!files}`);
      }

      const uploadFiles: FileInput[] = [];

      // Loop through provided files
      await Promise.all(
        files.map(async (file) => {
          log.debug(`Creating ${file.fileName}...`);
          // Prepare metadata about the provided files
          const newFileName = `${uuid()}${getFileExtension(file.fileName)}`;
          uploadFiles.push({
            name: file.name,
            sizeInBytes: file.size,
            publicUrl: `${config.backendUrl}/download?privateUrl=${newFileName}`,
            privateUrl: newFileName,
            new: true,
          });

          // Added as tags when uploading to S3
          const additionalData = [
            {
              key: 'partId',
              value: part.id,
            },
            {
              key: 'uniqueFileName',
              value: newFileName,
            },
            {
              key: 'autoballoon',
              value: false, // Don't autoballoon non-PDF/non-drawing files
            },
            {
              key: 'fileType',
              value: getFileExtension(newFileName),
            },
          ];
          return FileUploader.uploadToServer(file.originFileObj, 'part/file', newFileName, additionalData);
        }),
      );

      const newAttachments = part.attachments ? [...part.attachments] : [];
      uploadFiles.forEach((file) => {
        newAttachments.push({ ...file, id: file.id || '' });
      });

      // Upate the Part to exclude removed attachments
      let updatedPart = { ...part };
      const partEditInput = Parts.createGraphqlInput({ ...part, attachments: newAttachments });
      if (partEditInput) {
        const edited = await client.mutate<PartEdit, PartEditVars>({
          mutation: Parts.mutate.edit,
          variables: {
            id: part.id,
            data: partEditInput || { ...part },
          },
        });
        if (edited.data?.partUpdate) updatedPart = { ...edited.data.partUpdate };
      }

      Analytics.track({
        event: Analytics.events.partAttachmentUploaded,
        part: updatedPart,
      });

      dispatch({ type: PartActions.SET_PART_STATE, payload: { part: updatedPart, originalPart: updatedPart, partLoaded: true } });
    } catch (err: any) {
      log.error(`uploadAttachmentsThunk error: ${err}`);
      message.error(err.message, 10);
      dispatch({ type: PartActions.SET_PART_STATE, payload: { partLoaded: true, partError: err } });
    }
  });
};

export const deleteAttachmentThunk = (file: FileType) => async (dispatch: Dispatch, getState: () => AppState, clientPromise: Promise<ApolloClient<NormalizedCacheObject>>) => {
  return clientPromise.then(async (client) => {
    try {
      const state = getState();
      const { part } = state.part;

      if (!part || !file || !file.id) {
        throw new Error(`Missing data needed to remove attachment: Part ${!!part} Files ${!!file}`);
      }

      const newAttachments = part.attachments ? [...part.attachments] : [];
      newAttachments.splice(
        newAttachments.findIndex((att) => att.id === file.id),
        1,
      );

      // Upate the Part to exclude removed attachments
      const partEditInput = Parts.createGraphqlInput({ ...part, attachments: newAttachments });
      if (partEditInput) {
        await client.mutate<PartEdit, PartEditVars>({
          mutation: Parts.mutate.edit,
          variables: {
            id: part.id,
            data: partEditInput || { ...part },
          },
        });
      }

      Analytics.track({
        event: Analytics.events.partAttachmentRemoved,
        part: { ...part, attachments: newAttachments },
      });

      dispatch({ type: PartActions.SET_PART_STATE, payload: { part: { ...part, attachments: newAttachments }, originalPart: { ...part, attachments: newAttachments }, partLoaded: true } });
    } catch (err: any) {
      log.error(`deleteAttachmentThunk error: ${err}`);
      message.error(err.message, 10);
      dispatch({ type: PartActions.SET_PART_STATE, payload: { partLoaded: true, partError: err } });
    }
  });
};

export const nextLetter = (letter: string) => {
  if (letter.toUpperCase() === 'Z') {
    return 'ZA';
  }
  return String.fromCharCode(letter.toUpperCase().charCodeAt(0) + 1);
};
export const guessNextRevision = (revision: string) => {
  if (!revision || moment(revision, 'YYYY-MM-DD', true).isValid()) {
    return moment().local().format('YYYY-MM-DD');
  }

  const letterSequence = new RegExp(/[A-Z]+$/g);
  const numberSequence = new RegExp(/[0-9]+$/g);
  if (letterSequence.test(revision)) {
    const current = revision.match(letterSequence);
    if (!current) {
      log.debug('no match found');
      return revision;
    }
    const next = nextLetter(current[0][current[0].length - 1]);
    return revision.substring(0, revision.length - 1) + next;
  }
  const current = revision.match(numberSequence);
  if (!current) {
    log.debug('no match found');
    return revision;
  }
  return revision.replace(current[0], '') + (parseFloat(current[0]) + 1).toString();
};

export const doAddDrawingRevisions = (drawing: Drawing, history?: any) => async (dispatch: Dispatch<any>, getState: () => AppState, clientPromise: Promise<ApolloClient<any>>) => {
  return clientPromise.then(async (client) => {
    try {
      dispatch({ type: PartActions.SET_PART_LOADED, payload: false });

      const { part: partStore, characteristics, partEditor } = getState();
      const { revisions, part } = partStore;

      if (!partStore || !part || !characteristics) {
        throw new Error(`Missing part data needed for drawing revision: Part ${!!part} Features ${!!characteristics}`);
      }

      Analytics.track({
        event: Analytics.events.partDrawingRevisionStarted,
        part,
        drawing,
      });

      const revisedDrawing = await reviseDrawingMutation({ drawingId: drawing.id, partId: part.id }, client);

      Analytics.track({
        event: Analytics.events.partDrawingRevisionCompleted,
        part,
        drawing: revisedDrawing.data?.drawingRevision,
      });

      const newRevision = { ...revisions };
      const updatedDrawing = revisedDrawing.data?.drawingRevision.drawing;

      if (!updatedDrawing) throw new Error('Error creating drawing revision');

      // Update Revisions
      newRevision[part.id] = {
        ...newRevision[part.id],
        drawings: {
          ...newRevision[part.id]?.drawings,
          [updatedDrawing.id]: {
            id: updatedDrawing.id,
            name: updatedDrawing.name,
            number: updatedDrawing.number,
            revision: updatedDrawing.revision,
            previousRevision: updatedDrawing.previousRevision,
            nextRevision: updatedDrawing.nextRevision,
            createdAt: updatedDrawing.createdAt,
          },
        },
      };

      const foundPart = await findPartQuery({ id: part.id }, client);
      let fullPart = { ...foundPart.data.partFind };
      if (fullPart && foundPart.data.drawingList && foundPart.data.reportList) {
        fullPart = await populateFullPart(fullPart, foundPart.data.drawingList.rows, foundPart.data.reportList.rows, client);
      }

      const { revisionIndex, revisionType } = checkCurrentRevision(fullPart, newRevision, updatedDrawing);

      // Update Redux
      dispatch({ type: PartActions.SET_PART_STATE, payload: { revisions: newRevision, part: fullPart, revisionIndex, revisionType } });
      dispatch({ type: PartEditorActions.SET_PART_EDITOR_DRAWING, payload: updatedDrawing || drawing });
      dispatch({ type: PartEditorActions.SET_PART_EDITOR_FOCUS, payload: '1 revision-input' });

      if (history) {
        dispatch({ type: PartEditorActions.SET_PART_EDITOR_STATE, payload: { targetDrawing: updatedDrawing.id } });
        history.push(`/parts/${part.id}`);
      } else {
        // changing drawing revisions while viewing part
        const redirectToDocs = true;
        if (partEditor.assignedStyles) dispatch(loadCharacteristicsThunk(fullPart.id, partEditor.assignedStyles));
        dispatch(endDrawingAlignmentThunk(redirectToDocs));
      }
    } catch (error) {
      log.debug(error);
      dispatch({ type: PartActions.SET_PART_ERROR, payload: i18n('errors.parts.drawingRevision') });
    }
    dispatch({ type: PartActions.SET_PART_LOADED, payload: true });
  });
};

export const uploadDrawingThunk =
  (files: FileObject[], shouldCreatePart: boolean, redirectToDocsStep = false, previousDrawing?: Drawing | null, history?: any) =>
  async (dispatch: Dispatch<any>, getState: () => AppState, clientPromise: Promise<ApolloClient<NormalizedCacheObject>>) => {
    return clientPromise.then(async (client) => {
      try {
        dispatch({ type: PartActions.SET_PART_LOADED, payload: false });
        const state = getState();
        const { part } = state.part;
        const newDrawings = part?.drawings ? [...part.drawings] : [];
        const uploadDrawings: Drawing[] = [];
        const uploadMap: { [key: string]: any } = {};

        // Loop through provided files
        await Promise.all(
          files.map(async (file) => {
            log.debug(`Creating ${file.fileName}...`);
            // Prepare metadata about the provided files
            const newFileName = `${uuid()}.pdf`;
            const uploadFile: File[] = [
              {
                name: file.name,
                sizeInBytes: file.size || 1,
                publicUrl: `${config.backendUrl}/download?privateUrl=${newFileName}`,
                privateUrl: newFileName,
                new: true,
              },
            ];
            if (isPDF(file.name)) {
              const defaultTolerances = getDefaultTolerances(state.session.settings.tolerancePresets);
              const drawingData: any = {
                file: uploadFile,
                name: previousDrawing?.name ? previousDrawing.name : file.name || newFileName,
                number: previousDrawing?.number,
                documentType: previousDrawing?.documentType || 'Drawing',
                status: previousDrawing?.status || 'Active',
                sheetCount: previousDrawing?.sheetCount || 1,
                linearUnit: previousDrawing?.linearUnit || 'in',
                angularUnit: previousDrawing?.angularUnit || 'deg',
                markerOptions: previousDrawing?.markerOptions || 'none',
                linearTolerances: previousDrawing?.linearTolerances || 'none',
                angularTolerances: previousDrawing?.angularTolerances || 'none',
                tags: previousDrawing?.tags || 'none',
                revision: previousDrawing ? guessNextRevision(previousDrawing.revision) : moment().local().format('YYYY-MM-DD'),
                previousRevision: previousDrawing?.id,
                originalDrawing: previousDrawing?.originalDrawing,
              };

              if (!shouldCreatePart && part?.id) {
                drawingData.part = part.id;
              }

              const drawingInput = DrawingsAPI.createGraphqlInput(drawingData);
              const { data: drawing } = await createDrawingMutation({ data: drawingInput }, client);
              if (drawing?.drawingCreate) {
                const createdDrawing = { ...drawing.drawingCreate };
                newDrawings.push(createdDrawing);
                if (!shouldCreatePart && part?.id) {
                  // Added as tags when uploading to S3
                  const additionalData = [
                    {
                      key: 'partId',
                      value: part.id,
                    },
                    {
                      key: 'drawingId',
                      value: drawing.drawingCreate.id,
                    },
                    {
                      key: 'uniqueFileName',
                      value: newFileName,
                    },
                    {
                      key: 'autoballoon',
                      value: config.autoBallooning.enabled && file.ocr, // Set on file upload
                    },
                  ];

                  log.debug(`Uploading ${file.fileName}...`);
                  await FileUploader.uploadToServer(file.originFileObj, 'drawing/file', newFileName, additionalData);
                  const newSheets = await createDrawingSheets(file.originFileObj, part.id, drawing.drawingCreate.id, defaultTolerances, client, previousDrawing);
                  Analytics.track({
                    event: Analytics.events.partDrawingUploaded,
                    part,
                    drawing,
                    properties: {
                      multipleDocuments: files.length > 1,
                    },
                  });
                  newDrawings[newDrawings.length - 1].sheets = newSheets.map((res) => res.data!.drawingSheetCreate);
                } else {
                  uploadDrawings.push(drawing.drawingCreate);
                  uploadMap[drawing.drawingCreate.id] = { ...file, file: uploadFile[0] };
                }
              }
            } else if (part?.id) {
              // Added as tags when uploading to S3
              const additionalData = [
                {
                  key: 'partId',
                  value: part.id,
                },
                {
                  key: 'uniqueFileName',
                  value: newFileName,
                },
                {
                  key: 'autoballoon',
                  value: false, // Don't autoballoon non-PDF/non-drawing files
                },
              ];
              // TODO: should non-drawing files go somewhere other than drawing/file?
              await FileUploader.uploadToServer(file.originFileObj, 'drawing/file', newFileName, additionalData);
            }
          }),
        );

        if (shouldCreatePart) {
          // create a new part
          dispatch(createNewPartThunk(uploadDrawings, uploadMap));
        } else if (previousDrawing) {
          // created a drawing revision from the part details drawer
          dispatch({ type: PdfViewActions.SET_INDEX, payload: 0 });
          dispatch(doAddDrawingRevisions(previousDrawing, history));
        } else if (part) {
          // Upate the Part to include the added Drawing
          const partEditInput = Parts.createGraphqlInput({ ...part, drawings: newDrawings });
          let updatedPart = { ...part, drawings: newDrawings };
          if (partEditInput) {
            const newPart = await client.mutate<PartEdit, PartEditVars>({
              mutation: Parts.mutate.edit,
              variables: {
                id: part.id,
                data: partEditInput || { ...part },
              },
            });
            updatedPart = { ...updatedPart, ...newPart.data?.partUpdate, drawings: newDrawings };
          }
          dispatch({ type: PartActions.SET_PART_STATE, payload: { ...state, part: updatedPart, originalPart: updatedPart, partLoaded: true } });
          const newDraw = updatedPart.drawings[updatedPart.drawings.length - 1];
          if (redirectToDocsStep && newDraw) {
            // new drawing was added during part wizard/extract
            dispatch(changeDrawingThunk(newDraw.id));
            dispatch({ type: NavigationActions.SET_CURRENT_PART_STEP, payload: PartStepEnum.DOCS });
          }
        }
      } catch (err: any) {
        log.error(`handlePartDrawingUpload error: ${err}`);
        message.error(err.message, 10);
        dispatch({ type: PartActions.SET_PART_STATE, payload: { partLoaded: true, partError: err } });
      }
    });
  };

export const deleteDrawingThunk = (drawing: Drawing) => async (dispatch: Dispatch, getState: () => AppState, clientPromise: Promise<ApolloClient<NormalizedCacheObject>>) => {
  return clientPromise.then(async (client) => {
    try {
      dispatch({ type: PartActions.SET_PART_LOADED, payload: false });
      const state = getState();
      const { part } = state.part;

      if (!part || !drawing || !part.drawings) {
        throw new Error(`Missing data needed to remove drawing: Part ${!!part} Drawing ${!!drawing} Part Drawings ${!!part?.drawings}`);
      }

      Analytics.track({
        event: Analytics.events.userClickedDeleteDrawing,
        part,
        drawing,
        // properties: {},
      });

      await client.mutate({
        mutation: DrawingsAPI.mutate.delete,
        variables: {
          ids: [drawing.id],
        },
        fetchPolicy: 'network-only',
      });
      const updatedPart = { ...part, drawings: [...part.drawings.filter((draw) => draw.id !== drawing.id)] };
      dispatch({ type: PartActions.SET_PART_STATE, payload: { ...state, part: updatedPart, originalPart: updatedPart, partLoaded: true } });
    } catch (err: any) {
      log.error(`handleDeleteDrawing error: ${err}`);
      message.error(err.message, 10);
      dispatch({ type: PartActions.SET_PART_STATE, payload: { partLoaded: true, partError: err } });
    }
  });
};

// Revisions
/* {
  [partId]: {
    id: partId,
    revision: partRevision,
    drawings: {
      [drawingId]: {
        id: drawingId,
        revision: drawingRevision
      }
    }
  }
} */

export const doGetPartRevisions = (originalId: string) => async (dispatch: Dispatch, getState: () => AppState, clientPromise: Promise<ApolloClient<any>>) => {
  return clientPromise.then(async (client) => {
    try {
      dispatch({ type: PartActions.SET_PART_LOADED, payload: false });
      const { data } = await client.query<PartList, PartListVars>({
        query: Parts.query.revisions,
        fetchPolicy: 'network-only',
        variables: {
          filter: {
            originalPart: originalId,
          },
        },
      });
      if (data) {
        const revisions: { [key: string]: PartRevision } = {};
        data.partList.rows.forEach((part) => {
          revisions[part.id] = {
            id: part.id,
            name: part.name,
            number: part.number,
            revision: part.revision || undefined,
            previousRevision: part.previousRevision,
            nextRevision: part.nextRevision,
            drawings: {},
            createdAt: part.createdAt,
          };
          if (part.drawings) {
            part.drawings.forEach((drawing) => {
              revisions[part.id].drawings[drawing.id] = {
                id: drawing.id,
                name: drawing.name,
                number: drawing.number,
                revision: drawing.revision,
                previousRevision: drawing.previousRevision,
                nextRevision: drawing.nextRevision,
                createdAt: drawing.createdAt,
              };
            });
          }
        });
        const state = getState();
        const { part } = state.part;
        const { partDrawing } = state.partEditor;
        const { revisionIndex, revisionType } = part ? checkCurrentRevision(part, revisions, partDrawing) : { revisionIndex: 0, revisionType: null };
        dispatch({ type: PartActions.SET_PART_STATE, payload: { revisions, revisionIndex, revisionType } });
      }
      dispatch({ type: PartActions.SET_PART_LOADED, payload: true });
    } catch (error) {
      log.debug(error);
      dispatch({ type: PartActions.SET_PART_ERROR, payload: i18n('errors.parts.revisionList') });
    }
  });
};

export const doGetPart = (partId: string) => async (dispatch: Dispatch<any>, getState: () => AppState, clientPromise: Promise<ApolloClient<any>>) => {
  return clientPromise.then(async (client) => {
    try {
      dispatch({ type: PartActions.SET_PART_LOADED, payload: false });
      const { part, revisions } = getState().part;
      const foundPart = await findPartQuery({ id: partId }, client);
      if (!foundPart.data) {
        throw new Error('No Data Found');
      }
      let fullPart = { ...foundPart.data.partFind };
      if (fullPart && foundPart.data.drawingList && foundPart.data.reportList) {
        fullPart = await populateFullPart(fullPart, foundPart.data.drawingList.rows, foundPart.data.reportList.rows, client);
      }
      dispatch({ type: PartActions.SET_PART_STATE, payload: { part: fullPart, originalPart: fullPart, previousPart: part, partLoaded: true } });

      if (fullPart.originalPart) {
        if (!Object.keys(revisions).includes(partId)) {
          dispatch(doGetPartRevisions(fullPart.originalPart));
        } else {
          const { revisionIndex, revisionType } = fullPart ? checkCurrentRevision(fullPart, revisions, null) : { revisionIndex: 0, revisionType: null };
          dispatch({ type: PartActions.SET_PART_STATE, payload: { revisions, revisionIndex, revisionType } });
        }
      }
      dispatch({ type: PartActions.SET_PART_LOADED, payload: true });
    } catch (error: any) {
      log.debug('Encountered error while fetching part');
      dispatch({ type: PartActions.SET_PART_ERROR, payload: error.message });
    }
  });
};

export const doLoadCurrentPartVersion =
  (resetPart = true) =>
  async (dispatch: Dispatch<any>, getState: () => AppState, clientPromise: Promise<ApolloClient<any>>) => {
    return clientPromise.then(async (client) => {
      try {
        dispatch({ type: PartActions.SET_PART_LOADED, payload: false });
        const state = getState();
        const { part, revisions, revisionType } = state.part;
        const { partDrawing } = state.partEditor;
        const { report, step, max } = state.reportTemplates;

        const history = getHistory();

        if (part && revisionType === 'part' && resetPart) {
          // go to most recent part
          const currentPart = Object.keys(revisions).find((r) => revisions[r].nextRevision === null);

          if (!part || !currentPart) throw new Error(`Missing Part ${!!part} or Revisions ${!!revisions}`);

          if (report?.id && step === max) {
            // try to load the most recent report
            const reports = part.reports ? [...part.reports] : [];
            reports.sort((a, b) => (moment(a.updatedAt).isAfter(moment(b.updatedAt)) ? -1 : 1));
            if (!reports[0] || reports[0].id === report.id) {
              // current part had no newer reports, just load the part
              history.push(`/parts/${currentPart}`);
            } else {
              // current part has reports, load part and most recent report
              history.push(`/parts/${currentPart}/reports/${reports[0].id}`);
            }
          } else if (history.location.pathname.includes(part.id)) {
            // in part editor, need to load current part
            history.push(`/parts/${currentPart}`);
          } else if (currentPart) {
            // jumping ahead from the part details panel, just load the part
            dispatch(doGetPart(currentPart));
          }
        } else if (part && part.drawings) {
          // already on current part, switch to most current drawing instead
          const currentDrawing = part.drawings.find((d) => d.nextRevision === null && d.originalDrawing === (partDrawing?.originalDrawing || part.primaryDrawing.originalDrawing));
          if (history.location.pathname.includes(part.id)) {
            if (currentDrawing) dispatch(changeDrawingThunk(currentDrawing.id));
          } else {
            dispatch({ type: PartActions.SET_PART_STATE, payload: { revisionIndex: 0, revisionType: null } });
          }
        }
      } catch (error: any) {
        log.debug('Encountered error while fetching part');
        dispatch({ type: PartActions.SET_PART_ERROR, payload: error.message });
      }
      dispatch({ type: PartActions.SET_PART_LOADED, payload: true });
    });
  };

export const doAddPartRevisions = (targetPart: Part, history: any) => async (dispatch: Dispatch, getState: any, clientPromise: Promise<ApolloClient<any>>) => {
  return clientPromise.then(async (client) => {
    try {
      dispatch({ type: PartActions.SET_PART_LOADED, payload: false });

      Analytics.track({
        event: Analytics.events.partRevisionStarted,
        part: targetPart,
      });

      const revisedPart = await revisePartMutation({ id: targetPart.id }, client);

      if (!revisedPart.data) throw new Error('Error creating part revision');

      Analytics.track({
        event: Analytics.events.partRevisionCompleted,
        part: revisedPart.data.partRevision.part,
      });

      const { part, drawings } = revisedPart.data.partRevision;

      // Save Revisions
      const { part: partStore }: AppState = getState();
      const { revisions } = partStore;
      const newRevision = { ...revisions };
      newRevision[part.id] = {
        id: part.id,
        name: part.name,
        number: part.number,
        revision: part.revision || undefined,
        drawings: {},
        createdAt: part.createdAt,
      };
      drawings.forEach((drawing) => {
        newRevision[part.id].drawings[drawing.id] = {
          id: drawing.id,
          name: drawing.name,
          number: drawing.number,
          revision: drawing.revision,
          createdAt: drawing.createdAt,
        };
      });

      const { revisionIndex, revisionType } = checkCurrentRevision(part, newRevision, part.primaryDrawing);

      dispatch({ type: PartActions.SET_PART_STATE, payload: { revisions: newRevision, revisionIndex, revisionType } });
      dispatch({ type: PartEditorActions.SET_PART_EDITOR_FOCUS, payload: '0 revision-input' });
      dispatch({ type: PartActions.SET_PART_LOADED, payload: true });

      history.push(`/parts/${part.id}`);
    } catch (error) {
      log.debug(error);
      dispatch({ type: PartActions.SET_PART_ERROR, payload: i18n('errors.parts.revision') });
      dispatch({ type: PartActions.SET_PART_LOADED, payload: true });
    }
  });
};

export const doGetDrawingRevisions = (originalId: string) => async (dispatch: Dispatch, getState: any, clientPromise: Promise<ApolloClient<any>>) => {
  return clientPromise.then(async (client) => {
    try {
      dispatch({ type: PartActions.SET_PART_LOADED, payload: false });
      const { data } = await client.query<DrawingList, DrawingListVars>({
        query: DrawingsAPI.query.revisions,
        // fetchPolicy: 'network-only',
        variables: {
          filter: {
            originalDrawing: originalId,
          },
        },
      });
      if (data) {
        const { part: partStore } = getState();
        const { revisions } = partStore;
        const newRevision = { ...revisions };
        data.drawingList.rows.forEach((drawing) => {
          newRevision[drawing.part.id] = {
            ...newRevision[drawing.part.id],
            drawings: {
              ...newRevision[drawing.part.id]?.drawings,
              [drawing.id]: {
                id: drawing.id,
                name: drawing.name,
                number: drawing.number,
                revision: drawing.revision,
                previousRevision: drawing.previousRevision,
                nextRevision: drawing.nextRevision,
                createdAt: drawing.createdAt,
              },
            },
          };
        });
        dispatch({ type: PartActions.SET_REVISIONS, payload: newRevision });
      }
      dispatch({ type: PartActions.SET_PART_LOADED, payload: true });
    } catch (error) {
      log.debug(error);
      dispatch({ type: PartActions.SET_PART_ERROR, payload: i18n('errors.parts.drawingRevisionList') });
    }
  });
};
