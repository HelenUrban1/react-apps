import reducer, { initialState } from './partReducer';
import { PartActions } from './partActions';

const part = {
  accessControl: 'None',
  balloonedAt: null,
  characteristics: [],
  completedAt: null,
  createdAt: '2021-08-25T14:08:02.457Z',
  customer: null,
  defaultAngularTolerances: null,
  defaultLinearTolerances: null,
  defaultMarkerOptions: null,
  displayLeaderLines: true,
  drawingNumber: null,
  drawingRevision: null,
  id: '3e40e49b-7464-4b4e-8169-b6fb2183c0ff',
  measurement: 'Imperial',
  name: 'lowerplate',
  notes: null,
  number: null,
  presets: null,
  primaryDrawing: {
    angularTolerances: 'none',
    angularUnit: 'deg',
    characteristics: [],
    file: [{ name: 'lowerplate.pdf', privateUrl: '9cbabeb9-fecc-4022-8715-6bc094d915d6.pdf', publicUrl: 'https://ixc-local.com:8080/api/download?privateUrl=9cbabeb9-fecc-4022-8715-6bc094d915d6.pdf' }],
    id: '17ecafa1-44d1-4d0f-aeb2-17f2b260e1f9',
    linearTolerances: 'none',
    linearUnit: 'in',
    markerOptions: 'none',
    name: 'lowerplate.pdf',
    number: null,
    revision: null,
    sheetCount: 1,
    sheets: [],
    status: 'Active',
    tags: 'none',
  },
  purchaseOrderCode: null,
  purchaseOrderLink: null,
  reportGeneratedAt: null,
  reviewStatus: 'Unverified',
  revision: null,
  status: 'Active',
  tags: null,
  type: 'Part',
  updatedAt: '2021-08-25T14:08:02.462Z',
  workflowStage: 0,
  drawings: [
    {
      angularTolerances: 'none',
      angularUnit: 'deg',
      characteristics: [],
      file: [{ name: 'lowerplate.pdf', privateUrl: '9cbabeb9-fecc-4022-8715-6bc094d915d6.pdf', publicUrl: 'https://ixc-local.com:8080/api/download?privateUrl=9cbabeb9-fecc-4022-8715-6bc094d915d6.pdf' }],
      id: '17ecafa1-44d1-4d0f-aeb2-17f2b260e1f9',
      linearTolerances: 'none',
      linearUnit: 'in',
      markerOptions: 'none',
      name: 'lowerplate.pdf',
      number: null,
      revision: null,
      sheetCount: 1,
      sheets: [],

      status: 'Active',
      tags: 'none',
    },
  ],

  reports: [],
};

describe('Part Reducer', () => {
  it('should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual(initialState);
  });

  it('should reset to the initial state', () => {
    expect(reducer(initialState, { type: PartActions.PART_RESET })).toEqual(initialState);
  });

  it('should set error', () => {
    expect(reducer(initialState, { type: PartActions.SET_PART_ERROR, payload: 'Some Error' })).toEqual({ ...initialState, partError: 'Some Error' });
  });

  it('should set part', () => {
    expect(reducer(initialState, { type: PartActions.SET_PART, payload: part })).toEqual({ ...initialState, part });
  });

  it('should set table page', () => {
    expect(reducer(initialState, { type: PartActions.SET_TABLE_PAGE, payload: 3 })).toEqual({ ...initialState, tablePage: 3 });
  });

  it('should set table filter', () => {
    expect(reducer(initialState, { type: PartActions.SET_TABLE_FILTER, payload: { customer: ['443f13d8-e72b-460a-b2cb-873245bb386a'], reviewStatus: [], accessControl: [] } })).toEqual({
      ...initialState,
      tableFilter: { customer: ['443f13d8-e72b-460a-b2cb-873245bb386a'], reviewStatus: [], accessControl: [] },
    });
  });

  it('should set table sorter', () => {
    expect(reducer(initialState, { type: PartActions.SET_TABLE_SORTER, payload: { columnKey: 'customer', order: 'ascend' } })).toEqual({ ...initialState, tableSorter: { columnKey: 'customer', order: 'ascend' } });
  });
});
