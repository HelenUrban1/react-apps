import { createSelector } from 'reselect';
import { PartState } from '../state.d';

const selectRaw = (state: { part: PartState }) => state.part;
