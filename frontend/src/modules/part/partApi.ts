import { ApolloClient, gql, NormalizedCacheObject } from '@apollo/client';
import { PartFind, PartList, PartRevisionBE, Parts } from 'domain/part/partApi';
import { Part, PartUpdateInput } from 'domain/part/partTypes';
import { ReportPartial } from 'types/reportTemplates';
import { Drawing, DrawingFind, DrawingInput, DrawingsAPI } from 'graphql/drawing';
import { DrawingSheetCreate, DrawingSheetCreateVars, DrawingSheets } from 'graphql/drawing_sheet';
import getClient from 'modules/shared/graphql/graphqlClient';
import config from 'config';
import { FileObject } from 'styleguide/File/Upload';
import { Grid } from '../../domain/part/edit/grid/Grid';
import 'core-js/features/global-this'; // polyfill for running tests on node 10

export const steps = {
  get: async () => {
    const graphqlClient = await getClient();
    return graphqlClient.readQuery({
      query: Parts.query.step,
    });
  },
  set: async (current?: number, max?: number) => {
    const graphqlClient = await getClient();
    graphqlClient.writeFragment({
      id: 'partStep',
      fragment: gql`
        fragment PartSteps on Part {
          steps {
            currentStep
            maxStep
          }
        }
      `,
      data: {
        currentStep: current || 0,
        maxStep: max || 0,
      },
    });
  },
  current: async (step?: number) => {
    const graphqlClient = await getClient();
    graphqlClient.writeFragment({
      id: 'partStep',
      fragment: gql`
        fragment PartSteps on Part {
          steps {
            currentStep
            maxStep
          }
        }
      `,
      data: {
        currentStep: step,
      },
    });
  },
  max: async (step?: number) => {
    const graphqlClient = await getClient();
    graphqlClient.writeFragment({
      id: 'partStep',
      fragment: gql`
        fragment PartSteps on Part {
          steps {
            currentStep
            maxStep
          }
        }
      `,
      data: {
        maxStep: step,
      },
    });
  },
};

export const findPartQuery = async (queryObject: { id: string }, client: ApolloClient<NormalizedCacheObject>) => {
  return client.query<PartFind>({
    query: Parts.query.find,
    variables: queryObject,
    fetchPolicy: 'network-only',
  });
};

export const revisePartMutation = async (queryObject: { id: string }, client: ApolloClient<NormalizedCacheObject>) => {
  return client.mutate<PartRevisionBE>({
    mutation: Parts.mutate.revise,
    variables: queryObject,
    fetchPolicy: 'network-only',
  });
};

export const findPartListQuery = async (
  queryObject: {
    filter: { status: string[]; searchTerm?: string; customer?: string[]; reviewStatus?: string[] };
    orderBy: string;
    limit: number;
    offset: number;
  },
  client: ApolloClient<NormalizedCacheObject>,
) => {
  return client.query<PartList>({
    query: Parts.query.list,
    variables: queryObject,
    fetchPolicy: 'network-only',
  });
};

export const findDrawingQuery = async (queryObject: { id: string }, client: ApolloClient<NormalizedCacheObject>) => {
  return client.query<DrawingFind>({
    query: DrawingsAPI.query.find,
    variables: queryObject,
    fetchPolicy: 'network-only',
  });
};

export const populateFullPart = async (part: Part, drawingList: Drawing[], reportList: ReportPartial[], client: ApolloClient<NormalizedCacheObject>) => {
  const fullPart = { ...part };
  const drawings: Drawing[] = [...drawingList];
  const reports: ReportPartial[] = [...reportList];
  steps.set(part.workflowStage, part.workflowStage);
  let drawing;
  if (drawingList.length === 0 && part.primaryDrawing.id) {
    drawing = (await findDrawingQuery({ id: part.primaryDrawing.id }, client))?.data?.drawingFind;
  } else {
    for (let i = 0; i < drawings.length; i++) {
      if (drawings[i].id === part.primaryDrawing.id) {
        drawing = drawings[i];
      }
    }
  }

  if (drawing) {
    fullPart.primaryDrawing = { ...drawing };
  } else if (drawings[0]) {
    fullPart.primaryDrawing = { ...drawings[0] };
  }
  fullPart.drawings = drawings;
  fullPart.reports = reports;

  return fullPart;
};

export const updatePartMutation = async (queryObject: { id: string; data: PartUpdateInput }, client: ApolloClient<NormalizedCacheObject>) => {
  return client.mutate({
    mutation: Parts.mutate.edit,
    variables: queryObject,
    refetchQueries: [
      {
        query: Parts.query.find,
        variables: {
          id: queryObject.id,
        },
      },
      {
        query: Parts.query.list,
        variables: {
          filter: { status: ['Active', 'Inactive'] },
          orderBy: 'createdAt_DESC',
          limit: 10,
          offset: 0,
        },
      },
    ],
  });
};

export const createPartMutation = async (queryObject: { data: any }, client: ApolloClient<NormalizedCacheObject>) => {
  return client.mutate({
    mutation: Parts.mutate.create,
    variables: queryObject,
    refetchQueries: [
      {
        query: Parts.query.list,
        variables: {
          filter: { status: ['Active', 'Inactive'] },
          orderBy: 'createdAt_DESC',
          limit: 10,
          offset: 0,
        },
      },
    ],
  });
};

export const createDrawingMutation = async (queryObject: { data: DrawingInput }, client: ApolloClient<NormalizedCacheObject>) => {
  return client.mutate({
    mutation: DrawingsAPI.mutate.create,
    variables: { data: queryObject.data },
    refetchQueries: [
      {
        query: Parts.query.list,
        variables: {
          filter: { status: ['Active', 'Inactive'] },
          orderBy: 'createdAt_DESC',
          limit: 10,
          offset: 0,
        },
      },
    ],
  });
};

export const createDrawingSheetMutation = async (queryObject: { data: any }, client: ApolloClient<NormalizedCacheObject>) => {
  return client.mutate<DrawingSheetCreate, DrawingSheetCreateVars>({
    mutation: DrawingSheets.mutate.create,
    variables: { data: queryObject.data },
  });
};

export const createDrawingSheets = async (
  file: File | Blob | undefined,
  partId: string,
  partDrawingId: string,
  defaultTolerances: {
    linear: null | string;
    angular: null | string;
  },
  client: ApolloClient<NormalizedCacheObject>,
  drawing?: Drawing | null,
) => {
  // Core is imported in index.html from pdftronlib
  const coreControls = (globalThis as any).Core || (globalThis as any).CoreControls;
  coreControls.setWorkerPath('/pdftronlib/core');
  const doc = await coreControls.createDocument(file, {
    l: config.pdfTronKey,
  });

  const pageCount = await doc.getPageCount();

  const createDrawingSheetTasks = [];
  for (let index = 0; index < pageCount; index++) {
    const pageNumber = index + 1;
    // eslint-disable-next-line no-await-in-loop
    const page = await doc.getPageInfo(pageNumber);
    // eslint-disable-next-line no-await-in-loop
    const rotation = await doc.getPageRotation(pageNumber);
    const initialGrid = new Grid({
      initialCanvasWidth: rotation % 180 === 0 ? page.width : page.height,
      initialCanvasHeight: rotation % 180 === 0 ? page.height : page.width,
      initialPageRotation: rotation,
    });
    const prev = drawing?.sheets.find((s) => s.pageIndex === index);
    const data =
      drawing && prev
        ? {
            // creating a drawing revision
            ...DrawingSheets.createGraphqlInput(prev),
            height: page.height,
            width: page.width,
            rotation: `${rotation}`,
            part: partId,
            drawing: partDrawingId,
            previousRevision: prev.id,
          }
        : {
            // new drawing or revised drawing has more sheets than the original
            pageIndex: index,
            pageName: `Page ${pageNumber}`,
            height: page.height,
            width: page.width,
            rotation: `${rotation}`,
            grid: initialGrid.toJsonString(),
            displayLines: true,
            displayLabels: true,
            part: partId,
            drawing: partDrawingId,
            defaultLinearTolerances: defaultTolerances.linear,
            defaultAngularTolerances: defaultTolerances.angular,
          };
    createDrawingSheetTasks.push(createDrawingSheetMutation({ data }, client));
  }
  return Promise.all(createDrawingSheetTasks);
};

export const getFileDimensions = async (file: FileObject, jwt: string) => {
  const dims = { w: 0, h: 0 };

  try {
    // Core is imported in index.html from pdftronlib
    const coreControls = (globalThis as any).Core || (globalThis as any).CoreControls;
    coreControls.setWorkerPath('/pdftronlib/core');
    const doc = await coreControls.createDocument(file.originFileObj, {
      extension: 'pdf',
      licenseKey: config.pdfTronKey,
      customHeaders: {
        authorization: jwt ? `Bearer ${jwt}` : '',
      },
    });

    const page = await doc.getPageInfo(1);
    dims.w = page.width;
    dims.h = page.height;
  } catch (err) {
    console.error(err);
  }
  return dims;
};

export const getDrawingDimensions = async (drawing: Drawing, jwt: string) => {
  const dims = { w: 0, h: 0 };

  try {
    // Core is imported in index.html from pdftronlib
    const coreControls = (globalThis as any).Core || (globalThis as any).CoreControls;
    coreControls.setWorkerPath('/pdftronlib/core');
    const doc = await coreControls.createDocument(drawing.drawingFile.publicUrl, {
      extension: 'pdf',
      licenseKey: config.pdfTronKey,
      customHeaders: {
        authorization: jwt ? `Bearer ${jwt}` : '',
      },
    });
    const page = await doc.getPageInfo(1);
    dims.w = page.width;
    dims.h = page.height;
  } catch (err) {
    console.error(err);
  }

  return dims;
};

export const hasMatchingDimensionsFromFile = async (drawing: Drawing, revisionFile: FileObject, jwt: string) => {
  try {
    const currentDimensions = await getDrawingDimensions(drawing, jwt);
    const newDimensions = await getFileDimensions(revisionFile, jwt);

    if (currentDimensions.w !== newDimensions.w || currentDimensions.h !== newDimensions.h) return false;
  } catch (err) {
    console.error(err);
    return false;
  }
  return true;
};
