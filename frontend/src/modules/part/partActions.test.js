import configureStore from 'redux-mock-store';
import { routerMiddleware } from 'connected-react-router';
import thunkMiddleware from 'redux-thunk';
import moment from 'moment';
import { createEpicMiddleware } from 'redux-observable';
import { createMockClient } from 'mock-apollo-client';
import { createBrowserHistory } from 'history';
import { i18n } from 'i18n';
import { Parts } from 'domain/part/partApi';
import { DrawingsAPI } from 'graphql/drawing';
import { Characteristics } from 'graphql/characteristics';
import { DrawingSheets } from 'graphql/drawing_sheet';
import FileUploader from 'modules/shared/fileUpload/fileUploader';
import * as PartEditorActionCreators from 'modules/partEditor/partEditorActions';
import { part, drawing, revision, drawingRevision, sheetRevision, featureRevision, attachmentInput, attachment, feature } from './fixtures';
import { initialState } from './partReducer';
import * as PartActionCreators from './partActions';

// Start Mock Store
const history = createBrowserHistory();
const mockClient = createMockClient();

// Graphql Handlers
const queryHandler = jest.fn().mockResolvedValue({ data: { partFind: part, reportList: { count: 0, rows: [] }, drawingList: { count: 1, rows: [drawing] } } });
const queryDrawingHandler = jest.fn().mockResolvedValue({ data: { drawingFind: drawing } });
const mutationDrawingHandler = jest.fn().mockResolvedValue({ data: { drawingUpdate: { ...drawing, status: 'Deleted' } } });
const queryDrawingRevisionsHandler = jest.fn();
const mutationDrawingRevisionHandler = jest.fn().mockResolvedValue({ data: { part: {}, drawing: {}, sheets: [], characteristics: [] } });
mockClient.setRequestHandler(Parts.query.find, queryHandler);
mockClient.setRequestHandler(DrawingsAPI.query.find, queryDrawingHandler);
mockClient.setRequestHandler(DrawingsAPI.mutate.edit, mutationDrawingHandler);
mockClient.setRequestHandler(DrawingsAPI.mutate.revise, mutationDrawingRevisionHandler);
mockClient.setRequestHandler(DrawingsAPI.query.revisions, queryDrawingRevisionsHandler);

const mutationHandler = jest.fn();
const queryRevisionsHandler = jest.fn();
const mutationPartRevisionHandler = jest.fn().mockResolvedValue({ data: { part: {}, drawings: [], sheets: [], characteristics: [] } });
mockClient.setRequestHandler(Parts.mutate.edit, mutationHandler);
mockClient.setRequestHandler(Parts.mutate.delete, mutationHandler);
mockClient.setRequestHandler(Parts.query.revisions, queryRevisionsHandler);
mockClient.setRequestHandler(Parts.mutate.revise, mutationPartRevisionHandler);

// Copy Mocks
const copyHandler = jest.fn();
mockClient.setRequestHandler(DrawingsAPI.mutate.create, copyHandler);
mockClient.setRequestHandler(Characteristics.mutate.create, copyHandler);
mockClient.setRequestHandler(DrawingSheets.mutate.create, copyHandler);

// Mock Uploader
const mockUploader = jest.spyOn(FileUploader, 'uploadToServer').mockImplementation(() => {});
// Init Mocks
const getClient = async () => {
  return mockClient;
};
const epicMiddleware = createEpicMiddleware({
  dependencies: {
    clientPromise: getClient(),
  },
});
const middlewares = [thunkMiddleware.withExtraArgument(getClient()), routerMiddleware(history), epicMiddleware];
const mockStore = configureStore(middlewares);

describe('Part Actions', () => {
  // Note: The mock store doesn't call the reducer, so unless mutated the state should not change between tests
  const store = mockStore({ part: { ...initialState, part }, characteristics: { characteristics: [feature] }, partEditor: { partDrawing: drawing } });
  beforeEach(() => {
    jest.spyOn(console, 'error');
    // ignore those invariant errors
    console.error.mockImplementation(() => null);
    // reset store actions
    store.clearActions();
  });

  afterEach(() => {
    // restore console
    console.error.mockRestore();
  });

  describe('General', () => {
    it('should set the part', () => {
      // Return the promise
      return store.dispatch(PartActionCreators.doSetPart(part, null)).then(() => {
        const actions = store.getActions();
        expect(actions[0]).toEqual({ type: PartActionCreators.PartActions.SET_PART_STATE, payload: { part, originalPart: part, previousPart: null } });
        store.clearActions();
      });
    });

    it('should set the table page', () => {
      // Return the promise
      return store.dispatch(PartActionCreators.doSetTablePage(3)).then(() => {
        const actions = store.getActions();
        expect(actions[0]).toEqual({ type: PartActionCreators.PartActions.SET_TABLE_PAGE, payload: 3 });
        store.clearActions();
      });
    });

    it('should set the table sorter', () => {
      // Return the promise
      const tableSorter = { columnKey: 'customer', order: 'ascend' };
      return store.dispatch(PartActionCreators.doSetTableSorter(tableSorter)).then(() => {
        const actions = store.getActions();
        expect(actions[0]).toEqual({ type: PartActionCreators.PartActions.SET_TABLE_SORTER, payload: tableSorter });
        store.clearActions();
      });
    });

    it('should set the table filter', () => {
      // Return the promise
      const tableFilter = { customer: ['443f13d8-e72b-460a-b2cb-873245bb386a'], reviewStatus: [], accessControl: [] };
      return store.dispatch(PartActionCreators.doSetTableFilter(tableFilter)).then(() => {
        const actions = store.getActions();
        expect(actions[0]).toEqual({ type: PartActionCreators.PartActions.SET_TABLE_FILTER, payload: tableFilter });
        store.clearActions();
      });
    });
  });

  describe('CRUD', () => {
    it('should get the part', () => {
      queryHandler.mockClear();
      queryHandler.mockResolvedValue({ data: { partFind: part, reportList: { count: 0, rows: [] }, drawingList: { count: 1, rows: [drawing] } } });
      // Return the promise
      return store.dispatch(PartActionCreators.doGetPart(part.id)).then(() => {
        const actions = store.getActions();
        expect(queryHandler).toBeCalledTimes(1);
        expect(queryHandler).toBeCalledWith({ id: part.id });
        expect(actions[0]).toEqual({ type: PartActionCreators.PartActions.SET_PART_LOADED, payload: false });
        expect(actions[1]).toEqual({ type: PartActionCreators.PartActions.SET_PART_STATE, payload: { part, originalPart: part, previousPart: part, partLoaded: true } });
        expect(actions[2]).toEqual({ type: PartActionCreators.PartActions.SET_PART_LOADED, payload: true });
        store.clearActions();
      });
    });

    it('should log any errors when trying to delete the part', () => {
      store.clearActions();
      // Return the promise
      mutationHandler.mockRejectedValueOnce(new Error('Some Error'));
      return store.dispatch(PartActionCreators.doDeletePart(part)).then(() => {
        const actions = store.getActions();
        expect(mutationHandler).toBeCalledTimes(1);
        expect(mutationHandler).toBeCalledWith({ id: part.id });
        expect(actions[0]).toEqual({ type: PartActionCreators.PartActions.SET_PART_LOADED, payload: false });
        expect(actions[1]).toEqual({ type: PartActionCreators.PartActions.SET_PART_ERROR, payload: 'Some Error' });
        store.clearActions();
      });
    });

    it('should delete the part', () => {
      store.clearActions();
      // Return the promise
      mutationHandler.mockResolvedValueOnce({ data: { partDelete: { id: part.id } } });
      return store.dispatch(PartActionCreators.doDeletePart(part)).then(() => {
        const actions = store.getActions();
        expect(mutationHandler).toBeCalledTimes(1);
        expect(mutationHandler).toBeCalledWith({ id: part.id });
        expect(actions[0]).toEqual({ type: PartActionCreators.PartActions.SET_PART_LOADED, payload: false });
        expect(actions[1]).toEqual({ type: PartActionCreators.PartActions.PART_RESET });
        expect(actions[2]).toEqual({ type: PartActionCreators.PartActions.SET_PART_LOADED, payload: true });
        store.clearActions();
      });
    });
  });

  describe('Attachments', () => {
    describe('adding an attachment', () => {
      it('should add an attachment', () => {
        store.clearActions();
        mutationHandler.mockResolvedValueOnce({ data: { partUpdate: { ...part } } });
        return store.dispatch(PartActionCreators.uploadAttachmentsThunk([attachmentInput])).then(() => {
          const actions = store.getActions();
          expect(mockUploader).toBeCalledTimes(1);
          const tags = mockUploader.mock.calls[0][3];
          const [partTag, drawingTag, autoballoonTag, fileTag] = tags;
          expect(autoballoonTag.value).toBeFalsy();
          expect(fileTag.value).toBe('.txt');
          expect(actions[0]).toEqual({ type: PartActionCreators.PartActions.SET_PART_LOADED, payload: false });
          expect(actions[1].type).toEqual(PartActionCreators.PartActions.SET_PART_STATE);
          store.clearActions();
        });
      });
    });
    describe('removing an attachment', () => {
      it('should remove an attachment', () => {
        store.clearActions();
        const inp = Parts.createGraphqlInput({ ...part, attachments: [] });
        mutationHandler.mockResolvedValueOnce({ data: { partUpdate: { ...part, attachments: [] } } });
        return store.dispatch(PartActionCreators.deleteAttachmentThunk(attachment)).then(() => {
          const actions = store.getActions();
          expect(mutationHandler).toBeCalledWith({
            id: part.id,
            data: inp,
          });
          expect(actions[0].type).toEqual(PartActionCreators.PartActions.SET_PART_STATE);
          store.clearActions();
        });
      });
    });
  });

  describe('Revisions', () => {
    describe('Part', () => {
      it('should log any errors when trying to fetch the part revision', () => {
        store.clearActions();
        queryRevisionsHandler.mockClear();
        // Return the promise
        queryRevisionsHandler.mockRejectedValueOnce(new Error('Some Error'));
        return store.dispatch(PartActionCreators.doGetPartRevisions(part.id)).then(() => {
          const actions = store.getActions();
          expect(queryRevisionsHandler).toBeCalledTimes(1);
          expect(queryRevisionsHandler).toBeCalledWith({
            filter: {
              originalPart: part.id,
            },
          });
          expect(actions[0]).toEqual({ type: PartActionCreators.PartActions.SET_PART_LOADED, payload: false });
          expect(actions[1]).toEqual({ type: PartActionCreators.PartActions.SET_PART_ERROR, payload: i18n('errors.parts.revisionList') });
          store.clearActions();
        });
      });

      it('should fetch the part revision', () => {
        // Return the promise
        queryRevisionsHandler.mockClear();
        queryRevisionsHandler.mockResolvedValueOnce({
          data: {
            partList: {
              count: 2,
              rows: [
                { id: '1', revision: '1', name: '1', number: '1' },
                { id: '2', revision: '2', name: '2', number: '2' },
                { id: '3', revision: '3', name: '3', number: '3' },
              ],
            },
          },
        });
        return store.dispatch(PartActionCreators.doGetPartRevisions(part.id)).then(() => {
          const actions = store.getActions();
          expect(queryRevisionsHandler).toBeCalledTimes(1);
          expect(queryRevisionsHandler).toBeCalledWith({
            filter: {
              originalPart: part.id,
            },
          });
          expect(actions[0]).toEqual({ type: PartActionCreators.PartActions.SET_PART_LOADED, payload: false });
          expect(actions[1]).toEqual({
            type: PartActionCreators.PartActions.SET_PART_STATE,
            payload: {
              revisions: {
                1: { id: '1', name: '1', number: '1', revision: '1', drawings: {}, createdAt: undefined, nextRevision: undefined, previousRevision: undefined },
                2: { id: '2', name: '2', number: '2', revision: '2', drawings: {}, createdAt: undefined, nextRevision: undefined, previousRevision: undefined },
                3: { id: '3', name: '3', number: '3', revision: '3', drawings: {}, createdAt: undefined, nextRevision: undefined, previousRevision: undefined },
              },
              revisionIndex: 0,
              revisionType: null,
            },
          });
          expect(actions[2]).toEqual({ type: PartActionCreators.PartActions.SET_PART_LOADED, payload: true });
          store.clearActions();
        });
      });

      it('should log any errors when trying to create a part revision', () => {
        store.clearActions();
        // Return the promise
        copyHandler.mockClear();
        copyHandler.mockResolvedValueOnce({
          data: {
            drawingCreate: {
              ...drawingRevision,
              part: {
                id: '0e842d25-b373-4a2a-a0a2-c38db91152a1',
                originalPart: '3e40e49b-7464-4b4e-8169-b6fb2183c0ff',
                previousRevision: '3e40e49b-7464-4b4e-8169-b6fb2183c0ff',
              },
            },
          },
        });
        copyHandler.mockResolvedValueOnce({
          data: {
            characteristicCreate: {
              ...featureRevision,
              part: {
                id: '0e842d25-b373-4a2a-a0a2-c38db91152a1',
                originalPart: '3e40e49b-7464-4b4e-8169-b6fb2183c0ff',
                previousRevision: '3e40e49b-7464-4b4e-8169-b6fb2183c0ff',
              },
            },
          },
        });
        mutationPartRevisionHandler.mockRejectedValueOnce(new Error('Some Error'));
        return store.dispatch(PartActionCreators.doAddPartRevisions(part, history)).then(() => {
          const actions = store.getActions();
          expect(mutationPartRevisionHandler).toBeCalledTimes(1);
          expect(mutationPartRevisionHandler).toBeCalledWith({ id: part.id });
          expect(actions[0]).toEqual({ type: PartActionCreators.PartActions.SET_PART_LOADED, payload: false });
          expect(actions[1]).toEqual({ type: PartActionCreators.PartActions.SET_PART_ERROR, payload: i18n('errors.parts.revision') });
          store.clearActions();
        });
      });

      it('should create a part revision', () => {
        store.clearActions();
        // Return the promise
        mutationPartRevisionHandler.mockClear();
        mutationPartRevisionHandler.mockResolvedValueOnce({
          data: {
            partRevision: {
              part: {
                id: '0e842d25-b373-4a2a-a0a2-c38db91152a1',
                revision: 'Part B',
                primaryDrawing: { id: '9a5c963b-977e-488b-9773-2d3858b42886', revision: 'B', originalDrawing: '9a5c963b-977e-488b-9773-2d3858b42886', createdAt: '2021-08-25T14:08:02.457Z' },
                createdAt: '2021-08-25T14:08:02.457Z',
              },
              drawings: [{ id: '9a5c963b-977e-488b-9773-2d3858b42886', revision: 'B', originalDrawing: '9a5c963b-977e-488b-9773-2d3858b42886', createdAt: '2021-08-25T14:08:02.457Z' }],
              sheets: [{ id: '873d2405-abd7-41c2-b3d9-31ce108f8729' }],
              characteristics: [{ id: '2580de7c-64a7-49c6-85bf-bf7f1c322689' }],
            },
          },
        });
        return store.dispatch(PartActionCreators.doAddPartRevisions(part, history)).then(() => {
          const actions = store.getActions();
          expect(mutationPartRevisionHandler).toBeCalledTimes(1);
          expect(mutationPartRevisionHandler).toBeCalledWith({ id: part.id });
          expect(mutationPartRevisionHandler).toBeCalledTimes(1);
          expect(actions[0]).toEqual({ type: PartActionCreators.PartActions.SET_PART_LOADED, payload: false });
          delete actions[1].payload.revisions[revision.id].createdAt;
          delete actions[1].payload.revisions[revision.id].drawings[drawingRevision.id].createdAt;
          expect(actions[1]).toEqual({
            type: PartActionCreators.PartActions.SET_PART_STATE,
            payload: {
              revisions: {
                [revision.id]: {
                  id: revision.id,
                  revision: revision.revision,
                  drawings: {
                    [drawingRevision.id]: {
                      id: drawingRevision.id,
                      revision: drawingRevision.revision,
                    },
                  },
                },
              },
              revisionIndex: 0,
              revisionType: null,
            },
          });
          store.clearActions();
        });
      });
    });
    describe('Drawing', () => {
      const newFile = {
        name: 'New Revision',
        size: 10,
      };
      it('should log any errors when trying to fetch the drawing revision', () => {
        store.clearActions();
        // Return the promise
        copyHandler.mockResolvedValueOnce({
          data: {
            drawingSheetCreate: sheetRevision,
          },
        });
        queryDrawingRevisionsHandler.mockRejectedValueOnce(new Error('Some Error'));
        return store.dispatch(PartActionCreators.doGetDrawingRevisions(drawing.id)).then(() => {
          const actions = store.getActions();
          expect(queryDrawingRevisionsHandler).toBeCalledTimes(1);
          expect(queryDrawingRevisionsHandler).toBeCalledWith({
            filter: {
              originalDrawing: drawing.id,
            },
          });
          expect(actions[0]).toEqual({ type: PartActionCreators.PartActions.SET_PART_LOADED, payload: false });
          expect(actions[1]).toEqual({ type: PartActionCreators.PartActions.SET_PART_ERROR, payload: i18n('errors.parts.drawingRevisionList') });
          store.clearActions();
        });
      });

      it('should fetch the drawing revision', () => {
        // Return the promise
        copyHandler.mockResolvedValueOnce({
          data: {
            drawingSheetCreate: sheetRevision,
          },
        });
        queryDrawingRevisionsHandler.mockClear();
        queryDrawingRevisionsHandler.mockResolvedValueOnce({
          data: {
            drawingList: {
              count: 2,
              rows: [
                { id: '1', revision: '1', name: '1', number: '1', part: { id: '1', revision: '1' } },
                { id: '2', revision: '2', name: '2', number: '2', part: { id: '1', revision: '1' } },
                { id: '3', revision: '3', name: '3', number: '3', part: { id: '1', revision: '1' } },
              ],
            },
          },
        });
        return store.dispatch(PartActionCreators.doGetDrawingRevisions(drawing.id)).then(() => {
          const actions = store.getActions();
          expect(queryDrawingRevisionsHandler).toBeCalledTimes(1);
          expect(queryDrawingRevisionsHandler).toBeCalledWith({
            filter: {
              originalDrawing: drawing.id,
            },
          });
          expect(actions[0]).toEqual({ type: PartActionCreators.PartActions.SET_PART_LOADED, payload: false });
          expect(actions[1]).toEqual({
            type: PartActionCreators.PartActions.SET_REVISIONS,
            payload: {
              1: {
                drawings: {
                  1: {
                    id: '1',
                    revision: '1',
                    name: '1',
                    number: '1',
                  },
                  2: {
                    id: '2',
                    revision: '2',
                    name: '2',
                    number: '2',
                  },
                  3: {
                    id: '3',
                    revision: '3',
                    name: '3',
                    number: '3',
                  },
                },
              },
            },
          });
          expect(actions[2]).toEqual({ type: PartActionCreators.PartActions.SET_PART_LOADED, payload: true });
          store.clearActions();
        });
      });

      it('should log any errors when trying to create a the drawing revision', () => {
        store.clearActions();
        // Return the promise
        copyHandler.mockReset();
        mutationDrawingRevisionHandler.mockReset();
        copyHandler.mockRejectedValueOnce(new Error('Some Error'));
        return store.dispatch(PartActionCreators.doAddDrawingRevisions(drawing, history)).then(() => {
          const actions = store.getActions();
          expect(mutationDrawingRevisionHandler).toBeCalledTimes(1);
          expect(actions[0]).toEqual({ type: PartActionCreators.PartActions.SET_PART_LOADED, payload: false });
          expect(actions[1]).toEqual({ type: PartActionCreators.PartActions.SET_PART_ERROR, payload: i18n('errors.parts.drawingRevision') });
          store.clearActions();
        });
      });

      it('should create a drawing revision', () => {
        store.clearActions();
        queryHandler.mockClear();
        queryHandler.mockResolvedValue({ data: { partFind: part, reportList: { count: 0, rows: [] }, drawingList: { count: 1, rows: [drawing] } } });
        // Return the promise
        mutationDrawingRevisionHandler.mockClear();
        mutationDrawingRevisionHandler.mockResolvedValueOnce({
          data: {
            drawingRevision: {
              part,
              drawing: drawingRevision,
              sheets: [sheetRevision],
              characteristics: [featureRevision],
            },
          },
        });
        return store.dispatch(PartActionCreators.doAddDrawingRevisions(drawing, history)).then(() => {
          const actions = store.getActions();
          expect(mutationDrawingRevisionHandler).toBeCalledTimes(1);
          const drawingRevisionInput = mutationDrawingRevisionHandler.mock.calls[0][0];
          delete drawingRevisionInput.file; // The UUID here is generated at run, so it will always fail the test
          delete drawingRevisionInput.drawingFile; // The UUID here is generated at run, so it will always fail the test
          expect(drawingRevisionInput).toMatchObject({ drawingId: drawing.id, partId: part.id });
          expect(actions[0]).toEqual({ type: PartActionCreators.PartActions.SET_PART_LOADED, payload: false });
          expect(actions[1].type).toBe(PartActionCreators.PartActions.SET_PART_STATE);
          expect(actions[1].payload.revisions).toHaveProperty(drawingRevision.part.id);
          expect(actions[1].payload.revisions[drawingRevision.part.id].drawings).toHaveProperty(drawingRevision.id);
          expect(actions[1].payload.revisions[drawingRevision.part.id].drawings[drawingRevision.id].id).toBe(drawingRevision.id);
          expect(actions[1].payload.revisions[drawingRevision.part.id].drawings[drawingRevision.id].revision).toBe('B');
          expect(actions[1].payload.revisions[drawingRevision.part.id].drawings[drawingRevision.id].previousRevision).toBe(drawing.id);
          expect(actions[2]).toEqual({ type: PartEditorActionCreators.PartEditorActionTypes.SET_PART_EDITOR_DRAWING, payload: drawingRevision });
          expect(actions[3]).toEqual({ type: PartEditorActionCreators.PartEditorActionTypes.SET_PART_EDITOR_FOCUS, payload: '1 revision-input' });
          store.clearActions();
        });
      });
    });
  });
});

describe('Part Utilities', () => {
  describe('nextLetter', () => {
    it('returns the next letter in the alphabet', () => {
      let next = PartActionCreators.nextLetter('A');
      expect(next).toBe('B');
      next = PartActionCreators.nextLetter('G');
      expect(next).toBe('H');
      next = PartActionCreators.nextLetter('K');
      expect(next).toBe('L');
      next = PartActionCreators.nextLetter('V');
      expect(next).toBe('W');
    });
    it('loops around when it comes to the end of the alphabet', () => {
      const next = PartActionCreators.nextLetter('Z');
      expect(next).toBe('ZA');
    });
  });

  describe('guessNextRevision', () => {
    it('returns the next letter for a letter sequence', () => {
      let next = PartActionCreators.guessNextRevision('12.A');
      expect(next).toBe('12.B');
      next = PartActionCreators.guessNextRevision('Revision B');
      expect(next).toBe('Revision C');
      next = PartActionCreators.guessNextRevision('Part 1266 H');
      expect(next).toBe('Part 1266 I');
    });

    it('returns the next number for a number sequence', () => {
      let next = PartActionCreators.guessNextRevision('H.5');
      expect(next).toBe('H.6');
      next = PartActionCreators.guessNextRevision('Revision 12');
      expect(next).toBe('Revision 13');
      next = PartActionCreators.guessNextRevision('1252525');
      expect(next).toBe('1252526');
    });

    it('returns the current date for a date input', () => {
      const currentDate = moment().format('YYYY-MM-DD');
      let next = PartActionCreators.guessNextRevision('2020-12-12');
      expect(next).toBe(currentDate);
      next = PartActionCreators.guessNextRevision();
      expect(next).toBe(currentDate);
    });

    it('loops around when it comes to the end of the alphabet', () => {
      const next = PartActionCreators.guessNextRevision('12.Z');
      expect(next).toBe('12.ZA');
    });
  });
});
