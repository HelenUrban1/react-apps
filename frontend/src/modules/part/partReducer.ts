import { PartAction, PartState } from 'modules/state';

export const initialState = {
  part: null,
  revisions: {},
  revisionIndex: 0,
  revisionType: null,
  newPart: null,
  originalPart: null,
  previousPart: null,
  partEditor: null,
  parts: [],
  tablePage: 1,
  tableFilter: { customer: [], reviewStatus: [], accessControl: [] },
  tableSorter: null,
  partLoaded: true,
  partError: null,
};

export default (state: PartState = initialState, { type, payload }: PartAction): PartState => {
  switch (type) {
    case 'PART_SET_PART_STATE':
      return { ...state, ...payload };

    case 'PART_SET_PART':
      return { ...state, part: payload };

    case 'PART_SET_REVISIONS':
      return { ...state, revisions: payload };

    case 'PART_SET_REVISION_INDEX':
      return { ...state, revisionIndex: payload.revisionIndex, revisionType: payload.revisionType };

    case 'PART_SET_PARTS':
      return { ...state, parts: payload };

    case 'PART_SET_PREVIOUS_PART':
      return { ...state, previousPart: payload };

    case 'PART_SET_ORIGINAL_PART':
      return { ...state, originalPart: payload };

    case 'PART_SET_NEW_PART':
      return { ...state, newPart: payload };

    case 'PART_SET_TABLE_PAGE':
      return { ...state, tablePage: payload };

    case 'PART_SET_TABLE_FILTER':
      return { ...state, tableFilter: payload };

    case 'PART_SET_TABLE_SORTER':
      return { ...state, tableSorter: payload };

    case 'PART_SET_LOADED':
      return { ...state, partLoaded: payload };

    case 'PART_SET_ERROR':
      return { ...state, partError: payload, partLoaded: true };

    case 'PART_RESET':
      return { ...initialState };

    case 'PART_RESET_TABLE':
      return { ...state, tableSorter: initialState.tableSorter, tablePage: initialState.tablePage, tableFilter: initialState.tableFilter };
    default:
      return state;
  }
};
