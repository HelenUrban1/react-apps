import { i18n } from 'i18n';
import IdField from 'modules/shared/fields/idField';
import DateTimeField from 'modules/shared/fields/dateTimeField';
import StringField from 'modules/shared/fields/stringField';
import EnumeratorField from 'modules/shared/fields/enumeratorField';
import RelationToManyField from 'modules/shared/fields/relationToManyField';

function label(name) {
  return i18n(`entities.site.fields.${name}`);
}

function enumeratorLabel(name, value) {
  return i18n(`entities.site.enumerators.${name}.${value}`);
}

const fields = {
  id: new IdField('id', label('id')),
  status: new EnumeratorField(
    'status',
    label('status'),
    [
      { id: 'Active', label: enumeratorLabel('status', 'Active') },
      { id: 'Past_Due', label: enumeratorLabel('status', 'Past_Due') },
      { id: 'Suspended', label: enumeratorLabel('status', 'Suspended') },
      { id: 'Archived', label: enumeratorLabel('status', 'Archived') },
    ],
    {
      required: true,
    },
  ),
  settings: new StringField('settings', label('settings'), {
    required: true,
  }),
  siteName: new StringField('siteName', label('siteName'), {
    required: true,
  }),
  sitePhone: new StringField('sitePhone', label('sitePhone'), {}),
  siteEmail: new StringField('siteEmail', label('siteEmail'), {
    required: true,
  }),
  siteStreet: new StringField('siteStreet', label('siteStreet'), {}),
  siteStreet2: new StringField('siteStreet2', label('siteStreet2'), {}),
  siteCity: new StringField('siteCity', label('siteCity'), {}),
  siteRegion: new EnumeratorField(
    'siteRegion',
    label('siteRegion'),
    [
      { id: 'Alabama', label: enumeratorLabel('siteRegion', 'Alabama') },
      { id: 'Alaska', label: enumeratorLabel('siteRegion', 'Alaska') },
      { id: 'American_Samoa', label: enumeratorLabel('siteRegion', 'American_Samoa') },
      { id: 'Arizona', label: enumeratorLabel('siteRegion', 'Arizona') },
      { id: 'Arkansas', label: enumeratorLabel('siteRegion', 'Arkansas') },
      { id: 'California', label: enumeratorLabel('siteRegion', 'California') },
      { id: 'Colorado', label: enumeratorLabel('siteRegion', 'Colorado') },
      { id: 'Connecticut', label: enumeratorLabel('siteRegion', 'Connecticut') },
      { id: 'Delaware', label: enumeratorLabel('siteRegion', 'Delaware') },
      { id: 'District_of_Columbia', label: enumeratorLabel('siteRegion', 'District_of_Columbia') },
      { id: 'Florida', label: enumeratorLabel('siteRegion', 'Florida') },
      { id: 'Georgia', label: enumeratorLabel('siteRegion', 'Georgia') },
      { id: 'Guam', label: enumeratorLabel('siteRegion', 'Guam') },
      { id: 'Hawaii', label: enumeratorLabel('siteRegion', 'Hawaii') },
      { id: 'Idaho', label: enumeratorLabel('siteRegion', 'Idaho') },
      { id: 'Illinois', label: enumeratorLabel('siteRegion', 'Illinois') },
      { id: 'Indiana', label: enumeratorLabel('siteRegion', 'Indiana') },
      { id: 'Iowa', label: enumeratorLabel('siteRegion', 'Iowa') },
      { id: 'Kansas', label: enumeratorLabel('siteRegion', 'Kansas') },
      { id: 'Kentucky', label: enumeratorLabel('siteRegion', 'Kentucky') },
      { id: 'Louisiana', label: enumeratorLabel('siteRegion', 'Louisiana') },
      { id: 'Maine', label: enumeratorLabel('siteRegion', 'Maine') },
      { id: 'Maryland', label: enumeratorLabel('siteRegion', 'Maryland') },
      { id: 'Massachusetts', label: enumeratorLabel('siteRegion', 'Massachusetts') },
      { id: 'Michigan', label: enumeratorLabel('siteRegion', 'Michigan') },
      { id: 'Minnesota', label: enumeratorLabel('siteRegion', 'Minnesota') },
      {
        id: 'Minor_Outlying_Islands',
        label: enumeratorLabel('siteRegion', 'Minor_Outlying_Islands'),
      },
      { id: 'Mississippi', label: enumeratorLabel('siteRegion', 'Mississippi') },
      { id: 'Missouri', label: enumeratorLabel('siteRegion', 'Missouri') },
      { id: 'Montana', label: enumeratorLabel('siteRegion', 'Montana') },
      { id: 'Nebraska', label: enumeratorLabel('siteRegion', 'Nebraska') },
      { id: 'Nevada', label: enumeratorLabel('siteRegion', 'Nevada') },
      { id: 'New_Hampshire', label: enumeratorLabel('siteRegion', 'New_Hampshire') },
      { id: 'New_Jersey', label: enumeratorLabel('siteRegion', 'New_Jersey') },
      { id: 'New_Mexico', label: enumeratorLabel('siteRegion', 'New_Mexico') },
      { id: 'New_York', label: enumeratorLabel('siteRegion', 'New_York') },
      { id: 'North_Carolina', label: enumeratorLabel('siteRegion', 'North_Carolina') },
      { id: 'North_Dakota', label: enumeratorLabel('siteRegion', 'North_Dakota') },
      {
        id: 'Northern_Mariana_Islands',
        label: enumeratorLabel('siteRegion', 'Northern_Mariana_Islands'),
      },
      { id: 'Ohio', label: enumeratorLabel('siteRegion', 'Ohio') },
      { id: 'Oklahoma', label: enumeratorLabel('siteRegion', 'Oklahoma') },
      { id: 'Oregon', label: enumeratorLabel('siteRegion', 'Oregon') },
      { id: 'Pennsylvania', label: enumeratorLabel('siteRegion', 'Pennsylvania') },
      { id: 'Puerto_Rico', label: enumeratorLabel('siteRegion', 'Puerto_Rico') },
      { id: 'Rhode_Island', label: enumeratorLabel('siteRegion', 'Rhode_Island') },
      { id: 'South_Carolina', label: enumeratorLabel('siteRegion', 'South_Carolina') },
      { id: 'South_Dakota', label: enumeratorLabel('siteRegion', 'South_Dakota') },
      { id: 'Tennessee', label: enumeratorLabel('siteRegion', 'Tennessee') },
      { id: 'Texas', label: enumeratorLabel('siteRegion', 'Texas') },
      { id: 'US_Virgin_Islands', label: enumeratorLabel('siteRegion', 'US_Virgin_Islands') },
      { id: 'Utah', label: enumeratorLabel('siteRegion', 'Utah') },
      { id: 'Vermont', label: enumeratorLabel('siteRegion', 'Vermont') },
      { id: 'Virginia', label: enumeratorLabel('siteRegion', 'Virginia') },
      { id: 'Washington', label: enumeratorLabel('siteRegion', 'Washington') },
      { id: 'West_Virginia', label: enumeratorLabel('siteRegion', 'West_Virginia') },
      { id: 'Wisconsin', label: enumeratorLabel('siteRegion', 'Wisconsin') },
      { id: 'Wyoming', label: enumeratorLabel('siteRegion', 'Wyoming') },
    ],
    {},
  ),
  sitePostalcode: new StringField('sitePostalcode', label('sitePostalcode'), {}),
  siteCountry: new EnumeratorField('siteCountry', label('siteCountry'), [{ id: 'United_States', label: enumeratorLabel('siteCountry', 'United_States') }], {}),
  parts: new RelationToManyField('parts', label('parts'), {}),
  members: new RelationToManyField('members', label('members'), {}),
  reportTemplates: new RelationToManyField('reportTemplates', label('reportTemplates'), {}),
  reports: new RelationToManyField('reports', label('reports'), {}),
  createdAt: new DateTimeField('createdAt', label('createdAt')),
  updatedAt: new DateTimeField('updatedAt', label('updatedAt')),
};

export default {
  fields,
};
