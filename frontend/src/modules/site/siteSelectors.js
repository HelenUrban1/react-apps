import { createSelector } from 'reselect';
import authSelectors from 'modules/auth/authSelectors';
import PermissionChecker from 'modules/auth/permissionChecker';
import Permissions from 'security/permissions';

const selectPermissionToRead = createSelector([authSelectors.selectCurrentUser], (currentUser) => new PermissionChecker(currentUser).match(Permissions.values.siteRead));

const selectPermissionToEdit = createSelector([authSelectors.selectCurrentUser], (currentUser) => new PermissionChecker(currentUser).match(Permissions.values.siteEdit));

const selectPermissionToCreate = createSelector([authSelectors.selectCurrentUser], (currentUser) => new PermissionChecker(currentUser).match(Permissions.values.siteCreate));

const selectPermissionToImport = createSelector([authSelectors.selectCurrentUser], (currentUser) => new PermissionChecker(currentUser).match(Permissions.values.siteImport));

const selectPermissionToDestroy = createSelector([authSelectors.selectCurrentUser], (currentUser) => new PermissionChecker(currentUser).match(Permissions.values.siteDestroy));

const selectors = {
  selectPermissionToRead,
  selectPermissionToEdit,
  selectPermissionToCreate,
  selectPermissionToDestroy,
  selectPermissionToImport,
};

export default selectors;
