import gql from 'graphql-tag';
import getClient from 'modules/shared/graphql/graphqlClient';

export default class SiteService {
  static async update(id, data) {
    const graphqlClient = await getClient();
    const response = await graphqlClient.mutate({
      mutation: gql`
        mutation SITE_UPDATE($id: String!, $data: SiteInput!) {
          siteUpdate(id: $id, data: $data) {
            id
          }
        }
      `,

      variables: {
        id,
        data,
      },
    });

    return response.data.siteUpdate;
  }

  static async destroyAll(ids) {
    const graphqlClient = await getClient();
    const response = await graphqlClient.mutate({
      mutation: gql`
        mutation SITE_DESTROY($ids: [String!]!) {
          siteDestroy(ids: $ids)
        }
      `,

      variables: {
        ids,
      },
    });

    return response.data.siteDestroy;
  }

  static async create(data) {
    const graphqlClient = await getClient();
    const response = await graphqlClient.mutate({
      mutation: gql`
        mutation SITE_CREATE($data: SiteInput!) {
          siteCreate(data: $data) {
            id
          }
        }
      `,

      variables: {
        data,
      },
    });

    return response.data.siteCreate;
  }

  static async import(values, importHash) {
    const graphqlClient = await getClient();
    const response = await graphqlClient.mutate({
      mutation: gql`
        mutation SITE_IMPORT($data: SiteInput!, $importHash: String!) {
          siteImport(data: $data, importHash: $importHash)
        }
      `,

      variables: {
        data: values,
        importHash,
      },
    });

    return response.data.siteImport;
  }

  static async find(id) {
    const graphqlClient = await getClient();
    const response = await graphqlClient.query({
      query: gql`
        query SITE_FIND($id: String!) {
          siteFind(id: $id) {
            id
            status
            settings
            siteName
            sitePhone
            siteEmail
            siteStreet
            siteStreet2
            siteCity
            siteRegion
            sitePostalcode
            siteCountry
            parts {
              id
              name
            }
            members {
              id
            }
            reportTemplates {
              id
              title
            }
            reports {
              id
              title
            }
            createdAt
            updatedAt
          }
        }
      `,

      variables: {
        id,
      },
    });

    return response.data.siteFind;
  }

  static async list(filter, orderBy, limit, offset) {
    const graphqlClient = await getClient();
    const response = await graphqlClient.query({
      query: gql`
        query SITE_LIST($filter: SiteFilterInput, $orderBy: SiteOrderByEnum, $limit: Int, $offset: Int) {
          siteList(filter: $filter, orderBy: $orderBy, limit: $limit, offset: $offset) {
            count
            rows {
              id
              status
              settings
              siteName
              sitePhone
              siteEmail
              siteStreet
              siteStreet2
              siteCity
              siteRegion
              sitePostalcode
              siteCountry
              parts {
                id
                name
              }
              members {
                id
              }
              reportTemplates {
                id
                title
              }
              reports {
                id
                title
              }
              updatedAt
              createdAt
            }
          }
        }
      `,

      variables: {
        filter,
        orderBy,
        limit,
        offset,
      },
    });

    return response.data.siteList;
  }

  static async listAutocomplete(query, limit) {
    const graphqlClient = await getClient();
    const response = await graphqlClient.query({
      query: gql`
        query SITE_AUTOCOMPLETE($query: String, $limit: Int) {
          siteAutocomplete(query: $query, limit: $limit) {
            id
            label
          }
        }
      `,

      variables: {
        query,
        limit,
      },
    });

    return response.data.siteAutocomplete;
  }
}
