import list from 'modules/site/list/siteListReducers';
import form from 'modules/site/form/siteFormReducers';
import view from 'modules/site/view/siteViewReducers';
import destroy from 'modules/site/destroy/siteDestroyReducers';
import importerReducer from 'modules/site/importer/siteImporterReducers';
import { combineReducers } from 'redux';

export default combineReducers({
  list,
  form,
  view,
  destroy,
  importer: importerReducer,
});
