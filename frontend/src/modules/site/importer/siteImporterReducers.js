import importerReducers from 'modules/shared/importer/importerReducers';
import actions from 'modules/site/importer/siteImporterActions';

export default importerReducers(actions);
