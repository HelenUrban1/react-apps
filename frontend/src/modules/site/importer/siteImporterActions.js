import importerActions from 'modules/shared/importer/importerActions';
import selectors from 'modules/site/importer/siteImporterSelectors';
import SiteService from 'modules/site/siteService';
import fields from 'modules/site/importer/siteImporterFields';
import { i18n } from '../../../i18n';

export default importerActions('SITE_IMPORTER', selectors, SiteService.import, fields, i18n('entities.site.importer.fileName'));
