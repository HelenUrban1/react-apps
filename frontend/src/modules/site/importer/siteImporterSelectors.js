import importerSelectors from 'modules/shared/importer/importerSelectors';

export default importerSelectors('site.importer');
