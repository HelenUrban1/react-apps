import model from 'modules/site/siteModel';

const { fields } = model;

export default [
  fields.status,
  fields.settings,
  fields.siteName,
  fields.sitePhone,
  fields.siteEmail,
  fields.siteStreet,
  fields.siteStreet2,
  fields.siteCity,
  fields.siteRegion,
  fields.sitePostalcode,
  fields.siteCountry,
  fields.parts,
  fields.members,
  fields.reportTemplates,
  fields.reports,
];
