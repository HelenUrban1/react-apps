import { Dispatch } from 'redux';
import { ApolloClient } from '@apollo/client';
import { ReportTemplate, ReportTemplateInput, ReportData } from 'types/reportTemplates';
import log from 'modules/shared/logger';
import axios from 'modules/shared/network/axios';
import config from 'config';
import { i18n } from 'i18n';
import Analytics from 'modules/shared/analytics/analytics';
import { ReportTemplateAPI, ReportTemplateCreateData, ReportTemplateCreateVars } from 'graphql/reportTemplate';
import OrganizationAPI from 'graphql/organization';
import { getFileExtension } from 'utils/files';
import { v4 as uuid } from 'uuid';

const prefix = 'TEMPLATE';

const actions = {
  SET_ORG: `${prefix}_SET_ORG`,
  SET_PART: `${prefix}_SET_PART`,
  SET_TEMPLATE: `${prefix}_SET_TEMPLATE`,
  SET_TITLTE: `${prefix}_SET_TITLE`,
  UPDATE_TEMPLATE_CONTEXT: `${prefix}_UPDATE_TEMPLATE_CONTEXT`,
  SAVE_TEMPLATE: `${prefix}_SAVE_TEMPLATE`,
  SET_DELETABLE: `${prefix}_SET_DELETE`,
  TEMPLATE_LOADING: `${prefix}_LOADING`,
  TEMPLATE_ERROR: `${prefix}_ERROR`,
};

export default actions;

export const loadTemplate =
  ({ template, part, id, generate = false }: { template: ReportTemplate; part?: ReportData; id?: string; generate?: boolean }) =>
  async (dispatch: Dispatch, getState: any, clientPromise: Promise<ApolloClient<any>>) => {
    const state = getState();
    const { jwt } = state.auth;

    return clientPromise.then(async () => {
      if (!template.file || !template.file[0]) {
        log.warn('No file connected to this template');
        log.debug(template);
        return;
      }
      if (generate && !part) {
        log.warn('No part data to parse');
        return;
      }
      if (template.id.includes('default')) {
        log.warn('default template detected');
        return;
      }

      const url = template.file[0].publicUrl;
      dispatch({ type: 'TEMPLATE_LOADING', payload: { loading: true } });
      const apiEndpoint = generate ? `${config.backendUrl}/template` : `${config.backendUrl}/excel`;
      const templateData = await axios({
        method: 'post',
        url: apiEndpoint,
        data: { privateUrl: url, data: part },
        headers: {
          authorization: jwt ? `Bearer ${jwt}` : '',
        },
      }).catch((err) => {
        if (jwt && err.response.status === 401) {
          log.warn('Authorization Token Expired');
          return { data: 'auth' };
        }
        log.error(err.response.data);
        dispatch({ type: 'TEMPLATE_ERROR', payload: { error: err.response.data.message } });
        return null;
      });

      if (templateData && !!templateData.data && template && templateData.data?.data !== false && templateData.data !== 'auth') {
        const tempData = templateData?.data?.data ? [...templateData.data.data] : [...templateData.data];
        const sets = template?.settings ? JSON.parse(template.settings) : {};
        if (sets && sets.footers && templateData) {
          Object.keys(sets.footers).forEach((sheet: string) => {
            Object.keys(sets.footers![sheet]).forEach((cell: string) => {
              const footer = sets!.footers![sheet][cell];
              if (tempData[parseInt(sheet, 10)] && tempData[parseInt(sheet, 10)].rows[footer.row] && tempData[parseInt(sheet, 10)].rows[footer.row][footer.col]) {
                tempData[parseInt(sheet, 10)].rows[footer.row][footer.col].footerRow = footer.footer;
                tempData[parseInt(sheet, 10)].rows[footer.row][footer.col].ind = {
                  row: footer.row,
                  col: footer.col,
                };
              }
            });
          });
        }

        dispatch({ type: 'TEMPLATE_SET_TABLE_DATA', payload: tempData });
        dispatch({ type: 'TEMPLATE_SET_SETTINGS', payload: { ...sets, saved: !!id } });
        dispatch({ type: 'TEMPLATE_SET_DIRECTION', payload: sets?.direction || 'Vertical' });
        dispatch({ type: 'TEMPLATE_SET_TEMPLATE', payload: template });
        dispatch({ type: 'TEMPLATE_SET_ID', payload: template.id });
        if (!id) {
          dispatch({ type: 'TEMPLATE_CREATE_TEMPLATE', payload: false });
        } else {
          dispatch({ type: 'TEMPLATE_SET_STEP', payload: 1 });
        }

        dispatch({ type: 'TEMPLATE_LOADING', payload: { loading: false } });
      } else if (templateData?.data !== 'auth') {
        dispatch({ type: 'TEMPLATE_ERROR', payload: { error: i18n('entities.template.errors.blankFile') } });
      }
    });
  };

export const cloneTemplate =
  ({ template, provider, source }: { template: ReportTemplate; provider: string; source: string }) =>
  async (dispatch: Dispatch, getState: any, clientPromise: Promise<ApolloClient<any>>) => {
    const state = getState();
    const { jwt } = state.auth;

    return clientPromise.then(async (client) => {
      dispatch({ type: 'TEMPLATE_LOADING', payload: { loading: true } });
      try {
        // Prepare metadata about the provided files
        const extension = getFileExtension(template.file[0].name) || '.xlsx';
        const newFileName = `${uuid()}${extension}`;

        const newTemplate: ReportTemplateInput = {
          provider,
          title: `${template.title} (Copy)`,
          status: 'Active',
          direction: template.direction,
          settings: template.settings ? JSON.stringify({ ...JSON.parse(template.settings), saved: false }) : '{}',
          type: template.type,
          file: [
            {
              name: template.file[0].name,
              sizeInBytes: template.file[0].sizeInBytes,
              publicUrl: `${config.backendUrl}/download?privateUrl=${newFileName}`,
              privateUrl: newFileName,
              new: true,
            },
          ],
        };

        await axios({
          method: 'post',
          url: `${config.backendUrl}/template/copy`,
          data: {
            privateUrl: template.file[0].privateUrl || template.file[0].publicUrl,
            newFileName,
          },
          headers: {
            authorization: jwt ? `Bearer ${jwt}` : '',
          },
        })
          .then(async (response) => {
            const { file } = response.data;

            if (file) {
              const templateCreateResult = await client.mutate<ReportTemplateCreateData, ReportTemplateCreateVars>({
                mutation: ReportTemplateAPI.mutate.create,
                variables: { data: newTemplate },
                refetchQueries: [
                  {
                    query: OrganizationAPI.query.list,
                    variables: { filter: { status: 'Active' } },
                  },
                ],
              });

              dispatch({ type: 'TEMPLATE_CREATE_TEMPLATE', payload: true });

              dispatch({ type: 'REPORT_SET_TEMPLATE', payload: templateCreateResult.data?.reportTemplateCreate }); // REPORT_SET_TEMPLATE
              dispatch({ type: 'TEMPLATE_SET_TEMPLATE', payload: templateCreateResult.data?.reportTemplateCreate });

              // Update Template List
              const { organizations } = state.session;
              dispatch({ type: 'SESSION_UPDATE_ORGANIZATIONS', payload: { self: { ...organizations.self, templates: [...organizations.self.templates, templateCreateResult.data?.reportTemplateCreate] } } });

              Analytics.track({
                event: Analytics.events.cloneReportTemplate,
                part: null,
                template,
                properties: {
                  source,
                },
              });
              dispatch({ type: 'TEMPLATE_LOADING', payload: { loading: false } });
            }

            return response.data;
          })
          .catch((error) => {
            if (jwt && error.response.status === 401) {
              log.warn('Authorization Token Expired');
              return;
            }
            log.error(error);
            dispatch({ type: 'TEMPLATE_ERROR', payload: { error } });
          });
      } catch (err) {
        log.error(err);
        dispatch({ type: 'TEMPLATE_ERROR', payload: { err } });
      }
    });
  };
