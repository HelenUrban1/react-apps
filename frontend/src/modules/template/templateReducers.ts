import { isEqual } from 'lodash';
import log from 'modules/shared/logger';
import { TemplateAction, TemplateState } from 'modules/state';
import { ReportTemplate } from 'types/reportTemplates';
import { parseValidJSONString } from '../../utils/textOperations';

const emptyTemplate: ReportTemplate = {
  title: '',
  id: '',
  provider: {
    id: '',
    status: 'Active',
    type: 'Customer',
    name: '',
  },
  status: 'Inactive',
  file: [],
  direction: 'Vertical',
};

const initialData: TemplateState = {
  reportTemplate: null,
  tableData: null,
  partData: null,
  createNew: false,
  step: 0,
  id: null,
  settings: undefined,
  direction: undefined,
  org: null,
  title: undefined,
  used: [],
  loading: false,
  error: null,
};

export default (state = initialData, { type, payload }: TemplateAction): TemplateState => {
  switch (type) {
    case 'TEMPLATE_CLEAR_TEMPLATE':
      return initialData;
    case 'TEMPLATE_CREATE_TEMPLATE':
      return {
        ...state,
        createNew: payload,
      };
    case 'TEMPLATE_SET_TEMPLATE':
      const reportTemplate = payload;
      const newState = { ...state, step: 0, reportTemplate };
      if (payload) {
        const tempSettings = payload.settings ? parseValidJSONString(payload.settings) : {};
        if (tempSettings?.tokens && !isEqual(tempSettings.tokens, state.used)) {
          newState.used = tempSettings.tokens;
        }

        if (!newState.settings) {
          newState.settings = { saved: false };
        }

        if (!newState.settings.footers) {
          newState.settings.footers = {};
        }

        if (!newState.settings.tokens) {
          newState.settings.tokens = [];
        }

        if (newState.settings && tempSettings!.footers && !isEqual(tempSettings.footers, newState.settings.footers)) {
          newState.settings = tempSettings;
        }

        if (newState.settings && tempSettings.saved !== newState.settings.saved) {
          newState.settings.saved = tempSettings.saved;
        }
      }
      if (payload?.title && !state.title) {
        newState.title = payload.title;
      }
      return newState;
    case 'TEMPLATE_SET_ID':
      return {
        ...state,
        id: payload,
      };
    case 'TEMPLATE_SET_STEP':
      return {
        ...state,
        step: payload,
      };
    case 'TEMPLATE_SET_TABLE_DATA':
      return {
        ...state,
        tableData: payload,
      };
    case 'TEMPLATE_SET_TITLE':
      return {
        ...state,
        title: payload,
        reportTemplate: Object.assign(state.reportTemplate || emptyTemplate, { title: payload }),
      };
    case 'TEMPLATE_SET_PROVIDER':
      return {
        ...state,
        reportTemplate: Object.assign(state.reportTemplate || emptyTemplate, { provider: payload }),
      };
    case 'TEMPLATE_SET_SETTINGS':
      try {
        return {
          ...state,
          settings: Object.assign(state.settings || {}, payload),
          reportTemplate: Object.assign(state.reportTemplate || emptyTemplate, { settings: JSON.stringify(payload || {}) }),
        };
      } catch (err) {
        log.error(err);
        return {
          ...state,
          loading: false,
          error: 'Error setting Template Settings',
        };
      }
    case 'TEMPLATE_SET_TYPE':
      return {
        ...state,
        reportTemplate: Object.assign(state.reportTemplate || emptyTemplate, { type: payload }),
      };
    case 'TEMPLATE_SET_DIRECTION':
      return {
        ...state,
        direction: payload,
        reportTemplate: Object.assign(state.reportTemplate || emptyTemplate, { direction: payload }),
      };
    case 'TEMPLATE_SET_USED':
      return {
        ...state,
        used: payload,
      };
    case 'TEMPLATE_LOADING':
      return {
        ...state,
        loading: payload.loading,
      };
    case 'TEMPLATE_ERROR':
      return {
        ...state,
        loading: false,
        error: payload.error,
      };
    default:
      return state;
  }
};
