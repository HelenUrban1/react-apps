import { createSelector } from 'reselect';

export const selectRaw = (state: any) => state.template;

export const selectTemplate = createSelector(
  [selectRaw], //
  (template) => template.reportTemplate,
);

export const selectProvider = createSelector(
  [selectRaw], //
  (template) => template?.reportTemplate?.provider || {},
);

export const selectStep = createSelector(
  [selectRaw], //
  (template) => template.step,
);

export const selectSettings = createSelector(
  [selectRaw], //
  (template) => template.settings,
);
