import { ApolloClient } from '@apollo/client';
import { ReportTemplateAPI } from 'graphql/reportTemplate';
import OrganizationAPI from 'graphql/organization';
import { i18n } from 'i18n';
import { isEqual } from 'lodash';
import log from 'modules/shared/logger';
import { AppState, TemplateAction } from 'modules/state';
import { Epic, ActionsObservable, StateObservable } from 'redux-observable';
import { from } from 'rxjs';
import { filter, mergeMap, switchMap } from 'rxjs/operators';
import { isOfType } from 'typesafe-actions';

import TemplateActions from './templateActions';

export const saveTemplateEpic: Epic<TemplateAction, TemplateAction, AppState> = (action$: ActionsObservable<TemplateAction>, state$: StateObservable<AppState>, { clientPromise }) =>
  action$.pipe(
    filter(isOfType(TemplateActions.SAVE_TEMPLATE)),
    switchMap((action: TemplateAction) =>
      from<PromiseLike<ApolloClient<any>>>(clientPromise).pipe(
        switchMap((client: ApolloClient<any>) => {
          const { reportTemplate } = state$.value.template;
          const { additionalQueries, id, newData } = action.payload;
          const saveStatus = document.getElementById('excelSaveStatus');
          // TODO this should be an action received by a component, if possible
          if (saveStatus) saveStatus.innerHTML = i18n('common.saving');

          const refetchQueries = [
            {
              query: OrganizationAPI.query.list,
              variables: { filter: { status: 'Active' } },
            },
            {
              query: ReportTemplateAPI.query.find,
              variables: { id },
            },
          ];

          return from(
            client.mutate({
              mutation: ReportTemplateAPI.mutate.edit,
              variables: {
                id,
                data: newData,
              },
              refetchQueries: additionalQueries ? refetchQueries : [],
              awaitRefetchQueries: true,
              update: (cache: any, payload: any) => {
                const updateData = payload.data;
                if (saveStatus) saveStatus.innerHTML = i18n('common.saved');
                if (additionalQueries && updateData?.reportTemplateUpdate && reportTemplate) {
                  cache.modify({
                    id: cache.identify(updateData.reportTemplateUpdate),
                    fields: {
                      settings() {
                        return newData.settings;
                      },
                    },
                  });
                }
              },
            }),
          );
        }),
        mergeMap(() => {
          return [{ type: 'TEMPLATE_SET_SETTINGS', payload: { ...state$.value.template.settings, saved: true } }] as TemplateAction[];
        }),
      ),
    ),
  );

export const updateTemplateContextEpic: Epic<TemplateAction, TemplateAction, AppState> = (action$: ActionsObservable<TemplateAction>, state$: StateObservable<AppState>) =>
  action$.pipe(
    filter(isOfType(TemplateActions.UPDATE_TEMPLATE_CONTEXT)),
    switchMap((action: TemplateAction) => {
      // TODO we need to figure out a way to separate all of these wacky conditions into separate actions and handle them in other ways
      const { change, update } = action.payload;
      const { direction, id, org, partData, reportTemplate, step, tableData, settings, title, token, used } = state$.value.template;

      const emission = [];
      if (!change) return [] as TemplateAction[];

      let changedSettings = false;
      const newSettings = {
        tokens: settings && settings.tokens ? [...settings.tokens] : [],
        footers: settings && settings.footers ? { ...settings.footers } : {},
        saved: (settings && settings.saved) || false,
      };

      if (change.direction && change.direction !== direction) {
        emission.push({ type: 'TEMPLATE_SET_DIRECTION', payload: change.direction });
        Object.keys(newSettings.footers).forEach((sheet: string) => {
          Object.keys(newSettings.footers[sheet]).forEach((cell: string) => {
            newSettings.footers[sheet][cell].footer = -1;
          });
        });
        changedSettings = true;
      }

      // case in point from the above, direction alteration has to be made in a specific order or it'll be wrong in several cases
      if (change.reportTemplate && change.reportTemplate !== reportTemplate) {
        try {
          if (changedSettings) change.reportTemplate.settings = JSON.stringify(newSettings);
        } catch (err) {
          log.error(err);
          emission.push({ type: 'TEMPLATE_ERROR', payload: { error: 'Error Updating Template Sets' } });
        }
        emission.push({ type: 'TEMPLATE_SET_REPORT_TEMPLATE', payload: change.reportTemplate });
        if (change.reportTemplate.direction !== direction) {
          emission.push({ type: 'TEMPLATE_SET_DIRECTION', payload: change.reportTemplate.direction });
        }
      }

      if (change.direction && change.direction !== direction && reportTemplate) {
        emission.push({ type: 'TEMPLATE_SET_DIRECTION', payload: change.direction });
      }

      if (change.settings) {
        if (change.settings.footers) {
          newSettings.footers = change.settings.footers;
        }
        if (change.settings.tokens && !isEqual(change.settings.tokens, used)) {
          emission.push({ type: 'TEMPLATE_SET_USED', payload: change.settings.tokens });
          newSettings.tokens = change.settings.tokens;
        }
        if (change.settings.saved) {
          newSettings.saved = change.settings.saved;
        }
        if ((change.saved && settings?.saved !== change.saved) || !settings) {
          newSettings.saved = change.saved || false;
          emission.push({ type: 'TEMPLATE_SET_SETTINGS', payload: { ...state$.value.template, saved: change.saved || false } });
        }
        // tokens and footers and saved
        changedSettings = true;
      }

      if (change.tableData && change.tableData !== tableData) {
        emission.push({ type: 'TEMPLATE_SET_TABLE_DATA', payload: change.tableData });
      }
      if (change.partData && change.partData !== partData) {
        emission.push({ type: 'TEMPLATE_SET_PART_DATA', payload: change.partData });
      }
      if (change.step !== undefined && change.step !== null && change.step !== step) {
        emission.push({ type: 'TEMPLATE_SET_STEP', payload: change.step });
      }
      if (change.title && change.title !== title) {
        emission.push({ type: 'TEMPLATE_SET_TITLE', payload: change.title });
      }
      if (change.provider && reportTemplate?.provider) {
        emission.push({ type: 'TEMPLATE_SET_PROVIDER', payload: change.provider.id ? change.provider : { id: change.provider } });
      }
      if (change.type && reportTemplate) {
        emission.push({ type: 'TEMPLATE_SET_TYPE', payload: change.type });
      }
      if (change.id && change.id !== id) {
        emission.push({ type: 'TEMPLATE_SET_ID', payload: change.id });
      }
      if ((change.token || change.token === null) && change.token !== token) {
        emission.push({ type: 'TEMPLATE_SET_TOKEN', payload: change.token });
      }
      if (change.org && change.org !== org) {
        change.provider = change.org;
        emission.push({ type: 'TEMPLATE_SET_PROVIDER', payload: change.org.id ? change.org : { id: change.org } });
      }
      if (changedSettings) {
        emission.push({ type: 'TEMPLATE_SET_SETTINGS', payload: newSettings });
      }

      try {
        if (change.settings) {
          delete change.settings.saved;
          change.settings = JSON.stringify(change.settings);
        }
      } catch (err) {
        log.error(err);
        emission.push({ type: 'TEMPLATE_ERROR', payload: { error: 'Error Updating Template Settings' } });
      }

      const newData = reportTemplate ? ReportTemplateAPI.createGraphqlInput({ ...reportTemplate, ...change }) : null;
      if (!update && id && newData) {
        emission.push({ type: 'TEMPLATE_SAVE_TEMPLATE', payload: { newData, id } });
      } else if (update && change && (!change.id || change.id === id) && id && newData) {
        emission.push({ type: 'TEMPLATE_SAVE_TEMPLATE', payload: { newData, id, additionalQueries: true } });
      }

      return emission as TemplateAction[];
    }),
  );
