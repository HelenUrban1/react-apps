import gql from 'graphql-tag';
import getClient from 'modules/shared/graphql/graphqlClient';

export default class OperatorService {
  static async update(id, data) {
    const graphqlClient = await getClient();
    const response = await graphqlClient.mutate({
      mutation: gql`
        mutation OPERATOR_UPDATE($id: String!, $data: OperatorInput!) {
          operatorUpdate(id: $id, data: $data) {
            id
          }
        }
      `,

      variables: {
        id,
        data,
      },
    });

    return response.data.operatorUpdate;
  }

  static async create(data) {
    const graphqlClient = await getClient();
    const response = await graphqlClient.mutate({
      mutation: gql`
        mutation OPERATOR_CREATE($data: OperatorInput!) {
          operatorCreate(data: $data) {
            id
          }
        }
      `,

      variables: {
        data,
      },
    });

    return response.data.operatorCreate;
  }

  static async import(values, importHash) {
    const graphqlClient = await getClient();
    const response = await graphqlClient.mutate({
      mutation: gql`
        mutation OPERATOR_IMPORT($data: OperatorInput!, $importHash: String!) {
          operatorImport(data: $data, importHash: $importHash)
        }
      `,

      variables: {
        data: values,
        importHash,
      },
    });

    return response.data.operatorImport;
  }

  static async find(id) {
    const graphqlClient = await getClient();
    const response = await graphqlClient.query({
      query: gql`
        query OPERATOR_FIND($id: String!) {
          operatorFind(id: $id) {
            id
            account {
              id
              orgName
            }
            user {
              id
              fullName
              email
            }
            settings
            roles
            status
            createdAt
            updatedAt
          }
        }
      `,

      variables: {
        id,
      },
    });

    return response.data.operatorFind;
  }

  static async list(filter, orderBy, limit, offset) {
    const graphqlClient = await getClient();
    const response = await graphqlClient.query({
      query: gql`
        query OPERATOR_LIST($filter: OperatorFilterInput, $orderBy: OperatorOrderByEnum, $limit: Int, $offset: Int) {
          operatorList(filter: $filter, orderBy: $orderBy, limit: $limit, offset: $offset) {
            count
            rows {
              id
              fullName
              firstName
              lastName
              email
              status
              pin
              accessControl
              createdById
            }
          }
        }
      `,

      variables: {
        filter,
        orderBy,
        limit,
        offset,
      },
    });

    return response.data.operatorList;
  }

  static async listAutocomplete(query, limit) {
    const graphqlClient = await getClient();
    const response = await graphqlClient.query({
      query: gql`
        query OPERATOR_AUTOCOMPLETE($query: String, $limit: Int) {
          operatorAutocomplete(query: $query, limit: $limit) {
            id
            label
          }
        }
      `,

      variables: {
        query,
        limit,
      },
    });

    return response.data.operatorAutocomplete;
  }
}
