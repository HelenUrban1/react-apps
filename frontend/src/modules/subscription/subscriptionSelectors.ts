import { createSelector } from 'reselect';

export const selectRaw = (state: any) => state.subscription;

export const selectSubscription = createSelector(
  [selectRaw], //
  (subscription) => subscription.data,
);

export const selectCatalog = createSelector(
  [selectRaw], //
  (subscription) => subscription.catalog,
);

export const selectAuth = createSelector(
  [selectRaw], //
  (subscription) => subscription.auth,
);
