import moment from 'moment';
import { parseSubscriptionFields } from './subscriptionActions';

const MOCK_PRODUCT_CATALOG = require('./ix3_product-catalog-chargify.json');

const mockSubscription = {
  id: '07d26541-9294-41a4-969e-eb5ec077a176',
  accountId: 'e0a01769-c8ed-4090-bd24-9c1b2241fb75',
  billingId: 'e0a01779-c8ed-4090-bd44-9c1b2242fb75',
  companyId: '5788481594',
  activatedAt: '2020-05-04T13:44:32-04:00',
  providerProductId: 5233891,
  providerProductHandle: 'ix_standard',
  providerFamilyHandle: 'inspectionxpert-cloud',
  automaticallyResumeAt: null,
  balanceInCents: 375000,
  billingPeriod: 'Monthly',
  cancelAtEndOfPeriod: false,
  canceledAt: null,
  cancellationMessage: null,
  cancellationMethod: null,
  couponCode: null,
  // NOTE: Setting couponCodes to `[]` causes the Postgres error: "cannot determine type of empty array".
  //    There is a workaround in SubscriptionRepository that changes an empty array to `null`, but that
  //    does not affect seeding.
  // couponCodes: [],
  couponUseCount: null,
  couponUsesAllowed: null,
  createdAt: new Date(Date.now()),
  currentBillingAmountInCents: 387500,
  currentPeriodEndsAt: moment().add(1, 'years').toDate(),
  currentPeriodStartedAt: new Date(Date.now()),
  delayedCancelAt: null,
  expiresAt: moment().add(1, 'years').toDate(),
  netTerms: null,
  nextAssessmentAt: moment().add(1, 'years').toDate(),
  nextProductHandle: null,
  nextProductId: null,
  nextProductPricePointId: null,
  offerId: null,
  payerId: 37892060,
  paymentCollectionMethod: 'automatic',
  paymentSystemId: 37003269,
  paymentType: '25573100', // payment_profile_id
  previousStatus: 'active',
  productPriceInCents: 387500,
  drawingsUsed: 5,
  paidUserSeatsUsed: 27,
  partsUsed: 0,
  measurementsUsed: 0,
  pendingComponents: '[]',
  // components: '[1028184]',
  components: `[
    {
      "component_id": 1028184,
      "subscription_id": 37003269,
      "component_handle": "paid_user_seat_pu",
      "allocated_quantity": 30,
      "name": "User Seats (PU)",
      "kind": "quantity_based_component",
      "unit_name": "paid user seat",
      "pricing_scheme": "per_unit",
      "price_point_id": 961586,
      "price_point_handle": "current"
    }]`,
  // components: '[{"component_id":1028184,"subscription_id":36961356,"component_handle":"user_seat","allocated_quantity":30,"name":"User Seat","kind":"quantity_based_component","unit_name":"Paid User Seat","pricing_scheme":"per_unit","price_point_id":961586,"price_point_handle":"standard"}]',
  productPricePointId: 914840,
  productVersionNumber: 1,
  reasonCode: null,
  receivesInvoiceEmails: null,
  referralCode: null,
  signupPaymentId: 421144154,
  signupRevenue: '0.00',
  snapDay: null,
  status: 'active',
  storedCredentialTransactionId: null,
  totalRevenueInCents: 0,
  trialEndedAt: null,
  trialStartedAt: null,
  createdById: 'e0a01779-c8ed-4090-bd44-9c1b2242fb75',
  updatedById: 'e0a01779-c8ed-4090-bd44-9c1b2242fb75',
  updatedAt: new Date(Date.now()),
  cardId: 12345,
  maskedCardNumber: 'XXXX-XXXX-XXXX-3442',
  cardType: 'visa',
  expirationMonth: 9,
  expirationYear: 2021,
  customerId: '37892060',
  renewalDate: '2021-11-05T00:00:00-04:00',
  pendingTier: null,
  pendingBillingPeriod: null,
};

const mockComponents = [
  {
    component_id: 123,
    subscription_id: 123,
    component_handle: 'parts_ss',
    allocated_quantity: 6,
    name: 'Parts',
    kind: 'quantity_based_component',
    unit_name: 'parts_ss',
    pricing_scheme: 'stairstep',
    price_point_id: 123,
    price_point_handle: '123_monthly',
  },
];
const mockAddon = [
  {
    component_id: 123,
    subscription_id: 123,
    component_handle: 'parts_addon',
    allocated_quantity: 1,
    name: 'Parts',
    kind: 'quantity_based_component',
    unit_name: 'on/off',
    enabled: true,
    price_point_id: 123,
    price_point_handle: '123_monthly',
  },
];

describe('parseSubscriptionFields', () => {
  it('parses subscription components', () => {
    const { components } = parseSubscriptionFields({ ...mockSubscription, components: JSON.stringify(mockComponents) }, MOCK_PRODUCT_CATALOG);
    expect(components).toMatchObject(mockComponents);
  });

  it('parses subscription addons', () => {
    const { addons } = parseSubscriptionFields({ ...mockSubscription, components: JSON.stringify(mockAddon) }, MOCK_PRODUCT_CATALOG);
    expect(addons).toMatchObject({ parts: 1 });
  });

  it('sets subscription type', () => {
    const { subType: type1 } = parseSubscriptionFields(mockSubscription, MOCK_PRODUCT_CATALOG);
    expect(type1).toBe('Time');
    const { subType: type2 } = parseSubscriptionFields({ ...mockSubscription, components: JSON.stringify(mockComponents) }, MOCK_PRODUCT_CATALOG);
    expect(type2).toBe('Freemium');
  });

  it('sets subscription limit', () => {
    const { limit } = parseSubscriptionFields({ ...mockSubscription, components: JSON.stringify(mockComponents) }, MOCK_PRODUCT_CATALOG);
    expect(limit).toBe(6);
  });

  it('sets subscription state', () => {
    const { state: state1 } = parseSubscriptionFields({ ...mockSubscription, status: 'Active' }, MOCK_PRODUCT_CATALOG);
    expect(state1).toBe('active');
    const { state: state2 } = parseSubscriptionFields({ ...mockSubscription, status: 'trial_ended' }, MOCK_PRODUCT_CATALOG);
    expect(state2).toBe('trial_ended');
    const { state: state3 } = parseSubscriptionFields({ ...mockSubscription, status: 'expired' }, MOCK_PRODUCT_CATALOG);
    expect(state3).toBe('canceled');
    const { state: state4 } = parseSubscriptionFields({ ...mockSubscription, status: 'past_due' }, MOCK_PRODUCT_CATALOG);
    expect(state4).toBe('past_due');
    const { state: state5 } = parseSubscriptionFields({ ...mockSubscription, status: 'trialing' }, MOCK_PRODUCT_CATALOG);
    expect(state5).toBe('trial');
    const { state: state6 } = parseSubscriptionFields({ ...mockSubscription, status: 'trial_ended', paymentCollectionMethod: 'automattic', termsAcceptedOn: new Date(Date.now()) }, MOCK_PRODUCT_CATALOG);
    expect(state6).toBe('active');
    const { state: state7 } = parseSubscriptionFields({ ...mockSubscription, status: 'trial_ended', paymentCollectionMethod: 'remittance', termsAcceptedOn: new Date(Date.now()) }, MOCK_PRODUCT_CATALOG);
    expect(state7).toBe('pending');
    const { state: state8 } = parseSubscriptionFields({ ...mockSubscription, status: 'trialing', components: JSON.stringify(mockComponents), currentBillingAmountInCents: 5 }, MOCK_PRODUCT_CATALOG);
    expect(state8).toBe('active');
    const { state: state9 } = parseSubscriptionFields({ ...mockSubscription, cancelAtEndOfPeriod: true }, MOCK_PRODUCT_CATALOG);
    expect(state9).toBe('pend_cancel');
  });

  it('returns defaults if missing subscription or catalog', () => {
    const { components, state, subType } = parseSubscriptionFields();
    expect(components).toMatchObject([]);
    expect(state).toBe(true);
    expect(subType).toBe('Freemium');
  });
});
