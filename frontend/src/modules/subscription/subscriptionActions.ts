import { ApolloClient } from '@apollo/client';
import { Dispatch } from 'redux';
import { SubscriptionAPI, SubscriptionFind, SubscriptionFindVars, SubscriptionEdit, SubscriptionEditVars, SubscriptionInitBilling } from 'domain/setting/subscriptionApi';
import { getTrialLimit, isFreemiumPlan } from 'utils/SubscriptionUtils';
import { Catalog, Component, ComponentInput, Subscription, SubscriptionUpdateInput } from 'domain/setting/subscriptionTypes';
import { parseValidJSONString } from 'utils/textOperations';
import Analytics from 'modules/shared/analytics/analytics';
import log from 'modules/shared/logger';
import { User } from 'types/user';

const prefix = 'SUBSCRIPTION';

export const SubscriptionActions = {
  CLEAR: `${prefix}_CLEAR`,
  ERROR: `${prefix}_ERROR`,
  LOADING: `${prefix}_LOADING`,
  SUCCESS: `${prefix}_SUCCESS`,
  PAYMENT: `${prefix}_PAYMENT`,
  FAILURE: `${prefix}_FAILURE`,
  AUTH: `${prefix}_AUTH`,
  TRIAL_HIDE: `${prefix}_TRIAL_HIDE`,
};

export enum SubscriptionActionTypes {
  CLEAR = 'SUBSCRIPTION_CLEAR',
  LOADING = 'SUBSCRIPTION_LOADING',
  SUCCESS = 'SUBSCRIPTION_SUCCESS',
  PAYMENT = 'SUBSCRIPTION_PAYMENT',
  FAILURE = 'SUBSCRIPTION_FAILURE',
  ERROR = 'SUBSCRIPTION_ERROR',
  AUTH = 'SUBSCRIPTION_AUTH',
  TRIAL_HIDE = 'SUBSCRIPTION_TRIAL_HIDE',
}

export const parseSubscriptionFields = (subscription: Subscription, catalog: Catalog) => {
  // TODO: remove next line when all new accounts get a subscription
  if (!subscription || !catalog) return { components: [], state: true, subType: 'Freemium' };

  const components: Component[] = subscription.components ? parseValidJSONString(subscription.components) : [];
  const addons: { [key: string]: number } = {};
  components.forEach((comp) => {
    if (comp.component_handle?.includes('_addon')) {
      addons[comp.component_handle.replace('_addon', '')] = comp.allocated_quantity;
    }
  });

  let state = 'trial';
  let subType;
  switch (subscription.status?.toLowerCase()) {
    case 'active':
      state = 'active';
      break;

    case 'trial_ended':
      state = 'trial_ended';
      break;

    case 'canceled':
    case 'expired':
      state = 'canceled';
      break;

    case 'past_due':
    case 'unpaid':
      state = 'past_due';
      break;

    default:
      state = 'trial';
      break;
  }
  subType = 'Time';
  if ((subscription.status?.toLowerCase() === 'trialing' || subscription.status?.toLowerCase() === 'trial_ended') && subscription.termsAcceptedOn) {
    state = subscription.paymentCollectionMethod === 'remittance' ? 'pending' : 'active';
  } else if (isFreemiumPlan(subscription.billingPeriod || 'Annually', components, catalog[subscription.providerFamilyHandle || 'inspectionxpert-cloud']?.components || {})) {
    if (subscription.currentBillingAmountInCents && subscription.currentBillingAmountInCents > 0) {
      state = 'active';
    }
    subType = 'Freemium';
  }
  if (subscription.cancelAtEndOfPeriod) {
    state = 'pend_cancel';
  }
  const limit = getTrialLimit(subscription, components, subType === 'Freemium', catalog);
  return { components, addons, state, limit, subType };
};

export const doAddPayment =
  ({ id, accountId, customerId, billingToken }: { id: string; accountId: string; customerId: string; billingToken: string }) =>
  async (dispatch: Dispatch, getState: any, clientPromise: Promise<ApolloClient<any>>) => {
    return clientPromise.then(async (client) => {
      try {
        dispatch({ type: SubscriptionActions.LOADING });
        const { data, errors } = await client.mutate({
          mutation: SubscriptionAPI.mutate.addPaymentMethod,
          variables: {
            id,
            data: {
              accountId,
              billingToken,
              customerId,
            },
          },
        });
        if (data?.subscriptionBillingPaymentMethodCreate) {
          try {
            const sub = getState().subscription;
            const newSub = { ...sub.data, ...data.subscriptionBillingPaymentMethodCreate };
            const catalog: Catalog = sub?.catalog || {};
            const { components: newComponents, addons, state, limit, subType } = parseSubscriptionFields(newSub, catalog);
            dispatch({ type: SubscriptionActions.SUCCESS, payload: { data: newSub, state, type: subType, limit, components: newComponents, addons } });
          } catch (err: any) {
            log.error('SubscriptionActions.doAddPayment', err);
            dispatch({ type: SubscriptionActions.ERROR, payload: err.message });
          }
        } else if (errors) {
          log.error('SubscriptionActions.doAddPayment.elseif', errors[0]);
          dispatch({ type: SubscriptionActions.ERROR, payload: errors[0].message });
        } else {
          dispatch({ type: SubscriptionActions.FAILURE });
        }
      } catch (err: any) {
        log.error('SubscriptionActions.doAddPayment', err);
        dispatch({ type: SubscriptionActions.ERROR, payload: err.message });
      }
    });
  };

export const doRemovePayment =
  ({ id, customer }: { id: string; customer: string }) =>
  async (dispatch: Dispatch, getState: any, clientPromise: Promise<ApolloClient<any>>) => {
    return clientPromise.then(async (client) => {
      try {
        dispatch({ type: SubscriptionActions.LOADING });
        const { data, errors } = await client.mutate({
          mutation: SubscriptionAPI.mutate.removePaymentMethod,
          variables: {
            id,
            customer,
          },
        });
        if (data?.subscriptionBillingPaymentMethodRemove) {
          try {
            const sub = getState().subscription;
            const newSub = { ...sub.data, ...data.subscriptionBillingPaymentMethodRemove };
            const catalog: Catalog = sub?.catalog || {};
            const { components: newComponents, addons, state, limit, subType } = parseSubscriptionFields(newSub, catalog);
            dispatch({ type: SubscriptionActions.SUCCESS, payload: { data: newSub, state, type: subType, limit, components: newComponents, addons } });
          } catch (err: any) {
            log.error('SubscriptionActions.doRemovePayment', err);
            dispatch({ type: SubscriptionActions.ERROR, payload: err.message });
          }
        } else if (errors) {
          log.error('SubscriptionActions.doRemovePayment.elseif', errors[0]);
          dispatch({ type: SubscriptionActions.ERROR, payload: errors[0].message });
        } else {
          dispatch({ type: SubscriptionActions.FAILURE });
        }
      } catch (err: any) {
        log.error('SubscriptionActions.doRemovePayment', err);
        dispatch({ type: SubscriptionActions.ERROR, payload: err.message });
      }
    });
  };

export const doInitBilling = (accountId: string, currentUser?: User) => async (dispatch: Dispatch, getState: any, clientPromise: Promise<ApolloClient<any>>) => {
  return clientPromise.then(async (client) => {
    dispatch({ type: SubscriptionActions.LOADING });
    const { data, error } = await client.query<SubscriptionInitBilling, SubscriptionFindVars>({
      query: SubscriptionAPI.query.init,
      variables: {
        accountId,
      },
    });
    if (data?.subscriptionBillingInitData) {
      const catalog = parseValidJSONString(data?.subscriptionBillingInitData?.catalog);
      const auth = {
        publicKey: data.subscriptionBillingInitData?.publicKey,
        siteUrl: data.subscriptionBillingInitData?.siteUrl,
        securityToken: data.subscriptionBillingInitData?.securityToken,
      };
      if (data.subscriptionBillingInitData?.subscription) {
        dispatch({ type: SubscriptionActions.AUTH, payload: { auth, catalog, loading: true } });
        try {
          const { components, addons, state, limit, subType } = parseSubscriptionFields(data.subscriptionBillingInitData.subscription, catalog);
          dispatch({ type: SubscriptionActions.SUCCESS, payload: { data: data.subscriptionBillingInitData?.subscription, state, type: subType, limit, components, addons } });
        } catch (err: any) {
          Analytics.track({
            event: Analytics.events.subscriptionBillingInitParseError,
            subscription: data.subscriptionBillingInitData.subscription,
            error: err,
          });
          log.error('SubscriptionActions.doInitBilling', err);
          dispatch({ type: SubscriptionActions.ERROR, payload: err.message });
        }
      } else {
        dispatch({ type: SubscriptionActions.AUTH, payload: { auth, catalog, loading: false } });
      }
    } else if (error) {
      Analytics.track({
        event: Analytics.events.subscriptionBillingInitError,
        error,
      });
      log.error('SubscriptionActions.doInitBilling.elseif', error);
      dispatch({ type: SubscriptionActions.ERROR, payload: error.message });
    } else {
      Analytics.track({
        event: Analytics.events.subscriptionBillingInitFailure,
      });
      dispatch({ type: SubscriptionActions.FAILURE });
    }
  });
};

export const doSetSubscription = (accountId: string) => async (dispatch: Dispatch, getState: any, clientPromise: Promise<ApolloClient<any>>) => {
  return clientPromise.then(async (client) => {
    dispatch({ type: SubscriptionActions.LOADING });
    const { data, error } = await client.query<SubscriptionFind, SubscriptionFindVars>({
      query: SubscriptionAPI.query.find,
      fetchPolicy: 'network-only',
      variables: {
        accountId,
      },
    });
    if (data?.subscriptionFind) {
      try {
        const catalog: Catalog = getState().subscription?.catalog || {};
        const { components, addons, state, limit, subType } = parseSubscriptionFields(data.subscriptionFind, catalog);
        dispatch({ type: SubscriptionActions.SUCCESS, payload: { data: data.subscriptionFind, state, type: subType, limit, components, addons } });
      } catch (err: any) {
        log.error('SubscriptionActions.doSetSubscription', err);
        dispatch({ type: SubscriptionActions.ERROR, payload: err.message });
      }
    } else if (error) {
      log.error('SubscriptionActions.doSetSubscription.elseif', error);
      dispatch({ type: SubscriptionActions.ERROR, payload: error.message });
    } else {
      dispatch({ type: SubscriptionActions.FAILURE });
    }
  });
};

export const doUpdateCurrentSubscription =
  (id: string, updatedSubscription: SubscriptionUpdateInput, type: string, components?: ComponentInput[], billingToken?: string, handle?: string) => async (dispatch: Dispatch, getState: any, clientPromise: Promise<ApolloClient<any>>) => {
    return clientPromise.then(async (client) => {
      try {
        dispatch({ type: SubscriptionActions.LOADING });
        const { data, errors } = await client.mutate<SubscriptionEdit, SubscriptionEditVars>({
          mutation: SubscriptionAPI.mutate.edit,
          variables: {
            id,
            data: updatedSubscription,
            type,
            components,
            componentHandle: handle,
            billingToken,
          },
        });
        if (data?.subscriptionUpdate) {
          try {
            const catalog: Catalog = getState().subscription?.catalog || {};
            const { components: newComponents, addons, state, limit, subType } = parseSubscriptionFields(data.subscriptionUpdate, catalog);
            dispatch({ type: SubscriptionActions.SUCCESS, payload: { data: data.subscriptionUpdate, state, type: subType, limit, components: newComponents, addons } });
          } catch (err: any) {
            log.error('SubscriptionActions.doUpdateCurrentSubscription', err);
            dispatch({ type: SubscriptionActions.ERROR, payload: err.message });
          }
        } else if (errors) {
          log.error('SubscriptionActions.doUpdateCurrentSubscription.elseif', errors[0]);
          dispatch({ type: SubscriptionActions.ERROR, payload: errors[0].message });
        } else {
          dispatch({ type: SubscriptionActions.FAILURE });
        }
      } catch (err: any) {
        log.error('SubscriptionActions.doUpdateCurrentSubscription', err);
        dispatch({ type: SubscriptionActions.ERROR, payload: err.message });
      }
    });
  };

export default SubscriptionActions;
