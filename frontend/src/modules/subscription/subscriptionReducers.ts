import './subscriptionActions';
import { SubscriptionState, SubscriptionAction } from '../state.d';

const initialData: SubscriptionState = {
  loading: false,
  error: null,
  data: null,
  catalog: {},
  components: [],
  addons: {},
  type: null,
  limit: false,
  state: null,
  show: true,
  auth: null,
};

export default (state = initialData, { type, payload }: SubscriptionAction): SubscriptionState => {
  switch (type) {
    case 'SUBSCRIPTION_CLEAR':
      return {
        ...initialData,
      };
    case 'SUBSCRIPTION_LOADING':
      return {
        ...state,
        loading: true,
        error: null,
      };
    case 'SUBSCRIPTION_AUTH':
      return {
        ...state,
        loading: payload.loading,
        error: null,
        auth: payload.auth,
        catalog: payload.catalog,
      };
    case 'SUBSCRIPTION_SUCCESS':
      return {
        ...state,
        loading: false,
        data: payload?.data,
        type: payload?.type,
        limit: payload?.limit,
        state: payload?.state,
        components: payload?.components,
        addons: payload?.addons,
        show: true,
      };
    case 'SUBSCRIPTION_PAYMENT':
      return {
        ...state,
        loading: false,
        data: { ...state.data, ...payload?.data },
      };
    case 'SUBSCRIPTION_FAILURE':
      return {
        ...state,
        loading: false,
        error: null,
        data: null,
        type: null,
        limit: false,
        state: null,
        show: false,
      };
    case 'SUBSCRIPTION_ERROR':
      return {
        ...state,
        error: payload,
        loading: false,
        data: null,
        type: null,
        limit: false,
        state: null,
        show: false,
      };
    case 'SUBSCRIPTION_TRIAL_HIDE':
      return {
        ...state,
        show: false,
      };
    default:
      return state;
  }
};
