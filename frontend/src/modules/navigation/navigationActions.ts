import { Parts } from 'domain/part/partApi';
import Analytics from 'modules/shared/analytics/analytics';
import { AppState } from 'modules/state';
import { Dispatch } from 'redux';
import config from 'config';

export enum PartStepEnum {
  PART = 0,
  DOCS = 1,
  TOLS = 2,
  GRID = 3,
  STYLES = 4,
  EXTRACT = 5,
  REVIEW = 6,
  TEMPLATE = 7,
  REPORT = 8,
}

const prefix = 'NAVIGATION';
const PART_EDITOR_STEP_NAMES = Parts.step.order;

export const NavigationActions = {
  SET_CURRENT_PART_STEP: `${prefix}_SET_CURRENT_PART_STEP`,
  SET_MAX_PART_STEP: `${prefix}_SET_MAX_PART_STEP`,
  SET_MAX_TOTAL_STEPS: `${prefix}_SET_MAX_TOTAL_STEPS`,
  RESET: `${prefix}_RESET`,
};

export enum NavigationActionTypes {
  SET_CURRENT_PART_STEP = 'NAVIGATION_SET_CURRENT_PART_STEP',
  SET_MAX_PART_STEP = 'NAVIGATION_SET_MAX_PART_STEP',
  SET_MAX_TOTAL_STEPS = 'NAVIGATION_SET_MAX_TOTAL_STEPS',
  RESET = 'NAVIGATION_RESET',
}
export const updateCurrentPartStep = (payload: number) => ({ type: NavigationActions.SET_CURRENT_PART_STEP, payload });
export const updateMaxPartStep = (payload: number) => ({ type: NavigationActions.SET_MAX_PART_STEP, payload });
export const updateMaxTotalSteps = (payload: number) => ({ type: NavigationActions.SET_MAX_TOTAL_STEPS, payload });

export const updatePartStepThunk = (partStep: number) => async (dispatch: Dispatch, getState: () => AppState) => {
  const state = getState();
  let step = partStep;
  // Only update the navigation if it changed
  // Max starts at index 0
  const maxTotalSteps = state.session.settings.autoballoonEnabled?.value === 'true' ? config.autoBallooning.maxTotalSteps : config.autoBallooning.maxTotalSteps - 1;
  let max: number = state.navigation.maxPartStep ? state.navigation.maxPartStep : 0;
  // If currentPartStep is higher than max, the stage is being entered for the first time
  if (step > max) {
    // Record the first time each step is entered
    Analytics.track({
      event: Analytics.events.partStepStarted,
      eventNameSuffix: ` [${step}] ${PART_EDITOR_STEP_NAMES[step]}`,
      properties: {
        'editor.step': `[${step}] ${PART_EDITOR_STEP_NAMES[step]}`,
      },
    });
    // Make the new max, the current step
    max = step;
    // if autoballooning gets turned off after their state is stored.
    if (max > maxTotalSteps - 1) {
      max = maxTotalSteps - 1;
      step = max;
    }
  }
  // update the values in redux
  dispatch(updateCurrentPartStep(step));
  dispatch(updateMaxPartStep(max));
};
