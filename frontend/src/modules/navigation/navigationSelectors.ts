import { createSelector } from 'reselect';
import { NavigationState } from '../state.d';

const selectRaw = (state: { navigation: NavigationState }) => state.navigation;
