import { NavigationAction, NavigationState } from 'modules/state';

const initialState = {
  currentPartStep: -1,
  maxPartStep: -1,
  // sets the max steps based on if autoballooning is on
  maxTotalSteps: -1,
};

export default (state: NavigationState = initialState, { type, payload }: NavigationAction): NavigationState => {
  switch (type) {
    case 'NAVIGATION_SET_CURRENT_PART_STEP':
      return { ...state, currentPartStep: payload };

    case 'NAVIGATION_SET_MAX_PART_STEP':
      return { ...state, maxPartStep: payload };

    case 'NAVIGATION_SET_MAX_TOTAL_STEPS':
      return { ...state, maxTotalSteps: payload };

    case 'NAVIGATION_RESET':
      return { ...state, ...initialState };
    default:
      return state;
  }
};
