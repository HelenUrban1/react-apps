import { Catalog, Component, Subscription } from 'domain/setting/subscriptionTypes';
import { Organization } from 'graphql/organization';
import { Marker, MarkerStyle } from 'types/marker';
import { SettingState } from 'domain/setting/settingTypes';
import { ListObject } from 'domain/setting/listsTypes';
import { Report, ReportTemplate, ReportTemplateSettings, TokenType } from 'types/reportTemplates';
import { CurrentUser, User, UserStatusEnum } from 'types/user';
import { SheetData } from 'styleguide/Excel/types';
import { DrawingSheet } from 'graphql/drawing_sheet';
import { Part, PartRevision, PartTableFilter, PartTableSorter } from 'domain/part/partTypes';
import { Notification, NotificationTableFilter, NotificationTableSorter } from 'domain/setting/notification/notificationTypes';
import { Sample } from 'types/sample';
import { Measurement } from 'types/measurement';
import { Station } from 'types/station';
import { Drawing } from 'graphql/drawing';
import { Job } from 'domain/jobs/jobTypes';
import { CharacteristicActionTypes, CapturedItem, Characteristic } from 'types/characteristics';
import { Tolerance } from 'types/tolerances';
import { Grid } from 'domain/part/edit/grid/Grid';
import { GridCanvasRenderer } from 'domain/part/edit/grid/GridRenderer';
import { Annotation, AnnotationActionTypes } from 'types/annotation';
import { SubscriptionActionTypes } from './subscription/subscriptionActions';
import { SessionActionTypes } from './session/sessionActions';

/* Typescript Definitions */
type getState = () => AppState;

// Subscription

export interface SubscriptionAction {
  type: SubscriptionActionTypes;
  payload: any; // TODO don't use any
}

export interface SubscriptionState {
  loading: boolean;
  error: string | null;
  data: Subscription | null;
  catalog: Catalog;
  components: Component[];
  addons: { [key: string]: number };
  type: string | null;
  limit: number | boolean;
  state: string | null;
  show: boolean;
  auth: {
    publicKey: string;
    securityToken: string;
    siteUrl: string;
  } | null;
}

// Characteristics

export interface CharacteristicsAction {
  type: CharacteristicActionTypes;
  payload: any;
}

export interface AnnotationsAction {
  type: AnnotationActionTypes;
  payload: any;
}

export interface CharacteristicsState {
  characteristics: Characteristic[];
  inactiveCharacteristics: Characteristic[];
  annotations: Annotation[];
  markers: Marker[];
  defaultMarker: Marker | null;
  lastLabel: null | number;
  capturedItems: CapturedItem[][];
  selected: (Characteristic | Annotation)[];
  count: null | number;
  mode: string;
  singleCustom: boolean;
  verified: null | number;
  index: number;
  previousChangeTime: Date | null;
  previousCount: number;
  previousHRCount: number;
  loaded: boolean;
  parsed: boolean;
  error: string | null;
  partId: string;
  compareRevision: Characteristic | null;
  compareIndex: number;
}

// Reports

type ReportActionTypes =
  | 'REPORT_SET_STEP'
  | 'REPORT_CLEAR_REPORT'
  | 'REPORT_SET_FILE'
  | 'REPORT_LOAD_REPORT'
  | 'REPORT_UPDATE_REPORT_CONTEXT'
  | 'REPORT_SET_REPORT'
  | 'REPORT_SET_PART'
  | 'REPORT_SET_TEMPLATE'
  | 'REPORT_SET_ORG'
  | 'REPORT_SET_SWAP'
  | 'REPORT_SET_DELETE'
  | 'REPORT_LOADING'
  | 'REPORT_ERROR'
  | 'REPORT_LOAD';

export interface ReportAction {
  type: ReportActionTypes;
  payload: any; // TODO don't use any
}

export interface ReportState {
  source: string | null;
  report: Report | null;
  reportFile: string | null;
  reportTemplate: ReportTemplate | null;
  data: SheetData[] | null;
  partData: Part | null;
  step: number;
  ID: string | null;
  org: string | null;
  swap: boolean;
  deleteCurrentReport: boolean;
  loading: boolean;
  error: string | null;
}

// Templates

type TemplateActionTypes =
  | 'TEMPLATE_CREATE_TEMPLATE'
  | 'TEMPLATE_UPDATE_TEMPLATE_CONTEXT'
  | 'TEMPLATE_SET_SETTINGS'
  | 'TEMPLATE_CLEAR_TEMPLATE'
  | 'TEMPLATE_SAVE_TEMPLATE'
  | 'TEMPLATE_SET_SAVED'
  | 'TEMPLATE_SET_STEP'
  | 'TEMPLATE_SET_TABLE_DATA'
  | 'TEMPLATE_SET_ID'
  | 'TEMPLATE_SET_TEMPLATE'
  | 'TEMPLATE_SET_TITLE'
  | 'TEMPLATE_SET_PROVIDER'
  | 'TEMPLATE_SET_DIRECTION'
  | 'TEMPLATE_SET_TYPE'
  | 'TEMPLATE_SET_USED'
  | 'TEMPLATE_LOADING'
  | 'TEMPLATE_ERROR';

interface TemplateAction {
  type: TemplateActionTypes;
  payload: any; // TODO don't use any
}

export interface TemplateState {
  reportTemplate: ReportTemplate | null;
  tableData: SheetData[] | null;
  partData: Part | null;
  createNew: boolean;
  step: number;
  title?: string;
  id: string | null;
  direction: 'Vertical' | 'Horizontal' | undefined;
  token?: TokenType | null;
  saved?: boolean;
  used?: string[];
  settings?: ReportTemplateSettings | null;
  org: string | null;
  createdAt?: Date;
  updatedAt?: Date;
  loading: boolean;
  error: string | null;
}

// Sessions
interface SessionAction {
  type: SessionActionTypes;
  payload: any; // TODO don't use any
}

export interface SessionState {
  user: User | null;
  accountMembership: { id: string; accountId?: string; siteId?: string; accessControl: boolean; status: UserStatusEnum } | null;
  types: ListObject | null;
  classifications: ListObject | null;
  methods: ListObject | null;
  operations: ListObject | null;
  imperialUnits: ListObject | null;
  metricUnits: ListObject | null;
  filterList: { [key: string]: ListObject };
  markers: Marker[] | null;
  organizations: { all: Organization[]; self: Organization | undefined };
  newOrg: Organization | null;
  settings: SettingState;
  userSettings: SettingState;
  sessionLoaded: boolean;
}

// Part

type PartActionTypes =
  | 'PART_SET_PART_STATE'
  | 'PART_SET_PART'
  | 'PART_SET_PARTS'
  | 'PART_SET_REVISIONS'
  | 'PART_SET_REVISION_INDEX'
  | 'PART_SET_ORIGINAL_PART'
  | 'PART_SET_PREVIOUS_PART'
  | 'PART_SET_NEW_PART'
  | 'PART_SET_TABLE_PAGE'
  | 'PART_SET_TABLE_FILTER'
  | 'PART_SET_TABLE_SORTER'
  | 'PART_SET_LOADED'
  | 'PART_SET_ERROR'
  | 'PART_RESET'
  | 'PART_RESET_TABLE';

interface PartAction {
  type: PartActionTypes;
  payload: any; // TODO don't use any
}
export interface PartState {
  newPart: Part | null;
  part: Part | null;
  revisions: { [key: string]: PartRevision };
  revisionIndex: number;
  revisionType: 'part' | 'drawing' | 'feature' | null;
  parts: Part[] | [];
  partEditor: PartEditorState | null;
  originalPart: Part | null;
  previousPart: Part | null;
  tablePage: number;
  tableFilter: PartTableFilter | null;
  tableSorter: PartTableSorter | null;
  partLoaded: boolean;
  partError: any | null;
}

// PDF View

type PdfViewActionType =
  | 'PDF_VIEW_SET_STATE'
  | 'PDF_VIEW_SET_TRON'
  | 'PDF_VIEW_SET_TRON_OVERLAY'
  | 'PDF_VIEW_SET_ROTATION'
  | 'PDF_VIEW_SET_ZOOM'
  | 'PDF_VIEW_SET_INDEX'
  | 'PDF_VIEW_SET_DIMS'
  | 'PDF_VIEW_SET_FILE_URL'
  | 'PDF_VIEW_RESET'
  | 'PDF_VIEW_SET_LOADED';

interface PdfViewAction {
  type: PdfViewActionType;
  payload: any;
}

export interface PdfViewState {
  height: number;
  width: number;
  zoom: number;
  index: number;
  rotation: number;
  layerManager: CanvasLayerManager;
  pages: PdfPageProps[];
  tronInstance: WebViewerInstance | null;
  fileUrl: string | null;
  pdfLoaded: boolean;
}

// PDF Overlay View

type PdfOverlayViewActionType =
  | 'PDF_OVERLAY_VIEW_SET_STATE'
  | 'PDF_OVERLAY_VIEW_SET_TRON'
  | 'PDF_OVERLAY_VIEW_SET_ROTATION'
  | 'PDF_OVERLAY_VIEW_SET_ZOOM'
  | 'PDF_OVERLAY_VIEW_SET_INDEX'
  | 'PDF_OVERLAY_VIEW_SET_DIMS'
  | 'PDF_OVERLAY_VIEW_SET_FILE'
  | 'PDF_OVERLAY_VIEW_RESET'
  | 'PDF_OVERLAY_VIEW_SET_LOADED';

interface PdfOverlayViewAction {
  type: PdfOverlayViewActionType;
  payload: any;
}

export interface PdfOverlayViewState {
  height: number;
  width: number;
  zoom: number;
  index: number;
  rotation: number;
  layerManager: CanvasLayerManager;
  pages: PdfPageProps[];
  tronInstance: WebViewerInstance | null;
  revisionFile: File | null;
  revisionFiles: FileObject[] | null;
  pdfLoaded: boolean;
}

// Part Editor

type PartEditorActionTypes =
  | 'PART_EDITOR_SET_STATE'
  | 'PART_EDITOR_SET_LOADED'
  | 'PART_EDITOR_SET_ERROR'
  | 'PART_EDITOR_SET_DRAWING'
  | 'PART_EDITOR_SET_COMPARE'
  | 'PART_EDITOR_SET_SHEET'
  | 'PART_EDITOR_UPDATE_DRAWING_SHEET'
  | 'PART_EDITOR_SET_ASSIGNED_STYLES'
  | 'PART_EDITOR_SET_PRESETS'
  | 'PART_EDITOR_SET_TOLERANCES'
  | 'PART_EDITOR_SET_GRID_CACHE'
  | 'PART_EDITOR_SET_FOCUS'
  | 'PART_EDITOR_RESET_STATE';

interface PartEditorAction {
  type: PartEditorActionTypes;
  payload: any; // TODO don't use any
}

export interface PartEditorState {
  partDrawing: Drawing | null;
  partDrawingSheet: DrawingSheet | null;
  compareDrawing: Drawing | null;
  compareDrawingSheet: DrawingSheet | null;
  compareMode: 'Align' | 'Compare' | null;
  targetDrawing: string | null;
  gridRenderer: GridCanvasRenderer;
  gridCache: Grid[] | null;
  tolerances: Tolerance;
  usedStyles: null | string[];
  defaultMarkerOptions: null | string[];
  assignedStyles: null | { [key: string]: MarkerStyle };
  presets: null | { [key: string]: string };
  focus?: string | null;
  nextBalloonNumber: null | number;
  partEditorMessage: null | string;
  partEditorError: any | null;
  partEditorLoaded: boolean;

  // Lets us access values by part[variable]
  [key: string]: null | Part | Tolerance | Function | Drawing | DrawingSheet | GridCanvasRenderer | Grid | Grid[] | boolean | string | string[] | number | boolean | { [key: string]: MarkerStyle | string };
}

// Navigation
type NavigationActionTypes = 'NAVIGATION_SET_CURRENT_PART_STEP' | 'NAVIGATION_SET_MAX_PART_STEP' | 'NAVIGATION_SET_MAX_TOTAL_STEPS' | 'NAVIGATION_RESET';
interface NavigationAction {
  type: NavigationActionTypes;
  payload: number;
}
export interface NavigationState {
  currentPartStep: number;
  maxPartStep: number;
  maxTotalSteps: number;
}

// Auth

interface AuthState {
  accountCreated: boolean;
  accountCreating: boolean;
  currentUser: CurrentUser | null;
  jwt: string | null;
  restricted: boolean;
  loadingInit: boolean;
  loadingEmailConfirmation: boolean;
  loadingPasswordResetEmail: boolean;
  loadingVerifyEmail: boolean;
  loadingPasswordReset: boolean;
  loadingUpdateProfile: boolean;
  loadingMfaVerification: boolean;
  loadingMfaRegistration: boolean;
  loading: boolean;
  errorMessage: any;
  passwordExists: boolean;
  requestPolicy: any;
  requestSuccess: boolean;
  mfaEnabled: boolean;
  mfaRegistered: boolean;
  session: string | null;
}

// Account

type AccountActionTypes = 'ACCOUNT_START' | 'ACCOUNT_SUCCESS' | 'ACCOUNT_ERROR';
interface AccountAction {
  type: AccountActionTypes;
  payload: any; // TODO don't use any
}
interface AccountState {
  id: string | null;
  status: string | null;
  settings: { [key: string]: string } | null;
  orgName: string | null;
  orgPhone: string | null;
  orgEmail: string | null;
  orgStreet: string | null;
  orgStreet2: string | null;
  orgCity: string | null;
  orgRegion: string | null;
  orgPostalcode: string | null;
  orgCountry: string | null;
  orgLogo: File[] | null;
  orgUnitName: string | null;
  orgUnitId: string | null;
  parentAccountId: string | null;
  members: string | null;
  createdAt: string | null;
  updatedAt: string | null;
  loading: boolean | null;
  errorMessage: any | null;
  dbHost: string | null;
  dbName: string | null;
}

// Notification

type NotificationActionTypes =
  | 'NOTIFICATION_SET_NOTIFICATION'
  | 'NOTIFICATION_SET_SELECTED_NOTIFICATIONS'
  | 'NOTIFICATION_SET_TABLE_PAGE'
  | 'NOTIFICATION_SET_TABLE_FILTER'
  | 'NOTIFICATION_SET_TABLE_SORTER'
  | 'NOTIFICATION_SET_UNREAD_COUNT'
  | 'NOTIFICATION_SET_UNREAD_NOTIFICATION_PREVIEW';
export interface NotificationAction {
  type: NotificationActionTypes;
  payload: any; // TODO don't use any
}
export interface NotificationState {
  notification: Notification | null;
  selectedNotifications: Notification[];
  tablePage: number;
  tableFilter: NotificationTableFilter | null;
  tableSorter: NotificationTableSorter | null;
  unreadCount: number;
  unreadNotificationPreview: Notification[];
}

// Metro

type MetroActionTypes =
  | 'METRO_DO_INIT_METRO'
  | 'METRO_SET_OPERATOR'
  | 'METRO_SET_STATION'
  | 'METRO_SET_STATIONS'
  | 'METRO_FETCH_SAMPLES'
  | 'METRO_SET_JOB'
  | 'METRO_SET_INSTALLED'
  | 'METRO_MEASUREMENT_CREATE_ERROR'
  | 'METRO_MEASUREMENT_CREATE'
  | 'METRO_DO_SELECT_CELL'
  | 'METRO_SET_GAGE';

interface MetroAction {
  type: MetroActionTypes;
  payload: any; // TODO don't use any
}

interface SelectedCell {
  featureIndex: number;
  sampleIndex: number;
  measurementIndex: number;
  selectedFeature: Characteristic | null;
  selectedSample: Sample | null;
  selectedMeasurement: Measurement | null;
}

export interface MetroState {
  station: Station | null;
  stations: Station[];
  operator: any;
  operators: any[];
  job: Job | null;
  drawing: Drawing | null;
  samples: Sample[] | null;
  installed: boolean;
  initialized: boolean;
  selectedCell: SelectedCell;
  gage: string | null;
}

// Report Template

export enum ReportTemplateActionTypes {
  CLEAR = 'REPORT_TEMPLATE_CLEAR',
  SAVED = 'REPORT_TEMPLATE_SAVED',
  LOADING = 'REPORT_TEMPLATE_LOADING',
  ERROR = 'REPORT_TEMPLATE_ERROR',
  WARNING = 'REPORT_TEMPLATE_WARNING',
  SET_MODE = 'REPORT_TEMPLATE_SET_MODE',
  SET_REPORT = 'REPORT_TEMPLATE_SET_REPORT',
  SET_TEMPLATE = 'REPORT_TEMPLATE_SET_TEMPLATE',
  SET_STEP = 'REPORT_TEMPLATE_SET_STEP',
  SET_MAX = 'REPORT_TEMPLATE_SET_MAX',
  SET_NAV = 'REPORT_TEMPLATE_SET_NAV',
  SET_NEW_TEMPLATE = 'REPORT_TEMPLATE_SET_NEW_TEMPLATE',
  SET_NEW_REPORT = 'REPORT_TEMPLATE_SET_NEW_REPORT',
  SET_DATA = 'REPORT_TEMPLATE_SET_DATA',
  SET_FILTERS = 'REPORT_TEMPLATE_SET_FILTERS',
  SET_ID = 'REPORT_TEMPLATE_SET_ID',
  SET_SOURCE = 'REPORT_TEMPLATE_SET_SOURCE',
}
export interface ReportTemplateState {
  mode: 'report' | 'template' | null;
  report: Report | null;
  template: ReportTemplate | null;
  settings: ReportTemplateSettings;
  step: number;
  max: number;
  source: string | null;
  data: SheetData[] | null;
  filters: {
    [key: string]: { category: string[] | undefined; value: any };
  };
  id: string | null;
  saved: boolean;
  loading: boolean;
  warning: any | null;
  error: any | null;
}

interface ReportTemplateAction {
  type: ReportTemplateActionTypes;
  payload: any; // TODO don't use any
}

// App

export interface AppState {
  auth: AuthState;
  characteristics: CharacteristicsState;
  part: PartState;
  pdfView: PdfViewState;
  pdfOverlayView: PdfOverlayViewState;
  partEditor: PartEditorState;
  reportTemplates: ReportTemplateState;
  report: ReportState;
  session: SessionState;
  subscription: SubscriptionState;
  template: TemplateState;
  account: AccountState;
  navigation: NavigationState;
  metro: MetroState;
  notification: NotificationState;
}
