import gql from 'graphql-tag';
import getClient from 'modules/shared/graphql/graphqlClient';

export default class OrganizationService {
  static async createDefault(data) {
    const graphqlClient = await getClient();
    const response = await graphqlClient.mutate({
      mutation: gql`
        mutation ORGANIZATION_CREATE_DEFAULT($data: OrganizationDefaultInput!) {
          organizationCreateDefault(data: $data) {
            id
          }
        }
      `,

      variables: {
        data,
      },
    });

    return response.data.organizationDefaultCreate;
  }
}
