import { i18n } from 'i18n';
import IdField from 'modules/shared/fields/idField';
import DateTimeField from 'modules/shared/fields/dateTimeField';
import DateTimeRangeField from 'modules/shared/fields/dateTimeRangeField';
import StringField from 'modules/shared/fields/stringField';
import EnumeratorField from 'modules/shared/fields/enumeratorField';
import ImagesField from 'modules/shared/fields/imagesField';
import RelationToManyField from 'modules/shared/fields/relationToManyField';

function label(name) {
  return i18n(`entities.organization.fields.${name}`);
}

function enumeratorLabel(name, value) {
  return i18n(`entities.organization.enumerators.${name}.${value}`);
}

const fields = {
  id: new IdField('id', label('id')),
  status: new EnumeratorField('status', label('status'), [
    { id: 'Active', label: enumeratorLabel('status', 'Active') },
    { id: 'Past_Due', label: enumeratorLabel('status', 'Past_Due') },
    { id: 'Suspended', label: enumeratorLabel('status', 'Suspended') },
    { id: 'Archived', label: enumeratorLabel('status', 'Archived') },
  ]),
  settings: new StringField('settings', label('settings')),
  name: new StringField('name', label('name'), {
    required: true,
  }),
  phone: new StringField('phone', label('phone'), {}),
  email: new StringField('email', label('email')),
  street1: new StringField('street1', label('street1'), {}),
  street2: new StringField('street2', label('street2'), {}),
  city: new StringField('city', label('city'), {}),
  region: new EnumeratorField(
    'region',
    label('region'),
    [
      { id: 'Alabama', label: enumeratorLabel('region', 'Alabama') },
      { id: 'Alaska', label: enumeratorLabel('region', 'Alaska') },
      { id: 'American_Samoa', label: enumeratorLabel('region', 'American_Samoa') },
      { id: 'Arizona', label: enumeratorLabel('region', 'Arizona') },
      { id: 'Arkansas', label: enumeratorLabel('region', 'Arkansas') },
      { id: 'California', label: enumeratorLabel('region', 'California') },
      { id: 'Colorado', label: enumeratorLabel('region', 'Colorado') },
      { id: 'Connecticut', label: enumeratorLabel('region', 'Connecticut') },
      { id: 'Delaware', label: enumeratorLabel('region', 'Delaware') },
      { id: 'District_of_Columbia', label: enumeratorLabel('region', 'District_of_Columbia') },
      { id: 'Florida', label: enumeratorLabel('region', 'Florida') },
      { id: 'Georgia', label: enumeratorLabel('region', 'Georgia') },
      { id: 'Guam', label: enumeratorLabel('region', 'Guam') },
      { id: 'Hawaii', label: enumeratorLabel('region', 'Hawaii') },
      { id: 'Idaho', label: enumeratorLabel('region', 'Idaho') },
      { id: 'Illinois', label: enumeratorLabel('region', 'Illinois') },
      { id: 'Indiana', label: enumeratorLabel('region', 'Indiana') },
      { id: 'Iowa', label: enumeratorLabel('region', 'Iowa') },
      { id: 'Kansas', label: enumeratorLabel('region', 'Kansas') },
      { id: 'Kentucky', label: enumeratorLabel('region', 'Kentucky') },
      { id: 'Louisiana', label: enumeratorLabel('region', 'Louisiana') },
      { id: 'Maine', label: enumeratorLabel('region', 'Maine') },
      { id: 'Maryland', label: enumeratorLabel('region', 'Maryland') },
      { id: 'Massachusetts', label: enumeratorLabel('region', 'Massachusetts') },
      { id: 'Michigan', label: enumeratorLabel('region', 'Michigan') },
      { id: 'Minnesota', label: enumeratorLabel('region', 'Minnesota') },
      {
        id: 'Minor_Outlying_Islands',
        label: enumeratorLabel('region', 'Minor_Outlying_Islands'),
      },
      { id: 'Mississippi', label: enumeratorLabel('region', 'Mississippi') },
      { id: 'Missouri', label: enumeratorLabel('region', 'Missouri') },
      { id: 'Montana', label: enumeratorLabel('region', 'Montana') },
      { id: 'Nebraska', label: enumeratorLabel('region', 'Nebraska') },
      { id: 'Nevada', label: enumeratorLabel('region', 'Nevada') },
      { id: 'New_Hampshire', label: enumeratorLabel('region', 'New_Hampshire') },
      { id: 'New_Jersey', label: enumeratorLabel('region', 'New_Jersey') },
      { id: 'New_Mexico', label: enumeratorLabel('region', 'New_Mexico') },
      { id: 'New_York', label: enumeratorLabel('region', 'New_York') },
      { id: 'North_Carolina', label: enumeratorLabel('region', 'North_Carolina') },
      { id: 'North_Dakota', label: enumeratorLabel('region', 'North_Dakota') },
      {
        id: 'Northern_Mariana_Islands',
        label: enumeratorLabel('region', 'Northern_Mariana_Islands'),
      },
      { id: 'Ohio', label: enumeratorLabel('region', 'Ohio') },
      { id: 'Oklahoma', label: enumeratorLabel('region', 'Oklahoma') },
      { id: 'Oregon', label: enumeratorLabel('region', 'Oregon') },
      { id: 'Pennsylvania', label: enumeratorLabel('region', 'Pennsylvania') },
      { id: 'Puerto_Rico', label: enumeratorLabel('region', 'Puerto_Rico') },
      { id: 'Rhode_Island', label: enumeratorLabel('region', 'Rhode_Island') },
      { id: 'South_Carolina', label: enumeratorLabel('region', 'South_Carolina') },
      { id: 'South_Dakota', label: enumeratorLabel('region', 'South_Dakota') },
      { id: 'Tennessee', label: enumeratorLabel('region', 'Tennessee') },
      { id: 'Texas', label: enumeratorLabel('region', 'Texas') },
      { id: 'US_Virgin_Islands', label: enumeratorLabel('region', 'US_Virgin_Islands') },
      { id: 'Utah', label: enumeratorLabel('region', 'Utah') },
      { id: 'Vermont', label: enumeratorLabel('region', 'Vermont') },
      { id: 'Virginia', label: enumeratorLabel('region', 'Virginia') },
      { id: 'Washington', label: enumeratorLabel('region', 'Washington') },
      { id: 'West_Virginia', label: enumeratorLabel('region', 'West_Virginia') },
      { id: 'Wisconsin', label: enumeratorLabel('region', 'Wisconsin') },
      { id: 'Wyoming', label: enumeratorLabel('region', 'Wyoming') },
    ],
    {},
  ),
  postalCode: new StringField('postalCode', label('postalCode'), {}),
  country: new EnumeratorField('country', label('country'), [{ id: 'United_States', label: enumeratorLabel('country', 'United_States') }], {}),
  orgLogo: new ImagesField('orgLogo', label('orgLogo'), 'account/orgLogo', {}),
};

export default {
  fields,
};
