import reducer, { initialState } from './characteristicReducer';
import { CharacteristicActions } from './characteristicActions';

describe('Characteristics Reducer', () => {
  it('should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual(initialState);
  });

  it('should reset to the initial state', () => {
    expect(reducer(initialState, { type: CharacteristicActions.CLEANUP_STORE })).toEqual(initialState);
  });

  it('should update the load state', () => {
    const newState = reducer(initialState, { type: CharacteristicActions.LOADED, payload: { loaded: false } });
    expect(newState.loaded).toEqual(false);
  });

  it('should update the error', () => {
    const newState = reducer(initialState, { type: CharacteristicActions.ERROR, payload: { error: 'Some Error' } });
    expect(newState.error).toEqual('Some Error');
  });

  it('should update poll interval', () => {
    const time = new Date();
    const newState = reducer(initialState, {
      type: CharacteristicActions.UPDATE_POLL_INTERVAL,
      payload: {
        mode: 'Manual',
        previousChangeTime: time,
        previousCount: 5,
        previousHRCount: 5,
      },
    });
    expect(newState.mode).toEqual('Manual');
    expect(newState.previousChangeTime).toEqual(time);
    expect(newState.previousCount).toEqual(5);
    expect(newState.previousHRCount).toEqual(5);
  });

  it('should update the list of characteristics', () => {
    const newChars = [{ id: 'fake char' }];
    const newState = reducer(initialState, { type: CharacteristicActions.UPDATE_CHARACTERISTICS, payload: { characteristics: newChars } });
    expect(newState.characteristics).toEqual(newChars);
  });

  it('should update the list of inactive characteristics', () => {
    const newChars = [{ id: 'fake inactive' }];
    const newState = reducer(initialState, { type: CharacteristicActions.UPDATE_INACTIVE, payload: { inactiveCharacteristics: newChars } });
    expect(newState.inactiveCharacteristics).toEqual(newChars);
  });

  it('should update the list of selected characteristics', () => {
    const newChars = [{ id: 'fake selected' }];
    const newState = reducer(initialState, { type: CharacteristicActions.UPDATE_SELECTED, payload: { selected: newChars } });
    expect(newState.selected).toEqual(newChars);
  });

  it('should update the list of markers', () => {
    const newChars = [{ id: 'fake marker' }];
    const newState = reducer(initialState, { type: CharacteristicActions.UPDATE_MARKERS, payload: { markers: newChars } });
    expect(newState.markers).toEqual(newChars);
  });

  it('should update the list of captured items', () => {
    const newChars = [{ id: 'fake capture' }];
    const newState = reducer(initialState, { type: CharacteristicActions.UPDATE_CAPTURES, payload: { capturedItems: newChars } });
    expect(newState.capturedItems).toEqual(newChars);
  });

  it('should update the mode', () => {
    expect(initialState.mode).toEqual('');
    const newState = reducer(initialState, { type: CharacteristicActions.UPDATE_EXTRACT_MODE, payload: { mode: 'Manual' } });
    expect(newState.mode).toEqual('Manual');
  });

  it('should update the characteristics, captured items, index, and verified on verification change', () => {
    const oldChars = [{ id: 'fake char', verified: false }];
    const oldCapture = [{ id: 'fake capture', verified: false }];
    const oldSelected = [{ id: 'fake selected', verified: false }];
    const oldState = { ...initialState, characteristics: oldChars, capturedItems: oldCapture, selected: oldSelected };
    const newChars = [{ id: 'fake char', verified: true }];
    const newCapture = [{ id: 'fake capture', verified: true }];
    const newSelected = [{ id: 'fake selected', verified: true }];
    const newState = reducer(oldState, { type: CharacteristicActions.VERIFY_CHARACTERISTICS, payload: { characteristics: newChars, capturedItems: newCapture, selected: newSelected, verified: 3 } });
    expect(newState.characteristics).toEqual(newChars);
    expect(newState.capturedItems).toEqual(newCapture);
    expect(newState.selected).toEqual(newSelected);
    expect(newState.verified).toEqual(3);
  });

  it('should update all values on load', () => {
    const oldChars = [{ id: 'fake char' }, { id: 'fake char2' }];
    const oldCapture = [{ id: 'fake inactive' }];
    const oldInactive = [{ id: 'fake capture' }];
    const oldSelected = [{ id: 'fake selected' }];
    const oldState = {
      //
      ...initialState,
      characteristics: oldChars,
      capturedItems: oldCapture,
      selected: oldSelected,
      inactiveCharacteristics: oldInactive,
      partId: 'old part',
      markers: [{ id: 'some marker' }],
      count: 2,
      verified: 2,
      mode: 'Auto',
      index: 0,
    };
    const newChars = [{ id: 'new char' }];
    const newCapture = [{ id: 'new capture' }];
    const newSelected = [{ id: 'new selected' }];
    const newState = reducer(oldState, {
      type: CharacteristicActions.LOAD_CHARACTERISTICS,
      payload: {
        characteristics: newChars,
        capturedItems: newCapture,
        selected: newSelected,
        verified: 1,
        partId: 'New!',
      },
    });

    expect(newState.characteristics).toEqual(newChars);
    expect(newState.capturedItems).toEqual(newCapture);
    expect(newState.selected).toEqual(newSelected);
    expect(newState.inactiveCharacteristics).toEqual([]);
    expect(newState.partId).toEqual('New!');
    expect(newState.markers).toEqual([]);
    expect(newState.count).toEqual(1);
    expect(newState.verified).toEqual(1);
    expect(newState.mode).toEqual('');
    expect(newState.index).toEqual(-1);
  });

  it('should update the index', () => {
    const newState = reducer(initialState, { type: CharacteristicActions.UPDATE_INDEX, payload: { index: 4 } });
    expect(newState.index).toEqual(4);
  });

  it('should update the part ID', () => {
    const newState = reducer(initialState, { type: CharacteristicActions.UPDATE_PART, payload: { partId: 'Update' } });
    expect(newState.partId).toEqual('Update');
  });
});
