import { CharacteristicFind, Characteristics } from 'graphql/characteristics';
import {
  //
  Characteristic,
  CharacteristicEdit,
  CharacteristicEditVars,
  CharacteristicCreate,
  CharacteristicCreateVars,
  CharacteristicList,
  CharacteristicListVars,
  CapturedItem,
  CapturePayload,
  UpdateDetailsPayload,
  UpdateRectPayload,
  CharacteristicActionTypes,
  CharacteristicInput,
} from 'types/characteristics';
import { ApolloClient, NormalizedCacheObject } from '@apollo/client';
import { Part } from 'domain/part/partTypes';
import { AppState } from 'modules/state';
import { Marker, MarkerStyle } from 'types/marker';
import { Dispatch } from 'redux';
import { getAnnotationObject, getCapturedItem } from 'domain/part/edit/characteristics/characteristicsRenderer';
import { cloneDeep } from 'lodash';
import { Drawing } from 'graphql/drawing';
import moment from 'moment';
import { loadQuery as annotationLoadQuery } from 'modules/annotation/annotationActions';
import { Annotation } from 'types/annotation';

export const CharacteristicActions: { [key: string]: CharacteristicActionTypes } = {
  CLEANUP_STORE: 'CHARACTERISTICS_CLEANUP_STORE',
  LOADED: 'CHARACTERISTICS_LOADED',
  PARSED: 'CHARACTERISTICS_PARSED',
  ERROR: 'CHARACTERISTICS_ERROR',
  UPDATE_POLL_INTERVAL: 'CHARACTERISTICS_UPDATE_POLL_INTERVAL',
  UPDATE_CHARACTERISTICS: 'CHARACTERISTICS_UPDATE_CHARACTERISTICS',
  UPDATE_INACTIVE: 'CHARACTERISTICS_UPDATE_INACTIVE',
  UPDATE_SELECTED: 'CHARACTERISTICS_UPDATE_SELECTED',
  UPDATE_MARKERS: 'CHARACTERISTICS_UPDATE_MARKERS',
  UPDATE_CAPTURES: 'CHARACTERISTICS_UPDATE_CAPTURES',
  EDIT_CHARACTERISTICS: 'CHARACTERISTICS_EDIT_CHARACTERISTICS',
  UPDATE_EXTRACT_MODE: 'CHARACTERISTICS_UPDATE_EXTRACT_MODE',
  VERIFY_CHARACTERISTICS: 'CHARACTERISTICS_VERIFY_CHARACTERISTICS',
  LOAD_CHARACTERISTICS: 'CHARACTERISTICS_LOAD_CHARACTERISTICS',
  UPDATE_INDEX: 'CHARACTERISTICS_UPDATE_INDEX',
  UPDATE_LAST: 'CHARACTERISTICS_UPDATE_LAST',
  UPDATE_PART: 'CHARACTERISTICS_UPDATE_PART',
  SET_REVISION: 'CHARACTERISTICS_SET_REVISION',
  // Epics
  CREATE_CHARACTERISTICS: 'CHARACTERISTICS_CREATE_CHARACTERISTICS',
  EDIT_MARKERS: 'CHARACTERISTICS_EDIT_MARKERS',
  DELETE_CHARACTERISTICS: 'CHARACTERISTICS_DELETE_CHARACTERISTICS',
  SINGLE_CUSTOM: 'CHARACTERISTICS_SET_SINGLE_CUSTOM',
};

export const getCurrentDrawings = (drawings: Drawing[], partDrawingId?: string | undefined) => {
  const currentDrawings: Drawing[] = [];
  drawings?.forEach((drawing: Drawing) => {
    // filter out old revisions from drawing list unless its the partDrawing
    const rev = currentDrawings.findIndex((draw) => draw.originalDrawing === drawing.originalDrawing);
    if (rev >= 0) {
      if (drawing.id === partDrawingId || (currentDrawings[rev].id !== partDrawingId && moment(drawing.createdAt).isAfter(moment(currentDrawings[rev].createdAt)))) {
        currentDrawings[rev] = drawing;
      }
    } else {
      currentDrawings.push(drawing);
    }
  });
  return currentDrawings.sort((a, b) => (moment(a.createdAt).isAfter(moment(b.createdAt)) ? -1 : 1));
};

export const sortChars = (a: Characteristic | CharacteristicInput, b: Characteristic | CharacteristicInput) => {
  if (parseInt(a.markerGroup, 10) > parseInt(b.markerGroup, 10)) return 1;
  if (parseInt(a.markerGroup, 10) < parseInt(b.markerGroup, 10)) return -1;
  return a.markerSubIndex > b.markerSubIndex ? 1 : -1;
};

export const getCapturedItems = (characteristics: Characteristic[], annotations: Annotation[], usedMarkers: Marker[], defaultStyle: Marker, drawingId: string) => {
  // Prepare to return the list of captures as two dimensional array [page][capture]
  const capturedItems: CapturedItem[][] = [[]];

  // Loop over all of the characteristics
  characteristics.forEach((char) => {
    if ((char.drawing.id || char.drawing) === drawingId) {
      // Expand the capturedItems collection to match the highest page number found in characteristics
      if (char.drawingSheetIndex > capturedItems.length) {
        const pagesToAdd = char.drawingSheetIndex - capturedItems.length;
        for (let i = -1; i < pagesToAdd; i++) {
          capturedItems.push([]);
        }
      }

      // Assign the marker styles
      let markerStyle: undefined | Marker = defaultStyle;

      // Lookup the marker style that is appropriate for this characteristic type
      // TODO: Move to settings service and implement as   markerStyle = availableMarkers[characteristic.markerStyle]
      for (let i = 0; i < usedMarkers.length; i++) {
        if (usedMarkers[i].id === char.markerStyle) {
          markerStyle = usedMarkers[i];
          break;
        }
      }

      const item: CapturedItem = getCapturedItem(char, markerStyle);
      capturedItems[char.drawingSheetIndex - 1].push(item);
    }
  });

  // Loop over all of the annotations
  annotations.forEach((annotation) => {
    if ((annotation.drawing.id || annotation.drawing) === drawingId) {
      // Expand the capturedItems collection to match the highest page number found in characteristics
      if (annotation.drawingSheetIndex > capturedItems.length) {
        const pagesToAdd = annotation.drawingSheetIndex - capturedItems.length;
        for (let i = -1; i < pagesToAdd; i++) {
          capturedItems.push([]);
        }
      }

      const item: CapturedItem = getAnnotationObject(annotation);
      capturedItems[annotation.drawingSheetIndex - 1].push(item);
    }
  });

  return capturedItems;
};

export const updateAfterEdit = (select: Characteristic[], edited: { [key: string]: Characteristic }, characters: Characteristic[]) => {
  if (!edited || Object.keys(edited).length === 0) {
    return { selected: select, newCharas: characters };
  }
  const newCharas = characters.map((char) => {
    const edit = edited[char.id];
    return edit ? { ...char, ...edit } : char;
  });
  const selected = select.map((char) => {
    const edit = edited[char.id];
    return edit ? { ...char, ...edit } : char;
  });
  return { selected, newCharas };
};

export const addCapture = (characteristic: Characteristic, capturedItems: CapturedItem[][], allMarkers: Marker[], defaultStyle: Marker) => {
  // Prepare to return a two dimensional array of captures ([pageIndex][captures])
  const newCaptures: CapturedItem[][] = cloneDeep(capturedItems);

  // Assign the marker styles
  let markerStyle: undefined | Marker = defaultStyle;

  // Lookup the marker style that is appropriate for this characteristic type
  // TODO: Move to settings service and implement as   markerStyle = availableMarkers[characteristic.markerStyle]
  for (let i = 0; i < allMarkers.length; i++) {
    if (allMarkers[i].id === characteristic.markerStyle) {
      markerStyle = allMarkers[i];
      break;
    }
  }
  const item: CapturedItem = getCapturedItem(characteristic, markerStyle);

  // The first dimension of the capturedItems is the page index
  // Characteristics can comes back from AutoBallooning on a page higher than what the capturedItems collection was initialized for
  if (characteristic.drawingSheetIndex > newCaptures.length) {
    // Calculate the number of pages to add
    const pagesToAdd = characteristic.drawingSheetIndex - newCaptures.length;
    // Expand the capturedItems collection
    for (let i = 0; i < pagesToAdd; i++) {
      newCaptures.push([]);
    }
  }

  // Store the new capture to the correct pageIndex and return
  const dottedLine = newCaptures[characteristic.drawingSheetIndex - 1].findIndex((c) => c.id === item.id);
  if (dottedLine && dottedLine > -1) {
    newCaptures[characteristic.drawingSheetIndex - 1][dottedLine] = item;
  } else {
    newCaptures[characteristic.drawingSheetIndex - 1].push(item);
  }

  return newCaptures;
};

export const addRectNewCapture = (payload: CapturePayload) => ({ type: 'CHARACTERISTICS_RECT_ADD_NEW_CAPTURE', payload });
export const addRectExistingCapture = (payload: CapturePayload) => ({ type: 'CHARACTERISTICS_RECT_ADD_EXISTING_CAPTURE', payload });
export const updateRect = (payload: UpdateRectPayload) => ({ type: 'CHARACTERISTICS_UPDATE_RECT', payload });
export const updateDetails = (payload: UpdateDetailsPayload) => ({ type: 'CHARACTERISTICS_UPDATE_DETAILS', payload });

export const deleteCharacteristicThunk = (queryObject: { variables: { ids: string[] } }, part: Part) => async (dispatch: Dispatch, getState: () => AppState, clientPromise: Promise<ApolloClient<NormalizedCacheObject>>) => {
  return clientPromise.then(async (client) => {
    try {
      await client.mutate<boolean, { ids: string[] }>({
        mutation: Characteristics.mutate.delete,
        variables: queryObject.variables,
        refetchQueries: [
          {
            query: Characteristics.query.list,
            variables: {
              filter: { part: part!.id, deletedAtRange: undefined },
              orderBy: 'markerIndex_ASC',
            },
          },
        ],
      });

      const state = getState();

      const details = cloneDeep(state.characteristics);

      details.characteristics = details.characteristics.filter((characteristic: Characteristic) => !queryObject.variables.ids.includes(characteristic.id));
      details.capturedItems = details.capturedItems.map((sheet) => {
        return sheet.filter((capture) => !queryObject.variables.ids.includes(capture.id));
      });

      dispatch({ type: CharacteristicActions.EDIT_CHARACTERISTICS, payload: { characteristics: details.characteristics, capturedItems: details.capturedItems, selected: [] } });
    } catch (error) {
      dispatch({ type: CharacteristicActions.ERROR, payload: { error } });
    }
  });
};

export const findQuery = async (queryObject: { variables: { id: string }; fetchPolicy?: any }, client: ApolloClient<NormalizedCacheObject>) =>
  client.query<CharacteristicFind>({
    query: Characteristics.query.find,
    variables: queryObject.variables,
    notifyOnNetworkStatusChange: true,
    fetchPolicy: queryObject.fetchPolicy || 'cache-first',
  });

export const loadQuery = async (queryObject: { variables: CharacteristicListVars; fetchPolicy: any }, client: ApolloClient<NormalizedCacheObject>) =>
  client.query<CharacteristicList>({
    query: Characteristics.query.list,
    variables: queryObject.variables,
    notifyOnNetworkStatusChange: true,
    fetchPolicy: queryObject.fetchPolicy || 'cache-first',
  });

export const editMutation = async (queryObject: { variables: CharacteristicEditVars }, client: ApolloClient<NormalizedCacheObject>, refetchQueries: any) =>
  client.mutate<CharacteristicEdit, CharacteristicEditVars>({
    mutation: Characteristics.mutate.edit,
    variables: queryObject.variables,
    refetchQueries,
  });

export const createMutation = async (queryObject: { variables: CharacteristicCreateVars }, client: ApolloClient<NormalizedCacheObject>, refetchQueries: any) =>
  client.mutate<CharacteristicCreate, CharacteristicCreateVars>({
    mutation: Characteristics.mutate.create,
    variables: queryObject.variables,
    refetchQueries,
  });

export const editCharacteristicThunk =
  (queryObject: { variables: CharacteristicEditVars }, partId: string, part: Part, refetchQueries: any) => async (dispatch: Dispatch, getState: () => AppState, clientPromise: Promise<ApolloClient<NormalizedCacheObject>>) => {
    return clientPromise.then(async (client) => {
      try {
        const characteristicEdit = await editMutation(queryObject, client, refetchQueries);

        const state = getState();

        const details = cloneDeep(state.characteristics);
        const newSelected: Characteristic[] = [];
        details.characteristics = details.characteristics.map((characteristic) => {
          if (characteristicEdit.data && characteristic.id === queryObject.variables.id) {
            newSelected.push(characteristicEdit.data.characteristicUpdate);
            return characteristicEdit.data.characteristicUpdate;
          }
          return characteristic;
        });

        dispatch({ type: CharacteristicActions.UPDATE_SELECTED, payload: { characteristics: details.characteristics, selected: newSelected } });
      } catch (error) {
        dispatch({ type: CharacteristicActions.ERROR, payload: { error } });
      }
    });
  };

export const loadCharacteristicsThunk = (partId: string, assignedMarkers: { [key: string]: MarkerStyle }) => async (dispatch: Dispatch, getState: () => AppState, clientPromise: Promise<ApolloClient<NormalizedCacheObject>>) => {
  return clientPromise.then(async (client) => {
    try {
      const state = getState();
      const { partDrawing } = getState().partEditor;
      const { index: currentIndex } = getState().characteristics;
      const { part } = getState().part;
      // is there a better way to do this?
      // it is pulling in stale data from the cache on load even though it is being re-fetched in updateFeatureThunk
      const characteristicsLoad = await loadQuery({ variables: { filter: { part: partId, deletedAtRange: undefined }, orderBy: 'markerIndex_ASC' }, fetchPolicy: 'network-only' }, client);

      const annotationsLoad = await annotationLoadQuery({ variables: { filter: { part: partId, deletedAtRange: undefined } }, fetchPolicy: 'network-only' }, client);

      if (characteristicsLoad?.data?.characteristicList && partDrawing) {
        const loadedDrawings: string[] = part?.drawings ? getCurrentDrawings(part?.drawings, partDrawing.id).map((draw) => draw.id) : [partDrawing.id];
        const loadedFeatures: Characteristic[] = [...characteristicsLoad.data.characteristicList.rows].filter((feat) => loadedDrawings.includes(feat.drawing.id));
        const loadedAnnotations: Annotation[] = annotationsLoad.data && annotationsLoad.data.annotationList ? [...annotationsLoad.data.annotationList.rows].filter((a) => loadedDrawings.includes(a.drawing.id)) : [];

        const extracted: Characteristic[] = [];
        const inactive: Characteristic[] = [];
        const annotations: Annotation[] = [...loadedAnnotations];

        let index = currentIndex;
        let indexSet = currentIndex >= 0;

        if (loadedFeatures) {
          loadedFeatures.forEach((char) => {
            // Adding config.autoBallooning.enabled check here to suppress characteristics added by auto-ballooning
            if (char.captureMethod === 'Automated' && char.status === 'Inactive') {
              inactive.push(char);
            } else {
              extracted.push(char);
              if (!indexSet) {
                index += 1;
                if (!char.verified) indexSet = true;
              }
            }
          });
        }

        let capturedItems: CapturedItem[][] = [[]];
        const usedMarkers: Marker[] = [];
        let defaultStyle: Marker | null = null;
        if (state.session?.markers && state.session.settings?.stylePresets?.metadata?.Default?.setting) {
          const { markers } = state.session;
          Object.keys(assignedMarkers).forEach((style) => {
            for (let i = 0; i < markers.length; i++) {
              if (markers[i].id === assignedMarkers[style].style) {
                usedMarkers.push(markers[i]);
                if (style === 'default') {
                  defaultStyle = markers[i];
                }
                break;
              }
            }
          });
        }
        capturedItems = getCapturedItems(extracted.concat(inactive), annotations, usedMarkers, defaultStyle || usedMarkers[0], partDrawing?.id || '');
        const sorted = extracted.sort(sortChars);
        const newLast = sorted.length > 0 ? parseInt(sorted[sorted.length - 1].markerGroup, 10) : null;
        const ind = index > extracted.length - 1 ? extracted.length - 1 : index;
        dispatch({
          type: CharacteristicActions.LOAD_CHARACTERISTICS,
          payload: {
            characteristics: sorted,
            lastLabel: newLast,
            inactiveCharacteristics: inactive,
            annotations,
            selected: ind >= 0 ? [sorted[ind]] : [],
            markers: usedMarkers,
            capturedItems,
            mode: state.characteristics.mode,
            verified: characteristicsLoad.data.characteristicList ? [...loadedFeatures].filter((char) => char.verified).length : 0,
            index: ind,
          },
        });
      }
    } catch (error) {
      dispatch({ type: CharacteristicActions.ERROR, payload: { error } });
    }
  });
};

export const loadCharacteristicRevisionThunk = (currentFeatureId: string, originalCharacteristic: string, drawing: string) => async (dispatch: Dispatch, getState: () => AppState, clientPromise: Promise<ApolloClient<NormalizedCacheObject>>) => {
  return clientPromise.then(async (client) => {
    try {
      // Should only return 1 feature if any exists
      const characteristicsLoad = await loadQuery({ variables: { filter: { originalCharacteristic } }, fetchPolicy: 'network-only' }, client);
      const rows = characteristicsLoad.data.characteristicList?.rows;
      const compareRevision = rows?.find((c) => c.drawing.id === drawing);

      if (rows && compareRevision) {
        const sortedRows = [...rows].sort((a, b) => (moment(a.createdAt).isBefore(moment(b.createdAt)) ? -1 : 1));
        const currentIndex = sortedRows.findIndex((c) => c.id === currentFeatureId) || 0;
        const revisionIndex = sortedRows.findIndex((c) => c.id === compareRevision.id) || 0;
        const compareIndex = currentIndex - revisionIndex;
        dispatch({ type: CharacteristicActions.SET_REVISION, payload: { compareRevision, compareIndex } });
      }
    } catch (error) {
      console.error(error);
      dispatch({ type: CharacteristicActions.ERROR, payload: { error } });
    }
  });
};
