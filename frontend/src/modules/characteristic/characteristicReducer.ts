import { CharacteristicsState, CharacteristicsAction } from 'modules/state';

const initialCaptureMode = '';

export const initialState: CharacteristicsState = {
  characteristics: [],
  inactiveCharacteristics: [],
  annotations: [],
  selected: [],
  markers: [],
  defaultMarker: null,
  capturedItems: [[]],
  lastLabel: null,
  count: 0, // May not be used anywhere per https://bitbucket.org/ixeng/ixc-mono/pull-requests/127/ixc-823-ixc-824-ocr-captures-wip#comment-171758845
  mode: initialCaptureMode,
  singleCustom: false,
  verified: 0,
  index: -1,
  previousChangeTime: null,
  previousCount: 0,
  previousHRCount: 0,
  loaded: true,
  parsed: true,
  error: null,
  partId: '',
  compareRevision: null,
  compareIndex: 0,
};

export default (state: CharacteristicsState = initialState, { type, payload }: CharacteristicsAction): CharacteristicsState => {
  switch (type) {
    case 'CHARACTERISTICS_CLEANUP_STORE':
      return initialState;
    case 'CHARACTERISTICS_LOADED':
      return {
        ...state,
        loaded: payload.loaded,
      };
    case 'CHARACTERISTICS_PARSED':
      return {
        ...state,
        parsed: payload,
      };
    case 'CHARACTERISTICS_ERROR':
      return {
        ...state,
        error: payload.error,
      };
    case 'CHARACTERISTICS_UPDATE_POLL_INTERVAL':
      return {
        ...state,
        mode: payload.mode || initialCaptureMode,
        previousChangeTime: payload.previousChangeTime || null,
        previousCount: payload.previousCount || 0,
        previousHRCount: payload.previousHRCount || 0,
      };
    case 'CHARACTERISTICS_UPDATE_CHARACTERISTICS':
      return {
        ...state,
        characteristics: payload.characteristics || state.characteristics,
      };
    case 'CHARACTERISTICS_UPDATE_INACTIVE':
      return {
        ...state,
        inactiveCharacteristics: payload.inactiveCharacteristics || state.inactiveCharacteristics,
      };
    case 'CHARACTERISTICS_UPDATE_SELECTED':
      return {
        ...state,
        capturedItems: payload.capturedItems || state.capturedItems,
        inactiveCharacteristics: payload.inactiveCharacteristics || state.inactiveCharacteristics,
        selected: payload.selected || state.selected,
        characteristics: payload.characteristics || state.characteristics,
        annotations: payload.annotations || state.annotations,
        index: payload.index !== undefined ? payload.index : state.index,
        lastLabel: payload.lastLabel !== undefined ? payload.lastLabel : state.lastLabel,
        count: payload.characteristics ? payload.characteristics.length : state.characteristics?.length,
        verified: payload.verified ? payload.verified : state.verified,
        mode: payload.mode || state.mode,
        singleCustom: Object.prototype.hasOwnProperty.call(payload, 'singleCustom') ? payload.singleCustom : state.singleCustom,
        loaded: true,
      };
    case 'CHARACTERISTICS_UPDATE_MARKERS':
      return {
        ...state,
        markers: payload.markers || state.markers,
        capturedItems: payload.capturedItems || state.capturedItems,
        defaultMarker: payload.defaultMarker || state.defaultMarker,
      };
    case 'CHARACTERISTICS_UPDATE_CAPTURES':
      return {
        ...state,
        capturedItems: payload.capturedItems || state.capturedItems,
      };
    case 'CHARACTERISTICS_UPDATE_EXTRACT_MODE':
      return {
        ...state,
        mode: payload.mode || initialCaptureMode,
      };
    case 'CHARACTERISTICS_VERIFY_CHARACTERISTICS':
      return {
        ...state,
        characteristics: payload.characteristics || state.characteristics,
        capturedItems: payload.capturedItems || state.capturedItems,
        selected: payload.selected || state.selected,
        verified: payload.verified || state.verified,
        index: payload.index !== undefined ? payload.index : state.index,
        loaded: true,
      };
    case 'CHARACTERISTICS_LOAD_CHARACTERISTICS':
      return {
        ...state,
        characteristics: payload.characteristics || [],
        annotations: payload.annotations || [],
        inactiveCharacteristics: payload.inactiveCharacteristics || [],
        selected: payload.selected || [],
        markers: payload.markers || [],
        capturedItems: payload.capturedItems || [[]],
        mode: payload.mode || initialCaptureMode,
        verified: payload.verified || 0,
        index: payload.index !== undefined ? payload.index : -1,
        lastLabel: payload.lastLabel !== undefined ? payload.lastLabel : undefined,
        count: payload.characteristics ? payload.characteristics.length : 0,
        previousChangeTime: payload.previousChangeTime || null,
        previousCount: payload.previousCount || 0,
        previousHRCount: payload.previousHRCount || 0,
        loaded: true,
        error: null,
        partId: payload.partId || '',
      };
    case 'CHARACTERISTICS_UPDATE_INDEX':
      return {
        ...state,
        index: payload.index !== undefined ? payload.index : -1,
      };
    case 'CHARACTERISTICS_UPDATE_LAST':
      return {
        ...state,
        lastLabel: payload.lastLabel !== undefined ? payload.lastLabel : undefined,
      };
    case 'CHARACTERISTICS_UPDATE_PART':
      return {
        ...state,
        partId: payload.partId || '',
      };
    case 'CHARACTERISTICS_SET_REVISION':
      return {
        ...state,
        ...payload,
      };
    case 'CHARACTERISTICS_SET_SINGLE_CUSTOM':
      return { ...state, ...payload };
    default:
      return state;
  }
};
