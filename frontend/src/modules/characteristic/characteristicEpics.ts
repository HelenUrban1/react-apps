import { Epic, ActionsObservable, StateObservable } from 'redux-observable';
import { ApolloClient, NormalizedCacheObject } from '@apollo/client';
import { catchError, filter, map, switchMap } from 'rxjs/operators';
import { isOfType } from 'typesafe-actions';
import { forkJoin, from } from 'rxjs';
import { cloneDeep, compact } from 'lodash';
import log from 'modules/shared/logger';
import {
  //
  CapturedItem,
  CharacteristicActionTypes,
  CharacteristicActionPayloads,
  CreateCharacteristicsPayload,
  EditCharacteristicsPayload,
  MarkersPayload,
  CharacteristicDelete,
  CharacteristicDeleteVars,
  Characteristic,
} from 'types/characteristics';
import { Characteristics } from 'graphql/characteristics';
import { Parts } from 'domain/part/partApi';
import { Marker } from 'types/marker';
import { CharacteristicsAction, AppState } from 'modules/state';

import { renumberAfterDelete } from 'domain/part/edit/characteristics/utils/Renumber';
import { i18n } from 'i18n';
import { sortChars, CharacteristicActions, createMutation, editMutation, getCapturedItems, updateAfterEdit, addCapture } from './characteristicActions';

// prettier-ignore
export const createCharacteristicsEpic: Epic<CharacteristicsAction, CharacteristicsAction, AppState> = (action$: ActionsObservable<CharacteristicsAction>, state$: StateObservable<AppState>, { clientPromise }) =>
  action$.pipe(
    filter(isOfType(CharacteristicActions.CREATE_CHARACTERISTICS)),
    switchMap(({ payload }: CharacteristicsAction) =>
      from<PromiseLike<ApolloClient<NormalizedCacheObject>>>(clientPromise).pipe(
        switchMap((client: ApolloClient<NormalizedCacheObject>) => {
          const inputs = payload as CreateCharacteristicsPayload;
          if (!inputs) {
            throw new Error('No Features Provided to Create')
          };
          const newState = cloneDeep(state$.value.characteristics);

          const calls = inputs.map((char) => {
            const data = {...char};
            if(data.markerLabel === newState.lastLabel?.toString()){
              newState.lastLabel += 1;
              data.markerLabel = newState.lastLabel.toString();
              data.markerGroup = newState.lastLabel.toString();
              data.markerIndex = newState.characteristics.length;
            }
            return from(
              createMutation({ variables: { data } }, client, [
                {
                  query: Characteristics.query.list,
                  variables: {
                    filter: { part: char.part, deletedAtRange: undefined },
                    orderBy: 'markerIndex_ASC',
                  },
                },
                {
                  query: Parts.query.find,
                  variables: {
                    id: char.part,
                  },
                },
              ])
            );
          });

          return forkJoin(calls).pipe(
            map((res) => {
              const createdItems = compact(res.map((call) => (call && call.data ? call.data.characteristicCreate : undefined)));

              createdItems.forEach((item) => {
                const newItem = {...item};
                // Insert the new characteristic into the correct location in the list
                newState.characteristics.splice(newItem.markerIndex, 0, newItem).sort(sortChars);
                // if the item has an error we don't want the popup to show up for the first render
                if (newItem.captureError) {
                  newItem.firstRender = true;
                }
                if (newState.parsed) {
                  // select the new feature if no pending feature parsing from editing full spec
                  newState.selected = [newItem];
                  newState.index = newItem.markerIndex;
                }
                newState.count = newState.count ? newState.count + 1 : 1;
                newState.capturedItems = addCapture(newItem, newState.capturedItems, newState.markers, newState.defaultMarker||newState.markers[0]);
              });
              newState.characteristics = newState.characteristics.sort(sortChars);
              newState.lastLabel = parseInt(newState.characteristics[newState.characteristics.length - 1].markerGroup, 10);
              if (newState.singleCustom) {
                newState.singleCustom = false;
                newState.mode = 'Manual';
              }
              return {
                type: CharacteristicActions.UPDATE_SELECTED,
                payload: newState,
              };
             }),
             catchError((error) => {
              log.error(error);
              return [
                {
                  type: CharacteristicActions.ERROR,
                  payload: { error: i18n('errors.characteristics.create') },
                },
              ];
            })
          )
        }),
        catchError((error) => {
          log.error(error);
          return [
            {
              type: CharacteristicActions.ERROR,
              payload: { error: i18n('errors.characteristics.create') },
            },
          ];
        })
      )
    )
  );

export const deleteCharacteristicsEpic: Epic<CharacteristicsAction, CharacteristicsAction, AppState> = (action$: ActionsObservable<CharacteristicsAction>, state$: StateObservable<AppState>, { clientPromise }) =>
  action$.pipe(
    filter(isOfType(CharacteristicActions.DELETE_CHARACTERISTICS)),
    switchMap(({ payload }: CharacteristicsAction) =>
      from<PromiseLike<ApolloClient<NormalizedCacheObject>>>(clientPromise).pipe(
        switchMap((client: ApolloClient<NormalizedCacheObject>) => {
          const { ids, part } = payload;
          const success = client.mutate<CharacteristicDelete, CharacteristicDeleteVars>({
            mutation: Characteristics.mutate.delete,
            variables: { ids },
            refetchQueries: [
              {
                query: Characteristics.query.list,
                variables: {
                  filter: { part, deletedAtRange: undefined },
                  orderBy: 'markerIndex_ASC',
                },
              },
            ],
          });
          return from(success).pipe(
            map(() => {
              const { markers, defaultMarker, annotations } = state$.value.characteristics;
              let { characteristics } = state$.value.characteristics;
              if (!characteristics || characteristics.length === 0) {
                // this may trigger if a user deletes a drawing with characteristics from the part details drawer
                characteristics = state$.value.part.part?.characteristics || [];
              }
              const { partDrawing } = state$.value.partEditor;
              if (!characteristics || characteristics.length <= 0) {
                const action: { type: CharacteristicActionTypes; payload: CharacteristicActionPayloads } = {
                  type: CharacteristicActions.UPDATE_SELECTED,
                  payload: { characteristics: [], selected: [], lastLabel: 0 },
                };

                return [action];
              }
              const res = renumberAfterDelete(ids, characteristics);
              const newAnnotations = annotations.filter((a) => !ids.includes(a.characteristic?.id));

              const calls = res.itemsToUpdate.map((char) => {
                // Create a flattened version of the value object for graphql
                const updatedItem = Characteristics.util.input(char);
                // Call the graphql mutation to send the flattened object to the server
                return from(
                  editMutation({ variables: { id: updatedItem.id, data: updatedItem.char } }, client, [
                    {
                      query: Characteristics.query.find,
                      variables: {
                        id: char.id,
                      },
                    },
                    {
                      query: Parts.query.find,
                      variables: {
                        id: updatedItem.char.part,
                      },
                    },
                    {
                      query: Characteristics.query.list,
                      variables: {
                        filter: { part: updatedItem.char.part, deletedAtRange: undefined },
                        orderBy: 'markerIndex_ASC',
                      },
                    },
                  ]),
                );
              });
              if (!calls || calls.length === 0) {
                const newList = characteristics.filter((c) => !ids.includes(c.id));

                const capturedItems = getCapturedItems(newList, newAnnotations, markers, defaultMarker || markers[0], partDrawing?.id || '');
                let newLast = newList && newList.length > 0 ? parseInt(newList[newList.length - 1].markerGroup, 10) : 0;
                if (!newLast) {
                  newLast = 0;
                }

                return [
                  {
                    type: CharacteristicActions.UPDATE_SELECTED,
                    payload: { characteristics: newList, annotations: newAnnotations, capturedItems, selected: [], lastLabel: newLast },
                  },
                ];
              }

              return forkJoin(calls).pipe(
                map((c) => {
                  const updates: any = {};
                  c.forEach((call) => {
                    if (call && call.data && call.data.characteristicUpdate) {
                      updates[call.data.characteristicUpdate.id] = call.data.characteristicUpdate;
                    }
                  });
                  const newChanges = characteristics.filter((char) => !ids.includes(char.id)).map((char) => (updates[char.id] ? updates[char.id] : char));
                  const sorted = newChanges.sort(sortChars);
                  // compact(c.map((call) => (call && call.data ? call.data.characteristicUpdate : undefined)));
                  const capturedItems = getCapturedItems(newChanges, newAnnotations, markers, defaultMarker || markers[0], partDrawing?.id || '');
                  let newLast = sorted && sorted.length > 0 ? parseInt(sorted[sorted.length - 1].markerGroup, 10) : 0;
                  if (!newLast) {
                    newLast = 0;
                  }
                  const actions = [
                    {
                      type: CharacteristicActions.UPDATE_SELECTED,
                      payload: { capturedItems, characteristics: sorted, annotations: newAnnotations, lastLabel: newLast, selected: [] },
                    },
                  ] as { type: CharacteristicActionTypes; payload: CharacteristicActionPayloads }[];
                  return actions;
                }),
                switchMap((emission) => emission),
                catchError((error) => {
                  log.error(error);
                  return [
                    {
                      type: CharacteristicActions.ERROR,
                      payload: { error: i18n('errors.characteristics.delete') },
                    },
                  ];
                }),
              );
            }),
            switchMap((emission) => emission),
            catchError((error) => {
              log.error(error);
              return [
                {
                  type: CharacteristicActions.ERROR,
                  payload: { error: i18n('errors.characteristics.delete') },
                },
              ];
            }),
          );
        }),
        catchError((error) => {
          log.error(error);
          return [
            {
              type: CharacteristicActions.ERROR,
              payload: { error: i18n('errors.characteristics.delete') },
            },
          ];
        }),
      ),
    ),
  );

export const editCharacteristicsEpic: Epic<CharacteristicsAction, CharacteristicsAction, AppState> = (action$: ActionsObservable<CharacteristicsAction>, state$: StateObservable<AppState>, { clientPromise }) =>
  action$.pipe(
    filter(isOfType(CharacteristicActions.EDIT_CHARACTERISTICS)),
    switchMap(({ payload }: CharacteristicsAction) =>
      from<PromiseLike<ApolloClient<NormalizedCacheObject>>>(clientPromise).pipe(
        switchMap((client: ApolloClient<NormalizedCacheObject>) => {
          const { updates, newItems, verify, newSelected } = payload as EditCharacteristicsPayload;
          const { characteristics, annotations, inactiveCharacteristics, selected: oldSelected, markers, defaultMarker, verified } = state$.value.characteristics;
          const { partDrawing } = state$.value.partEditor;

          const calls = updates.map((char) => {
            // Create a flattened version of the value object for graphql
            const updatedItem = Characteristics.util.input(char);
            // Call the graphql mutation to send the flattened object to the server
            // TODO: These refetches make this a very slow call. Try to selectively update the cache instead to improve performance
            return from(
              editMutation({ variables: { id: updatedItem.id, data: updatedItem.char } }, client, [
                {
                  query: Characteristics.query.find,
                  variables: {
                    id: char.id,
                  },
                },
                {
                  query: Parts.query.find,
                  variables: {
                    id: updatedItem.char.part,
                  },
                },
                {
                  query: Characteristics.query.list,
                  variables: {
                    filter: { part: updatedItem.char.part, deletedAtRange: undefined },
                    orderBy: 'markerIndex_ASC',
                  },
                },
              ]),
            );
          });

          return forkJoin(calls).pipe(
            map((res) => {
              // Make changes a map for easier updates to the characteristics and selected arrays
              const changes: { [key: string]: Characteristic } = {};
              res.forEach((call) => {
                if (call && call.data && call.data.characteristicUpdate) {
                  changes[call.data.characteristicUpdate.id] = call.data.characteristicUpdate;
                }
              });
              // Update the feature arrays
              const { selected, newCharas } = updateAfterEdit(newSelected || (oldSelected.filter((s) => !!s.quantity) as Characteristic[]), changes, characteristics);
              const sorted = newCharas.sort(sortChars);
              // Update the captures
              const capturedItems = getCapturedItems(newCharas.concat(inactiveCharacteristics), annotations, markers, defaultMarker || markers[0], partDrawing?.id || '');
              const newLast = parseInt(sorted[sorted.length - 1].markerGroup, 10);
              // Update the verified count
              let verifiedCount = 0;
              if (verify) {
                sorted.forEach((c) => {
                  if (c.verified) {
                    verifiedCount += 1;
                  }
                });
              } else {
                verifiedCount = verified || 0;
              }
              // Update the inactives list, if we activated any autoballooning regions
              const inactives = [...inactiveCharacteristics];
              if (inactives.length > 0) {
                // Todo: would it be better to loop through the inactives and use the changes map?
                updates.forEach((update) => {
                  const ind = inactives.findIndex((inactive) => inactive.id === update.id);
                  if (update.status === 'Active') {
                    if (ind > -1) {
                      inactives.splice(ind, 1);
                    }
                  }
                });
              }
              const actions = [
                {
                  type: CharacteristicActions.UPDATE_SELECTED,
                  payload: { characteristics: sorted, lastLabel: newLast, capturedItems, selected, index: selected[0] ? selected[0].markerIndex : -1, verified: verifiedCount, inactiveCharacteristics: inactives },
                },
              ] as { type: CharacteristicActionTypes; payload: CharacteristicActionPayloads }[];

              // New characteristics have to be made after the update, to prevent race conditions
              if (newItems && newItems.length > 0) {
                actions.push({
                  type: CharacteristicActions.CREATE_CHARACTERISTICS as CharacteristicActionTypes,
                  payload: newItems as CreateCharacteristicsPayload,
                });
              }

              return actions;
            }),
            switchMap((emission) => emission),
            catchError((error) => {
              log.error(error);
              return [
                {
                  type: CharacteristicActions.ERROR,
                  payload: { error: i18n('errors.characteristics.edit') },
                },
              ];
            }),
          );
        }),
        catchError((error) => {
          log.error(error);
          return [
            {
              type: CharacteristicActions.ERROR,
              payload: { error: i18n('errors.characteristics.edit') },
            },
          ];
        }),
      ),
    ),
  );

/**
 * Updates the markers and captured items arrays in the characteristic redux when the part markers change
 * @param action$
 * payload: {
 *    assignedMarkers: marker object from the part
 *    markers: list of markers from the session
 *    updates: optional list of characteristic updates
 * }
 * @param state$ // app redux state
 * @returns // {type: CharacteristicActionTypes, payload: any}
 */
export const updateMarkersEpic: Epic<CharacteristicsAction, CharacteristicsAction, AppState> = (action$: ActionsObservable<CharacteristicsAction>, state$: StateObservable<AppState>) =>
  action$.pipe(
    filter(isOfType(CharacteristicActions.EDIT_MARKERS)),
    switchMap(({ payload }: CharacteristicsAction) => {
      try {
        const { assignedMarkers, markers, updates } = payload as MarkersPayload;
        const { characteristics, inactiveCharacteristics, annotations } = state$.value.characteristics;
        const { partDrawing } = state$.value.partEditor;
        const emission = [] as CharacteristicsAction[];
        if (assignedMarkers === null || !markers) return emission;

        const usedMarkers: Marker[] = [];
        let defaultStyle = markers[0];
        Object.keys(assignedMarkers).forEach((style) => {
          const marker = markers.find((m) => m.id === assignedMarkers[style].style);
          if (marker) {
            usedMarkers.push(marker);
            if (style === 'default') {
              defaultStyle = marker;
            }
          }
        });

        let capturedItems: CapturedItem[][] = [];
        // Build any markup that needs to get initialized or updated
        if (updates && updates.length === 0) {
          // this is happening during initializing, do all characters
          const superList = characteristics.concat(inactiveCharacteristics);
          capturedItems = getCapturedItems(superList, annotations, usedMarkers, defaultStyle, partDrawing?.id || '');
        } else {
          capturedItems = getCapturedItems(characteristics, annotations, usedMarkers, defaultStyle, partDrawing?.id || '');
        }

        emission.push({
          type: CharacteristicActions.UPDATE_MARKERS,
          payload: { capturedItems, markers: usedMarkers, defaultMarker: defaultStyle },
        });
        if (updates && updates.length > 0) {
          emission.push({
            type: CharacteristicActions.EDIT_CHARACTERISTICS,
            payload: { updates },
          });
        }

        return emission;
      } catch (error) {
        log.error(error);
        throw error;
      }
    }),
    catchError((error) => {
      log.error(error);
      return [
        {
          type: CharacteristicActions.ERROR,
          payload: { error: i18n('errors.characteristics.markers') },
        },
      ];
    }),
  );
