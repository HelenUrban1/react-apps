import gql from 'graphql-tag';
import getClient from 'modules/shared/graphql/graphqlClient';
import getStore, { getHistory } from 'modules/store';
import Permissions from 'security/permissions';
import PermissionChecker from 'modules/auth/permissionChecker';
import config from 'config';
import axios from 'modules/shared/network/axios';
import { i18n } from 'i18n';
import CryptoJS from 'crypto-js';

const securePhassphrase = process.env.REACT_APP_PASSWORD_ENCRYPTION_PASSPHRASE;

export default class AuthService {
  static async signUpSubmit(email, firstName) {
    const graphqlClient = await getClient();

    const regDot = /\./gi;
    const regAt = /@/gi;
    const regPlus = /\+/gi;

    const cognitoUsername = email.replace(regAt, '__at__').replace(regDot, '__dot__').replace(regPlus, '__plus__');
    const usingCognito = config.authStrategy && config.authStrategy === 'cognito';
    console.log('AuthService.signUpSubmit usingCognito', usingCognito);

    const response = usingCognito
      ? await axios.post(`${config.backendUrl}/cognitoSignup`, { email, firstName, username: cognitoUsername })
      : await graphqlClient.mutate({
          mutation: gql`
            mutation AUTH_SIGNUP_SUBMIT($email: String!, $firstName: String) {
              authSignUpSubmit(email: $email, firstName: $firstName)
            }
          `,
          variables: {
            email,
            firstName,
          },
        });

    const data = config.authStrategy && config.authStrategy === 'cognito' ? response.data.user : response.data.authSignUpSubmit;
    console.log('AuthService.signUpSubmit data', data);

    return data;
  }

  static async sendEmailVerification(email) {
    const graphqlClient = await getClient();
    const response = await graphqlClient.mutate({
      mutation: gql`
        mutation AUTH_SEND_EMAIL_ADDRESS_VERIFICATION_EMAIL($email: String!) {
          authSendEmailAddressVerificationEmail(email: $email)
        }
      `,
      variables: {
        email,
      },
    });

    return response.data.authSendEmailAddressVerificationEmail;
  }

  static async sendPasswordResetEmail(email) {
    const graphqlClient = await getClient();
    const response = await graphqlClient.mutate({
      mutation: gql`
        mutation AUTH_SEND_PASSWORD_RESET_EMAIL($email: String!) {
          authSendPasswordResetEmail(email: $email)
        }
      `,
      variables: {
        email,
      },
    });

    return response.data.authSendPasswordResetEmail;
  }

  static async requestAccountAccess(email, adminEmail, token) {
    const graphqlClient = await getClient();
    const response = await graphqlClient.mutate({
      mutation: gql`
        mutation AUTH_REQUEST_ACCESS($email: String!, $adminEmail: String, $token: String!) {
          authRequestAccess(email: $email, adminEmail: $adminEmail, token: $token)
        }
      `,
      variables: {
        email,
        adminEmail,
        token,
      },
    });

    return response.data.authRequestAccess;
  }

  static async registerOrphanUser(email, password, type) {
    const graphqlClient = await getClient();
    const response = await graphqlClient.mutate({
      mutation: gql`
        mutation AUTH_CREATE_ORPHAN($email: String!, $password: String!, $type: String!) {
          authCreateOrphan(email: $email, password: $password, type: $type)
        }
      `,
      variables: {
        email,
        password,
        type,
      },
    });

    return response.data.authCreateUser;
  }

  static async convertAccountToOrphan(accountId) {
    const graphqlClient = await getClient();
    const response = await graphqlClient.mutate({
      mutation: gql`
        mutation AUTH_CONVERT_ACCOUNT_TO_ORPHAN($accountId: String!) {
          authConvertAccountToOrphan(accountId: $accountId)
        }
      `,
      variables: {
        accountId,
      },
    });

    return response.data.authConvertToUser;
  }

  static async convertToTrial(email) {
    const graphqlClient = await getClient();
    const response = await graphqlClient.mutate({
      mutation: gql`
        mutation AUTH_CONVERT_TO_TRIAL($email: String!) {
          authConvertToTrial(email: $email)
        }
      `,
      variables: {
        email,
      },
    });

    return response.data.authConvertToTrial;
  }

  static async finishRegistration(email, plainPassword, firstName, lastName, orgName, phoneNumber, token) {
    const password = CryptoJS.AES.encrypt(plainPassword, securePhassphrase).toString();
    const graphqlClient = await getClient();
    const response =
      config.authStrategy && config.authStrategy === 'cognito'
        ? await axios.post(`${config.backendUrl}/cognitoFinishRegistration`, {
            firstName,
            familyName: lastName,
            email,
            orgName,
            phoneNumber,
            newPassword: password,
          })
        : await graphqlClient.mutate({
            mutation: gql`
              mutation AUTH_FINISH_REGISTRATION($email: String!, $password: String, $firstName: String, $lastName: String, $orgName: String, $phoneNumber: String, $token: String) {
                authFinishRegistration(email: $email, password: $password, firstName: $firstName, lastName: $lastName, orgName: $orgName, phoneNumber: $phoneNumber, token: $token)
              }
            `,
            variables: {
              email,
              password,
              firstName,
              lastName,
              orgName,
              phoneNumber,
              token,
            },
          });

    return response.data.authFinishRegistration;
  }

  static async acceptAccessRequest(adminEmail, token, userId) {
    const graphqlClient = await getClient();
    const response = await graphqlClient.mutate({
      mutation: gql`
        mutation AUTH_ACCEPT_REQUEST($email: String!, $token: String, $userId: String) {
          authRequestAccept(email: $email, token: $token, userId: $userId)
        }
      `,
      variables: {
        email: adminEmail,
        token,
        userId,
      },
    });

    return response.data.authRequestAccept;
  }

  static async rejectAccessRequest(adminEmail, token) {
    const graphqlClient = await getClient();
    const response = await graphqlClient.mutate({
      mutation: gql`
        mutation AUTH_REJECT_REQUEST($adminEmail: String!, $token: String!) {
          authRequestReject(adminEmail: $adminEmail, token: $token)
        }
      `,
      variables: {
        adminEmail,
        token,
      },
    });

    return response.data.authRequestReject;
  }

  static async signinWithMfaToken(mfaCode, email, plainPassword, rememberMe) {
    const password = CryptoJS.AES.encrypt(plainPassword, securePhassphrase).toString();
    const response = await axios.post(`${config.backendUrl}/cognitoSignin`, {
      email,
      password,
      mfaCode,
      rememberMe,
    });

    return response.data.authSignIn;
  }

  static async signinWithEmailAndPassword(email, plainPassword, rememberMe, mfaCode = undefined) {
    const graphqlClient = await getClient();
    const password = CryptoJS.AES.encrypt(plainPassword, securePhassphrase).toString();
    const response =
      config.authStrategy && config.authStrategy === 'cognito'
        ? await axios.post(`${config.backendUrl}/cognitoSignin`, {
            email,
            password,
            mfaCode,
            rememberMe,
          })
        : await graphqlClient.mutate({
            mutation: gql`
              mutation AUTH_SIGN_IN($email: String!, $password: String!) {
                authSignIn(email: $email, password: $password)
              }
            `,
            variables: {
              email,
              password,
            },
          });

    return response.data;
  }

  static async getDeviceToken() {
    axios.post(`${config.backendUrl}/deviceToken`).catch((error) => {
      // Device cannot be installed, most likely because the account doesn't have metro
      window.location.href = '/';
    });
  }

  static async signinWithDeviceToken() {
    const graphqlClient = await getClient();
    const response = await graphqlClient.mutate({
      mutation: gql`
        mutation AUTH_SIGN_IN_DEVICE {
          authSignInDevice
        }
      `,
    });

    return response.data.authSignIn;
  }

  static async fetchMe() {
    const graphqlClient = await getClient();
    await this.resetGraphqlClientCache(graphqlClient);
    // For resolver see backend/src/api/auth/queries/authMe.js
    // For schema see backend/src/api/auth/types/userWithRoles.js
    const { jwt, currentUser } = getStore().getState().auth;
    const response =
      config.authStrategy && config.authStrategy === 'cognito'
        ? await axios({
            method: 'POST',
            url: `${config.backendUrl}/cognitoAuthMe`,
            headers: {
              authorization: jwt ? `Bearer ${jwt}` : '',
            },
            data: {
              email: currentUser ? currentUser.email : '',
            },
          })
        : await graphqlClient.query({
            query: gql`
              {
                authMe {
                  id
                  authenticationUid
                  emailVerified
                  fullName
                  firstName
                  lastName
                  phoneNumber
                  email
                  roles
                  paid
                  activeAccountMemberId
                  isServiceAccount
                  isReviewAccount
                  accountSettings
                  accountId
                  accountName
                  siteId
                  siteName
                  subscription {
                    status
                    trialStartedAt
                    trialEndedAt
                    activatedAt
                    renewalDate
                    expiresAt
                    plan
                    providerProductHandle
                    components
                    paymentType
                    billingPeriod
                    paidUserSeatsUsed
                    drawingsUsed
                    partsUsed
                    measurementsUsed
                  }
                  accountMemberships {
                    id
                    accountId
                    status
                    roles
                    accessControl
                  }
                  avatars {
                    id
                    name
                    publicUrl
                  }
                }
              }
            `,
          });

    return response.data.authMe;
  }

  static async isEmailConfigured() {
    const graphqlClient = await getClient();
    const response = await graphqlClient.query({
      query: gql`
        {
          authIsEmailConfigured
        }
      `,
    });

    return response.data.authIsEmailConfigured;
  }

  static async resetGraphqlClientCache(graphqlClient) {
    await graphqlClient.cache.reset();
  }

  static async signout(email) {
    const graphqlClient = await getClient();
    // await this.resetGraphqlClientCache(graphqlClient);
    if (email)
      await graphqlClient.mutate({
        mutation: gql`
          mutation AUTH_SIGN_OUT($email: String!) {
            authSignOut(email: $email)
          }
        `,
        variables: {
          email,
        },
      });
    return true;
  }

  static redirectToRoute(route) {
    getHistory().push(route);
  }

  static redirectIfLimitedAccess(currentUser) {
    const permissionValidator = new PermissionChecker(currentUser);
    const routePrefs = {
      partRead: '/',
      reviewer: '/review',
      accountBillingAccess: '/settings/billing',
      metro: '/metro',
    };
    const history = getHistory();

    if (!currentUser.roles) {
      return history.push('/auth/empty-permissions');
    }

    const route = Object.keys(routePrefs).find((permission) => permissionValidator.match(Permissions.values[permission]));
    if (route) {
      return history.push(routePrefs[route]);
    }

    return history.push('/settings/profile');
  }

  static async checkSignupMeta(email) {
    const graphqlClient = await getClient();
    const response = await graphqlClient.query({
      query: gql`
        query authCheckSignupMeta($email: String) {
          authCheckSignupMeta(email: $email)
        }
      `,

      variables: {
        email,
      },
    });

    return response.data.authCheckSignupMeta;
  }

  static async checkSignupToken(token, email) {
    if (!token) {
      return false;
    }
    const graphqlClient = await getClient();

    const response = await graphqlClient.query({
      query: gql`
        query authValidSignupToken($token: String!, $email: String) {
          authValidSignupToken(token: $token, email: $email)
        }
      `,

      variables: {
        token,
        email,
      },
    });

    return response.data.authValidSignupToken || response.data;
  }

  static async updateProfile(firstName, lastName, phoneNumber, avatars) {
    const graphqlClient = await getClient();
    const response = await graphqlClient.mutate({
      mutation: gql`
        mutation AUTH_UPDATE_PROFILE($profile: UserProfileInput!) {
          authUpdateProfile(profile: $profile)
        }
      `,

      variables: {
        profile: {
          firstName,
          lastName,
          phoneNumber,
          avatars,
        },
      },
    });

    return response.data.authUpdateProfile;
  }

  static async passwordReset(token, email, plainPassword) {
    const graphqlClient = await getClient();
    const password = CryptoJS.AES.encrypt(plainPassword, securePhassphrase).toString();
    const response = await graphqlClient.mutate({
      mutation: gql`
        mutation AUTH_PASSWORD_RESET($token: String!, $email: String!, $password: String!) {
          authPasswordReset(token: $token, email: $email, password: $password)
        }
      `,
      variables: {
        token,
        password,
        email,
      },
    });

    return response.data.authPasswordReset;
  }

  static async checkForPassword(token) {
    const graphqlClient = await getClient();
    const response = await graphqlClient.mutate({
      mutation: gql`
        mutation CHECK_FOR_PASSWORD($token: String!) {
          authCheckForPassword(token: $token)
        }
      `,
      variables: {
        token,
      },
    });

    return response.data.authCheckForPassword;
  }

  static async checkForStatus(email) {
    const graphqlClient = await getClient();
    const response = await graphqlClient.mutate({
      mutation: gql`
        mutation CHECK_FOR_STATUS($email: String!) {
          authCheckForStatus(email: $email)
        }
      `,
      variables: {
        email,
      },
    });

    return response.data.authCheckForStatus;
  }

  static async verifyEmail(token) {
    const graphqlClient = await getClient();
    const response = await graphqlClient.mutate({
      mutation: gql`
        mutation AUTH_VERIFY_EMAIL($token: String!) {
          authVerifyEmail(token: $token)
        }
      `,
      variables: {
        token,
      },
    });

    return response.data.authVerifyEmail;
  }

  static async validateDomain(email) {
    const graphqlClient = await getClient();
    const response = await graphqlClient.mutate({
      mutation: gql`
        mutation AUTH_VALIDATE_DOMAIN($email: String!) {
          authValidateDomain(email: $email)
        }
      `,
      variables: {
        email,
      },
    });

    return response.data.authValidateDomain;
  }

  static async registerInvitedUser(token, email, plainPassword, firstName, lastName) {
    const graphqlClient = await getClient();
    const password = CryptoJS.AES.encrypt(plainPassword, securePhassphrase).toString();
    const response =
      config.authStrategy && config.authStrategy === 'cognito'
        ? await axios.post(`${config.backendUrl}/cognitoAcceptInvitation`, {
            firstName,
            familyName: lastName,
            email,
            newPassword: password,
          })
        : await graphqlClient.mutate({
            mutation: gql`
              mutation AUTH_REGISTER_INVITED($token: String!, $email: String!, $password: String!, $profile: UserProfileInput!) {
                authRegisterInvited(token: $token, email: $email, password: $password, profile: $profile)
              }
            `,
            variables: {
              token,
              email,
              password,
              profile: {
                firstName,
                lastName,
              },
            },
          });

    return response.data.authRegisterInvited;
  }

  static async doVerifyMfa(mfaCode, rememberMe) {
    const { jwt, currentUser, session } = getStore().getState().auth;
    const response = await axios({
      method: 'POST',
      url: `${config.backendUrl}/cognitoMfaCheck`,
      headers: {
        authorization: jwt ? `${jwt}` : '',
      },
      data: {
        email: currentUser.email,
        mfaCode,
        rememberMe,
        Session: session || undefined,
      },
    });
    return response.data;
  }

  static async doRegisterMfa(code) {
    const { jwt, currentUser } = getStore().getState().auth;
    const response = await axios({
      method: 'POST',
      url: `${config.backendUrl}/cognitoVerifyTotpMfa`,
      headers: {
        authorization: jwt ? `${jwt}` : '',
      },
      data: {
        email: currentUser.email,
        code,
        friendlyDeviceName: i18n('app.title'),
      },
    });
    return response.data;
  }

  static async doDisableMfa(mfaCode) {
    const { jwt, currentUser, session } = getStore().getState().auth;
    const response = await axios({
      method: 'POST',
      url: `${config.backendUrl}/cognitoDisableTotpMfa`,
      headers: {
        authorization: jwt ? `${jwt}` : '',
      },
      data: {
        email: currentUser.email,
        mfaCode,
        Session: session || undefined,
      },
    });
    return response.data;
  }

  static async doCheckMfa(email) {
    const graphqlClient = await getClient();
    const authCheckMfa =
      config.authStrategy === 'cognito'
        ? await graphqlClient.query({
            query: gql`
              query AUTH_CHECK_MFA($email: String!) {
                authCheckMfa(email: $email) {
                  mfaEnabled
                  mfaRegistered
                }
              }
            `,
            variables: {
              email,
            },
          })
        : { data: { authCheckMfa: { mfaRegistered: false, mfaEnabled: false } } };

    return authCheckMfa.data.authCheckMfa;
  }

  static async doResetMfa(email) {
    const graphqlClient = await getClient();
    await graphqlClient.mutate({
      mutation: gql`
        mutation AUTH_RESET_MFA($email: String!) {
          authResetMfa(email: $email)
        }
      `,
      variables: {
        email,
      },
    });
  }
}
