import RolesMapper from '../../security/rolesMapper';

export default class PermissionChecker {
  constructor(currentUser, rolesMapper = RolesMapper) {
    this.currentUser = currentUser;
    this.userRoles = currentUser ? currentUser.roles : [];
    this.rolesMapper = rolesMapper;
    this.membership = currentUser?.accountMemberships ? currentUser.accountMemberships.find((acc) => acc.accountId === currentUser.accountId) : null;
  }

  match(permission) {
    if (!permission) {
      return true;
    }

    return this.rolesMapper.hasMatchingRole(this.userRoles || [], permission.allowedRoles);
  }

  get isEmptyPermissions() {
    if (!this.isAuthenticated) {
      return false;
    }

    return !this.userRoles || !this.userRoles.length;
  }

  get isOrphan() {
    return !this.membership;
  }

  get isAuthenticated() {
    return !!this.currentUser && !!this.currentUser.id;
  }

  get isAcceptingRequest() {
    return this.currentUser?.signupToken === 'Requested' && !this.currentUser.id;
  }

  get isExpiredToken() {
    return this.currentUser?.signupToken === 'Expired' && !this.currentUser.id;
  }

  get isCanceledInvitation() {
    return this.currentUser?.signupToken === 'Canceled' && !this.currentUser.id;
  }

  get isEmailVerified() {
    if (!this.isAuthenticated) {
      return false;
    }

    return this.currentUser.emailVerified;
  }

  get isRequestPending() {
    if (!this.membership) {
      return false;
    }

    return this.membership.status === 'Pending' || this.membership.status === 'Requesting';
  }

  get isRestrictedAccess() {
    return !!this.membership?.accessControl;
  }
}
