import PermissionChecker from './permissionChecker';
import Permissions from '../../security/permissions';

const permissions = Permissions.values;

describe('PermissionChecker', () => {
  let permissionChecker;
  let currentUser;
  let rolesMapper;

  beforeEach(() => {
    rolesMapper = {
      hasMatchingRole: jest.fn(),
    };
    permissionChecker = new PermissionChecker(currentUser, rolesMapper);
  });

  describe('#match()', () => {
    it('should return true when a permission is not required', () => {
      const result = permissionChecker.match(null);
      expect(result).toBe(true);
    });

    it('should return false when the rolesMapper check does', () => {
      rolesMapper.hasMatchingRole.mockReturnValue(false);

      const result = permissionChecker.match(permissions.iamEdit);
      expect(result).toBe(false);
    });

    it('should return true when then rolesMapper check does', () => {
      rolesMapper.hasMatchingRole.mockReturnValue(true);

      const result = permissionChecker.match(permissions.iamEdit);
      expect(result).toBe(true);
    });
  });
});
