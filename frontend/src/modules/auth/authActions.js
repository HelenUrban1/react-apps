import getClient from 'modules/shared/graphql/graphqlClient';
import PermissionChecker from 'modules/auth/permissionChecker';
import { doUpdateCurrentSubscription } from 'modules/subscription/subscriptionActions';
import { initializeSessionThunk } from 'modules/session/sessionActions';
import { getHistory } from 'modules/store';
import log from 'modules/shared/logger';
import config from 'config';
import axios from 'modules/shared/network/axios';
import service from './authService';
import Errors from '../shared/error/errors';
import Analytics from '../shared/analytics/analytics';
import Message from '../../view/shared/message';
import { i18n } from '../../i18n';

const prefix = 'AUTH';

const actions = {
  ERROR_MESSAGE_CLEARED: `${prefix}_ERROR_MESSAGE_CLEARED`,

  AUTH_INIT_SUCCESS: `${prefix}_INIT_SUCCESS`,
  AUTH_INIT_ERROR: `${prefix}_INIT_ERROR`,

  CHECK_PASSWORD: `${prefix}_CHECK_PASSWORD`,

  TOKEN_VALID: 'TOKEN_VALID',
  TOKEN_ERROR: 'TOKEN_ERROR',

  AUTH_START: `${prefix}_START`,
  AUTH_SUCCESS: `${prefix}_SUCCESS`,
  AUTH_ERROR: `${prefix}_ERROR`,
  REMOVE_CURRENT_USER: `${prefix}_REMOVE_CURRENT_USER`,

  CREATE_SERVICE_ACCOUNT_START: `${prefix}_CREATE_SERVICE_ACCOUNT_START`,
  CREATE_SERVICE_ACCOUNT_SUCCESS: `${prefix}_CREATE_SERVICE_ACCOUNT_SUCCESS`,
  CREATE_SERVICE_ACCOUNT_ERROR: `${prefix}_CREATE_SERVICE_ACCOUNT_ERROR`,

  DELETE_ACCOUNT_START: `${prefix}_DELETE_ACCOUNT_START`,
  DELETE_ACCOUNT_SUCCESS: `${prefix}_DELETE_ACCOUNT_SUCCESS`,
  DELETE_ACCOUNT_ERROR: `${prefix}_DELETE_ACCOUNT_ERROR`,

  UPDATE_PROFILE_START: `${prefix}_UPDATE_PROFILE_START`,
  UPDATE_PROFILE_SUCCESS: `${prefix}_UPDATE_PROFILE_SUCCESS`,
  UPDATE_PROFILE_ERROR: `${prefix}_UPDATE_PROFILE_ERROR`,

  CURRENT_USER_REFRESH_START: `${prefix}_CURRENT_USER_REFRESH_START`,
  CURRENT_USER_REFRESH_SUCCESS: `${prefix}_CURRENT_USER_REFRESH_SUCCESS`,
  CURRENT_USER_REFRESH_ERROR: `${prefix}_CURRENT_USER_REFRESH_ERROR`,
  CURRENT_USER_SET_SHALLOW: `${prefix}_CURRENT_USER_SET_SHALLOW`,

  PASSWORD_RESET_EMAIL_START: `${prefix}_PASSWORD_RESET_EMAIL_START`,
  PASSWORD_RESET_EMAIL_SUCCESS: `${prefix}_PASSWORD_RESET_EMAIL_SUCCESS`,
  PASSWORD_RESET_EMAIL_ERROR: `${prefix}_PASSWORD_RESET_EMAIL_ERROR`,

  PASSWORD_RESET_START: `${prefix}_PASSWORD_RESET_START`,
  PASSWORD_RESET_SUCCESS: `${prefix}_PASSWORD_RESET_SUCCESS`,
  PASSWORD_RESET_ERROR: `${prefix}_PASSWORD_RESET_ERROR`,

  EMAIL_VERIFY_START: `${prefix}_EMAIL_VERIFY_START`,
  EMAIL_VERIFY_SUCCESS: `${prefix}_EMAIL_VERIFY_SUCCESS`,
  EMAIL_VERIFY_ERROR: `${prefix}_EMAIL_VERIFY_ERROR`,

  EMAIL_CONFIRMATION_START: `${prefix}_EMAIL_CONFIRMATION_START`,
  EMAIL_CONFIRMATION_SUCCESS: `${prefix}_EMAIL_CONFIRMATION_SUCCESS`,
  EMAIL_CONFIRMATION_ERROR: `${prefix}_EMAIL_CONFIRMATION_ERROR`,

  REQUEST_ACCOUNT_ACCESS: `${prefix}_REQUEST_ACCOUNT_ACCESS`,
  REQUEST_POLICY_CLEAR: `${prefix}_REQUEST_POLICY_CLEAR`,
  REQUEST_SUCCESS: `${prefix}_REQUEST_SUCCESS`,

  GET_DEVICE_TOKEN: `${prefix}_GET_DEVICE_TOKEN`,
  SET_TOKEN: `${prefix}_SET_TOKEN`,
  SET_MFA: `${prefix}_SET_MFA`,
  MFA_ERROR: `${prefix}_MFA_ERROR`,
  MFA_VERIFY_START: `${prefix}_MFA_VERIFY_START`,
  MFA_VERIFY_SUCCESS: `${prefix}_MFA_VERIFY_SUCCESS`,
  MFA_REGISTER_START: `${prefix}_MFA_REGISTER_START`,
  MFA_REGISTER_SUCCESS: `${prefix}_MFA_REGISTER_SUCCESS`,
  MFA_RESET_START: `${prefix}_MFA_RESET_START`,
  MFA_RESET_SUCCESS: `${prefix}_MFA_RESET_SUCCESS`,

  doClearErrorMessage() {
    return {
      type: actions.ERROR_MESSAGE_CLEARED,
    };
  },

  doClearRequestPolicy() {
    return {
      type: actions.REQUEST_POLICY_CLEAR,
    };
  },

  doSendEmailConfirmation: (email) => async (dispatch, getState) => {
    try {
      dispatch({ type: actions.EMAIL_CONFIRMATION_START });
      await service.sendEmailVerification(email);
      Message.success(i18n('auth.verificationEmailSuccess'));

      Analytics.track({
        event: Analytics.events.authEmailVerificationRequest,
        state: getState(),
        properties: {
          email,
        },
      });

      dispatch({
        type: actions.EMAIL_CONFIRMATION_SUCCESS,
      });
    } catch (error) {
      Errors.handle(error);
      dispatch({ type: actions.EMAIL_CONFIRMATION_ERROR });
    }
  },

  doSendPasswordResetEmail: (email) => async (dispatch, getState) => {
    try {
      dispatch({
        type: actions.PASSWORD_RESET_EMAIL_START,
      });
      await service.sendPasswordResetEmail(email);
      Message.success(i18n('auth.passwordResetEmailSuccess'));

      Analytics.track({
        event: Analytics.events.authPasswordResetRequest,
        state: getState(),
        properties: {
          email,
        },
      });

      dispatch({
        type: actions.PASSWORD_RESET_EMAIL_SUCCESS,
      });
    } catch (error) {
      Errors.handle(error);
      dispatch({
        type: actions.PASSWORD_RESET_EMAIL_ERROR,
      });
    }
  },

  doAcceptRequest: (adminEmail, token, userId) => async (dispatch) => {
    try {
      const bearer = await service.acceptAccessRequest(adminEmail, token, userId);
      if (bearer === 'resent') {
        Analytics.track({
          event: Analytics.events.authRegistrationBlocked,
          properties: {
            email: adminEmail,
            reason: 'Access request token was expired',
          },
        });

        service.redirectToRoute(`/auth/request-response?token=${token}&email=${encodeURIComponent(adminEmail)}&response=expired`);
      } else if (config.authStrategy && config.authStrategy === 'cognito') {
        Analytics.track({
          event: Analytics.events.accessRequestAccepted,
          properties: {
            currentUser: { email: adminEmail },
          },
        });

        service.redirectToRoute(`/auth/request-accepted?email=${encodeURIComponent(adminEmail)}`);
      } else {
        dispatch({ type: actions.AUTH_START });

        dispatch({
          type: actions.SET_TOKEN,
          payload: bearer,
        });

        const currentUser = await service.fetchMe();
        Analytics.identify({ user: currentUser });

        const permissionChecker = new PermissionChecker(currentUser);

        Message.success(i18n('auth.requestResponse.accepted'));

        Analytics.track({
          event: Analytics.events.accessRequestAccepted,
          properties: {
            currentUser,
          },
        });

        dispatch({
          type: actions.AUTH_SUCCESS,
          payload: {
            currentUser,
            jwt: bearer,
            restricted: permissionChecker.isRestrictedAccess,
          },
        });

        service.redirectToRoute('/settings/seats');
      }
    } catch (error) {
      await service.signout();
      dispatch({
        type: actions.TOKEN_ERROR,
        payload: error.message,
      });
      Errors.handle(error);
      if (error?.message && error.message.includes('xpire')) {
        service.redirectToRoute('/auth/invite-expired');
      } else if (error?.message && error.message.includes('ancel')) {
        service.redirectToRoute('/auth/invite-canceled');
      } else {
        service.redirectToRoute('/');
      }
    }
  },

  doRejectRequest: (adminEmail, token) => async (dispatch) => {
    try {
      dispatch({ type: actions.AUTH_START });

      await service.rejectAccessRequest(adminEmail, token, undefined);

      Message.success(i18n('auth.requestResponse.rejected'));

      dispatch({
        type: actions.AUTH_SUCCESS,
        payload: {
          currentUser: null,
        },
      });
    } catch (error) {
      await service.signout();
      dispatch({
        type: actions.AUTH_ERROR,
        payload: Errors.selectMessage(error),
      });
    }
  },

  doCheckMeta: (email) => async (dispatch) => {
    try {
      const meta = await service.checkSignupMeta(email);
      const metaObj = JSON.parse(meta);
      return metaObj;
    } catch (error) {
      // eat error
    }
  },

  doCheckToken: (email, token) => async (dispatch) => {
    Analytics.track({
      event: Analytics.events.authCheckInviteToken,
      properties: {
        email,
      },
    });
    try {
      const valid = await service.checkSignupToken(token, email);

      switch (valid) {
        case 'Expired':
          Analytics.track({
            event: Analytics.events.authRegistrationBlocked,
            properties: {
              email,
              reason: 'Token was expired',
            },
          });
          service.redirectToRoute((await service.checkForStatus(email)) === 'Active' ? '/auth/email-unverified?status=expired&sent=true' : '/auth/invite-expired');
          break;
        case 'Canceled':
          Analytics.track({
            event: Analytics.events.authRegistrationBlocked,
            properties: {
              email,
              reason: 'Token was canceled',
            },
          });
          service.redirectToRoute('/auth/invite-canceled');
          break;
        case 'Requested':
          Analytics.track({
            event: Analytics.events.authRequestAccepted,
            properties: {
              email,
            },
          });
          dispatch(actions.doAcceptRequest(email, token));
          break;
        case 'Active':
          service.redirectToRoute('/auth/email-unverified?status=expired&sent=true');
          break;
        default:
          dispatch({
            type: actions.TOKEN_VALID,
            payload: { currentUser: { email, signupToken: valid } },
          });
          break;
      }
      if (valid === 'Requested') {
        Analytics.track({
          event: Analytics.events.authRequestAccepted,
          properties: {
            email,
          },
        });
        dispatch(actions.doAcceptRequest(email, token));
      } else {
        dispatch({
          type: actions.TOKEN_VALID,
          payload: { currentUser: { email, signupToken: valid } },
        });
      }
    } catch (error) {
      Analytics.track({
        event: Analytics.events.authCheckInviteTokenError,
        error,
        properties: {
          email,
        },
      });
      await service.signout(email);
      Errors.handle(error);

      dispatch({
        type: actions.TOKEN_ERROR,
        payload: error,
      });
    }
  },

  doCheckForPassword: (token) => async (dispatch, getState) => {
    try {
      const passwordExists = await service.checkForPassword(token);
      dispatch({
        type: actions.CHECK_PASSWORD,
        payload: passwordExists,
      });
    } catch (error) {
      dispatch({
        type: actions.CHECK_PASSWORD,
        payload: false,
      });
    }
  },

  doSendRequest: (email, adminEmail, token) => async (dispatch, getState) => {
    try {
      dispatch({ type: actions.AUTH_START });
      const valid = await service.checkSignupToken(token, email);
      if (valid === 'Expired') {
        Analytics.track({
          event: Analytics.events.authExpiredInviteToken,
          properties: {
            email,
          },
        });
        dispatch({
          type: actions.AUTH_SUCCESS,
          payload: {
            currentUser: null,
          },
        });
        service.redirectToRoute('/auth/email-unverified?status=expired');
      } else {
        await service.requestAccountAccess(email, adminEmail, token);
        dispatch({
          type: actions.AUTH_SUCCESS,
          payload: {
            currentUser: null,
          },
        });
      }
    } catch (error) {
      await service.signout(email);

      if (Errors.errorCode(error) !== 400) {
        Errors.handle(error);
      }

      dispatch({
        type: actions.AUTH_ERROR,
        payload: Errors.selectMessage(error),
      });
    }
  },

  doGetDeviceToken: () => async (dispatch, getState) => {
    await service.getDeviceToken();
  },

  doGetServiceAccount: () => async (dispatch, getState) => {
    const { account } = getState();
    try {
      await service.getServiceAccount(account.id);

      dispatch({});
    } catch (e) {
      // TODO be more productive with this error
      throw e;
    }
  },

  doRegisterAccountAndUser: (email, firstName) => async (dispatch, getState) => {
    try {
      dispatch({ type: actions.AUTH_START });

      await service.signUpSubmit(email, firstName);

      dispatch({
        type: actions.AUTH_SUCCESS,
        payload: {
          currentUser: { email },
        },
      });

      service.redirectToRoute('/auth/email-unverified');
    } catch (error) {
      if (Errors.errorCode(error) !== 400) {
        Errors.handle(error);
      }

      dispatch({
        type: actions.AUTH_ERROR,
        payload: Errors.selectMessage(error),
      });
    }
  },

  doRegisterInvitedUser: (token, email, password, firstName, lastName) => async (dispatch, getState) => {
    try {
      dispatch({ type: actions.AUTH_START });

      if (config.authStrategy && config.authStrategy === 'cognito') {
        // verify the user in cognito
        await axios.post(`${config.backendUrl}/cognitoConfirmSignup`, { email, code: token });
      }

      await service.registerInvitedUser(token, email, password, firstName, lastName);
      const bearer = await service.signinWithEmailAndPassword(email, password);

      dispatch({
        type: actions.SET_TOKEN,
        payload: bearer.authSignIn,
      });

      dispatch({
        type: actions.CURRENT_USER_SET_SHALLOW,
        payload: {
          user: {
            email,
          },
        },
      });

      const currentUser = await service.fetchMe();
      Analytics.identify({ user: currentUser });

      const permissionChecker = new PermissionChecker(currentUser);

      Analytics.track({
        event: Analytics.events.authRegisterEmailPassword,
        state: getState(),
        properties: {
          currentUser,
        },
      });

      // Associate the user with their account using group
      // TODO: When multi-site support is added, also add them to a site group
      Analytics.group({
        groupId: currentUser.accountId,
        traits: {
          groupType: 'Account',
          name: currentUser.accountName,
        },
      });

      dispatch({
        type: actions.AUTH_SUCCESS,
        payload: {
          currentUser,
          jwt: bearer.authSignIn,
          restricted: permissionChecker.isRestrictedAccess,
        },
      });
      // redirect users to first accessible route
      service.redirectIfLimitedAccess(currentUser);
    } catch (error) {
      await service.signout(email);

      if (Errors.errorCode(error) !== 400) {
        Errors.handle(error);
      }

      dispatch({
        type: actions.AUTH_ERROR,
        payload: Errors.selectMessage(error),
      });
    }
  },

  doConvertToTrial: (email) => async (dispatch) => {
    try {
      dispatch({ type: actions.AUTH_START });
      Analytics.track({
        event: Analytics.events.accountTrialStarted,
        properties: {
          email,
        },
      });

      const route = await service.convertToTrial(email);

      dispatch({
        type: actions.AUTH_SUCCESS,
        payload: {
          currentUser: null,
        },
      });

      service.redirectToRoute(route);
    } catch (error) {
      await service.signout(email);

      if (Errors.errorCode(error) !== 400) {
        Errors.handle(error);
      }
      dispatch({
        type: actions.AUTH_ERROR,
        payload: Errors.selectMessage(error),
      });
    }
  },

  // Finishes registration by updating fields on Account, Membership, and Profile
  doFinishRegistration: (email, password, firstName, lastName, orgName, phoneNumber, token) => async (dispatch, getState) => {
    try {
      dispatch({ type: actions.AUTH_START });

      if (config.authStrategy && config.authStrategy === 'cognito') {
        // verify the user in cognito
        await axios.post(`${config.backendUrl}/cognitoConfirmSignup`, { email, code: token });
      }

      await service.finishRegistration(email, password, firstName, lastName, orgName, phoneNumber, token);
      const bearer = await service.signinWithEmailAndPassword(email, password);

      dispatch({
        type: actions.SET_TOKEN,
        payload: bearer.authSignIn,
      });

      dispatch({
        type: actions.CURRENT_USER_SET_SHALLOW,
        payload: {
          user: {
            email,
          },
        },
      });

      const currentUser = await service.fetchMe();

      Analytics.identify({ user: currentUser });

      const permissionChecker = new PermissionChecker(currentUser);

      Analytics.track({
        event: Analytics.events.authRegisterEmailPassword,
        state: getState(),
        properties: {
          ...currentUser,
        },
      });

      // This could move to backend in account service's create method
      Analytics.track({
        event: Analytics.events.accountTrialStarted,
        properties: {
          ...currentUser,
        },
      });

      // Associated the user with their account group
      // TODO: When multi-site support is added, also add them to a site group
      Analytics.group({
        groupId: currentUser.accountId,
        traits: {
          groupType: 'Account',
          name: orgName,
          phone: phoneNumber,
        },
      });

      dispatch({
        type: actions.AUTH_SUCCESS,
        payload: {
          currentUser,
          jwt: bearer.authSignIn,
          restricted: permissionChecker.isRestrictedAccess,
        },
      });

      // redirect users to first accessible route
      service.redirectIfLimitedAccess(currentUser);
    } catch (error) {
      // debugger;
      await service.signout(email);

      if (Errors.errorCode(error) !== 400) {
        Errors.handle(error);
      }

      dispatch({
        type: actions.AUTH_ERROR,
        payload: Errors.selectMessage(error),
      });
    }
  },

  doSigninWithEmailAndPassword: (email, password, rememberMe, mfaCode, redirect) => async (dispatch, getState) => {
    let user = null;
    try {
      dispatch({ type: actions.AUTH_START });

      let currentUser = null;

      const mfaStatus = config.authStrategy === 'cognito' ? await service.doCheckMfa(email) : { mfaRegistered: false, mfaEnabled: false };

      if (mfaStatus.mfaRegistered && !mfaCode) {
        dispatch({
          type: actions.SET_MFA,
          payload: {
            mfaEnabled: true,
            mfaRegistered: true,
          },
        });
        dispatch({
          type: 'SESSION_UPDATE_GLOBAL_SETTINGS',
          payload: { mfaEnabled: true },
        });
        const history = getHistory();
        history.push(`/auth/mfa${redirect ? '?redirect=seats' : ''}`, { email, password, rememberMe });
      } else {
        const bearer = await service.signinWithEmailAndPassword(email, password, rememberMe, mfaCode);

        if (!bearer) {
          log.error('No Bearer Token Found');
          throw new Error('Unauthorized');
        }
        if (bearer === 'reset') {
          throw new Error('Password reset required, check email for link');
        }

        // Identify the user to Analytics
        Analytics.identify({
          user: currentUser,
          mfaEnabled: !!bearer.mfaEnabled?.value,
          mfaRegistered: bearer.challengeName === 'SOFTWARE_TOKEN_MFA',
        });

        dispatch({
          type: actions.CURRENT_USER_REFRESH_SUCCESS,
          payload: {
            restricted: true,
            session: bearer.session || null,
            currentUser: { email },
          },
        });
        user = true;
        dispatch({
          type: actions.SET_TOKEN,
          payload: bearer.authSignIn,
        });
        if (mfaStatus.mfaEnabled && !mfaStatus.mfaRegistered) {
          // MFA device not registered
          // QR Modal will pop up to register device
          dispatch({
            type: actions.SET_MFA,
            payload: {
              mfaEnabled: mfaStatus.mfaEnabled,
              mfaRegistered: mfaStatus.mfaRegistered,
            },
          });
          dispatch({
            type: 'SESSION_UPDATE_GLOBAL_SETTINGS',
            payload: { mfaEnabled: mfaStatus.mfaEnabled },
          });
        } else {
          try {
            currentUser = await service.fetchMe();
          } catch (error) {
            currentUser = { email };
          }
          const permissionChecker = new PermissionChecker(currentUser);

          // Record the login event to Analytics tools
          Analytics.track({
            event: Analytics.events.authSigninEmailPassword,
            state: getState(),
            properties: {
              email,
            },
          });

          // Associated the user with their account group
          // TODO: When multi-site support is added, also add them to a site group
          Analytics.group({
            groupId: currentUser.accountId,
            traits: {
              groupType: 'Account',
              name: currentUser.accountName,
            },
          });

          dispatch({
            type: actions.AUTH_SUCCESS,
            payload: {
              currentUser,
              jwt: bearer.authSignIn,
              restricted: permissionChecker.isRestrictedAccess,
            },
          });

          if (redirect) {
            // redirect to given route - e.g. settings/seats on request accepted
            service.redirectToRoute(redirect);
          } else {
            // redirect users to first accessible route
            service.redirectIfLimitedAccess(currentUser);
          }
        }
      }
    } catch (error) {
      if (user) {
        await service.signout(email);
      }

      dispatch({
        type: actions.AUTH_ERROR,
        payload: Errors.selectMessage(error),
      });

      dispatch({
        type: actions.REMOVE_CURRENT_USER,
        payload: {
          currentUser: null,
          jwt: '',
        },
      });

      if (Errors.errorCode(error) !== 400) {
        Errors.handle(error);
      } else {
        service.redirectToRoute('/auth/signin');
      }
    }
  },

  doSigninWithDevice: () => async (dispatch, getState) => {
    try {
      dispatch({ type: actions.AUTH_START });

      let currentUser = null;

      const bearer = await service.signinWithDeviceToken();

      dispatch({
        type: actions.SET_TOKEN,
        payload: bearer,
      });

      try {
        currentUser = await service.fetchMe();
      } catch (error) {
        // Metro is likely not installed or there has been some error retrieving the service account
        service.redirectToRoute('/');
      }

      const permissionChecker = new PermissionChecker(currentUser);

      // TODO how to handle service account analytics?
      // Identify the user to Analytics
      // Analytics.identify({
      //   user: currentUser,
      //   // traits: {
      //   //   restricted: permissionChecker.isRestrictedAccess,
      //   // },
      // });

      // Record the login event to Analytics tools
      // Analytics.track({
      //   event: ???,
      //   state: getState(),
      //   properties: {
      //    ???
      //   },
      // });

      // Associated the user with their account group
      // TODO: When multi-site support is added, also add them to a site group
      // Analytics.group({
      //   groupId: currentUser.accountId,
      //   traits: {
      //     groupType: 'Account',
      //     name: currentUser.accountName,
      //   },
      // });

      dispatch({
        type: actions.AUTH_SUCCESS,
        payload: {
          currentUser,
          jwt: bearer,
          restricted: permissionChecker.isRestrictedAccess,
        },
      });
    } catch (error) {
      await service.signout();

      if (Errors.errorCode(error) !== 400) {
        Errors.handle(error);
      }

      dispatch({
        type: actions.AUTH_ERROR,
        payload: Errors.selectMessage(error),
      });

      service.redirectToRoute('/');
    }
  },

  doSignout: (email) => async (dispatch, getState) => {
    try {
      dispatch({ type: actions.AUTH_START });
      await service.signout(email);

      Analytics.track({
        event: Analytics.events.authSignout,
        state: getState(),
        properties: {
          email,
        },
      });

      // Reset the analytics user
      Analytics.reset();

      dispatch({
        type: actions.AUTH_SUCCESS,
        payload: {
          currentUser: null,
          jwt: null,
        },
      });
      dispatch({ type: 'USER_LOGOUT' });
      service.redirectToRoute('/');

      const graphqlClient = await getClient();
      await service.resetGraphqlClientCache(graphqlClient);
    } catch (error) {
      Errors.handle(error);

      dispatch({
        type: actions.AUTH_ERROR,
      });
    }
  },

  doSignoutInPlace: (email) => async (dispatch, getState) => {
    try {
      dispatch({ type: actions.AUTH_START });
      await service.signout(email);

      Analytics.track({
        event: Analytics.events.authSignout,
        state: getState(),
        properties: {
          email,
        },
      });

      // Reset the analytics user
      Analytics.reset();

      // dispatch({
      //   type: actions.AUTH_SUCCESS,
      //   payload: {
      //     currentUser: null,
      //     jwt: null,
      //   },
      // });

      const graphqlClient = await getClient();
      await service.resetGraphqlClientCache(graphqlClient);
    } catch (error) {
      Errors.handle(error);

      dispatch({
        type: actions.AUTH_ERROR,
      });
    }
  },

  doInit: () => async (dispatch, getState) => {
    const bearer = getState().auth.jwt;
    let currentUser;

    try {
      let restricted;

      try {
        currentUser = await service.fetchMe();
        const permissionChecker = new PermissionChecker(currentUser);
        restricted = permissionChecker.isRestrictedAccess;
      } catch {
        currentUser = null;
      }

      if (currentUser && config.authStrategy === 'cognito') {
        const mfaStatus = await service.doCheckMfa(currentUser.email);
        if (mfaStatus.mfaEnabled && !mfaStatus.mfaRegistered) {
          throw new Error('2FA is required by your account.');
        }
        dispatch({
          type: actions.SET_MFA,
          payload: {
            mfaEnabled: mfaStatus.mfaEnabled,
            mfaRegistered: mfaStatus.mfaRegistered,
          },
        });
      }

      dispatch({
        type: actions.AUTH_INIT_SUCCESS,
        payload: {
          currentUser,
          restricted,
        },
      });
    } catch (error) {
      await service.signout(currentUser?.email);

      dispatch({
        type: actions.AUTH_INIT_ERROR,
        payload: error,
      });
    }
  },

  doRefreshCurrentUser: () => async (dispatch, getState) => {
    try {
      dispatch({
        type: actions.CURRENT_USER_REFRESH_START,
      });

      const currentUser = await service.fetchMe();

      const permissionChecker = new PermissionChecker(currentUser);

      dispatch(initializeSessionThunk(currentUser));
      dispatch({
        type: actions.CURRENT_USER_REFRESH_SUCCESS,
        payload: {
          currentUser,
          restricted: permissionChecker.isRestrictedAccess,
        },
      });
    } catch (error) {
      await service.signout();
      Errors.handle(error);

      dispatch({
        type: actions.CURRENT_USER_REFRESH_ERROR,
        payload: error,
      });
    }
  },

  doUpdateProfile: (email, firstName, lastName, phoneNumber, avatars) => async (dispatch, getState) => {
    try {
      dispatch({
        type: actions.UPDATE_PROFILE_START,
      });

      await service.updateProfile(firstName, lastName, phoneNumber, avatars);

      // TODO: Call identify again here

      Analytics.track({
        event: Analytics.events.authProfileUpdate,
        state: getState(),
        properties: {
          email,
        },
      });

      dispatch({
        type: actions.UPDATE_PROFILE_SUCCESS,
      });
      dispatch(actions.doRefreshCurrentUser());
      Message.success(i18n('auth.profile.success'));
      service.redirectToRoute('/settings/profile');
    } catch (error) {
      Errors.handle(error);

      dispatch({
        type: actions.UPDATE_PROFILE_ERROR,
      });
    }
  },

  doRegisterMfa: (code) => async (dispatch, getState) => {
    try {
      dispatch({
        type: actions.MFA_REGISTER_START,
      });

      const ret = await service.doRegisterMfa(code);

      if (!ret) {
        dispatch({
          type: actions.MFA_ERROR,
          payload: i18n('mfa.register.error'),
        });
        return;
      }
      dispatch({ type: actions.AUTH_START });

      const currentUser = await service.fetchMe(code);
      // Identify the user to Analytics
      Analytics.identify({ user: currentUser });
      const permissionChecker = new PermissionChecker(currentUser);

      const mfaStatus = await service.doCheckMfa(currentUser.email);
      const mfaEnabled = currentUser.roles?.includes('Owner') ? true : mfaStatus.mfaEnabled;

      dispatch({
        type: actions.MFA_REGISTER_SUCCESS,
        payload: {
          currentUser,
          restricted: permissionChecker.isRestrictedAccess,
          mfaEnabled,
        },
      });

      Analytics.track({
        event: Analytics.events.authRegisterMfa,
        properties: {
          user: {
            currentUser,
          },
        },
      });

      const history = getHistory();
      if (history.location.pathname.includes('/auth')) {
        // redirect users to first accessible route
        service.redirectIfLimitedAccess(currentUser);
      }
    } catch (error) {
      log.error(error);
      await service.signout();
      dispatch({
        type: actions.MFA_ERROR,
        payload: i18n('mfa.register.error'),
      });
      Errors.handle(error);
    }
  },

  doVerifyMfa: (token, rememberMe, redirect) => async (dispatch, getState) => {
    try {
      dispatch({
        type: actions.MFA_VERIFY_START,
      });

      const ret = await service.doVerifyMfa(token, rememberMe);

      if (!ret) {
        dispatch({
          type: actions.MFA_ERROR,
          payload: null,
        });
        return;
      }

      dispatch({
        type: actions.SET_TOKEN,
        payload: ret.authSignIn,
      });
      dispatch({ type: actions.AUTH_START });

      const currentUser = await service.fetchMe(token);
      // Identify the user to Analytics
      Analytics.identify({ user: currentUser });
      const permissionChecker = new PermissionChecker(currentUser);

      dispatch({
        type: actions.MFA_VERIFY_SUCCESS,
        payload: {
          currentUser,
          restricted: permissionChecker.isRestrictedAccess,
        },
      });
      if (redirect) {
        // redirect to given route - e.g. settings/seats on request accepted
        service.redirectToRoute(redirect);
      } else {
        // redirect users to first accessible route
        service.redirectIfLimitedAccess(currentUser);
      }
    } catch (error) {
      await service.signout();
      dispatch({
        type: actions.MFA_ERROR,
        payload: error.message,
      });
      Errors.handle(error);
    }
  },

  doCheckMfa: (email) => async (dispatch, getState) => {
    try {
      const mfaStatus = config.authStrategy === 'cognito' ? await service.doCheckMfa(email) : { mfaRegistered: false, mfaEnabled: false };

      dispatch({
        type: actions.SET_MFA,
        payload: {
          mfaEnabled: mfaStatus.mfaEnabled || false,
          mfaRegistered: mfaStatus.mfaRegistered || false,
        },
      });
    } catch (error) {
      await service.signout();
      dispatch({
        type: actions.MFA_ERROR,
        payload: error.message,
      });
      Errors.handle(error);
    }
  },

  doResetMfa: (email) => async (dispatch, getState) => {
    try {
      dispatch({
        type: actions.MFA_RESET_START,
      });

      await service.doResetMfa(email);

      dispatch({
        type: actions.MFA_RESET_SUCCESS,
      });

      Message.success(`${email} will need to set their MFA on next sign in`);

      Analytics.track({
        event: Analytics.events.authResetMfa,
        properties: {
          'targetUser.email': email,
        },
      });

      return Promise.resolve(true);
    } catch (error) {
      dispatch({
        type: actions.MFA_ERROR,
        payload: error.message,
      });
      Errors.handle(error);

      return Promise.resolve(false);
    }
  },

  doVerifyEmail: (token) => async (dispatch, getState) => {
    try {
      dispatch({ type: actions.AUTH_START });

      const ret = await service.verifyEmail(token);

      if (ret === 'password') {
        dispatch({
          type: actions.TOKEN_ERROR,
          payload: null,
        });
        return;
      }

      const currentUser = await service.fetchMe();
      // Identify the user to Analytics
      Analytics.identify({ user: currentUser });
      const permissionChecker = new PermissionChecker(currentUser);

      Message.success(i18n('auth.verifyEmail.success'));

      Analytics.track({
        event: Analytics.events.authVerifiedEmail,
        state: getState(),
      });

      dispatch({
        type: actions.AUTH_SUCCESS,
        payload: {
          currentUser,
          restricted: permissionChecker.isRestrictedAccess,
        },
      });
      // redirect users to first accessible route
      service.redirectIfLimitedAccess(currentUser);
    } catch (error) {
      await service.signout();
      dispatch({
        type: actions.TOKEN_ERROR,
        payload: error.message,
      });
      Errors.handle(error);
      if (error?.message && error.message.includes('xpire')) {
        service.redirectToRoute('/auth/invite-expired');
      } else if (error?.message && error.message.includes('ancel')) {
        service.redirectToRoute('/auth/invite-canceled');
      } else {
        service.redirectToRoute('/');
      }
    }
  },

  doResetPassword: (token, email, password) => async (dispatch, getState) => {
    try {
      dispatch({
        type: actions.PASSWORD_RESET_START,
      });

      const user = await service.passwordReset(token, email, password);

      Message.success(i18n('auth.passwordResetSuccess'));

      Analytics.track({
        event: Analytics.events.authPasswordResetComplete,
        state: getState(),
        properties: {
          email: user.email,
        },
      });

      return true;
    } catch (error) {
      Errors.handle(error);

      dispatch({
        type: actions.PASSWORD_RESET_ERROR,
      });

      dispatch(actions.doSignout());
      service.redirectToRoute('/');
    }
  },

  doDeleteAccount: (data) => async (dispatch, getState) => {
    try {
      dispatch({ type: actions.DELETE_ACCOUNT_START });
      dispatch(doUpdateCurrentSubscription(data.subscriptionId, data.subscription, 'Immediate_Cancel'));
      // convert to orphan
      service.convertAccountToOrphan(data.accountId);

      dispatch(actions.doSignout(data.email));
      dispatch({ type: actions.DELETE_ACCOUNT_SUCCESS });
    } catch (error) {
      Errors.handle(error);

      dispatch({
        type: actions.DELETE_ACCOUNT_ERROR,
      });
    }
  },
};

export default actions;
