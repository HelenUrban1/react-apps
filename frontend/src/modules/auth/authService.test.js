import axios from 'modules/shared/network/axios';
import CryptoJS from 'crypto-js';
import config from 'config';
import AuthService from './authService';

jest.mock('config', () => ({
  authStrategy: 'cognito',
  backendUrl: 'https://ixc-local:8080/api',
  env: 'test',
  wsPath: '/api/socket.io/',
  autoBallooning: {
    enabled: false, // Whether to poll at all
    delay: 1000, // How long to wait until first check
    interval: 1000, // How long to wait between checks
    maxTotalSteps: 9,
  },
  remoteLogging: {
    isEnabled: false,
    level: 'trace',
    url: null, // `${backendUrl}/logger`
  },
  advancedCapture: {
    enabled: false, // Whether to show them or not
  },
  pdfTronKey: 'InspectionXpert Corporation(inspectionxpert.com):OEM:InspectionXpert Cloud::B+:AMS(20220228):68A5374D0437280A7360B13AC9A2737860617F9BEF56F50595B50BB4144C588024DABEF5C7',
}));

describe('AuthService', () => {
  beforeEach(() => {
    config.authStrategy = 'cognito';
  });

  describe('#signUpSubmit', () => {
    it('replaces email for username', async () => {
      const username = 'foo__plus__1__at__bar__dot__com';
      jest.spyOn(axios, 'post').mockImplementation(() => {
        return Promise.resolve({ data: { user: { username } }, status: 200 });
      });
      await AuthService.signUpSubmit('foo+1@bar.com', 'Person');
      expect(axios.post).toHaveBeenCalledWith(`${config.backendUrl}/cognitoSignup`, { email: 'foo+1@bar.com', firstName: 'Person', username });
    });
  });

  describe('#signinWithEmailAndPassword', () => {
    it('signs in without MFA', async () => {
      const email = 'foo@bar.com';
      const password = 'password';
      const encryptedPassword = 'iamencrypted';
      jest.spyOn(axios, 'post').mockImplementation(() => {
        return Promise.resolve({ data: { user: { email } } });
      });
      jest.spyOn(CryptoJS.AES, 'encrypt').mockImplementation(() => {
        return 'iamencrypted';
      });
      await AuthService.signinWithEmailAndPassword(email, password);
      expect(axios.post).toHaveBeenCalledWith(`${config.backendUrl}/cognitoSignin`, { email, password: encryptedPassword });
    });

    it('signs in with MFA', async () => {
      const email = 'foo@bar.com';
      const password = 'password';
      const encryptedPassword = 'iamencrypted';
      const mfaCode = '6969222';
      const rememberMe = true;
      jest.spyOn(axios, 'post').mockImplementation(() => {
        return Promise.resolve({ data: { user: { email } } });
      });
      jest.spyOn(CryptoJS.AES, 'encrypt').mockImplementation(() => {
        return 'iamencrypted';
      });
      await AuthService.signinWithEmailAndPassword(email, password, rememberMe, mfaCode);
      expect(axios.post).toHaveBeenCalledWith(`${config.backendUrl}/cognitoSignin`, { email, password: encryptedPassword, mfaCode, rememberMe });
    });
  });
});
