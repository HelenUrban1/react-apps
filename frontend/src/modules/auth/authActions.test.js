import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import authService from './authService';
import actions from './authActions';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('authActions', () => {
  describe('signout', () => {
    it('clears the Auth Token and GraphQL Cache and redirects to sign in', async () => {
      const store = mockStore({ auth: {} });
      const resetGraphqlClientCacheSpy = jest.spyOn(authService, 'resetGraphqlClientCache');
      const redirectToRouteSpy = jest.spyOn(authService, 'redirectToRoute');
      resetGraphqlClientCacheSpy.mockReturnValue(Promise.resolve());

      return store.dispatch(actions.doSignout()).then(() => {
        expect(resetGraphqlClientCacheSpy).toBeCalledTimes(1);
        expect(redirectToRouteSpy).toBeCalledTimes(1);
      });
    });
  });
});
