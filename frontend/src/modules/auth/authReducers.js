import { isNull } from 'lodash';
import actions from 'modules/auth/authActions';

const initialData = {
  accountCreating: false,
  currentUser: null,
  restricted: true,
  loadingInit: true,
  loadingEmailConfirmation: false,
  loadingPasswordResetEmail: false,
  loadingVerifyEmail: false,
  loadingPasswordReset: false,
  loadingUpdateProfile: false,
  loadingMfaVerification: false,
  loadingMfaRegistration: false,
  loading: false,
  jwt: null,
  errorMessage: null,
  passwordExists: false,
  deviceKey: null,
  requestPolicy: null,
  requestSuccess: false,
  mfaEnabled: false,
  mfaRegistered: false,
  session: null,
};

export default (state = initialData, { type, payload }) => {
  if (type === actions.ERROR_MESSAGE_CLEARED) {
    return {
      ...state,
      errorMessage: null,
    };
  }

  if (type === actions.SET_DEVICE_KEY) {
    return {
      ...state,
      deviceKey: payload,
    };
  }

  if (type === actions.CURRENT_USER_REFRESH_SUCCESS) {
    return {
      ...state,
      currentUser: payload.currentUser || null,
      session: payload.session,
      restricted: payload.restricted,
    };
  }

  if (type === actions.TOKEN_VALID) {
    if (state.currentUser?.signupToken === payload.currentUser?.signupToken) {
      return state;
    }
    return {
      ...state,
      currentUser: payload.currentUser || null,
      restricted: payload.restricted,
      errorMessage: null,
      loading: false,
    };
  }

  if (type === actions.TOKEN_ERROR) {
    return {
      ...state,
      currentUser: null,
      errorMessage: payload || null,
      loadingMfaVerification: false,
      loading: false,
    };
  }

  if (type === actions.CHECK_PASSWORD) {
    return {
      ...state,
      passwordExists: payload || false,
    };
  }

  if (type === actions.AUTH_START) {
    return {
      ...state,
      errorMessage: null,
      loading: true,
    };
  }

  if (type === actions.AUTH_SUCCESS) {
    return {
      ...state,
      currentUser: payload.currentUser || null,
      session: payload.session,
      restricted: payload.restricted,
      jwt: payload.jwt,
      errorMessage: null,
      loading: false,
    };
  }

  if (type === actions.AUTH_ERROR) {
    return {
      ...state,
      currentUser: null,
      errorMessage: payload || null,
      loading: false,
    };
  }

  if (type === actions.SET_TOKEN) {
    return {
      ...state,
      jwt: payload,
    };
  }

  if (type === actions.DELETE_ACCOUNT_START) {
    return {
      ...state,
      errorMessage: null,
      loading: true,
    };
  }

  if (type === actions.DELETE_ACCOUNT_SUCCESS) {
    return {
      ...state,
      currentUser: null,
      session: null,
      restricted: isNull,
      errorMessage: null,
      loading: false,
    };
  }

  if (type === actions.DELETE_ACCOUNT_ERROR) {
    return {
      ...state,
      currentUser: null,
      session: null,
      errorMessage: payload || null,
      loading: false,
    };
  }

  if (type === actions.REQUEST_ACCOUNT_ACCESS) {
    return {
      ...state,
      requestPolicy: payload,
      loading: false,
    };
  }

  if (type === actions.REQUEST_POLICY_CLEAR) {
    return {
      ...state,
      requestPolicy: undefined,
    };
  }

  if (type === actions.REQUEST_SUCCESS) {
    return {
      ...state,
      requestSuccess: true,
      errorMessage: null,
      loading: false,
    };
  }

  if (type === actions.EMAIL_CONFIRMATION_START) {
    return {
      ...state,
      loadingEmailConfirmation: true,
    };
  }

  if (type === actions.EMAIL_CONFIRMATION_SUCCESS) {
    return {
      ...state,
      loadingEmailConfirmation: false,
    };
  }

  if (type === actions.EMAIL_CONFIRMATION_ERROR) {
    return {
      ...state,
      loadingEmailConfirmation: false,
    };
  }

  if (type === actions.PASSWORD_RESET_EMAIL_START) {
    return {
      ...state,
      loadingPasswordResetEmail: true,
    };
  }

  if (type === actions.PASSWORD_RESET_EMAIL_SUCCESS) {
    return {
      ...state,
      loadingPasswordResetEmail: false,
    };
  }

  if (type === actions.PASSWORD_RESET_EMAIL_ERROR) {
    return {
      ...state,
      loadingPasswordResetEmail: false,
    };
  }

  if (type === actions.UPDATE_PROFILE_START) {
    return {
      ...state,
      loadingUpdateProfile: true,
    };
  }

  if (type === actions.UPDATE_PROFILE_SUCCESS) {
    return {
      ...state,
      loadingUpdateProfile: false,
    };
  }

  if (type === actions.UPDATE_PROFILE_ERROR) {
    return {
      ...state,
      loadingUpdateProfile: false,
    };
  }

  if (type === actions.CURRENT_USER_SET_SHALLOW) {
    return {
      ...state,
      currentUser: payload,
    };
  }

  if (type === actions.AUTH_INIT_SUCCESS) {
    return {
      ...state,
      currentUser: payload.currentUser || null,
      session: payload.session || state.session,
      restricted: payload.restricted,
      loadingInit: false,
    };
  }

  if (type === actions.AUTH_INIT_ERROR) {
    return {
      ...state,
      currentUser: null,
      session: null,
      loadingInit: false,
    };
  }

  if (type === actions.SET_MFA) {
    return {
      ...state,
      mfaEnabled: payload.mfaEnabled !== undefined ? payload.mfaEnabled : state.mfaEnabled,
      mfaRegistered: payload.mfaRegistered !== undefined ? payload.mfaRegistered : state.mfaRegistered,
    };
  }

  if (type === actions.MFA_VERIFY_START) {
    return {
      ...state,
      loadingMfaVerification: true,
    };
  }

  if (type === actions.MFA_ERROR) {
    return {
      ...state,
      loadingMfaVerification: false,
      loadingMfaRegistration: false,
      loading: false,
      errorMessage: payload || null,
    };
  }

  if (type === actions.MFA_VERIFY_SUCCESS) {
    return {
      ...state,
      loadingMfaVerification: false,
      currentUser: payload.currentUser || null,
      session: payload.session || state.session,
      restricted: payload.restricted,
      errorMessage: null,
    };
  }

  if (type === actions.MFA_REGISTER_START) {
    return {
      ...state,
      loadingMfaRegistration: true,
    };
  }

  if (type === actions.MFA_REGISTER_SUCCESS) {
    return {
      ...state,
      loadingMfaRegistration: false,
      currentUser: payload.currentUser || null,
      session: payload.session || state.session,
      restricted: payload.restricted,
      mfaEnabled: payload.mfaEnabled || false,
      mfaRegistered: true,
      errorMessage: null,
    };
  }

  if (type === actions.REMOVE_CURRENT_USER) {
    return {
      ...state,
      currentUser: payload.currentUser,
      jwt: payload.jwt,
    };
  }

  return state;
};
