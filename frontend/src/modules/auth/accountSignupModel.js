import StringField from 'modules/shared/fields/stringField';
import IdField from 'modules/shared/fields/idField';
import EnumeratorField from 'modules/shared/fields/enumeratorField';
import BooleanField from 'modules/shared/fields/booleanField';
import { i18n } from '../../i18n';

function enumeratorLabel(name, value) {
  return i18n(`entities.account.enumerators.${name}.${value}`);
}

function userLabel(name) {
  return i18n(`user.fields.${name}`);
}

function accountLabel(name) {
  return i18n(`entities.account.fields.${name}`);
}

const fields = {
  id: new IdField('id', userLabel('id')),
  firstName: new StringField('firstName', userLabel('firstName'), {
    max: 80,
    required: true,
  }),
  lastName: new StringField('lastName', userLabel('lastName'), {
    max: 175,
  }),
  password: new StringField('password', userLabel('newPassword'), {
    required: true,
  }),
  confirmPassword: new StringField('confirmPassword', userLabel('confirmPassword'), {
    required: true,
  }),
  email: new StringField('email', userLabel('email'), {
    required: true,
    max: 255,
  }),
  adminEmail: new StringField('adminEmail', userLabel('adminEmail'), {
    required: true,
    max: 80,
  }),
  phoneNumber: new StringField('phoneNumber', userLabel('phoneNumber'), {
    matches: /^[0-9]/,
    errorMessage: userLabel('phoneNumberError'),
    max: 24,
  }),
  termsAndConditions: new BooleanField('termsAndConditions', userLabel('acceptTermsAndConditions')),

  accountId: new IdField('accountId', accountLabel('accountId')),
  orgName: new StringField('orgName', accountLabel('orgName'), {
    min: 2,
    required: true,
  }),
  orgPhone: new StringField('orgPhone', accountLabel('orgPhone'), {}),
  orgEmail: new StringField('orgEmail', accountLabel('orgEmail'), {
    required: true,
  }),
  orgStreet: new StringField('orgStreet', accountLabel('orgStreet'), {
    required: true,
  }),
  orgStreet2: new StringField('orgStreet2', accountLabel('orgStreet2'), {}),
  orgCity: new StringField('orgCity', accountLabel('orgCity'), {
    required: true,
  }),
  orgRegion: new EnumeratorField(
    'orgRegion',
    accountLabel('orgRegion'),
    [
      { id: 'Alabama', label: enumeratorLabel('orgRegion', 'Alabama') },
      { id: 'Alaska', label: enumeratorLabel('orgRegion', 'Alaska') },
      { id: 'American_Samoa', label: enumeratorLabel('orgRegion', 'American_Samoa') },
      { id: 'Arizona', label: enumeratorLabel('orgRegion', 'Arizona') },
      { id: 'Arkansas', label: enumeratorLabel('orgRegion', 'Arkansas') },
      { id: 'California', label: enumeratorLabel('orgRegion', 'California') },
      { id: 'Colorado', label: enumeratorLabel('orgRegion', 'Colorado') },
      { id: 'Connecticut', label: enumeratorLabel('orgRegion', 'Connecticut') },
      { id: 'Delaware', label: enumeratorLabel('orgRegion', 'Delaware') },
      { id: 'District_of_Columbia', label: enumeratorLabel('orgRegion', 'District_of_Columbia') },
      { id: 'Florida', label: enumeratorLabel('orgRegion', 'Florida') },
      { id: 'Georgia', label: enumeratorLabel('orgRegion', 'Georgia') },
      { id: 'Guam', label: enumeratorLabel('orgRegion', 'Guam') },
      { id: 'Hawaii', label: enumeratorLabel('orgRegion', 'Hawaii') },
      { id: 'Idaho', label: enumeratorLabel('orgRegion', 'Idaho') },
      { id: 'Illinois', label: enumeratorLabel('orgRegion', 'Illinois') },
      { id: 'Indiana', label: enumeratorLabel('orgRegion', 'Indiana') },
      { id: 'Iowa', label: enumeratorLabel('orgRegion', 'Iowa') },
      { id: 'Kansas', label: enumeratorLabel('orgRegion', 'Kansas') },
      { id: 'Kentucky', label: enumeratorLabel('orgRegion', 'Kentucky') },
      { id: 'Louisiana', label: enumeratorLabel('orgRegion', 'Louisiana') },
      { id: 'Maine', label: enumeratorLabel('orgRegion', 'Maine') },
      { id: 'Maryland', label: enumeratorLabel('orgRegion', 'Maryland') },
      { id: 'Massachusetts', label: enumeratorLabel('orgRegion', 'Massachusetts') },
      { id: 'Michigan', label: enumeratorLabel('orgRegion', 'Michigan') },
      { id: 'Minnesota', label: enumeratorLabel('orgRegion', 'Minnesota') },
      {
        id: 'Minor_Outlying_Islands',
        label: enumeratorLabel('orgRegion', 'Minor_Outlying_Islands'),
      },
      { id: 'Mississippi', label: enumeratorLabel('orgRegion', 'Mississippi') },
      { id: 'Missouri', label: enumeratorLabel('orgRegion', 'Missouri') },
      { id: 'Montana', label: enumeratorLabel('orgRegion', 'Montana') },
      { id: 'Nebraska', label: enumeratorLabel('orgRegion', 'Nebraska') },
      { id: 'Nevada', label: enumeratorLabel('orgRegion', 'Nevada') },
      { id: 'New_Hampshire', label: enumeratorLabel('orgRegion', 'New_Hampshire') },
      { id: 'New_Jersey', label: enumeratorLabel('orgRegion', 'New_Jersey') },
      { id: 'New_Mexico', label: enumeratorLabel('orgRegion', 'New_Mexico') },
      { id: 'New_York', label: enumeratorLabel('orgRegion', 'New_York') },
      { id: 'North_Carolina', label: enumeratorLabel('orgRegion', 'North_Carolina') },
      { id: 'North_Dakota', label: enumeratorLabel('orgRegion', 'North_Dakota') },
      {
        id: 'Northern_Mariana_Islands',
        label: enumeratorLabel('orgRegion', 'Northern_Mariana_Islands'),
      },
      { id: 'Ohio', label: enumeratorLabel('orgRegion', 'Ohio') },
      { id: 'Oklahoma', label: enumeratorLabel('orgRegion', 'Oklahoma') },
      { id: 'Oregon', label: enumeratorLabel('orgRegion', 'Oregon') },
      { id: 'Pennsylvania', label: enumeratorLabel('orgRegion', 'Pennsylvania') },
      { id: 'Puerto_Rico', label: enumeratorLabel('orgRegion', 'Puerto_Rico') },
      { id: 'Rhode_Island', label: enumeratorLabel('orgRegion', 'Rhode_Island') },
      { id: 'South_Carolina', label: enumeratorLabel('orgRegion', 'South_Carolina') },
      { id: 'South_Dakota', label: enumeratorLabel('orgRegion', 'South_Dakota') },
      { id: 'Tennessee', label: enumeratorLabel('orgRegion', 'Tennessee') },
      { id: 'Texas', label: enumeratorLabel('orgRegion', 'Texas') },
      { id: 'US_Virgin_Islands', label: enumeratorLabel('orgRegion', 'US_Virgin_Islands') },
      { id: 'Utah', label: enumeratorLabel('orgRegion', 'Utah') },
      { id: 'Vermont', label: enumeratorLabel('orgRegion', 'Vermont') },
      { id: 'Virginia', label: enumeratorLabel('orgRegion', 'Virginia') },
      { id: 'Washington', label: enumeratorLabel('orgRegion', 'Washington') },
      { id: 'West_Virginia', label: enumeratorLabel('orgRegion', 'West_Virginia') },
      { id: 'Wisconsin', label: enumeratorLabel('orgRegion', 'Wisconsin') },
      { id: 'Wyoming', label: enumeratorLabel('orgRegion', 'Wyoming') },
    ],
    {
      required: true,
    },
  ),
  orgPostalcode: new StringField('orgPostalcode', accountLabel('orgPostalcode'), {
    required: true,
  }),
  orgCountry: new EnumeratorField('orgCountry', accountLabel('orgCountry'), [{ id: 'United_States', label: enumeratorLabel('orgCountry', 'United_States') }], {
    required: true,
  }),
};

export default {
  fields,
};
