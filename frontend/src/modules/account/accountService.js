import gql from 'graphql-tag';
import getClient from 'modules/shared/graphql/graphqlClient';

export default class AccountService {
  static async update(id, data) {
    const graphqlClient = await getClient();
    const response = await graphqlClient.mutate({
      mutation: gql`
        mutation ACCOUNT_UPDATE($id: String!, $data: AccountInput!) {
          accountUpdate(id: $id, data: $data) {
            id
          }
        }
      `,

      variables: {
        id,
        data,
      },
    });

    return response.data.accountUpdate;
  }

  static async destroyAll(ids) {
    const graphqlClient = await getClient();
    const response = await graphqlClient.mutate({
      mutation: gql`
        mutation ACCOUNT_DESTROY($ids: [String!]!) {
          accountDestroy(ids: $ids)
        }
      `,

      variables: {
        ids,
      },
    });

    return response.data.accountDestroy;
  }

  static async create(data) {
    const graphqlClient = await getClient();
    const response = await graphqlClient.mutate({
      mutation: gql`
        mutation ACCOUNT_CREATE($data: AccountInput!) {
          accountCreate(data: $data) {
            id
          }
        }
      `,

      variables: {
        data,
      },
    });

    return response.data.accountCreate;
  }

  static async import(values, importHash) {
    const graphqlClient = await getClient();
    const response = await graphqlClient.mutate({
      mutation: gql`
        mutation ACCOUNT_IMPORT($data: AccountInput!, $importHash: String!) {
          accountImport(data: $data, importHash: $importHash)
        }
      `,

      variables: {
        data: values,
        importHash,
      },
    });

    return response.data.accountImport;
  }

  static async find(id) {
    const graphqlClient = await getClient();
    const response = await graphqlClient.query({
      query: gql`
        query ACCOUNT_FIND($id: String!) {
          accountFind(id: $id) {
            id
            status
            settings
            orgName
            orgPhone
            orgEmail
            orgStreet
            orgStreet2
            orgCity
            orgRegion
            orgPostalcode
            orgCountry
            orgLogo {
              id
              name
              sizeInBytes
              publicUrl
              privateUrl
            }
            orgUnitName
            orgUnitId
            parentAccountId
            members {
              id
            }
            createdAt
            updatedAt
            dbHost
            dbName
          }
        }
      `,

      variables: {
        id,
      },
      fetchPolicy: 'no-cache',
    });

    return response.data.accountFind;
  }

  static async list(filter, orderBy, limit, offset) {
    const graphqlClient = await getClient();
    const response = await graphqlClient.query({
      query: gql`
        query ACCOUNT_LIST($filter: AccountFilterInput, $orderBy: AccountOrderByEnum, $limit: Int, $offset: Int) {
          accountList(filter: $filter, orderBy: $orderBy, limit: $limit, offset: $offset) {
            count
            rows {
              id
              status
              settings
              orgName
              orgPhone
              orgEmail
              orgStreet
              orgStreet2
              orgCity
              orgRegion
              orgPostalcode
              orgCountry
              orgLogo {
                id
                name
                sizeInBytes
                publicUrl
                privateUrl
              }
              orgUnitName
              orgUnitId
              parentAccountId
              members {
                id
              }
              updatedAt
              createdAt
            }
          }
        }
      `,

      variables: {
        filter,
        orderBy,
        limit,
        offset,
      },
    });

    return response.data.accountList;
  }

  static async listAutocomplete(query, limit) {
    const graphqlClient = await getClient();
    const response = await graphqlClient.query({
      query: gql`
        query ACCOUNT_AUTOCOMPLETE($query: String, $limit: Int) {
          accountAutocomplete(query: $query, limit: $limit) {
            id
            label
          }
        }
      `,

      variables: {
        query,
        limit,
      },
    });

    return response.data.accountAutocomplete;
  }
}
