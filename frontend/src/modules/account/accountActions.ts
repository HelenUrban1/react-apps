import { ApolloClient } from '@apollo/client';
import { Dispatch } from 'redux';
import service from './accountService';
import Errors from '../shared/error/errors';

const prefix = 'ACCOUNT';

export const AccountActions = {
  START: `${prefix}_START`,
  SUCCESS: `${prefix}_SUCCESS`,
  ERROR: `${prefix}_ERROR`,
};

export enum AccountActionTypes {
  START = 'ACCOUNT_START',
  SUCCESS = 'ACCOUNT_SUCCESS',
  ERROR = 'ACCOUNT_ERROR',
}

export const doSetAccount = (accountId: string) => async (dispatch: Dispatch) => {
  try {
    dispatch({ type: AccountActions.START });
    let account = await service.find(accountId);

    dispatch({
      type: AccountActions.SUCCESS,
      payload: {
        account,
      },
    });
  } catch (error) {
    Errors.handle(error);

    dispatch({
      type: AccountActions.ERROR,
      payload: error,
    });
  }
};

export const doUpdateAccount = (accountId: string, data: any) => async (dispatch: Dispatch, getState: any, clientPromise: Promise<ApolloClient<any>>) => {
  try {
    let updatedAccount = await service.update(accountId, data);
    dispatch({
      type: AccountActions.SUCCESS,
      payload: {
        account: {
          id: updatedAccount.id,
          ...data,
        },
      },
    });
  } catch (error) {
    Errors.handle(error);

    dispatch({
      type: AccountActions.ERROR,
      payload: error,
    });
  }
};

export default AccountActions;
