import './accountActions';
import { AccountState, AccountAction } from '../state.d';

const initialData: AccountState = {
  id: null,
  status: null,
  settings: null,
  orgName: null,
  orgPhone: null,
  orgEmail: null,
  orgStreet: null,
  orgStreet2: null,
  orgCity: null,
  orgRegion: null,
  orgPostalcode: null,
  orgCountry: null,
  orgLogo: null,
  orgUnitName: null,
  orgUnitId: null,
  parentAccountId: null,
  members: null,
  createdAt: null,
  updatedAt: null,
  loading: false,
  errorMessage: null,
  dbHost: null,
  dbName: null,
};

export default (state = initialData, { type, payload }: AccountAction): AccountState => {
  switch (type) {
    case 'ACCOUNT_START':
      return {
        ...state,
        loading: true,
      };
    case 'ACCOUNT_SUCCESS':
      return {
        ...state,
        ...payload.account,
        loading: false,
      };
    case 'ACCOUNT_ERROR':
      return {
        ...state,
        errorMessage: payload || null,
        loading: false,
      };
    default:
      return state;
  }
};
