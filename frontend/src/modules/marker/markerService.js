import gql from 'graphql-tag';
import getClient from 'modules/shared/graphql/graphqlClient';

export default class MarkerService {
  static async createDefaults(accountId) {
    const graphqlClient = await getClient();
    const response = await graphqlClient.mutate({
      mutation: gql`
        mutation MARKER_CREATE_DEFAULTS($accountId: String!) {
          markerCreateDefaults(accountId: $accountId)
        }
      `,

      variables: {
        accountId,
      },
    });

    return response.data.siteCreate;
  }
}
