import { connectRouter } from 'connected-react-router';
import layout from 'modules/layout/layoutReducers';
import auth from 'modules/auth/authReducers';
import iam from 'modules/iam/iamReducers';
import auditLog from 'modules/auditLog/auditLogReducers';
// import settings from 'modules/setting/settingReducers';
import account from 'modules/account/accountReducers';
import site from 'modules/site/siteReducers';
import metro from 'modules/metro/metroReducers';
import subscription from 'modules/subscription/subscriptionReducers';
import session from 'modules/session/sessionReducers';
import report from 'modules/report/reportReducers';
import template from 'modules/template/templateReducers';
import part from 'modules/part/partReducer';
import pdfView from 'modules/pdfView/pdfViewReducer';
import pdfOverlayView from 'modules/pdfOverlayView/pdfViewReducer';
import partEditor from 'modules/partEditor/partEditorReducer';
import navigation from 'modules/navigation/navigationReducer';
import characteristics from 'modules/characteristic/characteristicReducer';
import notification from 'modules/notification/notificationReducer';
import reportTemplates from 'modules/reportTemplate/reportTemplateReducers';
import { combineReducers } from 'redux';

const appReducer = (history) =>
  combineReducers({
    router: connectRouter(history),
    layout,
    auth,
    iam,
    auditLog,
    // settings,
    account,
    characteristics,
    site,
    session,
    template,
    report,
    metro,
    part,
    pdfView,
    pdfOverlayView,
    partEditor,
    notification,
    navigation,
    subscription,
    reportTemplates,
  });

export const resetEnhancer = (rootReducer) => (state, action) => {
  if (action.type !== 'USER_LOGOUT') return rootReducer(state, action);

  const newState = rootReducer(undefined, {});
  newState.router = state.router;
  newState.auth = state.auth;
  return newState;
};

export default appReducer;
