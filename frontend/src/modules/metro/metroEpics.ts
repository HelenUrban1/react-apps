import { Epic, ActionsObservable, StateObservable } from 'redux-observable';
import { Dispatch } from 'redux';
import { ApolloClient, NormalizedCacheObject } from '@apollo/client';
import { catchError, filter, map, switchMap } from 'rxjs/operators';
import { isOfType } from 'typesafe-actions';
import { forkJoin, from } from 'rxjs';
import { compact } from 'lodash';
import { Characteristic } from 'types/characteristics';
import { checkSampleStatus } from 'utils/DataCalculation';
import { Samples } from 'metro/graphql/samples';
import { Measurement } from 'types/measurement';
import { Sample } from 'types/sample';
import MetroAction, { CreateMeasurementPayload, createMutation } from './metroActions';
import { MetroAction as MetroActions, AppState, MetroActionTypes, MetroState } from '../state';

export const updateAfterMeasure = (samples: Sample[], measurement: Measurement, features: Characteristic[], metroState: MetroState, dispatch: Dispatch, del = false): Sample | null => {
  const newSamples = samples.map((samp: Sample) => {
    return { ...samp };
  });
  const newSample = newSamples.find((s: Sample) => s.id === measurement.sampleId);

  if (newSample) {
    const mIndex = newSample!.measurements.findIndex((m: Measurement) => m.id === measurement.id);
    newSample.measurements = newSample.measurements.map((meas: Measurement) => {
      return { ...meas };
    });

    if (mIndex >= 0) {
      if (del) {
        newSample.measurements.splice(mIndex, 1);
      } else {
        newSample.measurements[mIndex] = measurement;
      }
    } else {
      newSample.measurements.push(measurement);
    }

    newSamples[newSamples.findIndex((s: Sample) => s.id === measurement.sampleId)] = newSample;

    dispatch({ type: MetroAction.SET_SAMPLES, payload: newSamples });

    const featureMeasurements = newSample.measurements.filter((m: Measurement) => m.characteristicId === measurement.characteristicId);

    newSample.status = checkSampleStatus(newSample, features);

    const currentCell = metroState.selectedCell;
    const newIndex = del ? currentCell.measurementIndex : currentCell.measurementIndex + 1;
    dispatch({ type: MetroAction.DO_SELECT_CELL, payload: { ...currentCell, selectedSample: newSample, measurementIndex: newIndex, selectedMeasurement: featureMeasurements[newIndex] || null } });

    return newSample;
  }

  return null;
};

// prettier-ignore
// Moved to a thunk in metroActions - Currently not used
export const createMeasurementEpic: Epic<MetroActions, MetroActions, AppState> = (action$: ActionsObservable<MetroActions>, state$: StateObservable<AppState>, { clientPromise }) =>
  action$.pipe(
    filter(isOfType(MetroAction.CREATE_MEASUREMENT)),
    switchMap(({ payload }: MetroActions) =>
      from<PromiseLike<ApolloClient<NormalizedCacheObject>>>(clientPromise).pipe(
        switchMap((client: ApolloClient<NormalizedCacheObject>) => {
          const inputs = payload as CreateMeasurementPayload;
          if (! inputs) return [];
          const { samples: partSamples } = state$.value.metro;

          const calls = inputs.map((measurement) => {
            return from(
              createMutation({ data: measurement }, client, [
                {
                  query: Samples.query.find,
                  variables: {
                    id: measurement.sampleId,
                  },
                },
              ])
            );
          });

          return forkJoin(calls).pipe(
            map((res) => {
              const samples = partSamples ? [ ...partSamples ] : [];
              const createdMeasurements = compact(res.map((call) => (call && call.data ? call.data.measurementCreate : undefined)));

              const updatedSamples = samples.map((sample) => { return { ...sample } });

              createdMeasurements.forEach((measurement) => {
                // Add new measurement onto samples
                const measuredSample = updatedSamples.find((sample) => sample.id === measurement.sample.id);
                if (measuredSample) {
                  const newMeasurements = measuredSample.measurements.map((m: Measurement) => { return { ...m }});
                  measuredSample.measurements = [...measuredSample.measurements];
                  newMeasurements.push(measurement);
                  measuredSample.measurements = newMeasurements;
                }
              });
              return {
                type: MetroAction.SET_SAMPLES as MetroActionTypes,
                payload: updatedSamples,
              };
             }),
            catchError((error) => [
              {
                type: MetroAction.CREATE_MEASUREMENT_ERROR as MetroActionTypes,
                payload: error,
              },
            ])
          )
        })
      )
    )
  );
