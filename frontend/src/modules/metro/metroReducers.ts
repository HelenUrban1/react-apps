import actions from 'modules/metro/metroActions';
import { MetroAction, MetroState } from 'modules/state';

const initialData = {
  operator: null,
  operators: [],
  station: null,
  stations: [],
  job: null,
  drawing: null,
  samples: null,
  installed: false,
  initialized: false,
  selectedCell: {
    featureIndex: 0,
    sampleIndex: 0,
    measurementIndex: 0,
    selectedFeature: null,
    selectedSample: null,
    selectedMeasurement: null,
  },
  gage: null,
};

export default (state: MetroState = initialData, { type, payload }: MetroAction) => {
  if (type === actions.DO_INIT_METRO) {
    return {
      ...state,
      ...payload,
      initialized: true,
    };
  }
  if (type === actions.DO_INIT_MEASURE) {
    return {
      ...state,
      ...payload,
    };
  }
  if (type === actions.DO_SELECT_CELL) {
    return {
      ...state,
      selectedCell: payload,
    };
  }
  if (type === actions.SET_OPERATOR) {
    return {
      ...state,
      operator: payload,
    };
  }
  if (type === actions.SET_STATION) {
    return {
      ...state,
      station: payload,
    };
  }
  if (type === actions.SET_STATIONS) {
    return {
      ...state,
      stations: payload,
    };
  }
  if (type === actions.SET_JOB) {
    return {
      ...state,
      job: payload,
    };
  }
  if (type === actions.SET_GAGE) {
    return {
      ...state,
      gage: payload,
    };
  }
  if (type === actions.SET_DRAWING) {
    return {
      ...state,
      drawing: payload,
    };
  }
  if (type === actions.FETCH_SAMPLES) {
    return {
      ...state,
      samples: payload,
    };
  }
  if (type === actions.SET_SAMPLES) {
    return {
      ...state,
      samples: payload,
    };
  }
  if (type === actions.CREATE_MEASUREMENT_ERROR) {
    return {
      ...state,
    };
  }
  if (type === actions.CREATE_OPERATOR) {
    return {
      ...state,
      operators: [...state.operators, payload],
    };
  }
  if (type === actions.DELETE_OPERATOR) {
    return {
      ...state,
      operators: state.operators.filter((op) => op.id !== payload),
    };
  }
  if (type === actions.UPDATE_OPERATOR) {
    return {
      ...state,
      operators: state.operators.map((operator) => (operator.id === payload.id ? payload : operator)),
    };
  }
  if (type === actions.SET_INSTALLED) {
    return {
      ...state,
      installed: payload,
    };
  }
  if (type === actions.UPDATE_SAMPLE) {
    return {
      ...state,
      samples: payload,
    };
  }
  return state;
};
