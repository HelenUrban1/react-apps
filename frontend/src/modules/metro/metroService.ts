import gql from 'graphql-tag';
import getClient from 'modules/shared/graphql/graphqlClient';

export default class MetroService {
  static async fetchStations() {
    const graphqlClient = await getClient();
    const response = await graphqlClient.query({
      query: gql`
        {
          stationList {
            count
            rows {
              id
              name
              disabled
              isMaster
              createdAt
              updatedAt
            }
          }
        }
      `,
    });

    return response.data.stationList;
  }

  static async fetchJob(jobId: string) {
    const graphqlClient = await getClient();
    const response = await graphqlClient.query({
      variables: {
        id: jobId,
      },
      query: gql`
        query JOB_FIND($id: String!) {
          jobFind(id: $id) {
            id
            name
            parts {
              id
              name
            }
            jobSamples {
              id
              sampleIndex
              serial
              status
              partId
            }
            jobStatus
            sampling
            interval
            samples
            passing
            scrapped
            accessControl
            createdAt
            updatedAt
          }
        }
      `,
    });

    return response.data.jobFind;
  }

  static async fetchSamples(jobId: string, partId: string) {
    const graphqlClient = await getClient();
    const response = await graphqlClient.query({
      variables: {
        filter: { job: jobId, part: partId },
      },
      query: gql`
        query SAMPLE_LIST($filter: SampleFilterInput!) {
          sampleList(filter: $filter) {
            count
            rows {
              id
              serial
              sampleIndex
              featureCoverage
              status
              partId
              part {
                id
              }
              job {
                id
              }
              measurements {
                id
                value
                gage
                machine
                status
                characteristicId
                sampleId
                stationId
                operationId
                methodId
                createdById
                updatedById
              }
              createdAt
              updatedAt
            }
          }
        }
      `,
    });

    return response.data.sampleList;
  }
}
