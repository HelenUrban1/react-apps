import { Station } from 'types/station';
import { Dispatch } from 'redux';
import Errors from 'modules/shared/error/errors';
import service from 'modules/metro/metroService';
import { Sample } from 'types/sample';
import { Measurement, MeasurementInput } from 'types/measurement';
import { ApolloClient, NormalizedCacheObject } from '@apollo/client';
import { Measurements, MeasurementCreate, MeasurementCreateVars, MeasurementEdit, MeasurementEditVars, MeasurementDestroy, MeasurementDestroyVars } from 'metro/graphql/measurements';
import { SelectedCell, AppState } from 'modules/state';
import MetroService from 'modules/metro/metroService';
import { Drawing } from 'graphql/drawing';
import OperatorService from 'modules/operator/operatorService';

import { Samples } from 'metro/graphql/samples';
import { Characteristic } from 'types/characteristics';
import { Operator, OperatorCreate, OperatorInput } from 'types/operator';
import { updateAfterMeasure } from './metroEpics';

export interface OperatorState {
  operator: string;
  station: string;
}

const prefix = 'METRO';

const sortStations = (stations: Station[]) => {
  return [...stations].sort((a: Station, b: Station) => {
    if (a.isMaster) return -1;
    if (b.isMaster) return 1;
    return a.name.localeCompare(b.name);
  });
};

const actions = {
  DO_INIT_METRO: `${prefix}_DO_INIT_METRO`,
  DO_INIT_MEASURE: `${prefix}_DO_INIT_MEASURE`,
  DO_SELECT_CELL: `${prefix}_DO_SELECT_CELL`,
  CREATE_MEASUREMENT: `${prefix}_CREATE_MEASUREMENT`,
  CREATE_MEASUREMENT_ERROR: `${prefix}_CREATE_MEASUREMENT_ERROR`,
  CREATE_OPERATOR: `${prefix}_CREATE_OPERATOR`,
  DELETE_OPERATOR: `${prefix}_DELETE_OPERATOR`,
  UPDATE_MEASUREMENT: `${prefix}_UPDATE_MEASUREMENT`,
  SET_OPERATOR: `${prefix}_SET_OPERATOR`,
  UPDATE_OPERATOR: `${prefix}_UPDATE_OPERATOR`,
  SET_STATION: `${prefix}_SET_STATION`,
  SET_STATIONS: `${prefix}_SET_STATIONS`,
  SET_JOB: `${prefix}_SET_JOB`,
  SET_DRAWING: `${prefix}_SET_DRAWING`,
  FETCH_SAMPLES: `${prefix}_FETCH_SAMPLES`,
  SET_SAMPLES: `${prefix}_SET_SAMPLES`,
  UPDATE_SAMPLE: `${prefix}_UPDATE_SAMPLE`,
  SET_INSTALLED: `${prefix}_SET_INSTALLED`,
  SET_GAGE: `${prefix}_SET_GAGE`,

  doInitMetro: (accountMemberId: string, locState: OperatorState) => async (dispatch: Dispatch) => {
    try {
      const stationData = await service.fetchStations();
      const operatorData = await OperatorService.list({});

      dispatch({
        type: actions.DO_INIT_METRO,
        payload: {
          stations: sortStations(stationData.rows || []),
          station: locState?.station ? stationData.rows.find((st: Station) => st.id === locState.station) : null,
          operators: operatorData.rows,
          operator: locState?.operator ? operatorData.rows.find((op: any) => op.id === locState.operator) : null,
        },
      });
    } catch (error) {
      Errors.handle(error);
    }
  },
  doInitMeasure: (jobId: string, partId: string) => async (dispatch: Dispatch) => {
    const job = jobId ? await service.fetchJob(jobId) : null;
    const samples = await MetroService.fetchSamples(jobId, partId);

    dispatch({
      type: actions.DO_INIT_MEASURE,
      payload: {
        job,
        samples: samples.rows || [],
      },
    });
  },
  doAddOperator: (operator: Operator) => async (dispatch: Dispatch) => {
    try {
      if (operator.email?.length === 0) {
        operator.email = null;
      }
      const newOpId = await OperatorService.create(operator);
      dispatch({
        type: actions.CREATE_OPERATOR,
        payload: { ...operator, id: newOpId.id },
      });
    } catch (error) {
      Errors.handle(error);
    }
  },
  doUpdateOperator: (operator: Operator) => async (dispatch: Dispatch) => {
    try {
      const { id, ...updates } = operator;
      if (updates.email?.length === 0) {
        updates.email = null;
      }
      await OperatorService.update(id, updates);
      dispatch({
        type: actions.UPDATE_OPERATOR,
        payload: { id, ...updates },
      });
    } catch (error) {
      Errors.handle(error);
    }
  },
  doSetOperator: (operator: Operator | null) => async (dispatch: Dispatch) => {
    dispatch({
      type: actions.SET_OPERATOR,
      payload: operator,
    });
  },
  doSetStation: (station: Station | null) => async (dispatch: Dispatch) => {
    dispatch({
      type: actions.SET_STATION,
      payload: station,
    });
  },
  doSetStations: (stations: Station[]) => async (dispatch: Dispatch) => {
    dispatch({
      type: actions.SET_STATIONS,
      payload: sortStations(stations),
    });
  },
  doSetJob: (job: any | null) => async (dispatch: Dispatch) => {
    dispatch({
      type: actions.SET_JOB,
      payload: job,
    });
  },
  doSetDrawing: (drawing: Drawing | null) => async (dispatch: Dispatch) => {
    dispatch({
      type: actions.SET_DRAWING,
      payload: drawing,
    });
  },
  doFetchSamples: (jobId: string, partId: string) => async (dispatch: Dispatch) => {
    const response = await MetroService.fetchSamples(jobId, partId);
    dispatch({
      type: actions.FETCH_SAMPLES,
      payload: response.rows || null,
    });
  },
  doSetSamples: (samples: Sample[] | null) => async (dispatch: Dispatch) => {
    dispatch({
      type: actions.SET_SAMPLES,
      payload: samples,
    });
  },
  // TODO no any
  doUpdateSample: (sample: Sample) => async (dispatch: Dispatch, getState: () => AppState, clientPromise: Promise<ApolloClient<NormalizedCacheObject>>) => {
    return clientPromise.then(async (client) => {
      const sampleEdit = await client.mutate({
        mutation: Samples.mutate.edit,
        variables: {
          id: sample.id,
          data: {
            serial: sample.serial,
            sampleIndex: sample.sampleIndex,
            featureCoverage: sample.featureCoverage,
            status: sample.status,
            part: sample.partId || sample.part.id,
            job: sample.job.id,
            updatingOperatorId: sample.updatingOperatorId,
          },
        },
      });

      const state = getState();

      const updatedSamples = state.metro.samples?.map((sam: Sample) => (sam.id === sample.id ? sampleEdit.data!.sampleUpdate : sam));

      dispatch({ type: actions.UPDATE_SAMPLE, payload: updatedSamples?.sort((a: Sample, b: Sample) => (a.sampleIndex < b.sampleIndex ? -1 : 1)) });
    });
  },
  doSetInstalled: (installed: boolean) => async (dispatch: Dispatch) => {
    dispatch({
      type: actions.SET_INSTALLED,
      payload: installed,
    });
  },
  doCreateMeasurement: (measurement: MeasurementInput) => async (dispatch: Dispatch) => {
    dispatch({
      type: actions.CREATE_MEASUREMENT,
      payload: [measurement],
    });
  },
  doSelectCell: (cell: SelectedCell) => async (dispatch: Dispatch) => {
    dispatch({
      type: actions.DO_SELECT_CELL,
      payload: cell,
    });
  },
  doSetGage: (gage: string) => async (dispatch: Dispatch) => {
    dispatch({
      type: actions.SET_GAGE,
      payload: gage,
    });
  },
};

export const createMutation = async (queryObject: { data: MeasurementInput }, client: ApolloClient<NormalizedCacheObject>, refetchQueries: any) => {
  return client.mutate<MeasurementCreate, MeasurementCreateVars>({
    mutation: Measurements.mutate.create,
    variables: { data: { ...queryObject.data } },
    refetchQueries,
  });
};
export const updateMutation = async (queryObject: { id: string; data: MeasurementInput }, client: ApolloClient<NormalizedCacheObject>, refetchQueries: any) => {
  return client.mutate<MeasurementEdit, MeasurementEditVars>({
    mutation: Measurements.mutate.edit,
    variables: {
      id: queryObject.id,
      data: queryObject.data,
    },
    refetchQueries,
  });
};
export const deleteMutation = async (queryObject: { id: string }, client: ApolloClient<NormalizedCacheObject>, refetchQueries: any) => {
  return client.mutate<MeasurementDestroy, MeasurementDestroyVars>({
    mutation: Measurements.mutate.delete,
    variables: {
      ids: [queryObject.id],
    },
    refetchQueries,
  });
};

export type CreateMeasurementPayload = MeasurementInput[];
export type UpdateMeasurementPayload = MeasurementEditVars;

export const updateMeasurementThunk =
  (input: MeasurementInput, id: string, partFeatures: Characteristic[], del = false) =>
  async (dispatch: Dispatch, getState: () => AppState, clientPromise: Promise<ApolloClient<NormalizedCacheObject>>) => {
    return clientPromise.then(async (client) => {
      dispatch({ type: 'METRO_LOADED', payload: false });
      const partSamples = getState().metro.samples || [];
      const sample = partSamples.find((s: Sample) => s.id === input.sampleId);
      if (sample) {
        if (!del) {
          const updatedMeasurement = await updateMutation(
            {
              id,
              data: input,
            },
            client,
            [
              {
                query: Samples.query.find,
                variables: {
                  id: sample.id,
                },
              },
            ],
          );
          if (updatedMeasurement.data?.measurementUpdate) {
            const updatedSample = updateAfterMeasure(partSamples, updatedMeasurement.data?.measurementUpdate, partFeatures, getState().metro, dispatch);
            if (updatedSample) actions.doUpdateSample(updatedSample);
          }
        } else {
          const deletedMeasurements = await deleteMutation(
            {
              id,
            },
            client,
            [
              {
                query: Samples.query.find,
                variables: {
                  id: sample.id,
                },
              },
            ],
          );
          const m = sample.measurements.find((meas: Measurement) => meas.id === id);
          if (deletedMeasurements.data?.measurementDestroy && m) {
            const updatedSample = updateAfterMeasure(partSamples, m, partFeatures, getState().metro, dispatch, true);
            if (updatedSample) actions.doUpdateSample(updatedSample);
          }
        }
      }

      dispatch({ type: 'METRO_LOADED', payload: true });
    });
  };

export const createMeasurementThunk = (input: MeasurementInput, partFeatures: Characteristic[]) => async (dispatch: Dispatch, getState: () => AppState, clientPromise: Promise<ApolloClient<NormalizedCacheObject>>) => {
  return clientPromise.then(async (client) => {
    dispatch({ type: 'METRO_LOADED', payload: false });
    const partSamples = getState().metro.samples || [];
    const sample = partSamples.find((s: Sample) => s.id === input.sampleId);
    if (sample) {
      // batch update
      const createdMeasurement = await createMutation(
        {
          data: input,
        },
        client,
        [
          {
            query: Samples.query.find,
            variables: {
              id: sample.id,
            },
          },
        ],
      );

      if (createdMeasurement.data?.measurementCreate) {
        const updatedSample = updateAfterMeasure(partSamples, createdMeasurement.data?.measurementCreate, partFeatures, getState().metro, dispatch);
        if (updatedSample) actions.doUpdateSample(updatedSample);
      }
    }

    dispatch({ type: 'METRO_LOADED', payload: true });
  });
};

export const { doInitMetro } = actions;
export default actions;
