import { MetroState } from 'modules/state';
import { createSelector } from 'reselect';

const selectRaw = (state: { metro: MetroState }) => state.metro;
