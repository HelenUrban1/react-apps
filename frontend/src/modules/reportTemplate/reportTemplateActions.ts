import { ApolloClient } from '@apollo/client';
import config from 'config';
import { ReportData, Report, ReportInput, ReportPartial, ReportTemplate, ReportTemplateDirectionEnum, ReportTemplateInput } from 'types/reportTemplates';
import { Parts } from 'domain/part/partApi';
import { Part } from 'domain/part/partTypes';
import { ReportAPI, ReportCreateData, ReportCreateVars, ReportFindData, ReportFindVars, ReportUpdateData, ReportUpdateVars } from 'graphql/report';
import {
  ReportTemplateAPI,
  ReportTemplateCreateData,
  ReportTemplateCreateVars,
  ReportTemplateDestroyData,
  ReportTemplateDestroyVars,
  ReportTemplateFindData,
  ReportTemplateFindVars,
  ReportTemplateUpdateData,
  ReportTemplateUpdateVars,
} from 'graphql/reportTemplate';
import OrganizationAPI, { Organization } from 'graphql/organization';
import { i18n } from 'i18n';
import Analytics from 'modules/shared/analytics/analytics';
import FileUploader from 'modules/shared/fileUpload/fileUploader';
import log from 'modules/shared/logger';
import axios from 'modules/shared/network/axios';
import { getHistory } from 'modules/store';
import { Dispatch } from 'redux';
import { SheetData } from 'styleguide/Excel/types';
import { File } from 'types/file';
import { parseValidJSONString } from 'utils/textOperations';
import { defaultTemplates } from 'view/global/defaults';

const prefix = 'REPORT_TEMPLATE';

export const ReportTemplateActions = {
  CLEAR: `${prefix}_CLEAR`,
  SAVED: `${prefix}_SAVED`,
  LOADING: `${prefix}_LOADING`,
  ERROR: `${prefix}_ERROR`,
  WARNING: `${prefix}_WARNING`,
  SET_MODE: `${prefix}_SET_MODE`,
  SET_REPORT: `${prefix}_SET_REPORT`,
  SET_TEMPLATE: `${prefix}_SET_TEMPLATE`,
  SET_STEP: `${prefix}_SET_STEP`,
  SET_MAX: `${prefix}_SET_MAX`,
  SET_NAV: `${prefix}_SET_NAV`,
  SET_NEW_TEMPLATE: `${prefix}_SET_NEW_TEMPLATE`,
  SET_NEW_REPORT: `${prefix}_SET_NEW_REPORT`,
  SET_DATA: `${prefix}_SET_DATA`,
  SET_FILTERS: `${prefix}_SET_FILTERS`,
  SET_ID: `${prefix}_SET_ID`,
  SET_SOURCE: `${prefix}_SET_SOURCE`,
};

export default ReportTemplateActions;

const generateReport = async (url: string, part: ReportData, saveTo: string, jwt?: string, settings?: string, direction?: ReportTemplateDirectionEnum) => {
  return axios({
    method: 'post',
    url: `${config.backendUrl}/template`,
    data: {
      privateUrl: url,
      data: part,
      reportFile: saveTo,
      templateSettings: settings,
      direction: direction || 'Vertical',
    },
    headers: {
      authorization: jwt ? `Bearer ${jwt}` : '',
    },
  })
    .then((res) => {
      if (!res?.data || !res?.data?.data || !res?.data?.data[0]) {
        throw new Error(i18n('entities.template.errors.blankFile'));
      }
      return { data: res?.data, warning: res?.data?.unsupportedFeatures ? i18n('entities.template.warnings.unsupportedFeatures', res.data.unsupportedFeatures) : null };
    })
    .catch((err) => {
      log.error('ReportEditor.loadTemplateData error', err?.response?.data);
      return err;
    });
};

const loadExcel = async (url: string, jwt?: string) => {
  return axios({
    method: 'post',
    url: `${config.backendUrl}/excel`,
    data: { privateUrl: url },
    headers: {
      authorization: jwt ? `Bearer ${jwt}` : '',
    },
  })
    .then((res) => {
      if (!res?.data || !res.data?.data || !res.data.data[0]) {
        throw new Error(i18n('entities.template.errors.blankFile'));
      }
      return { data: res?.data, warning: res?.data?.unsupportedFeatures ? i18n('entities.template.warnings.unsupportedFeatures', res.data.unsupportedFeatures) : null };
    })
    .catch((err) => {
      log.error(err?.response?.data || err?.message);
      throw err;
    });
};

const copyExcel = async (url: string, file: string, jwt?: string) => {
  return axios({
    method: 'post',
    url: `${config.backendUrl}/template/copy`,
    data: {
      privateUrl: url,
      newFileName: file,
    },
    headers: {
      authorization: jwt ? `Bearer ${jwt}` : '',
    },
  })
    .then(async () => {
      return loadExcel(file, jwt);
    })
    .then(({ data, warning }) => {
      if (!data?.data || !data?.data[0]) {
        throw new Error(i18n('entities.template.errors.blankFile'));
      }
      return { data, warning };
    })
    .catch((err) => {
      log.error(err?.response?.data || err?.message);
      throw err;
    });
};

const convertExcelToSheetData = (templateData: SheetData[], settings: string) => {
  const sets = settings ? parseValidJSONString(settings) : null;
  if (templateData && sets && sets.footers) {
    const tempData: SheetData[] = [...templateData];
    Object.keys(sets.footers).forEach((sheet: string) => {
      Object.keys(sets.footers![sheet]).forEach((cell: string) => {
        const footer = sets!.footers![sheet][cell];
        if (tempData[parseInt(sheet, 10)] && tempData[parseInt(sheet, 10)].rows[footer.row] && tempData[parseInt(sheet, 10)].rows[footer.row][footer.col]) {
          tempData[parseInt(sheet, 10)].rows[footer.row][footer.col].footerRow = footer.footer;
          tempData[parseInt(sheet, 10)].rows[footer.row][footer.col].ind = {
            row: footer.row,
            col: footer.col,
          };
        }
      });
    });
    return tempData;
  }
  return templateData;
};

const getDefaultTemplate = (url: string) => {
  const templateInfo = defaultTemplates.find((temp) => temp.file[0] && temp.file[0].publicUrl && temp.file[0].publicUrl === url);
  if (!templateInfo) {
    log.warn('Default Template not found for provided url');
  }
  return templateInfo;
};

const updatePart = (part: Part | null, report?: Report, id?: string) => {
  let changed = false;
  if (!part) {
    return part;
  }
  const partial: ReportPartial = {
    //
    id: report?.id || '',
    status: report?.status || 'Active',
    partVersion: report?.partVersion || '',
    title: report?.title || '',
    templateUrl: report?.templateUrl || '',
    updatedAt: report?.updatedAt!,
    file: report?.file || [],
  };
  const newPartReports = [];
  part?.reports?.forEach((file) => {
    let newFile = { ...file };

    if (report && file.id === report.id) {
      changed = true;
      newFile = { ...file, ...partial };
    }
    if (!id || id !== file.id) {
      newPartReports.push(newFile);
    }
    if (id && id === file.id) {
      changed = true;
    }
  });
  if (!changed) {
    newPartReports.push(partial);
  }
  return {
    ...part,
    reports: newPartReports.sort((a, b) => {
      return new Date(b.updatedAt).getTime() - new Date(a.updatedAt).getTime();
    }),
  };
};

const updateOrganization = (organizations: Organization[], template?: ReportTemplate, id?: string) => {
  let changed = false;
  if (!organizations || organizations.length <= 0) {
    return organizations;
  }
  const newOrgList: Organization[] = [];
  organizations.forEach((org) => {
    const newOrg = { ...org };
    if (!template || org.id === template.provider.id) {
      const newOrgTemplates: ReportTemplate[] = [];
      org.templates?.forEach((temp) => {
        let newTemp = { ...temp };
        if (template && temp.id === template.id) {
          changed = true;
          newTemp = { ...temp, ...template };
        }
        if (!id || temp.id !== id) {
          newOrgTemplates.push(newTemp);
        }
      });
      if (!changed && template) {
        newOrgTemplates.push(template);
      }
      newOrg.templates = newOrgTemplates;
    }
    newOrgList.push(newOrg);
  });
  return newOrgList;
};

export const updateReportFile = async (file: File, changes: any, jwt?: string) => {
  return axios({
    method: 'post',
    url: `${config.backendUrl}/report/update`,
    data: {
      privateUrl: file.privateUrl,
      changes,
      report: file,
    },
    headers: {
      authorization: jwt ? `Bearer ${jwt}` : '',
    },
  })
    .then((response) => {
      return response.data.data;
    })
    .catch((error) => {
      log.error('TemplateDataEditor.updateReport error', error);
      throw error;
    });
};

export const doLoadReportTemplate =
  ({ url, settings }: { url: string; settings?: string }) =>
  async (dispatch: Dispatch, getState: any, clientPromise: Promise<ApolloClient<any>>) => {
    return clientPromise.then(async () => {
      // Track load times
      const startTime = performance.now();
      const { auth } = getState();
      const { jwt } = auth;
      dispatch({ type: ReportTemplateActions.LOADING, payload: true });

      if (!url) {
        log.warn('no template or report url', url);
        dispatch({ type: ReportTemplateActions.ERROR, payload: { message: 'Missing Data: URL' } });
        return false;
      }
      if (!settings) {
        log.warn('no template settings');
        dispatch({ type: ReportTemplateActions.ERROR, payload: { message: 'Missing Data: Template Settings' } });
        return false;
      }

      Analytics.track({
        event: Analytics.events.loadReportTemplateStarted,
        properties: {
          file: url,
        },
      });

      // Load File
      try {
        const { data: templateData, warning } = await loadExcel(url, jwt);
        if (warning) {
          dispatch({ type: ReportTemplateActions.WARNING, payload: warning });
        }
        const endTime = performance.now();
        const timeToLoad = endTime - startTime;
        Analytics.track({
          event: Analytics.events.loadReportTemplateFinished,
          properties: {
            file: url,
            'result.timeToLoad': timeToLoad,
          },
        });
        const sheetsData = convertExcelToSheetData(templateData?.data, settings);
        dispatch({ type: ReportTemplateActions.SET_DATA, payload: sheetsData });
        dispatch({ type: ReportTemplateActions.LOADING, payload: false });
        dispatch({ type: ReportTemplateActions.SAVED, payload: true });
        return templateData ? templateData.data : false;
      } catch (error) {
        dispatch({ type: ReportTemplateActions.ERROR, payload: error });
        Analytics.track({
          event: Analytics.events.reportTemplateError,
          properties: {
            file: url,
            action: 'Load File',
          },
        });
      }
      return false;
    });
  };

export const doGenerateReport =
  ({ url, report, part, id, settings, direction }: { url: string; report: ReportInput; part: ReportData; id?: string; settings?: string; direction?: ReportTemplateDirectionEnum }) =>
  async (dispatch: Dispatch, getState: any, clientPromise: Promise<ApolloClient<any>>) => {
    return clientPromise.then(async (client) => {
      // Track load times
      const startTime = performance.now();
      const { auth } = getState();
      const { jwt } = auth;
      dispatch({ type: ReportTemplateActions.LOADING, payload: true });

      if (!url) {
        log.warn('no template or report url');
        dispatch({ type: ReportTemplateActions.ERROR, payload: { message: 'Missing Data: URL' } });
        return false;
      }
      if (!part) {
        log.warn('no part data to generate from');
        dispatch({ type: ReportTemplateActions.ERROR, payload: { message: 'Missing Data: Part' } });
        return false;
      }
      if (!settings) {
        log.warn('no template settings');
        dispatch({ type: ReportTemplateActions.ERROR, payload: { message: 'Missing Data: Template Settings' } });
        return false;
      }

      Analytics.track({
        event: Analytics.events.loadReportTemplateStarted,
        part,
        properties: {
          file: url,
        },
      });

      // Generate Report
      try {
        let data: Report | undefined;
        if (id) {
          const mutateReturn = await client.mutate<ReportUpdateData, ReportUpdateVars>({
            mutation: ReportAPI.mutate.edit,
            variables: { id, data: report },
            refetchQueries: [
              {
                query: Parts.query.find,
                variables: { id: report.part },
              },
            ],
          });
          data = mutateReturn.data?.reportUpdate;
        } else {
          const mutateReturn = await client.mutate<ReportCreateData, ReportCreateVars>({
            mutation: ReportAPI.mutate.create,
            variables: { data: report },
            refetchQueries: [
              {
                query: Parts.query.find,
                variables: { id: report.part },
              },
            ],
          });
          data = mutateReturn.data?.reportCreate;
        }

        const { data: templateData, warning } = await generateReport(url, part, report.file[0]?.privateUrl || '', jwt, settings, direction);
        if (warning) {
          dispatch({ type: ReportTemplateActions.WARNING, payload: warning });
        }
        const endTime = performance.now();
        const timeToLoad = endTime - startTime;
        Analytics.track({
          event: Analytics.events.loadReportTemplateFinished,
          part,
          report: data,
          properties: {
            file: url,
            'result.timeToLoad': timeToLoad,
          },
        });
        // console.log(`loadTemplateData.done (${timeToLoad} ms)`);
        //
        if (data) {
          const sheetsData = convertExcelToSheetData(templateData?.data, settings);
          dispatch({ type: ReportTemplateActions.SET_DATA, payload: sheetsData });
          dispatch({ type: ReportTemplateActions.SET_ID, payload: data?.id });
          dispatch({ type: ReportTemplateActions.SET_REPORT, payload: data });
          const statePart = getState().part;
          const partUpdate = updatePart(statePart.part, data);
          dispatch({ type: 'PART_SET_PART', payload: partUpdate });
          const history = getHistory();
          history.push(`/parts/${statePart.part.id}/reports/${data.id}`, { step: Parts.step.order.length, max: Parts.step.order.length });
        }
        dispatch({ type: ReportTemplateActions.LOADING, payload: false });
        dispatch({ type: ReportTemplateActions.SAVED, payload: true });
        return templateData ? templateData.data : false;
      } catch (error) {
        dispatch({ type: ReportTemplateActions.ERROR, payload: error });
        Analytics.track({
          event: Analytics.events.reportError,
          report,
          part,
          properties: {
            file: url,
            action: 'Generate Report',
          },
        });
      }
      return false;
    });
  };

export const doGetReport = (reportId: string) => async (dispatch: Dispatch, getState: any, clientPromise: Promise<ApolloClient<any>>) => {
  return clientPromise.then(async (client) => {
    dispatch({ type: ReportTemplateActions.LOADING, payload: true });
    try {
      const { data, error } = await client.query<ReportFindData, ReportFindVars>({
        fetchPolicy: 'no-cache',
        query: ReportAPI.query.find,
        variables: {
          id: reportId,
        },
      });
      if (error) {
        dispatch({ type: ReportTemplateActions.ERROR, payload: error });
        return;
      }
      if (data) {
        try {
          dispatch({ type: ReportTemplateActions.SET_REPORT, payload: data.reportFind });
          let temp = data.reportFind.template;
          if (!temp) {
            temp = getDefaultTemplate(data.reportFind.templateUrl);
          }
          dispatch({ type: ReportTemplateActions.SET_TEMPLATE, payload: temp });
          dispatch({ type: ReportTemplateActions.SET_ID, payload: reportId });
          dispatch({ type: ReportTemplateActions.SET_MODE, payload: 'report' });
          if (data?.reportFind?.file[0]?.privateUrl) {
            const { auth } = getState();
            const { jwt } = auth;
            const { data: templateData, warning } = await loadExcel(data.reportFind.file[0].privateUrl, jwt);
            if (warning) {
              dispatch({ type: ReportTemplateActions.WARNING, payload: warning });
            }
            const sheetsData = convertExcelToSheetData(templateData?.data, temp?.settings || '{}');
            dispatch({ type: ReportTemplateActions.SET_DATA, payload: sheetsData });
            const history = getHistory();
            if (history.location.pathname !== `/parts/${data.reportFind?.part?.id}/reports/${reportId}`) history.push(`/parts/${data.reportFind?.part?.id}/reports/${reportId}`);
            dispatch({ type: ReportTemplateActions.LOADING, payload: false });
          } else {
            dispatch({ type: ReportTemplateActions.LOADING, payload: false });
          }
          dispatch({ type: ReportTemplateActions.SAVED, payload: true });
        } catch (err) {
          dispatch({ type: ReportTemplateActions.ERROR, payload: err });
        }
      }
    } catch (error) {
      dispatch({ type: ReportTemplateActions.ERROR, payload: error });
      Analytics.track({
        event: Analytics.events.reportError,
        properties: {
          id: reportId,
          action: 'Load Report',
        },
      });
    }
  });
};

export const doSaveReport =
  ({ id, report, push, changes }: { id: string; report: ReportInput; push?: string; changes?: any }) =>
  async (dispatch: Dispatch, getState: any, clientPromise: Promise<ApolloClient<any>>) => {
    return clientPromise.then(async (client) => {
      dispatch({ type: ReportTemplateActions.SAVED, payload: false });
      try {
        const { part } = getState();
        const { data, errors } = await client.mutate<ReportUpdateData, ReportUpdateVars>({
          mutation: ReportAPI.mutate.edit,
          variables: { id, data: report },
          refetchQueries: [
            {
              query: Parts.query.find,
              variables: { id: part.part.id },
            },
            {
              query: ReportAPI.query.find,
              variables: { id },
            },
          ],
        });
        if (errors) {
          log.error(errors);
          dispatch({ type: ReportTemplateActions.SAVED, payload: true });
          dispatch({ type: ReportTemplateActions.ERROR, payload: errors[0] });
          return;
        }
        if (data?.reportUpdate) {
          if (changes) {
            // Update File
            const { auth } = getState();
            const { jwt } = auth;
            const tableData = await updateReportFile(data.reportUpdate.file[0], changes, jwt);
            if (tableData) {
              dispatch({ type: ReportTemplateActions.SET_DATA, payload: tableData });
            }
          }
          // Update Redux
          dispatch({ type: ReportTemplateActions.SET_REPORT, payload: data.reportUpdate });
          const partUpdate = updatePart(part.part, data.reportUpdate);
          dispatch({ type: 'PART_SET_PART', payload: partUpdate });
        }
        if (push) {
          dispatch({ type: ReportTemplateActions.LOADING, payload: true });
          // dispatch({ type: ReportTemplateActions.SET_NAV, payload: { max: -1, step: -1 } });
          const history = getHistory();
          history.push(push);
          dispatch({ type: ReportTemplateActions.CLEAR });
          return;
        }
      } catch (error) {
        log.error(error);
        dispatch({ type: ReportTemplateActions.ERROR, payload: error });
        Analytics.track({
          event: Analytics.events.reportError,
          report,
          properties: {
            changes,
            action: 'Update Report',
          },
        });
      }
      dispatch({ type: ReportTemplateActions.SAVED, payload: true });
    });
  };

export const doDeleteReport =
  ({ id, report }: { id: string; report: ReportInput }) =>
  async (dispatch: Dispatch, getState: any, clientPromise: Promise<ApolloClient<any>>) => {
    return clientPromise.then(async (client) => {
      try {
        const { part } = getState();

        Analytics.track({
          event: Analytics.events.userClickedDeleteReport,
          part,
          report: { ...report, id },
        });

        const { data, errors } = await client.mutate<ReportUpdateData, ReportUpdateVars>({
          mutation: ReportAPI.mutate.edit,
          variables: { id, data: { ...report, status: 'Deleted' } },
          refetchQueries: [
            {
              query: Parts.query.find,
              variables: { id: part.part.id },
            },
            {
              query: ReportAPI.query.find,
              variables: { id },
            },
          ],
        });
        if (errors) {
          log.error(errors);
          dispatch({ type: ReportTemplateActions.ERROR, payload: errors[0] });
          return;
        }
        if (data?.reportUpdate) {
          const partUpdate = updatePart(part.part, undefined, id);
          dispatch({ type: 'PART_SET_PART', payload: partUpdate });
        }
        dispatch({ type: ReportTemplateActions.CLEAR });
        dispatch({ type: ReportTemplateActions.LOADING, payload: false });
      } catch (error) {
        log.error(error);
        dispatch({ type: ReportTemplateActions.ERROR, payload: error });
        Analytics.track({
          event: Analytics.events.reportError,
          report,
          properties: {
            action: 'Delete Report',
          },
        });
      }
    });
  };

export const doGenerateTemplate =
  ({ url, template, file, copy = false }: { url: string; template: ReportTemplateInput; file?: any; copy?: boolean }) =>
  async (dispatch: Dispatch, getState: any, clientPromise: Promise<ApolloClient<any>>) => {
    return clientPromise.then(async (client) => {
      // Track load times
      const startTime = performance.now();
      const { auth } = getState();
      const { jwt } = auth;
      dispatch({ type: ReportTemplateActions.LOADING, payload: true });

      if (!url) {
        log.warn('no template or report url');
        dispatch({ type: ReportTemplateActions.ERROR, payload: { message: 'Missing Data: URL' } });
        return false;
      }

      Analytics.track({
        event: Analytics.events.createNewReportTemplate,
        properties: {
          file: url,
        },
      });

      let id = null;

      // Generate Template
      try {
        const { data } = await client.mutate<ReportTemplateCreateData, ReportTemplateCreateVars>({
          mutation: ReportTemplateAPI.mutate.create,
          variables: { data: template },
        });
        id = data?.reportTemplateCreate.id;

        if (data?.reportTemplateCreate && data.reportTemplateCreate.file[0].privateUrl) {
          if (!copy) {
            // Upload File
            const additionalData = [
              {
                key: 'templateId',
                value: data.reportTemplateCreate.id,
              },
              {
                key: 'uniqueFileName',
                value: template.file[0].privateUrl,
              },
            ];
            await FileUploader.uploadToServer(file, 'reportTemplate/file', template.file[0].privateUrl, additionalData);
          }
          // Load File
          const { data: templateData, warning } = copy ? await copyExcel(url, data.reportTemplateCreate.file[0].privateUrl, jwt) : await loadExcel(url, jwt);
          if (warning) {
            dispatch({ type: ReportTemplateActions.WARNING, payload: warning });
          }
          const endTime = performance.now();
          const timeToLoad = endTime - startTime;
          Analytics.track({
            event: Analytics.events.loadReportTemplateFinished,
            properties: {
              'template.id': data?.reportTemplateCreate.id,
              file: url,
              'result.timeToLoad': timeToLoad,
            },
          });
          // console.log(`loadTemplateData.done (${timeToLoad} ms)`);
          //
          dispatch({ type: ReportTemplateActions.SET_DATA, payload: templateData?.data });
          dispatch({ type: ReportTemplateActions.SET_ID, payload: data.reportTemplateCreate.id });
          dispatch({ type: ReportTemplateActions.SET_TEMPLATE, payload: data.reportTemplateCreate });
          dispatch({ type: ReportTemplateActions.SET_MODE, payload: 'template' });
          const { session } = getState();
          const { organizations } = session;
          const orgUpdates = updateOrganization(organizations?.all, data.reportTemplateCreate);
          dispatch({ type: 'SESSION_UPDATE_ORGANIZATIONS', payload: { all: orgUpdates } });
          dispatch({ type: ReportTemplateActions.LOADING, payload: false });
          const history = getHistory();
          history.push(`/templates/${data.reportTemplateCreate.id}`, { step: 1, max: 1 });
          dispatch({ type: ReportTemplateActions.SAVED, payload: true });
          return templateData ? templateData.data : false;
        }
        dispatch({ type: ReportTemplateActions.SAVED, payload: true });
        return false;
      } catch (error) {
        dispatch({ type: ReportTemplateActions.SET_ID, payload: id });
        dispatch({ type: ReportTemplateActions.ERROR, payload: error });
        Analytics.track({
          event: Analytics.events.reportTemplateError,
          template,
          properties: {
            file,
            url,
            action: 'Generate Template',
          },
        });
        return id;
      }
    });
  };

export const doGetTemplate = (templateId: string) => async (dispatch: Dispatch, getState: any, clientPromise: Promise<ApolloClient<any>>) => {
  return clientPromise.then(async (client) => {
    dispatch({ type: ReportTemplateActions.LOADING, payload: true });
    try {
      const { data, error } = await client.query<ReportTemplateFindData, ReportTemplateFindVars>({
        fetchPolicy: 'no-cache',
        query: ReportTemplateAPI.query.find,
        variables: {
          id: templateId,
        },
      });
      if (error) {
        dispatch({ type: ReportTemplateActions.ERROR, payload: error });
        return;
      }
      if (data) {
        try {
          dispatch({ type: ReportTemplateActions.SET_TEMPLATE, payload: data.reportTemplateFind });
          dispatch({ type: ReportTemplateActions.SET_ID, payload: templateId });

          if (data?.reportTemplateFind?.file[0]?.privateUrl) {
            const { auth } = getState();
            const { jwt } = auth;
            const { data: templateData, warning } = await loadExcel(data.reportTemplateFind.file[0].publicUrl, jwt);
            if (warning) {
              dispatch({ type: ReportTemplateActions.WARNING, payload: warning });
            }
            const sheetsData = convertExcelToSheetData(templateData?.data, data.reportTemplateFind?.settings || '{}');
            dispatch({ type: ReportTemplateActions.SET_DATA, payload: sheetsData });
            dispatch({ type: ReportTemplateActions.LOADING, payload: false });
          } else {
            dispatch({ type: ReportTemplateActions.LOADING, payload: false });
          }
          dispatch({ type: ReportTemplateActions.SAVED, payload: true });
        } catch (err) {
          dispatch({ type: ReportTemplateActions.ERROR, payload: err });
        }
      }
    } catch (error) {
      dispatch({ type: ReportTemplateActions.ERROR, payload: error });
      Analytics.track({
        event: Analytics.events.reportTemplateError,
        properties: {
          id: templateId,
          action: 'Load Template',
        },
      });
    }
  });
};

export const doSaveTemplate =
  ({ id, template, push, changes }: { id: string; template: ReportTemplateInput; push?: string; changes?: any }) =>
  async (dispatch: Dispatch, getState: any, clientPromise: Promise<ApolloClient<any>>) => {
    return clientPromise.then(async (client) => {
      dispatch({ type: ReportTemplateActions.SAVED, payload: false });
      try {
        const { data, errors } = await client.mutate<ReportTemplateUpdateData, ReportTemplateUpdateVars>({
          mutation: ReportTemplateAPI.mutate.edit,
          variables: { id, data: template },
          refetchQueries: [
            // Because session context keeps refetching it
            {
              query: OrganizationAPI.query.list,
              variables: { filter: { status: 'Active' } },
            },
            {
              query: ReportTemplateAPI.query.find,
              variables: { id },
            },
          ],
          update(cache, { data: saveTemplate }) {
            if (!saveTemplate?.reportTemplateUpdate) {
              return;
            }
            cache.modify({
              id: saveTemplate.reportTemplateUpdate.id,
              fields: {
                reportTemplateUpdate(existing) {
                  return { ...existing, ...saveTemplate.reportTemplateUpdate };
                },
              },
            });
          },
        });
        if (errors) {
          log.error(errors);
          dispatch({ type: ReportTemplateActions.SAVED, payload: true });
          dispatch({ type: ReportTemplateActions.ERROR, payload: errors[0] });
          return;
        }
        if (data?.reportTemplateUpdate) {
          if (changes) {
            // Update File
            const { auth } = getState();
            const { jwt } = auth;
            const tableData = await updateReportFile(data.reportTemplateUpdate.file[0], changes, jwt);
            if (tableData) {
              const sheetsData = convertExcelToSheetData(tableData, template.settings || '{}');
              dispatch({ type: ReportTemplateActions.SET_DATA, payload: sheetsData });
            }
          }
          dispatch({ type: ReportTemplateActions.SET_TEMPLATE, payload: data.reportTemplateUpdate });
          const { session } = getState();
          const { organizations } = session;
          const orgUpdates = updateOrganization(organizations?.all, data.reportTemplateUpdate);
          dispatch({ type: 'SESSION_UPDATE_ORGANIZATIONS', payload: { all: orgUpdates } });
          dispatch({ type: ReportTemplateActions.SAVED, payload: true });
        }
        if (push) {
          dispatch({ type: ReportTemplateActions.LOADING, payload: true });
          // dispatch({ type: ReportTemplateActions.SET_NAV, payload: { max: -1, step: -1 } });
          const history = getHistory();
          history.push(push);
          dispatch({ type: ReportTemplateActions.CLEAR });
          return;
        }
      } catch (error) {
        log.error(error);
        dispatch({ type: ReportTemplateActions.ERROR, payload: error });
        Analytics.track({
          event: Analytics.events.reportTemplateError,
          template,
          properties: {
            changes,
            action: 'Update Template',
          },
        });
      }
      dispatch({ type: ReportTemplateActions.SAVED, payload: true });
    });
  };

export const doDeleteTemplate =
  ({ id }: { id: string[] }) =>
  async (dispatch: Dispatch, getState: any, clientPromise: Promise<ApolloClient<any>>) => {
    return clientPromise.then(async (client) => {
      try {
        const { errors } = await client.mutate<ReportTemplateDestroyData, ReportTemplateDestroyVars>({
          mutation: ReportTemplateAPI.mutate.delete,
          variables: { ids: id },
        });
        if (errors) {
          log.error(errors);
          dispatch({ type: ReportTemplateActions.ERROR, payload: errors[0] });
          return;
        }
        const { session } = getState();
        const { organizations } = session;
        id.forEach((i) => {
          const orgUpdates = updateOrganization(organizations?.all, undefined, i);
          dispatch({ type: 'SESSION_UPDATE_ORGANIZATIONS', payload: { all: orgUpdates } });
        });
        dispatch({ type: ReportTemplateActions.CLEAR });
        dispatch({ type: ReportTemplateActions.LOADING, payload: false });
      } catch (err) {
        log.error(err);
        dispatch({ type: ReportTemplateActions.ERROR, payload: err });
        Analytics.track({
          event: Analytics.events.reportTemplateError,
          properties: {
            id,
            action: 'Delete Template',
          },
        });
      }
    });
  };
