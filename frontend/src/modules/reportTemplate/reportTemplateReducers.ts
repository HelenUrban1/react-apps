import { Parts } from 'domain/part/partApi';
import { ReportTemplateAction, ReportTemplateState } from 'modules/state';
import { parseValidJSONString } from 'utils/textOperations';

const initialData: ReportTemplateState = {
  mode: 'template',
  report: null,
  template: null,
  settings: {
    saved: false,
  },
  step: -1,
  max: -1,
  source: null,
  data: null,
  filters: { 1: { category: undefined, value: undefined } },
  id: null,
  saved: true,
  loading: false,
  warning: null,
  error: null,
};

export default (state = initialData, { type, payload }: ReportTemplateAction): ReportTemplateState => {
  let settings = { ...state.settings };
  let filters = { ...state.filters };
  switch (type) {
    case 'REPORT_TEMPLATE_CLEAR':
      return initialData;
    case 'REPORT_TEMPLATE_SET_MODE':
      return { ...state, mode: payload };
    case 'REPORT_TEMPLATE_SET_REPORT':
      if (payload?.filters) {
        filters = parseValidJSONString(payload.filters);
      }
      if (Object.keys(filters).length === 0) {
        filters['1'] = { category: undefined, value: undefined };
      }
      return { ...state, report: payload, filters };
    case 'REPORT_TEMPLATE_SET_TEMPLATE':
      if (payload?.settings) {
        settings = parseValidJSONString(payload.settings);
      }
      return { ...state, template: payload, settings };
    case 'REPORT_TEMPLATE_SET_STEP':
      return { ...state, step: payload };
    case 'REPORT_TEMPLATE_SET_MAX':
      return { ...state, max: payload };
    case 'REPORT_TEMPLATE_SET_NAV':
      return { ...state, step: payload.step, max: payload.max };
    case 'REPORT_TEMPLATE_SET_NEW_TEMPLATE':
      return { ...state, step: 0, max: 1, id: null, mode: 'template' };
    case 'REPORT_TEMPLATE_SET_NEW_REPORT':
      return { ...state, step: Parts.step.order.length - 1, max: Parts.step.order.length, id: null, mode: 'report' };
    case 'REPORT_TEMPLATE_SET_DATA':
      return { ...state, data: payload };
    case 'REPORT_TEMPLATE_SET_FILTERS':
      return { ...state, filters: payload };
    case 'REPORT_TEMPLATE_SET_ID':
      return { ...state, id: payload };
    case 'REPORT_TEMPLATE_SET_SOURCE':
      return { ...state, source: payload };
    case 'REPORT_TEMPLATE_LOADING':
      return { ...state, loading: payload };
    case 'REPORT_TEMPLATE_SAVED':
      return { ...state, saved: payload };
    case 'REPORT_TEMPLATE_WARNING':
      return { ...state, loading: false, warning: payload };
    case 'REPORT_TEMPLATE_ERROR':
      return { ...state, loading: false, error: payload };
    default:
      return state;
  }
};
