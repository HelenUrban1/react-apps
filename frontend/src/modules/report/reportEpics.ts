import { Epic, ActionsObservable } from 'redux-observable';
import { ReportAPI, ReportFindData } from 'graphql/report';
import { from, ObservableInput } from 'rxjs';
import { filter, mergeMap, switchMap } from 'rxjs/operators';
import { isOfType } from 'typesafe-actions';
import { ApolloClient, ApolloQueryResult } from '@apollo/client';
import ReportActionTypes from './reportActions';

import { ReportAction, AppState } from '../state';

const loadReportEpic: Epic<ReportAction, ReportAction, AppState> = (action$: ActionsObservable<ReportAction>, _, { clientPromise }) =>
  action$.pipe(
    filter(isOfType(ReportActionTypes.FETCH_REPORT)),
    switchMap((action: ReportAction) =>
      from<PromiseLike<ApolloClient<any>>>(clientPromise).pipe(
        switchMap<ApolloClient<any>, ObservableInput<ApolloQueryResult<any>>>((client: ApolloClient<any>) => {
          return client.query({ query: ReportAPI.query.find, variables: { id: action.payload } });
        }),
        mergeMap((result: ApolloQueryResult<ReportFindData>) => {
          // TODO utilize loading and error for better experience
          const { data } = result;

          if (data) {
            const emission: ReportAction[] = [
              { type: 'REPORT_SET_REPORT', payload: data.reportFind },
              { type: 'REPORT_SET_PART', payload: data.reportFind.part },
            ];
            if (data.reportFind.template) {
              emission.push({ type: 'REPORT_SET_TEMPLATE', payload: data.reportFind.template });
              emission.push({ type: 'REPORT_SET_ORG', payload: data.reportFind.template.provider?.id || undefined });
            }
            if (data.reportFind.file && data.reportFind.file.length > 0) {
              emission.push({ type: 'REPORT_SET_FILE', payload: data.reportFind.file[0].publicUrl });
            }
            return emission;
          }

          return [];
        }),
      ),
    ),
  );

export default loadReportEpic;
