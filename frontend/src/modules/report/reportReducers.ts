import { ReportAction, ReportState } from '../state.d';

export const initialData = {
  data: null,
  step: 0,
  report: null,
  reportFile: null,
  reportTemplate: null,
  source: null,
  ID: null,
  org: null,
  partData: null,
  swap: false,
  deleteCurrentReport: false,
  loading: false,
  error: null,
};

export default (state = initialData, { type, payload }: ReportAction): ReportState => {
  switch (type) {
    case 'REPORT_CLEAR_REPORT':
      return {
        ...initialData,
      };
    // TODO huge update cases like this are an antipattern and should be decomposed via middleware or atomic calls
    case 'REPORT_UPDATE_REPORT_CONTEXT':
      return {
        ...state,
        ...Object.entries(payload || {}).reduce((a, [k, v]) => (v === null ? a : ((a[k] = v), a)), {} as { [index: string]: any }),
      };
    case 'REPORT_SET_REPORT':
      return {
        ...state,
        step: state.step || 0,
        report: payload,
      };
    case 'REPORT_LOAD':
      return {
        ...state,
        report: payload.report,
        data: payload.data,
        reportFile: payload.file,
        loading: false,
      };
    case 'REPORT_SET_PART':
      return {
        ...state,
        partData: payload,
      };
    case 'REPORT_SET_STEP':
      return {
        ...state,
        step: payload,
      };
    case 'REPORT_SET_TEMPLATE':
      return {
        ...state,
        reportTemplate: payload,
      };
    case 'REPORT_SET_ORG':
      return {
        ...state,
        org: payload,
      };
    case 'REPORT_SET_SWAP':
      return {
        ...state,
        swap: payload,
      };
    case 'REPORT_SET_DELETE':
      return {
        ...state,
        deleteCurrentReport: payload,
      };
    case 'REPORT_LOADING':
      return {
        ...state,
        loading: payload.loading,
      };
    case 'REPORT_ERROR':
      return {
        ...state,
        loading: false,
        error: payload.error,
      };
    default:
      return state;
  }
};
