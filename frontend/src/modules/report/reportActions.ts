import { Dispatch } from 'redux';
import FileUploader from 'modules/shared/fileUpload/fileUploader';
import { ApolloClient } from '@apollo/client';
import { ReportTemplateAPI, ReportTemplateCreateData, ReportTemplateCreateVars } from 'graphql/reportTemplate';
import { ReportTemplate, ReportTemplateInput, Report, ReportData } from 'types/reportTemplates';
import OrganizationAPI from 'graphql/organization';
import { UploadFile } from 'antd/lib/upload/interface';
import log from 'modules/shared/logger';
import axios from 'modules/shared/network/axios';
import config from 'config';
import { i18n } from 'i18n';

const prefix = 'REPORT';

const actions = {
  CLEAR_REPORT: `${prefix}_CLEAR_REPORT`,
  FETCH_REPORT: `${prefix}_FETCH_REPORT`,
  SET_FILE: `${prefix}_SET_FILE`,
  SET_ORG: `${prefix}_SET_ORG`,
  SET_PART: `${prefix}_SET_PART`,
  SET_REPORT: `${prefix}_SET_REPORT`,
  SET_TABLE_DATA: `${prefix}_SET_TABLE_DATA`,
  SET_TEMPLATE: `${prefix}_SET_TEMPLATE`,
  SET_SWAP: `${prefix}_SET_SWAP`,
  SET_DELETE: `${prefix}_SET_DELETE`,
  UPDATE_REPORT_CONTEXT: `${prefix}_UPDATE_REPORT_CONTEXT`,
  CREATE_REPORT: `${prefix}_CREATE_REPORT`,
  REPORT_LOADING: `${prefix}_LOADING`,
  REPORT_ERROR: `${prefix}_ERROR`,
  REPORT_LOAD: `${prefix}_LOAD`,
};

export const createTemplateThunk = (newTemplate: ReportTemplateInput, newFileName: string, files: UploadFile[], filters?: string[], skipUpload?: boolean) => async (dispatch: Dispatch, getState: any, clientPromise: Promise<ApolloClient<any>>) => {
  const state = getState();

  return clientPromise.then(async (client) => {
    const templateCreateResult = await client.mutate<ReportTemplateCreateData, ReportTemplateCreateVars>({
      mutation: ReportTemplateAPI.mutate.create,
      variables: { data: newTemplate },
      refetchQueries: [
        {
          query: OrganizationAPI.query.list,
          variables: { filter: { status: 'Active' } },
        },
      ],
    });

    // Update Template List
    const { organizations } = state.session;
    dispatch({ type: 'SESSION_UPDATE_ORGANIZATIONS', payload: { self: { ...organizations.self, templates: [...organizations.self.templates, templateCreateResult.data?.reportTemplateCreate] } } });

    dispatch({ type: 'TEMPLATE_CREATE_TEMPLATE', payload: true });
    // Added as tags when uploading to S3
    const additionalData = [
      {
        key: 'templateId',
        value: templateCreateResult.data?.reportTemplateCreate.id,
      },
      {
        key: 'uniqueFileName',
        value: newFileName,
      },
    ];

    // TODO: Error handling
    try {
      // cloned templates have file upload handled in backend/src/api : '/api/template/copy'
      if (!skipUpload) await FileUploader.uploadToServer(files[0].originFileObj, 'reportTemplate/file', newFileName, additionalData);
    } catch (err) {
      log.error('reportActions.createTemplateThunk', err);
    }

    dispatch({ type: actions.SET_TEMPLATE, payload: templateCreateResult.data?.reportTemplateCreate }); // REPORT_SET_TEMPLATE
    dispatch({ type: 'TEMPLATE_SET_TEMPLATE', payload: templateCreateResult.data?.reportTemplateCreate });
  });
};

export const loadReport =
  ({ report, template, part, generate = false }: { report?: Report; template?: ReportTemplate; part?: ReportData; generate?: boolean }) =>
  async (dispatch: Dispatch, getState: any, clientPromise: Promise<ApolloClient<any>>) => {
    const state = getState();
    const { jwt } = state.auth;

    return clientPromise.then(async () => {
      if (!report && !template) {
        log.warn('No file connected to this report');
        log.debug(report);
        return;
      }
      let url = report ? report.file[0].publicUrl : '';
      let payload: any = { privateUrl: url };
      if (generate) {
        if (!part || !template) {
          log.warn('No part data to parse');
          return;
        }
        url = template.file[0].publicUrl;
        payload = {
          //
          privateUrl: url,
          data: part,
          reportFile: report ? report.file[0].privateUrl : undefined,
          // file: template.file[0].privateUrl,
          templateSettings: template.settings,
          direction: template.direction,
        };
      }
      if (report?.id.includes('default')) {
        log.warn('default report detected');
        return;
      }

      dispatch({ type: 'REPORT_LOADING', payload: { loading: true } });
      const apiEndpoint = generate ? `${config.backendUrl}/template` : `${config.backendUrl}/excel`;
      const reportData = await axios({
        method: 'post',
        url: apiEndpoint,
        data: payload,
        headers: {
          authorization: jwt ? `Bearer ${jwt}` : '',
        },
      }).catch((err) => {
        if (jwt && err.response.status === 401) {
          log.warn('Authorization Token Expired');
          return { data: 'auth' };
        }
        log.error(err.response.data);
        dispatch({ type: 'REPORT_ERROR', payload: { error: err.response.data.message } });
        return null;
      });

      if (reportData && reportData.data && reportData?.data !== 'auth') {
        const tempData = reportData.data.data ? reportData.data.data : reportData.data;
        const file = reportData.data.report ? reportData.data.report : undefined;

        dispatch({ type: 'REPORT_LOAD', payload: { data: tempData, report, file } });

        if (template) {
          dispatch({ type: 'REPORT_SET_TEMPLATE', payload: template });
        }

        dispatch({ type: 'REPORT_LOADING', payload: { loading: false } });
      } else if (reportData?.data !== 'auth') {
        dispatch({ type: 'REPORT_ERROR', payload: { error: i18n('entities.report.errors.blankFile') } });
      }
    });
  };

export default actions;
