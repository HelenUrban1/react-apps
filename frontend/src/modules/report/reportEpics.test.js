import { StateObservable } from 'redux-observable';
import { Subject } from 'rxjs/Subject';

import { TestScheduler } from 'rxjs/testing';
import ReportActions from './reportActions';
import { initialState } from './reportReducers';
import loadReportEpic from './reportEpics';

const basicDataReport = {
  part: {},
};

const templateDataReport = {
  part: {},
  template: {
    provider: { id: 'blah' },
  },
};

const dataOutputActions = [
  { type: 'REPORT_SET_REPORT', payload: basicDataReport },
  { type: 'REPORT_SET_PART', payload: basicDataReport.part },
];

const templateOutputActions = [
  { type: 'REPORT_SET_REPORT', payload: templateDataReport },
  { type: 'REPORT_SET_PART', payload: templateDataReport.part },
  { type: 'REPORT_SET_TEMPLATE', payload: templateDataReport.template },
  { type: 'REPORT_SET_ORG', payload: templateDataReport.template.provider.id },
];

const deepEqual = (actual, expected) => {
  //bit of work here because 'expected' will be a notificatrion with the basic array (above) as a value
  //and 'actual' will be the notification for each action thanks to how mergemap works.

  actual.forEach(({ notification }, i) => expect(notification.value).toEqual(expected[0].notification.value[i]));
};

describe('Report Epics', () => {
  describe('loadReportEpic', () => {
    it('loads a report and sets data if data is present', () => {
      const testScheduler = new TestScheduler(deepEqual);

      testScheduler.run(({ hot, cold, expectObservable }) => {
        const client = {
          query: () => cold(marbles.r, values),
        };

        // marble chart: -i--c---r-----o
        // explanation: - is a 'tick' of the scheduler; 'i', 'c', 'r' and 'o' are values emitted into the stream by the scheduler
        const marbles = {
          i: '-i', //input action, i.e. REPORT_FETCH_REPORT,
          c: '--c', //get the apollo client
          r: '---r', //get the query result from the
          o: '----o', //ouput action(s)
        };
        const values = {
          i: { type: ReportActions.FETCH_REPORT, payload: 'foo' },
          c: client,
          r: { data: { reportFind: basicDataReport } },
          o: dataOutputActions,
        };
        const action$ = hot(marbles.i, values);
        const state$ = new StateObservable(new Subject(), initialState);
        const dependencies = {
          clientPromise: cold(marbles.c, values),
        };

        const output$ = loadReportEpic(action$, state$, dependencies);
        expectObservable(output$).toBe(marbles.o, values);
      });
    });

    it('loads a report and sets template+org if template is set', () => {
      const testScheduler = new TestScheduler(deepEqual);

      testScheduler.run(({ hot, cold, expectObservable }) => {
        const client = {
          query: () => cold(marbles.r, values),
        };

        // marble chart: -i--c---r-----o
        // explanation: - is a 'tick' of the scheduler; 'i', 'c', 'r' and 'o' are values emitted into the stream by the scheduler
        const marbles = {
          i: '-i', //input action, i.e. REPORT_FETCH_REPORT,
          c: '--c', //get the apollo client
          r: '---r', //get the query result from the
          o: '----o', //ouput action(s)
        };
        const values = {
          i: { type: ReportActions.FETCH_REPORT, payload: 'foo' },
          c: client,
          r: { data: { reportFind: templateDataReport } },
          o: templateOutputActions,
        };
        const action$ = hot(marbles.i, values);
        const state$ = new StateObservable(new Subject(), initialState);
        const dependencies = {
          clientPromise: cold(marbles.c, values),
        };

        const output$ = loadReportEpic(action$, state$, dependencies);
        expectObservable(output$).toBe(marbles.o, values);
      });
    });
  });
});
