import { createSelector } from 'reselect';
import { ReportState } from '../state.d';

const selectRaw = (state: { reports: ReportState }) => state.reports;
