import {
  //
  Characteristic,
} from 'types/characteristics';
import { ApolloClient, NormalizedCacheObject } from '@apollo/client';
import { Part } from 'domain/part/partTypes';
import { AppState } from 'modules/state';
import { Dispatch } from 'redux';
import { cloneDeep } from 'lodash';
import { Annotations } from 'graphql/annotation';
import {
  Annotation,
  AnnotationActionTypes,
  AnnotationCreate,
  AnnotationCreateVars,
  AnnotationDelete,
  AnnotationDeleteVars,
  AnnotationEdit,
  AnnotationEditVars,
  AnnotationFind,
  AnnotationList,
  AnnotationListVars,
  defaultAnnotation,
} from 'types/annotation';
import { CharacteristicActions, getCapturedItems } from 'modules/characteristic/characteristicActions';
import { Parts } from 'domain/part/partApi';
import { getAnnotationObject } from 'domain/part/edit/characteristics/characteristicsRenderer';
import Analytics from 'modules/shared/analytics/analytics';

export const AnnotationActions: { [key: string]: AnnotationActionTypes } = {
  EDIT_ANNOTATION: 'ANNOTATIONS_EDIT_ANNOTATION',
  ERROR: 'ANNOTATIONS_ERROR',
};

export const findQuery = async (queryObject: { variables: { id: string }; fetchPolicy?: any }, client: ApolloClient<NormalizedCacheObject>) =>
  client.query<AnnotationFind>({
    query: Annotations.query.find,
    variables: queryObject.variables,
    notifyOnNetworkStatusChange: true,
    fetchPolicy: queryObject.fetchPolicy || 'cache-first',
  });

export const loadQuery = async (queryObject: { variables: AnnotationListVars; fetchPolicy: any }, client: ApolloClient<NormalizedCacheObject>) =>
  client.query<AnnotationList>({
    query: Annotations.query.list,
    variables: queryObject.variables,
    notifyOnNetworkStatusChange: true,
    fetchPolicy: queryObject.fetchPolicy || 'cache-first',
  });

export const editMutation = async (queryObject: { variables: AnnotationEditVars }, client: ApolloClient<NormalizedCacheObject>, refetchQueries: any) =>
  client.mutate<AnnotationEdit, AnnotationEditVars>({
    mutation: Annotations.mutate.edit,
    variables: queryObject.variables,
    refetchQueries,
  });

export const createMutation = async (queryObject: { variables: AnnotationCreateVars }, client: ApolloClient<NormalizedCacheObject>, refetchQueries: any) =>
  client.mutate<AnnotationCreate, AnnotationCreateVars>({
    mutation: Annotations.mutate.create,
    variables: queryObject.variables,
    refetchQueries,
  });

export const deleteMutation = async (queryObject: { variables: { ids: string[] } }, client: ApolloClient<NormalizedCacheObject>, refetchQueries: any) =>
  client.mutate<AnnotationDelete, AnnotationDeleteVars>({
    mutation: Annotations.mutate.delete,
    variables: queryObject.variables,
    refetchQueries,
  });

export const editAnnotationThunk = (queryObject: { variables: AnnotationEditVars }) => async (dispatch: Dispatch<any>, getState: () => AppState, clientPromise: Promise<ApolloClient<NormalizedCacheObject>>) => {
  return clientPromise.then(async (client) => {
    try {
      const annotationEdit = (await editMutation(queryObject, client, null)).data?.annotationUpdate;

      const state = getState();
      const details = cloneDeep(state.characteristics);
      const newSelected: (Characteristic | Annotation)[] = annotationEdit ? [annotationEdit] : [];

      details.annotations = details.annotations.map((annotation) => {
        if (annotationEdit && annotation.id === annotationEdit.id) {
          return annotationEdit;
        }
        return annotation;
      });

      let { capturedItems } = details;

      if (details.defaultMarker) {
        capturedItems = getCapturedItems(details.characteristics, details.annotations, details.markers, details.defaultMarker, state.partEditor.partDrawing?.id || '');
      }

      if (annotationEdit) {
        Analytics.track({
          event: Analytics.events.annotationUpdated,
          properties: {
            annotation: annotationEdit,
          },
        });
      }

      dispatch({ type: CharacteristicActions.UPDATE_SELECTED, payload: { annotations: details.annotations, selected: newSelected, capturedItems } });
    } catch (error) {
      dispatch({ type: AnnotationActions.ERROR, payload: { error } });
    }
  });
};

export const deleteAnnotationThunk = (queryObject: { variables: { ids: string[] } }, part: Part) => async (dispatch: Dispatch, getState: () => AppState, clientPromise: Promise<ApolloClient<NormalizedCacheObject>>) => {
  return clientPromise.then(async (client) => {
    try {
      await deleteMutation(queryObject, client, [
        {
          query: Annotations.query.list,
          variables: {
            filter: { part: part!.id, deletedAtRange: undefined },
          },
        },
      ]);

      const state = getState();

      const details = cloneDeep(state.characteristics);

      details.annotations = details.annotations.filter((annotation: Annotation) => !queryObject.variables.ids.includes(annotation.id));
      details.capturedItems = details.capturedItems.map((sheet) => {
        return sheet.filter((capture) => !queryObject.variables.ids.includes(capture.id));
      });

      Analytics.track({
        event: Analytics.events.annotationDeleted,
        properties: {
          ids: queryObject.variables.ids,
        },
      });

      dispatch({ type: CharacteristicActions.UPDATE_SELECTED, payload: { annotations: details.annotations, capturedItems: details.capturedItems, selected: [] } });
    } catch (error) {
      dispatch({ type: CharacteristicActions.ERROR, payload: { error } });
    }
  });
};

export const createAnnotationThunk = () => async (dispatch: Dispatch, getState: () => AppState, clientPromise: Promise<ApolloClient<NormalizedCacheObject>>) => {
  return clientPromise.then(async (client) => {
    const state = getState();
    const { part } = state.part;
    const { partDrawing, partDrawingSheet } = state.partEditor;
    const { zoom, rotation, index } = state.pdfView;

    try {
      if (!part || !partDrawing || !partDrawingSheet) {
        throw new Error('Missing data');
      }
      const data = { ...defaultAnnotation, drawingScale: zoom, drawingRotation: rotation, partId: part.id, drawingId: partDrawing.id, drawingSheetId: partDrawingSheet.id, drawingSheetIndex: index + 1 };
      switch (rotation) {
        case 90:
          data.boxLocationX = 10;
          data.boxLocationY = partDrawingSheet.height - data.boxWidth - 10;
          data.boxHeight = defaultAnnotation.boxWidth;
          data.boxWidth = defaultAnnotation.boxHeight;
          break;
        case 180:
          data.boxLocationX = partDrawingSheet.width - data.boxWidth - 10;
          data.boxLocationY = partDrawingSheet.height - data.boxHeight - 10;
          break;
        case 270:
          data.boxLocationX = partDrawingSheet.width - data.boxHeight - 10;
          data.boxLocationY = 10;
          data.boxHeight = defaultAnnotation.boxWidth;
          data.boxWidth = defaultAnnotation.boxHeight;
          break;
        default:
          break;
      }
      const annotation = await createMutation({ variables: { data } }, client, [
        {
          query: Annotations.query.list,
          variables: {
            filter: { part: part.id, deletedAtRange: undefined },
          },
        },
        {
          query: Parts.query.find,
          variables: {
            id: part.id,
          },
        },
      ]);

      if (annotation?.data?.annotationCreate) {
        const details = cloneDeep(state.characteristics);
        details.annotations.push(annotation.data?.annotationCreate);

        const capture = getAnnotationObject(annotation.data.annotationCreate);

        if (index >= details.capturedItems.length) {
          const pagesToAdd = index - (details.capturedItems.length - 1);
          for (let i = 0; i < pagesToAdd; i++) {
            details.capturedItems.push([]);
          }
        }

        details.capturedItems[index].push(capture);

        Analytics.track({
          event: Analytics.events.annotationCreated,
          properties: {
            annotation: annotation.data.annotationCreate,
          },
        });

        dispatch({ type: CharacteristicActions.UPDATE_SELECTED, payload: { annotations: details.annotations, capturedItems: details.capturedItems, selected: [annotation.data.annotationCreate] } });
      }
    } catch (error) {
      dispatch({ type: CharacteristicActions.ERROR, payload: { error } });
    }
  });
};
