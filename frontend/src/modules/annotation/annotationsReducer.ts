import { CharacteristicsState, AnnotationsAction } from 'modules/state';

const initialCaptureMode = 'Manual';

export const initialState: CharacteristicsState = {
  characteristics: [],
  inactiveCharacteristics: [],
  annotations: [],
  selected: [],
  markers: [],
  defaultMarker: null,
  capturedItems: [[]],
  lastLabel: null,
  count: 0, // May not be used anywhere per https://bitbucket.org/ixeng/ixc-mono/pull-requests/127/ixc-823-ixc-824-ocr-captures-wip#comment-171758845
  mode: initialCaptureMode,
  singleCustom: false,
  verified: 0,
  index: -1,
  previousChangeTime: null,
  previousCount: 0,
  previousHRCount: 0,
  loaded: true,
  parsed: true,
  error: null,
  partId: '',
  compareRevision: null,
  compareIndex: 0,
};

export default (state: CharacteristicsState = initialState, { type, payload }: AnnotationsAction): CharacteristicsState => {
  switch (type) {
    case 'ANNOTATIONS_ERROR':
      return {
        ...state,
        error: payload.error,
      };
    default:
      return state;
  }
};
