import { v4 as uuid } from 'uuid';
import { i18n } from 'i18n';
import * as filesize from 'filesize';
import Axios from 'modules/shared/network/axios';
import config from 'config';
import getStore from '../../store';

function extractExtensionFrom(filename) {
  if (!filename) {
    return null;
  }

  const regex = /(?:\.([^.]+))?$/;
  return regex.exec(filename)[1];
}

export default class FileUploader {
  static validate(file, schema) {
    if (!schema) {
      return;
    }

    if (schema.image) {
      if (!file.type.startsWith('image')) {
        throw new Error(i18n('fileUploader.image'));
      }
    }

    if (schema.size && file.size > schema.size) {
      throw new Error(i18n('fileUploader.size', filesize(schema.size)));
    }

    const extension = extractExtensionFrom(file.name);

    if (schema.formats && !schema.formats.includes(extension)) {
      throw new Error(i18n('fileUploader.formats', schema.formats.join('/')));
    }
  }

  static uploadFromRequest(path, request, schema, onSuccess, onError) {
    try {
      this.validate(request.file, schema);
    } catch (error) {
      request.onError(error);
      onError(error);
      return;
    }

    const extension = extractExtensionFrom(request.file.name);
    const id = uuid();
    const filename = `${id}.${extension}`;
    const privateUrl = `${path}/${filename}`;

    this.uploadToServer(request.file, path, filename)
      .then((publicUrl) => {
        request.onSuccess();
        onSuccess({
          id,
          name: request.file.name,
          sizeInBytes: request.file.size,
          privateUrl,
          publicUrl,
          new: true,
        });
      })
      .catch((error) => {
        request.onError(error);
        onError(error);
      });
  }

  static async uploadToServer(file, path, filename, additionalData) {
    const { jwt } = getStore().getState().auth;
    const formData = new FormData();
    formData.append('filename', filename);
    if (additionalData) {
      for (let index = 0; index < additionalData.length; index++) {
        const data = additionalData[index];
        formData.append(data.key, data.value);
      }
    }
    // File needs to be added to formData last so we can read other form
    // fields first on the backend.
    formData.append('file', file);

    await Axios.post(`${config.backendUrl}/upload/${path}`, formData, {
      headers: {
        'Content-Type': 'multipart/form-data',
        authorization: jwt ? `Bearer ${jwt}` : '',
      },
    });

    return `${config.backendUrl}/download?privateUrl=${filename}`;
  }
}
