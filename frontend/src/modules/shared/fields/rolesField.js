import * as yup from 'yup';
import StringArrayField from 'modules/shared/fields/stringArrayField';

export default class RolesField extends StringArrayField {
  constructor(name, label, config) {
    super(name, label, config);

    this.config = config;
    this.options = config.rolesSelectOptions();
  }

  forExport() {
    return yup
      .mixed()
      .label(this.label)
      .transform((values) => (values ? values.map((value) => this.config.labelOfRole(value)).join(' ') : null));
  }
}
