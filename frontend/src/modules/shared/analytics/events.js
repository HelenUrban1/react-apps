/* 

See ./README.md for event naming guidelines

  // New Convention
  Object Action : param_name


   // Old Convention

   user + action performed + target of action + context

   user + state changed + context

*/

// ---------------------------------------------------------------
//                    Global events
// ---------------------------------------------------------------

// Fired in frontend/src/modules/shared/error/errors.js
export const errorsNonspecific = {
  name: 'Client Error',
  version: '1.0.0',
};

// Fired in frontend/src/view/shared/routes/RoutesComponent.jsx
export const pageBrowserReloaded = {
  name: 'Browser Reloaded',
  version: '1.0.0',
};

// Fired in frontend/src/view/layout/Nav.tsx
export const helpIconClicked = {
  name: 'Nav Help Icon Clicked',
  version: '1.0.0',
};

// Fired in frontend/src/view/layout/Nav.tsx
export const ideagenHomeIconClicked = {
  name: 'Ideagen Home Icon Clicked',
  version: '1.0.0',
};

// ---------------------------------------------------------------
//                    User Registration Lifecycle
// ---------------------------------------------------------------

// Fired in frontend/src/modules/auth/authActions.js
// visitor began signup from an invite email'
// 1.0.0 'Register from Invitation'
export const authCheckInviteToken = {
  name: 'Registration Token Checked',
  version: '1.0.1',
};

// Fired in frontend/src/modules/auth/authActions.js
// Visitor registration is blocked
export const authRegistrationBlocked = {
  name: 'Registration Blocked',
  version: '1.0.1',
};

// Fired in frontend/src/modules/auth/authActions.js
// visitor invite had been canceled
// 1.0.0 Register from Canceled Invitation
export const authCanceledInviteToken = {
  name: 'Register Token Was Canceled',
  version: '1.0.1',
};

// Fired in frontend/src/modules/auth/authActions.js
export const authRequestAccepted = {
  name: 'Access Request Accepted',
  version: '1.0.0',
};

// Fired in frontend/src/modules/auth/authActions.js
// error checking invite token
export const authCheckInviteTokenError = {
  name: 'Error Checking Invitation',
  version: '1.0.0',
};

// Fired in frontend/src/modules/auth/authActions.js
// visitor requested email verification resend
export const authEmailVerificationRequest = {
  name: 'Email Verification Resend Requested',
  version: '1.0.0',
};

// Fired in frontend/src/modules/auth/authActions.js
export const authVerifiedEmail = {
  name: 'Email Verified',
  version: '1.0.0',
};

// Fired in frontend/src/modules/auth/authActions.js
// visitor registered via email and password
// 1.0.0 Register Self
export const authRegisterEmailPassword = {
  name: 'Register Self',
  version: '1.0.0',
};

// Fired in frontend/src/modules/auth/authActions.js
// visitor finished registration
export const authRegistrationSubmitted = {
  name: 'Registration Submitted',
  version: '1.0.0',
};

// ---------------------------------------------------------------
//                    Session Management
// ---------------------------------------------------------------

// Fired in frontend/src/modules/auth/authActions.js
export const authPasswordResetRequest = {
  name: 'Password Reset Requested',
  version: '1.0.0',
};

// Fired in frontend/src/modules/auth/authActions.js
export const authPasswordResetComplete = {
  name: 'Password Reset Complete',
  version: '1.0.0',
};

// Fired in frontend/src/modules/auth/authActions.js
// user signed in via email and password
export const authSigninEmailPassword = {
  name: 'Login',
  version: '1.0.0',
};

// Fired in frontend/src/modules/auth/authActions.js
export const authSignout = {
  name: 'Logout',
  version: '1.0.0',
};

// ---------------------------------------------------------------
//                    Profile Management
// ---------------------------------------------------------------

// Fired in frontend/src/domain/setting/index.tsx
export const adminChangedUserProperties = {
  name: 'Account Member Profile Updated',
  version: '1.0.0',
};

// user updated profile
export const authProfileUpdate = {
  name: 'User Profile Updated',
  version: '1.0.0',
};

// ---------------------------------------------------------------
//                Account Management
// ---------------------------------------------------------------

// admin updated account details
// Fired from frontend/src/view/auth/CompanyProfileForm.jsx
export const companyProfileEdited = {
  name: 'Account Profile Updated',
  version: '1.0.0',
};

// Fired in frontend/src/domain/setting/index.tsx
// This is the start of the transfer process
export const adminRemovedOwnRole = {
  name: 'Account Ownership Transferred',
  version: '1.0.0',
};

// Fired from frontend/src/domain/setting/index.tsx
// account owner transferred their ownership to another user
// This is the end of the transfer process
export const ownerTransferredOwnership = {
  name: 'account owner transferred their ownership to another user',
  version: '1.0.0',
};

// ---------------------------------------------------------------
//                  Trial & Subscription Lifecycle
// ---------------------------------------------------------------

// Fired from frontend/src/modules/auth/authActions.js
// Also fired from frontend/src/view/auth/SignupPage.jsx
// TODO QUESTION: Are these two separate paths to creating a trial? Should this event be moved into a common service?
export const accountTrialStarted = {
  name: 'Trial Started',
  version: '1.0.0',
};

// TODO
export const accountTrialExtended = {
  name: 'Trial Extended',
  version: '1.0.0',
};

// TODO
export const accountTrialExpired = {
  name: 'Trial Expired',
  version: '1.0.0',
};

// ---------------------------------------------------------------
//                      Trial Notification Bar
// ---------------------------------------------------------------

// Fired from frontend/src/view/shared/styles/TrialBar.tsx
// user clicked to upgrade trial to subscription from the trial notification bar
// TODO: This should be merged with accountTrialStarted with source as a property
export const trialCreateSubscription = {
  name: 'Trial Bar Subscription Start',
  version: '1.0.0',
};

// Fired from frontend/src/view/shared/styles/TrialBar.tsx
// user clicked to add credit card while upgrading trial to subscription from the trial notification bar
export const trialSetCreditCard = {
  name: 'Trial Bar Subscription Add CC',
  version: '1.0.0',
};

// Fired from frontend/src/view/shared/styles/TrialBar.tsx
// user clicked to remove credit card while upgrading trial to subscription from the trial notification bar
export const trialRemoveCreditCard = {
  name: 'Trial Bar Subscription Remove CC',
  version: '1.0.0',
};

// frontend/src/styleguide/CreditCardForm/CreditCardForm.jsx
export const accountPaymentMethodUpdated = {
  name: 'Payment Method Updated',
  version: '1.0.0',
};

// frontend/src/styleguide/CreditCardForm/CreditCardForm.jsx
export const accountPaymentMethodUpdateError = {
  name: 'Payment Method Error',
  version: '1.0.0',
};

// frontend/src/view/shared/styles/TrialBar.tsx
export const accountSubscriptionStarted = {
  name: 'Subscription Started',
  version: '1.0.0',
};

// frontend/src/domain/setting/billing/SubscriptionInfo.tsx
export const accountSubscriptionUpdated = {
  name: 'Subscription Updated',
  version: '1.0.0',
};

// frontend/src/domain/setting/billing/SubscriptionInfo.tsx
export const accountSubscriptionCancellationRequested = {
  name: 'Subscription Cancellation Requested',
  version: '1.0.0',
};

// frontend/src/domain/setting/billing/SubscriptionInfo.tsx
export const accountSubscriptionReactivation = {
  name: 'Subscription Reactivated',
  version: '1.0.0',
};

// ---------------------------------------------------------------
//                       Subscription Management
// ---------------------------------------------------------------

// Fired from frontend/src/modules/subscription/subscriptionActions.ts
export const subscriptionBillingInitError = {
  name: 'Subscription Init Error',
  version: '1.0.0',
};

// TODO: QUESTION: How is InitError different from InitFailure
export const subscriptionBillingInitFailure = {
  name: 'Subscription Init Failure',
  version: '1.0.0',
};

// Fired from frontend/src/modules/subscription/subscriptionActions.ts
// TODO: Should this be a clientError or a detail of "Subscription Init Error"
export const subscriptionBillingInitParseError = {
  name: 'Subscription Init Error parsing sub data',
  version: '1.0.0',
};

// Fired from frontend/src/domain/settings/billing/modals/ChangeSubscriptionModal.tsx
// user updated subscription tier
export const subscriptionChangeTier = {
  name: 'Subscription Tier Updated',
  version: '1.0.0',
};

// Fired from frontend/src/domain/settings/billing/modals/ChangeSubscriptionModal.tsx
// user updated subscription seats
export const subscriptionChangeSeats = {
  name: 'Subscription Seats Updated',
  version: '1.0.0',
};

// Fired from frontend/src/domain/settings/billing/modals/ChangeSubscriptionModal.tsx
// user updated subscription billing period
export const subscriptionChangePaymentPeriod = {
  name: 'Subscription Billing Period Updated',
  version: '1.0.0',
};

// Fired from frontend/src/domain/settings/billing/modals/ChangeSubscriptionModal.tsx
// user updated subscription quota
export const subscriptionChangeQuota = {
  name: 'Subscription Quota Updated',
  version: '1.0.0',
};

// Fired from frontend/src/domain/settings/billing/modals/ChangeSubscriptionModal.tsx
// user viewed amount due for update
export const subscriptionChangeCalculateDue = {
  name: 'Subscription Update Cost Viewed',
  version: '1.0.0',
};

// Fired from frontend/src/domain/settings/billing/modals/ChangeSubscriptionModal.tsx
// user clicked help button for tier changes to view pricing table
export const subscriptionChangeTierHelp = {
  name: 'Subscription Tier Help Clicked',
  version: '1.0.0',
};

// Fired from frontend/src/domain/settings/billing/modals/ChangeSubscriptionModal.tsx
// user cancelled update to default payment
export const subscriptionChangeCardCancel = {
  name: 'Default Payment Update Cancelled',
  version: '1.0.0',
};

// Fired from frontend/src/domain/settings/billing/modals/ChangeSubscriptionModal.tsx
// user confirmed default payment update
export const subscriptionChangeCardConfirm = {
  name: 'Default Payment Update Confirmed',
  version: '1.0.0',
};

// Fired from frontend/src/domain/settings/billing/modals/ChangeSubscriptionModal.tsx
// user clicked change to update default payment
export const subscriptionChangeCardStart = {
  name: 'Default Payment Change Start',
  version: '1.0.0',
};

// Fired from frontend/src/domain/settings/billing/modals/ChangeSubscriptionModal.tsx
// user clicked change subscription
export const subscriptionChangeOpen = {
  name: 'Subscription Change Start',
  version: '1.0.0',
};

// Fired from frontend/src/domain/settings/billing/modals/ChangeSubscriptionModal.tsx
// user cancelled change subscription
export const subscriptionChangeCancel = {
  name: 'Subscription Change Cancelled',
  version: '1.0.0',
};

// Fired from frontend/src/domain/settings/billing/modals/ChangeSubscriptionModal.tsx
// user confirmed change subscription
export const subscriptionChangeConfirm = {
  name: 'Subscription Change Confirmed',
  version: '1.0.0',
};

// Fired from frontend/src/domain/settings/billing/SubscriptionInfo.tsx
// user changes successfully saved in the database
export const subscriptionChangeComplete = {
  name: 'Subscription Change Complete',
  version: '1.0.0',
};

// QUESTION: How is this different than user changing quota on line 613 'user updated subscription quota
// Fired from frontend/src/domain/part/modals/DrawingLimitModal.tsx
// user changed number of purchased drawings
export const subscriptionDrawingsChangeDrawings = {
  name: 'Subscription Drawings Change',
  version: '1.0.0',
};

// Fired from frontend/src/domain/part/modals/DrawingLimitModal.tsx
// user upgraded number of drawings with prorated due amount for change
export const subscriptionDrawingsChangeCalculateDue = {
  name: 'Subscription Drawings Change Calculate Due',
  version: '1.0.0',
};

// Fired from frontend/src/domain/part/modals/DrawingLimitModal.tsx
// QUESTION: How are these different from the CC events on line 30
// user cancelled changing their default payment credit card
export const subscriptionDrawingsChangeCardCancel = {
  name: 'Subscription Drawings Change Card Cancel',
  version: '1.0.0',
};

// Fired from frontend/src/domain/part/modals/DrawingLimitModal.tsx
// user confirmed changes to their default payment credit card
export const subscriptionDrawingsChangeCardConfirm = {
  name: 'Subscription Drawings Change Card Confirm',
  version: '1.0.0',
};

// Fired from frontend/src/domain/part/modals/DrawingLimitModal.tsx
// user clicked change to update their default payment credit card
export const subscriptionDrawingsChangeCardStart = {
  name: 'Subscription Drawings Change Card Start',
  version: '1.0.0',
};

// Fired from frontend/src/domain/part/list/index.tsx
// user subscription quota limit reached
export const subscriptionDrawingsChangeOpen = {
  name: 'Subscription Drawings Change Open',
  version: '1.0.0',
};

// Fired from frontend/src/domain/part/modals/DrawingLimitModal.tsx
// user cancelled out of changing the number of drawings for the subscription
export const subscriptionDrawingsChangeCancel = {
  name: 'Subscription Drawings Change Canceled',
  version: '1.0.0',
};

// Fired from frontend/src/domain/part/modals/DrawingLimitModal.tsx
// user agreed to prorated amount due
export const subscriptionDrawingsChangeConfirm = {
  name: 'Subscription Drawings Change Confirmed',
  version: '1.0.0',
};

// Fired from frontend/src/domain/part/modals/DrawingLimitModal.tsx
// user changes to the number of drawings for the subscription successfully saved in the database
export const subscriptionDrawingsChangeComplete = {
  name: 'Subscription Drawings Change Complete',
  version: '1.0.0',
};

// Fired from frontend/src/domain/setting/billing/modals/ChangeSubscriptionModal.tsx
// user added net-inspect to their subscription
export const subscriptionNetInspectPurchase = {
  name: 'Subscription Net-Inspect Added',
  version: '1.0.0',
};

// Fired from frontend/src/domain/part/list/PartDetailsActions.tsx
// user uploaded their part to net-inspect
export const subscriptionNetInspectUpload = {
  name: 'Net-Inspect Uploaded',
  version: '1.0.0',
};

// Fired from frontend/src/domain/part/list/PartDetailsActions.tsx
// user net-inspect upload encountered an error
export const subscriptionNetInspectFailure = {
  name: 'Net-Inspect Failure',
  version: '1.0.0',
};

// ---------------------------------------------------------------
//                       Team Management
// ---------------------------------------------------------------

// Fired from frontend/src/domain/settings/seats/InviteUsersModal.tsx
// user clicked invite users button
export const inviteUsersStarted = {
  name: 'Invite Users Clicked',
  version: '1.0.0',
};

// Fired from frontend/src/domain/settings/seats/InviteUsersModal.tsx
// user clicked cancel button on invite users modal
export const inviteUsersCanceled = {
  name: 'Invite Users Canceled',
  version: '1.0.0',
};

// Fired from frontend/src/domain/settings/seats/InviteUsersModal.tsx
// user entered invalid emails when inviting users
export const inviteUsersError = {
  name: 'Invite Users Input Error',
  version: '1.0.0',
};

// Fired from frontend/src/domain/settings/seats/InviteUsersModal.tsx
// user sent user invitations
export const inviteUsersSent = {
  name: 'User Invitations Sent',
  version: '1.0.0',
};

// Fired from frontend/src/domain/settings/seats/index.tsx
// user clicked cancel button on seats users table to cancel invitation
export const inviteEmailCanceledStarted = {
  name: 'User Invitation Cancel Started',
  version: '1.0.0',
};

// Fired from frontend/src/domain/settings/seats/index.tsx
// admin approved user access request
export const accessRequestAccepted = {
  name: 'User Access Request Approved',
  version: '1.0.0',
};

// Fired from frontend/src/domain/settings/seats/index.tsx
// user successfully cancelled invitation and removed user from their plan
export const inviteEmailCanceled = {
  name: 'User Invitation Cancel Complete',
  version: '1.0.0',
};

// Fired from frontend/src/domain/settings/seats/index.tsx
// successfully rejected access request from user
export const accessRequestRejected = {
  name: 'User Access Request Rejected',
  version: '1.0.0',
};

// Fired from frontend/src/domain/settings/seats/index.tsx
// error attempting to cancel a user invitation
export const inviteEmailCanceledError = {
  name: 'User Invitation Cancel Error',
  version: '1.0.0',
};

// Fired from frontend/src/domain/settings/seats/index.tsx
// error attempting to reject an access request
export const accessRequestRejectedError = {
  name: 'User Access Request Rejection Error',
  version: '1.0.0',
};

// Fired from frontend/src/domain/settings/seats/index.tsx
// user clicked resend button on seats users table to resend invitation email
export const inviteReSendStarted = {
  name: 'User Invitation Resend Start',
  version: '1.0.0',
};

// Fired from frontend/src/domain/settings/seats/index.tsx
// user encountered an error attempting to resend invitation email
export const inviteReSentError = {
  name: 'User Invitation Resend Error',
  version: '1.0.0',
};
// Fired from frontend/src/domain/settings/seats/index.tsx
// user successfully resent invitation email
export const inviteReSent = {
  name: 'User Invitation Resend Finished',
  version: '1.0.0',
};

// Fired from frontend/src/modules/auth/authActions.js
// Owner reset the 2FA of another user
export const authResetMfa = {
  name: 'Owner Reset User MFA',
  version: '1.0.0',
};

// Fired from frontend/src/modules/auth/authActions.js
// User completed their MFA registration successfully
export const authRegisterMfa = {
  name: 'User Registered MFA',
  version: '1.0.0',
};

// Fired from frontend/src/view/shared/MFAModal.tsx
// User cancelled MFA registration
// Fired from frontend/src/modules/auth/authActions.js
// Owner reset the 2FA of another user
export const authCancelMfaRegistration = {
  name: 'User Cancelled MFA Registration',
  version: '1.0.0',
};

// ---------------------------------------------------------------
//                    Configuration Events
// ---------------------------------------------------------------

// admin added an entry to one of their configuration lists
export const addedConfigurationEntry = {
  name: 'Config List Item Added',
  version: '1.0.0',
};

// admin removed an entry from one of their configuration lists
export const deletedConfigurationEntry = {
  name: 'Config List Item Deleted',
  version: '1.0.0',
};

// admin updated an entry on one of their configuration lists
export const editedConfigurationEntry = {
  name: 'Config List Item Updated',
  version: '1.0.0',
};

// admin reordered one of their configuration lists
export const reorderedConfigurationList = {
  name: 'Config List Reordered',
  version: '1.0.0',
};

// admin toggled the visibility for an entry on one of their configuration lists
export const toggleConfigurationEntry = {
  name: 'Config List Item Visibility Updated',
  version: '1.0.0',
};

// Fired from styleguide/ConfigTables/StylesTable/StylesTable.tsx
// created a new style preset from the Configurations page
export const createdStylePresetFromConfig = {
  name: 'Marker Style Added via Config',
  version: '1.0.0',
};

// Fired from styleguide/ConfigTables/StylesTable/StylesTable.tsx
// deleted a style preset from the Configs page
export const deletedStylePreset = {
  name: 'Marker Style Deleted via Config',
  version: '1.0.0',
};

// Fired from domain/part/edit/balloonStyles/BalloonStylesForm.tsx
// created a new style preset from the Balloon Styles step of the Wizard
export const createdStylePresetFromPart = {
  name: 'Marker Style Added via Part Wizard',
  version: '1.0.0',
};

// Fired from domain/part/edit/balloonStyles/BalloonStylesForm.tsx
// updated style preset from styles form
export const updatedStylePreset = {
  name: 'Marker Style Updated',
  version: '1.0.0',
};

// Fired from domain/part/edit/defaultTolerances/DefaultTolerancesForm.tsx
// created Tols preset from Tols step of the Wizard
export const createdTolerancePresetFromPart = {
  name: 'Tolerance Preset Added via Part Wizard',
  version: '1.0.0',
};

// Fired from styleguide/ConfigTables/TolerancesTable/index.tsx
// deleted a Tol preset from the Configs page
export const deletedTolerancePreset = {
  name: 'Tolerance Preset Deleted via Config',
  version: '1.0.0',
};

// Fired from domain/part/edit/balloonStyles/DefaultTolerancesForm.tsx
// updated tols preset from the tols form
export const updatedTolerancePreset = {
  name: 'Tolerance Preset Updated',
  version: '1.0.0',
};

// ---------------------------------------------------------------
//                    Part List Events
//  PL = Part List
//  PLR = Part List Row
//  PLD = Part List Drawer
// ---------------------------------------------------------------

// Fired from frontend/src/domain/part/list/index.tsx
// user clicked part list row
export const partRowClicked = {
  name: 'PL Row Clicked',
  version: '1.0.0',
};

// Fired from frontend/src/domain/part/list/index.tsx
// user clicked part list row
export const partRevisionSelected = {
  name: 'Part Revision Selected',
  version: '1.0.0',
};

// Fired from frontend/src/domain/part/list/index.tsx
// user clicked extract
export const userClickedExtract = {
  name: 'PL Extract Clicked',
  version: '1.0.0',
};

// Fired from frontend/src/domain/part/list/index.tsx
// user clicked delete part
export const userClickedDeletePart = {
  name: 'PL Delete Part Clicked',
  version: '1.0.0',
};

// Fired from frontend/src/domain/part/list/index.tsx
// part edited from drawer
export const partEditedFromDrawer = {
  name: 'PLD Part Updated',
  version: '1.0.0',
};

// Fired from frontend/src/domain/part/list/index.tsx
// user clicked delete part drawing
export const userClickedDeleteDrawing = {
  name: 'PLD Delete Drawing Clicked',
  version: '1.0.0',
};

// Fired from frontend/src/domain/part/list/index.tsx
// user clicked delete report
export const userClickedDeleteReport = {
  name: 'PLD Delete Report Clicked',
  version: '1.0.0',
};

// Fired from frontend/src/domain/part/list/index.tsx
// user clicked download file
export const userClickedDownloadFile = {
  name: 'PLD Download File Clicked',
  version: '1.0.0',
};

// Fired from frontend/src/domain/part/list/index.tsx
// user clicked add part
export const userClickedAddPart = {
  name: 'PL Add Part Clicked',
  version: '1.0.0',
};

// Fired from frontend/src/domain/part/list/index.tsx
// user clicked add part
export const userClickedAddDrawing = {
  name: 'PL Add Drawing Clicked',
  version: '1.0.0',
};

// Fired from frontend/src/domain/part/list/index.tsx
// user clicked add attachment
export const userClickedAddAttachment = {
  name: 'PL Attach Doc Clicked',
  version: '1.0.0',
};

// Fired from frontend/src/domain/part/list/index.tsx
// user canceled drawing uploaded
export const partDrawingUploadedCanceled = {
  name: 'Part Drawing Upload Canceled',
  version: '1.0.0',
};

// Fired from frontend/src/domain/part/list/index.tsx
// part drawing uploaded
export const partDrawingUploaded = {
  name: 'Part Drawing Uploaded',
  version: '1.0.0',
};

// Fired from frontend/src/modules/part/partActions.tsx
// part attachment uploaded
export const partAttachmentUploaded = {
  name: 'Part Attachment Uploaded',
  version: '1.0.0',
};

// Fired from frontend/src/modules/part/partActions.tsx
// part attachment uploaded
export const partAttachmentRemoved = {
  name: 'Part Attachment Removed',
  version: '1.0.0',
};

// ---------------------------------------------------------------
//                    Part Creation Events
//  PED = Part Editor Drawer
// ---------------------------------------------------------------

// Fired in frontend/src/modules/navigation/navigationActions.ts
// user started part editor step
export const partStepStarted = {
  name: 'Part Editor Step Started',
  version: '1.0.0',
};

// Fired in frontend/src/view/layout/Nav.tsx
// user clicked part sidenav
export const partSideNavClicked = {
  name: 'Part Sidenav Clicked',
  version: '1.0.0',
};

// Fired from frontend/src/domain/part/edit/index.tsx
// user entered invalid part id in URL
export const partIDError = {
  name: 'Invalid Part ID Error',
  version: '1.0.0',
};

// Fired from frontend/src/modules/partEditor/partEditorActions.ts
export const partEditorInitError = {
  name: 'Error Initializing Part Editor',
  version: '1.0.0',
};

// Fired from frontend/src/domain/part/edit/index.tsx
// restricted user tried to access a controlled part
export const partRestricted = {
  name: 'Access Denied to Controlled Part',
  version: '1.0.0',
};

// Fired in frontend/src/domain/part/edit/PartEditorDrawer.tsx
// user clicked part editor drawer next
export const partDrawerDrawerMoveNext = {
  name: 'PED Next Clicked',
  version: '1.0.0',
};

// Fired in frontend/src/domain/part/edit/PartEditorDrawer.tsx
// user clicked part editor drawer back
export const partEditorDrawerMoveBack = {
  name: 'PED Back Clicked',
  version: '1.0.0',
};

// Fired in frontend/src/domain/part/edit/PartEditorDrawer.tsx
// user clicked part editor drawer cancel
export const partEditorDrawerCancel = {
  name: 'PED Cancel Clicked ',
  version: '1.0.0',
};

// Fired in frontend/src/domain/part/edit/PartEditorDrawer.tsx
// user exited part editor
export const userExitedPartEditor = {
  name: 'Part Editor Exited',
  version: '1.0.0',
};

// Fired from frontend/src/domain/part/edit/partDetails/PartDetailsForm.tsx
// part info updated
export const partInfoUpdated = {
  name: 'Part Info Updated',
  version: '1.0.0',
};

// Fired from frontend/src/domain/part/edit/documentDetails/DocumentDetailsForm.tsx
// part info updated
export const docInfoUpdated = {
  name: 'Document Info Updated',
  version: '1.0.0',
};

// Fired from frontend/src/domain/part/edit/defaultTolerances/DefaultTolerancesForm.tsx
// part tolerances updated
export const partTolerancesUpdated = {
  name: 'Part Tolerances Updated',
  version: '1.0.0',
};

// Fired from frontend/src/domain/part/edit/balloonStyles/BalloonStylesForm.tsx
// balloon styles updated
export const featureMarkerStylesUpdated = {
  name: 'Feature Marker Styles Updated',
  version: '1.0.0',
};

// Fired from frontend/src/domain/part/edit/balloonStyles/BalloonSelect.tsx
// balloon style deleted
export const featureMarkerStyleDeleted = {
  name: 'Feature Marker Style Deleted',
  version: '1.0.0',
};

// Fired from frontend/src/domain/part/edit/balloonStyles/BalloonSelect.tsx
// balloon style created
export const featureMarkerStyleCreated = {
  name: 'Feature Marker Style Created',
  version: '1.0.0',
};

// Fired from frontend/src/domain/part/edit/balloonStyles/BalloonSelect.tsx
// balloon style edited
export const featureMarkerStyleUpdated = {
  name: 'Feature Marker Style Updated',
  version: '1.0.0',
};

// Fired from frontend/src/domain/part/edit/grid/GridForm.tsx
// part grid updated
export const partGridUpdated = {
  name: 'Part Grid Updated',
  version: '1.0.0',
};

// Fired from frontend/src/domain/part/edit/grid/GridForm.tsx
// part grid updated
export const drawingGridOn = {
  name: 'Drawing Grid On',
  version: '1.0.0',
};

// Fired from frontend/src/domain/part/edit/grid/GridForm.tsx
// part grid updated
export const drawingGridOff = {
  name: 'Drawing Grid Off',
  version: '1.0.0',
};

// Fired from frontend/src/domain/part/edit/grid/GridForm.tsx
// part grid applied to all sheets
export const partGridAppliedToAllSheets = {
  name: 'Part Grid Applied to all Sheets',
  version: '1.0.0',
};

// Fired from frontend/src/domain/modules/part/partActions.tsx
// A new part revision was started
export const partRevisionStarted = {
  name: 'Part Revision Started',
  version: '1.0.0',
};

// Fired from frontend/src/domain/modules/part/partActions.tsx
// A new part revision was completed
export const partRevisionCompleted = {
  name: 'Part Revision Completed',
  version: '1.0.0',
};

// Fired from frontend/src/domain/modules/part/partActions.tsx
// A new drawing revision was started
export const partDrawingRevisionStarted = {
  name: 'Drawing Revision Started',
  version: '1.0.0',
};

// Fired from frontend/src/domain/modules/part/partActions.tsx
// A new drawing revision was completed
export const partDrawingRevisionCompleted = {
  name: 'Drawing Revision Completed',
  version: '1.0.0',
};

// Fired from frontend/src/comain/modules/part/partEditorActions.tsx
export const partDrawingRevisionCompare = {
  name: 'Drawing Revision Compare Started',
  version: '1.0.0',
};

// Fired from frontend/src/comain/modules/part/partEditorActions.tsx
export const partDrawingRevisionCompareAndReplaceStarted = {
  name: 'Drawing Com&Rep Started',
  version: '1.0.0',
};

// Fired from frontend/src/comain/modules/part/partEditorActions.tsx
export const partDrawingRevisionCompareAndReplaceCompleted = {
  name: 'Drawing Com&Rep Completed',
  version: '1.0.0',
};

// Fired from frontend/src/comain/modules/part/partEditorActions.tsx
export const partDrawingRevisionCompareAndReplaceCanceled = {
  name: 'Drawing Com&Rep Canceled',
  version: '1.0.0',
};

// TODO: This may get wiped out by the report refactoring
// Fired in frontend/src/domain/part/edit/PartEditorDrawer.tsx
// part report creation started
export const reportWizardStarted = {
  name: 'Report Wizard Started',
  version: '1.0.0',
};

// ---------------------------------------------------------------
//                   Feature Capture Events
// ---------------------------------------------------------------

// Fired in frontend/src/domain/part/edit/characteristics/CharacteristicsCanvas.tsx
// manual capture attempt started
export const manualCaptureStarted = {
  name: 'Manual Capture Started',
  version: '1.0.0',
};

// Fired in frontend/src/domain/part/edit/characteristics/utils/extractUtil.ts
// manual capture error
export const manualCaptureError = {
  name: 'Manual Capture Error',
  version: '1.0.0',
};

// Fired in frontend/src/domain/part/edit/characteristics/utils/extractUtil.ts
// manual capture attempt performed
export const manualCapturePerformed = {
  name: 'Manual Capture Attempt Performed',
  version: '1.0.0',
};

// Fired in frontend/src/domain/part/edit/characteristics/CharacteristicsCanvas.tsx
// manual capture attempt returned
export const manualCaptureReturned = {
  name: 'Manual Capture Attempt Returned',
  version: '1.0.0',
};

// Fired in frontend/src/domain/part/edit/characteristics/CharacteristicsCanvas.tsx
// manual capture attempt returned
export const customCaptureReturned = {
  name: 'Custom Capture Attempt Returned',
  version: '1.0.0',
};

// Fired in frontend/src/domain/part/edit/characteristics/CharacteristicsCanvas.tsx
// manual capture update attempt started
export const manualCaptureUpdateStarted = {
  name: 'Manual Capture Update Attempt Started',
  version: '1.0.0',
};

// Fired in frontend/src/domain/part/edit/characteristics/CharacteristicsCanvas.tsx
// manual capture update attempt returned
export const manualCaptureUpdateReturned = {
  name: 'Manual Capture Update Attempt Returned',
  version: '1.0.0',
};

// Fired in frontend/src/styleguide/Modals/SpecialCharactersModal.tsx
// special chars modal opened
export const specialCharactersModalOpened = {
  name: 'Special Character Modal Opened',
  version: '1.0.0',
};

// ---------------------------------------------------------------
//                   Feature Details Management
// ---------------------------------------------------------------

// Fired in frontend/src/styleguide/Modals/SpecialCharactersModal.tsx
// special char selected
export const specialCharacterAdded = {
  name: 'Special Character Selected',
  version: '1.0.0',
};

// Fired in frontend/src/domain/part/edit/review/ReviewDrawer.tsx
// characteristic field changed
export const characteristicFieldChanged = {
  name: 'Feature Field Changed',
  version: '1.0.0',
};

// Fired in frontend/src/domain/part/edit/review/ReviewDrawer.tsx
// characteristic type changed
export const characteristicTypeChanged = {
  name: 'Feature Type Changed',
  version: '1.0.0',
};

// TODO
// user accepted auto-captured characteristic
export const featureVerified = {
  name: 'Feature Verified',
  version: '1.0.0',
};

// Fired from frontend/src/domain/characteristic/characteristicActions.tsx
export const characteristicCompare = {
  name: 'Comparing characteristics',
  version: '1.0.0',
};

// ---------------------------------------------------------------
//                   Feature List Management
// ---------------------------------------------------------------

// TODO:
// user grouped features
export const featuresGrouped = {
  name: 'Features Grouped',
  version: '1.0.0',
};

// TODO:
// user grouped characteristic
export const featuresUngrouped = {
  name: 'Features Ungrouped',
  version: '1.0.0',
};

export const featuresReordered = {
  name: 'Features Reordered',
  version: '1.0.0',
};

// Fired from frontend/src/domain/part/edit/characteristics/ItemPanel.tsx
// all chars marked as verified
export const allFeaturesMarkedVerified = {
  name: 'All Features Marked Verified',
  version: '1.0.0',
};

// Fired from frontend/src/domain/part/edit/characteristics/ItemPanel.tsx
// single char marked as verified
export const featureMarkedVerified = {
  name: 'Feature Marked Verified',
  version: '1.0.0',
};

// ---------------------------------------------------------------
//                   Annotation Events
// ---------------------------------------------------------------

// Fired from frontend/src/module/annotations/annoationActions.ts
// an annotation was created
export const annotationCreated = {
  name: 'Annotation Created',
  version: '1.0.0',
};

// Fired from frontend/src/module/annotations/annoationActions.ts
// an annotation was updated
export const annotationUpdated = {
  name: 'Annotation Updated',
  version: '1.0.0',
};

// Fired from frontend/src/module/annotations/annoationActions.ts
// an annotation was deleted
export const annotationDeleted = {
  name: 'Annotation Deleted',
  version: '1.0.0',
};

// ---------------------------------------------------------------
//                    OCR Performance
// ---------------------------------------------------------------

// Parse Text Controller
// Fired from frontend/src/utils/ParseTextController/ParseTextController.ts

// ocr results for a new feature is being parsed
export const parseOCR = {
  name: 'OCR Parsing Begin',
  version: '1.0.0',
};

// pytorch return has been parsed through the ENP
export const compareOCRModelsPytorch = {
  name: 'OCR Pytorch Parsed',
  version: '1.0.0',
};

// pytorch failed, tesseract return has been parsed through the ENP
export const compareOCRModelsTesseract = {
  name: 'OCR Pytorch Failed',
  version: '1.0.0',
};

// tesseract failed, return has been saved as a note
export const compareOCRModelsError = {
  name: 'OCR Tesseract Failed',
  version: '1.0.0',
};

// ocr successfully parsed and returned
export const compareOCRModelsSuccess = {
  name: 'OCR Parsed',
  version: '1.0.0',
};

// ---------------------------------------------------------------
//                    Drawing Feature Management
// ---------------------------------------------------------------

// new full specification for an existing feature is being parsed
export const parseFullSpecification = {
  name: 'Feature FullSpec Parsing',
  version: '1.0.0',
};

// parsed return is being classified into the application model
export const featureClassification = {
  name: 'Feature Classification',
  version: '1.0.0',
};

// classified feature is being assigned tolerances and spec limits
export const featureTolerances = {
  name: 'Feature Tolerances/Limits Assign',
  version: '1.0.0',
};

// ui lists and forms for the feature are being updated
export const updateUI = {
  name: 'Feature List/Forms Update',
  version: '1.0.0',
};

// feature is being saved to the database
export const updateDB = {
  name: 'Feature Saving',
  version: '1.0.0',
};

// new feature has been successfully parsed, saved, and returned
export const featureParsingComplete = {
  name: 'Feature Successfully Parsed',
  version: '1.0.0',
};

// the ParseTextController.parseFeatureFromFullSpec encountered an error and could not return a new feature
export const featureParsingError = {
  name: 'Feature FullSpec Parsing Failed',
  version: '1.0.0',
};

// TODO: We can consolidate all of these into a single featureUpdated event with an attribute that indicates what attribute was updated
// Fired from /frontend/src/utils/ParseTextController/ParseTextController.ts

// Changes for an existing feature are being assigned
export const updateChangedFeature = {
  name: 'Feature Update Started',
  version: '1.0.0',
};

// shared feature properties are being updated
export const featureUpdateGeneral = {
  name: 'Feature Properties Update',
  version: '1.0.0',
};

// type, subtype, and class for the feature are being updated
export const featureUpdateTypes = {
  name: 'Feature Type/Subtype/Class Update',
  version: '1.0.0',
};

// tolerances and spec limits for the feature are being updated
export const featureUpdateTolerances = {
  name: 'Feature Tolerances and Limits Update',
  version: '1.0.0',
};

// marker label for the feature is being updated
export const featureUpdateLabel = {
  name: 'Marker Label Update',
  version: '1.0.0',
};

// marker style for the feature is being updated
export const featureUpdateStyle = {
  name: 'Marker Style Update',
  version: '1.0.0',
};

// updated feature has been successfully edited, saved, and returned
export const featureUpdateComplete = {
  name: 'Feature Updated',
  version: '1.0.0',
};

// ---------------------------------------------------------------
//                    Human Review Events
// ---------------------------------------------------------------

// Fired from frontend/src/domain/humanReview/index.tsx
export const changeHumanReviewStatus = {
  name: 'Reviewer Session Status Changed',
  version: '1.0.0',
};

// Fired from frontend/src/domain/humanReview/review/index.tsx
export const rejectedCapture = {
  name: 'Reviewer Rejected Capture',
  version: '1.0.0',
};

// Fired from frontend/src/domain/humanReview/review/index.tsx
// Human reviewer was unsure of the capture and entered a custom reason for skipping
export const inconclusiveCapture = {
  name: 'Reviewer Skipped Capture',
  version: '1.0.0',
};

// Fired from frontend/src/domain/humanReview/review/index.tsx
export const captureCorrected = {
  name: 'Reviewer Corrected Capture',
  version: '1.0.0',
};

// Fired from frontend/src/domain/humanReview/review/index.tsx
export const captureConfirmed = {
  name: 'Reviewer Confirmed Capture',
  version: '1.0.0',
};

// Fired from frontend/src/domain/humanReview/review/index.tsx
export const captureRejected = {
  name: 'Reviewer Rejected Capture',
  version: '1.0.0',
};

// TODO: These may not populate Analytics properties correctly
export const humanReviewEventName = {
  captureCorrected: 'Capture Corrected',
  captureConfirmed: 'Capture Confirmed',
  captureRejected: 'Capture Rejected',
};

// ---------------------------------------------------------------
//                   Reporting Template Event
// ---------------------------------------------------------------

// Fired in frontend/src/domain/report/edit/index.tsx
// report template loading start
export const loadReportTemplateStarted = {
  name: 'Report Template Loading Start',
  version: '1.0.0',
};

// Fired in frontend/src/domain/report/edit/index.tsx
// report template loading finished
export const loadReportTemplateFinished = {
  name: 'Report Template Loading Finished',
  version: '1.0.0',
};

// Fired in frontend/src/domain/report/edit/reportTemplate/index.tsx
// report template cloned
export const cloneReportTemplate = {
  name: 'Report Template Cloned',
  version: '1.0.0',
};

// Fired from frontend/src/domain/report/edit/reportTemplate/index.tsx
// user clicked create new template
export const createNewReportTemplate = {
  name: 'New Report Template Clicked',
  version: '1.0.0',
};

// Fired from frontend/src/domain/report/edit/reportTemplate/index.tsx
// report template selected
export const reportTemplateSelected = {
  name: 'Report Template Selected',
  version: '1.0.0',
};

// TODO
// report template tokens updated
export const reportTemplateTokensUpdated = {
  name: 'Report Template Tokens Updated',
  version: '1.0.0',
};

// TODO
// report template downloaded
export const reportTemplateDownloaded = {
  name: 'Report Template Downloaded',
  version: '1.0.0',
};

// TODO
// report template downloaded
export const reportTemplatePrinted = {
  name: 'Report Template Printed',
  version: '1.0.0',
};

// Fired from frontend/src/domain/report/edit/reportTemplate/index.tsx
// report template deleted
export const reportTemplateDeleted = {
  name: 'Report Template Deleted',
  version: '1.0.0',
};

// Fired from frontend/src/modules/reportTemplate/reportTemplateActions.ts
// report template error
export const reportTemplateError = {
  name: 'Report Template Error',
  version: '1.0.0',
};

// ---------------------------------------------------------------
//                   Reporting Events
// ---------------------------------------------------------------

// Fired in frontend/src/domain/report/edit/ReportEditorDrawer.tsx
// report creation start
export const reportCreationStarted = {
  name: 'Report Creation Started',
  version: '1.0.0',
};

// Fired in frontend/src/domain/report/edit/ReportEditorDrawer.tsx
// report creation finished
export const reportCreationFinished = {
  name: 'Report Creation Finished',
  version: '1.0.0',
};

// Fired in frontend/src/domain/report/edit/ReportEditorDrawer.tsx
// user updated report
export const reportUpdated = {
  name: 'Report Updated',
  version: '1.0.0',
};

// Fired in frontend/src/domain/report/edit/ReportEditorDrawer.tsx
// user updated report title
export const reportTitleUpdated = {
  name: 'Report Title Updated',
  version: '1.0.0',
};

// Fired in frontend/src/domain/report/edit/ReportDataEditor.tsx
// report data refreshed
export const reportDataRefreshed = {
  name: 'Report Data Refreshed',
  version: '1.0.0',
};

// TODO
// report previewed
export const reportPreviewed = {
  name: 'Report Previewed',
  version: '1.0.0',
};

// Fired from frontend/src/domain/report/edit/ReportDataEditor.tsx
export const reportDownloaded = {
  name: 'Report Downloaded',
  version: '1.0.0',
};

// TODO
export const reportPrinted = {
  name: 'Report Printed',
  version: '1.0.0',
};

// Fired from frontend/src/domain/report/edit/ReportDataEditor.tsx
// report deleted
export const reportDeleted = {
  name: 'Report Deleted',
  version: '1.0.0',
};

// Fired from frontend/src/modules/reportTemplate/reportTemplateActions.ts
// report template error
export const reportError = {
  name: 'Report Error',
  version: '1.0.0',
};

// ---------------------------------------------------------------
//                        Job Management
// ---------------------------------------------------------------
// Fired from frontend/src/domain/jobs/list/index.tsx
// job updated
export const jobCreated = {
  name: 'Job Created',
  version: '1.0.0',
};

// Fired from frontend/src/domain/jobs/list/index.tsx
// job deleted
export const jobDeleted = {
  name: 'Job Deleted',
  version: '1.0.0',
};

// Fired from frontend/src/domain/jobs/list/index.tsx
// job updated
export const jobUpdated = {
  name: 'Job Updated',
  version: '1.0.0',
};

// ---------------------------------------------------------------
//                        Measurement Entry
// ---------------------------------------------------------------
// Fired from frontend/src/domain/jobs/list/index.tsx
// started measuring job
export const startMeasure = {
  name: 'Starting Measurements',
  version: '1.0.0',
};

// Fired from frontend/src/metro/measureEntry.tsx
// recorded a measurement
export const measurementCreated = {
  name: 'Recorded Measurement',
  version: '1.0.0',
};

// Fired from frontend/src/metro/measureEntry.tsx
// updated a measurement
export const measurementUpdated = {
  name: 'Updated Measurement',
  version: '1.0.0',
};

// Fired from frontend/src/metro/measureEntry.tsx
// selected a measurement
export const measurementSelected = {
  name: 'Selected Measurement',
  version: '1.0.0',
};

// ---------------------------------------------------------------
//                        Metro General
// ---------------------------------------------------------------
// Fired from frontend/src/metro/operatorSelect.tsx
// operator selected
export const operatorSelect = {
  name: 'Operator Selected',
  version: '1.0.0',
};

// Fired from frontend/src/metro/stationSelect.tsx
// station selected
export const stationSelected = {
  name: 'Station Selected',
  version: '1.0.0',
};

// Fired from frontend/src/metro/stationSelect.tsx
// station selected
export const stationCreated = {
  name: 'Station Created',
  version: '1.0.0',
};
