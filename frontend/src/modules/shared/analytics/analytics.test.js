import Analytics from './analytics';
import * as constants from './constants';
import { version } from '../../../../package.json';

const ANONYMOUS_ID = '3694d233-64eb-4676-94ea-5e9e2802dac2';

describe('Analytics', () => {
  let mockSdk;
  let onErrorSpy;

  beforeEach(() => {
    mockSdk = {
      user: () => {
        return {
          anonymousId: jest.fn(),
          id: jest.fn(),
        };
      },
      getAnonymousId: jest.fn(),
      identify: jest.fn(),
      group: jest.fn(),
      reset: jest.fn(),
      track: jest.fn(),
      page: jest.fn(),
    };
    onErrorSpy = jest.spyOn(Analytics, 'onError');
  });

  afterEach(() => {
    onErrorSpy.mockRestore();
  });

  describe('Common tests across all analytics methods', () => {
    ['getAnonymousId', 'track', 'page', 'identify', 'reset'].forEach((method) => {
      describe(`#${method}()`, () => {
        it('should catch an exception from the SDK call', () => {
          Analytics[method](); // Triggers an exception due to missing input.

          expect(mockSdk[method].mock.calls.length).toBe(0);
          expect(onErrorSpy.mock.calls.length).toBe(1);
        });
      });
    });
  });

  // Segment.io tracks this automatically in the background, so no need to test
  // describe('#getAnonymousId()', () => {
  //   it('should provide an anonymous id', () => {
  //     mockSdk.getAnonymousId.mockReturnValue(ANONYMOUS_ID);
  //     const result = Analytics.getAnonymousId({ sdk: mockSdk });
  //     expect(result).toBe(ANONYMOUS_ID);
  //     expect(onErrorSpy.mock.calls.length).toBe(0);
  //   });
  // });

  describe('#track()', () => {
    it('should set the event name and version', () => {
      const event = Analytics.events.authSignout;
      Analytics.track({ sdk: mockSdk, event });
      expect(onErrorSpy.mock.calls.length).toBe(0);
      const input = mockSdk.track.mock.calls[0];
      expect(input[0]).toBe(event.name);
      expect(input[1].eventVersion).toBe(event.version);
    });

    it('should set the event source', () => {
      const event = Analytics.events.authSignout;
      Analytics.track({ sdk: mockSdk, event });
      expect(onErrorSpy.mock.calls.length).toBe(0);
      const input = mockSdk.track.mock.calls[0];
      expect(input[1].source).toBe(constants.EVENT_SOURCE_FRONTEND);
    });

    it('should pass along any `options` provided', () => {
      const event = Analytics.events.authSignout;
      const options = {
        a: 0,
        b: 1,
      };
      Analytics.track({ sdk: mockSdk, event, options });
      expect(onErrorSpy.mock.calls.length).toBe(0);
      const input = mockSdk.track.mock.calls[0];
      expect(input[2].a).toEqual(0);
      expect(input[2].b).toEqual(1);
    });

    it('should pass along any callback provided', () => {
      const event = Analytics.events.authSignout;
      const callback = () => true;
      Analytics.track({ sdk: mockSdk, event, callback });
      expect(onErrorSpy.mock.calls.length).toBe(0);
      const input = mockSdk.track.mock.calls[0];
      expect(input[3]).toEqual(callback);
    });

    it('should set user metadata when provided with state', () => {
      const event = Analytics.events.authSignout;
      const state = {
        user: { id: 'a user', email: 'an email' },
      };
      Analytics.track({ sdk: mockSdk, event, state });
      expect(onErrorSpy.mock.calls.length).toBe(0);
      const input = mockSdk.track.mock.calls[0];
      expect(input[1].userId).toEqual(state.user.id);
      expect(input[1].email).toEqual(state.user.email);
    });
  });

  describe('#page()', () => {
    const url = '/auth/signup';

    it('should set the page category', () => {
      const category = 'auth';
      Analytics.page({ sdk: mockSdk, url, category });
      // BUG: Returns 1, because sdk is loaded from CDN
      expect(onErrorSpy.mock.calls.length).toBe(0);
      const input = mockSdk.page.mock.calls[0];
      expect(input[0]).toBe(category);
    });

    it('should use name and category if provided', () => {
      const name = 'Sign Up';
      const category = 'Registration';
      Analytics.page({ sdk: mockSdk, url, name, category });
      expect(onErrorSpy.mock.calls.length).toBe(0);
      const input = mockSdk.page.mock.calls[0];
      expect(input[0]).toBe(category);
      expect(input[1]).toBe(name);
    });

    it('should set the event source', () => {
      Analytics.page({ sdk: mockSdk, url });
      expect(onErrorSpy.mock.calls.length).toBe(0);
      const input = mockSdk.page.mock.calls[0];
      expect(input[2].source).toBe(constants.EVENT_SOURCE_FRONTEND);
    });

    it('should pass along any `options` provided', () => {
      const options = { a: 0, b: 1 };
      Analytics.page({ sdk: mockSdk, url, options });
      expect(onErrorSpy.mock.calls.length).toBe(0);
      const input = mockSdk.page.mock.calls[0];
      expect(input[3].a).toEqual(0);
      expect(input[3].b).toEqual(1);
    });

    it('should pass along any callback provided', () => {
      const callback = () => true;
      Analytics.page({ sdk: mockSdk, url, callback });
      expect(onErrorSpy.mock.calls.length).toBe(0);
      const input = mockSdk.page.mock.calls[0];
      expect(input[4]).toEqual(callback);
    });

    it('should set user metadata when provided with state', () => {
      const event = Analytics.events.authSignout;
      const state = {
        user: { id: 'a user', email: 'an email' },
      };
      Analytics.page({ sdk: mockSdk, url, state });
      expect(onErrorSpy.mock.calls.length).toBe(0);
      const input = mockSdk.page.mock.calls[0];
      expect(input[2].userId).toEqual(state.user.id);
      expect(input[2].email).toEqual(state.user.email);
    });
  });

  describe('#identify()', () => {
    // const throwsForUnsetUserId = (userId) => {
    //   if (!userId) {
    //     throw new Error('userId is required');
    //   }
    // };

    // it('should error if the userId is not provided', () => {
    //   // mockSdk.identify.mockImplementation(throwsForUnsetUserId);
    //   Analytics.identify({ sdk: mockSdk, user: {} });
    //   expect(onErrorSpy.mock.calls.length).toBe(1);
    // });

    it('should not error if the user is provided', () => {
      // mockSdk.identify.mockImplementation(throwsForUnsetUserId);
      Analytics.identify({ sdk: mockSdk, user: { id: 'a user id' } });
      expect(onErrorSpy.mock.calls.length).toBe(0);
    });

    it('should pass along any `traits` provided', () => {
      Analytics.identify({ sdk: mockSdk, user: { id: 'a user id' }, traits: { a: 0, b: 1 } });
      expect(onErrorSpy.mock.calls.length).toBe(0);
      const input = mockSdk.identify.mock.calls[0];
      expect(input[1]).toEqual({ userId: 'a user id', a: 0, b: 1 });
    });

    it('should pass along any `options` provided', () => {
      const options = { a: 0, b: 1 };
      Analytics.identify({ sdk: mockSdk, user: { id: 'a user id' }, options });
      expect(onErrorSpy.mock.calls.length).toBe(0);
      const input = mockSdk.identify.mock.calls[0];
      expect(input[2].a).toEqual(0);
      expect(input[2].b).toEqual(1);
    });

    it('should pass along any callback provided', () => {
      const callback = () => true;
      Analytics.identify({ sdk: mockSdk, user: { id: 'a user id' }, callback });
      expect(onErrorSpy.mock.calls.length).toBe(0);
      const input = mockSdk.identify.mock.calls[0];
      expect(input[3]).toEqual(callback);
    });
  });

  describe('#group()', () => {
    it('should error if the userId is not provided', () => {
      // mockSdk.identify.mockImplementation(throwsForUnsetUserId);
      Analytics.group({ sdk: mockSdk, user: {} });
      expect(onErrorSpy.mock.calls.length).toBe(1);
    });

    it('should not error if the user is provided', () => {
      // mockSdk.identify.mockImplementation(throwsForUnsetUserId);
      Analytics.group({ sdk: mockSdk, groupId: 'a group id' });
      expect(onErrorSpy.mock.calls.length).toBe(0);
    });

    it('should pass along any `traits` provided', () => {
      Analytics.group({ sdk: mockSdk, groupId: 'a group id', traits: { a: 0, b: 1 } });
      expect(onErrorSpy.mock.calls.length).toBe(0);
      const input = mockSdk.group.mock.calls[0];
      expect(input[0]).toEqual('a group id');
      expect(input[1]).toEqual({ a: 0, b: 1 });
    });
  });

  describe('#reset()', () => {
    it('should call the SDK with no arguments', () => {
      Analytics.reset({ sdk: mockSdk });
      expect(mockSdk.reset.mock.calls.length).toBe(1);
      expect(mockSdk.reset.mock.calls[0][0]).toBeUndefined();
      expect(onErrorSpy.mock.calls.length).toBe(0);
    });
  });

  describe('#doNotTrack()', () => {
    it('should return true when useragent is blocked', () => {
      let isBlocked = Analytics.doNotTrack({
        userAgent: 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/61.0.3163.100 Chrome/61.0.3163.100 Safari/537.36 PingdomPageSpeed/1.0 (pingbot/2.0; +http://www.pingdom.com/)',
      });
      expect(isBlocked).toBe(true);
      isBlocked = Analytics.doNotTrack({
        userAgent: 'ELB-HealthChecker/2.0',
      });
      expect(isBlocked).toBe(true);
      expect(onErrorSpy.mock.calls.length).toBe(0);
    });
    it('should return false when useragent is NOT blocked', () => {
      const isBlocked = Analytics.doNotTrack({ userAgent: 'Chrome' });
      expect(isBlocked).toBe(false);
      expect(onErrorSpy.mock.calls.length).toBe(0);
    });
  });
});
