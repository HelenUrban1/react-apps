# Event Creation Guidelines

## Intuitiveness

Copy should read like a story and be natural, clear, and consistently structured.

**DO** base naming on a user and their actions.

**DO NOT** base name on a characteristic of the product.

GOOD: "visitor selected desired location as new york".

BAD: "new york selected in dropdown menu"

## Be Consistent

Event names should be consistently structured:

- **user** + **action performed** + **target of action** + **context**

- **user** + **state changed** + **context**

Where...

### user

Should have labels that capture:

- Lifecycle state (e.g., visitor, prospect, trial user, trial admin, user, admin).

- Persona (e.g., owner, quality manager, quality inspector, machine operator, billing contact

### action performed

Should be a descriptive, transitive (or intransitive) verb in the past tense.

- Action should not be vaguely suggestive (e.g., use "selected" or "pressed" not "clicked" or "tapped" because the latter two may suggest a mouse click on desktop web or tap on a mobile device).

### target of action

Is the direct object.

### state changed

Is an intransitive verb (e.g., trial user deactivated).

Example of BAD: "song played"

Example of GOOD: "subscriber played song"

Example of GOOD: "prospect purchased subscription on upsell interstitial 3".

## Length of Event Names

Event names should only be as long as to be clear:

Example of GOOD: "candidate registered"

Example of BAD: "candidate registration successful"

## Event Name's Should be Precise

Be precise with when an event is fired. The following events are not the same:

A) "tax filler selected submit button"
This event name implies that a user successfully hit the submit button — that’s all.

B) "tax filler submitted income tax return"
This event name implies that a user successfully submitted and that there were no technical issues such as API or network failures.

## Metadata

Include metadata in events covering:

Goals: dater_goals; desired_salary

Preferences: shopper_primary_research_device: ipad

States/status: user_type: driver, rider; user_activation_status: registered, completed_profile, added_seventh_friend, sent_first_message

Demographics/psychographics: age; gender; race_and_ethnicity; favorite_youtube_channel

Historic engagement: cumulative_connection_requests_received; first_invite_sent_date

## Calculated Metrics

Use the appropriate capture and calculation methods for the analytical goal:

Absolute Values: Track unique Registrants (#) and Visitors (#).

Relative: This allows the user to see a calculated metric such as Registration Rate (%) = Registrants / Visitors.

Financial: It also enables the calculation of Cost Per Registration = Total Spend (\$) / Registrants
