import config, { isNotTestEnv } from 'config';
import Cookies from 'js-cookie';
import { sample } from 'lodash';
import * as events from './events';
import * as constants from './constants';
// import FeatureFlags from 'modules/featureFlags/featureFlags';
// import HeapAdapter from './analyticsAdapterHeap';

// Exclude heartbeat and healthchecker from request logging
// "user_agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/61.0.3163.100 Chrome/61.0.3163.100 Safari/537.36 PingdomPageSpeed/1.0 (pingbot/2.0; +http://www.pingdom.com/)"
// "user_agent": "ELB-HealthChecker/2.0"
const RE_USERAGENT_FILTER = new RegExp('(pingbot|elb-healthchecker)', 'i');

/**
 * Product instrumentation client that sends data to segment.io using
 * the Segment NODE SDK.
 *
 * @see {@link https://inspectionxpert.atlassian.net/wiki/spaces/IC/pages/81559558/Product+Instrumentation+Analytics}
 */
export default class Analytics {
  static getAnonymousId(params = {}) {
    try {
      const { sdk = window.analytics } = params;
      sdk.ready(() => {
        let id;
        if (sdk.user) {
          id = sdk.user().anonymousId();
        }
        //  console.log('sdk.user and window.analytics.user were null');
        return id;
      });
    } catch (error) {
      this.onError(error, 'getAnonymousId');
    }
  }

  /**
   * Sets metadata on the object provided by reference.
   *
   * @see {@link https://segment.com/docs/connections/spec/common/}
   *
   * @private
   * @param {Object} params
   */
  static constructOptions(options = {}) {
    const session_id = Cookies.get('session');
    const opts = {
      ...options,
      integrations: {
        Amplitude: {
          session_id: session_id || Date.now().valueOf(),
          httpOnly: true,
        },
      },
      context: {
        app: {
          name: 'IXC',
          namespace: 'com.inspectionxpert.app.frontend',
          // version: 'n.n.n',
          // build: '<GIT HASH HERE>',
        },
      },
    };
    if (config.package && config.package.version) opts.context.app.version = config.package.version;
    if (config.scm && config.scm.commit) opts.context.app.build = config.scm.commit;
    return opts;
  }

  /**
   * Links a user to a specific user id.
   * Note that backend/src/analytics/analytics.js also has an identify method.
   *
   * @param {Object} params
   */
  static identify(params) {
    if (Analytics.doNotTrack()) return;
    try {
      const { sdk = window.analytics, user, traits = {}, options = {}, callback = () => {} } = params;

      // If Identify has already been called sdk.user.userId will be set
      let currentUserId;
      if (sdk.user) {
        currentUserId = sdk.user().id();
      } else if (isNotTestEnv) {
        console.log('sdk.user is null. User as anonymous.');
      }

      // Analytics.identify NOT called because userId is already set
      if (currentUserId) return;

      const allOptions = this.constructOptions(options);

      // Identify hasn't been called yet so call identify with IX user.id
      if (user && user.id) {
        const allTraits = {
          ...traits,
          ...this.generateUserMetadata(user),
        };
        const { userId } = allTraits;
        if (isNotTestEnv) console.log('Analytics.identify', { userId, traits: allTraits, options: allOptions, callback });
        // Call segment's identify with userId
        sdk.identify(userId, allTraits, allOptions, callback);
      } else {
        console.log('Analytics.identify NOT called because IX user.id was not available');
      }

      // Segment may not relay data to Heap yet, so do so manually
      // HeapAdapter.identify({
      //   userId,
      //   userData: traits,
      //   // eventData: {},
      // });
    } catch (error) {
      this.onError(error, 'identify');
    }
  }

  /**
   * Links a user to a specific account or site id.
   * Note that backend/src/analytics/analytics.js also has an group method.
   *
   * @param {Object} params
   */
  static group(params) {
    if (Analytics.doNotTrack()) return;
    try {
      const { sdk = window.analytics, groupId, traits = {} } = params;

      if (!groupId) {
        throw new Error('Analytics.group failed because user or groupId was undefined');
      }

      if (isNotTestEnv) console.log('Analytics.group', { groupId, traits });
      // Call segment's group with groupId
      sdk.group(groupId, traits);
    } catch (error) {
      this.onError(error, 'group');
    }
  }

  /**
   * Emits a tracking event.
   * Note that `properties` data reliably makes it through to Amplitude, but
   * `options` data does not appear to.
   *
   * @param {Object} params
   */
  static track(params) {
    // If metrics are disabled for this user, don't send the event
    // if (!this.trackingEnabled(params)) return;
    if (Analytics.doNotTrack()) return;
    try {
      const { sdk = window.analytics, event = {}, eventNameSuffix = '', options = {}, callback = () => {} } = params;

      const allOptions = this.constructOptions(options);

      const eventName = `${event.name}${eventNameSuffix}`;

      // Extract event specific properties
      let { properties = {} } = params;
      const objectProperties = this.generateObjectMetadata(params);

      // Extract metadata that should be sent with all events
      const metadata = this.generateEventMetadata(params);

      // Combine all data into a single object
      properties = {
        ...properties,
        ...metadata,
        ...objectProperties,
      };

      if (isNotTestEnv) console.log('Analytics.track', { eventName, properties });
      // {String} event        A string that captures the name of the event that is being tracked
      // {Object} properties   An optional dictionary that tracks the properties of the event
      // {Object} options      An optional dictionary of information such as context, integrations, etc. Specific user traits can be provided as the context as well
      // {Function} callback   This function gets executed after successful execution of the track call
      sdk.track(eventName, properties, allOptions, callback);
    } catch (error) {
      this.onError(error, 'track');
    }
  }

  /**
   * Emits a page view event.
   *
   * @param {Object} params
   *
   * Note: Fired from frontend/src/view/shared/routes/RoutesComponent.jsx
   */
  static page(params) {
    // If metrics are disabled for this user, don't send the event
    // if (!this.trackingEnabled(params)) return;
    if (Analytics.doNotTrack()) return;
    try {
      const { sdk = window.analytics, options = {}, callback = () => {} } = params;

      const allOptions = this.constructOptions(options);

      const { category, name } = this.generatePageMetadata(params);

      let { properties = {} } = params;
      const metadata = this.generateEventMetadata(params);
      properties = {
        ...properties,
        ...metadata,
      };

      if (isNotTestEnv) console.log('Analytics.page', { url: params.url, category, name, properties });
      // {String} category     An optional string that defines the category of the page
      // {String} name         An optional string that defines the name of the page
      // {Object} properties   An optional dictionary that defines the properties of the page. These properties are auto-captured by the page
      // {Object} options      Properties associated with the context of the call such as IP, feature flags, etc
      // {Function} callback   This function gets executed after successful execution of the page() method.
      sdk.page(category, name, properties, allOptions, callback);
    } catch (error) {
      this.onError(error, 'page');
    }
  }

  /**
   * Clears the cookies and local storage set by Segment.io
   */
  static reset(params) {
    if (Analytics.doNotTrack()) return;
    try {
      params = params || {};
      const { sdk = window.analytics } = params;
      if (isNotTestEnv) console.log('Analytics.reset', params);
      sdk.reset();
    } catch (error) {
      this.onError(error, 'reset');
    }
  }

  /**
   * Generate metadata for Identify event, such as user and plan info.
   *
   * @private
   * @param {Object} params
   */
  static generateUserMetadata(user) {
    const metadata = {};

    if (!user) {
      throw new Error('generateUserMetadata failed because user was undefined');
    }

    // User details
    // See https://segment.com/docs/connections/spec/identify/
    if (user.id) metadata.userId = user.id;
    if (user.firstName) metadata.firstName = user.firstName;
    if (user.lastName) metadata.lastName = user.lastName;
    if (user.email) metadata.email = user.email;

    // Plan / Subscription
    // metadata.subscription = account.subscription;
    // metadata.isTrial = account.isTrial;
    if (user.paid) metadata.isPaid = user.paid;

    // Account / Site Details
    if (user.activeAccountMemberId) metadata.activeAccountMemberId = user.activeAccountMemberId;
    if (user.roles) metadata.roles = user.roles;

    if (user.accountId) {
      metadata.account = {
        id: user.accountId,
        name: user.accountName,
      };
    }

    if (user.siteId) {
      metadata.site = {
        id: user.siteId,
        name: user.siteName,
      };
    }

    if (user.subscription) {
      metadata.subscription = user.subscription;

      // "id": "7bb2c0f6-9bdb-46ac-8940-b0b5cfa1cf30",
      // "createdAt": "2021-04-28T21:11:41.000Z",
      // "trialEndedAt": "2021-05-12",
      // "trialStartedAt": "2021-04-28",
      // "activatedAt": null,
      // "currentPeriodStartedAt": "2021-04-28",
      // "currentPeriodEndsAt": "2021-05-12",
      // "nextAssessmentAt": "2021-05-12",
      // "renewalDate": "2022-05-12",

      // "status": "trialing",

      // "signupRevenue": "0.00",
      // "productPriceInCents": 0,

      // "balanceInCents": 0,
      // "currentBillingAmountInCents": 0,
      // "totalRevenueInCents": 0,

      // "billingPeriod": "Annually",
      // "paymentCollectionMethod": "remittance",
      // "paymentType": null,
      // "netTerms": null,

      // "productPricePointId": "1202911",
      // "productVersionNumber": "1",
      // "providerFamilyHandle": "inspectionxpert-cloud",
      // "providerProductHandle": "ix_standard",
      // "components": "[{\"component_id\":1241893,\"subscription_id\":41945510,\"component_handle\":\"internal_account_ss\",\"allocated_quantity\":1,\"name\":\"Internal Account\",\"kind\":\"quantity_based_component\",\"unit_name\":\"internal_account\",\"pricing_scheme\":\"per_unit\",\"price_point_id\":1248708,\"price_point_handle\":\"v20210101_annually\"}]",

      // "paidUserSeatsUsed": 1,
      // "partsUsed": 2,
      // "drawingsUsed": 2,
      // "measurementsUsed": null,

      // "expiresAt": null,
      // "delayedCancelAt": null,
      // "canceledAt": null,
      // "deletedAt": null,
      // "cancelAtEndOfPeriod": false,
      // "cancellationMessage": null,
      // "cancellationMethod": null,
      // "automaticallyResumeAt": null,
      // "reasonCode": null,
    }

    // log.debug('FE.analytics.generateUserMetadata.return', metadata);

    return metadata;
  }

  /**
   * Generate metadata for an event, such as user and plan info.
   * Note that this method reads app state but does not mutate it.
   *
   * @private
   * @param {Object} params
   */
  static generateEventMetadata(params) {
    const { state } = params;
    let metadata = {
      source: constants.EVENT_SOURCE_FRONTEND,
      eventVersion: params.event && params.event.version,
      sessionId: Cookies.get('session'),
    };

    // TODO: Make sure user exists on state
    if (!state || !state.user) {
      return metadata;
    }

    const { user } = state;
    const userMetadata = this.generateUserMetadata(user);

    metadata = {
      ...userMetadata,
      ...metadata,
    };

    return metadata;
  }

  /**
   * Returns the category of a page based on the url
   *
   * @private
   * @param {String} url - Required : The value of window.location.pathname
   * @param {String} category - Optional : The user specified category of the page
   * @param {String} name - Optional : The user specified title of the page
   */
  static generatePageMetadata(params) {
    let { url, category, name } = params;
    // Use the provided name and category, or calculate it
    const urlParts = url.split('/');
    if (!category) [, category, name] = urlParts;
    if (!name) name = url;
    // TODO: Catalog information architecture
    // if (url === '') {
    // }
    return {
      href: window.location.href,
      hostname: window.location.hostname,
      pathname: window.location.pathname,
      category,
      name,
    };
  }

  /**
   * Returns default attributes for key objects, such as Parts, Drawings, Characteristic
   *
   * @private
   * @param {object} params - The params passed to track or page
   */
  static generateObjectMetadata(params) {
    let properties = {};
    let { drawing, part } = params;
    const { error, characteristic, subscription, user, accountConfig, reviewCycle, marker, preset, job, operator, station, sample, measurement, report, template, drawingSheet, compareDrawing, compareCharacteristic, annotation } = params;

    // Standardize unpacking of common objects
    if (error) {
      properties = {
        ...properties,
        'error.message': error?.message || error?.errors,
        'error.name': error?.name,
        'error.fileName': error?.fileName,
        'error.lineNumber': error?.lineNumber,
        'error.columnNumber': error?.columnNumber,
        'error.stack': error?.stack,
        'error.string': error?.toString(),
        'error.status': error?.status,
      };
    }

    if (characteristic) {
      properties = {
        ...properties,
        'char.id': characteristic.id,
        'char.fullType': `${characteristic.notationType} : ${characteristic.notationSubtype}`,
        'char.type': characteristic.notationType,
        'char.subtype': characteristic.notationSubtype,
        'char.fullSpecification': characteristic.fullSpecification,
        'char.tolerances': characteristic.toleranceSource,
        'drawing.sheet': characteristic.sheet,
      };
      drawing = characteristic.drawing;
    }

    if (compareCharacteristic) {
      properties = {
        ...properties,
        'compareChar.id': compareCharacteristic.id,
        'compareChar.subtype': compareCharacteristic.notationSubtype,
        'compareChar.fullSpecification': compareCharacteristic.fullSpecification,
        'compareChar.tolerances': compareCharacteristic.toleranceSource,
      };
    }

    if (drawing) {
      properties = {
        ...properties,
        'drawing.id': drawing.id,
        'drawing.name': drawing.name,
        'drawing.revision': drawing.revision,
      };
      part = drawing.part;
    }

    if (compareDrawing) {
      properties = {
        ...properties,
        'compareDrawing.id': compareDrawing.id,
        'compareDrawing.name': compareDrawing.name,
        'compareDrawing.revision': compareDrawing.revision,
        'compareDrawing.file': compareDrawing.drawingFile,
      };
    }

    if (drawingSheet) {
      properties = {
        ...properties,
        'drawingSheet.id': drawingSheet.id,
        'drawingSheet.pageName': drawingSheet.pageName,
      };
    }

    if (part) {
      properties = {
        ...properties,
        'part.id': part.id,
        'part.number': part.number,
        'part.revision': part.revision,
        'part.accessControl': part.accessControl,
      };
    }

    if (accountConfig) {
      properties = {
        ...properties,
        'config.list': accountConfig.listType,
        'config.entry.id': accountConfig.id,
        'config.entry.name': accountConfig.name,
        'config.entry.metadata': accountConfig.meta,
        'config.entry.status': accountConfig.status,
      };
    }

    if (user) {
      properties = {
        ...properties,
        'currentUser.id': user.currentUser.id,
        'targetUser.id': user.targetUser.id,
        'targetUser.roles': user.targetUser.roles,
        'targetUser.accountMember.status': user.targetUser.status,
      };
    }

    if (subscription) {
      properties = {
        ...properties,
        'ixc.subscription.id': subscription.paymentSystemId,
        'chargify.subscription.id': subscription.providerProductId,
        'ixc.customer.id': subscription.accountId,
        'ixc.billing.id': subscription.billingId,
        'hubspot.company.id': subscription.companyId,
        'chargify.customer.id': subscription.customerId,
        'chargify.product.id': subscription.accessControl,
        'renewal.date': subscription.renewalDate,
      };
    }

    if (reviewCycle) {
      properties = {
        ...properties,
        'ixc.review.event': reviewCycle.event,
        'ixc.review.elapsed': reviewCycle.elapsed,
        'ixc.review.capture': reviewCycle.capture,
        'ixc.review.correction': reviewCycle.correction,
        'ixc.review.confidence': reviewCycle.confidence,
        'ixc.review.issue': reviewCycle.issue,
        'ixc.review.userId': reviewCycle.user.id,
        'ixc.review.userEmail': reviewCycle.user.email,
      };
    }

    if (marker) {
      properties = {
        ...properties,
        'marker.id': marker.id,
        'marker.shape': marker.shape,
        'marker.fillColor': marker.fillColor,
        'marker.borderColor': marker.borderColor,
        'marker.borderStyle': marker.borderStyle,
        'marker.fontColor': marker.fontColor,
      };
    }

    if (preset) {
      properties = {
        ...properties,
        'preset.name': preset.name,
        'preset.customers': preset.customers,
        'preset.styles': preset.assignedStyles,
      };
    }

    if (job) {
      properties = {
        ...properties,
        'job.id': job.id,
        'job.name': job.name,
        'job.accessControl': job.accessControl,
        'job.status': job.status,
      };
    }

    if (operator) {
      properties = {
        ...properties,
        'operator.id': operator.id,
        'operator.email': operator.email,
        'operator.name': operator.fullName,
      };
    }

    if (station) {
      properties = {
        ...properties,
        'station.id': station.id,
        'station.name': station.name,
      };
    }

    if (sample) {
      properties = {
        ...properties,
        'sample.id': sample.id,
        'sample.serial': sample.serial,
      };
    }

    if (measurement) {
      properties = {
        ...properties,
        'measurement.id': measurement.id,
        'measurement.value': measurement.value,
        'measurement.status': measurement.status,
      };
    }

    if (report) {
      properties = {
        ...properties,
        'report.id': report.id,
        'report.part': report.part?.id || report.part,
        'report.file': report.file[0]?.privateUrl,
        'report.customer': report.customer?.id || report.customer,
        'report.filters': report.filters,
        'report.template': report.templateUrl,
      };
    }

    if (template) {
      properties = {
        ...properties,
        'template.id': template.id,
        'template.file': template.file[0]?.privateUrl,
        'template.customer': template.provider?.id || template.provider,
      };
    }

    if (annotation) {
      properties = {
        ...properties,
        'annotation.id': annotation.id,
        'annotation.annotation': annotation.annotation,
        'annotation.characteristic': annotation.characteristic?.id || annotation.characteristic,
        'annotation.drawing': annotation.drawing?.id || annotation.drawing,
        'annotation.part': annotation.part?.id || annotation.part,
      };
    }

    return properties;
  }

  // Helper function that converts a series of event times to a JS object for analytics
  static perfLogToProperties(e) {
    const properties = {};
    let i;
    let eventName;
    let elapsedTime;
    let lastTime = 0;
    // let event: [number, string];
    for (i = 0; i < e.length - 1; i++) {
      [eventName, elapsedTime] = e[i];
      properties[`${eventName}.index`] = i;
      properties[`${eventName}.fromStart`] = elapsedTime;
      properties[`${eventName}.fromLast`] = elapsedTime - lastTime;
      lastTime = elapsedTime;
    }
    return properties;
  }

  /**
   * Helper function that checks if metrics are enabled for this user
   *
   * @private
   * @param {Object} params
   * @prop  {Object} state - The frontend state object that contains the user
   * @returns {Boolean}
   */
  // static trackingEnabled(params) {
  //   // If metrics are disabled for this user, don't send the event
  //   const { state } = params;
  //   if (FeatureFlags && FeatureFlags.isEnabled('emitSystemMetrics', state)) {
  //     if (isNotTestEnv) console.log('Analytics.FeatureFlag::emitSystemMetrics is TRUE');
  //     return true;
  //   }
  //   if (isNotTestEnv) console.log('Analytics.FeatureFlag::emitSystemMetrics is undefined or FALSE');
  //   return false;
  // }

  /**
   * Sets metadata on the object provided by reference.
   *
   * @see {@link https://segment.com/docs/connections/spec/common/}
   *
   * @private
   * @param {Object} params
   */
  static doNotTrack(params = {}) {
    const userAgent = params.userAgent || window.navigator.userAgent;
    return RE_USERAGENT_FILTER.test(userAgent);
  }

  /**
   * Handles a caught exception.
   *
   * @private
   * @param {Error} error - The error
   * @param {String} method - The name of the calling method
   */
  static onError(error, method) {
    if (isNotTestEnv) {
      console.error(`Analytics ${method} error`, error);
    }
  }
}

Analytics.events = events;
