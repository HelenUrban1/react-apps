import log from 'modules/shared/logger';

/**
 * Product instrumentation client that sends data to HEAP
 *
 * @see {@link https://inspectionxpert.atlassian.net/wiki/spaces/IC/pages/81559558/Product+Instrumentation+Analytics}
 */
export default class HeapAdapter {
  /**
   * Links a user to a specific user id.
   * Note that backend/src/analytics/analytics.js also has an identify method.
   *
   * @param {String} userId Globally unique ID of the user
   * @param {Object} userData Key values you wish to include in the analytics events
   */
  static identify(params) {
    try {
      const { userId, userData, eventData } = params;
      if (window.heap) {
        if (userId) {
          log.debug('Heap.identify.userId', userId);
          window.heap.identify(userId);
        }
        if (userData) {
          log.debug('Heap.set.userData', userData);
          window.heap.addUserProperties(userData);
        }
        if (eventData) {
          log.log('Heap.set.eventData', eventData);
          window.heap.addEventProperties(eventData);
        }
      }
    } catch (error) {
      this.onError(error, 'identify');
    }
  }

  /**
   * Handles a caught exception.
   *
   * @private
   * @param {Error} error - The error
   * @param {String} method - The name of the calling method
   */
  static onError(error, method) {
    if (process.env.NODE_ENV !== 'test') {
      log.error(`Heap Adapter ${method} error`, error);
    }
  }
}
