import Analytics from '../analytics/analytics';

/**
 * Loglevel plugin that intercepts log.error calls and sends data to segment.io
 *
 * @see {@link https://www.npmjs.com/package/loglevel}
 */

const segment = {
  apply: (logger) => {
    if (!logger || !logger.getLogger) {
      throw new TypeError('Argument is not a root loglevel object');
    }
    const originalFactory = logger.methodFactory;
    const pluginFactory = function remoteMethodFactory(methodName, logLevel, loggerName) {
      const rawMethod = originalFactory(methodName, logLevel, loggerName);
      return (...args) => {
        // Intercept log.error only
        if (methodName === 'error') {
          // Find the error object
          let errorObj;
          const comments = [];
          for (let i = 0; i < args.length; i++) {
            if (args[i]?.stack && args[i]?.message) {
              errorObj = args[i];
            } else {
              comments.push(args[i]);
            }
          }
          // Call the segment.io track method and pass the error object
          console.log('SENDING ERROR OBJECT TO SEGMENT.IO');
          Analytics.track({
            event: Analytics.events.errorsNonspecific,
            error: errorObj,
            properties: {
              source: 'loglevel plugin',
              comments,
            },
          });
        }
        // Bubble all other log calls to standard handler
        rawMethod(...args);
      };
    };
    logger.methodFactory = pluginFactory;
    logger.setLevel(logger.getLevel());
    return logger;
  },
};

export default segment;
