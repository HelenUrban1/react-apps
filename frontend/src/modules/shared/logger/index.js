import config from 'config';
import log from 'loglevel';
import remote from 'loglevel-plugin-remote';
import segment from './loglevel-plugin-segment';

// Apply the segment error interceptor plugin
segment.apply(log);

if (config.remoteLogging.isEnabled) {
  remote.apply(log, {
    format: remote.json,
    url: config.remoteLogging.url,
    level: config.remoteLogging.level,
    // token: '', // TODO: Add JWT
  });
  log.enableAll();
}

// log.trace('trace message');
// log.debug('debug message');
// log.info('info message');
// log.warn('warn message');
// log.error('error message');

export default log;
