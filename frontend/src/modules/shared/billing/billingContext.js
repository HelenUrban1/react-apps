import React, { useState } from 'react';
import { useQuery } from '@apollo/client';
import { parseValidJSONString } from 'utils/textOperations';
import billingApi from './billingApi';

const BillingContext = React.createContext([{}, () => {}]);

const BillingContextProvider = (props) => {
  const { accountId, isStillLoading } = props;

  const [state, setState] = useState({
    accountId,
    loading: true,
    succeeded: false,
    catalog: {},
  });

  useQuery(billingApi.query.getBillingInitData, {
    variables: {
      accountId,
    },
    onCompleted: (data) => {
      const catalog = parseValidJSONString(data?.subscriptionBillingInitData?.catalog);
      setState({
        ...state,
        ...data.subscriptionBillingInitData,
        catalog,
        loading: typeof isStillLoading === 'boolean' ? isStillLoading : false,
      });
    },
  });

  return <BillingContext.Provider value={[state, setState]}>{props.children}</BillingContext.Provider>;
};

export { BillingContext, BillingContextProvider };
