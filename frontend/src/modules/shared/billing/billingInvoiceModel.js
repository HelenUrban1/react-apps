import { i18n } from 'i18n';
import DecimalField from 'modules/shared/fields/decimalField';
import StringField from 'modules/shared/fields/stringField';
import DateField from 'modules/shared/fields/dateField';

function label(name) {
  return i18n(`entities.subscription.billing.invoice.fields.${name}`);
}

const fields = {
  id: new StringField('uid', label('download'), {}),
  created: new DateField('issue_date', label('created'), {}),
  items: new StringField('product_name', label('items'), {}),
  status: new StringField('status', label('status'), {}),
  totalAmount: new DecimalField('total_amount', label('totalAmount'), {}),
};

export default {
  fields,
};
