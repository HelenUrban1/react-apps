import gql from 'graphql-tag';

const BillingApi = {
  query: {
    getBillingInitData: gql`
      query SUBSCRIPTION_BILLING_INIT_DATA($accountId: String!) {
        subscriptionBillingInitData(accountId: $accountId) {
          publicKey
          siteUrl
          securityToken
          catalog
        }
      }
    `,
    getInvoices: gql`
      query SUBSCRIPTION_BILLING_INVOICES_LIST($customer: String) {
        subscriptionBillingInvoicesList(customer: $customer) {
          uid
          issue_date
          product_name
          status
          total_amount
        }
      }
    `,
    downloadInvoice: gql`
      query SUBSCRIPTION_BILLING_INVOICE_DOWNLOAD($id: String!) {
        subscriptionBillingInvoiceDownload(id: $id) {
          id
          base64Payload
        }
      }
    `,
    getSubscriptions: gql`
      query SUBSCRIPTION_LIST($customerId: String!) {
        subscriptionRecords(customerId: $customerId) {
          id
          state
          activated_at
          created_at
          updated_at
          balance_in_cents
          current_period_ends_at
          next_assessment_at
          canceled_at
          cancellation_message
          next_product_handle
          cancel_at_end_of_period
          payment_collection_method
          cancellation_method
          current_period_started_at
          previous_state
          signup_payment_id
          signup_revenue
          coupon_code
          total_revenue_in_cents
          product_price_in_cents
          product_version_number
          payment_type
          referral_code
          coupon_use_count
          coupon_uses_allowed
          receives_invoice_emails
          net_terms
          stored_credential_transaction_id
          currency
          on_hold_at
          product {
            id
            name
            handle
            description
            accounting_code
            request_credit_card
            expiration_interval_unit
            created_at
            updated_at
            price_in_cents
            interval
            interval_unit
            trial_interval_unit
            require_credit_card
            return_params
            update_return_url
            initial_charge_after_trial
            update_return_params
          }
          credit_card {
            id
            payment_type
            first_name
            last_name
            masked_card_number
            card_type
            expiration_month
            expiration_year
            billing_address
            billing_address_2
            billing_city
            billing_state
            billing_country
            billing_zip
            current_vault
            customer_id
          }
          payment_type
          referral_code
          coupon_use_count
          coupon_uses_allowed
          stored_credential_transaction_id
          next_product_handle
          currency
          on_hold_at
        }
      }
    `,
  },
  mutate: {
    createSubscription: gql`
      mutation SUBSCRIPTION_CREATE($data: SubscriptionInput!) {
        subscriptionCreate(data: $data) {
          id
        }
      }
    `,
    addPaymentMethod: gql`
      mutation SUBSCRIPTION_BILLING_PAYMENT_METHOD_CREATE($data: SubscriptionBillingPaymentMethodInput!) {
        subscriptionBillingPaymentMethodCreate(data: $data) {
          id
        }
      }
    `,
  },
};

export default BillingApi;
