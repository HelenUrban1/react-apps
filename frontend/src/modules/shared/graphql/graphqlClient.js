import { InMemoryCache, ApolloClient, gql, createHttpLink } from '@apollo/client';
import { setContext } from '@apollo/client/link/context';
import { RetryLink } from '@apollo/client/link/retry';
import config, { refreshEndpoint } from 'config';
import { getLanguageCode } from 'i18n';
import Analytics from 'modules/shared/analytics/analytics';
import localForage from 'localforage';
import fetch from 'cross-fetch';
import { onError } from '@apollo/client/link/error';
import { fromPromise } from '@apollo/client/link/utils';
import getStore from '../../store';
import axios from '../network/axios';

localForage.config({
  driver: localForage.INDEXEDDB,
  name: 'ixc',
});

const retryLink = new RetryLink({
  attempts: {
    max: 2,
  },
});

const authLink = setContext(async (_, { headers }) => {
  const bearer = getStore().getState().auth.jwt;

  return {
    headers: {
      ...headers,
      authorization: bearer ? `Bearer ${bearer}` : '',
      'Accept-Language': getLanguageCode(),
    },
  };
});

const analyticsLink = setContext(async (_, { headers }) => {
  return {
    headers: {
      ...headers,
      'x-ixc-anonymous-id': Analytics.getAnonymousId(),
    },
  };
});

const errorLink = onError(({ graphQLErrors, networkError, operation, forward }) => {
  if (!window.location.href.includes('/auth/') && ((networkError && networkError.response && networkError.response.status === 401) || (graphQLErrors && graphQLErrors.find((error) => error.code === 401)))) {
    return fromPromise(
      axios
        .post(refreshEndpoint)
        .then((accessToken) => accessToken)
        .catch((error) => {
          // TODO find a way to not need to mutate this directly
          getStore().getState().auth.jwt = null;
          Analytics.reset();
          localStorage.setItem('timedOut', 'true');
          window.location.href = '/auth/signin';
        }),
    )
      .filter((value) => Boolean(value))
      .flatMap((accessToken) => {
        const oldHeaders = operation.getContext().headers;
        // modify the operation context with a new token
        operation.setContext({
          headers: {
            ...oldHeaders,
            authorization: `Bearer ${accessToken}`,
          },
        });
        getStore().getState().auth.jwt = accessToken.data.authSignIn;
        // retry the request, returning the new observable
        return forward(operation);
      });
  }
  return null;
});

const httpLink = createHttpLink({
  uri: `${config.backendUrl}`,
  credentials: 'include',
  fetch,
});

const defaultOptions = {
  query: {
    // Leave cache enabled for IDB cache layer to work
    // fetchPolicy: 'no-cache',
    errorPolicy: 'all',
  },
};

const cache = new InMemoryCache({
  addTypename: false,
});

const graphqlClient = (async () => {
  // BUG: This call was breaking cypress/app/global/signup.spec.js
  // BUG DETAILS: frontend/src/modules/auth/authActions.js > doRegisterAccountAndUser line 144 was returning the wrong user from the cache
  // await persistCache({
  //   cache,
  //   storage: localForage,
  // });

  const client = new ApolloClient({
    link: retryLink.concat(analyticsLink.concat(errorLink.concat(authLink.concat(httpLink)))),
    cache,
    defaultOptions,
  });

  client.writeFragment({
    id: 'partStep',
    fragment: gql`
      fragment PartSteps on Part {
        steps {
          currentStep
          maxStep
        }
      }
    `,
    data: {
      currentStep: 0,
      maxStep: 0,
    },
  });

  return client;
})();

const getClient = async () => {
  return graphqlClient;
};

export default getClient;
