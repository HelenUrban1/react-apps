// import { refreshEndpoint } from 'config';
// import createAuthRefreshInterceptor from 'axios-auth-refresh';
// import getStore from '../../store';
import axios from 'axios';
import axiosRetry from 'axios-retry';

const axiosExport = axios.create();
// const refreshLogic = (failedRequest) => {
//   const { deviceKey } = getStore().getState().auth;
//   const req = axios.post(refreshEndpoint, { deviceKey });
//   return req.then((tokenRefreshResponse) => {
//     if (tokenRefreshResponse?.data) {
//       getStore().getState().auth.jwt = tokenRefreshResponse.data.authSignIn;
//       failedRequest.response.config.headers.authorization = `Bearer ${tokenRefreshResponse.data.authSignIn}`;
//       failedRequest.response.config.data = JSON.parse(failedRequest.response.config.data);
//       return Promise.resolve();
//     }
//     return Promise.reject();
//   });
// };
// createAuthRefreshInterceptor(axios, refreshLogic, { pauseInstanceWhileRefreshing: true });
axiosExport.defaults.withCredentials = true;

// Handle flakey networks with retry logic
// https://github.com/softonic/axios-retry/issues/87

const retryDelay = (retryNumber = 0) => {
  const seconds = 2 ** retryNumber * 1000;
  const randomMs = 1000 * Math.random();
  return seconds + randomMs;
};

axiosRetry(axiosExport, {
  retries: 2,
  retryDelay,
  // retry on Network Error & 5xx responses
  retryCondition: axiosRetry.isRetryableError,
});

export default axiosExport;
