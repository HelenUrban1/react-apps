import Message from 'view/shared/message';
import Analytics from 'modules/shared/analytics/analytics';
import { getHistory } from 'modules/store';
import { i18n } from 'i18n';
import log from 'modules/shared/logger';

const DEFAULT_ERROR_MESSAGE = i18n('errors.defaultErrorMessage');

function selectErrorMessage(error) {
  if (error?.graphQLErrors?.length && error.graphQLErrors[0].message) {
    return error.graphQLErrors[0].message;
  }

  if (error?.networkError?.result?.errors?.length && error.networkError.result.errors[0].message) {
    return error.networkError.result.errors[0].message;
  }

  if (error?.response?.data?.error && (error.response.data.error.name || error.response.data.error.message)) {
    return error.response.data.error.message || i18n(`errors.names.${error.response.data.error.name}`);
  }

  if (error?.response?.data && (typeof error.response.data === 'string' || error.response.data instanceof String)) {
    return error.response.data;
  }

  return error.message || DEFAULT_ERROR_MESSAGE;
}

function selectErrorCode(error) {
  if (error && error.networkError) {
    if (error.networkError.result && error.networkError.result.errors && error.networkError.result.errors.length && error.networkError.result.errors[0].code) {
      return Number(error.networkError.result.errors[0].code);
    }

    if (error.networkError.statusCode) {
      return Number(error.networkError.statusCode);
    }
  }

  if (error && error.graphQLErrors && error.graphQLErrors.length) {
    return 400;
  }

  if (error && error.response && error.response.status) {
    return error.response.status;
  }

  return 500;
}

export default class Errors {
  static handle(error) {
    if (process.env.NODE_ENV !== 'test') {
      log.error(selectErrorMessage(error));
      log.error(error);
    }

    Analytics.track({
      event: Analytics.events.errorsNonspecific,
      error,
      properties: {
        'message.selected': selectErrorMessage(error),
        source: 'errors.js',
      },
    });

    if (selectErrorCode(error) === 403) {
      getHistory().push('/403');
      return;
    }

    if (selectErrorCode(error) === 400) {
      Message.error(selectErrorMessage(error));
      return;
    }

    getHistory().push('/500');
  }

  static errorCode(error) {
    return selectErrorCode(error);
  }

  static selectMessage(error) {
    return selectErrorMessage(error);
  }

  static showMessage(error) {
    Message.error(selectErrorMessage(error));
  }
}
