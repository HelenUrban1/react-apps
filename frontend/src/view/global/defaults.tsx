import { SettingPresets } from 'domain/setting/settingTypes';
import { ReportTemplate } from 'types/reportTemplates';
import { Organization } from 'graphql/organization';
import { i18n } from 'i18n';
import { Tolerance } from 'types/tolerances';
import { CharacteristicInput } from 'graphql/characteristics';

export const inspectionMethods = [
  //
  'Air Gage',
  'Caliper',
  'Misc',
  'Indicator',
  'Pin Micrometer',
  'Micrometer',
  'Gage Pins',
  'CMM',
  'CO-Gage',
  'SG Gage',
  'Bore Gage',
  'Drop Indicator',
  'Gage Blocks',
  'Gage Kit',
  'Cone Micrometer',
  'Flange Micrometer',
  'Groove Micrometer',
  'Pitch Micrometer',
  'Pressure Micrometer',
  'V-ANVIL MIC',
  'Anvil Micrometer',
  'Borematic',
  'Surface Plate',
  'Radius Gage',
  'Pick / Plate',
  'Bench Centers',
  'Bench Micrometer',
  'Height Gage',
  'Force Tester',
  'Concentricity Gage',
  'Depth Micrometer',
  'Diatest',
  'Flatness Gage',
  'Multimeter',
  'Refractometer',
  'Johnson Gage',
  'Scope',
  'Sine Plate',
  'Profilometer',
  'Ring Gage',
  'VCMM',
  'Sunnen',
  'Super Micrometer',
  'Laser Micrometer',
  'Torque Wrench',
  'V-Block',
  'Blade Micrometer',
  'Air Gaging',
  'Thread Plug',
  'Surface Tester',
  'Gage Ball',
  'Visual',
  'Thread Ring',
  'Set Plug',
];

export const operations = [
  //
  'Deburring',
  'Drilling',
  'Punching',
  'Bending',
  'Milling',
  'Cutting',
  'Welding',
  'Anodizing',
  'Turning',
  'Grinding',
  'Chip Formation',
  'Reaming',
  'Boring',
];

export interface MethodType {
  value: string;
  label?: string;
  measurement?: string;
  children?: MethodType[];
}

export interface DimensionTypes {
  value: string;
  measurement?: string;
  children?: DimensionTypes[];
}

export const types: DimensionTypes[] = [
  {
    value: 'Dimension',
    children: [
      { value: 'Length', measurement: 'Length' },
      { value: 'Angle', measurement: 'Plane Angle' },
      { value: 'Radius', measurement: 'Length' },
      { value: 'Diameter', measurement: 'Length' },
      { value: 'Curvilinear', measurement: 'Length' },
      { value: 'Chamfer Size', measurement: 'Length' },
      { value: 'Chamfer Angle', measurement: 'Plane Angle' },
      { value: 'Bend Radius', measurement: 'Length' },
      { value: 'Counterbore Depth', measurement: 'Length' },
      { value: 'Counterbore Diameter', measurement: 'Length' },
      { value: 'Counterbore Angle', measurement: 'Plane Angle' },
      { value: 'Countersink Diameter', measurement: 'Length' },
      { value: 'Countersink Angle', measurement: 'Plane Angle' },
      { value: 'Depth', measurement: 'Length' },
      { value: 'Edge Radius', measurement: 'Length' },
      { value: 'Thickness', measurement: 'Length' },
      { value: 'Edge Burr Size', measurement: 'Length' },
      { value: 'Edge Undercut Size', measurement: 'Length' },
      { value: 'Edge Passing Size', measurement: 'Length' },
      { value: 'Spherical Radius', measurement: 'Length' },
      { value: 'Spherical Diameter', measurement: 'Length' },
    ],
  },
  {
    value: 'Geometric Tolerance',
    children: [
      { value: 'Profile of a Surface', measurement: 'Length' },
      { value: 'Position', measurement: 'Length' },
      { value: 'Flatness', measurement: 'Length' },
      { value: 'Perpendicularity', measurement: 'Length' },
      { value: 'Angularity', measurement: 'Plane Angle' },
      { value: 'Circularity', measurement: 'Length' },
      { value: 'Runout', measurement: 'Length' },
      { value: 'Total Runout', measurement: 'Length' },
      { value: 'Profile of a Line', measurement: 'Length' },
      { value: 'Cylindricity', measurement: 'Length' },
      { value: 'Concentricity', measurement: 'Length' },
      { value: 'Symmetry', measurement: 'Length' },
      { value: 'Parallelism', measurement: 'Length' },
      { value: 'Straightness', measurement: 'Length' },
    ],
  },
  {
    value: 'Surface Finish',
    children: [
      //
      { value: 'Roughness', measurement: 'Length' },
      { value: 'Lay', measurement: 'Length' },
      { value: 'Waviness', measurement: 'Length' },
      { value: 'Structure', measurement: 'Length' },
      { value: 'Material Removal Allowance', measurement: 'Length' },
      { value: 'Sampling Length', measurement: 'Length' },
      { value: 'Roughness Grade' },
    ],
  },
  {
    value: 'Weld',
    children: [
      //
      { value: 'Size', measurement: 'Length' },
      { value: 'Convexity' },
      { value: 'Reinforcement' },
      { value: 'Additional Characteristic' },
      { value: 'Bead Contour' },
      { value: 'Depth of Bevel', measurement: 'Length' },
      { value: 'Penetration' },
      { value: 'Length', measurement: 'Length' },
      { value: 'Location' },
      { value: 'Pitch', measurement: 'Length' },
      { value: 'Root Opening', measurement: 'Length' },
    ],
  },
  {
    value: 'Note',
    children: [{ value: 'Note' }, { value: 'Flag Note' }],
  },
  {
    value: 'Other',
    children: [
      { value: 'Thread', measurement: 'Length' },
      { value: 'Temperature', measurement: 'Temperature' },
      { value: 'Time' },
      { value: 'Torque' },
      { value: 'Voltage' },
      { value: 'Temper Conductivity' },
      { value: 'Temper Verification' },
      { value: 'Sound' },
      { value: 'Frequency', measurement: 'Frequency' },
      { value: 'Capacitance', measurement: 'Electric Capacitance' },
      { value: 'Electric Current', measurement: 'Electric Current' },
      { value: 'Resistance', measurement: 'Electric Resistance' },
      { value: 'Electric Charge' },
      { value: 'Inductance', measurement: 'Inductance' },
      { value: 'Apparent Power', measurement: 'Power' },
      { value: 'Reactive Power', measurement: 'Power' },
      { value: 'Electric Power', measurement: 'Power' },
      { value: 'Conductance' },
      { value: 'Admittance' },
      { value: 'Electric Flux' },
      { value: 'Magnetic Field' },
      { value: 'Magnetic Flux', measurement: 'Magnetic Flux' },
      { value: 'Area' },
      { value: 'Resistivity' },
      { value: 'Conductivity' },
      { value: 'Electric Field' },
      { value: 'Datum Target' },
      { value: 'Production Method' },
      { value: 'Annotation Table' },
      { value: 'Table' },
      { value: 'Cell' },
      { value: 'Callout' },
      { value: 'Datum' },
      { value: 'Balloon' },
    ],
  },
];

export interface ClassificationType {
  value: string;
  label: string;
}

export const classifications: ClassificationType[] = [
  { value: 'Critical', label: 'Critical' },
  { value: 'Major', label: 'Major' },
  { value: 'Minor', label: 'Minor' },
  { value: 'Incidental', label: 'Incidental' },
];

interface Colors {
  Purple: string;
  Coral: string;
  Sunshine: string;
  SunshineP1: string;
  Seafoam: string;
  SeafoamP1: string;
  Black: string;
  White: string;
  Transparent: string;
  [key: string]: string;
}
export const BrandColors: Colors = {
  Purple: '#963CBD',
  Coral: '#E56A54',
  Sunshine: '#EFBE7D',
  SunshineP1: '#e99c4f',
  Seafoam: '#69DBC8',
  SeafoamP1: '#029d93',
  Black: '#000000',
  White: '#FFFFFF',
  Transparent: 'rgba(0,0,0,0)',
  Azure: '#1682FF',
};

export const defaultUnitIDs = {
  degree: 'e1150ac6-6803-41fa-b525-9b8aba2eccb2',
  inch: 'b909e87e-0445-4ea4-857e-3c1dd31b530b',
  millimeter: '45976189-a841-4959-97e6-1dfe35da78e4',
};

export const DefaultStyles = {
  default: {
    style: 'f12c5566-7612-48e9-9534-ef90b276ebaa',
    assign: ['Default'],
  },
};

export const DefaultStylePresets: SettingPresets = {
  default: {
    customers: [],
    setting: DefaultStyles,
  },
};

export const DefaultTolerances: Tolerance = {
  linear: {
    type: 'PrecisionTolerance',
    rangeTols: [
      { rangeLow: '1', rangeHigh: '9', plus: '1', minus: '-1' },
      { rangeLow: '0.1', rangeHigh: '0.9', plus: '0.1', minus: '-0.1' },
      { rangeLow: '0.01', rangeHigh: '0.09', plus: '0.01', minus: '-0.01' },
      { rangeLow: '0.001', rangeHigh: '0.009', plus: '0.001', minus: '-0.001' },
      { rangeLow: '0.0001', rangeHigh: '0.0009', plus: '0.0001', minus: '-0.0001' },
    ],
    precisionTols: [
      { level: 'X', plus: '1', minus: '-1' },
      { level: 'X.X', plus: '0.1', minus: '-0.1' },
      { level: 'X.XX', plus: '0.01', minus: '-0.01' },
      { level: 'X.XXX', plus: '0.001', minus: '-0.001' },
      { level: 'X.XXXX', plus: '0.0001', minus: '-0.0001' },
    ],
    unit: defaultUnitIDs.inch,
  },
  angular: {
    type: 'PrecisionTolerance',
    rangeTols: [
      { rangeLow: '1', rangeHigh: '9', plus: '1', minus: '-1' },
      { rangeLow: '0.1', rangeHigh: '0.9', plus: '0.1', minus: '-0.1' },
      { rangeLow: '0.01', rangeHigh: '0.09', plus: '0.01', minus: '-0.01' },
      { rangeLow: '0.001', rangeHigh: '0.009', plus: '0.001', minus: '-0.001' },
      { rangeLow: '0.0001', rangeHigh: '0.0009', plus: '0.0001', minus: '-0.0001' },
    ],
    precisionTols: [
      { level: 'X', plus: '1', minus: '-1' },
      { level: 'X.X', plus: '0.1', minus: '-0.1' },
      { level: 'X.XX', plus: '0.01', minus: '-0.01' },
      { level: 'X.XXX', plus: '0.001', minus: '-0.001' },
      { level: 'X.XXXX', plus: '0.0001', minus: '-0.0001' },
    ],
    unit: defaultUnitIDs.degree,
  },
};

export const DefaultTolerancePresets: SettingPresets = {
  default: {
    customers: [],
    setting: DefaultTolerances,
  },
};

// GD&T Type Symbols

export interface GdtType {
  char: string;
  char_sml: string;
  framed_char: string;
  name: string;
}

export interface GdtTypes {
  Straightness: GdtType;
  Flatness: GdtType;
  Circularity: GdtType;
  Cylindricity: GdtType;
  Profile_of_a_Line: GdtType;
  Profile_of_a_Surface: GdtType;
  Perpendicularity: GdtType;
  Angularity: GdtType;
  Parallelism: GdtType;
  Position: GdtType;
  Concentricity: GdtType;
  Symmetry: GdtType;
  Runout: GdtType;
  Total_Runout: GdtType;
  [key: string]: GdtType;
}

export const GdtSymbols: { [key: string]: GdtType } = {
  straightness: { char: '', char_sml: '⏤', framed_char: '', name: 'Straightness' },
  flatness: { char: '', char_sml: '⏥', framed_char: '', name: 'Flatness' },
  circularity: { char: '', char_sml: '○', framed_char: '', name: 'Circularity' },
  cylindricity: { char: '', char_sml: '⌭', framed_char: '', name: 'Cylindricity' },
  profile_of_a_line: { char: '', char_sml: '⌒', framed_char: '', name: 'Profile of a Line' },
  profile_of_a_surface: {
    char: '',
    char_sml: '⌓',
    framed_char: '',
    name: 'Profile of a Surface',
  },
  perpendicularity: { char: '', char_sml: '⊥', framed_char: '', name: 'Perpendicularity' },
  angularity: { char: '', char_sml: '∠', framed_char: '', name: 'Angularity' },
  parallelism: { char: '', char_sml: '⑊', framed_char: '', name: 'Parallelism' },
  position: { char: '', char_sml: '⌖', framed_char: '', name: 'Position' },
  concentricity: { char: '', char_sml: '⌾', framed_char: '', name: 'Concentricity' },
  symmetry: { char: '', char_sml: '⌯', framed_char: '', name: 'Symmetry' },
  runout: { char: '', char_sml: '↗', framed_char: '', name: 'Runout' },
  total_runout: { char: '', char_sml: '⌰', framed_char: '', name: 'Total Runout' },
};

// Report Templates

export const defaultProvider: Organization = {
  id: 'none',
  status: 'Active',
  type: 'Account',
  name: 'Ideagen',
  parts: [],
  reports: [],
  templates: [],
  orgLogo: [{ id: 'none', name: 'logo', publicUrl: '/images/ID_Quality Control.svg' }],
};
export const defaultTemplates: ReportTemplate[] = [
  {
    id: 'default_fai',
    status: 'Active',
    title: 'All Data Template',
    type: 'FAI',
    direction: 'Vertical',
    settings:
      '{"saved": true,"direction":"Vertical", "footers":{"0":{ "B12":{"row":"11", "col":"1", "footer":"36"},"C12":{"row":"11", "col":"2", "footer":"36"},"D12":{"row":"11", "col":"3", "footer":"36"},"E12":{"row":"11", "col":"4", "footer":"36"},"F12":{"row":"11", "col":"5", "footer":"36"},"G12":{"row":"11", "col":"6", "footer":"36"},"H12":{"row":"11", "col":"7", "footer":"36"},"I12":{"row":"11", "col":"8", "footer":"36"},"J12":{"row":"11", "col":"9", "footer":"36"},"K12":{"row":"11", "col":"10", "footer":"36"},"L12":{"row":"11", "col":"11", "footer":"36"},"M12":{"row":"11", "col":"12", "footer":"36"},"N12":{"row":"11", "col":"13", "footer":"36"},"O12":{"row":"11", "col":"14", "footer":"36"},"P12":{"row":"11", "col":"15", "footer":"36"},"Q12":{"row":"11", "col":"16", "footer":"36"},"R12":{"row":"11", "col":"17", "footer":"36"},"S12":{"row":"11", "col":"18", "footer":"36"},"T12":{"row":"11", "col":"19", "footer":"36"},"U12":{"row":"11", "col":"20", "footer":"36"},"V12":{"row":"11", "col":"21", "footer":"36"}}}}',
    file: [
      {
        id: 'none',
        name: 'All Data',
        publicUrl: '/templates/All Data.xlsx',
      },
    ],
    provider: defaultProvider,
  },
  {
    id: 'default_as9102b',
    status: 'Active',
    title: 'AS9102B Template',
    type: 'FAI',
    direction: 'Vertical',
    settings: '{"saved": true, "direction":"Vertical", "footers":{"2":{"B11":{"row":"10","col":"1","footer":"25"}, "C11":{"row":"10","col":"2","footer":"25"}, "E11":{"row":"10","col":"4","footer":"25"}, "I11":{"row":"10","col":"8","footer":"25"}}}}',
    file: [
      {
        id: 'none',
        name: 'AS9102B',
        publicUrl: '/templates/AS9102B.xlsx',
      },
    ],
    provider: defaultProvider,
  },
  {
    id: 'default_as9102c',
    status: 'Active',
    title: 'AS9102C Template',
    type: 'FAI',
    direction: 'Vertical',
    settings: '{"saved":true,"footers":{"2":{"B10":{"row":9,"col":1,"footer":"25"},"C10":{"row":9,"col":2,"footer":"25"},"D10":{"row":9,"col":3,"footer":"25"},"E10":{"row":9,"col":4,"footer":"25"},"G10":{"row":9,"col":6,"footer":"25"},"I10":{"row":9,"col":8,"footer":"25"}}},"tokens":["{{Feature.Classification}}","{{Feature.Label}}","{{Feature.Location}}","{{Feature.Full_Spec}}","{{Feature.Method}}","{{Feature.Comment}}"]}',
    file: [
      {
        id: 'none',
        name: 'AS9102C',
        publicUrl: '/templates/AS9102C.xlsx',
      },
    ],
    provider: defaultProvider,
  },
  {
    id: 'default_ppap',
    status: 'Active',
    title: 'PPAP Dimensional Template',
    type: 'PPAP',
    direction: 'Vertical',
    settings:
      '{"saved": true, "direction":"Vertical", "footers":{"0":{"A6":{"row":"5","col":"0","footer":"6"}, "B6":{"row":"5","col":"1","footer":"6"}, "D6":{"row":"5","col":"3","footer":"6"}, "E6":{"row":"5","col":"4","footer":"6"}, "F6":{"row":"5","col":"5","footer":"6"}}}}',
    file: [
      {
        id: 'none',
        name: 'PPAP Dimensional',
        publicUrl: '/templates/PPAP Dimensional.xlsx',
      },
    ],
    provider: defaultProvider,
  },
];

export const defaultHash: { [key: string]: { settings: string; template: ReportTemplate } } = {
  '/templates/All Data.xlsx': {
    template: defaultTemplates[0],
    settings:
      '{"saved": true,"direction":"Vertical", "footers":{"0":{ "B12":{"row":"11", "col":"1", "footer":"36"},"C12":{"row":"11", "col":"2", "footer":"36"},"D12":{"row":"11", "col":"3", "footer":"36"},"E12":{"row":"11", "col":"4", "footer":"36"},"F12":{"row":"11", "col":"5", "footer":"36"},"G12":{"row":"11", "col":"6", "footer":"36"},"H12":{"row":"11", "col":"7", "footer":"36"},"I12":{"row":"11", "col":"8", "footer":"36"},"J12":{"row":"11", "col":"9", "footer":"36"},"K12":{"row":"11", "col":"10", "footer":"36"},"L12":{"row":"11", "col":"11", "footer":"36"},"M12":{"row":"11", "col":"12", "footer":"36"},"N12":{"row":"11", "col":"13", "footer":"36"},"O12":{"row":"11", "col":"14", "footer":"36"},"P12":{"row":"11", "col":"15", "footer":"36"},"Q12":{"row":"11", "col":"16", "footer":"36"},"R12":{"row":"11", "col":"17", "footer":"36"},"S12":{"row":"11", "col":"18", "footer":"36"},"T12":{"row":"11", "col":"19", "footer":"36"},"U12":{"row":"11", "col":"20", "footer":"36"},"V12":{"row":"11", "col":"21", "footer":"36"}}}}',
  },
  '/templates/AS9102B.xlsx': {
    template: defaultTemplates[1],
    settings: '{"saved": true, "direction":"Vertical", "footers":{"2":{"B11":{"row":"10","col":"1","footer":"25"}, "C11":{"row":"10","col":"2","footer":"25"}, "E11":{"row":"10","col":"4","footer":"25"}, "I11":{"row":"10","col":"8","footer":"25"}}}}',
  },
  '/templates/PPAP Dimensional.xlsx': {
    template: defaultTemplates[2],
    settings:
      '{"saved": true, "direction":"Vertical", "footers":{"0":{"A6":{"row":"5","col":"0","footer":"6"}, "B6":{"row":"5","col":"1","footer":"6"}, "D6":{"row":"5","col":"3","footer":"6"}, "E6":{"row":"5","col":"4","footer":"6"}, "F6":{"row":"5","col":"5","footer":"6"}}}}',
  },
};

// Template Tokens

export const defaultTokens = {
  partInfo: {
    group: 'Part_info',
    name: i18n('entities.tokens.part.group'),
    children: [
      {
        group: 'Part_Info',
        name: i18n('entities.tokens.part.name'),
        token: '{{Part.Name}}',
        example: 'MULTI-TOOL',
        type: 'Single',
      },
      {
        group: 'Part_Info',
        name: i18n('entities.tokens.part.number'),
        token: '{{Part.Number}}',
        example: 'PRT-80001',
        type: 'Single',
      },
      {
        group: 'Part_Info',
        name: i18n('entities.tokens.part.revision'),
        token: '{{Part.Revision}}',
        example: 'A.1',
        type: 'Single',
      },
      {
        group: 'Part_Info',
        name: i18n('entities.tokens.drawing.name'),
        token: '{{Primary.Name}}',
        example: 'MULTI-TOOL',
        type: 'Single',
      },
      {
        group: 'Part_Info',
        name: i18n('entities.tokens.drawing.number'),
        token: '{{Primary.Number}}',
        example: 'PRT-80001',
        type: 'Single',
      },
      {
        group: 'Part_Info',
        name: i18n('entities.tokens.drawing.revision'),
        token: '{{Primary.Revision}}',
        example: 'A.1',
        type: 'Single',
      },
      {
        group: 'Part_Info',
        name: i18n('entities.tokens.drawing.drawingAll'),
        token: '{{Drawings}}',
        example: 'MULTI-TOOL PRT-80001 A.1, MULTI-TOOL PRT-80002 A.2',
        type: 'Single',
      },
      {
        group: 'Part_Info',
        name: i18n('entities.tokens.part.customer'),
        token: '{{Customer.Name}}',
        example: 'Wayne Enterprises',
        type: 'Single',
      },
      {
        group: 'Part_Info',
        name: i18n('entities.tokens.drawing.control'),
        token: '{{Primary.Control}}',
        example: 'ITAR/EAR',
        type: 'Single',
      },
      {
        group: 'Part_Info',
        name: i18n('entities.tokens.part.status'),
        token: '{{Part.Status}}',
        example: 'New',
        type: 'Single',
      },
      {
        group: 'Part_Info',
        name: i18n('entities.tokens.part.updated'),
        token: '{{Part.Updated}}',
        example: '2020-06-03',
        type: 'Single',
      },
    ],
  },
  features: {
    group: 'Features',
    name: i18n('entities.tokens.features.group'),
    children: [
      {
        group: 'Features',
        name: i18n('entities.tokens.features.number'),
        token: '{{Feature.Label}}',
        example: '42',
        type: 'Dynamic',
      },
      {
        group: 'Features',
        name: i18n('entities.tokens.features.location'),
        token: '{{Feature.Location}}',
        example: 'MULTI-TOOL, pg. 2, Feature: B.2, Balloon: B.3, Connection Point: B.4',
        type: 'Dynamic',
      },
      {
        group: 'Features',
        name: i18n('entities.tokens.features.zone'),
        token: '{{Feature.Zone}}',
        example: 'B.2',
        type: 'Dynamic',
      },
      {
        group: 'Features',
        name: i18n('entities.tokens.features.balloonZone'),
        token: '{{Feature.Balloon_Zone}}',
        example: 'B.2',
        type: 'Dynamic',
      },
      {
        group: 'Features',
        name: i18n('entities.tokens.features.connectionPointZone'),
        token: '{{Feature.Connection_Point_Zone}}',
        example: 'B.2',
        type: 'Dynamic',
      },
      {
        group: 'Features',
        name: i18n('entities.tokens.features.page'),
        token: '{{Feature.Page}}',
        example: 'pg. 2',
        type: 'Dynamic',
      },
      {
        group: 'Features',
        name: i18n('entities.tokens.features.type'),
        token: '{{Feature.Type}}',
        example: 'Dimension',
        type: 'Dynamic',
      },
      {
        group: 'Features',
        name: i18n('entities.tokens.features.subtype'),
        token: '{{Feature.Subtype}}',
        example: 'Length',
        type: 'Dynamic',
      },
      {
        group: 'Features',
        name: i18n('entities.tokens.features.quantity'),
        token: '{{Feature.Quantity}}',
        example: '3',
        type: 'Dynamic',
      },
      {
        group: 'Features',
        name: i18n('entities.tokens.features.unit'),
        token: '{{Feature.Unit}}',
        example: 'mm',
        type: 'Dynamic',
      },
      {
        group: 'Features',
        name: i18n('entities.tokens.features.fullSpec'),
        token: '{{Feature.Full_Spec}}',
        example: '.325±.020',
        type: 'Dynamic',
      },
      {
        group: 'Features',
        name: i18n('entities.tokens.features.tolType'),
        token: '{{Feature.Tol_Type}}',
        example: '"Basic", "Tolerance", "Reference"',
        type: 'Dynamic',
      },
      {
        group: 'Features',
        name: i18n('entities.tokens.features.nominal'),
        token: '{{Feature.Nominal}}',
        example: '0.325',
        type: 'Dynamic',
      },
      {
        group: 'Features',
        name: i18n('entities.tokens.features.plusTol'),
        token: '{{Feature.Plus}}',
        example: '0.02',
        type: 'Dynamic',
      },
      {
        group: 'Features',
        name: i18n('entities.tokens.features.minusTol'),
        token: '{{Feature.Minus}}',
        example: '0.02',
        type: 'Dynamic',
      },
      {
        group: 'Features',
        name: i18n('entities.tokens.features.usl'),
        token: '{{Feature.USL}}',
        example: '0.345',
        type: 'Dynamic',
      },
      {
        group: 'Features',
        name: i18n('entities.tokens.features.lsl'),
        token: '{{Feature.LSL}}',
        example: '0.305',
        type: 'Dynamic',
      },
      {
        group: 'Items',
        name: i18n('entities.tokens.features.tolSource'),
        token: '{{Feature.Source}}',
        example: 'Document_Defined',
        type: 'Dynamic',
      },
      {
        group: 'Features',
        name: i18n('entities.tokens.features.classification'),
        token: '{{Feature.Classification}}',
        example: 'Critical',
        type: 'Dynamic',
      },
      {
        group: 'Features',
        name: i18n('entities.tokens.features.operation'),
        token: '{{Feature.Operation}}',
        example: 'Milling',
        type: 'Dynamic',
      },
      {
        group: 'Features',
        name: i18n('entities.tokens.features.method'),
        token: '{{Feature.Method}}',
        example: 'CMM',
        type: 'Dynamic',
      },
      {
        group: 'Features',
        name: i18n('entities.tokens.features.comment'),
        token: '{{Feature.Comment}}',
        example: 'Use Caliper C530',
        type: 'Dynamic',
      },
      {
        group: 'Features',
        name: i18n('entities.tokens.features.verification'),
        token: '{{Feature.Verification}}',
        example: 'Verified',
        type: 'Dynamic',
      },
    ],
  },
};

// Color map used for station and user profile selection
export const profileColorMap = [
  //
  '#C08AD7',
  '#685BC7',
  '#2E008B',
  '#755C9C',
  '#E56A54',
  '#E99C4F',
  '#00BBB0',
  '#8E9FBC',
  '#479AF9',
  '#0D6FE0',
];

// Subscription defaults

export const features = [
  //
  'Balloon & digitize PDF part drawings',
  'Publish inspection reports to Excel',
  'Create custom report templates',
  'Unlimited drawings & reports',
  'Unlimited reviewers',
  'ITAR compliant security controls',
  'Free training & support',
];

export const plans = [
  {
    name: i18n('entities.subscription.tier.ix_standard'),
    id: '5233891',
    cost: '$1,500',
    features: [
      //
      'Balloon & digitize PDF part drawings',
      'Publish inspection reports to Excel',
      'Create custom report templates',
      'Unlimited drawings & reports',
      'Unlimited reviewers',
      'ITAR compliant security controls',
      'Free training & support',
    ],
  },
  // {
  //   name: 'Advanced',
  //   id: '1234',
  //   cost: '$2,800',
  //   features: ['Balloon & extract PDF files', 'Create AS9102B FAI', 'Create custom inspection reports', 'Publish reports to Excel', 'Free training & support', 'Import CMM measurements', 'Jobs scheduling'],
  // },
];

// TODO: get these from Chargify, maybe?
export const pricingCards = [
  {
    name: i18n('entities.subscription.trial.solo.name'),
    qty: i18n('entities.subscription.trial.solo.qty'),
    cost: 1700,
    savings: 0,
    startQty: 1,
  },
  {
    name: i18n('entities.subscription.trial.team.name'),
    qty: i18n('entities.subscription.trial.team.qty'),
    cost: 3400,
    savings: 5100,
    startQty: 2,
  },
  {
    name: i18n('entities.subscription.trial.crew.name'),
    qty: i18n('entities.subscription.trial.crew.qty'),
    cost: 5100,
    savings: 8500,
    startQty: 4,
  },
];

// TODO: Get this from chargify IXC-1219
const drawingPrices = [
  { from: 0, to: 5, price: 0 },
  { from: 6, to: 100, price: 12500 },
  { from: 101, to: 250, price: 22500 },
  { from: 251, to: 500, price: 32500 },
  { from: 501, to: 1000, price: 42500 },
  { from: 1001, to: 'Unlimited', price: 52500 },
];

export const getDrawingPrice = (prices: typeof drawingPrices, value?: number) => {
  let step;
  let price;
  const purchased = value || 0;

  for (let index = 0; index < prices.length; index++) {
    const range = prices[index];
    if (range.from < purchased && (range.to === 'Unlimited' || range.to >= purchased)) {
      step = index;
      price = range.price;
    }
  }
  return { step, price };
};

interface ProductIDs {
  [key: string]: {
    tier: string;
    costPerSeat: number;
    drawings: typeof drawingPrices;
    level: number;
  };
}

export const products: ProductIDs = {
  // test site?
  '5256309': {
    tier: i18n('entities.subscription.tier.trial'),
    costPerSeat: 0,
    level: 0,
    drawings: [],
  },
  '5233891': {
    tier: i18n('entities.subscription.tier.ix_standard'),
    costPerSeat: 12500,
    drawings: drawingPrices,
    level: 1,
  },
  '1234': {
    tier: 'Advanced',
    costPerSeat: 22500,
    drawings: drawingPrices,
    level: 2,
  },
  // production site?
  '5217942': {
    tier: i18n('entities.subscription.tier.ix_standard'),
    costPerSeat: 12500,
    drawings: drawingPrices,
    level: 1,
  },
};

export const roles = {
  paid: [
    {
      label: i18n('user.owner.singular'),
      description: i18n('user.owner.description'),
      roles: ['Owner'],
      restricted: true,
      key: 'owner',
    },
    {
      label: i18n('user.planner.singular'),
      description: i18n('user.planner.description'),
      roles: ['Admin'],
      restricted: false,
      key: 'planner',
    },
    {
      label: i18n('user.billing.planner'),
      description: i18n('user.billing.plannerDescription'),
      roles: ['Admin', 'Billing'],
      restricted: false,
      key: 'plannerBilling',
    },
  ],
  free: [
    {
      label: i18n('user.collaborator.singular'),
      description: i18n('user.collaborator.description'),
      roles: ['Collaborator'],
      restricted: false,
      key: 'collaborator',
    },
    {
      label: i18n('user.billing.singular'),
      description: i18n('user.billing.description'),
      roles: ['Billing'],
      restricted: false,
      key: 'collaboratorBilling',
    },
  ],
  inactive: {
    label: i18n('user.inactive.singular'),
    description: i18n('user.inactive.description'),
    roles: [],
    restricted: false,
    key: 'inactive',
  },
};

export const rolesMap: {
  [key: string]: { label: string; description: string; roles: string[]; paid: boolean };
} = {
  planner: {
    label: i18n('user.planner.singular'),
    description: i18n('user.planner.description'),
    roles: ['Admin'],
    paid: true,
  },
  plannerBilling: {
    label: i18n('user.billing.planner'),
    description: i18n('user.billing.plannerDescription'),
    roles: ['Admin', 'Billing'],
    paid: true,
  },
  collaborator: {
    label: i18n('user.collaborator.singular'),
    description: i18n('user.collaborator.description'),
    roles: ['Collaborator'],
    paid: false,
  },
  collaboratorBilling: {
    label: i18n('user.billing.singular'),
    description: i18n('user.billing.description'),
    roles: ['Billing'],
    paid: false,
  },
  collaboratorViewer: {
    label: i18n('user.viewer.singular'),
    description: i18n('user.viewer.description'),
    roles: ['Collaborator'],
    paid: false,
  },
  inactive: {
    label: i18n('user.inactive.singular'),
    description: i18n('user.inactive.description'),
    roles: [],
    paid: false,
  },
};

// Default List Item IDs

export const defaultTypeIDs = {
  Dimension: 'db687ac1-7bf8-4ada-be2b-979f8921e1a0',
  'Geometric Tolerance': 'f3c4d8e0-fb79-4c72-a3f8-3f584ea075a7',
  Note: 'fa3284ed-ad3e-43c9-b051-87bd2e95e927',
  Other: 'b8599a29-7560-48e2-807a-6f3c3c0f2366',
};

export const defaultSubtypeIDs = {
  Note: '805fd539-8063-4158-8361-509e10a5d371',
  Length: 'edc8ecda-5f88-4b81-871f-6c02f1c50afd',
  Diameter: '4396f3c2-a915-4458-8cff-488b6d301ecd',
  Radius: '49d45a6c-1073-4c6e-805f-8f4105641bf1',
  Position: '19a7a9a1-76c5-45ef-8cf9-611ebe7cf207',
  Curvilinear: '3e951cbe-1211-436b-b45d-7cf14edcff79',
  Angle: 'b2e1e0aa-0da4-4844-853a-f20971b013a4',
  Angularity: '5ce784e8-f614-4e6d-a79b-237b4dc751dd',
  'Profile of a Surface': 'ea0e747f-9e70-4e2e-9a34-4491f6b63593',
  'Profile of a Line': '1e36505f-e05e-4796-a261-63701ad07b57',
  ElectricPower: 'd02aa7bc-b791-413d-be10-fe5359bb8385',
};

export const defaultClassificationIDs = {
  Incidental: '975a513d-394c-4123-901e-1244756b488e',
  Minor: '502a8ba4-82a7-423a-a339-b8f358ba3d3a',
  Major: '187b5423-dafd-402a-b471-2ed2fe76d2f9',
  Key: '22ff867a-a24c-4f6a-a3af-4df63d1c3fad',
  Critical: '168eba1e-a8bf-41bb-a0d2-955e8a7b8d66',
};

export const defaultMethodIDs = {
  'Air Gage': '8c249592-c581-4eaf-8f31-de5ca21466cb',
  Boring: '23fe4070-5e7b-4931-aeee-5200eb6d43ca',
  Caliper: 'd52dffc8-59fa-42b3-8561-8bd9bdb6df73',
  Misc: 'ae157fc5-05cd-40c9-beed-6444404cc5a2',
  Indicator: '1526159c-180d-4d3a-946a-377082d2b6b2',
  'Pin Micrometer': '5713901c-fb4e-4f49-b28a-51ec272e7951',
  Micrometer: '6325d6d4-5138-4de2-b6c9-e830ec7ca10b',
  'Gage Pins': 'abfa316f-04aa-4b18-880f-88c1c713117a',
  CMM: 'e18329cf-9875-4475-bee9-f40911dd2ada',
  'CO-Gage': 'ecc0170d-f38a-4b5f-8462-42a203ed712a',
  'SG Gage': '79316224-1a0a-4de4-a5bd-f1fd2412af0a',
  'Bore Gage': '1b87d2b9-e718-4141-85fb-06ba4ac1094f',
  'Drop Indicator': '2765de00-bef5-4e33-9c25-0e1484d74f84',
  'Gage Blocks': '78511917-4cc1-46e1-8a3a-14062fc6d703',
  'Gage Kit': 'a15139f2-601e-412c-9cac-183d29277555',
  'Cone Micrometer': '0afef458-506a-4bef-87a6-8c5fdf09ae9d',
};

export const defaultOperationIDs = {
  Deburring: '976742db-7e86-43ed-81eb-8d420ef77792',
  Drilling: '04756ef3-affc-4b09-b1ce-ab901a75ce69',
  Punching: 'e7948e9c-c98c-4338-9a95-a4bd707dffe5',
  Bending: '33c4ed1e-7476-4937-9885-b16e0f604824',
  Milling: '3ec29f3c-0af9-4b9c-82b2-8ecc195e7a5d',
};
export interface ControlLevel {
  name: string;
  value: string;
  label: string;
  description: string;
}

export const documentControls: ControlLevel[] = [
  {
    name: 'None',
    value: 'None',
    label: i18n('entities.part.enumerators.accessControl.None'),
    description: i18n('settings.seats.unrestricted'),
  },
  {
    name: 'ITAR/EAR',
    value: 'ITAR/EAR',
    label: i18n('entities.part.enumerators.accessControl.ITAR'),
    description: i18n('settings.seats.restricted'),
  },
];

// Default Frequently Used Characters
export const symbolDefaults = {
  '': 1,
  '°': 1,
  '±': 1,
  '↧': 1,
  '⌵': 1,
  '⌴': 1,
  '⌓': 1,
  '⌖': 1,
  '⏥': 1,
  '⊥': 1,
  '∠': 1,
  '○': 1,
  '↗': 1,
  '⌒': 1,
  '⌾': 1,
  '⌯': 1,
  '⎯': 1,
  '⌭': 1,
};

// External URLs (right place for these? Config.js maybe?)
export const hubspotSignupUrl = process.env.REACT_APP_HUBSPOT_SIGNUP_URL;
export const legalTermsUrl = 'https://www.ideagen.com/legal-terms';
export const privacyPolicyUrl = 'https://www.ideagen.com/privacy-policy';
export const bookTrainingUrl = 'https://www.inspectionxpert.com/getting-started#tab-3';
export const contact = {
  sales: {
    phone: '(919) 928-5609',
    email: 'orders.qcessentials@ideagen.com',
  },
};

export const defaultFeature: CharacteristicInput = {
  drawingSheetIndex: 0,
  previousRevision: null,
  originalCharacteristic: null,
  status: 'Active',
  captureMethod: 'Manual',
  captureError: '',
  notationType: 'fa3284ed-ad3e-43c9-b051-87bd2e95e927',
  notationSubtype: '805fd539-8063-4158-8361-509e10a5d371',
  notationClass: 'Tolerance',
  fullSpecification: '',
  quantity: 1,
  nominal: '',
  upperSpecLimit: '',
  lowerSpecLimit: '',
  plusTol: '',
  minusTol: '',
  unit: '',
  notes: '',
  drawingRotation: 0,
  drawingScale: 1,
  boxLocationY: 0,
  boxLocationX: 0,
  boxWidth: 0,
  boxHeight: 0,
  boxRotation: 0,
  markerGroup: '',
  markerGroupShared: false,
  markerIndex: 0,
  markerSubIndex: 0,
  markerLabel: '',
  markerStyle: 'f12c5566-7612-48e9-9534-ef90b276ebaa',
  markerLocationX: 0,
  markerLocationY: 0,
  markerFontSize: 0,
  markerSize: 0,
  markerRotation: 0,
  gridCoordinates: '',
  balloonGridCoordinates: '',
  connectionPointGridCoordinates: '',
  toleranceSource: 'Default_Tolerance',
  operation: '',
  criticality: '',
  inspectionMethod: '',
  part: '',
  drawing: '',
  drawingSheet: '',
  verified: false,
  fullSpecificationFromOCR: '',
  confidence: -1,
  connectionPointLocationX: 0,
  connectionPointLocationY: 0,
  connectionPointIsFloating: false,
  displayLeaderLine: true,
  leaderLineDistance: 0,
};

//Email Regular Expression Validation
export const emailRegExp = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
