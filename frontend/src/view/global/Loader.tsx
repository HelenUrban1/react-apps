import React from 'react';
import { Result, Button, Spin } from 'antd';

type Props = {
  loading: number | undefined;
  queries?: boolean[];
  error: any;
  closeError: () => void;
  className?: string;
};

export const Loader = ({ loading, queries, error, closeError, className }: Props) => {
  const percentage = queries ? Math.ceil((queries.filter((isLoading) => !isLoading).length / queries.length) * 100) + 1 : loading;
  const closeButton = (
    <Button type="primary" key="close" onClick={closeError}>
      Close
    </Button>
  );

  if (!error && (!percentage || percentage >= 100)) {
    return <></>;
  }

  return (
    <div className={`loader ${className}`} data-cy="loader">
      {percentage && <Spin className="loader-indicator" size="large" delay={100} />}
      {error && <Result status="error" title="There was an error with your request." subTitle={error.message || 'No message was given. Please check with customer support.'} extra={closeButton} />}
      <div className="loaderBackground" />
    </div>
  );
};
export default Loader;

Loader.defaultProps = {
  queries: undefined,
  className: '',
};
