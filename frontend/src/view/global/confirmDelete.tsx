import React from 'react';
import { DeleteOutlined, DeleteTwoTone, WarningOutlined } from '@ant-design/icons';
import { Popconfirm, Tooltip } from 'antd';
import { i18n } from 'i18n';
import { TooltipPlacement } from 'antd/lib/tooltip';

interface Props {
  confirm: () => void;
  entity?: string;
  confirmText?: string;
  classes?: string;
  colored?: boolean;
  placement?: TooltipPlacement;
}

export const ConfirmDelete: React.FC<Props> = ({ confirm, entity, confirmText, classes = 'icon-button', colored = true, placement = 'top' }) => {
  const cancelPopup = () => {};
  return (
    <Popconfirm
      title={confirmText || i18n('common.areYouSure')}
      onConfirm={confirm}
      onCancel={cancelPopup}
      icon={<WarningOutlined style={{ color: 'red' }} />}
      okText={i18n('common.destroy')}
      cancelText={i18n('common.cancel')}
      cancelButtonProps={{ id: 'popconfirm-btn-cancel' }}
      okButtonProps={{ id: 'popconfirm-btn-ok' }}
      placement={placement}
    >
      <Tooltip title={entity ? `${i18n('common.destroy')} ${entity.toLowerCase()}` : ''}>
        <button type="button" className={classes}>
          {colored && <DeleteTwoTone twoToneColor="#f5222d" />}
          {!colored && <DeleteOutlined />}
        </button>
      </Tooltip>
    </Popconfirm>
  );
};
