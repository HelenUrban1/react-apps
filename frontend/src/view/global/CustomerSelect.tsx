import React, { useState, ChangeEvent, useEffect } from 'react';
import { Modal, Input, Button, Tooltip, Form } from 'antd';
import { Organization } from 'graphql/organization';
import Spinner from 'view/shared/Spinner';
import { AppState } from 'modules/state';
import { useDispatch, useSelector } from 'react-redux';
import { SessionActions, createOrganizationThunk } from 'modules/session/sessionActions';
import { Select } from './inputs';
import en from 'i18n/en';

interface OrganizationSelectProps {
  value: string | null;
  onChange: (value: string) => void;
  canEdit?: boolean;
}

const OrganizationSelect: React.FC<OrganizationSelectProps> = ({ value, onChange, canEdit = true }) => {
  // Track whether the modal is open or not
  const [displayModal, setDisplayModal] = useState(false);
  // Track whether there is any text in input field
  const [nameIsBlank, setNameIsBlank] = useState(true);
  // Track whether to show the toolip on Ok button
  const [tooltip, setTooltip] = useState(false);
  // Track the name of the organization being added
  const [newOrganizationName, setNewOrganizationName] = useState('');
  const { sessionLoaded, organizations, newOrg } = useSelector((state: AppState) => state.session);

  const dispatch = useDispatch();

  // Build a list of organization options for the select dropdown
  const [organizationOptions, setOrgs] = useState<{ name: string; label: string; value: string }[]>([]);

  useEffect(() => {
    setNameIsBlank(!newOrganizationName.trim().length);
  }, [newOrganizationName]);

  useEffect(() => {
    if (sessionLoaded && organizationOptions.length === 0) {
      const orgOpts: { name: string; label: string; value: string }[] = [];
      organizations.all.forEach((organization: Organization) => {
        orgOpts.push({
          name: organization.id,
          label: organization.name,
          value: organization.id,
        });
      });
      setOrgs(orgOpts);
    }
  }, [sessionLoaded]);

  useEffect(() => {
    if (newOrg) {
      onChange(newOrg.id);
      setOrgs([...organizationOptions, { name: newOrg.id, label: newOrg.name, value: newOrg.id }]);
      dispatch({ type: SessionActions.UPDATE_NEW_ORG, payload: null });
    }
  }, [newOrg]);

  // Method to coordinate the creation of a new organization record from inside the modal
  const handleNewOrganizationSubmit = async () => {
    dispatch(
      createOrganizationThunk({
        name: newOrganizationName.toLocaleLowerCase(),
        status: 'Active',
        type: 'Customer',
      }),
    );
    setNewOrganizationName('');
    setDisplayModal(false);
  };

  const cancel = () => {
    setDisplayModal(false);
    setNewOrganizationName('');
  };

  const handleChange = (event: ChangeEvent<HTMLSelectElement>) => {
    if (event.target.value === 'organization-add') {
      return;
    }
    onChange(event.target.value);
  };

  if (!organizations.all || organizations.all.length === 0) {
    return <Spinner style={{ margin: '0px', height: '48px' }} />;
  }

  const checkOrgExists = () => {
    return organizationOptions.filter((org) => newOrganizationName.toLocaleLowerCase() === org.label.toLocaleLowerCase()).length > 0 ? true : false;
  };

  return (
    <div className="customer-select">
      <Select
        input={{
          name: 'customer',
          label: 'Customer',
          value,
          type: 'select',
          group: organizationOptions,
          change: handleChange,
          search: true,
          add: (organization: string) => {
            setNewOrganizationName(organization);
            setDisplayModal(true);
          },
          disabled: !canEdit,
        }}
      />
      <Modal
        title="Create Customer"
        visible={displayModal}
        onCancel={cancel}
        footer={[
          <>
            <Button onClick={cancel} style={{ margin: '5px' }}>
              Cancel
            </Button>

            {/* We need to track mouseOver on a div wrapping the button, since disabled buttons can't fire events */}
            <div
              style={{ display: 'inline-block' }}
              onMouseOver={() => {
                setTooltip(true);
              }}
              onMouseLeave={() => {
                setTooltip(false);
              }}
            >
              <Tooltip title="Name is blank!" visible={nameIsBlank && tooltip}>
                <Button type="primary" onClick={handleNewOrganizationSubmit} style={{ margin: '5px' }} disabled={nameIsBlank || checkOrgExists()}>
                  Ok
                </Button>
              </Tooltip>
            </div>
          </>,
        ]}
      >
        <Form.Item validateStatus={checkOrgExists() ? 'error' : 'success'} help={checkOrgExists() ? en.errors.customerExists : ''}>
          <label>
            New Customer Name:
            <Input
              value={newOrganizationName}
              onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
                setNewOrganizationName(e.target.value.replace(/[^a-zA-Z0-9 ]/gm, ''));
              }}
            />
          </label>
        </Form.Item>
      </Modal>
    </div>
  );
};

export default OrganizationSelect;
