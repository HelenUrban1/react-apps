import React from 'react';
import { InputType, Radio, Switch, Select, Group, InvisibleInput, Cascader, NumberPicker } from '.';

const Section: React.FC<{ sections: InputType[]; id: number }> = ({ sections, id }) => {
  return (
    <section id={`section-${id}`} className="form-section">
      {sections.map((input) => {
        if (input.group) {
          switch (input.type) {
            case 'radio':
              return <Radio key={input.name} input={input} />;
            case 'switch':
              return <Switch key={input.name} input={input} />;
            case 'select':
              return <Select key={input.name} input={input} />;
            case 'component':
              return input.component;
            case 'cascader':
              return <Cascader key={input.name} input={input} />;
            case 'numberPicker':
              return <NumberPicker key={input.name} input={input} />;
            default:
              return <Group key={input.name} input={input} />;
          }
        }
        return <InvisibleInput key={input.name} input={input} />;
      })}
    </section>
  );
};

export default Section;
