import { Dropdown, Select, Tooltip, Menu, Button } from 'antd';
import { i18n } from 'i18n';
import React, { ReactNode } from 'react';

interface SelectSaveProps {
  disabled: boolean;
  value: string;
  handleOnChange: (e: any) => void;
  handleSave: (e: any) => void;
  options: ReactNode;
  field: string;
}

export const SelectSave = ({ disabled, value, handleOnChange, handleSave, options, field }: SelectSaveProps) => {
  return (
    <section className="select-with-button wide preset-row">
      <Select value={value} onChange={handleOnChange} size="large" className="select-save-input" data-cy={`${field}-preset-select`}>
        {options}
      </Select>
      <Dropdown
        disabled={disabled}
        overlay={
          <Menu onClick={handleSave}>
            <Menu.Item key="save">{i18n(field === 'styles' ? 'wizard.newStyle' : 'wizard.newTolerance')}</Menu.Item>
            <Menu.Item key="overwrite">{i18n('wizard.updatePreset', value || '')}</Menu.Item>
          </Menu>
        }
      >
        <Tooltip title={disabled ? 'No changes found' : undefined}>
          <Button className="btn-primary" size="large" disabled={disabled}>
            {i18n('common.save')}
          </Button>
        </Tooltip>
      </Dropdown>
    </section>
  );
};
