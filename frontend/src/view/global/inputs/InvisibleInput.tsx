import React, { useState, useEffect, CSSProperties, useRef } from 'react';
import { Input, Tooltip, Button } from 'antd';
import { getFramedText, isFramed } from 'utils/textOperations';
import { TextAreaRef } from 'antd/lib/input/TextArea';
import { InputType } from '.';
import { QuestionCircleOutlined, CopyOutlined } from '@ant-design/icons';

const { TextArea } = Input;

export const InvisibleInput: React.FC<{ input: InputType }> = ({ input }) => {
  const [value, setValue] = useState(input.value || '');
  const [oldValue, setOldValue] = useState(input.value);
  const [cursor, setCursor] = useState(-1);
  const inputEl = useRef<Input>(null);
  const areaInputEl = useRef<TextAreaRef>(null);

  // update value when clearing limits to reset defaults
  useEffect(() => {
    if (input && input.value && (!value || value === '')) {
      // Avoid updating input value that user is editing to avoid rubber-banding
      const inpEle = inputEl.current?.input || areaInputEl.current?.resizableTextArea?.textArea;
      if (inpEle !== document.activeElement) {
        if (input.validateTo) {
          setValue(input.value.replace(input.validateTo, ''));
        } else {
          setValue(input.value);
        }
      }
    }
  }, [input]);

  // update value when switching features
  useEffect(() => {
    // Avoid updating input value that user is editing to avoid rubber-banding
    const inpEle = inputEl.current?.input || areaInputEl.current?.resizableTextArea?.textArea;
    if (inpEle !== document.activeElement) {
      setValue(input.value || '');
    }
  }, [input.value]);

  // update cursor position
  useEffect(() => {
    const inpEle = inputEl.current?.input || areaInputEl.current?.resizableTextArea?.textArea;
    if (cursor > -1 && inpEle) {
      inpEle.setSelectionRange(cursor, cursor);
      setCursor(-1);
    }
  }, [cursor]);

  // replace value after applying any rules
  const handleReplaceValue = (str: string, start: number) => {
    if (!value || !oldValue) return;
    const cursorPos = value.length - str.length + start;
    setCursor(cursorPos);
    setValue(str);
  };

  // apply any input rules
  const handleKeyUp = () => {
    const inpEle = inputEl.current?.input || areaInputEl.current?.resizableTextArea?.textArea;
    if (!inpEle || !value) return;

    let start = (document.activeElement as any)?.selectionStart;
    if (start === null) start = 0;

    if (input.limitTo === 'nominal') {
      // erase character if its not a number, decimal, space, dash, forward slash or comma
      const newVal = value.replace(/[^\d-\.,\/\s ]/g, '');
      handleReplaceValue(newVal, start);
    } else if (input.limitTo === 'number') {
      // erase character if its not a number, decimal, or comma
      const newVal = value.replace(/[^\-\d\.,]/g, '');
      handleReplaceValue(newVal, start);
    } else if (input.limitTo === 'uppercase') {
      setValue(value.toLocaleUpperCase());
    } else if (isFramed(value)) {
      const newVal = getFramedText(value);
      handleReplaceValue(newVal, start);
    }
  };

  const change = (e: any) => {
    e.preventDefault();
    setOldValue(value);
    if (input.validateTo) {
      setValue(e.target.value.replace(input.validateTo, ''));
    } else {
      setValue(e.target.value);
    }
    if (input.change) {
      input.change(e);
    }
  };

  const defaultStyle: CSSProperties = input.style ? input.style : { resize: 'none' };
  if (input.errorConditional) {
    defaultStyle.borderColor = 'red';
  }

  const getTipText = () => {
    if (input.errorConditional && input.errorText) {
      return input.errorText;
    }
    return input.type != 'drawingNumber' ? input.tooltip || '' : '';
  };

  const handleCopyPartNumber = () => {
    const textElement = areaInputEl.current?.resizableTextArea?.textArea;

    if (textElement !== document.activeElement && input.partNumber) {
      setOldValue(value);
      if (input.validateTo) {
        setValue(input.partNumber.replace(input.validateTo, ''));
      } else {
        setValue(input.partNumber);
      }
      if (textElement) textElement.focus();
    }
  };

  return (
    <section>
      <div className="drawing-number-input">
        <label htmlFor={`${input.name}-input`} key={input.name} id={input.name} className={`form-input ${input.classes} ${input.disabled ? 'disabled' : ''}`}>
          {input.label && (
            <>
              <span className="label-text">{input.label}:</span>
              <span className="required">{`${input.required ? '*' : ''}`}</span>
            </>
          )}
          <Tooltip placement="top" title={getTipText()}>
            <div id={`${input.name}-input`} className={input.modal !== undefined ? 'text-area-wrapper' : ''} data-cy={`${input.name}-input`}>
              {input.limitTo ? (
                <Input
                  placeholder={`${input.required ? 'required' : ''}`}
                  style={defaultStyle}
                  disabled={!!input.disabled}
                  value={value}
                  name={input.name}
                  onKeyUp={handleKeyUp}
                  onChange={change}
                  onBlur={input.blur}
                  onFocus={input.focus}
                  ref={inputEl}
                />
              ) : (
                <TextArea
                  ref={areaInputEl}
                  placeholder={`${input.required ? 'required' : ''}`}
                  style={defaultStyle}
                  autoSize
                  disabled={!!input.disabled}
                  value={value}
                  name={input.name}
                  onKeyUp={handleKeyUp}
                  onChange={change}
                  onBlur={input.blur}
                  onFocus={input.focus}
                />
              )}
              {input.type == 'drawingNumber' && (
                <Button className="btn-primary" size="middle" onClick={handleCopyPartNumber}>
                  <CopyOutlined />
                </Button>
              )}
              <span>
                {input.tooltip && input.type == 'drawingNumber' && (
                  <Tooltip placement="right" title={input.tooltip}>
                    <QuestionCircleOutlined className="form-label-hint"/>
                  </Tooltip>
                )}
              </span>

              {input.modal}
            </div>
          </Tooltip>
        </label>
      </div>
    </section>
  );
};

export default InvisibleInput;
