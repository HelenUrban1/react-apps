import React, { useState } from 'react';
import { Select, Divider } from 'antd';
import { toDisplayFormat } from 'utils/textOperations';
import AddOptionButton from 'styleguide/Buttons/AddOption';
import { InputType } from '.';

const { Option } = Select;

const SelectInput: React.FC<{ input: InputType; loading?: boolean }> = ({ input, loading = false }) => {
  const [searchValue, setValue] = useState('');

  const getValue = () => {
    if (!input.value) return undefined;
    if (input.multiple) {
      return input.value.split(',');
    }
    return input.value;
  };

  return (
    <label key={input.name} data-cy={input.name} id={input.name} className={`form-input ${input.classes}`}>
      {input.label && <span className="label-text">{input.label}:</span>}
      {input.required && !input.value && <span className="required">*</span>}
      {input.group && input.search && (
        // Searchable select field
        <Select
          loading={loading}
          mode={input.multiple ? 'multiple' : undefined}
          value={getValue()}
          aria-label={input.label}
          aria-labelledby={input.name}
          showSearch
          disabled={input.disabled}
          optionFilterProp="children"
          placeholder={input.placeholder}
          dropdownMatchSelectWidth={false}
          dropdownClassName={`${input.name}-dropdown`}
          filterOption={(text, option) => {
            if (input.add) {
              // Save our search text when adding a new option so it can be passed
              setValue(toDisplayFormat(text));
            }
            // Filter options as a user types, always keeping at least "+ New {whatever this field is}"
            return (
              typeof option?.props.children !== 'string' ||
              (option.props.children && typeof option.props.children === 'string' ? option.props.children.toLowerCase().indexOf(text.toLowerCase()) >= 0 || option.props.children.toLowerCase().indexOf(` New ${input.name}`.toLowerCase()) >= 0 : false)
            );
          }}
          onChange={(value: string | string[]) => (input.change && value !== 'customer-add' ? input.change({ target: { name: input.name, value } }) : (value = ''))}
          dropdownRender={(menu) => (
            <div>
              {menu}
              {!!input.add && (
                <>
                  <Divider style={{ margin: '4px 0' }} />
                  <div className="add-option">
                    <AddOptionButton
                      add={(e) => {
                        e.preventDefault();
                        if (input.add) {
                          input.add(searchValue);
                        }
                        setValue('');
                        return null;
                      }}
                      option={toDisplayFormat(input.label)}
                    />
                  </div>
                </>
              )}
            </div>
          )}
        >
          {input.group &&
            input.group.map((group) => {
              return (
                <Option key={group.id || group.name} value={group.value || ''} data-cy={group.name} className={`${group.deleted ? 'hide-option' : 'show-option'}`}>
                  {toDisplayFormat(group.label)}
                </Option>
              );
            })}
        </Select>
      )}
      {input.group && !input.search && (
        // Non-searchable select
        <Select
          mode={input.multiple ? 'multiple' : undefined}
          dropdownClassName={`${input.name}-dropdown`}
          dropdownMatchSelectWidth={false}
          placeholder={input.placeholder}
          value={getValue()}
          disabled={input.disabled}
          onChange={(value: string | string[]) => (input.change ? input.change({ target: { name: input.name, value } }) : null)}
          dropdownRender={(menu) => (
            <div>
              {menu}
              {!!input.add && (
                <>
                  <Divider style={{ margin: '4px 0' }} />
                  <div className="add-option">
                    <AddOptionButton
                      add={(e) => {
                        e.preventDefault();
                        if (input.add) input.add(e);
                      }}
                      option={toDisplayFormat(input.label)}
                    />
                  </div>
                </>
              )}
            </div>
          )}
        >
          {input.group &&
            input.group.map((group) => (
              <Option key={group.id || group.name} value={group.value || ''} data-cy={group.name} className={`${group.deleted ? 'hide-option' : 'show-option'}`}>
                {input.doNotFormat ? group.label : toDisplayFormat(group.label)}
              </Option>
            ))}
        </Select>
      )}
    </label>
  );
};

export default SelectInput;
