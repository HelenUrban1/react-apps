import React from 'react';
import { Radio } from 'antd';
import { InputType } from '.';

const RadioInput: React.FC<{ input: InputType }> = ({ input }) => {
  return (
    <Radio.Group //
      id={input.name}
      key={input.name}
      name={input.name}
      className={`form-input ${input.classes}`}
      defaultValue={input.group ? input.group[0].value : ''}
      value={input.value}
      onChange={input.change}
      buttonStyle="solid"
    >
      <fieldset>
        {input.label && input.label !== '' && <legend className={`form-input ${input.classes}`}>{`${input.label}:`}</legend>}
        {input.group &&
          input.group.map((group) => (
            <Radio key={group.name} id={`radio-${input.name}-${group.name}`} value={group.value} disabled={input.disabled}>
              <span id={`radio-${input.name}-${group.name}-label`} data-cy={`radio-${input.name}-${group.name}-label`}>
                {group.label}
              </span>
            </Radio>
          ))}
      </fieldset>
    </Radio.Group>
  );
};

export default RadioInput;
