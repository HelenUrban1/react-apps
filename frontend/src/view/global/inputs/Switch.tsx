import React from 'react';
import { Switch, Tooltip } from 'antd';
import { CheckOutlined, CloseOutlined, QuestionCircleOutlined } from '@ant-design/icons';
import { InputType } from '.';

const SwitchInput: React.FC<{ input: InputType }> = ({ input }) => {
  return (
    <label htmlFor={`switch-${input.name}`} className={`${input.classes}`}>
      <span className="label-text">
        {input.label}:
        {input.tooltip && (
          <Tooltip placement="right" title={input.tooltip}>
            <QuestionCircleOutlined className="form-label-hint" />
          </Tooltip>
        )}
      </span>
      <section className="form-row-container">
        <Switch //
          key={input.name}
          checked={!!input.value}
          onChange={input.change}
          data-cy={`switch-${input.name}`}
          checkedChildren={<CheckOutlined />}
          unCheckedChildren={<CloseOutlined />}
        />
      </section>
    </label>
  );
};

export default SwitchInput;
