import React from 'react';
import { InputNumber } from 'antd';
import { InputType } from '.';

const NumberPicker: React.FC<{ input: InputType }> = ({ input }) => {
  return (
    <label htmlFor={`${input.name}-input`} key={input.name} id={input.name} className={`form-input ${input.classes} ${input.disabled ? 'disabled' : ''}`}>
      {input.label && (
        <>
          <span className="label-text">{input.label}:</span>
          <span className="required">{`${input.required ? '*' : ''}`}</span>
        </>
      )}
      <InputNumber id={input.name} key={input.name} name={input.name} className={`form-input ${input.classes}`} min={input.range?.min.toString() || 0} max={input.range?.max.toString() || 1} value={input.value || 0} onChange={input.change} />
    </label>
  );
};

export default NumberPicker;
