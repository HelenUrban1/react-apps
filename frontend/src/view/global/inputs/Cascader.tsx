import React, { useRef, useState } from 'react';
import AddOptionButton from 'styleguide/Buttons/AddOption';
import { Cascader, Divider } from 'antd';
import { BaseOptionType, CascaderRef } from 'antd/lib/cascader';
import { getValueOfHiddenType } from 'utils/Lists';
import { toDisplayFormat } from 'utils/textOperations';
import { InputType } from '.';

const IXCascader: React.FC<{ input: InputType }> = ({ input }) => {
  const [searchValue, setValue] = useState('');
  const cascaderRef = useRef<CascaderRef | null>(null);
  // Only show final assignment selection in dropdown

  const displayRender = (label: string[]) => {
    if (!input.cascaderValue) {
      return input.placeholder ? null : <span key="None">-None-</span>;
    }

    let type = label[0] ? label[0] : 'Not found';
    let subtype = label[1] ? label[1] : 'Not found';
    if (type === 'Geometric Tolerance') {
      type = 'GDT';
    }

    // If the type was hidden after setting, the label values are undefined
    if ((!label[0] || !label[1]) && input.list) {
      const hiddenValues = getValueOfHiddenType(input.list, input.cascaderValue);
      type = hiddenValues.type;
      subtype = hiddenValues.subtype;
    }
    return <span key={input.cascaderValue.join(' / ')}>{`${type} / ${subtype}`}</span>;
  };

  const filter = (inputValue: string, path: BaseOptionType[]) => {
    return path.some((option: any) => option.label.toLowerCase().indexOf(inputValue.toLowerCase()) > -1);
  };

  // TODO: Use dropdownRender prop to add a button to add a new Type (check Select.tsx example), AntD v4 enhancement
  return (
    <label key={input.name} id={input.name} data-cy={input.name} className={`form-input ${input.classes}`}>
      <span className="label-text">{input.label}: </span>
      <Cascader
        data-cy={`${input.name}-cascader`}
        ref={cascaderRef}
        value={input.cascaderValue}
        defaultValue={input.cascaderValue}
        placeholder={input.placeholder}
        allowClear={false}
        onChange={input.change}
        disabled={input.disabled}
        dropdownClassName={`${input.name}-cascader`}
        className="type-picker"
        displayRender={displayRender}
        options={input.cascaderOptions}
        showSearch={{ filter }}
        dropdownRender={(menu: any) => (
          <div>
            {menu}
            {!!input.add && (
              <>
                <Divider style={{ margin: '4px 0' }} />
                <div className="add-option">
                  <AddOptionButton
                    add={(e) => {
                      e.preventDefault();
                      if (input.add) {
                        input.add(searchValue);
                      }
                      setValue('');
                      if (cascaderRef.current) {
                        // close the cascader when the add button is clicked
                        cascaderRef.current.blur();
                        // cascaderRef.current.setState({ ...cascaderRef.current.state, popupVisible: false });
                      }
                      return null;
                    }}
                    option={toDisplayFormat(input.label)}
                  />
                </div>
              </>
            )}
          </div>
        )}
      />
    </label>
  );
};

export default IXCascader;
