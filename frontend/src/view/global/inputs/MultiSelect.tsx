import React, { useState } from 'react';
import { Select, Divider } from 'antd';
import { toDisplayFormat } from 'utils/textOperations';
import AddOptionButton from 'styleguide/Buttons/AddOption';
import { InputType } from '.';

const { Option } = Select;

const SelectInput: React.FC<{ input: InputType }> = ({ input }) => {
  const [searchValue, setValue] = useState('');

  return (
    <label key={input.name} data-cy={input.name} id={input.name} className={`form-input ${input.classes}`}>
      {input.label && <span className="label-text">{input.label}:</span>}
      {input.required && !input.value && <span className="required">*</span>}
      {input.group && input.search && (
        // Searchable select field
        <Select
          mode="multiple"
          value={input.valueArr ? input.valueArr : undefined}
          aria-label={input.label}
          aria-labelledby={input.name}
          showSearch
          optionFilterProp="children"
          dropdownMatchSelectWidth={false}
          dropdownClassName={`${input.name}-dropdown`}
          filterOption={(text, option) => {
            if (input.add) {
              // Save our search text when adding a new option so it can be passed
              setValue(toDisplayFormat(text));
            }
            // Filter options as a user types, always keeping at least "+ New {whatever this field is}"
            return (
              typeof option?.props.children !== 'string' ||
              (option.props.children && typeof option.props.children === 'string' ? option.props.children.toLowerCase().indexOf(text.toLowerCase()) >= 0 || option.props.children.toLowerCase().indexOf(` New ${input.name}`.toLowerCase()) >= 0 : false)
            );
          }}
          onChange={(value: string[]) => (input.change && value.toString() !== ['customer-add'].toString() ? input.change({ target: { name: input.name, value } }) : (value = ['']))}
          dropdownRender={(menu) => (
            <div>
              {menu}
              {!!input.add && (
                <>
                  <Divider style={{ margin: '4px 0' }} />
                  <div className="add-option">
                    <AddOptionButton
                      add={(e) => {
                        e.preventDefault();
                        if (input.add) {
                          input.add(searchValue);
                        }
                        setValue('');
                        return null;
                      }}
                      option={toDisplayFormat(input.label)}
                    />
                  </div>
                </>
              )}
            </div>
          )}
        >
          {input.group &&
            input.group.map((group) => {
              return (
                <Option key={group.name} value={group.value || ''} data-cy={group.name} className={`${group.deleted ? 'hide-option' : 'show-option'}`}>
                  {toDisplayFormat(group.label)}
                </Option>
              );
            })}
        </Select>
      )}
      {input.group && !input.search && (
        // Non-searchable select
        <Select
          mode="multiple"
          dropdownClassName={`${input.name}-dropdown`}
          dropdownMatchSelectWidth={false}
          value={input.valueArr ? input.valueArr : undefined}
          onChange={(value: string[]) => (input.change ? input.change({ target: { name: input.name, value } }) : null)}
          dropdownRender={(menu) => (
            <div>
              {menu}
              {!!input.add && (
                <>
                  <Divider style={{ margin: '4px 0' }} />
                  <div className="add-option">
                    <AddOptionButton
                      add={(e) => {
                        e.preventDefault();
                        if (input.add) input.add(e);
                      }}
                      option={toDisplayFormat(input.label)}
                    />
                  </div>
                </>
              )}
            </div>
          )}
        >
          {input.group &&
            input.group.map((group) => (
              <Option key={group.name} value={group.value || ''} data-cy={group.name} className={`${group.deleted ? 'hide-option' : 'show-option'}`}>
                {input.doNotFormat ? group.label : toDisplayFormat(group.label)}
              </Option>
            ))}
        </Select>
      )}
    </label>
  );
};

export default SelectInput;
