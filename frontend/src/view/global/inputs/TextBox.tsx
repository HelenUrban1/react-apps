import React from 'react';
import { InputType } from '.';
import { Input } from 'antd';

const { TextArea } = Input;

const TextAreaInput: React.FC<{ input: InputType }> = ({ input }) => {
  return <TextArea value={input.value ? input.value : ''} autoSize={{ minRows: input.minimumSize, maxRows: 6 }} rows={input.minimumSize ? input.minimumSize : 4} />;
};

export default TextAreaInput;
