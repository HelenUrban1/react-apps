import React from 'react';
import { InputType, Radio, Switch, Select, Cascader, TextBox, NumberPicker } from '.';
import { InvisibleInput } from './InvisibleInput';

const Grouped: React.FC<{ input: InputType }> = ({ input }) => {
  const getInputElement = (group: InputType) => {
    switch (group.type) {
      case 'radio':
        return <Radio key={group.name} input={group} />;
      case 'switch':
        return <Switch key={group.name} input={group} />;
      case 'select':
        return <Select key={group.name} input={group} />;
      case 'component':
        return group.component;
      case 'cascader':
        return <Cascader key={group.name} input={group} />;
      case 'textbox':
        return <TextBox key={group.name} input={group} />;
      case 'numberPicker':
        return <NumberPicker key={group.name} input={group} />;
      default:
        return <InvisibleInput key={group.name} input={group} />;
    }
  };

  return (
    <div key={input.name} id={input.name} className={`form-group ${input.classes}`}>
      {input.group &&
        input.group.map((group) => (
          <label key={group.name} id={group.name} className={`form-input ${group.classes}`}>
            {getInputElement(group)}
          </label>
        ))}
    </div>
  );
};

export default Grouped;
