import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { AppState } from 'modules/state';
import { useMutation } from '@apollo/client';
import { Modal, Input, Form } from 'antd';
import OrganizationAPI, { OrganizationCreate, OrganizationCreateVars } from 'graphql/organization';
import { SessionActions } from 'modules/session/sessionActions';
import MultiSelect from './inputs/MultiSelect';
import en from 'i18n/en';

interface OrganizationSelectProps {
  value: string[] | undefined;
  onChange: (value: string[]) => void;
}

export const CustomerMultiSelect: React.FC<OrganizationSelectProps> = ({ value, onChange }) => {
  // Track whether the modal is open or not
  const [displayModal, setDisplayModal] = useState(false);
  const dispatch = useDispatch();
  const { organizations } = useSelector((state: AppState) => state.session);

  // Build a list of organization options for the select dropdown
  const [organizationOptions, setOrgs] = useState<{ name: string; label: string; value: string }[]>([]);

  useEffect(() => {
    const newOrgs: { name: string; label: string; value: string }[] = [];
    if (organizations) {
      organizations.all.forEach((organization) => {
        if (organization.id !== organizations.self?.id) {
          newOrgs.push({
            name: organization.id,
            label: organization.name,
            value: organization.id,
          });
        }
      });
    }
    setOrgs(newOrgs);
  }, [organizations]);

  // Track the name of the organization being added
  const [newOrganizationName, setNewOrganizationName] = useState('');

  // Function to create the new organization
  const [createOrganization] = useMutation<OrganizationCreate, OrganizationCreateVars>(OrganizationAPI.mutate.create, {
    onCompleted({ organizationCreate }) {
      onChange(value ? [...value, organizationCreate.id] : [organizationCreate.id]);
      setOrgs([...organizationOptions, { name: organizationCreate.id, label: organizationCreate.name, value: organizationCreate.id }]);
      dispatch({ type: SessionActions.UPDATE_ORGANIZATIONS, payload: { all: organizations ? [...organizations.all, organizationCreate] : [organizationCreate] } });
    },
  });

  // Method to coordinate the creation of a new organization record from inside the modal
  const handleNewOrganizationSubmit = async () => {
    createOrganization({
      variables: {
        data: {
          name: newOrganizationName.toLocaleLowerCase(),
          status: 'Active',
          type: 'Customer',
        },
      },
    });
    setNewOrganizationName('');
    setDisplayModal(false);
  };

  const handleChange = (event: any) => {
    if (event.target.value === 'organization-add') {
      return;
    }
    onChange(event.target.value);
  };

  const checkOrgExists = () => {
    return organizationOptions.filter((org) => newOrganizationName.toLocaleLowerCase() === org.label.toLocaleLowerCase()).length > 0 ? true : false;
  };

  return (
    <div className="customer-select">
      <MultiSelect
        input={{
          name: 'customer',
          label: 'Customer',
          value: '',
          valueArr: value,
          type: 'select',
          group: organizationOptions,
          change: handleChange,
          search: true,
          add: (organization: string) => {
            setNewOrganizationName(organization);
            setDisplayModal(true);
          },
        }}
      />
      <Modal
        title="Create Customer"
        visible={displayModal}
        onOk={handleNewOrganizationSubmit}
        okButtonProps={{ disabled: checkOrgExists() }}
        onCancel={() => {
          setDisplayModal(false);
          setNewOrganizationName('');
        }}
      >
        <Form.Item validateStatus={checkOrgExists() ? 'error' : 'success'} help={checkOrgExists() ? en.errors.customerExists : ''}>
          <label>
            New Customer Name:
            <Input
              value={newOrganizationName}
              onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
                setNewOrganizationName(e.target.value);
              }}
            />
          </label>
        </Form.Item>
      </Modal>
    </div>
  );
};
