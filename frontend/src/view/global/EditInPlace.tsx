// TODO: Add to UI library
import React, { CSSProperties, ChangeEvent, useState, useEffect } from 'react';
import { Input, Tooltip } from 'antd';
import { i18n } from 'i18n';

const { TextArea } = Input;
interface Props {
  label?: string;
  name?: string;
  placeholder?: string;
  defaultValue?: string;
  classes?: string;
  style?: CSSProperties;
  errorConditional?: boolean;
  errorText?: string;
  onChange?: (event: ChangeEvent<HTMLTextAreaElement>) => void;
  onBlur?: (event: ChangeEvent<HTMLTextAreaElement>) => void;
  autosize: boolean | { maxRows: number };
  canEdit?: boolean;
  saving?: boolean;
}

const EditInPlace: React.FC<Props> = ({ name, defaultValue, style, onBlur, onChange, label, classes, placeholder, errorConditional, errorText, autosize, canEdit = true, saving }) => {
  const [val, setVal] = useState(defaultValue || '');

  useEffect(() => {
    setVal(defaultValue || '');
  }, [defaultValue]);

  const handleChange = (e: any) => {
    if (saving) return;
    setVal(e.target?.value.replace(/[^a-zA-Z0-9 ]/gm, '') || '');
    if (onChange) onChange(e);
  };

  const defaultStyle: CSSProperties = style || { resize: 'none' };
  if (errorConditional) {
    defaultStyle.borderColor = 'red';
  }
  return (
    <div className={`edit-in-place ${classes} ${saving ? 'saving' : ''}`}>
      {label || (name && <small>{i18n(`entities.part.fields[${name}]`)}</small>)}
      <Tooltip placement="top" title={errorConditional ? errorText : ''}>
        <TextArea style={defaultStyle} name={name} autoSize={autosize} value={val} placeholder={placeholder} onBlur={onBlur} onChange={handleChange} disabled={!canEdit} />
      </Tooltip>
    </div>
  );
};

export default EditInPlace;
