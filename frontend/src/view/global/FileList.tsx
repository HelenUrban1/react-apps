// TODO: Move to UI Library
// TODO: abstract out the inline functions
import React, { useRef, useCallback } from 'react';
import { InboxOutlined, PlusCircleTwoTone } from '@ant-design/icons';
import { Upload, message } from 'antd';
import { UploadFile } from 'antd/lib/upload/interface';
import { isPDF } from 'utils/files';
import { File as FileType } from 'types/file';
import { ConfirmDelete } from './confirmDelete';

const { Dragger } = Upload;

interface Props {
  files: FileType[];
  title: string;
  deleteFile: (file: any, type: string) => void;
  uploadFile: (file: UploadFile, type: string) => void;
}

interface FileProp {
  key: string;
  name: string;
  url: string;
  deleteFile: () => void;
}

const showDropzones = useCallback(() => {
  const dropzones = document.getElementsByClassName('file-list-dragger');
  Array.prototype.forEach.call(dropzones, (el) => {
    el.style.display = 'block';
  });

  document.removeEventListener('dragover', showDropzones);
  document.addEventListener('dragleave', hideDropzones);
  return function cleanup() {
    document.removeEventListener('dragleave', hideDropzones);
  };
}, []);

const hideDropzones = (e: MouseEvent) => {
  if (e.relatedTarget !== null) {
    return;
  }
  const dropzones = document.getElementsByClassName('file-list-dragger');
  Array.prototype.forEach.call(dropzones, (el) => {
    el.style.display = 'none';
  });

  document.removeEventListener('dragleave', hideDropzones);
  document.addEventListener('dragover', showDropzones);
};

const drop = (e: MouseEvent) => {
  e.preventDefault();
  const dropzones = document.getElementsByClassName('file-list-dragger');
  Array.prototype.forEach.call(dropzones, (el) => {
    el.style.display = 'none';
  });

  document.removeEventListener('dragleave', hideDropzones);
  document.addEventListener('dragover', showDropzones);
};

document.addEventListener('dragover', showDropzones);
document.addEventListener('drop', drop);

const File: React.FC<FileProp> = ({ url, name, deleteFile }) => {
  return (
    <li className="file-item">
      <a href={url}>{name}</a>
      <ConfirmDelete classes="file-delete" confirm={deleteFile} />
    </li>
  );
};

const FileList: React.FC<Props> = ({ title, files, deleteFile, uploadFile }) => {
  const thisList = useRef<HTMLDivElement>(null);
  return (
    <div className="file-list" ref={thisList}>
      <Dragger
        className="file-list-dragger"
        showUploadList={false}
        beforeUpload={(file) => {
          if (!file || file === undefined || file === null) {
            return false;
          }
          const pdf = isPDF(file.name);
          if (!pdf) {
            message.error({
              content: 'IXC only accepts PDF files',
            });
            return false;
          }
          uploadFile(file, title);
          return false;
        }}
      >
        <p className="ant-upload-drag-icon">
          <InboxOutlined />
        </p>
        <small className="ant-upload-text">Click or drag {title} to this area to upload</small>
      </Dragger>
      <small>
        {title}
        <Upload
          className="file-upload"
          showUploadList={false}
          beforeUpload={(file) => {
            if (!file || file === undefined || file === null) {
              return false;
            }
            const pdf = isPDF(file.name);
            if (!pdf) {
              message.error({
                content: 'IXC only accepts PDF files',
              });
              return false;
            }
            uploadFile(file, title);
            return false;
          }}
        >
          <PlusCircleTwoTone twoToneColor="#1890ff" />
        </Upload>
      </small>
      <ul>
        {files.map((file) => (
          <File key={file.id} url={file.publicUrl} name={file.name} deleteFile={() => deleteFile(file, title)} />
        ))}
      </ul>
    </div>
  );
};

export default FileList;
