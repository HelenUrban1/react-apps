// From https://reactjs.org/docs/error-boundaries.html

import React from 'react';
import { Redirect } from 'react-router-dom';
import log from 'modules/shared/logger';
import Analytics from 'modules/shared/analytics/analytics';
import { i18n } from 'i18n';
import { Result } from 'antd';
import { IxButton } from 'styleguide';
import { HomeOutlined, ReloadOutlined } from '@ant-design/icons';

function refreshPage() {
  window.location.reload(false);
}

export class ErrorBoundary extends React.Component {
  constructor(props) {
    super(props);
    this.state = { hasError: false, error: {}, redirect: null };

    this.closeError = this.closeError.bind(this);
  }

  static getDerivedStateFromError(error) {
    // Update state so the next render will show the fallback UI.
    return { hasError: true, error };
  }

  componentDidCatch(error, errorInfo) {
    // Log the error
    log.error(error, errorInfo);
  }

  closeError() {
    this.setState({ redirect: <Redirect to="/" /> });
  }

  render() {
    const { hasError, error, redirect } = this.state;
    const { children } = this.props;
    const buttons = (
      <div>
        <IxButton text={i18n('errors.backToHome')} data_cy="error-home" onClick={this.closeError} leftIcon={<HomeOutlined />} ghost style={{ marginRight: '10px' }} />
        <IxButton text={i18n('errors.refresh')} data_cy="error-refresh" leftIcon={<ReloadOutlined />} onClick={refreshPage} />
      </div>
    );
    if (hasError) {
      Analytics.track({
        event: Analytics.events.errorsNonspecific,
        error,
        properties: {
          source: 'ErrorBoundary.jsx',
        },
      });
      // Render fallback UI
      return (
        <div className="error-container" data-cy="error-container">
          <Result status="error" title={i18n('errors.validation.message')} subTitle={error.message || error.name || i18n('errors.generic')} extra={buttons} />
          {redirect}
        </div>
      );
    }
    return children;
  }
}

export default ErrorBoundary;
