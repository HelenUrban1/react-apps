import React from 'react';
import { useQuery } from '@apollo/client';
import { Link } from 'react-router-dom';
import { Button, Card } from 'antd';
import Spinner from 'view/shared/Spinner';
import Errors from 'modules/shared/error/errors';
import SphinxApi from 'modules/sphinx/sphinxApi';
import { getApiKeyFromConnections, isResourceNotFoundError, isServerError } from 'modules/sphinx/connectionModel';
import { Connection, ConnectionFindInput, Definition } from 'modules/sphinx/sphinxInterfaces';
import { getDestinationName, getSourceName } from 'modules/sphinx/definitionModel';
import '../connection.less';
import log from 'modules/shared/logger';
import { useSelector } from 'react-redux';
import { AppState } from 'modules/state';
import { i18n } from '../../../i18n';
import { ApiKeyView } from './ApiKeyView';

const ConnectionView: React.FC = () => {
  const user = useSelector((state: AppState) => state.auth.currentUser);
  const { accountMembership } = useSelector((state: AppState) => state.session);
  // const { accountId, siteId } = accountMembership || { accountId: null, siteId: null };
  let { accountId, siteId } = accountMembership || { accountId: null, siteId: null };

  // This is a workaround for `user.siteId` being set, but `siteId` not being set in `accountMembership`.
  // Since the `User` interface does not have a top level `siteId` property, the cloning to an `any`-typed
  // object is necessary to access the `siteId`.
  if (!siteId) {
    const userClone: any = { ...user };
    if (userClone?.siteId) {
      siteId = userClone.siteId;
    }
  }

  const findInput: ConnectionFindInput = { accountId: accountId || '', siteId: siteId || '', userId: user?.id || '' };

  const connectionFindQuery = useQuery(SphinxApi.query.connectionFind, {
    variables: { data: findInput },

    // Prevent query result caching so that updates and removals of Connections
    // are reflected in this view.
    fetchPolicy: 'no-cache',
  });
  log.debug(JSON.stringify(connectionFindQuery.data, null, 2));

  const definitionListQuery = useQuery(SphinxApi.query.definitionList);

  let content: React.ReactNode;

  if (connectionFindQuery.loading || definitionListQuery.loading) {
    content = <Spinner />;
  }

  if (connectionFindQuery.error) {
    // A 404 is expected if the user does not have any enabled Connections
    if (!isResourceNotFoundError(connectionFindQuery.error)) {
      Errors.handle(connectionFindQuery.error);
    }
  }

  if (definitionListQuery.error) {
    // A 500 is expected if SPHINX is down
    if (!isServerError(definitionListQuery.error)) {
      Errors.handle(definitionListQuery.error);
    }
  }

  const renderDestinations = (connection: Connection): React.ReactNode => {
    if (!Array.isArray(connection.destinationIds)) {
      return null;
    }

    const definitions: Definition[] = definitionListQuery.data?.definitionList;
    if (!definitions || !definitions.length) {
      return null;
    }

    return connection.destinationIds.map((destinationId: string) => (
      <div key={destinationId} className="destination-item">
        <span className="destination-name">{getDestinationName(destinationId, definitions)}</span>
      </div>
    ));
  };

  const getConnections = (): Connection[] => {
    const connections: Connection[] = connectionFindQuery.data?.connectionFind;
    if (!connections || !connections.length) {
      return [];
    }
    return connections;
  };

  const renderConnection = (connection: Connection, index: number): React.ReactNode => {
    const definitions: Definition[] = definitionListQuery.data?.definitionList;
    const sourceName: string = getSourceName(connection.sourceId, definitions);

    return (
      <section key={connection.id} className="existing-connection" data-connection-id={connection.id}>
        <Card>
          <header>
            <h3>
              {i18n('entities.connection.labelSingular')} {index + 1}
            </h3>
            <div className="disabled-indicator">{!connection.enabled && i18n('entities.connection.edit.disabled')}</div>
            <Link to={`/settings/connection/${connection.id}/edit`} className="edit">
              {i18n('entities.connection.edit.label')}
            </Link>
          </header>
          <div className="source">
            <h3>{i18n('entities.connection.source')}</h3>
            <span className="source-name">{sourceName}</span>
          </div>
          <div className="destinations">
            <h3>{i18n('entities.connection.destinations')}</h3>
            {renderDestinations(connection)}
          </div>
        </Card>
      </section>
    );
  };

  const renderConnections = (connections: Connection[]): React.ReactNode => {
    if (!connections.length) {
      return <div className="empty-list">{i18n('entities.connection.noConnectionsExist')}</div>;
    }

    return connections.map((connection: Connection, index: number) => renderConnection(connection, index));
  };

  const renderApiKey = (apiKey: string): React.ReactNode => {
    if (!apiKey) {
      return <p className="no-key">{i18n('entities.connection.apiKey.noKeyExists')}</p>;
    }

    return <ApiKeyView apiKey={apiKey} />;
  };

  const connections: Connection[] = getConnections();
  const apiKey: string = getApiKeyFromConnections(connections);

  content = (
    <div className="connection-view">
      <div className="api-key">
        <h3>{i18n('entities.connection.apiKey.label')}</h3>
        {renderApiKey(apiKey)}
      </div>
      <hr />
      <div className="connections">
        <h3>{i18n('entities.connection.labelPlural')}</h3>
        <Link to="/settings/connection/new">
          <Button type="primary" className="create">
            {i18n('entities.connection.create.title')}
          </Button>
        </Link>
        {renderConnections(connections)}
      </div>
    </div>
  );

  return (
    <section className="settings-section connections">
      <h2>{i18n('entities.connection.title')}</h2>
      {content}
    </section>
  );
};

export default ConnectionView;
