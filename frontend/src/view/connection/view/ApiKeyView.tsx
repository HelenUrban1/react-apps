import React from 'react';
import { useMutation } from '@apollo/client';
import { useHistory } from 'react-router-dom';
import { CopyToClipboard } from 'react-copy-to-clipboard';
import { Button, Popconfirm, Tooltip } from 'antd';
import Message from 'view/shared/message';
import Errors from 'modules/shared/error/errors';
import SphinxApi from 'modules/sphinx/sphinxApi';
import { i18n } from '../../../i18n';
import '../connection.less';

interface Props {
  apiKey: string;
}

export const ApiKeyView: React.FC<Props> = ({ apiKey }) => {
  const history = useHistory();

  const [rotateApiKey] = useMutation(SphinxApi.mutate.apiKeyRotate, {
    onCompleted: () => {
      Message.success(i18n('entities.connection.apiKey.rotateSuccess'));
      history.push('/settings/connection');
    },
    onError: (error) => {
      Errors.handle(error);
    },
  });

  const makeRotateRequest = () => {
    rotateApiKey({ variables: { data: { apiKey } } });
  };

  if (!apiKey) {
    return null;
  }

  return (
    <div className="api-key-view">
      <div>
        <span className="key-value">{apiKey}</span>
        <CopyToClipboard text={apiKey} onCopy={() => Message.success(i18n('entities.connection.apiKey.keyCopiedToClipboard'))}>
          <Button className="copy">{i18n('entities.connection.apiKey.copyToClipboard')}</Button>
        </CopyToClipboard>
        <Popconfirm title={i18n('entities.connection.apiKey.rotateConfirm')} onConfirm={() => makeRotateRequest()} okText={i18n('common.yes')} cancelText={i18n('common.no')}>
          <Tooltip title={i18n('entities.connection.apiKey.rotateTooltip')} placement="bottom">
            <Button className="rotate">{i18n('entities.connection.apiKey.rotate')}</Button>
          </Tooltip>
        </Popconfirm>
      </div>
    </div>
  );
};

export default ApiKeyView;
