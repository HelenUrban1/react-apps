import React from 'react';
import ConnectionNewView from './ConnectionNewView';

export default function ConnectionNewPage(props) {
  return <ConnectionNewView {...props} />;
}
