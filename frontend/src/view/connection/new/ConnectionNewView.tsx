import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';
import { useQuery, useMutation } from '@apollo/client';
import { Button, Radio, Space, Steps } from 'antd';
import { AppState } from 'modules/state';
import { useSelector } from 'react-redux';
import Message from 'view/shared/message';
import Spinner from 'view/shared/Spinner';
import Errors from 'modules/shared/error/errors';
import SphinxApi from 'modules/sphinx/sphinxApi';
import { Source, Definition, Destination, DestinationCredential } from 'modules/sphinx/sphinxInterfaces';
import { getDestinationDefinitions, getDestinationName, getSourceDefinitions, isNetInspectDestination } from 'modules/sphinx/definitionModel';
import { destinationSelectChangeHandler } from 'modules/sphinx/connectionModel';
import AddDestinations from '../shared/AddDestinations';
import { i18n } from '../../../i18n';
import '../connection.less';

interface CreateConnectionStep {
  title: string;
  content: React.ReactNode;
}

const ConnectionNewView: React.FC = () => {
  const history = useHistory();
  const [currentStep, setCurrentStep] = useState<number>(0);
  const [sourceSelectionValue, setSourceSelectionValue] = useState<string>();

  const [selectedDestinationIds, setSelectedDestinationIds] = useState<string[]>([]);

  const [netInspectUserId, setNetInspectUserId] = useState<string>('');
  const [netInspectPassword, setNetInspectPassword] = useState<string>('');
  const [netInspectCompany, setNetInspectCompany] = useState<string>('');
  const [netInspectCredentialsAllDisabled, setNetInspectCredentialsAllDisabled] = useState<boolean>(true);

  const user = useSelector((state: AppState) => state.auth.currentUser);
  const { accountMembership } = useSelector((state: AppState) => state.session);
  // const { accountId, siteId } = accountMembership || { accountId: null, siteId: null };
  let { accountId, siteId } = accountMembership || { accountId: null, siteId: null };

  // This is a workaround for `user.siteId` being set, but `siteId` not being set in `accountMembership`.
  // Since the `User` interface does not have a top level `siteId` property, the cloning to an `any`-typed
  // object is necessary to access the `siteId`.
  if (!siteId) {
    const userClone: any = { ...user };
    if (userClone?.siteId) {
      siteId = userClone.siteId;
    }
  }

  const definitionListQuery = useQuery(SphinxApi.query.definitionList);

  const definitions: Definition[] = definitionListQuery.data?.definitionList;

  const sources: Source[] = getSourceDefinitions(definitions);
  const destinations: Destination[] = getDestinationDefinitions(definitions);

  let content: React.ReactNode;

  if (definitionListQuery.loading) {
    content = <Spinner />;
  }

  if (definitionListQuery.error) {
    Errors.handle(definitionListQuery.error);
  }

  const [createConnection] = useMutation(SphinxApi.mutate.connectionCreate, {
    onCompleted: () => {
      Message.success(i18n('entities.connection.create.success'));
      history.push('/settings/connection');
    },
    onError: (error) => {
      Errors.handle(error);
    },
  });

  const makeCreateRequest = () => {
    const credentials: DestinationCredential[] = [];
    const destinationIds: string[] = Array.from(new Set(selectedDestinationIds));

    destinationIds.forEach((id: string) => {
      if (isNetInspectDestination(id, destinations)) {
        credentials.push({
          destinationId: id,
          userName: netInspectUserId,
          password: netInspectPassword,
          company: netInspectCompany,
        });
      }
    });

    createConnection({
      variables: {
        data: {
          accountId,
          siteId,
          userId: user?.id,
          sourceId: sourceSelectionValue,
          destinationIds,
          destinationCredentials: credentials,
        },
      },
    });
  };

  const nextStep = () => setCurrentStep(currentStep + 1);
  const prevStep = () => setCurrentStep(currentStep - 1);

  if (!definitions) {
    return null;
  }
  if (!sources.length || !destinations.length) {
    return null;
  }

  const selectedDefinitionName = (itemId: string | undefined): string => {
    const item = definitions.find((definition: Definition) => itemId === definition.id);
    return item ? item.name : '';
  };

  const renderSources = (sourcesToRender: Source[]): React.ReactNode => (
    <Radio.Group name="source-radio-group" value={sourceSelectionValue} onChange={(e) => setSourceSelectionValue(e.target.value)}>
      <Space direction="vertical">
        {sourcesToRender.map((source: Source) => (
          <Radio value={source.id} key={source.id}>
            {source.name}
          </Radio>
        ))}
      </Space>
    </Radio.Group>
  );

  const onDestinationSelectChange = (destinationId: string, selected: boolean) => {
    destinationSelectChangeHandler(selectedDestinationIds, setSelectedDestinationIds, destinationId, selected);
  };

  const renderDestinations = (destinationsToRender: Destination[]): React.ReactNode => (
    <AddDestinations
      destinations={destinationsToRender}
      netInspectUserId={netInspectUserId}
      netInspectPassword={netInspectPassword}
      netInspectCompany={netInspectCompany}
      netInspectCredentialsAllDisabled={netInspectCredentialsAllDisabled}
      onDestinationSelectChange={onDestinationSelectChange}
      selectedDestinationIds={selectedDestinationIds}
      setNetInspectUserId={setNetInspectUserId}
      setNetInspectPassword={setNetInspectPassword}
      setNetInspectCompany={setNetInspectCompany}
      setNetInspectCredentialsAllDisabled={setNetInspectCredentialsAllDisabled}
    />
  );

  const createSteps: CreateConnectionStep[] = [
    {
      title: i18n('entities.connection.create.stepSelectSource.title'),
      content: (
        <div className="source-add">
          <div className="instructions">{i18n('entities.connection.create.stepSelectSource.instructions')}</div>
          <div>{renderSources(sources)}</div>
        </div>
      ),
    },
    {
      title: i18n('entities.connection.create.stepConfigureDestinations.title'),
      content: (
        <div className="configure-destinations">
          <div className="instructions">{i18n('entities.connection.create.stepConfigureDestinations.instructions')}</div>
          {renderDestinations(destinations)}
        </div>
      ),
    },
    {
      title: i18n('entities.connection.create.stepReviewSelections.title'),
      content: (
        <div className="review-selections">
          <div className="instructions">{i18n('entities.connection.create.stepReviewSelections.instructions')}</div>
          <div className="source">
            {i18n('entities.connection.source')}:<span className="selection">{selectedDefinitionName(sourceSelectionValue)}</span>
          </div>
          <div className="destinations">
            {i18n('entities.connection.destination')}:
            <ul>
              {selectedDestinationIds.map((id: string) => (
                <li key={id} className="selection">
                  {getDestinationName(id, definitions)}
                </li>
              ))}
            </ul>
          </div>
        </div>
      ),
    },
  ];

  const nextDisabled = (): boolean => {
    // Source selection
    if (currentStep === 0) {
      return !sourceSelectionValue;
    }

    // Destination configuration
    if (currentStep === 1) {
      if (!selectedDestinationIds.length) {
        return true;
      }

      if (selectedDestinationIds.find((id: string) => isNetInspectDestination(id, destinations))) {
        return !netInspectUserId || !netInspectPassword || !netInspectCompany;
      }
    }

    return false;
  };

  content = (
    <div className="connection-create">
      <Steps current={currentStep}>
        {createSteps.map((item: CreateConnectionStep) => (
          <Steps.Step key={item.title} title={item.title} />
        ))}
      </Steps>
      <div className="steps-content">{createSteps[currentStep].content}</div>
      <div className="steps-action">
        {currentStep < createSteps.length - 1 && (
          <Button type="primary" disabled={nextDisabled()} onClick={() => nextStep()}>
            Next
          </Button>
        )}
        {currentStep === createSteps.length - 1 && (
          <Button type="primary" onClick={() => makeCreateRequest()}>
            Done
          </Button>
        )}
        {currentStep > 0 && (
          <Button style={{ margin: '0 8px' }} onClick={() => prevStep()}>
            Previous
          </Button>
        )}
      </div>
    </div>
  );

  return (
    <section className="settings-section connections">
      <h2>{i18n('entities.connection.create.title')}</h2>
      {content}
    </section>
  );
};

export default ConnectionNewView;
