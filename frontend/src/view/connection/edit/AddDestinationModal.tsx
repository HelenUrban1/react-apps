import React from 'react';
import { Modal } from 'antd';
import { i18n } from '../../../i18n';
import { Destination } from 'modules/sphinx/sphinxInterfaces';
import NetInspectCredentialsInputs, { NetInspectCredentialsInputsProps } from '../shared/netInspectDestinationCredentialsInputs';
import '../connection.less';

interface Props extends NetInspectCredentialsInputsProps {
  destination: Destination;
  visible: boolean;
  setVisible: (destinationId: string) => void;
  isNetInspect: boolean;
  addDestinationHandler: (destination: Destination) => void;
}

export const AddDestinationModal: React.FC<Props> = (props: Props) => {
  const { destination, visible, setVisible, isNetInspect, addDestinationHandler, setNetInspectUserId, setNetInspectPassword, setNetInspectCompany, netInspectUserId = '', netInspectPassword = '', netInspectCompany = '' } = props;

  const renderNetInspectCredentials = (): React.ReactNode => (
    <NetInspectCredentialsInputs
      setNetInspectUserId={setNetInspectUserId}
      setNetInspectPassword={setNetInspectPassword}
      setNetInspectCompany={setNetInspectCompany}
      netInspectUserId={netInspectUserId}
      netInspectPassword={netInspectPassword}
      netInspectCompany={netInspectCompany}
    />
  );

  const disableOkButton = (): boolean => {
    return isNetInspect && (!netInspectUserId || !netInspectPassword || !netInspectCompany);
  };

  return (
    <Modal visible={visible} title={i18n('entities.connection.connectDestination')} onCancel={() => setVisible('')} onOk={() => addDestinationHandler(destination)} okButtonProps={{ disabled: disableOkButton() }}>
      <h3>{destination.name}</h3>
      <div>{isNetInspect && renderNetInspectCredentials()}</div>
    </Modal>
  );
};
