import React, { useState } from 'react';
import { useMutation } from '@apollo/client';
import { Button } from 'antd';
import Errors from 'modules/shared/error/errors';
import Message from 'view/shared/message';
import SphinxApi from 'modules/sphinx/sphinxApi';
import { Connection, Destination, DestinationCredential } from 'modules/sphinx/sphinxInterfaces';
import { isNetInspectDestination } from 'modules/sphinx/definitionModel';
import { getCredentialsForDestination } from 'modules/sphinx/connectionModel';
import NetInspectDestinationCredentialsInputs from '../shared/netInspectDestinationCredentialsInputs';
import { i18n } from '../../../i18n';
import '../connection.less';

interface Props {
  connection: Connection;
  destination: Destination;
  setConnection: (data: any) => void;
  netInspectCredentialsAllDisabled: boolean;
  setNetInspectCredentialsAllDisabled: (state: boolean) => void;
}

export const ConfiguredDestination: React.FC<Props> = (props) => {
  const { connection, destination, setConnection, netInspectCredentialsAllDisabled, setNetInspectCredentialsAllDisabled } = props;

  const credentials: DestinationCredential | undefined = getCredentialsForDestination(connection, destination.id);

  const [destinationUserId, setDestinationUserId] = useState<string>(credentials ? credentials.userName : '');
  const [destinationPassword, setDestinationPassword] = useState<string>(credentials ? credentials.password : '');
  const [destinationCompany, setDestinationCompany] = useState<string>(credentials ? credentials.company : '');

  const [updateConnection] = useMutation(SphinxApi.mutate.connectionUpdate, {
    onCompleted: (data) => {
      setConnection(data.connectionUpdate);
      Message.success(i18n('entities.connection.edit.updateSuccess'));
    },
    onError: (error) => {
      Errors.handle(error);
    },
  });

  const disconnectDestination = () => {
    const existingDestinationIds: string[] = Array.isArray(connection.destinationIds) ? connection.destinationIds : [];
    const updatedDestinationIds: string[] = existingDestinationIds.filter((destinationId: string) => {
      return destinationId !== destination.id;
    });

    updateConnection({
      variables: {
        data: {
          connectionId: connection.id,
          destinationIds: updatedDestinationIds,
        },
      },
    });
  };

  const saveCredentialsChanges = (updatedCredentials: DestinationCredential) => {
    updateConnection({
      variables: {
        data: {
          connectionId: connection.id,
          destinationCredentials: updatedCredentials,
        },
      },
    });
  };

  const renderDestinationCredentials = (destination: Destination, connection: Connection): React.ReactNode => {
    if (!connection.destinationCredentials) {
      return null;
    }

    let inputs: React.ReactNode;
    let updatedCredentials: DestinationCredential;

    if (isNetInspectDestination(destination.id, [destination])) {
      inputs = (
        <NetInspectDestinationCredentialsInputs
          setNetInspectUserId={setDestinationUserId}
          setNetInspectPassword={setDestinationPassword}
          setNetInspectCompany={setDestinationCompany}
          netInspectUserId={destinationUserId}
          netInspectPassword={destinationPassword}
          netInspectCompany={destinationCompany}
          netInspectCredentialsAllDisabled={netInspectCredentialsAllDisabled}
        />
      );

      updatedCredentials = {
        destinationId: destination.id,
        userName: destinationUserId,
        password: destinationPassword,
        company: destinationCompany,
      };
    }

    if (!inputs) {
      return null;
    }

    return (
      <>
        {inputs}
        <div className="save-container">
          <Button type="primary" className="save-credentials-changes" disabled={netInspectCredentialsAllDisabled} onClick={() => saveCredentialsChanges(updatedCredentials)}>
            {i18n('common.save')}
          </Button>
        </div>
      </>
    );
  };

  return (
    <div className="connected-destination">
      <div className="heading">
        <h4>{destination.name}</h4>
        {credentials !== undefined && (
          <Button className="edit-credentials" type="link" onClick={() => setNetInspectCredentialsAllDisabled(false)}>
            {i18n('entities.connection.credentials.edit')}
          </Button>
        )}
        <Button className="disconnect" type="link" onClick={() => disconnectDestination()}>
          {i18n('entities.connection.edit.disconnect')}
        </Button>
      </div>
      <div className="credentials">{renderDestinationCredentials(destination, connection)}</div>
    </div>
  );
};
