import React from 'react';
import { matchPath, useHistory } from 'react-router-dom';
import { useQuery } from '@apollo/client';
import Spinner from 'view/shared/Spinner';
import Errors from 'modules/shared/error/errors';
import { ConnectionEditView } from './ConnectionEditView';
import SphinxApi from 'modules/sphinx/sphinxApi';
import { Connection, Definition } from 'modules/sphinx/sphinxInterfaces';

export default function ConnectionEditPage() {
  const history = useHistory();
  const idParamMatch = matchPath<{ id: 'string' }>(history.location.pathname, {
    path: '/settings/connection/:id/edit',
  });

  if (!idParamMatch || !idParamMatch.params.id) {
    Errors.handle(new Error('Unable to locate Connection'));
  }

  const id = idParamMatch && idParamMatch.params.id;

  const connectionFindQuery = useQuery(SphinxApi.query.connectionFindById, {
    variables: { data: { connectionId: id } },
    fetchPolicy: 'no-cache',
  });

  const definitionListQuery = useQuery(SphinxApi.query.definitionList);

  if (connectionFindQuery.loading || definitionListQuery.loading) {
    return <Spinner />;
  }

  if (connectionFindQuery.error) {
    Errors.handle(connectionFindQuery.error);
  }

  if (definitionListQuery.error) {
    Errors.handle(definitionListQuery.error);
  }

  if (connectionFindQuery.data && definitionListQuery.data) {
    const connection: Connection = connectionFindQuery.data.connectionFindById;
    const definitions: Definition[] = definitionListQuery.data.definitionList;

    return <ConnectionEditView providedConnection={connection} definitions={definitions} />;
  }

  return <>Unable to edit connection</>;
}
