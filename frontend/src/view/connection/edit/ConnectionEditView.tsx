import React, { useState } from 'react';
import { useMutation } from '@apollo/client';
import { Link, useHistory } from 'react-router-dom';
import { Button, Card, Popconfirm } from 'antd';
import Errors from 'modules/shared/error/errors';
import Message from 'view/shared/message';
import SphinxApi from 'modules/sphinx/sphinxApi';
import { Connection, Definition, Destination, DestinationCredential } from 'modules/sphinx/sphinxInterfaces';
import { addDestinationIds, getCredentialsForDestination } from 'modules/sphinx/connectionModel';
import { getDestination, getDestinationDefinitions, getSourceName, isNetInspectDestination } from 'modules/sphinx/definitionModel';
import { ConfiguredDestination } from './ConfiguredDestination';
import { AddDestinationModal } from './AddDestinationModal';
import { i18n } from '../../../i18n';
import '../connection.less';

interface Props {
  providedConnection: Connection;
  definitions: Definition[];
}

export const ConnectionEditView: React.FC<Props> = ({ providedConnection, definitions }) => {
  const history = useHistory();
  const [connection, setConnection] = useState<Connection>(providedConnection);

  const [showModalAddDestinationForId, setShowModalAddDestinationForId] = useState<string>('');

  const [netInspectUserId, setNetInspectUserId] = useState<string>('');
  const [netInspectPassword, setNetInspectPassword] = useState<string>('');
  const [netInspectCompany, setNetInspectCompany] = useState<string>('');

  const [editNetInspectCredentialsAllDisabled, setEditNetInspectCredentialsAllDisabled] = useState<boolean>(true);

  const destinations: Destination[] = getDestinationDefinitions(definitions);

  let content: React.ReactNode;

  const [updateConnection] = useMutation(SphinxApi.mutate.connectionUpdate, {
    onCompleted: (data) => {
      setConnection(data.connectionUpdate);
      Message.success(i18n('entities.connection.edit.updateSuccess'));
    },
    onError: (error) => {
      Errors.handle(error);
    },
  });

  const [deleteConnection] = useMutation(SphinxApi.mutate.connectionDelete, {
    onCompleted: () => {
      Message.success(i18n('entities.connection.deleteSuccess'));
      history.push('/settings/connection');
    },
    onError: (error) => {
      Errors.handle(error);
    },
  });

  const makeDeleteConnectionRequest = () => {
    deleteConnection({
      variables: {
        data: { connectionId: connection.id },
      },
    });
  };

  const makeConnectDestinationsRequest = (destination: Destination) => {
    const destinationIds: string[] = addDestinationIds(connection, [destination.id]);
    const allCredentials: DestinationCredential[] = [];

    destinationIds.forEach((destinationId: string) => {
      if (destinationId === destination.id && isNetInspectDestination(destinationId, destinations)) {
        allCredentials.push({
          destinationId: destinationId,
          userName: netInspectUserId,
          password: netInspectPassword,
          company: netInspectCompany,
        });

        return;
      }

      // The Destination being added cannot be the only one processed here, or existing
      // destination credentials already set on the Connection would be lost.
      const credentials: DestinationCredential | undefined = getCredentialsForDestination(connection, destinationId);
      if (credentials) {
        allCredentials.push(credentials);
      }
    });

    updateConnection({
      variables: {
        data: {
          connectionId: connection.id,
          destinationIds,
          destinationCredentials: allCredentials,
        },
      },
    });
  };

  const toggleEnabledState = () => {
    updateConnection({
      variables: {
        data: {
          connectionId: connection.id,
          enabled: !connection.enabled,
        },
      },
    });
  };

  const getAvailableDestinations = (): Destination[] => {
    const configuredDestinationIds: string[] = connection.destinationIds || [];

    return destinations.filter((destination: Destination) => !configuredDestinationIds.includes(destination.id));
  };

  const renderConfiguredDestinations = (): React.ReactNode => {
    if (!connection.destinationIds) {
      return null;
    }

    return connection.destinationIds.map((destinationId: string) => {
      const destination: Destination | undefined = getDestination(destinationId, definitions);
      if (!destination) {
        return null;
      }

      return (
        <ConfiguredDestination
          key={destination.id}
          connection={connection}
          destination={destination}
          setConnection={setConnection}
          netInspectCredentialsAllDisabled={editNetInspectCredentialsAllDisabled}
          setNetInspectCredentialsAllDisabled={setEditNetInspectCredentialsAllDisabled}
        />
      );
    });
  };

  const renderAvailableDestinationButtons = (): React.ReactNode => {
    const availableDestinations: Destination[] = getAvailableDestinations();
    return availableDestinations.map((destination: Destination) => (
      <Button key={destination.id} className="connect-destination" onClick={() => setShowModalAddDestinationForId(destination.id)}>
        {i18n('common.add')} {destination.name}
      </Button>
    ));
  };

  content = (
    <div className="connection-edit">
      <div className="back">
        <Link to="/settings/connection">{i18n('entities.connection.backToManager')}</Link>
      </div>
      <div className="state-controls">
        <Button className="toggle-enabled" onClick={() => toggleEnabledState()}>
          {connection.enabled ? i18n('entities.connection.edit.disable') : i18n('entities.connection.edit.enable')}
        </Button>
        <Popconfirm title={i18n('common.areYouSure')} onConfirm={() => makeDeleteConnectionRequest()} okText={i18n('common.yes')} cancelText={i18n('common.no')}>
          <Button className="delete">{i18n('entities.connection.deleteLabel')}</Button>
        </Popconfirm>
      </div>
      <section className="source">
        <Card>
          <h3>{i18n('entities.connection.source')}</h3>
          <h4>{getSourceName(connection.sourceId, definitions)}</h4>
        </Card>
      </section>
      <section className="destinations">
        <Card>
          <div className="heading">
            <h3>{i18n('entities.connection.destinations')}</h3>
            {renderAvailableDestinationButtons()}
          </div>
          {renderConfiguredDestinations()}
          {getAvailableDestinations().map((destination: Destination) => (
            <AddDestinationModal
              destination={destination}
              visible={showModalAddDestinationForId === destination.id}
              setVisible={setShowModalAddDestinationForId}
              isNetInspect={isNetInspectDestination(destination.id, destinations)}
              addDestinationHandler={makeConnectDestinationsRequest}
              netInspectUserId={netInspectUserId}
              netInspectPassword={netInspectPassword}
              netInspectCompany={netInspectCompany}
              setNetInspectUserId={setNetInspectUserId}
              setNetInspectPassword={setNetInspectPassword}
              setNetInspectCompany={setNetInspectCompany}
            />
          ))}
        </Card>
      </section>
    </div>
  );

  return (
    <section className="settings-section connections">
      <h2>{i18n('entities.connection.edit.title')}</h2>
      {content}
    </section>
  );
};
