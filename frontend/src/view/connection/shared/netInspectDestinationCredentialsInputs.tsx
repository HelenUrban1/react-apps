import React from 'react';
import { Input } from 'antd';

export interface NetInspectCredentialsInputsProps {
  setNetInspectUserId: (value: string) => void;
  setNetInspectPassword: (value: string) => void;
  setNetInspectCompany: (value: string) => void;
  netInspectUserId: string;
  netInspectPassword: string;
  netInspectCompany: string;
  netInspectCredentialsAllDisabled?: boolean;
}

const NetInspectCredentialsInputs: React.FC<NetInspectCredentialsInputsProps> = (props) => {
  const { setNetInspectUserId, setNetInspectPassword, setNetInspectCompany, netInspectUserId = '', netInspectPassword = '', netInspectCompany = '', netInspectCredentialsAllDisabled = false } = props;

  return (
    <div className="destination-credentials">
      <div>
        <Input placeholder="User ID" onChange={(e) => setNetInspectUserId(e.target.value)} value={netInspectUserId} disabled={netInspectCredentialsAllDisabled} />
      </div>
      <div>
        <Input.Password placeholder="Password" onChange={(e) => setNetInspectPassword(e.target.value)} value={netInspectPassword} disabled={netInspectCredentialsAllDisabled} />
      </div>
      <div>
        <Input placeholder="Company Name" onChange={(e) => setNetInspectCompany(e.target.value)} value={netInspectCompany} disabled={netInspectCredentialsAllDisabled} />
      </div>
    </div>
  );
};

export default NetInspectCredentialsInputs;
