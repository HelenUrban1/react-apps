import React from 'react';
import { Checkbox } from 'antd';
import { Destination } from 'modules/sphinx/sphinxInterfaces';
import { isNetInspectDestination } from 'modules/sphinx/definitionModel';
import NetInspectCredentialsInputs, { NetInspectCredentialsInputsProps } from '../shared/netInspectDestinationCredentialsInputs';
import '../connection.less';

interface Props extends NetInspectCredentialsInputsProps {
  destinations: Destination[];
  selectedDestinationIds: string[];
  onDestinationSelectChange?: (destinationId: string, selected: boolean) => void;
  setNetInspectCredentialsAllDisabled: (value: boolean) => void;
}

const AddDestinations: React.FC<Props> = (props) => {
  const {
    destinations,
    netInspectUserId = '',
    netInspectPassword = '',
    netInspectCompany = '',
    netInspectCredentialsAllDisabled = false,
    onDestinationSelectChange,
    selectedDestinationIds = [],
    setNetInspectUserId,
    setNetInspectPassword,
    setNetInspectCompany,
    setNetInspectCredentialsAllDisabled,
  } = props;

  const onSelectChange = (selected: boolean, destinationId: string) => {
    if (isNetInspectDestination(destinationId, destinations)) {
      setNetInspectUserId('');
      setNetInspectPassword('');
      setNetInspectCompany('');

      setNetInspectCredentialsAllDisabled(!selected);
    }

    onDestinationSelectChange && onDestinationSelectChange(destinationId, selected);
  };

  const content: React.ReactNode = destinations.map((destination: Destination) => {
    let credentials: React.ReactNode;

    if (isNetInspectDestination(destination.id, destinations)) {
      credentials = (
        <NetInspectCredentialsInputs
          setNetInspectUserId={setNetInspectUserId}
          setNetInspectPassword={setNetInspectPassword}
          setNetInspectCompany={setNetInspectCompany}
          netInspectUserId={netInspectUserId}
          netInspectPassword={netInspectPassword}
          netInspectCompany={netInspectCompany}
          netInspectCredentialsAllDisabled={netInspectCredentialsAllDisabled}
        />
      );
    }

    return (
      <div className="destination" key={destination.id}>
        <Checkbox checked={selectedDestinationIds.includes(destination.id)} onChange={(e) => onSelectChange(e.target.checked, destination.id)}>
          {destination.name}
        </Checkbox>
        {credentials}
      </div>
    );
  });

  return <div>{content}</div>;
};

export default AddDestinations;
