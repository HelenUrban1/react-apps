import { DeleteOutlined, EditOutlined, FileSearchOutlined } from '@ant-design/icons';
import { Button, Popconfirm } from 'antd';
import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { i18n } from '../../../i18n';
import Toolbar from 'view/shared/styles/Toolbar';
import { connect } from 'react-redux';
import accountMemberSelectors from 'modules/accountMember/accountMemberSelectors';
import destroySelectors from 'modules/accountMember/destroy/accountMemberDestroySelectors';
import destroyActions from 'modules/accountMember/destroy/accountMemberDestroyActions';
import auditLogSelectors from 'modules/auditLog/auditLogSelectors';

class AccountMemberViewToolbar extends Component {
  id = () => {
    return this.props.match.params.id;
  };

  doDestroy = () => {
    const { dispatch } = this.props;
    dispatch(destroyActions.doDestroy(this.id()));
  };

  render() {
    const { hasPermissionToEdit, hasPermissionToAuditLogs, hasPermissionToDestroy, destroyLoading } = this.props;

    return (
      <Toolbar>
        {hasPermissionToEdit && (
          <Link to={`/account-member/${this.id()}/edit`}>
            <Button type="primary" icon={<EditOutlined />}>
              {i18n('common.edit')}
            </Button>
          </Link>
        )}

        {hasPermissionToDestroy && (
          <Popconfirm title={i18n('common.areYouSure')} onConfirm={() => this.doDestroy()} okText={i18n('common.yes')} cancelText={i18n('common.no')}>
            <Button type="primary" icon={<DeleteOutlined />} disabled={destroyLoading}>
              {i18n('common.destroy')}
            </Button>
          </Popconfirm>
        )}

        {hasPermissionToAuditLogs && (
          <Link to={`/audit-logs?entityId=${encodeURIComponent(this.id())}`}>
            <Button icon={<FileSearchOutlined />}>{i18n('auditLog.menu')}</Button>
          </Link>
        )}
      </Toolbar>
    );
  }
}

function select(state) {
  return {
    hasPermissionToAuditLogs: auditLogSelectors.selectPermissionToRead(state),
    hasPermissionToEdit: accountMemberSelectors.selectPermissionToEdit(state),
    hasPermissionToDestroy: accountMemberSelectors.selectPermissionToDestroy(state),
    destroyLoading: destroySelectors.selectLoading(state),
  };
}

export default connect(select)(AccountMemberViewToolbar);
