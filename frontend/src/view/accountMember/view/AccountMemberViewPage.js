import React, { Component } from 'react';
import ContentWrapper from 'view/layout/styles/ContentWrapper';
import PageTitle from 'view/shared/styles/PageTitle';
import Breadcrumb from 'view/shared/Breadcrumb';
import AccountMemberView from 'view/accountMember/view/AccountMemberView';
import { i18n } from '../../../i18n';
import actions from 'modules/accountMember/view/accountMemberViewActions';
import { connect } from 'react-redux';
import selectors from 'modules/accountMember/view/accountMemberViewSelectors';
import AccountMemberViewToolbar from 'view/accountMember/view/AccountMemberViewToolbar';

class AccountMemberPage extends Component {
  componentDidMount() {
    const { dispatch, match } = this.props;
    dispatch(actions.doFind(match.params.id));
  }

  render() {
    return (
      <React.Fragment>
        <Breadcrumb items={[[i18n('home.menu'), '/'], [i18n('entities.accountMember.menu'), '/account-member'], [i18n('entities.accountMember.view.title')]]} />

        <ContentWrapper>
          <PageTitle>{i18n('entities.accountMember.view.title')}</PageTitle>

          <AccountMemberViewToolbar match={this.props.match} />

          <AccountMemberView loading={this.props.loading} record={this.props.record} />
        </ContentWrapper>
      </React.Fragment>
    );
  }
}

function select(state) {
  return {
    loading: selectors.selectLoading(state),
    record: selectors.selectRecord(state),
  };
}

export default connect(select)(AccountMemberPage);
