import { Tooltip } from 'antd';
import model from 'modules/accountMember/accountMemberModel';
import React, { Component } from 'react';
import Spinner from 'view/shared/Spinner';
import ViewWrapper from 'view/shared/styles/ViewWrapper';
import TextViewItem from 'view/shared/view/TextViewItem';
import UserViewItem from 'view/iam/view/UserViewItem';
import AccountViewItem from 'view/account/view/AccountViewItem';
import CustomViewItem from 'view/shared/view/CustomViewItem';

const { fields } = model;

class AccountMemberView extends Component {
  renderView() {
    const { record } = this.props;

    return (
      <ViewWrapper>
        <TextViewItem label={fields.id.label} value={fields.id.forView(record.id)} />

        <AccountViewItem label={fields.account.label} value={fields.account.forView(record.account)} />

        <UserViewItem label={fields.user.label} value={fields.user.forView(record.user)} />

        <TextViewItem label={fields.settings.label} value={fields.settings.forView(record.settings)} />

        <CustomViewItem
          label={fields.roles.label}
          value={record.roles}
          render={(value) =>
            value.map((role) => (
              <div key={role}>
                <Tooltip title={model.descriptionOfRole(role)}>
                  <span>{model.labelOfRole(role)}</span>
                </Tooltip>
              </div>
            ))
          }
        />

        <TextViewItem label={fields.status.label} value={fields.status.forView(record.status)} />

        <TextViewItem label={fields.createdAt.label} value={fields.createdAt.forView(record.createdAt)} />

        <TextViewItem label={fields.updatedAt.label} value={fields.updatedAt.forView(record.updatedAt)} />
      </ViewWrapper>
    );
  }

  render() {
    const { record, loading } = this.props;

    if (loading || !record) {
      return <Spinner />;
    }

    return this.renderView();
  }
}

export default AccountMemberView;
