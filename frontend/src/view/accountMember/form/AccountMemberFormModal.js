import React, { Component } from 'react';
import { Modal } from 'antd';
import { i18n } from '../../../i18n';
import AccountMemberForm from 'view/accountMember/form/AccountMemberForm';
import AccountMemberService from 'modules/accountMember/accountMemberService';
import Errors from 'modules/shared/error/errors';

class AccountMemberFormModal extends Component {
  state = {
    saveLoading: false,
  };

  doSubmit = async (_, data) => {
    try {
      this.setState({
        saveLoading: true,
      });
      const { id } = await AccountMemberService.create(data);
      const record = await AccountMemberService.find(id);
      this.props.onSuccess(record);
    } catch (error) {
      Errors.handle(error);
    } finally {
      this.setState({
        saveLoading: false,
      });
    }
  };

  render() {
    if (!this.props.visible) {
      return null;
    }

    return (
      <Modal title={i18n('entities.accountMember.new.title')} visible={this.props.visible} onCancel={() => this.props.onCancel()} footer={false} width="80%">
        <AccountMemberForm saveLoading={this.state.saveLoading} onSubmit={this.doSubmit} onCancel={this.props.onCancel} modal />
      </Modal>
    );
  }
}

export default AccountMemberFormModal;
