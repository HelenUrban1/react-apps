import { CloseOutlined, SaveOutlined, UndoOutlined } from '@ant-design/icons';
import { Button, Form } from 'antd';
import { Formik } from 'formik';
import model from 'modules/accountMember/accountMemberModel';
import React, { Component } from 'react';
import ViewFormItem from 'view/shared/form/items/ViewFormItem';
import Spinner from 'view/shared/Spinner';
import FormWrapper, { tailFormItemLayout } from 'view/shared/styles/FormWrapper';
import FormSchema from 'view/shared/form/formSchema';
import TextAreaFormItem from 'view/shared/form/items/TextAreaFormItem';
import UserAutocompleteFormItem from 'view/iam/autocomplete/UserAutocompleteFormItem';
import SelectFormItem from 'view/shared/form/items/SelectFormItem';
import AccountAutocompleteFormItem from 'view/account/autocomplete/AccountAutocompleteFormItem';
import { i18n } from '../../../i18n';

const { fields } = model;

class AccountMemberForm extends Component {
  schema = new FormSchema(fields.id, [fields.account, fields.user, fields.settings, fields.roles, fields.status]);

  handleSubmit = (values) => {
    const { id, ...data } = this.schema.cast(values);
    this.props.onSubmit(id, data);
  };

  initialValues = () => {
    const { record } = this.props;
    return this.schema.initialValues(record || {});
  };

  renderForm() {
    const { saveLoading, isEditing } = this.props;

    return (
      <FormWrapper>
        <Formik initialValues={this.initialValues()} validationSchema={this.schema.schema} onSubmit={this.handleSubmit}>
          {(form) => {
            return (
              <Form layout="vertical" onFinish={form.handleSubmit}>
                {isEditing && <ViewFormItem name={fields.id.name} label={fields.id.label} />}

                <AccountAutocompleteFormItem name={fields.account.name} label={fields.account.label} required={fields.account.required} showCreate={!this.props.modal} form={form} />
                <UserAutocompleteFormItem name={fields.user.name} label={fields.user.label} required={fields.user.required} showCreate={!this.props.modal} form={form} />
                <TextAreaFormItem name={fields.settings.name} label={fields.settings.label} required={fields.settings.required} />
                <SelectFormItem
                  name={fields.roles.name}
                  label={fields.roles.label}
                  options={fields.roles.options.map((item) => ({
                    value: item.id,
                    label: item.label,
                  }))}
                  required={fields.roles.required}
                  mode="multiple"
                />
                <SelectFormItem
                  name={fields.status.name}
                  label={fields.status.label}
                  options={fields.status.options.map((item) => ({
                    value: item.id,
                    label: item.label,
                  }))}
                  required={fields.status.required}
                />

                <Form.Item className="form-buttons" {...tailFormItemLayout}>
                  <Button loading={saveLoading} type="primary" onClick={form.handleSubmit} icon={<SaveOutlined />}>
                    {i18n('common.save')}
                  </Button>

                  <Button disabled={saveLoading} onClick={form.handleReset} icon={<UndoOutlined />}>
                    {i18n('common.reset')}
                  </Button>

                  {this.props.onCancel ? (
                    <Button disabled={saveLoading} onClick={() => this.props.onCancel()} icon={<CloseOutlined />}>
                      {i18n('common.cancel')}
                    </Button>
                  ) : null}
                </Form.Item>
              </Form>
            );
          }}
        </Formik>
      </FormWrapper>
    );
  }

  render() {
    const { isEditing, findLoading, record } = this.props;

    if (findLoading) {
      return <Spinner />;
    }

    if (isEditing && !record) {
      return <Spinner />;
    }

    return this.renderForm();
  }
}

export default AccountMemberForm;
