import React, { Component } from 'react';
import ContentWrapper from 'view/layout/styles/ContentWrapper';
import PageTitle from 'view/shared/styles/PageTitle';
import Breadcrumb from 'view/shared/Breadcrumb';
import AccountMemberForm from 'view/accountMember/form/AccountMemberForm';
import { i18n } from '../../../i18n';
import { getHistory } from 'modules/store';
import actions from 'modules/accountMember/form/accountMemberFormActions';
import selectors from 'modules/accountMember/form/accountMemberFormSelectors';
import { connect } from 'react-redux';

class AccountMemberFormPage extends Component {
  state = {
    dispatched: false,
  };

  componentDidMount() {
    const { dispatch, match } = this.props;

    if (this.isEditing()) {
      dispatch(actions.doFind(match.params.id));
    } else {
      dispatch(actions.doNew());
    }

    this.setState({ dispatched: true });
  }

  doSubmit = (id, data) => {
    const { dispatch } = this.props;
    if (this.isEditing()) {
      dispatch(actions.doUpdate(id, data));
    } else {
      dispatch(actions.doCreate(data));
    }
  };

  isEditing = () => {
    const { match } = this.props;
    return !!match.params.id;
  };

  title = () => {
    return this.isEditing() ? i18n('entities.accountMember.edit.title') : i18n('entities.accountMember.new.title');
  };

  render() {
    return (
      <React.Fragment>
        <Breadcrumb items={[[i18n('home.menu'), '/'], [i18n('entities.accountMember.menu'), '/account-member'], [this.title()]]} />

        <ContentWrapper>
          <PageTitle>{this.title()}</PageTitle>

          {this.state.dispatched && (
            <AccountMemberForm saveLoading={this.props.saveLoading} findLoading={this.props.findLoading} record={this.props.record} isEditing={this.isEditing()} onSubmit={this.doSubmit} onCancel={() => getHistory().push('/account-member')} />
          )}
        </ContentWrapper>
      </React.Fragment>
    );
  }
}

function select(state) {
  return {
    findLoading: selectors.selectFindLoading(state),
    saveLoading: selectors.selectSaveLoading(state),
    record: selectors.selectRecord(state),
  };
}

export default connect(select)(AccountMemberFormPage);
