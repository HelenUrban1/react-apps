import React, { Component } from 'react';
import AccountMemberListFilter from 'view/accountMember/list/AccountMemberListFilter';
import AccountMemberListTable from 'view/accountMember/list/AccountMemberListTable';
import AccountMemberListToolbar from 'view/accountMember/list/AccountMemberListToolbar';
import ContentWrapper from 'view/layout/styles/ContentWrapper';
import PageTitle from 'view/shared/styles/PageTitle';
import Breadcrumb from 'view/shared/Breadcrumb';
import { i18n } from '../../../i18n';

class AccountMemberListPage extends Component {
  render() {
    return (
      <React.Fragment>
        <Breadcrumb items={[[i18n('home.menu'), '/'], [i18n('entities.accountMember.menu')]]} />

        <ContentWrapper>
          <PageTitle>{i18n('entities.accountMember.list.title')}</PageTitle>

          <AccountMemberListToolbar />
          <AccountMemberListFilter />
          <AccountMemberListTable />
        </ContentWrapper>
      </React.Fragment>
    );
  }
}

export default AccountMemberListPage;
