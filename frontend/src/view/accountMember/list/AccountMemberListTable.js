import { Popconfirm, Table, Tooltip } from 'antd';
import { i18n } from '../../../i18n';
import actions from 'modules/accountMember/list/accountMemberListActions';
import destroyActions from 'modules/accountMember/destroy/accountMemberDestroyActions';
import selectors from 'modules/accountMember/list/accountMemberListSelectors';
import destroySelectors from 'modules/accountMember/destroy/accountMemberDestroySelectors';
import model from 'modules/accountMember/accountMemberModel';
import accountMemberSelectors from 'modules/accountMember/accountMemberSelectors';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import TableWrapper from 'view/shared/styles/TableWrapper';
import ButtonLink from 'view/shared/styles/ButtonLink';
import UserListItem from 'view/iam/list/users/UserListItem';
import AccountListItem from 'view/account/list/AccountListItem';

const { fields } = model;

class AccountMemberListTable extends Component {
  handleTableChange = (pagination, filters, sorter) => {
    const { dispatch } = this.props;

    dispatch(actions.doChangePaginationAndSort(pagination, sorter));
  };

  doDestroy = (id) => {
    const { dispatch } = this.props;
    dispatch(destroyActions.doDestroy(id));
  };

  columns = [
    fields.id.forTable(),
    fields.account.forTable({
      render: (value) => <AccountListItem value={value} />,
    }),
    fields.user.forTable({
      render: (value) => <UserListItem value={value} />,
    }),
    fields.settings.forTable(),
    fields.roles.forTable({
      render: (roles) =>
        roles.map((role) => (
          <div key={role}>
            <Tooltip title={model.descriptionOfRole(role)}>
              <span>{model.labelOfRole(role)}</span>
            </Tooltip>
          </div>
        )),
    }),
    fields.status.forTable(),
    fields.createdAt.forTable(),
    {
      title: '',
      dataIndex: '',
      width: '160px',
      render: (_, record) => (
        <div className="table-actions">
          <Link to={`/account-member/${record.id}`}>{i18n('common.view')}</Link>
          {this.props.hasPermissionToEdit && <Link to={`/account-member/${record.id}/edit`}>{i18n('common.edit')}</Link>}
          {this.props.hasPermissionToDestroy && (
            <Popconfirm title={i18n('common.areYouSure')} onConfirm={() => this.doDestroy(record.id)} okText={i18n('common.yes')} cancelText={i18n('common.no')}>
              <ButtonLink>{i18n('common.destroy')}</ButtonLink>
            </Popconfirm>
          )}
        </div>
      ),
    },
  ];

  rowSelection = () => {
    return {
      selectedRowKeys: this.props.selectedKeys,
      onChange: (selectedRowKeys) => {
        const { dispatch } = this.props;
        dispatch(actions.doChangeSelected(selectedRowKeys));
      },
    };
  };

  render() {
    const { pagination, rows, loading } = this.props;

    return (
      <TableWrapper>
        <Table rowKey="id" loading={loading} columns={this.columns} dataSource={rows} pagination={pagination} onChange={this.handleTableChange} rowSelection={this.rowSelection()} scroll={{ x: true }} />
      </TableWrapper>
    );
  }
}

function select(state) {
  return {
    loading: selectors.selectLoading(state) || destroySelectors.selectLoading(state),
    rows: selectors.selectRows(state),
    pagination: selectors.selectPagination(state),
    filter: selectors.selectFilter(state),
    selectedKeys: selectors.selectSelectedKeys(state),
    hasPermissionToEdit: accountMemberSelectors.selectPermissionToEdit(state),
    hasPermissionToDestroy: accountMemberSelectors.selectPermissionToDestroy(state),
  };
}

export default connect(select)(AccountMemberListTable);
