import React, { Component } from 'react';
import Toolbar from 'view/shared/styles/Toolbar';

import { DeleteOutlined, FileExcelOutlined, FileSearchOutlined, PlusOutlined, UploadOutlined } from '@ant-design/icons';

import { Button, Tooltip, Popconfirm } from 'antd';
import { connect } from 'react-redux';
import accountMemberSelectors from 'modules/accountMember/accountMemberSelectors';
import selectors from 'modules/accountMember/list/accountMemberListSelectors';
import auditLogSelectors from 'modules/auditLog/auditLogSelectors';
import actions from 'modules/accountMember/list/accountMemberListActions';
import destroyActions from 'modules/accountMember/destroy/accountMemberDestroyActions';
import { Link } from 'react-router-dom';
import { i18n } from '../../../i18n';
import destroySelectors from 'modules/accountMember/destroy/accountMemberDestroySelectors';

class AccountMemberToolbar extends Component {
  doExport = () => {
    const { dispatch } = this.props;
    dispatch(actions.doExport());
  };

  doDestroyAllSelected = () => {
    const { dispatch } = this.props;
    dispatch(destroyActions.doDestroyAll(this.props.selectedKeys));
  };

  renderExportButton() {
    const { hasRows, loading, exportLoading } = this.props;

    const disabled = !hasRows || loading;

    const button = (
      <Button disabled={disabled} icon={<FileExcelOutlined />} onClick={this.doExport} loading={exportLoading}>
        {i18n('common.export')}
      </Button>
    );

    if (disabled) {
      return <Tooltip title={i18n('common.noDataToExport')}>{button}</Tooltip>;
    }

    return button;
  }

  renderDestroyButton() {
    const { selectedKeys, destroyLoading, loading, hasPermissionToDestroy } = this.props;

    if (!hasPermissionToDestroy) {
      return null;
    }

    const disabled = !selectedKeys.length || loading;

    const button = (
      <Button disabled={disabled} loading={destroyLoading} type="primary" icon={<DeleteOutlined />}>
        {i18n('common.destroy')}
      </Button>
    );

    const buttonWithConfirm = (
      <Popconfirm title={i18n('common.areYouSure')} onConfirm={() => this.doDestroyAllSelected()} okText={i18n('common.yes')} cancelText={i18n('common.no')}>
        {button}
      </Popconfirm>
    );

    if (disabled) {
      return <Tooltip title={i18n('common.mustSelectARow')}>{button}</Tooltip>;
    }

    return buttonWithConfirm;
  }

  render() {
    return (
      <Toolbar>
        {this.props.hasPermissionToCreate && (
          <Link to="/account-member/new">
            <Button type="primary" icon={<PlusOutlined />}>
              {i18n('common.new')}
            </Button>
          </Link>
        )}

        {this.props.hasPermissionToImport && (
          <Link to="/account-member/importer">
            <Button type="primary" icon={<UploadOutlined />}>
              {i18n('common.import')}
            </Button>
          </Link>
        )}

        {this.renderDestroyButton()}

        {this.props.hasPermissionToAuditLogs && (
          <Link to="/audit-logs?entityNames=accountMember">
            <Button icon={<FileSearchOutlined />}>{i18n('auditLog.menu')}</Button>
          </Link>
        )}

        {this.renderExportButton()}
      </Toolbar>
    );
  }
}

function select(state) {
  return {
    selectedKeys: selectors.selectSelectedKeys(state),
    loading: selectors.selectLoading(state),
    destroyLoading: destroySelectors.selectLoading(state),
    exportLoading: selectors.selectExportLoading(state),
    hasRows: selectors.selectHasRows(state),
    hasPermissionToAuditLogs: auditLogSelectors.selectPermissionToRead(state),
    hasPermissionToEdit: accountMemberSelectors.selectPermissionToEdit(state),
    hasPermissionToDestroy: accountMemberSelectors.selectPermissionToDestroy(state),
    hasPermissionToCreate: accountMemberSelectors.selectPermissionToCreate(state),
    hasPermissionToImport: accountMemberSelectors.selectPermissionToImport(state),
  };
}

export default connect(select)(AccountMemberToolbar);
