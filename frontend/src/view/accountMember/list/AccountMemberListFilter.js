import { SearchOutlined, UndoOutlined } from '@ant-design/icons';
import { Button, Col, Row, Form } from 'antd';
import { Formik } from 'formik';
import actions from 'modules/accountMember/list/accountMemberListActions';
import selectors from 'modules/accountMember/list/accountMemberListSelectors';
import model from 'modules/accountMember/accountMemberModel';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import FilterWrapper, { formItemLayout } from 'view/shared/styles/FilterWrapper';
import FormFilterSchema from 'view/shared/form/formFilterSchema';
import InputFormItem from 'view/shared/form/items/InputFormItem';
import DatePickerRangeFormItem from 'view/shared/form/items/DatePickerRangeFormItem';
import UserAutocompleteFormItem from 'view/iam/autocomplete/UserAutocompleteFormItem';
import SelectFormItem from 'view/shared/form/items/SelectFormItem';
import AccountAutocompleteFormItem from 'view/account/autocomplete/AccountAutocompleteFormItem';
import { i18n } from '../../../i18n';

const { fields } = model;

const schema = new FormFilterSchema([fields.id, fields.createdAtRange, fields.account, fields.user, fields.settings, fields.roles, fields.status]);

class AccountMemberListFilter extends Component {
  componentDidMount() {
    const { dispatch } = this.props;
    dispatch(actions.doFetch(this.initialFilter()));
  }

  initialFilter = () => {
    return schema.initialValues(this.props.filter, this.props.location);
  };

  handleSubmit = (values) => {
    const valuesToSubmit = schema.cast(values);
    const { dispatch } = this.props;
    dispatch(actions.doFetch(valuesToSubmit));
  };

  handleReset = (form) => {
    form.setValues({});
    const { dispatch } = this.props;
    dispatch(actions.doReset());
  };

  render() {
    const { loading } = this.props;

    return (
      <FilterWrapper>
        <Formik initialValues={this.initialFilter()} validationSchema={schema.schema} onSubmit={this.handleSubmit}>
          {(form) => {
            return (
              <Form layout="vertical" onFinish={form.handleSubmit}>
                <Row gutter={24}>
                  <Col md={24} lg={12}>
                    <InputFormItem name={fields.id.name} label={fields.id.label} layout={formItemLayout} />
                  </Col>
                  <Col md={24} lg={12}>
                    <DatePickerRangeFormItem name={fields.createdAtRange.name} label={fields.createdAtRange.label} layout={formItemLayout} showTime />
                  </Col>
                  <Col md={24} lg={12}>
                    <AccountAutocompleteFormItem name={fields.account.name} label={fields.account.label} layout={formItemLayout} />
                  </Col>
                  <Col md={24} lg={12}>
                    <UserAutocompleteFormItem name={fields.user.name} label={fields.user.label} layout={formItemLayout} />
                  </Col>
                  <Col md={24} lg={12}>
                    <InputFormItem name={fields.settings.name} label={fields.settings.label} layout={formItemLayout} />
                  </Col>
                  <Col md={24} lg={12}>
                    <SelectFormItem
                      name={fields.roles.name}
                      label={fields.roles.label}
                      options={fields.roles.options.map((item) => ({
                        value: item.id,
                        label: item.label,
                      }))}
                      layout={formItemLayout}
                      mode="multiple"
                    />
                  </Col>
                  <Col md={24} lg={12}>
                    <SelectFormItem
                      name={fields.status.name}
                      label={fields.status.label}
                      options={fields.status.options.map((item) => ({
                        value: item.id,
                        label: item.label,
                      }))}
                      layout={formItemLayout}
                    />
                  </Col>
                </Row>
                <Row>
                  <Col className="filter-buttons" span={24}>
                    <Button loading={loading} icon={<SearchOutlined />} type="primary" htmlType="submit">
                      {i18n('common.search')}
                    </Button>
                    <Button loading={loading} onClick={() => this.handleReset(form)} icon={<UndoOutlined />}>
                      {i18n('common.reset')}
                    </Button>
                  </Col>
                </Row>
              </Form>
            );
          }}
        </Formik>
      </FilterWrapper>
    );
  }
}

function select(state) {
  return {
    filter: selectors.selectFilter(state),
  };
}

export default withRouter(connect(select)(AccountMemberListFilter));
