import React, { Component } from 'react';
import ContentWrapper from 'view/layout/styles/ContentWrapper';
import PageTitle from 'view/shared/styles/PageTitle';
import Breadcrumb from 'view/shared/Breadcrumb';
import { i18n } from '../../../i18n';
import importerHoc from 'view/shared/importer/Importer';
import selectors from 'modules/accountMember/importer/accountMemberImporterSelectors';
import actions from 'modules/accountMember/importer/accountMemberImporterActions';
import fields from 'modules/accountMember/importer/accountMemberImporterFields';

class AccountMemberImportPage extends Component {
  render() {
    const Importer = importerHoc(selectors, actions, fields, i18n('entities.accountMember.importer.hint'));

    return (
      <React.Fragment>
        <Breadcrumb items={[[i18n('home.menu'), '/'], [i18n('entities.accountMember.menu'), '/account-member'], [i18n('entities.accountMember.importer.title')]]} />

        <ContentWrapper>
          <PageTitle>{i18n('entities.accountMember.importer.title')}</PageTitle>

          <Importer />
        </ContentWrapper>
      </React.Fragment>
    );
  }
}

export default AccountMemberImportPage;
