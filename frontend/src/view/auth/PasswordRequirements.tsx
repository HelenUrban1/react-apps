import React from 'react';
import { CloseCircleTwoTone, CheckCircleTwoTone } from '@ant-design/icons';
import { i18n } from '../../i18n';
import './passwordRequirements.less';

interface PasswordRequirementsProps {
  password: string;
}

export const PasswordRequirements = ({ password }: PasswordRequirementsProps) => {
  const hasLength = password.length >= 10;
  const hasUpperCase = /[A-Z]/.test(password);
  const hasLowerCase = /[a-z]/.test(password);
  const hasSpecialChar = /[!@#$%^&*()]/.test(password);
  const hasNumber = /[0-9]/.test(password);
  const isEmail = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(password);

  return (
    <section className="password-requirements">
      <span className="password-requirement" data-cy="min-length-req">
        {hasLength ? <CheckCircleTwoTone twoToneColor="#52c41a" /> : <CloseCircleTwoTone twoToneColor="#f5222d" />}
        {i18n('password.requirements.length')}
      </span>
      <span className="password-requirement" data-cy="upper-case-req">
        {hasUpperCase ? <CheckCircleTwoTone twoToneColor="#52c41a" /> : <CloseCircleTwoTone twoToneColor="#f5222d" />}
        {i18n('password.requirements.upperCase')}
      </span>
      <span className="password-requirement" data-cy="lower-case-req">
        {hasLowerCase ? <CheckCircleTwoTone twoToneColor="#52c41a" /> : <CloseCircleTwoTone twoToneColor="#f5222d" />}
        {i18n('password.requirements.lowerCase')}
      </span>
      <span className="password-requirement" data-cy="number-req">
        {hasNumber ? <CheckCircleTwoTone twoToneColor="#52c41a" /> : <CloseCircleTwoTone twoToneColor="#f5222d" />}
        {i18n('password.requirements.number')}
      </span>
      <span className="password-requirement" data-cy="same-as-username">
        {!isEmail ? <CheckCircleTwoTone twoToneColor="#52c41a" /> : <CloseCircleTwoTone twoToneColor="#f5222d" />}
        {i18n('password.requirements.email')}
      </span>
      <span className="password-requirement" data-cy="special-char-req">
        {hasSpecialChar ? <CheckCircleTwoTone twoToneColor="#52c41a" /> : <CloseCircleTwoTone twoToneColor="#f5222d" />}
        {i18n('password.requirements.special')}
        <span className="special-characters">! @ # $ % ^ & * ( )</span>
      </span>
    </section>
  );
};
