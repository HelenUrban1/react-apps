import { Button, Checkbox, Form } from 'antd';
import { Formik } from 'formik';
import actions from 'modules/auth/authActions';
import model from 'modules/auth/userModel';
import selectors from 'modules/auth/authSelectors';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import queryString from 'query-string';
import Content from 'view/auth/styles/Content';
import { OtherActionsMultiple } from 'view/auth/styles/OtherActions';
import SigninPageWrapper from 'view/auth/styles/SigninPageWrapper';
import InputFormItem, { InputFormItemNotFast } from 'view/shared/form/items/InputFormItem';
import FormSchema from 'view/shared/form/formSchema';
import { isBeforeProduction } from 'config';
import { hubspotSignupUrl } from 'view/global/defaults';
import { i18n } from '../../i18n';
import { HealthCheck } from '../layout/HealthCheck';
import { FormLogo } from './styles/FormLogo';
import './signinPage.less';
import { notification } from 'antd';
import getStore from 'modules/store';

const { fields } = model;

const signUpLink = isBeforeProduction ? '/auth/signup' : hubspotSignupUrl;

const privacyPolicyLink = 'https://www.ideagen.com/privacy-policy';
const termsAndConditionsLink = 'https://www.ideagen.com/terms-and-conditions';
const dataProtectionPolicyLink = 'https://www.ideagen.com/data-protection-policy';

class SigninPage extends Component {
  schema = new FormSchema(fields.id, [fields.email, fields.password, fields.rememberMe, fields.mfaCode]);

  handleChange(event, form) {
    if (this.props.errorMessage) {
      this.clearErrorMessage();
    }

    form.handleChange(event);
  }

  componentDidMount() {
    if (localStorage.getItem('timedOut') && localStorage.getItem('timedOut') === 'true') {
      notification.error({
        message: i18n('auth.timedOut.notification.title'),
        description: i18n('auth.timedOut.notification.description'),
        placement: 'bottom',
      });
      localStorage.setItem('timedOut', false);
    }
  }

  clearErrorMessage = () => {
    const { dispatch } = this.props;
    dispatch(actions.doClearErrorMessage());
  };

  doSubmit = ({ email, password, rememberMe, mfaCode }) => {
    const { dispatch } = this.props;
    dispatch(actions.doSigninWithEmailAndPassword(email, password, rememberMe, mfaCode));
  };

  emailFromUrl = () => {
    const { location } = this.props;
    return queryString.parse(location.search).email;
  };

  initialValues = () => {
    return { email: this.emailFromUrl(), password: '', rememberMe: true, mfaCode: '' };
  };

  render() {
    return (
      <SigninPageWrapper>
        <HealthCheck />
        <Content>
          <div className="signinPageWrapper">
            <div style={{ display: 'flex', flexDirection: 'column' }}>
              <FormLogo />

              <Formik initialValues={this.initialValues()} validationSchema={this.schema.schema} onSubmit={this.doSubmit}>
                {(form) => (
                  <Form layout="vertical" onFinish={form.handleSubmit}>
                    <InputFormItemNotFast name={fields.email.name} placeholder={fields.email.label} autoComplete={fields.email.name} size="large" autoFocus errorMessage={this.props.errorMessage} layout={null} form={form} />

                    <InputFormItem name={fields.password.name} placeholder={fields.password.label} autoComplete={fields.password.name} type="password" size="large" layout={null} />

                    {/* {config.authStrategy && config.authStrategy === 'cognito' && (
                      <InputFormItem name={fields.mfaCode.name} placeholder={'MFA Code'} autoComplete={fields.mfaCode.name} size="large" layout={null} errorMessage={this.props.mfaError} form={form} />
                    )} */}

                    <Form.Item>
                      <Checkbox id={fields.rememberMe.name} checked={form.values.rememberMe} onChange={form.handleChange} data-testid="rememberMeCheckbox">
                        {fields.rememberMe.label}
                      </Checkbox>

                      <Link style={{ float: 'right' }} to="/auth/forgot-password" data-testid="forgotPasswordLink">
                        {i18n('auth.forgotPassword')}
                      </Link>
                    </Form.Item>

                    <Button type="primary" size="large" block htmlType="submit" loading={this.props.loading} data-testid="signInButton">
                      {i18n('auth.signin')}
                    </Button>

                    <OtherActionsMultiple>
                      <Link to={{ pathname: signUpLink }} target={isBeforeProduction ? undefined : '_blank'} data-testid="requestAccountLink">
                        {i18n('auth.createAnAccount')}
                      </Link>
                      <Link to={`/auth/email-unverified?email=${form.values.email || ''}`} data-testid="resendEmailVerificationLink">
                        {i18n('auth.emailUnverified.submit')}
                      </Link>
                    </OtherActionsMultiple>
                  </Form>
                )}
              </Formik>
            </div>
            <div style={{ display: 'flex', justifyContent: 'space-between' }}>
              <Link to={{ pathname: privacyPolicyLink }} target="_blank">
                {i18n('auth.privacyPolicy')}
              </Link>
              <Link to={{ pathname: termsAndConditionsLink }} target="_blank">
                {i18n('auth.termsAndConditions')}
              </Link>
              <Link to={{ pathname: dataProtectionPolicyLink }} target="_blank">
                {i18n('auth.dataProtectionPolicy')}
              </Link>
            </div>
          </div>
        </Content>
      </SigninPageWrapper>
    );
  }
}

const select = (state) => ({
  loading: selectors.selectLoading(state),
  errorMessage: selectors.selectErrorMessage(state),
});

export default connect(select)(SigninPage);
