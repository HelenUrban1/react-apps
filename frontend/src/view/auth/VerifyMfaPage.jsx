import actions from 'modules/auth/authActions';
import selectors from 'modules/auth/authSelectors';
import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import Content from 'view/auth/styles/Content';
import { FormLogo } from 'view/auth/styles/FormLogo';
import EmailUnverifiedPageWrapper from 'view/auth/styles/EmailUnverifiedPageWrapper';
import { Formik } from 'formik';
import { Button, Form } from 'antd';
import ButtonLink from 'view/shared/styles/ButtonLink';
import model from 'modules/auth/accountSignupModel';
import FormSchema from 'view/shared/form/formSchema';
import { InputFormItemNotFast } from 'view/shared/form/items/InputFormItem';
import { OtherActionsMultiple } from 'view/auth/styles/OtherActions';
import { i18n } from 'i18n';
import StringField from 'modules/shared/fields/stringField';
import { useHistory } from 'react-router';
import { ContactInfo } from 'styleguide/PricingTable/ContactInfo';
import { HealthCheck } from '../layout/HealthCheck';

const { fields } = model;
const mfaField = new StringField('mfa', i18n('auth.verifyMfa.mfaField'), {
  matches: /^[0-9]/,
  errorMessage: i18n('auth.verifyMfa.mfaFieldError'),
  required: true,
});

const VerifyMfaPage = (props) => {
  const schemaPassword = new FormSchema(fields.id, [mfaField]);
  const { loading, errorMessage, location, dispatch } = props;
  const history = useHistory();

  const [initialValues, setInitialValues] = useState({
    email: undefined,
    password: undefined,
    mfa: undefined,
  });
  const [lost, setLost] = useState(false);

  useEffect(() => {
    if (location.state) {
      const { email, password } = location.state;
      setInitialValues((prev) => ({
        ...prev,
        email,
        password,
      }));
    }
  }, [location]);

  const clearErrorMessage = () => {
    dispatch(actions.doClearErrorMessage());
  };

  const doSignout = () => {
    dispatch(actions.doSignout(location.state.email));
  };

  const handleChange = (event, form) => {
    if (errorMessage) {
      clearErrorMessage();
    }

    form.handleChange(event);
  };

  const doSubmit = ({ mfa }) => {
    const { email, password, rememberMe } = history.location.state;
    dispatch(actions.doSigninWithEmailAndPassword(email, password, rememberMe, mfa));
  };

  const handleToggleLost = () => {
    setLost(!lost);
  };

  return (
    <EmailUnverifiedPageWrapper>
      <HealthCheck />
      <Content>
        <FormLogo />

        <h3 style={{ textAlign: 'center' }}>{lost ? i18n('auth.verifyMfa.lostDeviceTitle') : i18n('auth.verifyMfa.title')}</h3>
        <p style={{ textAlign: 'center' }}>{lost ? i18n('auth.verifyMfa.lostDeviceInfo') : i18n('auth.verifyMfa.message')}</p>
        {lost && (
          <section className="lost-contact">
            <ContactInfo phone="888-984-3199 ext 2" email="support" />
          </section>
        )}
        {errorMessage && <p className="error-message">{errorMessage}</p>}

        <Formik //
          enableReinitialize
          initialValues={initialValues}
          validationSchema={schemaPassword.schema}
          onSubmit={doSubmit}
        >
          {(form) => {
            return (
              <Form layout="vertical" onFinish={form.handleSubmit} onChange={(e) => handleChange(e, form)}>
                {!lost && <InputFormItemNotFast name={mfaField.name} placeholder={mfaField.label} autoComplete={mfaField.name} size="large" autoFocus layout={null} form={form} errorMessage={errorMessage} />}
                {!lost && (
                  <Button disabled={!form.values.mfa} type="primary" size="large" block htmlType="submit" loading={loading}>
                    {i18n('auth.signin')}
                  </Button>
                )}

                <OtherActionsMultiple>
                  <ButtonLink onClick={doSignout}>{i18n('auth.signinWithAnotherAccount')}</ButtonLink>
                  <ButtonLink onClick={handleToggleLost}>{lost ? i18n('auth.verifyMfa.enterCode') : i18n('auth.verifyMfa.lostDevice')}</ButtonLink>
                </OtherActionsMultiple>
              </Form>
            );
          }}
        </Formik>
      </Content>
    </EmailUnverifiedPageWrapper>
  );
};

const select = (state) => ({
  loading: selectors.selectLoadingMfaVerification(state),
});

export default connect(select)(VerifyMfaPage);
