import actions from 'modules/auth/authActions';
import selectors from 'modules/auth/authSelectors';
import React from 'react';
import { connect } from 'react-redux';
import Content from 'view/auth/styles/Content';
import { FormLogo } from 'view/auth/styles/FormLogo';
import RequestSentPageWrapper from 'view/auth/styles/RequestSentPageWrapper';
import ButtonLink from 'view/shared/styles/ButtonLink';
import { i18n } from 'i18n';

const RequestSentPage = (props) => {
  const doSignout = () => {
    const { dispatch, email } = props;
    dispatch(actions.doSignout(email));
  };

  const { email } = props;

  return (
    <RequestSentPageWrapper>
      <Content>
        <FormLogo />

        <h3 style={{ textAlign: 'center' }}>{i18n('auth.requestSent.message', email)}</h3>
        <p style={{ textAlign: 'center' }}>{i18n('auth.requestSent.support')}</p>

        <ButtonLink onClick={doSignout}>{i18n('auth.signinWithAnotherAccount')}</ButtonLink>
      </Content>
    </RequestSentPageWrapper>
  );
};

const select = (state) => ({
  email: selectors.selectCurrentUserEmail(state),
  loading: selectors.selectLoadingEmailConfirmation(state),
});

export default connect(select)(RequestSentPage);
