import { Button, Form } from 'antd';
import { Formik } from 'formik';
import actions from 'modules/auth/authActions';
import model from 'modules/auth/accountSignupModel';
import selectors from 'modules/auth/authSelectors';
import queryString from 'query-string';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import Content from 'view/auth/styles/Content';
import { FormLogo } from 'view/auth/styles/FormLogo';
import { OtherActions } from 'view/auth/styles/OtherActions';
import SignupPageWrapper from 'view/auth/styles/SignupPageWrapper';
import InputFormItem, { InputFormItemNotFast } from 'view/shared/form/items/InputFormItem';
import FormSchema from 'view/shared/form/formSchema';
import { i18n } from 'i18n';
import { isBeforeProduction } from 'config';
import { hubspotSignupUrl } from 'view/global/defaults';
import { HealthCheck } from '../layout/HealthCheck';

const { fields } = model;

class SignupPage extends Component {
  schema = new FormSchema(fields.id, [fields.email, fields.firstName]);

  componentDidMount() {
    if (!isBeforeProduction) window.location.replace(hubspotSignupUrl);
  }

  clearErrorMessage = () => {
    const { dispatch } = this.props;
    dispatch(actions.doClearErrorMessage());
  };

  emailFromInvitation = () => {
    const { location } = this.props;
    return queryString.parse(location.search).email;
  };

  initialValues = () => {
    return {
      email: this.emailFromInvitation() || undefined,
      firstName: undefined,
    };
  };

  doSubmit = ({ email, firstName }) => {
    const { dispatch } = this.props;
    dispatch(actions.doRegisterAccountAndUser(email, firstName));
  };

  render() {
    if (!isBeforeProduction) return <></>;

    const { errorMessage, loading } = this.props;

    return (
      // eslint-disable-next-line react/jsx-filename-extension
      <SignupPageWrapper>
        <HealthCheck />
        <Content>
          <FormLogo />

          <Formik initialValues={this.initialValues()} validationSchema={this.schema.schema} onSubmit={this.doSubmit}>
            {(form) => {
              return (
                <Form layout="vertical" onFinish={form.handleSubmit}>
                  <InputFormItemNotFast name={fields.email.name} placeholder={fields.email.label} autoComplete={fields.email.name} size="large" autoFocus errorMessage={errorMessage} layout={null} form={form} />

                  <InputFormItem name={fields.firstName.name} placeholder={fields.firstName.label} autoComplete={fields.firstName.name} size="large" layout={null} />

                  <Button disabled={!form.values.email} type="primary" size="large" block htmlType="submit" loading={loading} data-testid="signUpButton">
                    {i18n('auth.signup')}
                  </Button>

                  <OtherActions>
                    <Link to="/auth/signin" data-testid="signInWithOtherAccount">
                      {i18n('auth.alreadyHaveAnAccount')}
                    </Link>
                  </OtherActions>
                </Form>
              );
            }}
          </Formik>
        </Content>
      </SignupPageWrapper>
    );
  }
}

const select = (state) => ({
  loading: selectors.selectLoading(state),
  errorMessage: selectors.selectErrorMessage(state),
});

export default connect(select)(SignupPage);
