import actions from 'modules/auth/authActions';
import selectors from 'modules/auth/authSelectors';
import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import Content from 'view/auth/styles/Content';
import { FormLogo } from 'view/auth/styles/FormLogo';
import queryString from 'query-string';
import EmailUnverifiedPageWrapper from 'view/auth/styles/EmailUnverifiedPageWrapper';
import { Formik } from 'formik';
import { Button, Form, Checkbox } from 'antd';
import ButtonLink from 'view/shared/styles/ButtonLink';
import model from 'modules/auth/accountSignupModel';
import FormSchema from 'view/shared/form/formSchema';
import InputFormItem, { InputFormItemNotFast } from 'view/shared/form/items/InputFormItem';
import { OtherActions } from 'view/auth/styles/OtherActions';
import { i18n } from '../../i18n';
import { HealthCheck } from '../layout/HealthCheck';
import Analytics from '../../modules/shared/analytics/analytics';

const { fields } = model;

const VerifyEmailPage = (props) => {
  const orgSchemaPassword = new FormSchema(fields.id, [fields.email, fields.password, fields.confirmPassword, fields.firstName, fields.lastName, fields.orgName, fields.phoneNumber]);
  const schemaPassword = new FormSchema(fields.id, [fields.email, fields.password, fields.confirmPassword, fields.firstName, fields.lastName]);
  // Form on this page will always require password to be set for now
  // const orgSchema = new FormSchema(fields.id, [fields.email, fields.firstName, fields.lastName, fields.orgName]);
  // const schema = new FormSchema(fields.id, [fields.email, fields.firstName, fields.lastName]);

  const { loading, errorMessage, passwordExists, location, dispatch } = props;

  const tokenFromUrl = () => {
    return queryString.parse(location.search).token;
  };

  const clearErrorMessage = () => {
    dispatch(actions.doClearErrorMessage());
  };

  const emailFromUrl = () => {
    return queryString.parse(location.search).email;
  };

  const formFromUrl = () => {
    return queryString.parse(location.search).form;
  };

  const doSignout = () => {
    dispatch(actions.doSignout(emailFromUrl()));
  };

  const [initialValues, setInitialValues] = useState({
    email: emailFromUrl() || undefined,
    password: undefined,
    confirmPassword: undefined,
    adminEmail: undefined,
    firstName: undefined,
    lastName: undefined,
    orgName: undefined,
    phoneNumber: undefined,
    termsAndConditions: false,
  });

  const checkForMeta = async () => {
    const meta = await dispatch(actions.doCheckMeta(emailFromUrl()));
    if (meta) setInitialValues({ ...initialValues, firstName: meta.firstName });
  };

  useEffect(() => {
    if (formFromUrl() === 'verify') {
      dispatch(actions.doVerifyEmail(tokenFromUrl()));
    } else if (!loading) {
      dispatch(actions.doCheckToken(emailFromUrl(), tokenFromUrl()));
      clearErrorMessage();
      checkForMeta();
    }
  }, []);

  const handleChange = (event, form) => {
    if (errorMessage) {
      clearErrorMessage();
    }

    form.handleChange(event);
  };

  const doSubmit = ({ email, password, firstName, lastName, orgName, phoneNumber, termsAndConditions }) => {
    if (!termsAndConditions) return;
    // finish registration for user
    Analytics.track({
      event: Analytics.events.authRegistrationSubmitted,
      properties: {
        email,
        firstName,
        lastName,
        orgName,
        phoneNumber,
      },
    });

    const form = formFromUrl();
    if (form === 'invite') {
      dispatch(actions.doRegisterInvitedUser(tokenFromUrl(), email, password, firstName, lastName));
    } else {
      dispatch(actions.doFinishRegistration(email, password, firstName, lastName, orgName, phoneNumber, tokenFromUrl()));
    }
  };

  const handleKeyDown = (e, form) => {
    if ((e.charCode || e.keyCode) === 13) {
      // enter key
      e.preventDefault();
      form.setFieldValue('termsAndConditions', !form.values.termsAndConditions);
    }
  };

  const formType = formFromUrl();

  return (
    <EmailUnverifiedPageWrapper>
      <HealthCheck />
      <Content>
        <FormLogo />

        <h3 style={{ textAlign: 'center' }}>{formFromUrl() === 'verify' && passwordExists ? i18n('auth.verifyEmail.message') : i18n('auth.finishReg.title')}</h3>
        <p style={{ textAlign: 'center' }}>{i18n('auth.finishReg.message')}</p>

        <Formik enableReinitialize initialValues={initialValues} validationSchema={formType === 'new' ? orgSchemaPassword.schema : schemaPassword.schema} onSubmit={doSubmit}>
          {(form) => {
            return (
              <>
                <Form layout="vertical" onFinish={form.handleSubmit} onChange={(e) => handleChange(e, form)}>
                  <InputFormItemNotFast name={fields.email.name} placeholder={fields.email.label} autoComplete={fields.email.name} size="large" autoFocus layout={null} form={form} disabled />

                  <InputFormItem name={fields.password.name} placeholder={fields.password.label} autoComplete={fields.password.name} type="password" size="large" layout={null} />
                  <InputFormItem name={fields.confirmPassword.name} placeholder={fields.confirmPassword.label} type="password" size="large" layout={null} />

                  <InputFormItem name={fields.firstName.name} placeholder={fields.firstName.label} autoComplete={fields.firstName.name} size="large" layout={null} />
                  <InputFormItem name={fields.lastName.name} placeholder={fields.lastName.label} autoComplete={fields.lastName.name} size="large" layout={null} />

                  {formFromUrl() === 'new' && (
                    <>
                      <InputFormItem name={fields.orgName.name} placeholder={fields.orgName.label} autoComplete={fields.orgName.name} size="large" layout={null} />
                      <InputFormItem name={fields.phoneNumber.name} placeholder={fields.phoneNumber.label} size="large" layout={null} />
                    </>
                  )}

                  <Form.Item>
                    <Checkbox id={fields.termsAndConditions.name} onKeyDown={(e) => handleKeyDown(e, form)} checked={form.values.termsAndConditions} data-testid="termsAndConditionsCheckBox">
                      {`${fields.termsAndConditions.label} `}
                      <a target="_blank" href={i18n('user.fields.termsAndConditionsLink')} rel="noreferrer">
                        {i18n('user.fields.termsAndConditions')}
                      </a>
                    </Checkbox>
                  </Form.Item>

                  <Button disabled={!form.values.email || !form.values.termsAndConditions} type="primary" size="large" block htmlType="submit" loading={loading} data-testid="finishRegistrationButton">
                    {i18n('auth.requestForm.button')}
                  </Button>

                  <OtherActions>
                    <ButtonLink onClick={doSignout}>{i18n('auth.signinWithAnotherAccount')}</ButtonLink>
                  </OtherActions>
                </Form>
              </>
            );
          }}
        </Formik>
      </Content>
    </EmailUnverifiedPageWrapper>
  );
};

const select = (state) => ({
  loading: selectors.selectLoading(state),
  passwordExists: selectors.selectPasswordExists(state),
});

export default connect(select)(VerifyEmailPage);
