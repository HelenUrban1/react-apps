import { Button, Form } from 'antd';
import actions from 'modules/auth/authActions';
import selectors from 'modules/auth/authSelectors';
import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import Message from 'view/shared/message';
import queryString from 'query-string';
import Content from 'view/auth/styles/Content';
import GenericAuthPageWrapper from 'view/auth/styles/GenericAuthPageWrapper';
import { FormLogo } from 'view/auth/styles/FormLogo';
import { OtherActions } from 'view/auth/styles/OtherActions';
import { Link } from 'react-router-dom';
import { i18n, i18nHtml } from 'i18n';
import { Formik } from 'formik';
import InputFormItem, { InputFormItemNotFast } from 'view/shared/form/items/InputFormItem';
import FormSchema from 'view/shared/form/formSchema';
import model from 'modules/auth/userModel';

const { fields } = model;

const RequestAcceptedPage = (props) => {
  const schema = new FormSchema(fields.id, [fields.email, fields.password, fields.rememberMe, fields.mfaCode]);

  const { loading, dispatch, location, errorMessage } = props;

  const getEmailFromUrl = () => {
    return queryString.parse(location.search).email;
  };

  const doSubmit = ({ email, password, rememberMe, mfaCode }) => {
    dispatch(actions.doSigninWithEmailAndPassword(email, password, rememberMe, mfaCode, '/settings/seats'));
  };

  const initialValues = () => {
    return {
      email: getEmailFromUrl(),
    };
  };

  useEffect(() => {
    Message.success(i18n('auth.requestResponse.accepted'));
  }, []);

  return (
    <GenericAuthPageWrapper>
      <Content>
        <FormLogo />

        <h3 style={{ textAlign: 'center' }}>{i18nHtml('auth.requestAccepted.message')}</h3>
        <p style={{ textAlign: 'center' }}>{i18n('auth.requestAccepted.support')}</p>

        <Formik initialValues={initialValues()} validationSchema={schema.schema} onSubmit={doSubmit}>
          {(form) => {
            return (
              <Form layout="vertical" onFinish={form.handleSubmit}>
                <InputFormItemNotFast name={fields.email.name} placeholder={fields.email.label} autoComplete={fields.email.name} size="large" autoFocus errorMessage={errorMessage} layout={null} form={form} disabled />

                <InputFormItem name={fields.password.name} placeholder={fields.password.label} autoComplete={fields.password.name} type="password" size="large" layout={null} />

                <Button type="primary" size="large" block htmlType="submit" loading={loading}>
                  {i18n('auth.signin')}
                </Button>

                <OtherActions>
                  <Link to="/auth/signin">{i18n('auth.signinWithAnotherAccount')}</Link>
                </OtherActions>
              </Form>
            );
          }}
        </Formik>
      </Content>
    </GenericAuthPageWrapper>
  );
};

const select = (state) => ({
  email: selectors.selectCurrentUserEmail(state),
  loading: selectors.selectLoadingEmailConfirmation(state),
  errorMessage: selectors.selectErrorMessage(state),
});

export default connect(select)(RequestAcceptedPage);
