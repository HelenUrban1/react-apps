import { Button, Form } from 'antd';
import { Formik } from 'formik';
import actions from 'modules/auth/authActions';
import model from 'modules/auth/userModel';
import selectors from 'modules/auth/authSelectors';
import { i18n } from 'i18n';
import queryString from 'query-string';
import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import Content from 'view/auth/styles/Content';
import { FormLogo } from 'view/auth/styles/FormLogo';
import { OtherActions } from 'view/auth/styles/OtherActions';
import InputFormItem from 'view/shared/form/items/InputFormItem';
import ForgotPasswordPageWrapper from 'view/auth/styles/EmptyPermissionsPageWrapper';
import FormSchema from 'view/shared/form/formSchema';

const { fields } = model;

const PasswordResetPage = (props) => {
  const { dispatch, loading, errorMessage, location } = props;
  const schema = new FormSchema(fields.id, [fields.password, fields.confirmPassword]);

  const clearErrorMessage = () => {
    dispatch(actions.doClearErrorMessage());
  };

  useEffect(() => {
    clearErrorMessage();
  }, []);

  const handleChange = (event, form) => {
    if (errorMessage) {
      clearErrorMessage();
    }
    form.handleChange(event);
  };

  const initialValues = () => {
    return {
      password: '',
      confirmPassword: '',
    };
  };

  const token = () => {
    return queryString.parse(location.search).token;
  };

  const email = () => {
    return queryString.parse(location.search).email;
  };

  const doSubmit = async ({ password }) => {
    const success = await dispatch(actions.doResetPassword(token(), email(), password));
    if (success) dispatch(actions.doSigninWithEmailAndPassword(email(), password));
  };

  return (
    <ForgotPasswordPageWrapper>
      <Content>
        <FormLogo />

        <Formik initialValues={initialValues()} validationSchema={schema.schema} onSubmit={doSubmit}>
          {(form) => (
            <Form layout="vertical" onFinish={form.handleSubmit}>
              <InputFormItem name={fields.password.name} placeholder={fields.password.label} autoComplete={fields.password.name} type="password" size="large" layout={null} />
              <InputFormItem name={fields.confirmPassword.name} placeholder={fields.confirmPassword.label} autoComplete={fields.confirmPassword.name} type="password" size="large" layout={null} />
              <Button type="primary" size="large" block htmlType="submit" loading={loading} data-testid="passwordResetButton">
                {i18n('auth.passwordReset.message')}
              </Button>

              <OtherActions>
                <Link to="/auth/signin" data-testid="cancelPasswordReset">
                  {i18n('common.cancel')}
                </Link>
              </OtherActions>
            </Form>
          )}
        </Formik>
      </Content>
    </ForgotPasswordPageWrapper>
  );
};

const select = (state) => ({
  loading: selectors.selectLoadingPasswordReset(state),
  errorMessage: selectors.selectErrorMessage(state),
});

export default connect(select)(PasswordResetPage);
