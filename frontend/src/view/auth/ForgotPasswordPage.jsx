import { Button, Form } from 'antd';
import { Formik } from 'formik';
import actions from 'modules/auth/authActions';
import model from 'modules/auth/userModel';
import selectors from 'modules/auth/authSelectors';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import Content from 'view/auth/styles/Content';
import ForgotPasswordPageWrapper from 'view/auth/styles/ForgotPasswordPageWrapper';
import { FormLogo } from 'view/auth/styles/FormLogo';
import { OtherActions } from 'view/auth/styles/OtherActions';
import InputFormItem from 'view/shared/form/items/InputFormItem';
import FormSchema from 'view/shared/form/formSchema';
import { i18n } from '../../i18n';

const { fields } = model;

class ForgotPasswordPage extends Component {
  schema = new FormSchema(null, [fields.email]);

  initialValues = () => {
    return this.schema.initialValues();
  };

  doSubmit = ({ email }) => {
    const { dispatch } = this.props;
    dispatch(actions.doSendPasswordResetEmail(email));
  };

  render() {
    return (
      <ForgotPasswordPageWrapper>
        <Content>
          <FormLogo />

          <Formik initialValues={this.initialValues()} validationSchema={this.schema.schema} onSubmit={this.doSubmit}>
            {(form) => (
              <Form layout="vertical" onFinish={form.handleSubmit}>
                <InputFormItem name={fields.email.name} size="large" placeholder={fields.email.label} autoFocus autoComplete={fields.email.name} layout={null} />

                <Button type="primary" size="large" block htmlType="submit" loading={this.props.loading}>
                  {i18n('auth.passwordResetEmail.message')}
                </Button>

                <OtherActions>
                  <Link to="/auth/signin">{i18n('common.cancel')}</Link>
                </OtherActions>
              </Form>
            )}
          </Formik>
        </Content>
      </ForgotPasswordPageWrapper>
    );
  }
}

const select = (state) => ({
  loading: selectors.selectLoadingPasswordResetEmail(state),
});

export default connect(select)(ForgotPasswordPage);
