import actions from 'modules/auth/authActions';
import selectors from 'modules/auth/authSelectors';
import React, { useEffect } from 'react';
import Message from 'view/shared/message';
import ButtonLink from 'view/shared/styles/ButtonLink';
import { connect } from 'react-redux';
import Content from 'view/auth/styles/Content';
import EmailUnverifiedPageWrapper from 'view/auth/styles/EmailUnverifiedPageWrapper';
import { FormLogo } from 'view/auth/styles/FormLogo';
import { OtherActionsMultiple } from 'view/auth/styles/OtherActions';
import { i18n, i18nHtml } from 'i18n';
import service from '../../modules/auth/authService';

const InviteExpiredPage = (props) => {
  const { dispatch, email } = props;

  const doSignout = () => {
    dispatch(actions.doSignout(email));
  };

  const doStartTrial = () => {
    dispatch(actions.doConvertToTrial(email));
  };

  useEffect(() => {
    if (!email) {
      service.redirectToRoute('/auth/signin');
    } else {
      Message.success(i18n('auth.inviteExpired.resent'));
    }
  }, []);

  return (
    <EmailUnverifiedPageWrapper>
      <Content>
        <FormLogo />

        {email && <h3 style={{ textAlign: 'center' }}>{i18nHtml('auth.inviteExpired.message', email)}</h3>}
        {!email && <h3 style={{ textAlign: 'center' }}>{i18n('auth.inviteExpired.messageAnon')}</h3>}
        <p style={{ textAlign: 'center' }}>{i18n('auth.inviteExpired.resend')}</p>

        <OtherActionsMultiple>
          <ButtonLink onClick={doStartTrial}>{i18n('auth.startTrial')}</ButtonLink>
          <ButtonLink onClick={doSignout}>{i18n('auth.signinWithAnotherAccount')}</ButtonLink>
        </OtherActionsMultiple>
      </Content>
    </EmailUnverifiedPageWrapper>
  );
};

const select = (state) => ({
  email: selectors.selectCurrentUserEmail(state),
});

export default connect(select)(InviteExpiredPage);
