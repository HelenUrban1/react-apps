import { Button, Form } from 'antd';
import { Formik } from 'formik';
import actions from 'modules/auth/authActions';
import model from 'modules/auth/accountSignupModel';
import selectors from 'modules/auth/authSelectors';
import queryString from 'query-string';
import React, { useEffect } from 'react';
import { MailTwoTone } from '@ant-design/icons';
import { connect } from 'react-redux';
import ButtonLink from 'view/shared/styles/ButtonLink';
import Content from 'view/auth/styles/Content';
import { FormLogo } from 'view/auth/styles/FormLogo';
import { OtherActions } from 'view/auth/styles/OtherActions';
import RequestAccessPageWrapper from 'view/auth/styles/RequestAccessPageWrapper';
import { InputFormItemNotFast } from 'view/shared/form/items/InputFormItem';
import FormSchema from 'view/shared/form/formSchema';
import { i18n } from 'i18n';

const { fields } = model;

const RequestAccessPage = (props) => {
  const schema = new FormSchema(fields.id, [fields.adminEmail]);

  const { dispatch, location, email: userEmail } = props;

  const doSignout = () => {
    dispatch(actions.doSignout(userEmail));
  };

  const clearErrorMessage = () => {
    dispatch(actions.doClearErrorMessage());
  };

  const emailFromUrl = () => {
    return queryString.parse(location.search).email;
  };

  const tokenFromUrl = () => {
    return queryString.parse(props.location.search).token;
  };

  const policyFromUrl = () => {
    return queryString.parse(location.search).policy;
  };

  const doSubmit = ({ adminEmail }) => {
    dispatch(actions.doSendRequest(emailFromUrl(), adminEmail, tokenFromUrl()));
  };

  const handleChange = (event, form) => {
    const { errorMessage } = props;
    if (errorMessage) {
      clearErrorMessage();
    }

    form.handleChange(event);
  };

  useEffect(() => {
    clearErrorMessage();
    if (policyFromUrl() !== 'Email') dispatch(actions.doSendRequest(emailFromUrl(), undefined, tokenFromUrl()));
  }, []);

  const { errorMessage, loading, email } = props;

  const initialValues = () => {
    return {
      adminEmail: undefined,
    };
  };

  const getPageMessage = (err, load, policy) => {
    if (load) return <p style={{ textAlign: 'center' }}>{i18n('auth.requestForm.loading')}</p>;

    if (err)
      return (
        <>
          <h3 style={{ textAlign: 'center' }}>{i18n('auth.requestForm.error', email)}</h3>
          <p style={{ textAlign: 'center' }}>{i18n('auth.requestForm.errorTip')}</p>
          <span style={{ textAlign: 'center', fontSize: '24px' }}>
            <MailTwoTone twoToneColor="#f5222d" />
          </span>
        </>
      );

    if (policy === 'Email')
      return (
        <>
          <h3 style={{ textAlign: 'center' }}>{i18n('auth.requestForm.email', email)}</h3>
          <p style={{ textAlign: 'center' }}>{i18n('auth.requestForm.emailTip')}</p>
        </>
      );

    return (
      <>
        <h3 style={{ textAlign: 'center' }}>{i18n('auth.requestForm.success', email)}</h3>
        <p style={{ textAlign: 'center' }}>{i18n('auth.requestForm.successTip')}</p>
        <span style={{ textAlign: 'center', fontSize: '24px' }}>
          <MailTwoTone twoToneColor="#52c41a" />
        </span>
      </>
    );
  };

  return (
    <RequestAccessPageWrapper>
      <Content>
        <FormLogo />

        {getPageMessage(errorMessage, loading, policyFromUrl())}

        {policyFromUrl() === 'Email' && (
          <Formik initialValues={initialValues()} validationSchema={schema.schema} onSubmit={doSubmit}>
            {(form) => {
              return (
                <Form layout="vertical" onFinish={form.handleSubmit} onChange={(e) => handleChange(e, form)}>
                  <InputFormItemNotFast name={fields.adminEmail.name} placeholder={fields.adminEmail.label} autoComplete={fields.adminEmail.name} size="large" autoFocus errorMessage={errorMessage} layout={null} form={form} />

                  <Button disabled={!form.values.adminEmail} type="primary" size="large" block htmlType="submit" loading={loading}>
                    {i18n('auth.requestForm.button')}
                  </Button>
                </Form>
              );
            }}
          </Formik>
        )}
        <OtherActions>
          <ButtonLink onClick={doSignout}>{i18n('auth.signinWithAnotherAccount')}</ButtonLink>
        </OtherActions>
      </Content>
    </RequestAccessPageWrapper>
  );
};

const select = (state) => ({
  email: selectors.selectCurrentUserEmail(state),
  loading: selectors.selectLoading(state),
  errorMessage: selectors.selectErrorMessage(state),
});

export default connect(select)(RequestAccessPage);
