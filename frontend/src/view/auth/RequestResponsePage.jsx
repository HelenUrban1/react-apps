import actions from 'modules/auth/authActions';
import selectors from 'modules/auth/authSelectors';
import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import Content from 'view/auth/styles/Content';
import { Link } from 'react-router-dom';
import { FormLogo } from 'view/auth/styles/FormLogo';
import { OtherActions } from 'view/auth/styles/OtherActions';
import queryString from 'query-string';
import RequestResponsePageWrapper from 'view/auth/styles/RequestResponsePageWrapper';
import { i18n } from '../../i18n';
import { HealthCheck } from '../layout/HealthCheck';

const RequestResponsePage = (props) => {
  const { location, dispatch, loading, errorMessage } = props;

  const emailFromUrl = () => {
    return queryString.parse(location.search).email;
  };

  const tokenFromUrl = () => {
    return queryString.parse(location.search).token;
  };

  const responseFromUrl = () => {
    return queryString.parse(location.search).response;
  };

  useEffect(() => {
    const token = tokenFromUrl();
    const adminEmail = emailFromUrl();
    const resp = responseFromUrl();
    if (token && adminEmail && resp !== 'expired') {
      if (resp === 'approve') {
        dispatch(actions.doAcceptRequest(adminEmail, token, undefined));
      } else {
        dispatch(actions.doRejectRequest(adminEmail, token));
      }
    }
  }, []);

  const getPageMessage = (err, load, res) => {
    if (load) return <p style={{ textAlign: 'center' }}>{i18n('auth.requestResponse.loading')}</p>;

    if (err)
      return (
        <>
          <h3 style={{ textAlign: 'center' }}>{i18n('auth.requestResponse.error')}</h3>
          <p style={{ textAlign: 'center' }}>{i18n('auth.requestResponse.errorTip')}</p>
        </>
      );

    if (res === 'accept')
      return (
        <>
          <h3 style={{ textAlign: 'center' }}>{i18n('auth.requestResponse.accepted')}</h3>
          <p style={{ textAlign: 'center' }}>{i18n('auth.requestResponse.acceptedTip')}</p>
        </>
      );

    if (res === 'reject')
      return (
        <>
          <h3 style={{ textAlign: 'center' }}>{i18n('auth.requestResponse.rejected')}</h3>
          <p style={{ textAlign: 'center' }}>{i18n('auth.requestResponse.rejectedTip')}</p>
        </>
      );

    if (res === 'expired')
      return (
        <>
          <h3 style={{ textAlign: 'center' }}>{i18n('auth.requestResponse.expired')}</h3>
          <p style={{ textAlign: 'center' }}>{i18n('auth.requestResponse.expiredTip')}</p>
        </>
      );

    return (
      <>
        <h3 style={{ textAlign: 'center' }}>{i18n('auth.requestResponse.loading')}</h3>
      </>
    );
  };

  return (
    <RequestResponsePageWrapper>
      <HealthCheck />
      <Content>
        <FormLogo />

        {getPageMessage(errorMessage, loading, responseFromUrl())}

        <OtherActions>
          <Link to="/auth/signin">{i18n('auth.signinWithAnotherAccount')}</Link>
        </OtherActions>
      </Content>
    </RequestResponsePageWrapper>
  );
};

const select = (state) => ({
  loading: selectors.selectLoading(state),
  errorMessage: selectors.selectErrorMessage(state),
});

export default connect(select)(RequestResponsePage);
