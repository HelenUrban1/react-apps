import { Button, Form } from 'antd';
import actions from 'modules/auth/authActions';
import selectors from 'modules/auth/authSelectors';
import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import Message from 'view/shared/message';
import queryString from 'query-string';
import Content from 'view/auth/styles/Content';
import EmailUnverifiedPageWrapper from 'view/auth/styles/EmailUnverifiedPageWrapper';
import { FormLogo } from 'view/auth/styles/FormLogo';
import { OtherActions } from 'view/auth/styles/OtherActions';
import ButtonLink from 'view/shared/styles/ButtonLink';
import { i18n, i18nHtml } from 'i18n';
import { Formik } from 'formik';
import { InputFormItemNotFast } from 'view/shared/form/items/InputFormItem';
import FormSchema from 'view/shared/form/formSchema';
import model from 'modules/auth/accountSignupModel';

const { fields } = model;

const EmailUnverifiedPage = (props) => {
  const schema = new FormSchema(fields.id, [fields.email]);
  const { loading, email, dispatch, location, errorMessage } = props;
  const emailIdentifierChars = 7;

  const getStatusFromUrl = () => {
    return queryString.parse(location.search).status;
  };

  const status = getStatusFromUrl();

  const getSentFromUrl = () => {
    return queryString.parse(location.search).sent;
  };

  const sent = getSentFromUrl();

  const getEmailFromUrl = () => {
    return location.search.slice(emailIdentifierChars);
  };

  const doSignout = () => {
    dispatch(actions.doSignout(email));
  };

  const doSubmit = ({ email: formEmail }) => {
    dispatch(actions.doSendEmailConfirmation(formEmail || email));
  };

  const clearErrorMessage = () => {
    dispatch(actions.doClearErrorMessage());
  };

  useEffect(() => {
    clearErrorMessage();
    if (sent === 'true') Message.success(i18n('auth.verificationEmailSuccess'));
  }, []);

  const initialValues = () => {
    return {
      email: email || getEmailFromUrl(),
    };
  };

  const getMessage = () => {
    if (!email) return i18nHtml('auth.requestVerification');
    if (status === 'expired' && sent !== 'true') {
      return i18nHtml('auth.emailUnverified.expired');
    }
    return i18nHtml('auth.emailUnverified.message', email);
  };

  return (
    <EmailUnverifiedPageWrapper>
      <Content>
        <FormLogo />

        <h3 style={{ textAlign: 'center' }}>{getMessage()}</h3>

        <Formik initialValues={initialValues()} validationSchema={schema.schema} onSubmit={doSubmit}>
          {(form) => {
            return (
              <Form layout="vertical" onFinish={form.handleSubmit}>
                {!email && <InputFormItemNotFast name={fields.email.name} placeholder={fields.email.label} autoComplete={fields.email.name} size="large" autoFocus errorMessage={errorMessage} layout={null} form={form} />}

                {(sent !== 'true' || !email) && (
                  <Button type="primary" size="large" block htmlType="submit" loading={loading} disabled={!form.values.email} data-testid="resendVerificationButton">
                    {i18n('auth.emailUnverified.submit')}
                  </Button>
                )}
              </Form>
            );
          }}
        </Formik>

        <OtherActions>
          <ButtonLink onClick={doSignout}>{i18n('auth.signinWithAnotherAccount')}</ButtonLink>
        </OtherActions>
      </Content>
    </EmailUnverifiedPageWrapper>
  );
};

const select = (state) => ({
  email: selectors.selectCurrentUserEmail(state),
  loading: selectors.selectLoadingEmailConfirmation(state),
  errorMessage: selectors.selectErrorMessage(state),
});

export default connect(select)(EmailUnverifiedPage);
