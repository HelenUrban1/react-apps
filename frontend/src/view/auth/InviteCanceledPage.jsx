import { Button } from 'antd';
import actions from 'modules/auth/authActions';
import selectors from 'modules/auth/authSelectors';
import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import Content from 'view/auth/styles/Content';
import EmailUnverifiedPageWrapper from 'view/auth/styles/EmailUnverifiedPageWrapper';
import { FormLogo } from 'view/auth/styles/FormLogo';
import { OtherActions } from 'view/auth/styles/OtherActions';
import ButtonLink from 'view/shared/styles/ButtonLink';
import { i18n, i18nHtml } from 'i18n';
import service from '../../modules/auth/authService';

const InviteCanceledPage = (props) => {
  const { dispatch, email } = props;

  const doSignout = () => {
    dispatch(actions.doSignout(email));
  };

  const doStartTrial = () => {
    dispatch(actions.doConvertToTrial(email));
  };

  useEffect(() => {
    if (!email) {
      service.redirectToRoute('/auth/signin');
    }
  }, []);

  return (
    <EmailUnverifiedPageWrapper>
      <Content>
        <FormLogo />

        <h3 style={{ textAlign: 'center' }}>{i18nHtml('auth.inviteCanceled.message', email)}</h3>
        <p>{i18n('auth.inviteCanceled.support')}</p>

        <Button onClick={doStartTrial} style={{ marginTop: '24px' }} type="primary" size="large" block htmlType="submit">
          {i18n('auth.startTrial')}
        </Button>

        <OtherActions>
          <ButtonLink onClick={doSignout}>{i18n('auth.signinWithAnotherAccount')}</ButtonLink>
        </OtherActions>
      </Content>
    </EmailUnverifiedPageWrapper>
  );
};

const select = (state) => ({
  email: selectors.selectCurrentUserEmail(state),
});

export default connect(select)(InviteCanceledPage);
