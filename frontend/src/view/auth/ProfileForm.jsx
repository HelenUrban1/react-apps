import { CloseOutlined, SaveOutlined, UndoOutlined } from '@ant-design/icons';
import { Button, Form } from 'antd';
import { Formik } from 'formik';
import actions from 'modules/auth/authActions';
import model from 'modules/auth/userModel';
import selectors from 'modules/auth/authSelectors';
import { i18n } from 'i18n';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import ImagesFormItem from 'view/shared/form/items/ImagesFormItem';
import InputFormItem, { InputFormItemNotFast } from 'view/shared/form/items/InputFormItem';
import ViewFormItem from 'view/shared/form/items/ViewFormItem';
import FormSchema from 'view/shared/form/formSchema';
import FormWrapper, { tailFormItemLayout } from 'view/shared/styles/FormWrapper';

const { fields } = model;

class ProfileFormPage extends Component {
  schema = new FormSchema(fields.id, [fields.email, fields.firstName, fields.lastName, fields.phoneNumber, fields.avatarsProfile]);
  handleSubmit = (values) => {
    const { dispatch } = this.props;
    dispatch(actions.doUpdateProfile(values.email, values.firstName, values.lastName, values.phoneNumber, values.avatars));
  };

  handleChange(event, form) {
    if (this.props.errorMessage) {
      this.clearErrorMessage();
    }

    form.handleChange(event);
  }

  clearErrorMessage = () => {
    const { dispatch } = this.props;
    dispatch(actions.doClearErrorMessage());
  };

  initialValues = () => {
    const { currentUser } = this.props;
    return this.schema.initialValues(currentUser);
  };

  renderForm() {
    const { saveLoading } = this.props;

    return (
      <FormWrapper>
        <Formik initialValues={this.initialValues()} validationSchema={this.schema.schema} onSubmit={this.handleSubmit}>
          {(form) => {
            return (
              <Form layout="vertical" onFinish={form.handleSubmit}>
                <ViewFormItem name={fields.email.name} label={fields.email.label} />

                <InputFormItemNotFast name={fields.firstName.name} label={fields.firstName.label} autoComplete={fields.firstName.name} autoFocus form={form} errorMessage={this.props.errorMessage} required={fields.firstName.required} />

                <InputFormItemNotFast name={fields.lastName.name} label={fields.lastName.label} autoComplete={fields.lastName.name} form={form} errorMessage={this.props.errorMessage} />

                <InputFormItem name={fields.phoneNumber.name} label={fields.phoneNumber.label} autoComplete={fields.phoneNumber.name} prefix="+" />

                <ImagesFormItem
                  name={fields.avatarsProfile.name}
                  label={fields.avatarsProfile.label}
                  path={fields.avatarsProfile.path(this.props.currentUser.authenticationUid)}
                  schema={{
                    size: fields.avatarsProfile.size,
                  }}
                  max={fields.avatarsProfile.max}
                />

                <Form.Item className="form-buttons" {...tailFormItemLayout}>
                  <Button loading={saveLoading} type="primary" onClick={form.handleSubmit} icon={<SaveOutlined />}>
                    {i18n('common.save')}
                  </Button>

                  <Button disabled={saveLoading} onClick={form.handleReset} icon={<UndoOutlined />}>
                    {i18n('common.reset')}
                  </Button>

                  {this.props.onCancel ? (
                    <Button disabled={saveLoading} onClick={() => this.props.onCancel()} icon={<CloseOutlined />}>
                      {i18n('common.cancel')}
                    </Button>
                  ) : null}
                </Form.Item>
              </Form>
            );
          }}
        </Formik>
      </FormWrapper>
    );
  }

  render() {
    return this.renderForm();
  }
}

function select(state) {
  return {
    saveLoading: selectors.selectLoadingUpdateProfile(state),
    currentUser: selectors.selectCurrentUser(state),
    errorMessage: selectors.selectErrorMessage(state),
  };
}

export default connect(select)(ProfileFormPage);
