import React from 'react';
import { SaveOutlined, UndoOutlined } from '@ant-design/icons';
import { Button, Form } from 'antd';
import { Formik } from 'formik';
import Analytics from 'modules/shared/analytics/analytics';
import model from 'modules/account/accountModel';
import authSelectors from 'modules/auth/authSelectors';
import { doUpdateAccount } from 'modules/account/accountActions';

import { i18n } from 'i18n';
import { useDispatch, useSelector } from 'react-redux';
import InputFormItem from 'view/shared/form/items/InputFormItem';
import SelectFormItem from 'view/shared/form/items/SelectFormItem';
import { selectSubscription } from 'modules/subscription/subscriptionSelectors';

import FormSchema from 'view/shared/form/formSchema';
import Message from 'view/shared/message';
import PermissionChecker from 'modules/auth/permissionChecker';
import Permissions from 'security/permissions';

const { fields } = model;

const CompanyProfileForm = () => {
  const schema = new FormSchema(fields.id, [fields.status, fields.settings, fields.orgName, fields.orgPhone, fields.orgEmail, fields.orgStreet, fields.orgStreet2, fields.orgCity, fields.orgRegion, fields.orgPostalcode, fields.orgCountry]);
  const saveLoading = useSelector((state) => authSelectors.selectLoadingUpdateProfile(state));
  const { organizations } = useSelector((state) => state.session);
  const { currentUser } = useSelector((state) => state.auth);
  const subscription = useSelector((state) => selectSubscription(state));
  const currentAccount = useSelector((state) => state.account);
  const dispatch = useDispatch();

  const permissionValidator = new PermissionChecker(currentUser);
  const userPermissions = {
    hasOwnership: permissionValidator.match(Permissions.values.accountOwner),
    hasManageAccess: permissionValidator.match(Permissions.values.accountManagement),
    hasBillingAccess: permissionValidator.match(Permissions.values.accountBillingAccess),
    hasReviewer: permissionValidator.match(Permissions.values.reviewer),
  };
  const handleSubmit = async (values) => {
    dispatch(
      doUpdateAccount(currentAccount.id, {
        dbHost: values.dbHost,
        dbName: values.dbName,
        status: values.status,
        settings: values.settings,
        orgEmail: values.orgEmail,
        orgName: values.orgName,
        orgPhone: values.orgPhone,
        orgStreet: values.orgStreet,
        orgStreet2: values.orgStreet2,
        orgCity: values.orgCity,
        orgRegion: values.orgRegion,
        orgPostalcode: values.orgPostalcode,
        orgCountry: values.orgCountry,
        organizationId: organizations.self?.id,
      }),
    );
    Analytics.track({
      event: Analytics.events.companyProfileEdited,
      properties: {
        companyId: subscription.companyId,
        oldName: currentAccount.orgName,
        oldPhone: currentAccount.orgPhone,
        oldStreet1: currentAccount.orgStreet,
        oldStreet2: currentAccount.orgStreet2,
        oldCity: currentAccount.orgCity,
        oldRegion: currentAccount.orgRegion,
        oldPostalCode: currentAccount.orgPostalcode,
        oldCountry: currentAccount.orgCountry,
        newName: values.orgName,
        newPhone: values.orgPhone,
        newStreet1: values.orgStreet,
        newStreet2: values.orgStreet2,
        newCity: values.orgCity,
        newRegion: values.orgRegion,
        newPostalCode: values.orgPostalcode,
        newCountry: values.orgCountry,
      },
    });
    // Update the account group with the details
    Analytics.group({
      groupId: currentUser.accountId,
      traits: {
        groupType: 'Account',
        name: values.orgName,
        phone: values.orgPhone,
        street1: values.orgStreet,
        street2: values.orgStreet2,
        city: values.orgCity,
        region: values.orgRegion,
        postalCode: values.orgPostalcode,
        country: values.orgCountry,
      },
    });
    Message.success(i18n('entities.account.update.success'));
  };

  return (
    <div className="settings-company-edit">
      <Formik initialValues={currentAccount} validationSchema={schema.schema} onSubmit={handleSubmit} enableReinitialize>
        {(form) => {
          return (
            <Form layout="vertical" onFinish={form.handleSubmit} data-cy="setting-company-edit">
              <InputFormItem disabled={!userPermissions.hasOwnership} name={fields.orgName.name} label={fields.orgName.label} required={fields.orgName.required} />
              <InputFormItem disabled={!userPermissions.hasOwnership} name={fields.orgPhone.name} label={fields.orgPhone.label} autoComplete={fields.orgPhone.name} prefix="+" />
              <InputFormItem disabled={!userPermissions.hasOwnership} name={fields.orgStreet.name} label={fields.orgStreet.label} required={fields.orgStreet.required} />
              <InputFormItem disabled={!userPermissions.hasOwnership} name={fields.orgStreet2.name} label={fields.orgStreet2.label} required={fields.orgStreet2.required} />
              <InputFormItem disabled={!userPermissions.hasOwnership} name={fields.orgCity.name} label={fields.orgCity.label} required={fields.orgCity.required} />
              <SelectFormItem
                disabled={!userPermissions.hasOwnership}
                autoComplete="new-password" // to trick google autocomplete
                name={fields.orgRegion.name}
                label={fields.orgRegion.label}
                options={fields.orgRegion.options.map((item) => ({
                  value: item.id,
                  label: item.label,
                }))}
                required={fields.orgRegion.required}
              />
              <InputFormItem disabled={!userPermissions.hasOwnership} name={fields.orgPostalcode.name} label={fields.orgPostalcode.label} required={fields.orgPostalcode.required} />
              <SelectFormItem
                disabled={!userPermissions.hasOwnership}
                autoComplete="new-password" // to trick google autocomplete
                name={fields.orgCountry.name}
                label={fields.orgCountry.label}
                options={fields.orgCountry.options.map((item) => ({
                  value: item.id,
                  label: item.label,
                }))}
                required={fields.orgCountry.required}
              />
              <Form.Item className="form-buttons">
                <Button disabled={!userPermissions.hasOwnership} loading={saveLoading} type="primary" size="large" onClick={form.handleSubmit} icon={<SaveOutlined />}>
                  {i18n('common.save')}
                </Button>

                <Button disabled={!userPermissions.hasOwnership || saveLoading} size="large" onClick={form.handleReset} icon={<UndoOutlined />}>
                  {i18n('common.reset')}
                </Button>
              </Form.Item>
            </Form>
          );
        }}
      </Formik>
    </div>
  );
};

export default CompanyProfileForm;
