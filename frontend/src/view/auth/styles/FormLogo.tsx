import React from 'react';
import { TextLogo } from 'styleguide/Logo/Logo';

export const FormLogo = () => {
  return (
    <section className="ix-logo-title">
      <TextLogo />
      <hr className="ix-logo-title-hr" />
    </section>
  );
};
