import styled from 'styled-components';
import Wrapper from 'view/auth/styles/Wrapper';

const RequestResponsePageWrapper = styled(Wrapper)`
  background-image: url(/images/emailUnverified.jpg);
`;

export default RequestResponsePageWrapper;
