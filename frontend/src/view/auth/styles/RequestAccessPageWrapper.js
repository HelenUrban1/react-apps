import styled from 'styled-components';
import Wrapper from 'view/auth/styles/Wrapper';

const RequestAccessPageWrapper = styled(Wrapper)`
  background-image: url(/images/emailUnverified.jpg);
`;

export default RequestAccessPageWrapper;
