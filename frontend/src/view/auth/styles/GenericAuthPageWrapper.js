import styled from 'styled-components';
import Wrapper from 'view/auth/styles/Wrapper';

const GenericAuthPageWrapper = styled(Wrapper)`
  background-image: url(/images/emailUnverified.jpg);
`;

export default GenericAuthPageWrapper;
