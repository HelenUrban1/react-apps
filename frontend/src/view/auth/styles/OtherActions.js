import styled from 'styled-components';

export const OtherActions = styled.div`
  margin-top: 36px;
  text-align: center;
  display: flex;
  flex-direction: row;
  justify-content: space-evenly;
`;

export const OtherActionsMultiple = styled.div`
  margin-top: 36px;
  text-align: center;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
`;

export default OtherActions;
