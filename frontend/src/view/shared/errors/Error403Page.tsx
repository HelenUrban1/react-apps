import { Button } from 'antd';
import React from 'react';
import { Link } from 'react-router-dom';
import Permissions from 'security/permissions';
import PermissionChecker from 'modules/auth/permissionChecker';
import ErrorWrapper from 'view/shared/errors/styles/ErrorWrapper';
import { useSelector } from 'react-redux';
import authSelectors from 'modules/auth/authSelectors';
import { AppState } from 'modules/state';
import { i18n } from '../../../i18n';

const Error403Page = () => {
  const user = useSelector((state: AppState) => authSelectors.selectCurrentUser(state));
  const permissionValidator = new PermissionChecker(user);
  const isReviewer = permissionValidator.match(Permissions.values.reviewer);
  const hasPartReadAccess = permissionValidator.match(Permissions.values.accountManagement);
  const hasBillingAccess = permissionValidator.match(Permissions.values.accountBillingAccess);
  const isMetro = permissionValidator.match(Permissions.values.metro);

  let redirectUrl = '/';
  if (hasBillingAccess && !hasPartReadAccess) redirectUrl = '/settings/billing';
  if (isReviewer) redirectUrl = '/review';
  if (!hasBillingAccess && !hasPartReadAccess && !isReviewer && isMetro) redirectUrl = '/metro/operator';

  return (
    <ErrorWrapper>
      <div className="exception">
        <div className="imgBlock">
          <div
            className="imgEle"
            style={{
              backgroundImage: `url(/images/403.svg)`,
            }}
          />
        </div>
        <div className="content">
          <h1>403</h1>
          <div className="desc">{i18n('errors.403')}</div>
          <div className="actions">
            <Link to={redirectUrl}>
              <Button type="primary">{i18n('errors.backToHome')}</Button>
            </Link>
          </div>
        </div>
      </div>
    </ErrorWrapper>
  );
};

export default Error403Page;
