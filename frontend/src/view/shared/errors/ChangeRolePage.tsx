import React, { useEffect } from 'react';
import { Button } from 'antd';
import ErrorWrapper from 'view/shared/errors/styles/ErrorWrapper';
import { useLocation } from 'react-router';
import { LocationState } from 'history';
import { i18n } from '../../../i18n';

const ChangeRolePage = () => {
  const { state } = useLocation<LocationState>() as { state: { newRoles: string[] } };

  const getNavigate = () => {
    // user is still has admin privilege, return to seats
    if (state.newRoles.includes('Admin') || state.newRoles.includes('Owner')) {
      return '/settings/seats';
    }
    // user only has a billing role, go to billing
    if (state.newRoles.includes('Billing')) {
      return '/settings/billing';
    }
    return '/';
  };

  const redirectUrl = getNavigate();

  const redirect = () => {
    window.location.replace(redirectUrl);
  };

  // redirect user after 5 seconds
  const startTimer = () => {
    setTimeout(function () {
      redirect();
    }, 5000);
  };

  // start timer on page load
  useEffect(() => {
    startTimer();
  });

  return (
    <ErrorWrapper>
      <div className="exception">
        <div className="imgBlock">
          <div
            className="imgEle"
            style={{
              backgroundImage: `url(/images/user-swap.svg)`,
            }}
          />
        </div>
        <div className="content">
          <h1>{i18n('errors.changingHats')}</h1>
          <div className="desc">{i18n('errors.countdown')}</div>
          <div className="actions">
            <Button type="primary" className="btn-primary" onClick={redirect}>
              {i18n('common.back')}
            </Button>
          </div>
        </div>
      </div>
    </ErrorWrapper>
  );
};

export default ChangeRolePage;
