import React, { useState } from 'react';
import { TagOutlined } from '@ant-design/icons';
import { Button, Modal, Tooltip } from 'antd';
import { i18n } from 'i18n';
import { ChangeSubscriptionModal } from 'domain/setting/billing/modals/ChangeSubscriptionModal';
import './trialBar.less';
import Analytics from 'modules/shared/analytics/analytics';
import { Subscription, BillingPeriodEnum, ComponentInput, ComponentInfo } from 'domain/setting/subscriptionTypes';
import { useDispatch, useSelector } from 'react-redux';
import { AppState } from 'modules/state';
import { SubscriptionAPI } from 'domain/setting/subscriptionApi';
import { doUpdateCurrentSubscription } from 'modules/subscription/subscriptionActions';
import log from 'modules/shared/logger';
import { ContactInfo } from 'styleguide/PricingTable/ContactInfo';
import { getTrialExpiredCountdown } from 'utils/DateOperations';
import { bookTrainingUrl } from 'view/global/defaults';

interface Props {
  accountId: string;
  hideExpired: () => void;
  isBilling: boolean;
}

const TrialBar: React.FC<Props> = ({ accountId, hideExpired, isBilling }) => {
  const dispatch = useDispatch();

  // --------------- State ---------------
  const { data: subscription, show, type, limit, state } = useSelector((reduxState: AppState) => reduxState.subscription);
  const [visibility, setVisibility] = useState<boolean>(false);
  const [creditCardToken, setToken] = useState<string | undefined>();
  // const [showExpired, setShowExpired] = useState(show && subscription?.status !== 'trialing' && subscription?.status !== 'active');
  const [showExpired, setShowExpired] = useState(show && state === 'trial_ended');

  // ------------- Subscribe -------------
  const showChangeModal = () => {
    setVisibility(isBilling);
  };
  const closeChangeModal = () => {
    setVisibility(false);
  };

  const changePeriod = (sub: Subscription, period: BillingPeriodEnum) => {
    const input = { ...sub };
    input.billingPeriod = period;
    input.pendingBillingPeriod = null;
    if (input.pendingBillingPeriod === period) {
      input.pendingBillingPeriod = null;
    }
    return input;
  };

  const changePrice = (sub: Subscription, price: number) => {
    const input = { ...sub };
    if (input.currentBillingAmountInCents && price < input.currentBillingAmountInCents) {
      input.pendingBillingAmountInCents = price;
    } else {
      input.currentBillingAmountInCents = price;
      input.pendingBillingAmountInCents = null;
    }
    return input;
  };

  const confirmSubscription = ({
    //
    productHandle,
    period,
    price,
    handle,
    componentInputs,
    pendingComponents,
  }: {
    productHandle?: string;
    period?: BillingPeriodEnum;
    price?: number;
    handle?: string;
    componentInputs?: ComponentInput[];
    pendingComponents?: ComponentInfo;
  }) => {
    if (!subscription) {
      log.warn('No Subscription');
      return;
    }
    let input = { ...subscription };

    if (period) {
      input = changePeriod(input, period);
    }
    if (price !== undefined) {
      input = changePrice(input, price);
    }
    if (productHandle) {
      input.providerProductHandle = productHandle;
    }

    let inputs: ComponentInput[] = componentInputs ? [...componentInputs] : [];
    if (pendingComponents) {
      inputs = inputs.map((comp) => {
        const pend = pendingComponents[comp.component_id];
        if (pend) {
          return { ...comp, ...pend };
        }
        return comp;
      });
    }

    const data = SubscriptionAPI.createGraphqlObject(input);

    Analytics.track({
      event: Analytics.events.accountSubscriptionStarted,
      properties: {
        'chargify.id': subscription.paymentSystemId,
        'chargify.customer': subscription.customerId,
        'account.id': subscription.accountId,
      },
    });
    dispatch(doUpdateCurrentSubscription(subscription.id, data, 'Trial_To_Subscription', inputs, creditCardToken, handle) as any);
  };

  // --------- Change Billing Card ---------

  const closeCardModal = () => {
    Analytics.track({
      event: Analytics.events.trialCreateSubscription,
      properties: {
        account: accountId,
        'trial-type': type,
      },
    });
    Modal.destroyAll();
  };

  const saveCreditCard = (token: string) => {
    Analytics.track({
      event: Analytics.events.trialSetCreditCard,
      properties: {
        account: accountId,
        'trial-type': type,
        billingToken: token,
      },
    });
    setToken(token);
    Modal.destroyAll();
  };

  const removeCreditCard = () => {
    Analytics.track({
      event: Analytics.events.trialRemoveCreditCard,
      properties: {
        account: accountId,
        'trial-type': type,
      },
    });
    setToken(undefined);
    Modal.destroyAll();
  };

  const subscribeModal = (
    <>
      <Tooltip title={!isBilling ? i18n('entities.subscription.trial.contactAdmin') : ''}>
        <Button type="default" size="large" className="btn-primary" onClick={showChangeModal} data-cy="trial-subscribe-btn" disabled={!isBilling}>
          <TagOutlined style={{ marginRight: '5px' }} />
          {i18n('entities.subscription.trial.subscribe')}
        </Button>
      </Tooltip>
      <ChangeSubscriptionModal
        newSubscription
        token={creditCardToken || subscription?.cardId?.toString()}
        visibility={visibility}
        closeChangeModal={closeChangeModal}
        saveCreditCard={saveCreditCard}
        removeCreditCard={removeCreditCard}
        closeCardModal={closeCardModal}
        handleSubscriptionChange={confirmSubscription}
      />
    </>
  );

  const closeExpired = () => {
    setShowExpired(false);
    hideExpired();
  };

  const subscribeFromExpired = () => {
    closeExpired();
    showChangeModal();
  };

  const handleContactSales = () => {
    window.open(bookTrainingUrl, '_blank');
  };

  const expiredModal = (
    <Modal
      key="expired-trial-modal"
      data-cy="expired-trial-modal"
      className="expired-trial-modal"
      visible={showExpired}
      maskClosable={false}
      title={i18n('entities.subscription.trial.timeExpired')}
      onCancel={closeExpired}
      footer={[
        <Button className="btn-secondary" data-cy="close-expired-modal-btn" key="close-btn" onClick={handleContactSales}>
          {i18n('entities.subscription.trial.contactSalesBtn')}
        </Button>,
        <Tooltip key="contact-admin-tip" data-cy="contact-admin-tip" title={!isBilling ? i18n('entities.subscription.trial.contactAdmin') : ''}>
          <Button className="btn-primary" data-cy="subscribe-expired-modal-btn" key="subscribe-btn" onClick={subscribeFromExpired} disabled={!isBilling}>
            {i18n('entities.subscription.trial.subscribe')}
          </Button>
        </Tooltip>,
      ]}
    >
      <section key="trial-ended-copy-key" className="trial-ended-modal-copy" data-cy="expired-modal-copy-section">
        <section className="countdown" data-cy="expired-modal-countdown">
          {subscription?.trialEndedAt && (
            <span className="days-left-num" data-cy="expired-modal-countdown-num">
              {getTrialExpiredCountdown(subscription.trialEndedAt)}
            </span>
          )}
          <span className="days-left-text" data-cy="expired-modal-countdown-text">
            {i18n('entities.subscription.trial.daysLeft')}
          </span>
        </section>
        <section className="copy">{i18n('entities.subscription.trial.expiredTrialCopy')}</section>
        <section className="contact-support">{i18n('entities.subscription.trial.contactSales')}</section>
        <div className="info-text" data-cy="contact-message">
          <ContactInfo />
        </div>
      </section>
    </Modal>
  );

  if (type === 'Freemium' && typeof limit === 'number') {
    return (
      <div className={`trial-bar ${state}`} data-cy="trial-bar-container">
        <span data-cy="trial-bar-remaining">{(subscription?.partsUsed === 0 || (subscription?.partsUsed && subscription?.partsUsed < limit)) && i18n('entities.subscription.trial.drawingLimit', limit - (subscription?.partsUsed || 0))}</span>
        {subscribeModal}
        {expiredModal}
      </div>
    );
  }
  return (
    <div className={`trial-bar ${state}`} data-cy="trial-bar-container">
      <span data-cy="trial-bar-remaining">{state === 'trial' ? i18n('entities.subscription.trial.timeExpire', limit) : i18n('entities.subscription.trial.timeExpired')}</span>
      {subscribeModal}
      {expiredModal}
    </div>
  );
};

export default TrialBar;
