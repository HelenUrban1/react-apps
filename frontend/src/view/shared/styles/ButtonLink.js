import styled from 'styled-components';
import IxcTheme from 'styleguide/styles/IxcTheme';

const ButtonLink = styled.button`
  background-color: transparent;
  border: none;
  cursor: pointer;
  color: ${IxcTheme.colors.primaryColor};
  text-decoration: none;
  display: inline;
  margin: 0;
  padding: 0;

  &:hover {
    text-decoration: none;
  }
  &:focus {
    text-decoration: none;
  }

  &:disabled {
    cursor: not-allowed;
    color: rgba(0, 0, 0, 0.5);
  }
`;

export default ButtonLink;
