import React, { useState, useEffect } from 'react';
import { Input, Modal } from 'antd';
import { useDispatch, useSelector } from 'react-redux';
import QRCode from 'react-qr-code';
import config from 'config';
import { i18n } from 'i18n';
import { AppState } from 'modules/state';
import actions from 'modules/auth/authActions';
import axios from 'modules/shared/network/axios';
import log from 'modules/shared/logger';
import './styles/mfa.less';
import { useHistory } from 'react-router-dom';
import { ContactInfo } from 'styleguide/PricingTable/ContactInfo';
import Analytics from 'modules/shared/analytics/analytics';

const MFAModal = () => {
  const { mfaEnabled, mfaRegistered, loadingMfaRegistration, errorMessage, currentUser, jwt } = useSelector((state: AppState) => state.auth);
  const dispatch = useDispatch();
  const [qr, setQR] = useState<string>();
  const [code, setCode] = useState<string>('');

  const history = useHistory();

  const fetchQRCode = async () => {
    // Enable MFA in Cognito and get the secret code
    const secretCode = await axios({
      method: 'POST',
      url: `${config.backendUrl}/cognitoEnableTotpMfa`,
      headers: {
        authorization: jwt ? `${jwt}` : '',
      },
    }).catch((err) => {
      log.error(err);
      dispatch({ type: actions.MFA_ERROR, payload: i18n('mfa.register.error') });
    });
    if (secretCode && secretCode.data && typeof secretCode.data === 'string') {
      // make code into QR image
      setQR(`otpauth://totp/${i18n('app.title')}?secret=${secretCode.data}&issuer=${currentUser?.email}`);
    }
  };

  useEffect(() => {
    if (mfaEnabled && !mfaRegistered && !qr) {
      // If somehow they get through login without registering their device
      fetchQRCode();
    }
  }, [mfaEnabled, mfaRegistered, qr]);

  useEffect(() => {
    if (!mfaEnabled && qr) {
      // If they disable mfa, clear state
      setQR(undefined);
      setCode('');
    }
  }, [mfaEnabled, qr]);

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const { value } = e.target;
    setCode(value);
  };

  const handleOk = () => {
    dispatch(actions.doRegisterMfa(code) as any);
  };

  const handleCancel = () => {
    const isAuth = history.location.pathname.includes('/auth');
    dispatch({
      type: actions.SET_MFA,
      payload: {
        mfaEnabled: false,
        mfaRegistered: isAuth ? false : mfaRegistered,
      },
    });
    Analytics.track({
      event: Analytics.events.authCancelMfaRegistration,
      properties: {
        user: {
          currentUser,
        },
      },
    });
    if (history.location.pathname.includes('/auth')) {
      // user canceled during login
      dispatch(actions.doSignout(currentUser?.email) as any);
    }
  };

  return (
    <Modal //
      title={i18n('mfa.register.title')}
      visible={mfaEnabled && !mfaRegistered}
      width={600}
      okText={i18n('common.verify')}
      okButtonProps={{ disabled: !code }}
      onOk={handleOk}
      confirmLoading={loadingMfaRegistration}
      onCancel={handleCancel}
    >
      <p>{i18n('mfa.register.message')}</p>
      <section className="mfa-registration">
        <div className="qr-container">{qr && <QRCode value={qr} />}</div>
        <div className="qr-instructions">
          <p>{i18n('mfa.register.step1')}</p>
          <p>{i18n('mfa.register.step2')}</p>
          <p>{i18n('mfa.register.step3')}</p>
        </div>
      </section>
      <section className="mfa-contact">
        <Input placeholder={i18n('auth.verifyMfa.mfaField')} value={code} onChange={handleChange} onPressEnter={handleOk} />
        {errorMessage && <p className="error-message">{errorMessage}</p>}
        <p>{i18n('mfa.register.contact')}</p>
        <section className="lost-contact">
          <ContactInfo phone="888.984.3199 ext 2" email="support" />
        </section>
      </section>
    </Modal>
  );
};

export default MFAModal;
