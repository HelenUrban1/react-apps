import React from 'react';
import { Spin } from 'antd';

const Spinner = (props: any) => {
  const { style } = props;
  return (
    <div
      key="spinner-key"
      style={{
        width: '100%',
        height: '100%',
        margin: '24px',
        display: 'flex',
        justifyContent: 'center',
        alignContent: 'center',
        alignItems: 'center',
        ...style,
      }}
    >
      <Spin />
    </div>
  );
};

export default Spinner;
