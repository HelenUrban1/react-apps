import React, { useEffect } from 'react';
import { Redirect, Route, useHistory } from 'react-router-dom';
import { initializeSessionThunk } from 'modules/session/sessionActions';
import { useDispatch, useSelector } from 'react-redux';
import PermissionChecker from 'modules/auth/permissionChecker';
import Permissions from 'security/permissions';
import Layout from 'view/layout/Layout';
import { doSetSubscription, doInitBilling } from 'modules/subscription/subscriptionActions';
import { doSetAccount } from 'modules/account/accountActions';
import { Spin } from 'antd';

function PrivateRoute({ component: Component, currentUser, permissionRequired, ...rest }) {
  const dispatch = useDispatch();
  const { id: account } = useSelector((state) => state.account);
  const { data: subscription, auth, loading, error, state, addons } = useSelector((state) => state.subscription);
  const permissionChecker = new PermissionChecker(currentUser);
  const hasBillingAccess = permissionChecker.match(Permissions.values.accountBillingAccess);
  const history = useHistory();
  // TODO: Adjust feature flag when ready for staging
  useEffect(() => {

    if (currentUser===null) {
      const ideagenHomeUrl = process.env.REACT_APP_IDEAGEN_HOME_URL;
      const ideagenHomeReRouteActive = process.env.REACT_APP_IDEAGEN_HOME_REDIRECT_ACTIVE;

      // Check whether history has only two elements, proving user is performing a first time 
      // navigation to the application this session
      const firstNavigation = history.length === 2;
      
      if (firstNavigation && ideagenHomeReRouteActive) {
        window.location.replace(ideagenHomeUrl);
      }
    }
    if (!loading && !auth && hasBillingAccess && currentUser && currentUser.accountId) {
      dispatch(doInitBilling(currentUser.accountId, currentUser));
    }
    if (!loading && !error && (auth || !hasBillingAccess) && !subscription && currentUser && currentUser.accountId) {
      // If BillingInit didn't find a subscription during sync, read the local DB copy
      dispatch(doSetSubscription(currentUser.accountId));
    }
    if (!loading && currentUser && currentUser.accountId && !account) {
      // sets account in redux
      dispatch(doSetAccount(currentUser.accountId));
      dispatch(initializeSessionThunk(currentUser));
    }
  }, [loading, currentUser?.accountId, auth, account, error, subscription]);
  return (
    <Route
      {...rest}
      render={(props) => {
        if (!permissionChecker.isAuthenticated) {
          return (
            <Redirect
              to={{
                pathname: '/auth/signin',
                state: { from: props.location },
              }}
            />
          );
        }

        if (!permissionChecker.isEmailVerified || permissionChecker.isOrphan) {
          return <Redirect to="/auth/email-unverified" />;
        }

        if (permissionChecker.isOrphan) {
          const createAccountUrl = `/auth/verify-email?=email${encodeURIComponent(currentUser.email)}&form=new`;
          return <Redirect to={createAccountUrl} />;
        }

        if (permissionChecker.isEmptyPermissions) {
          return <Redirect to="/auth/empty-permissions" />;
        }
        if (!permissionChecker.match(permissionRequired)) {
          return <Redirect to="/403" />;
        }

        if (permissionRequired?.id === 'sphinxApiKeysAccess') {
          // Net Inspect Data Connections
          if (!state) {
            return (
              <div className="loader" data-cy="loader">
                <Spin className="loader-indicator" size="large" delay={500} />
              </div>
            );
          }
          if (state !== 'trial' && !addons.net_inspect) {
            return <Redirect to="/403" />;
          }
        }

        return (
          <Layout currentUser={currentUser} isBilling={hasBillingAccess}>
            <Component restricted={permissionChecker.isRestrictedAccess} {...props} />
          </Layout>
        );
      }}
    />
  );
}

export default PrivateRoute;
