import PermissionChecker from 'modules/auth/permissionChecker';
import React from 'react';
import { Redirect, Route } from 'react-router-dom';

function PublicRoute({ component: Component, currentUser, ...rest }) {
  return (
    <Route
      {...rest}
      render={(props) => {
        const permissionChecker = new PermissionChecker(currentUser);

        if (permissionChecker.isAuthenticated) {
          return <Redirect to="/" />;
        }

        if (permissionChecker.isCanceledInvitation) {
          return <Redirect to="/auth/invite-canceled" />;
        }

        if (permissionChecker.isExpiredToken) {
          return <Redirect to="/auth/invite-expired" />;
        }

        return <Component {...props} />;
      }}
    />
  );
}

export default PublicRoute;
