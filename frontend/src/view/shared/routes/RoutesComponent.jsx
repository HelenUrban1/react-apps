import authSelectors from 'modules/auth/authSelectors';
import layoutSelectors from 'modules/layout/layoutSelectors';
import React from 'react';
import { connect } from 'react-redux';
import { Route, Switch, withRouter, useHistory } from 'react-router-dom';
import loadable from '@loadable/component';

import EmailUnverifiedRoute from 'view/shared/routes/EmailUnverifiedRoute';
import EmptyPermissionsRoute from 'view/shared/routes/EmptyPermissionsRoute';
import PrivateRoute from 'view/shared/routes/PrivateRoute';
import PublicRoute from 'view/shared/routes/PublicRoute';
import ProgressBar from 'view/shared/ProgressBar';
import routes from 'view/routes';
import Analytics from 'modules/shared/analytics/analytics';
import InviteExpiredRoutes from './InviteExpiredRoutes';

function RoutesComponent({ loading, currentUser }) {

  const history = useHistory();

  const trackPageLoad = () => {
    // If a user selected "Remember Me" they could enter the site at any URL. Call identify to ensure they are tracked.
    Analytics.identify({
      user: currentUser,
    });

    // Check for hard refresh of the page using the Timing API
    if (window.performance) {
      // Navigation type can be one of
      // enum NavigationType {
      //     "navigate",
      //     "reload",
      //     "back_forward",
      //     "prerender"
      // };
      const navType = window.performance.getEntriesByType('navigation')[0].type;
      if (navType === 'reload') {
        Analytics.track({
          event: Analytics.events.pageBrowserReloaded,
          properties: {
            href: window.location.href,
            hostname: window.location.hostname,
            pathname: window.location.pathname,
          },
        });
      }
    }
  };

  const trackPageView = (location, action) => {
    if (action === 'PUSH') {
      Analytics.page({ url: window.location.pathname });
    }
  };

  React.useEffect(() => {

    // Handle first load & refreshes
    trackPageLoad();

    // Handle navigation events
    history.listen((location, action) => {
      trackPageView(location, action);
    });
  }, [history]);

  if (loading) {
    ProgressBar.start();
    return <div />;
  }
  if (!loading) {
    ProgressBar.done();
  }

  return (
    <Switch>
      {routes.publicRoutes.map((route) => (
        <PublicRoute key={route.path} exact path={route.path} currentUser={currentUser} component={loadable(route.loader)} />
      ))}

      {routes.emptyPermissionsRoutes.map((route) => (
        <EmptyPermissionsRoute key={route.path} exact path={route.path} currentUser={currentUser} component={loadable(route.loader)} />
      ))}

      {routes.emailUnverifiedRoutes.map((route) => (
        <EmailUnverifiedRoute key={route.path} exact path={route.path} currentUser={currentUser} component={loadable(route.loader)} />
      ))}

      {routes.inviteExpiredRoutes.map((route) => (
        <InviteExpiredRoutes key={route.path} exact path={route.path} currentUser={currentUser} component={loadable(route.loader)} />
      ))}

      {routes.privateRoutes.map((route) => (
        <PrivateRoute key={route.path} currentUser={currentUser} permissionRequired={route.permissionRequired} path={route.path} component={loadable(route.loader)} exact={!!route.exact} />
      ))}

      {routes.simpleRoutes.map((route) => (
        <Route key={route.path} exact path={route.path} component={loadable(route.loader)} />
      ))}
    </Switch>
  );
}

const select = (state) => ({
  loading: authSelectors.selectLoadingInit(state) || layoutSelectors.selectLoading(state),
  authError: authSelectors.selectErrorMessage(state),
  currentUser: authSelectors.selectCurrentUser(state),
});

export default withRouter(connect(select)(RoutesComponent));
