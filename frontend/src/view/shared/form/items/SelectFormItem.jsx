import React from 'react';
import { Form, Select } from 'antd';
import { formItemLayout } from 'view/shared/styles/FormWrapper';
import PropTypes from 'prop-types';
import FormErrors from 'view/shared/form/formErrors';
import { FastField } from 'formik';

const SelectFormItemNotFast = ({ label, name, form, hint, layout, size, placeholder, options, mode, autoFocus, autoComplete, prefix, formItemProps, inputProps, errorMessage, required, disabled }) => {
  const blur = () => {
    form.setFieldTouched(name);
  };

  const change = (value) => {
    form.setFieldValue(name, value);
  };

  return (
    <Form.Item {...layout} label={label} validateStatus={FormErrors.validateStatus(form, name, errorMessage)} required={required} help={FormErrors.displayableError(form, name, errorMessage) || hint} {...formItemProps}>
      <Select
        id={name}
        onChange={change}
        onBlur={blur}
        value={form.values[name]}
        size={size}
        placeholder={placeholder}
        autoFocus={autoFocus}
        autoComplete={autoComplete}
        prefix={prefix}
        mode={mode}
        filterOption={(input, option) => option.title.toLocaleLowerCase().includes(input.toLocaleLowerCase())}
        showSearch
        allowClear
        disabled={disabled}
        {...inputProps} //
      >
        {options.map((option) => (
          <Select.Option key={option.value} value={option.value} title={option.label} label={option.label}>
            {option.label}
          </Select.Option>
        ))}
      </Select>
    </Form.Item>
  );
};

SelectFormItemNotFast.defaultProps = {
  layout: formItemLayout,
  required: false,
  type: undefined,
  label: undefined,
  hint: undefined,
  autoFocus: false,
  size: undefined,
  prefix: undefined,
  placeholder: undefined,
  errorMessage: undefined,
  formItemProps: {},
  inputProps: {},
  mode: undefined,
};

SelectFormItemNotFast.propTypes = {
  form: PropTypes.object.isRequired,
  name: PropTypes.string.isRequired,
  options: PropTypes.array.isRequired,
  type: PropTypes.string,
  label: PropTypes.string,
  hint: PropTypes.string,
  autoFocus: PropTypes.bool,
  required: PropTypes.bool,
  size: PropTypes.string,
  prefix: PropTypes.string,
  placeholder: PropTypes.string,
  layout: PropTypes.object,
  errorMessage: PropTypes.string,
  formItemProps: PropTypes.object,
  inputProps: PropTypes.object,
  mode: PropTypes.string,
};

const SelectFormItem = (props) => <FastField name={props.name}>{({ form }) => <SelectFormItemNotFast {...props} form={form} />}</FastField>;

export default SelectFormItem;
