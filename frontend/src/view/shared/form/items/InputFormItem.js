import React, { Component } from 'react';
import { Form, Popover, Input } from 'antd';
import { PasswordRequirements } from 'view/auth/PasswordRequirements';
import { formItemLayout } from 'view/shared/styles/FormWrapper';
import { i18n } from 'i18n';
import PropTypes from 'prop-types';
import FormErrors from 'view/shared/form/formErrors';
import { FastField } from 'formik';

export class InputFormItemNotFast extends Component {
  render() {
    const { label, name, form, hint, layout, size, type, placeholder, autoFocus, autoComplete, prefix, formItemProps, inputProps, errorMessage, required, disabled } = this.props;

    return (
      <Form.Item {...layout} label={label} required={required} validateStatus={FormErrors.validateStatus(form, name, errorMessage)} help={FormErrors.displayableError(form, name, errorMessage) || hint} {...formItemProps}>
        {name === 'password' && 'confirmPassword' in form.values ? (
          <Popover placement="leftTop" title={i18n('password.requirements.title')} trigger="focus" content={<PasswordRequirements password={form.values.password || ''} />}>
            <Input
              id={name}
              data-testid={name}
              type={type}
              onChange={form.handleChange}
              onBlur={form.handleBlur}
              value={form.values[name]}
              size={size || undefined}
              placeholder={placeholder || undefined}
              autoFocus={autoFocus || false}
              autoComplete={autoComplete || undefined}
              prefix={prefix || undefined}
              {...inputProps}
              disabled={disabled}
            />
          </Popover>
        ) : (
          <Input
            id={name}
            data-testid={name}
            type={type}
            onChange={form.handleChange}
            onBlur={form.handleBlur}
            value={form.values[name]}
            size={size || undefined}
            placeholder={placeholder || undefined}
            autoFocus={autoFocus || false}
            autoComplete={autoComplete || undefined}
            prefix={prefix || undefined}
            {...inputProps}
            disabled={disabled}
          />
        )}
      </Form.Item>
    );
  }
}

InputFormItemNotFast.defaultProps = {
  layout: formItemLayout,
  type: 'text',
  required: false,
  disabled: false,
};

InputFormItemNotFast.propTypes = {
  form: PropTypes.object.isRequired,
  name: PropTypes.string.isRequired,
  required: PropTypes.bool,
  type: PropTypes.string,
  label: PropTypes.string,
  hint: PropTypes.string,
  autoFocus: PropTypes.bool,
  size: PropTypes.string,
  prefix: PropTypes.string,
  placeholder: PropTypes.string,
  layout: PropTypes.object,
  errorMessage: PropTypes.string,
  formItemProps: PropTypes.object,
  inputProps: PropTypes.object,
  disabled: PropTypes.bool,
};

class InputFormItem extends Component {
  render() {
    return <FastField name={this.props.name}>{({ form }) => <InputFormItemNotFast {...this.props} form={form} />}</FastField>;
  }
}

export default InputFormItem;
