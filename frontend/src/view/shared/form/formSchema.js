import * as yup from 'yup';
import { i18n } from 'i18n';

const passwordValidation = yup
  .string()
  .required(i18n('user.fields.required', 'Password'))
  .notOneOf([yup.ref('email'), null], i18n('password.requirements.badMatch'))
  .test({
    message: i18n('password.requirements.weak'),
    test: (value) => {
      const hasLength = value && value.length >= 10;
      const hasUpperCase = /[A-Z]/.test(value);
      const hasLowerCase = /[a-z]/.test(value);
      const hasSpecialChar = /[!@#$%^&*()]/.test(value);
      const hasNumber = /[0-9]/.test(value);
      const isEmail = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(value);
      return hasLength && hasUpperCase && hasLowerCase && hasSpecialChar && hasNumber && !isEmail;
    },
  });

const confirmPasswordValidation = yup
  .string()
  .required(i18n('user.fields.required', 'Confirm Password'))
  .oneOf([yup.ref('password'), null], i18n('password.requirements.match'));

const nameFieldValidation = yup
  .string()
  .required(i18n('user.fields.required', 'Name'))
  .test({
    message: i18n('user.fieldsError.name', 'Name'),
    test: (value) => {
      const pass = /^[A-Za-z!”&*%$£()-]+$/.test(value);
      return pass;
    },
  });

export default class FormSchema {
  constructor(idField, fields) {
    this.idField = idField;
    this.fields = fields;
    this.schema = this.buildSchema();
  }

  initialValues(record = {}) {
    const intialValues = {};

    if (this.idField) {
      intialValues[this.idField.name] = record[this.idField.name];
    }

    this.fields.forEach((field) => {
      intialValues[field.name] = field.forFormInitialValue(record[field.name]);
    });

    return intialValues;
  }

  buildSchema() {
    const shape = {};

    this.fields.forEach((field) => {
      shape[field.name] = field.forForm();

      switch (field.name) {
        case 'confirmPassword':
          shape[field.name] = confirmPasswordValidation;
          shape.password = passwordValidation;
          break;
        case 'firstName':
          shape[field.name] = nameFieldValidation;
          break;
        case 'lastName':
          shape[field.name] = nameFieldValidation;
          break;
        default:
          break;
      }
    });

    return yup.object().shape(shape);
  }

  cast(values) {
    return this.schema.cast(values);
  }
}
