import { CloseOutlined, SaveOutlined, UndoOutlined } from '@ant-design/icons';
import { Button, Form } from 'antd';
import { Formik } from 'formik';
import actions from 'modules/iam/form/iamFormActions';
import selectors from 'modules/iam/form/iamFormSelectors';
import model from 'modules/auth/userModel';
import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import ImagesFormItem from 'view/shared/form/items/ImagesFormItem';
import InputFormItem from 'view/shared/form/items/InputFormItem';
import ViewFormItem from 'view/shared/form/items/ViewFormItem';
import CustomViewItem from 'view/shared/view/CustomViewItem';
import Spinner from 'view/shared/Spinner';
import FormWrapper, { tailFormItemLayout } from 'view/shared/styles/FormWrapper';
import FormSchema from 'view/shared/form/formSchema';
import { i18n } from '../../../i18n';

const { fields } = model;

class IamEditForm extends Component {
  schema = new FormSchema(fields.id, [fields.email, fields.firstName, fields.lastName, fields.phoneNumber, fields.avatarsIam]);

  componentDidMount() {
    const { dispatch, match } = this.props;
    dispatch(actions.doFind(match.params.id));
  }

  handleSubmit = (values) => {
    const { dispatch, user } = this.props;
    const data = {
      id: user.id,
      ...values,
    };
    delete data.email;
    dispatch(actions.doUpdate(data));
  };

  initialValues = () => {
    const { user } = this.props;
    return this.schema.initialValues(user);
  };

  renderForm() {
    const { user, saveLoading } = this.props;

    return (
      <FormWrapper>
        <Formik initialValues={this.initialValues()} validationSchema={this.schema.schema} onSubmit={this.handleSubmit}>
          {(form) => {
            return (
              <Form layout="vertical" onFinish={form.handleSubmit}>
                <ViewFormItem name={fields.id.name} label={fields.id.label} />

                <ViewFormItem name={fields.email.name} label={fields.email.label} />

                <InputFormItem name={fields.firstName.name} label={fields.firstName.label} autoFocus />

                <InputFormItem name={fields.lastName.name} label={fields.lastName.label} autoComplete={fields.lastName.name} />

                <InputFormItem name={fields.phoneNumber.name} label={fields.phoneNumber.label} autoComplete={fields.phoneNumber.name} prefix="+" />

                <ImagesFormItem
                  name={fields.avatarsIam.name}
                  label={fields.avatarsIam.label}
                  path={fields.avatarsIam.path}
                  width={fields.avatarsIam.width}
                  height={fields.avatarsIam.height}
                  schema={{
                    size: fields.avatarsIam.size,
                  }}
                  max={fields.avatarsIam.max}
                />

                <Form.Item className="form-buttons" {...tailFormItemLayout}>
                  <Button loading={saveLoading} type="primary" onClick={form.handleSubmit} icon={<SaveOutlined />}>
                    {i18n('common.save')}
                  </Button>

                  <Button disabled={saveLoading} onClick={form.handleReset} icon={<UndoOutlined />}>
                    {i18n('common.reset')}
                  </Button>

                  {this.props.onCancel ? (
                    <Button disabled={saveLoading} onClick={() => this.props.onCancel()} icon={<CloseOutlined />}>
                      {i18n('common.cancel')}
                    </Button>
                  ) : null}
                </Form.Item>
              </Form>
            );
          }}
        </Formik>
      </FormWrapper>
    );
  }

  render() {
    const { findLoading, user } = this.props;

    if (findLoading) {
      return <Spinner />;
    }

    if (!user) {
      return null;
    }

    return this.renderForm();
  }
}

function select(state) {
  return {
    findLoading: selectors.selectFindLoading(state),
    saveLoading: selectors.selectSaveLoading(state),
    user: selectors.selectUser(state),
  };
}

export default connect(select)(IamEditForm);
