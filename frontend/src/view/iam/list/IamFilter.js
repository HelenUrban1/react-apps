import iamListModeSelectors from 'modules/iam/list/mode/iamListModeSelectors';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import IamUsersFilter from 'view/iam/list/users/IamUsersFilter';

class IamFilter extends Component {
  render() {
    return <IamUsersFilter />;
  }
}

function select(state) {
  return {
    mode: iamListModeSelectors.selectMode(state),
  };
}

export default connect(select)(IamFilter);
