import Permissions from 'security/permissions';
import { i18n } from 'i18n';
import { isBeforeQA } from 'config';

const permissions = Permissions.values;

const privateRoutes = [
  {
    path: '/',
    icon: 'home',
    label: i18n('home.menu'),
    menu: {
      exact: true,
    },
    // loader: () => import('view/home/Home'), // TODO: Make part/list homepage for v1
    // permissionRequired: null,
    loader: () => import('domain/part/list'),
    permissionRequired: permissions.partRead,
    exact: true,
  },
  {
    path: '/parts',
    loader: () => import('domain/part/list'),
    permissionRequired: permissions.partRead,
    exact: true,
    label: i18n('entities.part.menu'),
    menu: true,
  },
  {
    path: '/parts/:id',
    loader: () => import('domain/part/edit/index'),
    menu: false,
    permissionRequired: permissions.partEdit,
    exact: true,
  },
  {
    path: '/parts/:partId/reports/',
    loader: () => import('domain/reportTemplates/index'),
    permissionRequired: permissions.reportEdit,
    exact: true,
    label: i18n('entities.report.label'),
    menu: true,
  },
  {
    path: '/parts/:partId/reports/:reportId',
    loader: () => import('domain/reportTemplates/index'),
    permissionRequired: permissions.reportEdit,
    exact: true,
    label: i18n('entities.report.label'),
    menu: true,
  },
  {
    path: '/templates/:templateId',
    loader: () => import('domain/reportTemplates/index'),
    permissionRequired: permissions.reportTemplateEdit,
    exact: true,
    label: i18n('entities.template.label'),
    menu: true,
  },
  {
    path: '/templates',
    loader: () => import('domain/reportTemplates/index'),
    permissionRequired: permissions.reportTemplateView,
    exact: true,
    label: i18n('entities.template.label'),
    menu: true,
  },
  {
    path: '/settings/profile',
    loader: () => import('domain/setting'),
    permissionRequired: null,
    exact: true,
    label: i18n('settings.nav.myProfile'),
    menu: true,
  },
  {
    path: '/settings/preferences',
    loader: () => import('domain/setting'),
    permissionRequired: null,
    exact: true,
    label: i18n('settings.nav.preferences'),
    menu: true,
  },
  {
    path: '/settings/security',
    loader: () => import('domain/setting'),
    permissionRequired: null,
    exact: true,
    label: i18n('settings.nav.security'),
    menu: true,
  },
  {
    path: '/settings/notifications',
    loader: () => import('domain/setting'),
    permissionRequired: null,
    exact: true,
    label: i18n('settings.nav.notifications'),
    menu: true,
  },
  {
    path: '/settings/account',
    loader: () => import('domain/setting'),
    permissionRequired: permissions.accountRead,
    exact: true,
    label: i18n('settings.nav.companyProfile'),
    menu: true,
  },
  {
    path: '/settings/seats',
    loader: () => import('domain/setting'),
    permissionRequired: permissions.accountManagement,
    exact: true,
    label: i18n('settings.nav.seats'),
    menu: true,
  },
  {
    path: '/settings/connection',
    loader: () => import('domain/setting'),
    permissionRequired: permissions.sphinxApiKeysAccess,
    exact: true,
    label: i18n('settings.nav.connection'),
    menu: true,
  },
  {
    path: '/settings/connection/*',
    loader: () => import('domain/setting'),
    permissionRequired: permissions.sphinxApiKeysAccess,
    exact: false,
    label: i18n('settings.nav.connection'),
    menu: true,
  },
  {
    path: '/settings/billing',
    loader: () => import('domain/setting'),
    permissionRequired: permissions.accountBillingAccess,
    exact: true,
    label: i18n('settings.nav.billing'),
    menu: true,
  },
  {
    path: '/settings/configs',
    loader: () => import('domain/setting'),
    permissionRequired: permissions.accountManagement,
    exact: true,
    label: i18n('settings.nav.data'),
    menu: true,
  },
  {
    path: '/review',
    loader: () => import('domain/humanReview'),
    permissionRequired: permissions.reviewer,
    exact: true,
  },
  {
    path: '/temporary-data-export-tester',
    loader: () => import('temporaryDataExportTester/index'),
    permissionRequired: permissions.sphinxApiKeysAccess,
    exact: true,
    label: 'Data Export Tester',
    menu: false,
  },
  // UNUSED ROUTES
  // {
  //   path: '/iam',
  //   loader: () => import('view/iam/list/IamPage'),
  //   permissionRequired: permissions.iamRead,
  //   exact: true,
  //   icon: 'user-add',
  //   label: i18n('iam.menu'),
  //   menu: true,
  // },
  // {
  //   path: '/iam/new',
  //   loader: () => import('view/iam/new/IamNewPage'),
  //   menu: false,
  //   permissionRequired: permissions.iamCreate,
  //   exact: true,
  // },
  // {
  //   path: '/iam/importer',
  //   loader: () => import('view/iam/importer/IamImporterPage'),
  //   menu: false,
  //   permissionRequired: permissions.iamImport,
  //   exact: true,
  // },
  // {
  //   path: '/iam/:id/edit',
  //   loader: () => import('view/iam/edit/IamEditPage'),
  //   menu: false,
  //   permissionRequired: permissions.iamEdit,
  //   exact: true,
  // },
  // {
  //   path: '/iam/:id',
  //   loader: () => import('view/iam/view/IamViewPage'),
  //   menu: false,
  //   permissionRequired: permissions.iamRead,
  //   exact: true,
  // },

  // {
  //   path: '/audit-logs',
  //   icon: 'file-search',
  //   label: i18n('auditLog.menu'),
  //   loader: () => import('view/auditLog/AuditLogPage'),
  //   menu: true,
  //   permissionRequired: permissions.auditLogRead,
  // },

  // {
  //   path: '/account-member',
  //   loader: () => import('view/accountMember/list/AccountMemberListPage'),
  //   permissionRequired: permissions.accountMemberRead,
  //   exact: true,
  //   icon: 'right',
  //   label: i18n('entities.accountMember.menu'),
  //   menu: true,
  // },
  // {
  //   path: '/account-member/new',
  //   loader: () => import('view/accountMember/form/AccountMemberFormPage'),
  //   menu: false,
  //   permissionRequired: permissions.accountMemberCreate,
  //   exact: true,
  // },
  // {
  //   path: '/account-member/importer',
  //   loader: () => import('view/accountMember/importer/AccountMemberImporterPage'),
  //   menu: false,
  //   permissionRequired: permissions.accountMemberImport,
  //   exact: true,
  // },
  // {
  //   path: '/account-member/:id/edit',
  //   loader: () => import('view/accountMember/form/AccountMemberFormPage'),
  //   menu: false,
  //   permissionRequired: permissions.accountMemberEdit,
  //   exact: true,
  // },
  // {
  //   path: '/account-member/:id',
  //   loader: () => import('view/accountMember/view/AccountMemberViewPage'),
  //   menu: false,
  //   permissionRequired: permissions.accountMemberRead,
  //   exact: true,
  // },

  // {
  //   path: '/connection',
  //   loader: () => import('view/connection/view/ConnectionViewPage'),
  //   permissionRequired: permissions.sphinxApiKeysAccess,
  //   exact: true,
  // },
  // {
  //   path: '/connection/new',
  //   loader: () => import('view/connection/new/ConnectionNewPage'),
  //   permissionRequired: permissions.sphinxApiKeysAccess,
  //   exact: true,
  // },
  // {
  //   path: '/connection/:id/edit',
  //   loader: () => import('view/connection/edit/ConnectionEditPage'),
  //   permissionRequired: permissions.sphinxApiKeysAccess,
  //   exact: true,
  // },
];

// Move Jobs/Metro routes behind feature flag
const privateRoutesBeforeQA = [
  ...privateRoutes,
  {
    path: '/jobs',
    loader: () => import('domain/jobs/list'),
    permissionRequired: permissions.jobsRead,
    exact: true,
    menu: true,
    label: i18n('entities.job.menu'),
  },
  {
    path: '/jobs/:id',
    loader: () => import('domain/jobs/list'),
    permissionRequired: permissions.jobsRead,
    exact: true,
    menu: false,
  },
  {
    path: '/settings/operators',
    loader: () => import('domain/setting'),
    permissionRequired: null,
    exact: true,
    label: i18n('settings.nav.operators'),
    menu: true,
  },
  // TEMPORARY
  {
    path: '/metro',
    loader: () => import('metro/index'),
    permissionRequired: permissions.metro && isBeforeQA,
    menu: false,
    exact: false,
    hasHeader: false,
    hasContent: false,
    hasFooter: false,
    hasDrawer: false,
  },
  {
    path: '/metro/installed',
    loader: () => import('metro/index'),
    permissionRequired: permissions.metro && isBeforeQA,
    menu: false,
    exact: false,
    hasHeader: false,
    hasContent: false,
    hasFooter: false,
    hasDrawer: false,
  },
];

const publicRoutes = [
  {
    path: '/auth/signin',
    loader: () => import('view/auth/SigninPage'),
  },
  {
    path: '/auth/signup',
    loader: () => import('view/auth/SignupPage'),
  },
  {
    path: '/auth/forgot-password',
    loader: () => import('view/auth/ForgotPasswordPage'),
  },
  {
    path: '/auth/mfa',
    loader: () => import('view/auth/VerifyMfaPage'),
  },
];

const emptyPermissionsRoutes = [
  {
    path: '/auth/empty-permissions',
    loader: () => import('view/auth/EmptyPermissionsPage'),
  },
];

const emailUnverifiedRoutes = [
  // {
  //   path: '/auth/email-unverified',
  //   loader: () => import('view/auth/EmailUnverifiedPage'),
  // },
];

const inviteExpiredRoutes = [
  {
    path: '/auth/invite-expired',
    loader: () => import('view/auth/InviteExpiredPage'),
  },
  {
    path: '/auth/invite-canceled',
    loader: () => import('view/auth/InviteCanceledPage'),
  },
];

const simpleRoutes = [
  {
    path: '/auth/password-reset',
    loader: () => import('view/auth/PasswordResetPage'),
  },
  {
    path: '/auth/verify-email',
    loader: () => import('view/auth/VerifyEmailPage'),
  },
  {
    path: '/auth/request-access',
    loader: () => import('view/auth/RequestAccessPage'),
  },
  {
    path: '/auth/request-accepted',
    loader: () => import('view/auth/RequestAcceptedPage'),
  },
  {
    path: '/auth/request-sent',
    loader: () => import('view/auth/RequestSentPage'),
  },
  {
    path: '/auth/email-unverified',
    loader: () => import('view/auth/EmailUnverifiedPage'),
  },
  {
    path: '/auth/request-response',
    loader: () => import('view/auth/RequestResponsePage'),
  },
  {
    path: '/403',
    loader: () => import('view/shared/errors/Error403Page'),
  },
  {
    path: '/500',
    loader: () => import('view/shared/errors/Error500Page'),
  },
  {
    path: '/changing-roles',
    loader: () => import('view/shared/errors/ChangeRolePage'),
  },
  {
    path: '**',
    loader: () => import('view/shared/errors/Error404Page'),
  },
];

export default {
  privateRoutes: isBeforeQA ? privateRoutesBeforeQA : privateRoutes,
  publicRoutes,
  emptyPermissionsRoutes,
  emailUnverifiedRoutes,
  inviteExpiredRoutes,
  simpleRoutes,
};
