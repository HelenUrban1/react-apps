import { getUserSubMenuItems, getSettingsSubMenuItems } from './UserSubMenuItems';
import PermissionChecker from 'modules/auth/permissionChecker';

class MockPermissionChecker extends PermissionChecker {
  match() {
    return () => true;
  }
}

describe('UserSubMenuItems', () => {
  let currentUser;

  beforeEach(() => {
    currentUser = {};
  });

  describe('#getUserSubMenuItems()', () => {
    it('does include the profile navigation', () => {
      const items = getUserSubMenuItems(currentUser);
      expect(items.some((item) => item.route.includes('/settings/profile'))).toBe(true);
    });
  });

  describe('#getSettingsSubMenuItems()', () => {
    it('does include account, templates and billing for owners', () => {
      const items = getSettingsSubMenuItems(currentUser, false, {}, MockPermissionChecker);
      expect(items.some((item) => item.route.includes('/settings/account'))).toBe(true);
      expect(items.some((item) => item.route.includes('/settings/billing'))).toBe(true);
    });

    it('does include data connections, if purchased', () => {
      const items = getSettingsSubMenuItems(currentUser, false, { net_inspect: 1 }, MockPermissionChecker);
      expect(items.some((item) => item.route.includes('/settings/connection'))).toBe(true);
    });
  });
});
