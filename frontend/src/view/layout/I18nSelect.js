import React from 'react';
import actions from 'modules/layout/layoutActions';
import { Select } from 'antd';
import { getLanguages, getLanguageCode } from 'i18n';

const I18nSelect = () => {
  const doChangeLanguage = (language) => {
    actions.doChangeLanguage(language);
  };

  const currentLanguage = getLanguageCode();
  return (
    <Select value={currentLanguage} style={{ width: 100 }} onChange={doChangeLanguage} data-cy="i18n-select" className="i18n-select">
      {getLanguages().map((language) => (
        <Select.Option key={language.id} value={language.id}>
          {language.label}
        </Select.Option>
      ))}
    </Select>
  );
};

export default I18nSelect;
