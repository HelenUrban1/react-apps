import React from 'react';
import { i18n } from 'i18n';
import Icon, {
  InfoCircleOutlined,
  BgColorsOutlined,
  TableOutlined,
  CheckCircleOutlined,
  ReconciliationOutlined,
  CalculatorOutlined,
  ScheduleOutlined,
  QuestionOutlined,
  BuildOutlined,
  UnorderedListOutlined,
  BellOutlined,
  FileTextOutlined,
} from '@ant-design/icons';
import { PlusMinSVG, BalloonSVG } from 'styleguide/shared/svg';
import PermissionChecker from 'modules/auth/permissionChecker';
import Permissions from 'security/permissions';
import { isBeforeQA, isBeforeProduction } from 'config';

export const partsMenu = (autoballooning: boolean) => {
  return {
    name: i18n('entities.part.menu'),
    custom: false,
    icon: <BuildOutlined />,
    subitems: [
      {
        name: i18n('settings.nav.myParts'),
        icon: <BuildOutlined />,
        route: '/parts',
      },
      {
        name: i18n('settings.nav.templates'),
        icon: <UnorderedListOutlined />,
        route: '/templates',
      },
    ],
    route: '/parts',
    bottom: false,
    steps: {
      flow: i18n('part-creation'),
      route: '/parts',
      steps: autoballooning
        ? [
            { name: i18n('wizard.navigation.partInfo'), custom: false, icon: <InfoCircleOutlined /> },
            { name: i18n('wizard.navigation.docsInfo'), custom: false, icon: <FileTextOutlined /> },
            { name: i18n('wizard.navigation.tolerances'), custom: true, icon: <Icon component={PlusMinSVG} /> },
            { name: i18n('wizard.navigation.grid'), custom: false, icon: <TableOutlined /> },
            { name: i18n('wizard.navigation.styles'), custom: false, icon: <BgColorsOutlined /> },
            { name: i18n('wizard.navigation.extraction'), custom: true, icon: <Icon component={BalloonSVG} /> },
            { name: i18n('wizard.navigation.review'), custom: false, icon: <CheckCircleOutlined /> },
            { name: i18n('wizard.navigation.reporting'), custom: false, disabled: true, icon: <ReconciliationOutlined /> },
          ]
        : [
            { name: i18n('wizard.navigation.partInfo'), custom: false, icon: <InfoCircleOutlined /> },
            { name: i18n('wizard.navigation.docsInfo'), custom: false, icon: <FileTextOutlined /> },
            { name: i18n('wizard.navigation.tolerances'), custom: true, icon: <Icon component={PlusMinSVG} /> },
            { name: i18n('wizard.navigation.grid'), custom: false, icon: <TableOutlined /> },
            { name: i18n('wizard.navigation.styles'), custom: false, icon: <BgColorsOutlined /> },
            { name: i18n('wizard.navigation.extraction'), custom: true, icon: <Icon component={BalloonSVG} /> },
            { name: i18n('wizard.navigation.reporting'), custom: false, disabled: true, icon: <ReconciliationOutlined /> },
          ],
    },
  };
};

const metro = {
  name: i18n('METRO'),
  icon: <CalculatorOutlined />,
  subitems: [],
  route: '/metro',
  bottom: false,
};

const jobs = {
  name: i18n('Jobs'),
  icon: <ScheduleOutlined />,
  subitems: [],
  route: '/jobs',
  bottom: false,
};

export const help = {
  name: i18n('Help'),
  icon: <QuestionOutlined />,
  subitems: [],
  route: '/help',
  bottom: true,
};

export const notifications = {
  name: i18n('Notifications'),
  icon: <BellOutlined />,
  subitems: [],
  route: '/settings/notifications',
  bottom: false,
  popover: true,
};

export const getMenuItems = (currentUser: any, autoballooning: boolean, permissionChecker = PermissionChecker) => {
  const permissionValidator = new permissionChecker(currentUser);
  const hasPartRead = permissionValidator.match(Permissions.values.partRead);
  const hasNotificationRead = permissionValidator.match(Permissions.values.notificationRead);

  const menuItems = [];

  if (hasPartRead) {
    menuItems.push(partsMenu(autoballooning));
    if (isBeforeQA) {
      menuItems.push(jobs);
      menuItems.push(metro);
    }
  }

  menuItems.push(help);
  if (isBeforeProduction && hasNotificationRead) {
    menuItems.push(notifications);
  }
  return menuItems;
};
