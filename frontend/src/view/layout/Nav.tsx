import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useMutation, useQuery } from '@apollo/client';
import { withRouter } from 'react-router-dom';
import { TextLogoWhite, IconLogo } from 'styleguide/Logo/Logo';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { RouteComponentProps } from 'react-router-dom';
import { i18n } from 'i18n';
import Analytics from 'modules/shared/analytics/analytics';
import authActions from 'modules/auth/authActions';
import authSelectors from 'modules/auth/authSelectors';
import { ReportTemplateActions } from 'modules/reportTemplate/reportTemplateActions';
import { NavMenu } from 'styleguide';
import { getMenuItems } from 'view/layout/MenuItems';
import { getUserSubMenuItems, getSettingsSubMenuItems } from 'view/layout/UserSubMenuItems';
import { SettingOutlined, HomeOutlined } from '@ant-design/icons';
import { Button, Col, Row, Tooltip, notification } from 'antd';
import { IxAvatar } from 'styleguide/Avatar/IxAvatar';
import { AppState } from 'modules/state';
import { updateCurrentPartStep, updatePartStepThunk } from 'modules/navigation/navigationActions';
import { Parts } from 'domain/part/partApi';
import config from 'config';
import { io } from 'socket.io-client';

import { NotificationUserStatus } from 'domain/setting/notification/notificationUserStatusTypes';
import { NotificationUserStatusEdit, NotificationUserStatuses } from 'domain/setting/notification/notificationUserStatusApi';
import { Notifications, NotificationList } from 'domain/setting/notification/notificationApi';
import { doSetUnreadCount, doSetUnreadNotificationPreview } from 'modules/notification/notificationActions';
import PermissionChecker from 'modules/auth/permissionChecker';
import Permissions from 'security/permissions';

interface Item {
  key: string;

  item: {
    props: {
      className: string;
    };
  };
}

interface Props extends RouteComponentProps<{ id: string }> {
  collapsed: boolean;
}

const getHomeRoute = (currentUser: any) => {
  if (currentUser.roles.includes('Billing') && currentUser.roles.length === 1) return '/settings/billing';
  if (currentUser.roles.includes('Reviewer')) return '/review';
  return '/';
};

const PART_EDITOR_STEP_NAMES = Parts.step.order;

const NavRouter: React.FC<Props> = ({ history, collapsed }) => {
  const navigation = useSelector((state: AppState) => state.navigation);
  const dispatch = useDispatch();
  const { jwt } = useSelector((state: AppState) => state.auth);
  const user = useSelector((state: AppState) => authSelectors.selectCurrentUser(state));
  const { settings } = useSelector((state: AppState) => state.session);
  const { addons, state } = useSelector((store: AppState) => store.subscription);

  const defaultSort = 'createdAt_DESC';

  const permissionValidator = new PermissionChecker(user);
  const hasNotificationRead = permissionValidator.match(Permissions.values.notificationRead);

  const { refetch: refetchCount } = useQuery<NotificationList>(Notifications.query.list, {
    variables: {
      filter: { userStatuses: { user: user?.id, archivedAt: false, readAt: false } },
      orderBy: defaultSort,
      limit: 3,
      offset: 0,
    },
    fetchPolicy: 'cache-and-network',
    notifyOnNetworkStatusChange: true, // causes onCompleted to be run during refetch
    onCompleted: (result) => {
      dispatch(doSetUnreadCount(result?.notificationList?.count || 0));
      dispatch(doSetUnreadNotificationPreview(result?.notificationList?.rows || []));
    },
    skip: !hasNotificationRead,
  });

  const findUnreadCount = async () => {
    refetchCount({
      variables: {
        filter: { userStatuses: { user: user?.id, archivedAt: false, readAt: false } },
        orderBy: defaultSort,
        limit: 3,
        offset: 0,
      },
      fetchPolicy: 'no-cache',
    });
  };

  const [editNotificationUserStatus] = useMutation<NotificationUserStatusEdit>(NotificationUserStatuses.mutate.edit);

  const markAsRead = async (notification: { userStatuses: NotificationUserStatus[]; id: any }) => {
    const userStatus = { ...notification?.userStatuses?.[0], notification: notification.id } as NotificationUserStatus;
    if (userStatus) {
      const newUserStatus = { notification: userStatus.notification, archivedAt: userStatus.archivedAt, readAt: userStatus?.readAt || new Date(), user: userStatus.userId };
      await editNotificationUserStatus({
        variables: {
          id: userStatus?.id,
          data: newUserStatus,
        },
      });
      await findUnreadCount();
    }
  };

  const showNotification = async (data: any) => {
    // console.log('notification data', data);
    if (data.action) {
      const actionsArray = data.action.split(',');
      notification.success({
        message: data.content,
        description: data.title,
        duration: 5,
        key: data.id,
        btn: (
          <Row>
            {actionsArray.map((item: string, index: number) => {
              if (item === i18n('notifications.reviewFeatures')) {
                return (
                  <Col span={12} key={`col_${data.id}_${index}`}>
                    <Button
                      key={`${data.id}_${index}`}
                      className="btn-primary"
                      size="small"
                      onClick={async () => {
                        await markAsRead(data);
                        notification.destroy();
                        history.push(`/parts/${data.relationId}`);
                      }}
                    >
                      {item}
                    </Button>
                  </Col>
                );
              }
              if (item === i18n('notifications.reviewLater')) {
                return (
                  <Col span={12} key={`col_${data.id}_${index}`}>
                    <Button
                      key={`${data.id}_${index}`}
                      className="btn-secondary"
                      size="small"
                      onClick={async () => {
                        await markAsRead(data);
                        notification.destroy();
                      }}
                    >
                      {item}
                    </Button>
                  </Col>
                );
              }
            })}
          </Row>
        ),
      });
    }
    await findUnreadCount();
  };
  // useEffect for socket.io
  useEffect(() => {
    // console.log(settings.autoballoonEnabled, settings.autoballoonEnabled?.value === 'true', config.wsUrl);
    if (settings.autoballoonEnabled?.value === 'true' && config.wsUrl) {
      try {
        // show notification to the user when one is created
        const socket = io(config.wsUrl, { transports: ['websocket'], withCredentials: true, auth: { token: `Bearer ${jwt}` }, path: config.wsPath });
        socket.on('notification.create', showNotification);
        socket.on('notification.update', showNotification);
        socket.on('notificationUserStatus.update', showNotification);
        return () => {
          socket.off('notification.create');
          socket.off('notification.update');
          socket.off('notificationUserStatus.update');
          socket.disconnect();
        };
      } catch (e) {
        console.log('error', e);
      }
    }
    return () => {};
  }, []);
  // TODO: Swap User/Account to context?? Or leave it as redux?
  const email = useSelector((state: any) => authSelectors.selectCurrentUserEmail(state));

  const onClick = (item: Item) => {
    if (item.item.props.className && item.item.props.className.includes('ixc-step-item')) {
      // write the current step to client cache, use Query component to watch currentStep for changes
      const step: number = parseInt(item.key, 10);

      Analytics.track({
        event: Analytics.events.partSideNavClicked,
        eventNameSuffix: ` to ${step} ${PART_EDITOR_STEP_NAMES[step]}`, // MenuItems[0].steps.steps[step].name,
        properties: {
          // TODO: 'part.id': part.id,
          'part.editor.step.current': `${navigation.currentPartStep} ${PART_EDITOR_STEP_NAMES[navigation.currentPartStep]}`,
          'part.editor.step.next': `${step} ${PART_EDITOR_STEP_NAMES[step]}`,
        },
      });

      // Push new history if in the reporting workflow
      if (history.location.pathname.includes('/report')) {
        dispatch(updateCurrentPartStep(-1));
        const endPathAt = history.location.pathname.indexOf('/report');
        history.push(history.location.pathname.substring(0, endPathAt), { step });
        dispatch({ type: ReportTemplateActions.CLEAR });
        return;
      }

      if (step > -1 && step <= 6) {
        dispatch(updatePartStepThunk(step));
      }
    } else {
      if (item.key === 'login') {
        dispatch(authActions.doSignout(email));
        // dispatch({ type: subscriptionActions.CLEAR });
        return;
      }
      if (item.key === '/templates') {
        dispatch({ type: ReportTemplateActions.CLEAR });
        dispatch({ type: 'PART_SET_PART', payload: null });
      }

      if (item.key === '/help') {
        Analytics.track({
          event: Analytics.events.helpIconClicked,
          properties: {
            url: window.location.href,
          },
        });
        return;
      }
      if (item.key === '/ihome') {
        Analytics.track({
          event: Analytics.events.ideagenHomeIconClicked,
          properties: {
            url: window.location.href,
          },
        });
        return;
      }
      if (item.key === '/settings/notifications') {
        return;
      }
      history.push({ pathname: item.key });
    }
  };

  const currentUser = useSelector((state: any) => authSelectors.selectCurrentUser(state));
  const userSubMenuItems = getUserSubMenuItems(currentUser || {});
  const settingsSubMenuItems = getSettingsSubMenuItems(currentUser, state === 'trial', addons);

  const menuItems: any[] = [...getMenuItems(currentUser, settings.autoballoonEnabled?.value === 'true')];

  if (settingsSubMenuItems.length > 0) {
    menuItems.push({
      name: i18n('settings.menu'),
      custom: false,
      icon: <SettingOutlined />,
      subitems: settingsSubMenuItems,
      route: '',
      bottom: false,
    });
  }

  menuItems.push({
    name: i18n('common.user'),
    custom: false,
    icon: (
      <span role="img" aria-label="user" className="anticon fake-icon">
        <IxAvatar type="user" data={currentUser} size="small" />
      </span>
    ),
    subitems: userSubMenuItems,
    route: '',
    bottom: false,
  });

  menuItems.push({
    name: i18n('app.ideagenhome'),
    custom: false,
    icon: <HomeOutlined />,
    subitems: [],
    route: '/ihome',
    bottom: false,
  });

  if (navigation.currentPartStep !== null) {
    return (
      <>
        <Tooltip placement="right" title={i18n('common.home')}>
          <div
            key="/"
            onClick={() => {
              history.push(getHomeRoute(currentUser));
            }}
            className="logo"
            data-testid="logo"
          >
            {collapsed ? <IconLogo /> : <TextLogoWhite />}
          </div>
        </Tooltip>
        <NavMenu onClick={onClick} menuItems={menuItems} currentStep={navigation.currentPartStep.toString()} currentPath={history.location.pathname} maxStep={navigation.maxPartStep} />
      </>
    );
  }

  return (
    <>
      <Tooltip placement="right" title={i18n('common.home')}>
        <div
          key="/"
          onClick={() => {
            history.push(getHomeRoute(currentUser));
          }}
          className="logo"
        >
          {collapsed ? <IconLogo /> : <TextLogoWhite />}
        </div>
      </Tooltip>
      <NavMenu onClick={onClick} menuItems={menuItems} currentPath={history.location.pathname} />
    </>
  );
};

export default withRouter(NavRouter);
