import { Layout as AntLayout } from 'antd';
import styled from 'styled-components';

const LayoutWrapper = styled(AntLayout)`
  background-color: white !important;
`;

export default LayoutWrapper;

// .ant-layout-content {
//   margin: 24px;
// }
