import React from 'react';
import PermissionChecker from 'modules/auth/permissionChecker';
import { i18n } from 'i18n';
import { UserOutlined, LogoutOutlined, SettingOutlined, TeamOutlined, WalletOutlined, ProfileOutlined, ApiOutlined, IdcardOutlined, SafetyOutlined } from '@ant-design/icons';
import Permissions from 'security/permissions';
import config, { isBeforeQA } from 'config';

/**
 * Gets the User navigation sub menu items.
 *
 * @param {Object} currentUser
 * @param {Object} [permissionChecker]
 * @returns {Array<Object>}
 */
export const getUserSubMenuItems = (currentUser, permissionChecker = PermissionChecker) => {
  const permissionValidator = new permissionChecker(currentUser);
  const hasOwnerAccess = permissionValidator.match(Permissions.values.accountOwner);

  const subItems = [
    {
      name: i18n('settings.nav.myProfile'),
      icon: <UserOutlined />,
      route: '/settings/profile',
    },
  ];

  subItems.push({
    name: i18n('settings.nav.mySettings'),
    icon: <SettingOutlined />,
    route: '/settings/preferences',
  });

  if (config && config.authStrategy === 'cognito' && !hasOwnerAccess) {
    subItems.push({
      name: i18n('settings.nav.security'),
      icon: <SafetyOutlined />,
      route: '/settings/security',
    });
  }

  subItems.push({
    name: i18n('common.logout'),
    icon: <LogoutOutlined />,
    route: 'login',
  });

  return subItems;
};

export const getSettingsSubMenuItems = (currentUser, trial, addons, permissionChecker = PermissionChecker) => {
  const permissionValidator = new permissionChecker(currentUser);
  const hasBillingAccess = permissionValidator.match(Permissions.values.accountBillingAccess);
  const hasAdminAccess = permissionValidator.match(Permissions.values.accountManagement);
  const hasOwnerAccess = permissionValidator.match(Permissions.values.accountOwner);
  const hasSphinxAccess = permissionValidator.match(Permissions.values.sphinxApiKeysAccess);

  const subItems = [];

  if (hasAdminAccess) {
    subItems.push({
      name: i18n('settings.nav.companyProfile'),
      icon: <UserOutlined />,
      route: '/settings/account',
    });

    subItems.push({
      name: i18n('settings.nav.seats'),
      icon: <TeamOutlined />,
      route: '/settings/seats',
    });

    if (isBeforeQA) {
      subItems.push({
        name: i18n('settings.nav.operators'),
        icon: <IdcardOutlined />,
        route: '/settings/operators',
      });
    }
  }

  if (hasOwnerAccess) {
    subItems.push({
      name: i18n('settings.nav.security'),
      icon: <SafetyOutlined />,
      route: '/settings/security',
    });
  }

  if (hasBillingAccess) {
    subItems.push({
      name: i18n('settings.nav.billing'),
      icon: <WalletOutlined />,
      route: '/settings/billing',
    });
  }

  if (hasAdminAccess) {
    subItems.push({
      name: i18n('settings.nav.configs'),
      icon: <ProfileOutlined />,
      route: '/settings/configs',
    });
  }

  if (config && config.sphinx.netinspect.enabled && hasSphinxAccess && (trial || addons.net_inspect)) {
    subItems.push({
      name: i18n('entities.connection.titleShort'),
      icon: <ApiOutlined />,
      route: '/settings/connection',
    });
  }

  return subItems;
};
