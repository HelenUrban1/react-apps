import { getMenuItems } from './MenuItems';
import PermissionChecker from 'modules/auth/permissionChecker';

describe('MenuItems', () => {
  let currentUser;

  beforeEach(() => {
    currentUser = {};
  });

  describe('#getMenuItems()', () => {
    it('does not include the parts menu option', () => {
      class MockPermissionChecker extends PermissionChecker {
        match() {
          return undefined;
        }
      }
      const items = getMenuItems(currentUser, false, MockPermissionChecker);
      expect(items.find((item) => item.route.includes('/parts'))).toBeUndefined();
    });

    it('does include the parts menu option', () => {
      class MockPermissionChecker extends PermissionChecker {
        match() {
          return () => true;
        }
      }
      const items = getMenuItems(currentUser, false, MockPermissionChecker);
      expect(items.find((item) => item.route.includes('/parts'))).toBeDefined();
      expect(items.find((item) => item.route.includes('/help'))).toBeDefined();
    });
  });
});
