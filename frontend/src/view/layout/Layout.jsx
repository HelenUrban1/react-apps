import { Button, Layout as AntLayout } from 'antd';
import React, { useEffect, useState } from 'react';
import { useHistory } from 'react-router';
import { useDispatch, useSelector } from 'react-redux';
import { useMutation } from '@apollo/client';
import { i18n } from 'i18n';
import Modal from 'antd/lib/modal/Modal';
import { SubscriptionActions } from 'modules/subscription/subscriptionActions';
import TrialBar from 'view/shared/styles/TrialBar';
import LayoutWrapper from 'view/layout/styles/LayoutWrapper';
import { ErrorBoundary } from 'view/global/ErrorBoundary';
import { iamAPI } from 'domain/setting/iamApi';
import InviteUsersModal from 'domain/setting/seats/InviteUsersModal';
import { ContactInfo } from 'styleguide/PricingTable/ContactInfo';
import NavRouter from './Nav';
import { IxSider } from '../../styleguide';
import { HealthCheck } from './HealthCheck';
import './styles/layoutStyle.less';

const { Footer, Content } = AntLayout;

const Layout = ({ isBilling, currentUser, children }) => {
  const [collapsed, setCollapsed] = useState(true);
  const dispatch = useDispatch();
  const history = useHistory();

  const { type, state, show } = useSelector((appState) => appState.subscription);
  const [showPastDue, setPastDue] = useState();
  const [inviteModal, showInviteModal] = useState(false);

  useEffect(() => {
    // initialize modal after subscription has loaded
    if (showPastDue === undefined && show && state && state.trim() === 'past_due') {
      setPastDue(true);
    }
  }, [state]);

  const [inviteUser] = useMutation(iamAPI.mutate.invite);

  const toggleCollapse = () => {
    setCollapsed(!collapsed);
  };

  const hideExpired = () => {
    dispatch({
      type: SubscriptionActions.TRIAL_HIDE,
    });
  };

  const closeExpired = () => {
    setPastDue(false);
    hideExpired();
  };

  const openInvite = () => {
    closeExpired();
    showInviteModal(true);
  };

  const closeInvite = () => {
    showInviteModal(false);
  };

  const goToBillingSettings = () => {
    closeExpired();
    history.push('/settings/billing');
  };

  const handleContactSales = () => {
    window.open('mailto:orders.qcessentials@ideagen.com', '_blank');
  };

  const pastDueModal = (
    <Modal
      key="past-due-modal"
      data-cy="past-due-modal"
      className="past-due-modal"
      visible={showPastDue}
      title={i18n('entities.subscription.past_due.title')}
      onCancel={closeExpired}
      footer={[
        <Button className="btn-secondary" data-cy="close-past-due-modal-btn" key="close-btn" onClick={handleContactSales}>
          {i18n('entities.subscription.past_due.orders')}
        </Button>,
        isBilling ? (
          <Button className="btn-primary" data-cy="subscribe-past-due-modal-btn" key="subscribe-btn" onClick={goToBillingSettings}>
            {i18n('entities.subscription.past_due.billing')}
          </Button>
        ) : (
          <Button className="btn-primary" data-cy="subscribe-past-due-modal-btn" key="subscribe-btn" onClick={openInvite}>
            {i18n('entities.subscription.past_due.invite')}
          </Button>
        ),
      ]}
    >
      <section key="past-due-copy-key" className="past-due-modal-copy" data-cy="past-due-modal-copy-section">
        <section className="copy">
          <p>{i18n('entities.subscription.past_due.desc')}</p>
          {isBilling && (
            <p>
              {i18n('entities.subscription.past_due.admin1')}
              <button type="button" className="link" onClick={goToBillingSettings}>
                {i18n('entities.subscription.past_due.billing')}
              </button>
              {i18n('entities.subscription.past_due.admin2')}
            </p>
          )}
          {!isBilling && <p>{i18n('entities.subscription.past_due.user')}</p>}
        </section>
        <div className="info-text" data-cy="contact-message">
          {i18n('entities.subscription.past_due.contact')}
          <ContactInfo email="orders.qcessentials@ideagen.com" />
        </div>
      </section>
    </Modal>
  );

  return (
    <LayoutWrapper>
      <HealthCheck />
      <IxSider onCollapse={toggleCollapse} menu={<NavRouter collapsed={collapsed} />} />
      <AntLayout className={`main-content-layout ${state === 'trial' ? 'trial' : ''}`}>
        <Content>
          <ErrorBoundary>{children}</ErrorBoundary>
        </Content>
        {pastDueModal}
        <InviteUsersModal //
          showModal={inviteModal}
          closeInvitation={closeInvite}
          show={inviteModal}
          account={{}}
          inviteUser={inviteUser}
          users={[]}
          enforceSeats
          openSeats={0}
          loading={false}
          defaultRole="collaboratorBilling"
          button={false}
        />
        {(state === 'trial' || state === 'trial_ended' || type === 'Freemium') && (
          <Footer className="layout-trial-footer">
            <TrialBar accountId={currentUser.accountId} isBilling={isBilling} hideExpired={hideExpired} />
          </Footer>
        )}
      </AntLayout>
    </LayoutWrapper>
  );
};

export default Layout;
