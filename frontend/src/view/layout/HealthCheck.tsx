import React from 'react';
import { isObject, reduce, merge } from 'lodash';
import config from 'config';

export const HealthCheck: React.FC = React.memo(() => {
  // TODO: Move to utility class
  const flattenKeys = (obj: object, path: any[] = []): any => (!isObject(obj) ? { [path.join('.')]: obj } : reduce(obj, (cum, next, key) => merge(cum, flattenKeys(next, [...path, key])), {}));

  // Flatten the JSON and write as HTML (for humans to read in browser) with data attributes (for dev tools to read)
  const flatConfig = Object.entries(flattenKeys(config));

  // Write the output to the console
  // log.debugtable(flatConfig);
  // log.info('App.config', config);

  // Write to hidden HTML table
  const configMarkup = (
    <table id="config" data-json={JSON.stringify(config)}>
      <thead>
        <tr>
          <th>Path</th>
          <th>Value</th>
        </tr>
      </thead>
      <tbody>
        {flatConfig.map((setting: any) => (
          <tr key={setting[0]} data-path={setting[0]} data-value={setting[1]}>
            <th className="path">{setting[0]}</th>
            <td className="value">{setting[1]}</td>
          </tr>
        ))}
      </tbody>
    </table>
  );

  return (
    <div id="v" style={{ display: 'none' }}>
      {configMarkup}
    </div>
  );
});
