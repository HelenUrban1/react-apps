import { Part, PartRevision } from 'domain/part/partTypes';
import { Drawing } from 'graphql/drawing';
import moment from 'moment';

export const getRevisionIndex = ({ parts, drawings, partId, drawingId }: { parts?: { [key: string]: PartRevision }; drawings?: Drawing[]; partId?: string; drawingId?: string }) => {
  let index = -1;
  let type = null;
  if (parts && partId) {
    const partKeys = Object.keys(parts).sort((a, b) => (moment(parts[a].createdAt).isAfter(moment(parts[b].createdAt)) ? -1 : 1));
    index = partKeys.indexOf(partId);
    if (index > 0) {
      type = 'part';
    }
  }
  const currentDrawing = drawings?.find((d) => d.id === drawingId);
  if (index < 1 && drawings && drawingId && currentDrawing) {
    const originalDrawingId = currentDrawing.originalDrawing;
    index = drawings
      .filter((d) => d.originalDrawing === originalDrawingId)
      .sort((a, b) => (moment(a.createdAt).isAfter(moment(b.createdAt)) ? -1 : 1))
      .map((d) => d.id)
      .indexOf(drawingId);
    if (index > 0) {
      type = 'drawing';
    }
  }
  return { index, type };
};

export const checkCurrentRevision = (part: Part, revisions: { [key: string]: PartRevision }, partDrawing: Drawing | null) => {
  let revisionIndex = 0;
  let revisionType = null;
  if (part) {
    const rev = getRevisionIndex({
      parts: revisions,
      partId: part.id,
      drawings: part.drawings,
      drawingId: partDrawing?.id || part.primaryDrawing.id,
    });
    revisionIndex = rev.index;
    revisionType = rev.type;
  }
  return { revisionIndex, revisionType };
};
