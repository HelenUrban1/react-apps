import { i18n } from 'i18n';

/*
    Calculates the difference in days between two Dates
    Assumes the Dates are already 
    // TODO: make sure this accounts for daylight savings time?
*/
export const getDifferenceInDays = (start: Date, end: Date) => {
  const utc1 = new Date(start.getFullYear(), start.getMonth(), start.getDate());
  const utc2 = new Date(end.getFullYear(), end.getMonth(), end.getDate());
  const differenceInTime = utc2.getTime() - utc1.getTime();
  return Math.round(differenceInTime / (1000 * 60 * 60 * 24));
};

/*
  Converts date format from 'YYYY-MM-DD' to Month Day, Year
*/
export const dateFromNumbersToWords = (date: Date | undefined) => {
  if (!date) return '';
  const dateBits = date.toString().split('-');
  const i18string = `home.charts.months.${parseInt(dateBits[1], 10)}.abr`;
  return `${i18n(i18string)} ${dateBits[2]}, ${dateBits[0]}`;
};

export const getTrialExpiredCountdown = (trialEndedAt: Date | undefined) => {
  if (!trialEndedAt) return i18n('comming.unknown');
  return 30 - getDifferenceInDays(new Date(trialEndedAt), new Date());
};
