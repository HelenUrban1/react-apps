import { fabric } from 'fabric';
import { Grid } from 'domain/part/edit/grid/Grid';
import { defaultSubtypeIDs, defaultTypeIDs, DefaultTolerances } from 'view/global/defaults';
import { ParseTextController } from './ParseTextController';

const mockInterpretNotationText = jest.spyOn(ParseTextController, 'interpretNotationText');
const mockOCR = { ocr: 'something', tess: 'something' };
const mockPytorch = {
  type: 'pytorch',
  notation: { type: 'length', nominal: '2.5' },
};
const mockTesseract = {
  type: 'tesseract',
  notation: { type: 'length', nominal: '2.5' },
};
const mockParseError = {
  error: { name: 'Lexing Errors' },
};
const mockDimension = {
  input: '1.25±.01',
  notation: {
    nominal: { text: '1.25', value: '1.25', units: '' },
    upper_tol: { text: '.01', value: '.01', units: '', sign: '+' },
    lower_tol: { text: '-.01', value: '.01', units: '', sign: '-' },
    text: '1.25±.01',
    type: 'nominalWithTolerances',
  },
  quantity: {
    text: '',
    units: '',
    value: '1',
  },
  text: '1.25±.01',
  words: '',
};
const mockGDT = {
  input: '|<|1.5|A|B|C|',
  notation: {
    datums: [{ id: 'A' }, { id: 'B' }, { id: 'C' }],
    subtype: 'Angularity',
    text: '|<|1.5|A|B|C|',
    type: 'gdtControlFrame',
    zones: { primary: { modifier: '', postModifierValue: '', prefix: '', text: '1.5', value: '1.5' } },
  },
  quantity: {
    text: '',
    units: '',
    value: '1',
  },
  text: '|<|1.5|A|B|C|',
  words: '',
};
const mockNote = {
  input: 'This is a Note We Captured',
  notation: {
    type: 'unknown',
  },
  quantity: {
    text: '',
    units: '',
    value: '1',
  },
  text: 'This is a Note We Captured',
  words: 'This is a Note We Captured',
};
const uuid = 'e36abada-2a27-40e0-9dd2-e58fea53bf02';

const dimension = {
  status: 'Active',
  verified: false,
  notationType: 'db687ac1-7bf8-4ada-be2b-979f8921e1a0', // Dimension
  notationSubtype: 'edc8ecda-5f88-4b81-871f-6c02f1c50afd', // Length
  notationClass: 'Tolerance',
  fullSpecification: '1.25±.01',
  quantity: 1,
  nominal: '1.25',
  upperSpecLimit: '1.26',
  lowerSpecLimit: '1.24',
  plusTol: '.01',
  minusTol: '-.01',
  toleranceSource: 'Document_Defined',
  drawingRotation: 0.0,
  drawingScale: 1,
  boxLocationY: 120,
  boxLocationX: 220,
  boxWidth: 250,
  boxHeight: 100,
  boxRotation: 0,
  markerLocationX: 160,
  markerLocationY: 180,
  markerFontSize: 40,
  markerSize: 80,
  markerGroup: '1',
  markerGroupShared: false,
  markerIndex: 0,
  markerSubIndex: -1,
  markerLabel: '1',
  markerStyle: 'f12c5566-7612-48e9-9534-ef90b276ebaa',
  gridCoordinates: '3B',
  balloonGridCoordinates: '4B',
  connectionPointGridCoordinates: 'External',
};

const note = {
  status: 'Active',
  verified: false,
  notationType: 'fa3284ed-ad3e-43c9-b051-87bd2e95e927', // Note
  notationSubtype: '805fd539-8063-4158-8361-509e10a5d371', // Note
  notationClass: 'N_A',
  fullSpecification: 'This is a Note We Captured',
  quantity: 1,
  nominal: 'This is a Note We Captured',
  toleranceSource: 'N_A',
  drawingRotation: 0.0,
  drawingScale: 1,
  boxLocationY: 120,
  boxLocationX: 220,
  boxWidth: 250,
  boxHeight: 100,
  boxRotation: 0,
  markerLocationX: 160,
  markerLocationY: 180,
  markerFontSize: 40,
  markerSize: 80,
  markerGroup: '1',
  markerGroupShared: false,
  markerIndex: 0,
  markerSubIndex: -1,
  markerLabel: '1',
  markerStyle: 'f12c5566-7612-48e9-9534-ef90b276ebaa',
  gridCoordinates: '3B',
  balloonGridCoordinates: '4B',
  connectionPointGridCoordinates: 'External',
};

const GDT = {
  status: 'Active',
  verified: false,
  notationType: 'f3c4d8e0-fb79-4c72-a3f8-3f584ea075a7', // Geometric Tolerance
  notationSubtype: '5ce784e8-f614-4e6d-a79b-237b4dc751dd', // Angularity
  notationClass: 'Tolerance',
  fullSpecification: '<', // |<|1.5|A|B|C|
  quantity: 1,
  upperSpecLimit: '1.5',
  lowerSpecLimit: '',
  gdtSymbol: '',
  gdtPrimaryToleranceZone: '1.5',
  gdtSecondaryToleranceZone: '',
  gdtPrimaryDatum: 'A',
  gdtSecondaryDatum: 'B',
  gdtTertiaryDatum: 'C',
  toleranceSource: 'Document_Defined',
  drawingRotation: 0.0,
  drawingScale: 1,
  boxLocationY: 120,
  boxLocationX: 220,
  boxWidth: 250,
  boxHeight: 100,
  boxRotation: 0,
  markerLocationX: 160,
  markerLocationY: 180,
  markerFontSize: 40,
  markerSize: 80,
  markerGroup: '1',
  markerGroupShared: false,
  markerIndex: 0,
  markerSubIndex: -1,
  markerLabel: '1',
  markerStyle: 'f12c5566-7612-48e9-9534-ef90b276ebaa',
  gridCoordinates: '3B',
  balloonGridCoordinates: '4B',
  connectionPointGridCoordinates: 'External',
};

const units = {
  'e8a47ed1-22b8-4ef0-baf9-374df6189b97': {
    id: 'e8a47ed1-22b8-4ef0-baf9-374df6189b97',
    value: 'watt',
    default: true,
    meta: {
      unit: 'W',
      type: 'Metric',
      measurement: 'power',
    },
    status: 'Active',
    listType: 'Unit_of_Measurement',
    index: 0,
    children: [],
  },
  '45976189-a841-4959-97e6-1dfe35da78e4': {
    id: '45976189-a841-4959-97e6-1dfe35da78e4',
    value: 'millimeter',
    default: true,
    meta: {
      unit: 'mm',
      type: 'Metric',
      measurement: 'length',
    },
    status: 'Active',
    listType: 'Unit_of_Measurement',
    index: 1,
    children: [],
  },
  'e1150ac6-6803-41fa-b525-9b8aba2eccb2': {
    id: 'e1150ac6-6803-41fa-b525-9b8aba2eccb2',
    value: 'degree',
    default: true,
    meta: {
      unit: 'deg',
      type: 'Imperial,Metric',
      measurement: 'plane_angle',
    },
    status: 'Active',
    listType: 'Unit_of_Measurement',
    index: 2,
    children: [],
  },
};

const types = {
  'db687ac1-7bf8-4ada-be2b-979f8921e1a0': {
    id: 'db687ac1-7bf8-4ada-be2b-979f8921e1a0',
    value: 'Dimension',
    default: true,
    meta: '',
    status: 'Active',
    listType: 'Type',
    index: 0,
    children: {
      'edc8ecda-5f88-4b81-871f-6c02f1c50afd': {
        id: 'edc8ecda-5f88-4b81-871f-6c02f1c50afd',
        value: 'Length',
        default: true,
        meta: { measurement: 'Length' },
        status: 'Active',
        listType: 'Type',
        index: 0,
        parentId: 'db687ac1-7bf8-4ada-be2b-979f8921e1a0',
      },
      'b2e1e0aa-0da4-4844-853a-f20971b013a4': {
        id: 'b2e1e0aa-0da4-4844-853a-f20971b013a4',
        value: 'Angle',
        default: true,
        meta: { measurement: 'Plane Angle' },
        status: 'Active',
        listType: 'Type',
        index: 1,
        parentId: 'db687ac1-7bf8-4ada-be2b-979f8921e1a0',
      },
      '49d45a6c-1073-4c6e-805f-8f4105641bf1': {
        id: '49d45a6c-1073-4c6e-805f-8f4105641bf1',
        value: 'Radius',
        default: true,
        meta: { measurement: 'Length' },
        status: 'Active',
        listType: 'Type',
        index: 2,
        parentId: 'db687ac1-7bf8-4ada-be2b-979f8921e1a0',
      },
      '4396f3c2-a915-4458-8cff-488b6d301ecd': {
        id: '4396f3c2-a915-4458-8cff-488b6d301ecd',
        value: 'Diameter',
        default: true,
        meta: { measurement: 'Length' },
        status: 'Active',
        listType: 'Type',
        index: 3,
        parentId: 'db687ac1-7bf8-4ada-be2b-979f8921e1a0',
      },
    },
  },
  'f3c4d8e0-fb79-4c72-a3f8-3f584ea075a7': {
    id: 'f3c4d8e0-fb79-4c72-a3f8-3f584ea075a7',
    value: 'Geometric Tolerance',
    default: true,
    meta: '',
    status: 'Active',
    listType: 'Type',
    index: 1,
    children: {
      'ea0e747f-9e70-4e2e-9a34-4491f6b63593': {
        id: 'ea0e747f-9e70-4e2e-9a34-4491f6b63593',
        value: 'Profile of a Surface',
        default: true,
        meta: { measurement: 'Length' },
        status: 'Active',
        listType: 'Type',
        index: 0,
        parentId: 'f3c4d8e0-fb79-4c72-a3f8-3f584ea075a7',
      },
      '19a7a9a1-76c5-45ef-8cf9-611ebe7cf207': {
        id: '19a7a9a1-76c5-45ef-8cf9-611ebe7cf207',
        value: 'Position',
        default: true,
        meta: { measurement: 'Length' },
        status: 'Active',
        listType: 'Type',
        index: 1,
        parentId: 'f3c4d8e0-fb79-4c72-a3f8-3f584ea075a7',
      },
      '5ce784e8-f614-4e6d-a79b-237b4dc751dd': {
        id: '5ce784e8-f614-4e6d-a79b-237b4dc751dd',
        value: 'Angularity',
        default: true,
        meta: { measurement: 'Plane Angle' },
        status: 'Active',
        listType: 'Type',
        index: 2,
        parentId: 'f3c4d8e0-fb79-4c72-a3f8-3f584ea075a7',
      },
    },
  },
  'fa3284ed-ad3e-43c9-b051-87bd2e95e927': {
    id: 'fa3284ed-ad3e-43c9-b051-87bd2e95e927',
    value: 'Note',
    default: true,
    meta: '',
    status: 'Active',
    listType: 'Type',
    index: 1,
    children: {
      '805fd539-8063-4158-8361-509e10a5d371': {
        id: '805fd539-8063-4158-8361-509e10a5d371',
        value: 'Note',
        default: true,
        meta: '',
        status: 'Active',
        listType: 'Type',
        index: 0,
        parentId: 'fa3284ed-ad3e-43c9-b051-87bd2e95e927',
      },
    },
  },
  'b8599a29-7560-48e2-807a-6f3c3c0f2366': {
    id: 'b8599a29-7560-48e2-807a-6f3c3c0f2366',
    value: 'Other',
    default: true,
    meta: '',
    status: 'Active',
    listType: 'Type',
    index: 3,
    children: {
      'd02aa7bc-b791-413d-be10-fe5359bb8385': {
        id: 'd02aa7bc-b791-413d-be10-fe5359bb8385',
        value: 'Electric Power',
        default: true,
        meta: { measurement: 'Power' },
        status: 'Active',
        listType: 'Type',
        index: 0,
        parentId: 'b8599a29-7560-48e2-807a-6f3c3c0f2366',
      },
    },
  },
};
const tolerances = {
  linear: {
    type: 'PrecisionTolerance',
    rangeTols: [
      { rangeLow: '1', rangeHigh: '9', plus: '1', minus: '-1' },
      { rangeLow: '0.1', rangeHigh: '0.9', plus: '0.1', minus: '-0.1' },
      { rangeLow: '0.01', rangeHigh: '0.09', plus: '0.01', minus: '-0.01' },
      { rangeLow: '0.001', rangeHigh: '0.009', plus: '0.001', minus: '-0.001' },
      { rangeLow: '0.0001', rangeHigh: '0.0009', plus: '0.0001', minus: '-0.0001' },
    ],
    precisionTols: [
      { level: 'X', plus: '1', minus: '-1' },
      { level: 'X.X', plus: '0.1', minus: '-0.1' },
      { level: 'X.XX', plus: '0.01', minus: '-0.01' },
      { level: 'X.XXX', plus: '0.001', minus: '-0.001' },
      { level: 'X.XXXX', plus: '0.0001', minus: '-0.0001' },
    ],
    unit: 'placeholder',
  },
  angular: {
    type: 'PrecisionTolerance',
    rangeTols: [
      { rangeLow: '1', rangeHigh: '9', plus: '1', minus: '-1' },
      { rangeLow: '0.1', rangeHigh: '0.9', plus: '0.1', minus: '-0.1' },
      { rangeLow: '0.01', rangeHigh: '0.09', plus: '0.01', minus: '-0.01' },
      { rangeLow: '0.001', rangeHigh: '0.009', plus: '0.001', minus: '-0.001' },
      { rangeLow: '0.0001', rangeHigh: '0.0009', plus: '0.0001', minus: '-0.0001' },
    ],
    precisionTols: [
      { level: 'X', plus: '1', minus: '-1' },
      { level: 'X.X', plus: '0.1', minus: '-0.1' },
      { level: 'X.XX', plus: '0.01', minus: '-0.01' },
      { level: 'X.XXX', plus: '0.001', minus: '-0.001' },
      { level: 'X.XXXX', plus: '0.0001', minus: '-0.0001' },
    ],
    unit: 'placeholder',
  },
};
const assignedStyles = {
  default: {
    style: 'f12c5566-7612-48e9-9534-ef90b276ebaa',
    assign: ['Default'],
  },
};
const canvas = new fabric.Canvas();
const drawingRotation = 0;
const drawingScale = 1;
const grid = new Grid({
  initialCanvasWidth: 1000,
  initialCanvasHeight: 2000,
  lineWidth: 1,
});
const capture = {
  top: 120,
  left: 220,
  width: 250,
  height: 100,
};

describe('Parse Text Controller', () => {
  describe('Utilities', () => {
    it('should identify if the type needs to be updated', () => {
      const change = { notationType: 'something different' };
      const nonChange = { nominal: 'new nom' };
      const returnTrue = ParseTextController.typeHasChanged(change);
      const returnFalse = ParseTextController.typeHasChanged(nonChange);
      expect(returnTrue).toBeTruthy();
      expect(returnFalse).toBeFalsy();
    });
    it('should identify if the tolerances need to be updated', () => {
      let change = { nominal: 'new nom' };
      const nonChange = { quantity: 'something different' };
      let returnTrue = ParseTextController.toleranceHasChanged(change, dimension);
      const returnFalse = ParseTextController.toleranceHasChanged(nonChange, dimension);
      expect(returnTrue).toBeTruthy();
      expect(returnFalse).toBeFalsy();
      change = { gdtPrimaryToleranceZone: 'new nom' };
      returnTrue = ParseTextController.toleranceHasChanged(change, GDT);
      expect(returnTrue).toBeTruthy();
    });
    it('should throw extraction error if the ENP has a network error', () => {
      let error = 'Error: Network Error';
      expect(() => ParseTextController.checkForParsingErrors(error, {})).toThrowError('Network Error');
      error = 'Error: Request failed with status code 404';
      expect(() => ParseTextController.checkForParsingErrors(error, {})).toThrowError('Network Error');
    });
    it('should throw parsing error if the ENP lexing or parsing fails', () => {
      let error = { name: 'Lexing Errors' };
      expect(() => ParseTextController.checkForParsingErrors(error, {})).toThrowError('Saved as a Note');
      error = { name: 'Parsing Errors' };
      expect(() => ParseTextController.checkForParsingErrors(error, {})).toThrowError('Saved as a Note');
      error = '';
      const parsed = { notation: { type: 'unknown' } };
      expect(() => ParseTextController.checkForParsingErrors(error, parsed)).toThrowError('Saved as a Note');
    });
    it('should return the pytorch results if available', async () => {
      // needs to run twice becasue interpretNotationText is wrapped in promise.all now
      mockInterpretNotationText.mockReturnValueOnce(mockPytorch).mockReturnValueOnce(mockPytorch);
      const parseResults = await ParseTextController.compareOCRParseResults(mockOCR);
      expect(parseResults.parsed).toMatchObject(mockPytorch);
    });
    it('should return the tesseract results if pytorch is unavailable', async () => {
      mockInterpretNotationText.mockReturnValueOnce(mockParseError).mockReturnValueOnce(mockTesseract);
      const parseResults = await ParseTextController.compareOCRParseResults(mockOCR);
      expect(parseResults.parsed).toMatchObject(mockTesseract);
    });
    it('should return a note if both pytorch and tesseract are unavailable', async () => {
      mockInterpretNotationText.mockReturnValueOnce(mockParseError).mockReturnValueOnce(mockParseError);
      const parseResults = await ParseTextController.compareOCRParseResults(mockOCR);
      expect(parseResults.captureError).toBe('{"message":"Saved as a Note","description":"We couldn\'t recognize this feature type, it\'s been saved as a Note."}');
    });
  });
  describe('Updates', () => {
    it('updates spec limits for new nominal', () => {
      const change = { nominal: '1.50' };
      const updatedFeature = ParseTextController.updateFeatureTolerances({ change, oldFeature: dimension, types, tolerances, isGDT: false });
      expect(updatedFeature.nominal).toBe('1.50');
      expect(updatedFeature.plusTol).toBe('0.01');
      expect(updatedFeature.minusTol).toBe('-0.01');
      expect(updatedFeature.upperSpecLimit).toBe('1.51');
      expect(updatedFeature.lowerSpecLimit).toBe('1.49');
    });

    it('updates spec limits for new tolerances', () => {
      const change = { plusTol: '.05', minusTol: '-.05' };
      const updatedFeature = ParseTextController.updateFeatureTolerances({ change, oldFeature: dimension, types, tolerances, isGDT: false });
      expect(updatedFeature.nominal).toBe('1.25');
      expect(updatedFeature.plusTol).toBe('.05');
      expect(updatedFeature.minusTol).toBe('-.05');
      expect(updatedFeature.upperSpecLimit).toBe('1.30');
      expect(updatedFeature.lowerSpecLimit).toBe('1.20');
    });

    it('does not allow manual changing of the spec limits', () => {
      const change = { upperSpecLimit: '.05', lowerSpecLimit: '.05' };
      const updatedFeature = ParseTextController.updateFeatureTolerances({ change, oldFeature: dimension, types, tolerances, isGDT: false });
      expect(updatedFeature.nominal).toBe('1.25');
      expect(updatedFeature.plusTol).toBe('.01');
      expect(updatedFeature.minusTol).toBe('-.01');
      expect(updatedFeature.upperSpecLimit).toBe('1.26');
      expect(updatedFeature.lowerSpecLimit).toBe('1.24');
    });

    it('updates default tolerances for new nominal', () => {
      const change = { nominal: '1.234' };
      const updatedFeature = ParseTextController.updateFeatureTolerances({ change, oldFeature: { ...dimension, plusTol: '', minusTol: '' }, types, tolerances, isGDT: false });
      expect(updatedFeature.nominal).toBe('1.234');
      expect(updatedFeature.plusTol).toBe('0.001');
      expect(updatedFeature.minusTol).toBe('-0.001');
      expect(updatedFeature.upperSpecLimit).toBe('1.235');
      expect(updatedFeature.lowerSpecLimit).toBe('1.233');
    });

    it('updates spec limits for new GDT tolerance', () => {
      const change = { gdtPrimaryToleranceZone: '.05' };
      const updatedFeature = ParseTextController.updateFeatureTolerances({ change, oldFeature: GDT, types, tolerances, isGDT: true });
      expect(updatedFeature.gdtPrimaryToleranceZone).toBe('.05');
      expect(updatedFeature.upperSpecLimit).toBe('.05');
    });

    it('updates gdt fields when switching to gdt type', () => {
      const change = { notationType: 'f3c4d8e0-fb79-4c72-a3f8-3f584ea075a7', notationSubtype: '5ce784e8-f614-4e6d-a79b-237b4dc751dd' };
      const updatedFeature = ParseTextController.updateFeatureType({ change, oldFeature: dimension, types, tolerances, units });
      expect(updatedFeature.notationType).toBe('f3c4d8e0-fb79-4c72-a3f8-3f584ea075a7'); // GDT
      expect(updatedFeature.notationSubtype).toBe('5ce784e8-f614-4e6d-a79b-237b4dc751dd'); // Angularity
      expect(updatedFeature.gdtPrimaryToleranceZone).toBe('1.25');
      expect(updatedFeature.nominal).toBe(undefined);
      expect(updatedFeature.gdtSymbol).toBe(''); // Angularity
    });

    it('updates dimension fields when switching to a dimension type', () => {
      const change = { notationType: 'db687ac1-7bf8-4ada-be2b-979f8921e1a0', notationSubtype: 'edc8ecda-5f88-4b81-871f-6c02f1c50afd' };
      const updatedFeature = ParseTextController.updateFeatureType({ change, oldFeature: GDT, types, tolerances: DefaultTolerances, units });
      expect(updatedFeature.notationType).toBe('db687ac1-7bf8-4ada-be2b-979f8921e1a0'); // Dimension
      expect(updatedFeature.notationSubtype).toBe('edc8ecda-5f88-4b81-871f-6c02f1c50afd'); // Length
      expect(updatedFeature.gdtPrimaryToleranceZone).toBe(undefined);
      expect(updatedFeature.nominal).toBe('1.5');
    });

    it('updates from linear to angular when switching types', () => {
      const oldFeature = { ...dimension, unit: '04756ef3-affc-4b09-b1ce-ab901a75ce69' };
      const change = { notationType: defaultTypeIDs.Dimension, notationSubtype: defaultSubtypeIDs.Angle };
      const updatedFeature = ParseTextController.updateFeatureType({ change, oldFeature, types, tolerances: DefaultTolerances, units });
      expect(updatedFeature.unit).toBe('e1150ac6-6803-41fa-b525-9b8aba2eccb2');
    });

    it('updates from angular to linear when switching types', () => {
      const oldFeature = { ...dimension, unit: 'b944ffa6-c175-4625-968c-2c8c2e5f2dc6' };
      const change = { notationType: defaultTypeIDs.Dimension, notationSubtype: defaultSubtypeIDs.Diameter };
      const updatedFeature = ParseTextController.updateFeatureType({ change, oldFeature, types, tolerances: DefaultTolerances, units });
      expect(updatedFeature.unit).toBe('b909e87e-0445-4ea4-857e-3c1dd31b530b');
    });

    it('updates to an Other unit when switching types', () => {
      const oldFeature = { ...dimension, unit: 'b944ffa6-c175-4625-968c-2c8c2e5f2dc6' };
      const change = { notationType: defaultTypeIDs.Other, notationSubtype: defaultSubtypeIDs.ElectricPower };
      const updatedFeature = ParseTextController.updateFeatureType({ change, oldFeature, types, tolerances: DefaultTolerances, units });
      expect(updatedFeature.unit).toBe('e8a47ed1-22b8-4ef0-baf9-374df6189b97');
    });

    it('does not duplicate FCF when switching between GDT types', () => {
      const change = { notationType: defaultTypeIDs['Geometric Tolerance'], notationSubtype: defaultSubtypeIDs['Profile of a Line'] };
      const updatedFeature = ParseTextController.updateFeatureType({ change, oldFeature: GDT, types, tolerances: DefaultTolerances, units });
      expect(updatedFeature.notationType).toBe(defaultTypeIDs['Geometric Tolerance']); // GDT
      expect(updatedFeature.notationSubtype).toBe(defaultSubtypeIDs['Profile of a Line']); // Length
      expect(updatedFeature.gdtPrimaryToleranceZone).toBe(GDT.gdtPrimaryToleranceZone);
    });

    it('leaves extra fields alone when switching to a note', () => {
      const change = { notationType: 'fa3284ed-ad3e-43c9-b051-87bd2e95e927', notationSubtype: '805fd539-8063-4158-8361-509e10a5d371' };
      const updatedFeature = ParseTextController.updateFeatureType({ change, oldFeature: GDT, types, tolerances, units });
      expect(updatedFeature.notationType).toBe('fa3284ed-ad3e-43c9-b051-87bd2e95e927'); // Note
      expect(updatedFeature.notationSubtype).toBe('805fd539-8063-4158-8361-509e10a5d371'); // Note
      expect(updatedFeature.gdtPrimaryToleranceZone).toBe('1.5');
      expect(updatedFeature.gdtSecondaryToleranceZone).toBe('');
      expect(updatedFeature.gdtPrimaryDatum).toBe('A');
      expect(updatedFeature.gdtSecondaryDatum).toBe('B');
      expect(updatedFeature.gdtTertiaryDatum).toBe('C');
    });
  });

  describe('Controllers', () => {
    it('should parse and assign a new full specification for an existing feature', async () => {
      mockInterpretNotationText.mockReturnValueOnce(mockDimension);
      let res = await ParseTextController.parseFeatureFromFullSpec({
        assignedStyles,
        boxRotation: 0,
        canvas,
        capture,
        drawingRotation,
        drawingScale,
        fullSpecification: dimension.fullSpecification,
        grid,
        oldFeature: GDT,
        tolerances,
        types,
      });
      expect(res.feature).toMatchObject(dimension);
      mockInterpretNotationText.mockReturnValueOnce(mockGDT);
      res = await ParseTextController.parseFeatureFromFullSpec({
        assignedStyles,
        boxRotation: 0,
        canvas,
        capture,
        drawingRotation,
        drawingScale,
        fullSpecification: GDT.fullSpecification,
        grid,
        oldFeature: dimension,
        tolerances,
        types,
      });
      expect(res.feature).toMatchObject(GDT);
    });

    it('should parse and assign OCR for a new capture', async () => {
      mockInterpretNotationText.mockReturnValueOnce(mockDimension).mockReturnValueOnce(mockDimension);
      let res = await ParseTextController.parseFeatureFromFullSpec({
        assignedStyles,
        boxRotation: 0,
        canvas,
        capture,
        drawingRotation,
        drawingScale,
        ocrRes: { ocr: dimension.fullSpecification, tess: dimension.fullSpecification },
        grid,
        tolerances,
        types,
      });
      expect(res.feature).toMatchObject(dimension);
      mockInterpretNotationText.mockReturnValueOnce(mockGDT).mockReturnValueOnce(mockGDT);
      res = await ParseTextController.parseFeatureFromFullSpec({
        assignedStyles,
        boxRotation: 0,
        canvas,
        capture,
        drawingRotation,
        drawingScale,
        ocrRes: { ocr: GDT.fullSpecification, tess: GDT.fullSpecification },
        grid,
        tolerances,
        types,
      });
      expect(res.feature).toMatchObject(GDT);
      mockInterpretNotationText.mockReturnValueOnce(mockNote).mockReturnValueOnce(mockNote);
      res = await ParseTextController.parseFeatureFromFullSpec({
        assignedStyles,
        boxRotation: 0,
        canvas,
        capture,
        drawingRotation,
        drawingScale,
        ocrRes: { ocr: note.fullSpecification, tess: note.fullSpecification },
        grid,
        tolerances,
        types,
      });
      expect(res.feature).toMatchObject(note);
    });

    it('should return null if no OCR or full specification are passed to the parser', async () => {
      const res = await ParseTextController.parseFeatureFromFullSpec({
        assignedStyles,
        boxRotation: 0,
        canvas,
        capture,
        drawingRotation,
        drawingScale,
        grid,
        tolerances,
        types,
      });
      expect(res).toBeNull();
    });

    it('should assign changed values to the existing feature', async () => {
      const change = { nominal: '1.5', unit: 'new', notationType: 'db687ac1-7bf8-4ada-be2b-979f8921e1a0', notationSubtype: 'edc8ecda-5f88-4b81-871f-6c02f1c50afd' };
      const { feature } = await ParseTextController.updateChangedFeature({
        assignedStyles,
        change,
        grid,
        oldFeature: GDT,
        tolerances,
        types,
      });
      expect(feature.nominal).toBe('1.5');
      expect(feature.plusTol).toBe('0.1');
      expect(feature.minusTol).toBe('-0.1');
      expect(feature.upperSpecLimit).toBe('1.6');
      expect(feature.lowerSpecLimit).toBe('1.4');
      expect(feature.unit).toBe('new');
      expect(feature.notationType).toBe('db687ac1-7bf8-4ada-be2b-979f8921e1a0'); // Dimension
      expect(feature.notationSubtype).toBe('edc8ecda-5f88-4b81-871f-6c02f1c50afd'); // Length
      expect(feature).not.toHaveProperty('gdtPrimaryToleranceZone');
    });

    it('should update spec limits for existing GDT', async () => {
      const change = { upperSpecLimit: '1', lowerSpecLimit: '.5' };
      const { feature } = await ParseTextController.updateChangedFeature({
        assignedStyles,
        change,
        grid,
        oldFeature: GDT,
        tolerances,
        types,
      });
      expect(feature.nominal).toBe('');
      expect(feature.upperSpecLimit).toBe('1');
      expect(feature.lowerSpecLimit).toBe('.5');
      expect(feature.gdtPrimaryToleranceZone).toBe('1.5');
    });

    it('should parse and assign new tolerances for an existing feature', async () => {
      mockInterpretNotationText.mockReturnValueOnce(mockDimension);
      const res = await ParseTextController.parseNewTolerancesFromFullSpec({
        fullSpecification: dimension.fullSpecification,
        oldFeature: { ...dimension, nominal: '20.00', plusTol: '5', minusTol: '-5', upperSpecLimit: '25', lowerSpecLimit: '15' },
        tolerances,
        types,
      });
      expect(res.feature).toMatchObject({ ...dimension, nominal: '20.00', plusTol: '.01', minusTol: '-.01', upperSpecLimit: '20.01', lowerSpecLimit: '19.99' });
    });

    it('should identify if the feature is new or not', async () => {
      const change = { gdtPrimaryToleranceZone: '.05', notationType: 'f3c4d8e0-fb79-4c72-a3f8-3f584ea075a7', notationSubtype: '5ce784e8-f614-4e6d-a79b-237b4dc751dd' };
      const { feature, isNewFeature } = await ParseTextController.updateChangedFeature({
        assignedStyles,
        change,
        grid,
        oldFeature: { ...GDT, id: uuid },
        tolerances,
        types,
      });
      expect(feature.gdtPrimaryToleranceZone).toBe('.05');
      expect(feature.upperSpecLimit).toBe('.05');
      expect(feature.notationType).toBe('f3c4d8e0-fb79-4c72-a3f8-3f584ea075a7'); // GDT
      expect(feature.notationSubtype).toBe('5ce784e8-f614-4e6d-a79b-237b4dc751dd'); // Angularity
      expect(feature.nominal).toBe('');
      expect(feature.gdtSymbol).toBe(''); // Angularity
      expect(isNewFeature).toBeFalsy();
    });

    it('should return an input for saving to the database', async () => {
      const change = { notationType: 'fa3284ed-ad3e-43c9-b051-87bd2e95e927', notationSubtype: '805fd539-8063-4158-8361-509e10a5d371' };
      const { input } = await ParseTextController.updateChangedFeature({
        assignedStyles,
        change,
        grid,
        oldFeature: { ...GDT, id: uuid, part: { id: uuid } },
        tolerances,
        types,
      });
      expect(input.notationType).toBe('fa3284ed-ad3e-43c9-b051-87bd2e95e927'); // Note
      expect(input.notationSubtype).toBe('805fd539-8063-4158-8361-509e10a5d371'); // Note
      expect(input.gdtPrimaryToleranceZone).toBe('1.5');
      expect(input.gdtSecondaryToleranceZone).toBe('');
      expect(input.gdtPrimaryDatum).toBe('A');
      expect(input.gdtSecondaryDatum).toBe('B');
      expect(input.gdtTertiaryDatum).toBe('C');
      expect(input).not.toHaveProperty('id');
      expect(input).toHaveProperty('part');
      expect(input.part).toBe(uuid);
    });

    it('should not alter tolerances when Criticality is changed', async () => {
      const change = { criticality: 'Major' };
      const { feature } = await ParseTextController.updateChangedFeature({
        assignedStyles,
        change,
        grid,
        oldFeature: dimension,
        tolerances,
        types,
      });
      expect(feature.criticality).toBe('Major');
      expect(feature.plusTol).toBe('.01');
      expect(feature.minusTol).toBe('-.01');
      expect(feature.upperSpecLimit).toBe('1.26');
      expect(feature.lowerSpecLimit).toBe('1.24');
      expect(feature.toleranceSource).toBe('Document_Defined');
    });

    it('should not alter tolerances when Operation is changed', async () => {
      const change = { operation: 'Cutting' };
      const { feature } = await ParseTextController.updateChangedFeature({
        assignedStyles,
        change,
        grid,
        oldFeature: dimension,
        tolerances,
        types,
      });
      expect(feature.operation).toBe('Cutting');
      expect(feature.plusTol).toBe('.01');
      expect(feature.minusTol).toBe('-.01');
      expect(feature.upperSpecLimit).toBe('1.26');
      expect(feature.lowerSpecLimit).toBe('1.24');
      expect(feature.toleranceSource).toBe('Document_Defined');
    });

    it('should not alter tolerances when Inspection Method is changed', async () => {
      const change = { inspectionMethod: 'Micrometer' };
      const { feature } = await ParseTextController.updateChangedFeature({
        assignedStyles,
        change,
        grid,
        oldFeature: dimension,
        tolerances,
        types,
      });
      expect(feature.inspectionMethod).toBe('Micrometer');
      expect(feature.plusTol).toBe('.01');
      expect(feature.minusTol).toBe('-.01');
      expect(feature.upperSpecLimit).toBe('1.26');
      expect(feature.lowerSpecLimit).toBe('1.24');
      expect(feature.toleranceSource).toBe('Document_Defined');
    });

    it('should not alter tolerances when Notes is changed', async () => {
      const change = { notes: 'Some Notes' };
      const { feature } = await ParseTextController.updateChangedFeature({
        assignedStyles,
        change,
        grid,
        oldFeature: dimension,
        tolerances,
        types,
      });
      expect(feature.notes).toBe('Some Notes');
      expect(feature.plusTol).toBe('.01');
      expect(feature.minusTol).toBe('-.01');
      expect(feature.upperSpecLimit).toBe('1.26');
      expect(feature.lowerSpecLimit).toBe('1.24');
      expect(feature.toleranceSource).toBe('Document_Defined');
    });
  });
});
