/* 

- scale capture
- set doc properties
- can extract
- get font size
- get type style
- assign feature
-- assign defaults
-- assign styles
-- type specific? (unit?)
- place markers
- place capture rectangles


*/

import { fabric } from 'fabric';
import { Canvas } from 'fabric/fabric-impl';
import { getAnnotationTextCoords, getCartesianFromPolar, getTransformedDimensions, gridPosition } from 'domain/part/edit/characteristics/utils/coordTransforms';
import { Grid } from 'domain/part/edit/grid/Grid';
import log from 'modules/shared/logger';
// import { getListNameById } from 'utils/Lists';
import { CapturedItem, Characteristic } from 'types/characteristics';
import { MarkerStyle } from 'types/marker';
// import { markers } from 'styleguide/ItemList/fakeList';
import { Annotation, ANNOTATION_TEXT_PADDING } from 'types/annotation';
import { CaptureCoordinates, CaptureDimensions, ParsedCharacteristic } from './types';

export class FeatureUI {
  // Utilities
  static scaleCaptureDimensions({ tl, br, scale }: { tl: fabric.Point; br: fabric.Point; scale: number }) {
    tl.divideEquals(scale);
    br.divideEquals(scale);

    return {
      left: tl.x,
      top: tl.y,
      width: br.x - tl.x,
      height: br.y - tl.y,
    };
  }

  static captureHasOverlap({ select, pointer, canvas }: { select: CaptureCoordinates; pointer: CaptureCoordinates; canvas: Canvas }) {
    const { tl, br } = this.getCaptureCorners({ select, pointer });
    // Check to see if the rectangle includes other captures
    const canvasObjects = canvas.getObjects();
    for (let i = 0; i < canvasObjects.length; i++) {
      if (canvasObjects[i].isContainedWithinRect(tl, br)) {
        // balloon or rectangle was within bounding box
        return true;
      }
    }
    return false;
  }

  static getFontSize({ height, width, rotation }: { height: number; width: number; rotation: number }) {
    let size = height;
    if (width * 1.2 < height && rotation % 90 === 0) {
      size = width;
    }
    let fontSize = Math.floor(Math.abs(size) / 1.6);
    if (fontSize % 2 === 1) {
      fontSize += 1;
    }
    fontSize = Math.min(fontSize, 40);
    fontSize = Math.max(fontSize, 8);

    return fontSize;
  }

  static getTypeMarkerStyle({ styleOptions, type, subtype }: { styleOptions: { [key: string]: { style: string; assign: string[] } }; type: string; subtype: string }) {
    let markerStyle = styleOptions.default.style;
    Object.keys(styleOptions).forEach((key) => {
      if (styleOptions[key].assign[1] === type && styleOptions[key].assign[2] === subtype) markerStyle = styleOptions[key].style;
    });
    return { style: markerStyle, matched: markerStyle !== styleOptions.default.style };
  }

  static getMarkerStyleFromId({ styleOptions, styleId }: { styleOptions: { [key: string]: { style: string; assign: string[] } }; styleId: string }) {
    let markerStyle = styleOptions.default.style;
    Object.keys(styleOptions).forEach((key) => {
      if (styleOptions[key].assign[1] === styleId) markerStyle = styleOptions[key].style;
    });
    return { style: markerStyle, matched: markerStyle !== styleOptions.default.style };
  }

  static getMarkerStyle({
    styleOptions,
    type,
    subtype,
    classification,
    operation,
    inspectionMethod,
  }: {
    styleOptions: { [key: string]: { style: string; assign: string[] } };
    type: string;
    subtype: string;
    classification?: string | null;
    operation?: string | null;
    inspectionMethod?: string | null;
  }) {
    let matchedStyle = { style: '', matched: false };
    if (classification) matchedStyle = this.getMarkerStyleFromId({ styleOptions, styleId: classification });
    if (!matchedStyle?.matched && operation) matchedStyle = this.getMarkerStyleFromId({ styleOptions, styleId: operation });
    if (!matchedStyle?.matched && inspectionMethod) matchedStyle = this.getMarkerStyleFromId({ styleOptions, styleId: inspectionMethod });
    // check against types
    if (!matchedStyle?.matched) matchedStyle = this.getTypeMarkerStyle({ styleOptions, type, subtype });
    return matchedStyle;
  }

  static getCaptureCorners({ select, pointer }: { select: CaptureCoordinates; pointer: CaptureCoordinates }) {
    const left = Math.min(select.x, pointer.x);
    const top = Math.min(select.y, pointer.y);
    const right = left + Math.abs(select.x - pointer.x);
    const bottom = top + Math.abs(select.y - pointer.y);

    const tl = new fabric.Point(left, top);
    const br = new fabric.Point(right, bottom);

    return { tl, br };
  }

  // Fabric Rendering
  // NOTE: Might be the wrong place for these, since the capture shows up before we get to parsing the character
  static renderFeatureMarker({
    anchorCoord,
    charCoord,
    balloon,
    canvas,
    rotation,
    markerRotation,
  }: {
    //
    anchorCoord: CaptureDimensions;
    charCoord: CaptureCoordinates;
    balloon: fabric.Group;
    canvas: fabric.Canvas;
    rotation: number;
    markerRotation: number;
  }) {
    const markerCartesian = getCartesianFromPolar({ x: anchorCoord.left, y: anchorCoord.top }, { x: charCoord.x, y: (charCoord.y + rotation) % 360 }, markerRotation);
    const newBalloon = balloon;
    newBalloon.left = markerCartesian.x + anchorCoord.width / 2;
    newBalloon.top = markerCartesian.y + anchorCoord.height / 2;
    canvas.add(newBalloon);
    return newBalloon;
  }

  static renderFeatureCapture({ rectangle, anchorCoord, boxRotation, canvas }: { rectangle: fabric.Rect; anchorCoord: CaptureDimensions; boxRotation: number; canvas: fabric.Canvas }) {
    // set the rotated dimensions for the rectangle
    const newRect = rectangle;
    newRect.left = anchorCoord.left + anchorCoord.width / 2;
    newRect.top = anchorCoord.top + anchorCoord.height / 2;
    newRect.width = anchorCoord.width;
    newRect.height = anchorCoord.height;
    newRect.angle = boxRotation;
    newRect.setCoords();
    // fabric.js caches objects as images for performance, originally the rect was going to the right position but wasn't rotating
    // apparently it was using the cached image and just changing location, set it to dirty to replace the cached image with the new rect
    newRect.set('dirty', true);
    canvas.add(newRect);
    return newRect;
  }

  static renderAnnotationText({ annotation, anchorCoord, boxRotation, rotation, canvas }: { annotation: fabric.Group; anchorCoord: CaptureDimensions; boxRotation: number; rotation: number; canvas: fabric.Canvas }) {
    // set the rotated dimensions for the rectangle
    const newGroup = annotation;
    newGroup.set({
      left: anchorCoord.left + anchorCoord.width / 2,
      top: anchorCoord.top + anchorCoord.height / 2,
      width: anchorCoord.width,
      height: anchorCoord.height,
      angle: boxRotation,
      dirty: true,
    });

    const objs = newGroup._objects;
    objs.forEach((obj) => {
      if (obj.type === 'annotation-text') {
        (obj as fabric.Textbox).set({
          ...getAnnotationTextCoords({ anchorCoord, boxRotation, rotation }),
          angle: rotation,
          originX: 'left',
          originY: 'top',
          dirty: true,
        });
      } else {
        obj.set({
          width: anchorCoord.width,
          height: anchorCoord.height,
          left: 0,
          top: 0,
          dirty: true,
        });
      }
    });

    newGroup.setCoords();
    canvas.add(newGroup);
    return newGroup;
  }

  static encodeAnnotationText = (anText: fabric.Textbox) => {
    // Need to encode where the text wraps in the Fabric textbox differently
    // than where the user-added new lines are so they can be handled correctly
    // on the canvas and when downloading the ballooned PDF (pdftron doesn't auto handle text wrapping)
    const newlineIndices: number[] = [];
    const str = anText.text || '';
    for (let i = 0; i < str.length; i++) {
      if (str[i] === '\n') {
        // track where user-added new lines are to replace with <NEWLINE> later
        newlineIndices.push(i);
      }
    }
    const lines = anText._textLines;
    let newText = '';
    let ind = 0;
    lines.forEach((chars, index) => {
      ind += chars.length;
      newText += chars.join('');
      if (index !== lines.length - 1) {
        // only add newline or space if this isn't the last line of text
        newText += newlineIndices.includes(ind) ? '<NEWLINE>' : '<WRAP>';
      }
      ind += 1;
    });

    return newText;
  };

  static renderTextboxAfterEdit({ rect, anText, rotation }: { rect: fabric.Rect; anText: fabric.Textbox; rotation: number }) {
    if (rect && rect.width && rect.height && rect.scaleX && rect.scaleY && anText.width && anText.height && rect.left && rect.top) {
      const annot = anText.toObject() as Annotation;
      const newWidth = anText.width;
      const newHeight = anText.height;

      switch ((360 + rotation - annot.drawingRotation) % 360) {
        case 90:
          if (rect.height * rect.scaleY < anText.width + ANNOTATION_TEXT_PADDING) {
            const diff = newWidth - rect.height;
            rect.set({
              height: rect.height + diff + ANNOTATION_TEXT_PADDING,
              top: rect.top + (diff + ANNOTATION_TEXT_PADDING) / 2,
            });
          } else if (rect.width * rect.scaleX < anText.height + ANNOTATION_TEXT_PADDING) {
            const diff = newHeight - rect.width;
            rect.set({
              width: rect.width + diff + ANNOTATION_TEXT_PADDING,
              left: rect.left - (diff + ANNOTATION_TEXT_PADDING) / 2,
            });
          }
          break;
        case 180:
          if (rect.width * rect.scaleX < anText.width + ANNOTATION_TEXT_PADDING) {
            const diff = newWidth - rect.width;
            rect.set({
              width: rect.width + diff + ANNOTATION_TEXT_PADDING,
              left: rect.left - (diff + ANNOTATION_TEXT_PADDING) / 2,
            });
          } else if (rect.height * rect.scaleY < anText.height + ANNOTATION_TEXT_PADDING) {
            const diff = newHeight - rect.height;
            rect.set({
              height: rect.height + diff + ANNOTATION_TEXT_PADDING,
              top: rect.top - (diff + ANNOTATION_TEXT_PADDING) / 2,
            });
          }
          break;
        case 270:
          if (rect.height * rect.scaleY < anText.width + ANNOTATION_TEXT_PADDING) {
            const diff = newWidth - rect.height;
            rect.set({
              height: rect.height + diff + ANNOTATION_TEXT_PADDING,
              top: rect.top - (diff + ANNOTATION_TEXT_PADDING) / 2,
            });
          } else if (rect.width * rect.scaleX < anText.height + ANNOTATION_TEXT_PADDING) {
            const diff = newHeight - rect.width;
            rect.set({
              width: rect.width + diff + ANNOTATION_TEXT_PADDING,
              left: rect.left + (diff + ANNOTATION_TEXT_PADDING) / 2,
            });
          }
          break;
        default:
          if (rect.width * rect.scaleX < anText.width + ANNOTATION_TEXT_PADDING) {
            const diff = newWidth - rect.width;
            rect.set({
              width: rect.width + diff + ANNOTATION_TEXT_PADDING,
              left: rect.left + (diff + ANNOTATION_TEXT_PADDING) / 2,
            });
          } else if (rect.height * rect.scaleY < anText.height + ANNOTATION_TEXT_PADDING) {
            const diff = newHeight - rect.height;
            rect.set({
              height: rect.height + diff + ANNOTATION_TEXT_PADDING,
              top: rect.top + (diff + ANNOTATION_TEXT_PADDING) / 2,
            });
          }
          break;
      }
    }
  }

  static renderAnnotation({
    capture,
    annotation,
    canvas,
    scale,
    rotation,
    caution,
    selected,
  }: {
    capture: CapturedItem;
    annotation: fabric.Group;
    canvas: fabric.Canvas;
    scale: number;
    rotation: number;
    caution: boolean;
    selected: (Characteristic | Annotation)[];
  }) {
    // Unpack the Fabric object
    const annot = annotation.toObject() as Annotation;
    let lastItem = capture.annotation;

    // rotate the top left and bottom right corners of the rectangle, set the new top left point and dimensions
    const { newLeft, newTop, newWidth, newHeight } = getTransformedDimensions(annot.boxLocationX, annot.boxLocationY, annot.boxWidth, annot.boxHeight, canvas.getWidth(), canvas.getHeight(), scale, rotation);
    const anchorCoord = { left: newLeft, top: newTop, width: newWidth, height: newHeight };

    // clone the rectangle so we can update it without conflict
    const captureAnnotation = fabric.util.object.clone(capture.annotation);
    lastItem = FeatureUI.renderAnnotationText({ annotation: captureAnnotation, anchorCoord, boxRotation: annot.boxRotation, rotation: (360 + rotation - annot.drawingRotation) % 360, canvas });

    // Place balloon icons
    if (capture.balloon && annot.characteristic && !caution) {
      const charCoord = {
        x: annot.characteristic.markerLocationX,
        y: annot.characteristic.markerLocationY,
      };
      FeatureUI.renderFeatureMarker({ anchorCoord, charCoord, balloon: capture.balloon, canvas, rotation, markerRotation: annot.characteristic.markerRotation });
    }

    return selected &&
      selected.some((someChar) => {
        return someChar.id === annot?.id;
      })
      ? lastItem
      : undefined;
  }

  static reRenderAnnotationGroup({ objects, canvas, left, top }: { objects: fabric.Object[]; canvas: fabric.Canvas; left: number; top: number }) {
    const anText = objects.find((o) => o.type === 'annotation-text') as fabric.Textbox;
    if (!anText) return;
    const annotation = anText.toObject();
    const annotationGroup = new fabric.Group([], {
      left,
      top,
      originX: 'center',
      originY: 'center',
      selectable: true,
      type: 'annotation',
      hasControls: true,
      hasBorders: true,
      objectCaching: false,
      name: annotation.id,
      lockRotation: true,
    });
    objects.forEach((o) => {
      canvas.remove(o);
      annotationGroup.addWithUpdate(o);
    });
    anText?.bringToFront();
    annotationGroup.toObject = () => annotation;
    canvas.setActiveObject(annotationGroup);
    canvas.add(annotationGroup);
  }

  static renderFeatureLineToCapture({
    anchorCoord,
    charCoord,
    line,
    canvas,
    rotation,
    markerRotation,
  }: {
    anchorCoord: CaptureDimensions;
    charCoord: CaptureCoordinates;
    line: fabric.Line;
    canvas: fabric.Canvas;
    rotation: number;
    markerRotation: number;
  }) {
    const markerCartesian = getCartesianFromPolar({ x: anchorCoord.left, y: anchorCoord.top }, { x: charCoord.x, y: (charCoord.y + rotation) % 360 }, markerRotation);
    const newLine = line;
    // find distance between top, bottom, left, right of rect and balloon

    const x1 = markerCartesian.x + anchorCoord.width / 2;
    const y1 = markerCartesian.y + anchorCoord.height / 2;

    const newCoords = FeatureUI.shortestDistanceCoords({ x1, y1, width: anchorCoord.width, height: anchorCoord.height, top: anchorCoord.top, left: anchorCoord.left });
    newLine.set({
      x1: newCoords?.x1,
      y1: newCoords?.y1,
      x2: newCoords?.x2,
      y2: newCoords?.y2,
    });
    canvas.add(newLine);
    return newLine;
  }

  static renderFeatureLineToPoint({
    anchorCoord,
    charCoord,
    x2,
    y2,
    line,
    canvas,
    rotation,
    markerRotation,
  }: {
    anchorCoord: CaptureDimensions;
    charCoord: CaptureCoordinates;
    x2: number;
    y2: number;
    line: fabric.Line;
    canvas: fabric.Canvas;
    rotation: number;
    markerRotation: number;
  }) {
    const markerCartesian = getCartesianFromPolar({ x: anchorCoord.left, y: anchorCoord.top }, { x: charCoord.x, y: (charCoord.y + rotation) % 360 }, markerRotation);
    const newLine = line;
    // find distance between top, bottom, left, right of rect and balloon

    const x1 = markerCartesian.x + anchorCoord.width / 2;
    const y1 = markerCartesian.y + anchorCoord.height / 2;

    newLine.set({
      x1,
      y1,
      x2,
      y2,
    });
    canvas.add(newLine);
    return newLine;
  }

  static renderConnectionPoint({ x2, y2, connectionPoint, canvas }: { x2: number; y2: number; connectionPoint: fabric.Circle; canvas: fabric.Canvas }) {
    const newConnectionPoint = connectionPoint;
    newConnectionPoint.set({
      top: y2,
      left: x2,
    });
    canvas.add(newConnectionPoint);

    return newConnectionPoint;
  }

  // helper functions connection points and leader lines
  static shortestDistanceCoords({ x1, y1, left, top, width, height }: { x1: number; y1: number; width: number; height: number; left: number; top: number }) {
    // top
    const topX = x1 - (left + width / 2);
    const topY = y1 - top;

    const topDistance = Math.sqrt(topX * topX + topY * topY);

    // bottom
    const bottomX = x1 - (left + width / 2);
    const bottomY = y1 - (top + height);

    const bottomDistance = Math.sqrt(bottomX * bottomX + bottomY * bottomY);

    // left
    const leftX = x1 - left;
    const leftY = y1 - (top + height / 2);

    const leftDistance = Math.sqrt(leftX * leftX + leftY * leftY);

    // right
    const rightX = x1 - (left + width);
    const rightY = y1 - (top + height / 2);

    const rightDistance = Math.sqrt(rightX * rightX + rightY * rightY);

    const minDistance = Math.min(topDistance, bottomDistance, leftDistance, rightDistance);

    if (minDistance === topDistance) {
      return {
        x1,
        y1,
        x2: left + width / 2,
        y2: top,
      };
    }
    if (minDistance === bottomDistance) {
      return {
        x1,
        y1,
        x2: left + width / 2,
        y2: top + height,
      };
    }
    if (minDistance === leftDistance) {
      return {
        x1,
        y1,
        x2: left,
        y2: top + height / 2,
      };
    }
    if (minDistance === rightDistance) {
      return {
        x1,
        y1,
        x2: left + width,
        y2: top + height / 2,
      };
    }
    log.error('Error determining shortest coord distance');
    return null;
  }

  static shortestDistanceX2Y2({ x1, y1, left, top, width, height, rotation }: { x1: number; y1: number; width: number; height: number; left: number; top: number; rotation: number }) {
    // left and top are the center of the rectangle here
    if (rotation === 90) {
      const bottomX90 = left + width;
      const bottomXDiff90 = x1 - bottomX90;

      const bottomY90 = top - height / 2;
      const bottomYDiff90 = y1 - bottomY90;

      const bottomDistance90 = Math.sqrt(bottomXDiff90 * bottomXDiff90 + bottomYDiff90 * bottomYDiff90);

      const topX90 = left + width;
      const topXDiff90 = x1 - topX90;

      const topY90 = top + height / 2;
      const topYDiff90 = y1 - topY90;

      const topDistance90 = Math.sqrt(topXDiff90 * topXDiff90 + topYDiff90 * topYDiff90);

      const leftX90 = left - width / 2 + width;
      const leftXDiff90 = x1 - leftX90;

      const leftY90 = top;
      const leftYDiff90 = y1 - leftY90;

      const leftDistance90 = Math.sqrt(leftXDiff90 * leftXDiff90 + leftYDiff90 * leftYDiff90);

      const rightX90 = left + width / 2 + width;
      const rightXDiff90 = x1 - rightX90;

      const rightY90 = top;
      const rightYDiff90 = y1 - rightY90;

      const rightDistance90 = Math.sqrt(rightXDiff90 * rightXDiff90 + rightYDiff90 * rightYDiff90);

      const minDistance = Math.min(topDistance90, bottomDistance90, leftDistance90, rightDistance90);
      if (minDistance === bottomDistance90) {
        return {
          x2: bottomX90,
          y2: bottomY90,
          distance: bottomDistance90,
        };
      }
      if (minDistance === topDistance90) {
        return {
          x2: topX90,
          y2: topY90,
          distance: topDistance90,
        };
      }
      if (minDistance === leftDistance90) {
        return {
          x2: leftX90,
          y2: leftY90,
          distance: leftDistance90,
        };
      }
      if (minDistance === rightDistance90) {
        return {
          x2: rightX90,
          y2: rightY90,
          distance: rightDistance90,
        };
      }
    } else if (rotation === 180) {
      const bottomX180 = left + width;
      const bottomXDiff180 = x1 - bottomX180;

      const bottomY180 = top - height / 2 + height;
      const bottomYDiff180 = y1 - bottomY180;

      const bottomDistance180 = Math.sqrt(bottomXDiff180 * bottomXDiff180 + bottomYDiff180 * bottomYDiff180);

      const topX180 = left + width;
      const topXDiff180 = x1 - topX180;

      const topY180 = top + height / 2 + height;
      const topYDiff180 = y1 - topY180;

      const topDistance180 = Math.sqrt(topXDiff180 * topXDiff180 + topYDiff180 * topYDiff180);

      const leftX180 = left - width / 2 + width;
      const leftXDiff180 = x1 - leftX180;

      const leftY180 = top + height;
      const leftYDiff180 = y1 - leftY180;

      const leftDistance180 = Math.sqrt(leftXDiff180 * leftXDiff180 + leftYDiff180 * leftYDiff180);

      const rightX180 = left + width / 2 + width;
      const rightXDiff180 = x1 - rightX180;

      const rightY180 = top + height;
      const rightYDiff180 = y1 - rightY180;

      const rightDistance180 = Math.sqrt(rightXDiff180 * rightXDiff180 + rightYDiff180 * rightYDiff180);

      const minDistance = Math.min(topDistance180, bottomDistance180, leftDistance180, rightDistance180);
      if (minDistance === bottomDistance180) {
        return {
          x2: bottomX180,
          y2: bottomY180,
          distance: bottomDistance180,
        };
      }
      if (minDistance === topDistance180) {
        return {
          x2: topX180,
          y2: topY180,
          distance: topDistance180,
        };
      }
      if (minDistance === leftDistance180) {
        return {
          x2: leftX180,
          y2: leftY180,
          distance: leftDistance180,
        };
      }
      if (minDistance === rightDistance180) {
        return {
          x2: rightX180,
          y2: rightY180,
          distance: rightDistance180,
        };
      }
    } else if (rotation === 270) {
      const bottomX270 = left;
      const bottomXDiff270 = x1 - bottomX270;

      const bottomY270 = top - height / 2 + height;
      const bottomYDiff270 = y1 - bottomY270;

      const bottomDistance270 = Math.sqrt(bottomXDiff270 * bottomXDiff270 + bottomYDiff270 * bottomYDiff270);

      const topX270 = left;
      const topXDiff270 = x1 - topX270;

      const topY270 = top + height / 2 + height;
      const topYDiff270 = y1 - topY270;

      const topDistance270 = Math.sqrt(topXDiff270 * topXDiff270 + topYDiff270 * topYDiff270);

      const leftX270 = left - width / 2;
      const leftXDiff270 = x1 - leftX270;

      const leftY270 = top + height;
      const leftYDiff270 = y1 - leftY270;

      const leftDistance270 = Math.sqrt(leftXDiff270 * leftXDiff270 + leftYDiff270 * leftYDiff270);

      const rightX270 = left + width / 2;
      const rightXDiff270 = x1 - rightX270;

      const rightY270 = top + height;
      const rightYDiff270 = y1 - rightY270;

      const rightDistance270 = Math.sqrt(rightXDiff270 * rightXDiff270 + rightYDiff270 * rightYDiff270);

      const minDistance = Math.min(topDistance270, bottomDistance270, leftDistance270, rightDistance270);
      if (minDistance === bottomDistance270) {
        return {
          x2: bottomX270,
          y2: bottomY270,
          distance: bottomDistance270,
        };
      }
      if (minDistance === topDistance270) {
        return {
          x2: topX270,
          y2: topY270,
          distance: topDistance270,
        };
      }
      if (minDistance === leftDistance270) {
        return {
          x2: leftX270,
          y2: leftY270,
          distance: leftDistance270,
        };
      }
      if (minDistance === rightDistance270) {
        return {
          x2: rightX270,
          y2: rightY270,
          distance: rightDistance270,
        };
      }
    } else {
      const bottomX = left;
      const bottomXDiff = x1 - bottomX;

      const bottomY = top - height / 2;
      const bottomYDiff = y1 - bottomY;

      const bottomDistance = Math.sqrt(bottomXDiff * bottomXDiff + bottomYDiff * bottomYDiff);

      const topX = left;
      const topXDiff = x1 - topX;

      const topY = top + height / 2;
      const topYDiff = y1 - topY;

      const topDistance = Math.sqrt(topXDiff * topXDiff + topYDiff * topYDiff);

      const leftX = left - width / 2;
      const leftXDiff = x1 - leftX;

      const leftY = top;
      const leftYDiff = y1 - leftY;

      const leftDistance = Math.sqrt(leftXDiff * leftXDiff + leftYDiff * leftYDiff);

      const rightX = left + width / 2;
      const rightXDiff = x1 - rightX;

      const rightY = top;
      const rightYDiff = y1 - rightY;

      const rightDistance = Math.sqrt(rightXDiff * rightXDiff + rightYDiff * rightYDiff);

      const minDistance = Math.min(topDistance, bottomDistance, leftDistance, rightDistance);
      if (minDistance === bottomDistance) {
        return {
          x2: bottomX,
          y2: bottomY,
          distance: bottomDistance,
        };
      }
      if (minDistance === topDistance) {
        return {
          x2: topX,
          y2: topY,
          distance: topDistance,
        };
      }
      if (minDistance === leftDistance) {
        return {
          x2: leftX,
          y2: leftY,
          distance: leftDistance,
        };
      }
      if (minDistance === rightDistance) {
        return {
          x2: rightX,
          y2: rightY,
          distance: rightDistance,
        };
      }
    }
    log.error('Error determining shortest X2Y2 distance');
    return null;
  }

  // gets length between coords line
  static getLengthOfLine({ x1, y1, x2, y2 }: { x1: number; y1: number; x2: number; y2: number }) {
    const diffX = x2 - x1;
    const diffY = y2 - y1;
    const distance = Math.sqrt(diffX * diffX + diffY * diffY);

    return distance;
  }

  // Assignment
  static assignPartInformation({ feature, drawingRotation, drawingScale }: { feature: ParsedCharacteristic; drawingRotation: number; drawingScale: number }) {
    const assignedItem = { ...feature };

    assignedItem.verified = false;
    assignedItem.drawingRotation = drawingRotation;
    assignedItem.drawingScale = drawingScale;
    assignedItem.confidence = -1;
    assignedItem.status = 'Active';

    return assignedItem;
  }

  static assignCapturePosition({ feature, capture, rotation }: { feature: ParsedCharacteristic; capture: CaptureDimensions; rotation: number }) {
    const assignedItem = { ...feature };

    assignedItem.boxLocationY = capture.top;
    assignedItem.boxLocationX = capture.left;
    assignedItem.boxWidth = capture.width;
    assignedItem.boxHeight = capture.height;
    assignedItem.boxRotation = rotation;

    return assignedItem;
  }

  static assignMarkerPosition({
    item,
    drawingSheetMarkerSize,
    feature,
    capture,
    rotation,
    drawingSheetRotation,
  }: {
    item?: Characteristic;
    drawingSheetMarkerSize?: number | null;
    feature: ParsedCharacteristic;
    capture: CaptureDimensions;
    rotation: number;
    drawingSheetRotation: number;
  }) {
    const assignedItem = { ...feature };

    let fontSize = drawingSheetMarkerSize ? Math.round(drawingSheetMarkerSize / 2) : item?.markerFontSize;
    if (!fontSize) {
      fontSize = this.getFontSize({ height: capture.height, width: capture.width, rotation });
    }
    let markerSize = drawingSheetMarkerSize || item?.markerSize;
    if (!markerSize) {
      markerSize = Math.round(fontSize * 2);
    }
    assignedItem.markerLocationX = markerSize * 2; // place marker twice the width of the marker away
    assignedItem.markerLocationY = capture.left - markerSize * 2 < 0 ? 0 : 180; // degrees, places marker to the left if there's room, to the right if not
    assignedItem.markerFontSize = fontSize;
    assignedItem.markerSize = markerSize;
    assignedItem.markerRotation = drawingSheetRotation || 0;

    return assignedItem;
  }

  static assignMarkerLabel({ item, feature }: { item?: Characteristic; feature: ParsedCharacteristic }) {
    const assignedItem = { ...feature };

    const itemNumber = item?.markerGroup ? parseInt(item.markerGroup, 10) + 1 : 1;
    assignedItem.markerGroup = itemNumber.toString();
    assignedItem.markerLabel = itemNumber.toString();
    assignedItem.markerIndex = item && item.markerIndex >= 0 ? item.markerIndex + 1 : 0;
    assignedItem.markerGroupShared = false;
    assignedItem.markerSubIndex = -1;

    return assignedItem;
  }

  static assignMarkerStyles({
    feature,
    assignedStyles,
  }: {
    feature: ParsedCharacteristic;
    assignedStyles: {
      [key: string]: MarkerStyle;
    };
  }) {
    const assignedItem = { ...feature };

    let markerStyle = assignedStyles.default.style;
    if (assignedItem.notationType) {
      const type = assignedItem.notationType; // getListNameById({ list: types, value: assignedItem.notationType }); < was passing english text instead of UUID
      const subtype = assignedItem.notationSubtype; // getListNameById({ list: types, value: assignedItem.notationSubtype, parentId: assignedItem.notationType });
      if (type && subtype) {
        const { style } = this.getMarkerStyle({
          styleOptions: assignedStyles,
          type,
          subtype,
          classification: assignedItem.criticality || undefined,
          operation: assignedItem.operation || undefined,
          inspectionMethod: assignedItem.inspectionMethod || undefined,
        });
        markerStyle = style;
      }
    }
    assignedItem.markerStyle = markerStyle;
    return assignedItem;
  }

  static assignGridLocation({ feature, capture, grid, canvas, rotation }: { feature: ParsedCharacteristic; capture: CaptureDimensions; grid: Grid; canvas: fabric.Canvas; rotation: number }) {
    const updatedFeature = { ...feature };
    const boxCartesian = {
      x: capture.left + capture.width / 2,
      y: capture.top + capture.height / 2,
    };
    let balloon = boxCartesian;
    if (feature.markerLocationX && feature.markerLocationY) {
      balloon = getCartesianFromPolar(boxCartesian, { x: feature.markerLocationX, y: feature.markerLocationY }, feature.markerRotation || 0);
    }

    const { gridCoordinates, connectionPointGridCoordinates, balloonGridCoordinates } = gridPosition(balloon, capture, rotation, canvas, grid, feature as any);
    updatedFeature.gridCoordinates = gridCoordinates;
    updatedFeature.balloonGridCoordinates = balloonGridCoordinates;
    updatedFeature.connectionPointGridCoordinates = connectionPointGridCoordinates;

    return updatedFeature;
  }

  // Controllers
  static createFeature({
    canvas,
    drawingRotation,
    drawingScale,
    boxRotation,
    parsedFeature,
    lastItem,
    drawingSheetMarkerSize,
    drawingSheetRotation,
    capture,
    assignedStyles,
    grid,
  }: {
    canvas: fabric.Canvas;
    drawingRotation: number;
    drawingScale: number;
    boxRotation: number;
    parsedFeature: ParsedCharacteristic;
    lastItem?: Characteristic;
    drawingSheetMarkerSize?: number | null;
    drawingSheetRotation?: number;
    capture: CaptureDimensions;
    assignedStyles: {
      [key: string]: MarkerStyle;
    };
    grid: Grid;
  }) {
    let feature: ParsedCharacteristic = { ...parsedFeature };

    feature = this.assignPartInformation({ feature, drawingRotation, drawingScale });
    feature = this.assignCapturePosition({ feature, capture, rotation: boxRotation });
    feature = this.assignMarkerPosition({ item: lastItem, drawingSheetMarkerSize, feature, capture, rotation: boxRotation, drawingSheetRotation: drawingSheetRotation || 0 });
    feature = this.assignMarkerLabel({ item: lastItem, feature });
    feature = this.assignMarkerStyles({ feature, assignedStyles });
    feature = this.assignGridLocation({ feature, capture, grid, canvas, rotation: drawingRotation });

    return feature;
  }
}
