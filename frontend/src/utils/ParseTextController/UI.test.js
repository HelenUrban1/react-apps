import { fabric } from 'fabric';
import { getCircle, getRect, getConnectionPoint, getAnnotationObject } from 'domain/part/edit/characteristics/characteristicsRenderer';
import { getCartesianFromPolar } from 'domain/part/edit/characteristics/utils/coordTransforms';
import { Grid } from 'domain/part/edit/grid/Grid';
import { defaultAnnotation } from 'types/annotation';
import { FeatureUI } from './UI';
import { setupJestCanvasMock } from 'jest-canvas-mock';

const dummyFeature = {
  drawingSheetIndex: 1,
  captureMethod: 'Automated',
  status: 'Active',
  verified: false,
  notationType: 'placeholder', // Dimension
  notationSubtype: 'placeholder', // Curvilinear
  notationClass: 'Tolerance',
  fullSpecification: '1.25 +/- 0.1',
  quantity: 3,
  nominal: '1.25',
  upperSpecLimit: '1.26',
  lowerSpecLimit: '1.24',
  plusTol: '0.1',
  minusTol: '0.1',
  unit: 'placeholder',
  toleranceSource: 'Document_Defined',
  inspectionMethod: 'placeholder', // 'Document_Defined'
  criticality: null,
  notes: '',
  drawingRotation: 0.0,
  drawingScale: 1.0,
  boxLocationY: 510.7251086456913,
  boxLocationX: 202.30217210136476,
  boxWidth: 33.53615354307212,
  boxHeight: 15.562144712486088,
  boxRotation: 0.0,
  markerGroup: '1',
  markerGroupShared: false,
  markerIndex: 0,
  markerSubIndex: null,
  markerLabel: '1',
  markerStyle: 'aad19683-337a-4039-a910-98b0bdd5ee57',
  markerLocationX: 45,
  markerLocationY: 180,
  markerFontSize: 18,
  markerSize: 36,
  gridCoordinates: null,
  balloonGridCoordinates: null,
  connectionPointGridCoordinates: null,
  createdAt: 'placeholder',
  updatedAt: 'placeholder',
  partId: 'd6c5bc49-9fb2-4fbb-8a61-77bc8d28036f', // belongsTo
  drawingId: 'c200ce70-72b8-44f7-8d3f-4b93f6d1b491', // belongsTo
  drawingSheetId: 'd6c5bc49-9fb2-4fbb-8a61-77bc8d380352',
  connectionPointLocationX: 510.7251086456913,
  connectionPointLocationY: 202.30217210136476,
};

const types = {
  'db687ac1-7bf8-4ada-be2b-979f8921e1a0': {
    id: 'db687ac1-7bf8-4ada-be2b-979f8921e1a0',
    value: 'Dimension',
    default: true,
    meta: '',
    status: 'Active',
    listType: 'Type',
    index: 0,
    children: {
      'edc8ecda-5f88-4b81-871f-6c02f1c50afd': {
        id: 'edc8ecda-5f88-4b81-871f-6c02f1c50afd',
        value: 'Length',
        default: true,
        meta: { measurement: 'Length' },
        status: 'Active',
        listType: 'Type',
        index: 0,
        parentId: 'db687ac1-7bf8-4ada-be2b-979f8921e1a0',
      },
      'b2e1e0aa-0da4-4844-853a-f20971b013a4': {
        id: 'b2e1e0aa-0da4-4844-853a-f20971b013a4',
        value: 'Angle',
        default: true,
        meta: { measurement: 'Plane Angle' },
        status: 'Active',
        listType: 'Type',
        index: 1,
        parentId: 'db687ac1-7bf8-4ada-be2b-979f8921e1a0',
      },
      '49d45a6c-1073-4c6e-805f-8f4105641bf1': {
        id: '49d45a6c-1073-4c6e-805f-8f4105641bf1',
        value: 'Radius',
        default: true,
        meta: { measurement: 'Length' },
        status: 'Active',
        listType: 'Type',
        index: 2,
        parentId: 'db687ac1-7bf8-4ada-be2b-979f8921e1a0',
      },
      '4396f3c2-a915-4458-8cff-488b6d301ecd': {
        id: '4396f3c2-a915-4458-8cff-488b6d301ecd',
        value: 'Diameter',
        default: true,
        meta: { measurement: 'Length' },
        status: 'Active',
        listType: 'Type',
        index: 3,
        parentId: 'db687ac1-7bf8-4ada-be2b-979f8921e1a0',
      },
    },
  },
  'f3c4d8e0-fb79-4c72-a3f8-3f584ea075a7': {
    id: 'f3c4d8e0-fb79-4c72-a3f8-3f584ea075a7',
    value: 'Geometric Tolerance',
    default: true,
    meta: '',
    status: 'Active',
    listType: 'Type',
    index: 1,
    children: {
      'ea0e747f-9e70-4e2e-9a34-4491f6b63593': {
        id: 'ea0e747f-9e70-4e2e-9a34-4491f6b63593',
        value: 'Profile of a Surface',
        default: true,
        meta: { measurement: 'Length' },
        status: 'Active',
        listType: 'Type',
        index: 0,
        parentId: 'f3c4d8e0-fb79-4c72-a3f8-3f584ea075a7',
      },
      '19a7a9a1-76c5-45ef-8cf9-611ebe7cf207': {
        id: '19a7a9a1-76c5-45ef-8cf9-611ebe7cf207',
        value: 'Position',
        default: true,
        meta: { measurement: 'Length' },
        status: 'Active',
        listType: 'Type',
        index: 1,
        parentId: 'f3c4d8e0-fb79-4c72-a3f8-3f584ea075a7',
      },
      '5ce784e8-f614-4e6d-a79b-237b4dc751dd': {
        id: '5ce784e8-f614-4e6d-a79b-237b4dc751dd',
        value: 'Angularity',
        default: true,
        meta: { measurement: 'Plane Angle' },
        status: 'Active',
        listType: 'Type',
        index: 2,
        parentId: 'f3c4d8e0-fb79-4c72-a3f8-3f584ea075a7',
      },
    },
  },
  'fa3284ed-ad3e-43c9-b051-87bd2e95e927': {
    id: 'fa3284ed-ad3e-43c9-b051-87bd2e95e927',
    value: 'Note',
    default: true,
    meta: '',
    status: 'Active',
    listType: 'Type',
    index: 1,
    children: {
      '805fd539-8063-4158-8361-509e10a5d371': {
        id: '805fd539-8063-4158-8361-509e10a5d371',
        value: 'Note',
        default: true,
        meta: '',
        status: 'Active',
        listType: 'Type',
        index: 0,
        parentId: 'fa3284ed-ad3e-43c9-b051-87bd2e95e927',
      },
    },
  },
};

const assignedStyles = {
  default: { assign: ['style', 'default'], style: '123' },
  fakeClassification: { assign: ['style', 'fakeClassification'], style: '456' },
  fakeType: { assign: ['style', 'fakeType', 'fakeSubtype'], style: '789' },
  Length: { assign: ['style', 'db687ac1-7bf8-4ada-be2b-979f8921e1a0', 'edc8ecda-5f88-4b81-871f-6c02f1c50afd'], style: 'Length' },
};

const capture = { left: 0, top: 0, width: 20, height: 10 };

const parsedFeature = {
  notationType: 'db687ac1-7bf8-4ada-be2b-979f8921e1a0', // Dimension
  notationSubtype: 'edc8ecda-5f88-4b81-871f-6c02f1c50afd', // length
  fullSpecification: '2.99',
  nominal: '2.99',
  upperSpecLimit: '3.00',
  lowerSpecLimit: '2.98',
  plusTol: '0.01',
  minusTol: '0.01',
  toleranceSource: 'Default_Tolerance',
  markerLocationX: capture.left + capture.width / 2,
  markerLocationY: capture.top + capture.height / 2,
  markerRotation: 0,
  drawingSheetRotation: 0,
};

const canvas = new fabric.Canvas();
const rotation = 0;
// const grid = new Grid({
//   initialCanvasWidth: 1000,
//   initialCanvasHeight: 500,
//   lineWidth: 1,
//   labelColor: '#000000',
//   lineColor: '#ffffff',
//   columnLeftLabel: '1',
//   columnRightLabel: '5',
//   rowTopLabel: 'A',
//   rowBottomLabel: 'C',
//   skipLabels: [],
//   showLabels: true,
//   rowLabels: ['A', 'B', 'C'],
//   rowLineRatios: [0, 0.25, 0.5, 1],
//   rowLines: [0, 250, 500, 1000],
//   colLabels: ['1', '2', '3', '4', '5'],
//   colLineRatios: [0, 0.2, 0.4, 0.6, 0.8, 1],
//   colLines: [0, 100, 200, 300, 400, 500],
// });

const grid = new Grid({
  initialCanvasWidth: 1280,
  initialCanvasHeight: 720,
});
const testAssignedStyles = {
  default: { style: 'f12c5566-7612-48e9-9534-ef90b276ebaa', assign: ['Default'] },
  1: {
    style: 'f1386f09-9c03-4c59-ada9-acb6a16bfbab',
    assign: ['Type', '9f78f226-11b1-4e70-9705-440c0ef290bc', '4e6617aa-5f61-4257-8e18-787b80ddece1'],
  }, // surface finish, structure
  2: {
    style: 'f12c5566-7612-48e9-9534-ef90b276ebaa',
    assign: ['Classification', '168eba1e-a8bf-41bb-a0d2-955e8a7b8d66'],
  }, // Critical
  3: {
    style: 'c858d5fb-5fc2-41f3-b4a6-5aa09ec27307',
    assign: ['Classification', '187b5423-dafd-402a-b471-2ed2fe76d2f9'],
  }, // Major
  4: {
    style: 'd276af65-0938-4e25-aa28-6f12273574ce',
    assign: ['Classification', '502a8ba4-82a7-423a-a339-b8f358ba3d3a'],
  }, // Minor
  5: {
    style: 'aad19683-337a-4039-a910-98b0bdd5ee57',
    assign: ['Classification', '975a513d-394c-4123-901e-1244756b488e'],
  }, // Incidental
  6: {
    style: '8d1a8859-c1bc-4c90-b7c6-2bf1640856ac',
    assign: ['Type', 'db687ac1-7bf8-4ada-be2b-979f8921e1a0', '49d45a6c-1073-4c6e-805f-8f4105641bf1'],
  }, // Dim, Radius
};
describe('Feature UI', () => {
  beforeEach(() => {
    jest.resetAllMocks();
    setupJestCanvasMock();
  });
  describe('Utilities', () => {
    it('should adjust coordinates to document scale', () => {
      const tl = new fabric.Point(0, 0);
      const br = new fabric.Point(1, 1);
      const { left, top, width, height } = FeatureUI.scaleCaptureDimensions({ tl, br, scale: 0.5 });
      expect(left).toBe(0);
      expect(top).toBe(0);
      expect(width).toBe(2);
      expect(height).toBe(2);
    });
    it('determine if capture space already contains object', () => {
      const canvas = new fabric.Canvas();
      // rectangle from (1,1) to (2,2)
      const rect = getRect(1, 1, 'rgb(27, 98, 233, 0.25)', 'rgb(27, 98, 233, 0.25)', 0, 1, 1, 0, 'extract');
      canvas.add(rect);
      // selection from (0,0) to (5,5)
      const overlaps = FeatureUI.captureHasOverlap({ select: { x: 0, y: 0 }, pointer: { x: 5, y: 5 }, canvas });
      expect(overlaps).toBeTruthy();
      const canvas2 = new fabric.Canvas();
      // rectangle from (0,0) to (1,1)
      const rect2 = getRect(0, 0, 'rgb(27, 98, 233, 0.25)', 'rgb(27, 98, 233, 0.25)', 0, 1, 1, 0, 'extract');
      canvas2.add(rect2);
      // selection from (2,2) to (5,5)
      const overlaps2 = FeatureUI.captureHasOverlap({ select: { x: 2, y: 2 }, pointer: { x: 5, y: 5 }, canvas: canvas2 });
      expect(overlaps2).toBeFalsy();
    });
    it('determine the font size based on the capture size', () => {
      const fontSize = FeatureUI.getFontSize({ height: 20, width: 50, rotation: 90 });
      expect(fontSize).toBe(12);
    });
    it('determine the marker style based on the type of capture', () => {
      let returned = FeatureUI.getMarkerStyle({ styleOptions: assignedStyles, type: 'fakeType', subtype: 'fakeSubtype' });
      expect(returned.style).toBe('789');
      expect(returned.matched).toBeTruthy();
      returned = FeatureUI.getMarkerStyle({ styleOptions: assignedStyles, type: 'fakeType', subtype: 'fakeSubtype', classification: 'fakeClassification' });
      expect(returned.style).toBe('456');
      expect(returned.matched).toBeTruthy();
      returned = FeatureUI.getMarkerStyle({ styleOptions: assignedStyles, type: 'someType', subtype: 'fakeSubtype' });
      expect(returned.style).toBe('123');
      expect(returned.matched).toBeFalsy();
    });
    it('determine the top left and bottom right corners of the selected capture area', () => {
      const tl = new fabric.Point(0, 0);
      const br = new fabric.Point(1, 1);
      const corners = FeatureUI.getCaptureCorners({ select: { x: 0, y: 0 }, pointer: { x: 1, y: 1 } });
      expect(corners.tl).toMatchObject(tl);
      expect(corners.br).toMatchObject(br);
    });

    it('Returns the default marker style if no assignments are matched', () => {
      const styleOne = FeatureUI.getMarkerStyle({ styleOptions: testAssignedStyles, type: '', subtype: '' });
      const styleTwo = FeatureUI.getMarkerStyle({ styleOptions: testAssignedStyles, type: 'db687ac1-7bf8-4ada-be2b-979f8921e1a0', subtype: '4396f3c2-a915-4458-8cff-488b6d301ecd' }); // Dim, Diameter
      expect(styleOne.style).toBe('f12c5566-7612-48e9-9534-ef90b276ebaa');
      expect(styleTwo.style).toBe('f12c5566-7612-48e9-9534-ef90b276ebaa');
    });

    it('Returns the correct MarkerStyle', () => {
      const styleOne = FeatureUI.getMarkerStyle({ styleOptions: testAssignedStyles, type: '9f78f226-11b1-4e70-9705-440c0ef290bc', subtype: '4e6617aa-5f61-4257-8e18-787b80ddece1' }); // Surface, Structure
      const styleTwo = FeatureUI.getMarkerStyle({ styleOptions: testAssignedStyles, type: '', subtype: '', classification: '168eba1e-a8bf-41bb-a0d2-955e8a7b8d66' }); // Critical
      const styleThree = FeatureUI.getMarkerStyle({ styleOptions: testAssignedStyles, type: 'db687ac1-7bf8-4ada-be2b-979f8921e1a0', subtype: '49d45a6c-1073-4c6e-805f-8f4105641bf1' }); // Dim, Radius
      const styleFour = FeatureUI.getMarkerStyle({ styleOptions: testAssignedStyles, type: 'db687ac1-7bf8-4ada-be2b-979f8921e1a0', subtype: '49d45a6c-1073-4c6e-805f-8f4105641bf1', classification: '502a8ba4-82a7-423a-a339-b8f358ba3d3a' }); // Dim, Radius, Minor

      expect(styleOne.style).toBe('f1386f09-9c03-4c59-ada9-acb6a16bfbab');
      expect(styleTwo.style).toBe('f12c5566-7612-48e9-9534-ef90b276ebaa');
      expect(styleThree.style).toBe('8d1a8859-c1bc-4c90-b7c6-2bf1640856ac');
      expect(styleFour.style).toBe('d276af65-0938-4e25-aa28-6f12273574ce');
    });

    it('Returns the distance of the line', () => {
      const distance = FeatureUI.getLengthOfLine({ x1: 0, y1: 0, x2: 10, y2: 10 }); // Surface, Structure
      expect(distance).toBe(Math.sqrt(200));
    });
  });

  describe('Fabric Rendering', () => {
    it('should add a feature marker to the canvas ', () => {
      const canvas = new fabric.Canvas();
      const charCoord = { x: dummyFeature.markerLocationX, y: dummyFeature.markerLocationY };
      const anchorCoord = { x: dummyFeature.boxLocationX, y: dummyFeature.boxLocationY };
      const markerCartesian = getCartesianFromPolar(anchorCoord, charCoord);
      const balloon = getCircle(markerCartesian.x, markerCartesian.y, '#E56A54', '#E56A54', dummyFeature.markerFontSize / 10, dummyFeature.markerFontSize);
      const balloonGroup = new fabric.Group([balloon]);
      const newBalloon = FeatureUI.renderFeatureMarker({ anchorCoord, charCoord, balloon: balloonGroup, canvas });
      expect(newBalloon).not.toBeNull();
      const canvasBalloon = canvas.getObjects();
      expect(canvasBalloon).toHaveLength(1);
      expect(canvasBalloon[0]).toMatchObject(newBalloon);
      expect(canvasBalloon[0]).toMatchObject(balloonGroup);
    });
    it('should add a feature capture to the canvas', () => {
      const canvas = new fabric.Canvas();
      const charCoord = { x: dummyFeature.markerLocationX, y: dummyFeature.markerLocationY };
      const anchorCoord = { x: dummyFeature.boxLocationX, y: dummyFeature.boxLocationY };
      const rectangle = getRect(anchorCoord.x, anchorCoord.y, 'rgb(27, 98, 233, 0.25)', 'rgb(27, 98, 233, 0.25)', 1, dummyFeature.boxWidth, dummyFeature.boxHeight, dummyFeature.boxRotation, 'extract');
      const newRect = FeatureUI.renderFeatureCapture({ anchorCoord, charCoord, boxRotation: dummyFeature.boxRotation, rectangle, canvas });
      expect(newRect).not.toBeNull();
      const canvasRect = canvas.getObjects();
      expect(canvasRect).toHaveLength(1);
      expect(canvasRect[0]).toMatchObject(newRect);
      expect(canvasRect[0]).toMatchObject(rectangle);
    });

    it('should add a connection point to the canvas ', () => {
      const canvas = new fabric.Canvas();
      const connectionPoint = getConnectionPoint(dummyFeature.connectionPointLocationX, dummyFeature.connectionPointLocationY, '#E56A54');
      const newConnectionPoint = FeatureUI.renderConnectionPoint({ x2: dummyFeature.connectionPointLocationX, y2: dummyFeature.connectionPointLocationY, connectionPoint, canvas });
      expect(newConnectionPoint).not.toBeNull();
      const canvasConnectionPoint = canvas.getObjects();
      expect(canvasConnectionPoint).toHaveLength(1);
      expect(canvasConnectionPoint[0]).toMatchObject(newConnectionPoint);
      expect(canvasConnectionPoint[0]).toMatchObject(connectionPoint);
    });

    it('should add an annotation to the canvas', () => {
      const canv = new fabric.Canvas();
      const annotation = { id: 'fake-id', ...defaultAnnotation, annotation: 'Testing This<WRAP>with some<NEWLINE>new lines and wraps<WRAP>wrapping around for<NEWLINE>newlines' };
      const annotationObject = getAnnotationObject(annotation);
      FeatureUI.renderAnnotation({ capture: annotationObject, annotation: annotationObject.annotation, canvas: canv, scale: 1, rotation: 0, caution: false, selected: [] });
      const canvasAnnotation = canv.getObjects();
      expect(canvasAnnotation).toHaveLength(1);
      const annot = canvasAnnotation[0].toObject();
      expect(annot.id).toBe(annotation.id);
      const textObj = canvasAnnotation[0]._objects.find((o) => o.type === 'annotation-text');
      expect(textObj.text).toBe('Testing This with some\nnew lines and wraps wrapping around for\nnewlines');
    });

    it('should regroup an annotation after editing', () => {
      const canv = new fabric.Canvas();
      const annotation = { id: 'fake-id', ...defaultAnnotation };
      const rect = new fabric.Rect();
      const anText = new fabric.Object({
        type: 'annotation-text',
        toObject: () => {
          return annotation;
        },
      });
      canv.add(rect);
      canv.add(anText);
      FeatureUI.reRenderAnnotationGroup({ objects: [rect, anText], canvas: canv, left: annotation.left, top: annotation.top });
      const objects = canv.getObjects();
      expect(objects).toHaveLength(1);
      expect(objects[0].toObject().id).toBe(annotation.id);
    });

    it('should expand an annotation when text exceeds bounds - 0 degrees rotation', () => {
      const canv = new fabric.Canvas();
      const annotation = { id: 'fake-id', ...defaultAnnotation };
      const rect = new fabric.Rect({ left: 200, top: 200, height: 20, width: 20, type: 'rect' });
      const anText = new fabric.Rect('really long anText test examples should expand the rect', { toObject: () => annotation });
      canv.add(rect);
      canv.add(anText);
      FeatureUI.renderTextboxAfterEdit({ rect, anText, rotation: 0 });
      const objects = canv.getObjects();
      const renderedRect = objects.find((r) => r.type === 'rect');
      expect(renderedRect.width).toBeGreaterThanOrEqual(anText.width);
      expect(renderedRect.height).toBe(rect.height);
      expect(renderedRect.left).toBe(rect.left);
      expect(renderedRect.top).toBe(rect.top);
    });

    it('should expand an annotation when text exceeds bounds - 90 degrees rotation', () => {
      const canv = new fabric.Canvas();
      const annotation = { id: 'fake-id', ...defaultAnnotation, drawingRotation: 90 };
      const rect = new fabric.Rect({ left: 200, top: 200, height: 20, width: 20, type: 'rect' });
      const anText = new fabric.Rect('really long anText test examples should expand the rect', { toObject: () => annotation });
      canv.add(rect);
      canv.add(anText);
      FeatureUI.renderTextboxAfterEdit({ rect, anText, rotation: 0 });
      const objects = canv.getObjects();
      const renderedRect = objects.find((r) => r.type === 'rect');
      expect(renderedRect.width).toBe(rect.width);
      expect(renderedRect.height).toBeGreaterThanOrEqual(anText.width);
      expect(renderedRect.left).toBe(rect.left);
      expect(renderedRect.top).toBe(rect.top);
    });
  });

  describe('Assignment', () => {
    it('should assign general information to the feature', () => {
      const newFeature = FeatureUI.assignPartInformation({ feature: parsedFeature, drawingRotation: 0, drawingScale: 1 });
      expect(newFeature).toMatchObject({ ...parsedFeature, verified: false, drawingRotation: 0, drawingScale: 1, confidence: -1, status: 'Active' });
    });
    it('should assign capture positioning information to the feature', () => {
      const newFeature = FeatureUI.assignCapturePosition({ feature: parsedFeature, capture, rotation: 0 });
      expect(newFeature).toMatchObject({ ...parsedFeature, boxLocationY: 0, boxLocationX: 0, boxWidth: 20, boxHeight: 10, boxRotation: 0 });
    });
    it('should assign marker positioning information to the feature', () => {
      const newFeature = FeatureUI.assignMarkerPosition({ feature: parsedFeature, capture, rotation: 0, drawingSheetRotation: 0 });
      expect(newFeature).toMatchObject({
        ...parsedFeature,
        markerLocationX: 32,
        markerLocationY: 0,
        markerFontSize: 8,
      });
    });
    it('should assign marker label information to the feature', () => {
      const newFeature = FeatureUI.assignMarkerLabel({ feature: parsedFeature });
      expect(newFeature).toMatchObject({
        ...parsedFeature,
        markerGroup: '1',
        markerLabel: '1',
        markerIndex: 0,
        markerGroupShared: false,
        markerSubIndex: -1,
      });
    });
    it('should assign the correct markerIndex based on the previous feature', () => {
      const newFeature = FeatureUI.assignMarkerLabel({ item: { ...dummyFeature }, feature: parsedFeature });
      expect(newFeature).toMatchObject({
        ...parsedFeature,
        markerGroup: '2',
        markerLabel: '2',
        markerIndex: 1,
        markerGroupShared: false,
        markerSubIndex: -1,
      });
    });
    it('should assign marker styles to the feature', () => {
      const newFeature = FeatureUI.assignMarkerStyles({ feature: parsedFeature, assignedStyles, types });
      expect(newFeature).toMatchObject({ ...parsedFeature, markerStyle: 'Length' });
    });
    it('should assign the grid location to the feature', () => {
      // set coords to match 4B
      const newCapture = { left: 45, top: 45, width: 10, height: 10 };
      const feature = {
        ...parsedFeature,
        boxLocationY: newCapture.top,
        boxLocationX: newCapture.left,
        boxWidth: newCapture.width,
        boxHeight: newCapture.height,
        boxRotation: 0,
        markerLocationX: newCapture.left + newCapture.width / 2,
        markerLocationY: newCapture.top + newCapture.height / 2,
      };
      const newFeature = FeatureUI.assignGridLocation({ feature, capture: newCapture, grid, canvas, rotation });
      expect(newFeature).toMatchObject({ ...feature, gridCoordinates: '4B', balloonGridCoordinates: '4B', connectionPointGridCoordinates: 'External' }); // top left
    });
  });

  describe('Controllers', () => {
    it('should assign remaining properties to a parsed capture for creating a new feature', () => {
      const newFeature = FeatureUI.createFeature({ drawingRotation: 0, drawingScale: 1, boxRotation: 0, parsedFeature, capture, assignedStyles, grid, canvas });
      expect(newFeature).toMatchObject({
        ...parsedFeature,
        verified: false,
        drawingRotation: 0,
        drawingScale: 1,
        confidence: -1,
        status: 'Active',
        boxLocationY: 0,
        boxLocationX: 0,
        boxWidth: 20,
        boxHeight: 10,
        boxRotation: 0,
        markerLocationX: 32,
        markerLocationY: 0,
        markerFontSize: 8,
        markerGroup: '1',
        markerLabel: '1',
        markerIndex: 0,
        markerGroupShared: false,
        markerSubIndex: -1,
        markerStyle: 'Length',
      });
    });
  });
});
