import log from 'modules/shared/logger';
import { Characteristics } from 'graphql/characteristics';
import { Characteristic } from 'types/characteristics';
import { Tolerance } from 'types/tolerances';
import { ListObject } from 'domain/setting/listsTypes';
import Analytics from 'modules/shared/analytics/analytics';
import { MarkerStyle } from 'types/marker';
import { Grid } from 'domain/part/edit/grid/Grid';
import { getListMeasurementById, getListNameById } from 'utils/Lists';
import { sanitizedTextComparison } from 'utils/textOperations';
import { unFrameText } from 'utils/characters';
import { Tolerancer } from './Tolerancer';
import { Classifier } from './Classifier';
import { FeatureUI } from './UI';
import { ParsedCharacteristic, OCRResults, CaptureDimensions } from './types';
// @ts-ignore
import { sanitize, sequence, interpret } from '@ideagen/engineering-notation-parser';

export class ParseTextController {
  // Utilities
  static typeHasChanged(change: ParsedCharacteristic) {
    return change.notationType || change.notationSubtype || change.notationClass || change.gdtSymbol;
  }

  static toleranceHasChanged(change: ParsedCharacteristic, feature?: ParsedCharacteristic) {
    if (!feature) {
      return true;
    }
    return change.notationType || change.notationSubtype || change.nominal || change.plusTol || change.minusTol || change.gdtPrimaryToleranceZone || change.gdtSecondaryToleranceZone || !feature.upperSpecLimit || !feature.lowerSpecLimit;
  }

  static shouldUpdateToleranceSource(change: ParsedCharacteristic, feature?: ParsedCharacteristic) {
    if (!feature) {
      return true;
    }
    return change.plusTol !== feature.plusTol || change.minusTol !== feature.minusTol || change.upperSpecLimit !== feature.upperSpecLimit || change.lowerSpecLimit !== feature.lowerSpecLimit;
  }

  static checkForParsingErrors(error: any, parsed: any, captureError?: any) {
    // Check for Network failure
    if (error === 'Error: Network Error' || error === 'Error: Request failed with status code 404') {
      throw new Error('Network Error');
    }

    // Lexing / Parsing failure
    if (error?.name === 'Lexing Errors' || error?.name === 'Parsing Errors' || parsed?.notation?.type === 'unknown') {
      throw new Error('Saved as a Note');
    }
    return captureError || '';
  }

  static async interpretNotationText(text: string) {
    return interpret(sequence(sanitize(text)));
  }

  static async compareOCRParseResults(ocrRes: OCRResults, textRes?: OCRResults) {
    // Track how long it takes for events to occur
    const startTime = performance.now();
    const performanceLog: [string, number][] = [];
    let parsed: any = null;
    let sanitizedText;
    let responseError;
    let captureError = null;
    let successfulModel = 'ocr';
    let tessTextResult;
    let pytorchTextResult;
    let tessOcrResult;
    let pytorchOcrResult;
    let pytorchTextResponse;
    let tessTextResponse;
    let pytorchOcrResponse;
    let tessOcrResponse;

    // console.log({
    //   'ocrRes.ocr': ocrRes.ocr,
    //   'ocrRes.tess': ocrRes.tess,
    //   'textRes.ocr': textRes?.ocr,
    //   'textRes.tess': textRes?.tess,
    // });

    try {
      // Handle failure of the OCR service to extract text from the image
      if (!ocrRes || ocrRes.error || ocrRes.ocr.length === 0) {
        if (ocrRes.error) responseError = ocrRes.error;
        if (textRes?.error) responseError = textRes.error;
        // textRes will come back as success for empty captures so we could use that in the future for error handling
        if (ocrRes.error?.message === 'Network Error' && textRes?.error?.message === 'Network Error') {
          throw new Error('Network Error');
        } else {
          throw new Error('Empty Capture');
        }
      }

      // set up results objects for ocr
      pytorchOcrResult = {
        raw: ocrRes.ocr,
        sanitized: ocrRes.ocr.replace(/^\n|\n$/g, ''), // TODO: sanitization should be handled by the notation parser
      };
      tessOcrResult = {
        raw: ocrRes.tess,
        sanitized: ocrRes.tess.replace(/^\n|\n$/g, ''), // TODO: sanitization should be handled by the notation parser
      };

      if (textRes) {
        // if text is needed set up those objects and run the notation parser.
        pytorchTextResult = {
          raw: textRes.ocr,
          sanitized: textRes.ocr.replace(/^\n|\n$/g, ''), // TODO: sanitization should be handled by the notation parser
        };

        tessTextResult = {
          raw: textRes.tess,
          sanitized: textRes.tess.replace(/^\n|\n$/g, ''), // TODO: sanitization should be handled by the notation parser
        };
        
        [pytorchTextResponse, tessTextResponse, pytorchOcrResponse, tessOcrResponse] = await Promise.all([
          this.interpretNotationText(pytorchTextResult?.sanitized),
          this.interpretNotationText(tessTextResult?.sanitized),
          this.interpretNotationText(pytorchOcrResult.sanitized),
          this.interpretNotationText(tessOcrResult.sanitized),
        ]);
      } else {
        // run the plain notation parser with just ocr
        [pytorchOcrResponse, tessOcrResponse] = await Promise.all([this.interpretNotationText(pytorchOcrResult.sanitized), this.interpretNotationText(tessOcrResult.sanitized)]);
      }

      // console.log({
      //   pytorchTextResponse,
      //   tessTextResponse,
      //   pytorchOcrResponse,
      //   tessOcrResponse,
      // });

      // Parse text from tesseract result
      performanceLog.push(['interpretNotationText.ocr', performance.now() - startTime]);
      if (pytorchTextResponse && !pytorchTextResponse.error) {
        // if pytorch text response doesn't fail pass it on
        sanitizedText = pytorchTextResult?.sanitized;
        // Parsing succeded, so assign the response to parsed
        parsed = pytorchTextResponse;
        successfulModel = 'ocrText';
      } else if (tessTextResponse && !tessTextResponse.error) {
        // if tess text response doesn't fail pass it on
        sanitizedText = tessTextResult?.sanitized;
        // Parsing succeded, so assign the response to parsed
        parsed = tessTextResponse;
        successfulModel = 'tessText';
      } else if (pytorchOcrResponse && !pytorchOcrResponse.error) {
        // if pytorch dimension response doesn't fail pass it on
        sanitizedText = pytorchOcrResult.sanitized;
        // Parsing succeded, so assign the response to parsed
        parsed = pytorchOcrResponse;
        successfulModel = 'ocrDimension';
      } else {
        // last one to check is tessOcrResponse.
        // if this fails handle the errors
        // Check if both models failed to produce a notation, throw an error so capture will be treated as a Note
        if (tessOcrResponse.error) responseError = tessOcrResponse.error;

        // Check for Network failure
        this.checkForParsingErrors(tessOcrResponse.error, tessOcrResponse);
        sanitizedText = tessOcrResult.sanitized;
        // Parsing succeded, so assign the response to parsed
        parsed = tessOcrResponse;
        successfulModel = 'tessDimension';
      }
    } catch (err: any) {
      log.warn('ParseTextController.compareOCRParseResults', responseError, err);
      performanceLog.push(['error', performance.now() - startTime]);

      successfulModel = 'none';

      const errTips: any = {
        'Empty Capture': 'No text was found, adjust the capture box to try again.',
        'Notation Interpreter requires text as input': 'No text was found, adjust the capture box to try again.',
        'Saved as a Note': "We couldn't recognize this feature type, it's been saved as a Note.",
        'Network Error': 'Our network had an issue, adjust the capture box to try again.',
      };
      captureError = JSON.stringify({
        message: err.message,
        description: errTips[err.message],
      });

      // Convert the performance events
      const perfLogProperties = Analytics.perfLogToProperties(performanceLog);

      // Record result of adding items
      Analytics.track({
        event: Analytics.events.compareOCRModelsError,
        properties: {
          'ocr.used': successfulModel,
          'ocr.tess': tessOcrResult?.sanitized || '',
          'ocr.ocr': pytorchOcrResult?.sanitized || '',
          'text.tess': tessTextResult?.sanitized || '',
          'text.ocr': pytorchTextResult?.sanitized || '',
          'error.cause': responseError?.name || responseError, // May need to extract responseError.name or responseError.message
          responseError,
          error: err,
          parsed,
          ...perfLogProperties,
        },
      });

      // If an error occurred, send back
      // if sanitized text exists then we can use that. Not sure what edge case this hits but it was here before so I'll leave it.
      if (sanitizedText) {
        parsed = {
          input: sanitizedText,
          notation: {
            type: 'note', // TODO: Should a null feature default to a note or dimension?
            text: sanitizedText,
          },
          error: responseError,
        };
      }
      // if sanitized text is undefined and the text response was called (!isGdtOrBasic)
      // return the text input. This is usually correct, it just fails parsing.
      else if (!sanitizedText && pytorchTextResponse?.input) {
        parsed = {
          input: pytorchTextResponse.input,
          notation: {
            type: 'note', // TODO: Should a null feature default to a note or dimension?
            text: pytorchTextResponse.input,
          },
          error: responseError,
        };
      }
      // if the text response doesn't exist then it is gdt or basic so use that model
      else {
        parsed = {
          input: pytorchOcrResponse?.input,
          notation: {
            type: 'note', // TODO: Should a null feature default to a note or dimension?
            text: pytorchOcrResponse?.input,
          },
          error: responseError,
        };
      }
    }

    performanceLog.push(['finished', performance.now() - startTime]);
    const perfLogProperties = Analytics.perfLogToProperties(performanceLog);

    // Record result of adding items
    Analytics.track({
      event: Analytics.events.manualCapturePerformed,
      properties: {
        'ocr.used': successfulModel,
        parsed,
        'ocr.tess': tessOcrResult?.sanitized || '',
        'ocr.ocr': pytorchOcrResult?.sanitized || '',
        'text.tess': tessTextResult?.sanitized || '',
        'text.ocr': pytorchTextResult?.sanitized || '',
        ...perfLogProperties,
      },
    });
    return { parsed, ocrRes: ocrRes.ocr, captureError };
  }

  static buildDimensionFullSpecification(feature: ParsedCharacteristic) {
    const newFeature = { ...feature };
    let fullSpecification = '';
    if (newFeature.quantity && newFeature.quantity > 1) {
      fullSpecification += `${newFeature.quantity}X `;
    }
    fullSpecification += newFeature.nominal;
    if (newFeature?.toleranceSource !== 'Default_Tolerance') {
      if (newFeature?.plusTol && newFeature?.plusTol === newFeature?.minusTol) {
        fullSpecification += `±${newFeature.plusTol}`;
      } else {
        if (newFeature.plusTol) fullSpecification += newFeature.plusTol;
        if (newFeature.minusTol) fullSpecification += newFeature.minusTol;
      }
    }

    newFeature.fullSpecification = fullSpecification;
    return newFeature;
  }

  // Check change types
  static hasToleranceChanged(change: ParsedCharacteristic) {
    return change.fullSpecification || change.nominal || change.plusTol || change.minusTol || change.upperSpecLimit || change.lowerSpecLimit || change.toleranceSource || change.notationClass || change.gdtPrimaryToleranceZone;
  }

  // Updates
  static updateFeatureTolerances({ change, oldFeature, types, tolerances, isGDT }: { change: ParsedCharacteristic; oldFeature: ParsedCharacteristic; types: ListObject; tolerances: Tolerance; isGDT: boolean }) {
    let feature = { ...oldFeature };
    if (change.plusTol || change.minusTol || change.upperSpecLimit || change.lowerSpecLimit) {
      feature.toleranceSource = 'Manually_Set';
      if ((change.plusTol || change.minusTol) && !(change.upperSpecLimit || change.lowerSpecLimit)) {
        feature.upperSpecLimit = undefined;
        feature.lowerSpecLimit = undefined;
      }
    }
    if (change.toleranceSource) {
      feature.toleranceSource = change.toleranceSource;
    }

    if (feature.toleranceSource !== 'Manually_Set') {
      // reset tolerances to defaults
      feature.plusTol = '';
      feature.minusTol = '';
      feature.upperSpecLimit = undefined;
      feature.lowerSpecLimit = undefined;
    }
    const hasChanged = this.toleranceHasChanged(change, feature);
    const missingLimit = (!feature.upperSpecLimit || feature.upperSpecLimit === '') && (!feature.lowerSpecLimit || feature.lowerSpecLimit === '');

    if (hasChanged || missingLimit) {
      const measurement = getListMeasurementById({
        list: types,
        value: feature.notationSubtype,
        parentId: feature.notationType,
      });
      Analytics.track({
        event: Analytics.events.featureUpdateTolerances,
        properties: {
          characteristic: feature,
          change,
          tolerances,
          measurement,
        },
      });

      const temp = Tolerancer.assignFeatureTolerances({ feature: { plusTol: '', minusTol: '', ...feature, upperSpecLimit: '', lowerSpecLimit: '', ...change }, tolerances, measurement, isGDT });
      feature = { ...feature, ...change };
      feature.nominal = temp.nominal;
      feature.plusTol = temp.plusTol;
      feature.minusTol = temp.minusTol;
      feature.toleranceSource = temp.toleranceSource;

      if (missingLimit) {
        feature.upperSpecLimit = temp.upperSpecLimit;
        feature.lowerSpecLimit = temp.lowerSpecLimit;
      }
    }
    if (change.plusTol || change.minusTol) {
      feature.toleranceSource = 'Manually_Set';
    }
    return feature;
  }

  static updateFeatureUnit({ oldFeature, measurement, units, tolerances }: { oldFeature: ParsedCharacteristic; measurement: string; units: ListObject; tolerances: Tolerance }) {
    const feature = { ...oldFeature };
    let unit = '';

    if (sanitizedTextComparison(measurement, 'Length')) {
      // get the part's default linear unit
      unit = tolerances.linear.unit;
    } else if (sanitizedTextComparison(measurement, 'Plane Angle')) {
      // get the part's default angular unit
      unit = tolerances.angular.unit;
    } else {
      // if measurement isn't linear or angular, grab the first unit of that measurement type
      Object.keys(units).forEach((key) => {
        if (sanitizedTextComparison(units[key].meta?.measurement || '', measurement) && !unit) unit = key;
      });
    }
    feature.unit = unit;

    return feature;
  }

  static updateFeatureType({ change, oldFeature, types, tolerances, units }: { change: ParsedCharacteristic; oldFeature: ParsedCharacteristic; types: ListObject; tolerances: Tolerance; units?: ListObject }) {
    let feature = { ...oldFeature };
    const changes = { ...change };
    feature = { ...feature, ...change };
    const type = getListNameById({ list: types, value: feature.notationType });
    if (this.typeHasChanged(change) || type === 'Geometric Tolerance') {
      const subType = getListNameById({ list: types, value: feature.notationSubtype, parentId: feature.notationType });
      Analytics.track({
        event: Analytics.events.featureUpdateTypes,
        properties: {
          characteristic: feature,
          change,
          subType,
          type,
        },
      });
      if (!changes.fullSpecification && feature.notationType === change.notationType) {
        changes.fullSpecification = feature.fullSpecification;
      }
      switch (type) {
        case 'Geometric Tolerance':
          changes.gdtSymbol = Classifier.getGDTSymbol(subType);
          changes.gdtPrimaryToleranceZone = feature.gdtPrimaryToleranceZone || feature.nominal;
          feature = Classifier.updateGDT({ feature, change: changes });
          break;
        case 'Dimension':
          changes.nominal = change.nominal || feature.gdtPrimaryToleranceZone;
          feature = Classifier.updateDimension({ feature, change: changes });
          break;
        default:
          log.debug(`Feature type "${type}" was not Dimension or GDT.`);
          break;
      }

      let measurement = '';
      if (types[feature.notationType || '']?.children && types[feature.notationType || ''].children![feature.notationSubtype || '']?.meta) {
        measurement = types[feature.notationType || ''].children![feature.notationSubtype || ''].meta!.measurement;
      }
      // only update unit if the measurement type changed
      feature = this.updateFeatureUnit({ oldFeature: feature, measurement, units: units || {}, tolerances });
    }
    return feature;
  }

  static async parseNewTolerancesFromFullSpec({
    fullSpecification,
    ocrRes,
    textRes,
    oldFeature,
    tolerances,
    types,
  }: {
    fullSpecification?: string;
    ocrRes?: OCRResults;
    textRes?: OCRResults;
    oldFeature?: Characteristic;
    tolerances: Tolerance;
    types: ListObject;
  }) {
    // Parse Notation
    let parsed;
    if (fullSpecification) {
      Analytics.track({
        event: Analytics.events.parseFullSpecification,
        properties: {
          oldFullSpec: fullSpecification,
          newFullSpec: oldFeature?.fullSpecification,
          characteristic: oldFeature,
        },
      });
      // re-parse updated full specification
      parsed = await this.interpretNotationText(fullSpecification);
    } else if (ocrRes) {
      Analytics.track({
        event: Analytics.events.parseOCR,
        properties: {
          'ocr.tess': ocrRes.tess,
          'ocr.ocr': ocrRes.ocr,
        },
      });
      // parse OCR results, choosing the best model
      const obj = await this.compareOCRParseResults(ocrRes, textRes);
      parsed = obj.parsed;
    } else {
      Analytics.track({
        event: Analytics.events.featureParsingError,
        properties: {
          errors: 'Cannot parse feature without OCR result or new full specification',
        },
      });
      log.error('Cannot parse feature without OCR result or new full specification'); // TODO: i18n
      return null;
    }
    let feature: ParsedCharacteristic = {
      ...oldFeature,
    };
    // Classify Notation
    const classified = Classifier.parseInterpretedNotation({ interpreted: parsed, types });
    feature.plusTol = classified.plusTol;
    feature.minusTol = classified.minusTol;
    delete feature.upperSpecLimit;
    delete feature.lowerSpecLimit;
    // Apply Tolerances
    const measurement = getListMeasurementById({
      list: types,
      value: feature.notationSubtype,
      parentId: feature.notationType,
    });
    feature.toleranceSource = 'Document_Defined';
    if (measurement !== 'Note') {
      Analytics.track({
        event: Analytics.events.featureTolerances,
        properties: {
          characteristic: feature,
          parsed,
          tolerances,
        },
      });
      const type = getListNameById({ list: types, value: feature.notationType });
      const isGDT = type === 'Geometric Tolerance';
      feature = Tolerancer.assignFeatureTolerances({
        feature,
        tolerances,
        measurement,
        isGDT,
      });
    }
    feature.captureError = '';
    if (parsed.error) {
      // Feature was a note with a parsing error
      try {
        this.checkForParsingErrors(parsed.error, parsed);
      } catch (err: any) {
        // catch err
        const errTips: any = {
          'Empty Capture': 'No text was found, adjust the capture box to try again.',
          'Notation Interpreter requires text as input': 'No text was found, adjust the capture box to try again.',
          'Saved as a Note': "We couldn't recognize this feature type, it's been saved as a Note.",
          'Network Error': 'Our network had an issue, adjust the capture box to try again.',
        };
        feature.captureError = JSON.stringify({
          message: err.message,
          description: errTips[err.message],
        });
      }
    }
    const input = Characteristics.util.inputFromParsed(feature);

    Analytics.track({
      event: Analytics.events.featureParsingComplete,
      properties: {
        characteristic: feature,
        parsed,
      },
    });

    return { feature, input };
  }

  // Controllers
  static async parseFeatureFromFullSpec({
    assignedStyles,
    boxRotation,
    canvas,
    capture,
    drawingRotation,
    drawingScale,
    fullSpecification,
    grid,
    lastItem,
    ocrRes,
    textRes,
    oldFeature,
    tolerances,
    types,
    drawingSheetMarkerSize,
    drawingSheetRotation,
  }: {
    assignedStyles: {
      [key: string]: MarkerStyle;
    };
    boxRotation: number;
    canvas?: fabric.Canvas;
    capture: CaptureDimensions;
    drawingRotation?: number;
    drawingScale?: number;
    fullSpecification?: string;
    grid: Grid;
    lastItem?: Characteristic;
    ocrRes?: OCRResults;
    textRes?: OCRResults;
    oldFeature?: Characteristic;
    tolerances: Tolerance;
    types: ListObject;
    drawingSheetMarkerSize?: number | null;
    drawingSheetRotation?: number;
  }) {
    // Parse Notation
    let parsed;
    if (fullSpecification) {
      Analytics.track({
        event: Analytics.events.parseFullSpecification,
        properties: {
          oldFullSpec: fullSpecification,
          newFullSpec: oldFeature?.fullSpecification,
          characteristic: oldFeature,
        },
      });
      // re-parse updated full specification
      parsed = await this.interpretNotationText(fullSpecification);
    } else if (ocrRes) {
      Analytics.track({
        event: Analytics.events.parseOCR,
        properties: {
          'ocr.tess': ocrRes.tess,
          'ocr.ocr': ocrRes.ocr,
        },
      });
      // parse OCR results, choosing the best model
      const obj = await this.compareOCRParseResults(ocrRes, textRes);
      parsed = { ...obj.parsed, captureError: obj.captureError };
    } else {
      Analytics.track({
        event: Analytics.events.featureParsingError,
        properties: {
          errors: 'Cannot parse feature without OCR result or new full specification',
        },
      });
      log.error('Cannot parse feature without OCR result or new full specification'); // TODO: i18n
      return null;
    }
    let feature: ParsedCharacteristic = {
      ...oldFeature,
    };
    // reset feature  fields
    delete feature.gdtPrimaryToleranceZone;
    delete feature.gdtSecondaryToleranceZone;
    delete feature.gdtPrimaryDatum;
    delete feature.gdtSecondaryDatum;
    delete feature.gdtTertiaryDatum;
    delete feature.gdtSymbol;
    delete feature.nominal;
    delete feature.plusTol;
    delete feature.minusTol;
    delete feature.upperSpecLimit;
    delete feature.lowerSpecLimit;
    (parsed.notation.type && parsed.notation.type === 'note') || (parsed.notation.type && parsed.notation.type === 'unknown') ? (feature.toleranceSource = 'N_A') : (feature.toleranceSource = 'Default_Tolerance');

    Analytics.track({
      event: Analytics.events.featureClassification,
      properties: {
        characteristic: oldFeature,
        parsed,
      },
    });
    // Classify Notation
    const classified = Classifier.parseInterpretedNotation({ interpreted: parsed, types });
    feature = { ...feature, ...classified };
    // Apply Tolerances
    const measurement = getListMeasurementById({
      list: types,
      value: feature.notationSubtype,
      parentId: feature.notationType,
    });

    if (measurement !== 'Note') {
      Analytics.track({
        event: Analytics.events.featureTolerances,
        properties: {
          characteristic: feature,
          parsed,
          tolerances,
        },
      });
      const type = getListNameById({ list: types, value: feature.notationType });
      const isGDT = type === 'Geometric Tolerance';
      feature = Tolerancer.assignFeatureTolerances({
        feature,
        tolerances,
        measurement,
        isGDT,
      });
    }
    feature.captureError = '';
    if (parsed.error) {
      // Feature was a note with a parsing error
      try {
        feature.toleranceSource = 'N_A';
        feature.notationClass = 'N_A';
        feature.captureError = this.checkForParsingErrors(parsed.error, parsed, parsed.captureError);
      } catch (err: any) {
        // catch err
        const errTips: any = {
          'Empty Capture': 'No text was found, adjust the capture box to try again.',
          'Notation Interpreter requires text as input': 'No text was found, adjust the capture box to try again.',
          'Saved as a Note': "We couldn't recognize this feature type, it's been saved as a Note.",
          'Network Error': 'Our network had an issue, adjust the capture box to try again.',
        };
        feature.captureError = JSON.stringify({
          message: err.message,
          description: errTips[err.message],
        });
      }
    }

    if (!this.shouldUpdateToleranceSource(feature, oldFeature)) {
      feature.toleranceSource = oldFeature?.toleranceSource;
    }

    if (canvas && drawingRotation !== undefined && drawingScale !== undefined) {
      // Update UI
      Analytics.track({
        event: Analytics.events.updateUI,
        properties: {
          characteristic: feature,
          capture,
          drawingRotation,
          drawingScale,
          boxRotation,
          lastItem,
          grid,
          assignedStyles,
        },
      });
      feature = FeatureUI.createFeature({ drawingRotation, drawingScale, boxRotation, parsedFeature: feature, lastItem, drawingSheetMarkerSize, drawingSheetRotation, capture, assignedStyles, grid, canvas });
    }
    const isNewFeature = !feature.id;
    if (!isNewFeature && feature?.markerSubIndex !== undefined && feature.markerSubIndex >= 0) {
      feature.quantity = oldFeature?.quantity || 1;
    }
    if (!isNewFeature && !feature?.nominal) {
      feature.nominal = '';
    }
    feature = FeatureUI.assignMarkerStyles({ feature, assignedStyles });
    const input = Characteristics.util.inputFromParsed(feature);

    Analytics.track({
      event: Analytics.events.featureParsingComplete,
      properties: {
        characteristic: feature,
        parsed,
        isNewFeature,
      },
    });

    return { feature, input, isNewFeature };
  }

  static async updateChangedFeature({
    assignedStyles,
    change,
    oldFeature,
    tolerances,
    types,
    units,
  }: {
    assignedStyles: {
      [key: string]: MarkerStyle;
    };
    change: ParsedCharacteristic;
    oldFeature: Characteristic;
    tolerances: Tolerance;
    types: ListObject;
    units?: ListObject;
  }) {
    let feature: ParsedCharacteristic = { ...oldFeature };
    Analytics.track({
      event: Analytics.events.updateChangedFeature,
      properties: {
        characteristic: feature,
        change,
      },
    });
    const type = getListNameById({ list: types, value: change.notationType || feature.notationType });
    const isGDT = type === 'Geometric Tolerance';
    if (!isGDT) {
      const oldType = getListNameById({ list: types, value: oldFeature.notationType });
      if (oldType === 'Geometric Tolerance') {
        feature.toleranceSource = 'Default_Tolerance';
        feature.fullSpecification = unFrameText(feature.fullSpecification);
      }
    }
    if (type === 'Dimension' && (change.upperSpecLimit || change.lowerSpecLimit)) {
      log.warn('Upper and Lower Spec Limits should be assigned by the Full Specification, or applying Tolerances to the Nominal');
    }
    // Update full spec, nominal, quantity, unit, verification, status, capture method
    Analytics.track({
      event: Analytics.events.featureUpdateGeneral,
      properties: {
        characteristic: feature,
        change,
      },
    });
    feature = Classifier.updateGeneral({ feature, change });

    // Update type, subtype, or class
    feature = this.updateFeatureType({ change, oldFeature: feature, types, units, tolerances });

    // Update tolerances and spec limits
    if (change && this.hasToleranceChanged(change)) {
      feature = this.updateFeatureTolerances({ change, oldFeature: feature, types, tolerances, isGDT });
    }

    // Turned off dimension fullspec update based on beta feedback
    // if (type === 'Dimension') {
    //   feature = this.buildDimensionFullSpecification(feature);
    // } else
    if (type === 'Geometric Tolerance') {
      feature.nominal = '';
      const subType = getListNameById({ list: types, value: feature.notationSubtype, parentId: feature.notationType });
      feature.fullSpecification = Classifier.buildGDTFullSpecification(subType, feature);
    }

    // Update feature UI styles
    Analytics.track({
      event: Analytics.events.featureUpdateStyle,
      properties: {
        characteristic: feature,
        change,
        assignedStyles,
      },
    });
    feature = FeatureUI.assignMarkerStyles({ feature, assignedStyles });

    // make sure quantity is a number
    feature.quantity = parseInt(feature?.quantity?.toString() || '1', 10);

    // Update unit if it was provided, in case type or tolerances changed it
    if (change.unit && change.unit !== oldFeature.unit) {
      feature.unit = change.unit;
    }

    // TODO: Update grid locations and capture position here?
    // Or handle it in the proposed fabric controller module?

    const input = Characteristics.util.inputFromParsed(feature);
    const isNewFeature = !feature.id;
    Analytics.track({
      event: Analytics.events.featureUpdateComplete,
      properties: {
        characteristic: feature,
        change,
        isNewFeature,
      },
    });

    return { feature, input, isNewFeature };
  }
}
