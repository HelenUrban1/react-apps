import { ListObject } from 'domain/setting/listsTypes';
import { CharacteristicNotationClassEnum } from 'types/characteristics';
import { frameText, lookup } from 'utils/characters';
import { getListIdByValue } from 'utils/Lists';
import { getFramedText, getUnframedText, isFramed } from 'utils/textOperations';
import { GdtSymbols } from 'view/global/defaults';
import { GeneralObject, ParsedCharacteristic, NotationWidth, NotationValue, ParserReturnValue } from './types';

export class Classifier {
  static typeMap: GeneralObject = {
    champher: 'champher',
    countersink: 'countersink_diameter',
    counterbore: 'counterbore_diameter',
    holeWithCounterbore: { depth: 'counterbore_depth', diameter: 'counterbore_diameter' },
    holeWithCountersink: { angle: 'countersink_angle', diameter: 'countersink_diameter' },
    metricThreading: 'thread',
    metricThreadingInternal: 'thread',
    metricThreadingExternal: 'thread',
    standardThreadedFastner: 'thread',
    standardThreadingInternal: 'thread',
    valueWithOptionalUnits: 'thread',
    nominalWithTolerances: { '°': 'angle', '': 'length' },
    extreme: { '°': 'angle', '': 'length' },
    splitLimits: 'length',
    unknown: 'note',
  };

  // Utils
  static toSnakeCase(text: string) {
    return text.toLowerCase().replace(/ /g, '_');
  }

  static isBasic(modifier?: string) {
    let result = false;
    if (modifier) {
      result = modifier === 'basic' || modifier === 'framed';
    }
    return result;
  }

  static updateToBasic(fullSpec: string) {
    let cleanSpec = isFramed(fullSpec) ? getUnframedText(fullSpec) : fullSpec;
    cleanSpec = cleanSpec.replace('(', '|').replace(')', '|');
    if (cleanSpec.toLocaleLowerCase().includes('basic') || cleanSpec.toLocaleLowerCase().includes('bsc')) return cleanSpec;
    if (/\|(.*?)\|/.test(cleanSpec)) return getFramedText(cleanSpec);
    const singlePipe = cleanSpec.indexOf('|');
    if (singlePipe >= 0) {
      return singlePipe < cleanSpec.length / 2 ? getFramedText(`${cleanSpec}|`) : getFramedText(`|${cleanSpec}`);
    }
    return getFramedText(`|${cleanSpec}|`);
  }

  static isReference(modifier?: string) {
    let result = false;
    if (modifier) {
      result = modifier === 'reference';
    }
    return result;
  }

  static updateToRef(fullSpec: string) {
    let cleanSpec = isFramed(fullSpec) ? getUnframedText(fullSpec) : fullSpec;
    cleanSpec = cleanSpec.replace('|', '(').replace('|', ')');
    if (cleanSpec.toLocaleLowerCase().includes('ref') || /\((.*?)\)/.test(cleanSpec)) return cleanSpec;
    const start = cleanSpec.indexOf('(');
    if (start >= 0) return `${cleanSpec})`;
    const end = cleanSpec.indexOf(')');
    if (end >= 0) return `(${cleanSpec}`;
    return `(${cleanSpec})`;
  }

  static updateToTol(fullSpec: string, oldClass: CharacteristicNotationClassEnum) {
    let cleanSpec = isFramed(fullSpec) ? getUnframedText(fullSpec) : fullSpec;
    if (oldClass === 'Basic') {
      cleanSpec = cleanSpec.replace(/\|/g, '');
    } else {
      cleanSpec = cleanSpec.replace('(', '').replace(')', '');
    }
    return cleanSpec;
  }

  static getClassForNotation(modifierOrType?: string) {
    let notationClass: CharacteristicNotationClassEnum = 'Tolerance';
    if (this.isBasic(modifierOrType)) {
      notationClass = 'Basic';
    } else if (this.isReference(modifierOrType)) {
      notationClass = 'Reference';
    } else if (modifierOrType === 'note' || modifierOrType === 'unknown') {
      notationClass = 'N_A';
    }
    return notationClass;
  }

  static getSubTypeVariant(interpreted: ParserReturnValue) {
    let subtype = '';
    if (interpreted?.notation?.type === 'holeWithCounterbore') {
      if (interpreted?.notation?.countersink?.angle?.amount) {
        subtype = 'countersink_angle';
      } else {
        subtype = 'countersink_diameter';
      }
    }
    if (interpreted?.notation?.type === 'holeWithCounterbore') {
      if (interpreted?.notation?.counterbore?.depth?.amount) {
        subtype = 'counterbore_depth';
      } else {
        subtype = 'counterbore_diameter';
      }
    }
    if (interpreted?.notation?.type === 'nominalWithTolerances' || interpreted?.notation?.type === 'extreme') {
      if (interpreted?.notation?.nominal?.units === '°') {
        subtype = 'angle';
      } else {
        subtype = 'length';
      }
    }
    return subtype;
  }

  static getSubType(interpreted: ParserReturnValue) {
    let subtype: string | GeneralObject = '';

    if (interpreted?.notation?.symbol === 'R') {
      subtype = 'radius';
    } else if (interpreted?.notation?.symbol) {
      const { name } = lookup(interpreted.notation.symbol);
      subtype = this.toSnakeCase(name);
    } else if (interpreted?.notation?.type) {
      subtype = this.typeMap[interpreted.notation.type];
      if (!subtype) {
        subtype = interpreted.notation.type;
      }
    }
    if (typeof subtype !== 'string') {
      subtype = this.getSubTypeVariant(interpreted);
    }

    return subtype;
  }

  static getGDTSymbol(subtype?: string) {
    if (!subtype) {
      return subtype;
    }
    const type = this.toSnakeCase(subtype);
    const symbol = GdtSymbols[type];
    return symbol?.char || GdtSymbols.position.char;
  }

  static buildGDTFullSpecification(subType: string, feature: ParsedCharacteristic) {
    const newFeature = { ...feature };
    let fullSpecification = '|';
    const symbol = Classifier.getGDTSymbol(subType);

    fullSpecification += `${symbol}|`;
    fullSpecification += `${newFeature.gdtPrimaryToleranceZone}|`;

    if (newFeature.gdtSecondaryToleranceZone) {
      fullSpecification += `${newFeature.gdtSecondaryToleranceZone}|`;
    }
    if (newFeature.gdtPrimaryDatum) {
      fullSpecification += `${newFeature.gdtPrimaryDatum}|`;
    }
    if (newFeature.gdtSecondaryDatum) {
      fullSpecification += `${newFeature.gdtSecondaryDatum}|`;
    }
    if (newFeature.gdtTertiaryDatum) {
      fullSpecification += `${newFeature.gdtTertiaryDatum}|`;
    }

    return frameText(fullSpecification);
  }

  static getTypeMarkerStyle(styleOptions: { [key: string]: { style: string; assign: string[] } }, type: string, subtype: string): { style: string; matched: boolean } {
    const markerStyle = Object.values(styleOptions).find((style) => style.assign[1] === type && style.assign[2] === subtype);
    return { style: markerStyle?.style || styleOptions.default.style, matched: !!markerStyle };
  }

  // Updates
  static updateGeneral({ feature, change }: { feature: ParsedCharacteristic; change: ParsedCharacteristic }) {
    const newFeature: ParsedCharacteristic = { ...feature };
    if (change.quantity) {
      newFeature.quantity = parseInt(change.quantity.toString(), 10);
    }
    if (change.unit || change.unit === '') {
      newFeature.unit = change.unit;
    }
    if (change.notationType) {
      newFeature.notationType = change.notationType;
    }
    if (change.notationSubtype) {
      newFeature.notationSubtype = change.notationSubtype;
    }
    if (change.status) {
      newFeature.status = change.status;
    }
    if (change.captureMethod) {
      newFeature.captureMethod = change.captureMethod;
    }
    if (change.criticality || change.criticality === '') {
      newFeature.criticality = change.criticality;
    }
    if (change.operation || change.operation === '') {
      newFeature.operation = change.operation;
    }
    if (change.inspectionMethod || change.inspectionMethod === '') {
      newFeature.inspectionMethod = change.inspectionMethod;
    }
    if (change.notes || change.notes === '') {
      newFeature.notes = change.notes;
    }
    if (change.verified) {
      newFeature.verified = change.verified;
    } else {
      newFeature.verified = false;
    }
    return newFeature;
  }

  static updateDimension({ feature, change }: { feature: ParsedCharacteristic; change: ParsedCharacteristic }) {
    const dimension: ParsedCharacteristic = { ...feature };
    if (change.nominal) {
      dimension.nominal = change.nominal;
    }
    if (change.notationClass) {
      dimension.notationClass = change.notationClass;
      if (change.notationClass === 'Basic') {
        dimension.fullSpecification = this.updateToBasic(dimension.fullSpecification || '');
      } else if (change.notationClass === 'Reference') {
        dimension.fullSpecification = this.updateToRef(dimension.fullSpecification || '');
      } else {
        dimension.fullSpecification = this.updateToTol(dimension.fullSpecification || '', dimension.notationClass);
      }
    }
    if (change.unit) {
      dimension.unit = change.unit;
    }
    if (change.fullSpecification) {
      dimension.fullSpecification = change.fullSpecification;
    } else if (!change.notationClass) {
      dimension.fullSpecification = dimension.nominal;
      if (dimension.plusTol && dimension.minusTol) {
        dimension.fullSpecification += ` ${dimension.plusTol}/${dimension.minusTol}`;
      }
    }
    delete dimension.gdtPrimaryToleranceZone;
    delete dimension.gdtSecondaryToleranceZone;
    delete dimension.gdtPrimaryDatum;
    delete dimension.gdtSecondaryDatum;
    delete dimension.gdtTertiaryDatum;
    delete dimension.gdtSymbol;
    return dimension;
  }

  static updateGDT({ feature, change }: { feature: ParsedCharacteristic; change: ParsedCharacteristic }) {
    const gdt: ParsedCharacteristic = { ...feature };
    if (change.gdtSymbol) {
      gdt.gdtSymbol = change.gdtSymbol;
    }
    if (change.gdtPrimaryToleranceZone) {
      gdt.gdtPrimaryToleranceZone = change.gdtPrimaryToleranceZone;
    }
    if (change.gdtSecondaryToleranceZone) {
      gdt.gdtSecondaryToleranceZone = change.gdtSecondaryToleranceZone;
    }
    if (change.gdtPrimaryDatum) {
      gdt.gdtPrimaryDatum = change.gdtPrimaryDatum;
    }
    if (change.gdtSecondaryDatum) {
      gdt.gdtSecondaryDatum = change.gdtSecondaryDatum;
    }
    if (change.gdtTertiaryDatum) {
      gdt.gdtTertiaryDatum = change.gdtTertiaryDatum;
    }
    if (change.fullSpecification) {
      gdt.fullSpecification = change.fullSpecification;
    }
    delete gdt.nominal;
    delete gdt.plusTol;
    delete gdt.minusTol;
    // delete gdt.upperSpecLimit;
    // delete gdt.lowerSpecLimit;
    return gdt;
  }

  // Assignment
  static assignNote({ feature, change, interpreted }: { feature: ParsedCharacteristic; change?: string; interpreted?: ParserReturnValue }) {
    const note = { ...feature };
    note.notationType = 'Note';
    note.notationSubtype = 'Note';
    if (change) {
      note.fullSpecification = change;
      note.nominal = change;
    } else if (interpreted?.parsed?.input) {
      note.fullSpecification = interpreted.parsed.input.trim();
      note.nominal = interpreted.parsed.input.trim();
    } else {
      // Calling getFramedText ensures that Notes with Basic or GDTs embeded in them get framed
      note.fullSpecification = interpreted?.input ? getFramedText(interpreted?.input) : interpreted?.input;
      note.nominal = interpreted?.input;
    }
    note.unit = '';
    return note;
  }

  static assignGDT({ feature, interpreted }: { feature: ParsedCharacteristic; interpreted: ParserReturnValue }) {
    const GDT = { ...feature };
    GDT.notationType = 'Geometric_Tolerance';
    GDT.fullSpecification = interpreted?.input;
    GDT.gdtSymbol = '';
    GDT.gdtPrimaryToleranceZone = '';
    GDT.gdtSecondaryToleranceZone = '';
    GDT.gdtPrimaryDatum = '';
    GDT.gdtSecondaryDatum = '';
    GDT.gdtTertiaryDatum = '';
    GDT.upperSpecLimit = '';
    GDT.lowerSpecLimit = '';
    if (interpreted?.input) {
      GDT.fullSpecification = frameText(interpreted.input);
    }
    if (interpreted?.notation?.subtype) {
      GDT.notationSubtype = this.toSnakeCase(interpreted.notation.subtype);
      GDT.gdtSymbol = this.getGDTSymbol(GDT.notationSubtype);
    }
    if (interpreted?.notation?.zones) {
      GDT.gdtPrimaryToleranceZone = interpreted.notation.zones.primary.text;
      GDT.gdtSecondaryToleranceZone = interpreted.notation.zones?.secondary?.value || '';
    }
    if (interpreted?.notation?.datums) {
      GDT.gdtPrimaryDatum = interpreted.notation.datums[0]?.id;
      GDT.gdtSecondaryDatum = interpreted.notation.datums[1]?.id;
      GDT.gdtTertiaryDatum = interpreted.notation.datums[2]?.id;
    }
    return GDT;
  }

  static assignDimensionWidth({ feature, width }: { feature: ParsedCharacteristic; width: NotationWidth }) {
    let dimension = { ...feature };

    if (width?.upper_tol) {
      dimension.plusTol = width.upper_tol.sign === '+' ? width.upper_tol.value : `${width.upper_tol.sign}${width.upper_tol.value}`;
      dimension.toleranceSource = 'Document_Defined';
    }

    if (width?.lower_tol) {
      dimension.minusTol = width.lower_tol.sign === '+' ? width.lower_tol.value : `${width.lower_tol.sign}${width.lower_tol.value}`;
      dimension.toleranceSource = 'Document_Defined';
    }

    if (width?.upper_limit) {
      dimension.upperSpecLimit = width.upper_limit.value;
      dimension.toleranceSource = 'Document_Defined';
    }

    if (width?.lower_limit) {
      dimension.lowerSpecLimit = width.lower_limit.value;
      dimension.toleranceSource = 'Document_Defined';
    }

    if (width?.nominal) {
      dimension.nominal = width.nominal.value;
    } else {
      dimension.nominal = width?.text;
    }

    if (width?.limit) {
      dimension = this.assignDimensionLimit({
        feature: dimension,
        limit: width.limit,
        edge: width?.edge,
      });
    }
    return dimension;
  }

  static assignDimensionLimit({ feature, edge, limit }: { feature: ParsedCharacteristic; edge?: string; limit: NotationValue }) {
    const dimension = { ...feature };
    dimension.nominal = limit.value;
    dimension.upperSpecLimit = edge === 'MAX' ? limit.value : '';
    dimension.lowerSpecLimit = edge === 'MIN' ? limit.value : '';
    dimension.toleranceSource = 'Document_Defined';
    return dimension;
  }

  static assignSpecialDimension({ feature, interpreted }: { feature: ParsedCharacteristic; interpreted: ParserReturnValue }) {
    let dimension = { ...feature };
    dimension.notationType = 'Dimension';
    dimension.notationSubtype = this.getSubType(interpreted);
    dimension.fullSpecification = interpreted?.input;

    if (interpreted?.notation?.amount) {
      dimension.nominal = interpreted.notation.amount.value;
    }

    if (interpreted?.notation?.width) {
      dimension = this.assignDimensionWidth({
        feature: dimension,
        width: interpreted.notation.width,
      });
    }

    if (interpreted?.notation?.limit) {
      dimension = this.assignDimensionLimit({
        feature: dimension,
        limit: interpreted.notation.limit,
        edge: interpreted?.notation?.edge,
      });
    }

    return dimension;
  }

  static assignBasicDimension({ feature, interpreted }: { feature: ParsedCharacteristic; interpreted: ParserReturnValue }) {
    const dimension = { ...feature };

    if (interpreted?.notation?.upper_limit) {
      dimension.upperSpecLimit = interpreted.notation.upper_limit.value;
      dimension.toleranceSource = 'Document_Defined';
    }

    if (interpreted?.notation?.lower_limit) {
      dimension.lowerSpecLimit = interpreted.notation.lower_limit.value;
      dimension.toleranceSource = 'Document_Defined';
    }
    // plus plus, minus, minus ???
    if (interpreted?.notation?.upper_tol) {
      dimension.plusTol = interpreted.notation.upper_tol.sign === '+' ? interpreted.notation.upper_tol.value : `${interpreted.notation.upper_tol.sign}${interpreted.notation.upper_tol.value}`;
      dimension.toleranceSource = 'Document_Defined';
    }

    if (interpreted?.notation?.lower_tol) {
      dimension.minusTol = interpreted.notation.lower_tol.sign === '+' ? interpreted.notation.lower_tol.value : `${interpreted.notation.lower_tol.sign}${interpreted.notation.lower_tol.value}`;
      dimension.toleranceSource = 'Document_Defined';
    }

    if (interpreted?.notation?.nominal) {
      dimension.nominal = interpreted.notation.nominal.value;
    } else {
      dimension.nominal = interpreted?.notation?.text;
    }

    return dimension;
  }

  static assignDimension({ feature, interpreted }: { feature: ParsedCharacteristic; interpreted: ParserReturnValue }) {
    let dimension = { ...feature };
    dimension.notationType = 'Dimension';
    dimension.notationSubtype = this.getSubType(interpreted);
    dimension.fullSpecification = interpreted?.input;
    dimension.nominal = '';
    dimension.plusTol = '';
    dimension.minusTol = '';
    dimension.upperSpecLimit = '';
    dimension.lowerSpecLimit = '';

    dimension = this.assignBasicDimension({ feature: dimension, interpreted });
    dimension = this.assignSpecialDimension({ feature: dimension, interpreted });
    return dimension;
  }

  static assignTypeAndSubtype(feature: ParsedCharacteristic, types: ListObject) {
    const typeSubType = { ...feature };
    const typeId = getListIdByValue({
      list: types,
      value: feature.notationType,
      defaultValue: 'fa3284ed-ad3e-43c9-b051-87bd2e95e927',
    });
    typeSubType.notationType = typeId;
    if (typeId && types[typeId]?.children) {
      const subTypeId = getListIdByValue({
        list: types[typeId].children,
        value: feature.notationSubtype,
        defaultValue: '805fd539-8063-4158-8361-509e10a5d371',
      });
      typeSubType.notationSubtype = subTypeId;
    }
    return typeSubType;
  }

  // Controller
  static parseInterpretedNotation({ interpreted, types }: { interpreted: ParserReturnValue; types: ListObject }) {
    if (!interpreted) {
      throw new Error('Cannot parse blank results');
    }
    let feature: ParsedCharacteristic = {};

    switch (interpreted?.notation?.type) {
      case 'note':
      case 'unknown':
        feature = this.assignNote({ feature, interpreted });
        break;
      case 'gdtControlFrame':
        feature = this.assignGDT({ feature, interpreted });
        break;
      case undefined:
        throw new Error('Interpretation not parsed');
      default:
        feature = this.assignDimension({ feature, interpreted });
        break;
    }

    feature.quantity = interpreted?.quantity?.value ? parseInt(interpreted.quantity.value, 10) : 1;
    feature = this.assignTypeAndSubtype(feature, types);
    feature.notationClass = this.getClassForNotation(interpreted?.notation?.modifier ? interpreted.notation.modifier : interpreted?.notation?.type);
    if (feature.notationClass === 'Basic') {
      feature.fullSpecification = this.updateToBasic(feature.fullSpecification || '');
    } else if (feature.notationClass === 'Reference') {
      feature.fullSpecification = this.updateToRef(feature.fullSpecification || '');
    }
    return feature;
  }
}
