import { CharacteristicCaptureEnum, CharacteristicNotationClassEnum, CharacteristicStatusEnum, CharacteristicToleranceEnum } from 'types/characteristics';

export interface ParsedCharacteristic {
  status?: CharacteristicStatusEnum;
  captureMethod?: CharacteristicCaptureEnum;
  notationType?: string;
  notationSubtype?: string;
  notationClass?: CharacteristicNotationClassEnum;
  fullSpecification?: string;
  quantity?: number;
  nominal?: string;
  upperSpecLimit?: string;
  lowerSpecLimit?: string;
  plusTol?: string;
  minusTol?: string;
  unit?: string;
  gdtSymbol?: string;
  gdtPrimaryToleranceZone?: string;
  gdtSecondaryToleranceZone?: string;
  gdtPrimaryDatum?: string;
  gdtSecondaryDatum?: string;
  gdtTertiaryDatum?: string;
  fullSpecificationFromOCR?: string;
  confidence?: number;
  IsBasic?: boolean;
  IsReference?: boolean;
  id?: string;
  drawingSheetIndex?: number;
  criticality?: string | null;
  inspectionMethod?: string | null;
  notes?: string;
  drawingRotation?: number;
  drawingScale?: number;
  boxLocationY?: number;
  boxLocationX?: number;
  boxWidth?: number;
  boxHeight?: number;
  boxRotation?: number;
  markerGroup?: string;
  markerGroupShared?: boolean;
  markerIndex?: number;
  markerSubIndex?: number;
  markerLabel?: string;
  markerStyle?: string;
  markerLocationX?: number;
  markerLocationY?: number;
  markerFontSize?: number;
  markerSize?: number;
  markerRotation?: number;
  gridCoordinates?: string;
  balloonGridCoordinates?: string;
  connectionPointGridCoordinates?: string;
  verified?: boolean;
  operation?: string | null;
  toleranceSource?: CharacteristicToleranceEnum;
  part?: any;
  drawing?: any;
  drawingSheet?: any;
  leaderLineDistance?: number;
  [key: string]: any | string | number | boolean | Date | CharacteristicNotationClassEnum | CharacteristicCaptureEnum | CharacteristicStatusEnum | CharacteristicToleranceEnum | undefined | null;
}

export interface NotationValue {
  text: string;
  value: string;
  sign: string;
  units: string;
}

export interface NotationWidth {
  edge?: string;
  text?: string;
  input?: string;
  type?: string;
  limit?: NotationValue;
  upper_tol?: NotationValue;
  lower_tol?: NotationValue;
  upper_limit?: NotationValue;
  lower_limit?: NotationValue;
  nominal?: NotationValue;
}

export interface NotationZone {
  modifier?: string;
  postModifierValue?: string;
  prefix?: string;
  text?: string;
  value?: string;
}

export interface ParserReturnValue {
  ocrRes?: string;
  errors?: {
    name?: string;
    errors?: {
      context?: any;
      message?: string;
      name?: string;
      previousToken?: any;
      resyncedTokens?: any;
      token?: any;
    };
  };
  input?: string;
  text?: string;
  words?: string;
  quantity?: NotationValue;
  parsed?: {
    input?: string;
  };
  notation?: {
    input?: string;
    depth?: {
      text?: string;
      symbol?: string;
      word?: string;
      type?: string;
      amount?: NotationValue;
    };
    modifier?: string;
    symbol?: string;
    text?: string;
    type?: string;
    subtype?: string;
    amount?: NotationValue;
    width?: NotationWidth;
    datums?: { id: string }[];
    zones?: {
      primary: NotationZone;
      secondary?: NotationZone;
    };
    limit?: NotationValue;
    edge?: string;
    upper_tol?: NotationValue;
    lower_tol?: NotationValue;
    upper_limit?: NotationValue;
    lower_limit?: NotationValue;
    nominal?: NotationValue;
    countersink?: {
      angle?: {
        amount?: NotationValue;
      };
    };
    counterbore?: {
      depth?: {
        amount?: NotationValue;
      };
    };
  };
}

export interface CaptureDimensions {
  left: number;
  top: number;
  width: number;
  height: number;
}

export interface CaptureCoordinates {
  x: number;
  y: number;
}

export interface GeneralObject {
  [key: string]: string | GeneralObject;
}
export interface OCRResults {
  tess: string;
  ocr: string;
  error?: any;
}
