import { PrecisionTolerance, RangeTolerance, Tolerance, TolSection } from 'types/tolerances';
import { defaultSubtypeIDs } from 'view/global/defaults';
import { ParsedCharacteristic } from './types';

export class Tolerancer {
  // Utils
  static toSnakeCase(text: string) {
    return text.toLowerCase().replace(/ /g, '_');
  }

  static toNumericString(text: string) {
    return text.replace(/[^0-9.,]/g, '');
  }

  static getPrecision(value: string) {
    if (!value) {
      return 0;
    }
    const parts = value.trim().split('.');
    return parts[1] ? parts[1].length : 0;
  }

  static getGreatestPrecision({ nominal, plus, minus }: { nominal: string; plus?: string; minus?: string }) {
    const all = [this.getPrecision(nominal)];
    if (plus) {
      all.push(this.getPrecision(plus));
    }
    if (minus) {
      all.push(this.getPrecision(minus));
    }
    all.sort();
    return all[all.length - 1];
  }

  static getTolerancesForSubtype({ subType, tolerances }: { subType: string; tolerances: Tolerance }) {
    return subType === defaultSubtypeIDs.Angle || (subType && subType.toLowerCase() === 'angle') ? tolerances.angular : tolerances.linear;
  }

  static getRangedTolerance(tolerance: RangeTolerance[], nominal: string) {
    let plus = '';
    let minus = '';
    for (let index = 0; index < tolerance.length; index++) {
      const range = tolerance[index];
      const nominalNum = parseFloat(nominal);
      if (nominalNum >= parseFloat(range.rangeLow) && nominalNum <= parseFloat(range.rangeHigh)) {
        plus = range.plus;
        minus = range.minus;
        break;
      }
    }
    return { plus, minus };
  }

  static getPrecisionTolerance(tolerance: PrecisionTolerance[], nominal: string) {
    const precision = this.getPrecision(nominal);
    let plus = '';
    let minus = '';
    for (let index = 0; index < tolerance.length; index++) {
      const tol = tolerance[index];
      const tolPrecision = this.getPrecision(tol.level);
      if (precision === tolPrecision) {
        plus = tol.plus;
        minus = tol.minus;
        break;
      }
    }
    return { plus, minus };
  }

  // Calculations
  static calculateSpecLimitFromTols({ nominal, plus, minus }: { nominal: string; plus?: string; minus?: string }) {
    let upper = '';
    let lower = '';
    const precision = this.getGreatestPrecision({ nominal, plus, minus });
    if (plus) {
      upper = (parseFloat(nominal) + parseFloat(plus)).toFixed(precision).toString();
    }
    if (minus) {
      lower = (parseFloat(nominal) + parseFloat(minus)).toFixed(precision).toString(); // plus plus, minus, minus ???
    }
    return { upper, lower };
  }

  static calculateGDTTolerances(totalTol: string) {
    // if the totalTol is odd then we need to add an extra precision so a tolerance of 0.25 becomes 0.125 instead of 0.13
    const buffer = parseInt(totalTol.charAt(totalTol.length - 1), 10) % 2 === 1 ? 1 : 0;
    const precision = this.getPrecision(totalTol) + buffer;
    const plus = (parseFloat(totalTol) / 2).toFixed(precision);
    const minus = ((parseFloat(totalTol) * -1) / 2).toFixed(precision); // plus plus, minus, minus ???
    return {
      plus,
      minus,
    };
  }

  static calculateDimensionTolerances({ nominal, tolerances }: { nominal: string; tolerances: TolSection }) {
    if (tolerances.type === 'PrecisionTolerance') {
      return this.getPrecisionTolerance(tolerances.precisionTols, nominal);
    }
    return this.getRangedTolerance(tolerances.rangeTols, nominal);
  }

  static calculateGDTSpecLimits(feature: ParsedCharacteristic, measurement: string, primary?: string) {
    if (!primary) {
      throw new Error('No Primary Tolerance Zone');
    }
    const primaryVal = this.toNumericString(primary);
    const gdtWithLimits = { ...feature };
    gdtWithLimits.toleranceSource = 'Document_Defined';
    if (measurement.toLocaleLowerCase().includes('profile')) {
      const { plus, minus } = this.calculateGDTTolerances(primaryVal);
      const { upper, lower } = this.calculateSpecLimitFromTols({
        nominal: '0',
        plus,
        minus,
      });
      gdtWithLimits.upperSpecLimit = upper;
      gdtWithLimits.lowerSpecLimit = lower;
    } else {
      gdtWithLimits.upperSpecLimit = primaryVal;
      gdtWithLimits.lowerSpecLimit = '';
    }
    return gdtWithLimits;
  }

  static calculateDimensionSpecLimits({ feature, tolerances, measurement }: { feature: ParsedCharacteristic; tolerances: Tolerance; measurement: string }) {
    const dimWithLimits = { ...feature };
    if (!dimWithLimits.nominal) {
      return dimWithLimits;
    }
    const { nominal } = dimWithLimits;

    if (dimWithLimits.upperSpecLimit && dimWithLimits.lowerSpecLimit) {
      return dimWithLimits;
    }

    const tols = this.getTolerancesForSubtype({
      subType: measurement,
      tolerances,
    });
    let plus = dimWithLimits.plusTol || '';
    let minus = dimWithLimits.minusTol || '';

    // Get defaults if either tolerance is missing
    if (plus === '' && minus === '') {
      dimWithLimits.toleranceSource = 'Default_Tolerance';
    }
    if (plus === '' || minus === '') {
      const tol = this.calculateDimensionTolerances({
        nominal: nominal,
        tolerances: tols,
      });
      plus = tol.plus;
      minus = tol.minus;
      dimWithLimits.plusTol = dimWithLimits.plusTol || plus;
      dimWithLimits.minusTol = dimWithLimits.minusTol || minus;
    }

    // Calculate spec limits using tolerances
    const { upper, lower } = this.calculateSpecLimitFromTols({
      nominal: nominal,
      plus,
      minus,
    });
    dimWithLimits.upperSpecLimit = upper;
    dimWithLimits.lowerSpecLimit = lower;
    if (!dimWithLimits.unit || dimWithLimits.unit === '') {
      // Don't replace with tolerance unit unless feature has none
      dimWithLimits.unit = tols.unit;
    }

    return dimWithLimits;
  }

  // Controller
  static assignFeatureTolerances({
    feature,
    tolerances,
    measurement, // subtype or subtype measurement for dimensions and GDT
    isGDT,
  }: {
    feature: ParsedCharacteristic;
    tolerances: Tolerance;
    measurement?: string;
    isGDT: boolean;
  }) {
    if (!feature) {
      throw new Error('Cannot assign tolerances to null feature');
    }
    if (!tolerances) {
      throw new Error('Cannot assign tolerances without part defaults');
    }
    let tolerancedFeature: ParsedCharacteristic = { ...feature };
    if (measurement) {
      if (isGDT) {
        tolerancedFeature = this.calculateGDTSpecLimits(tolerancedFeature, measurement, tolerancedFeature.gdtPrimaryToleranceZone);
      } else {
        tolerancedFeature = this.calculateDimensionSpecLimits({
          feature: tolerancedFeature,
          tolerances,
          measurement,
        });
      }
    }
    // no measurement = note
    return tolerancedFeature;
  }
}
