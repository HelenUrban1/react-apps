import React from 'react';
import { MarkerStrokeStyleEnum } from 'types/marker';
import { BrandColors } from 'view/global/defaults';

const hexRegex = /^#([0-9a-fA-F]{3}){1,2}$/;
const shapes = ['Triangle', 'Square', 'Pentagon', 'Diamond', 'Hexagon', 'Circle'];

export const getSvgStrokeDashArray = (style: MarkerStrokeStyleEnum) => {
  switch (style) {
    case 'Dashed':
      return '3 1';
    case 'Dotted':
      return '1';
    default:
      return '1 0';
  }
};

const getBalloon = (shape: string, borderColor: string, fillColor: string, strokeDashArray: string, strokeWidth: number) => {
  const stroke = BrandColors[borderColor] || borderColor;
  const fill = BrandColors[fillColor] || fillColor;

  switch (shape) {
    case 'Square':
      return <polygon points="2,2 26,2 26,26 2,26" stroke={stroke} strokeWidth={strokeWidth} fill={fill} strokeDasharray={strokeDashArray} />;
    case 'Diamond':
      return <polygon points="14,2 26,14 14,26 2,14" stroke={stroke} strokeWidth={strokeWidth} fill={fill} strokeDasharray={strokeDashArray} />;
    case 'Triangle':
      return <polygon points="14,2 26,26 2,26" stroke={stroke} strokeWidth={strokeWidth} fill={fill} strokeDasharray={strokeDashArray} />;
    case 'Pentagon':
      return <path d="M2 14.5L8 4H20L26 14.5L20 25H8L2 14.5Z" stroke={stroke} strokeWidth={strokeWidth} fill={fill} strokeDasharray={strokeDashArray} />;
    case 'Hexagon':
      return <path d="M2 14.5L8 4H20L26 14.5L20 25H8L2 14.5Z" stroke={stroke} strokeWidth={strokeWidth} fill={fill} strokeDasharray={strokeDashArray} />;
    default:
      return <ellipse cx="14" cy="14" rx="13" ry="13" stroke={stroke} strokeWidth={strokeWidth} fill={fill} strokeDasharray={strokeDashArray} />;
  }
};

export const svgMaker = (color: string, stroke: string, strokeWidth: number, shape: string, strokeStyle: MarkerStrokeStyleEnum = 'Solid') => {
  const balloonShape = shapes.includes(shape) ? shape : 'Circle';
  const fillColor = BrandColors[color] || color.match(hexRegex) ? color : 'Coral';
  const borderColor = BrandColors[stroke] || stroke.match(hexRegex) ? stroke : 'Coral';
  const strokeDashArray = getSvgStrokeDashArray(strokeStyle);

  const balloon = getBalloon(shape, borderColor, fillColor, strokeDashArray, strokeWidth);
  const id = `${borderColor}-${balloonShape}-${borderColor === fillColor ? 'solid' : 'hollow'}`.toLowerCase();

  return (
    <svg width="28" id={id} height="28" viewBox="0 0 28 28" fill="none" xmlns="http://www.w3.org/2000/svg">
      {balloon}
    </svg>
  );
};

export const numberedSvgMaker = (color: string, stroke: string, strokeWidth: number, text: string, shape: string, strokeStyle: MarkerStrokeStyleEnum = 'Solid', number: string = '7') => {
  const balloonShape = shapes.includes(shape) ? shape : 'Circle';
  const fillColor = BrandColors[color] || color.match(hexRegex) ? color : 'Coral';
  const borderColor = BrandColors[stroke] || stroke.match(hexRegex) ? stroke : 'Coral';
  const textColor = BrandColors[text] || text.match(hexRegex) ? text : 'White';
  const strokeDashArray = getSvgStrokeDashArray(strokeStyle);

  const balloon = getBalloon(shape, borderColor, fillColor, strokeDashArray, strokeWidth);
  const id = `${borderColor}-${balloonShape}-${borderColor === fillColor ? 'solid' : 'hollow'}`.toLowerCase();

  return (
    <svg width="28" id={id} height="28" viewBox="0 0 28 28" fill="none" xmlns="http://www.w3.org/2000/svg">
      {balloon}
      <text x="10" y={shape === 'Triangle' || shape === 'Pentagon' ? '24' : '19'} fontSize={14} fill={BrandColors[textColor] || textColor}>
        {number}
      </text>
    </svg>
  );
};

export const numberedLeaderSvgMaker = (
  color: string,
  stroke: string,
  strokeWidth: number,
  text: string,
  shape: string,
  lineColor: string,
  strokeStyle: MarkerStrokeStyleEnum = 'Solid',
  lineStyle: MarkerStrokeStyleEnum = 'Solid',
  number: string = '7',
) => {
  const balloonShape = shapes.includes(shape) ? shape : 'Circle';
  const fillColor = BrandColors[color] || color.match(hexRegex) ? color : 'Coral';
  const borderColor = BrandColors[stroke] || stroke.match(hexRegex) ? stroke : 'Coral';
  const textColor = BrandColors[text] || text.match(hexRegex) ? text : 'White';
  const strokeDashArray = getSvgStrokeDashArray(strokeStyle);
  const lineDashArray = getSvgStrokeDashArray(lineStyle);

  const balloon = getBalloon(shape, borderColor, fillColor, strokeDashArray, strokeWidth);
  const id = `${borderColor}-${balloonShape}-${borderColor === fillColor ? 'solid' : 'hollow'}`.toLowerCase();
  const lineY = 14 - strokeWidth / 2;

  return (
    <svg width="36" id={id} height="28" viewBox="0 0 36 28" fill="none" xmlns="http://www.w3.org/2000/svg">
      <line x1="14" y1={lineY} x2="34" y2={lineY} strokeWidth={strokeWidth} stroke={BrandColors[lineColor] || lineColor} strokeDasharray={lineDashArray} />
      {balloon}
      <text x="10" y={shape === 'Triangle' || shape === 'Pentagon' ? '24' : '19'} fill={BrandColors[textColor] || textColor}>
        {number}
      </text>
    </svg>
  );
};
