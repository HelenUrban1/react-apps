import { getDifferenceInDays, getTrialExpiredCountdown } from './DateOperations';

describe('Date Operations', () => {
  describe('Difference in Days', () => {
    it('Calculates edges correctly', () => {
      const startDate = new Date();
      const endDate = new Date();
      startDate.setHours(0);
      startDate.setMinutes(0);
      startDate.setDate(1);
      endDate.setHours(23);
      endDate.setMinutes(59);
      endDate.setSeconds(59);
      endDate.setDate(2);
      expect(getDifferenceInDays(startDate, endDate)).toEqual(1);
    });

    it('Calculates dates a minute apart correctly', () => {
      const startDate = new Date();
      const endDate = new Date();
      startDate.setHours(23);
      startDate.setMinutes(59);
      startDate.setSeconds(59);
      startDate.setDate(1);
      endDate.setHours(0);
      endDate.setMinutes(0);
      endDate.setSeconds(1);
      endDate.setDate(2);
      expect(getDifferenceInDays(startDate, endDate)).toEqual(1);
    });

    it.skip('Returns a negative number if the start date is after the end date', () => {
      const startDate = new Date();
      const endDate = new Date();
      startDate.setHours(23);
      startDate.setMinutes(59);
      startDate.setSeconds(59);
      startDate.setDate(5);
      endDate.setHours(0);
      endDate.setMinutes(0);
      endDate.setSeconds(1);
      endDate.setDate(1);
      expect(getDifferenceInDays(startDate, endDate)).toEqual(-4);
    });

    it('Calculates across months correctly', () => {
      const startDate = new Date();
      const endDate = new Date();
      startDate.setHours(23);
      startDate.setMinutes(59);
      startDate.setSeconds(59);
      startDate.setDate(5);
      startDate.setMonth(0);
      endDate.setHours(0);
      endDate.setMinutes(0);
      endDate.setSeconds(1);
      endDate.setDate(14);
      endDate.setMonth(1);
      expect(getDifferenceInDays(startDate, endDate)).toEqual(40);
    });

    it('Returns 0 on the same date', () => {
      const startDate = new Date();
      const endDate = new Date();
      startDate.setHours(0);
      startDate.setMinutes(0);
      startDate.setDate(1);
      endDate.setHours(23);
      endDate.setMinutes(59);
      endDate.setSeconds(59);
      endDate.setDate(1);
      expect(getDifferenceInDays(startDate, endDate)).toEqual(0);
    });

    it('Returns the countdown until trial expired', () => {
      const trialEndDate = new Date();
      trialEndDate.setDate(trialEndDate.getDate() - 5);
      expect(getTrialExpiredCountdown(trialEndDate)).toBeGreaterThanOrEqual(25);
      expect(getTrialExpiredCountdown(trialEndDate)).toBeLessThanOrEqual(26);
    });
  });
});
