import { profileColorMap } from 'view/global/defaults';
import md5 from 'md5';
import { Organization } from 'graphql/organization';
import { User } from 'types/user';

export const getUserColor = (id: string) => {
  // Use a color from the profile color map based on the title or id hash
  const hashMod: number = parseInt(md5(id), 16) % profileColorMap.length;
  const color = profileColorMap[hashMod];
  return color;
};

export const getUserInitials = (currentUser: User) => {
  let initials = '';
  if (!currentUser) return initials;
  if (currentUser.fullName) {
    const words = currentUser.fullName.split(' ');
    initials = words[0].charAt(0).toLocaleUpperCase();
    if (words[1]) initials += words[1].charAt(0).toLocaleUpperCase();
  } else {
    if (currentUser.firstName && currentUser.firstName.length > 0) {
      initials = currentUser.firstName.charAt(0).toLocaleUpperCase();
    }
    if (currentUser.lastName && currentUser.lastName.length > 0) {
      initials += currentUser.lastName.charAt(0).toLocaleUpperCase();
    }
  }
  if (initials.length === 0 && currentUser.email) {
    initials = currentUser.email.charAt(0).toLocaleUpperCase();
  }
  return initials;
};

export const getCompanyInitials = (org: Organization) => {
  let initials = '';
  const words = org.name.split(' ');
  if (words.length === 1) {
    initials = words[0].substring(0, 2).toLocaleUpperCase();
  } else {
    initials = words[0].charAt(0) + words[1].charAt(0);
  }

  return initials;
};
