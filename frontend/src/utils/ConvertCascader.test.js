import { ListObject } from 'domain/setting/listsTypes';
import { convertListObjectToCascaderOptions } from './ConvertCascader';

// id: string;
//   value: string;
//   default: boolean;
//   measurement?: string;
//   meta?: MetaData | undefined;
//   children?: ListObjectItem[];

const testListObject = {
  first: {
    id: '1111',
    value: '1111',
    default: true,
    measurement: 'Length',
    listType: 'Test',
    status: 'Active',
    meta: {
      unit: '',
      type: '',
      measurement: '',
    },
    children: [
      {
        id: '2222',
        value: '2222',
        default: true,
        measurement: 'Length',
        listType: 'Test',
        status: 'Active',
        meta: {
          unit: '',
          type: '',
          measurement: '',
        },
      },
      {
        id: '3333',
        value: '3333',
        default: true,
        measurement: 'Length',
        listType: 'Test',
        status: 'Active',
        meta: {
          unit: '',
          type: '',
          measurement: '',
        },
      },
    ],
  },
  second: {
    id: '4444',
    value: '4444',
    default: true,
    measurement: 'Length',
    listType: 'Test',
    status: 'Active',
    meta: {
      unit: '',
      type: '',
      measurement: '',
    },
    children: [],
  },
};

describe('The cascader builder', () => {
  it('Builds a cascader menu from a ListObject', () => {
    const menu = convertListObjectToCascaderOptions(testListObject, 'Test', []);
    expect(menu.value).toBe('Test');
    expect(menu.children[0].value).toBe('1111');
    expect(menu.children[0].children[0].value).toBe('2222');
    expect(menu.children[1].value).toBe('4444');
  });

  it('Disables used entries', () => {
    const menu = convertListObjectToCascaderOptions(testListObject, 'Test', ['2222']);
    expect(menu.value).toBe('Test');
    expect(menu.children[0].value).toBe('1111');
    expect(menu.children[0].children[0].disabled).toBeTruthy();
    expect(menu.children[1].value).toBe('4444');
  });
});
