import moment from 'moment';
import log from 'modules/shared/logger';
import { MOCK_CATALOG, MOCK_CATALOG_COMPONENTS, MOCK_FREEMIUM, MOCK_NON_FREEMIUM, MOCK_FAKE, MOCK_FREEMIUM_DRAWINGS } from './fixtures/billing';
import { isFreemiumPlan, canAddDrawings, getTrialLimit } from './SubscriptionUtils';

const warnSpy = jest.spyOn(log, 'warn');

describe('isFreemiumPlan', () => {
  it('Returns false if no components are passed', () => {
    const freemium = isFreemiumPlan();
    expect(freemium).toBeFalsy();
  });

  it('Returns false if the components have no free price option', () => {
    const freemium = isFreemiumPlan('Annually', [MOCK_NON_FREEMIUM], MOCK_CATALOG_COMPONENTS);
    expect(freemium).toBeFalsy();
  });

  it('returns true if the pending and billing amounts are 0', () => {
    const freemium = isFreemiumPlan('Monthly', [MOCK_FREEMIUM], MOCK_CATALOG_COMPONENTS);
    expect(freemium).toBeTruthy();
  });
});

describe('canAddDrawings', () => {
  it('Warns if subscription is missing', () => {
    warnSpy.mockReset();
    const allowed = canAddDrawings({}, [MOCK_NON_FREEMIUM], MOCK_CATALOG_COMPONENTS, 1);
    expect(allowed).toBeTruthy();
    expect(warnSpy).toHaveBeenCalledTimes(1);
  });

  it('Warns if subscription is not freemium', () => {
    warnSpy.mockReset();
    const allowed = canAddDrawings({ billingPeriod: 'Monthly', components: [MOCK_NON_FREEMIUM] }, [MOCK_NON_FREEMIUM], MOCK_CATALOG_COMPONENTS, 1);
    expect(allowed).toBeTruthy();
    expect(warnSpy).toHaveBeenCalledTimes(1);
  });

  it('returns true if parts or drawings are not being metered', () => {
    const allowed = canAddDrawings({ billingPeriod: 'Monthly', components: [MOCK_FAKE] }, [MOCK_FAKE], MOCK_CATALOG_COMPONENTS, 1);
    expect(allowed).toBeTruthy();
  });

  it('returns false if more parts would be used than the subscription allows', () => {
    const allowed = canAddDrawings({ billingPeriod: 'Monthly', components: [MOCK_FREEMIUM], partsUsed: 5 }, [MOCK_FREEMIUM], MOCK_CATALOG_COMPONENTS, 1);
    expect(allowed).toBeFalsy();
  });

  it('returns false if more drawings would be used than the subscription allows', () => {
    const allowed = canAddDrawings({ billingPeriod: 'Monthly', components: [MOCK_FREEMIUM_DRAWINGS], drawingsUsed: 5 }, [MOCK_FREEMIUM_DRAWINGS], MOCK_CATALOG_COMPONENTS, 1);
    expect(allowed).toBeFalsy();
  });

  it('returns true if more parts would be used than the subscription allows', () => {
    const allowed = canAddDrawings({ billingPeriod: 'Monthly', components: [MOCK_FREEMIUM], partsUsed: 1 }, [MOCK_FREEMIUM], MOCK_CATALOG_COMPONENTS, 1);
    expect(allowed).toBeTruthy();
  });

  it('returns true if more drawings would be used than the subscription allows', () => {
    const allowed = canAddDrawings({ billingPeriod: 'Monthly', components: [MOCK_FREEMIUM_DRAWINGS], drawingsUsed: 1 }, [MOCK_FREEMIUM_DRAWINGS], MOCK_CATALOG_COMPONENTS, 1);
    expect(allowed).toBeTruthy();
  });
});

describe('getTrialLimit', () => {
  it('warns if no subscription is passed', () => {
    warnSpy.mockReset();
    const limit = getTrialLimit();
    expect(limit).toBe(0);
    expect(warnSpy).toHaveBeenCalledTimes(1);
  });

  it('warns if freemium and no freemium component is found', () => {
    warnSpy.mockReset();
    const limit = getTrialLimit({}, [MOCK_NON_FREEMIUM], true, MOCK_CATALOG);
    expect(limit).toBe(0);
    expect(warnSpy).toHaveBeenCalledTimes(1);
  });

  it('warns if non-freemium subscription is not a trial', () => {
    warnSpy.mockReset();
    const limit = getTrialLimit({ status: 'active' }, [MOCK_NON_FREEMIUM], false, MOCK_CATALOG);
    expect(limit).toBe(0);
    expect(warnSpy).toHaveBeenCalledTimes(1);
  });

  it('warns if no end date is found for time trial', () => {
    warnSpy.mockReset();
    const limit = getTrialLimit({ status: 'trialing' }, [MOCK_NON_FREEMIUM], false, MOCK_CATALOG);
    expect(limit).toBe(0);
    expect(warnSpy).toHaveBeenCalledTimes(1);
  });

  it('returns the remaining allocation for freemium trial', () => {
    const limit = getTrialLimit({ billingPeriod: 'Monthly', providerFamilyHandle: 'inspectionxpert-cloud', components: [MOCK_FREEMIUM], partsUsed: 1 }, [MOCK_FREEMIUM], true, MOCK_CATALOG);
    expect(limit).toBe(4);
  });

  it('returns the remaining days for time trial', () => {
    const limit = getTrialLimit({ providerFamilyHandle: 'inspectionxpert-cloud', status: 'trialing', nextAssessmentAt: moment().add(1, 'days') }, [MOCK_NON_FREEMIUM], false, MOCK_CATALOG);
    expect(limit).toBe(1);
  });
});
