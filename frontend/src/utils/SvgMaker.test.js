import { BrandColors } from 'view/global/defaults';
import { svgMaker, numberedSvgMaker, numberedLeaderSvgMaker, getSvgStrokeDashArray } from './SvgMaker';

const shapes = ['Circle', 'Triangle', 'Diamond', 'Hexagon'];
const types = ['hollow', 'solid'];

describe('The SVG Maker', () => {
  it('Should return an SVG', () => {
    const svg = svgMaker('red', 'red', 'circle');
    expect(svg.type).toEqual('svg');
  });

  it('Should return the default Style SVG for invalid parameters', () => {
    const svg = svgMaker('12', 'sdv', 1, 'sdv', 'Dashed');
    expect(svg.type).toEqual('svg');
    expect(svg.props.id).toEqual('coral-circle-solid');
  });

  it('Should return a Hollow Purple Triangle', () => {
    const svg = svgMaker('Transparent', 'Purple', 1, 'Triangle', 'Solid');
    expect(svg.type).toEqual('svg');
    expect(svg.props.id).toEqual('purple-triangle-hollow');
  });

  it('Should return a Solid Seafoam Diamond', () => {
    const svg = svgMaker('Seafoam', 'Seafoam', 1, 'Diamond', 'Dotted');
    expect(svg.type).toEqual('svg');
    expect(svg.props.id).toEqual('seafoam-diamond-solid');
  });

  it('Should Create SVGs for All of the Combos', () => {
    const temp = { ...BrandColors };
    delete temp.Transparent;
    const colors = Object.keys(temp);
    colors.forEach((color) => {
      shapes.forEach((shape) => {
        types.forEach((type) => {
          const svg = type === 'solid' ? svgMaker(color, color, 1, shape, 'Solid') : svgMaker('Transparent', color, 1, shape, 'Dashed');
          expect(svg.type).toEqual('svg');
          expect(svg.props.id).toEqual(`${`${color.toLowerCase()}-${shape.toLowerCase()}-${type}`}`);
        });
      });
    });
  });
});

describe('The SVG Maker Numbered', () => {
  it('Should return an SVG', () => {
    const svg = numberedSvgMaker('red', 'red', 'White', 'circle', '7');
    expect(svg.type).toEqual('svg');
  });

  it('Should return the default Style SVG for invalid parameters', () => {
    const svg = numberedSvgMaker('a', 'b', '7', 'octagon', '7');
    expect(svg.type).toEqual('svg');
    expect(svg.props.id).toEqual('coral-circle-solid');
  });

  it('Should return a Hollow Purple Triangle', () => {
    const svg = numberedSvgMaker('Transparent', 'Purple', 1, 'Purple', 'Triangle', 'Dashed', '7');
    expect(svg.type).toEqual('svg');
    expect(svg.props.id).toEqual('purple-triangle-hollow');
  });

  it('Should return a Solid Seafoam Diamond', () => {
    const svg = numberedSvgMaker('Seafoam', 'Seafoam', 1, 'White', 'Diamond', 'Solid', '7');
    expect(svg.type).toEqual('svg');
    expect(svg.props.id).toEqual('seafoam-diamond-solid');
  });

  it('Should return with Text', () => {
    const svg = numberedSvgMaker('Seafoam', 'Seafoam', 2, 'White', 'Diamond', 'Dotted', '7');
    expect(svg.type).toEqual('svg');
    expect(svg.props.children[1].type === 'text');
    expect(svg.props.children[1].props.children).toEqual('7');
  });

  it('Should Create Numbered SVGs for All of the Combos', () => {
    const temp = { ...BrandColors };
    delete temp.Transparent;
    const colors = Object.keys(temp);
    for (let i = 0; i < colors.length; i++) {
      for (let j = 0; j < shapes.length; j++) {
        for (let k = 0; k < types.length; k++) {
          const svg = types[k] === 'solid' ? numberedSvgMaker(colors[i], colors[i], 1, 'White', shapes[j], 'Solid', i.toString()) : numberedSvgMaker('Transparent', colors[i], 1, colors[i], shapes[j], 'Dashed', i.toString());
          expect(svg.type).toEqual('svg');
          expect(svg.props.children[1].type === 'text');
          expect(svg.props.children[1].props.children).toEqual(i.toString());
          expect(svg.props.id).toEqual(`${`${colors[i].toLowerCase()}-${shapes[j].toLowerCase()}-${types[k]}`}`);
        }
      }
    }
  });
});

describe('The SVG Maker Numbered with Leaders', () => {
  it('Should return an SVG', () => {
    const svg = numberedLeaderSvgMaker('red', 'red', 1, 'White', 'Circle', 'Coral', 'Solid', 'Solid', '7');
    expect(svg.type).toEqual('svg');
  });

  it('Should return the default Style SVG for invalid parameters', () => {
    const svg = numberedLeaderSvgMaker('a', 'b', 10, 'octagon', 'Solid', 'Dashed', 'Dashed', 'Dashed', '7');
    expect(svg.type).toEqual('svg');
    expect(svg.props.id).toEqual('coral-circle-solid');
  });

  it('Should return a Hollow Purple Triangle', () => {
    const svg = numberedLeaderSvgMaker('Transparent', 'Purple', 1, 'Purple', 'Triangle', 'Coral', 'Dashed', 'Dashed', '7');
    expect(svg.type).toEqual('svg');
    expect(svg.props.id).toEqual('purple-triangle-hollow');
  });

  it('Should return a Solid Seafoam Diamond', () => {
    const svg = numberedLeaderSvgMaker('Seafoam', 'Seafoam', 1, 'White', 'Diamond', 'Coral', 'Dashed', 'Solid', '7');
    expect(svg.type).toEqual('svg');
    expect(svg.props.id).toEqual('seafoam-diamond-solid');
  });

  it('Should return with Text', () => {
    const svg = numberedLeaderSvgMaker('Seafoam', 'Seafoam', 2, 'White', 'Diamond', 'White', 'Dotted', 'Dotted', '7');
    expect(svg.type).toEqual('svg');
    expect(svg.props.children[2].type === 'text');
    expect(svg.props.children[2].props.children).toEqual('7');
  });

  it('Should Create Numbered SVGs for All of the Combos', () => {
    const temp = { ...BrandColors };
    delete temp.Transparent;
    const colors = Object.keys(temp);
    for (let i = 0; i < colors.length; i++) {
      for (let j = 0; j < shapes.length; j++) {
        for (let k = 0; k < types.length; k++) {
          const svg =
            types[k] === 'solid'
              ? numberedLeaderSvgMaker(colors[i], colors[i], 1, 'White', shapes[j], 'White', 'Solid', 'Solid', i.toString())
              : numberedLeaderSvgMaker('Transparent', colors[i], 1, colors[i], shapes[j], 'White', 'Solid', 'Solid', i.toString());
          expect(svg.type).toEqual('svg');
          expect(svg.props.children[1].type === 'line');
          expect(svg.props.children[2].type === 'text');
          expect(svg.props.children[2].props.children).toEqual(i.toString());
          expect(svg.props.id).toEqual(`${`${colors[i].toLowerCase()}-${shapes[j].toLowerCase()}-${types[k]}`}`);
        }
      }
    }
  });
});

describe('Builds the dash array', () => {
  it('returns as expected for dashed', () => {
    const arr = getSvgStrokeDashArray('Dashed');
    expect(arr).toBe('3 1');
  });
  it('returns as expected for solid', () => {
    const arr = getSvgStrokeDashArray('Solid');
    expect(arr).toBe('1 0');
  });
  it('returns as expected for dotted', () => {
    const arr = getSvgStrokeDashArray('Dotted');
    expect(arr).toBe('1');
  });
  it('returns as solid for invalid inputs', () => {
    const arr = getSvgStrokeDashArray('esfef');
    expect(arr).toBe('1 0');
  });
});
