import { getRevisionIndex } from './PartUtilities';

const parts = {
  current: {
    revision: 'current',
    createdAt: '2021-01-01',
  },
  previous: {
    revision: 'previous',
    createdAt: '2020-01-01',
  },
  original: {
    revision: 'original',
    createdAt: '2019-01-01',
  },
};
const drawings = [
  { id: 'current', originalDrawing: 'OGD', createdAt: '2021-01-01' },
  { id: 'previous', originalDrawing: 'OGD', createdAt: '2020-01-01' },
  { id: 'original', originalDrawing: 'OGD', createdAt: '2019-01-01' },
];

describe('getRevisionIndex', () => {
  it('returns -1 if not found for any reason', () => {
    const { index, type } = getRevisionIndex({});
    expect(index).toBe(-1);
    expect(type).toBe(null);
  });

  it('returns index of part revision', () => {
    const { index, type } = getRevisionIndex({ parts, partId: 'original' });
    expect(index).toBe(2);
    expect(type).toBe('part');
  });

  it('returns index of drawing revision', () => {
    const { index, type } = getRevisionIndex({ drawings, drawingId: 'previous' });
    expect(index).toBe(1);
    expect(type).toBe('drawing');
  });

  it('prioritizes part revision if both are available', () => {
    const { index, type } = getRevisionIndex({ parts, partId: 'original', drawings, drawingId: 'previous' });
    expect(index).toBe(2);
    expect(type).toBe('part');
  });
});
