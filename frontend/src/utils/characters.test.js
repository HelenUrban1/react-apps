const { lookup, matchPatternFor, frameText, CAD_CHARACTERS, TYPE_MAP, GDT_SYMBOLS, GDT_MODIFIERS, GDT_SYMBOLS_SML, GDT_MODIFIERS_SML } = require('./characters');

/*
function hexEncode(str) {
  let hex;
  let result = '';
  for (let i = 0; i < str.length; i++) {
    hex = str.charCodeAt(i).toString(16);
    result += `000${hex}`.slice(-4);
  }
  return result;
}

function hexDecode(hex) {
  const hexes = hex.match(/.{1,4}/g) || [];
  let back = '';
  for (let j = 0; j < hexes.length; j++) {
    back += String.fromCharCode(parseInt(hexes[j], 16));
  }
  return back;
}
*/

describe('Eng Notation Character Reference', () => {
  describe('.lookup', () => {
    it('should match large and small versions of characters', () => {
      let i;
      let resultLrg;
      let resultSml;
      for (i = 0; i < GDT_SYMBOLS.length; i++) {
        resultLrg = lookup(GDT_SYMBOLS[i]);
        resultSml = lookup(GDT_SYMBOLS_SML[i]);
        expect(resultLrg.name).toBe(resultSml.name);
      }
      for (i = 0; i < GDT_MODIFIERS.length; i++) {
        resultLrg = lookup(GDT_MODIFIERS[i]);
        resultSml = lookup(GDT_MODIFIERS_SML[i]);
        expect(resultLrg.name).toBe(resultSml.name);
      }
    });
  });

  describe('.matchPatternFor', () => {
    it('should match large chars to small and framed versions', () => {
      let result;
      CAD_CHARACTERS.forEach((charData) => {
        try {
          result = matchPatternFor(charData.char);
          expect(result.test(charData.char)).toBe(true);
          if (charData.char_sml) {
            expect(result.test(charData.char_sml)).toBe(true);
          }
          if (charData.framed_char) {
            expect(result.test(charData.framed_char)).toBe(true);
          }
        } catch (err) {
          console.log(charData);
          console.log(result);
          console.log(err);
          expect(false).toBe(true);
        }
      });
    });

    it('should match small chars to large and framed versions', () => {
      let result;
      CAD_CHARACTERS.forEach((charData) => {
        try {
          if (charData.char_sml) {
            result = matchPatternFor(charData.char_sml);
            expect(result.test(charData.char)).toBe(true);
            if (charData.framed_char) {
              expect(result.test(charData.framed_char)).toBe(true);
            }
          }
        } catch (err) {
          console.log(charData);
          console.log(result);
          console.log(err);
          expect(false).toBe(true);
        }
      });
    });

    it('should match framed chars to large and small versions', () => {
      let result;
      CAD_CHARACTERS.forEach((charData) => {
        try {
          if (charData.framed_char) {
            result = matchPatternFor(charData.framed_char);
            expect(result.test(charData.char)).toBe(true);
            if (charData.char_sml) {
              expect(result.test(charData.char_sml)).toBe(true);
            }
          }
        } catch (err) {
          console.log(charData);
          console.log(result);
          console.log(err);
          expect(false).toBe(true);
        }
      });
    });
  });

  describe('.frameText', () => {
    it('should find framed versions of all GD&T symbols', () => {
      let result;
      // prettier-ignore
      // eslint-disable-next-line
      const samples = [].concat(GDT_SYMBOLS, GDT_SYMBOLS_SML);
      samples.forEach((sample) => {
        try {
          result = lookup(sample);
          // console.log(result);
          // console.log(`'${result.name}' is ${sample}   ${result.framed_char}`);
          expect(result.name).toBeDefined();
          expect(result.char).toBeDefined();
          expect(result.framed_char).toBeDefined();
          // expect(result.desc).toBe('');
        } catch (err) {
          console.log(err);
          expect(false).toBe(true);
        }
      });
    });

    it('should find framed versions of all GD&T modifiers', () => {
      let result;
      // prettier-ignore
      // eslint-disable-next-line
      const samples = [].concat(GDT_MODIFIERS, GDT_MODIFIERS_SML);
      samples.forEach((sample) => {
        try {
          result = lookup(sample);
          // console.log(result);
          // console.log(`'${result.name}' is ${sample}   ${result.framed_char}`);
          expect(result.name).toBeDefined();
          expect(result.char).toBeDefined();
          expect(result.framed_char).toBeDefined();
          // expect(result.desc).toBe('');
        } catch (err) {
          console.log(err);
          expect(false).toBe(true);
        }
      });
    });

    it('should find framed versions of all alpha-numeric characters', () => {
      let result;
      // prettier-ignore
      const samples = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'.split('');
      samples.forEach((sample) => {
        try {
          result = lookup(sample);
          expect(result.char).toBeDefined();
          expect(result.framed_char).toBeDefined();
        } catch (err) {
          console.log(err);
          expect(false).toBe(true);
        }
      });
    });

    it('should find framed versions of supported punctuation', () => {
      let result;
      // prettier-ignore
      // const all_symbols = '!@#$%^&*()_+-={}|[]:"<>?;,./~`\'\\'.split('');
      // NOT IMPLEMENTED: !@#$&*={}:"<>?;'\
      const samples = '%^()_+-|[],./~`'.split('');
      samples.forEach((sample) => {
        try {
          result = lookup(sample);
          expect(result.char).toBeDefined();
          expect(result.framed_char).toBeDefined();
        } catch (err) {
          console.log(err);
          expect(false).toBe(true);
        }
      });
    });

    it('should find framed versions of CAD symbols', () => {
      let result;
      // prettier-ignore
      // eslint-disable-next-line
      const samples = [
        'μ',
        '°',
        '',
        '',
        '',
        ''
      ]
      /*
    TODO: Need framed symbols
      '',
      '',
      '',
      '',
      '',
      '',
      '',
      '',
      '',
      '',
      '',
      '',
      '',
      '',
      '',
      '',
      '',
      '',
      '',
      '',
      ''
    */
      samples.forEach((sample) => {
        try {
          // console.log(sample);
          result = lookup(sample);
          // console.log(result);
          expect(result.char).toBeDefined();
          expect(result.framed_char).toBeDefined();
        } catch (err) {
          console.log(err);
          expect(false).toBe(true);
        }
      });
    });

    it('should not fail when given plain text', () => {
      const res = frameText('test');
      expect(res).toEqual('test');
    });

    describe('Framing smoke tests', () => {
      it('should frame entire basic diminesions', () => {
        expect(frameText('|1.25±0.020|')).toBe('');
      });
      it('should frame entire GD&T frames', () => {
        expect(frameText('|⌓|0.020|A|B|C|')).toBe('');
      });
    });
  });

  describe('TYPE_MAP', () => {
    it('should return subtypes of type Geometric Tolerance', () => {
      TYPE_MAP['Geometric Tolerance'].forEach((subtype) => {
        // console.log(subtype);
        // console.log(`\t${subtype.group} ->  ${subtype.char}   ${subtype.name}`);
        expect(subtype.type).toBe('Geometric Tolerance');
        expect(subtype.name).toBeDefined();
        expect(subtype.char).toBeDefined();
        expect(subtype.framed_char).toBeDefined();
        expect(subtype.group).toBeDefined();
      });
    });
  });
});
