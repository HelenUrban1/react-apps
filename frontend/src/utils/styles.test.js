import { converRgbToHex, convertHexToRgb, styleToInline } from './styles';

describe('Style Object to Inline CSS', () => {
  it('should convert valid styles to inline css', () => {
    const output = styleToInline({ color: '#000', backgroundColor: '#fff' });
    expect(output).toBe('color: rgb(0, 0, 0); background-color: rgb(255, 255, 255);');
  });
  it('should ignore invalid styles', () => {
    const output = styleToInline({
      color: '#000',
      backgroundColor: '#fff',
      ham: 'cheese',
      bagel: 'bites',
    });
    expect(output).toBe('color: rgb(0, 0, 0); background-color: rgb(255, 255, 255);');
  });
});

describe('Can convert between RGB and Hex', () => {
  it('should convert from rgb to hex', () => {
    const hex = converRgbToHex([197, 197, 1]);
    expect(hex).toBe('#c5c501');
  });

  it('should convert from hex to rgb', () => {
    const rgb = convertHexToRgb('c5c501');
    expect(rgb).toStrictEqual([197, 197, 1, 1]);
  });
});
