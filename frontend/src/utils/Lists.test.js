import {
  convertToListObject,
  getCurrentList,
  getSingleLevelListValue,
  getListIdByValue,
  getListNameById,
  convertListToDataSource,
  updateLists,
  getValueOfHiddenType,
  getMeasurementUnits,
  getMeasurementsSelectOptions,
  organizeListItems,
} from './Lists';
import { testMethods, testTypes, testOperations, testClassifications, testMetric, testImperial, testIDs, fakeUpdate, editedClassification } from './listTestData';

const testList = [
  {
    id: '1',
    name: 'one',
    metadata: '{"measurement": "Length"}',
    default: true,
    status: 'Active',
    listType: 'Type',
    itemIndex: 0,
    siteId: null,
    userId: null,
    parentId: null,
  },
  {
    id: '2',
    name: 'two',
    metadata: '{"measurement": "Length"}',
    default: true,
    status: 'Active',
    listType: 'Type',
    itemIndex: 0,
    siteId: null,
    userId: null,
    parentId: '1',
  },
  {
    id: '3',
    name: 'three',
    metadata: '{"measurement": "Length"}',
    default: true,
    status: 'Active',
    listType: 'Type',
    itemIndex: 0,
    siteId: null,
    userId: null,
    parentId: '1',
  },
  {
    id: '4',
    name: 'four',
    metadata: '{"measurement": "Length"}',
    default: true,
    status: 'Active',
    listType: 'Type',
    itemIndex: 0,
    siteId: null,
    userId: null,
    parentId: null,
  },
];

const testListFull = [
  {
    id: '1',
    name: 'one',
    metadata: '{"measurement": "Length"}',
    default: true,
    status: 'Active',
    listType: 'Type',
    itemIndex: 0,
    siteId: null,
    userId: null,
    parentId: null,
  },
  {
    id: '2',
    name: 'two',
    metadata: '{"measurement": "Length"}',
    default: true,
    status: 'Active',
    listType: 'Type',
    itemIndex: 0,
    siteId: null,
    userId: null,
    parentId: '1',
  },
  {
    id: '3',
    name: 'Class',
    metadata: null,
    default: true,
    status: 'Active',
    listType: 'Classification',
    itemIndex: 0,
    siteId: null,
    userId: null,
    parentId: null,
  },
  {
    id: '4',
    name: 'Class2',
    metadata: null,
    default: true,
    status: 'Active',
    listType: 'Classification',
    itemIndex: 1,
    siteId: null,
    userId: null,
    parentId: null,
  },
  {
    id: '5',
    name: 'Method',
    metadata: null,
    default: true,
    status: 'Active',
    listType: 'Inspection_Method',
    itemIndex: 0,
    siteId: null,
    userId: null,
    parentId: null,
  },
  {
    id: '6',
    name: 'Operation',
    metadata: null,
    default: true,
    status: 'Active',
    listType: 'Operation',
    itemIndex: 0,
    siteId: null,
    userId: null,
    parentId: null,
  },
  {
    id: '7',
    name: 'Operation',
    metadata: null,
    default: true,
    status: 'Active',
    listType: 'Operation',
    itemIndex: 0,
    siteId: null,
    userId: null,
    parentId: null,
  },
  {
    id: '8',
    name: 'Metric',
    metadata: '{"unit":"N4","type":"Metric","measurement":"roughness_grade"}',
    default: true,
    status: 'Active',
    listType: 'Unit_of_Measurement',
    itemIndex: 0,
    siteId: null,
    userId: null,
    parentId: null,
  },
  {
    id: '9',
    name: 'Imperial',
    metadata: '{"unit":"N4","type":"Imperial","measurement":"roughness_grade"}',
    default: true,
    status: 'Active',
    listType: 'Unit_of_Measurement',
    itemIndex: 2,
    siteId: null,
    userId: null,
    parentId: null,
  },
];

describe('The List utilities', () => {
  it('Converts retrieved list items from the DB into a ListObject', () => {
    const listObj = convertToListObject(testList);
    expect(listObj['1'].value).toBe('one');
    expect(Object.keys(listObj['1'].children).length).toBe(2);
    expect(listObj['1'].children['2'].value).toBe('two');
    expect(listObj['4'].value).toBe('four');
  });

  it('Stores the metadata as an object', () => {
    const listObj = convertToListObject(testList);
    expect(listObj['1'].meta.measurement).toBe('Length');
  });

  describe('gets the current list', () => {
    it('successfully with valid input', () => {
      let listObj = getCurrentList('Classification', testClassifications, testMethods, testTypes, testOperations, testImperial, testMetric);
      expect(listObj[testIDs.Critical].listType).toBe('Classification');
      listObj = getCurrentList('Type', testClassifications, testMethods, testTypes, testOperations, testImperial, testMetric);
      expect(listObj[testIDs.Dimension].listType).toBe('Type');
      listObj = getCurrentList('Inspection_Method', testClassifications, testMethods, testTypes, testOperations, testImperial, testMetric);
      expect(listObj[testIDs.Caliper].listType).toBe('Inspection_Method');
      listObj = getCurrentList('Operation', testClassifications, testMethods, testTypes, testOperations, testImperial, testMetric);
      expect(listObj[testIDs.Drilling].listType).toBe('Operation');
    });
    it('returns null with invalid inputs', () => {
      const listObj = getCurrentList('Invalid', testClassifications, testMethods, testTypes, testOperations, testImperial, testMetric);
      expect(listObj).toBe(null);
    });
  });

  describe('gets the value from a single level list', () => {
    it('gets a Classification', () => {
      const ret = getSingleLevelListValue(testClassifications, testIDs.Critical);
      expect(ret).toBe('Critical');
    });

    it('gets a Method', () => {
      const ret = getSingleLevelListValue(testMethods, testIDs.Caliper);
      expect(ret).toBe('Caliper');
    });

    it('gets an Operation', () => {
      const ret = getSingleLevelListValue(testOperations, testIDs.Drilling);
      expect(ret).toBe('Drilling');
    });

    it('returns undefined if nothing matches', () => {
      const ret = getSingleLevelListValue(testOperations, testIDs.Critical);
      expect(ret).toBe(undefined);
    });
  });

  describe('gets the type ID from names', () => {
    it('gets the correct IDs', () => {
      let typeId = getListIdByValue({ list: testTypes, value: 'Dimension' });
      let subtypeId = getListIdByValue({ list: testTypes[typeId].children, value: 'Diameter' });
      expect(typeId).toBe(testIDs.Dimension);
      expect(subtypeId).toBe(testIDs.Diameter);
      typeId = getListIdByValue({ list: testTypes, value: 'Geometric Tolerance' });
      subtypeId = getListIdByValue({ list: testTypes[typeId].children, value: 'Position' });
      expect(typeId).toBe(testIDs['Geometric Tolerance']);
      expect(subtypeId).toBe(testIDs.Position);
    });
    it('defaults to not found without a match', () => {
      const typeId = getListIdByValue({ list: testTypes, value: 'Dim' });
      expect(typeId).toBe('Not found');
    });
    it('defaults to not found with the wrong list', () => {
      const typeId = getListIdByValue({ list: testClassifications, value: 'Dimension' });
      expect(typeId).toBe('Not found');
    });
    it('defaults to passed value if provided', () => {
      const typeId = getListIdByValue({
        list: testTypes,
        value: 'Dim',
        defaultValue: 'fa3284ed-ad3e-43c9-b051-87bd2e95e927',
      });
      expect(typeId).toBe(testIDs.Note);
    });
  });

  describe('gets the type Names from the item', () => {
    it('gets the correct names', () => {
      const type = getListNameById({ list: testTypes, value: testIDs.Dimension });
      const subtype = getListNameById({
        list: testTypes[testIDs.Dimension].children,
        value: testIDs.Length,
      });
      expect(type).toBe('Dimension');
      expect(subtype).toBe('Length');
    });
    it('returns not found without a match', () => {
      const type = getListNameById({ list: testTypes, value: testIDs.Caliper });
      const subtype = getListNameById({
        list: testTypes,
        parentId: testIDs.Caliper,
        value: testIDs.Length,
      });
      expect(type).toBe('Not found');
      expect(subtype).toBe('Not found');
    });
  });

  describe('converts a single depth list to an array of IDs', () => {
    it('gets an array of IDs', () => {
      const ret = convertListToDataSource(testClassifications);
      expect(ret[0].id).toBe(`Classification-${testIDs.Incidental}`);
      expect(ret[3].id).toBe(`Classification-${testIDs.Critical}`);
    });
    it('gets an empty array if passed null', () => {
      const ret = convertListToDataSource(null);
      expect(ret.length).toBe(0);
    });
  });

  describe('updates lists', () => {
    it('does not error out when updating lists', () => {
      updateLists('Classification', testClassifications, editedClassification, fakeUpdate);
      expect(true).toBeTruthy();
    });
  });

  describe('gets missing cascader labels for hidden entries', () => {
    it('returns type and subtype for 2 length array', () => {
      const ret = getValueOfHiddenType(testTypes, [testIDs.Dimension, testIDs.Length]);
      expect(ret.type).toBe('Dimension');
      expect(ret.subtype).toBe('Length');
    });

    it('returns type and subtype for 3 length array', () => {
      const ret = getValueOfHiddenType(testTypes, ['Type', testIDs.Dimension, testIDs.Length]);
      expect(ret.type).toBe('Dimension');
      expect(ret.subtype).toBe('Length');
    });

    it('returns not found for missing values', () => {
      const ret = getValueOfHiddenType(testTypes, ['Type', 'invalid', 'invalid']);
      expect(ret.type).toBe('Not found');
      expect(ret.subtype).toBe('Not found');
    });

    it('returns classifications', () => {
      const ret = getValueOfHiddenType(testClassifications, [testIDs.Major]);
      expect(ret.type).toBe('Major');
      expect(ret.subtype).toBe('Not found');
    });
  });

  describe('builds a select list for measurement options', () => {
    it('returns a list of measurements', () => {
      const ret = getMeasurementsSelectOptions(testMetric, testImperial);
      expect(ret[0].name).toBe('None');
      expect(ret[1].name).toBe('length');
      expect(ret[1].units[0].name).toBe('millimeter');
    });

    it('returns an empty array with a none option', () => {
      const ret = getMeasurementsSelectOptions(null, null);
      expect(ret[0].name).toBe('None');
      expect(ret[1]).toBe(undefined);
    });
  });

  describe('gets the units for a given measurement type', () => {
    it('returns the units', () => {
      const ret = getMeasurementUnits(getMeasurementsSelectOptions(testMetric, testImperial), 'length');
      expect(ret.metric[0]).toBe('mm');
      expect(ret.imperial[0]).toBe('in');
    });
  });

  describe('filters the full list into the right sublists', () => {
    it('should return the various lists', () => {
      const { types, methods, operations, classifications, metricUnits, imperialUnits } = organizeListItems(testListFull);
      expect(Object.keys(types)).toHaveLength(1);
      expect(Object.keys(methods)).toHaveLength(1);
      expect(Object.keys(operations)).toHaveLength(2);
      expect(Object.keys(classifications)).toHaveLength(2);
      expect(Object.keys(metricUnits)).toHaveLength(1);
      expect(Object.keys(imperialUnits)).toHaveLength(1);
    });
  });
});
