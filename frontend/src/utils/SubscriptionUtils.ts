import moment from 'moment';
import log from 'modules/shared/logger';
import { BillingPeriodEnum, Catalog, CatalogComponent, Component, ComponentPrice, PricePoint, Subscription } from 'domain/setting/subscriptionTypes';
import { getComponentMetric } from './DataCalculation';

const getFreemiumMeteredUsed = (subscription: Subscription, component: Component) => {
  const unit = getComponentMetric(component.unit_name);
  const used = subscription ? (subscription[`${unit}sUsed` as keyof Subscription] as number) || 0 : 0;
  return used;
};

export const getCurrentPrice = (interval: BillingPeriodEnum, prices: PricePoint[], quantity?: number) => {
  const pricePoint = prices
    .filter((price) => {
      const { handle } = price;
      return handle && handle.slice(handle.indexOf('_') + 1) === interval.toLowerCase();
    })
    .sort((a, b) => {
      const aDate = a.handle.slice(1, a.handle.indexOf('_'));
      const bDate = b.handle.slice(1, b.handle.indexOf('_'));
      if (moment(aDate).isBefore(moment(bDate))) {
        return -1;
      }
      if (moment(bDate).isBefore(moment(aDate))) {
        return 1;
      }
      return 0;
    })
    .slice(0, 1)[0];
  if (quantity && pricePoint) {
    for (let index = 0; index < pricePoint.prices.length; index++) {
      const price = pricePoint.prices[index];
      if (price.starting_quantity <= quantity && (!price.ending_quantity || price.ending_quantity >= quantity)) {
        return price;
      }
    }
  }
  return pricePoint;
};

export const getComponentPrice = (prices: ComponentPrice[], quantity: number) => {
  let cost = 0;
  let id;
  let handle;
  prices.forEach((step) => {
    if (quantity >= step.starting_quantity && (!step.ending_quantity || quantity <= step.ending_quantity)) {
      cost = parseFloat(step.unit_price) * 100;
      id = step.id.toString();
      handle = step.price_point_handle;
    }
  });
  return { cost, id, handle };
};

const getFreemiumComponent = (billingPeriod: BillingPeriodEnum, component: Component, product: { [key: string]: CatalogComponent }) => {
  if (!component || !product || !component.component_handle || !product[component.component_handle]) {
    return false;
  }
  if (component.unit_name === 'part') return true;
  const prices = getCurrentPrice(billingPeriod || 'Annually', product[component.component_handle || 'none']?.prices || {});
  if (!prices) {
    return false;
  }
  const freemiumComp = (prices as PricePoint).prices.find((price) => price.unit_price === '0.0');
  if (freemiumComp) {
    return freemiumComp;
  }
  return false;
};

export const isFreemiumPlan = (billingPeriod: BillingPeriodEnum, components: Component[], catalog: { [key: string]: CatalogComponent }) => {
  if (!components || !catalog) {
    return false;
  }
  for (let index = 0; index < components.length; index++) {
    const comp = components[index];
    // Filter out components for internal use, such as the internal_account $0 component
    if (comp.component_handle && !comp.component_handle.includes('internal')) {
      const free = getFreemiumComponent(billingPeriod, comp, catalog);
      if (free) {
        return free;
      }
    }
  }
  return false;
};

export const canAddDrawings = (subscription: Subscription | null, components: Component[], catalog: { [key: string]: CatalogComponent }, amount?: number) => {
  if (!subscription || !isFreemiumPlan(subscription.billingPeriod || 'Annually', components, catalog)) {
    log.warn('Did not change drawings for non-freemium subscription');
    return true;
  }
  const increase = amount || 1;
  const parts = components.find((comp) => comp.unit_name.includes('part'));
  const drawings = components.find((comp) => comp.unit_name.includes('drawing'));
  if (parts) {
    const freemium = getFreemiumComponent(subscription.billingPeriod || 'Annually', parts, catalog);
    if (freemium) {
      const used = getFreemiumMeteredUsed(subscription, parts);
      const newTotal = used + increase;
      if (parts.allocated_quantity < newTotal) {
        return false;
      }
    }
  }
  if (drawings) {
    const freemium = getFreemiumComponent(subscription.billingPeriod || 'Annually', drawings, catalog);
    if (freemium) {
      const used = getFreemiumMeteredUsed(subscription, drawings);
      const newTotal = used + increase;
      if (drawings.allocated_quantity < newTotal) {
        return false;
      }
    }
  }
  return true;
};

export const getTrialLimit = (subscription: Subscription, components: Component[], freemium: boolean, catalog: Catalog) => {
  if (!subscription) {
    log.warn('No subscription found');
    return 0;
  }

  if (freemium) {
    const productInfo = catalog[subscription?.providerFamilyHandle || 'null'];
    for (let index = 0; index < components.length; index++) {
      const comp = components[index];
      const free = getFreemiumComponent(subscription.billingPeriod || 'Annually', comp, productInfo?.components);
      if (free) {
        const used = getFreemiumMeteredUsed(subscription, comp);
        if (comp?.allocated_quantity && comp.allocated_quantity >= used) {
          return comp.allocated_quantity - used;
        }
      }
    }
    log.warn('No freemium component found for freemium subscription');
    return 0;
  }
  if (!subscription?.status?.toLowerCase().includes('trial')) {
    log.warn('Subscription is not a trial');
    return 0;
  }
  if (!subscription.nextAssessmentAt && !subscription.trialEndedAt) {
    log.warn('Trial end date not found');
    return 0;
  }
  const today = moment().startOf('day');
  const trialEnds = moment(subscription.nextAssessmentAt || subscription.trialEndedAt).endOf('day');
  return trialEnds.diff(today, 'days');
};
