import config from 'config';
import { Part } from 'domain/part/partTypes';
import { DrawingSheet } from '../../graphql/drawing_sheet';

export interface AutoballoonValues {
  found: number;
  accepted: number;
  rejected: number;
  acceptedTimeout: number;
  reviewed: number;
  reviewedTimeout: number;
  createdAt: Date | null | undefined;
}

/**
 * Utility function to check if auto-ballooning has completed. It checks if the accepted, rejected and timed-out captures are equel or greater than the originally found captures.
 * @param autoballoonValues
 * @param part
 * @returns True is auto-ballooning has completed. False if auto-ballooning has not completed.
 */
export const isAutoballooningDone = (autoballoonValues: AutoballoonValues, part: Part | null) => {
  if (!autoballoonValues) return false;

  const totalProcessed = autoballoonValues.accepted + autoballoonValues.rejected + autoballoonValues.acceptedTimeout;
  // Captures been found and All the found captures been accepted or rejected
  let ballooned = autoballoonValues.found > 0 && totalProcessed >= autoballoonValues.found;

  // All the accepted captures been reviewed
  const reviewed = autoballoonValues.reviewed + autoballoonValues.reviewedTimeout >= autoballoonValues.accepted;

  return config.autoBallooning.useHumanReview ? ballooned && reviewed : ballooned;
};

/**
 * Utility function to reduce a Drawing Sheet into an Auto-balloon Values object.
 * @param accumulator
 * @param currentValue
 * @returns An Auto-balloon Values object containing the number of found captures and thier status.
 */
export const getAutoBalloonResults = (accumulator: AutoballoonValues, currentValue?: DrawingSheet) => {
  if (!currentValue || !accumulator) {
    return accumulator;
  }
  if (currentValue.foundCaptureCount) {
    accumulator.found += currentValue.foundCaptureCount;
  }
  if (currentValue.acceptedCaptureCount) {
    accumulator.accepted += currentValue.acceptedCaptureCount;
  }
  if (currentValue.acceptedTimeoutCaptureCount) {
    accumulator.acceptedTimeout += currentValue.acceptedTimeoutCaptureCount;
  }
  if (currentValue.rejectedCaptureCount) {
    accumulator.rejected += currentValue.rejectedCaptureCount;
  }
  if (currentValue.reviewedCaptureCount) {
    accumulator.reviewed += currentValue.reviewedCaptureCount;
  }
  if (currentValue.reviewedTimeoutCaptureCount) {
    accumulator.reviewedTimeout += currentValue.reviewedTimeoutCaptureCount;
  }
  return accumulator;
};

/**
 * Utility function to check if the auto-balloon process has timed out.
 * @param part
 * @returns True if the current time is more than 10 minutes after part was uploaded.
 */
export const autoBallooningTimedOut = (createdAt: Date) => {
  const dateDiffMins = Math.floor(Math.abs(new Date(createdAt).getTime() - new Date().getTime()) / 1000 / 60);
  // If it takes more than 10 mins to get captures, assume its not being autoballooned
  if (dateDiffMins > 10) return true;
  return false;
};

/**
 * Utility function to get the percentage progress for auto-ballooning a part.
 * @param autoballoonValues
 * @returns An integer between 0 and 100.
 */
export const getAutoBalloonProgress = (autoballoonValues: AutoballoonValues) => {
  if (autoballoonValues?.found < autoballoonValues?.accepted + autoballoonValues?.rejected + autoballoonValues?.acceptedTimeout) return 0.05;
  return Math.floor(((autoballoonValues?.accepted + autoballoonValues?.rejected + autoballoonValues?.acceptedTimeout) / autoballoonValues?.found) * 100);
};

/**
 * Utility function to get the percentage progress for Human Reviewing auto ballooned features.
 * For more information on Human Review see: https://ideagen.atlassian.net/wiki/spaces/IX/pages/39451394069/Human+Review
 * @param autoballoonValues
 * @returns An integer between 0 and 100.
 */
export const getAutoBalloonReviewProgress = (autoballoonValues: AutoballoonValues) => {
  return Math.floor(((autoballoonValues?.reviewed + autoballoonValues?.reviewedTimeout) / autoballoonValues.accepted) * 100);
};
