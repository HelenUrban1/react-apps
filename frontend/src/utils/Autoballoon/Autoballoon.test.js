import { isAutoballooningDone, getAutoBalloonResults, autoBallooningTimedOut, getAutoBalloonProgress, getAutoBalloonReviewProgress } from 'utils/Autoballoon/Autoballoon';

describe('Autoballoon utils', () => {
  describe('#isAutoballooningDone', () => {
    it('Adds capture count values across multiple sheets', () => {
      const sheets = [
        {
          foundCaptureCount: 2,
          acceptedCaptureCount: 4,
          acceptedTimeoutCaptureCount: 6,
          rejectedCaptureCount: 8,
          reviewedCaptureCount: 10,
          reviewedTimeoutCaptureCount: 12,
        },
        {
          foundCaptureCount: 1,
          acceptedCaptureCount: 3,
          acceptedTimeoutCaptureCount: 5,
          rejectedCaptureCount: 7,
          reviewedCaptureCount: 9,
          reviewedTimeoutCaptureCount: 11,
        },
      ];
      const result = sheets.reduce(getAutoBalloonResults, {
        found: 0,
        accepted: 0,
        acceptedTimeout: 0,
        rejected: 0,
        reviewed: 0,
        reviewedTimeout: 0,
        createdAt: new Date(),
      });
      expect(result.found).toBe(3);
      expect(result.accepted).toBe(7);
      expect(result.acceptedTimeout).toBe(11);
      expect(result.rejected).toBe(15);
      expect(result.reviewed).toBe(19);
      expect(result.reviewedTimeout).toBe(23);
    });
  });

  describe('#isAutoballooningDone', () => {
    it('Returns true if capture counts are complete', () => {
      const result = isAutoballooningDone(
        {
          found: 10,
          accepted: 7,
          acceptedTimeout: 0,
          rejected: 3,
          reviewed: 7,
          reviewedTimeout: 0,
          createdAt: new Date(),
        },
        null,
      );
      expect(result).toBe(true);
    });

    it('returns false if no captures are found', () => {
      const result = isAutoballooningDone(
        {
          found: 0,
          accepted: 7,
          acceptedTimeout: 0,
          rejected: 3,
          reviewed: 7,
          reviewedTimeout: 0,
          createdAt: new Date(),
        },
        null,
      );
      expect(result).toBe(false);
    });

    it('returns false if all captures are not accepted or rejected', () => {
      const result = isAutoballooningDone(
        {
          found: 10,
          accepted: 7,
          acceptedTimeout: 0,
          rejected: 2,
          reviewed: 7,
          reviewedTimeout: 0,
          createdAt: new Date(),
        },
        null,
      );
      expect(result).toBe(false);
    });

    // Test skipped while review not required to complete autoballooning
    it.skip('returns false if all accepted captures are not reviewed', () => {
      const result = isAutoballooningDone(
        {
          found: 10,
          accepted: 7,
          acceptedTimeout: 0,
          rejected: 3,
          reviewed: 6,
          reviewedTimeout: 0,
          createdAt: new Date(),
        },
        null,
      );
      expect(result).toBe(false);
    });
  });

  describe('#getAutoBalloonResults', () => {
    it('Correctly accumulates auto-balloon values from multiple sheets', () => {
      const sheets = [
        {
          foundCaptureCount: 2,
          acceptedCaptureCount: 4,
          acceptedTimeoutCaptureCount: 6,
          rejectedCaptureCount: 8,
          reviewedCaptureCount: 10,
          reviewedTimeoutCaptureCount: 12,
        },
        {
          foundCaptureCount: 1,
          acceptedCaptureCount: 3,
          acceptedTimeoutCaptureCount: 5,
          rejectedCaptureCount: 7,
          reviewedCaptureCount: 9,
          reviewedTimeoutCaptureCount: 11,
        },
      ];
      const result = sheets.reduce(getAutoBalloonResults, {
        found: 0,
        accepted: 0,
        acceptedTimeout: 0,
        rejected: 0,
        reviewed: 0,
        reviewedTimeout: 0,
        createdAt: new Date(),
      });
      expect(result.found).toBe(3);
      expect(result.accepted).toBe(7);
      expect(result.acceptedTimeout).toBe(11);
      expect(result.rejected).toBe(15);
      expect(result.reviewed).toBe(19);
      expect(result.reviewedTimeout).toBe(23);
    });

    it('Does not modify the accumulator if the current value is undefined', () => {
      const result = getAutoBalloonResults(
        {
          found: 10,
          accepted: 7,
          acceptedTimeout: 0,
          rejected: 3,
          reviewed: 7,
          reviewedTimeout: 0,
          createdAt: new Date('2023-11-27T15:11:28.832Z'),
        },
        undefined,
      );
      expect(result).toEqual({
        found: 10,
        accepted: 7,
        acceptedTimeout: 0,
        rejected: 3,
        reviewed: 7,
        reviewedTimeout: 0,
        createdAt: new Date('2023-11-27T15:11:28.832Z'),
      });
    });

    it('Handles undefined current value gracefully', () => {
      const result = getAutoBalloonResults(
        {
          found: 2,
          accepted: 4,
          acceptedTimeout: 6,
          rejected: 8,
          reviewed: 10,
          reviewedTimeout: 12,
          createdAt: new Date('2023-11-27T15:11:28.832Z'),
        },
        undefined,
      );
      expect(result).toEqual({
        found: 2,
        accepted: 4,
        acceptedTimeout: 6,
        rejected: 8,
        reviewed: 10,
        reviewedTimeout: 12,
        createdAt: new Date('2023-11-27T15:11:28.832Z'),
      });
    });
  });

  describe('#autoBallooningTimedOut', () => {
    it('Returns true if more than 10 minutes have passed since the part was created', () => {
      const createdAt = new Date(new Date().getTime() - 11 * 60 * 1000);
      const result = autoBallooningTimedOut(createdAt);
      expect(result).toBe(true);
    });

    it('Returns false if less than 10 minutes have passed since the part was created', () => {
      const createdAt = new Date();
      const result = autoBallooningTimedOut(createdAt);
      expect(result).toBe(false);
    });
  });

  describe('#getAutoBalloonProgress', () => {
    it('Returns 0.05 if found captures are greater than processed captures', () => {
      const autoballoonValues = {
        found: 5,
        accepted: 7,
        acceptedTimeout: 2,
        rejected: 1,
        reviewed: 6,
        reviewedTimeout: 0,
        createdAt: new Date(),
      };
      const result = getAutoBalloonProgress(autoballoonValues);
      expect(result).toBe(0.05);
    });

    it('Calculates and returns the correct percentage progress', () => {
      const autoballoonValues = {
        found: 10,
        accepted: 7,
        acceptedTimeout: 2,
        rejected: 1,
        reviewed: 6,
        reviewedTimeout: 0,
        createdAt: new Date(),
      };
      const result = getAutoBalloonProgress(autoballoonValues);
      const expected = Math.floor(((7 + 2 + 1) / 10) * 100);
      expect(result).toBe(expected);
    });

    it('Returns false for an invalid date', () => {
      const result = autoBallooningTimedOut(new Date('invalid date'));
      expect(result).toBe(false);
    });

    it('Handles undefined autoballoonValues gracefully', () => {
      const result = getAutoBalloonProgress(undefined);
      expect(result).toBe(NaN);
    });

    it('Handles zero found captures gracefully', () => {
      const autoballoonValues = {
        found: 0,
        accepted: 0,
        acceptedTimeout: 0,
        rejected: 0,
        reviewed: 0,
        reviewedTimeout: 0,
        createdAt: new Date(),
      };
      const result = getAutoBalloonProgress(autoballoonValues);
      expect(result).toBe(NaN);
    });
  });

  describe('#getAutoBalloonReviewProgress', () => {
    it('Calculates and returns the correct percentage progress for auto-balloon review', () => {
      const autoballoonValues = {
        found: 10,
        accepted: 7,
        acceptedTimeout: 2,
        rejected: 1,
        reviewed: 6,
        reviewedTimeout: 0,
        createdAt: new Date(),
      };
      const result = getAutoBalloonReviewProgress(autoballoonValues);
      const expected = Math.floor(((6 + 0) / 7) * 100);
      expect(result).toBe(expected);
    });

    it('Handles zero accepted captures gracefully', () => {
      const autoballoonValues = {
        found: 10,
        accepted: 0,
        acceptedTimeout: 0,
        rejected: 0,
        reviewed: 0,
        reviewedTimeout: 0,
        createdAt: new Date(),
      };
      const result = getAutoBalloonReviewProgress(autoballoonValues);
      expect(result).toBe(NaN);
    });
  });
});
