import { Loading3QuartersOutlined, CheckCircleOutlined, InfoCircleTwoTone } from '@ant-design/icons';
import { Tooltip } from 'antd';
import { i18n } from 'i18n';
import React from 'react';
import { AutoballoonValues } from './Autoballoon';
import { Part } from 'domain/part/partTypes';
import { TooltipPlacement } from 'antd/lib/tooltip';
import IxcTheme from 'styleguide/styles/IxcTheme';

type Props = {
  autoballoonValues: AutoballoonValues;
  part: Part;
  tooltipPlacement: TooltipPlacement;
};

export default function AutoballoonStatus({ autoballoonValues, part, tooltipPlacement }: Props) {
  
  // If autoballooing is true, but the markup hasn't been found yet, indicate that it's still in progress
  if (part.autoMarkupStatus === 'In_Progress') {
    return (
      <Tooltip placement={tooltipPlacement} title={i18n('entities.part.tooltips.findingMarkups')}>
         <Loading3QuartersOutlined spin style={{ color: IxcTheme.colors.blue }} data-testid="loading-state" />
      </Tooltip>
    );
  }
  
  // If autoballooing is true, and the markup has been found, show a checkmark
  if (part.autoMarkupStatus === 'Completed') {
    return (
      <Tooltip placement={tooltipPlacement} title={i18n('entities.part.tooltips.autoMarkupComplete')}>
        <CheckCircleOutlined style={{ color: IxcTheme.colors.primaryColor }}/>
      </Tooltip>
    );
  }
  
  return (
    <span>
        <Tooltip placement={tooltipPlacement} title={i18n('entities.part.tooltips.partNotAutoballooned')}>
          <InfoCircleTwoTone />
        </Tooltip>
    </span>
  );
}

