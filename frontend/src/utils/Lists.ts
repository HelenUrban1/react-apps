import { ListItem, ListObject, ListObjectItem, ListTypeEnum, MetaData } from 'domain/setting/listsTypes';
import { i18n } from 'i18n';
import { cleanLowercaseString, sanitizedTextComparison } from './textOperations';

export type ListTypes = 'Type' | 'Classification' | 'Operation' | 'Inspection_Method' | 'Unit_of_Measurement';

export const lists: ListTypes[] = ['Type', 'Classification', 'Operation', 'Inspection_Method', 'Unit_of_Measurement'];

// updates a list in the session context after an item has been created, edited, or deleted
export const updateList = (listType: ListTypeEnum, newList: ListObject): any => {
  const metric: ListObject = {};
  const imperial: ListObject = {};
  switch (listType) {
    case 'Classification':
      return { classifications: newList };
    case 'Type':
      return { types: newList };
    case 'Inspection_Method':
      return { methods: newList };
    case 'Operation':
      return { operations: newList };
    case 'Unit_of_Measurement':
      Object.keys(newList).forEach((key) => {
        if (newList[key].meta?.type?.includes('Metric')) {
          metric[key] = newList[key];
        }
        if (newList[key].meta?.type?.includes('Imperial')) {
          imperial[key] = newList[key];
        }
      });
      return { metricUnits: metric, imperialUnits: imperial };
    default:
      break;
  }

  return null;
};

// updates existing list with new/edited item to keep data in sync
export const updateLists = (listType: ListTypeEnum, currentList: ListObject | null, listItem: ListItem): any => {
  if (!currentList) return;
  const newList = { ...currentList };
  if (listItem.parentId) {
    newList[listItem.parentId].children = newList[listItem.parentId]?.children ? newList[listItem.parentId].children : {};
    // add to parent's children
    newList[listItem.parentId].children![listItem.id] = {
      children: {},
      value: listItem.name,
      default: listItem.default,
      id: listItem.id,
      status: listItem.status,
      meta: listItem.metadata ? JSON.parse(listItem.metadata) : undefined,
      parentId: listItem.parentId,
      listType: listItem.listType,
      index: listItem.itemIndex,
      deleted: !!listItem.deletedAt,
    };
  } else {
    // add to top level
    newList[listItem.id] = {
      children: {},
      value: listItem.name,
      default: listItem.default,
      id: listItem.id,
      status: listItem.status,
      meta: listItem.metadata ? JSON.parse(listItem.metadata) : undefined,
      listType: listItem.listType,
      index: listItem.itemIndex,
      deleted: !!listItem.deletedAt,
    };
  }

  return updateList(listType, newList);
};

// Creates key-value object of a given list type
export const convertToListObject = (entries: ListItem[]) => {
  const typeObject: ListObject = {};
  const children: { [key: string]: ListObject } = {};

  entries.forEach((entry: ListItem) => {
    if (!entry.parentId || entry.parentId === null) {
      // create keys from the top level items (e.g. types) that don't have a parentId
      typeObject[`${entry.id}`] = {
        id: entry.id,
        value: entry.name,
        default: entry.default,
        meta: entry.metadata ? JSON.parse(entry.metadata) : '',
        status: entry.status,
        listType: entry.listType,
        index: entry.itemIndex,
        children: {},
        deleted: !!entry.deletedAt,
      };
    } else if (entry.parentId) {
      // populates the children (e.g. subtypes) of parent items
      children[entry.parentId] = children[entry.parentId] ? children[entry.parentId] : {}; // ensure not undefined or null
      children[entry.parentId][`${entry.id}`] = {
        id: entry.id,
        value: entry.name,
        default: entry.default,
        meta: entry.metadata ? JSON.parse(entry.metadata) : '',
        status: entry.status,
        listType: entry.listType,
        index: entry.itemIndex,
        parentId: entry.parentId,
        deleted: !!entry.deletedAt,
      };
    }
  });
  // ensures the parent exists before assigning the children
  Object.keys(children).forEach((parentId) => {
    typeObject[parentId].children = children[parentId];
  });
  return typeObject;
};

// given a list and an id, returns the list object with that id
export const getSingleLevelListValue = (list: ListObject | null, id: string | null): string | undefined => {
  let matched: string | undefined;
  if (!list || !id) return matched;
  Object.keys(list).forEach((key) => {
    if (key === id) matched = list[key].value;
  });
  return matched;
};

export const getListIdByValue = ({ list, value, defaultValue = i18n('common.notFound') }: { list?: ListObject | null; value?: string; defaultValue?: string }) => {
  if (!value) {
    throw new Error('Type not provided');
  }
  if (!list) {
    return defaultValue;
  }
  const ID = Object.keys(list).find((key) => cleanLowercaseString(list[key].value) === cleanLowercaseString(value));
  return ID || defaultValue;
};
export const getListNameById = ({ list, value, parentId, defaultValue = i18n('common.notFound') }: { list?: ListObject | null; value?: string; parentId?: string; defaultValue?: string }) => {
  if (!value) {
    throw new Error('Type not provided');
  }
  if (!list) {
    return defaultValue;
  }
  if (parentId && list[parentId]?.children) {
    const children: ListObject = list[parentId].children!;
    return children[value]?.value;
  }
  return list[value]?.value || defaultValue;
};
export const getListMeasurementById = ({ list, value, parentId }: { list?: ListObject | null; value?: string; parentId?: string }) => {
  if (!value) {
    throw new Error('Type not provided');
  }
  if (!list) {
    return undefined;
  }
  if (parentId && list[parentId]?.children) {
    const children: ListObject = list[parentId].children!;
    return children[value]?.measurement || children[value]?.value;
  }
  return list[value]?.measurement || list[value]?.value;
};

export const getValueOfHiddenType = (list: ListObject, vals: string[]) => {
  let type = i18n('common.notFound');
  let subtype = i18n('common.notFound');
  let [typeID, subtypeID] = vals;
  if (vals.length > 2) {
    [, typeID, subtypeID] = vals;
  }
  if (typeID) {
    type = getListNameById({ list, value: typeID, defaultValue: i18n('common.notFound') });
  }
  if (subtypeID) {
    subtype = getListNameById({
      list,
      value: subtypeID,
      parentId: typeID,
      defaultValue: i18n('common.notFound'),
    });
  }

  return { type, subtype };
};

// retrieves the active list based on the list Type name
export const getCurrentList = (activeList: string, classifications: ListObject | null, methods: ListObject | null, types: ListObject | null, operations: ListObject | null, imperialUnits: ListObject | null, metricUnits: ListObject | null) => {
  switch (activeList) {
    case 'Classification':
      return classifications;
    case 'Type':
      return types;
    case 'Inspection_Method':
      return methods;
    case 'Operation':
      return operations;
    case 'Unit_of_Measurement':
      return { ...imperialUnits, ...metricUnits };
    default:
      return null;
  }
};

export interface DataType {
  orderKey: string;
  inputKey: string;
  activeKey: string;
  subtypeKey: string;
  measureKey: string;
  key: string;
  id: string;
  obj: ListObjectItem;
  index: number;
  childs?: DataType[];
}

// converts a list to a format consumable by the ant design table component
export const convertListToDataSource = (list: ListObject | null, showInactive: boolean, search = '') => {
  let data: DataType[] = [];
  if (!list) return data;
  data = Object.values(list)
    .filter((item) => {
      return (showInactive || item.status === 'Active') && item.value.toLowerCase().includes(search.toLowerCase());
    })
    .map((item) => {
      return {
        key: item.id,
        id: `${item.listType}-${item.id}`,
        orderKey: `${item.parentId || 'parent'}-order-${item.id}`,
        inputKey: `${item.parentId || 'parent'}-in-${item.id}`,
        activeKey: `${item.parentId || 'parent'}-active-${item.id}`,
        measureKey: `${item.parentId || 'parent'}-active-${item.id}`,
        subtypeKey: `${item.parentId || 'parent'}-active-${item.id}`,
        obj: item,
        index: item.index,
      };
    });
  return data;
};

// converts the Types list to a format consumable by the ant design table component
export const convertTypeListToDataSource = (list: ListObject | null, showInactive: boolean, search = '') => {
  let data: DataType[] = [];
  if (!list) return data;
  data = Object.values(list)
    .map((item) => {
      return {
        key: item.id,
        id: item.id,
        orderKey: `order-${item.id}`,
        inputKey: `in-${item.id}`,
        activeKey: `active-${item.id}`,
        measureKey: `measure-${item.id}`,
        subtypeKey: `subtype-${item.id}`,
        obj: item,
        index: item.index,
        childs: item.children
          ? Object.values(item.children)
              .filter((child) => {
                return (showInactive || child.status === 'Active') && child.value.toLowerCase().includes(search.toLowerCase());
              })
              .map((child) => {
                return {
                  key: child.id,
                  orderKey: `order-${child.id}`,
                  inputKey: `in-${child.id}`,
                  activeKey: `active-${child.id}`,
                  measureKey: `measure-${item.id}`,
                  subtypeKey: `subtype-${item.id}`,
                  id: child.id,
                  obj: child,
                  index: child.index,
                };
              })
              .sort((a, b) => a.index - b.index)
          : undefined,
      };
    })
    .sort((a, b) => a.index - b.index);

  return data;
};

// checks if a given string is the duplicate of any parent list item's value
export const isDuplicateSingleLevel = (list: ListObject, val: string) => {
  if (Object.keys(list).find((key) => cleanLowercaseString(list[key].value) === cleanLowercaseString(val))) {
    return true;
  }
  return false;
};

// checks if a given string is the duplicate of a child list item's value
export const isDuplicateDoubleLevel = (list: ListObject, val: string, parentId: string) => {
  if (!list[parentId].children || JSON.stringify(list[parentId].children) === '{}') {
    return false;
  }
  return isDuplicateSingleLevel(list[parentId].children!, val);
};

// checks if a given string is the duplicate of any parent list item's value
export const isDuplicateValue = (list: ListObject | null, val: string, listItem?: ListObjectItem) => {
  if (!list || (listItem && cleanLowercaseString(listItem.value) === cleanLowercaseString(val))) return false;
  if (listItem?.parentId) {
    return isDuplicateDoubleLevel(list, val, listItem.parentId);
  }
  return isDuplicateSingleLevel(list, val);
};

export interface MeasurementOption {
  name: string | undefined;
  units: { id: string; name: string; unit: string; type: string }[];
}

// transforms the units lists into a format consumable by ant design select component
export const getMeasurementsSelectOptions = (metric: ListObject | null, imperial: ListObject | null) => {
  const combined = { ...metric, ...imperial };
  const measurements: MeasurementOption[] = [
    {
      name: 'None',
      units: [
        {
          id: '',
          name: 'None',
          unit: 'None',
          type: '',
        },
      ],
    },
  ];
  Object.keys(combined).forEach((unit) => {
    if (!measurements.some((measurement) => measurement.name === combined[unit].meta?.measurement) && combined[unit].meta) {
      measurements.push({
        name: combined[unit].meta?.measurement,
        units: [
          {
            id: combined[unit].id,
            name: combined[unit].value,
            unit: combined[unit].meta?.unit || '',
            type: combined[unit].meta?.type || '',
          },
        ],
      });
    } else {
      const measurement = measurements.find((entry) => entry.name === combined[unit].meta?.measurement);
      measurement?.units.push({
        id: combined[unit].id,
        name: combined[unit].value,
        unit: combined[unit].meta?.unit || '',
        type: combined[unit].meta?.type || '',
      });
    }
  });
  return measurements;
};

// returns the units associated with a given measurement type
export const getMeasurementUnits = (measurements: MeasurementOption[], measurement: string | undefined): { metric: string[]; imperial: string[]; both: string[] } => {
  const units: { metric: string[]; imperial: string[]; both: string[] } = {
    metric: [],
    imperial: [],
    both: [],
  };
  for (let i = 0; i < measurements.length; i++) {
    if (sanitizedTextComparison(measurements[i].name || '', measurement || 'f')) {
      measurements[i].units.forEach((unit) => {
        if (unit.type === 'Metric') {
          units.metric.push(unit.unit);
        } else if (unit.type === 'Imperial') {
          units.imperial.push(unit.unit);
        } else {
          units.both.push(unit.unit);
        }
      });
    }
  }
  return units;
};

export const organizeListItems = (filteredItems: ListItem[]) => {
  // filters full list down to different types
  const filteredTypes: ListItem[] = [];
  const filteredMethods: ListItem[] = [];
  const filteredOperations: ListItem[] = [];
  const filteredClassifications: ListItem[] = [];
  const filteredMetricUnits: ListItem[] = [];
  const filteredImperialUnits: ListItem[] = [];

  let meta: MetaData;

  for (let i = 0; i < filteredItems.length; i++) {
    const item = filteredItems[i];
    switch (item.listType.toString()) {
      case 'Type':
        filteredTypes.push(item);
        break;
      case 'Inspection_Method':
        filteredMethods.push(item);
        break;
      case 'Operation':
        filteredOperations.push(item);
        break;
      case 'Classification':
        filteredClassifications.push(item);
        break;
      case 'Unit_of_Measurement':
        if (!item.metadata) break;
        meta = JSON.parse(item.metadata);
        if (meta && meta.type) {
          if (meta.type.includes('Metric')) {
            filteredMetricUnits.push(item);
          }
          if (meta.type.includes('Imperial')) {
            filteredImperialUnits.push(item);
          }
        }
        break;
      default:
        break;
    }
  }

  // builds the list object from the lists for easier lookups and managing
  const typesObject = convertToListObject(filteredTypes.sort((a: ListItem, b: ListItem) => a.itemIndex - b.itemIndex));
  const methodsObject = convertToListObject(filteredMethods.sort((a: ListItem, b: ListItem) => a.itemIndex - b.itemIndex));
  const operationsObject = convertToListObject(filteredOperations.sort((a: ListItem, b: ListItem) => a.itemIndex - b.itemIndex));
  const classificationsObject = convertToListObject(filteredClassifications.sort((a: ListItem, b: ListItem) => a.itemIndex - b.itemIndex));
  const metricObject = convertToListObject(filteredMetricUnits.sort((a: ListItem, b: ListItem) => a.itemIndex - b.itemIndex));
  const imperialObject = convertToListObject(filteredImperialUnits.sort((a: ListItem, b: ListItem) => a.itemIndex - b.itemIndex));

  let subTypes: ListObject = {};

  if (typesObject) {
    Object.values(typesObject).forEach((t: ListObjectItem) => {
      if (t.children) {
        subTypes = { ...subTypes, ...t.children };
      }
    });
  }

  return {
    types: typesObject,
    methods: methodsObject,
    operations: operationsObject,
    classifications: classificationsObject,
    metricUnits: metricObject,
    imperialUnits: imperialObject,
    filterList: { operation: operationsObject, criticality: classificationsObject, inspectionMethod: methodsObject, notationType: typesObject, notationSubtype: subTypes },
  };
};
