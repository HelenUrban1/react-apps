import { getPresetSelectOptions, currentSettingMatchesPreset, getCustomerSettingPreset } from 'utils/PresetUtils';
import { DefaultStyles, DefaultTolerances, defaultUnitIDs } from 'view/global/defaults';

describe('Preset utility functions', () => {
  const sampleStylesLoadedPresets = {
    metadata: {
      Default: {
        customers: [],
        setting: {
          default: {
            style: 'f12c5566-7612-48e9-9534-ef90b276ebaa',
            assign: ['Default'],
          },
        },
      },
      Test: {
        customers: ['1111'],
        setting: {
          default: {
            style: 'f12c5566-7612-48e9-9534-ef90b276ebaa',
            assign: ['Default'],
          },
          1: {
            style: '1212',
            assign: ['Dimension', '1313', '1414'],
          },
        },
      },
    },
  };

  const sampleTolsLoadedPresets = {
    metadata: {
      Default: {
        customers: [],
        setting: DefaultTolerances,
      },
      TestTol: {
        customers: ['2222'],
        setting: {
          ...DefaultTolerances,
          linear: {
            type: 'PrecisionTolerance',
            rangeTols: [
              { rangeLow: '2', rangeHigh: '9', plus: '1', minus: '-1' },
              { rangeLow: '0.2', rangeHigh: '0.9', plus: '0.1', minus: '-0.1' },
              { rangeLow: '0.02', rangeHigh: '0.09', plus: '0.01', minus: '-0.01' },
              { rangeLow: '0.002', rangeHigh: '0.009', plus: '0.001', minus: '-0.001' },
              { rangeLow: '0.0002', rangeHigh: '0.0009', plus: '0.0001', minus: '-0.0001' },
            ],
            precisionTols: [
              { level: 'X', plus: '2', minus: '-2' },
              { level: 'X.X', plus: '0.2', minus: '-0.2' },
              { level: 'X.XX', plus: '0.02', minus: '-0.02' },
              { level: 'X.XXX', plus: '0.002', minus: '-0.002' },
              { level: 'X.XXXX', plus: '0.0002', minus: '-0.0002' },
            ],
            unit: defaultUnitIDs.millimeter,
          },
        },
      },
    },
  };

  const sampleOrg = {
    id: '1111',
    name: 'Test Org',
  };

  it('should get select input options split between customer and other', () => {
    const options = getPresetSelectOptions(sampleStylesLoadedPresets, sampleOrg);
    expect(options.props.children.length).toBe(2);
    expect(options.props.children[0].props.label).toBe('Test Org Presets');
    expect(options.props.children[0].props.children[0].key).toBe('Test');
    expect(options.props.children[1].props.label).toBe('Other Presets');
    expect(options.props.children[1].props.children[0].key).toBe('Default');
  });

  it('should get select input options only for other', () => {
    const options = getPresetSelectOptions(sampleTolsLoadedPresets, sampleOrg);
    expect(options.props.label).toBe('Style Presets');
    expect(options.props.children[0].key).toBe('Default');
    expect(options.props.children[1].key).toBe('TestTol');
  });

  it('returns true if two presets are identical', () => {
    expect(currentSettingMatchesPreset('Default', sampleStylesLoadedPresets, DefaultStyles)).toBeTruthy();
  });

  it('returns false if two presets are different', () => {
    expect(currentSettingMatchesPreset('TestTol', sampleTolsLoadedPresets, DefaultStyles)).toBeFalsy();
  });

  it('returns false if there are no available presets', () => {
    expect(currentSettingMatchesPreset('Default', undefined, DefaultStyles)).toBeFalsy();
  });

  it('returns the presets associated with a customer', () => {
    const noMatchedPresets = getCustomerSettingPreset(sampleTolsLoadedPresets, sampleOrg.id);
    expect(noMatchedPresets.length).toBe(0);

    const matchedPresets = getCustomerSettingPreset(sampleStylesLoadedPresets, sampleOrg.id);
    expect(matchedPresets.length).toBe(1);
    expect(matchedPresets[0].presetName).toBe('Test');
  });
});
