import { types } from 'view/global/defaults';
import moment from 'moment';
import { i18n } from 'i18n';
import { BillingPeriodEnum, Catalog, Component, ComponentInfo, ComponentPrice } from 'domain/setting/subscriptionTypes';
import log from 'modules/shared/logger';
import { Characteristic } from 'types/characteristics';
import { Measurement, MeasurementStatusEnum } from 'types/measurement';
import { Sample, SampleStatusEnum } from 'types/sample';
import { Tolerancer } from './ParseTextController/Tolerancer';

interface MeasurementTypes {
  [key: string]: {
    [key: string]: string;
  };
}

export const isValidNumber = new RegExp(/^-?\d*\.?\d*$/);

const typeMeasurements: MeasurementTypes = {};

types.forEach((type) => {
  if (type.children) {
    typeMeasurements[type.value] = {};
    type.children.forEach((child) => {
      typeMeasurements[type.value][child.value] = child.measurement ? child.measurement : 'none';
    });
  }
});

export const validateNumber = (number: string) => {
  return !!isValidNumber.test(number);
};

export const validateNominal = (nominal: string) => {
  // check if nominal is a plain number
  if (isValidNumber.test(nominal)) return true;

  if (nominal.includes('/') && !nominal.includes('//')) {
    // check if nominal is a ranged dimension split by a forward slash
    const array = nominal.split('/');
    if (array.length !== 2) return false;
    for (let i = 0; i < array.length; i++) {
      if ((!validateNumber(array[i].trim()) && array[i].trim() !== '/') || array[i].length === 0) {
        return false;
      }
    }
    return true;
  }
  if (nominal.includes('-') && !nominal.includes('--')) {
    // check if nominal is a ranged dimension split by a dash
    const array = nominal.split('-');
    for (let i = 0; i < array.length; i++) {
      if ((!validateNumber(array[i].trim()) && array[i].trim() !== '-') || array[i].length === 0) {
        return false;
      }
    }
    return true;
  }
  return !!isValidNumber.test(nominal);
};

// Dates

export const dateIsSameOrBefore = (date1: Date, date2: Date | null) => {
  if (!date2) {
    return true;
  }
  return moment(date2).isSameOrBefore(date1);
};

// $$$
// TODO: This should probably account for other forms of currency
/*
  Converts a value in cents to a string representing dollars
*/
export const convertCentsToUSD = (cents: number | undefined) => {
  if (!cents) return '$0.00';
  const dollars = cents / 100;
  return dollars.toLocaleString('en-US', { style: 'currency', currency: 'USD' });
};

export const getComponentMetric = (unit: string) => {
  return unit
    .toLowerCase()
    .split(' ')
    .map((word: string, index: number) => (index === 0 ? word : word.charAt(0).toUpperCase() + word.slice(1)))
    .join('');
};

export const createComponentMap = (period: BillingPeriodEnum, catalog: Catalog, components: Component[], product: string = 'inspectionxpert-cloud', startQty?: number) => {
  const map: ComponentInfo = {};
  if (!catalog[product]) {
    log.error('Product Handle not found in Catalog');
    return map;
  }
  if (!components) {
    log.error('Must pass components to create a map');
    return map;
  }

  components.forEach((comp) => {
    let compPrice = 0;
    let pricePointId = 'custom';
    let unitName = comp.unit_name && comp.unit_name !== 'on/off' ? comp.unit_name : comp.component_handle;
    let pricing = comp.pricing_scheme;
    if (catalog[product]?.components && catalog[product].components[comp.component_handle || 'none']) {
      // price array
      const prices: { [key: string]: ComponentPrice } = {};
      const componentPrices: { [key: string]: ComponentPrice[] } = {};
      if (catalog[product].components[comp.component_handle || 'none']) {
        const compPrices = catalog[product].components[comp.component_handle || 'none'].prices || [];
        compPrices
          .filter((price) => !!price.handle)
          .sort((a, b) => {
            const aDate = a.handle.slice(1, a.handle.indexOf('_'));
            const bDate = b.handle.slice(1, b.handle.indexOf('_'));
            if (moment(aDate).isBefore(moment(bDate))) {
              return -1;
            }
            if (moment(bDate).isBefore(moment(aDate))) {
              return 1;
            }
            return 0;
          })
          .forEach((price) => {
            const interval = price.handle.slice(price.handle.indexOf('_') + 1);
            const allocatedQuantity = startQty || comp.allocated_quantity;
            if ((interval === 'annually' || interval === 'monthly') && !componentPrices[interval]) {
              componentPrices[price.handle] = price.prices;
              price.prices.forEach((step) => {
                prices[step.id] = step;
                if (interval === period.toLowerCase() && allocatedQuantity >= step.starting_quantity && (!step.ending_quantity || comp.allocated_quantity <= step.ending_quantity)) {
                  compPrice = parseFloat(step.unit_price) * 100;
                  pricePointId = step.id.toString(); // Because for some reason price_point_id doesn't match this
                }
              });
            }
          });
      }

      if (!unitName) {
        unitName = 'none';
      } else if (unitName.includes('_addon')) {
        unitName = unitName.replace('_addon', '');
      }

      if (!pricing) {
        pricing = comp.kind ? comp.kind.replace('_component', '') : 'none';
      }

      const unit = getComponentMetric(unitName);

      map[comp.component_handle || 'none'] = {
        metric: unit,
        name: comp.name,
        unit: unitName,
        id: comp.component_id,
        handle: comp.component_handle || 'none',
        used: 0,
        purchased: comp.allocated_quantity,
        priceId: pricePointId,
        price: compPrice,
        type: pricing,
        priceMap: prices,
        prices: componentPrices,
      };
    } else {
      log.warn(`Could not create component map for ${product}`);
    }
  });
  return map;
};

export const calculateComponentTotal = (components: ComponentInfo) => {
  let total = 0;

  Object.values(components).forEach((comp) => {
    total += comp.type === 'stairstep' ? comp.price : comp.price * comp.purchased;
  });
  return total;
};

export const calculateDue = ({
  originalComponents,
  newComponents,
  label,
  started,
  nextAssessmentAt,
  today = moment(),
}: {
  originalComponents: ComponentInfo;
  newComponents: ComponentInfo;
  label: { [key: string]: string };
  started?: moment.Moment;
  nextAssessmentAt?: moment.Moment;
  today?: moment.Moment;
}) => {
  const newLabel = { ...label };
  const costPerPeriod = calculateComponentTotal(newComponents);
  if (!nextAssessmentAt || !started) {
    log.warn('Cant calculate due without assessment date');
    return { costPerPeriod, newLabel, dueAmount: 0 };
  }
  newLabel.payment = convertCentsToUSD(costPerPeriod);
  if (costPerPeriod <= 0) {
    return { costPerPeriod, newLabel, dueAmount: 0 };
  }
  const remainingInPeriod = nextAssessmentAt.diff(today);
  const possible = nextAssessmentAt.diff(started);
  const percentRemaining = remainingInPeriod / possible;

  let proratedDue = 0;
  for (let index = 0; index < Object.keys(newComponents).length; index++) {
    const handle = Object.keys(newComponents)[index];
    const newComp = newComponents[handle];
    const originalComp = originalComponents[handle];
    const newPrice = newComp.type === 'stairstep' ? newComp.price : newComp.price * newComp.purchased;
    const oldPrice = originalComp.type === 'stairstep' ? originalComp.price : originalComp.price * originalComp.purchased;
    const difference = (newPrice - oldPrice) / newPrice;
    if (difference > 0) {
      proratedDue += difference * percentRemaining * newPrice;
    }
  }

  newLabel.remaining = `${nextAssessmentAt.diff(today, 'days')} ${i18n('common.days')}`;
  newLabel.due = `${convertCentsToUSD(proratedDue)} ${i18n('common.plusTax')}`;
  newLabel.ending = nextAssessmentAt.format('MMM D, YYYY');

  return { newLabel, dueAmount: proratedDue };
};

export const validateMeasurement = (value: string, feature: Characteristic | null) => {
  if (value === MeasurementStatusEnum.Pass || value === MeasurementStatusEnum.Fail) return MeasurementStatusEnum[value];
  if (!feature || (!feature.upperSpecLimit && !feature.lowerSpecLimit)) {
    return MeasurementStatusEnum.Unmeasured;
  }
  const lowerSpec = parseFloat(feature.lowerSpecLimit);
  const upperSpec = parseFloat(feature.upperSpecLimit);
  const measurement = parseFloat(value);
  if ((measurement || measurement === 0) && ((lowerSpec && !Number.isNaN(lowerSpec) && measurement < lowerSpec) || (upperSpec && !Number.isNaN(upperSpec) && measurement > upperSpec))) return MeasurementStatusEnum.Fail;
  return MeasurementStatusEnum.Pass;
};

export const getCellStatus = (measurements: Measurement[]) => {
  if (measurements.length === 0) return 'Unmeasured';

  return measurements.some((m: Measurement) => m.status === 'Fail') ? 'Fail' : 'Pass';
};

export const getDisplayValue = (feature: Characteristic, measurements: Measurement[]): string => {
  if (feature && measurements) {
    if (measurements.length === 0) return '';

    const nums = measurements.filter((m: Measurement) => !isNaN(parseFloat(m.value))).map((m: Measurement) => parseFloat(m.value));

    if (measurements.some((m: Measurement) => m.value === 'Fail')) return 'Fail';
    if (nums.length === 0) return 'Pass';
    if (nums.length === 1) return measurements[0].value;

    const min = Math.min(...nums);
    const minStr = min.toString();
    const max = Math.max(...nums);
    const maxStr = max.toString();
    const lowerSpec = parseFloat(feature.lowerSpecLimit);
    const upperSpec = parseFloat(feature.upperSpecLimit);

    const precision = Tolerancer.getGreatestPrecision({ nominal: validateNumber(feature.nominal) ? feature.nominal : '', plus: feature.upperSpecLimit, minus: feature.lowerSpecLimit });

    const avg = (nums.reduce((a: number, b: number) => a + b) / nums.length).toFixed(precision);
    const avgStr = avg.toString();

    if (feature.upperSpecLimit && feature.lowerSpecLimit) {
      if (min >= lowerSpec && max <= upperSpec) return avg.toString();
      if (min < lowerSpec) {
        if (max > upperSpec) {
          const minDif = lowerSpec - min;
          const maxDif = max - upperSpec;
          return minDif > maxDif ? minStr : maxStr;
        }
        return minStr;
      }
      return maxStr;
    }
    if (feature.upperSpecLimit) {
      if (max > upperSpec) return maxStr;
    } else if (feature.lowerSpecLimit) {
      if (min < lowerSpec) return minStr;
    }

    return avgStr;
  }

  return '';
};

export const checkSampleStatus = (sample: Sample, features: Characteristic[]): SampleStatusEnum => {
  const measurements = [...sample.measurements];
  let status: SampleStatusEnum = 'Pass';

  for (let i = 0; i < features.length; i++) {
    const featureMeasurements = measurements.filter((m: Measurement) => m.characteristicId === features[i].id && m.sampleId === sample.id);
    if (featureMeasurements.length === 0) {
      status = 'Unmeasured';
    } else if (featureMeasurements.some((m: Measurement) => m.status === 'Fail')) {
      return 'Fail';
    }
  }

  return status;
};
