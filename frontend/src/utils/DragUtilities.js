import { isEqual } from 'lodash';

// Set shiftForGroup to adjust styles when dragging
// TODO: link this to default styles somehow? so the 48 continues to equal the card height
export const getDraggingItemHeight = (result, items) => {
  const moved = items.find((feature) => feature.id && feature.id === result.draggableId);
  if (!moved) {
    return null;
  }
  let length = 1;
  // get length for the optimistic splice
  if (moved.markerSubIndex === 0) {
    const featureGroup = items.filter((feature) => feature.markerGroup === moved.markerGroup);
    length = featureGroup.length;
  }
  return { top: result.source.index * 48, height: length * 48 - 48 };
};

// Splice item list to update UI and send to be renumbered
export const moveDragToNewIndex = (result, items) => {
  // since the indexes are by marker number, get the moved item by ID
  const moved = items.find((feature) => feature.id === result.draggableId);
  // guard against weird dragging states
  if (!result.destination || result.source.index === result.destination.index || !moved) {
    return null;
  }
  const index = moved.markerIndex;
  let destination = result.destination.index;
  let movedTo;
  if (result.source.index > result.destination.index) {
    // moved down
    movedTo = items.find((item) => parseInt(item.markerGroup, 10) === destination + 1);
  } else {
    // moved up
    movedTo = items.find((item) => parseInt(item.markerGroup, 10) === destination + 2);
  }
  if (!movedTo && destination + 1 <= items.length - 1) {
    movedTo = items[items.length - 1];
  } else if (!movedTo) {
    return null;
  }

  // get length for the optimistic splice
  let length = 1;
  if (moved.markerSubIndex === 0) {
    const featureGroup = items.filter((feature) => feature.markerGroup === moved.markerGroup);
    length = featureGroup.length;
  }
  // move the card in local state to prevent UX lag
  const newItems = [...items];
  // grab the feature(s) out of the group
  const removed = newItems.splice(index, length);
  destination = newItems.indexOf(movedTo);
  // insert feature(s) at the new index
  newItems.splice(destination, 0, ...removed);
  return newItems;
};

export const dragMultipleToNewIndex = (result, items, selected) => {
  // guard against weird dragging states
  if (!result.destination || result.source.index === result.destination.index || !selected || selected.length <= 0) {
    return null;
  }
  let moved = [];
  let movedIds = [];
  selected.forEach((feature) => {
    // move any sub features that were partially selected as a group
    if (feature.markerSubIndex !== -1 && feature.markerSubIndex !== null) {
      const group = items.filter((item) => item.markerGroup === feature.markerGroup && !movedIds.includes(item.id));
      moved = [...moved, ...group];
      movedIds = [...movedIds, ...group.map((a) => a.id)];
    } else if (!movedIds.includes(feature.id)) {
      moved.push(feature);
      movedIds.push(feature.id);
    }
  });

  moved = moved.sort((a, b) => a.markerIndex - b.markerIndex);
  let destination = result.destination.index;
  let movedTo;
  if (result.source.index > result.destination.index) {
    // moved down
    movedTo = items.find((item) => parseInt(item.markerGroup, 10) === destination + 1);
  } else {
    // moved up
    movedTo = items.find((item) => parseInt(item.markerGroup, 10) === destination + 2);
  }
  if (!movedTo && destination + 1 <= items.length - 1) {
    movedTo = items[items.length - 1];
  } else if (!movedTo) {
    return null;
  }

  // removed moved features
  const newItems = items.filter((item) => !movedIds.includes(item.id));
  // get destination index
  destination = newItems.indexOf(movedTo);
  // insert feature(s) at the new index
  newItems.splice(destination, 0, ...moved);
  return newItems;
};

// Get Draggable Styles
export const getStyle = ({ provided, style, shiftForGroup }) => {
  // Adjust spacing when moving groups
  if (shiftForGroup.height !== null && !provided.draggableProps.style?.zIndex && style.top > shiftForGroup.top) {
    return {
      ...style,
      ...provided.draggableProps.style,
      top: style.top - shiftForGroup.height,
    };
  }
  // assign drag and drop styles
  return {
    ...style,
    ...provided.draggableProps.style,
  };
};

// Get Card Selection Class
export const isSelected = (item, selected) => {
  if (!selected || selected.length <= 0) {
    return null;
  }
  return selected.some((selectedChar) => {
    return selectedChar.id === item.id;
  });
};

// Get Marker SVG
export const getMarker = (item, markers, defaultMarker) => {
  if (!markers || markers.length <= 0) {
    return null;
  }
  const itemMarkerID = item.markerStyle;
  let defaultSvg;
  for (let i = 0; i < markers.length; i++) {
    if (markers[i].id === itemMarkerID) {
      return markers[i].svg;
    }
    if (markers[i].id === defaultMarker) {
      defaultSvg = markers[i].svg;
    }
  }
  return defaultSvg;
};

// Get Balloon Number Size Class
export const getSizeClass = (item) => {
  const num = item.markerLabel.replace('.', '').length;
  if (num <= 3) {
    return 'number-small';
  }
  if (num <= 5) {
    return 'number-med';
  }
  return 'number-large';
};

// Create List Item ClassName
export const listItemClass = (feature, selected, classes) => {
  const active = isSelected(feature, selected);
  const size = getSizeClass(feature);
  let className = classes;
  if (active) {
    className += ' active ';
  } else {
    className += ' inactive ';
  }
  if (feature.verified) {
    className += 'verified ';
  } else {
    className += 'unverified ';
  }
  const captureError = feature?.captureError ? JSON.parse(feature.captureError) : null;
  if (captureError) {
    className += 'error ';
  }
  className += size;
  return className;
};

export const ItemListPropCompare = (prev, next) => {
  // only rerender if data is different
  if (
    //
    !isEqual(prev.itemList, next.itemList) ||
    !isEqual(prev.selected, next.selected) ||
    prev.markers.length !== next.markers.length ||
    prev.collapsed !== next.collapsed ||
    prev.review !== next.review ||
    prev.itemWidth !== next.itemWidth ||
    prev.loaded !== next.loaded
  ) {
    // for debugging
    // console.log('rerendered because', {
    //   list: !isEqual(prev.itemList, next.itemList),
    //   selected: !isEqual(prev.selected, next.selected),
    //   markers: !isEqual(prev.markers, next.markers),
    //   collapsed: prev.collapsed !== next.collapsed,
    //   review: prev.review !== next.review,
    //   itemWidth: prev.itemWidth !== next.itemWidth,
    // });
    return false;
  }
  // svg are always considered different, so compare the actual marker style
  if (!isEqual(prev.markers, next.markers) && prev.markers.length === next.markers.length) {
    for (let index = 0; index < prev.markers.length; index++) {
      const prevMarker = { ...prev.markers[index], svg: null };
      const nextMarker = { ...next.markers[index], svg: null };
      if (!isEqual(prevMarker, nextMarker)) {
        return false;
      }
    }
  }
  return true;
};
