const { getUserColor, getUserInitials, getCompanyInitials } = require('./Avatars');

const fullName = {
  firstName: 'Ryan',
  lastName: 'Mee',
  email: 'ryan@gmail.com',
};

const firstOnly = {
  firstName: 'Ryan',
  lastName: '',
  email: 'ryan@gmail.com',
};

const lastOnly = {
  firstName: null,
  lastName: 'Mee',
  email: 'ryan@gmail.com',
};

const emailOnly = {
  firstName: '',
  lastName: null,
  email: 'ryan@gmail.com',
};

const org = {
  name: 'Organization',
};

const orgTwo = {
  name: 'Inspection Xpert',
};

describe('Avatar Helper Functions', () => {
  it('Gets the right color', () => {
    const color = getUserColor('test-id-string');
    expect(color).toBe('#2E008B');
  });

  it('Gets the right initials', () => {
    const first = getUserInitials(firstOnly);
    const last = getUserInitials(lastOnly);
    const full = getUserInitials(fullName);
    const email = getUserInitials(emailOnly);

    expect(first).toBe('R');
    expect(last).toBe('M');
    expect(full).toBe('RM');
    expect(email).toBe('R');
  });

  it('Gets the initials from a company', () => {
    const inits = getCompanyInitials(org);
    const initsTwo = getCompanyInitials(orgTwo);
    expect(inits).toBe('OR');
    expect(initsTwo).toBe('IX');
  });
});
