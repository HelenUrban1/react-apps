import { BaseOptionType } from 'antd/lib/cascader';
import { MethodType } from 'view/global/defaults';
import { i18n } from 'i18n';
import { ListObjectItem, ListItem, ListObject } from 'domain/setting/listsTypes';

import { toDisplayFormat } from './textOperations';

// Converts list provided from defaults.tsx
export const convertCascader = (data: MethodType[], name: string, used: BaseOptionType[], i18Category?: string, i18SubCategory?: string) => {
  if (data && data.length > 0) {
    const options: BaseOptionType = {
      value: name.replace(/ /g, '_'),
      label: toDisplayFormat(name),
      children: [],
    };
    data.forEach((item) => {
      let label = item.label ? item.label : item.value.charAt(0).toUpperCase() + item.value.slice(1);
      if (i18Category) {
        if (item.label) {
          label = i18n(`${i18Category}[${item.label.replace(/ /g, '_')}]`);
        } else {
          label = i18n(`${i18Category}[${item.value.replace(/ /g, '_')}]`);
        }
      }
      const level1: BaseOptionType = {
        value: item.value.replace(/ /g, '_'),
        label,
      };
      if (item.children) {
        level1.children = [];
        item.children.forEach((child) => {
          let label = child.label ? child.label : child.value.charAt(0).toUpperCase() + child.value.slice(1);
          if (i18SubCategory) {
            if (child.label) {
              label = i18n(`${i18SubCategory}[${child.label.replace(/ /g, '_')}]`);
            } else {
              label = i18n(`${i18SubCategory}[${child.value.replace(/ /g, '_')}]`);
            }
          }
          level1.children!.push({
            value: child.value.replace(/ /g, '_'),
            label,
            disabled: !!used.some((value) => value.includes(child.value)),
          });
        });
      } else {
        level1.disabled = !!used.some((value) => value.includes(item.value));
      }
      options.children!.push(level1);
    });
    return options;
  }
  console.warn('No data to convert to cascader');
};

// converts a list object into an ant design cascader options array with options in alphabetical order
export const convertListObjectToCascaderOptions = (list: ListObject, value: string, used: BaseOptionType[]) => {
  const childs: BaseOptionType[] = [];
  let listType = '';
  const keys = Object.entries(list);
  keys.forEach((key) => {
    const entry: ListObjectItem = key[1];
    if (entry.status === 'Active') {
      listType = entry.listType;
      const children = Object.values(entry.children || {});
      if (entry.children && children.length > 0) {
        const secondLevel: BaseOptionType[] = [];
        children.forEach((subtype: ListObjectItem) => {
          if (subtype.status === 'Active') {
            secondLevel.push({
              value: subtype.id,
              label: subtype.value,
              disabled: !!used.some((val) => val.includes(subtype.id)),
              deleted: subtype.deleted,
              loading: subtype.deleted,
            });
          }
        });
        // secondLevel.sort((a, b) => ((a.value || '') > (b.value || '') ? 1 : -1));
        childs.push({
          value: entry.id,
          label: entry.value,
          children: secondLevel,
        });
      } else {
        childs.push({
          value: entry.id,
          label: entry.value,
          disabled: !!used.some((val) => val.includes(entry.id)),
          loading: entry.deleted,
        });
      }
    }
    // childs.sort((a, b) => ((a.value || '') > (b.value || '') ? 1 : -1));
  });
  return {
    value: listType,
    label: value,
    children: childs,
  };
};

// Converts Type list fetched from DB
export const convertToTypesObjects = (types: ListItem[]) => {
  const typeObject: ListObject = {};

  // create keys from the Types
  types.forEach((type: ListItem) => {
    if (!type.parentId || type.parentId === null) {
      typeObject[`${type.id}`] = {
        id: type.id,
        value: type.name,
        default: type.default,
        meta: type.metadata ? JSON.parse(type.metadata) : '',
        listType: type.listType,
        index: type.itemIndex,
        status: type.status,
        children: {},
        deleted: !!type.deletedAt,
      };
    }
  });

  // create subtypes from the
  types.forEach((type: ListItem) => {
    if (type.parentId && typeObject[type.parentId]) {
      typeObject[type.parentId].children = typeObject[type.parentId].children || {};
      typeObject[type.parentId].children![type.id] = {
        id: type.id,
        value: type.name,
        default: type.default,
        measurement: type.metadata ? JSON.parse(type.metadata).measurement : '',
        meta: type.metadata ? JSON.parse(type.metadata) : '',
        listType: type.listType,
        index: type.itemIndex,
        status: type.status,
        deleted: !!type.deletedAt,
      };
    }
  });

  return typeObject;
};
