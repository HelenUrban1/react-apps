import log from 'modules/shared/logger';
import moment from 'moment';
import {
  //
  validateNumber,
  validateNominal,
  dateIsSameOrBefore,
  convertCentsToUSD,
  calculateDue,
  createComponentMap,
  calculateComponentTotal,
  validateMeasurement,
  getDisplayValue,
  checkSampleStatus,
} from './DataCalculation';
import { MOCK_CATALOG, MOCK_PRODUCT_COMPONENT_SET, MOCK_COMPONENT_INFO, SS_COMPONENT, PU_COMPONENT } from './fixtures/billing';
import { range, max, min } from './fixtures/features';
import { testSample, threeTwoFourFail, threeTwoSixPass, threeTwoFourPass, twoFail, twoPass, pass, fail, fourFail, fourPass } from './fixtures/measurements';

describe('Billing Calculations', () => {
  it('converts a number of cents to a currency string', () => {
    const number = 150000;
    const currency = convertCentsToUSD(number);
    expect(currency).toBe('$1,500.00');
  });

  it('returns a currency strong for 0 dollars if no cents are provided', () => {
    const currency = convertCentsToUSD();
    expect(currency).toBe('$0.00');
  });

  it('calculates prorated amount and label for annual subscription', () => {
    const label = {
      payment: '$1,500.00',
      due: '$0.00',
      remaining: '',
      ending: 'Jan. 1, 2021',
    };
    const originalComponents = { test: { handle: test, price: 0, type: 'stairstep' } };
    const newComponents = { test: { handle: test, price: 150000, type: 'stairstep' } };
    const started = moment('1/1/2021');
    const nextAssessmentAt = moment('1/1/2022');
    const today = moment('2/1/2021');
    const { newLabel, dueAmount } = calculateDue({
      originalComponents,
      newComponents,
      label,
      started,
      nextAssessmentAt,
      today,
    });
    expect(Math.round(dueAmount)).toBe(137260);
    expect(newLabel.due).toBe('$1,372.60 (+tax)');
    expect(newLabel.remaining).toBe('334 Days');
  });

  it('calculates prorated amount and label for monthly subscription', () => {
    const label = {
      payment: '$125.00',
      due: '$0.00',
      remaining: '',
      ending: 'Jan. 1, 2021',
    };
    const originalComponents = { test: { handle: test, price: 0, purchased: 1, type: 'per_unit' } };
    const newComponents = { test: { handle: test, price: 12500, purchased: 1, type: 'per_unit' } };
    const started = moment('1/1/2020');
    const nextAssessmentAt = moment('2/1/2020');
    const today = moment('1/10/2020');
    const { newLabel, dueAmount } = calculateDue({
      originalComponents,
      newComponents,
      label,
      started,
      nextAssessmentAt,
      today,
    });
    expect(Math.round(dueAmount)).toBe(8871);
    expect(newLabel.due).toBe('$88.71 (+tax)');
    expect(newLabel.remaining).toBe('22 Days');
  });
});

describe('Data Validation', () => {
  it('validates true for a valid number', () => {
    expect(validateNumber('123')).toBeTruthy();
    expect(validateNumber('0.123')).toBeTruthy();
    expect(validateNumber('.123')).toBeTruthy();
    expect(validateNumber('-.123')).toBeTruthy();
    expect(validateNumber('-0.123')).toBeTruthy();
  });

  it('validates false for an invalid number', () => {
    expect(validateNumber('123abc')).toBeFalsy();
    expect(validateNumber('abc')).toBeFalsy();
    expect(validateNumber('1|l.0oO')).toBeFalsy();
    expect(validateNumber('O.123')).toBeFalsy();
    expect(validateNumber('- .123')).toBeFalsy();
    expect(validateNumber('. 123')).toBeFalsy();
    expect(validateNumber(false)).toBeFalsy();
    expect(validateNumber(null)).toBeFalsy();
  });

  it('validates true for valid nominals', () => {
    expect(validateNominal('3.24')).toBeTruthy();
    expect(validateNominal('3.24-2.45')).toBeTruthy();
    expect(validateNominal('3.24/3.25')).toBeTruthy();
    expect(validateNominal('-3.24')).toBeTruthy();
    expect(validateNominal('-3.24 / -3.21')).toBeTruthy();
    expect(validateNominal('12')).toBeTruthy();
    expect(validateNominal('12-45')).toBeTruthy();
    expect(validateNominal('12 - 34')).toBeTruthy();
    expect(validateNominal('12/45')).toBeTruthy();
    expect(validateNominal('12 / 54')).toBeTruthy();
  });

  it('validates false for invalid nominals', () => {
    expect(validateNominal('3.24as')).toBeFalsy();
    expect(validateNominal('3.24 // 12')).toBeFalsy();
    expect(validateNominal('/3.23')).toBeFalsy();
    expect(validateNominal('3.24 -')).toBeFalsy();
    expect(validateNominal('3.24 $ 3.76')).toBeFalsy();
    expect(validateNominal('3. 12')).toBeFalsy();
    expect(validateNominal('3 / 3 / 3')).toBeFalsy();
  });

  it('returns accurately whether first date is newest', () => {
    expect(dateIsSameOrBefore('2020-10-10', '2020-12-10')).toBeFalsy();
    expect(dateIsSameOrBefore('2020-12-10', '2020-10-10')).toBeTruthy();
    expect(dateIsSameOrBefore('2020-10-10')).toBeTruthy();
    expect(dateIsSameOrBefore(1602288000, 1607558400)).toBeFalsy();
    expect(dateIsSameOrBefore(1607558400, 1602288000)).toBeTruthy();
    expect(dateIsSameOrBefore(1602288000)).toBeTruthy();
    expect(dateIsSameOrBefore('2020-10-10T00:00:00+00:00', '2020-12-10T00:00:00+00:00')).toBeFalsy();
    expect(dateIsSameOrBefore('2020-12-10T00:00:00+00:00', '2020-10-10T00:00:00+00:00')).toBeTruthy();
    expect(dateIsSameOrBefore('2020-10-10T00:00:00+00:00')).toBeTruthy();
  });
});

describe('Components', () => {
  it('Creates an Info Map from a Catalog when passed Product Handle', () => {
    const map = createComponentMap('Monthly', MOCK_CATALOG, MOCK_PRODUCT_COMPONENT_SET, 'inspectionxpert-cloud');
    expect(map[MOCK_PRODUCT_COMPONENT_SET[0].component_handle]).toMatchObject(MOCK_COMPONENT_INFO);
  });
  it('Creates an Info Map of the default Catalog when not passed Product Handle', () => {
    const map = createComponentMap('Monthly', MOCK_CATALOG, MOCK_PRODUCT_COMPONENT_SET);
    expect(map[MOCK_PRODUCT_COMPONENT_SET[0].component_handle]).toMatchObject(MOCK_COMPONENT_INFO);
  });
  it('Logs an error passed Product Handle for Non-Existent Product', () => {
    const consoleSpy = jest.spyOn(log, 'error');
    const map = createComponentMap(MOCK_CATALOG, MOCK_PRODUCT_COMPONENT_SET, 'Deffo-Fake');
    expect(map).toMatchObject({});
    expect(consoleSpy).toHaveBeenCalledTimes(1);
  });

  it('Calculates a Component Cost for a Tiered Component', () => {
    const cost = calculateComponentTotal({ [SS_COMPONENT.handle]: SS_COMPONENT });
    expect(cost).toBe(12500);
  });

  it('Calculates a Component Cost for a Per Unit Component', () => {
    const cost = calculateComponentTotal({ [PU_COMPONENT.handle]: PU_COMPONENT });
    expect(cost).toBe(25000);
  });

  it('Calculates Total Component Cost', () => {
    const cost = calculateComponentTotal({ [SS_COMPONENT.handle]: SS_COMPONENT, [PU_COMPONENT.handle]: PU_COMPONENT });
    expect(cost).toBe(37500);
  });
});

describe('Measurement validation', () => {
  it('returns Pass when value is Pass', () => {
    const status = validateMeasurement('Pass', {});
    expect(status).toBe('Pass');
  });

  it('returns Fail when value is Fail', () => {
    const status = validateMeasurement('Fail', {});
    expect(status).toBe('Fail');
  });

  it('Returns Unmeasered if there is no feature data of spec limits', () => {
    const a = validateMeasurement('3.14', null);
    const b = validateMeasurement('3.14', { id: 'test', nominal: 'test', fullSpecification: 'test' });
    expect(a).toBe('Unmeasured');
    expect(b).toBe('Unmeasured');
  });

  it('Returns Pass when the value is within the spec limits', () => {
    const results = [validateMeasurement('3.14', { upperSpecLimit: '3.14' }), validateMeasurement('3.14', { upperSpecLimit: '3.15', lowerSpecLimit: '3.13' }), validateMeasurement('3.14', { lowerSpecLimit: '3.13' })];

    results.forEach((res) => {
      expect(res).toBe('Pass');
    });
  });

  it('Returns Pass when the value is 0 and within specs', () => {
    const res = validateMeasurement('0', { upperSpecLimit: '0.5', lowerSpecLimit: '-0.5' });

    expect(res).toBe('Pass');
  });

  it('Returns Fail when the value is outside the spec limits', () => {
    const results = [
      validateMeasurement('3.14', { upperSpecLimit: '3.13' }),
      validateMeasurement('3.14', { upperSpecLimit: '3.11', lowerSpecLimit: '3.13' }),
      validateMeasurement('3.14', { upperSpecLimit: '3.15', lowerSpecLimit: '3.17' }),
      validateMeasurement('3.14', { lowerSpecLimit: '3.15' }),
    ];

    results.forEach((res) => {
      expect(res).toBe('Fail');
    });
  });

  it('Returns Fail when the value is 0 and outside specs', () => {
    const res = validateMeasurement('0', { upperSpecLimit: '0.5', lowerSpecLimit: '0.1' });

    expect(res).toBe('Fail');
  });
});

describe('Measurement display value', () => {
  it('should display an average when all values are within spec limits', () => {
    const display = getDisplayValue(range, [threeTwoFourPass, threeTwoSixPass]);
    expect(display).toBe('3.25');
  });

  it('should display the most extreme failure', () => {
    const display = getDisplayValue(range, [twoFail, fourFail]);
    expect(display).toBe('2');
  });

  it('should display pass for a passing attribute', () => {
    const display = getDisplayValue(range, [pass, pass]);
    expect(display).toBe('Pass');
  });

  it('should display fail for a failing attribute', () => {
    const display = getDisplayValue(range, [fail, pass]);
    expect(display).toBe('Fail');
  });

  it('should display a failing value for a max spec', () => {
    const display = getDisplayValue(max, [twoPass, fourFail]);
    expect(display).toBe('4');
  });

  it('should display a failing value for a min spec', () => {
    const display = getDisplayValue(min, [twoFail, fourPass]);
    expect(display).toBe('2');
  });

  it('should display an average for a passing max', () => {
    const display = getDisplayValue(max, [twoPass, threeTwoFourPass]);
    expect(display).toBe('2.62');
  });

  it('should display an average for a passing min', () => {
    const display = getDisplayValue(min, [fourPass, threeTwoFourPass, pass]);
    expect(display).toBe('3.62');
  });
});

describe('Sample Status Checker', () => {
  it('should return pass if all features have passing values', () => {
    const sample = { ...testSample };
    const features = [range, max, min];
    const featOneM = { ...threeTwoFourPass, characteristicId: 'feature-id-1' };
    const featTwoM = { ...threeTwoFourPass, characteristicId: 'feature-id-2' };
    const featThreeM = { ...threeTwoFourPass, characteristicId: 'feature-id-3' };
    sample.measurements = [featOneM, featTwoM, featThreeM];

    const status = checkSampleStatus(sample, features);
    expect(status).toBe('Pass');
  });

  it('should return fail if any features has a failing values', () => {
    const sample = { ...testSample };
    const features = [range, max, min];
    const featOneM = { ...twoFail, characteristicId: 'feature-id-1' };
    const featTwoM = { ...threeTwoFourPass, characteristicId: 'feature-id-2' };
    const featThreeM = { ...threeTwoFourPass, characteristicId: 'feature-id-3' };
    sample.measurements = [featOneM, featTwoM, featThreeM];

    const status = checkSampleStatus(sample, features);
    expect(status).toBe('Fail');
  });

  it('should return unmeasured if no values failed and at least one is unmeasured', () => {
    const sample = { ...testSample };
    const features = [range, max, min];
    const featOneM = { ...threeTwoFourPass, characteristicId: 'feature-id-1' };
    const featTwoM = { ...threeTwoFourPass, characteristicId: 'feature-id-2' };
    sample.measurements = [featOneM, featTwoM];

    const status = checkSampleStatus(sample, features);
    expect(status).toBe('Unmeasured');
  });
});
