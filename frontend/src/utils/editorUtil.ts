import Handsontable from 'handsontable';
import { i18n } from 'i18n';
import { v4 as uuid } from 'uuid';
import { message } from 'antd';
import { styles } from 'styleguide/AntDesignCustomTheme';
import { defaultTokens } from 'view/global/defaults';
import { SheetData, CellData } from 'styleguide/Excel/types';
import { TokenType } from 'types/reportTemplates';
import { cloneDeep } from 'lodash';

export interface Item {
  content: any;
  cell: string;
  type: string;
  id: string;
  token: TokenType | undefined;
}

export const getToken = (data: any, token: string) => {
  let matched: TokenType | undefined;

  Object.keys(data).forEach((category) => {
    data[category].children.forEach((t: any) => {
      if (token === t.token) {
        matched = t;
      }
    });
  });

  return matched;
};

export const hasDynamic = (list: Item[]) => {
  for (let i = 0; i < list.length; i++) {
    if (list[i].token && list[i].token!.type === 'Dynamic') return true;
  }
  return false;
};

// concatenates consecutive text fields, adds blank text fields at start and end if needed
export const cleanList = (list: Item[], row: number, col: number) => {
  const cleanedList: Item[] = [];

  let ind = 0;
  for (let i = 0; i < list.length; i++) {
    if (i === 0) {
      const frontItem = { id: uuid(), cell: `${row}-${col}-input-0`, content: '', type: 'text', token: undefined };
      cleanedList.push(frontItem);
    }
    if (list[i].type === 'text' && cleanedList[cleanedList.length - 1].type === 'text') {
      cleanedList[cleanedList.length - 1].content = (cleanedList[cleanedList.length - 1].content += ` ${list[i].content}`).trim();
    } else {
      cleanedList.push({ ...list[i], id: uuid(), cell: `${row}-${col}-input-${(ind += 1)}` });
    }
  }

  if (cleanedList[cleanedList.length - 1].type === 'token') {
    cleanedList.push({ id: uuid(), cell: `${row}-${col}-input-${cleanedList.length}`, content: '', type: 'text', token: undefined });
  }

  return cleanedList;
};

// removes a token from the list
export const removeItem = (list: Item[], item: Item) => {
  const result = [...list];
  const removed = result.splice(
    result.findIndex((a) => a?.cell === item?.cell),
    1,
  );

  return { list: result, token: removed[0].token };
};

// reorders the list after drag and drop
export const reorder = (list: Item[], startIndex: number, endIndex: number, remove = false) => {
  const result = list;
  const [removed] = result.splice(startIndex, 1);
  if (remove) {
    result.splice(endIndex, 0);
  } else {
    result.splice(endIndex, 0, removed);
  }

  return result;
};

// creates initial list of items from cell's text
export const initItems = (data: any, row: number, col: number) => {
  let bits: Item[] = [];
  if (data) {
    const trimmedData = data.trim();
    const words: string[] = trimmedData.split(' ');
    for (let i = 0; i < words.length; i++) {
      if (words[i].substring(0, 2) === '{{' && words[i].substring(words[i].length - 2, words[i].length) === '}}') {
        const token = getToken(defaultTokens, words[i]);
        const type = token ? 'token' : 'text';
        bits.push({ id: uuid(), content: words[i], cell: `${row}-${col}-input-${i}`, type, token });
      } else if (bits[bits.length - 1] && bits[bits.length - 1].type === 'text') {
        bits[bits.length - 1].content += ` ${words[i]}`;
      } else {
        bits.push({ id: uuid(), content: words[i], cell: `${row}-${col}-input-${i}`, type: 'text', token: undefined });
      }
    }
    if (bits[bits.length - 1].type === 'token') {
      bits.push({ id: uuid(), cell: `${row}-${col}-input-back`, content: '', type: 'text', token: undefined });
    }
    if (bits[0].type === 'token') {
      const front: Item = { id: uuid(), cell: `${row}-${col}-input-front`, content: '', type: 'text', token: undefined };
      bits = [front].concat(bits);
    }
  } else {
    bits.push({ id: uuid(), cell: `${row}-${col}-input-front`, content: '', type: 'text', token: undefined });
  }

  let r = row;
  if (row === null) r = Math.random() * 10000;

  return cleanList(bits, r, col);
};

// converts the items list into text to place in the cell
export const buildString = (items: Item[]) => {
  let str = '';

  items.forEach((item) => {
    str += `${item.content} `;
  });

  return str.replace(/&nbsp;/g, ' ').trim();
};

export const mergeText = (list: Item[], row: number, col: number) => {
  let tempList: Item[] = [];
  tempList = [];
  list.forEach((item) => {
    tempList.push({ id: uuid(), cell: item.cell, content: item.content, type: item.type, token: item.token });
  });

  let merged: Item[] = [{ id: uuid(), cell: `${row}-${col}-input-0`, content: '', type: 'text', token: undefined }];
  merged = [{ id: uuid(), cell: `${row}-${col}-input-0`, content: '', type: 'text', token: undefined }];
  let ind = 1;
  for (let i = 0; i < tempList.length; i++) {
    if (merged[merged.length - 1].type === 'text' && tempList[i].type === 'text') {
      merged[merged.length - 1].content = `${merged[merged.length - 1].content} ${tempList[i].content}`.trim().replace(/&nbsp;/g, ' ');
    } else {
      merged.push({ id: uuid(), cell: `${row}-${col}-input-${ind}`, content: tempList[i].content, type: tempList[i].type, token: tempList[i].token });
      ind += 1;
    }
  }

  if (merged[merged.length - 1].type === 'token') {
    merged.push({ id: uuid(), cell: `${row}-${col}-input-${ind}`, content: '', type: 'text', token: undefined });
  }

  return merged;
};

export const nextText = (ind: number, items: Item[]) => {
  for (let i = ind + 1; i < items.length; i++) {
    if (items[i].type === 'text') return i;
  }

  return ind;
};

export const prevText = (ind: number, items: Item[]) => {
  for (let i = ind - 1; i >= 0; i -= 1) {
    if (items[i].type === 'text') return i;
  }

  return ind;
};

export const hasText = (items: Item[]) => {
  if (items.length === 1 && items[0].content.trim().length === 0) return false;

  return true;
};

// checks if a given string represents a dynamic token
export const isTokenDynamic = (token: string | undefined, tokens: any) => {
  if (!token) return false;
  const tokenList = tokens || defaultTokens;

  let dynamic = false;
  Object.keys(tokenList).forEach((category) => {
    tokenList[category].children.forEach((t: any) => {
      if (token === t.token && t.type === 'Dynamic') {
        dynamic = true;
      }
    });
  });

  return dynamic;
};

// checks if the user double clicked a rich text cell and displays a message letting them know those cells can't be edited in IXC
export const handleRichCell = (event: MouseEvent, coords: Handsontable.wot.CellCoords, td: HTMLTableCellElement, data: SheetData[], currentSheet: number, step: number) => {
  const now = new Date().getTime();
  const ele: any = event.target;
  if (!ele) return;
  // check if dbl-clicked within 1/5th of a second. change 200 (milliseconds) to other value if you want
  if (!(ele.lastClick && now - ele.lastClick < 200)) {
    ele.lastClick = now;
    return; // no double-click detected
  }

  if (!data || currentSheet === undefined || currentSheet === null) {
    return;
  }

  const cell = data[currentSheet].rows[coords.row][coords.col];

  if (cell && cell.rich && step >= 1) {
    message.warning(i18n('entities.template.editor.richText'));
  }
};

export const removeTemp = (changeSheet: SheetData, data: SheetData[], currentSheet: number) => {
  const updatedSheet: SheetData = cloneDeep(changeSheet);
  if (!data) return updatedSheet;

  for (let i = 0; i < updatedSheet.rows.length; i++) {
    for (let j = 0; j < updatedSheet.columns.length; j++) {
      if (updatedSheet.rows[i][j]) {
        const cell = { ...updatedSheet.rows[i][j] };
        if (cell.tempStyle || cell.tempData) {
          delete cell.tempStyle;
          delete cell.tempData;
          updatedSheet.rows[i][j] = cell;
        }
      }
    }
  }

  return updatedSheet;
};

// sets up the footer row picker for the active dynamic cell on a horizontal template
export const setFooterRowPickerHorizontal = (rows: CellData[][], row: number, colStart: number, changeSheet: SheetData) => {
  const updatedSheet: SheetData = cloneDeep(changeSheet);
  const curRow = rows[row];
  let cell = null;
  for (let i = colStart; i < curRow.length; i++) {
    cell = { ...curRow[i] };
    if (!cell) break;
    cell.tempStyle = {
      ...cell.style,
      fontSize: '14px',
      backgroundColor: styles.colors.royalM4,
      border: '1px solid gray',
      fontWeight: 'normal',
    };
    cell.tempData = i18n('entities.template.editor.addFooter');
    updatedSheet.rows[row][i] = cell;
  }
  return updatedSheet;
};

// sets up the footer row picker for the active dynamic cell on a vertical template
export const setFooterRowPicker = (rows: CellData[][], rowStart: number, col: number, changeSheet: SheetData) => {
  const updatedSheet: SheetData = cloneDeep(changeSheet);
  const matches = rows[rowStart - 1][col].address.match(/(\D*)/);
  const tokenCellCol = matches ? matches[1] : null;
  let cell = null;
  let lastCellColSpan;
  for (let i = rowStart; i < rows.length; i++) {
    cell = { ...rows[i][col] };
    // if (!cell) break;
    if (cell && tokenCellCol) {
      const match = cell.address.match(/(\D*)/);
      const column = match ? match[1] : null;
      if ((column && column !== tokenCellCol) || (lastCellColSpan !== rows[i][col].colspan && lastCellColSpan)) break;
      cell.tempStyle = {
        ...cell.style,
        fontSize: '14px',
        backgroundColor: styles.colors.royalM4,
        border: '1px solid gray',
        fontWeight: 'normal',
      };
      cell.tempData = i18n('entities.template.editor.addFooter');
      updatedSheet.rows[i][col] = cell;
      lastCellColSpan = cell.colspan;
    }
  }
  return updatedSheet;
};

// unsets a footer row for the active dynamic cell on a vertical template
export const removeFooter = (rows: CellData[][], rowStart: number, col: number, changeSheet: SheetData, data: SheetData[], currentSheet: number) => {
  const updatedSheet: SheetData = cloneDeep(changeSheet);
  let cell = null;
  let tokenCell;

  if (!data) return updatedSheet;

  // remove the footer and let user set a new one
  for (let i = rowStart; i >= 0; i -= 1) {
    cell = { ...rows[i][col] };
    if (cell) {
      if (cell.footerRow) {
        cell.footerRow = -1;
        cell.ind = { row: i, col };
        tokenCell = cell;
      } else {
        cell.tempData = i18n('entities.template.editor.addFooter');
        cell.tempStyle = {
          ...cell.style,
          fontSize: '14px',
          backgroundColor: styles.colors.royalM4,
          border: '1px solid gray',
          fontWeight: 'normal',
        };
      }
      updatedSheet.rows[i][col] = cell;
    }
  }

  // remove the temp styles from the highlighted footer row
  const cols = updatedSheet.columns.length;
  for (let i = 0; i < cols; i++) {
    cell = { ...rows[rowStart][i] };
    if (!cell) break;
    if (i !== col && cell.tempStyle) {
      delete cell.tempStyle;
      updatedSheet.rows[rowStart][i] = cell;
    }
  }

  setFooterRowPicker(rows, rowStart, col, updatedSheet);
  return { tokenCell, updatedSheet };
};

// unsets a footer row for a dynamic cell on a horizontal template
export const removeFooterHorizontal = (rows: CellData[][], row: number, colStart: number, changeSheet: SheetData, data: SheetData[]) => {
  const updatedSheet: SheetData = cloneDeep(changeSheet);
  let cell = null;
  const curRow = rows[row];
  let tokenCell;

  if (!data) return updatedSheet;

  // remove the footer and let user set a new one
  for (let i = colStart; i >= 0; i -= 1) {
    cell = { ...curRow[i] };
    if (!cell) break;
    if (cell.footerRow) {
      cell.footerRow = -1;
      tokenCell = cell;
    } else {
      cell.tempData = i18n('entities.template.editor.addFooter');
      cell.tempStyle = {
        ...cell.style,
        fontSize: '14px',
        backgroundColor: styles.colors.royalM4,
        border: '1px solid gray',
        fontWeight: 'normal',
      };
    }
    updatedSheet.rows[row][i] = cell;
  }

  // remove the temp styles from the highlighted footer row
  // const cols = updatedSheet.columns.length;
  for (let i = 0; i < rows.length; i++) {
    cell = { ...rows[i][colStart] };
    if (!cell) break;
    if (i !== row && cell.tempStyle) {
      delete cell.tempStyle;
      updatedSheet.rows[i][colStart] = cell;
    }
  }

  setFooterRowPickerHorizontal(rows, row, colStart, updatedSheet);
  return { tokenCell, updatedSheet };
};

// sets a footer row for the active dynamic cell on a vertical template
export const setFooterRow = (rows: CellData[][], rowStart: number, col: number, changeSheet: SheetData, data: SheetData[], currentSheet: number) => {
  const updatedSheet: SheetData = cloneDeep(changeSheet);
  let cell = null;
  let tokenCell = null;

  if (!data || !rows) return updatedSheet;

  // remove the add footer text from the column cells above the footer row, leave the tempStyle
  for (let i = rowStart; i >= 0; i -= 1) {
    cell = { ...rows[i][col] };
    if (cell) {
      if (cell.footerRow !== null && cell.footerRow !== undefined) {
        cell.footerRow = rowStart;
        cell.ind = { row: i, col };
        tokenCell = cell;
      } else {
        cell.tempData = '';
        cell.tempStyle = {
          ...cell.style,
          fontSize: '14px',
          backgroundColor: styles.colors.royalM4,
          border: '1px solid gray',
          fontWeight: 'normal',
        };
      }
      updatedSheet.rows[i][col] = cell;
    }
  }

  // remove the temp data and styles from the cells below the selected footer row
  for (let i = rowStart + 1; i < rows.length; i++) {
    cell = { ...rows[i][col] };
    if (!cell || !cell.tempData) break;
    delete cell.tempData;
    delete cell.tempStyle;
    updatedSheet.rows[i][col] = cell;
  }

  // set a tempStyle for the selected row, change the clicked cell's style and text for removal
  const cols = updatedSheet.columns.length;
  for (let i = 0; i < cols; i++) {
    cell = { ...rows[rowStart][i] };
    if (cell) {
      if (i === col) {
        cell.tempStyle = {
          ...cell.style,
          fontSize: '14px',
          backgroundColor: styles.colors.coralM1,
          border: '1px solid gray',
          fontWeight: 'normal',
          boxShadow: '0px 0px 8px rgba(0,0,0,0.25)',
        };
        cell.tempData = i18n('entities.template.editor.removeFooter');
      } else {
        cell.tempStyle = {
          ...cell.style,
          fontSize: '14px',
          backgroundColor: styles.colors.royalM4,
          border: '1px solid gray',
          fontWeight: 'normal',
        };
      }
      updatedSheet.rows[rowStart][i] = cell;
    }
  }

  return { tokenCell, updatedSheet };
};

// sets a footer row for the active dynamic cell on a horizontal template
export const setFooterRowHorizontal = (row: number, colStart: number, changeSheet: SheetData, data: SheetData[], currentSheet: number) => {
  const updatedSheet: SheetData = cloneDeep(changeSheet);
  let cell = null;
  let tokenCell;
  const curRow = { ...updatedSheet.rows[row] };

  if (!data) return updatedSheet;

  // remove the add footer text from the row cells to the left of the footer row, leave the tempStyle
  for (let i = colStart; i >= 0; i -= 1) {
    cell = { ...curRow[i] };
    if (cell) {
      if (cell.footerRow !== null && cell.footerRow !== undefined) {
        cell.footerRow = colStart;
        cell.ind = { row, col: i };
        tokenCell = cell;
      } else {
        cell.tempData = '';
        cell.tempStyle = {
          ...cell.style,
          fontSize: '14px',
          backgroundColor: styles.colors.royalM4,
          border: '1px solid gray',
          fontWeight: 'normal',
        };
      }
      updatedSheet.rows[row][i] = cell;
    }
  }

  // remove the temp data and styles from the cells to the right of the selected footer row
  for (let i = colStart + 1; i < curRow.length; i++) {
    cell = { ...curRow[i] };
    if (!cell || !cell.tempData) break;
    delete cell.tempData;
    delete cell.tempStyle;
    updatedSheet.rows[row][i] = cell;
  }

  // set a tempStyle for the selected column, change the clicked cell's style and text for removal
  for (let i = 0; i < updatedSheet.rows.length; i++) {
    cell = { ...updatedSheet.rows[i][colStart] };
    if (cell) {
      if (i === row) {
        cell.tempStyle = {
          ...cell.style,
          fontSize: '14px',
          backgroundColor: styles.colors.coralM1,
          border: '1px solid gray',
          fontWeight: 'normal',
          boxShadow: '0px 0px 8px rgba(0,0,0,0.25)',
        };
        cell.tempData = i18n('entities.template.editor.removeFooter');
      } else {
        cell.tempStyle = {
          ...cell.style,
          fontSize: '14px',
          backgroundColor: styles.colors.royalM4,
          border: '1px solid gray',
          fontWeight: 'normal',
        };
      }
      updatedSheet.rows[i][colStart] = cell;
    }
  }

  return { tokenCell, updatedSheet };
};

// initializes a footer row for an active dynamic cell on a vertical template
export const initFooterRow = (data: SheetData[], currentSheet: number, row: number, col: number, changeSheet: SheetData) => {
  let updatedSheet: SheetData = cloneDeep(changeSheet);
  if (!updatedSheet) return { changes: updatedSheet, row: -1 };
  const { rows } = updatedSheet;

  const tempCell = updatedSheet.rows[row][col];
  if (!tempCell.footerRow) tempCell.footerRow = -1;

  let footerRow;
  let tokenCell = { ...tempCell };

  // check cells in this row for footers and match if found
  for (let j = 0; j < updatedSheet.columns.length; j++) {
    if (rows[row][j]) {
      const cell = rows[row][j];
      if (cell.footerRow && cell.footerRow !== -1) {
        footerRow = cell.footerRow;
        tokenCell = { ...cell };
      }
    }
  }

  if (!footerRow) {
    footerRow = -1;
    updatedSheet = setFooterRowPicker(updatedSheet.rows, row + 1, col, updatedSheet);
  } else {
    const footerSet = setFooterRow(rows, footerRow, col, updatedSheet, data, currentSheet);
    tokenCell = footerSet.tokenCell;
    updatedSheet = footerSet.updatedSheet;
  }

  const returnedCell = tokenCell.cell ? tokenCell.cell : tokenCell;

  return { changes: updatedSheet, row: footerRow, cell: returnedCell };
};

// initializes a footer row for an active dynamic cell on a horizontal template
export const initFooterRowHorizontal = (data: SheetData[], currentSheet: number, row: number, col: number, changeSheet: SheetData) => {
  let updatedSheet: SheetData = cloneDeep(changeSheet);
  if (!updatedSheet) return { changes: updatedSheet, row: -1 };
  const { rows } = updatedSheet;

  let footerRow;
  for (let j = 0; j < rows.length; j++) {
    if (rows[j][col]) {
      const cell = rows[j][col];
      if (cell.footerRow && cell.footerRow !== -1) {
        footerRow = cell.footerRow;
      }
    }
  }

  if (!footerRow) {
    footerRow = -1;
    updatedSheet = setFooterRowPickerHorizontal(updatedSheet.rows, row, col + 1, updatedSheet);
  } else {
    const cell = updatedSheet.rows[row][col];
    cell.footerRow = -1;
    const footerSet = setFooterRowHorizontal(row, footerRow, updatedSheet, data, currentSheet);
    updatedSheet = footerSet.updatedSheet;
  }

  return { changes: updatedSheet, row: footerRow };
};

// parses out all substrings in token fromat {{token}} from a string
export const getTokens = (str: string) => {
  if (!str) return [];
  const reg = /{{[a-zA-Z._]*}}/g;
  const match = str.toString().match(reg);

  return match !== null ? match : [];
};

// checks if a given string contains a Dynamic token
export const containsDynamic = (str: string) => {
  const match = getTokens(str);
  if (match !== null) {
    for (let i = 0; i < match.length; i++) {
      const token = getToken(defaultTokens, match[i]);
      if (token && token.type === 'Dynamic') return true;
    }
  }
  return false;
};

// removes a token from a cell as a result of dragging the token out and dropping it in elsewhere
export const removeToken = (row: number, col: number, td: HTMLTableCellElement, changeSheet: SheetData, data: SheetData[], currentSheet: number, token: TokenType | undefined, oldValue: string, direction = 'Vertical') => {
  const updatedSheet: SheetData = cloneDeep(changeSheet);
  if (!data || currentSheet === undefined || !token) {
    return { updatedCell: undefined, removed: undefined };
  }
  const cell = { ...updatedSheet.rows[row][col] };

  cell.data = cell.data ? oldValue.replace(token.token, '') : '';

  let removedFooter = false;

  if (token.type === 'Dynamic' && !containsDynamic(cell.data)) {
    delete cell.footerRow;
    removedFooter = true;
  }
  updatedSheet.rows[row][col] = cell;

  return { updatedCell: cell, removed: removedFooter, updatedSheet };
};

// TODO: handle edge case where str = {{not token}} {{Token}}
export const addToken = (str: string, cell: string, row: number, col: number, data: SheetData[], currentSheet: number) => {
  const idBits = cell.split('-');
  const idNum = parseInt(idBits[3], 10);
  const start = /{{/;
  const end = /}}/;

  const startInd = str.match(start) ? str.match(start)!.index : 0;
  const endInd = str.match(end) ? str.match(end)!.index : 0;

  const prefix = str.substring(0, startInd);
  const token = str.substring(startInd!, endInd! + 2);
  const suffix = str.substring(endInd! + 2, str.length);

  const items: Item[] = [];
  let inc = 0;
  if (prefix.trim().length > 0) {
    items.push({
      id: uuid(),
      cell: `${idBits[0]}-${idBits[1]}-input-${idNum}`,
      content: prefix.replace(/&nbsp;/g, ' '),
      type: 'text',
      token: undefined,
    });
    inc += 1;
  }
  const t = getToken(defaultTokens, token);
  const type = t ? 'token' : 'text';
  items.push({ id: uuid(), cell: `${idBits[0]}-${idBits[1]}-input-${idNum + inc}`, content: token, type, token: t });
  inc += 1;
  items.push({
    id: uuid(),
    cell: `${idBits[0]}-${idBits[1]}-input-${idNum + inc}`,
    content: suffix.replace(/&nbsp;/g, ' '),
    type: 'text',
    token: undefined,
  });

  let setFooter;
  if (t && t.type === 'Dynamic' && !data[currentSheet].rows[row][col].footerRow && data[currentSheet].rows[row][col].footerRow !== -1) {
    setFooter = initFooterRow(data, currentSheet, row, col, data[currentSheet]).cell;
  }

  return { list: cleanList(items, row, col), footer: setFooter, token: t };
};

// adds a token to a cell as a result of dragging in a token from the list or another cell
export const addDraggedToken = (row: number, col: number, td: HTMLTableCellElement, changeSheet: SheetData, data: SheetData[], currentSheet: number, token: string | undefined, direction = 'Vertical') => {
  let updatedSheet: SheetData = cloneDeep(changeSheet);
  if (!data || currentSheet === undefined || !token) {
    return updatedSheet;
  }
  const cell = { ...updatedSheet.rows[row][col] };

  cell.data = cell.data ? (cell.data += ` ${token}`) : ` ${token}`;
  if (token && isTokenDynamic(token, defaultTokens)) {
    const tempCell = updatedSheet.rows[row][col];
    if (!tempCell.footerRow) tempCell.footerRow = -1;
    const footerInfo = direction === 'Vertical' ? initFooterRow(data, currentSheet, row, col, changeSheet) : initFooterRowHorizontal(data, currentSheet, row, col, changeSheet);
    cell.footerRow = footerInfo.row;
    tempCell.footerRow = footerInfo.row;
    updatedSheet = footerInfo.changes;
  }
  updatedSheet.rows[row][col] = cell;

  return { cell, updatedSheet };
};

// removes all of the set footers for Dynamic tokens after a template direction change
export const removeAllFooters = (data: SheetData[], currentChanges?: SheetData[]) => {
  const updatedChanges: SheetData[] = currentChanges ? cloneDeep(currentChanges) : cloneDeep(data);
  data.forEach((sheet, index) => {
    if (sheet) {
      if (!updatedChanges[index]) {
        updatedChanges[index] = cloneDeep(sheet);
      }
      sheet.rows.forEach((row: CellData[], rowInd: number) => {
        if (row) {
          if (!updatedChanges[index].rows[rowInd]) {
            updatedChanges[index].rows[rowInd] = cloneDeep(row);
          }
          row.forEach((cell: CellData, col: number) => {
            if (cell.footerRow) {
              updatedChanges[index].rows[rowInd][col] = { ...cell, footerRow: -1 };
            }
          });
        }
      });
    }
  });

  return updatedChanges;
};
