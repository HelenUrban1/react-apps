/* eslint-disable prettier/prettier */
import { cloneDeep } from 'lodash';
import {
  isTokenDynamic,
  removeFooter,
  removeTemp,
  setFooterRow,
  setFooterRowPicker,
  addToken,
  removeAllFooters,
  removeFooterHorizontal,
  setFooterRowHorizontal,
  setFooterRowPickerHorizontal,
  reorder,
  initItems,
  buildString,
  cleanList,
  removeItem,
  nextText,
  prevText,
  mergeText,
  hasText,
  addDraggedToken,
} from './editorUtil';

const data = [
  {
    name: 'form1',
    columns: [12, 80, 62, 62, 62],
    rows: [
      [
        { address: 'A1', data: '', styles: {} },
        { address: 'B1', data: '', styles: {}, footerRow: 3 },
        { address: 'C1', data: '', styles: {}, footerRow: -1 },
        { address: 'D1', data: '', styles: {} },
      ],
      [
        { address: 'A2', data: '', styles: {} },
        { address: 'B2', data: '', styles: {} },
        { address: 'C2', data: '', styles: {} },
        { address: 'D2', data: '', styles: {} },
      ],
      [
        { address: 'A3', data: '', styles: {} },
        { address: 'B3', data: '', styles: {} },
        { address: 'C3', data: '', styles: {} },
        { address: 'D3', data: '', styles: {} },
      ],
      [
        { address: 'A4', data: '', styles: {} },
        { address: 'B4', data: '', styles: {} },
        { address: 'C4', data: '', styles: {} },
        { address: 'D4', data: '', styles: {} },
      ],
    ],
  },
];

const horizontalData = [
  {
    name: 'form1',
    columns: [12, 80, 62, 62, 62],
    rows: [
      [
        { address: 'A1', data: '', styles: {} },
        { address: 'B1', data: '', styles: {}, footerRow: 5 },
        { address: 'C1', data: '', styles: {} },
        { address: 'D1', data: '', styles: {} },
      ],
      [
        { address: 'A2', data: '', styles: {} },
        { address: 'B2', data: '', styles: {}, footerRow: -1 },
        { address: 'C2', data: '', styles: {} },
        { address: 'D2', data: '', styles: {} },
      ],
      [
        { address: 'A3', data: '', styles: {} },
        { address: 'B3', data: '', styles: {} },
        { address: 'C3', data: '', styles: {} },
        { address: 'D3', data: '', styles: {} },
      ],
      [
        { address: 'A4', data: '', styles: {} },
        { address: 'B4', data: '', styles: {} },
        { address: 'C4', data: '', styles: {} },
        { address: 'D4', data: '', styles: {} },
      ],
    ],
  },
];

const tempData = [
  {
    name: 'form1',
    columns: [80, 80, 80, 80],
    rows: [
      [
        { address: 'A1', data: '', styles: {}, tempData: 'Temp', tempStyle: {} },
        { address: 'B1', data: '', styles: {}, tempData: 'Temp', tempStyle: {} },
        { address: 'C1', data: '', styles: {}, tempData: 'Temp', tempStyle: {} },
        { address: 'D1', data: '', styles: {}, tempData: 'Temp', tempStyle: {} },
      ],
      [
        { address: 'A2', data: '', styles: {}, tempData: 'Temp', tempStyle: {} },
        { address: 'B2', data: '', styles: {}, tempData: 'Temp', tempStyle: {} },
        { address: 'C2', data: '', styles: {}, tempData: 'Temp', tempStyle: {} },
        { address: 'D2', data: '', styles: {}, tempData: 'Temp', tempStyle: {} },
      ],
      [
        { address: 'A3', data: '', styles: {}, tempData: 'Temp', tempStyle: {} },
        { address: 'B3', data: '', styles: {}, tempData: 'Temp', tempStyle: {} },
        { address: 'C3', data: '', styles: {}, tempData: 'Temp', tempStyle: {} },
        { address: 'D3', data: '', styles: {}, tempData: 'Temp', tempStyle: {} },
      ],
      [
        { address: 'A4', data: '', styles: {}, tempData: 'Temp', tempStyle: {} },
        { address: 'B4', data: '', styles: {}, tempData: 'Temp', tempStyle: {} },
        { address: 'C4', data: '', styles: {}, tempData: 'Temp', tempStyle: {} },
        { address: 'D4', data: '', styles: {}, tempData: 'Temp', tempStyle: {} },
      ],
    ],
  },
];
const testItems = [
  {
    id: 1,
    cell: '1',
    content: 'test',
    type: 'text',
  },
  {
    id: 2,
    cell: '2',
    content: '{{Part.Name}}',
    type: 'token',
  },
];

describe('The SelectedCell Functions', () => {
  it('for Reording a List should work', () => {
    const testList = [...testItems];
    const newList = cleanList(reorder(testList, 1, 0, false), 1, 2);
    const expectedList = [
      { cell: '1-2-input-0', content: '', type: 'text' },
      { cell: '1-2-input-1', content: '{{Part.Name}}', type: 'token' },
      { cell: '1-2-input-2', content: 'test', type: 'text' },
    ];

    expect(newList[0].cell).toEqual(expectedList[0].cell);
    expect(newList[0].content).toEqual(expectedList[0].content);
    expect(newList[1].cell).toEqual(expectedList[1].cell);
    expect(newList[1].content).toEqual(expectedList[1].content);
    expect(newList[2].cell).toEqual(expectedList[2].cell);
    expect(newList[2].content).toEqual(expectedList[2].content);
  });

  it('for Initializing a List of Items should work', () => {
    const initList = initItems('test {{Part.Name}}');
    expect(initList[0].content).toEqual('test');
    expect(initList[1].content).toEqual('{{Part.Name}}');
    expect(initList[2].content).toEqual('');
  });

  it('for Building a String from Items should work', () => {
    const test = [
      {
        cell: '1',
        content: 'test',
        type: 'text',
      },
      {
        cell: '2',
        content: '{{Part.Name}}',
        type: 'token',
      },
    ];
    expect('test {{Part.Name}}').toEqual(buildString(test));
  });

  it('for Cleaning a List should work', () => {
    const testList = [...testItems];
    const cleaned = cleanList(testList, 1, 2);
    delete cleaned[0].id;
    delete cleaned[1].id;
    delete cleaned[2].id;
    expect(cleaned[0]).toEqual({ cell: '1-2-input-0', content: 'test', type: 'text' });
    expect(cleaned[1]).toEqual({ cell: '1-2-input-1', content: '{{Part.Name}}', type: 'token' });
    expect(cleaned[2]).toEqual({ cell: '1-2-input-2', content: '', type: 'text' });
  });

  it('for Removing a Token from the List', () => {
    const testList = [...testItems];
    const newList = removeItem(
      testList,
      {
        cell: '2',
        content: '{{Part.Name}}',
        type: 'token',
      },
      1,
      2,
    );
    expect(newList.list[0].content).toEqual('test');
    expect(newList.list[1]).toEqual(undefined);
  });

  it('should add a Token', () => {
    const ret = addToken('test {{Part.Name}}', '1-1-input-0', 1, 1);
    const expected = [
      { cell: '1-1-input-0', content: 'test', type: 'text' },
      { cell: '1-1-input-1', content: '{{Part.Name}}', type: 'token' },
      { cell: '1-1-input-2', content: '', type: 'text' },
    ];
    expect(ret.list[0].content).toEqual(expected[0].content);
    expect(ret.list[1].cell).toEqual(expected[1].cell);
    expect(ret.list[2].type).toEqual(expected[2].type);
    expect(ret.token).toEqual({ group: 'Part_Info', name: 'Part Name', token: '{{Part.Name}}', example: 'MULTI-TOOL', type: 'Single' });
  });

  it('should return true if items has text', () => {
    const expected = [
      { cell: '1-1-input-0', content: 'test', type: 'text' },
      { cell: '1-1-input-1', content: '{{Part.Name}}', type: 'token' },
      { cell: '1-1-input-2', content: '', type: 'text' },
    ];
    expect(hasText(expected)).toEqual(true);
  });

  it('should return false if items has no text', () => {
    const expected = [{ cell: '1-1-input-0', content: '', type: 'text' }];
    expect(hasText(expected)).toEqual(false);
  });

  it('should return the index of the next text field', () => {
    const expected = [
      { cell: '1-1-input-0', content: 'test', type: 'text' },
      { cell: '1-1-input-1', content: '{{Part.Name}}', type: 'token' },
      { cell: '1-1-input-2', content: '', type: 'text' },
      { cell: '1-1-input-3', content: '{{Part.Name}}', type: 'token' },
      { cell: '1-1-input-4', content: '{{Part.Name}}', type: 'token' },
      { cell: '1-1-input-5', content: 'this', type: 'text' },
    ];
    expect(nextText(2, expected)).toEqual(5);
  });

  it('should return the index of the previous text field', () => {
    const expected = [
      { cell: '1-1-input-0', content: 'test', type: 'text' },
      { cell: '1-1-input-1', content: '{{Part.Name}}', type: 'token' },
      { cell: '1-1-input-2', content: '', type: 'text' },
      { cell: '1-1-input-3', content: '{{Part.Name}}', type: 'token' },
      { cell: '1-1-input-4', content: '{{Part.Name}}', type: 'token' },
      { cell: '1-1-input-5', content: 'this', type: 'text' },
    ];
    expect(prevText(5, expected)).toEqual(2);
  });

  it('should merge text and add buffers to list', () => {
    const res = mergeText(
      [
        { cell: '1-1-input-0', content: '{{Part.Name}}', type: 'token' },
        { cell: '1-1-input-1', content: 'text', type: 'text' },
        { cell: '1-1-input-2', content: 'two', type: 'text' },
        { cell: '1-1-input-3', content: '{{Part.Number}}', type: 'token' },
        { cell: '1-1-input-4', content: 'end', type: 'text' },
      ],
      1,
      1,
    );
    const expected = [
      { cell: '1-1-input-0', content: '', type: 'text' },
      { cell: '1-1-input-1', content: '{{Part.Name}}', type: 'token' },
      { cell: '1-1-input-2', content: 'text two', type: 'text' },
      { cell: '1-1-input-3', content: '{{Part.Number}}', type: 'token' },
      { cell: '1-1-input-4', content: 'end', type: 'text' },
    ];
    delete res[0].id;
    delete res[1].id;
    delete res[2].id;
    delete res[3].id;
    delete res[4].id;
    expect(res).toEqual(expected);
  });
});

describe('The Handsontable Cell Editing utilities', () => {
  it('Can Add a Dynamic Token', () => {
    const testData = [...data];
    const td = { lastClick: 0 };
    const { cell, updatedSheet } = addDraggedToken(2, 2, td, testData[0], testData, 0, '{{Feature.Location}}');
    expect(cell.data).toMatch(/{{Feature.Location}}/);
    expect(cell.footerRow).toEqual(-1);
    expect(updatedSheet.rows[2][2].data).toMatch(/{{Feature.Location}}/);
    expect(updatedSheet.rows[2][2].footerRow).toEqual(-1);
    expect(updatedSheet.rows[3][2].tempData).toEqual('Last Data Row');
  });

  it('Can Add a Single Value Token', () => {
    const testData = [...data];
    const td = { lastClick: 0 };
    const { cell, updatedSheet } = addDraggedToken(0, 0, td, testData[0], testData, 0, '{{single}}');

    expect(cell.data).toMatch(/{{single}}/);
    expect(cell.footerRow).toEqual(undefined);
    expect(updatedSheet.rows[0][0].data).toMatch(/{{single}}/);
    expect(updatedSheet.rows[0][0].footerRow).toEqual(undefined);
  });

  it('Can check if a token is dynamic', () => {
    const res = isTokenDynamic('{{Feature.Location}}');
    expect(res).toBeTruthy();
  });

  it('Returns false if a token is not Dynamic', () => {
    const res = isTokenDynamic('{{Dynamic.Token}}');
    expect(res).toBeFalsy();
  });

  it('Sets a footer row for vertical templates', () => {
    const testData = [...data];
    const { tokenCell, updatedSheet } = setFooterRow(testData[0].rows, 3, 2, testData[0], testData, 0);
    expect(updatedSheet.rows[1][2].tempData).toEqual('');
    expect(updatedSheet.rows[2][2].tempData).toEqual('');
    expect(updatedSheet.rows[3][2].tempData).toEqual('Remove');

    // column colors
    expect(updatedSheet.rows[1][2].tempStyle.backgroundColor).toEqual('#E8E6F7');
    expect(updatedSheet.rows[2][2].tempStyle.backgroundColor).toEqual('#E8E6F7');
    expect(updatedSheet.rows[3][2].tempStyle.backgroundColor).toEqual('#F5C3BB');
    // row colors
    expect(updatedSheet.rows[3][1].tempStyle.backgroundColor).toEqual('#E8E6F7');
    expect(updatedSheet.rows[3][3].tempStyle.backgroundColor).toEqual('#E8E6F7');

    expect(updatedSheet.rows[0][2].footerRow).toEqual(3);
    expect(tokenCell.footerRow).toEqual(3);
  });

  it('Sets a footer row for horizontal templates', () => {
    const testData = [...horizontalData];
    const { tokenCell, updatedSheet } = setFooterRowHorizontal(1, 3, testData[0], testData, 0);

    expect(updatedSheet.rows[1][2].tempData).toEqual(''); // C2
    expect(updatedSheet.rows[1][3].tempData).toEqual('Remove'); // D2

    // column colors
    expect(updatedSheet.rows[1][2].tempStyle.backgroundColor).toEqual('#E8E6F7'); // C2
    expect(updatedSheet.rows[1][3].tempStyle.backgroundColor).toEqual('#F5C3BB'); // D2
    // row colors
    expect(updatedSheet.rows[0][3].tempStyle.backgroundColor).toEqual('#E8E6F7'); // D1
    expect(updatedSheet.rows[2][3].tempStyle.backgroundColor).toEqual('#E8E6F7'); // D3
    expect(updatedSheet.rows[3][3].tempStyle.backgroundColor).toEqual('#E8E6F7'); // D4

    expect(updatedSheet.rows[1][1].footerRow).toEqual(3); // B2
    expect(tokenCell.footerRow).toEqual(3);
  });

  it('Displays the temp styles and data for the footer row', () => {
    const testData = [...data];
    const res = setFooterRowPicker(testData[0].rows, 2, 2, testData[0]);
    expect(res.rows[2][2].tempData).toEqual('Last Data Row');
    expect(res.rows[3][2].tempData).toEqual('Last Data Row');
    expect(res.rows[1][2].tempData).toEqual(undefined);
  });

  it('Displays the temp styles and data for horizontal footer rows', () => {
    const testData = [...data];
    const res = setFooterRowPickerHorizontal(testData[0].rows, 2, 1, testData[0]);
    expect(res.rows[2][1].tempData).toEqual('Last Data Row');
    expect(res.rows[2][2].tempData).toEqual('Last Data Row');
    expect(res.rows[2][3].tempData).toEqual('Last Data Row');
    expect(res.rows[3][0].tempData).toEqual(undefined);
  });

  it('Removes a vertical footer', () => {
    const testData = [...data];
    const rowsWithFooter = cloneDeep(data[0].rows);
    rowsWithFooter[0][1].footerRow = 1;
    rowsWithFooter[1][1].tempData = 'Remove';
    rowsWithFooter[1][1].tempStyle = {
      fontSize: '14px',
      backgroundColor: '#F5C3BB',
      border: '1px solid gray',
      fontWeight: 'normal',
      boxShadow: '0px 0px 8px rgba(0,0,0,0.25)',
    };
    const { tokenCell, updatedSheet } = removeFooter(rowsWithFooter, 1, 1, { ...testData[0], rows: rowsWithFooter }, testData, 0);
    expect(updatedSheet.rows[1][1].tempData).toEqual('Last Data Row');
    expect(updatedSheet.rows[0][1].tempData).toEqual(undefined);
    expect(updatedSheet.rows[0][1].footerRow).toEqual(-1);
    expect(tokenCell.tempData).toEqual(undefined);
    expect(tokenCell.footerRow).toEqual(-1);
  });

  it('Removes a horizontal footer', () => {
    const testData = [...horizontalData];
    const colWithFooter = cloneDeep(testData[0]);
    colWithFooter.rows[0][1].footerRow = 1;
    colWithFooter.rows[0][2].tempData = 'Remove';
    colWithFooter.rows[0][2].tempStyle = {
      fontSize: '14px',
      backgroundColor: '#F5C3BB',
      border: '1px solid gray',
      fontWeight: 'normal',
      boxShadow: '0px 0px 8px rgba(0,0,0,0.25)',
    };
    const { tokenCell, updatedSheet } = removeFooterHorizontal(colWithFooter.rows, 0, 2, colWithFooter, testData);
    expect(updatedSheet.rows[0][2].tempData).toEqual('Last Data Row');
    expect(updatedSheet.rows[0][1].tempData).toEqual(undefined);
    expect(updatedSheet.rows[0][1].footerRow).toEqual(-1);
    expect(tokenCell.tempData).toEqual(undefined);
    expect(tokenCell.footerRow).toEqual(-1);

    // expect(res.rows[0][2].tempData).toEqual('Last Data Row');
    // expect(res.rows[0][3].tempStyle.fontSize).toEqual('14px');
    // expect(testData[0].rows[0][1].footerRow).toEqual(-1);
  });

  it('Removes temporary styling and data', () => {
    const testData = [...tempData];
    const res = removeTemp(testData[0], testData, 0);
    expect(res.rows[0][0].tempData).toEqual(undefined);
    expect(res.rows[0][0].tempStyle).toEqual(undefined);
    expect(res.rows[1][1].tempData).toEqual(undefined);
    expect(res.rows[1][1].tempStyle).toEqual(undefined);
    expect(res.rows[2][2].tempData).toEqual(undefined);
    expect(res.rows[2][2].tempStyle).toEqual(undefined);
    expect(res.rows[3][3].tempData).toEqual(undefined);
    expect(res.rows[3][3].tempStyle).toEqual(undefined);
  });

  it('Unsets all footers when switching directions', () => {
    const testData = [...horizontalData];
    const res = removeAllFooters(testData);
    expect(testData['0'].rows[0][1].footerRow).toEqual(5);
    expect(res['0'].rows[0][1].footerRow).toEqual(-1);
  });
});
