export const styleToInline = (style: { [key: string]: any }) => {
  if (!style) return;
  const holderElement = new Option();
  Object.keys(style).forEach((a: any) => {
    if (a === 'font') {
      holderElement.style.fontFamily = style[a];
    }
    holderElement.style[a] = style[a];
  });
  return holderElement.getAttribute('style');
};

export const converRgbToHex = (rgb: number[]) => {
  // eslint-disable-next-line no-bitwise
  return `#${((1 << 24) + (rgb[0] << 16) + (rgb[1] << 8) + rgb[2]).toString(16).slice(1)}`;
};

export const convertHexToRgb = (hex: string) => {
  const result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
  return result ? [parseInt(result[1], 16), parseInt(result[2], 16), parseInt(result[3], 16), 1] : null;
};
