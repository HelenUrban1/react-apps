// import config from 'config';
// import axios from '../modules/shared/network/axios';

import log from 'loglevel';

const config = require('config').default;
const axios = require('../modules/shared/network/axios').default;

export const interpretNotationText = async (text: string, debug: boolean = false) => {
  // Prepare a standard result object
  // const result: { result?: any; error?: any } = {};
  // const result;

  // Validate input
  // TODO: Throw real InvalidParameter error object
  if (!text || typeof text !== 'string') {
    log.debug(text);
    throw new Error(`Notation Interpreter requires text as input`);
  }

  // Prepare the post body
  const data: { text: string; debug?: undefined | boolean } = { text };

  // sanitize text string
  // TODO: sanitization should be handled by the notation parser
  data.text = text.replace(/^\n|\n$/g, '');

  // Determine if debugging should be enabled
  if (debug || config.env === 'development') data.debug = true;

  // try {
  const response = await axios({
    url: config.enpUrl,
    dataType: 'json',
    method: 'POST',
    headers: { 'Content-Type': 'application/json; charset=utf-8' },
    withCredentials: false,
    data,
  });
  // If debugging is enabled, write the output to the console
  if (data.debug) {
    log.debug('enp.response', response);
    log.debug('enp.response.data.trace', response.data.trace);
    log.debug('enp.response.data.result', response.data.result);
    log.debug('enp.response.data.error', response.data.error);
    // Always return the same output to the calling function
    // result.result = response.data.result.interpreted;
    // log.debug('enp.result.result', result.result);
    // result.result.input = `${text}`;
  }
  // result = response.data.result;
  // } catch (error) {
  //   console.error('enp.error', error);
  //   result.result = 'Error';
  //   result.error = error;
  // }
  // log.debug('enp.result', result);
  return response.data.result;
};
