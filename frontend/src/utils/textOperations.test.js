import {
  toEnumFormat,
  toDisplayFormat,
  sanitizedTextComparison,
  cleanLowercaseString,
  getInitials,
  findLongestNumber,
  getBetweenPipes,
  getFramedText,
  getUnframedText,
  isFramed,
  getLettersFromNumber,
  getNumberFromLetters,
  flipAlphaNumeric,
  camelize,
} from './textOperations';

// TODO: Merge these operations with frontend/src/styleguide/Modals/SpecialCharactersModal/symbolsUtil.test.js

describe('Text Operations can be Used To', () => {
  describe('#toEnumFormat', () => {
    it('Convert to Enum Format', () => {
      expect(toEnumFormat('turn into enum')).toEqual('Turn_Into_Enum');
      expect(toEnumFormat('word')).toEqual('Word');
    });
  });

  describe('#toDisplayFormat', () => {
    it('Convert to Display Format', () => {
      expect(toDisplayFormat('geometric_tolerance')).toEqual('Geometric Tolerance');
      expect(toDisplayFormat('profile of a surface')).toEqual('Profile of a Surface');
    });
  });

  describe('#cleanLowercaseString', () => {
    it('Convert to Comparable Sanitized String', () => {
      expect(cleanLowercaseString('He  ll o W oRl d')).toBe('helloworld');
      expect(cleanLowercaseString('Ge oMetr ic_to lEranc e')).toBe('geometrictolerance');
    });
    it('returns passed value if invalid', () => {
      expect(cleanLowercaseString(1234)).toStrictEqual(1234);
      expect(cleanLowercaseString(['1234'])).toStrictEqual(['1234']);
      expect(cleanLowercaseString({ 12: 34 })).toStrictEqual({ 12: 34 });
    });
  });

  describe('#sanitizedTextComparison', () => {
    it('Compares Strings in Different Formats', () => {
      expect(sanitizedTextComparison('geometric_tolerance', 'Geometric Tolerance')).toEqual(true);
      expect(sanitizedTextComparison('angle', 'angles')).toEqual(false);
      expect(sanitizedTextComparison('profile of a surface', 'Profile_ofA Surface')).toEqual(true);
    });
  });

  describe('#getInitials', () => {
    it('Convert Name to Initials', () => {
      expect(getInitials('Geometric Tolerance')).toEqual('GT');
      expect(getInitials('Lisi Medical')).toEqual('LM');
      expect(getInitials('Boeing')).toEqual('B');
    });
  });

  describe('#findLongestNumber', () => {
    it('Can find the longest number in a string', () => {
      expect(findLongestNumber('test 1.5')).toEqual('1.5');
      expect(findLongestNumber('')).toEqual('');
      expect(findLongestNumber('5')).toEqual('5');
      expect(findLongestNumber('A5')).toEqual('5');
      expect(findLongestNumber('5 test 1.5')).toEqual('1.5');
      expect(findLongestNumber('test .5')).toEqual('.5');
      expect(findLongestNumber('5.1 test 1.55')).toEqual('1.55');
      expect(findLongestNumber('test')).toEqual('');
      expect(findLongestNumber('4X 3.14 THRU')).toEqual('3.14');
      expect(findLongestNumber('15')).toEqual('15');
      expect(findLongestNumber('100.150')).toEqual('100.150');
    });
  });

  describe('#getBetweenPipes', () => {
    it('Returns a substring from the first to last pipe symbol', () => {
      expect(getBetweenPipes('1.34')).toEqual(null);
      expect(getBetweenPipes('|1.34')).toEqual(null);
      expect(getBetweenPipes('|1.34|')[0]).toEqual('|1.34|');
      expect(getBetweenPipes('2X |1.23|24| THRU')[0]).toEqual('|1.23|24|');
    });
  });

  describe('#getFramedText', () => {
    it('Converts to framed text between the first and last pipes', () => {
      expect(getFramedText('1.34')).toEqual('1.34');
      expect(getFramedText('|1.34')).toEqual('|1.34');
      expect(getFramedText('|1.34|')).toEqual('');
      expect(getFramedText('2X |1.34|')).toEqual('2X ');
      expect(getFramedText('2X |1.34|24| THRU')).toEqual('2X  THRU');
    });
  });

  describe('#getUnframedText', () => {
    it('Converts framed text to unframed text', () => {
      expect(getUnframedText('1.34')).toEqual('1.34');
      expect(getUnframedText('|1.34')).toEqual('|1.34');
      expect(getUnframedText('')).toEqual('|1.34|');
      expect(getUnframedText('2X ')).toEqual('2X |1.34|');
      expect(getUnframedText('2X  THRU')).toEqual('2X |1.34|24| THRU');
    });
  });

  describe('#isFramed', () => {
    it('Checks if text is framed', () => {
      expect(isFramed('')).toBeTruthy();
      expect(isFramed('1.45')).toBeFalsy();
      expect(isFramed('|12.4|')).toBeFalsy();
      expect(isFramed('12.4|')).toBeFalsy();
      expect(isFramed('235')).toBeTruthy();
      expect(isFramed('')).toBeTruthy();
    });
  });

  describe('#Flips Letters and Numbers', () => {
    it('Changes a Letter to a Number', () => {
      expect(getNumberFromLetters('A')).toEqual(1);
      expect(getNumberFromLetters('D')).toEqual(4);
      expect(getNumberFromLetters('AC')).toEqual(29);
      expect(getNumberFromLetters('ALP')).toEqual(1004);
    });

    it('Changes a Number to a Letter', () => {
      expect(getLettersFromNumber(1)).toEqual('A');
      expect(getLettersFromNumber(4)).toEqual('D');
      expect(getLettersFromNumber(29)).toEqual('AC');
      expect(getLettersFromNumber(1004)).toEqual('ALP');
    });

    it('Flips numbers and letters', () => {
      expect(flipAlphaNumeric('1')).toEqual('A');
      expect(flipAlphaNumeric('A')).toEqual('1');
      expect(flipAlphaNumeric('0')).toEqual('');
    });
  });
});
