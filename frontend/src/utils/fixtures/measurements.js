export const testSample = {
  id: 'sample-id',
  serial: 'srl1',
  sampleIndex: 1,
  status: 'Unmeasured',
  featureCoverage: null,
  partId: 'part-id',
  measurements: [],
  updatingOperatorId: null,
  createdAt: null,
  updatedAt: null,
  deletedAt: null,
};

export const threeTwoFourPass = {
  value: '3.24',
  gage: 'test',
  machine: '',
  status: 'Pass',
  characteristicId: 'feature-id',
  operatorId: '2',
  sampleId: 'sample-id',
  stationId: '',
  methodId: null,
  operationId: null,
};

export const threeTwoFourFail = {
  value: '3.24',
  gage: 'test',
  machine: '',
  status: 'Fail',
  characteristicId: '1',
  operatorId: '2',
  sampleId: 'sample-id',
  stationId: '',
  methodId: null,
  operationId: null,
};

export const threeTwoSixPass = {
  value: '3.26',
  gage: 'test',
  machine: '',
  status: 'Pass',
  characteristicId: '1',
  operatorId: '2',
  sampleId: 'sample-id',
  stationId: '',
  methodId: null,
  operationId: null,
};

export const twoPass = {
  value: '2',
  gage: 'test',
  machine: '',
  status: 'Pass',
  characteristicId: '1',
  operatorId: '2',
  sampleId: 'sample-id',
  stationId: '',
  methodId: null,
  operationId: null,
};

export const twoFail = {
  value: '2',
  gage: 'test',
  machine: '',
  status: 'Fail',
  characteristicId: '1',
  operatorId: '2',
  sampleId: 'sample-id',
  stationId: '',
  methodId: null,
  operationId: null,
};

export const fourPass = {
  value: '4',
  gage: 'test',
  machine: '',
  status: 'Pass',
  characteristicId: '1',
  operatorId: '2',
  sampleId: 'sample-id',
  stationId: '',
  methodId: null,
  operationId: null,
};

export const fourFail = {
  value: '4',
  gage: 'test',
  machine: '',
  status: 'Fail',
  characteristicId: '1',
  operatorId: '2',
  sampleId: 'sample-id',
  stationId: '',
  methodId: null,
  operationId: null,
};

export const pass = {
  value: 'Pass',
  gage: 'test',
  machine: '',
  status: 'Pass',
  characteristicId: '1',
  operatorId: '2',
  sampleId: 'sample-id',
  stationId: '',
  methodId: null,
  operationId: null,
};

export const fail = {
  value: 'Fail',
  gage: 'test',
  machine: '',
  status: 'Fail',
  characteristicId: '1',
  operatorId: '2',
  sampleId: 'sample-id',
  stationId: '',
  methodId: null,
  operationId: null,
};
