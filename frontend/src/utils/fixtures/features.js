import { defaultTypeIDs, defaultSubtypeIDs, defaultUnitIDs, defaultMethodIDs } from 'view/global/defaults';

export const range = {
  id: 'feature-id-1',
  drawingSheetIndex: 2,
  status: 'Inactive',
  captureMethod: 'Automated',
  notationType: defaultTypeIDs.Dimension, // Dimension
  notationSubtype: defaultSubtypeIDs.Length, // Length
  notationClass: 'Basic',
  fullSpecification: '3.20 / 3.50',
  quantity: 1,
  nominal: '3.20 / 3.50',
  upperSpecLimit: '3.50',
  lowerSpecLimit: '3.20',
  plusTol: '0',
  minusTol: '0',
  unit: defaultUnitIDs.millimeter,
  toleranceSource: 'Document_Defined',
  inspectionMethod: defaultMethodIDs.Caliper, // Caliper
  criticality: undefined,
  notes: '',
  drawingRotation: 0.0,
  drawingScale: 1.0,
  boxLocationY: 120.5,
  boxLocationX: 120.5,
  boxWidth: 250,
  boxHeight: 100,
  boxRotation: 0.0,
  markerGroup: '1',
  markerGroupShared: false,
  markerIndex: 0,
  markerSubIndex: -1,
  markerLabel: '1',
  markerStyle: 'f1386f09-9c03-4c59-ada9-acb6a16bfbab',
  markerLocationX: 100.5,
  markerLocationY: 120.5,
  markerFontSize: 18,
  markerSize: 36,
  gridCoordinates: '',
  balloonGridCoordinates: '',
  connectionPointGridCoordinates: '',
  verified: false,
  part: {
    id: 'd6c5bc49-9fb2-4fbb-8a61-77bc8d28036f', // belongsTo
  },
  drawing: {
    id: 'c200ce70-72b8-44f7-8d3f-4b93f6d1b491', // belongsTo
  },
  drawingSheet: {
    id: 'd6c5bc49-9fb2-4fbb-8a61-77bc8d380352',
  },
  createdAt: Date.now(),
  updatedAt: Date.now(),
  deletedAt: Date.now(),
};

export const max = {
  id: 'feature-id-2',
  drawingSheetIndex: 2,
  status: 'Inactive',
  captureMethod: 'Automated',
  notationType: defaultTypeIDs.Dimension, // Dimension
  notationSubtype: defaultSubtypeIDs.Length, // Length
  notationClass: 'Basic',
  fullSpecification: '3.24 MAX',
  quantity: 1,
  nominal: '3.24',
  upperSpecLimit: '3.24',
  lowerSpecLimit: '',
  plusTol: '0',
  minusTol: '0',
  unit: defaultUnitIDs.millimeter,
  toleranceSource: 'Document_Defined',
  inspectionMethod: defaultMethodIDs.Caliper, // Caliper
  criticality: undefined,
  notes: '',
  drawingRotation: 0.0,
  drawingScale: 1.0,
  boxLocationY: 120.5,
  boxLocationX: 120.5,
  boxWidth: 250,
  boxHeight: 100,
  boxRotation: 0.0,
  markerGroup: '1',
  markerGroupShared: false,
  markerIndex: 0,
  markerSubIndex: -1,
  markerLabel: '1',
  markerStyle: 'f1386f09-9c03-4c59-ada9-acb6a16bfbab',
  markerLocationX: 100.5,
  markerLocationY: 120.5,
  markerFontSize: 18,
  markerSize: 36,
  gridCoordinates: '',
  balloonGridCoordinates: '',
  connectionPointGridCoordinates: '',
  verified: false,
  part: {
    id: 'd6c5bc49-9fb2-4fbb-8a61-77bc8d28036f', // belongsTo
  },
  drawing: {
    id: 'c200ce70-72b8-44f7-8d3f-4b93f6d1b491', // belongsTo
  },
  drawingSheet: {
    id: 'd6c5bc49-9fb2-4fbb-8a61-77bc8d380352',
  },
  createdAt: Date.now(),
  updatedAt: Date.now(),
  deletedAt: Date.now(),
};

export const min = {
  id: 'feature-id-3',
  drawingSheetIndex: 2,
  status: 'Inactive',
  captureMethod: 'Automated',
  notationType: defaultTypeIDs.Dimension, // Dimension
  notationSubtype: defaultSubtypeIDs.Length, // Length
  notationClass: 'Basic',
  fullSpecification: '3.24 MAX',
  quantity: 1,
  nominal: '3.24',
  upperSpecLimit: '',
  lowerSpecLimit: '3.24',
  plusTol: '0',
  minusTol: '0',
  unit: defaultUnitIDs.millimeter,
  toleranceSource: 'Document_Defined',
  inspectionMethod: defaultMethodIDs.Caliper, // Caliper
  criticality: undefined,
  notes: '',
  drawingRotation: 0.0,
  drawingScale: 1.0,
  boxLocationY: 120.5,
  boxLocationX: 120.5,
  boxWidth: 250,
  boxHeight: 100,
  boxRotation: 0.0,
  markerGroup: '1',
  markerGroupShared: false,
  markerIndex: 0,
  markerSubIndex: -1,
  markerLabel: '1',
  markerStyle: 'f1386f09-9c03-4c59-ada9-acb6a16bfbab',
  markerLocationX: 100.5,
  markerLocationY: 120.5,
  markerFontSize: 18,
  markerSize: 36,
  gridCoordinates: '',
  balloonGridCoordinates: '',
  connectionPointGridCoordinates: '',
  verified: false,
  part: {
    id: 'd6c5bc49-9fb2-4fbb-8a61-77bc8d28036f', // belongsTo
  },
  drawing: {
    id: 'c200ce70-72b8-44f7-8d3f-4b93f6d1b491', // belongsTo
  },
  drawingSheet: {
    id: 'd6c5bc49-9fb2-4fbb-8a61-77bc8d380352',
  },
  createdAt: Date.now(),
  updatedAt: Date.now(),
  deletedAt: Date.now(),
};
