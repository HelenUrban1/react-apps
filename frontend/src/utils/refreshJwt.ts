import getStore from 'modules/store';
import axios from 'modules/shared/network/axios';
import Analytics from 'modules/shared/analytics/analytics';
import { refreshEndpoint } from 'config';

/**

 * This function obtains a new user access token from cognito and is only used on requests that are not GraphQL or Axios requests.
 * Both Axios and GraphQL requests have built in interceptors to refresh the users access token.
 * @returns A JWT String once the Promise has been resolved
 */

export async function getRefreshedJwt() {
  const { deviceKey } = getStore().getState().auth;

  try {
    const req = await axios.post(refreshEndpoint, { deviceKey });
    return req.data.authSignIn;
  } catch (err) {
    // Refresh token expired
    getStore().getState().auth.jwt = null;
    Analytics.reset();
    localStorage.setItem('timedOut', 'true');
    window.location.href = '/auth/signin';
  }
}
