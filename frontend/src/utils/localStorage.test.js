import { saveState, loadState } from './localStorage';

const localStorageMock = (function () {
  let store = {};

  return {
    getItem(key) {
      return store[key];
    },

    setItem(key, value) {
      store[key] = value;
    },

    clear() {
      store = {};
    },

    removeItem(key) {
      delete store[key];
    },

    getAll() {
      return store;
    },
  };
})();

Object.defineProperty(global, 'localStorage', { value: localStorageMock });

describe('localStorage serializer', () => {
  beforeEach(() => {
    global.localStorage.clear();
  });

  it('saveState serializes state', () => {
    const state = {
      foo: 'bar',
    };

    saveState(state);

    expect(global.localStorage.getItem('state')).toEqual(JSON.stringify(state));
  });

  it('loadState deserializes state', () => {
    const state = {
      foo: 'bar',
    };

    saveState(state);

    const loaded = loadState();

    expect(loaded).toEqual(state);
  });

  it('loadState returns undefined if state is not set', () => {
    expect(loadState()).toBeUndefined();
  });
});
