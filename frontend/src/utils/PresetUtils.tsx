import React from 'react';
import { LoadedPresets, SettingPresets } from 'domain/setting/settingTypes';
import { Select } from 'antd';
import { i18n } from 'i18n';
import { Organization } from 'graphql/organization';
import { Tolerance } from 'types/tolerances';
import { MarkerStyle } from 'types/marker';

const buildOtherPresets = (otherPresetKeys: string[]) => {
  return (
    <Select.OptGroup key="other-presets" label={i18n('wizard.stylePresets')}>
      {otherPresetKeys.map((key) => (
        <Select.Option key={key} value={key}>
          {key}
        </Select.Option>
      ))}
    </Select.OptGroup>
  );
};

const buildCustomerAndOtherPresets = (customerPresetKeys: string[], otherPresetKeys: string[], customerOnPart: Organization) => {
  return (
    <>
      <Select.OptGroup key="customer-presets" label={i18n('wizard.customerPresets', customerOnPart?.name || i18n('entities.customer.singular'))}>
        {customerPresetKeys.map((key) => (
          <Select.Option key={key} value={key}>
            {key}
          </Select.Option>
        ))}
      </Select.OptGroup>
      <Select.OptGroup key="other-presets" label={i18n('wizard.otherPresets')}>
        {otherPresetKeys.map((key) => (
          <Select.Option key={key} value={key}>
            {key}
          </Select.Option>
        ))}
      </Select.OptGroup>
    </>
  );
};

const sortPresetsIntoCustomerAndOther = (presetsObj: SettingPresets, customerOnPart: Organization | null) => {
  const presetKeys = Object.keys(presetsObj);
  const customerPresetKeys: string[] = [];
  const otherPresetKeys: string[] = ['Default'];
  presetKeys.forEach((key) => {
    if (customerOnPart && presetsObj[key].customers && presetsObj[key].customers.includes(customerOnPart.id)) {
      customerPresetKeys.push(key);
    } else if (key.localeCompare('Default', 'en', { sensitivity: 'base' })) {
      otherPresetKeys.push(key);
    }
  });

  return { customerPresetKeys, otherPresetKeys };
};

export const getPresetSelectOptions = (presets: LoadedPresets | undefined, customerOnPart: Organization | null) => {
  const presetsObj = presets?.metadata || {};
  const { customerPresetKeys, otherPresetKeys } = sortPresetsIntoCustomerAndOther(presetsObj, customerOnPart);
  if (customerPresetKeys.length > 0 && customerOnPart) {
    return buildCustomerAndOtherPresets(customerPresetKeys, otherPresetKeys, customerOnPart);
  }
  return buildOtherPresets(otherPresetKeys);
};

export const currentSettingMatchesPreset = (currentPresetName: string | undefined, availablePresets: LoadedPresets | undefined, currentSetting: { [key: string]: any }) => {
  const currentPresetObject = availablePresets?.metadata[currentPresetName || 'Default'];
  if (currentPresetObject) {
    if (JSON.stringify(currentPresetObject.setting) === JSON.stringify(currentSetting)) return true;
  }
  return false;
};

export const getCustomerSettingPreset = (presets: LoadedPresets, customerId: string) => {
  const matchedPresets: { preset: Tolerance | { [key: string]: MarkerStyle }; presetName: string }[] = [];
  const availablePresets = presets.metadata;
  Object.keys(availablePresets).forEach((key) => {
    if (availablePresets[key].customers && availablePresets[key].customers.includes(customerId)) {
      matchedPresets.push({ preset: availablePresets[key].setting, presetName: key });
    }
  });

  return matchedPresets;
};
