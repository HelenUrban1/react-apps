import { i18n } from 'i18n';
import log from 'modules/shared/logger';
import { frameText, lookup, unFrameText } from './characters';

// TODO: Consolidate frontend/src/utils/characters.js into this

export const cleanLowercaseString = (text: string): string => {
  if (!text || typeof text !== 'string') {
    return text;
  }
  return text.replace(/ /g, '').replace(/_/g, '').toLowerCase();
};

// sanitize OCR text strings
export const sanitizeText = (text: string) => {
  const newText = text.replace(/^\n|\n$/g, '');
  return newText;
};

// remove underscores and spaces and set to lower case
export const sanitizedTextComparison = (textA: string, textB: string): boolean => {
  if (!textA || typeof textA !== 'string' || !textB || typeof textB !== 'string') {
    return false;
  }
  const cleanedA = cleanLowercaseString(textA);
  const cleanedB = cleanLowercaseString(textB);
  if (cleanedA === cleanedB) {
    return true;
  }
  return false;
};

const keepLowerCase = ['a', 'the', 'is', 'of', 'an'];

// fix capitalization to standard format
export const toDisplayFormat = (text: undefined | string) => {
  if (!text) return '';

  const words = text.split(/[\s_]+/g);
  for (let i = 0; i < words.length; i++) {
    if (!keepLowerCase.includes(words[i])) {
      words[i] = words[i].charAt(0).toLocaleUpperCase() + words[i].substring(1);
    }
  }

  return words.join(' ');
};

// format text as an Enum
export const toEnumFormat = (text: undefined | string) => {
  if (!text) return '';

  const words = text.split(/[\s_]+/g);
  for (let i = 0; i < words.length; i++) {
    if (!keepLowerCase.includes(words[i])) {
      words[i] = words[i].charAt(0).toLocaleUpperCase() + words[i].substring(1);
    }
  }

  return words.join('_');
};

export const getOptionsFromStrings = (category: string, enumerator: string, strings: string[]) => {
  const options: { name: string; value: string; label: string }[] = [];
  if (enumerator === 'operations' || enumerator === 'criticality' || enumerator === 'inspectionMethods') {
    options.push({
      name: `${enumerator}_none`,
      value: 'none',
      label: 'None',
    });
  }
  strings.forEach((option) => {
    options.push({
      name: option,
      value: option,
      label: i18n(`${category}[${enumerator}][${option}]`),
    });
  });
  return options;
};

// Get initials from a name
export const getInitials = function (text: string) {
  if (!text) {
    log.warn('No text passed to getInitials function');
    return text;
  }

  const initials = text.replace(/[^a-zA-Z- ]/g, '').match(/\b\w/g);
  if (initials) {
    return initials.join('');
  }

  log.warn('No initials found');
  return text;
};

// Validate and return JSON
export const parseValidJSONString = (str: string) => {
  let value;
  try {
    value = JSON.parse(str);
    if (typeof value === 'string') {
      value = JSON.parse(value);
    }
  } catch (e) {
    log.error('TextOperations.parseValidJSONString error', e);
  }
  return value;
};

// gets the longest number from a string
export const findLongestNumber = (str: string) => {
  const matched = str.match(/(\d+)\.(\d+)|\.(\d+)|(\d+)/g);
  let longest = '';
  matched?.forEach((match) => {
    if (match.length > longest.length) longest = match;
  });

  return longest;
};

export const isFramed = (str: string) => {
  for (let i = 0; i < str.length; i++) {
    const c = lookup(str.charAt(i));
    if (c && str.charAt(i) === c.framed_char) return true;
  }
  return false;
};

// gets the substring between the first and last pipe characters
export const getBetweenPipes = (str: string) => {
  return str.match(/\|(.*)\|/);
};

export const getFramedText = (str: string) => {
  const text = unFrameText(str);
  const frameSubstring = getBetweenPipes(text);
  if (frameSubstring) {
    const inFrames = frameText(frameSubstring[0]);
    return text.replace(frameSubstring[0], inFrames);
  }
  return text;
};

export const getUnframedText = (str: string) => {
  const text = unFrameText(str);

  return text;
};

const alphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';

// Converts an Integer a Letter
export const getLettersFromNumber = (num: number) => {
  if (num === 0) return '';
  let res = '';
  let charIndex = num % alphabet.length; // 4
  let quotient = (num + 1) / alphabet.length; // 1
  if (charIndex - 1 === -1) {
    charIndex = alphabet.length;
    quotient -= 1;
  }
  res += alphabet.charAt(charIndex - 1) + res;
  if (quotient >= 1) {
    res = getLettersFromNumber(parseInt(quotient.toString(), 10)) + res;
  }
  return res;
};

// Converts excel address format strings (e.g. AZ5) to column numbers, 1 indexed
export const getNumberFromLetters = (label: string) => {
  const letters = label.replace(/[0-9]/g, '');
  let number = 0;
  for (let i = 0, j = letters.length - 1; i < letters.length; i += 1, j -= 1) {
    number += alphabet.length ** j * (alphabet.indexOf(letters[i]) + 1);
  }
  return number;
};

export const flipAlphaNumeric = (val: string) => {
  const letterSequence = new RegExp(/[A-Z]+$/g);
  const numberSequence = new RegExp(/[0-9]+$/g);
  if (letterSequence.test(val)) {
    return getNumberFromLetters(val).toString();
  }
  if (numberSequence.test(val)) {
    return getLettersFromNumber(parseInt(val, 10)).toString();
  }

  return '';
};
