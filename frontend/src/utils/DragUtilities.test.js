import { getDraggingItemHeight, moveDragToNewIndex, getStyle, isSelected, getMarker, getSizeClass, ItemListPropCompare, dragMultipleToNewIndex, listItemClass } from './DragUtilities';

const previousProps = {
  itemList: [
    //
    { id: 1, markerGroup: '1', markerLabel: '1', markerIndex: 0, markerSubIndex: -1 },
    { id: 2, markerGroup: '2', markerLabel: '2', markerIndex: 1, markerSubIndex: -1 },
    { id: 3, markerGroup: '3', markerLabel: '3.1', markerIndex: 2, markerSubIndex: 0 },
    { id: 4, markerGroup: '3', markerLabel: '3.2', markerIndex: 3, markerSubIndex: 1 },
    {},
  ],
  markers: [
    { id: 'default', svg: 'defaultMarker' },
    { id: '123', svg: '123Marker' },
  ],
  collapsed: false,
  listRef: null,
  review: true,
  defaultMarker: 'default',
  selected: [
    { id: 3, markerGroup: '3', markerLabel: '3.1', markerIndex: 2, markerSubIndex: 0 },
    { id: 4, markerGroup: '3', markerLabel: '3.2', markerIndex: 3, markerSubIndex: 1 },
  ],
  itemWidth: 80,
  expand: () => {},
  expandGroup: () => {},
  editItem: () => {},
  onClick: () => {},
  onDropdownVisibleChange: () => {},
  onTextBlur: () => {},
  onDropChange: () => {},
  onDragFeature: () => {},
};

const dragGroup = {
  source: {
    index: 2,
  },
  destination: {
    index: 1,
  },
  draggableId: 3,
};
const groupMoveList = [
  //
  { id: 1, markerGroup: '1', markerLabel: '1', markerIndex: 0, markerSubIndex: -1 },
  { id: 3, markerGroup: '3', markerLabel: '3.1', markerIndex: 2, markerSubIndex: 0 },
  { id: 4, markerGroup: '3', markerLabel: '3.2', markerIndex: 3, markerSubIndex: 1 },
  { id: 2, markerGroup: '2', markerLabel: '2', markerIndex: 1, markerSubIndex: -1 },
  {},
];

const dragSingle = {
  source: {
    index: 0,
  },
  destination: {
    index: 1,
  },
  draggableId: 1,
};
const singleMoveList = [
  //
  { id: 2, markerGroup: '2', markerLabel: '2', markerIndex: 1, markerSubIndex: -1 },
  { id: 1, markerGroup: '1', markerLabel: '1', markerIndex: 0, markerSubIndex: -1 },
  { id: 3, markerGroup: '3', markerLabel: '3.1', markerIndex: 2, markerSubIndex: 0 },
  { id: 4, markerGroup: '3', markerLabel: '3.2', markerIndex: 3, markerSubIndex: 1 },
  {},
];

const dragMultipleGroup = {
  source: {
    index: 2,
  },
  destination: {
    index: 0,
  },
  draggableId: 4,
};
const multipleGroupSelected = [
  { id: 2, markerGroup: '2', markerLabel: '2', markerIndex: 1, markerSubIndex: -1 },
  { id: 4, markerGroup: '3', markerLabel: '3.2', markerIndex: 3, markerSubIndex: 1 },
];
const groupMultipleMoveList = [
  //
  { id: 2, markerGroup: '2', markerLabel: '2', markerIndex: 1, markerSubIndex: -1 },
  { id: 3, markerGroup: '3', markerLabel: '3.1', markerIndex: 2, markerSubIndex: 0 },
  { id: 4, markerGroup: '3', markerLabel: '3.2', markerIndex: 3, markerSubIndex: 1 },
  { id: 1, markerGroup: '1', markerLabel: '1', markerIndex: 0, markerSubIndex: -1 },
  {},
];

const dragMultipleSingle = {
  source: {
    index: 0,
  },
  destination: {
    index: 2,
  },
  draggableId: 1,
};
const singleMultipleSelected = [
  { id: 1, markerGroup: '1', markerLabel: '1', markerIndex: 0, markerSubIndex: -1 },
  { id: 2, markerGroup: '2', markerLabel: '2', markerIndex: 1, markerSubIndex: -1 },
];
const singleMultipleMoveList = [
  //
  { id: 3, markerGroup: '3', markerLabel: '3.1', markerIndex: 2, markerSubIndex: 0 },
  { id: 4, markerGroup: '3', markerLabel: '3.2', markerIndex: 3, markerSubIndex: 1 },
  { id: 1, markerGroup: '1', markerLabel: '1', markerIndex: 0, markerSubIndex: -1 },
  { id: 2, markerGroup: '2', markerLabel: '2', markerIndex: 1, markerSubIndex: -1 },
  {},
];

describe('getDraggingItemHeight', () => {
  it('returns null if no feature matches the dragging card', () => {
    const shiftGroup = getDraggingItemHeight({}, previousProps.itemList);
    expect(shiftGroup).toBeNull();
  });
  it('has a top equal to the height of cards above it', () => {
    const shiftGroup = getDraggingItemHeight(dragGroup, previousProps.itemList);
    expect(shiftGroup).not.toBeNull();
    expect(shiftGroup.top).toBe(96);
  });
  it('has a height equal to one card less than the height of the dragged group/card', () => {
    const shiftGroup = getDraggingItemHeight(dragSingle, previousProps.itemList);
    expect(shiftGroup).not.toBeNull();
    expect(shiftGroup.height).toBe(0);
  });
});

describe('moveDragToNewIndex', () => {
  it('returns null if no feature matches the dragging card', () => {
    const newList = moveDragToNewIndex({}, previousProps.itemList);
    expect(newList).toBeNull();
  });
  it('returns null if start and destination are the same', () => {
    const newList = moveDragToNewIndex({ draggableId: 1, source: { index: 1 }, destination: { index: 1 } }, previousProps.itemList);
    expect(newList).toBeNull();
  });
  it('returns null if no destination exists', () => {
    const newList = moveDragToNewIndex({ draggableId: 1, source: { index: 1 }, destination: { index: 9 } }, previousProps.itemList);
    expect(newList).toBeNull();
  });
  it('moves dragged group into new location and returns an array of features in the right order', () => {
    const newList = moveDragToNewIndex(dragGroup, previousProps.itemList);
    expect(newList).not.toBeNull();
    expect(newList).toEqual(groupMoveList);
  });
  it('moves dragged feature into new location and returns an array of features in the right order', () => {
    const newList = moveDragToNewIndex(dragSingle, previousProps.itemList);
    expect(newList).not.toBeNull();
    expect(newList).toEqual(singleMoveList);
  });
});

describe('dragMultipleToNewIndex', () => {
  it('returns null if no feature matches the dragging card', () => {
    const newList = dragMultipleToNewIndex({}, previousProps.itemList, multipleGroupSelected);
    expect(newList).toBeNull();
  });
  it('returns null if start and destination are the same', () => {
    const newList = dragMultipleToNewIndex({ draggableId: 1, source: { index: 1 }, destination: { index: 1 } }, previousProps.itemList, multipleGroupSelected);
    expect(newList).toBeNull();
  });
  it('returns null if no destination exists', () => {
    const newList = dragMultipleToNewIndex({ draggableId: 1, source: { index: 1 }, destination: { index: 9 } }, previousProps.itemList, multipleGroupSelected);
    expect(newList).toBeNull();
  });
  it('moves dragged group into new location and returns an array of features in the right order', () => {
    const newList = dragMultipleToNewIndex(dragMultipleGroup, previousProps.itemList, multipleGroupSelected);
    expect(newList).not.toBeNull();
    expect(newList).toEqual(groupMultipleMoveList);
  });
  it('moves dragged feature into new location and returns an array of features in the right order', () => {
    const newList = dragMultipleToNewIndex(dragMultipleSingle, previousProps.itemList, singleMultipleSelected);
    expect(newList).not.toBeNull();
    expect(newList).toEqual(singleMultipleMoveList);
  });
});

describe('getStyle', () => {
  const shiftForGroup = { top: 0, height: 48 };
  const provided = { draggableProps: { style: { width: 'provided' } } };
  const style = { top: 48 };
  it('returns combined prop styles and dragging styles', () => {
    const adjusted = getStyle({ provided, style, shiftForGroup: { top: null, height: null } });
    expect(adjusted).not.toBeNull();
    expect(adjusted).toEqual({ width: 'provided', top: 48 });
  });
  it('adjusts the height for cards when another card is dragging', () => {
    const adjusted = getStyle({ provided, style, shiftForGroup });
    expect(adjusted).not.toBeNull();
    expect(adjusted).toEqual({ width: 'provided', top: 0 });
  });
});

describe('isSelected', () => {
  const one = { id: 1, markerGroup: '1', markerLabel: '1', markerIndex: 0, markerSubIndex: -1 };
  const three = { id: 3, markerGroup: '3', markerLabel: '3.1', markerIndex: 2, markerSubIndex: 0 };
  it('returns null if there are no features selected', () => {
    const featureSelected = isSelected(one, []);
    expect(featureSelected).toBeNull();
  });
  it('returns true if the passed feature is in the list of selected features', () => {
    const featureSelected = isSelected(three, previousProps.selected);
    expect(featureSelected).toBeTruthy();
  });
  it('returns false if the passed feature is not in the list of selected features', () => {
    const featureSelected = isSelected(one, previousProps.selected);
    expect(featureSelected).toBeFalsy();
  });
});

describe('getMarker', () => {
  it('returns null if not passed a marker list', () => {
    const marker = getMarker({ markerStyle: '123' }, [], 'default');
    expect(marker).toBeNull();
  });
  it('returns the default if the item marker is not in the marker list', () => {
    const marker = getMarker({ markerStyle: '432' }, previousProps.markers, 'default');
    expect(marker).toBe('defaultMarker');
  });
  it('returns the svg for the item marker from the list of markers', () => {
    const marker = getMarker({ markerStyle: '123' }, previousProps.markers, 'default');
    expect(marker).toBe('123Marker');
  });
});

describe('getSizeClass', () => {
  it('returns small if the length of the label is 3 characters or less', () => {
    const size = getSizeClass({ markerLabel: '123' });
    expect(size).toBe('number-small');
  });
  it('returns medium if the length of the label is between 3 and 5 characters', () => {
    const size = getSizeClass({ markerLabel: '12345' });
    expect(size).toBe('number-med');
  });
  it('returns large if the length of the label is larger than 5 characters', () => {
    const size = getSizeClass({ markerLabel: '12345678' });
    expect(size).toBe('number-large');
  });
});

describe('listItemClass', () => {
  const one = { id: 12345, markerGroup: '12345', markerLabel: '12345', markerIndex: 12344, markerSubIndex: -1, verified: false, captureError: '{"message":"some error","description":"some error"}' };
  const three = { id: 3, markerGroup: '3', markerLabel: '3.1', markerIndex: 2, markerSubIndex: 0, verified: true };
  it('returns any passed classes', () => {
    const itemClass = listItemClass(three, previousProps.selected, 'class');
    expect(itemClass).toContain('class');
  });
  it('returns active or inactive class', () => {
    let itemClass = listItemClass(three, previousProps.selected, 'class');
    expect(itemClass).toContain('active');
    itemClass = listItemClass(one, previousProps.selected, 'class');
    expect(itemClass).toContain('inactive');
  });
  it('returns verified or unverified class', () => {
    let itemClass = listItemClass(three, previousProps.selected, 'class');
    expect(itemClass).toContain('verified');
    itemClass = listItemClass(one, previousProps.selected, 'class');
    expect(itemClass).toContain('unverified');
  });
  it('returns possible error class', () => {
    let itemClass = listItemClass(three, previousProps.selected, 'class');
    expect(itemClass).not.toContain('error');
    itemClass = listItemClass(one, previousProps.selected, 'class');
    expect(itemClass).toContain('error');
  });
  it('returns size class', () => {
    let itemClass = listItemClass(three, previousProps.selected, 'class');
    expect(itemClass).toContain('number-small');
    itemClass = listItemClass(one, previousProps.selected, 'class');
    expect(itemClass).toContain('number-med');
  });
});

describe('ItemListPropCompare', () => {
  const newFunction = () => 'NEW';
  const newItemList = [
    //
    { id: 1, markerGroup: '1', markerLabel: '1', markerIndex: 0, markerSubIndex: -1 },
    { id: 2, markerGroup: '2', markerLabel: '2', markerIndex: 1, markerSubIndex: -1 },
    { id: 3, markerGroup: '3', markerLabel: '3.1', markerIndex: 2, markerSubIndex: 0 },
    { id: 4, markerGroup: '3', markerLabel: '3.2', markerIndex: 3, markerSubIndex: 1 },
    {},
  ];
  const newMarkers = [
    { id: 'default', svg: 'defaultMarker' },
    { id: '123', svg: '123Marker' },
  ];
  const newSelected = [
    { id: 3, markerGroup: '3', markerLabel: '3.1', markerIndex: 2, markerSubIndex: 0 },
    { id: 4, markerGroup: '3', markerLabel: '3.2', markerIndex: 3, markerSubIndex: 1 },
  ];

  it('returns true if all props are the same', () => {
    const same = ItemListPropCompare(previousProps, { ...previousProps });
    expect(same).toBeTruthy();
  });
  it('returns true if only the functions are different', () => {
    const same = ItemListPropCompare(previousProps, { ...previousProps, editItem: newFunction });
    expect(same).toBeTruthy();
  });
  it('returns true if the item list is a different object with the same data', () => {
    const same = ItemListPropCompare(previousProps, { ...previousProps, itemList: newItemList });
    expect(same).toBeTruthy();
  });
  it('returns false if the item list is different', () => {
    const same = ItemListPropCompare(previousProps, { ...previousProps, itemList: ['NEW'] });
    expect(same).toBeFalsy();
  });
  it('returns true if the list of selected features is a different object with the same data', () => {
    const same = ItemListPropCompare(previousProps, { ...previousProps, selected: newSelected });
    expect(same).toBeTruthy();
  });
  it('returns false if the list of selected features is different', () => {
    const same = ItemListPropCompare(previousProps, { ...previousProps, selected: ['NEW'] });
    expect(same).toBeFalsy();
  });
  it('returns true if the list of markers is a different object with the same data', () => {
    const same = ItemListPropCompare(previousProps, { ...previousProps, markers: newMarkers });
    expect(same).toBeTruthy();
  });
  it('returns false if the list of markers is different', () => {
    const same = ItemListPropCompare(previousProps, { ...previousProps, markers: ['NEW'] });
    expect(same).toBeFalsy();
  });
  it('returns false if the collapsed state is different', () => {
    const same = ItemListPropCompare(previousProps, { ...previousProps, collapsed: !previousProps.collapsed });
    expect(same).toBeFalsy();
  });
  it('returns false if the review state is different', () => {
    const same = ItemListPropCompare(previousProps, { ...previousProps, review: !previousProps.review });
    expect(same).toBeFalsy();
  });
  it('returns false if the item width is different', () => {
    const same = ItemListPropCompare(previousProps, { ...previousProps, itemWidth: 0 });
    expect(same).toBeFalsy();
  });
});
