// TODO: where did the test file for this go?

import log from 'modules/shared/logger';
import axios from 'modules/shared/network/axios';
import config from 'config';
import { saveAs } from 'file-saver';
import { i18n } from 'i18n';
import getStore from '../modules/store';

export const getFileExtension = (file: string) => {
  if (!file) {
    log.warn('No file provided', file);
    return false;
  }
  const regexMatch = file.match(/\.[0-9a-z]+$/i);
  const extension = regexMatch ? regexMatch[0] : null;
  if (!extension) {
    log.warn('No file extension found');
  }

  return extension;
};

export const isAttachment = (file: string) => {
  const extension = getFileExtension(file);
  const attachmentFileTypes = ['.xlsx', '.pdf', '.doc', '.docx', '.txt', '.csv'];
  if (extension && attachmentFileTypes.includes(extension.toLowerCase())) {
    return true;
  }
  return false;
};

export const isPDF = (file: string) => {
  const extension = getFileExtension(file);
  if (extension && extension.toLowerCase() === '.pdf') {
    return true;
  }
  return false;
};

export const isExcel = (file: string) => {
  const extension = getFileExtension(file);
  if (extension && extension.toLowerCase() === '.xlsx') {
    return true;
  }
  return false;
};

export const isImage = (file: string) => {
  const extension = getFileExtension(file);
  if (extension && ['.png', '.jpg', '.jpeg'].includes(extension.toLowerCase())) {
    return true;
  }
  return false;
};

export const downloadFile = async (publicUrl: string) => {
  const { jwt } = getStore().getState().auth;
  return axios({
    method: 'get',
    url: `${publicUrl}`,
    responseType: 'blob',
    headers: {
      authorization: jwt ? `Bearer ${jwt}` : '',
    },
  })
    .then((response) => {
      return response.data;
    })
    .catch((error) => log.error('files.downloadFile error', error));
};

export const convertFile = async (privateUrl: string, type: string, font?: boolean, unframed?: boolean, filename?: string) => {
  log.debug('Downloading as Type', type, ' - font:', font, ', convert:', unframed);
  const { jwt } = getStore().getState().auth;
  return axios({
    method: 'post',
    url: `${config.backendUrl}/excel`,
    data: { privateUrl, type, font, unframed, filename },
    responseType: 'blob',
    headers: {
      authorization: jwt ? `Bearer ${jwt}` : '',
    },
  })
    .then((response) => {
      return response.data;
    })
    .catch((error) => log.error('files.convertFile error', error));
};

export const downloadBalloonedFile = async (privateUrl: string, partId: string, drawingId: string) => {
  try {
    const { jwt } = getStore().getState().auth;
    const response = await axios({
      method: 'get',
      url: `${config.backendUrl}/download/ballooned?privateUrl=${privateUrl}&partId=${partId}&drawingId=${drawingId}`,
      responseType: 'blob',
      headers: {
        authorization: jwt ? `Bearer ${jwt}` : '',
      },
    });
    return response.data;
  } catch (error) {
    log.error('files.downloadBalloonedFile error', error);
    return null;
  }
};

// Download a file
export const handleFileDownload = async ({ publicUrl, fileName, type, partId, drawingId, font, unframed }: { publicUrl: string; fileName: string; type?: string; partId?: string; drawingId?: string; font?: boolean; unframed?: boolean }) => {
  let file;
  let name = fileName;
  let extension = getFileExtension(fileName);
  // supported types
  if (extension === '.pdf' || extension === '.xlsx' || extension === '.csv' || extension === '.zip') {
    name = fileName.replace(extension, '');
  } else {
    extension = '.xlsx';
  }
  const cypressTest = window.top as any;
  if (name.length === 0) name = i18n('common.untitled');
  if (type) {
    file = await convertFile(publicUrl, type, font, unframed, name);
    if (type === 'xlsx' && !unframed && !font) {
      extension = '.xlsx';
    } else {
      extension = '.zip';
    }
  } else if (partId && drawingId) {
    file = await downloadBalloonedFile(publicUrl, partId, drawingId);
    extension = '.pdf';
  } else {
    file = await downloadFile(publicUrl);
  }
  if (file && cypressTest.Cypress) {
    // don't open window dialogues when cypress testing since cypress can't see them
    console.debug('Cypress');
    return;
  }
  if (file) {
    saveAs(file, name + extension);
  }
};
