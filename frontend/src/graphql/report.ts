import gql from 'graphql-tag';
import { ReportInput, Report, ReportFilterInput, ReportOrderByEnum } from 'types/reportTemplates';

export interface ReportUpdateVars {
  id: string;
  data: ReportInput;
}
export interface ReportUpdateData {
  reportUpdate: Report;
}

export interface ReportDestroyVars {
  id: string[];
}
export interface ReportDestroyData {
  reportDestroy: null;
}

export interface ReportCreateVars {
  data: ReportInput;
}
export interface ReportCreateData {
  reportCreate: Report;
}

export interface ReportFindVars {
  id: string;
}
export interface ReportFindData {
  reportFind: Report;
}

export interface ReportListVars {
  filter: ReportFilterInput;
  orderBy: ReportOrderByEnum;
  limit: number;
  offset: number;
}
export interface ReportListData {
  reportFind: Report;
}

export const ReportAPI = {
  mutate: {
    edit: gql`
      mutation REPORT_UPDATE($id: String!, $data: ReportInput!) {
        reportUpdate(id: $id, data: $data) {
          id
          customer {
            id
            name
          }
          part {
            id
            name
          }
          partVersion
          template {
            id
            direction
            settings
            provider {
              id
              name
            }
            file {
              id
              name
              sizeInBytes
              publicUrl
              privateUrl
            }
          }
          data
          title
          templateUrl
          filters
          file {
            id
            name
            sizeInBytes
            publicUrl
            privateUrl
          }
          status
          createdAt
          updatedAt
        }
      }
    `,
    delete: gql`
      mutation REPORT_DESTROY($ids: [String!]!) {
        reportDestroy(ids: $ids)
      }
    `,
    create: gql`
      mutation REPORT_CREATE($data: ReportInput!) {
        reportCreate(data: $data) {
          id
          customer {
            id
            name
          }
          part {
            id
            name
          }
          partVersion
          template {
            id
            direction
            settings
            provider {
              id
              name
            }
            file {
              id
              name
              sizeInBytes
              publicUrl
              privateUrl
            }
          }
          data
          title
          templateUrl
          filters
          file {
            id
            name
            sizeInBytes
            publicUrl
            privateUrl
          }
          status
          createdAt
          updatedAt
        }
      }
    `,
  },
  query: {
    find: gql`
      query REPORT_FIND($id: String!) {
        reportFind(id: $id) {
          id
          customer {
            id
            name
          }
          part {
            id
            name
            reports {
              id
            }
          }
          partVersion
          template {
            id
            direction
            settings
            provider {
              id
              name
            }
            file {
              id
              name
              sizeInBytes
              publicUrl
              privateUrl
            }
          }
          data
          title
          templateUrl
          filters
          file {
            id
            name
            sizeInBytes
            publicUrl
            privateUrl
          }
          status
          createdAt
          updatedAt
        }
      }
    `,
    list: gql`
      query REPORT_LIST($filter: ReportFilterInput, $orderBy: ReportOrderByEnum, $limit: Int, $offset: Int) {
        reportList(filter: $filter, orderBy: $orderBy, limit: $limit, offset: $offset) {
          count
          rows {
            id
            customer {
              id
              name
            }
            part {
              id
              name
            }
            partVersion
            template {
              id
              direction
              settings
              provider {
                id
                name
              }
              file {
                id
                name
                sizeInBytes
                publicUrl
                privateUrl
              }
            }
            data
            title
            templateUrl
            file {
              id
              name
              sizeInBytes
              publicUrl
              privateUrl
            }
            status
            createdAt
            updatedAt
          }
        }
      }
    `,
  },
  createGraphqlInput: (report: Report) => {
    const input: ReportInput = {
      customer: report.customer ? report.customer.id : report.id,
      file: report.file ? report.file : [],
      part: report.part ? report.part.id : report.id,
      partVersion: report.partVersion ? report.partVersion : '',
      template: report.template ? report.template.id : report.id,
      title: report.title ? report.title.trim() : '',
      templateUrl: report.templateUrl,
      filters: report.filters ? report.filters : '{}',
      partUpdatedAt: report.partUpdatedAt ? report.partUpdatedAt : null,
      status: report.status ? report.status : 'Active',
    };
    return input;
  },
};
