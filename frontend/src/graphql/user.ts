import gql from 'graphql-tag';
import { User, UserInput } from 'types/user';

export interface UserEdit {
  userUpdate: UserInput;
}

export interface UserCreate {
  userCreate: User;
}

export interface UserFind {
  userFind: User;
}

export interface UserList {
  userList: User[];
}

const UserAPI = {
  mutate: {
    edit: gql`
      mutation USER_UPDATE($id: String!, $data: UserInput!) {
        userUpdate(id: $id, data: $data) {
          id
        }
      }
    `,
    delete: gql`
      mutation USER_DESTROY($id: String!, $data: UserInput!) {
        userDestroy(id: $id, data: $data) {
          id
        }
      }
    `,
    create: gql`
      mutation USER_CREATE($id: String!, $data: UserInput!) {
        userCreate(id: $id, data: $data) {
          id
        }
      }
    `,
  },
  query: {
    find: gql`
      query USER_FIND($id: String!) {
        userFind(id: $id) {
          id
        }
      }
    `,
    list: gql`
      query USER_LIST($filter: UserFilterInput, $orderBy: UserOrderByEnum, $limit: Int, $offset: Int) {
        userList(filter: $filter, orderBy: $orderBy, limit: $limit, offset: $offset) {
          id
          fullName
          firstName
          lastName
          email
          accountMemberships {
            accountId
            roles
            status
          }
        }
      }
    `,
  },
};

export default UserAPI;
