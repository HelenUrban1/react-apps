import { Part } from 'domain/part/partTypes';
import gql from 'graphql-tag';
import { Characteristic } from 'types/characteristics';
import { ParsedCharacteristic } from 'utils/ParseTextController/types';
import { defaultFeature } from 'view/global/defaults';
import { Drawing } from './drawing';
import { DrawingSheet } from './drawing_sheet';

export type CharacteristicNotationTypeEnum = 'Dimension' | 'Geometric_Tolerance' | 'Surface_Finish' | 'Weld' | 'Note' | 'Other';

export type CharacteristicCriticalityEnum = '' | 'Incidental' | 'Minor' | 'Major' | 'Key' | 'Critical';
export type CharacteristicToleranceEnum = 'Default_Tolerance' | 'Document_Defined' | 'Manually_Set' | 'N_A';
export type CharacteristicStatusEnum = 'Active' | 'Inactive' | 'Rejected';
export type CharacteristicCaptureEnum = 'Automated' | 'Manual' | 'Custom' | 'Annotation';
export type CharacteristicNotationSubtypeEnum =
  | 'Length'
  | 'Angle'
  | 'Radius'
  | 'Diameter'
  | 'Curvilinear'
  | 'Chamfer_Size'
  | 'Chamfer_Angle'
  | 'Bend_Radius'
  | 'Counterbore_Depth'
  | 'Counterbore_Diameter'
  | 'Counterbore_Angle'
  | 'Countersink_Diameter'
  | 'Countersink_Angle'
  | 'Depth'
  | 'Edge_Radius'
  | 'Thickness'
  | 'Thread'
  | 'Edge_Burr_Size'
  | 'Edge_Undercut_Size'
  | 'Edge_Passing_Size'
  | 'Profile_of_a_Surface'
  | 'Position'
  | 'Flatness'
  | 'Perpendicularity'
  | 'Angularity'
  | 'Circularity'
  | 'Runout'
  | 'Total_Runout'
  | 'Profile_of_a_Line'
  | 'Cylindricity'
  | 'Concentricity'
  | 'Symmetry'
  | 'Parallelism'
  | 'Straightness'
  | 'Roughness'
  | 'Lay'
  | 'Waviness'
  | 'Structure'
  | 'Material_Removal_Allowance'
  | 'Size'
  | 'Convexity'
  | 'Reinforcement'
  | 'Additional_Characteristic'
  | 'Bead_Contour'
  | 'Depth_of_Bevel'
  | 'Penetration'
  | 'Location'
  | 'Pitch'
  | 'Root_Opening'
  | 'Note'
  | 'Flag_Note'
  | 'Temperature'
  | 'Time'
  | 'Torque'
  | 'Voltage'
  | 'Temper_Conductivity'
  | 'Temper_Verification'
  | 'Sound'
  | 'Frequency'
  | 'Capacitance'
  | 'Electric_Current'
  | 'Resistance'
  | 'Electric_Charge'
  | 'Inductance'
  | 'Apparent_Power'
  | 'Reactive_Power'
  | 'Electric_Power'
  | 'Conductance'
  | 'Admittance'
  | 'Electric_Flux'
  | 'Magnetic_Field'
  | 'Magnetic_Flux'
  | 'Area'
  | 'Resistivity'
  | 'Conductivity'
  | 'Electric_Field'
  | 'Datum_Target'
  | 'Production_Method'
  | 'Annotation_Table'
  | 'Table'
  | 'Cell'
  | 'Callout'
  | 'Datum'
  | 'Balloon';
export type CharacteristicNotationClassEnum = 'Tolerance' | 'Basic' | 'Reference' | 'N_A';

export interface CharacteristicInput {
  previousRevision?: string | null;
  nextRevision?: string | null;
  originalCharacteristic?: string | null;
  drawingSheetIndex: number;
  status: CharacteristicStatusEnum;
  captureMethod: CharacteristicCaptureEnum;
  captureError: string;
  notationType: string;
  notationSubtype: string;
  notationClass: CharacteristicNotationClassEnum;
  fullSpecification: string;
  quantity: number;
  nominal: string;
  upperSpecLimit: string;
  lowerSpecLimit: string;
  plusTol: string;
  minusTol: string;
  unit: string;
  gdtSymbol?: string;
  gdtPrimaryToleranceZone?: string;
  gdtSecondaryToleranceZone?: string;
  gdtPrimaryDatum?: string;
  gdtSecondaryDatum?: string;
  gdtTertiaryDatum?: string;
  criticality?: string | null;
  inspectionMethod?: string | null;
  operation?: string | null;
  notes: string;
  drawingRotation: number;
  drawingScale: number;
  boxLocationY: number;
  boxLocationX: number;
  boxWidth: number;
  boxHeight: number;
  boxRotation: number;
  markerGroup: string;
  markerGroupShared: boolean;
  markerIndex: number;
  markerSubIndex: number;
  markerLabel: string;
  markerStyle: string;
  markerLocationX: number;
  markerLocationY: number;
  markerFontSize: number;
  markerSize: number;
  markerRotation: number;
  gridCoordinates: string;
  balloonGridCoordinates: string;
  connectionPointGridCoordinates: string;
  part: string;
  drawing: string;
  drawingSheet: string;
  verified: boolean;
  fullSpecificationFromOCR: string;
  confidence: number;
  toleranceSource: CharacteristicToleranceEnum;
  connectionPointLocationX: number;
  connectionPointLocationY: number;
  connectionPointIsFloating: boolean;
  displayLeaderLine: boolean;
  leaderLineDistance: number;
  reviewTaskId?: string;
  [key: string]:
    | string
    | number
    | boolean
    | CharacteristicNotationTypeEnum
    | CharacteristicNotationSubtypeEnum
    | CharacteristicNotationClassEnum
    | CharacteristicCriticalityEnum
    | CharacteristicToleranceEnum
    | CharacteristicStatusEnum
    | CharacteristicCaptureEnum
    | Date
    | Part
    | Drawing
    | DrawingSheet
    | undefined
    | null;
}

// Data for token replacement when creating a report
export interface CharacteristicData {
  label: string;
  location: string;
  zone: string;
  balloon_zone: string;
  connection_point_zone: string;
  page: number;
  type: string;
  subtype: string;
  quantity: number;
  unit: string;
  full_spec: string;
  tol_type: CharacteristicNotationClassEnum;
  nominal: string;
  plus: string;
  minus: string;
  usl: string;
  lsl: string;
  source: CharacteristicToleranceEnum;
  classification: string | null;
  operation: string | null;
  method: string | null;
  comment: string;
  verification: boolean;
  number: number;
  status: CharacteristicStatusEnum;
  capturemethod: CharacteristicCaptureEnum;
  captureError: string;
  gdtsymbol: string;
  gdtprimarytolerancezone: string;
  gdtsecondarytolerancezone: string;
  gdtprimarydatum: string;
  gdtsecondarydatum: string;
  gdttertiarydatum: string;
  markergroup: string;
  markergroupshared: boolean;
  markersubindex: number;
  markerstyle: string;
  markerfontsize: number;
  markerSize: number;
  ocr: string;
  confidence: number;
}
export interface CharacteristicEditVars {
  id: string;
  data: CharacteristicInput;
}

export interface CharacteristicCreateVars {
  data: CharacteristicInput;
}

export interface CharacteristicEdit {
  characteristicUpdate: Characteristic;
}
export interface CharacteristicCreate {
  characteristicCreate: Characteristic;
}

export interface CharacteristicFind {
  characteristicFind: Characteristic;
}

export type CharacteristicOrderByEnum =
  | 'id_ASC'
  | 'id_DESC'
  | 'drawingSheetIndex_ASC'
  | 'drawingSheetIndex_DESC'
  | 'notationType_ASC'
  | 'notationType_DESC'
  | 'notationSubtype_ASC'
  | 'notationSubtype_DESC'
  | 'notationClass_ASC'
  | 'notationClass_DESC'
  | 'fullSpecification_ASC'
  | 'fullSpecification_DESC'
  | 'quantity_ASC'
  | 'quantity_DESC'
  | 'nominal_ASC'
  | 'nominal_DESC'
  | 'upperSpecLimit_ASC'
  | 'upperSpecLimit_DESC'
  | 'lowerSpecLimit_ASC'
  | 'lowerSpecLimit_DESC'
  | 'plusTol_ASC'
  | 'plusTol_DESC'
  | 'minusTol_ASC'
  | 'minusTol_DESC'
  | 'unit_ASC'
  | 'unit_DESC'
  | 'gdtSymbol_ASC'
  | 'gdtSymbol_DESC'
  | 'gdtPrimaryToleranceZone_ASC'
  | 'gdtPrimaryToleranceZone_DESC'
  | 'gdtSecondaryToleranceZone_ASC'
  | 'gdtSecondaryToleranceZone_DESC'
  | 'gdtPrimaryDatum_ASC'
  | 'gdtPrimaryDatum_DESC'
  | 'gdtSecondaryDatum_ASC'
  | 'gdtSecondaryDatum_DESC'
  | 'gdtTertiaryDatum_ASC'
  | 'gdtTertiaryDatum_DESC'
  | 'criticality_ASC'
  | 'criticality_DESC'
  | 'inspectionMethod_ASC'
  | 'inspectionMethod_DESC'
  | 'notes_ASC'
  | 'notes_DESC'
  | 'drawingRotation_ASC'
  | 'drawingRotation_DESC'
  | 'drawingScale_ASC'
  | 'drawingScale_DESC'
  | 'boxLocationY_ASC'
  | 'boxLocationY_DESC'
  | 'boxLocationX_ASC'
  | 'boxLocationX_DESC'
  | 'boxWidth_ASC'
  | 'boxWidth_DESC'
  | 'boxHeight_ASC'
  | 'boxHeight_DESC'
  | 'boxRotation_ASC'
  | 'boxRotation_DESC'
  | 'markerGroup_ASC'
  | 'markerGroup_DESC'
  | 'markerIndex_ASC'
  | 'markerIndex_DESC'
  | 'markerSubIndex_ASC'
  | 'markerSubIndex_DESC'
  | 'markerLabel_ASC'
  | 'markerLabel_DESC'
  | 'markerStyle_ASC'
  | 'markerStyle_DESC'
  | 'markerLocationX_ASC'
  | 'markerLocationX_DESC'
  | 'markerLocationY_ASC'
  | 'markerLocationY_DESC'
  | 'gridCoordinates_ASC'
  | 'gridCoordinates_DESC'
  | 'balloonGridCoordinates_ASC'
  | 'balloonGridCoordinates_DESC'
  | 'connectionPointGridCoordinates_ASC'
  | 'connectionPointGridCoordinates_DESC'
  | 'createdAt_ASC'
  | 'createdAt_DESC'
  | 'deletedAt_ASC'
  | 'deletedAt_DESC';

export interface CharacteristicFilterInput {
  id?: string;
  previousRevision?: string | null;
  nextRevision?: string;
  originalCharacteristic?: string;
  drawingSheetIndexRange?: number[];
  status?: CharacteristicStatusEnum[];
  notationType?: string;
  notationSubtype?: string;
  notationClass?: CharacteristicNotationClassEnum;
  fullSpecification?: string;
  quantityRange?: number[];
  nominal?: string;
  upperSpecLimit?: string;
  lowerSpecLimit?: string;
  plusTol?: string;
  minusTol?: string;
  unit?: string;
  gdtSymbol?: string;
  gdtPrimaryToleranceZone?: string;
  gdtSecondaryToleranceZone?: string;
  gdtPrimaryDatum?: string;
  gdtSecondaryDatum?: string;
  gdtTertiaryDatum?: string;
  inspectionMethod?: string;
  operation?: string;
  notes?: string;
  drawingRotationRange?: number[];
  drawingScaleRange?: number[];
  boxLocationYRange?: number[];
  boxLocationXRange?: number[];
  boxWidthRange?: number[];
  boxHeightRange?: number[];
  boxRotationRange?: number[];
  markerGroup?: string;
  markerGroupShared?: boolean;
  markerIndexRange?: number[];
  markerSubIndexRange?: number[];
  markerLabel?: string;
  markerStyle?: string;
  markerLocationXRange?: number[];
  markerLocationYRange?: number[];
  markerFontSize?: number[];
  markerSize?: number[];
  gridCoordinates?: string;
  balloonGridCoordinates?: string;
  connectionPointGridCoordinates?: string;
  part?: string;
  drawing?: string;
  drawingSheet?: string;
  createdAtRange?: Date[];
  deletedAtRange?: Date[];
}

export interface CharacteristicList {
  characteristicList?: {
    count: number;
    rows: Characteristic[];
  };
}

export interface CharacteristicListVars {
  filter?: CharacteristicFilterInput;
  orderBy?: CharacteristicOrderByEnum;
  limit?: number;
  offset?: number;
}

export const Characteristics = {
  mutate: {
    edit: gql`
      mutation CHARACTERISTIC_UPDATE($id: String!, $data: CharacteristicInput!) {
        characteristicUpdate(id: $id, data: $data) {
          id
          previousRevision
          nextRevision
          originalCharacteristic
          status
          captureMethod
          captureError
          drawingSheetIndex
          notationType
          notationSubtype
          notationClass
          fullSpecification
          quantity
          nominal
          upperSpecLimit
          lowerSpecLimit
          plusTol
          minusTol
          unit
          gdtSymbol
          gdtPrimaryToleranceZone
          gdtSecondaryToleranceZone
          gdtPrimaryDatum
          gdtSecondaryDatum
          gdtTertiaryDatum
          criticality
          inspectionMethod
          notes
          drawingRotation
          drawingScale
          boxLocationY
          boxLocationX
          boxWidth
          boxHeight
          boxRotation
          markerGroup
          markerGroupShared
          markerIndex
          markerSubIndex
          markerLabel
          markerStyle
          markerLocationX
          markerLocationY
          markerFontSize
          markerSize
          markerRotation
          gridCoordinates
          balloonGridCoordinates
          connectionPointGridCoordinates
          operation
          toleranceSource
          connectionPointLocationX
          connectionPointLocationY
          connectionPointIsFloating
          displayLeaderLine
          leaderLineDistance
          part {
            id
            name
          }
          drawing {
            id
            name
          }
          drawingSheet {
            id
            pageIndex
          }
          createdAt
          updatedAt
          deletedAt
          verified
          fullSpecificationFromOCR
          confidence
        }
      }
    `,
    delete: gql`
      mutation CHARACTERISTIC_DESTROY($ids: [String!]!) {
        characteristicDestroy(ids: $ids)
      }
    `,
    create: gql`
      mutation CHARACTERISTIC_CREATE($data: CharacteristicInput!) {
        characteristicCreate(data: $data) {
          id
          previousRevision
          nextRevision
          originalCharacteristic
          status
          captureMethod
          captureError
          drawingSheetIndex
          notationType
          notationSubtype
          notationClass
          fullSpecification
          quantity
          nominal
          upperSpecLimit
          lowerSpecLimit
          plusTol
          minusTol
          unit
          gdtSymbol
          gdtPrimaryToleranceZone
          gdtSecondaryToleranceZone
          gdtPrimaryDatum
          gdtSecondaryDatum
          gdtTertiaryDatum
          criticality
          inspectionMethod
          notes
          drawingRotation
          drawingScale
          boxLocationY
          boxLocationX
          boxWidth
          boxHeight
          boxRotation
          markerGroup
          markerGroupShared
          markerIndex
          markerSubIndex
          markerLabel
          markerStyle
          markerLocationX
          markerLocationY
          markerFontSize
          markerSize
          markerRotation
          gridCoordinates
          balloonGridCoordinates
          connectionPointGridCoordinates
          operation
          toleranceSource
          connectionPointLocationX
          connectionPointLocationY
          connectionPointIsFloating
          displayLeaderLine
          leaderLineDistance
          part {
            id
            name
          }
          drawing {
            id
            name
          }
          drawingSheet {
            id
            pageIndex
          }
          createdAt
          updatedAt
          deletedAt
          verified
          fullSpecificationFromOCR
          confidence
        }
      }
    `,
  },
  query: {
    find: gql`
      query CHARACTERISTIC_FIND($id: String!) {
        characteristicFind(id: $id) {
          id
          previousRevision
          nextRevision
          originalCharacteristic
          status
          captureMethod
          captureError
          drawingSheetIndex
          notationType
          notationSubtype
          notationClass
          fullSpecification
          quantity
          nominal
          upperSpecLimit
          lowerSpecLimit
          plusTol
          minusTol
          unit
          gdtSymbol
          gdtPrimaryToleranceZone
          gdtSecondaryToleranceZone
          gdtPrimaryDatum
          gdtSecondaryDatum
          gdtTertiaryDatum
          criticality
          inspectionMethod
          notes
          drawingRotation
          drawingScale
          boxLocationY
          boxLocationX
          boxWidth
          boxHeight
          boxRotation
          markerGroup
          markerGroupShared
          markerIndex
          markerSubIndex
          markerLabel
          markerStyle
          markerLocationX
          markerLocationY
          markerFontSize
          markerSize
          markerRotation
          gridCoordinates
          balloonGridCoordinates
          connectionPointGridCoordinates
          operation
          toleranceSource
          connectionPointLocationX
          connectionPointLocationY
          connectionPointIsFloating
          displayLeaderLine
          leaderLineDistance
          part {
            id
            name
          }
          drawing {
            id
            name
          }
          drawingSheet {
            id
            pageIndex
          }
          createdAt
          updatedAt
          deletedAt
          verified
          fullSpecificationFromOCR
          confidence
          reviewTaskId
        }
      }
    `,
    list: gql`
      query CHARACTERISTIC_LIST($filter: CharacteristicFilterInput, $orderBy: CharacteristicOrderByEnum, $limit: Int, $offset: Int) {
        characteristicList(filter: $filter, orderBy: $orderBy, limit: $limit, offset: $offset) {
          count
          rows {
            id
            previousRevision
            nextRevision
            originalCharacteristic
            status
            captureMethod
            captureError
            drawingSheetIndex
            notationType
            notationSubtype
            notationClass
            fullSpecification
            quantity
            nominal
            upperSpecLimit
            lowerSpecLimit
            plusTol
            minusTol
            unit
            gdtSymbol
            gdtPrimaryToleranceZone
            gdtSecondaryToleranceZone
            gdtPrimaryDatum
            gdtSecondaryDatum
            gdtTertiaryDatum
            criticality
            inspectionMethod
            notes
            drawingRotation
            drawingScale
            boxLocationY
            boxLocationX
            boxWidth
            boxHeight
            boxRotation
            markerGroup
            markerGroupShared
            markerIndex
            markerSubIndex
            markerLabel
            markerStyle
            markerLocationX
            markerLocationY
            markerFontSize
            markerSize
            markerRotation
            gridCoordinates
            balloonGridCoordinates
            connectionPointGridCoordinates
            toleranceSource
            connectionPointLocationX
            connectionPointLocationY
            connectionPointIsFloating
            displayLeaderLine
            leaderLineDistance
            operation
            part {
              id
              name
            }
            drawing {
              id
              name
            }
            drawingSheet {
              id
              pageIndex
            }
            updatedAt
            createdAt
            deletedAt
            verified
            fullSpecificationFromOCR
            confidence
            reviewTaskId
          }
        }
      }
    `,
  },
  util: {
    inputFromParsed: (feature: ParsedCharacteristic) => {
      const char = { ...defaultFeature, ...feature, reviewerTaskId: 'none', createdAt: new Date(), updatedAt: new Date(), deletedAt: new Date() };
      const newFeature = Characteristics.util.input(char as Characteristic);
      return newFeature.char;
    },
    inputFromCustom: (feature: ParsedCharacteristic) => {
      const char = { ...defaultFeature, ...feature, captureMethod: 'Custom', reviewerTaskId: 'none', createdAt: new Date(), updatedAt: new Date() };
      const newFeature = Characteristics.util.input(char as Characteristic);
      return newFeature.char;
    },
    input: (item: Characteristic) => {
      const newCharacter: CharacteristicInput = {
        drawingSheetIndex: item.drawingSheetIndex,
        previousRevision: item.previousRevision,
        originalCharacteristic: item.originalCharacteristic,
        status: item.status,
        captureMethod: item.captureMethod,
        captureError: item.captureError,
        notationType: item.notationType,
        notationSubtype: item.notationSubtype,
        notationClass: item.notationClass,
        fullSpecification: item.fullSpecification || '',
        quantity: item.quantity,
        nominal: item.nominal || '',
        upperSpecLimit: item.upperSpecLimit,
        lowerSpecLimit: item.lowerSpecLimit,
        plusTol: item.plusTol,
        minusTol: item.minusTol,
        unit: item.unit,
        gdtSymbol: item.gdtSymbol,
        gdtPrimaryToleranceZone: item.gdtPrimaryToleranceZone,
        gdtSecondaryToleranceZone: item.gdtSecondaryToleranceZone,
        gdtPrimaryDatum: item.gdtPrimaryDatum,
        gdtSecondaryDatum: item.gdtSecondaryDatum,
        gdtTertiaryDatum: item.gdtTertiaryDatum,
        inspectionMethod: item.inspectionMethod,
        criticality: item.criticality,
        notes: item.notes,
        drawingRotation: item.drawingRotation,
        drawingScale: item.drawingScale,
        boxLocationY: item.boxLocationY,
        boxLocationX: item.boxLocationX,
        boxWidth: item.boxWidth,
        boxHeight: item.boxHeight,
        boxRotation: item.boxRotation,
        markerGroup: item.markerGroup,
        markerGroupShared: item.markerGroupShared,
        markerIndex: item.markerIndex,
        markerSubIndex: item.markerSubIndex,
        markerLabel: item.markerLabel,
        markerStyle: item.markerStyle,
        markerLocationX: item.markerLocationX,
        markerLocationY: item.markerLocationY,
        markerFontSize: item.markerFontSize,
        markerSize: item.markerSize,
        markerRotation: item.markerRotation || 0,
        gridCoordinates: item.gridCoordinates,
        balloonGridCoordinates: item.balloonGridCoordinates,
        connectionPointGridCoordinates: item.connectionPointGridCoordinates,
        toleranceSource: item.toleranceSource,
        operation: item.operation,
        part: item.part?.id,
        drawing: item.drawing?.id,
        drawingSheet: item.drawingSheet?.id,
        verified: item.verified,
        fullSpecificationFromOCR: item.fullSpecificationFromOCR,
        confidence: item.confidence,
        connectionPointLocationX: item.connectionPointLocationX,
        connectionPointLocationY: item.connectionPointLocationY,
        connectionPointIsFloating: item.connectionPointIsFloating,
        displayLeaderLine: item.displayLeaderLine,
        leaderLineDistance: item.leaderLineDistance,
      };
      return { id: item.id, char: newCharacter };
    },
  },
};

export default Characteristics;
