import gql from 'graphql-tag';
import { ReportTemplateInput, ReportTemplate, ReportTemplateFilterInput, ReportTemplateOrderByEnum } from 'types/reportTemplates';

export interface ReportTemplateUpdateVars {
  id: string;
  data: ReportTemplateInput;
}
export interface ReportTemplateUpdateData {
  reportTemplateUpdate: ReportTemplate;
}

export interface ReportTemplateDestroyVars {
  ids: string[];
}
export interface ReportTemplateDestroyData {
  reportTemplateDestroy: null;
}

export interface ReportTemplateCreateVars {
  data: ReportTemplateInput;
}
export interface ReportTemplateCreateData {
  reportTemplateCreate: ReportTemplate;
}

export interface ReportTemplateFindVars {
  id: string;
}
export interface ReportTemplateFindData {
  reportTemplateFind: ReportTemplate;
}

export interface ReportTemplateListVars {
  filter?: ReportTemplateFilterInput;
  orderBy?: ReportTemplateOrderByEnum;
  limit?: number;
  offset?: number;
}
export interface ReportTemplateListData {
  reportTemplateList: { count: number; rows: ReportTemplate[] };
}

export interface ReportTemplateListAll {
  default: { count: number; rows: ReportTemplate[] };
  custom: { count: number; rows: ReportTemplate[] };
}
export const ReportTemplateAPI = {
  mutate: {
    edit: gql`
      mutation REPORT_TEMPLATE_UPDATE($id: String!, $data: ReportTemplateInput!) {
        reportTemplateUpdate(id: $id, data: $data) {
          id
          provider {
            id
            status
            settings
            name
          }
          title
          type
          status
          direction
          settings
          file {
            id
            name
            privateUrl
            publicUrl
            privateUrl
            sizeInBytes
          }
          sortIndex
          filters
          createdAt
          updatedAt
        }
      }
    `,
    delete: gql`
      mutation REPORT_TEMPLATE_DESTROY($ids: [String!]!) {
        reportTemplateDestroy(ids: $ids)
      }
    `,
    create: gql`
      mutation REPORT_TEMPLATE_CREATE($data: ReportTemplateInput!) {
        reportTemplateCreate(data: $data) {
          id
          provider {
            id
            status
            settings
            name
          }
          title
          type
          status
          direction
          settings
          file {
            id
            name
            privateUrl
            publicUrl
            sizeInBytes
          }
          sortIndex
          filters
          createdAt
          updatedAt
        }
      }
    `,
  },
  query: {
    find: gql`
      query REPORT_TEMPLATE_FIND($id: String!) {
        reportTemplateFind(id: $id) {
          id
          provider {
            id
            status
            settings
            name
          }
          title
          type
          status
          direction
          settings
          file {
            id
            name
            privateUrl
            publicUrl
            sizeInBytes
          }
          sortIndex
          filters
          createdAt
          updatedAt
        }
      }
    `,
    list: gql`
      query REPORT_TEMPLATE_LIST($filter: ReportTemplateFilterInput, $orderBy: ReportTemplateOrderByEnum, $limit: Int, $offset: Int) {
        reportTemplateList(filter: $filter, orderBy: $orderBy, limit: $limit, offset: $offset) {
          count
          rows {
            id
            provider {
              id
              status
              settings
              name
              orgLogo {
                id
                publicUrl
              }
            }
            title
            type
            status
            direction
            settings
            file {
              id
              name
              privateUrl
              publicUrl
              privateUrl
              sizeInBytes
            }
            sortIndex
            filters
            createdAt
            updatedAt
          }
        }
      }
    `,
    listAll: gql`
      query ALL_TEMPLATES_LIST($filter: ReportTemplateFilterInput, $orderBy: ReportTemplateOrderByEnum, $limit: Int, $offset: Int) {
        default: reportTemplateList(filter: { status: [Active], provider: ["9b208135-ade6-4426-b31c-411d24140050"] }) {
          count
          rows {
            id
            provider {
              id
              status
              settings
              name
            }
            title
            type
            status
            direction
            settings
            file {
              id
              name
              privateUrl
              publicUrl
              privateUrl
              sizeInBytes
            }
            sortIndex
            filters
            createdAt
            updatedAt
          }
        }
        custom: reportTemplateList(filter: $filter, orderBy: $orderBy, limit: $limit, offset: $offset) {
          count
          rows {
            id
            provider {
              id
              status
              settings
              name
            }
            title
            type
            status
            direction
            settings
            file {
              id
              name
              privateUrl
              publicUrl
              privateUrl
            }
            sortIndex
            filters
            createdAt
            updatedAt
          }
        }
      }
    `,
  },
  createGraphqlInput: (template: ReportTemplate) => {
    const provider = template.provider ? (typeof template.provider === 'string' ? template.provider : template.provider.id) : template.id;
    const input: ReportTemplateInput = {
      provider,
      title: template.title ? template.title : 'No Title',
      type: template.type ? template.type : 'Other',
      status: template.status ? template.status : 'Active',
      direction: template.direction ? template.direction : 'Vertical',
      settings: template.settings ? template.settings : '',
      file: template.file ? template.file : [],
      sortIndex: template.sortIndex ? template.sortIndex : 0,
      filters: template.filters ? template.filters : '',
    };
    return input;
  },
};
