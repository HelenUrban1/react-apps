import gql from 'graphql-tag';
import { Annotation, AnnotationInput, defaultAnnotation } from 'types/annotation';

export const Annotations = {
  mutate: {
    edit: gql`
      mutation ANNOTATION_UPDATE($id: String!, $data: AnnotationInput!) {
        annotationUpdate(id: $id, data: $data) {
          id
          annotation
          fontSize
          fontColor
          borderColor
          backgroundColor
          previousRevision
          nextRevision
          originalAnnotation
          drawingSheetIndex
          drawingRotation
          drawingScale
          boxLocationY
          boxLocationX
          boxWidth
          boxHeight
          boxRotation
          part {
            id
            name
          }
          drawing {
            id
            name
          }
          drawingSheet {
            id
            pageIndex
          }
          characteristic {
            id
            fullSpecification
          }
          createdAt
          updatedAt
          deletedAt
        }
      }
    `,
    delete: gql`
      mutation ANNOTATION_DESTROY($ids: [String!]!) {
        annotationDestroy(ids: $ids)
      }
    `,
    create: gql`
      mutation ANNOTATION_CREATE($data: AnnotationInput!) {
        annotationCreate(data: $data) {
          id
          annotation
          fontSize
          fontColor
          borderColor
          backgroundColor
          previousRevision
          nextRevision
          originalAnnotation
          drawingSheetIndex
          drawingRotation
          drawingScale
          boxLocationY
          boxLocationX
          boxWidth
          boxHeight
          boxRotation
          part {
            id
            name
          }
          drawing {
            id
            name
          }
          drawingSheet {
            id
            pageIndex
          }
          characteristic {
            id
            fullSpecification
          }
          createdAt
          updatedAt
          deletedAt
        }
      }
    `,
  },
  query: {
    find: gql`
      query ANNOTATION_FIND($id: String!) {
        annotationFind(id: $id) {
          id
          annotation
          fontSize
          fontColor
          borderColor
          backgroundColor
          previousRevision
          nextRevision
          originalAnnotation
          drawingSheetIndex
          drawingRotation
          drawingScale
          boxLocationY
          boxLocationX
          boxWidth
          boxHeight
          boxRotation
          part {
            id
            name
          }
          drawing {
            id
            name
          }
          drawingSheet {
            id
            pageIndex
          }
          characteristic {
            id
            fullSpecification
          }
          createdAt
          updatedAt
          deletedAt
        }
      }
    `,
    list: gql`
      query ANNOTATION_LIST($filter: AnnotationFilterInput, $orderBy: AnnotationOrderByEnum, $limit: Int, $offset: Int) {
        annotationList(filter: $filter, orderBy: $orderBy, limit: $limit, offset: $offset) {
          count
          rows {
            id
            annotation
            fontSize
            fontColor
            borderColor
            backgroundColor
            previousRevision
            nextRevision
            originalAnnotation
            drawingSheetIndex
            drawingRotation
            drawingScale
            boxLocationY
            boxLocationX
            boxWidth
            boxHeight
            boxRotation
            part {
              id
              name
            }
            drawing {
              id
              name
            }
            drawingSheet {
              id
              pageIndex
            }
            characteristic {
              id
              fullSpecification
            }
            createdAt
            updatedAt
            deletedAt
          }
        }
      }
    `,
  },
  util: {
    input: (annotation: Annotation) => {
      const newAnnotation: AnnotationInput = {
        annotation: annotation.annotation,
        fontSize: annotation.fontSize || defaultAnnotation.fontSize,
        fontColor: annotation.fontColor || defaultAnnotation.fontColor,
        borderColor: annotation.borderColor || defaultAnnotation.borderColor,
        backgroundColor: annotation.backgroundColor || defaultAnnotation.backgroundColor,
        drawingSheetIndex: annotation.drawingSheetIndex,
        previousRevision: annotation.previousRevision || null,
        originalAnnotation: annotation.originalAnnotation || null,
        drawingRotation: annotation.drawingRotation,
        drawingScale: annotation.drawingScale,
        boxLocationY: annotation.boxLocationY,
        boxLocationX: annotation.boxLocationX,
        boxWidth: annotation.boxWidth,
        boxHeight: annotation.boxHeight,
        boxRotation: annotation.boxRotation,
        partId: annotation.part.id,
        drawingId: annotation.drawing.id,
        drawingSheetId: annotation.drawingSheet.id,
        characteristicId: annotation.characteristic?.id,
      };
      return { id: annotation.id, annotation: newAnnotation };
    },
  },
};

export default Annotations;
