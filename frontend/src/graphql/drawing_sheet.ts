import gql from 'graphql-tag';
import { Part } from 'domain/part/partTypes';
import { Drawing } from 'graphql/drawing';
import { Characteristic } from 'types/characteristics';
import { DefaultTolerances } from 'view/global/defaults';

export type DrawingSheet = {
  id: string;
  previousRevision: string | null;
  nextRevision: string | null;
  originalSheet: string | null;
  part: Part;
  drawing: Drawing;
  pageName: string;
  pageIndex: number;
  height: number;
  width: number;
  markerSize: number;
  rotation: string;
  grid: string;
  useGrid: boolean;
  displayLines: boolean;
  displayLabels: boolean;
  characteristics: Characteristic[];
  foundCaptureCount: number;
  acceptedCaptureCount: number;
  acceptedTimeoutCaptureCount: number;
  rejectedCaptureCount: number;
  reviewedCaptureCount: number;
  reviewedTimeoutCaptureCount: number;
  defaultLinearTolerances: string;
  defaultAngularTolerances: string;
  createdAt: Date;
  updatedAt: Date;
};

// The DrawingSheetChange type represents a change to one or more values
// which can be used to construct a valid DrawingSheetUpdateInput (see below)
// All fields are optional to allow partial updates without implicit assignment of undefined
export interface DrawingSheetChange {
  pageIndex?: number;
  height?: number;
  width?: number;
  markerSize?: number;
  rotation?: string;
  grid?: string;
  useGrid?: boolean;
  displayLines?: boolean;
  displayLabels?: boolean;
  characteristics?: string[];
  foundCaptureCount?: number;
  acceptedCaptureCount?: number;
  acceptedTimeoutCaptureCount?: number;
  rejectedCaptureCount?: number;
  reviewedCaptureCount?: number;
  reviewedTimeoutCaptureCount?: number;
  defaultLinearTolerances?: string;
  defaultAngularTolerances?: string;
  [key: string]: boolean | number | string | string[] | undefined;
}

// This is the 'DrawingSheetInput' type which is expected by the backend's update method's
// The backend's 'DrawingSheetInput' is at backend/src/api/drawingSheet/types/drawingSheetInput.js
export interface DrawingSheetInput {
  part: string;
  previousRevision?: string | null;
  nextRevision?: string | null;
  originalSheet?: string | null;
  drawing: string;
  pageIndex: number;
  height?: number;
  width?: number;
  markerSize?: number;
  rotation?: string;
  grid?: string;
  useGrid?: boolean;
  displayLines?: boolean;
  displayLabels?: boolean;
  characteristics?: string[];
  foundCaptureCount?: number;
  acceptedCaptureCount?: number;
  acceptedTimeoutCaptureCount?: number;
  rejectedCaptureCount?: number;
  reviewedCaptureCount?: number;
  reviewedTimeoutCaptureCount?: number;
  defaultLinearTolerances?: string;
  defaultAngularTolerances?: string;
  [key: string]: boolean | number | string | string[] | undefined | null;
}

export interface DrawingSheetEdit {
  drawing_sheetUpdate: DrawingSheetInput;
}
export interface DrawingSheetEditVars {
  id: string;
  data: DrawingSheetInput;
}

export interface DrawingSheetCreate {
  drawingSheetCreate: DrawingSheet;
}
export interface DrawingSheetCreateVars {
  data: DrawingSheetInput;
}

export interface DrawingSheetFind {
  drawing_sheetFind: DrawingSheet;
}

export interface DrawingSheetList {
  drawing_sheetList: {
    count: number;
    rows: DrawingSheet[];
  };
}

export const DrawingSheets = {
  mutate: {
    edit: gql`
      mutation DRAWINGSHEET_UPDATE($id: String!, $data: DrawingSheetInput!) {
        drawingSheetUpdate(id: $id, data: $data) {
          id
          previousRevision
          nextRevision
          originalSheet
          pageIndex
          height
          width
          rotation
          grid
          useGrid
          markerSize
          displayLines
          displayLabels
          part {
            id
            name
          }
          drawing {
            id
            name
          }
          characteristics {
            id
            markerLabel
          }
          foundCaptureCount
          acceptedCaptureCount
          acceptedTimeoutCaptureCount
          rejectedCaptureCount
          reviewedCaptureCount
          reviewedTimeoutCaptureCount
          defaultLinearTolerances
          defaultAngularTolerances
          createdAt
          updatedAt
        }
      }
    `,
    delete: gql`
      mutation DRAWINGSHEET_DESTROY($ids: [String!]!) {
        drawingSheetDestroy(ids: $ids)
      }
    `,
    create: gql`
      mutation DRAWINGSHEET_CREATE($data: DrawingSheetInput!) {
        drawingSheetCreate(data: $data) {
          id
          previousRevision
          nextRevision
          originalSheet
          pageIndex
          height
          width
          rotation
          grid
          displayLines
          displayLabels
          part {
            id
            name
          }
          drawing {
            id
            name
          }
          characteristics {
            id
            markerLabel
          }
          foundCaptureCount
          acceptedCaptureCount
          acceptedTimeoutCaptureCount
          rejectedCaptureCount
          reviewedCaptureCount
          reviewedTimeoutCaptureCount
          createdAt
          updatedAt
        }
      }
    `,
  },
  query: {
    find: gql`
      query DRAWINGSHEET_FIND($id: String!) {
        drawingSheetFind(id: $id) {
          id
          previousRevision
          nextRevision
          originalSheet
          pageIndex
          height
          width
          rotation
          grid
          useGrid
          markerSize
          displayLines
          displayLabels
          part {
            id
            name
          }
          drawing {
            id
            name
          }
          characteristics {
            id
            markerLabel
          }
          foundCaptureCount
          acceptedCaptureCount
          acceptedTimeoutCaptureCount
          rejectedCaptureCount
          reviewedCaptureCount
          reviewedTimeoutCaptureCount
          defaultLinearTolerances
          defaultAngularTolerances
          createdAt
          updatedAt
        }
      }
    `,
    list: gql`
      query DRAWINGSHEET_LIST($filter: DrawingSheetFilterInput, $orderBy: DrawingSheetOrderByEnum, $limit: Int, $offset: Int) {
        drawingSheetList(filter: $filter, orderBy: $orderBy, limit: $limit, offset: $offset) {
          count
          rows {
            id
            previousRevision
            nextRevision
            originalSheet
            pageIndex
            height
            width
            rotation
            grid
            useGrid
            markerSize
            displayLines
            displayLabels
            part {
              id
              name
            }
            drawing {
              id
              name
            }
            characteristics {
              id
              markerLabel
            }
            foundCaptureCount
            acceptedCaptureCount
            acceptedTimeoutCaptureCount
            rejectedCaptureCount
            reviewedCaptureCount
            reviewedTimeoutCaptureCount
            defaultLinearTolerances
            defaultAngularTolerances
            updatedAt
            createdAt
          }
        }
      }
    `,
  },
  createGraphqlInput: (drawingSheet: DrawingSheet) => {
    const validInput: DrawingSheetInput = {
      part: drawingSheet.part?.id || '',
      previousRevision: drawingSheet.previousRevision,
      nextRevision: drawingSheet.nextRevision,
      originalSheet: drawingSheet.originalSheet || drawingSheet.id,
      drawing: drawingSheet.drawing?.id || '',
      pageName: drawingSheet.pageName || 'Sheet',
      pageIndex: drawingSheet.pageIndex || 0,
      height: drawingSheet.height,
      width: drawingSheet.width,
      rotation: drawingSheet.rotation,
      grid: drawingSheet.grid,
      useGrid: drawingSheet.useGrid,
      markerSize: drawingSheet.markerSize,
      displayLines: drawingSheet.displayLines,
      displayLabels: drawingSheet.displayLabels,
      characteristics: drawingSheet?.characteristics?.map((char) => char.id),
      foundCaptureCount: drawingSheet.foundCaptureCount,
      acceptedCaptureCount: drawingSheet.acceptedCaptureCount,
      acceptedTimeoutCaptureCount: drawingSheet.acceptedTimeoutCaptureCount,
      rejectedCaptureCount: drawingSheet.rejectedCaptureCount,
      reviewedCaptureCount: drawingSheet.reviewedCaptureCount,
      reviewedTimeoutCaptureCount: drawingSheet.reviewedTimeoutCaptureCount,
      defaultLinearTolerances: drawingSheet.defaultLinearTolerances || JSON.stringify(DefaultTolerances.linear),
      defaultAngularTolerances: drawingSheet.defaultAngularTolerances || JSON.stringify(DefaultTolerances.angular),
    };
    return validInput;
  },
};
