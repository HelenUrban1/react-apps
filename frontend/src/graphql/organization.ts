import gql from 'graphql-tag';
import { RegionEnum, CountryEnum } from 'types/enums';
import { Report, ReportTemplate } from 'types/reportTemplates';
import { Part } from 'domain/part/partTypes';
import { File } from 'types/file';

type OrganizationStatusEnum = 'Active' | 'Archived';

type OrganizationTypeEnum = 'Account' | 'Customer' | 'App';

type OrganizationOrderByEnum = 'id_ASC' | 'id_DESC' | 'name_ASC' | 'name_DESC' | 'crmLink_ASC' | 'crmLink_DESC' | 'status_ASC' | 'status_DESC' | 'createdAt_ASC' | 'createdAt_DESC';

// QUESTION: Is Customer and CustomerType equivalent?
export interface Organization {
  id: string;
  status: OrganizationStatusEnum;
  type: OrganizationTypeEnum;
  settings?: string;
  name: string;
  phone?: string;
  email?: string;
  street1?: string;
  street2?: string;
  city?: string;
  region?: RegionEnum;
  postalCode?: string;
  country?: CountryEnum;
  orgLogo?: File[];
  parts?: Part[];
  reports?: Report[];
  templates?: ReportTemplate[];
  createdAt?: Date;
  updatedAt?: Date;
}

export interface OrganizationPage {
  rows: Organization[];
  count: number;
}

export interface OrganizationFilterInput {
  id?: string[];
  status?: OrganizationStatusEnum;
  type?: OrganizationTypeEnum;
  settings?: string;
  name?: string;
  phone?: string;
  email?: string;
  street1?: string;
  street2?: string;
  city?: string;
  region?: RegionEnum;
  postalCode?: string;
  country?: CountryEnum;
  parts?: string[];
}

export interface OrganizationInput {
  status: OrganizationStatusEnum;
  type: OrganizationTypeEnum;
  settings?: string;
  name: string;
  phone?: string;
  email?: string;
  street1?: string;
  street2?: string;
  city?: string;
  region?: RegionEnum;
  postalCode?: string;
  country?: CountryEnum;
  companyId?: string;
}

export interface OrganizationEdit {
  organizationUpdate: OrganizationInput;
}
export interface OrganizationEditVars {
  id: string;
  data: OrganizationInput;
}

export interface OrganizationCreate {
  organizationCreate: Organization;
}
export interface OrganizationCreateVars {
  data: OrganizationInput;
}

export interface OrganizationFind {
  organizationFind: Organization;
}
export interface OrganizationFindVars {
  id: string;
}

export interface OrganizationList {
  organizationList: {
    count: number;
    rows: Organization[];
  };
  self: {
    rows: Organization[];
  };
}
export interface OrganizationListVars {
  filter?: OrganizationFilterInput;
  orderBy?: OrganizationOrderByEnum;
  limit?: number;
  offset?: number;
}

const OrganizationAPI = {
  mutate: {
    edit: gql`
      mutation ORGANIZATION_UPDATE($id: String!, $data: OrganizationInput!) {
        organizationUpdate(id: $id, data: $data) {
          id
          status
          settings
          name
          phone
          email
          street1
          street2
          city
          region
          postalCode
          country
          orgLogo {
            id
            publicUrl
          }
          parts {
            id
          }
          reports {
            id
          }
          templates {
            id
          }
        }
      }
    `,
    delete: gql`
      mutation ORGANIZATION_DESTROY($id: [String!]!) {
        organizationDestroy(id: $ids)
      }
    `,
    create: gql`
      mutation ORGANIZATION_CREATE($data: OrganizationInput!) {
        organizationCreate(data: $data) {
          id
          status
          settings
          name
          phone
          email
          street1
          street2
          city
          region
          postalCode
          country
          orgLogo {
            id
            publicUrl
          }
          parts {
            id
          }
          reports {
            id
          }
          templates {
            id
          }
        }
      }
    `,
  },
  query: {
    find: gql`
      query ORGANIZATION_FIND($id: String!) {
        organizationFind(id: $id) {
          id
          status
          settings
          name
          phone
          email
          street1
          street2
          city
          region
          postalCode
          country
          orgLogo {
            id
            publicUrl
          }
          parts {
            id
          }
          reports {
            id
          }
          templates {
            id
          }
        }
      }
    `,
    list: gql`
      query ORGANIZATION_LIST($filter: OrganizationFilterInput, $orderBy: OrganizationOrderByEnum, $limit: Int, $offset: Int) {
        organizationList(filter: $filter, orderBy: $orderBy, limit: $limit, offset: $offset) {
          rows {
            id
            status
            settings
            name
            phone
            email
            street1
            street2
            city
            region
            postalCode
            country
            orgLogo {
              id
              publicUrl
            }
            parts {
              id
            }
            reports {
              id
            }
            templates {
              id
              title
              type
              status
              direction
              settings
              file {
                id
                name
                sizeInBytes
                publicUrl
                privateUrl
              }
              sortIndex
              filters
              createdAt
              updatedAt
            }
          }
        }
        self: organizationList(filter: { type: Account }) {
          count
          rows {
            id
            status
            settings
            name
            phone
            email
            street1
            street2
            city
            region
            postalCode
            country
            orgLogo {
              id
              publicUrl
            }
            parts {
              id
            }
            reports {
              id
            }
            templates {
              id
              title
              type
              status
              direction
              settings
              file {
                id
                name
                sizeInBytes
                publicUrl
                privateUrl
              }
              sortIndex
              filters
              createdAt
              updatedAt
            }
          }
        }
      }
    `,
  },
};

export default OrganizationAPI;
