import gql from 'graphql-tag';
import { Marker, MarkerShape, MarkerStrokeStyleEnum } from '../types/marker';

export interface MarkerInput {
  markerStyleName?: string;
  shape: MarkerShape;
  hasLeaderLine?: Boolean;
  fontSize?: number;
  fontColor: string;
  fillColor: string;
  borderColor: string;
  borderWidth: number;
  borderStyle: MarkerStrokeStyleEnum;
  leaderLineColor: string;
  leaderLineWidth: number;
  leaderLineStyle: MarkerStrokeStyleEnum;
  defaultPosition?: string;
}

export interface MarkerFilterInput {
  id?: string[];
  markerStyleName?: string;
  shape?: MarkerShape;
  defaultPosition?: string;
  hasLeaderLine?: Boolean;
  fontSizeRange?: number[];
  fontColor?: string;
  fillColor?: string;
  borderColor?: string;
  borderWidthRange?: number[];
  leaderLineColor?: string;
  leaderLineWidthRange?: number[];
  createdAtRange?: Date[];
}

export interface MarkerEdit {
  markerUpdate: MarkerInput;
}

export interface MarkerCreate {
  markerCreate: Marker;
}

export interface MarkerFind {
  markerFind: Marker;
}

export interface MarkerList {
  markerList: {
    count: number;
    rows: Marker[];
  };
}

type MarkerOrderByEnum =
  | 'id_ASC'
  | 'id_DESC'
  | 'markerStyleName_ASC'
  | 'markerStyleName_DESC'
  | 'shape_ASC'
  | 'shape_DESC'
  | 'defaultPosition_ASC'
  | 'defaultPosition_DESC'
  | 'hasLeaderLine_ASC'
  | 'hasLeaderLine_DESC'
  | 'fontSize_ASC'
  | 'fontSize_DESC'
  | 'fontColor_ASC'
  | 'fontColor_DESC'
  | 'fillColor_ASC'
  | 'fillColor_DESC'
  | 'borderColor_ASC'
  | 'borderColor_DESC'
  | 'borderWidth_ASC'
  | 'borderWidth_DESC'
  | 'leaderLineColor_ASC'
  | 'leaderLineColor_DESC'
  | 'leaderLineWidth_ASC'
  | 'leaderLineWidth_DESC'
  | 'createdAt_ASC'
  | 'createdAt_DESC';

export interface MarkerListVars {
  filter?: MarkerFilterInput;
  orderBy?: MarkerOrderByEnum;
  limit?: number;
  offset?: number;
}

const Markers = {
  mutate: {
    edit: gql`
      mutation MARKER_UPDATE($id: String!, $data: MarkerInput!) {
        markerUpdate(id: $id, data: $data) {
          id
          markerStyleName
          shape
          defaultPosition
          hasLeaderLine
          fontSize
          fontColor
          fillColor
          borderColor
          borderWidth
          borderStyle
          leaderLineColor
          leaderLineWidth
          leaderLineStyle
          updatedAt
          createdAt
          deletedAt
        }
      }
    `,
    delete: gql`
      mutation MARKER_DESTROY($ids: [String!]!) {
        markerDestroy(ids: $ids)
      }
    `,
    create: gql`
      mutation MARKER_CREATE($data: MarkerInput!) {
        markerCreate(data: $data) {
          id
          markerStyleName
          shape
          defaultPosition
          hasLeaderLine
          fontSize
          fontColor
          fillColor
          borderColor
          borderWidth
          borderStyle
          leaderLineColor
          leaderLineWidth
          leaderLineStyle
          updatedAt
          createdAt
          deletedAt
        }
      }
    `,
  },
  query: {
    find: gql`
      query MARKER_FIND($id: String!) {
        markerFind(id: $id) {
          id
          shape
          fillColor
          borderColor
        }
      }
    `,
    list: gql`
      query MARKER_LIST($filter: MarkerFilterInput, $orderBy: MarkerOrderByEnum, $limit: Int, $offset: Int, $paranoid: Boolean) {
        markerList(filter: $filter, orderBy: $orderBy, limit: $limit, offset: $offset, paranoid: $paranoid) {
          count
          rows {
            id
            markerStyleName
            shape
            defaultPosition
            hasLeaderLine
            fontSize
            fontColor
            fillColor
            borderColor
            borderWidth
            borderStyle
            leaderLineColor
            leaderLineWidth
            leaderLineStyle
            updatedAt
            createdAt
            deletedAt
          }
        }
      }
    `,
  },
  createInputFromObject: (markerObject: any) => {
    const data: MarkerInput = {
      markerStyleName: markerObject.markerStyleName || 'Custom',
      shape: markerObject.shape || 'Circle',
      defaultPosition: markerObject.defaultPosition || 'Left',
      hasLeaderLine: markerObject.hasLeaderLine || false,
      fontSize: markerObject.fontSize || 10,
      fontColor: markerObject.fontColor || 'White',
      fillColor: markerObject.fillColor || 'Coral',
      borderColor: markerObject.borderColor || 'Coral',
      borderWidth: markerObject.borderWidth || 1,
      borderStyle: markerObject.borderStyle || 'Solid',
      leaderLineColor: markerObject.leaderLineColor || 'Coral',
      leaderLineWidth: markerObject.leaderLineWidth || 1,
      leaderLineStyle: markerObject.leaderLineStyle || 'Solid',
    };
    return data;
  },
};

export default Markers;
