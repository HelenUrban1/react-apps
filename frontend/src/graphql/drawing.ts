import gql from 'graphql-tag';

import { File, FileInput } from 'types/file';
import { Part } from 'domain/part/partTypes';
import { Characteristic } from 'types/characteristics';
import { ApolloClient, NormalizedCacheObject } from '@apollo/client';
import { DrawingSheet } from './drawing_sheet';

export type DrawingStatusEnum = 'Active' | 'Deleted';
export type DocumentTypeEnum = 'Drawing' | 'Notes' | 'Materials' | 'Specifications' | 'Other';

// The default value of a Drawing when its first being created in the DB
export const newDrawingDefaults = {
  part: null,
  previousRevision: null,
  nextRevision: null,
  originalDrawing: null,
  documentType: 'Drawing',
  file: [],
  drawingFile: null,
  name: null,
  number: null,
  revision: null,
  linearUnit: null,
  angularUnit: null,
  markerOptions: null,
  gridOptions: JSON.stringify({ status: true }),
  linearTolerances: null,
  angularTolerances: null,
  sheetCount: 0,
  sheets: [],
  characteristics: [],
  tags: null,
};

export interface Drawing {
  id: string;
  previousRevision: string | null;
  nextRevision: string | null;
  originalDrawing: string | null;
  status: DrawingStatusEnum;
  documentType: DocumentTypeEnum;
  part: Part;
  file: File[];
  drawingFile: File;
  name: string;
  number: string;
  revision: string;
  linearUnit: string;
  angularUnit: string;
  markerOptions: string;
  gridOptions: string;
  linearTolerances: string;
  angularTolerances: string;
  sheetCount: number;
  sheets: DrawingSheet[];
  characteristics?: Characteristic[];
  tags: string;
  createdAt: Date;
  updatedAt: Date;
}

export type DrawingOrderByEnum =
  | 'id_ASC'
  | 'id_DESC'
  | 'sheetCount_ASC'
  | 'sheetCount_DESC'
  | 'documentType_ASC'
  | 'documentType_DESC'
  | 'name_ASC'
  | 'name_DESC'
  | 'number_ASC'
  | 'number_DESC'
  | 'revision_ASC'
  | 'revision_DESC'
  | 'linearUnit_ASC'
  | 'linearUnit_DESC'
  | 'angularUnit_ASC'
  | 'angularUnit_DESC'
  | 'markerOptions_ASC'
  | 'markerOptions_DESC'
  | 'gridOptions_ASC'
  | 'gridOptions_DESC'
  | 'linearTolerances_ASC'
  | 'linearTolerances_DESC'
  | 'tags_ASC'
  | 'tags_DESC'
  | 'angularTolerances_ASC'
  | 'angularTolerances_DESC'
  | 'createdAt_ASC'
  | 'createdAt_DESC';
export interface DrawingFilterInput {
  id?: string;
  site?: string;
  documentType?: DocumentTypeEnum;
  status?: DrawingStatusEnum;
  part?: string;
  sheetCount?: number;
  name?: string;
  number?: string;
  revision?: string;
  previousRevision?: string | null;
  originalDrawing?: string | null;
  linearUnit?: string;
  angularUnit?: string;
  markerOptions?: string;
  gridOptions?: string;
  linearTolerances?: string;
  tags?: string;
  angularTolerances?: string;
  createdAtRange?: Date[];
}

// The DrawingChange type represents a change to one or more values
// which can be used to construct a valid DrawingUpdateInput (see below)
// All fields are optional to allow partial updates without implicit assignment of undefined
export interface DrawingChange {
  name?: string;
  documentType?: DocumentTypeEnum;
  number?: string;
  revision?: string;
  linearUnit?: string;
  angularUnit?: string;
  markerOptions?: string;
  gridOptions?: string;
  linearTolerances?: string;
  tags?: string;
  angularTolerances?: string;
  // file: string[];
  sheetCount?: number;
  sheets?: string[];
  characteristics?: string[];
  [key: string]: boolean | number | string | string[] | undefined;
}

export interface DrawingInput {
  part: string;
  previousRevision?: string | null;
  nextRevision?: string | null;
  originalDrawing: string | null;
  documentType: DocumentTypeEnum;
  status: DrawingStatusEnum;
  file: FileInput[];
  drawingFile?: FileInput;
  sheetCount: number;
  name: string;
  number: string;
  revision: string;
  linearUnit: string;
  angularUnit: string;
  markerOptions: string;
  gridOptions?: string;
  linearTolerances: string;
  tags: string;
  angularTolerances: string;
  sheets: string[];
  characteristics?: string[];
  [key: string]: boolean | FileInput | FileInput[] | number | string | string[] | null | undefined;
}

export interface DrawingEdit {
  drawingUpdate: Drawing;
}

export interface DrawingEditVars {
  id: string;
  data: DrawingInput;
}

export interface DrawingCreate {
  drawingCreate: Drawing;
}
export interface DrawingCreateVars {
  data: DrawingInput;
}

export interface DrawingRevision {
  drawingRevision: {
    part: Part;
    drawing: Drawing;
    sheets: DrawingSheet[];
    characteristics: Characteristic[];
  };
}

export interface DrawingRevisionVars {
  drawingId: string;
  partId: string;
}

export interface DrawingFind {
  drawingFind: Drawing;
}

export interface DrawingList {
  drawingList: {
    count: number;
    rows: Drawing[];
  };
}
export interface DrawingListVars {
  filter?: DrawingFilterInput;
  limit?: number;
  offset?: number;
  orderBy?: DrawingOrderByEnum;
}

export const DrawingsAPI = {
  mutate: {
    edit: gql`
      mutation DRAWING_UPDATE($id: String!, $data: DrawingInput!) {
        drawingUpdate(id: $id, data: $data) {
          id
          documentType
          part {
            id
            name
          }
          file {
            id
            name
            sizeInBytes
            publicUrl
            privateUrl
          }
          drawingFile {
            id
            name
            sizeInBytes
            publicUrl
            privateUrl
          }
          name
          previousRevision
          nextRevision
          originalDrawing
          status
          number
          revision
          linearUnit
          angularUnit
          markerOptions
          gridOptions
          linearTolerances
          tags
          angularTolerances
          sheetCount
          sheets {
            id
            previousRevision
            nextRevision
            originalSheet
            pageIndex
            height
            width
            markerSize
            rotation
            grid
            useGrid
            displayLines
            displayLabels
            part {
              id
              name
            }
            drawing {
              id
              name
            }
            characteristics {
              id
              markerLabel
              drawing {
                id
                name
                number
              }
            }
            foundCaptureCount
            acceptedCaptureCount
            acceptedTimeoutCaptureCount
            rejectedCaptureCount
            reviewedCaptureCount
            reviewedTimeoutCaptureCount
            createdAt
            updatedAt
          }
          characteristics {
            id
            markerLabel
            drawing {
              id
              name
              number
            }
          }
          createdAt
          updatedAt
        }
      }
    `,
    revise: gql`
      mutation DRAWING_REVISION($drawingId: String!, $partId: String!) {
        drawingRevision(partId: $partId, drawingId: $drawingId) {
          part {
            id
            customer {
              id
              name
            }
            purchaseOrderCode
            purchaseOrderLink
            type
            name
            number
            revision
            originalPart
            previousRevision
            nextRevision
            drawingNumber
            drawingRevision
            defaultMarkerOptions
            defaultLinearTolerances
            defaultAngularTolerances
            presets
            status
            reviewStatus
            workflowStage
            completedAt
            balloonedAt
            reportGeneratedAt
            notes
            accessControl
            measurement
            tags
            displayLeaderLines
            primaryDrawing {
              id
              name
              number
              revision
              previousRevision
              nextRevision
              originalDrawing
              angularTolerances
              angularUnit
              linearTolerances
              linearUnit
              markerOptions
              sheetCount
              status
              tags
              createdAt
              updatedAt
              gridOptions
              file {
                id
                sizeInBytes
                name
                publicUrl
                privateUrl
              }
            }
            drawings {
              id
              name
              number
              revision
              previousRevision
              nextRevision
              originalDrawing
              linearUnit
              angularUnit
              markerOptions
              gridOptions
              linearTolerances
              tags
              angularTolerances
              sheetCount
              createdAt
              updatedAt
              gridOptions
              sheets {
                id
                previousRevision
                nextRevision
                originalSheet
                pageIndex
                height
                width
                markerSize
                rotation
                grid
                useGrid
                displayLines
                displayLabels
                part {
                  id
                  name
                }
                drawing {
                  id
                  name
                }
                characteristics {
                  id
                  markerLabel
                  drawing {
                    id
                    name
                    number
                  }
                }
                foundCaptureCount
                acceptedCaptureCount
                acceptedTimeoutCaptureCount
                rejectedCaptureCount
                reviewedCaptureCount
                reviewedTimeoutCaptureCount
                createdAt
                updatedAt
              }
              file {
                id
                sizeInBytes
                name
                publicUrl
                privateUrl
              }
              drawingFile {
                id
                sizeInBytes
                name
                publicUrl
                privateUrl
              }
            }
            reports {
              id
              title
              updatedAt
              file {
                name
                publicUrl
                privateUrl
              }
            }
            characteristics {
              id
              fullSpecification
              previousRevision
              nextRevision
              originalCharacteristic
              drawing {
                id
                name
                number
              }
            }
            attachments {
              id
              sizeInBytes
              name
              publicUrl
              privateUrl
            }
            createdAt
            updatedAt
          }
          drawing {
            id
            name
            number
            revision
            previousRevision
            nextRevision
            originalDrawing
            linearUnit
            angularUnit
            markerOptions
            gridOptions
            linearTolerances
            tags
            angularTolerances
            sheetCount
            createdAt
            updatedAt
            gridOptions
            sheets {
              id
              previousRevision
              nextRevision
              originalSheet
              pageIndex
              pageName
              height
              width
              markerSize
              rotation
              grid
              useGrid
              displayLines
              displayLabels
              part {
                id
              }
              drawing {
                id
              }
              characteristics {
                id
              }
              foundCaptureCount
              acceptedCaptureCount
              acceptedTimeoutCaptureCount
              rejectedCaptureCount
              reviewedCaptureCount
              reviewedTimeoutCaptureCount
              createdAt
              updatedAt
            }
            file {
              id
              sizeInBytes
              name
              publicUrl
              privateUrl
            }
            drawingFile {
              id
              sizeInBytes
              name
              publicUrl
              privateUrl
            }
          }
          sheets {
            id
            previousRevision
            nextRevision
            originalSheet
            pageIndex
            height
            width
            markerSize
            rotation
            grid
            useGrid
            displayLines
            displayLabels
            part {
              id
              name
            }
            drawing {
              id
              name
            }
            characteristics {
              id
              markerLabel
              drawing {
                id
                name
                number
              }
            }
            foundCaptureCount
            acceptedCaptureCount
            acceptedTimeoutCaptureCount
            rejectedCaptureCount
            reviewedCaptureCount
            reviewedTimeoutCaptureCount
            createdAt
            updatedAt
          }
          characteristics {
            id
            previousRevision
            nextRevision
            originalCharacteristic
            status
            captureMethod
            captureError
            drawingSheetIndex
            notationType
            notationSubtype
            notationClass
            fullSpecification
            quantity
            nominal
            upperSpecLimit
            lowerSpecLimit
            plusTol
            minusTol
            unit
            gdtSymbol
            gdtPrimaryToleranceZone
            gdtSecondaryToleranceZone
            gdtPrimaryDatum
            gdtSecondaryDatum
            gdtTertiaryDatum
            criticality
            inspectionMethod
            notes
            drawingRotation
            drawingScale
            boxLocationY
            boxLocationX
            boxWidth
            boxHeight
            boxRotation
            markerGroup
            markerGroupShared
            markerIndex
            markerSubIndex
            markerLabel
            markerStyle
            markerLocationX
            markerLocationY
            markerFontSize
            markerSize
            markerRotation
            gridCoordinates
            balloonGridCoordinates
            connectionPointGridCoordinates
            operation
            toleranceSource
            verified
            fullSpecificationFromOCR
            confidence
            connectionPointLocationX
            connectionPointLocationY
            connectionPointIsFloating
            displayLeaderLine
            leaderLineDistance
            drawing {
              id
              name
              number
            }
            drawingSheet {
              id
            }
          }
        }
      }
    `,
    delete: gql`
      mutation DRAWING_DESTROY($ids: [String!]!) {
        drawingDestroy(ids: $ids)
      }
    `,
    create: gql`
      mutation DRAWING_CREATE($data: DrawingInput!) {
        drawingCreate(data: $data) {
          id
          documentType
          part {
            id
            name
          }
          file {
            id
            name
            sizeInBytes
            publicUrl
            privateUrl
          }
          drawingFile {
            id
            name
            sizeInBytes
            publicUrl
            privateUrl
          }
          name
          previousRevision
          nextRevision
          originalDrawing
          status
          number
          revision
          linearUnit
          angularUnit
          markerOptions
          gridOptions
          linearTolerances
          tags
          angularTolerances
          sheetCount
          sheets {
            id
            previousRevision
            nextRevision
            originalSheet
            pageIndex
            height
            width
            markerSize
            rotation
            grid
            useGrid
            displayLines
            displayLabels
            part {
              id
              name
            }
            drawing {
              id
              name
            }
            characteristics {
              id
              markerLabel
              drawing {
                id
                name
                number
              }
            }
            foundCaptureCount
            acceptedCaptureCount
            acceptedTimeoutCaptureCount
            rejectedCaptureCount
            reviewedCaptureCount
            reviewedTimeoutCaptureCount
            createdAt
            updatedAt
          }
          characteristics {
            id
            markerLabel
            drawing {
              id
              name
              number
            }
          }
          createdAt
          updatedAt
        }
      }
    `,
  },
  query: {
    find: gql`
      query DRAWING_FIND($id: String!) {
        drawingFind(id: $id) {
          id
          previousRevision
          nextRevision
          originalDrawing
          documentType
          part {
            id
            name
          }
          file {
            id
            name
            sizeInBytes
            publicUrl
            privateUrl
          }
          drawingFile {
            id
            name
            sizeInBytes
            publicUrl
            privateUrl
          }
          name
          number
          revision
          linearUnit
          angularUnit
          markerOptions
          gridOptions
          linearTolerances
          tags
          angularTolerances
          sheetCount
          sheets {
            id
            previousRevision
            nextRevision
            originalSheet
            pageIndex
            height
            width
            markerSize
            rotation
            grid
            useGrid
            displayLines
            displayLabels
            part {
              id
              name
            }
            drawing {
              id
              name
            }
            characteristics {
              id
              markerLabel
              drawing {
                id
                name
                number
              }
            }
            foundCaptureCount
            acceptedCaptureCount
            acceptedTimeoutCaptureCount
            rejectedCaptureCount
            reviewedCaptureCount
            reviewedTimeoutCaptureCount
            createdAt
            updatedAt
          }
          characteristics {
            id
            markerLabel
            drawing {
              id
              name
              number
            }
          }
          createdAt
          updatedAt
        }
      }
    `,
    list: gql`
      query DRAWING_LIST($filter: DrawingFilterInput, $orderBy: DrawingOrderByEnum, $limit: Int, $offset: Int) {
        drawingList(filter: $filter, orderBy: $orderBy, limit: $limit, offset: $offset) {
          count
          rows {
            id
            previousRevision
            nextRevision
            originalDrawing
            documentType
            part {
              id
              name
            }
            file {
              id
              name
              sizeInBytes
              publicUrl
              privateUrl
            }
            drawingFile {
              id
              name
              sizeInBytes
              publicUrl
              privateUrl
            }
            sheetCount
            name
            number
            revision
            linearUnit
            angularUnit
            markerOptions
            gridOptions
            linearTolerances
            tags
            angularTolerances
            sheets {
              id
              previousRevision
              nextRevision
              originalSheet
              height
              width
              markerSize
              rotation
              grid
              useGrid
              displayLines
              displayLabels
              pageIndex
              foundCaptureCount
              acceptedCaptureCount
              acceptedTimeoutCaptureCount
              rejectedCaptureCount
              reviewedCaptureCount
              reviewedTimeoutCaptureCount
            }
            characteristics {
              id
              markerLabel
              drawing {
                id
                name
                number
              }
            }
            updatedAt
            createdAt
          }
        }
      }
    `,
    revisions: gql`
      query DRAWING_REVISIONS($filter: DrawingFilterInput, $orderBy: DrawingOrderByEnum, $limit: Int, $offset: Int) {
        drawingList(filter: $filter, orderBy: $orderBy, limit: $limit, offset: $offset) {
          count
          rows {
            id
            previousRevision
            nextRevision
            originalDrawing
            name
            number
            revision
            createdAt
            part {
              id
            }
          }
        }
      }
    `,
  },
  createGraphqlInput: (drawing: Drawing) => {
    const validInput: DrawingInput = {
      status: drawing.status ? drawing.status : 'Active',
      previousRevision: drawing.previousRevision,
      nextRevision: drawing.nextRevision,
      originalDrawing: drawing.originalDrawing,
      documentType: drawing.documentType ? drawing.documentType : 'Drawing',
      part: drawing.part?.id,
      file: drawing.file ? [...drawing.file] : [],
      drawingFile: drawing.drawingFile,
      sheetCount: drawing.sheetCount ? drawing.sheetCount : 0,
      name: drawing.name ? drawing.name : '',
      number: drawing.number ? drawing.number : '',
      revision: drawing.revision ? drawing.revision : '',
      linearUnit: drawing.linearUnit ? drawing.linearUnit : '',
      angularUnit: drawing.angularUnit ? drawing.angularUnit : '',
      markerOptions: drawing.markerOptions ? drawing.markerOptions : '',
      gridOptions: drawing.gridOptions ? drawing.gridOptions : JSON.stringify({ status: true }),
      linearTolerances: drawing.linearTolerances ? drawing.linearTolerances : '',
      tags: drawing.tags ? drawing.tags : '',
      angularTolerances: drawing.angularTolerances ? drawing.angularTolerances : '',
      sheets: drawing.sheets ? drawing.sheets.map((sheet) => sheet.id) : [],
      characteristics: drawing.characteristics ? drawing.characteristics.map((char) => char.id) : [],
    };
    return validInput;
  },
};

export const reviseDrawingMutation = async (queryObject: { drawingId: string; partId: string }, client: ApolloClient<NormalizedCacheObject>) => {
  return client.mutate<DrawingRevision, DrawingRevisionVars>({
    mutation: DrawingsAPI.mutate.revise,
    variables: queryObject,
    fetchPolicy: 'network-only',
  });
};

export default Drawing;
