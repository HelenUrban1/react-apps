import { ConnectedRouter } from 'connected-react-router';
import React, { useEffect, useState } from 'react';
import { Provider } from 'react-redux';
import { ApolloProvider } from '@apollo/client';
import Spinner from 'view/shared/Spinner';
import RoutesComponent from 'view/shared/routes/RoutesComponent';
import MFAModal from 'view/shared/MFAModal';
import { configureStore, getHistory } from 'modules/store';
import getClient from 'modules/shared/graphql/graphqlClient';
import './App.less';
import './overrides.less';

const store = configureStore();

const App = () => {
  const [client, setClient] = useState(undefined);
  useEffect(() => {
    getClient().then((graphqlClient) => {
      setClient(graphqlClient);
    });
    return () => {};
  }, []);

  if (client === undefined) return <Spinner style={{ height: '100vh' }} />;
  return (
    <ApolloProvider client={client}>
      <Provider store={store}>
        <ConnectedRouter history={getHistory()}>
          <MFAModal />
          <RoutesComponent />
        </ConnectedRouter>
      </Provider>
    </ApolloProvider>
  );
};

// expose store when run in Cypress
if (window.Cypress) {
  window.store = store;
}
export default App;
