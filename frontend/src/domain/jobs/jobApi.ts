import gql from 'graphql-tag';
import log from 'modules/shared/logger';
import { Job, JobInput } from './jobTypes';

// Type definitions for the returned api data
export interface JobCreate {
  jobCreate: Job;
}

export interface JobFind {
  jobFind: Job;
}

export interface JobList {
  jobList: {
    count: number;
    rows: Job[];
  };
}

export interface JobEdit {
  jobUpdate: JobInput;
}

export const Jobs = {
  mutate: {
    edit: gql`
      mutation JOB_UPDATE($id: String!, $data: JobInput!) {
        jobUpdate(id: $id, data: $data) {
          id
          name
          parts {
            id
            name
          }
          jobSamples {
            id
            sampleIndex
            serial
            status
            partId
          }
          sampling
          jobStatus
          interval
          samples
          passing
          scrapped
          accessControl
          createdAt
          updatedAt
        }
      }
    `,
    delete: gql`
      mutation JOB_DESTROY($ids: [String]!) {
        jobDestroy(ids: $ids)
      }
    `,
    create: gql`
      mutation JOB_CREATE($data: JobInput!) {
        jobCreate(data: $data) {
          id
          name
          parts {
            id
            name
          }
          jobSamples {
            id
            sampleIndex
            serial
            status
            partId
          }
          jobStatus
          sampling
          interval
          samples
          passing
          scrapped
          accessControl
          createdAt
          updatedAt
        }
      }
    `,
  },
  query: {
    find: gql`
      query JOB_FIND($id: String!) {
        jobFind(id: $id) {
          id
          parts {
            id
          }
          jobSamples {
            id
            sampleIndex
            serial
            status
            partId
          }
          jobStatus
          accessControl
          name
          sampling
          interval
          samples
          passing
          scrapped
        }
      }
    `,
    list: gql`
      query JOB_LIST($filter: JobFilterInput, $orderBy: JobOrderByEnum, $limit: Int, $offset: Int) {
        jobList(filter: $filter, orderBy: $orderBy, limit: $limit, offset: $offset) {
          count
          rows {
            id
            name
            parts {
              id
              name
            }
            jobSamples {
              id
              sampleIndex
              serial
              status
              partId
            }
            jobStatus
            sampling
            interval
            samples
            passing
            scrapped
            accessControl
            createdAt
            updatedAt
          }
        }
      }
    `,
  },
  createGraphqlInput: (data: Job) => {
    if (!data) {
      log.error('No data was passed to create an input', data);
      return null;
    }
    const input: JobInput = {
      name: data.name,
      parts: data.parts && data.parts[0].id ? data.parts?.map((part) => part.id) || [] : data.parts || [],
      jobStatus: data.jobStatus,
      sampling: data.sampling,
      interval: data.interval,
      samples: data.samples,
      accessControl: data.accessControl,
      passing: data.passing,
      scrapped: data.scrapped,
    };
    return input;
  },
};
