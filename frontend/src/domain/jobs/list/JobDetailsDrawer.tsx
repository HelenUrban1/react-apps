import React, { ChangeEvent } from 'react';
import { Button, Drawer, Tooltip } from 'antd';
import { DeleteOutlined, CloseOutlined } from '@ant-design/icons';
import { Section } from 'view/global/inputs';
import { IxButton } from 'styleguide';
import { i18n } from 'i18n';
import { useSelector } from 'react-redux';
import { AppState } from 'modules/state';
import { Job } from 'domain/jobs/jobTypes';
import { PartListAutocomplete, Parts } from 'domain/part/partApi';
import { useQuery } from '@apollo/client';

interface Props {
  parentSelector: string;
  job: Job;
  isOpen: boolean;
  handleDrawerClose: () => void;
  handleJobFieldChange: (event: ChangeEvent<HTMLTextAreaElement>) => void;
  handleJobDelete: (job: Job, source: string) => void;
  handleSave: () => void;
  currentTablePage: number;
  currentTableFilter: object;
  currentTableSorter: object;
}

export const JobDetailsDrawer: React.FC<Props> = ({
  job, // Job to be displayed
  parentSelector,
  isOpen, // Whether drawer is visible
  handleDrawerClose, // Closing of drawer
  handleJobFieldChange, // Changes to any EditInPlace text inputs
  handleSave,
  handleJobDelete,
}) => {
  const { state: subState } = useSelector((app: AppState) => app.subscription);

  const [partsAutocompleteParams, setPartsAutocompleteParams] = React.useState({
    query: '',
    limit: 0,
  });

  const partIds = job.parts ? job.parts.map((part) => part.id) : null;

  const samplingMethod = job && job.sampling ? job.sampling : 'Quantity';

  const partListResult = useQuery<PartListAutocomplete>(Parts.query.autocomplete, {
    variables: partsAutocompleteParams,
  });

  const partData = partListResult.data;

  const parts = partData ? partData.partAutocomplete : [];

  const inputs = {
    sections: [
      [
        {
          name: 'name',
          label: i18n('entities.job.form.number'),
          classes: 'group-flex-3 required',
          value: (job && job.name ? job.name : null) as string | null,
          blur: handleJobFieldChange,
          type: 'text',
          required: true,
        },

        {
          name: 'partGroup',
          label: '',
          value: '',
          group: [
            {
              name: 'parts',
              label: i18n('entities.job.form.part'),
              value: parts
                .filter((part) => partIds?.includes(part.id))
                .map((part) => part.id)
                .join(','),
              change: handleJobFieldChange,
              type: 'select',
              multiple: true,
              search: true,
              group: parts.map((part) => ({
                name: part.label || part.id,
                label: part.label || part.id,
                value: part.id,
                id: part.id,
              })),
            },
          ],
        },
        {
          name: 'accessControl',
          label: i18n('entities.job.form.access'),
          value: job.accessControl,
          change: handleJobFieldChange,
          type: 'select',
          group: [
            {
              name: 'None',
              label: 'None',
              value: 'None',
            },
            {
              name: 'ITAR',
              label: 'ITAR',
              value: 'ITAR',
            },
          ],
        },

        {
          name: 'Sampling',
          label: 'Sampling',
          value: '',
          group: [
            {
              name: 'sampling',
              label: i18n('entities.job.form.method'),
              classes: 'group-flex-3 required',
              value: samplingMethod,
              change: handleJobFieldChange,
              type: 'select',
              group: [
                {
                  name: 'Quantity',
                  label: 'Quantity',
                  value: 'Quantity',
                },
                {
                  name: 'Timed',
                  label: 'Timed',
                  value: 'Timed',
                },
              ],
            },
            {
              name: 'interval',
              label: i18n('entities.job.form.interval', samplingMethod === 'Quantity' ? 'Parts' : 'Minutes'),
              classes: 'group-flex-3 required',
              value: job && job.interval ? job.interval.toString() : '0',
              blur: handleJobFieldChange,
              type: 'number',
            },
            {
              name: 'samples',
              label: i18n('entities.job.form.total'),
              classes: 'group-flex-3 required',
              value: job && job.samples ? job.samples.toString() : '0',
              blur: handleJobFieldChange,
              type: 'number',
            },
          ],
        },
      ],
    ],
  };
  return (
    <Drawer getContainer={parentSelector} visible={isOpen} closable={false} zIndex={1} width={360} mask={false} placement="right" className={`list-page-drawer  ${subState}`}>
      <form>
        <section className="drawer-actions">
          <Tooltip title="Delete Part">
            <Button
              data-cy="detail-delete"
              className="btn-primary btn-danger square-lg"
              onClick={(e: any) => {
                e.preventDefault();
                handleJobDelete(job, 'drawer icon');
              }}
            >
              <DeleteOutlined />
            </Button>
          </Tooltip>
          <Tooltip title="Close Drawer" placement="topLeft">
            <Button data-cy="drawer-close" className="btn-secondary square-lg" onClick={handleDrawerClose}>
              <CloseOutlined />
            </Button>
          </Tooltip>
        </section>
        {inputs.sections.map((section, index) => (
          <Section key={`form-section-${index}`} id={index} sections={section} />
        ))}
        <span>
          <div>
            <IxButton className="btn-primary" text="Save" onClick={handleSave} />
          </div>
        </span>
      </form>
    </Drawer>
  );
};

export default JobDetailsDrawer;
