import React from 'react';
import { Table } from 'antd';
import { i18n } from 'i18n';
import { ConfirmDelete } from 'view/global/confirmDelete';
import { SorterResult, TablePaginationConfig } from 'antd/lib/table/interface';
import { Job, JobSamplingEnum, JobStatusEnum, JobAccessControlEnum } from '../jobTypes';

interface Data {
  jobList: {
    rows: Job[];
  };
}

interface Props {
  data: Data | undefined;
  pagination: {} | undefined;
  handleJobDelete: (record: Job, source: string) => void;
  handleRowClick: (record: Job) => void;
  handleChange: (filter: any, sorter: any) => void;
  tableFilter: { job: Array<string>; jobStatus: Array<string> };
  tableSorter: { columnKey?: string | undefined; order?: 'descend' | 'ascend' | null | undefined };
}

interface Filters {
  text: JobSamplingEnum | JobStatusEnum | JobAccessControlEnum;
  value: JobSamplingEnum | JobStatusEnum | JobAccessControlEnum;
}

interface Jobs {
  text: string;
  value: string;
}

const JobsTable: React.FC<Props> = ({ data, pagination, tableFilter, tableSorter, handleJobDelete, handleRowClick, handleChange }) => {
  const pageConfig = pagination || {};
  const columns: any = [
    {
      width: 90,
      dataIndex: 'key',
      className: 'actions testing-job-actions-header',
      render: (text: string, record: Job) => (
        <span className="job-actions">
          <ConfirmDelete classes="icon-button job-delete" confirm={() => handleJobDelete(record, 'list icon')} />
        </span>
      ),
    },
    {
      title: i18n('entities.job.fields.name'),
      className: 'testing-job-name-header',
      dataIndex: 'name',
      sorter: (a: Job, b: Job) => (b.name && a.name ? b.name.localeCompare(a.name) : 1),
      sortOrder: tableSorter.columnKey === 'name' && tableSorter.order,
    },
    {
      title: i18n('entities.job.fields.part'),
      className: 'testing-part-number-header',
      dataIndex: 'part',
      key: 'part',
      render: (text: string, record: Job) => <span>{record.parts ? record.parts[0].name : ''}</span>,
    },
    {
      title: i18n('entities.job.fields.jobStatus'),
      className: 'testing-status-header',
      dataIndex: 'jobStatus',
      key: 'jobStatus',
    },
    {
      title: i18n('entities.job.fields.interval'),
      className: 'testing-interval-header',
      dataIndex: 'interval',
      key: 'interval',
      sorter: (a: Job, b: Job) => (a.interval && b.interval ? a.interval - b.interval : 0),
      sortOrder: tableSorter.columnKey === 'interval' ? tableSorter.order : null,
      sortDirections: ['ascend', 'descend'],
      onFilter: (value: string | number | boolean, record: Job) => (record.interval ? record.interval === value : false),
    },
    {
      title: i18n('entities.job.fields.itar'),
      className: 'testing-itar-header',
      dataIndex: 'accessControl',
      key: 'accessControl',
    },
    {
      title: i18n('entities.job.fields.updatedAt'),
      className: 'testing-edited-header',
      dataIndex: 'updatedAt',
      key: 'updatedAt',
      sorter: (a: Job, b: Job) => (a.updatedAt && b.updatedAt ? new Date(b.updatedAt).getTime() - new Date(a.updatedAt).getTime() : 0),
      sortOrder: tableSorter.columnKey === 'updatedAt' ? tableSorter.order : null,
      sortDirections: ['ascend', 'descend'],
    },
  ];

  const handleTableChange = (pagination: TablePaginationConfig, filters: Record<string, (string | number | boolean)[] | null>, sorter: SorterResult<Job> | SorterResult<Job>[]) => {
    handleChange(filters, { filteredInfo: filters || [], sortedInfo: sorter });
  };

  return (
    <div>
      {data && (
        <Table
          columns={columns}
          dataSource={data.jobList.rows}
          rowKey="id"
          onChange={handleTableChange}
          onRow={(record: Job) => {
            return {
              onClick: (e: any) => {
                handleRowClick(record);
              },
            };
          }}
          pagination={pageConfig}
        />
      )}
    </div>
  );
};

export default JobsTable;
