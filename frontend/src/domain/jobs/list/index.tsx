import React, { useEffect, useState, ChangeEvent } from 'react';
import { useLocation } from 'react-router-dom';
import { useQuery, useLazyQuery, useMutation } from '@apollo/client';
import { Jobs, JobList, JobCreate, JobFind, JobEdit } from 'domain/jobs/jobApi';
import { PlusSquareOutlined } from '@ant-design/icons';
import { i18n } from 'i18n';
import { Button, message } from 'antd';
import { Job } from '../jobTypes';
import JobDetailsDrawer from './JobDetailsDrawer';
import JobsTable from './JobsTable';
import Analytics from 'modules/shared/analytics/analytics';

const JobsList: React.FC = () => {
  const { state } = useLocation<{
    job?: Job;
    tablePage: number | 1;
    tableFilter: { job: Array<string>; jobStatus: Array<string> };
    tableSorter: {
      columnKey?: string | undefined;
      order?: 'descend' | 'ascend' | null | undefined;
    };
  }>();

  const [currentJob, setCurrentJob] = useState<Job | null>();

  // Track whether the details drawer is displayed or not
  const [displayDrawer, setDisplayDrawer] = useState(false);

  const [currentTablePage, setCurrentTablePage] = useState<number>(1);
  const [currentTableFilter, setCurrentTableFilter] = useState<{
    job: Array<string>;
    jobStatus: Array<string>;
  }>({ job: [], jobStatus: [] });
  const [currentTableSorter, setCurrentTableSorter] = useState<{
    columnKey?: string | undefined;
    order?: 'descend' | 'ascend' | null | undefined;
  }>({ columnKey: undefined, order: undefined });

  const [jobsListQueryParams, setJobsListQueryParams] = useState({
    filter: { jobStatus: ['Active', 'Inactive', 'Complete'] },
    orderBy: 'createdAt_DESC',
    limit: 10,
    offset: 0,
  });

  const [partsListQueryParams, setPartsListQueryParams] = useState({
    filter: { status: ['Active', 'Inactive'] },
    orderBy: 'createdAt_DESC',
    offset: 0,
  });

  const jobListResult = useQuery<JobList>(Jobs.query.list, {
    variables: jobsListQueryParams,
  });

  const { data, fetchMore } = jobListResult;

  const [findJob] = useLazyQuery<JobFind>(Jobs.query.find, {
    onCompleted: (jobFind: JobFind) => {
      const foundJob = { ...jobFind.jobFind };
      setCurrentJob(foundJob);
    },
    fetchPolicy: 'no-cache',
  });

  const [editJob] = useMutation<JobEdit>(Jobs.mutate.edit, {
    onCompleted: (returnData: any) => {
      if (returnData.jobUpdate && returnData.jobUpdate.jobStatus !== 'Deleted') {
        const newJob = { ...currentJob, ...returnData.jobUpdate };
        Analytics.track({
          event: Analytics.events.jobDeleted,
          properties: {
            job: newJob,
          },
        });
      } else {
        Analytics.track({
          event: Analytics.events.jobUpdated,
          properties: {
            job: returnData.jobUpdate,
          },
        });
      }
      setCurrentJob(undefined);
      jobListResult.refetch({ fetchPolicy: 'no-cache' });
    },
  });

  const [createJob] = useMutation<JobCreate>(Jobs.mutate.create, {
    onCompleted: (returnData: any) => {
      if (returnData.jobUpdate && returnData.jobUpdate.jobStatus !== 'Deleted') {
        const newJob = { ...currentJob, ...returnData.jobUpdate };
      }
      setCurrentJob(undefined);
      jobListResult.refetch();
      Analytics.track({
        event: Analytics.events.jobCreated,
        properties: {
          job: returnData.jobCreate,
        },
      });
    },
  });

  useEffect(() => {
    if (state && state.job && !currentJob) {
      setCurrentJob(state.job);
      setDisplayDrawer(true);
    }
  }, [currentJob?.id, state]);

  // sets pagination when landing on this page if you are redirected
  useEffect(() => {
    if (state && state.tablePage) {
      setCurrentTablePage(state.tablePage);
      handlePagination(state.tablePage);
    }
    if (state && state.tableFilter) {
      setCurrentTableFilter(state.tableFilter);
    }
    if (state && state.tableSorter) {
      setCurrentTableSorter(state.tableSorter);
    }
  }, []);

  // / Pagination of table records
  const handlePagination = (pageNumber: number) => {
    // set page number to send with report
    setCurrentTablePage(pageNumber);
    if (!data || !data.jobList.rows) {
      return;
    }

    if (pageNumber - 1 >= data.jobList.rows.length / 10) {
      fetchMore({
        variables: {
          offset: data.jobList.rows.length,
          limit: pageNumber * 10 - data.jobList.rows.length,
        },
        updateQuery: (prev, { fetchMoreResult }) => {
          if (!fetchMoreResult) return prev;

          return {
            ...prev,
            jobList: {
              count: prev.jobList.count,
              rows: [...prev.jobList.rows, ...fetchMoreResult.jobList.rows],
            },
          };
        },
      });
    }
  };

  const pagination = {
    total: data ? data.jobList.count : -1,
    current: currentTablePage,
    pageSize: 9,
    onChange: handlePagination,
  };

  const handleJobDelete = async (job: Job, source: string) => {
    setCurrentJob(undefined);
    const change = Jobs.createGraphqlInput({ ...job, jobStatus: 'Deleted' });
    editJob({
      variables: {
        id: job.id,
        data: change,
      },
    });
  };

  const handleRowClick = (job: Job) => {
    findJob({ variables: { id: job.id } });
    setDisplayDrawer(true);
  };

  // Handle close event (from inside or outside the drawer)
  const handleDrawerClose = () => {
    setDisplayDrawer(false);
    setCurrentJob(undefined);
  };

  const handleJobFieldChange = (event: ChangeEvent<HTMLTextAreaElement | HTMLInputElement>) => {
    let value: string[] | string = event.target.value;
    const newRecord: Job = { ...currentJob! };
    newRecord[event.target.name] = value;
    setCurrentJob(newRecord);
  };

  const handleNewJob = () => {
    setCurrentJob({
      id: '',
      name: '',
      sampling: 'Timed',
      partId: '',
      interval: 0,
      samples: 0,
      scrapped: 0,
      passing: 0,
      jobStatus: 'Inactive',
      accessControl: 'None',
      updatedAt: new Date().toString(),
    });
    setDisplayDrawer(true);
  };

  const handleSave = () => {
    if (!currentJob) return;

    if (!currentJob.parts || currentJob.parts.length === 0) {
      // TODO: improve error handling on job create/update form
      message.error('Select at least one part');
      return;
    }

    currentJob.samples = Number(currentJob.samples);
    currentJob.interval = Number(currentJob.interval);

    const change = Jobs.createGraphqlInput(currentJob);
    currentJob.id === ''
      ? createJob({
          variables: {
            data: change,
          },
        })
      : editJob({
          variables: {
            id: currentJob.id,
            data: change,
          },
        });
    jobListResult.refetch({ fetchPolicy: 'no-cache' });
    setCurrentJob(undefined);
  };

  const handleChange = (filter: any, sorter: any) => {
    setCurrentTableFilter(filter);
    const sortedInfo = { ...sorter.sortedInfo };
    setCurrentTableSorter({ columnKey: sortedInfo.columnKey, order: sortedInfo.order });
  };

  const handleJobFieldBlur = async (event: ChangeEvent<HTMLTextAreaElement | HTMLInputElement>) => {
    if (!currentJob) {
      return;
    }
    const change = { ...currentJob };
    change[event.target.name] = event.target.value;

    const changeInput = Jobs.createGraphqlInput(change);
    if (!changeInput || !data) {
      return;
    }

    editJob({
      variables: { id: currentJob!.id, data: changeInput },
      update(cache, { data: updated }) {
        // TODO should be in redux/middleware
        cache.writeQuery({
          query: Jobs.query.list,
          variables: jobsListQueryParams,
          data: {
            jobList: {
              count: data.jobList.count,
              rows: data.jobList.rows.map((job) => {
                if (job.id === currentJob!.id && updated) {
                  return updated.jobUpdate;
                }
                return job;
              }),
            },
          },
        });
      },
    });
  };

  return (
    <main id="jobsList" className="jobs-list">
      {currentJob && (
        <JobDetailsDrawer
          job={currentJob}
          currentTablePage={currentTablePage}
          currentTableFilter={currentTableFilter}
          currentTableSorter={currentTableSorter}
          parentSelector="main#jobsList"
          isOpen={displayDrawer}
          handleDrawerClose={handleDrawerClose}
          handleJobDelete={handleJobDelete}
          handleJobFieldChange={handleJobFieldChange}
          handleSave={handleSave}
        />
      )}
      <div className="wrapper">
        <h1>{i18n('entities.job.list.title')}</h1>
        <Button size="large" className="btn-primary" id="NewJob-btn" style={{ marginBottom: '15px' }} onClick={handleNewJob}>
          <PlusSquareOutlined style={{ marginRight: '5px' }} />
          {i18n('entities.job.new.title')}
        </Button>
        {data && <JobsTable data={data} pagination={pagination} tableFilter={currentTableFilter} tableSorter={currentTableSorter} handleChange={handleChange} handleJobDelete={handleJobDelete} handleRowClick={handleRowClick} />}
      </div>
    </main>
  );
};

export default JobsList;
