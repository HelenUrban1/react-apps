import { Part } from 'domain/part/partTypes';
import { Sample } from 'types/sample';

export type JobSamplingEnum = 'Timed' | 'Quantity';
export type JobStatusEnum = 'Inactive' | 'Active' | 'Completed' | 'Deleted';
export type JobAccessControlEnum = 'None' | 'ITAR';

export interface Job {
  id: string;
  name: string;
  parts?: Part[];
  jobSamples?: Sample[];
  sampling: JobSamplingEnum;
  interval: number;
  samples: number;
  passing: number;
  scrapped: number;
  jobStatus: JobStatusEnum;
  accessControl: JobAccessControlEnum;
  updatedAt: string;
  [key: string]: Date | number | string | string[] | Sampled[] | Part[] | JobAccessControlEnum | JobSamplingEnum | JobStatusEnum | undefined | null;
}

export interface JobInput {
  name: string;
  parts: Part[] | string[];
  jobStatus: JobStatusEnum;
  sampling: JobSamplingEnum;
  accessControl: string;
  interval: null | number;
  samples: null | number;
  passing: null | number;
  scrapped: null | number;
  updatedAt?: null | string;
}

export interface JobFilterInput {
  id?: string;
  parts?: Part[];
  jobStatus?: string;
  sampling?: string;
  interval?: number;
  samples?: number;
  jobStatus?: JobStatusEnum;
  createdAtRange?: Date[];
  [key: string]: string | undefined | JobStatusEnum | Date[] | Part[];
}
