import { Button } from 'antd';
import { i18n } from 'i18n';
import React from 'react';
import { useSelector } from 'react-redux';
import { AppState } from 'modules/state';
import { ReviewStatus } from './reviewTypes';
import { selectSettings } from 'modules/session/sessionSelectors';

interface InactiveProps {
  user: any;
  status?: ReviewStatus;
  setStatus: (status: ReviewStatus) => void;
}

export const Inactive = ({ user, status, setStatus }: InactiveProps) => {
  const settings = useSelector((state: AppState) => selectSettings(state));

  const handleSetStatus = (st: ReviewStatus) => {
    setStatus(st);
  };

  //Review use of this section in https://inspectionxpert.atlassian.net/browse/IXC-3079
  /* const { show: showNotification } = useBrowserNotifications({
    title: i18n('humanReview.notification.title'),
    body: i18n('humanReview.notification.body'),
    debug: true,
    enabled: true,
    onShow: () => {
      handleSetStatus('Reviewing');
    },
    onClick: () => {
      handleSetStatus('Reviewing');
    },
  });*/

  const { jwt } = useSelector((state: AppState) => state.auth);

  /*useEffect(() => {
    if (settings.autoballoonEnabled?.value === 'true' && config.wsUrl) {
      const socket = io(config.wsUrl, { transports: ['websocket'], withCredentials: true, auth: { token: `Bearer ${jwt}` }, path: config.wsPath });
      socket.on('reviewTask.addToQueue', showNotification);
      return () => {
        socket.off('reviewTask.addToQueue', showNotification);
        socket.disconnect();
      };
    }
    return () => {};
  }, [showNotification, status]);*/

  const getContent = () => {
    switch (status) {
      case 'Finished':
        return (
          <section className="human-review-content centered" data-cy="finished-container">
            <h2>{i18n('humanReview.finished')}</h2>
            <h3>{i18n('humanReview.checkLater')}</h3>
            <Button
              className="btn-primary btn-fixed inactive-review-btn"
              data-cy="check-review-btn"
              size="large"
              onClick={() => {
                handleSetStatus('Reviewing');
              }}
            >
              {i18n('humanReview.check')}
            </Button>
          </section>
        );
      case 'Welcome':
        return (
          <section className="human-review-content centered" data-cy="paused-container">
            <h1 className="review-header">{`${i18n('humanReview.welcome')}, ${user.fullName && user.fullName.length > 0 ? user.fullName : `${i18n('humanReview.reviewer')}`}`}</h1>
            <h3>{i18n('humanReview.getStarted')}</h3>
            <Button
              className="btn-primary btn-fixed inactive-review-btn"
              data-cy="start-review-btn"
              size="large"
              onClick={() => {
                handleSetStatus('Reviewing');
              }}
            >
              {i18n('humanReview.start')}
            </Button>
          </section>
        );
      default:
        // paused
        return (
          <section className="human-review-content centered" data-cy="paused-container">
            <h2>{i18n('humanReview.away')}</h2>
            <h3>{i18n('humanReview.ready')}</h3>
            <Button
              className="btn-primary btn-fixed inactive-review-btn"
              data-cy="resume-review-btn"
              size="large"
              onClick={() => {
                handleSetStatus('Reviewing');
              }}
            >
              {i18n('humanReview.resume')}
            </Button>
          </section>
        );
    }
  };

  return (
    <section className="human-review-content-container inactive-container" data-cy="inactive-container">
      {getContent()}
    </section>
  );
};

Inactive.defaultProps = {
  status: 'Start',
};
