import { getStatusEvent } from './humanReviewUtils';

describe('Utilities for human review', () => {
  it('Gets the new status event based on previous and new status', () => {
    let status = getStatusEvent('Welcome', 'Reviewing');
    expect(status).toBe('Starting Review');

    status = getStatusEvent('Reviewing', 'Paused');
    expect(status).toBe('Pausing Review');

    status = getStatusEvent('Reviewing', 'Welcome');
    expect(status).toBe('Stopped Review');

    status = getStatusEvent('Paused', 'Reviewing');
    expect(status).toBe('Resuming Review');
  });
});
