import gql from 'graphql-tag';
import log from 'modules/shared/logger';
import { ReviewTask, ReviewTaskInput } from './reviewTaskTypes';

// Type definitions for the returned api data
export interface ReviewTaskCreate {
  reviewTaskCreate: ReviewTask;
}

export interface ReviewTaskFind {
  reviewTaskFind: ReviewTask;
}

export interface ReviewTaskList {
  reviewTaskList: {
    count: number;
    rows: ReviewTask[];
  };
}

export interface ReviewTaskEdit {
  reviewTaskUpdate: ReviewTaskInput;
}

// Using the gql strings allows for use of Apollo Client React Hooks
// as well as the option of passing cache updates or query refetch requests to our mutations

export const ReviewTasks = {
  mutate: {
    edit: gql`
      mutation REVIEW_TASK_UPDATE($id: String!, $data: ReviewTaskInput!) {
        reviewTaskUpdate(id: $id, data: $data) {
          id
          type
          status
          metadata
          processingStart
          processingEnd
          accountId
          siteId
          partId
          drawingId
          reviewerId
          createdAt
          updatedAt
        }
      }
    `,
    delete: gql`
      mutation REVIEW_TASK_DESTROY($ids: [String]!) {
        reviewTaskDestroy(ids: $ids)
      }
    `,
    create: gql`
      mutation REVIEW_TASK_CREATE($data: ReviewTaskInput!) {
        reviewTaskCreate(data: $data) {
          id
          type
          status
          metadata
          processingStart
          processingEnd
          accountId
          siteId
          partId
          drawingId
          reviewerId
          createdAt
          updatedAt
        }
      }
    `,
    accept: gql`
      mutation REVIEW_TASK_ACCEPT($id: String!, $data: ReviewTaskInput!) {
        reviewTaskAcceptCapture(id: $id, data: $data)
      }
    `,
    correct: gql`
      mutation REVIEW_TASK_CORRECT($id: String!, $data: ReviewTaskInput!) {
        reviewTaskCorrectCapture(id: $id, data: $data)
      }
    `,
    reject: gql`
      mutation REVIEW_TASK_REJECT($id: String!, $data: ReviewTaskInput!) {
        reviewTaskRejectCapture(id: $id, data: $data)
      }
    `,
    return: gql`
      mutation REVIEW_TASK_RETURN($id: String!) {
        reviewTaskReturnToQueue(id: $id)
      }
    `,
  },
  query: {
    find: gql`
      query REVIEW_TASK_FIND($id: String!) {
        reviewTaskFind(id: $id) {
          id
          type
          status
          metadata
          processingStart
          processingEnd
          accountId
          siteId
          partId
          drawingId
          reviewerId
          createdAt
          updatedAt
        }
      }
    `,
    list: gql`
      query REVIEW_TASK_LIST($filter: ReviewTaskFilterInput, $orderBy: ReviewTaskOrderByEnum, $limit: Int, $offset: Int) {
        reviewTaskList(filter: $filter, orderBy: $orderBy, limit: $limit, offset: $offset) {
          count
          rows {
            id
            type
            status
            metadata
            processingStart
            processingEnd
            accountId
            siteId
            partId
            drawingId
            reviewerId
            createdAt
            updatedAt
          }
        }
      }
    `,
    pull: gql`
      query REVIEW_TASK_PULL {
        reviewTaskPullFromQueue {
          id
          type
          status
          metadata
          processingStart
          processingEnd
          accountId
          siteId
          partId
          drawingId
          reviewerId
          createdAt
          updatedAt
        }
      }
    `,
  },

  createGraphqlInput: (data: ReviewTask) => {
    if (!data) {
      log.error('No data was passed to create an input', data);
      return null;
    }

    const input: ReviewTaskInput = {
      status: data.status ? data.status : 'Waiting',
      type: data.type ? data.type : 'Capture',
      metadata: data.metadata,
      processingStart: data.processingStart,
      processingEnd: data.processingEnd,
      accountId: data.accountId,
      siteId: data.siteId,
      partId: data.partId,
      drawingId: data.drawingId,
      reviewerId: data.reviewerId,
    };
    return input;
  },
};
