import React, { useEffect, useRef, useState } from 'react';
import { Button, Input, Select, Tooltip } from 'antd';
import { DislikeOutlined, WarningOutlined, PauseCircleOutlined, LikeOutlined } from '@ant-design/icons';
import config from 'config';
import { i18n } from 'i18n';
import { GetAllCharacters } from 'styleguide/Modals/SpecialCharactersModal/character-list';
import { CharacterButton } from 'styleguide/Modals/SpecialCharactersModal/CharacterButton';
import { SpecialCharactersModal } from 'styleguide/Modals/SpecialCharactersModal';
import { Characteristic } from 'types/characteristics';
import { downloadFile } from 'utils/files';
import { SymbolButton } from 'styleguide/Buttons/SymbolButton';
import { ReviewTask } from './reviewTaskTypes';
import { parseValidJSONString } from '../../../utils/textOperations';

const { TextArea } = Input;
const symbols = GetAllCharacters();

interface reviewFormProps {
  currentCapture: ReviewTask | undefined;
  handleStopReview: () => void;
  handleSubmitCapture: (correction: string, confidence: string, issue: string) => void;
  handleRejectCapture: () => void;
}

export const ReviewForm = ({ currentCapture, handleStopReview, handleSubmitCapture, handleRejectCapture }: reviewFormProps) => {
  const metadata = currentCapture?.metadata ? parseValidJSONString(currentCapture.metadata) : null;
  const [correction, setCorrection] = useState<string>(metadata ? metadata.ocrText : '');
  const [cursorPos, setCursorPos] = useState<number>(correction.length);
  const [showModal, setShowModal] = useState(false);
  const [reason, setReason] = useState<string | undefined>();
  const [error, setError] = useState(false);
  const correctionInput = useRef<any>();
  const metadataRef = useRef<any>();

  const loadImage = async (characteristicId: string) => {
    if (characteristicId) {
      const imageCapture = document.getElementById('capture-image');
      imageCapture?.setAttribute('src', '');
      const imageData = await downloadFile(`${config.backendUrl}/download?privateUrl=${characteristicId}.png`);
      const reader = new window.FileReader();
      reader.readAsDataURL(imageData);
      reader.onload = () => {
        const imageDataUrl = reader.result as string;
        imageCapture?.setAttribute('src', imageDataUrl);
      };
    }
  };

  // auto focuses the correction input
  useEffect(() => {
    if (correctionInput.current) {
      correctionInput.current.resizableTextArea.textArea.focus();
      if (cursorPos) {
        correctionInput.current.resizableTextArea.textArea.selectionStart = cursorPos;
        correctionInput.current.resizableTextArea.textArea.selectionEnd = cursorPos;
      } else {
        correctionInput.current.resizableTextArea.textArea.selectionStart = correction.length;
        correctionInput.current.resizableTextArea.textArea.selectionEnd = correction.length;
      }
    }
  }, [correctionInput.current, correction]);

  // adjusts cursor position after insert
  useEffect(() => {
    if (correctionInput.current) {
      correctionInput.current.resizableTextArea.textArea.selectionStart = cursorPos;
      correctionInput.current.resizableTextArea.textArea.selectionEnd = cursorPos;
    }
  }, [cursorPos]);

  useEffect(() => {
    metadataRef.current = metadata;
  }, [metadata]);

  useEffect(() => {
    setCorrection(metadataRef.current ? metadataRef.current.ocrText : '');
    loadImage(metadataRef.current ? metadataRef.current.characteristicId : '');
  }, [currentCapture?.id]);

  // updates correction input and adjusts cursor position
  const handleChangeCorrection = (e: any) => {
    setCorrection(e.target.value);
    if (!correctionInput.current) return;
    const input = correctionInput.current.resizableTextArea.textArea as HTMLTextAreaElement;
    setCursorPos(input.selectionStart || input.selectionStart === 0 ? input.selectionStart : e.target.value.length);
  };

  // updates the cursor position after clicking in the input
  const updateCursor = () => {
    if (!correctionInput.current) return;
    const input = correctionInput.current.resizableTextArea.textArea as HTMLTextAreaElement;
    const ind = input.selectionStart;
    const indEnd = input.selectionEnd;
    if (ind !== indEnd || ind === null) return;
    setCursorPos(ind);
  };

  const submit = (confidence: 'High' | 'Low', issue = '') => {
    handleSubmitCapture(correction, confidence, confidence === 'Low' ? issue : '');
    setError(false);
    setReason(undefined);
  };

  const insertCharacter = (
    char: string,
    modalOut?: {
      target: {
        name: string;
        value: string;
        item: Characteristic;
      };
    },
  ) => {
    if (!correctionInput.current) return;
    const character = modalOut?.target.value || char;
    const input = correctionInput.current.resizableTextArea.textArea as HTMLTextAreaElement;
    const start = input && input.selectionStart !== null ? input.selectionStart : 0;
    const end = input && input.selectionEnd !== null ? input.selectionEnd : 0;
    const newVal = correction.slice(0, start) + character + correction.slice(end, Math.max(correction.length, 0));
    setCorrection(newVal);
    setCursorPos(start + 1);
    input.focus();
  };

  const handleModalInsert = (e: any) => {
    if (e.target.value) {
      setCorrection(e.target.value);
      setCursorPos(e.target.value.length);
    }
  };

  const subList = symbols.CommonCharacters.map((char) => (
    <CharacterButton
      key={char.char}
      char={char.char}
      onClick={() => {
        insertCharacter(char.char);
      }}
    />
  ));

  const handleSelectReason = (value: string) => {
    setError(false);
    setReason(value);
  };

  const handleHighConfidence = () => {
    submit('High');
  };

  const handleLowConfidence = () => {
    if (reason) {
      submit('Low');
    } else {
      setError(true);
    }
  };

  const handleCloseModal = () => {
    setShowModal(false);
  };

  const handleShowModal = (field: string, cursor: number) => {
    setCursorPos(cursor);
    setShowModal(true);
  };

  return (
    <section className="human-review-content-container reviewing-captures" data-cy="reviewing-captures">
      <section className="human-review-content" data-cy="review-content">
        <h2 className="rev-form-title" data-cy="rev-form-title">{`${i18n('humanReview.prompt')}`}</h2>
        <section className="current-capture" data-cy="current-capture-container">
          <img id="capture-image" className="capture-img" src="" alt="Loading..." />
        </section>
        <section className="correction-form">
          <section className="correction-input-section">
            <h4 className="label">{i18n('humanReview.capturedText')}</h4>
            <TextArea
              id="correction-input"
              ref={correctionInput}
              title="Captured Text"
              defaultValue={metadataRef.current ? metadataRef.current.ocrText : ''}
              value={correction}
              onChange={handleChangeCorrection}
              onClick={updateCursor}
              className="capture-correction-input"
              data-cy="capture-correction-input"
            />
          </section>
          <section className="common-characters form-section">
            <section className="characters-header">
              <h4 className="label">{i18n('humanReview.chars')}</h4>
              <SpecialCharactersModal
                field="correction"
                value={correction || ''}
                update={handleModalInsert}
                item={{} as Characteristic}
                cursorPos={cursorPos}
                label={i18n('entities.feature.fields.fullSpecification')}
                framed={false}
                show={showModal}
                source="review"
                handleCloseModal={handleCloseModal}
              />
              <SymbolButton field="correction" showModal={handleShowModal} btnLabel={i18n('humanReview.allChars')} btnClass="btn-secondary all-chars-btn" btnColor="#4c4c4c" />
            </section>
            <section className="character-bank">{subList}</section>
          </section>
          <section className="confidence-input form-section">
            <h4 className="label">{i18n('humanReview.reasonLabel')}</h4>
            <Select
              key={currentCapture?.id || ''}
              defaultValue={undefined}
              className={`reason-select${error && ' select-error'}`}
              placeholder="select reason"
              onChange={handleSelectReason}
              dropdownMatchSelectWidth={false}
              style={{ minWidth: '200px' }}
            >
              <Select.Option value="missing-character">{i18n('humanReview.select.character')}</Select.Option>
              <Select.Option value="format">{i18n('humanReview.select.format')}</Select.Option>
              <Select.Option value="other">{i18n('humanReview.select.other')}</Select.Option>
            </Select>
            {error && <span className="error-text">{i18n('humanReview.reasonHint')}</span>}
          </section>
          <section className="all-buttons form-section">
            <section className="review-btns">
              <Tooltip placement="top" title={i18n('humanReview.tips.reject')} mouseEnterDelay={0.5}>
                <Button className="btn-primary btn-danger" data-cy="bad-capture-btn" size="large" onClick={handleRejectCapture}>
                  <DislikeOutlined />
                  <div className="button-text">
                    <span>{i18n('humanReview.reject')}</span>
                    <span>{i18n('humanReview.bad')}</span>
                  </div>
                </Button>
              </Tooltip>
              <Tooltip placement="top" title={i18n('humanReview.tips.low')} mouseEnterDelay={0.5}>
                <Button className="btn-primary btn-low" data-cy="low-capture-btn" size="large" onClick={handleLowConfidence}>
                  <WarningOutlined />
                  <div className="button-text">
                    <span>{i18n('humanReview.submit')}</span>
                    <span>{i18n('humanReview.low')}</span>
                  </div>
                </Button>
              </Tooltip>
              <Tooltip placement="top" title={i18n('humanReview.tips.high')} mouseEnterDelay={0.5}>
                <Button className="btn-primary btn-high" data-cy="submit-review-btn" size="large" onClick={handleHighConfidence}>
                  <LikeOutlined />
                  <div className="button-text">
                    <span>{i18n('humanReview.submit')}</span>
                    <span>{i18n('humanReview.high')}</span>
                  </div>
                </Button>
              </Tooltip>
            </section>
            <Button className="btn-secondary" data-cy="stop-review-btn" size="large" onClick={handleStopReview}>
              <PauseCircleOutlined />
              {i18n('humanReview.pause')}
            </Button>
          </section>
        </section>
      </section>
    </section>
  );
};
