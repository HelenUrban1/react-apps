export type ReviewTaskTypeEnum = 'Capture' | 'Grid';
export type ReviewTaskStatusEnum = 'Waiting' | 'Processing' | 'Accepted' | 'Corrected' | 'Rejected';

// The default value of a ReviewTask when its first being created in the DB
export const newReviewTaskDefaults = {
  status: 'Waiting', // Waiting | Processing | Accepted | Corrected | Rejected
  type: 'Capture', // Capture | Grid
  metaData: '{ ocrText: "", correctedText: "", characteristicId: null }',
  processingStart: null,
  processingEnd: null,
  accountId: null,
  siteId: null,
  partId: null,
  drawingId: null,
  reviewerId: null,
};

// The schema of ReviewTask objects when returned from the DB
export interface ReviewTask {
  id: string;
  status: ReviewTaskStatusEnum;
  type: ReviewTaskTypeEnum;
  metadata: null | string;
  processingStart: null | Date;
  processingEnd: null | Date;
  accountId: string;
  siteId: string;
  partId: null | string;
  drawingId: null | string;
  reviewerId: null | string;
  createdAt: null | Date;
  updatedAt: null | Date;

  [key: string]: ReviewTaskTypeEnum | Date | string | number | null | undefined;
}

// This is the 'ReviewTaskInput' type which is expected by the backend's update method
// The backend's 'ReviewTaskInput' is at backend/src/api/reviewTask/types/reviewTaskInput.js
export interface ReviewTaskInput {
  type: ReviewTaskTypeEnum;
  status: ReviewTaskStatusEnum;
  metadata: null | string;
  processingStart: null | Date;
  processingEnd: null | Date;
  accountId: null | string;
  siteId: null | string;
  partId: null | string;
  drawingId: null | string;
  reviewerId: null | string;
  [key: string]: Date | number | string | string[] | ReviewTaskTypeEnum | undefined | null;
}

export interface ReviewTaskReturnVars {
  id: string;
}
export interface ReviewTaskReturn {
  reviewTaskReturn: boolean;
}

export interface ReviewTaskAcceptVars {
  id: string;
  data: ReviewTaskInput;
}

export interface ReviewTaskAccept {
  reviewTaskAccept: boolean;
}

export interface ReviewTaskCorrectVars {
  id: string;
  data: ReviewTaskInput;
}

export interface ReviewTaskCorrect {
  reviewTaskCorrect: boolean;
}

export interface ReviewTaskRejectVars {
  id: string;
  data: ReviewTaskInput;
}

export interface ReviewTaskReject {
  reviewTaskReject: boolean;
}
