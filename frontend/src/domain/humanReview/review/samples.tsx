import React from 'react';

export const badSamples = [
  { key: 'revCutoff', info: ['The text is cut off.'], image: <img className="capture-img" src="/images/examples/revCutoff.png" alt="capture" /> },
  { key: 'revGeo', info: ['This is drawing geometry, not a feature.'], image: <img className="capture-img" src="/images/examples/revGeo.png" alt="capture" /> },
  { key: 'revMult', info: ['Multiple features were captured.'], image: <img className="capture-img" src="/images/examples/revMult.png" alt="capture" /> },
];

export const goodSamples = [
  { key: 'revThru', text: '2X ⌀.12 THRU', info: ['Include all of the text in the capture.'], image: <img className="capture-img" src="/images/examples/revThru.png" alt="capture" /> },
  { key: 'revSplit', text: '85.52 +1.5/0', info: ['Place split tolerances on the same line separated by a forward slash.'], image: <img className="capture-img" src="/images/examples/revSplit.png" alt="capture" /> },
  {
    key: 'revGdt',
    text: '|⌖|⌀.01Ⓢ|A|B|C|',
    info: ['Geometric Tolerance', 'Use the pipe | character to separate the sections of a GDT.', 'Use the common characters to add special symbols.'],
    image: <img className="capture-img" src="/images/examples/revGdt.png" alt="capture" />,
  },
  {
    key: 'revNotes',
    text: 'SEE MICROFILM OR\nELECTRONIC IMAGES FOR\nPREVIOUS REVISIONS',
    info: ['Notes are plain text callouts on a drawing.', 'Place each line of text on a new line.'],
    image: <img className="capture-img" src="/images/examples/revNotes.png" alt="capture" />,
  },
  { key: 'revStacked', text: '3.39/3.36', info: ['Stacked Range', 'Place the range on the same line separated by a forward slash.'], image: <img className="capture-img" src="/images/examples/revStacked.png" alt="capture" /> },
];

export const samples = [
  { id: '1', text: '.325 + .020', image: <img className="capture-img" src="/images/examples/review1.png" alt="capture" /> },
  { id: '2', text: '|⊥|.010|A|B|', image: <img className="capture-img" src="/images/examples/review2.png" alt="capture" /> },
  { id: '3', text: '3.503/3.496', image: <img className="capture-img" src="/images/examples/review3.png" alt="capture" /> },
  { id: '4', text: '12ne 23bf9', image: <img className="capture-img" src="/images/examples/revGeo.png" alt="capture" /> },
];
