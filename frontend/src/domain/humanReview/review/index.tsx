import React, { useState, useEffect, useRef } from 'react';
import { useLazyQuery, useMutation } from '@apollo/client';
import Analytics from 'modules/shared/analytics/analytics';
import { Layout } from 'antd';
import { ReviewStatus } from '../reviewTypes';
import { ReviewTask, ReviewTaskReturn, ReviewTaskReturnVars, ReviewTaskAccept, ReviewTaskAcceptVars, ReviewTaskCorrect, ReviewTaskCorrectVars, ReviewTaskReject, ReviewTaskRejectVars } from './reviewTaskTypes';
import { ReviewForm } from './ReviewForm';
import { Info } from './Info';
import { ReviewTasks } from './reviewTaskApi';
import { parseValidJSONString } from '../../../utils/textOperations';

interface reviewProps {
  user: any;
  setStatus: (status: ReviewStatus) => void;
  eventTime: number;
  setEventTime: (time: number) => void;
}

const Review = ({ user, setStatus, eventTime, setEventTime }: reviewProps) => {
  const [currentCapture, setCurrentCapture] = useState<ReviewTask | undefined>();
  const currentCaptureRef = useRef<ReviewTask | undefined>();

  const [returnCapture] = useMutation<ReviewTaskReturn, ReviewTaskReturnVars>(ReviewTasks.mutate.return, {});
  const [acceptCapture] = useMutation<ReviewTaskAccept, ReviewTaskAcceptVars>(ReviewTasks.mutate.accept, {});
  const [correctCapture] = useMutation<ReviewTaskCorrect, ReviewTaskCorrectVars>(ReviewTasks.mutate.correct, {});
  const [rejectCapture] = useMutation<ReviewTaskReject, ReviewTaskRejectVars>(ReviewTasks.mutate.reject, {});
  const [pullReviewTask] = useLazyQuery(ReviewTasks.query.pull, {
    fetchPolicy: 'no-cache',
    onCompleted: (reviewTaskFind: ReviewTask) => {
      if (!currentCapture) {
        if (!reviewTaskFind || !(reviewTaskFind as any).reviewTaskPullFromQueue) {
          setCurrentCapture(undefined);
          setStatus('Finished');
        } else {
          setCurrentCapture((reviewTaskFind as any).reviewTaskPullFromQueue);
        }
      }
    },
  });

  const changeCapture = () => {
    if (!currentCapture) {
      pullReviewTask();
    }
  };

  // Load a capture if one hasn't been loaded and update the reference
  useEffect(() => {
    changeCapture();
    currentCaptureRef.current = currentCapture;
  }, [currentCapture]);

  // Return capture to queue on unload
  useEffect(() => {
    return () => {
      if (currentCaptureRef?.current) {
        returnCapture({
          variables: { id: currentCaptureRef.current.id },
        });
      }
    };
  }, []);

  const handleStopReview = () => {
    setStatus('Welcome');
  };

  const handleSubmitCapture = (correction: string, confidence?: string, issue?: string) => {
    if (currentCapture?.metadata) {
      const metadata = parseValidJSONString(currentCapture.metadata);
      if (metadata) {
        if (metadata.ocrText === correction) {
          // user confirmed the capture was correct
          metadata.confidence = confidence || '';
          metadata.issue = issue || '';
          const data = ReviewTasks.createGraphqlInput({ ...currentCapture, metadata: JSON.stringify(metadata), reviewerId: user.id });
          if (data) {
            acceptCapture({
              variables: { id: currentCapture.id, data },
            });

            Analytics.track({
              event: Analytics.events.captureConfirmed,
              reviewCycle: {
                event: Analytics.events.humanReviewEventName.captureConfirmed,
                elapsed: new Date().getMilliseconds() - eventTime,
                capture: currentCapture,
                correction,
                confidence: confidence || '',
                issue: issue || '',
                user,
              },
            });
          }
        } else {
          // user corrected the capture
          metadata.correctedText = correction;
          metadata.confidence = confidence || '';
          metadata.issue = issue || '';
          const data = ReviewTasks.createGraphqlInput({ ...currentCapture, metadata: JSON.stringify(metadata), reviewerId: user.id });
          if (data) {
            correctCapture({
              variables: { id: currentCapture.id, data },
            });

            Analytics.track({
              event: Analytics.events.captureCorrected,
              reviewCycle: {
                event: Analytics.events.humanReviewEventName.captureCorrected,
                elapsed: new Date().getMilliseconds() - eventTime,
                capture: currentCapture,
                correction,
                confidence: confidence || '',
                issue: issue || '',
                user,
              },
            });
          }
        }
      }
    }

    setEventTime(new Date().getMilliseconds());
    setCurrentCapture(undefined);
  };

  const handleRejectCapture = () => {
    // user confirmed the capture was invalid
    if (currentCapture) {
      const data = ReviewTasks.createGraphqlInput({ ...currentCapture, reviewerId: user.id });
      if (data) {
        rejectCapture({
          variables: { id: currentCapture.id, data },
        });

        Analytics.track({
          event: Analytics.events.captureRejected,
          reviewCycle: {
            event: Analytics.events.humanReviewEventName.captureRejected,
            elapsed: new Date().getMilliseconds() - eventTime,
            capture: currentCapture,
            correction: '',
            user,
          },
        });
      }
    }

    setEventTime(new Date().getMilliseconds());
    setCurrentCapture(undefined);
  };

  return (
    <Layout>
      <ReviewForm currentCapture={currentCapture} handleStopReview={handleStopReview} handleSubmitCapture={handleSubmitCapture} handleRejectCapture={handleRejectCapture} />
      <Info />
    </Layout>
  );
};

export default Review;
