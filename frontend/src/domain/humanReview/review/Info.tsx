import { Layout, Carousel, Input } from 'antd';
import { LeftCircleOutlined, RightCircleOutlined } from '@ant-design/icons';
import { i18n } from 'i18n';
import React, { useState } from 'react';
import PanelHeader from 'styleguide/PanelHeader/PanelHeader';
import { badSamples, goodSamples } from './samples';

const { Sider } = Layout;

export const Info = () => {
  const [good, setGood] = useState(0);
  const [bad, setBad] = useState(0);

  const handleSetIndex = (ind: number, set: 'Good' | 'Bad' | 'Samp') => {
    switch (set) {
      case 'Good':
        setGood(ind);
        break;
      case 'Bad':
        setBad(ind);
        break;
      default:
        break;
    }
  };
  return (
    <Sider collapsible={false} className="human-review-drawer" width={360} theme="light" style={{ padding: 20 }}>
      <PanelHeader header={i18n('humanReview.info.examples')} copy={i18n('humanReview.info.hint')} dataCy="human-review-header" />
      <section className="review-help" data-cy="review-help">
        <section className="capture-help">
          <h3>{i18n('humanReview.info.accepted')}</h3>
          <Carousel
            dots
            arrows
            prevArrow={<LeftCircleOutlined style={{ color: 'black' }} />}
            nextArrow={<RightCircleOutlined style={{ color: 'black' }} />}
            dotPosition="bottom"
            afterChange={(current) => {
              handleSetIndex(current, 'Good');
            }}
          >
            {goodSamples.map((sample, index) => {
              return (
                <div key={sample.key} className="carousel-example" data-cy={`accepted-sample-image-${index}`}>
                  {sample.image}
                </div>
              );
            })}
          </Carousel>
          <Input.TextArea defaultValue={goodSamples[good].text} key={goodSamples[good].text} data-cy="accepted-sample-text" autoSize disabled />
          <ul className="info-list" data-cy="accepted-sample-info">
            {goodSamples[good].info.map((text) => (
              <li key={text}>{text}</li>
            ))}
          </ul>
          <hr className="form-split" />
          <h3>{i18n('humanReview.info.rejected')}</h3>
          <Carousel
            dots
            arrows
            prevArrow={<LeftCircleOutlined style={{ color: 'black' }} />}
            nextArrow={<RightCircleOutlined style={{ color: 'black' }} />}
            dotPosition="bottom"
            afterChange={(current) => {
              handleSetIndex(current, 'Bad');
            }}
          >
            {badSamples.map((sample, index) => {
              return (
                <div key={sample.key} className="carousel-example" data-cy={`rejected-sample-image-${index}`}>
                  {sample.image}
                </div>
              );
            })}
          </Carousel>
          <ul className="info-list">
            {badSamples[bad].info.map((text) => (
              <li key={text}>{text}</li>
            ))}
          </ul>
        </section>
      </section>
    </Sider>
  );
};
