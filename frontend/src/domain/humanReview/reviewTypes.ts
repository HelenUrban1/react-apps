export type ReviewStatus = 'Welcome' | 'Paused' | 'Reviewing' | 'Finished';

export type Capture = { id: string; text: string; image: any };
