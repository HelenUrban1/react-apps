import { ReviewStatus } from './reviewTypes';

// split out to reduce complexity
const newStatusEventFromReviewing = (newStatus: ReviewStatus) => {
  if (newStatus === 'Paused') {
    return 'Pausing Review';
  }
  if (newStatus === 'Welcome') {
    return 'Stopped Review';
  }
  return 'Finished Review';
};

// Gets the Human Review event name based on the transition from a currentStatus to a newStatus
export const getStatusEvent = (currentStatus: ReviewStatus, newStatus: ReviewStatus) => {
  let statusEvent = '';

  if (currentStatus === 'Reviewing') {
    statusEvent = newStatusEventFromReviewing(newStatus);
  } else if (newStatus === 'Reviewing') {
    if (currentStatus === 'Welcome') {
      statusEvent = 'Starting Review';
    } else if (currentStatus === 'Paused') {
      statusEvent = 'Resuming Review';
    } else if (currentStatus === 'Finished') {
      statusEvent = 'Refreshing Review';
    }
  }

  return statusEvent;
};
