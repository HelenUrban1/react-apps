import React, { useRef, useState } from 'react';
import Analytics from 'modules/shared/analytics/analytics';
import { useSelector } from 'react-redux';
import authSelectors from 'modules/auth/authSelectors';
import createActivityDetector from 'activity-detector';
import { Inactive } from './Inactive';
import Review from './review';
import { ReviewStatus } from './reviewTypes';
import { getStatusEvent } from './humanReviewUtils';
import { AppState } from 'modules/state';

import './review.less';

const HumanReview = () => {
  const [status, setStatus] = useState<ReviewStatus>('Welcome');
  const [eventTime, setEventTime] = useState<number>(new Date().getMilliseconds());
  const activityDetector = useRef<any>();

  const user = useSelector((state: AppState) => authSelectors.selectCurrentUser(state));

  const handleSetStatus = (newStatus: ReviewStatus) => {
    const statusEvent = getStatusEvent(status, newStatus);

    if (statusEvent !== '') {
      Analytics.track({
        event: Analytics.events.changeHumanReviewStatus,
        reviewCycle: {
          event: statusEvent,
          elapsed: new Date().getMilliseconds() - eventTime,
          capture: '',
          correction: '',
          user,
        },
      });
      setEventTime(new Date().getMilliseconds());
      setStatus(newStatus);
    }
  };

  if (!activityDetector.current) {
    activityDetector.current = createActivityDetector({
      timeToIdle: 10000,
      inactivityEvents: [],
    });

    activityDetector.current.on('idle', () => {
      handleSetStatus('Paused');
    });
  } else {
    // clears out old event handlers
    activityDetector.current.stop();
    activityDetector.current.on('idle', () => {
      handleSetStatus('Paused');
    });
    activityDetector.current.init();
  }

  const stopActivityDetector = () => {
    if (activityDetector.current) {
      activityDetector.current.stop();
    }
  };

  return (
    <main id="humanReview" className="human-review">
      {status === 'Reviewing' ? <Review user={user} setStatus={handleSetStatus} eventTime={eventTime} setEventTime={setEventTime} /> : <Inactive user={user} status={status} setStatus={handleSetStatus} />}
      <button data-cy="stop-activity-detector-btn" type="button" hidden onClick={stopActivityDetector}>
        hidden button for cypress to turn off activity detector
      </button>
    </main>
  );
};

export default HumanReview;
