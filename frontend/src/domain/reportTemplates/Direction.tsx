import React from 'react';
import { i18n } from 'i18n';
import { Radio, Popconfirm, Tooltip } from 'antd';
import { useDispatch, useSelector } from 'react-redux';
import { ArrowDownOutlined, ArrowRightOutlined, QuestionCircleOutlined } from '@ant-design/icons';
import { AppState } from 'modules/state';
import { doSaveTemplate } from 'modules/reportTemplate/reportTemplateActions';
import { ReportTemplateAPI } from 'graphql/reportTemplate';

export const TemplateDirection = () => {
  const { template } = useSelector((state: AppState) => state.reportTemplates);
  const dispatch = useDispatch();
  if (!template) {
    return <></>;
  }

  const setDirectionHorizontal = () => {
    const templateInput = ReportTemplateAPI.createGraphqlInput({ ...template, direction: 'Horizontal' });
    dispatch(doSaveTemplate({ id: template.id, template: templateInput }));
  };

  const setDirectionVertical = () => {
    const templateInput = ReportTemplateAPI.createGraphqlInput({ ...template, direction: 'Vertical' });
    dispatch(doSaveTemplate({ id: template.id, template: templateInput }));
  };

  return (
    <section className="section direction-section">
      {i18n('entities.template.direction.title')}
      <Tooltip title={i18n('entities.template.direction.hint')}>
        <QuestionCircleOutlined className="direction-icon" />
      </Tooltip>
      <section className="direction-radios">
        {template?.direction === 'Vertical' && (
          <Radio.Group name="directionRadio" defaultValue={template?.direction} value={template?.direction}>
            <Radio value="Vertical">
              {i18n('entities.template.direction.vertical')}
              <ArrowDownOutlined className="direction-icon" />
            </Radio>
            <Popconfirm title={i18n('entities.template.direction.confirmChange')} onConfirm={setDirectionHorizontal}>
              <Radio value="Horizontal">
                {i18n('entities.template.direction.horizontal')}
                <ArrowRightOutlined className="direction-icon" />
              </Radio>
            </Popconfirm>
          </Radio.Group>
        )}

        {template?.direction === 'Horizontal' && (
          <Radio.Group name="directionRadio" defaultValue={template?.direction} value={template?.direction}>
            <Popconfirm title={i18n('entities.template.direction.confirmChange')} onConfirm={setDirectionVertical}>
              <Radio value="Vertical">
                {i18n('entities.template.direction.vertical')}
                <ArrowDownOutlined />
              </Radio>
            </Popconfirm>
            <Radio value="Horizontal">
              {i18n('entities.template.direction.horizontal')}
              <ArrowRightOutlined />
            </Radio>
          </Radio.Group>
        )}
      </section>
    </section>
  );
};
