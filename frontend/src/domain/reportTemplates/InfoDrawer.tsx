import React, { useCallback, useEffect, useRef, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router';
import { useLazyQuery, useMutation } from '@apollo/client';
import Sider from 'antd/lib/layout/Sider';
import { Tooltip } from 'antd';
import { BaseOptionType } from 'antd/lib/cascader';
import { cloneDeep } from 'lodash';
import { i18n } from 'i18n';
import Permissions from 'security/permissions';
import OrganizationAPI, { OrganizationCreate, OrganizationCreateVars } from 'graphql/organization';
import { ReportTemplateActions, doLoadReportTemplate, doSaveReport, doSaveTemplate, doGetTemplate } from 'modules/reportTemplate/reportTemplateActions';
import log from 'modules/shared/logger';
import { AppState } from 'modules/state';
import { doGetPart } from 'modules/part/partActions';
import PermissionChecker from 'modules/auth/permissionChecker';
import PanelHeader from 'styleguide/PanelHeader/PanelHeader';
import { ProgressFooter } from 'styleguide/ProgressionFooter';
import { Providers } from 'styleguide/ReportTemplates/Providers/Providers';
import { Select } from 'view/global/inputs';
import { PartListShallow, Parts } from 'domain/part/partApi';
import { ReportAPI } from 'graphql/report';
import { ReportTemplateAPI } from 'graphql/reportTemplate';
import { ReportTemplate, FilterObject } from 'types/reportTemplates';
import DataFilter from 'styleguide/ReportTemplates/DataFilter/DataFilter';
import { TemplateDirection } from './Direction';
import { TokenSection } from './Tree';
import './reportTemplateStyles.less';

interface OptionArray {
  name: string;
  label: string;
  value: string;
}

type Props = {
  handleGenerateReport: (temp?: ReportTemplate, overwrite?: string, filters?: FilterObject) => void;
  handleCloneTemplate: (temp?: ReportTemplate | undefined) => void;
};

const ReportTemplatesInfoDrawer: React.FC<Props> = ({ handleGenerateReport, handleCloneTemplate }) => {
  const { part, revisions } = useSelector((state: AppState) => state.part);
  const { organizations, filterList } = useSelector((state: AppState) => state.session);
  const { id, data, step, max, template, report, mode, filters, saved, source } = useSelector((state: AppState) => state.reportTemplates);
  const dispatch = useDispatch();
  const history = useHistory();
  const { currentUser } = useSelector((state: AppState) => state.auth);
  const showFilters = !!template;

  const [templatePart, setPart] = useState<string | undefined>(part?.id);
  const [customer, setCustomer] = useState<string | undefined>();
  const [partOptions, setPartOptions] = useState<OptionArray[]>([]);
  const [orgOptions, setOrgOptions] = useState<OptionArray[]>([]);

  const nextText = useRef(template?.provider?.id !== 'none' ? i18n('common.edit') : i18n('common.copy'));
  const hasTemplateEdit = useRef<boolean>(false);
  const hasReportEdit = useRef<boolean>(false);
  const used = useRef<(string | undefined)[]>(
    Object.values(filters)
      .filter((a) => a.category)
      .map((a) => (a.category ? a.category[1] : undefined)),
  );
  const options = useRef<BaseOptionType[]>();

  useEffect(() => {
    if (showFilters && !options.current && part?.characteristics && part.characteristics.length > 0) {
      let drawing = false;
      let page = false;
      let grid = false;
      let balloon = false;
      let point = false;
      let operation = false;
      let classification = false;
      let method = false;
      let number = false;
      let type = false;
      let subtype = false;

      for (let index = 0; index < part.characteristics.length; index++) {
        if (page && grid && balloon && point && operation && classification && method && number && type && subtype) {
          // No need to keep looping if everything is true
          break;
        }
        const e = part.characteristics[index];
        const eDraw = part.drawings?.find((draw) => draw.id === e.drawing.id) || part.primaryDrawing;
        const useGrid = JSON.parse(eDraw.gridOptions || '{}').status !== false;

        if (!type && e.notationType && e.notationType !== '') {
          type = true;
        }
        if (!subtype && e.notationSubtype && e.notationSubtype !== '') {
          subtype = true;
        }
        if (!number && e.markerLabel && e.markerLabel !== '') {
          number = true;
        }
        if (!operation && e.operation && e.operation !== '') {
          operation = true;
        }
        if (!classification && e.criticality && e.criticality !== '') {
          classification = true;
        }
        if (!method && e.inspectionMethod && e.inspectionMethod !== '') {
          method = true;
        }
        if (!drawing && e.drawing?.id !== undefined && e.drawing?.id !== null) {
          drawing = true;
        }
        if (!page && e.drawingSheetIndex !== undefined && e.drawingSheetIndex !== null) {
          page = true;
        }
        if (!grid && useGrid && e.gridCoordinates !== undefined && e.gridCoordinates !== null) {
          grid = true;
        }
        if (!balloon && useGrid && e.balloonGridCoordinates !== undefined && e.balloonGridCoordinates !== null) {
          balloon = true;
        }
        if (!point && useGrid && e.connectionPointGridCoordinates !== undefined && e.connectionPointGridCoordinates !== null) {
          point = true;
        }
        if (page && grid && balloon && point && operation && classification && method && number && type && subtype) {
          break;
        }
      }
      options.current = [
        {
          value: 'Features',
          label: 'Features',
          children: [
            {
              value: 'Notation Type',
              label: 'Type',
              disabled: !type || used.current.includes('Notation Type'),
            },
            {
              value: 'Notation Subtype',
              label: 'SubType',
              disabled: !subtype || used.current.includes('Notation Subtype'),
            },
            {
              value: 'Marker Label',
              label: 'Number',
              disabled: !number || used.current.includes('Marker Label'),
            },
          ],
        },
        {
          value: 'Inspection',
          label: 'Inspection',
          children: [
            {
              value: 'Inspection Method',
              label: 'Method',
              disabled: !method || used.current.includes('Inspection Method'),
            },
            {
              value: 'Criticality',
              label: 'Classification',
              disabled: !classification || used.current.includes('Criticality'),
            },
            {
              value: 'Operation',
              label: 'Operation',
              disabled: !operation || used.current.includes('Operation'),
            },
          ],
        },
        {
          value: 'Location',
          label: 'Location',
          children: [
            {
              value: 'Drawing',
              label: 'Drawing',
              disabled: !drawing || used.current.includes('Drawing'),
            },
            {
              value: 'Drawing Sheet Index',
              label: 'Page',
              disabled: !page || used.current.includes('Drawing Sheet Index'),
            },
          ],
        },
      ];
      if (balloon) {
        options.current[options.current.length - 1].children?.push({
          value: 'Balloon Grid Coordinates',
          label: 'Balloon',
          disabled: !balloon || used.current.includes('Balloon Grid Coordinates'),
        });
      }
      if (grid) {
        options.current[options.current.length - 1].children?.push({
          value: 'Grid Coordinates',
          label: 'Grid',
          disabled: !grid || used.current.includes('Grid Coordinates'),
        });
      }
      if (point) {
        options.current[options.current.length - 1].children?.push({
          value: 'Connection Point Grid Coordinates',
          label: 'Feature',
          disabled: !point || used.current.includes('Connection Point Grid Coordinates'),
        });
      }
    }
    if (mode === 'template' && part && part.id !== templatePart) setPart(part.id);
  }, [showFilters, part?.characteristics]);

  const [getList, { loading }] = useLazyQuery<PartListShallow>(Parts.query.listShallow, {
    variables: { filter: { status: ['Active'] }, orderBy: 'updatedAt_DESC' },
    fetchPolicy: 'cache-and-network',
    onCompleted: ({ partListShallow }) => {
      if (partListShallow.rows) {
        const partOpts = partListShallow.rows
          .filter((p) => !!p.number)
          .filter((p) => p.characteristics)
          .map((p) => {
            let name = p.number || '';
            if (p.name && p.revision) {
              name += ` (${p.name} - ${p.revision})`;
            } else if (p.name) {
              name += ` (${p.name})`;
            } else if (p.revision) {
              name += ` (${p.revision})`;
            }
            return {
              name: p.id,
              label: name,
              value: p.id,
            };
          });
        setPartOptions(partOpts);
      }
    },
  });

  useEffect(() => {
    if (partOptions.length <= 0) {
      if (history.location.pathname.includes('templates')) {
        // in templates - part can be changed from the dropdown, get full list
        getList();
      } else if (part) {
        // in report - part select is disabled, only need the current part
        setPartOptions([
          {
            value: part.id,
            label: part.number || '',
            name: part.id,
          },
        ]);
      }
    }
  }, [part, revisions]);

  useEffect(() => {
    if (organizations && orgOptions.length !== organizations.all.length) {
      const newOrgs: OptionArray[] = [];
      organizations.all.forEach((organization) => {
        newOrgs.push({
          name: organization.id,
          label: organization.name,
          value: organization.id,
        });
      });
      setOrgOptions(newOrgs);
    }
  }, [organizations]);

  useEffect(() => {
    let newProvider;
    if (mode === 'report') {
      if (report?.customer?.id) {
        newProvider = report?.customer?.id;
      } else if (part?.customer?.id) {
        newProvider = part?.customer?.id;
      }
    } else {
      newProvider = template?.provider?.id;
    }
    if (newProvider === 'none') {
      newProvider = template?.provider?.name;
    }

    setCustomer(newProvider);
    if (!nextText.current || (nextText.current === i18n('common.copy') && template?.provider?.id !== 'none')) {
      nextText.current = template?.provider?.id !== 'none' ? i18n('common.edit') : i18n('common.copy');
    }
  }, [mode, report?.customer?.id, part?.customer?.id, template?.provider?.id]);

  useEffect(() => {
    if (currentUser) {
      const permissionValidator = new PermissionChecker(currentUser);
      hasTemplateEdit.current = permissionValidator.match(Permissions.values.reportTemplateEdit);
      hasReportEdit.current = permissionValidator.match(Permissions.values.reportEdit);
    }
  }, [currentUser]);

  const progressToEdit = () => {
    if (!template) {
      // must have a template in order to edit
      return null;
    }
    // progress forward
    dispatch({ type: ReportTemplateActions.SET_STEP, payload: step + 1 });
    if (mode === 'report' && (!report || report?.template?.id !== template?.id)) {
      // generate or regenerate report data
      return handleGenerateReport(template, report?.id);
    }
    if (!data) {
      if (mode === 'report') {
        // get report from DB
        return dispatch(doLoadReportTemplate({ url: report?.file[0]?.publicUrl || '', settings: template.settings }));
      }
      if (template?.id && template.id.substr(0, 8) === 'default_') {
        // only clone default templates
        return handleCloneTemplate(template);
      }
      // get template from the DB
      dispatch(doGetTemplate(template.id));
    }
    if (mode === 'template' && history.location.pathname !== `/templates/${template.id}`) {
      // set the URL ID after loading, if it isn't already
      history.push(`/templates/${template.id}`);
    }

    return null;
  };

  const saveAndExit = () => {
    if (mode === 'report') {
      if (report) {
        // Save report and exit to home
        const reportInput = ReportAPI.createGraphqlInput(report);
        return dispatch(doSaveReport({ id: report.id, report: reportInput, push: `/parts` }));
      }
    } else if (template) {
      // save template
      const templateInput = ReportTemplateAPI.createGraphqlInput(template);
      dispatch(doSaveTemplate({ id: template.id, template: templateInput }));
      if (source === 'report') {
        // return to report creation using the new template
        dispatch({ type: ReportTemplateActions.LOADING, payload: true });
        dispatch({ type: ReportTemplateActions.SET_NEW_REPORT });
        handleGenerateReport(template, report?.id);
        return history.push(`/parts/${report?.part?.id || part?.id}/reports/${report?.id || ''}`);
      }
      // return to the templates page
      history.push(`/templates`, { step: 0, max: 1 });
      return dispatch({ type: ReportTemplateActions.CLEAR });
    }
    // return to home
    history.push(`/`);
    return dispatch({ type: ReportTemplateActions.CLEAR });
  };

  const back = () => {
    if (mode === 'template' && source === 'report') {
      // return to report creation
      dispatch({ type: ReportTemplateActions.SET_MODE, payload: 'report' });
      dispatch({ type: ReportTemplateActions.SET_DATA, payload: null });
      history.push(`/parts/${part?.id}/reports/${report?.id || ''}`, { step: Parts.step.order.length - 1, max: Parts.step.order.length });
    } else if (step === max) {
      // step backward
      dispatch({ type: ReportTemplateActions.SET_STEP, payload: step - 1 });
      if (mode === 'template') {
        history.push('/templates/');
      }
    } else if (mode === 'report') {
      // return to the part wizard
      history.push(`/parts/${part?.id || ''}`);
    } else {
      // return to home
      history.push('/');
    }
    if (step !== max) {
      dispatch({ type: ReportTemplateActions.CLEAR });
    }
  };

  const handleChangeProvider = useCallback(
    (event: React.ChangeEvent<HTMLTextAreaElement | HTMLSelectElement>) => {
      if (!event.target.value) {
        return;
      }
      if (mode === 'report') {
        if (report?.id) {
          const reportInput = ReportAPI.createGraphqlInput(report);
          reportInput.customer = event.target.value;
          dispatch(doSaveReport({ id: report.id, report: reportInput }));
        } else {
          dispatch({ type: ReportTemplateActions.SET_REPORT, payload: { customer: { id: event.target.value } } });
        }
      }
      if (mode === 'template') {
        if (template?.id) {
          const templateInput = ReportTemplateAPI.createGraphqlInput(template);
          templateInput.provider = event.target.value;
          dispatch(doSaveTemplate({ id: template.id, template: templateInput }));
        } else {
          dispatch({ type: ReportTemplateActions.SET_TEMPLATE, payload: { customer: { id: event.target.value } } });
        }
      }
    },
    [mode, report, template],
  );

  // Function to create the new organization
  const [createOrganization] = useMutation<OrganizationCreate, OrganizationCreateVars>(OrganizationAPI.mutate.create, {
    onCompleted({ organizationCreate }) {
      if (organizationCreate) {
        dispatch({ type: 'SESSION_UPDATE_ORGANIZATIONS', payload: { all: [...organizations.all, organizationCreate] } });
        const change: any = { target: { value: organizationCreate.id } };
        handleChangeProvider(change);
      }
    },
  });

  // Method to coordinate the creation of a new organization record from inside the modal
  const handleNewOrganizationSubmit = async (newOrganizationName: string) => {
    createOrganization({
      variables: {
        data: {
          name: newOrganizationName,
          status: 'Active',
          type: 'Customer',
        },
      },
    });
  };

  const handleChangePartNumber = (event: React.ChangeEvent<HTMLTextAreaElement | HTMLSelectElement>) => {
    if (!event.target.value) {
      return;
    }
    if (mode === 'report') {
      log.warn('Reports Should not be able to change loaded part');
      dispatch({ type: ReportTemplateActions.ERROR, payload: { message: 'Reports should not change Part' } }); // TODO: better error message
    }
    if (mode === 'template') {
      setPart(event.target.value);
      if (event.target.value !== part?.id) {
        dispatch(doGetPart(event.target.value));
      }
    }
  };

  const toggleOptions = (val: string[], disable: boolean) => {
    const newOptions = cloneDeep(options.current);
    if (newOptions) {
      for (let a = 0; a < newOptions.length; a++) {
        const cat = newOptions[a];
        if (cat.value === val[0] && cat.children) {
          for (let b = 0; b < cat.children.length; b++) {
            const level = cat.children[b];
            if (level.value === val[1]) {
              level.disabled = disable;
              break;
            }
          }
          break;
        }
      }
    }
    return newOptions;
  };

  const onChange = useCallback(
    (val: string[], key: string) => {
      if (filters[key].category) {
        // Remove old categroy
        const newOptions = toggleOptions(filters[key].category!, false);
        options.current = newOptions;
        if (used.current.includes(filters[key].category![1])) {
          used.current = used.current.filter((a) => a !== filters[key].category![1]);
        }
      }
      const payload = { ...filters, [key]: { ...filters[key], category: val, value: undefined } };
      dispatch({ type: ReportTemplateActions.SET_FILTERS, payload });
      // set new category as used
      const newOptions = toggleOptions(val, true);
      if (val !== undefined && !used.current.includes(val[1])) {
        used.current.push(val[1]);
      }
      options.current = newOptions;
    },
    [report, filters],
  );

  const setFilter = useCallback(
    (val: any, key: string) => {
      const oldValue = filters[key].value;
      const payload = { ...filters, [key]: { ...filters[key], value: val } };
      dispatch({ type: ReportTemplateActions.SET_FILTERS, payload });
      if (val !== oldValue) {
        handleGenerateReport(report?.template, report?.id, payload);
      }
    },
    [report, filters, part],
  );

  const addFilter = useCallback(() => {
    const keys = Object.keys(filters);
    const newKey = parseInt(keys[keys.length - 1], 10) + 1;
    const payload = { ...filters, [newKey]: { category: undefined, value: undefined } };
    dispatch({ type: ReportTemplateActions.SET_FILTERS, payload });
  }, [report, filters]);

  const removeFilter = useCallback(
    (key: string) => {
      const payload = { ...filters };
      const regenerate = !!filters[key].value;
      if (filters[key].category) {
        const newOptions = toggleOptions(filters[key].category!, false);
        options.current = newOptions;
        if (used.current.includes(filters[key].category![1])) {
          used.current = used.current.filter((a) => a !== filters[key].category![1]);
        }
      }
      delete payload[key];
      if (Object.keys(payload).length === 0) {
        payload['1'] = { category: undefined, value: undefined };
      }
      dispatch({ type: ReportTemplateActions.SET_FILTERS, payload });
      if (regenerate) {
        handleGenerateReport(report?.template, report?.id, payload);
      }
    },
    [report, filters, part],
  );

  return (
    <Sider //
      collapsible={false}
      collapsed={false}
      width="350px"
      className="wizard-drawer"
    >
      <div className="drawer-container">
        <div className="drawer-header">
          <PanelHeader //
            header={`${step + 1}. ${i18n(`reportTemplates.title.${mode}.${step < max}`)}`}
            copy={i18n(`reportTemplates.description.${mode}.${step < max}`)}
            dataCy={`${mode}_header`}
          />
        </div>
        <div className="drawer-contents">
          {mode === 'report' && hasReportEdit.current && (
            <>
              <div className="part-select" data-cy="report-part">
                <Select
                  loading={loading}
                  input={{
                    name: 'part',
                    label: i18n('entities.part.fields.number'),
                    value: report?.part?.number || part?.number || null,
                    type: 'select',
                    group: partOptions,
                    change: handleChangePartNumber,
                    disabled: true,
                    search: true,
                  }}
                />
              </div>
              <div className="customer-select" data-cy="report-customers">
                <Providers name="customer" label={i18n('reportTemplates.customer')} value={customer} onChange={handleChangeProvider} orgs={orgOptions} handleNewOrg={handleNewOrganizationSubmit} />
              </div>
            </>
          )}
          {!showFilters && mode === 'report' && step === max && (
            <Tooltip title={i18n('reportTemplates.filters.none')}>
              <DataFilter //
                title="data"
                disabled
                onChange={onChange}
                addFilter={addFilter}
                setFilter={setFilter}
                removeFilter={removeFilter}
                features={part?.characteristics || []}
                lists={filterList}
                filters={filters}
                options={options.current}
              />
            </Tooltip>
          )}
          {showFilters && mode === 'report' && step === max && (
            <DataFilter //
              title="data"
              onChange={onChange}
              addFilter={addFilter}
              setFilter={setFilter}
              removeFilter={removeFilter}
              features={part?.characteristics || []}
              lists={filterList}
              filters={filters}
              options={options.current}
            />
          )}
          {mode === 'template' && step !== max && hasReportEdit.current && (
            <div className="part-select" data-cy="report-part">
              <Select
                loading={loading}
                input={{
                  name: 'part',
                  label: i18n('entities.part.fields.number'),
                  value: templatePart || null,
                  placeholder: 'Parts with Features',
                  type: 'select',
                  group: partOptions,
                  change: handleChangePartNumber,
                  disabled: step === max,
                  search: true,
                }}
              />
            </div>
          )}
          {mode === 'template' && template && step === max && (
            <>
              <div className="customer-select" data-cy="report-customers">
                <Providers //
                  name="provider"
                  label={i18n('reportTemplates.provider')}
                  value={customer}
                  onChange={handleChangeProvider}
                  orgs={orgOptions}
                  handleNewOrg={handleNewOrganizationSubmit}
                  disabled={step !== max}
                />
              </div>
              <div className="template-settings">
                <TemplateDirection />
                <TokenSection />
              </div>
            </>
          )}
        </div>
        {(hasTemplateEdit.current || hasReportEdit.current) && (
          <div className="drawer-footer">
            <ProgressFooter //
              id="reports-footer"
              classes="panel-progression-footer extraction-footer"
              cypress="progress-footer"
              steps={max + 1}
              current={step + 1}
              onNext={progressToEdit}
              onExit={saveAndExit}
              canBack
              canProgress={!!template}
              canExit={step === max && saved}
              onBack={back}
              onCancel={back}
              exitText={saved ? i18n('common.exit') : i18n('common.wait')}
              nextText={mode === 'template' ? nextText.current : i18n('common.next')}
              backText={!id ? i18n('common.cancel') : i18n('common.back')}
              backCy="back-btn-footer"
              nextCy="forward-btn-footer"
            />
          </div>
        )}
      </div>
    </Sider>
  );
};

export default ReportTemplatesInfoDrawer;
// ReportTemplatesInfoDrawer.whyDidYouRender = true;
