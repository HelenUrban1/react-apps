import { UploadFile } from 'antd/lib/upload/interface';
import { v4 as uuid } from 'uuid';
import { camelCase, cloneDeep } from 'lodash';
import config from 'config';
import log from 'modules/shared/logger';
import { ReportTemplate, ReportTemplateInput, Filter, FilterObject, Report, ReportInput } from 'types/reportTemplates';
import { FileInput } from 'types/file';
import { getFileExtension } from 'utils/files';
import { Characteristic } from 'types/characteristics';
import { Part } from 'domain/part/partTypes';
import { Drawing } from 'graphql/drawing';
import { DrawingSheet } from 'graphql/drawing_sheet';
import { defaultProvider } from 'view/global/defaults';

const picker = ['quantity', 'markerLabel', 'drawingSheetIndex'];
const range = ['tolerance', 'nominal', 'specification'];
const list = ['operation', 'inspectionMethod', 'criticality', 'notationType', 'notationSubtype', 'drawing', 'gridCoordinates', 'balloonGridCoordinates', 'connectionPointGridCoordinates'];

// Prepare metadata about the provided files
export const convertUploadToFileInput = (files: UploadFile[], source?: string) => {
  const uploadFiles: FileInput[] = [];
  files.forEach((file) => {
    const extension = getFileExtension(source || file.name);
    const newFileName = `${uuid()}${extension}`;
    const newFile: FileInput = {
      name: file.name,
      sizeInBytes: file.size,
      publicUrl: `${config.backendUrl}/download?privateUrl=${newFileName}`,
      privateUrl: newFileName,
      new: true,
    };
    uploadFiles.push(newFile);
  });
  return uploadFiles;
};

export const createNewTemplateInput = (files: UploadFile[], provider: string, source?: string) => {
  const uploadFiles = convertUploadToFileInput(files, source);

  const newTemplate: ReportTemplateInput = {
    provider,
    title: uploadFiles[0].name.replace('.xlsx', '').replace('.csv', ''),
    status: 'Active',
    direction: 'Vertical',
    file: uploadFiles,
  };
  if (source) {
    newTemplate.title += ' (Copy)';
  }
  return newTemplate;
};

export const determineFilterType = (value: string[] | undefined) => {
  if (!value) {
    log.warn('Cannot determine filter type without value');
    return { pickerType: null, cat: null };
  }
  let type = null;
  const cat = value[value.length - 1];

  const finalValue = camelCase(cat);
  if (picker.includes(finalValue)) {
    type = 'picker';
  }
  if (range.includes(finalValue)) {
    type = 'range';
  }
  if (list.includes(finalValue)) {
    type = 'list';
  }
  return { pickerType: type, cat: finalValue };
};

export const filterFeatureRange = (features: Characteristic[], filterRange: { start: string | number; end: string | number }, filterProp: string) => {
  if (!filterRange || !filterRange.start) {
    log.warn('Cannot filter features by range without a starting range');
    return features;
  }
  if (!filterProp) {
    log.warn('Cannot filter features by range without a feature prop');
    return features;
  }
  // Start and ed adjusted to be inclusive even with sub balloons
  const start = parseFloat(filterRange.start.toString()) - 1;
  const end = filterRange.end ? parseFloat(filterRange.end.toString()) + 1 : undefined;
  const filteredFeatures: Characteristic[] = [];
  features.forEach((feat) => {
    const propCompare = feat[filterProp];
    // if (propCompare === undefined || propCompare === null) {
    //   filteredFeatures.push(feat);
    // }
    if (propCompare && propCompare > start) {
      if (!end || propCompare < end) {
        filteredFeatures.push(feat);
      }
    }
  });

  return filteredFeatures;
};

export const filterFeatureList = (features: Characteristic[], filterValues: string[], filterProp: string) => {
  if (!filterValues || filterValues.length === 0) {
    log.warn('Cannot filter features by value without a list of values');
    return features;
  }
  if (!filterProp) {
    log.warn('Cannot filter features by value without a feature prop');
    return features;
  }
  const filteredFeatures: Characteristic[] = [];
  features.forEach((feat) => {
    const propCompare = feat[filterProp];
    // if (propCompare === undefined || propCompare === null) {
    //   filteredFeatures.push(feat);
    // }
    filterValues.forEach((val) => {
      if (propCompare === val) {
        filteredFeatures.push(feat);
      } else if (propCompare && Object.prototype.hasOwnProperty.call(propCompare, 'id')) {
        const { id } = propCompare as Part | Drawing | DrawingSheet;
        if (id === val) {
          filteredFeatures.push(feat);
        }
      }
    });
  });
  return filteredFeatures;
};

export const filterFeatures = (features: Characteristic[], filter: Filter) => {
  const { pickerType: filterType, cat } = determineFilterType(filter.category);
  if (!filter.category || !cat) {
    log.warn('Cannot filter features when filter type is unknown');
    return features;
  }

  if (filterType === 'range' || filterType === 'picker') {
    return filterFeatureRange(features, filter.value, cat);
  }

  if (filterType === 'list') {
    return filterFeatureList(features, filter.value, cat);
  }

  log.warn(`Could not filter for unknown filter type ${filter.category}`);
  return features;
};

export const generateReport = (template: ReportTemplate | null, features?: Characteristic[], report?: Report | null, part?: Part, filters?: FilterObject) => {
  if (!template || !template.file) {
    log.warn(`Missing Template`);
    return { reportInput: null, filteredFeatures: features };
  }
  const newFileName = report?.file && report.file[0]?.privateUrl ? report.file[0]?.privateUrl : `${uuid()}.xlsx`;
  const uploadFiles: FileInput[] = [
    {
      name: `${part?.name} Report`,
      publicUrl: `${config.backendUrl}/download?privateUrl=${newFileName}`,
      privateUrl: newFileName,
      new: true,
    },
  ];
  const reportInput: ReportInput = {
    customer: report?.customer?.id || part?.customer?.id || part?.id,
    file: uploadFiles,
    part: part!.id,
    partVersion: part!.revision ? part!.revision : 'None',
    template: template.provider?.name === defaultProvider.name ? '' : template!.id,
    templateUrl: template.file[0]?.privateUrl ? template.file[0].privateUrl : template.file[0].publicUrl,
    filters: JSON.stringify(filters) || '{}',
    title: report?.title || `${part?.number || 'Report'} - ${part?.revision || 'New'} ${part?.type || ''}`,
    status: 'Active',
  };
  let filteredFeatures = features ? cloneDeep(features) : [];
  // Apply Filters
  if (filters && Object.keys(filters).length > 0 && filteredFeatures.length > 0) {
    Object.values(filters).forEach((filter) => {
      if (filter.category) {
        filteredFeatures = filterFeatures(filteredFeatures, filter);
      }
    });
  }
  return { reportInput, filteredFeatures };
};
