import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import Handsontable from 'handsontable';
import { i18n } from 'i18n';
import { Part } from 'domain/part/partTypes';
import { ReportAPI } from 'graphql/report';
import { Settings } from 'domain/setting/settingApi';
import { Setting, SettingInput } from 'domain/setting/settingTypes';
import { ReportTemplateAPI } from 'graphql/reportTemplate';
import { ReportTemplate, TokenType, ReportTemplateSettings } from 'types/reportTemplates';
import { doDeleteReport, doDeleteTemplate, doSaveReport, doSaveTemplate } from 'modules/reportTemplate/reportTemplateActions';
import Analytics from 'modules/shared/analytics/analytics';
import log from 'modules/shared/logger';
import { AppState } from 'modules/state';
import ExcelTable from 'styleguide/Excel';
import { CellChanges, SheetData } from 'styleguide/Excel/types';
import { dateIsSameOrBefore } from 'utils/DataCalculation';
import { handleFileDownload } from 'utils/files';
import { createSettingThunk, updateSettingThunk } from 'modules/session/sessionActions';
import { defaultProvider } from 'view/global/defaults';

export interface TemplateInput {
  [key: string]: any;
  reportTemplate?: ReportTemplate | null;
  tableData?: SheetData[] | null;
  partData?: Part | null;
  title?: string;
  provider?: string;
  type?: string;
  step?: number;
  id?: string | null;
  direction?: 'Vertical' | 'Horizontal' | undefined;
  token?: TokenType | null;
  settings?: ReportTemplateSettings | null;
  saved?: boolean;
  org?: string | null;
  updatedAt?: Date | string;
  createdAt?: Date | string;
}

const ReportTemplatesDataEditor: React.FC<{ handleGenerateReport: () => void }> = ({ handleGenerateReport }) => {
  const { part } = useSelector((state: AppState) => state.part);
  const { id, step, max, data, mode, report, template, loading, saved, settings } = useSelector((state: AppState) => state.reportTemplates);
  const { currentUser } = useSelector((state: AppState) => state.auth);
  const { userSettings } = useSelector((state: AppState) => state.session);
  const dispatch = useDispatch();
  const isNew = false; // TODO: how to do this with id in params?

  const [currentSheet, setSheet] = useState<number>(0);
  const [confirmRefresh, setConfirm] = useState<boolean>(false);

  if (!data || loading) {
    return <></>;
  }

  const handleDownload = async (type: string, font?: boolean, framed?: boolean) => {
    const currentFile = mode === 'report' ? report : template;
    if (!currentFile || currentFile.file.length < 0 || !currentFile.file[0].publicUrl) {
      log.warn('no file to download');
      return;
    }
    Analytics.track({
      event: Analytics.events.reportDownloaded,
      part,
      report,
      template,
    });

    handleFileDownload({
      publicUrl: currentFile.file[0].publicUrl,
      fileName: currentFile.title || i18n('common.untitled'),
      type,
      font,
      unframed: framed,
      partId: part?.id,
    });
  };

  const handleUpdatePrompt = () => {
    if (!currentUser || !userSettings) {
      log.error(`Missing required info: user? ${!currentUser} settings? ${!userSettings}`);
      return;
    }
    if (userSettings.promptFont) {
      const updatedSetting: Setting = { ...userSettings.promptFont, id: (userSettings.promptFont as Setting).id, value: 'false' };
      dispatch(updateSettingThunk(Settings.createGraphqlObject(updatedSetting), updatedSetting.id));
    } else {
      const newSetting: SettingInput = { name: 'promptFont', userId: currentUser.id, value: 'false' };
      dispatch(createSettingThunk(newSetting, currentUser.id));
    }
  };

  const saveChanges = (changes: Handsontable.CellChange[] | null, source: Handsontable.ChangeSource) => {
    if (!changes || currentSheet === null || currentSheet === undefined) {
      return;
    }
    const currentChange: CellChanges[] = [];

    changes.forEach(([row, props, oldValue, newValue]) => {
      if (source === 'edit') {
        const col = parseInt(props.toString(), 10);
        if (newValue && newValue !== oldValue && true) {
          if (!data || data[currentSheet].rows[row][col] === oldValue) {
            return;
          }
          currentChange.push({
            sheet: data[currentSheet].name,
            address: newValue.address,
            data: newValue.data,
          });
        }
      }
    });
    if (currentChange.length > 0) {
      if (mode === 'report' && report) {
        const reportInput = ReportAPI.createGraphqlInput(report);
        dispatch(doSaveReport({ id: report.id, report: reportInput, changes: currentChange }));
      }
      if (mode === 'template' && template) {
        const templateInput = ReportTemplateAPI.createGraphqlInput(template);
        dispatch(doSaveTemplate({ id: template.id, template: templateInput, changes: currentChange }));
      }
    }
  };

  const editFileName = (newTitle: string) => {
    if (mode === 'report' && report && newTitle !== report.title) {
      const reportUpdate = ReportAPI.createGraphqlInput({ ...report, title: newTitle });
      dispatch(doSaveReport({ id: report.id, report: reportUpdate }));
    }
    if (mode === 'template' && template && newTitle !== template.title) {
      const templateInput = ReportTemplateAPI.createGraphqlInput({ ...template, title: newTitle });
      dispatch(doSaveTemplate({ id: template.id, template: templateInput }));
    }
  };

  const refreshReportData = async () => {
    setConfirm(false);
    return handleGenerateReport();
  };

  const handleRefresh = () => {
    if (part && report && report.updatedAt) {
      const isCurrent = dateIsSameOrBefore(report.updatedAt, part.updatedAt);
      if (isCurrent) {
        setConfirm(true);
      } else {
        refreshReportData();
      }
    }
  };

  const cancelRefresh = () => {
    setConfirm(false);
  };

  const handleDelete = () => {
    if (!id) {
      return;
    }
    if (mode === 'report' && report) {
      const change = ReportAPI.createGraphqlInput(report);
      dispatch(doDeleteReport({ id, report: change }));
    } else {
      dispatch(doDeleteTemplate({ id: [id] }));
    }
  };

  const updateTemplateContext = (change: TemplateInput, updates?: Handsontable.CellChange[]) => {
    if (!!change && !!template && !!template.id) {
      const templateInput = ReportTemplateAPI.createGraphqlInput({ ...template, ...change.reportTemplate });
      Object.keys(change).forEach((key) => {
        if (change[key] && Object.prototype.hasOwnProperty.call(templateInput, key)) {
          if (key === 'settings') {
            try {
              templateInput[key] = JSON.stringify(change[key]);
            } catch (err) {
              log.error(err);
            }
          } else {
            templateInput[key] = change[key];
          }
        }
      });
      if (updates && updates.length > 0) {
        const currentChange: CellChanges[] = [];

        updates.forEach(([row, props, oldValue, newValue]) => {
          const col = parseInt(props.toString(), 10);
          if (newValue && newValue !== oldValue && true) {
            if (!data || data[currentSheet].rows[row][col] === oldValue) {
              return;
            }
            currentChange.push({
              sheet: data[currentSheet].name,
              address: newValue.address,
              data: newValue.data,
            });
          }
        });
        if (currentChange.length > 0) {
          return dispatch(doSaveTemplate({ id: template.id, template: templateInput, changes: currentChange }));
        }
      }
      dispatch(doSaveTemplate({ id: template.id, template: templateInput }));
    }
    return null;
  };

  return (
    <ExcelTable
      title={mode === 'template' ? template?.title : report?.title}
      table={data}
      currentSheet={currentSheet}
      direction={template?.direction}
      settings={settings}
      readOnly={step < max}
      refresh={refreshReportData}
      checkRefresh={handleRefresh}
      editFileName={editFileName}
      deleteFile={isNew ? handleDelete : undefined}
      confirm={confirmRefresh}
      cancel={cancelRefresh}
      setSheet={setSheet}
      saveChanges={saveChanges}
      isDefault={template?.provider?.name === defaultProvider.name ? template?.file[0].name : ''}
      downloadFile={handleDownload}
      mode={mode || 'report'}
      saved={saved}
      canEdit={step === max}
      showPrompt={userSettings.promptFont?.value !== 'false'}
      canRefresh={!!template?.file && !!part}
      handleUpdatePrompt={handleUpdatePrompt}
      handleUpdateContext={updateTemplateContext}
    />
  );
};

export default ReportTemplatesDataEditor;
