import React from 'react';
import { i18n } from 'i18n';
import TokenTree from 'styleguide/TokenTree/TokenTree';
import { TokenType } from 'types/reportTemplates';
import { useDispatch, useSelector } from 'react-redux';
import { AppState } from 'modules/state';
import { ReportTemplateAPI } from 'graphql/reportTemplate';
import { doSaveTemplate } from 'modules/reportTemplate/reportTemplateActions';
import { Tooltip } from 'antd';
import { QuestionCircleOutlined } from '@ant-design/icons';

export const TokenSection = () => {
  const { template, settings } = useSelector((state: AppState) => state.reportTemplates);
  const dispatch = useDispatch();
  if (!template) {
    return <></>;
  }

  const changeToken = (token: TokenType) => {
    if (!token.token) {
      return;
    }
    const newUsed = settings?.tokens ? [...settings.tokens, token.token] : [token.token];
    const newSettings = { ...settings, tokens: newUsed };
    const templateInput = ReportTemplateAPI.createGraphqlInput({ ...template, settings: JSON.stringify(newSettings) });
    dispatch(doSaveTemplate({ id: template.id, template: templateInput }));
  };
  return (
    <>
      <section className="section token-section">
        {i18n('entities.tokens.tree.title')}
        <Tooltip title={i18n('entities.template.token.hint')}>
          <QuestionCircleOutlined className="direction-icon" />
        </Tooltip>
      </section>
      <TokenTree setSelectedToken={changeToken} usedTokens={settings?.tokens || []} customTokens={[]} />
    </>
  );
};
