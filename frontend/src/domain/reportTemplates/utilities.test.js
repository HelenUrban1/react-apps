// convertUploadToFileInput
// createNewTemplateInput

import { convertUploadToFileInput, createNewTemplateInput, determineFilterType, filterFeatureList, filterFeatureRange, filterFeatures, generateReport } from './utilities';

const files = [
  {
    name: 'file.csv',
    size: 10,
  },
];

const template = {
  id: 'template',
  file: [{ privateUrl: 'url' }],
};

const defaultTemplate = {
  id: 'template',
  provider: {
    name: 'Ideagen',
  },
  file: [{ privateUrl: 'url' }],
};

const report = { id: 'report', title: 'title', customer: { id: 'customer' } };
const part = { id: 'part', revision: 'revision' };

describe('convertUploadToFileInput', () => {
  it('returns an empty array if no files are passed', () => {
    const inputs = convertUploadToFileInput([]);
    expect(inputs).toEqual([]);
  });

  it('returns an array of file inputs', () => {
    const inputs = convertUploadToFileInput(files);
    expect(inputs[0]).toEqual(
      expect.objectContaining({
        name: 'file.csv',
        sizeInBytes: 10,
        new: true,
      }),
    );
  });
});

describe('createNewTemplateInput', () => {
  it('creates a template input for the db', () => {
    const input = createNewTemplateInput(files, 'test');
    expect(input).toEqual(
      expect.objectContaining({
        provider: 'test',
        title: 'file',
        status: 'Active',
        direction: 'Vertical',
      }),
    );
  });
});

describe('determineFilterType', () => {
  it('returns null if no value is passed', () => {
    const { pickerType, cat } = determineFilterType(undefined);
    expect(pickerType).toBeNull();
    expect(cat).toBeNull();
  });

  it('returns range if the category is a float', () => {
    const { pickerType, cat } = determineFilterType(['Feature', 'Nominal']);
    expect(pickerType).toBe('range');
    expect(cat).toBe('nominal');
  });

  it('returns picker if the category is an integer', () => {
    const { pickerType, cat } = determineFilterType(['Feature', 'Quantity']);
    expect(pickerType).toBe('picker');
    expect(cat).toBe('quantity');
  });

  it('returns list if the category is a string', () => {
    const { pickerType, cat } = determineFilterType(['Feature', 'Inspection Method']);
    expect(pickerType).toBe('list');
    expect(cat).toBe('inspectionMethod');
  });

  it('returns the camel case feature prop', () => {
    const { pickerType, cat } = determineFilterType(['Feature', 'Notation Subtype']);
    expect(pickerType).toBe('list');
    expect(cat).toBe('notationSubtype');
  });
});

describe('filterFeatureRange', () => {
  it('returns unfiltered if no range is provided', () => {
    const unFiltered = [
      { id: 1, markerLabel: '1' },
      { id: 2, markerLabel: '2.1' },
      { id: 3, markerLabel: '2.2' },
      { id: 4, markerLabel: '3' },
    ];
    const filtered = filterFeatureRange(unFiltered, undefined, 'markerLabel');
    expect(filtered).toMatchObject(unFiltered);
  });

  it('returns unfiltered if no property if provided', () => {
    const unFiltered = [
      { id: 1, markerLabel: '1' },
      { id: 2, markerLabel: '2.1' },
      { id: 3, markerLabel: '2.2' },
      { id: 4, markerLabel: '3' },
    ];
    const filtered = filterFeatureRange(unFiltered, { start: 1 }, undefined);
    expect(filtered).toMatchObject(unFiltered);
  });

  it('returns all features after the starting range if end is undefined', () => {
    const unFiltered = [
      { id: 1, markerLabel: '1' },
      { id: 2, markerLabel: '2.1' },
      { id: 3, markerLabel: '2.2' },
      { id: 4, markerLabel: '3' },
    ];
    const filtered = filterFeatureRange(unFiltered, { start: 2 }, 'markerLabel');
    expect(filtered).toMatchObject([
      { id: 2, markerLabel: '2.1' },
      { id: 3, markerLabel: '2.2' },
      { id: 4, markerLabel: '3' },
    ]);
  });

  it('returns all features between start and end', () => {
    const unFiltered = [
      { id: 1, markerLabel: '1' },
      { id: 2, markerLabel: '2.1' },
      { id: 3, markerLabel: '2.2' },
      { id: 4, markerLabel: '3' },
    ];
    const filtered = filterFeatureRange(unFiltered, { start: 2, end: 2 }, 'markerLabel');
    expect(filtered).toMatchObject([
      { id: 2, markerLabel: '2.1' },
      { id: 3, markerLabel: '2.2' },
    ]);
  });
});

describe('filterFeatureList', () => {
  it('returns unfiltered if no list values are provided', () => {
    const unFiltered = [
      { id: 1, criticality: 'Minor' },
      { id: 2, criticality: 'Major' },
      { id: 3, criticality: 'Incidental' },
    ];
    const filtered = filterFeatureList(unFiltered, undefined, 'criticality');
    expect(filtered).toMatchObject(unFiltered);
  });

  it('returns unfiltered if no feature property is provided', () => {
    const unFiltered = [
      { id: 1, criticality: 'Minor' },
      { id: 2, criticality: 'Major' },
      { id: 3, criticality: 'Incidental' },
    ];
    const filtered = filterFeatureList(unFiltered, ['Major'], undefined);
    expect(filtered).toMatchObject(unFiltered);
  });

  it('returns the features whose property value matches one of the filter values', () => {
    const unFiltered = [
      { id: 1, criticality: 'Minor' },
      { id: 2, criticality: 'Major' },
      { id: 3, criticality: 'Incidental' },
    ];
    const filtered = filterFeatureList(unFiltered, ['Major', 'Minor'], 'criticality');
    expect(filtered).toMatchObject([
      { id: 1, criticality: 'Minor' },
      { id: 2, criticality: 'Major' },
    ]);
  });
});

describe('filterFeatures', () => {
  it('returns unfiltered if no category is provided', () => {
    const unFiltered = [
      { id: 1, markerLabel: '1' },
      { id: 2, markerLabel: '2.1' },
      { id: 3, markerLabel: '2.2' },
      { id: 4, markerLabel: '3' },
    ];
    const filtered = filterFeatures(unFiltered, { value: { start: 1 }, category: undefined });
    expect(filtered).toMatchObject(unFiltered);
  });

  it('filters by range if the category is a number or float', () => {
    const unFiltered = [
      { id: 1, markerLabel: '1' },
      { id: 2, markerLabel: '2.1' },
      { id: 3, markerLabel: '2.2' },
      { id: 4, markerLabel: '3' },
    ];
    const filtered = filterFeatures(unFiltered, { value: { start: 2, end: 2 }, category: ['Feature', 'Marker Label'] });
    expect(filtered).toMatchObject([
      { id: 2, markerLabel: '2.1' },
      { id: 3, markerLabel: '2.2' },
    ]);
  });

  it('filters by a list of strings if the category is a string', () => {
    const unFiltered = [
      { id: 1, criticality: 'Minor' },
      { id: 2, criticality: 'Major' },
      { id: 3, inspectionMethod: 'Drilling' },
    ];
    const filtered = filterFeatures(unFiltered, { value: ['Minor'], category: ['Inspection', 'Criticality'] });
    expect(filtered).toMatchObject([{ id: 1, criticality: 'Minor' }]);
  });

  it('returns unfiltered if the category type is not recognized', () => {
    const unFiltered = [
      { id: 1, markerLabel: '1' },
      { id: 2, markerLabel: '2.1' },
      { id: 3, markerLabel: '2.2' },
      { id: 4, markerLabel: '3' },
    ];
    const filtered = filterFeatures(unFiltered, { value: { start: 1 }, category: ['Fake', 'Property'] });
    expect(filtered).toMatchObject(unFiltered);
  });
});

describe('generateReport', () => {
  it('returns null if no template is provided', () => {
    const { reportInput, filteredFeatures } = generateReport(null);
    expect(reportInput).toBeNull();
    expect(filteredFeatures).toBeUndefined();
  });

  it('returns a report object', () => {
    const { reportInput, filteredFeatures } = generateReport(template, [], report, part);
    expect(reportInput).toEqual(
      expect.objectContaining({
        customer: report.customer.id,
        part: part.id,
        partVersion: part.revision,
        template: template.id,
        templateUrl: template.file[0].privateUrl,
        filters: '{}',
        title: report.title,
        status: 'Active',
      }),
    );
    expect(filteredFeatures).toMatchObject([]);
  });

  it('returns a report object with no template when using a default template', () => {
    const { reportInput, filteredFeatures } = generateReport(defaultTemplate, [], report, part);
    expect(reportInput).toEqual(
      expect.objectContaining({
        customer: report.customer.id,
        part: part.id,
        partVersion: part.revision,
        template: '',
        templateUrl: defaultTemplate.file[0].privateUrl,
        filters: '{}',
        title: report.title,
        status: 'Active',
      }),
    );
    expect(filteredFeatures).toMatchObject([]);
  });

  it('returns a filtered feature list if filters are provided', () => {
    const unFiltered = [
      { id: 1, markerLabel: '1' },
      { id: 2, markerLabel: '2.1' },
      { id: 3, markerLabel: '2.2' },
      { id: 4, markerLabel: '3' },
    ];
    const { reportInput, filteredFeatures } = generateReport(template, unFiltered, report, part, { 1: { value: { start: 1, end: 2 }, category: ['Feature', 'Marker Label'] } });
    expect(reportInput).toEqual(
      expect.objectContaining({
        customer: report.customer.id,
        part: part.id,
        partVersion: part.revision,
        template: template.id,
        templateUrl: template.file[0].privateUrl,
        filters: '{"1":{"value":{"start":1,"end":2},"category":["Feature","Marker Label"]}}',
        title: report.title,
        status: 'Active',
      }),
    );
    expect(filteredFeatures).toMatchObject([
      { id: 1, markerLabel: '1' },
      { id: 2, markerLabel: '2.1' },
      { id: 3, markerLabel: '2.2' },
    ]);
  });

  it('returns the unfiltered feature list if no filters are provided', () => {
    const unFiltered = [
      { id: 1, markerLabel: '1' },
      { id: 2, markerLabel: '2.1' },
      { id: 3, markerLabel: '2.2' },
      { id: 4, markerLabel: '3' },
    ];
    const { reportInput, filteredFeatures } = generateReport(template, unFiltered, report, part);
    expect(reportInput).toEqual(
      expect.objectContaining({
        customer: report.customer.id,
        part: part.id,
        partVersion: part.revision,
        template: template.id,
        templateUrl: template.file[0].privateUrl,
        filters: '{}',
        title: report.title,
        status: 'Active',
      }),
    );
    expect(filteredFeatures).toMatchObject(unFiltered);
  });
});
