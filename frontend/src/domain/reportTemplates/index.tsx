import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { i18n } from 'i18n';
import { useHistory, useLocation, useParams } from 'react-router';
import log from 'modules/shared/logger';
import { doGetPart } from 'modules/part/partActions';
import { doDeleteTemplate, doGenerateReport, doGenerateTemplate, doGetReport, doGetTemplate, ReportTemplateActions } from 'modules/reportTemplate/reportTemplateActions';
import { AppState } from 'modules/state';
import { updatePartStepThunk } from 'modules/navigation/navigationActions';
import Analytics from 'modules/shared/analytics/analytics';
import { Loader } from 'view/global/Loader';
import { FilterObject, ReportTemplate } from 'types/reportTemplates';
import { Parts } from 'domain/part/partApi';
import { Result, Button } from 'antd';
import { RevisionBanner } from 'styleguide/Revision/RevisionBanner';
import ReportTemplatesDataEditor from './DataEditor';
import ReportTemplatesInfoDrawer from './InfoDrawer';
import ReportTemplatesGallery from './Gallery';
import { createNewTemplateInput, generateReport } from './utilities';

interface Params {
  partId?: string;
  reportId?: string;
  templateId?: string;
}
interface LocationState {
  from: string;
  source: string;
  step?: number;
  max?: number;
}

const ReportTemplates: React.FC = () => {
  const { partId, reportId, templateId } = useParams<Params>();
  const { part, revisionIndex, revisionType } = useSelector((state: AppState) => state.part);
  const { currentPartStep } = useSelector((state: AppState) => state.navigation);
  const { id, template, report, step, max, loading, error, mode, warning } = useSelector((state: AppState) => state.reportTemplates);
  const { organizations, types, classifications, methods, operations, imperialUnits, metricUnits } = useSelector((app: AppState) => app.session);
  const units = { ...imperialUnits, ...metricUnits };
  const dispatch = useDispatch();
  const history = useHistory();
  const { state } = useLocation<LocationState>();

  const handleGenerateReport = (temp?: ReportTemplate, overwrite?: string, filters?: FilterObject) => {
    const useTemplate = temp || template;
    if (!part) {
      log.warn(`Missing Part`);
      return;
    }
    const { reportInput, filteredFeatures } = generateReport(useTemplate, part.characteristics, report, part, filters);
    const partData = Parts.createPartDataObject(part, part.customer, filteredFeatures, types, classifications, methods, operations, units);
    if (!partData) {
      log.warn('No Part Data');
      return;
    }
    if (!reportInput) {
      log.warn('No Report Data');
      return;
    }
    dispatch(
      doGenerateReport({
        //
        url: useTemplate?.file[0]?.publicUrl || report?.templateUrl || '',
        part: partData,
        report: reportInput,
        id: overwrite || report?.id,
        settings: useTemplate?.settings,
        direction: useTemplate?.direction,
      }),
    );
  };

  const handleCloneTemplate = (temp?: ReportTemplate) => {
    if (!temp && !template) {
      dispatch({ type: ReportTemplateActions.ERROR, payload: { message: i18n('entities.template.errors.copy') } });
      return;
    }
    const newTemplate = temp || template;
    if (!newTemplate || !newTemplate?.file[0]?.publicUrl) {
      dispatch({ type: ReportTemplateActions.ERROR, payload: { message: i18n('entities.template.errors.copy') } });
      return;
    }

    let providerId = newTemplate?.provider?.id;
    if (!providerId || providerId === 'none') {
      if (organizations?.self?.id) {
        providerId = organizations.self.id;
      } else {
        dispatch({ type: ReportTemplateActions.ERROR, payload: { message: 'No Provider Available for Template' } });
        return;
      }
    }
    const copiedTemplate = createNewTemplateInput(
      [
        {
          uid: '',
          name: newTemplate.title,
          size: newTemplate.file[0].sizeInBytes || 0,
          type: '',
        },
      ],
      providerId,
      newTemplate.file[0].publicUrl,
    );
    copiedTemplate.settings = typeof newTemplate.settings === 'object' ? JSON.stringify(newTemplate.settings || {}) : newTemplate.settings || '{}';
    Analytics.track({
      event: Analytics.events.cloneReportTemplate,
      template: newTemplate,
    });
    dispatch(doGenerateTemplate({ url: newTemplate.file[0]?.publicUrl, template: copiedTemplate, copy: true }));
  };

  // Set Mode
  useEffect(() => {
    if (step < 0) {
      if (history.location.pathname.includes('report')) {
        dispatch({ type: ReportTemplateActions.SET_NEW_REPORT });
      } else if (history.location.pathname.includes('template')) {
        dispatch({ type: ReportTemplateActions.SET_NEW_TEMPLATE });
      }
    }
  }, [step, history.location.pathname]);

  // Set Nav
  useEffect(() => {
    if (state?.step || state?.max) {
      let newStep = step;
      let newMax = max;
      if (state?.step !== undefined && state.step !== step) {
        newStep = state.step;
      }
      if (state?.max !== undefined && state.max !== max) {
        newMax = state.max;
      }
      dispatch({ type: ReportTemplateActions.SET_NAV, payload: { step: newStep, max: newMax } });
      state.step = undefined;
      state.max = undefined;
    }
    if (currentPartStep < max - 1) {
      dispatch(updatePartStepThunk(max - 1));
    }
  }, [state, step, max, currentPartStep]);

  // Set Template
  useEffect(() => {
    if (templateId && (!template || template.id !== templateId)) {
      dispatch(doGetTemplate(templateId));
      dispatch({ type: ReportTemplateActions.SET_STEP, payload: 1 });
    }
  }, [templateId, template]);

  // Set Report
  useEffect(() => {
    if (reportId && (!report || report.id !== reportId)) {
      dispatch(doGetReport(reportId));
      dispatch({ type: ReportTemplateActions.SET_STEP, payload: Parts.step.order.length });
    }
  }, [reportId, report]);

  // Set Part
  useEffect(() => {
    if (partId && (!part || part.id !== partId)) {
      dispatch(doGetPart(partId));
    }
  }, [partId, part]);

  const closeError = () => {
    dispatch({ type: ReportTemplateActions.CLEAR });
    if (mode === 'template') {
      if (id || templateId) {
        dispatch(doDeleteTemplate({ id: [id || templateId!] }));
      }
      return history.push('/templates');
    }
    return history.push('/');
  };

  const closeWarning = () => {
    dispatch({ type: ReportTemplateActions.WARNING, payload: null });
  };

  return (
    <>
      <section className="ant-layout ant-layout-has-sider">
        {warning && (
          <div className="loader" data-cy="loader">
            <Result
              status="warning"
              title={i18n('entities.template.warnings.title')}
              subTitle={warning}
              extra={
                <Button type="primary" onClick={closeWarning}>
                  {i18n('common.close')}
                </Button>
              }
            />
          </div>
        )}
        <Loader loading={undefined} queries={[loading]} error={error} closeError={closeError} />
        <ReportTemplatesInfoDrawer handleGenerateReport={handleGenerateReport} handleCloneTemplate={handleCloneTemplate} />
        {step < max && <ReportTemplatesGallery handleGenerateReport={handleGenerateReport} handleCloneTemplate={handleCloneTemplate} />}
        {step === max && <ReportTemplatesDataEditor handleGenerateReport={handleGenerateReport} />}
      </section>
      {revisionIndex > 0 && revisionType && <RevisionBanner previous={revisionIndex} type={revisionType} />}
    </>
  );
};

export default ReportTemplates;
// ReportTemplates.whyDidYouRender = true;
