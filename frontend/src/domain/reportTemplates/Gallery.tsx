import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router';
import { UploadFile } from 'antd/lib/upload/interface';
import { i18n } from 'i18n';
import { ReportTemplate, FilterObject } from 'types/reportTemplates';
import PermissionChecker from 'modules/auth/permissionChecker';
import { ReportTemplateActions, doGenerateTemplate, doDeleteTemplate, doGetTemplate } from 'modules/reportTemplate/reportTemplateActions';
import Analytics from 'modules/shared/analytics/analytics';
import { AppState } from 'modules/state';
import Permissions from 'security/permissions';
import { defaultTemplates } from 'view/global/defaults';
import { TemplateCard } from 'styleguide/ReportTemplates/Templates/Card';
import { createNewTemplateInput } from './utilities';

interface Props {
  handleGenerateReport: (temp?: ReportTemplate, overwrite?: string, filters?: FilterObject) => void;
  handleCloneTemplate: (temp?: ReportTemplate | undefined) => void;
}

const ReportTemplatesGallery: React.FC<Props> = ({ handleGenerateReport, handleCloneTemplate }) => {
  const { organizations } = useSelector((state: AppState) => state.session);
  const { template } = useSelector((state: AppState) => state.reportTemplates);
  const { part } = useSelector((state: AppState) => state.part);
  const [customTemplates, setTemplates] = useState<ReportTemplate[]>([]);
  const { currentUser } = useSelector((state: AppState) => state.auth);
  const permissionValidator = new PermissionChecker(currentUser);
  const hasTemplateEdit = permissionValidator.match(Permissions.values.reportTemplateEdit);
  const hasReportEdit = permissionValidator.match(Permissions.values.reportEdit);
  const dispatch = useDispatch();
  const history = useHistory();

  useEffect(() => {
    if (organizations?.all) {
      const custom: ReportTemplate[] = [];
      organizations.all.forEach((org) => {
        if (org.templates) {
          org.templates.forEach((temp) => {
            custom.push({ ...temp, provider: org });
          });
        }
      });
      setTemplates(custom);
    }
  }, [organizations]);

  const handleSetTemplate = (reportTemplate: ReportTemplate) => {
    Analytics.track({
      event: Analytics.events.reportTemplateSelected,
      template: reportTemplate,
    });
    dispatch({ type: ReportTemplateActions.SET_TEMPLATE, payload: reportTemplate });
  };

  const handleCreateNewTemplate = (files: UploadFile[]) => {
    if (!files || (!organizations.self && !template?.provider)) {
      dispatch({ type: ReportTemplateActions.ERROR, payload: { message: i18n('entities.template.errors.copy') } });
      return;
    }
    let providerId = template?.provider?.id;
    if (!providerId || providerId === 'none') {
      if (organizations?.self?.id) {
        providerId = organizations.self.id;
      } else {
        dispatch({ type: ReportTemplateActions.ERROR, payload: { message: 'No Provider Available for Template' } });
        return;
      }
    }
    if (history.location.pathname.includes('report')) {
      dispatch({ type: ReportTemplateActions.SET_SOURCE, payload: 'report' });
      dispatch({ type: ReportTemplateActions.LOADING, payload: true });
      dispatch({ type: ReportTemplateActions.SET_NAV, payload: { max: -1, step: -1 } });
    }
    const copiedTemplate = createNewTemplateInput(files, providerId);
    copiedTemplate.settings = JSON.stringify({ saved: false, direction: 'Vertical' });
    dispatch(doGenerateTemplate({ url: copiedTemplate.file[0]?.publicUrl, template: copiedTemplate, file: files[0].originFileObj }));
  };

  // use the loaded template if triggered from action buttons, card template if triggered from card buttons
  const handleEditTemplate = async (temp?: ReportTemplate) => {
    if (!temp && !template) {
      dispatch({ type: ReportTemplateActions.ERROR, payload: { message: i18n('entities.template.error.edit') } });
      return;
    }

    if (temp?.file && temp.file[0] && !temp.file[0].publicUrl && template?.file && !template?.file[0]?.publicUrl) {
      dispatch({ type: ReportTemplateActions.ERROR, payload: { message: i18n('entities.template.error.edit') } });
      return;
    }
    if (history.location.pathname.includes('report')) {
      dispatch({ type: ReportTemplateActions.SET_SOURCE, payload: 'report' });
      dispatch({ type: ReportTemplateActions.LOADING, payload: true });
      dispatch({ type: ReportTemplateActions.SET_NAV, payload: { max: -1, step: -1 } });
    }
    dispatch(doGetTemplate(temp?.id || template!.id));
    history.push(`/templates/${temp?.id || template!.id}`, { step: 1, max: 1 });
  };

  const handleDeleteTemplate = (temp: ReportTemplate) => {
    dispatch(doDeleteTemplate({ id: [temp.id] }));
  };

  const handleReport = (temp: ReportTemplate) => {
    if (!template) {
      Analytics.track({
        event: Analytics.events.reportTemplateSelected,
        template: temp,
      });
      dispatch({ type: ReportTemplateActions.SET_TEMPLATE, payload: temp });
    }
    dispatch({ type: ReportTemplateActions.SET_NEW_REPORT });
    handleGenerateReport(temp, undefined);
  };

  return (
    <section className="ant-layout main-content-layout template-gallery-container">
      <h2>{i18n('entities.reportTemplate.list.title')}</h2>
      <div className="template-gallery" data-cy="template_list">
        {hasTemplateEdit && (
          <TemplateCard //
            key="add"
            enableReport={false}
            onClick={handleCreateNewTemplate}
          />
        )}
        {defaultTemplates.map((temp) => (
          <TemplateCard //
            key={temp.id}
            template={temp}
            enableReport={!!part}
            showReport={!!hasReportEdit}
            report={handleReport}
            image={`${process.env.PUBLIC_URL}/templates/${temp.title.replace(' Template', '')}/0.png`}
            copy={hasTemplateEdit ? handleCloneTemplate : undefined}
            onClick={handleSetTemplate}
          />
        ))}
        {customTemplates.map((temp) => (
          <TemplateCard //
            key={temp.id}
            template={temp}
            enableReport={!!part}
            showReport={!!hasReportEdit}
            report={handleReport}
            edit={hasTemplateEdit ? handleEditTemplate : undefined}
            copy={hasTemplateEdit ? handleCloneTemplate : undefined}
            del={hasTemplateEdit ? handleDeleteTemplate : undefined}
            onClick={handleSetTemplate}
          />
        ))}
      </div>
    </section>
  );
};

export default ReportTemplatesGallery;
