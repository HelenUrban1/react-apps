import { MarkerStyle } from 'types/marker';
import { Tolerance } from 'types/tolerances';

export interface SettingInput {
  name: string;
  value?: string;
  metadata?: string;
  siteId?: string;
  userId?: string;
}

export interface Setting {
  id: string;
  name: string;
  value?: string;
  metadata?: string;
  siteId?: string;
  userId?: string;
  createdAt?: Date;
  updatedAt?: Date;
}

export interface Preset {
  customers: string[];
  setting: Tolerance | { [key: string]: MarkerStyle };
}

export interface SettingPresets {
  [key: string]: Preset;
}

export interface FrequentlyUsedSymbols {
  [key: string]: number;
}

export interface LoadedSetting {
  id?: string;
  name: string;
  value?: string;
  siteId?: string;
  userId?: string;
  createdAt?: Date;
  updatedAt?: Date;
}

export interface LoadedPresets extends LoadedSetting {
  metadata: SettingPresets;
}

export interface SymbolsSetting extends LoadedSetting {
  metadata: FrequentlyUsedSymbols;
}

export interface SettingState {
  promptFont?: Setting | SettingInput;
  stylePresets?: LoadedPresets;
  tolerancePresets?: LoadedPresets;
  symbols?: SymbolsSetting;
  autoballoonEnabled?: Setting | SettingInput;
  [key: string]: any;
}
