import { Button, message, Tooltip, Modal, Form, Input } from 'antd';
import { i18n } from 'i18n';
import authSelectors from 'modules/auth/authSelectors';
import PermissionChecker from 'modules/auth/permissionChecker';
import Permissions from 'security/permissions';
import { AppState } from 'modules/state';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import actions from 'modules/auth/authActions';
import service from 'modules/auth/authService';
import { createSettingThunk, updateSettingThunk } from 'modules/session/sessionActions';
import axios from 'axios';
import config from 'config';
import log from 'modules/shared/logger';
import { ExclamationCircleOutlined } from '@ant-design/icons';
import { Settings } from '../settingApi';
import { SettingState } from '../settingTypes';

const { confirm } = Modal;

export const Security = () => {
  const user = useSelector((state: AppState) => authSelectors.selectCurrentUser(state));
  const dispatch = useDispatch();
  const { settings } = useSelector((state: AppState) => state.session);
  const { mfaEnabled, mfaRegistered, jwt, session } = useSelector((state: AppState) => state.auth);
  const [currentSettings, setCurrentSettings] = useState<SettingState>({ ...settings, mfaEnabled: { value: mfaEnabled, level: 'account' } });
  const permissionValidator = new PermissionChecker(user);
  const hasOwnerAccess = permissionValidator.match(Permissions.values.accountOwner);

  const [form] = Form.useForm();
  useEffect(() => {
    const checkSettings = async () => {
      setCurrentSettings({ ...currentSettings, ...settings });
      if (settings?.mfaEnabled !== undefined && settings.mfaEnabled.value !== mfaEnabled.toString()) {
        const mfaStatus = user ? await service.doCheckMfa(user.email) : false;
        dispatch({
          type: actions.SET_MFA,
          payload: {
            mfaEnabled: mfaStatus.mfaEnabled || false,
            mfaRegistered: mfaStatus.mfaRegistered || false,
          },
        });
      }
    };

    checkSettings();
  }, [settings]);

  useEffect(() => {
    if (hasOwnerAccess && mfaEnabled && mfaRegistered && (!settings.mfaEnabled || settings.mfaEnabled.value === 'false') && user) {
      const setting = settings.mfaEnabled || currentSettings.mfaEnabled;
      if (setting.id) {
        const data = Settings.createGraphqlObject({ ...setting, value: 'true' });
        dispatch(updateSettingThunk(data, setting.id));
      } else {
        const data = Settings.createGraphqlObject({ ...setting, name: 'mfaEnabled', value: 'true' });
        dispatch(createSettingThunk(data, setting.id));
      }
    }
  }, [mfaRegistered]);

  const handleEnableMfa = () => {
    dispatch({ type: actions.SET_MFA, payload: { mfaEnabled: true } });
  };

  const disableMfa = async (mfaCode: string, reset: boolean) => {
    // handle disable or reset mfa
    if (user) {
      const setting = currentSettings.mfaEnabled;
      const data = Settings.createGraphqlObject({ ...setting, value: 'false' });
      await axios({
        method: 'POST',
        url: `${config.backendUrl}/cognitoDisableTotpMfa`,
        headers: {
          authorization: jwt ? `${jwt}` : '',
        },
        data: {
          email: user?.email,
          mfaCode,
          Session: session || undefined,
          friendlyDeviceName: i18n('app.title'),
        },
      })
        .then(() => {
          if (!reset && mfaEnabled) dispatch(updateSettingThunk(data, setting.id));
          dispatch({
            type: actions.SET_MFA,
            payload: {
              mfaEnabled: reset,
              mfaRegistered: false,
            },
          });
        })
        .catch((err) => {
          log.error(err);
          message.error(i18n('mfa.disable.error'));
        });
    }
  };

  const handleDisableMfa = (reset: boolean) => {
    confirm({
      title: reset ? i18n('mfa.reset.title') : i18n('mfa.disable.title'),
      icon: <ExclamationCircleOutlined />,
      content: (
        <div>
          <p>{reset ? i18n('mfa.reset.instructions') : i18n('mfa.disable.instructions')}</p>
          <Form form={form}>
            <Form.Item name="mfaCode" label={i18n('mfa.label')} rules={[{ required: true, message: reset ? i18n('mfa.reset.warning') : i18n('mfa.disable.warning') }]}>
              <Input />
            </Form.Item>
          </Form>
        </div>
      ),
      okText: reset ? i18n('common.reset') : i18n('common.disable'),
      onOk() {
        form
          .validateFields()
          .then((values) => {
            form.resetFields();
            disableMfa(values.mfaCode, reset);
          })
          .catch((info) => {
            log.warn('Validate Failed:', info);
            message.error(reset ? i18n('mfa.reset.warning') : i18n('mfa.disable.warning'));
          });
      },
    });
  };

  return (
    <section id="securitySettings" className="settings-section security">
      <h2 data-cy="setting-section-title">{i18n('settings.security.title')}</h2>
      <section className="settings-section-body security">
        <main className="seatsMain settings-wide-column">
          <h3 data-cy="mfa-title">{i18n('settings.security.mfa')}</h3>
          <section className="mfa-section" data-cy="mfa-section">
            <p>{`${i18n('settings.security.enable')} ${hasOwnerAccess ? i18n('settings.security.require') : ''}`}</p>
            {mfaRegistered && <p>{i18n('settings.security.reset')}</p>}
            {hasOwnerAccess ? <p>{i18n('settings.security.lostDevice.admin')}</p> : <p>{i18n('settings.security.lostDevice.other')}</p>}
            <section className="mfa-buttons" data-cy="mfa-buttons">
              {!mfaRegistered && (
                <Button className="btn-primary" size="large" onClick={handleEnableMfa}>
                  {hasOwnerAccess ? i18n('settings.security.requireLabel') : i18n('settings.security.enableLabel')}
                </Button>
              )}
              {/* {mfaEnabled && !mfaRegistered && (
                <Button className="btn-primary" size="large" disabled={isDisabled} onClick={handleEnableMfa}>
                  Register 2FA
                </Button>
              )} */}
              {mfaRegistered && (
                <Button className="btn-primary" size="large" onClick={() => handleDisableMfa(true)}>
                  {i18n('settings.security.resetLabel')}
                </Button>
              )}
              {mfaRegistered && (
                <Tooltip title={mfaEnabled ? i18n('settings.security.required') : ''}>
                  <Button className="btn-danger" size="large" disabled={mfaEnabled && !hasOwnerAccess} onClick={() => handleDisableMfa(false)}>
                    {i18n('settings.security.disableLabel')}
                  </Button>
                </Tooltip>
              )}
            </section>
          </section>
        </main>
        <aside className="securitySidebar settings-thin-column" />
      </section>
    </section>
  );
};
