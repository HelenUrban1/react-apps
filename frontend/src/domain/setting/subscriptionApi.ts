import gql from 'graphql-tag';
import moment from 'moment';
import { ComponentInput, Subscription, SubscriptionUpdateInput } from './subscriptionTypes';

export interface SubscriptionInput {
  accountId: string; // IX3 account ID
  billingId: string; // IX3 billing contact user ID
  paymentSystemId: string; // Chargify account ID
  billingToken: string; // Chargify API key
  providerProductId: number; // Chargify Product ID
  providerProductHandle: string; // Chargify Product Handle
  providerFamilyHandle: string; // Chargify Product Family ID
  status?: string;
  canceledAt: Date | null;
  previousStatus?: string;
  cancellationMethod?: string;
  cancellationMessage?: string;
  cancelAtEndOfPeriod?: boolean;
  delayedCancelAt?: Date;
}

export interface SubscriptionFind {
  subscriptionFind: Subscription;
}
export interface SubscriptionFindVars {
  accountId: string;
}
export interface SubscriptionInitBilling {
  subscriptionBillingInitData: {
    publicKey: string;
    siteUrl: string;
    securityToken: string;
    catalog: string;
    subscription?: Subscription;
  };
}

export interface SubscriptionList {
  subscriptionList: {
    count: number;
    rows: Subscription[];
  };
}

export interface SubscriptionFilterInput {
  id: string;
  createdAtRange?: Date[];
}

type SubscriptionOrderByEnum = 'id_ASC' | 'id_DESC';

export interface SubscriptionListVars {
  filter?: SubscriptionFilterInput;
  orderBy?: SubscriptionOrderByEnum;
  limit: number;
  offset: number;
}

export interface SubscriptionEdit {
  subscriptionUpdate: Subscription;
}

export interface SubscriptionEditVars {
  id: string;
  data: SubscriptionUpdateInput;
  type: string;
  components?: ComponentInput[];
  componentHandle?: string;
  billingToken?: string;
}

export const SubscriptionAPI = {
  mutate: {
    edit: gql`
      mutation SUBSCRIPTION_EDIT($id: String!, $data: SubscriptionInput!, $type: String!, $components: [SubscriptionComponentInput], $billingToken: String, $componentHandle: String) {
        subscriptionUpdate(id: $id, data: $data, type: $type, components: $components, billingToken: $billingToken, componentHandle: $componentHandle) {
          id
          accountId
          billingId
          companyId
          enforcePlan
          providerProductId
          providerProductHandle
          providerFamilyHandle
          status
          activatedAt
          paidUserSeatsUsed
          drawingsUsed
          partsUsed
          measurementsUsed
          components
          pendingComponents
          enforcePlan
          paymentType
          paymentSystemId
          billingPeriod
          renewalDate
          pendingTier
          pendingBillingPeriod
          pendingBillingAmountInCents
          currentPeriodStartedAt
          currentPeriodEndsAt
          currentBillingAmountInCents
          cancelAtEndOfPeriod
          canceledAt
          previousStatus
          status
          trialEndedAt
          trialStartedAt
          expiresAt
          updatedAt
          automaticallyResumeAt
          balanceInCents
          cancellationMessage
          cancellationMethod
          couponCode
          couponCodes
          couponUseCount
          couponUsesAllowed
          createdAt
          delayedCancelAt
          netTerms
          nextAssessmentAt
          nextProductHandle
          nextProductId
          offerId
          payerId
          paymentCollectionMethod
          plan
          planNotes
          productPriceInCents
          productPricePointId
          productVersionNumber
          reasonCode
          receivesInvoiceEmails
          referralCode
          signupPaymentId
          signupRevenue
          snapDay
          storedCredentialTransactionId
          totalRevenueInCents
          cardId
          maskedCardNumber
          cardType
          expirationMonth
          expirationYear
          customerId
          termsAcceptedOn
        }
      }
    `,
    addPaymentMethod: gql`
      mutation SUBSCRIPTION_BILLING_PAYMENT_METHOD_CREATE($id: String!, $data: SubscriptionBillingPaymentMethodInput!) {
        subscriptionBillingPaymentMethodCreate(id: $id, data: $data) {
          id
          cardId
          maskedCardNumber
          cardType
          expirationMonth
          expirationYear
          customerId
          paymentCollectionMethod
        }
      }
    `,
    removePaymentMethod: gql`
      mutation SUBSCRIPTION_BILLING_PAYMENT_METHOD_REMOVE($id: String!, $customer: String!) {
        subscriptionBillingPaymentMethodRemove(id: $id, customer: $customer) {
          id
          cardId
          maskedCardNumber
          cardType
          expirationMonth
          expirationYear
          customerId
          paymentCollectionMethod
        }
      }
    `,
  },
  query: {
    find: gql`
      query SUBSCRIPTION_FIND($accountId: String!) {
        subscriptionFind(accountId: $accountId) {
          id
          accountId
          billingId
          companyId
          enforcePlan
          providerProductId
          providerProductHandle
          providerFamilyHandle
          status
          activatedAt
          paidUserSeatsUsed
          drawingsUsed
          partsUsed
          measurementsUsed
          components
          pendingComponents
          enforcePlan
          paymentType
          paymentSystemId
          billingPeriod
          renewalDate
          pendingTier
          pendingBillingPeriod
          pendingBillingAmountInCents
          currentPeriodStartedAt
          currentPeriodEndsAt
          currentBillingAmountInCents
          cancelAtEndOfPeriod
          canceledAt
          previousStatus
          status
          trialEndedAt
          trialStartedAt
          expiresAt
          updatedAt
          automaticallyResumeAt
          balanceInCents
          cancellationMessage
          cancellationMethod
          couponCode
          couponCodes
          couponUseCount
          couponUsesAllowed
          createdAt
          delayedCancelAt
          netTerms
          nextAssessmentAt
          nextProductHandle
          nextProductId
          offerId
          payerId
          paymentCollectionMethod
          plan
          planNotes
          productPriceInCents
          productPricePointId
          productVersionNumber
          reasonCode
          receivesInvoiceEmails
          referralCode
          signupPaymentId
          signupRevenue
          snapDay
          storedCredentialTransactionId
          totalRevenueInCents
          cardId
          maskedCardNumber
          cardType
          expirationMonth
          expirationYear
          customerId
          termsAcceptedOn
        }
      }
    `,
    list: gql`
      query SUBSCRIPTION_LIST($filter: SubscriptionFilterInput, $orderBy: SubscriptionOrderByEnum, $limit: Int, $offset: Int) {
        subscriptionList(filter: $filter, orderBy: $orderBy, limit: $limit, offset: $offset) {
          count
          rows {
            id
            accountId
            billingId
            companyId
            enforcePlan
            providerProductId
            providerProductHandle
            providerFamilyHandle
            paymentSystemId
            paymentType
            billingPeriod
            renewalDate
            pendingTier
            pendingBillingPeriod
            pendingBillingAmountInCents
            status
            activatedAt
            paidUserSeatsUsed
            drawingsUsed
            partsUsed
            measurementsUsed
            components
            pendingComponents
            enforcePlan
            currentPeriodStartedAt
            currentPeriodEndsAt
            currentBillingAmountInCents
            cancelAtEndOfPeriod
            canceledAt
            previousStatus
            status
            trialEndedAt
            trialStartedAt
            expiresAt
            updatedAt
          }
        }
      }
    `,
    init: gql`
      query SUBSCRIPTION_BILLING_INIT_DATA($accountId: String!) {
        subscriptionBillingInitData(accountId: $accountId) {
          publicKey
          siteUrl
          securityToken
          catalog
          subscription {
            id
            accountId
            billingId
            companyId
            enforcePlan
            providerProductId
            providerProductHandle
            providerFamilyHandle
            status
            activatedAt
            paidUserSeatsUsed
            drawingsUsed
            partsUsed
            measurementsUsed
            components
            pendingComponents
            enforcePlan
            paymentType
            paymentSystemId
            billingPeriod
            renewalDate
            pendingTier
            pendingBillingPeriod
            pendingBillingAmountInCents
            currentPeriodStartedAt
            currentPeriodEndsAt
            currentBillingAmountInCents
            cancelAtEndOfPeriod
            canceledAt
            previousStatus
            status
            trialEndedAt
            trialStartedAt
            expiresAt
            updatedAt
            automaticallyResumeAt
            balanceInCents
            cancellationMessage
            cancellationMethod
            couponCode
            couponCodes
            couponUseCount
            couponUsesAllowed
            createdAt
            delayedCancelAt
            netTerms
            nextAssessmentAt
            nextProductHandle
            nextProductId
            offerId
            payerId
            paymentCollectionMethod
            plan
            planNotes
            productPriceInCents
            productPricePointId
            productVersionNumber
            reasonCode
            receivesInvoiceEmails
            referralCode
            signupPaymentId
            signupRevenue
            snapDay
            storedCredentialTransactionId
            totalRevenueInCents
            cardId
            maskedCardNumber
            cardType
            expirationMonth
            expirationYear
            customerId
            termsAcceptedOn
          }
        }
      }
    `,
  },
  createGraphqlObject: (subscription: Subscription) => {
    const data: SubscriptionUpdateInput = {
      accountId: subscription.accountId,
      billingId: subscription.billingId,
      companyId: subscription.companyId,
      billingPeriod: subscription.billingPeriod,
      renewalDate: subscription.renewalDate,
      pendingTier: subscription.pendingTier,
      pendingBillingPeriod: subscription.pendingBillingPeriod,
      pendingBillingAmountInCents: subscription.pendingBillingAmountInCents,
      enforcePlan: subscription.enforcePlan,
      providerProductId: subscription.providerProductId,
      providerProductHandle: subscription.providerProductHandle,
      providerFamilyHandle: subscription.providerFamilyHandle,
      previousStatus: subscription.previousStatus,
      status: subscription.status,
      activatedAt: subscription.activatedAt,
      paidUserSeatsUsed: subscription.paidUserSeatsUsed,
      drawingsUsed: subscription.drawingsUsed,
      partsUsed: subscription.partsUsed,
      measurementsUsed: subscription.measurementsUsed,
      pendingComponents: subscription.pendingComponents,
      components: subscription.components,
      currentPeriodStartedAt: subscription.currentPeriodStartedAt,
      currentPeriodEndsAt: subscription.currentPeriodEndsAt,
      cancelAtEndOfPeriod: subscription.cancelAtEndOfPeriod,
      canceledAt: subscription.canceledAt || undefined,
      trialEndedAt: subscription.trialEndedAt,
      trialStartedAt: subscription.trialStartedAt,
      expiresAt: subscription.expiresAt,
      updatedAt: subscription.updatedAt,
      automaticallyResumeAt: subscription.automaticallyResumeAt,
      balanceInCents: subscription.balanceInCents,
      cancellationMessage: subscription.cancellationMessage,
      cancellationMethod: subscription.cancellationMethod,
      couponCode: subscription.couponCode,
      couponCodes: subscription.couponCodes,
      couponUseCount: subscription.couponUseCount,
      couponUsesAllowed: subscription.couponUsesAllowed,
      createdAt: subscription.createdAt,
      currentBillingAmountInCents: subscription.currentBillingAmountInCents,
      delayedCancelAt: subscription.delayedCancelAt,
      netTerms: subscription.netTerms,
      nextAssessmentAt: subscription.nextAssessmentAt,
      nextProductHandle: subscription.nextProductHandle,
      nextProductId: subscription.nextProductId,
      offerId: subscription.offerId,
      payerId: subscription.payerId,
      paymentCollectionMethod: subscription.paymentCollectionMethod,
      paymentSystemId: subscription.paymentSystemId,
      paymentType: subscription.paymentType,
      plan: subscription.plan,
      planNotes: subscription.planNotes,
      productPriceInCents: subscription.productPriceInCents,
      productPricePointId: subscription.productPricePointId,
      productVersionNumber: subscription.productVersionNumber,
      reasonCode: subscription.reasonCode,
      receivesInvoiceEmails: subscription.receivesInvoiceEmails,
      referralCode: subscription.referralCode,
      signupPaymentId: subscription.signupPaymentId,
      signupRevenue: subscription.signupRevenue,
      snapDay: subscription.snapDay,
      storedCredentialTransactionId: subscription.storedCredentialTransactionId,
      totalRevenueInCents: subscription.totalRevenueInCents,
      cardId: subscription.cardId,
      maskedCardNumber: subscription.maskedCardNumber,
      cardType: subscription.cardType,
      expirationMonth: subscription.expirationMonth,
      expirationYear: subscription.expirationYear,
      customerId: subscription.customerId,
      termsAcceptedOn: subscription.termsAcceptedOn,
    };
    return data;
  },
};

export const cancellationInput = (sub: Subscription) => {
  const endDate = new Date(moment(sub.currentPeriodEndsAt).format());
  const now = new Date(moment().format());
  const input: SubscriptionInput = {
    accountId: sub.accountId,
    billingId: sub.billingId,
    paymentSystemId: sub.paymentSystemId,
    billingToken: '11111',
    providerProductId: sub.providerProductId,
    providerProductHandle: sub.providerProductHandle,
    providerFamilyHandle: sub.providerFamilyHandle,
    // status: 'active',
    canceledAt: null,
    previousStatus: sub.status,
    cancellationMethod: 'Customer Initiated',
    cancellationMessage: 'In-App Cancellation',
    cancelAtEndOfPeriod: true,
    delayedCancelAt: endDate,
  };

  return input;
};

export const reactivationInput = (sub: Subscription) => {
  const input: SubscriptionInput = {
    accountId: sub.accountId,
    billingId: sub.billingId,
    paymentSystemId: sub.paymentSystemId,
    billingToken: '11111',
    providerProductId: sub.providerProductId,
    providerProductHandle: sub.providerProductHandle,
    providerFamilyHandle: sub.providerFamilyHandle,
    status: 'Active',
    canceledAt: null,
    previousStatus: sub.status,
    cancellationMethod: 'Customer Initiated',
    cancellationMessage: 'In-App Reactivation',
    cancelAtEndOfPeriod: false,
    delayedCancelAt: undefined,
  };

  return input;
};

export default SubscriptionAPI;
