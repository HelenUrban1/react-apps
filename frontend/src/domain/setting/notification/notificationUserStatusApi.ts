import gql from 'graphql-tag';
import { NotificationUserStatus, NotificationUserStatusUpdateInput } from 'domain/setting/notification/notificationUserStatusTypes';

// Type definitions for the returned api data
export interface NotificationFind {
  notificationFind: Notification;
}
export interface NotificationList {
  notificationList: {
    count: number;
    rows: Notification[];
  };
}

export interface NotificationUserStatusEdit {
  notificationUpdate: NotificationUserStatusUpdateInput;
}

export const NotificationUserStatuses = {
  mutate: {
    edit: gql`
      mutation NOTIFICATION_USER_STATUS_UPDATE($id: String!, $data: NotificationUserStatusInput!) {
        notificationUserStatusUpdate(id: $id, data: $data) {
          id
          readAt
          archivedAt
          createdAt
          updatedAt
        }
      }
    `,
  },
  query: {
    find: gql`
      query NOTIFICATION_FIND($id: String!) {
        notificationFind(id: $id) {
          id
          readAt
          archivedAt
          notificationId
          userId
          createdAt
          updatedAt
        }
      }
    `,
  },

  createGraphqlInput: (data: NotificationUserStatus) => {
    const input: NotificationUserStatusUpdateInput = {
      readAt: data.readAt || undefined,
      archivedAt: data.archivedAt || undefined,
      notification: data.notification || undefined,
      user: data.user || undefined,
    };
    return input;
  },
};
