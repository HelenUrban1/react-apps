// The schema of Notification objects when returned from the DB
import { NotificationUserStatus } from './notificationUserStatusTypes';

export interface Notification {
  id: string;
  title: null | string;
  content: null | string;
  action: null | string;
  actionAt: null | Date;
  priority: number;
  relationId: null | string;
  relationType: null | string;
  actorId: null | string;
  siteId: null | string;

  // Collections populated by Sequelize
  userStatuses?: NotificationUserStatus[];
  createdAt: null | Date;
  updatedAt: null | Date;

  [key: string]: NotificationUserStatus[] | Date | string | number | boolean | null | undefined;
}

export interface NotificationTableFilter {
  [key: string]: Array<string> | any;
}

export interface NotificationTableSorter {
  columnKey?: string | undefined;
  order?: 'descend' | 'ascend' | null | undefined;
}

// The NotificationChange type represents a change to one or more values
// which can be used to construct a valid NotificationUpdateInput (see below)
// All fields are optional to allow partial updates without implicit assignment of undefined
export interface NotificationChange {
  title?: string;
  content?: string;
  action?: string;
  actionAt?: Date;
  priority?: number;
  relationId?: string;
  relationType?: string;
  actorId?: string;
  siteId: string;

  // Collections populated by Sequelize
  userStatuses?: string[]; // notificationUserStatus.id
  [key: string]: string | string[] | boolean | number | Date | undefined;
}

// This is the 'NotificationInput' type which is expected by the backend's update method's
// The backend's 'NotificationInput' is at backend/src/api/notifcation/types/notificationInput.js
export interface NotificationUpdateInput {
  title?: string;
  content?: string;
  action?: string;
  actionAt?: Date;
  priority: number;
  relationId?: string;
  relationType?: string;
  actorId?: string;
  siteId?: string;

  // Collections populated by Sequelize
  userStatuses: string[]; // notificationUserStatus.id
  [key: string]: Date | number | boolean | string | string[] | undefined | null;
}
