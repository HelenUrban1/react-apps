import gql from 'graphql-tag';
import log from 'modules/shared/logger';
import { Notification, NotificationUpdateInput } from 'domain/setting/notification/notificationTypes';

// Type definitions for the returned api data
export interface NotificationFind {
  notificationFind: Notification;
}
export interface NotificationList {
  notificationList: {
    count: number;
    rows: Notification[];
  };
}

export interface NotificationEdit {
  notificationUpdate: NotificationUpdateInput;
}

export const Notifications = {
  mutate: {
    edit: gql`
      mutation NOTIFICATION_UPDATE($id: String!, $data: NotificationInput!) {
        notificationUpdate(id: $id, data: $data) {
          id
          title
          content
          action
          actionAt
          priority
          relationId
          relationType
          actorId
          siteId
          notificationUserStatuses {
            id
            readAt
            archivedAt
            notificationId
            userId
            createdAt
            updatedAt
          }
          createdById
          updatedById
          createdAt
          updatedAt
        }
      }
    `,
  },
  query: {
    find: gql`
      query NOTIFICATION_FIND($id: String!) {
        notificationFind(id: $id) {
          id
          title
          content
          action
          actionAt
          priority
          relationId
          relationType
          actorId
          siteId
          notificationUserStatuses {
            id
            readAt
            archivedAt
            notificationId
            userId
            createdAt
            updatedAt
          }
          createdById
          updatedById
          createdAt
          updatedAt
        }
      }
    `,
    list: gql`
      query NOTIFICATION_LIST($filter: NotificationFilterInput, $orderBy: NotificationOrderByEnum, $limit: Int, $offset: Int) {
        notificationList(filter: $filter, orderBy: $orderBy, limit: $limit, offset: $offset) {
          count
          rows {
            id
            title
            content
            action
            actionAt
            priority
            relationId
            relationType
            userStatuses {
              id
              readAt
              archivedAt
              user
              createdAt
              updatedAt
            }
            createdAt
            updatedAt
          }
        }
      }
    `,
    count: gql`
      query NOTIFICATION_COUNT($filter: NotificationFilterInput) {
        notificationCount(filter: $filter) {
          count
        }
      }
    `,
  },

  createGraphqlInput: (data: Notification) => {
    if (!data || !data.userStatuses) {
      log.error('No data was passed to create an input', data);
      return null;
    }
    const userStatuses = data.userStatuses.map((userStatus) => userStatus.id);
    const input: NotificationUpdateInput = {
      title: data.title || undefined,
      content: data.content || undefined,
      action: data.action || undefined,
      actionAt: data.actionAt || undefined,
      priority: data.priority,
      relationId: data.relationId || undefined,
      relationType: data.relationType || undefined,
      actorId: data.actorId || undefined,
      siteId: data.siteId || undefined,
      userStatuses,
    };
    return input;
  },
};
