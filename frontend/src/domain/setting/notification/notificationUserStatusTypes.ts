// The schema of NotificationUserStatus objects when returned from the DB
export interface NotificationUserStatus {
  id: string;
  readAt: null | Date;
  archivedAt: null | Date;
  user: null | string;
  notification: null | string;
  createdAt: null | Date;
  updatedAt: null | Date;

  [key: string]: NotificationUserStatus[] | Date | string | number | boolean | null | undefined;
}

// The NotificationUserStatusChange type represents a change to one or more values
// which can be used to construct a valid NotificationUserStatusUpdateInput (see below)
// All fields are optional to allow partial updates without implicit assignment of undefined
export interface NotificationUserStatusChange {
  readAt?: Date;
  archivedAt?: Date;
  user?: string;
  notification?: string;
  [key: string]: string | string[] | boolean | number | Date | undefined;
}

// This is the 'NotificationUserStatusInput' type which is expected by the backend's update method's
// The backend's 'NotificationUserStatusInput' is at backend/src/api/notifcationUserStatus/types/notificationUserStatusInput.js
export interface NotificationUserStatusUpdateInput {
  readAt?: Date;
  archivedAt?: Date;
  user?: string;
  notification?: string;
  [key: string]: Date | number | boolean | string | string[] | undefined | null;
}
