import React from 'react';
import { Table, Tooltip } from 'antd';
import { i18n } from 'i18n';
import { useSelector } from 'react-redux';
import { FilterValue, SorterResult, TablePaginationConfig } from 'antd/lib/table/interface';
import { AppState } from 'modules/state';
import moment from 'moment';
import { Notification } from '../notificationTypes';

interface Props {
  data: Notification[];
  pageCount: number;
  handleChange: (pagination: TablePaginationConfig, filters: Record<string, FilterValue | null>, sorter: SorterResult<Notification> | SorterResult<Notification>[], extra: any) => void;
  loading: boolean;
  rowSelection: any;
  handleActionLink: (notification: Notification) => Promise<void>;
}

export const NotificationsTable: React.FC<Props> = ({
  data, //
  pageCount,
  handleChange,
  loading,
  rowSelection,
  handleActionLink,
}) => {
  const { currentUser } = useSelector((state: AppState) => state.auth);
  const { tablePage } = useSelector((state: AppState) => state.notification);

  const buildLinks = (action: string, notification: Notification) => {
    const actions = action ? action.split(',') : [];

    return actions.map((a) => {
      switch (a) {
        case 'View Part':
          return (
            <button key={`${a}-view`} type="button" className="link" onClick={() => handleActionLink(notification)}>
              {i18n(`entities.notification.actions.viewPart`)}
            </button>
          );
        case 'Review features':
          return (
            <button key={`${a}-view`} type="button" className="link" onClick={() => handleActionLink(notification)}>
              {i18n(`entities.notification.actions.reviewFeatures`)}
            </button>
          );
        default:
          return <span key={`${a}-view`} />;
      }
    });
  };
  const columns: any = [
    {
      title: i18n('entities.notification.fields.content'),
      className: 'ix-table-cell testing-notification-content-header',
      dataIndex: 'title',
      key: 'title',
      sorter: false,
    },
    {
      title: i18n('entities.notification.fields.createdAt'),
      className: 'ix-table-cell testing-edited-header',
      dataIndex: 'createdAt',
      key: 'createdAt',
      sorter: false,
      render: (date: string) => (
        <Tooltip placement="topLeft" title={date}>
          {moment(date).fromNow()}
        </Tooltip>
      ),
    },
    {
      title: i18n('entities.notification.fields.resolution'),
      className: 'ix-table-cell testing-edited-header',
      dataIndex: 'action',
      key: 'action',
      sorter: false,
      render: (action: string, notification: Notification) => buildLinks(action, notification),
    },
  ];

  return (
    <Table
      className="main-table"
      columns={columns}
      dataSource={data}
      rowKey="id"
      rowClassName={(record: Notification) => `ix-table-row ${record?.userStatuses?.find((us) => us.user === currentUser?.id)?.readAt === null ? ' ix-table-row-unread' : ' ix-table-row-read'}`}
      onChange={handleChange}
      rowSelection={rowSelection}
      pagination={{
        total: pageCount,
        current: tablePage, // from redux
        pageSize: 10, // TODO: Make pagesize configurable
        showSizeChanger: false,
      }}
      loading={loading}
    />
  );
};
