import React, { useState, useEffect, Key } from 'react';
import { useQuery, useMutation } from '@apollo/client';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { Notifications, NotificationList } from 'domain/setting/notification/notificationApi';
import { NotificationUserStatuses, NotificationUserStatusEdit } from 'domain/setting/notification/notificationUserStatusApi';
import { AppState } from 'modules/state';
import { i18n } from 'i18n';
import { Row, Col, Checkbox, Button, TablePaginationConfig } from 'antd';
import '../notificationStyles.less';
import { doSetTableFilter, doSetTablePage, doSetTableSorter, doSetSelectedNotifications, doSetUnreadCount } from 'modules/notification/notificationActions';
import authSelectors from 'modules/auth/authSelectors';
import config from 'config';
import { io } from 'socket.io-client';
import { selectSettings } from 'modules/session/sessionSelectors';
import { NotificationsTable } from './NotificationsTable';
import { Notification } from '../notificationTypes';
import { NotificationUserStatus } from '../notificationUserStatusTypes';

const NotificationsList: React.FC = () => {
  const { tableSorter, tableFilter, selectedNotifications, unreadCount } = useSelector((state: AppState) => state.notification);
  const { jwt } = useSelector((state: AppState) => state.auth);
  const settings = useSelector((state: AppState) => selectSettings(state));

  const history = useHistory();
  const user = useSelector((state: AppState) => authSelectors.selectCurrentUser(state));

  const [error, setError] = useState<any>();
  const [selectedNotificationCount, setSelectedNotificationCount] = useState<number>();

  const dispatch = useDispatch();
  const [selectedNotificationKeys, setSelectedNotificationKeys] = useState({
    selectedRowKeys: [] as Key[],
  });

  const { selectedRowKeys } = selectedNotificationKeys;
  const rowSelection = {
    selectedRowKeys,
    onChange: (selectedNotificationRowKeys: Key[], selectedRows: Notification[]) => {
      setSelectedNotificationKeys({
        ...selectedNotificationKeys,
        selectedRowKeys: selectedNotificationRowKeys,
      });

      dispatch(doSetSelectedNotifications(selectedRows));
    },
  };

  const deselectAllRows = () => {
    setSelectedNotificationKeys({
      ...selectedNotificationKeys,
      selectedRowKeys: [],
    });

    dispatch(doSetSelectedNotifications([]));
  };

  // Always sorting by createdAt_DESC for now, will eventually take priority into account, user won't be selecting order of notifications
  const defaultSort = 'createdAt_DESC';

  // Always refetch with the same query params
  const [notificationListQueryParams, setNotificationListQueryParams] = useState<{ filter: {}; orderBy: string; limit: number; offset: number }>({
    filter: { userStatuses: { user: user?.id, archivedAt: false, readAt: true } },
    orderBy: defaultSort,
    limit: 10,
    offset: 0,
  });

  // Graphql query to pull back list of notifications
  const {
    loading,
    error: notificationError,
    data,
    fetchMore,
    refetch,
  } = useQuery<NotificationList>(Notifications.query.list, {
    variables: notificationListQueryParams,
    fetchPolicy: 'cache-and-network',
  });

  const findNotifications = async () => {
    setNotificationListQueryParams({
      ...notificationListQueryParams,
      filter: {
        ...notificationListQueryParams.filter,
        ...tableFilter,
      },
      orderBy: defaultSort,
    });
    if (data && data.notificationList && data.notificationList.count !== data.notificationList.rows.length) {
      refetch({
        variables: {
          ...notificationListQueryParams,
          orderBy: defaultSort,
        },
        fetchPolicy: 'no-cache',
      });
    }
  };

  const notificationUpdateHandler = () => {
    if (refetch) {
      refetch({
        variables: {
          ...notificationListQueryParams,
        },
        fetchPolicy: 'no-cache',
      });
    }
  };

  useEffect(() => {
    if (settings.autoballoonEnabled?.value === 'true' && config.wsUrl) {
      // Update notification list when notifications or notificationUserStatus updates so we can see updated statuses
      const socket = io(config.wsUrl, { transports: ['websocket'], withCredentials: true, auth: { token: `Bearer ${jwt}` }, path: config.wsPath });
      socket.on('notification.create', notificationUpdateHandler);
      socket.on('notification.update', notificationUpdateHandler);
      socket.on('notificationUserStatus.update', notificationUpdateHandler);
      return () => {
        socket.off('notification.create', notificationUpdateHandler);
        socket.off('notification.update', notificationUpdateHandler);
        socket.off('notificationUserStatus.update', notificationUpdateHandler);
        socket.disconnect();
      };
    }
    return () => {};
  }, []);

  const { refetch: refetchCount } = useQuery<NotificationList>(Notifications.query.list, {
    variables: {
      filter: { userStatuses: { user: user?.id, archivedAt: false, readAt: false } },
      orderBy: defaultSort,
      limit: 100,
      offset: 0,
    },
    fetchPolicy: 'cache-and-network',
    notifyOnNetworkStatusChange: true, // causes onCompleted to be run during refetch
    onCompleted: (result) => {
      dispatch(doSetUnreadCount(result?.notificationList?.count || 0));
      refetch({
        variables: {
          ...notificationListQueryParams,
          orderBy: defaultSort,
        },
        fetchPolicy: 'no-cache',
      });
    },
  });

  const findUnreadCount = async () => {
    refetchCount({
      variables: {
        filter: { userStatuses: { user: user?.id, archivedAt: false, readAt: false } },
        orderBy: defaultSort,
        limit: 100,
        offset: 0,
      },
      fetchPolicy: 'no-cache',
    });
  };

  useEffect(() => {
    // tableSorter will have {columnKey: string "title" | "createdAt" | "action", order: "ascend" | "descend"
    if (tableFilter !== notificationListQueryParams.filter) {
      deselectAllRows();
      findNotifications();
    }
  }, [tableSorter, tableFilter]);

  useEffect(() => {
    setSelectedNotificationCount(selectedNotifications.length);
  }, [selectedNotifications]);

  // ------------------ Methods used by NotificationsTable --------------------

  // / Pagination of table records
  const handlePagination = (pageNumber: number) => {
    // set page number to send with report
    dispatch(doSetTablePage(pageNumber));
    if (!data || !data.notificationList.rows) {
      return;
    }

    // If turning to a page we haven't fetched results for yet, fetch the next 10 results
    if (pageNumber - 1 >= data.notificationList.rows.length / 10 && fetchMore) {
      fetchMore({
        variables: {
          offset: data.notificationList.rows.length,
          limit: pageNumber * 10 - data.notificationList.rows.length,
        },
        updateQuery: (prev, { fetchMoreResult }) => {
          if (!fetchMoreResult || !prev.notificationList) return prev;
          return {
            ...prev,
            notificationList: {
              count: prev.notificationList.count,
              rows: [...prev.notificationList.rows, ...fetchMoreResult.notificationList.rows],
            },
          };
        },
      });
    }
  };

  // Handle change of anything on the table
  const handleChange = (pagination: TablePaginationConfig, filters: any, sorter: any, extra: any) => {
    if (extra.action === 'paginate') {
      handlePagination(pagination.current || 1);
    } else if (extra.action === 'filter') {
      handlePagination(1);
      dispatch(doSetTableFilter(filters));
    } else if (extra.action === 'sort') {
      handlePagination(1);
      dispatch(doSetTableSorter({ columnKey: sorter.columnKey, order: sorter.order }));
    }
  };

  useEffect(() => {
    if (notificationError) {
      setError(notificationError);
    } else {
      setError(undefined);
    }
  }, [notificationError]);

  const handleHideReadCheckbox = () => {
    if (tableFilter?.userStatuses) {
      // toggle setting
      dispatch(doSetTableFilter({ userStatuses: { user: user?.id, archivedAt: tableFilter.userStatuses.archivedAt, readAt: !tableFilter.userStatuses.readAt } }));
    } else {
      // set for first time
      dispatch(doSetTableFilter({ userStatuses: { user: user?.id, archivedAt: false, readAt: false } }));
    }

    // reset to first page after changing filter
    handlePagination(1);
  };

  const handleViewArchivedCheckbox = () => {
    if (tableFilter?.userStatuses) {
      // toggle setting
      dispatch(doSetTableFilter({ userStatuses: { user: user?.id, archivedAt: !tableFilter.userStatuses.archivedAt, readAt: tableFilter.userStatuses.readAt } }));
    } else {
      // set for first time
      dispatch(doSetTableFilter({ userStatuses: { user: user?.id, archivedAt: true, readAt: true } }));
    }

    // reset to first page after changing filter
    handlePagination(1);
  };

  const [editNotificationUserStatus] = useMutation<NotificationUserStatusEdit>(NotificationUserStatuses.mutate.edit);

  const handleActionLink = async (notification: Notification) => {
    if (notification?.userStatuses?.length === 1) {
      const userStatus = { ...notification?.userStatuses?.[0], notification: notification.id };
      const newUserStatus = { notification: userStatus.notification, archivedAt: userStatus.archivedAt, readAt: userStatus?.readAt || new Date(), user: userStatus.user };
      await editNotificationUserStatus({
        variables: {
          id: userStatus?.id,
          data: newUserStatus,
        },
      });
    }

    if (notification.action === 'View Part' || notification.action?.includes('Review features')) {
      history.push(`/parts/${notification.relationId}`);
    }
  };

  const handleMarkAsReadButton = async () => {
    // Each notification should have a single userStatus for the current user, that we're going to add a readAt date to
    const userStatuses = selectedNotifications.filter((notification) => notification?.userStatuses?.length === 1).map((notification) => ({ ...notification?.userStatuses?.[0], notification: notification.id }));

    const editUserStatuses = [];
    for (let index = 0; index < userStatuses.length; index++) {
      const userStatus = userStatuses[index] as NotificationUserStatus;
      if (userStatus) {
        const newUserStatus = { notification: userStatus.notification, archivedAt: userStatus.archivedAt, readAt: userStatus?.readAt || new Date(), user: userStatus.user };
        editUserStatuses.push(
          editNotificationUserStatus({
            variables: {
              id: userStatus?.id,
              data: newUserStatus,
            },
          }),
        );
      }
    }
    await Promise.all(editUserStatuses);

    refetch({
      variables: {
        ...notificationListQueryParams,
        orderBy: defaultSort,
      },
      fetchPolicy: 'no-cache',
    });

    deselectAllRows();
    findUnreadCount();
  };

  const handleArchiveButton = async () => {
    // Each notification should have a single userStatus for the current user, that we're going to add an archivedAt date to
    const userStatuses = selectedNotifications.filter((notification) => notification?.userStatuses?.length === 1).map((notification) => ({ ...notification?.userStatuses?.[0], notification: notification.id }));

    const editUserStatuses = [];
    for (let index = 0; index < userStatuses.length; index++) {
      const userStatus = userStatuses[index] as NotificationUserStatus;
      if (userStatus) {
        const newUserStatus = tableFilter?.userStatuses?.archivedAt
          ? { notification: userStatus.notification, archivedAt: null, readAt: userStatus.readAt, user: userStatus.user }
          : { notification: userStatus.notification, archivedAt: userStatus.archivedAt || new Date(), readAt: userStatus.readAt, user: userStatus.user };
        editUserStatuses.push(
          editNotificationUserStatus({
            variables: {
              id: userStatus?.id,
              data: newUserStatus,
            },
          }),
        );
      }
    }
    await Promise.all(editUserStatuses);

    refetch({
      variables: {
        ...notificationListQueryParams,
        orderBy: defaultSort,
      },
      fetchPolicy: 'no-cache',
    });

    deselectAllRows();
    findUnreadCount();
  };

  const markAsReadText = `${i18n('entities.notification.list.markAsRead')} (${selectedNotificationCount})`;
  const archiveText = tableFilter?.userStatuses?.archivedAt ? `${i18n('entities.notification.list.restore')} (${selectedNotificationCount})` : `${i18n('entities.notification.list.archive')} (${selectedNotificationCount})`;

  return (
    <section id="notificationsList" className="settings-section notifications-list-section">
      <h2 data-cy="setting-section-title">{i18n('entities.notification.list.title')}</h2>
      <section className="settings-section-body billing">
        <main className="billingMain settings-wide-column">
          <Row>
            <Col span={8}>
              <h3 style={{ display: 'inline-block', marginRight: 'auto' }}>{i18n('entities.notification.list.unreadNotifications', unreadCount)}</h3>
            </Col>
            <Col span={16} className="button-col">
              <Checkbox onClick={handleViewArchivedCheckbox}>{i18n('entities.notification.list.viewArchived')}</Checkbox>
              <Checkbox onClick={handleHideReadCheckbox}>{i18n('entities.notification.list.hideRead')}</Checkbox>
              <Button type="default" size="large" className="btn-default" data-cy="mark-notifications-as-read-btn" onClick={handleMarkAsReadButton}>
                {markAsReadText}
              </Button>
              <Button type="default" size="large" className="btn-default" data-cy="archive-notifications-btn" onClick={handleArchiveButton}>
                {archiveText}
              </Button>
            </Col>
          </Row>
          <Row>
            <Col span={24}>
              <section className="settings-subsection">
                <NotificationsTable //
                  rowSelection={rowSelection}
                  data={data?.notificationList?.rows || []}
                  pageCount={data ? data.notificationList.count : 0}
                  handleChange={handleChange}
                  loading={loading}
                  handleActionLink={handleActionLink}
                />
              </section>
            </Col>
          </Row>
        </main>
      </section>
    </section>
  );
};

export default NotificationsList;

// For performance testing
// NotificationsList.whyDidYouRender = {
//   logOnDifferentValues: false,
// };
