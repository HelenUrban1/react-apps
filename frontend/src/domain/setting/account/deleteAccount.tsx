import React from 'react';
import { Col, Row } from 'antd';
import { DeleteAccountModal } from './modals/DeleteAccountModal';
import { i18n } from 'i18n';

const DeleteAccount: React.FC = () => {
  return (
    <Row data-cy="setting-delete-account">
      <Col span={4} style={{ paddingTop: '5px' }}>
        <DeleteAccountModal />
      </Col>
      <Col span={16}>
        <span className="dangerText">{i18n('settings.account.deleteTitle')}</span>
      </Col>
    </Row>
  );
};

export default DeleteAccount;
