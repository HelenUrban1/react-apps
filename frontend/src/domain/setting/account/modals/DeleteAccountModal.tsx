import React, { useState } from 'react';
import { Col, Input, Row } from 'antd';
import { i18n } from 'i18n';
import { ContactInfo } from 'styleguide/PricingTable/ContactInfo';
import { SimpleModal } from 'styleguide/Modals/SimpleModal';
import { useDispatch, useSelector } from 'react-redux';
import authActions from 'modules/auth/authActions';
import subscriptionActions from 'modules/subscription/subscriptionActions';
import authSelectors from 'modules/auth/authSelectors';
import { AppState } from 'modules/state';
import { SubscriptionAPI, cancellationInput } from 'domain/setting/subscriptionApi';
import { selectSubscription } from 'modules/subscription/subscriptionSelectors';

export const DeleteAccountModal = () => {
  const [confirm, setConfirm] = useState('');
  const user = useSelector((state: AppState) => authSelectors.selectCurrentUser(state));
  const subscription = useSelector((state) => selectSubscription(state));
  const dispatch = useDispatch();

  const handleAccountDeletion = () => {
    //just logging out right now
    const input = cancellationInput(subscription);
    const updatedSubscription = SubscriptionAPI.createGraphqlObject({
      ...subscription,
      ...input,
    });
    dispatch(authActions.doDeleteAccount({ email: user?.email, accountId: user?.accountId, subscriptionId: subscription.id, subscription: updatedSubscription }));
    dispatch({ type: subscriptionActions.CLEAR });
  };

  // contact information
  const contact = (
    <Row className="info-text" data-cy="contact-message">
      <Col span={24}>
        <span>{i18n('settings.billing.info.cancel.contact')}</span>
        <ContactInfo />
      </Col>
    </Row>
  );

  const confirmation = (
    <Row>
      <Col span={24}>
        <h4>{i18n('settings.account.info.confirmation')}</h4>
        <Input
          data-cy="confirmation-input"
          placeholder={i18n('settings.account.input.confirm')}
          onChange={(e) => {
            setConfirm(e.target.value);
          }}
        />
      </Col>
    </Row>
  );

  const warning = (
    <Row data-cy="warning-message">
      <Col span={24}>
        <span className="span-bold">{i18n('settings.account.info.warning')}</span>
      </Col>
    </Row>
  );

  const body = (
    <div className="cancel-modal-body-text" data-cy="cancel-modal-body">
      {warning}
      <br />
      {contact}
      {confirmation}
    </div>
  );
  return (
    <SimpleModal
      buttonText={i18n('settings.account.buttons.delete')}
      title={i18n('settings.account.info.deleteAccount')}
      body={body}
      okText={i18n('settings.account.buttons.delete')}
      cancelText={i18n('common.close')}
      onAccept={handleAccountDeletion}
      extraClass="cancel-modal-btn"
      warning
      disabled={confirm !== i18n('settings.account.input.confirm')}
    />
  );
};
