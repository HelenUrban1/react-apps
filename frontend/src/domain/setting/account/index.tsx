import React from 'react';
import { i18n } from 'i18n';
import { selectSubscription } from 'modules/subscription/subscriptionSelectors';
import { useSelector } from 'react-redux';
import authSelectors from 'modules/auth/authSelectors';
import PermissionChecker from 'modules/auth/permissionChecker';
import Permissions from 'security/permissions';
import CompanyProfileForm from 'view/auth/CompanyProfileForm';
import { Divider } from 'antd';
import { AppState } from 'modules/state';
import DeleteAccount from './deleteAccount';

const Account = () => {
  const user = useSelector((state: AppState) => authSelectors.selectCurrentUser(state));
  const subscription = useSelector((state) => selectSubscription(state));
  const permissionValidator = new PermissionChecker(user);
  const userPermissions = {
    hasOwnership: permissionValidator.match(Permissions.values.accountOwner),
    hasManageAccess: permissionValidator.match(Permissions.values.accountManagement),
    hasBillingAccess: permissionValidator.match(Permissions.values.accountBillingAccess),
    hasReviewer: permissionValidator.match(Permissions.values.reviewer),
  };

  return (
    <section id="accountSettings" className="settings-section">
      <h2 data-cy="setting-section-title">{i18n('settings.account.title')}</h2>
      <section id="accountProfile" className="settings-subsection">
        <h4>{i18n('settings.account.companyEdit')}</h4>
        <CompanyProfileForm />
      </section>
      {userPermissions.hasOwnership && subscription?.status.toLowerCase() !== 'active' && (
        <section id="accountDeletion" className="settings-subsection">
          <Divider style={{ borderWidth: '2px', borderColor: '#b72e1a' }} className="dangerDivider" />
          <DeleteAccount />
        </section>
      )}
    </section>
  );
};

export default Account;
