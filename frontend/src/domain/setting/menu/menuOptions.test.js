import { getMenuOptions } from './menuOptions';

const owner = {
  roles: ['Owner'],
  emailVerified: true,
  id: 'e0a01779-c8ed-4090-bd44-9c1b2242fb75',
};

const admin = {
  roles: ['Admin'],
  emailVerified: true,
  id: 'e0a01779-c8ed-4090-bd44-9c1b2242fb75',
};

const billing = {
  roles: ['Billing'],
  emailVerified: true,
  id: 'e0a01779-c8ed-4090-bd44-9c1b2242fb75',
};

const collaborator = {
  roles: ['Collaborator'],
  emailVerified: true,
  id: 'e0a01779-c8ed-4090-bd44-9c1b2242fb75',
};

const ownerPermissions = {
  hasOwnership: true,
  hasManageAccess: true,
  hasBillingAccess: true,
  hasSphinxAccess: true,
};

const adminPermissions = {
  hasOwnership: false,
  hasManageAccess: true,
  hasBillingAccess: false,
  hasSphinxAccess: true,
};

const billingPermissions = {
  hasOwnership: false,
  hasManageAccess: false,
  hasBillingAccess: true,
  hasSphinxAccess: true,
};

const collabPermissions = {
  hasOwnership: false,
  hasManageAccess: false,
  hasBillingAccess: false,
  hasSphinxAccess: false,
};

describe('The Settings Menu', () => {
  it('Generates correctly for Owners', () => {
    const menu = getMenuOptions({ currentUser: owner, permissions: ownerPermissions, addons: {} });
    expect(menu.userSettings.some((tab) => tab.key.includes('profile'))).toBe(true);
    expect(menu.userSettings.some((tab) => tab.key.includes('preferences'))).toBe(true);
    expect(menu.userSettings.some((tab) => tab.key.includes('notifications'))).toBe(true);
    expect(menu.companySettings.some((tab) => tab.key.includes('account'))).toBe(true);
    expect(menu.companySettings.some((tab) => tab.key.includes('seats'))).toBe(true);
    expect(menu.companySettings.some((tab) => tab.key.includes('billing'))).toBe(true);
    expect(menu.companySettings.some((tab) => tab.key.includes('configs'))).toBe(true);
  });

  it('Generates correctly for Admins', () => {
    const menu = getMenuOptions({ currentUser: admin, permissions: adminPermissions, addons: {} });
    expect(menu.userSettings.some((tab) => tab.key.includes('profile'))).toBe(true);
    expect(menu.userSettings.some((tab) => tab.key.includes('preferences'))).toBe(true);
    expect(menu.userSettings.some((tab) => tab.key.includes('notifications'))).toBe(true);
    expect(menu.companySettings.some((tab) => tab.key.includes('account'))).toBe(true);
    expect(menu.companySettings.some((tab) => tab.key.includes('seats'))).toBe(true);
    expect(menu.companySettings.some((tab) => tab.key.includes('billing'))).toBe(false);
    expect(menu.companySettings.some((tab) => tab.key.includes('configs'))).toBe(true);
  });

  it('Generates correctly for Billing', () => {
    const menu = getMenuOptions({ currentUser: billing, permissions: billingPermissions, addons: {} });
    expect(menu.userSettings.some((tab) => tab.key.includes('profile'))).toBe(true);
    expect(menu.userSettings.some((tab) => tab.key.includes('preferences'))).toBe(true);
    expect(menu.companySettings.some((tab) => tab.key.includes('account'))).toBe(false);
    expect(menu.companySettings.some((tab) => tab.key.includes('seats'))).toBe(false);
    expect(menu.companySettings.some((tab) => tab.key.includes('billing'))).toBe(true);
    expect(menu.companySettings.some((tab) => tab.key.includes('configs'))).toBe(false);
  });

  it('Generates correctly for Collaborator', () => {
    const menu = getMenuOptions({ currentUser: collaborator, permissions: collabPermissions, addons: {} });
    expect(menu.userSettings.some((tab) => tab.key.includes('profile'))).toBe(true);
    expect(menu.userSettings.some((tab) => tab.key.includes('preferences'))).toBe(true);
    expect(menu.companySettings).toMatchObject([]);
  });

  it('Generates correctly for Net-Inspect Addon', () => {
    const menu = getMenuOptions({ currentUser: owner, permissions: ownerPermissions, addons: { net_inspect: 1 } });
    expect(menu.userSettings.some((tab) => tab.key.includes('profile'))).toBe(true);
    expect(menu.userSettings.some((tab) => tab.key.includes('preferences'))).toBe(true);
    expect(menu.userSettings.some((tab) => tab.key.includes('notifications'))).toBe(true);
    expect(menu.companySettings.some((tab) => tab.key.includes('account'))).toBe(true);
    expect(menu.companySettings.some((tab) => tab.key.includes('seats'))).toBe(true);
    expect(menu.companySettings.some((tab) => tab.key.includes('billing'))).toBe(true);
    expect(menu.companySettings.some((tab) => tab.key.includes('configs'))).toBe(true);
    expect(menu.companySettings.some((tab) => tab.key.includes('connection'))).toBe(true);
  });
});
