import React, { useEffect, useRef, useState } from 'react';
import { Organization } from 'graphql/organization';
import { Button, Tabs } from 'antd';
import { i18n } from 'i18n';
import { useSelector } from 'react-redux';
import { AppState } from 'modules/state';
import { getBackendConfig, isBeforeStaging } from 'config';
import { getMenuOptions } from './menuOptions';

const { TabPane } = Tabs;

interface settingsMenuProps {
  currentTab: string;
  setKey: (key: string) => void;
  currentUser: any;
  currentOrg: Organization | undefined;
  permissions: {
    hasOwnership: boolean;
    hasManageAccess: boolean;
    hasBillingAccess: boolean;
    hasSphinxAccess: boolean;
  };
  openConfigPanels: string[];
  setOpenConfigPanels: (value: string[] | ((prevVar: string[]) => string[])) => void;
}

const SettingsMenu = ({ currentUser, currentOrg, currentTab, setKey, permissions, openConfigPanels, setOpenConfigPanels }: settingsMenuProps) => {
  const { addons, state } = useSelector((store: AppState) => store.subscription);
  const settingsMenus = getMenuOptions({ currentUser, currentOrg, permissions, addons, trial: state === 'trial' });
  const jumpTo = useRef<string>();
  const [versionNumber, setVersionNumber] = useState('Unknown');
  const [build, setBuild] = useState('Unknown');

  const anchorScroll = (id: string) => {
    document.getElementById(id)?.scrollIntoView({ behavior: 'smooth' });
  };

  // Get version number & build on page load
  useEffect(() => {
    getBackendConfig().then((res) => {
      setVersionNumber(res.package.version);
      setBuild(res.scm.build.slice(0, 6));
    });
  }, []);

  useEffect(() => {
    if (jumpTo.current) {
      anchorScroll(jumpTo.current);
      jumpTo.current = undefined;
    }
  }, [openConfigPanels]);

  const handleAnchorClick = (id: string) => {
    jumpTo.current = id;
    switch (id) {
      case 'tolerances-presets-table':
        setOpenConfigPanels(['tols']);
        break;
      case 'styles-presets-table':
        setOpenConfigPanels(['styles']);
        break;
      case 'feature-types-table':
        setOpenConfigPanels(['types']);
        break;
      case 'inspection-properties-table':
        setOpenConfigPanels(['props']);
        break;
      default:
        break;
    }
  };

  return (
    <div className="settings-menu-container">
      <div className="settings-menu">
        <h3>{i18n('settings.nav.mySettings')}</h3>
        <Tabs
          className="my-settings-menu"
          tabPosition="left"
          activeKey={currentTab}
          onTabClick={(key: string) => {
            setKey(key);
          }}
        >
          {settingsMenus.userSettings.map((pane) => {
            return (
              <TabPane tab={pane.tab} key={pane.key}>
                {pane.content}
              </TabPane>
            );
          })}
        </Tabs>
      </div>
      {settingsMenus.companySettings && settingsMenus.companySettings.length > 0 && (
        <div className="settings-menu">
          <h3>{i18n('settings.nav.companySettings')}</h3>
          <Tabs
            className="company-settings-menu"
            tabPosition="left"
            activeKey={currentTab}
            onTabClick={(key: string) => {
              setKey(key);
            }}
          >
            {settingsMenus.companySettings.map((pane) => {
              return (
                <TabPane tab={pane.tab} key={pane.key}>
                  {pane.content}
                </TabPane>
              );
            })}
          </Tabs>
          {currentTab === 'configs' && (
            <div className="config-anchor-links-container">
              <h4>{`${i18n('settings.custom.jump')}...`}</h4>
              <ul className="config-anchor-links">
                <li className="config-anchor-link" data-cy="tols-anchor">
                  <Button type="link" onClick={() => handleAnchorClick('tolerances-presets-table')}>
                    {i18n('settings.custom.table.tolerances')}
                  </Button>
                </li>
                <li className="config-anchor-link" data-cy="styles-anchor">
                  <Button type="link" onClick={() => handleAnchorClick('styles-presets-table')}>
                    {i18n('settings.custom.table.styles')}
                  </Button>
                </li>
                <li className="config-anchor-link" data-cy="features-anchor">
                  <Button type="link" onClick={() => handleAnchorClick('feature-types-table')}>
                    {i18n('settings.custom.table.feature')}
                  </Button>
                </li>
                <li className="config-anchor-link" data-cy="props-anchor">
                  <Button type="link" onClick={() => handleAnchorClick('inspection-properties-table')}>
                    {i18n('settings.custom.table.property')}
                  </Button>
                </li>
              </ul>
            </div>
          )}
        </div>
      )}
      <div className="version-div">
        <span>
          {i18n('settings.edition')}: {i18n('settings.essentials')}
        </span>
        <span>
          {i18n('common.version')}: {versionNumber}
        </span>
        <span>
          {i18n('common.build')}: {build}
        </span>
        {isBeforeStaging && (
          <div className="version-div">
            <span>
              {i18n('common.userId')}: {currentUser.id.slice(0,8)}
            </span>
            <span>
              {i18n('common.accountId')}: {currentUser.accountId.slice(0,8)}
            </span>
            <span>
              {i18n('common.siteId')}: {currentUser.siteId.slice(0,8)}
            </span>
          </div>
        )}
      </div>
    </div>
  );
};

export default SettingsMenu;
