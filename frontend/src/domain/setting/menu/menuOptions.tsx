import React from 'react';
import { ProfileOutlined, SettingOutlined, ShopOutlined, TeamOutlined, WalletOutlined, ApiOutlined, BellOutlined, IdcardOutlined, SafetyOutlined } from '@ant-design/icons';
import { IxAvatar } from 'styleguide/Avatar/IxAvatar';
import { i18n } from 'i18n';
import { Organization } from 'graphql/organization';
import config, { isBeforeProduction, isBeforeQA } from 'config';

interface Props {
  currentUser: any;
  currentOrg: Organization | undefined;
  permissions: {
    hasOwnership: boolean;
    hasManageAccess: boolean;
    hasBillingAccess: boolean;
    hasSphinxAccess: boolean;
  };
  addons: { [key: string]: number };
  trial: boolean;
}

export const getMenuOptions = ({ currentUser, currentOrg, permissions, addons, trial }: Props) => {
  const userSettings = [
    {
      tab: (
        <span className="tab-content">
          <IxAvatar size="small" type="user" data={currentUser} />
          {i18n('settings.nav.myProfile')}
        </span>
      ),
      key: 'profile',
      content: '',
      disabled: false,
    },
    {
      tab: (
        <span className="tab-content">
          <SettingOutlined />
          {i18n('settings.nav.preferences')}
        </span>
      ),
      key: 'preferences',
      content: '',
      disabled: false,
    },
  ];
  if (config.authStrategy && config.authStrategy === 'cognito' && !permissions.hasOwnership) {
    userSettings.push({
      tab: (
        <span className="tab-content">
          <SafetyOutlined />
          {i18n('settings.nav.security')}
        </span>
      ),
      key: 'security',
      content: '',
      disabled: false,
    });
  }
  if ((permissions.hasOwnership || permissions.hasManageAccess) && isBeforeProduction) {
    userSettings.push({
      tab: (
        <span className="tab-content">
          <BellOutlined />
          {i18n('settings.nav.notifications')}
        </span>
      ),
      key: 'notifications',
      content: '',
      disabled: false,
    });
  }

  let companySettings: any[] = [];

  if (permissions.hasManageAccess) {
    // has manage, add company and seats
    companySettings = [
      {
        tab: (
          <span className="tab-content">
            {currentOrg ? <IxAvatar type="company" size="small" data={currentOrg} /> : <ShopOutlined />}
            {i18n('settings.nav.companyProfile')}
          </span>
        ),
        key: 'account',
        content: '',
      },
      {
        tab: (
          <span className="tab-content">
            <TeamOutlined />
            {i18n('settings.nav.seats')}
          </span>
        ),
        key: 'seats',
        content: '',
      },
    ];
    if (isBeforeQA) {
      companySettings.push({
        tab: (
          <span className="tab-content">
            <IdcardOutlined />
            {i18n('settings.nav.operators')}
          </span>
        ),
        key: 'operators',
        content: '',
      });
    }
    if (config.authStrategy && config.authStrategy === 'cognito' && permissions.hasOwnership) {
      companySettings.push({
        tab: (
          <span className="tab-content">
            <SafetyOutlined />
            {i18n('settings.nav.security')}
          </span>
        ),
        key: 'security',
        content: '',
        disabled: false,
      });
    }
    if (permissions.hasBillingAccess) {
      // also has billing
      companySettings.push({
        tab: (
          <span className="tab-content">
            <WalletOutlined />
            {i18n('settings.nav.billing')}
          </span>
        ),
        key: 'billing',
        content: '',
      });
    }
    // add configurations
    companySettings.push({
      tab: (
        <div className="tab-content">
          <ProfileOutlined />
          {i18n('settings.nav.configs')}
        </div>
      ),
      key: 'configs',
      content: '',
    });
  } else if (permissions.hasBillingAccess) {
    // only has billing, add billing
    companySettings = [
      {
        tab: (
          <span className="tab-content">
            <WalletOutlined />
            {i18n('settings.nav.billing')}
          </span>
        ),
        key: 'billing',
        content: '',
      },
    ];
  }

  if (config && config.sphinx.netinspect.enabled && permissions.hasSphinxAccess && (trial || addons.net_inspect)) {
    companySettings.push({
      tab: (
        <div className="tab-content">
          <ApiOutlined />
          {i18n('entities.connection.titleShort')}
        </div>
      ),
      key: 'connection',
      content: '',
    });
  }

  return { userSettings, companySettings };
};
