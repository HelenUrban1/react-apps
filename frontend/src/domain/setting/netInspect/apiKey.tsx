import React from 'react';
import { useMutation } from '@apollo/client';
import { useHistory } from 'react-router-dom';
import { Button, Col, Popconfirm, Row, Tooltip } from 'antd';
import Message from 'view/shared/message';
import Errors from 'modules/shared/error/errors';
import SphinxApi from 'modules/sphinx/sphinxApi';
import { RetweetOutlined } from '@ant-design/icons';
import { i18n } from 'i18n';
import './netInspect.less';

interface Props {
  apiKey: string;
}

export const ApiKey: React.FC<Props> = ({ apiKey }) => {
  const history = useHistory();

  const [rotateApiKey] = useMutation(SphinxApi.mutate.apiKeyRotate, {
    onCompleted: () => {
      Message.success(i18n('entities.connection.apiKey.rotateSuccess'));
      history.push('/settings/connection');
    },
    onError: (error) => {
      Errors.handle(error);
    },
  });

  const makeRotateRequest = () => {
    rotateApiKey({ variables: { data: { apiKey } } });
  };

  if (!apiKey) {
    return null;
  }

  return (
    <div>
      <Row>
        <Col span={16}>
          <span className="sub-info-item">Keeping your data safe is a top priority at InspectionXpert. To ensure your Net-Inspect credentials are secure, we encrypt them with a unique key.</span>
        </Col>
      </Row>
      <br />
      <Row>
        <Col span={16}>
          <span className="sub-info-item">If you suspect that your Net-Inspect account has been compromised, we recommend you update your password and rotate this key. </span>
        </Col>
      </Row>
      <br />
      <Row>
        <Col span={16}>
          <span className="sub-info-item">{`${apiKey.split('-')[0]}-****-****-****-***********`}</span>
        </Col>
      </Row>
      <br />
      <Row>
        <Col>
          <Popconfirm title={i18n('entities.connection.apiKey.rotateConfirm')} onConfirm={() => makeRotateRequest()} okText={i18n('common.yes')} cancelText={i18n('common.no')}>
            <Tooltip title={i18n('entities.connection.apiKey.rotateTooltip')} placement="bottom">
              <Button className="rotate" icon={<RetweetOutlined />}>
                {i18n('entities.connection.apiKey.rotate')}
              </Button>
            </Tooltip>
          </Popconfirm>
        </Col>
      </Row>
    </div>
  );
};

export default ApiKey;
