import React, { useState, useEffect } from 'react';
import { useLazyQuery, useMutation, useQuery } from '@apollo/client';
import { Badge, Button, Col, Input, notification, Row, Space, Tooltip } from 'antd';
import Spinner from 'view/shared/Spinner';
import Errors from 'modules/shared/error/errors';
import SphinxApi from 'modules/sphinx/sphinxApi';
import { getApiKeyFromConnections, getCredentialsForDestination, isResourceNotFoundError, isServerError } from 'modules/sphinx/connectionModel';
import { Connection, ConnectionFindInput, Definition, Destination, DestinationCredential, Source } from 'modules/sphinx/sphinxInterfaces';
import { getDestinationDefinitions, getSourceDefinitions } from 'modules/sphinx/definitionModel';
import { useSelector } from 'react-redux';
import { AppState } from 'modules/state';
import { i18n } from 'i18n';
import { ApiOutlined, EditOutlined, EyeInvisibleOutlined, EyeTwoTone, StopOutlined } from '@ant-design/icons';
import { ApiKey } from './apiKey';
import './netInspect.less';

const ConnectionView = () => {
  const user = useSelector((state: AppState) => state.auth.currentUser);
  const { accountMembership } = useSelector((state: AppState) => state.session);
  let { accountId, siteId } = accountMembership || { accountId: null, siteId: null };
  const [netInspectUserId, setNetInspectUserId] = useState<string | undefined>();
  const [netInspectUserPassword, setNetInspectUserPassword] = useState<string | undefined>();
  const [netInspectUserCompany, setNetInspectUserCompany] = useState<string | undefined>();
  const [connections, setConnections] = useState<Connection[]>();
  const [apiKey, setApiKey] = useState<string | undefined>();
  const [status, setStatus] = useState<'success' | 'processing' | 'error' | 'default' | 'warning'>('default');
  const [canEditConnection, setCanEditConnection] = useState<boolean>(false);

  const resetState = () => {
    setConnections(undefined);
    setApiKey(undefined);
    setNetInspectUserId(undefined);
    setNetInspectUserPassword(undefined);
    setNetInspectUserCompany(undefined);
    setCanEditConnection(false);
  };

  // This is a workaround for `user.siteId` being set, but `siteId` not being set in `accountMembership`.
  // Since the `User` interface does not have a top level `siteId` property, the cloning to an `any`-typed
  // object is necessary to access the `siteId`.
  if (!siteId) {
    const userClone: any = { ...user };
    if (userClone?.siteId) {
      siteId = userClone.siteId;
    }
  }

  const definitionListQuery = useQuery(SphinxApi.query.definitionList);
  const [getConnectionHealth, connectionStatus] = useLazyQuery<{ connectionHealth: string }, { data: { apiKey: string } }>(SphinxApi.query.connectionHealth, { fetchPolicy: 'network-only' });

  const definitions: Definition[] = definitionListQuery.data?.definitionList;
  const destinations: Destination[] = getDestinationDefinitions(definitions);
  const sources: Source[] = getSourceDefinitions(definitions);

  let netInspectDestination: string;
  try {
    netInspectDestination = destinations?.filter((item) => item.name === 'Net-Inspect')[0].id;
  } catch {
    console.warn('Net-Inspect does not appear in the destinations list:', destinations);
  }

  const findInput: ConnectionFindInput = { accountId: accountId || '', siteId: siteId || '', userId: user?.id || '' };

  const connectionFindResult = useQuery(SphinxApi.query.connectionFind, {
    variables: { data: findInput },

    // Prevent query result caching so that updates and removals of Connections
    // are reflected in this view.
    fetchPolicy: 'no-cache',
    notifyOnNetworkStatusChange: true, // causes onCompleted to be run during refetch
    onCompleted: (result) => {
      setConnections(result.connectionFind);
    },
    onError: (error) => {
      // A 404 is expected if the user does not have any enabled Connections
      if (!isResourceNotFoundError(error)) {
        Errors.handle(error);
      }
    },
  });

  const { loading: connectionFindLoading } = connectionFindResult;

  if (definitionListQuery.error) {
    // A 500 is expected if SPHINX is down
    if (!isServerError(definitionListQuery.error)) {
      Errors.handle(definitionListQuery.error);
    }
  }

  const [createConnection, { data: created }] = useMutation(SphinxApi.mutate.connectionCreate, {
    onCompleted: () => {
      resetState();
      connectionFindResult.refetch();
      definitionListQuery.refetch({ fetchPolicy: 'no-cache' });
    },
    onError: (error) => {
      Errors.handle(error);
    },
  });

  const makeCreateRequest = () => {
    setStatus('processing');
    // filter out the net inspect desination
    createConnection({
      variables: {
        data: {
          accountId,
          siteId,
          userId: user?.id,
          // grabbing the first source because there is only one
          // it's inspectionxpert
          sourceId: sources[0].id,
          // netinspectionDestination id in an array
          destinationIds: [netInspectDestination],
          destinationCredentials: [
            {
              destinationId: netInspectDestination,
              userName: netInspectUserId,
              password: netInspectUserPassword,
              company: netInspectUserCompany,
            },
          ],
        },
      },
    });
  };

  const [updateConnection, { data: edited }] = useMutation(SphinxApi.mutate.connectionUpdate, {
    onCompleted: () => {
      resetState();
      connectionFindResult.refetch();
      definitionListQuery.refetch({ fetchPolicy: 'no-cache' });
    },
    onError: (error) => {
      Errors.handle(error);
    },
  });

  const saveCredentialsChanges = () => {
    setStatus('processing');
    if (connections) {
      updateConnection({
        variables: {
          data: {
            connectionId: connections[0].id,
            destinationCredentials: {
              destinationId: netInspectDestination,
              userName: netInspectUserId,
              password: netInspectUserPassword,
              company: netInspectUserCompany,
            },
          },
        },
      });
    }
  };

  const [deleteConnection] = useMutation(SphinxApi.mutate.connectionDelete, {
    onCompleted: () => {
      notification.success({ message: i18n('entities.connection.deleteSuccess') });
      resetState();
      connectionFindResult.refetch();
      definitionListQuery.refetch({ fetchPolicy: 'no-cache' });
    },
    onError: (error) => {
      Errors.handle(error);
    },
  });

  const makeDeleteConnectionRequest = () => {
    // delete the active connection
    if (connections && connections.length > 0) {
      deleteConnection({
        variables: {
          data: { connectionId: connections[0].id },
        },
      });
    }
  };

  const toggleCanEditConnection = () => {
    setCanEditConnection(!canEditConnection);
  };
  useEffect(() => {
    if (connections && connections.length > 0) {
      const sphinxApiKey: string = getApiKeyFromConnections(connections);
      setApiKey(sphinxApiKey);
      getConnectionHealth({ variables: { data: { apiKey: sphinxApiKey } } });
      const credentials: DestinationCredential | undefined = getCredentialsForDestination(connections[0], netInspectDestination);
      setNetInspectUserId(credentials?.userName);
      setNetInspectUserPassword(credentials?.password);
      setNetInspectUserCompany(credentials?.company);
    }
  }, [connections]);

  useEffect(() => {
    if (apiKey) {
      getConnectionHealth({ variables: { data: { apiKey } } });
    } else {
      setStatus('default');
    }
  }, [apiKey]);

  useEffect(() => {
    if (connectionStatus.loading) {
      setStatus('processing');
    } else if (connectionStatus.error || (connectionStatus?.data && connectionStatus.data?.connectionHealth && connectionStatus.data.connectionHealth !== 'OK')) {
      notification.error({ message: i18n('entities.connection.status.error') });
      setStatus('error');
    } else if (connectionStatus?.data && connectionStatus.data?.connectionHealth && connectionStatus.data.connectionHealth === 'OK') {
      if (edited) {
        notification.success({ message: i18n('entities.connection.edit.updateSuccess') });
      } else if (created) {
        notification.success({ message: i18n('entities.connection.create.success') });
      }
      setStatus('success');
    } else {
      setStatus('default');
    }
  }, [connectionStatus.data, connectionStatus.error, connectionStatus.loading]);

  if (connectionFindLoading || definitionListQuery.loading) {
    return <Spinner />;
  }

  const isDisabled: any = apiKey?.length && !canEditConnection;

  return (
    <section className="settings-section connections">
      <h2>{i18n('entities.connection.titleShort')}</h2>
      <div className="connection-view">
        <Space style={{ width: '100%' }} direction="vertical" size={12}>
          <Row>
            <Col span={16}>
              <span className="sub-info-item">
                {i18n('entities.connection.connect')}
                <a target="_blank" href={i18n('entities.connection.netInspectUrl')} rel="noreferrer">
                  {i18n('entities.connection.titleShort')}
                </a>
                {i18n('entities.connection.portal')}
              </span>
            </Col>
          </Row>
          <Row>
            <Col span={16}>
              <span>User</span>
              <Input
                autoComplete="off"
                placeholder="Net-Inspect User ID"
                value={netInspectUserId}
                onChange={(e) => {
                  setNetInspectUserId(e.target.value);
                }}
                disabled={isDisabled}
              />
            </Col>
          </Row>
          <Row>
            <Col span={16}>
              <span>Password</span>
              <Input.Password
                autoComplete="off"
                placeholder="***"
                value={netInspectUserPassword}
                onChange={(e) => {
                  setNetInspectUserPassword(e.target.value);
                }}
                iconRender={(visible) => (visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />)}
                disabled={isDisabled}
              />
            </Col>
          </Row>
          <Row>
            <Col span={16}>
              <span>Company Name</span>
              <Input
                autoComplete="off"
                placeholder="Net-Inspect company name"
                value={netInspectUserCompany}
                onChange={(e) => {
                  setNetInspectUserCompany(e.target.value);
                }}
                disabled={isDisabled}
              />
            </Col>
          </Row>
          <Row gutter={12}>
            <Col>Connection Status:</Col>
            <Col>
              <Tooltip title={i18n(`entities.connection.status.${status}`)}>
                <Badge className="connection-badge" status={status} />
              </Tooltip>
            </Col>
          </Row>
          <Row gutter={12}>
            <Col>
              {(canEditConnection || !apiKey?.length) && (
                <Button
                  type="primary"
                  className="rotate"
                  icon={<ApiOutlined />}
                  onClick={() => {
                    if (apiKey?.length) {
                      saveCredentialsChanges();
                    } else {
                      makeCreateRequest();
                    }
                  }}
                >
                  Save Connection
                </Button>
              )}
              {!canEditConnection && apiKey?.length && (
                <Button type="primary" className="rotate" icon={<EditOutlined />} onClick={toggleCanEditConnection}>
                  Edit Connection
                </Button>
              )}
            </Col>
            {apiKey && (
              <Col>
                <Button className="rotate" danger icon={<StopOutlined />} onClick={makeDeleteConnectionRequest}>
                  Remove Connection
                </Button>
              </Col>
            )}
          </Row>
          {apiKey && (
            <div className="api-key">
              <h3>{i18n('entities.connection.apiKey.label')}</h3>
              <ApiKey apiKey={apiKey} />
            </div>
          )}
        </Space>
      </div>
    </section>
  );
};

export default ConnectionView;
