import gql from 'graphql-tag';
import { ListItem, ListItemInput, ListObject, ListObjectItem } from './listsTypes';

export interface ListItemFilterInput {
  id?: string;
  name?: string;
  siteId?: string;
  userId?: string;
  parentId?: string;
  listType?: string;
  status?: string;
  createdAtRange?: Date[];
  deletedAtRange?: Date[];
}

export interface ListItemEdit {
  listItemUpdate: ListItem;
}
export interface ListItemEditVars {
  id: string;
  data: ListItemInput;
}

export interface ListItemCreate {
  listItemCreate: ListItem;
}

export interface ListItemDelete {
  listItemDestroy: boolean;
}
export interface ListItemCreateVars {
  data: ListItemInput;
}

export interface ListItemFind {
  listItemFind: ListItem;
}
export interface ListItemFindVars {
  name: string;
}

export interface ListItemList {
  listItemList: {
    count: number;
    rows: ListItem[];
  };
}

type ListItemOrderByEnum = 'id_ASC' | 'id_DESC' | 'name_ASC' | 'name_DESC' | 'createdAt_ASC' | 'createdAt_DESC';

export interface ListItemListVars {
  filter?: ListItemFilterInput;
  orderBy?: ListItemOrderByEnum;
  limit?: number;
  offset?: number;
  paranoid?: boolean;
}

export const ListItems = {
  mutate: {
    create: gql`
      mutation LIST_ITEM_CREATE($data: ListItemInput!) {
        listItemCreate(data: $data) {
          id
          name
          default
          status
          listType
          itemIndex
          metadata
          userId
          siteId
          parentId
        }
      }
    `,
    edit: gql`
      mutation LIST_ITEM_EDIT($id: String!, $data: ListItemInput!) {
        listItemUpdate(id: $id, data: $data) {
          id
          name
          default
          status
          listType
          itemIndex
          metadata
          userId
          siteId
          parentId
        }
      }
    `,
    delete: gql`
      mutation LIST_ITEM_DELETE($ids: [String!]!) {
        listItemDestroy(ids: $ids)
      }
    `,
  },
  query: {
    find: gql`
      query LIST_ITEM_FIND($name: String!) {
        listItemFind(name: $name) {
          id
          name
          metadata
          default
          status
          listType
          itemIndex
          siteId
          userId
          parentId
          deletedAt
        }
      }
    `,
    list: gql`
      query LIST_ITEM_LIST($filter: ListItemFilterInput, $orderBy: ListItemOrderByEnum, $limit: Int, $offset: Int, $paranoid: Boolean) {
        listItemList(filter: $filter, orderBy: $orderBy, limit: $limit, offset: $offset, paranoid: $paranoid) {
          count
          rows {
            id
            name
            metadata
            default
            status
            listType
            itemIndex
            siteId
            userId
            parentId
            deletedAt
          }
        }
      }
    `,
  },
  createGraphqlObject: (listItem: ListItemInput) => {
    const data: ListItemInput = {
      name: listItem.name,
      metadata: listItem.metadata,
      default: listItem.default,
      status: listItem.status,
      listType: listItem.listType,
      itemIndex: listItem.itemIndex,
      siteId: listItem.siteId,
      userId: listItem.userId,
      parentId: listItem.parentId,
    };
    return data;
  },
  createInputFromObject: (listItem: ListObjectItem) => {
    const data: ListItemInput = {
      name: listItem.value,
      status: listItem.status,
      itemIndex: listItem.index,
      listType: listItem.listType,
      default: listItem.default || false,
      metadata: listItem.meta ? JSON.stringify(listItem.meta) : undefined,
    };
    return data;
  },
};

export default ListItems;
