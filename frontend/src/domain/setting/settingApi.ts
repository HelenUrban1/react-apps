import gql from 'graphql-tag';
import { Setting, SettingInput } from './settingTypes';

export interface SettingFilterInput {
  id?: string;
  name?: string;
  siteId?: string;
  userId?: string;
  createdAtRange?: Date[];
}

export interface SettingEdit {
  settingUpdate: Setting;
}
export interface SettingEditVars {
  id: string;
  data: SettingInput;
}

export interface SettingCreate {
  settingCreate: Setting;
}
export interface SettingCreateVars {
  data: SettingInput;
}

export interface SettingFind {
  settingFind: Setting;
}
export interface SettingFindVars {
  name: string;
}

export interface SettingList {
  settingList: {
    count: number;
    rows: Setting[];
  };
}

type SettingOrderByEnum = 'id_ASC' | 'id_DESC' | 'name_ASC' | 'name_DESC' | 'createdAt_ASC' | 'createdAt_DESC';

export interface SettingListVars {
  filter?: SettingFilterInput;
  orderBy?: SettingOrderByEnum;
  limit?: number;
  offset?: number;
}

export const Settings = {
  mutate: {
    edit: gql`
      mutation SETTING_UPDATE($id: String!, $data: SettingInput!) {
        settingUpdate(id: $id, data: $data) {
          id
          name
          value
          metadata
          userId
          siteId
        }
      }
    `,
    create: gql`
      mutation SETTING_CREATE($data: SettingInput!) {
        settingCreate(data: $data) {
          id
          name
          value
          metadata
          userId
          siteId
        }
      }
    `,
  },
  query: {
    find: gql`
      query SETTING_FIND($name: String!) {
        settingFind(name: $name) {
          id
          name
          value
          metadata
          userId
          siteId
        }
      }
    `,
    list: gql`
      query SETTING_LIST($filter: SettingFilterInput, $orderBy: SettingOrderByEnum, $limit: Int, $offset: Int) {
        settingList(filter: $filter, orderBy: $orderBy, limit: $limit, offset: $offset) {
          count
          rows {
            id
            name
            value
            metadata
            userId
            siteId
          }
        }
      }
    `,
  },
  createGraphqlObject: (setting: Setting) => {
    const data: SettingInput = {
      name: setting.name,
      value: setting.value || '',
      metadata: setting.metadata || '',
      siteId: setting.siteId,
      userId: setting.userId,
    };
    if (typeof data.metadata === 'object') {
      data.metadata = JSON.stringify(data.metadata);
    }
    if (typeof data.value === 'object') {
      data.value = JSON.stringify(data.value);
    }
    return data;
  },
};

export default Settings;
