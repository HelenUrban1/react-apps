export type ListItemStatusEnum = 'Inactive' | 'Active' | 'Rejected' | 'Multi_Year';

export type MetaData = {
  unit?: string;
  type?: string;
  measurement: string;
};

export type ListObjectItem = {
  id: string;
  value: string;
  default: boolean;
  listType: ListTypeEnum;
  measurement?: string;
  index: number;
  meta?: MetaData | undefined;
  parentId?: string | undefined;
  status: ListItemStatusEnum;
  children?: ListObject;
  deleted: boolean;
};

export type ListObject = { [key: string]: ListObjectItem };

export type ListTypeEnum = 'Type' | 'Unit_of_Measurement' | 'Classification' | 'Operation' | 'Inspection_Method' | 'Access_Control';

export interface ListItem {
  id: string;
  name: string;
  metadata?: string;
  default: boolean;
  status: ListItemStatusEnum;
  listType: ListTypeEnum;
  itemIndex: number;
  siteId?: string;
  userId?: string;
  parentId?: string;
  deletedAt: string | null | undefined;
}

export interface ListItemInput {
  name: string;
  metadata?: string;
  default: boolean;
  status: ListItemStatusEnum;
  listType: ListTypeEnum;
  itemIndex: number;
  parentId?: string;
  siteId?: string;
  userId?: string;
}
