import React, { useRef, useState } from 'react';
import { i18n } from 'i18n';
import log from 'modules/shared/logger';
import Analytics from 'modules/shared/analytics/analytics';
import { string, array } from 'yup';
import { UserEmails, UserRoles, UserControl } from 'styleguide';
import { PlusSquareOutlined } from '@ant-design/icons';
import { Button, notification, Modal } from 'antd';
import { roles, rolesMap, emailRegExp } from 'view/global/defaults';
import { RadioChangeEvent } from 'antd/lib/radio';
import { FetchResult, MutationFunctionOptions } from '@apollo/client';
import { useSelector } from 'react-redux';
import { AccountState, AppState } from 'modules/state';
import { User } from 'types/user';
import authSelectors from 'modules/auth/authSelectors';
import PermissionChecker from 'modules/auth/permissionChecker';
import Permissions from 'security/permissions';
import { iamAPI } from '../iamApi';

const schema = array().of(string().matches(emailRegExp));

interface Props {
  inviteUser: (options?: MutationFunctionOptions<any, any> | undefined) => Promise<FetchResult<any, Record<string, any>, Record<string, any>>>;
  users: User[] | undefined;
  showModal: () => void;
  closeInvitation: () => void;
  account: AccountState;
  show: boolean;
  enforceSeats: boolean;
  openSeats: number;
  loading: boolean;
  refetchUsers: () => void;
  defaultRole?: string;
  button?: boolean;
}

const InviteUsersModal: React.FC<Props> = ({ inviteUser, users, showModal, closeInvitation, account, show, enforceSeats, openSeats, loading, refetchUsers, defaultRole, button = true }) => {
  const [emails, setEmails] = useState<string[]>([]);
  const [role, setRole] = useState<string>(defaultRole || (!enforceSeats || openSeats > 0 ? 'planner' : 'collaborator'));
  const [control, setControl] = useState(true);
  const [error, setError] = useState();
  const user = useSelector((state: AppState) => authSelectors.selectCurrentUser(state));
  const permissionValidator = new PermissionChecker(user);
  const billingPermission = permissionValidator.match(Permissions.values.accountBillingAccess);

  const resetModal = () => {
    setRole(!enforceSeats || openSeats - emails.length > 0 ? 'planner' : 'collaborator');
    setEmails([]);
    setControl(true);
    setError(undefined);
  };

  const handleOpenModal = () => {
    resetModal();
    showModal();
  };

  const cancelModal = () => {
    Analytics.track({
      event: Analytics.events.inviteUsersCanceled,
      properties: {},
    });
    resetModal();
    closeInvitation();
  };

  const inviteUsers = async () => {
    if (!emails || emails.length < 1) {
      Analytics.track({
        event: Analytics.events.inviteUsersError,
        properties: {
          emails,
          role,
          control,
          error,
        },
      });
      setError(i18n('user.invite.addressEmpty'));
      return;
    }
    if (enforceSeats && roles.paid.find((r) => r.key === role) && emails.length > openSeats) {
      setError(i18n('user.invite.insufficientSeats'));
      return;
    }
    if (emails.length !== new Set(emails).size) {
      setError(i18n('user.invite.duplicates'));
      return;
    }

    const valid = await schema.isValid(emails);
    if (valid) {
      const assignRoles = rolesMap[role].roles;

      const newEmails: string[] = [];
      const existingEmails: string[] = [];

      emails.forEach((email) => {
        if (users && users.find((u) => u.email.toLowerCase() === email.toLowerCase())) {
          existingEmails.push(email);
        } else {
          newEmails.push(email);
        }
      });

      if (existingEmails.length > 0 && newEmails.length === 0) {
        // all users entered already exist as a member of this account
        setError(i18n('user.invite.membersExist', existingEmails.length));
        return;
      }

      Promise.all(
        newEmails.map(async (address) => {
          await inviteUser({
            variables: { email: address, roles: assignRoles, control },
          });
        }),
      )
        .then(() => {
          Analytics.track({
            event: Analytics.events.inviteUsersSent,
            properties: {
              newEmails,
              role,
              control,
              error,
            },
          });
          refetchUsers();
          resetModal();
          closeInvitation();
          existingEmails.forEach((email) => {
            notification.warning({
              message: i18n('user.invite.memberExists', email),
            });
          });
        })
        .catch((err) => {
          log.error('InviteUsersModal.inviteUsers error', err);
          Analytics.track({
            event: Analytics.events.inviteUsersError,
            properties: {
              newEmails,
              role,
              control,
              error: err.message,
            },
          });
          // TODO: How do I get the address into this catch block?
          if (err.message.includes(i18n('iam.errors.userAlreadyExists'))) {
            // user already exists as a member of another account
            notification.warning({
              message: i18n('user.invite.memberExistsExternal'),
            });
            resetModal();
            closeInvitation();
          }
          setError(err.message);
        });
    } else {
      Analytics.track({
        event: Analytics.events.inviteUsersError,
        properties: {
          emails,
          role,
          control,
          error,
        },
      });
      setError(i18n('user.invite.addressError'));
    }
  };

  const storeEmails = (addresses: string[]) => {
    setEmails(addresses);
  };

  const handleRole = (val: string) => {
    setRole(val);
  };

  const handleControlled = (e: RadioChangeEvent) => {
    setControl(e.target.value);
  };

  const container = useRef<any>();

  return (
    <>
      {button && (
        <Button type="primary" size="large" className="btn-primary" data-cy="invite-users" onClick={handleOpenModal}>
          <PlusSquareOutlined />
          {i18n('user.invite.button')}
        </Button>
      )}
      <Modal
        destroyOnClose
        title={i18n('user.invite.title')}
        visible={show}
        onCancel={cancelModal}
        onOk={inviteUsers}
        footer={
          <>
            <Button size="large" onClick={cancelModal} data-cy="cancel-invite" disabled={loading}>
              {i18n('common.cancel')}
            </Button>
            <Button type="primary" size="large" className="primary" data-cy="confirm-invite" onClick={inviteUsers} loading={loading}>
              {i18n('user.invite.confirm')}
            </Button>
          </>
        }
      >
        <div data-cy="invite-user-modal" className="modal-body">
          <UserEmails change={storeEmails} error={error} />
          <div className="change-section" ref={container}>
            <div className="invite-role-select">
              <label htmlFor="modal-email-input" data-cy="modal-role-title">
                {i18n('user.invite.role')}
              </label>
              <UserRoles container={container} current={role} options={roles} change={handleRole} limited billingPermission={billingPermission} hasOpenSeat={!enforceSeats || openSeats > 0} />
            </div>
            <p data-cy="modal-role-description" className="small">
              {rolesMap[role]?.description}
            </p>
          </div>
          <UserControl control={control} plural={emails.length > 1} change={handleControlled} />
        </div>
      </Modal>
    </>
  );
};

export default InviteUsersModal;
