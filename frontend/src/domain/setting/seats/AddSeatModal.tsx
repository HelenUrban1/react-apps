import React from 'react';
import { User } from 'types/user';
import { IxAvatar } from 'styleguide/Avatar/IxAvatar';
import { Modal } from 'antd';
import { i18n } from 'i18n';
import { Subscription } from '../subscriptionTypes';

interface addSeatProps {
  subscription: Subscription | undefined;
  user: User | undefined;
  additional?: boolean;
  closePurchase: () => void;
  confirmPurchase: () => void;
  handleCancelAddSeat: () => void;
  handleConfirmAddSeat: () => void;
}

export const AddSeatModal = ({ subscription, user, additional = false, closePurchase, confirmPurchase, handleCancelAddSeat, handleConfirmAddSeat }: addSeatProps) => {
  if (!subscription) {
    return <></>;
  }
  if (!user) {
    return (
      <Modal title={i18n('settings.seats.addSeat')} visible={!!additional} className="add-seat-modal" onCancel={closePurchase} okText={i18n('settings.seats.upgradeAndPay')} onOk={confirmPurchase}>
        <div className="add-seat-modal-body" data-cy="add-seat-modal-body">
          <div className="add-seat-modal-info" data-cy="add-seat-modal-info">
            Info will go here
          </div>
        </div>
      </Modal>
    );
  }
  return (
    <Modal title={i18n('settings.seats.upgradeSeat')} visible={!!user} className="add-seat-modal" onCancel={handleCancelAddSeat} okText={i18n('settings.seats.upgradeAndPay')} onOk={handleConfirmAddSeat}>
      <div className="add-seat-modal-body" data-cy="add-seat-modal-body">
        <div className="user-card">
          <IxAvatar type="user" data={user} size="large" />
          <div className="card-content user-content-override">
            <div className="user-field">
              {!!user.fullName && <span className="name">{user.fullName}</span>}
              <a className={`email${user.fullName ? '' : ' large'}`} data-cy="email-link" href={`mailto:${user.email}`} target="_blank">
                {user.email}
              </a>
            </div>
          </div>
        </div>
        <div className="add-seat-modal-info" data-cy="add-seat-modal-info">
          Info will go here
        </div>
      </div>
    </Modal>
  );
};
