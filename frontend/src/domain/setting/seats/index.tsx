import React, { useEffect, useRef, useState } from 'react';
import { i18n } from 'i18n';
import { InfoCircleOutlined, SearchOutlined } from '@ant-design/icons';
import { Checkbox, Input, Radio, Tooltip } from 'antd';
import Analytics from 'modules/shared/analytics/analytics';
import UserMiniList from 'styleguide/MiniList/UserList/UserMiniList';
import { User } from 'types/user';
import { FetchResult, MutationFunctionOptions } from '@apollo/client';
import { isBeforeProduction } from 'config';
import log from 'modules/shared/logger';
import { AppState } from 'modules/state';
import { useDispatch, useSelector } from 'react-redux';
import authSelectors from 'modules/auth/authSelectors';
import { SessionActions, updateSettingThunk } from 'modules/session/sessionActions';
import { iamAPI, buildIamEditInput, IamEditFields, IamEditInput } from '../iamApi';
import { AddSeatModal } from './AddSeatModal';
import InviteUsersModal from './InviteUsersModal';
import InviteReviewersModal from './InviteReviewerModal';
import { Settings } from '../settingApi';

interface seatsProps {
  users: User[] | undefined;
  editIamUser: (options?: MutationFunctionOptions<any, any> | undefined) => Promise<FetchResult<any, Record<string, any>, Record<string, any>>>;
  transferOwner: (options?: MutationFunctionOptions<any, any> | undefined) => Promise<FetchResult<any, Record<string, any>, Record<string, any>>>;
  inviteUser: (options?: MutationFunctionOptions<any, any> | undefined) => Promise<FetchResult<any, Record<string, any>, Record<string, any>>>;
  resendUserInvite: (options?: MutationFunctionOptions<any, any> | undefined) => Promise<FetchResult<any, Record<string, any>, Record<string, any>>>;
  cancelUserInvite: (options?: MutationFunctionOptions<any, any> | undefined) => Promise<FetchResult<any, Record<string, any>, Record<string, any>>>;
  rejectUserRequest: (options?: MutationFunctionOptions<any, any> | undefined) => Promise<FetchResult<any, Record<string, any>, Record<string, any>>>;
  acceptUserRequest: (options?: MutationFunctionOptions<any, any> | undefined) => Promise<FetchResult<any, Record<string, any>, Record<string, any>>>;
  permissions: {
    hasOwnership: boolean;
    hasManageAccess: boolean;
    hasBillingAccess: boolean;
  };
  refetchUsers: () => void;
  inviteLoading: boolean;
}

interface seatFilters {
  Search: string;
  Planner: boolean;
  Billing: boolean;
  Collaborator: boolean;
  Viewer: boolean;
  Reviewer: boolean;
  Inactive: boolean;
  [key: string]: string | boolean;
}

interface AddUserInput {
  user: User;
  iamEditInput: IamEditInput;
}

const Seats = ({
  //
  users,
  permissions,
  editIamUser,
  transferOwner,
  inviteUser,
  resendUserInvite,
  cancelUserInvite,
  rejectUserRequest,
  acceptUserRequest,
  refetchUsers,
  inviteLoading,
}: seatsProps) => {
  const { account: currentAccount } = useSelector((state: AppState) => state);
  const { accountMembership } = useSelector((state: AppState) => state.session);
  const { data: subscription, components, state: subState } = useSelector((reduxState: AppState) => reduxState.subscription);
  const seatsPurchased = useRef<number>();
  const enforceSeats = useRef<boolean>(false);
  const settingsContainer = useRef(null);
  const filters = useRef<seatFilters>({
    Search: '',
    Planner: true,
    Billing: true,
    Collaborator: true,
    Viewer: false,
    Inactive: false,
    Reviewer: true,
  });
  const [paidUsers, setPaidUsers] = useState<User[] | undefined>(users);
  const [freeUsers, setFreeUsers] = useState<User[] | undefined>(users);
  const [addUserInput, setAddUserInput] = useState<AddUserInput>();
  const [inviteModal, showInviteModal] = useState(false);
  const [purchaseSeats, setPurchase] = useState(false);

  useEffect(() => {
    if (components) {
      const seats = components.filter((comp) => comp.unit_name.includes('seat'));
      let purchased = 0;
      for (let index = 0; index < seats.length; index++) {
        const comp = seats[index];
        if (comp && subState !== 'trial' && !comp.component_handle?.includes('internal')) {
          enforceSeats.current = true;
        }
        if (comp.allocated_quantity && comp.allocated_quantity > purchased) {
          purchased = comp.allocated_quantity;
        }
      }
      seatsPurchased.current = purchased;
    }
  }, [components]);

  const user = useSelector((state: AppState) => authSelectors.selectCurrentUser(state));
  const currentUser = users?.find((u) => u.id === user?.id);
  const { settings } = useSelector((state: AppState) => state.session);
  const dispatch = useDispatch();
  const accountSettings = JSON.parse(user?.accountSettings || '{}');

  // checks a user against current settings (filters, search) and returns whether to add user to lists
  const isIncluded = (account: User) => {
    // check if user fits current search text
    if (filters.current.Search && filters.current.Search.length >= 1) {
      if (!account.email.includes(filters.current.Search) && !account.fullName?.includes(filters.current.Search)) return false;
    }

    // check if user is filtered out by inactive
    if (account.status === 'Archived') {
      if (!filters.current.Inactive) {
        return false;
      }
      return true;
    }

    // check if user is filtered out by role
    if (!account.roles) return true;
    let include = false;
    account.roles.forEach((role: string) => {
      if (((role === 'Owner' || role === 'Admin') && filters.current.Planner) || filters.current[role]) {
        include = true;
      }
    });

    return include;
  };

  const sortUsers = (a: User, b: User) => {
    const aString = a.fullName || a.email;
    const bString = b.fullName || b.email;
    return aString.toLowerCase().localeCompare(bString.toLowerCase());
  };

  const sortStatus = (a: User, b: User) => {
    if (a.status !== b.status) {
      if (a.status === 'Pending' || a.status === 'Requesting') return -1;
      return a.status === 'Archived' ? 1 : -1;
    }
    if (a.roles?.includes('Owner') && !b.roles?.includes('Owner')) {
      return a.roles.includes('Owner') ? -1 : 1;
    }
    return 0;
  };

  // splits the users into paid and unpaid, and then pending, active, inactive, and sorts alphabetically
  const buildSeatsLists = () => {
    if (!users) return;

    const paid = [...users]
      .filter((a) => isIncluded(a) && a.paid && !a.disabled)
      .sort(sortUsers)
      .sort(sortStatus);
    const free = [...users]
      .filter((a) => isIncluded(a) && !a.paid && !a.disabled)
      .sort(sortUsers)
      .sort(sortStatus);
    setPaidUsers(paid);
    setFreeUsers(free);
  };

  useEffect(() => {
    // split list of users into Paid and Free subsets
    if (!users) return;
    buildSeatsLists();
  }, [users]);

  const searchUsers = (e: any) => {
    filters.current.Search = e.target.value;
    buildSeatsLists();
  };

  // Looks at current seat usage vs. seats purchased
  const getOpenSeats = () => {
    if (!paidUsers || !subscription || !seatsPurchased.current) return 0;
    const paidCount = users?.filter((a) => {
      const membership = a.accountMemberships.find((mem) => mem.accountId === a.activeAccountId);
      return a.paid && (membership?.status === 'Active' || membership?.status === 'Pending');
    }).length;
    if (!paidCount) return seatsPurchased.current;
    const total = seatsPurchased.current - paidCount;
    if (total < 0) return 0;
    return total;
  };

  const handleCancelAddSeat = () => {
    setAddUserInput(undefined);
  };

  const handleConfirmAddSeat = () => {
    setAddUserInput(undefined);
    if (addUserInput) editIamUser({ variables: { data: addUserInput.iamEditInput } });
  };

  const handleIamEdit = (account: User, iamEditInput: IamEditFields) => {
    if (!subscription && !isBeforeProduction) {
      log.error("Can't add users when subscription is unknown");
      return;
    }
    const editInput = buildIamEditInput(account.id, account, iamEditInput);
    if (
      //
      getOpenSeats() === 0 &&
      enforceSeats.current &&
      iamEditInput.roles?.includes('Admin') &&
      (!account.roles?.includes('Admin') || account.status === 'Archived')
    ) {
      // user is trying to give admin privilege to an inactive or non-admin seat, but there isn't an open seat
      setAddUserInput({ user: account, iamEditInput: editInput });
    } else {
      editIamUser({ variables: { data: editInput } });
    }
  };

  // TODO: Move overflow to deferred downgrade listener
  // // Handle More Users than Purchased Seats
  // const [overflowModal, showOverflowModal] = useState(false);
  // const [modalLoading, setModalLoading] = useState(false);
  // const [archiveOverflow] = useMutation(iamAPI.mutate.archiveOverflow, {
  //   onCompleted: () => {
  //     setModalLoading(false);
  //   },
  //   refetchQueries: [
  //     {
  //       query: iamAPI.query.list,
  //       variables: { filter: { activeAccountId: accountMembership.accountId } },
  //     },
  //   ],
  // });

  // useEffect(() => {
  //   // NOTE: If purchased seats is undefined or 0, assume that the subscription is not seat based
  //   if (seatsPurchased.current && paidUsers && paidUsers.length > 0 && enforceSeats) {
  //     const activePaid = paidUsers.filter((seat) => seat.status !== 'Archived');
  //     if (seatsPurchased.current < activePaid.length) {
  //       showOverflowModal(true);
  //       setModalLoading(true);
  //       // Remove extra paid users and show alert if necessary
  //       archiveOverflow({ variables: { paid: seatsPurchased.current } });
  //     }
  //   }
  // }, [seatsPurchased.current, paidUsers]);

  const handleTransferOwner = (id: string) => {
    transferOwner({ variables: { data: { id } } });
  };

  const onChange = (e: any) => {
    filters.current = { ...filters.current, Inactive: e.target.checked };
    buildSeatsLists();
  };

  const canSendInvitation = () => {
    if (!subscription && !isBeforeProduction && enforceSeats) {
      log.error("Can't add users when subscription is unknown");
      return;
    }
    Analytics.track({
      event: Analytics.events.inviteUsersStarted,
      properties: {},
    });
    showInviteModal(true);
  };
  const closeInvitation = () => {
    showInviteModal(false);
  };
  const closePurchase = () => {
    setPurchase(false);
  };

  const confirmPurchase = () => {
    setPurchase(false);
    Analytics.track({
      event: Analytics.events.inviteUsersStarted,
      properties: {},
    });
    showInviteModal(true);
  };

  const resendInvitation = (email: string) => {
    Analytics.track({
      event: Analytics.events.inviteReSendStarted,
      properties: {
        emails: [email],
      },
    });
    resendUserInvite({
      variables: { email },
      refetchQueries: [
        {
          query: iamAPI.query.list,
          variables: { filter: { activeAccountId: accountMembership?.id } },
        },
      ],
    });
  };

  const cancelInvitation = (email: string) => {
    Analytics.track({
      event: Analytics.events.inviteEmailCanceledStarted,
      properties: {
        emails: [email],
      },
    });
    cancelUserInvite({
      variables: { email },
      refetchQueries: [
        {
          query: iamAPI.query.list,
          variables: { filter: { activeAccountId: accountMembership?.accountId } },
        },
      ],
    });
  };

  const acceptRequest = (email: string, userId: string) => {
    Analytics.track({
      event: Analytics.events.accessRequestAccepted,
      properties: {
        emails: [email],
      },
    });
    acceptUserRequest({
      variables: { email, token: undefined, userId },
      refetchQueries: [
        {
          query: iamAPI.query.list,
          variables: { filter: { activeAccountId: accountMembership?.id } },
        },
      ],
    });
  };

  const requestValue = settings.request?.value;

  const handleChangeRequest = (e: any) => {
    if (settings && settings.request) {
      dispatch({
        type: SessionActions.UPDATE_GLOBAL_SETTINGS,
        payload: {
          settings: {
            ...settings,
            request: {
              ...settings.request,
              value: e.target.value,
            },
          },
        },
      });
      const data = Settings.createGraphqlObject({ ...settings.request, value: e.target.value });
      dispatch(updateSettingThunk(data, settings.request.id));
    }
  };

  const rejectRequest = (token: string) => {
    console.log('are we rejecting?', token, user?.email);
    rejectUserRequest({
      variables: { adminEmail: user?.email, token },
      refetchQueries: [
        {
          query: iamAPI.query.list,
          variables: { filter: { activeAccountId: accountMembership?.id } },
        },
      ],
    });
  };

  return (
    <section ref={settingsContainer} id="seatSettings" className="settings-section billing">
      <h2 data-cy="setting-section-title">{i18n('settings.seats.title')}</h2>
      <section className="settings-section-body billing">
        <main className="seatsMain settings-wide-column">
          {/* <Modal //
            title={i18n('settings.seats.overLimit.title')}
            visible={overflowModal}
            onOk={() => showOverflowModal(false)}
            confirmLoading={modalLoading}
            cancelButtonProps={{ disabled: true, className: 'hidden' }}
          >
            <p>{i18n('settings.seats.overLimit.message')}</p>
          </Modal> */}
          <h3 data-cy="payment">{i18n('settings.seats.managementTitle')}</h3>
          <section className="seats-actions" data-cy="seats-control-block">
            <section className="seats-count">
              {user?.isReviewAccount ? ( //
                <InviteReviewersModal //
                  inviteUser={inviteUser}
                  showModal={canSendInvitation}
                  closeInvitation={closeInvitation}
                  show={inviteModal}
                  account={currentAccount}
                />
              ) : (
                <InviteUsersModal //
                  showModal={canSendInvitation}
                  closeInvitation={closeInvitation}
                  show={inviteModal}
                  account={currentAccount}
                  inviteUser={inviteUser}
                  users={users}
                  refetchUsers={refetchUsers}
                  enforceSeats={enforceSeats.current}
                  openSeats={getOpenSeats()}
                  loading={inviteLoading}
                />
              )}
              {subscription && //
                enforceSeats.current && (
                  <h3 data-cy="seats-open" style={{ whiteSpace: 'pre' }}>
                    {`${getOpenSeats()} ${i18n(`settings.seats.seatsOpen.${getOpenSeats() === 1 ? 'singular' : 'plural'}`)}`}
                  </h3>
                )}
            </section>
            <section className="search-filter">
              <Input //
                size="large"
                placeholder="search by name or email"
                onChange={searchUsers}
                prefix={<SearchOutlined />}
                allowClear
              />
              <Checkbox //
                defaultChecked={false}
                onChange={onChange}
                className="inactive-checkbox"
                data-cy="inactive-checkbox"
              >
                {i18n('settings.seats.showInactive')}
              </Checkbox>
            </section>
          </section>

          {user && currentUser && user.isReviewAccount && (
            <section className="users-list" data-cy="review-users">
              <UserMiniList //
                title="Human Reviewers"
                icon="file-search"
                currentUser={currentUser}
                users={paidUsers?.concat(freeUsers || [])}
                handleIamEdit={handleIamEdit}
                handleTransferOwner={() => console.log('no')}
                permissions={permissions}
                resend={resendInvitation}
                cancel={cancelInvitation}
                acceptRequest={acceptRequest}
                rejectRequest={rejectRequest}
                settingsContainer={settingsContainer}
                reviewAccount
              />
            </section>
          )}
          {user && currentUser && !user.isReviewAccount && (
            <>
              <section className="users-list" data-cy="paid-users">
                <UserMiniList //
                  title={i18n('user.paidSeatsListTitle')}
                  icon="solution"
                  currentUser={currentUser}
                  users={paidUsers}
                  handleIamEdit={handleIamEdit}
                  handleTransferOwner={handleTransferOwner}
                  permissions={permissions}
                  resend={resendInvitation}
                  cancel={cancelInvitation}
                  acceptRequest={acceptRequest}
                  rejectRequest={rejectRequest}
                  settingsContainer={settingsContainer}
                  reviewAccount={false}
                />
              </section>
              <section className="users-list" data-cy="free-users">
                <UserMiniList //
                  title={i18n('user.freeSeatsListTitle')}
                  icon="team"
                  currentUser={currentUser}
                  users={freeUsers}
                  handleIamEdit={handleIamEdit}
                  handleTransferOwner={handleTransferOwner}
                  permissions={permissions}
                  resend={resendInvitation}
                  cancel={cancelInvitation}
                  acceptRequest={acceptRequest}
                  rejectRequest={rejectRequest}
                  settingsContainer={settingsContainer}
                  reviewAccount={false}
                />
              </section>
            </>
          )}
          {enforceSeats.current && (
            <AddSeatModal //
              user={addUserInput?.user}
              subscription={subscription || undefined}
              additional={purchaseSeats}
              closePurchase={closePurchase}
              confirmPurchase={confirmPurchase}
              handleCancelAddSeat={handleCancelAddSeat}
              handleConfirmAddSeat={handleConfirmAddSeat}
            />
          )}
        </main>
        <aside className="seatsSidebar settings-thin-column">
          <h3 data-cy="payment">{i18n('settings.seats.settingsTitle')}</h3>
          <section id="language">
            <h4>
              {i18n('settings.account.request.title')}
              <Tooltip title={i18n('settings.seats.requestPolicy')}>
                <InfoCircleOutlined className="span-icon" style={{ opacity: '1.0' }} />
              </Tooltip>
            </h4>
            <Tooltip title={!accountSettings.allowRequestPolicy ? i18n('settings.account.request.disabled') : undefined}>
              <Radio.Group value={!accountSettings.allowRequestPolicy ? 'Closed' : requestValue} onChange={handleChangeRequest} disabled={!accountSettings.allowRequestPolicy}>
                {/* Option to require admin email input
              <Radio className="vertical-radio" value="Email">
                {i18n('settings.account.request.email')}
              </Radio>
              */}
                <Radio className="vertical-radio" value="Closed">
                  {i18n('settings.account.request.closed')}
                </Radio>
                <Radio className="vertical-radio" value="Open">
                  {i18n('settings.account.request.open')}
                </Radio>
              </Radio.Group>
            </Tooltip>
          </section>
        </aside>
      </section>
    </section>
  );
};

export default Seats;
