import React, { useState } from 'react';
import { i18n } from 'i18n';
import log from 'modules/shared/logger';
import { AccountState } from 'modules/state';
import { string, array } from 'yup';
import { UserEmails } from 'styleguide';
import { PlusSquareOutlined } from '@ant-design/icons';
import { Button, Modal } from 'antd';
import { FetchResult, MutationFunctionOptions } from '@apollo/client';
import { iamAPI } from '../iamApi';
import { emailRegExp } from 'view/global/defaults';

const schema = array().of(string().matches(emailRegExp));

interface Props {
  inviteUser: (options?: MutationFunctionOptions<any, any> | undefined) => Promise<FetchResult<any, Record<string, any>, Record<string, any>>>;
  showModal: () => void;
  closeInvitation: () => void;
  account: AccountState;
  show: boolean;
}

const InviteReviewersModal: React.FC<Props> = ({ inviteUser, showModal, closeInvitation, account, show }) => {
  const [emails, setEmails] = useState<string[]>([]);
  const [error, setError] = useState();

  const resetModal = () => {
    setEmails([]);
    setError(undefined);
  };

  const cancelModal = () => {
    resetModal();
    closeInvitation();
  };

  const inviteUsers = async () => {
    if (!emails || emails.length < 1) {
      setError(i18n('user.invite.addressEmpty'));
      return;
    }
    const valid = await schema.isValid(emails);
    if (valid) {
      const assignRoles = ['Reviewer'];

      Promise.all(
        emails.map(async (address, index) => {
          await inviteUser({
            variables: { email: address, roles: assignRoles },
            refetchQueries: [{ query: iamAPI.query.list, variables: { filter: { activeAccountId: account.id } } }],
          });
        }),
      )
        .then(() => {
          resetModal();
          closeInvitation();
        })
        .catch((err) => {
          log.error('InviteUsersModal.inviteUsers error', err);
          setError(err.message);
        });
    } else {
      setError(i18n('user.invite.addressError'));
    }
  };

  const storeEmails = (addresses: string[]) => {
    setEmails(addresses);
  };

  return (
    <>
      <Button type="primary" size="large" className="btn-primary" data-cy="invite-reviewers" onClick={showModal}>
        <PlusSquareOutlined />
        Invite Reviewer
      </Button>
      <Modal
        destroyOnClose
        title="Invite Reviewer"
        visible={show}
        onCancel={cancelModal}
        onOk={inviteUsers}
        footer={
          <>
            <Button size="large" onClick={cancelModal} data-cy="cancel-invite">
              {i18n('common.cancel')}
            </Button>
            <Button type="primary" size="large" className="primary" data-cy="confirm-invite" onClick={inviteUsers}>
              {i18n('user.invite.confirm')}
            </Button>
          </>
        }
      >
        <div data-cy="invite-reviewer-modal" className="modal-body">
          <UserEmails change={storeEmails} error={error} />
        </div>
      </Modal>
    </>
  );
};

export default InviteReviewersModal;
