import React, { useState } from 'react';
import { Input, Modal, Form } from 'antd';
import { isDuplicateValue } from 'utils/Lists';
import { i18n } from 'i18n';
import { ListObject, ListTypeEnum } from '../listsTypes';

interface ListItemModalProps {
  modalTitle: string;
  listType: ListTypeEnum;
  listItems: ListObject | null;
  handleCreateListItem: (name: string, listType: ListTypeEnum, parentId?: string, metadata?: string) => void;
  visible: boolean;
  setDisplayModal: (show: boolean) => void;
}

export const ListItemModal = ({ modalTitle, listType, listItems, handleCreateListItem, visible, setDisplayModal }: ListItemModalProps) => {
  const [status, setStatus] = useState<'' | 'error' | 'success' | 'warning' | 'validating' | undefined>();
  const [message, setMessage] = useState<string | undefined>();
  const [value, setValue] = useState<string>('');

  const resetInput = () => {
    setStatus(undefined);
    setMessage(undefined);
    setValue('');
    setDisplayModal(false);
  };

  const handleOk = () => {
    if (value.replaceAll(' ', '').length === 0) {
      setStatus('error');
      setMessage(`${i18n(`errors.config.empty`)}`);
    } else if (isDuplicateValue(listItems, value)) {
      setStatus('error');
      setMessage(`${i18n(`errors.config.duplicate`)}`);
    } else {
      handleCreateListItem(value, listType);
      resetInput();
    }
  };

  return (
    <div>
      <Modal title={modalTitle} visible={visible} onOk={handleOk} onCancel={resetInput} className="list-feature-modal" data-cy="list-feature-modal">
        <Form layout="vertical" className="list-feature-modal-form" data-cy="list-feature-modal-form">
          <Form.Item label={i18n(`settings.custom.lists.${listType}`)} hasFeedback validateStatus={status} help={message}>
            <Input
              className="list-feature-modal-input"
              data-cy="list-feature-modal-input"
              value={value}
              onChange={(e) => setValue(e.target.value)}
              title={`${i18n(`settings.custom.lists.${listType}`)} Name`}
              placeholder={i18n(`settings.custom.lists.${listType}`).toLocaleLowerCase()}
            />
          </Form.Item>
        </Form>
      </Modal>
    </div>
  );
};
