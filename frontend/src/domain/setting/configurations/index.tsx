import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { AppState } from 'modules/state';
import { i18n } from 'i18n';
import { notification, Collapse } from 'antd';
import { useMutation } from '@apollo/client';
import { getMeasurementsSelectOptions, updateList, updateLists } from 'utils/Lists';
import Analytics from 'modules/shared/analytics/analytics';
import { InspectionTable } from 'styleguide/ConfigTables/InspectionTable';
import { TypesTable } from 'styleguide/ConfigTables/TypesTable';
import { SessionActions, updateSettingThunk } from 'modules/session/sessionActions';
import { StylesTable } from 'styleguide/ConfigTables/StylesTable/StylesTable';
import { TolerancesTable } from 'styleguide/ConfigTables/TolerancesTable';
import { ListItemModal } from './ListItemModal';
import { ListItem, ListObjectItem, ListTypeEnum } from '../listsTypes';
import { ListItemCreate, ListItemDelete, ListItemEdit, ListItems } from '../listsApi';
import { ListTypeModal } from './ListTypeModal';
import { LoadedPresets, Setting, SettingPresets } from '../settingTypes';
import { Settings } from '../settingApi';
import './configurations.less';

const { Panel } = Collapse;

interface ConfigProps {
  openConfigPanels: string[];
  setOpenConfigPanels: (value: string[] | ((prevVar: string[]) => string[])) => void;
}

const Configurations = ({ openConfigPanels, setOpenConfigPanels }: ConfigProps) => {
  const { types, classifications, operations, methods, imperialUnits, metricUnits, markers, organizations, settings } = useSelector((state: AppState) => state.session);
  const dispatch = useDispatch();
  const [displayModal, setDisplayModal] = useState<boolean>(false);
  const [selectedType, setSelectedType] = useState<string>('');
  const [listType, setListType] = useState<ListTypeEnum>('Type');

  const getCurrentList = (activeList: string) => {
    switch (activeList) {
      case 'Classification':
        return classifications;
      case 'Type':
        return types;
      case 'Inspection_Method':
        return methods;
      case 'Operation':
        return operations;
      case 'Unit_of_Measurement':
        return { ...imperialUnits, ...metricUnits };
      default:
        return null;
    }
  };

  const [editListItem] = useMutation<ListItemEdit>(ListItems.mutate.edit, {
    onError: () => {
      notification.error({
        message: i18n('errors.config.edit.message'),
        description: i18n('errors.config.edit.description'),
        duration: 5,
      });
    },
    refetchQueries: [
      {
        query: ListItems.query.list,
        variables: { status: 'Active', deletedAtRange: undefined },
      },
    ],
  });

  const [deleteListItem] = useMutation<ListItemDelete>(ListItems.mutate.delete, {
    onError: () => {
      notification.error({
        message: i18n('errors.config.delete.message'),
        description: i18n('errors.config.delete.description'),
        duration: 5,
      });
    },
    refetchQueries: [
      {
        query: ListItems.query.list,
        variables: { status: 'Active', deletedAtRange: undefined },
      },
    ],
  });

  const getParentRow = (listItem: ListItem) => {
    let parentRow;
    // find the top level row in the table
    if (listItem.listType === 'Type') {
      parentRow = document.querySelector(`[data-row-key="${listItem.parentId}"]`);
    } else if (listItem.listType === 'Classification') {
      parentRow = document.querySelector('[data-row-key=classifications-list-row]');
    } else if (listItem.listType === 'Inspection_Method') {
      parentRow = document.querySelector('[data-row-key=methods-list-row]');
    } else if (listItem.listType === 'Operation') {
      parentRow = document.querySelector('[data-row-key=operations-list-row]');
    }
    return parentRow;
  };

  const getTableBody = (listItem: ListItem) => {
    let tableBody;
    // find the top level row in the table
    if (listItem.listType === 'Type') {
      tableBody = document.querySelector(`[data-row-key="${listItem.parentId}-extra-row"]`)?.querySelector('.ant-table-tbody');
    } else if (listItem.listType === 'Classification') {
      tableBody = document.querySelector('[data-row-key=classifications-list-row-extra-row]')?.querySelector('.ant-table-tbody');
    } else if (listItem.listType === 'Inspection_Method') {
      tableBody = document.querySelector('[data-row-key=methods-list-row-extra-row]')?.querySelector('.ant-table-tbody');
    } else if (listItem.listType === 'Operation') {
      tableBody = document.querySelector('[data-row-key=operations-list-row-extra-row]')?.querySelector('.ant-table-tbody');
    }
    return tableBody;
  };

  const scrollNewItemIntoView = (listItem: ListItem, parentRow: Element) => {
    if (!listItem) return;

    // get the new row, table body, and settings section body elements
    const newRow = document.querySelector(`[data-row-key="${`${listItem.listType === 'Type' ? '' : `${listItem.listType}-`}`}${listItem.id}"]`);
    // const sectionBody = document.querySelector('#ConfigurationSettings');
    // const tableBody = getTableBody(listItem);

    // scroll parent row to top of settings section (with some buffer) and scroll added row into view within table
    // if (newRow && tableBody) tableBody.scrollTop = newRow.getBoundingClientRect().top;
    // if (parentRow && sectionBody) sectionBody.scrollTop += parentRow.getBoundingClientRect().top - sectionBody.getBoundingClientRect().top - 100;

    // scroll new row into view (with some buffer), no scrolling within table
    newRow?.scrollIntoView();
  };

  const [createListItem] = useMutation<ListItemCreate>(ListItems.mutate.create, {
    onCompleted: ({ listItemCreate }) => {
      const currentList = getCurrentList(listItemCreate.listType);
      dispatch({ type: SessionActions.UPDATE_LISTS, payload: updateLists(listItemCreate.listType, currentList, listItemCreate) });
      Analytics.track({
        events: Analytics.events.addedConfigurationEntry,
        accountConfig: listItemCreate,
      });
      const parentRow = getParentRow(listItemCreate);
      // click the expand icon in the top level row to open the sublist
      if (parentRow) {
        const clickEvent = new MouseEvent('click', {
          view: window,
          bubbles: true,
          cancelable: false,
        });
        // won't find element if sublist is already expanded
        parentRow.querySelector('[aria-label="caret-right"')?.dispatchEvent(clickEvent);

        // need time to let element render?
        setTimeout(() => {
          scrollNewItemIntoView(listItemCreate, parentRow);
        }, 200);
      }
    },
    onError: () => {
      notification.error({
        message: i18n('errors.config.create.message'),
        description: i18n('errors.config.create.description'),
        duration: 5,
      });
    },
    refetchQueries: [
      {
        query: ListItems.query.list,
        variables: { status: 'Active', deletedAtRange: undefined },
      },
    ],
  });

  const handleCreateListItem = (name: string, list: ListTypeEnum, parentId?: string, metadata?: string) => {
    const activeListItems = getCurrentList(list);
    const itemIndex = parentId && activeListItems ? Object.keys(activeListItems[parentId].children || {}).length : Object.keys(activeListItems || {}).length;
    createListItem({
      variables: {
        data: ListItems.createGraphqlObject({
          name,
          default: false,
          status: 'Active',
          listType: list,
          itemIndex: itemIndex || 0,
          parentId,
          metadata,
        }),
      },
    });
  };

  const handleUpdateList = (item: ListObjectItem) => {
    const activeListItems = getCurrentList(item.listType);
    const newList = { ...activeListItems };

    if (newList) {
      if (item.listType === 'Type' && item.parentId && newList[item.parentId].children) {
        newList[item.parentId].children![item.id] = item;
      } else {
        newList[item.id] = item;
      }
      dispatch({ type: SessionActions.UPDATE_LISTS, payload: updateList(item.listType, newList) });
    }
  };

  const handleEditListItem = async (id: string, item: ListObjectItem, update = true) => {
    const { data } = await editListItem({
      variables: {
        id,
        data: ListItems.createInputFromObject(item),
      },
    });

    if (data?.listItemUpdate) {
      if (data.listItemUpdate.name !== item.value) {
        Analytics.track({
          events: Analytics.events.editedConfigurationEntry,
          accountConfig: data.listItemUpdate,
        });
      }

      if (update) handleUpdateList(item);
    }
  };

  const handleToggleActive = (listItem: ListObjectItem) => {
    const newListItem = { ...listItem };
    newListItem.status = newListItem.status === 'Active' ? 'Inactive' : 'Active';
    handleEditListItem(listItem.id, newListItem);
  };

  // after an entry is deleted the indexes of the entries after it should decrement
  const reorderAfterDelete = (deletedItem: ListObjectItem) => {
    const activeListItems = getCurrentList(deletedItem.listType);
    const newList = { ...activeListItems };
    if (!newList) return;
    if (!deletedItem.parentId) {
      // delete a top level item
      delete newList[deletedItem.id];
      const keys = Object.keys(newList);
      for (let i = deletedItem.index; i < keys.length; i++) {
        newList[keys[i]].index -= 1;
        handleEditListItem(keys[i], newList[keys[i]], false);
      }
    } else if (newList[deletedItem.parentId]?.children) {
      // delete a second level item
      Object.values(newList[deletedItem.parentId].children!).forEach((child: any) => {
        if (child.index > deletedItem.index) {
          const newChild = { ...child };
          newChild.index -= 1;
          handleEditListItem(child.id, child, false);
          newList[deletedItem.parentId!].children![child.id] = newChild;
        }
        return child;
      });
      delete newList[deletedItem.parentId].children![deletedItem.id];
    }
    dispatch({ type: SessionActions.UPDATE_LISTS, payload: updateList(deletedItem.listType, newList) });
  };

  const handleDeleteListItem = async (listItem: ListObjectItem) => {
    const { data } = await deleteListItem({
      variables: {
        ids: [listItem.id],
      },
    });

    if (data?.listItemDestroy) {
      Analytics.track({
        events: Analytics.events.deletedConfigurationEntry,
        accountConfig: listItem,
      });
      reorderAfterDelete(listItem);
    }
  };

  const getModal = (list: ListTypeEnum) => {
    const listItems = getCurrentList(list);
    if (list === 'Type') {
      return (
        <ListTypeModal
          modalTitle={`Add ${list.replace('_', ' ')}`}
          defaultType={selectedType}
          listItems={listItems}
          handleCreateListItem={handleCreateListItem}
          visible={displayModal}
          setDisplayModal={setDisplayModal}
          measurementOptions={getMeasurementsSelectOptions(metricUnits, imperialUnits)}
        />
      );
    }
    return <ListItemModal modalTitle={`Add ${list.replace('_', ' ')}`} listType={list} listItems={listItems} handleCreateListItem={handleCreateListItem} visible={displayModal} setDisplayModal={setDisplayModal} />;
  };

  const handleOpenModal = (list: ListTypeEnum, type: string) => {
    setSelectedType(type);
    setListType(list);
    setDisplayModal(true);
  };

  const handleUpdatePreset = (presetObj: SettingPresets, loadedPresets: LoadedPresets | undefined) => {
    if (loadedPresets?.id) {
      const updatedSetting: Setting = { ...loadedPresets, id: loadedPresets.id, metadata: JSON.stringify(presetObj) };
      dispatch(updateSettingThunk(Settings.createGraphqlObject(updatedSetting), loadedPresets.id));
    }
  };

  const handleUpdatePresetName = (presetName: string, newName: string, loadedPresets: LoadedPresets | undefined) => {
    const presetObj = loadedPresets ? { ...loadedPresets.metadata } : {};
    const keys = Object.keys(presetObj);
    const newKeys = keys.map((key) => (key === presetName ? newName : key));
    const newObj: { [key: string]: any } = {};
    newKeys.forEach((key) => {
      if (keys.includes(key)) {
        newObj[key] = presetObj[key];
      } else {
        newObj[newName] = presetObj[presetName];
      }
    });
    handleUpdatePreset(newObj, loadedPresets);
  };

  const handleTogglePanel = (key: any) => {
    setOpenConfigPanels(key);
  };

  return (
    <section id="ConfigurationSettings" className="settings-section configurations">
      <main className="configurationsMain settings-wide-column">
        <h2 data-cy="setting-section-title">{i18n('settings.custom.title')}</h2>
        {getModal(listType)}
        <h3 className="inline">{i18n('settings.custom.presets.title')}</h3>
        <Collapse className="configs-collapse" data-cy="configs-collapse" activeKey={openConfigPanels} onChange={handleTogglePanel} ghost>
          <Panel className="configs-collapse-panel tols-collapse" header={<span className="panel-header">{i18n('settings.custom.presets.tolerances.tolerancePresets')}</span>} key="tols">
            <TolerancesTable handleUpdatePreset={handleUpdatePreset} handleUpdatePresetName={handleUpdatePresetName} />
          </Panel>
          <Panel className="configs-collapse-panel styles-collapse" header={<span className="panel-header">{i18n('settings.custom.presets.styles.stylePresets')}</span>} key="styles">
            <StylesTable presets={settings.stylePresets} markers={markers} organizations={organizations} handleUpdatePreset={handleUpdatePreset} handleUpdatePresetName={handleUpdatePresetName} />
          </Panel>
          <h3 className="inline">{`${i18n('settings.custom.data')}`}</h3>
          <Panel className="configs-collapse-panel types-collapse" header={<span className="panel-header">{i18n('settings.custom.table.features')}</span>} key="types">
            <TypesTable handleEditListItem={handleEditListItem} handleToggleActive={handleToggleActive} handleDeleteListItem={handleDeleteListItem} handleOpenModal={handleOpenModal} />
          </Panel>
          <Panel className="configs-collapse-panel inspection-props-collapse" header={<span className="panel-header">{i18n('settings.custom.table.properties')}</span>} key="props">
            <InspectionTable handleEditListItem={handleEditListItem} handleToggleActive={handleToggleActive} handleDeleteListItem={handleDeleteListItem} handleOpenModal={handleOpenModal} getCurrentList={getCurrentList} />
          </Panel>
        </Collapse>
      </main>
    </section>
  );
};

export default Configurations;
