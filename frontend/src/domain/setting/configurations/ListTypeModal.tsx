import React, { useEffect, useRef, useState } from 'react';
import { Input, Modal, Select, Form } from 'antd';
import { SelectValue } from 'antd/lib/select';
import { i18n } from 'i18n';
import { getMeasurementUnits, isDuplicateDoubleLevel, MeasurementOption } from 'utils/Lists';
import { toDisplayFormat } from 'utils/textOperations';
import { defaultTypeIDs } from 'view/global/defaults';
import { ListObject, ListTypeEnum } from '../listsTypes';
import './typeModal.less';

const { Option } = Select;

interface ListItemModalProps {
  modalTitle: string;
  defaultType: string | undefined;
  listItems: ListObject | null;
  handleCreateListItem: (name: string, listType: ListTypeEnum, parentId?: string, metadata?: string) => void;
  visible: boolean;
  setDisplayModal: (show: boolean) => void;
  measurementOptions: MeasurementOption[];
}

export const ListTypeModal = ({ modalTitle, defaultType, listItems, handleCreateListItem, visible, setDisplayModal, measurementOptions }: ListItemModalProps) => {
  const [selectedType, setSelectedType] = useState<string>(defaultType || defaultTypeIDs.Dimension);
  const [subtype, setSubtype] = useState<string>('');
  const [measurement, setMeasurement] = useState<string>('None');
  const [metric, setMetric] = useState<string[]>([]);
  const [imperial, setImperial] = useState<string[]>([]);
  const [both, setBoth] = useState<string[]>([]);
  const [status, setStatus] = useState<'' | 'error' | 'success' | 'warning' | 'validating' | undefined>();
  const [message, setMessage] = useState<string | undefined>();
  const inpRef = useRef<any | null>(null);

  const handleChangeType = (value: SelectValue) => {
    if (!listItems || !value) return;
    setSelectedType(value.toString());
  };

  useEffect(() => {
    if (defaultType) handleChangeType(defaultType);
  }, [defaultType]);

  useEffect(() => {
    if (visible && inpRef) inpRef.current?.focus();
  }, [visible]);

  const handleChangeMeasurement = (value: string) => {
    setMeasurement(value);
    const { metric: newMetric, imperial: newImperial, both: newBoth } = getMeasurementUnits(measurementOptions, value);
    setMetric(newMetric);
    setImperial(newImperial);
    setBoth(newBoth);
  };

  const resetInput = () => {
    setStatus(undefined);
    setMessage(undefined);
    setSelectedType(defaultTypeIDs.Dimension);
    setSubtype('');
    setMeasurement('None');
    setDisplayModal(false);
    setMetric([]);
    setImperial([]);
  };

  const handleSubmit = () => {
    if (subtype.replaceAll(' ', '').length === 0) {
      setStatus('error');
      setMessage(`${i18n(`errors.config.empty`)}`);
    } else if (isDuplicateDoubleLevel(listItems || {}, subtype, selectedType)) {
      setStatus('error');
      setMessage(`${i18n(`errors.config.duplicate`)}`);
    } else {
      const meta = measurement !== 'None' ? `{"measurement": "${measurement}"}` : undefined;
      handleCreateListItem(subtype, 'Type', selectedType, meta);
      resetInput();
    }
  };

  return (
    <div>
      <Modal title={modalTitle} visible={visible} onOk={handleSubmit} onCancel={resetInput}>
        <Form layout="vertical">
          <Form.Item label={`${i18n(`settings.custom.lists.Type`)} Name`}>
            <Select key={selectedType} className="type-select" showSearch defaultValue={selectedType} value={selectedType} onChange={(value: SelectValue) => handleChangeType(value)}>
              {Object.keys(listItems || {})
                .filter((entry) => entry !== defaultTypeIDs['Geometric Tolerance'])
                .map((key) => {
                  return (
                    <Option key={listItems ? `${listItems[key].value}-select-option` : 'select-option'} data-cy={listItems ? `${listItems[key].value}-select-option` : 'select-option'} value={listItems ? listItems[key].id : ''}>
                      {listItems ? listItems[key].value : ''}
                    </Option>
                  );
                })}
            </Select>
          </Form.Item>
          <Form.Item label="Subtype" hasFeedback validateStatus={status} help={message}>
            <Input
              ref={inpRef}
              className="list-type-modal-input"
              data-cy="list-type-modal-input"
              value={subtype}
              onChange={(e) => setSubtype(e.target.value)}
              title={`${i18n(`settings.custom.lists.Subtype`)} Name`}
              placeholder={i18n(`settings.custom.lists.Subtype`).toLocaleLowerCase()}
            />
          </Form.Item>
          <Form.Item label="Measurement Type">
            <Select className="measurement-select" showSearch dropdownMatchSelectWidth={false} value={measurement || i18n('common.none')} onChange={(value: SelectValue) => handleChangeMeasurement(value?.toString() || '')}>
              {measurementOptions.map((measurementOption: MeasurementOption) => {
                return (
                  <Option data-cy={`${measurementOption.name}-select-option`} key={`${measurementOption.name}-select-key`} value={measurementOption.name || ''}>
                    {toDisplayFormat(measurementOption.name)}
                  </Option>
                );
              })}
            </Select>
            <div className="type-modal-units">
              {metric.length > 0 && (
                <div className="unit-hint">
                  <span className="unit-list-title" data-cy="unit-list-title">
                    {`${i18n('common.metric')} ${i18n('common.units')}: `.toLocaleLowerCase()}
                  </span>
                  {metric.map((unit: string, index: number) => {
                    return <span className="unit-list-item">{`${unit}${metric[index + 1] ? ', ' : ''}`}</span>;
                  })}
                </div>
              )}
              {imperial.length > 0 && (
                <div className="unit-hint">
                  <span className="unit-list-title" data-cy="unit-list-title">
                    {`${i18n('common.imperial')} ${i18n('common.units')}: `.toLocaleLowerCase()}
                  </span>
                  {imperial.map((unit: string, index: number) => {
                    return <span className="unit-list-item">{`${unit}${imperial[index + 1] ? ', ' : ''}`}</span>;
                  })}
                </div>
              )}
              {both.length > 0 && (
                <div className="unit-hint">
                  <span className="unit-list-title" data-cy="unit-list-title">
                    {`${i18n('common.imperial')}/${i18n('common.metric')} ${i18n('common.units')}: `.toLocaleLowerCase()}
                  </span>
                  {both.map((unit: string, index: number) => {
                    return <span className="unit-list-item">{`${unit}${both[index + 1] ? ', ' : ''}`}</span>;
                  })}
                </div>
              )}
            </div>
          </Form.Item>
        </Form>
      </Modal>
    </div>
  );
};
