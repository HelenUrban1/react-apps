import { PlusSquareOutlined, SearchOutlined } from '@ant-design/icons';
import { Button, Form, Input, Modal, Radio } from 'antd';
import { i18n } from 'i18n';
import React, { useRef, useEffect, useState } from 'react';
import OperatorMiniList from 'styleguide/MiniList/OperatorMiniList/OperatorMiniList';
import { Operator } from 'types/operator';
import { User } from 'types/user';

interface operatorProps {
  operators: Operator[];
  addOperator: (operator: Operator) => void;
  deleteOperator: (operator: Operator) => void;
  editOperator: (operator: Operator) => void;
  users: User[];
}

interface operatorFilters {
  Search: string;
  [key: string]: string | boolean;
}

const Operators = ({ operators, addOperator, deleteOperator, editOperator, users }: operatorProps) => {
  const [showAddModal, setShowAddModal] = useState<boolean>(false);
  const [filteredOperators, setFilteredOperators] = useState<Operator[]>([]);
  const filters = useRef<operatorFilters>({
    Search: '',
  });

  const isIncluded = (operator: Operator) => {
    // check if user fits current search text
    if (filters.current.Search && filters.current.Search.length >= 1) {
      if (!operator.firstName.toLowerCase().includes(filters.current.Search) && !operator.lastName?.toLowerCase().includes(filters.current.Search) && !operator.email?.toLowerCase().includes(filters.current.Search)) return false;
    }
    return !users.find((user) => operator.email === user.email);
  };

  const sortOperators = (a: Operator, b: Operator) => {
    const aString = `${a.firstName} ${a.lastName}` || a.email || '';
    const bString = `${b.firstName} ${b.lastName}` || b.email || '';
    return aString.toLowerCase().localeCompare(bString.toLowerCase());
  };

  const buildOperatorsList = () => {
    const filtered = operators.filter((a) => isIncluded(a)).sort(sortOperators);
    setFilteredOperators(filtered);
  };

  useEffect(() => {
    if (!operators || !users) return;

    buildOperatorsList();
  }, [operators, users]);

  const permissions = {
    canDelete: true,
    canEdit: true,
  };

  const settingsContainer = useRef(null);

  const searchOperators = (e: any) => {
    filters.current.Search = e.target.value.toLowerCase();
    buildOperatorsList();
  };

  const handleEdit = async (operator: Operator) => {
    editOperator(operator);
  };

  const handleDelete = async (operator: Operator) => {
    deleteOperator(operator);
  };

  const handleAdd = async (operator: Operator) => {
    addOperator(operator);
  };

  const [form] = Form.useForm();

  const onOk = () => {
    form
      .validateFields()
      .then((values) => {
        form.resetFields();
        handleAdd({ ...values });
        setShowAddModal(false);
      })
      .catch((info) => {
        console.error('Validation failed:', info);
      });
  };

  const onCancel = () => {
    setShowAddModal(false);
  };

  return (
    <section className="settings-section">
      <h2 data-cy="setting-section-title">{i18n('settings.operators.title')}</h2>
      <section className="settings-section-body billing">
        <main className="seatsMain settings-wide-column">
          <h3 data-cy="payment">{i18n('settings.operators.managementTitle')}</h3>
          <section data-cy="seats-control-block" style={{ display: 'flex', flexDirection: 'column' }}>
            <section className="seats-count">
              <Modal visible={showAddModal} title={i18n('settings.operators.edit')} okText="Submit" cancelText="Cancel" onCancel={onCancel} onOk={onOk}>
                <Form form={form} layout="vertical" name="add_operator">
                  <Form.Item name="firstName" label="First Name" rules={[{ required: true, message: "Please fill in the operator's first name." }]}>
                    <Input />
                  </Form.Item>
                  <Form.Item name="lastName" label="Last Name" rules={[{ required: true, message: "Please fill in the operator's last name." }]}>
                    <Input />
                  </Form.Item>
                  <Form.Item name="email" label="Email" rules={[{ type: 'email', message: 'Please provide a valid email' }]}>
                    <Input />
                  </Form.Item>
                  <Form.Item name="accessControl" label="Access Control" initialValue="None">
                    <Radio.Group defaultValue="None">
                      <Radio value="None">None</Radio>
                      <Radio value="ITAR">ITAR</Radio>
                    </Radio.Group>
                  </Form.Item>
                  <Form.Item
                    name="pin"
                    label="PIN"
                    rules={[
                      { required: form.getFieldValue('accessControl') === 'ITAR', message: 'A PIN is required for ITAR-enabled operators' },
                      { max: 8, message: 'Please enter a code between 4 and 8 characters' },
                      { min: 4, message: 'Please enter a code between 4 and 8 characters' },
                    ]}
                  >
                    <Input />
                  </Form.Item>
                </Form>
              </Modal>
            </section>
            <section className="search-filter" style={{ display: 'flex' }}>
              <Button
                type="primary"
                size="large"
                className="btn-primary"
                data-cy="invite-users"
                style={{ marginRight: '16px' }}
                onClick={() => {
                  setShowAddModal(true);
                }}
              >
                <PlusSquareOutlined />
                {i18n('settings.operators.add')}
              </Button>
              <Input //
                size="large"
                placeholder="search by name"
                onChange={searchOperators}
                prefix={<SearchOutlined />}
                allowClear
              />
            </section>
            <section className="operators-list" data-cy="review-operators">
              <OperatorMiniList //
                title="Operators"
                icon="idcard"
                operators={filteredOperators}
                handleEdit={handleEdit}
                handleDelete={handleDelete}
                permissions={permissions}
                settingsContainer={settingsContainer}
              />
            </section>
          </section>
        </main>
      </section>
    </section>
  );
};

export default Operators;
