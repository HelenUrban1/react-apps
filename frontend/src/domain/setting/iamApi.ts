import gql from 'graphql-tag';
import { FileInput } from 'types/file';
import { User, UserStatusEnum } from 'types/user';

type UserWithRolesOrderByEnum = 'email_ASC' | 'email_DESC' | 'fullName_ASC' | 'fullName_DESC' | 'disabled_ASC' | 'disabled_DESC' | 'createdAt_ASC' | 'createdAt_DESC';

export interface IamListUsersFilterInput {
  id?: string;
  fullName?: string;
  email?: string;
  status?: string;
  createdAtRange?: Date[];
  activeAccountId?: string;
}

export interface IamListUsers {
  iamListUsers: {
    count: number;
    rows: User[];
  };
}

export interface IamEdit {
  iamEdit: User;
}

export interface IamEditFields {
  firstName?: string;
  lastName?: string;
  phoneNumber?: string;
  avatars?: FileInput[];
  roles?: string[];
  accessControl?: boolean;
  status?: UserStatusEnum;
}

export interface IamEditInput {
  id: string;
  firstName?: string;
  lastName?: string;
  phoneNumber?: string;
  avatars?: FileInput[];
  roles?: string[];
  accessControl?: Boolean;
  status: UserStatusEnum;
}

export interface IamEditInputVars {
  data: IamEditInput;
}

export interface IamChangeOwner {
  iamChangeOwner: User[];
}

export interface IamChangeOwnerInput {
  id: string;
}

export interface IamChangeOwnerInputVars {
  data: IamChangeOwnerInput;
}

export interface IamEditStatus {
  iamChangeAccountMemberStatus: User;
}

export interface IamStatusInput {
  id: string;
  disabled: Boolean;
}

export interface IamStatusInputVars {
  data: IamStatusInput;
}

export interface IamUsersListVars {
  filter?: IamListUsersFilterInput;
  orderBy?: UserWithRolesOrderByEnum;
  limit?: number;
  offset?: number;
}

export interface authInviteUserVars {
  email: string;
  roles?: string[];
}
export interface authInviteUser {
  authInviteUser: boolean;
}

export interface authResendInviteVars {
  email: string;
}
export interface authResendInvite {
  authResendInvite: boolean;
}

export interface authCancelInviteVars {
  email: string;
}

export interface authRequestAcceptVars {
  email: string;
}
export interface authCancelInvite {
  authCancelInvite: boolean;
}

export interface authRequestAccept {
  authRequestAccept: boolean;
}

export interface authRequestReject {
  authRequestReject: boolean;
}

export const iamAPI = {
  mutate: {
    edit: gql`
      mutation IAM_USER_UPDATE($data: IamEditInput!) {
        iamEdit(data: $data) {
          id
          fullName
          email
          phoneNumber
          avatars {
            id
            name
            sizeInBytes
            publicUrl
            privateUrl
          }
          activeAccountId
          accountMemberships {
            id
            accountId
            status
            roles
            accessControl
          }
          disabled
          roles
          paid
          status
          emailVerified
        }
      }
    `,
    archiveOverflow: gql`
      mutation IAM_ARCHIVE_OVERFLOW($paid: Int!) {
        iamArchiveOverflow(paid: $paid)
      }
    `,
    changeStatus: gql`
      mutation IAM_STATUS_UPDATE($data: IamStatusInput!) {
        iamChangeAccountMemberStatus(data: $data) {
          id
          fullName
          email
          phoneNumber
          avatars {
            id
            name
            sizeInBytes
            publicUrl
            privateUrl
          }
          activeAccountId
          accountMemberships {
            id
            accountId
            status
            roles
            accessControl
          }
          disabled
          roles
          paid
          status
          emailVerified
        }
      }
    `,
    transferOwner: gql`
      mutation IAM_TRANSFER_OWNER($data: IamChangeOwnerInput!) {
        iamChangeOwner(data: $data) {
          id
          fullName
          email
          phoneNumber
          avatars {
            id
            name
            sizeInBytes
            publicUrl
            privateUrl
          }
          activeAccountId
          accountMemberships {
            id
            accountId
            status
            roles
            accessControl
          }
          disabled
          roles
          paid
          status
          emailVerified
          createdAt
        }
      }
    `,
    invite: gql`
      mutation authInviteUser($email: String!, $roles: [String], $control: Boolean) {
        authInviteUser(email: $email, roles: $roles, control: $control)
      }
    `,
    resendInvite: gql`
      mutation authResendInvite($email: String!) {
        authResendInvite(email: $email)
      }
    `,
    cancelInvite: gql`
      mutation authCancelInvite($email: String!) {
        authCancelInvite(email: $email)
      }
    `,
    acceptRequest: gql`
      mutation authRequestAccept($email: String!, $token: String, $userId: String) {
        authRequestAccept(email: $email, token: $token, userId: $userId)
      }
    `,
    rejectRequest: gql`
      mutation authRequestReject($adminEmail: String!, $token: String!) {
        authRequestReject(adminEmail: $adminEmail, token: $token)
      }
    `,
  },
  query: {
    list: gql`
      query IAM_USERS_LIST($filter: IamListUsersFilterInput, $orderBy: UserWithRolesOrderByEnum, $limit: Int, $offset: Int) {
        iamListUsers(filter: $filter, orderBy: $orderBy, limit: $limit, offset: $offset) {
          count
          rows {
            id
            fullName
            email
            phoneNumber
            avatars {
              id
              name
              sizeInBytes
              publicUrl
              privateUrl
            }
            activeAccountId
            accountMemberships {
              id
              accountId
              status
              roles
              accessControl
            }
            disabled
            mfaRegistered
            roles
            paid
            status
            emailVerified
            emailVerificationToken
            emailVerificationTokenExpiresAt
            createdAt
          }
        }
      }
    `,
  },
};

export const buildIamEditInput = (id: string, user: User, input: IamEditFields) => {
  const membership = user.accountMemberships.find((accountMembership) => accountMembership.accountId === user.activeAccountId);
  const editInput: IamEditInput = {
    id,
    accessControl: membership!.accessControl,
    status: membership!.status,
    ...input,
  };
  return editInput;
};

export default iamAPI;
