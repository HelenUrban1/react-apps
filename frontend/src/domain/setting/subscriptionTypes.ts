export type BillingPeriodEnum = 'Pay_as_you_go' | 'Monthly' | 'Annually' | 'Multi_Year';
export type SubscriptionStatusEnum = 'Active' | 'Active (Pending Cancellation)' | 'Expired' | 'Trialing' | 'Trial_Ended';

export interface Subscription {
  id: string;
  accountId: string;
  billingId: string;
  companyId: string;
  enforcePlan: boolean;
  paymentType?: string;
  paymentSystemId: string;
  providerProductId: number;
  providerProductHandle: string;
  providerFamilyHandle: string;
  previousStatus: string;
  status: string;
  activatedAt?: Date;
  paidUserSeatsUsed?: number;
  drawingsUsed?: number;
  partsUsed?: number;
  measurementsUsed?: number;
  pendingComponents?: string;
  components?: string;
  billingPeriod?: BillingPeriodEnum;
  renewalDate: string;
  pendingTier?: number | null;
  pendingBillingPeriod?: BillingPeriodEnum | null;
  pendingBillingAmountInCents?: number | null;
  currentPeriodStartedAt?: Date;
  currentPeriodEndsAt?: Date;
  currentBillingAmountInCents?: number;
  cancelAtEndOfPeriod?: boolean;
  canceledAt?: Date | null;
  trialEndedAt?: Date;
  trialStartedAt?: Date;
  expiresAt?: Date;
  updatedAt?: Date;
  automaticallyResumeAt?: Date;
  balanceInCents?: number;
  cancellationMessage?: string;
  cancellationMethod?: string;
  couponCode?: string;
  couponCodes?: string[];
  couponUseCount?: number;
  couponUsesAllowed?: number;
  createdAt?: Date;
  delayedCancelAt?: Date;
  netTerms?: number;
  nextAssessmentAt?: Date;
  nextProductHandle?: string;
  nextProductId?: string;
  offerId?: string;
  payerId?: string;
  paymentCollectionMethod?: string;
  plan?: string;
  planNotes?: string;
  productPriceInCents?: number;
  productPricePointId?: string;
  productVersionNumber?: string;
  reasonCode?: string;
  receivesInvoiceEmails?: boolean;
  referralCode?: string;
  signupPaymentId?: string;
  signupRevenue?: string;
  snapDay?: string;
  storedCredentialTransactionId?: string;
  totalRevenueInCents?: number;
  cardId?: number;
  maskedCardNumber?: string;
  cardType?: string;
  expirationMonth?: number;
  expirationYear?: number;
  customerId?: string;
  termsAcceptedOn?: string;
  // [key: string]: string | number | Date | boolean | BillingPeriodEnum | undefined | null | string[];
}

export interface SubscriptionInput {
  accountId: string;
  billingId: string;
  companyId: string;
  billingToken: string;
  productId: number;
}

export interface SubscriptionUpdateInput {
  billingToken?: string;
  due?: number;
  accountId: string;
  billingId: string;
  companyId: string;
  billingPeriod?: string;
  renewalDate: string;
  pendingTier?: number | null;
  pendingBillingPeriod?: BillingPeriodEnum | null;
  pendingBillingAmountInCents?: number | null;
  enforcePlan?: boolean;
  providerProductId?: number;
  providerProductHandle?: string;
  providerFamilyHandle: string;
  previousStatus?: string;
  status?: string;
  activatedAt?: Date;
  paidUserSeatsUsed?: number;
  drawingsUsed?: number;
  partsUsed?: number;
  measurementsUsed?: number;
  pendingComponents?: string;
  components?: string;
  currentPeriodStartedAt?: Date;
  currentPeriodEndedAt?: Date;
  cancelAtEndOfPeriod?: boolean;
  canceledAt?: Date;
  trialEndedAt?: Date;
  trialStartedAt?: Date;
  expiresAt?: Date;
  updatedAt?: Date;
  automaticallyResumeAt?: Date;
  balanceInCents?: number;
  cancellationMessage?: string;
  cancellationMethod?: string;
  couponCode?: string;
  couponCodes?: string[];
  couponUseCount?: number;
  couponUsesAllowed?: number;
  createdAt?: Date;
  currentBillingAmountInCents?: number;
  currentPeriodEndsAt?: Date;
  delayedCancelAt?: Date;
  netTerms?: number;
  nextAssessmentAt?: Date;
  nextProductHandle?: string;
  nextProductId?: string;
  offerId?: string;
  payerId?: string;
  paymentCollectionMethod?: string;
  paymentSystemId?: string;
  paymentType?: string;
  plan?: string;
  planNotes?: string;
  productPriceInCents?: number;
  productPricePointId?: string;
  productVersionNumber?: string;
  reasonCode?: string;
  receivesInvoiceEmails?: boolean;
  referralCode?: string;
  signupPaymentId?: string;
  signupRevenue?: string;
  snapDay?: string;
  storedCredentialTransactionId?: string;
  totalRevenueInCents?: number;
  cardId?: number;
  maskedCardNumber?: string;
  cardType?: string;
  expirationMonth?: number;
  expirationYear?: number;
  customerId?: string;
  termsAcceptedOn?: string;
}

export interface ComponentPrice {
  component_id: number;
  ending_quantity: number | null;
  formatted_unit_price: string;
  id: number;
  price_point_id: number;
  price_point_handle?: string;
  segment_id: number | null;
  starting_quantity: number;
  unit_price: string;
}

export interface Component {
  component_id: number;
  subscription_id: number;
  component_handle: string | null;
  allocated_quantity: number;
  enabled?: boolean;
  name: string;
  kind: string;
  unit_name: string;
  pricing_scheme: string | null;
  price_point_id: number | null;
  price_point_handle: string | null;
}

export interface ComponentInput {
  component_id: number;
  handle?: string;
  product_family_key: string;
  quantity: number;
  memo?: string;
  price_point?: string | number;
  unit_price?: string;
  name?: string;
  type?: string;
  proration_downgrade_scheme?: string;
  payment_collection_method?: string;
  proration_upgrade_scheme?: string;
  accrue_charge?: boolean;
  upgrade_charge?: string;
  downgrade_credit?: string;
}

export interface ProductFamily {
  accounting_code: string | null;
  created_at: string;
  description: string | null;
  handle: string;
  id: number;
  name: string;
  updated_at: string;
}

export interface PricePoint {
  id: number;
  default: boolean;
  name: string;
  pricing_scheme: string;
  component_id: number;
  handle: string;
  archived_at: string;
  created_at: string;
  updated_at: string;
  prices: ComponentPrice[];
}

export interface CatalogComponent {
  allow_fractional_quantities: boolean;
  archived: boolean;
  archived_at: string | null;
  created_at: string | null;
  default_price_point_id: number;
  default_price_point_name: string;
  description: string | null;
  downgrade_credit: string | null;
  handle: string;
  hide_date_range_on_invoice: boolean;
  id: number;
  kind: string;
  name: string;
  price_per_unit_in_cents: number | null;
  price_point_count: number;
  price_points_url: string;
  prices: PricePoint[];
  pricing_scheme: 'per_unit';
  product_family_id: number;
  product_family_name: string;
  recurring: boolean;
  tax_code: string | null;
  taxable: boolean;
  unit_name: string;
  unit_price: string | null;
  updated_at: string;
  upgrade_charge: string | null;
}

export interface CatalogProduct {
  accounting_code: string | null;
  archived_at: string | null;
  created_at: string;
  default_product_price_point_id: number;
  description: string | null;
  expiration_interval: number | null;
  expiration_interval_unit: string;
  handle: string;
  id: number;
  initial_charge_after_trial: false;
  initial_charge_in_cents: number | null;
  interval: number;
  interval_unit: string;
  name: string;
  price_in_cents: number;
  product_family: ProductFamily;
  product_price_point_handle: string;
  product_price_point_id: number;
  product_price_point_name: string;
  public_signup_pages: any[];
  request_billing_address: false;
  request_credit_card: true;
  require_billing_address: false;
  require_credit_card: false;
  require_shipping_address: false;
  return_params: string | null;
  tax_code: string | null;
  taxable: boolean;
  trial_interval: null;
  trial_interval_unit: string;
  trial_price_in_cents: string | null;
  update_return_params: string | null;
  update_return_url: string | null;
  updated_at: string;
  version_number: number;
}

export interface Catalog {
  [key: string]: {
    accounting_code: string | null;
    components: { [key: string]: CatalogComponent };
    created_at: string;
    description: string | null;
    handle: string;
    id: number;
    name: string;
    products: { [key: string]: CatalogProduct };
    updated_at: string;
  };
}

export interface ComponentInfo {
  [key: string]: {
    name: string;
    metric: string;
    unit: string;
    handle: string;
    id: number;
    used: number;
    purchased: number;
    priceId: string;
    price: number;
    type: string;
    priceMap: { [key: string]: ComponentPrice };
    prices: { [key: string]: ComponentPrice[] };
  };
}

export interface Info {
  name: string;
  unit: string;
  handle: string;
  id: number;
  used: number;
  purchased: number;
  priceId: string;
  price: number;
  type: string;
  priceMap: { [key: string]: ComponentPrice };
  prices: ComponentPrice[];
}
