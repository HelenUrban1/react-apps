import React, { useState, useEffect, useRef } from 'react';
import { i18n } from 'i18n';
import moment from 'moment';
import log from 'modules/shared/logger';
import { BillingPeriodEnum, Component, ComponentInfo, ComponentInput, Subscription } from 'domain/setting/subscriptionTypes';
import { convertCentsToUSD } from 'utils/DataCalculation';
import { Button, Modal, Badge } from 'antd';
import { useMutation } from '@apollo/client';
import Analytics from 'modules/shared/analytics/analytics';
import { useDispatch, useSelector } from 'react-redux';
import { doUpdateCurrentSubscription } from 'modules/subscription/subscriptionActions';
import { AppState } from 'modules/state';
import { parseValidJSONString } from 'utils/textOperations';
import { isBeforeStaging } from 'config';
import { ChangeSubscriptionModal } from './modals/ChangeSubscriptionModal';
import { UpgradeModal } from './modals/UpgradeModal';
import { CancelSubscriptionModal } from './modals/CancelSubscriptionModal';
import { SubscriptionAPI, cancellationInput, reactivationInput } from '../subscriptionApi';

export const SubscriptionInfo = () => {
  const dispatch = useDispatch();
  const { data: subscription, catalog, components, state } = useSelector((reduxState: AppState) => reduxState.subscription);
  const product = useRef(
    subscription?.providerFamilyHandle && catalog && subscription?.providerProductId && catalog[subscription.providerFamilyHandle] ? catalog[subscription.providerFamilyHandle].products[subscription.providerProductId.toString()] : null,
  );

  // Chargify can't prorate a change in billing cycle, so we've blocked this feature for the moment
  // Hides the billing period changes unless on dev (for testing) or converting from trial to subscription (so they can set the initial billing cycle)
  const allowBillingChange = isBeforeStaging;

  const [notice, setNotice] = useState<JSX.Element[]>([]);
  // TODO: rework this so we don't repeat these functions everywhere we call the chargify credit card modal
  const [visibility, setVisibility] = useState<boolean>(false);

  const showChangeModal = () => {
    setVisibility(true);
  };

  const closeChangeModal = () => {
    setVisibility(false);
  };

  const closeCardModal = () => {
    Analytics.track({
      event: Analytics.events.subscriptionChangeCardCancel,
      subscription,
      properties: {
        'subscription.period': subscription?.billingPeriod,
        // 'subscription.seats': subscription?.seatsPurchased,
        // 'subscription.drawings': subscription?.drawingsPurchased,
        'subscription.tier': subscription?.providerProductId,
      },
    });
    Modal.destroyAll();
  };

  const [updatedBillingToken] = useMutation(SubscriptionAPI.mutate.addPaymentMethod, {
    onCompleted: (data) => {
      if (data?.subscriptionBillingPaymentMethodCreate) {
        closeCardModal();
      } else {
        Modal.error({
          title: i18n('errors.chargify.changeCard.message'),
          content: <p>{i18n('errors.chargify.changeCard.description')}</p>,
          onOk: closeCardModal,
        });
      }
    },
    onError: (error) => {
      log.error('SubscriptionInfo.updatedBillingToken', error);
      return false;
    },
  });

  const saveCreditCard = async (token: string) => {
    Analytics.track({
      event: Analytics.events.subscriptionChangeCardConfirm,
      subscription,
      properties: {
        'subscription.period': subscription?.billingPeriod,
        // 'subscription.seats': subscription?.seatsPurchased,
        // 'subscription.drawings': subscription?.drawingsPurchased,
        'subscription.tier': subscription?.providerProductId,
        billingToken: token,
      },
    });
    await updatedBillingToken({
      variables: {
        id: subscription?.id,
        data: {
          accountId: subscription?.accountId,
          billingToken: token,
          customerId: subscription?.customerId,
        },
      },
    });
  };

  // Set Pending Notices
  useEffect(() => {
    const messages = [];
    if (subscription?.pendingBillingPeriod) {
      messages.push(
        <p className="notice" data-cy="subscription-renewal-downgrade" key="period">
          {`Your billing period is scheduled to switch to ${subscription.pendingBillingPeriod} on ${moment(subscription.renewalDate).format('MMM Do, YYYY')}.`}
        </p>,
      );
    }
    if (subscription?.pendingTier) {
      messages.push(
        <p className="notice" data-cy="subscription-renewal-downgrade" key="tier">
          {`Your subscription tier is scheduled to switch to ${catalog[subscription.providerFamilyHandle].products[subscription.pendingTier.toString()]?.name} on ${moment(subscription.renewalDate).format('MMM Do, YYYY')}.`}
        </p>,
      );
    }
    if (subscription?.pendingComponents) {
      const pendingComp: Component[] = parseValidJSONString(subscription.pendingComponents);
      if (pendingComp && pendingComp.length > 0) {
        pendingComp.forEach((comp) => {
          messages.push(
            <p className="notice" data-cy="subscription-renewal-downgrade" key={comp.component_id}>
              {`Your subscription ${comp.unit_name} allocation is scheduled to switch to ${comp.allocated_quantity} on ${moment(subscription.renewalDate).format('MMM Do, YYYY')}.`}
            </p>,
          );
        });
      }
    }
    setNotice(messages);
  }, [subscription?.pendingBillingPeriod, subscription?.pendingTier, subscription?.pendingComponents]);

  // Set product when subscription or catalog changes
  useEffect(() => {
    if (subscription?.providerProductId && product?.current?.id !== subscription.providerProductId && catalog && catalog[subscription.providerFamilyHandle]) {
      product.current = catalog[subscription.providerFamilyHandle].products[subscription.providerProductId.toString()];
    }
  }, [catalog, subscription?.providerProductId]);

  if (!subscription) {
    return <></>;
  }

  // Constructs the Renewal info message based on subscription status, cost, and period
  const getRenewalInfo = () => {
    let renewalInfo = '';
    if (!subscription) {
      renewalInfo += `${i18n('errors.chargify.subscriptionInfo.description')}`;
      return renewalInfo;
    }

    if (subscription?.cancelAtEndOfPeriod) {
      // subscription has been cancelled, account is still in the grace period
      renewalInfo += `${i18n('settings.billing.info.grace')}`;
    } else if (state === 'expired' || state === 'canceled') {
      // renewal period has passed, account is expired
      renewalInfo += `${i18n('settings.billing.info.expired')}`;
    } else if (subscription.paymentCollectionMethod === 'automatic') {
      // active account set up for auto renewal
      renewalInfo += `${i18n('settings.billing.info.autoRenewal')}`;
    } else {
      // active account set to pay by invoice
      renewalInfo += `${i18n('settings.billing.info.manualRenewal')}`;
    }

    return `${renewalInfo} ${moment(state === 'canceled' ? subscription.canceledAt : subscription.renewalDate).format('MMM. Do, YYYY')}.`;
  };

  // Constructs text for Next Payment message
  const getPaymentInfo = () => {
    const payment = subscription?.pendingBillingAmountInCents !== undefined && subscription?.pendingBillingAmountInCents !== null ? subscription.pendingBillingAmountInCents : subscription.currentBillingAmountInCents;
    const period = allowBillingChange ? subscription?.pendingBillingPeriod || subscription.billingPeriod : subscription.billingPeriod;
    let nextAssessmentAt = moment(subscription.nextAssessmentAt);

    if (state === 'pending') {
      const interval = period === 'Annually' ? 'years' : 'months';
      nextAssessmentAt = moment(subscription.termsAcceptedOn).add(1, interval);
    }

    if (payment === undefined || payment === null || !period) {
      return null;
    }

    const price = convertCentsToUSD(payment);
    return (
      <>
        {subscription.status === 'trialing' && state === 'pending' && subscription.paymentCollectionMethod === 'remittance' ? (
          <section className="sub-info-item">
            <Badge
              status="warning"
              text={
                <>
                  {i18n('settings.billing.info.remittanceDue')}
                  <span className="span-bold">{price}</span>
                  {i18n('settings.billing.info.on')}
                  <span className="span-bold">{moment(subscription.nextAssessmentAt).format('MMM. Do, YYYY')}</span>
                </>
              }
            />
          </section>
        ) : (
          <section className="sub-info-item">
            {i18n('common.next')}
            <span data-cy="subscription-renewal-period" className="span-bold">
              {` ${i18n(`settings.billing.info.${period.toString().toLocaleLowerCase()}`)} `}
            </span>
            {i18n('settings.billing.info.paymentOf')}
            <span data-cy="subscription-renewal-price" className="span-bold">
              {` ${price} `}
            </span>
            {i18n('settings.billing.info.billedOn')}
            <span data-cy="subscription-renewal-date" className="span-bold">
              {` ${moment(nextAssessmentAt).format('MMM. Do, YYYY')} `}
            </span>
          </section>
        )}
      </>
    );
  };

  // checks if there's an available tier to upgrade to
  const canUpgrade = () => {
    let upgrade = false;
    if (product.current?.handle.toLowerCase().includes('standard')) {
      const products = catalog[subscription?.providerFamilyHandle || 0]?.products || {};
      const advanced = Object.values(products).some((prod) => prod.handle.toLowerCase().includes('advanced'));
      if (advanced) {
        upgrade = true;
      }
    }
    return upgrade;
  };

  // get the SubscriptionInput formatted for cancellation and call the API
  const handleSubscriptionCancellation = () => {
    const input = cancellationInput(subscription);
    const updatedSubscription = SubscriptionAPI.createGraphqlObject({
      ...subscription,
      ...input,
    });
    Analytics.track({
      event: Analytics.events.accountSubscriptionCancellationRequested,
      properties: {
        'chargify.id': subscription.paymentSystemId,
        'chargify.customer': subscription.customerId,
        'account.id': subscription.accountId,
      },
    });
    dispatch(doUpdateCurrentSubscription(subscription.id, updatedSubscription, 'Delayed_Cancel'));
  };

  const changePeriod = (sub: Subscription, period: BillingPeriodEnum) => {
    const input = { ...sub };
    if (allowBillingChange) {
      input.billingPeriod = period;
      input.pendingBillingPeriod = null;
    } else if (input.pendingBillingPeriod === period) {
      input.pendingBillingPeriod = null;
    } else {
      input.pendingBillingPeriod = period;
    }
    return input;
  };

  const changePrice = (sub: Subscription, price: number) => {
    const input = { ...sub };
    if (input.currentBillingAmountInCents && price < input.currentBillingAmountInCents) {
      input.pendingBillingAmountInCents = price;
    } else {
      input.currentBillingAmountInCents = price;
      input.pendingBillingAmountInCents = null;
    }
    return input;
  };

  const changeComponents = (sub: Subscription, componentInputs: ComponentInput[], pendingComponents: Component[] = []) => {
    const input = { ...sub };
    const newComponentInputs: ComponentInput[] = [];
    const pending: Component[] = [...pendingComponents];
    componentInputs.forEach((comp) => {
      const original = components.find((c) => c.component_id === comp.component_id);
      if (original && original.allocated_quantity > comp.quantity) {
        pending.push({ ...original, allocated_quantity: comp.quantity });
      } else if (original && original.allocated_quantity <= comp.quantity) {
        newComponentInputs.push({ ...comp });
      }
    });

    if (pending) {
      input.pendingComponents = JSON.stringify(pending);
    }
    return { input, newComponentInputs };
  };

  // get the SubscriptionInput formatted for cancellation and call the API
  const handleSubscriptionChange = ({
    //
    productHandle,
    period,
    price,
    handle,
    componentInputs,
    pendingComponents,
  }: {
    productHandle?: string;
    period?: BillingPeriodEnum;
    price?: number;
    handle?: string;
    componentInputs?: ComponentInput[];
    pendingComponents?: ComponentInfo;
  }) => {
    let input = { ...subscription };

    if (period) {
      input = changePeriod(subscription, period);
    }
    if (price !== undefined) {
      input = changePrice(input, price);
    }
    if (productHandle) {
      input.providerProductHandle = productHandle;
    }
    let inputs: ComponentInput[] = [];
    const pending: Component[] = [];
    if (pendingComponents) {
      // Include pending changes from a deferred billing change
      Object.values(pendingComponents).forEach((comp) => {
        const original = components.find((c) => c.component_id === comp.id);
        if (!original) {
          return;
        }
        pending.push({ ...original, allocated_quantity: comp.purchased, price_point_id: parseInt(comp.priceId, 10) });
      });
    }
    if (componentInputs) {
      const compUpdate = changeComponents(input, componentInputs, pending);
      input = compUpdate.input;
      inputs = compUpdate.newComponentInputs;
    }
    const data = SubscriptionAPI.createGraphqlObject(input);

    Analytics.track({
      event: Analytics.events.accountSubscriptionUpdated,
      properties: {
        'chargify.id': subscription.paymentSystemId,
        'chargify.customer': subscription.customerId,
        'account.id': subscription.accountId,
      },
    });
    dispatch(doUpdateCurrentSubscription(subscription.id, data, 'Update_Subscription', inputs, undefined, handle));
  };

  // get the SubscriptionInput formatted for reactivation and call the API
  const handleSubscriptionReactivation = () => {
    const input = reactivationInput(subscription);
    const updatedSubscription = SubscriptionAPI.createGraphqlObject({
      ...subscription,
      ...input,
    });
    Analytics.track({
      event: Analytics.events.accountSubscriptionReactivation,
      properties: {
        'chargify.id': subscription.paymentSystemId,
        'chargify.customer': subscription.customerId,
        'account.id': subscription.accountId,
      },
    });
    dispatch(doUpdateCurrentSubscription(subscription.id, updatedSubscription, 'Reactivate_Pending'));
  };

  // Returns the buttons/modals the user needs depending on the status of the subscription
  const getSubscriptionButtons = () => {
    if (subscription.cancelAtEndOfPeriod || subscription.canceledAt) {
      return (
        <div className="info-buttons">
          <Button
            onClick={() => {
              handleSubscriptionReactivation();
            }}
            type="primary"
            size="large"
            className="btn-primary"
            data-cy="reactivate-subscription-btn"
          >
            {i18n('settings.billing.info.reactivate')}
          </Button>
        </div>
      );
    }
    return (
      <div className="info-buttons">
        {canUpgrade() && <UpgradeModal />}

        <Button type="default" size="large" className="billing-button" onClick={showChangeModal} data-cy="change-modal-btn">
          {i18n('settings.billing.buttons.change')}
        </Button>

        <CancelSubscriptionModal expDate={moment(subscription.renewalDate).format('MMM. Do, YYYY')} handleCancellation={handleSubscriptionCancellation} />
      </div>
    );
  };
  const productFamily = catalog ? catalog[subscription?.providerFamilyHandle] : null;
  const productHandle = productFamily ? productFamily.products[subscription?.providerProductHandle]?.handle : null;

  return (
    <section className="settings-subsection">
      <h3 className="inline" data-cy="subscription-current-tier">
        {`${i18n(`entities.subscription.tier[${productHandle}]`)} ${i18n('entities.subscription.tier.title')}`}
      </h3>
      {state !== 'active' && <h3 className="inline cancel" data-cy="settings-billing">{`(${i18n(`settings.billing.info.${state}`)})`}</h3>}
      {/* <h4>{`${i18n('settings.billing.info.subscription')}`}</h4> */}

      {notice && notice.map((item) => item)}
      <div className="sub-info">
        <span className="sub-info-item">{getRenewalInfo()}</span>
        {(state === 'active' || state === 'pending') && getPaymentInfo()}
      </div>
      {getSubscriptionButtons()}
      <ChangeSubscriptionModal //
        saveCreditCard={saveCreditCard}
        visibility={visibility}
        closeChangeModal={closeChangeModal}
        closeCardModal={closeCardModal}
        handleSubscriptionChange={handleSubscriptionChange}
      />
    </section>
  );
};
