import React, { useState, useEffect } from 'react';
import billingApi from 'modules/shared/billing/billingApi';
import { useLazyQuery, useQuery } from '@apollo/client';
import moment from 'moment';
import { saveAs } from 'file-saver';
import { i18n } from 'i18n';
import log from 'modules/shared/logger';
import Message from 'view/shared/message';
import { notification } from 'antd';
import { User } from 'types/user';
import UserAPI, { UserList } from 'graphql/user';
import { useDispatch, useSelector } from 'react-redux';
import { doAddPayment, doRemovePayment, doUpdateCurrentSubscription } from 'modules/subscription/subscriptionActions';
import { AppState } from 'modules/state';
import { InvoiceData } from './types';
import Invoices from './Invoices';
import Payment from './Payment';
import { SubscriptionAPI } from '../subscriptionApi';
import { SubscriptionInfo } from './SubscriptionInfo';
import Usage from './Usage';

interface subscribedProps {
  manageSeats: () => void;
  users: User[];
  permissions: {
    hasOwnership: boolean;
    hasManageAccess: boolean;
    hasBillingAccess: boolean;
  };
}

export const SubscribedBilling = ({ users, manageSeats, permissions }: subscribedProps) => {
  const dispatch = useDispatch();
  const { data: subscription, auth } = useSelector((reduxState: AppState) => reduxState.subscription);

  const [invoices, setInvoices] = useState<InvoiceData[]>();

  const updateBillingCard = async (token: string) => {
    if (!token) {
      log.error('Cannot update chargify without billing token');
      return;
    }
    if (!subscription) {
      log.error('Cannot update chargify without subscription information');
      return;
    }
    if (!subscription.customerId) {
      log.error('Cannot update chargify without customer information');
      return;
    }

    dispatch(doAddPayment({ id: subscription.id, accountId: subscription.accountId, customerId: subscription.customerId, billingToken: token }));
  };

  const removeBillingCard = async () => {
    if (!subscription) {
      log.error('Cannot update chargify without subscription information');
      return;
    }
    if (!subscription.customerId) {
      log.error('Cannot update chargify without customer information');
      return;
    }

    dispatch(doRemovePayment({ id: subscription.id, customer: subscription.customerId }));
  };

  const base64ToArrayBuffer = (base64Input: string) => {
    const binaryString = window.atob(base64Input);
    const binaryLen = binaryString.length;
    const bytes = new Uint8Array(binaryLen);
    for (let i = 0; i < binaryLen; i++) {
      bytes[i] = binaryString.charCodeAt(i);
    }
    return bytes;
  };

  const [downloadRequest] = useLazyQuery(billingApi.query.downloadInvoice, {
    onCompleted: (data) => {
      const { id, base64Payload } = data.subscriptionBillingInvoiceDownload;
      const binaryPayload = base64ToArrayBuffer(base64Payload);
      const blob = new Blob([binaryPayload], { type: 'application/pdf' });

      let fileName = 'InspectionXpert-Invoice';
      const invoiceData = invoices ? invoices.find((row: any) => row.uid === id) : null;
      if (invoiceData) {
        const date = moment(invoiceData.issue_date).format('YYYY-MM-DD');
        fileName += `-${date}`;
      }
      fileName += '.pdf';

      saveAs(blob, fileName);
    },
    onError: (error) => {
      log.error('SubscribedBilling.downloadRequest error', error);
      notification.error({
        message: i18n('errors.chargify.download.message'),
        description: i18n('errors.chargify.download.description'),
        duration: 0,
      });
    },
  });

  const doDownload = (event: any) => {
    event.preventDefault();

    try {
      const doesBrowserSupportDownloads = !!new Blob();
      if (!doesBrowserSupportDownloads) {
        throw new Error('');
      }
    } catch (error) {
      Message.error(i18n('entities.subscription.billing.invoice.unsupported'));
      return;
    }

    downloadRequest({
      variables: {
        id: event.target.getAttribute('data-invoice-id'),
      },
    });
  };

  const [fetchInvoices, { loading: invoicesLoading }] = useLazyQuery(billingApi.query.getInvoices, {
    variables: { customer: subscription?.customerId },
    fetchPolicy: 'cache-and-network',
    onCompleted: (data) => {
      if (data?.subscriptionBillingInvoicesList) {
        const foundInvoices = [...data?.subscriptionBillingInvoicesList];
        setInvoices(foundInvoices.sort((a: InvoiceData, b: InvoiceData) => moment(b.issue_date).diff(moment(a.issue_date))));
      }
    },
    onError: (error) => {
      log.error('SubscribedBilling.getInvoices error', error);
      notification.error({
        message: i18n('errors.chargify.invoices.message'),
        description: i18n('errors.chargify.invoices.description'),
        duration: 0,
      });
      setInvoices([]);
    },
  });

  // fetch invoices on page load and subscription change
  useEffect(() => {
    fetchInvoices();
  }, [subscription]);

  useQuery<UserList>(UserAPI.query.list, {
    variables: { filter: { roles: ['Billing', 'Owner', 'Admin'] } },
  });

  const setBillingContact = (id: string) => {
    if (!subscription) {
      return;
    }
    const data = SubscriptionAPI.createGraphqlObject({
      ...subscription,
      billingId: id,
    });
    if (data) {
      dispatch(doUpdateCurrentSubscription(subscription?.id, data, 'Update_Subscription'));
    }
  };

  return (
    <section id="billingSettings" className="settings-section billing">
      <h2 data-cy="setting-section-title">{i18n('settings.billing.title')}</h2>
      <section className="settings-section-body billing">
        <main className="billingMain settings-wide-column">
          {subscription && <SubscriptionInfo />}
          <Usage manageSeats={manageSeats} hasManageAccess={permissions.hasManageAccess} />
          <Invoices rows={invoices || []} doDownload={doDownload} loading={invoicesLoading} />
        </main>
        {auth && (
          <aside className="billingSidebar settings-thin-column">
            <Payment
              users={users.filter((memberUser) => {
                const membership = memberUser.accountMemberships.find((acct) => acct.accountId === memberUser.activeAccountId);
                return (memberUser.roles?.includes('Billing') || memberUser.roles?.includes('Owner')) && membership && membership.status === 'Active';
              })}
              setBillingContact={setBillingContact}
              updateBillingCard={updateBillingCard}
              removeBillingCard={removeBillingCard}
            />
          </aside>
        )}
      </section>
    </section>
  );
};
