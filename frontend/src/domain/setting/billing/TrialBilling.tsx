import React from 'react';
import { i18n } from 'i18n';
import { PricingTable } from 'styleguide/PricingTable/PricingTable';
import { plans } from 'view/global/defaults';
import './billing.less';
import { useSelector } from 'react-redux';
import authSelectors from 'modules/auth/authSelectors';
import { AppState } from 'modules/state';

export const TrialBilling = () => {
  const user = useSelector((state: AppState) => authSelectors.selectCurrentUser(state));
  const { limit, type } = useSelector((reduxState: AppState) => reduxState.subscription);

  return (
    <section className="settings-section">
      <h2 data-cy="setting-section-title">{limit < 1 ? `${i18n('settings.billing.trial.expired')} - ${i18n('settings.billing.trial.start')}` : `${i18n('settings.billing.trial.trial')} - ${limit} ${i18n('settings.billing.trial.daysLeft')}`}</h2>
      <div className="trial-billing">
        <section className="subscription-pricing-table">
          <PricingTable accountId={user?.accountId || ''} type={type} plans={plans} />
        </section>
      </div>
    </section>
  );
};
