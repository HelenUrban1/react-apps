import React from 'react';
import { User } from 'types/user';
import { useSelector } from 'react-redux';
import { AppState } from 'modules/state';
import { TrialBilling } from './TrialBilling';
import { SubscribedBilling } from './SubscribedBilling';

interface billingProps {
  setKey: (key: string) => void;
  users: User[];
  permissions: {
    hasOwnership: boolean;
    hasManageAccess: boolean;
    hasBillingAccess: boolean;
  };
}

const Billing = ({ users, permissions, setKey }: billingProps) => {
  const { data: subscription, state } = useSelector((reduxState: AppState) => reduxState.subscription);

  const manageSeats = () => {
    setKey('seats');
  };
  if (!subscription) {
    return <>No Active Subscription or Trial</>;
  }
  return <>{state?.includes('trial') ? <TrialBilling /> : <SubscribedBilling users={users} manageSeats={manageSeats} permissions={permissions} />}</>;
};

export default Billing;
