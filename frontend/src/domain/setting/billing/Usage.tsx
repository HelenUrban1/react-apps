import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import { AppState } from 'modules/state';
import { i18n } from 'i18n';
import { Progress, Button } from 'antd';
import { getComponentMetric } from 'utils/DataCalculation';
import { UsageStat } from './types';

interface Props {
  manageSeats: () => void;
  hasManageAccess: boolean;
}

const Usage = ({ manageSeats, hasManageAccess }: Props) => {
  const { data: subscription, components } = useSelector((reduxState: AppState) => reduxState.subscription);

  const [stats, setStats] = useState<UsageStat[]>([]);
  useEffect(() => {
    setStats([]);
    const usedKeys: string[] = [];
    const currentStats = [];
    components
      .filter((c) => !c.unit_name.includes('internal') && !c.component_handle?.includes('_addon')) // Filter out internal component
      .forEach((comp) => {
        const metric = getComponentMetric(comp.unit_name);
        const used = subscription ? subscription.paidUserSeatsUsed || 0 : 0;
        if (comp.unit_name && !usedKeys.includes(comp.unit_name)) {
          usedKeys.push(comp.unit_name);
          currentStats.push({ title: i18n(`settings.metrics[${metric}]`), max: comp.allocated_quantity, current: used, percent: (used / comp.allocated_quantity) * 100 });
        }
      });
    currentStats.push({ title: i18n(`settings.metrics.drawing`), max: -1, current: subscription?.drawingsUsed || 0, percent: 5 });
    setStats(currentStats);
  }, [components, subscription?.drawingsUsed, subscription?.paidUserSeatsUsed, components[0].allocated_quantity]);

  return (
    <section id="usage" data-cy="usage" className="billingSection">
      <h3 data-cy="usage-title">{i18n('settings.billing.usage.title')}</h3>
      {stats.map((stat) => {
        return (
          <div key={stat.title} className="usageStat" data-cy="usageStat">
            <span className="seat-title" data-cy="seat-title">
              {stat.title}
            </span>
            <Progress strokeColor="#1682FF" percent={stat.percent} showInfo={false} />
            <span data-cy="seats-used" className="usage-current">{`${stat.current} / ${stat.max >= 0 ? stat.max : i18n('settings.metrics.unlimited')}`}</span>
          </div>
        );
      })}
      {hasManageAccess && (
        <Button data-cy="manage-seats" className="btn-secondary" onClick={manageSeats} size="large">
          {i18n('settings.seats.manage')}
        </Button>
      )}
    </section>
  );
};

export default Usage;
