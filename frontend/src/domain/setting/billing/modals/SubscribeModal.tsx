import React, { useState } from 'react';
import { DynamicModal } from 'styleguide/Modals/DynamicModal';
import { ModalState } from 'types/modal';
import { i18n } from 'i18n';

interface subscribeProps {
  tier: string;
}

const titles = [`1. ${i18n('settings.billing.subscribe.stepOne.title')}`, '2. ', '3. '];

export const SubscribeModal = ({ tier }: subscribeProps) => {
  const [subscriptionTier, setSubscriptionTier] = useState<string>(tier);
  const [modalState, setModalState] = useState<ModalState>({
    title: titles[0],
    content: [],
    visibility: false,
    footer: undefined,
  });

  const updateState = (newState: ModalState) => {
    setModalState(newState);
  };

  // opens the subscription modal from the Standard section's subscribe button
  const handleOpen = () => {
    updateState({
      title: `1. ${i18n('settings.billing.subscribe.stepOne.title')}`,
      content: [`Chose ${subscriptionTier} Plan`, 'Content'],
      visibility: true,
      footer: undefined,
    });
  };

  return <DynamicModal modalState={modalState} updateState={updateState} handleOpen={handleOpen} buttonText={i18n('common.subscribe')} primary extraClass={tier} />;
};
