import React, { useState } from 'react';
import { DynamicModal } from 'styleguide/Modals/DynamicModal';
import { i18n } from 'i18n';
import { ModalState } from 'types/modal';
import { PricingTable } from 'styleguide/PricingTable/PricingTable';
import { plans } from 'view/global/defaults';
import { useSelector } from 'react-redux';
import authSelectors from 'modules/auth/authSelectors';
import { AppState } from 'modules/state';

const titles = [`1. ${i18n('settings.billing.subscribe.stepOne.title')}`, '2. ', '3. '];

export const UpgradeModal = () => {
  const user = useSelector((state: AppState) => authSelectors.selectCurrentUser(state));
  const { type } = useSelector((reduxState: AppState) => reduxState.subscription);

  const [modalState, setModalState] = useState<ModalState>({
    title: titles[0],
    content: [],
    visibility: false,
    footer: undefined,
  });

  const updateState = (newState: ModalState) => {
    setModalState(newState);
  };

  const contents = [<PricingTable accountId={user?.accountId || ''} type={type} plans={plans} />];

  // opens the subscription modal from the Standard section's subscribe button
  const handleOpen = () => {
    updateState({
      title: titles[0],
      content: contents,
      visibility: true,
      footer: undefined,
    });
  };

  return <DynamicModal handleOpen={handleOpen} modalState={modalState} updateState={updateState} buttonText={i18n('settings.billing.buttons.upgrade')} extraClass="upgrade-modal-btn" primary />;
};
