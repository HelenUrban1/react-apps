import React, { useEffect, useState, useRef } from 'react';
import log from 'modules/shared/logger';
import moment from 'moment';
import { i18n } from 'i18n';
import Analytics from 'modules/shared/analytics/analytics';
import { SubscriptionTier, SubscriptionPayment, SubscriptionTotal, SubscriptionDue, SubscriptionCard, SubscriptionNew, SubscriptionDeferred } from 'styleguide';
import { BillingPeriodEnum, ComponentInfo, ComponentInput, Subscription } from 'domain/setting/subscriptionTypes';
import { plans, features, privacyPolicyUrl, legalTermsUrl } from 'view/global/defaults';
import { CheckOutlined } from '@ant-design/icons';
import { Button, Modal, Checkbox, Col, Row } from 'antd';
import { calculateComponentTotal, calculateDue, convertCentsToUSD, createComponentMap } from 'utils/DataCalculation';
import CreditCardForm from 'styleguide/CreditCardForm/CreditCardForm';
import { useSelector } from 'react-redux';
import StepComponent from 'styleguide/Subscription/StepComponent/StepComponent';
import UnitComponent from 'styleguide/Subscription/UnitComponent/UnitComponent';
import { AppState } from 'modules/state';
import { getComponentPrice } from 'utils/SubscriptionUtils';
import { parseValidJSONString } from 'utils/textOperations';
import { ContactInfo } from 'styleguide/PricingTable/ContactInfo';
import '../billing.less';
import ToggleComponent from 'styleguide/Subscription/ToggleComponent/ToggleComponent';

interface modalProps {
  visibility: boolean;
  newSubscription?: boolean;
  token?: string;
  closeChangeModal: () => void;
  handleSubscriptionChange: ({
    //
    productHandle,
    period,
    price,
    handle,
    componentInputs,
    pendingComponents,
  }: {
    productHandle?: string;
    period?: BillingPeriodEnum;
    price?: number;
    handle?: string;
    componentInputs?: ComponentInput[];
    pendingComponents?: ComponentInfo;
  }) => void;
  saveCreditCard: (token: string) => void;
  removeCreditCard?: () => void;
  closeCardModal: () => void;
  startPeriod?: BillingPeriodEnum | undefined;
  startQty?: number | undefined;
}
// show extra options for tiers when testing in cypress
const cypressTest = window.top as any;
const testOptions = [
  {
    label: 'Standard',
    value: '5233891',
    name: 'Standard',
  },
  {
    label: 'Advanced',
    value: '1234',
    name: 'Advanced',
  },
];

export const ChangeSubscriptionModal = ({
  //
  visibility,
  token,
  newSubscription = false,
  closeChangeModal,
  saveCreditCard,
  removeCreditCard,
  closeCardModal,
  handleSubscriptionChange,
  startQty,
  startPeriod,
}: modalProps) => {
  const { data: subscription, state, auth, catalog, components } = useSelector((reduxState: AppState) => reduxState.subscription);
  const productFamily = useRef<string>(subscription?.providerFamilyHandle || 'inspectionxpert-cloud');
  // replace cypressTest with true to manually test with extra tier options
  const products = catalog[productFamily.current]?.products || {};
  const tierOptions = cypressTest.Cypress
    ? testOptions
    : Object.values(products)
        .filter((plan) => !plan.archived_at)
        .map((plan) => {
          return {
            label: plan.name,
            value: plan.handle || plan.name.toLowerCase().replaceAll(' ', '_'),
            name: plan.name,
          };
        });

  const [subTier, setTier] = useState<string>(subscription?.providerProductHandle || 'ix_standard'); // TODO: not wired correctly right now due to component changes

  const [component, setComponent] = useState<ComponentInfo>({});
  const [pendingComponent, setPendingComponent] = useState<ComponentInfo>({});
  const originalComponent = useRef<ComponentInfo>({});
  const componentsChangeData = useRef<{ [key: string]: ComponentInput }>({});
  const nextAssessmentAt = useRef(moment(subscription?.nextAssessmentAt));

  const [warn, setWarn] = useState<{ [key: string]: boolean }>({});
  const [subPay, setPay] = useState<BillingPeriodEnum>(subscription?.pendingBillingPeriod ? subscription.pendingBillingPeriod : subscription?.billingPeriod || 'Annually');
  const [due, setDue] = useState<number>(0);
  const [periodCost, setPeriodCost] = useState<number>(subscription?.currentBillingAmountInCents || 0);
  const [label, setLabel] = useState<{
    [x: string]: string;
  }>({
    payment: convertCentsToUSD(subscription?.currentBillingAmountInCents || 0),
    due: convertCentsToUSD(0),
    remaining: '',
    ending: moment(subscription?.nextAssessmentAt).format('MMM D, YYYY'),
  });
  const [deferred, setDeferred] = useState<
    | {
        [x: string]: string;
      }
    | undefined
  >();
  const [termsAndConditions, setTermsAndConditions] = useState<boolean>(false);
  const [loading, setLoading] = useState(false);
  // Chargify can't prorate a change in billing cycle, so we've blocked this feature for the moment

  // We currently aren't allowing monthly payments, so we've disabled it here
  // Setting to newSubscription instead of 'false' will re-enable the monthly payment option
  const allowBillingPeriodChange = false;

  useEffect(() => {
    if (subPay !== 'Annually') {
      if (warn.remittance && token) {
        setWarn({ ...warn, remittance: false });
      } else if (!warn.remittance && !token) {
        setWarn({ ...warn, remittance: true });
      }
    }
  }, [token, subPay]);

  /**
   *
   * @param newComp Current components and upgrades
   * @param pendingComp Deferred component changes, to take effect at the end of the annual contract
   * @param period Billing period (deferred if billing period changes are blocked)
   *
   * Calculate Cost for the current billing period, prorated due on upgrade, and cost for any pending changes
   */
  const setCost = ({ original = originalComponent.current, newComp = component, pendingComp = pendingComponent, period = subPay }: { original?: ComponentInfo; newComp?: ComponentInfo; pendingComp?: ComponentInfo; period?: BillingPeriodEnum }) => {
    const calcPeriod = subscription?.pendingBillingPeriod || period;
    const cost = calculateComponentTotal(newComp);
    setPeriodCost(cost);
    if (!allowBillingPeriodChange && (subscription?.billingPeriod !== calcPeriod || Object.keys(pendingComp).length > 0)) {
      // Show label for pending subscription changes
      const pending = Object.keys(pendingComp).length > 0 ? calculateComponentTotal({ ...newComp, ...pendingComp }) : cost;
      setDeferred({
        period: calcPeriod,
        payment: convertCentsToUSD(pending),
        ending: moment(subscription?.renewalDate).format('MMM Do, YYYY'),
      });
    } else if (deferred) {
      // Clear pending label when there are no pending changes
      setDeferred(undefined);
    }

    if (newSubscription || (subscription && subscription.status === 'trialing' && subscription.paymentCollectionMethod === 'automatic')) {
      // Charge full amount for new subscriptions
      // or for automatic subscriptions in limbo waiting for Chargify to recognize them
      setLabel({
        payment: convertCentsToUSD(cost),
        due: convertCentsToUSD(cost),
        remaining: '',
        ending: nextAssessmentAt.current.format('MMM D, YYYY'),
      });
      setDue(newSubscription ? cost : 0);
    } else if (!subscription || !nextAssessmentAt) {
      log.error('Cannot calculate proration when missing subscription fields');
    } else {
      // Calculate Proration
      const newPeriod = allowBillingPeriodChange ? period : subscription.billingPeriod || 'Annually'; // If mid-subscription billing is allowed, use changed period, otherwise use subscription period
      let started = newPeriod === 'Monthly' ? moment(subscription.currentPeriodStartedAt) : moment(subscription.activatedAt);
      if (!started.isValid()) {
        started = moment();
      }
      const { newLabel, dueAmount } = calculateDue({ originalComponents: original, newComponents: newComp, label, started, nextAssessmentAt: nextAssessmentAt.current });
      setDue(dueAmount);
      setLabel(newLabel);
    }
  };

  const updateComponentPrice = (period: BillingPeriodEnum) => {
    // Update Component Price
    const updatedComponents = { ...component, ...pendingComponent };
    Object.values(component).forEach((comp) => {
      const pricehandle = Object.keys(comp.prices).find((handle) => handle.includes(period.toLowerCase())) || 'none';
      const prices = comp.prices[pricehandle];
      const { cost, id } = getComponentPrice(prices, comp.purchased);
      if (allowBillingPeriodChange) {
        componentsChangeData.current[comp.handle] = {
          component_id: componentsChangeData.current[comp.handle]?.component_id || comp.id,
          handle: comp.handle,
          name: componentsChangeData.current[comp.handle]?.name || comp.name,
          product_family_key: productFamily.current,
          quantity: componentsChangeData.current[comp.handle]?.quantity || comp.purchased,
          upgrade_charge: 'prorated',
          type: componentsChangeData.current[comp.handle]?.type || comp.type,
          memo: 'User Triggered Component Change via Application',
          price_point: pricehandle,
          unit_price: cost ? (cost / 100).toString() : '',
        };
        const orgComp = originalComponent.current[comp.handle];
        const orgCompPricehandle = Object.keys(orgComp.prices).find((handle) => handle.includes(period.toLowerCase())) || 'none';
        const orgCompPrices = orgComp.prices[orgCompPricehandle];
        const { cost: orgCompCost, id: orgCompId } = getComponentPrice(orgCompPrices, orgComp.purchased);
        originalComponent.current[comp.handle] = {
          ...orgComp,
          price: orgCompCost || orgComp.price,
          priceId: orgCompId || orgComp.priceId,
        };
      }
      updatedComponents[comp.handle] = {
        ...comp,
        price: cost || comp.price,
        priceId: id || comp.priceId,
      };
    });
    if (allowBillingPeriodChange) {
      // Allow subscription billing to be changed in Chargify
      let started = period === 'Monthly' ? moment(subscription?.currentPeriodStartedAt) : moment(subscription?.activatedAt);
      if (subscription?.billingPeriod === 'Annually' && period === 'Monthly') {
        started = moment(nextAssessmentAt.current);
      }
      if (!started.isValid()) {
        started = moment();
      }
      nextAssessmentAt.current = period === 'Monthly' ? started.add(1, 'months') : started.add(1, 'years');
      setComponent(updatedComponents);
      setCost({ newComp: updatedComponents, pendingComp: pendingComponent, original: originalComponent.current, period });
    } else {
      // Defer Billing changes to the end of the annual subscription
      const pending = { ...updatedComponents };
      Object.keys(updatedComponents).forEach((handle) => {
        if (component[handle].price === pending[handle].price) {
          // Remove pending components that were changed back
          delete pending[handle];
        }
      });
      setPendingComponent(pending);
      setCost({ newComp: component, pendingComp: pending, period });
    }

    if (period === 'Monthly' && !token && !subscription?.cardId) {
      // don't allow remittance payments for monthly subscriptions
      setWarn({ ...warn, remittance: true });
    } else if (warn.remittance) {
      setWarn({ ...warn, remittance: false });
    }
  };

  // Opens the subscription modal from the Standard section's subscribe button
  // Acts as a reset
  const handleOpen = () => {
    let pay = subscription?.billingPeriod || 'Annually';
    const pendingPay = subscription?.pendingBillingPeriod;
    const providerProductHandle = subscription?.providerProductHandle || 'ix_standard';
    setTier(providerProductHandle);
    setWarn({});
    if (newSubscription && startPeriod) {
      pay = startPeriod;
      setPay(pay);
      updateComponentPrice(pay);
    } else {
      setPay(pendingPay || pay);
      updateComponentPrice(pendingPay || pay);
    }

    const map = createComponentMap(pay, catalog, components, subscription?.providerFamilyHandle, startQty);
    const analytics: any = {};
    Object.values(map).forEach((comp) => {
      const used: number = subscription ? (subscription[`${comp.metric}sUsed` as keyof Subscription] as number) || 0 : 0;
      map[comp.handle] = { ...comp, used };
      analytics[`subscription.${comp.metric}`] = comp.purchased;
    });
    setComponent(map);
    originalComponent.current = map;
    let pendingMap: ComponentInfo = {};
    if (subscription?.pendingComponents && subscription.pendingComponents !== '[]') {
      const pending = parseValidJSONString(subscription.pendingComponents) || [];
      pendingMap = createComponentMap(pendingPay || pay, catalog, pending, subscription?.providerFamilyHandle);
      Object.values(pendingMap).forEach((comp) => {
        const used: number = subscription ? (subscription[`${comp.metric}sUsed` as keyof Subscription] as number) || 0 : 0;
        pendingMap[comp.handle] = { ...comp, used };
        if (map[comp.handle] && comp.purchased < map[comp.handle].purchased) {
          setWarn({ ...warn, [comp.handle]: true });
        }
      });
      setPendingComponent(pendingMap);
    }

    let next = moment(subscription?.nextAssessmentAt);
    if (state === 'pending') {
      const period = pendingPay || pay;
      const interval = period === 'Annually' ? 'years' : 'months';
      next = moment(subscription?.termsAcceptedOn).add(1, interval);
    }

    nextAssessmentAt.current = next;

    setDue(0);
    const cost = calculateComponentTotal(map);
    setLabel({
      payment: convertCentsToUSD(cost),
      due: convertCentsToUSD(0),
      remaining: '',
      ending: next.format('MMM D, YYYY'),
    });
    Analytics.track({
      event: Analytics.events.subscriptionChangeOpen,
      subscription,
      properties: {
        ...analytics,
        'subscription.period': subscription?.billingPeriod,
        'subscription.tier': subscription?.providerProductHandle,
      },
    });
  };

  // Toggle Modal Visibility
  useEffect(() => {
    if (visibility === true) {
      handleOpen();
    }
  }, [visibility]);

  // Currently only one tier available. Maps to the Chargify Product
  const changeTier = (event: { target: { value: string; name: string } }) => {
    Analytics.track({
      event: Analytics.events.subscriptionChangeTier,
      subscription,
      properties: {
        'subscription.period.old': subscription?.billingPeriod,
        'subscription.period.new': subPay,
        'subscription.tier.old': subscription?.providerProductHandle,
        'subscription.tier.new': subTier,
      },
    });
    const newTier = event.target.value;
    const costPerPeriod = 0;
    const paid = 0;
    if (newSubscription) {
      const increment = subPay === 'Monthly' ? 'months' : 'years';
      const started = moment();
      const { newLabel, dueAmount } = calculateDue({ originalComponents: originalComponent.current, newComponents: component, label, started, nextAssessmentAt: moment().add(1, increment) });

      Analytics.track({
        event: Analytics.events.subscriptionChangeCalculateDue,
        subscription,
        properties: {
          'subscription.period.old': subscription?.billingPeriod,
          'subscription.period.new': subPay,
          'subscription.tier.old': subscription?.providerProductHandle,
          'subscription.tier.new': newTier,
          'subscription.paid': paid,
          'subscription.due': dueAmount,
          'subscription.cost': costPerPeriod,
        },
      });
      setPeriodCost(costPerPeriod);
      setLabel(newLabel);
      setTier(newTier);
    } else if (!subscription || !subscription.currentPeriodStartedAt || !subscription.currentPeriodEndsAt) {
      log.error('Cannot calculate due when missing subscription fields');
    } else {
      const started = subPay === 'Monthly' ? moment(subscription.currentPeriodStartedAt) : moment(subscription.activatedAt);
      const { newLabel, dueAmount } = calculateDue({ originalComponents: originalComponent.current, newComponents: component, label, started, nextAssessmentAt: nextAssessmentAt.current });

      Analytics.track({
        event: Analytics.events.subscriptionChangeCalculateDue,
        subscription,
        properties: {
          'subscription.period.old': subscription?.billingPeriod,
          'subscription.period.new': subPay,
          'subscription.tier.old': subscription?.providerProductHandle,
          'subscription.tier.new': newTier,
          'subscription.paid': paid,
          'subscription.due': dueAmount,
          'subscription.cost': costPerPeriod,
        },
      });
      setDue(dueAmount);
      setPeriodCost(costPerPeriod);
      setLabel(newLabel);
      setTier(newTier);
    }
  };

  // Maps to the Chargify Component. Controls the price based on number of components purchased
  const changeComponent = (handle: string, compType: string, value: string) => {
    const newComponent = { ...component[handle], handle };
    const newPending = { ...pendingComponent };
    const interval = allowBillingPeriodChange ? subPay : subscription?.billingPeriod || 'Annually';

    if (!newSubscription && deferred?.period && deferred.period !== interval) {
      // Deferred billing period is active
      // Calculate deferred component price
      const pending = pendingComponent[handle] ? { ...pendingComponent[handle], handle } : { ...component[handle], handle };
      const pendingPrice = Object.keys(pending.prices).find((price) => price.includes(deferred.period.toLowerCase())) || 'none';
      if (compType === 'stairstep') {
        const step = pending.prices[pendingPrice].find((price) => price.id.toString() === value);
        if (!step || !step.ending_quantity) {
          // Contact sales for amounts past limit
          // TODO: sales contact
          return;
        }
        pending.purchased = step.ending_quantity;
        pending.priceId = value;
        pending.price = parseFloat(step.unit_price) * 100;
      } else {
        pending.purchased = parseFloat(value);
        pending.price = parseFloat(pending.prices[pendingPrice][0].unit_price) * 100;
      }
      newPending[handle] = pending;
      setPendingComponent(newPending);
    }

    const pricehandle = Object.keys(newComponent.prices).find((price) => price.includes(interval.toLowerCase())) || 'none';
    if (compType === 'stairstep') {
      const step = newComponent.prices[pricehandle].find((price) => price.id.toString() === value);
      if (!step || !step.ending_quantity) {
        // Contact sales for amounts past limit
        // TODO: sales contact
        return;
      }
      newComponent.purchased = step.ending_quantity;
      newComponent.priceId = value;
      newComponent.price = parseFloat(step.unit_price) * 100;
    }
    if (compType === 'per_unit') {
      newComponent.purchased = parseFloat(value);
    }
    if (compType === 'on/off') {
      newComponent.purchased = parseFloat(value);
    }

    if (!newSubscription && originalComponent?.current[handle] && newComponent.purchased < originalComponent.current[handle].purchased) {
      // Is a downgrade
      if (!deferred?.period || deferred?.period === interval) {
        // Deferred billing period is not active, update pending with current billing price
        newPending[handle] = newComponent;
        setPendingComponent(newPending);
      }
      setWarn({ ...warn, [handle]: true });
      setCost({ newComp: { ...component, [handle]: originalComponent.current[handle] }, pendingComp: newPending });
    } else {
      // Upgrade immediately
      const prices = newComponent.prices[pricehandle];
      const { cost } = getComponentPrice(prices, newComponent.purchased);
      componentsChangeData.current[newComponent.handle] = {
        component_id: newComponent.id,
        handle: newComponent.handle,
        name: newComponent.name,
        product_family_key: productFamily.current,
        quantity: newComponent.purchased,
        upgrade_charge: 'prorated',
        price_point: pricehandle,
        type: newComponent.type,
        unit_price: cost ? (cost / 100).toString() : '',
        memo: 'User Triggered Component Change via Application',
      };
      const updatedComponents = { ...component, [handle]: { ...newComponent, handle } };
      setComponent(updatedComponents);
      if (warn[handle]) {
        const newWarn = { ...warn };
        delete newWarn[handle];
        setWarn(newWarn);
      }
      if (!deferred?.period || deferred.period === interval) {
        // Deferred billing period is not active, remove pending component
        delete newPending[handle];
        setPendingComponent(newPending);
      }
      setCost({ newComp: updatedComponents, pendingComp: newPending });
      if (handle.includes('net_inspect') && newComponent.purchased > 0) {
        Analytics.track({
          event: Analytics.events.subscriptionNetInspectPurchase,
          subscription,
        });
      }
    }
  };

  // Controls the billing period custom field and price points in Chargify
  const changePaymentPeriod = (event: { target: { value: 'Monthly' | 'Annually'; name: string } }) => {
    Analytics.track({
      event: Analytics.events.subscriptionChangePaymentPeriod,
      subscription,
      properties: {
        'subscription.period.old': subscription?.billingPeriod,
        'subscription.period.new': subPay,
        'subscription.tier.old': subscription?.providerProductHandle,
        'subscription.tier.new': subTier,
      },
    });
    const period = event.target.value;
    setPay(period);

    updateComponentPrice(period);
  };

  const getTitle = () => {
    switch (subTier) {
      case 'Entry':
        return i18n('entities.subscription.tier.EntryFeatures');
      case '5233891':
        return i18n('entities.subscription.tier.ix_standardFeatures');
      case 'ix_standard':
        return i18n('entities.subscription.tier.ix_standardFeatures');
      case 'Advanced':
        return i18n('entities.subscription.tier.advancedFeatures');
      default:
        return i18n('common.unknown');
    }
  };

  // Features Table for a Tier
  const showTable = () => {
    Analytics.track({
      event: Analytics.events.subscriptionChangeTierHelp,
      subscription,
      properties: {
        'subscription.period.old': subscription?.billingPeriod,
        'subscription.period.new': subPay,
        'subscription.tier.old': subscription?.providerProductHandle,
        'subscription.tier.new': subTier,
      },
    });
    Modal.info({
      title: '',
      icon: <></>,
      className: 'tier-info-modal',
      content: (
        <section className="feature-list">
          <h3>{getTitle()}</h3>
          {features.map((feature: string) => {
            return (
              <section key={`feature-row-${feature}`} className="pricing-table-feature-row">
                {plans.map((plan: any) => {
                  return (
                    <section key={`feature-check-${feature}`} className="plan-feature-check">
                      {plan.features.includes(feature) ? <CheckOutlined /> : ''}
                    </section>
                  );
                })}
                <section key={`feature-info-${feature}`} className="pricing-table-feature-cell">
                  {feature}
                </section>
              </section>
            );
          })}
        </section>
      ),
    });
  };

  const handleSaveCard = (key: string) => {
    saveCreditCard(key);
    setLoading(false);
  };

  // Change Default Payment on Chargify
  const changeCreditCard = () => {
    Analytics.track({
      event: Analytics.events.subscriptionChangeCardStart,
      subscription,
      properties: {
        'subscription.period.old': subscription?.billingPeriod,
        'subscription.period.new': subPay,
        'subscription.tier.old': subscription?.providerProductHandle,
        'subscription.tier.new': subTier,
      },
    });
    Modal.info({
      title: <span className="info-change-card-title">{i18n(`settings.billing.payment.addCardTitle`)}</span>,
      icon: <></>,
      width: 564,
      className: 'cardModal',
      content: (
        <>
          <CreditCardForm subscription={subscription} formSignifier={subscription?.cardType} chargifyAuth={auth} loading={loading} setLoading={setLoading} handleClose={closeCardModal} onSubmit={handleSaveCard} onError={closeCardModal} />
          <div className="info-footer contact-us Horizontal">
            <ContactInfo layout="Horizontal" />
          </div>
          <div className="footer-border" />
        </>
      ),
    });
  };

  // Close Credit Card Modal
  const handleClose = () => {
    Analytics.track({
      event: Analytics.events.subscriptionChangeCancel,
      subscription,
      properties: {
        'subscription.period.old': subscription?.billingPeriod,
        'subscription.period.new': subPay,
        'subscription.tier.old': subscription?.providerProductHandle,
        'subscription.tier.new': subTier,
        'subscription.due': due,
      },
    });
    closeChangeModal();
  };
  // Trigger Change to Subscription
  const handleSave = () => {
    Analytics.track({
      event: Analytics.events.subscriptionChangeConfirm,
      subscription,
      properties: {
        'subscription.period.old': subscription?.billingPeriod,
        'subscription.period.new': subPay,
        'subscription.tier.old': subscription?.providerProductHandle,
        'subscription.tier.new': subTier,
        'subscription.due': due,
      },
    });
    handleSubscriptionChange({
      productHandle: subTier,
      period: subPay,
      price: periodCost,
      handle: Object.keys(component)[0],
      componentInputs: Object.values(componentsChangeData.current),
      pendingComponents: pendingComponent,
    });
    closeChangeModal();
  };

  const modalButtons = () => (
    <>
      <Button size="large" onClick={handleClose} data-cy="subscription-modal-cancel">
        {i18n('common.cancel')}
      </Button>
      <Button disabled={!termsAndConditions || warn.remittance} type="primary" size="large" className="primary" onClick={handleSave} data-cy="subscription-modal-confirm">
        {due > 0 ? i18n('entities.subscription.change.pay') : i18n('entities.subscription.change.confirm')}
      </Button>
    </>
  );

  return (
    <Modal
      destroyOnClose
      visible={visibility}
      title={<span data-cy="subscription-modal-title">{newSubscription ? i18n('entities.subscription.trial.confirm') : i18n('entities.subscription.change.title')}</span>}
      onCancel={handleClose}
      className="general-modal change-modal-btn"
      okText={due > 0 ? i18n('entities.subscription.change.pay') : i18n('entities.subscription.change.confirm')}
      onOk={handleSave}
      cancelText={i18n('common.cancel')}
      centered
      footer={modalButtons()}
    >
      <div data-cy="subscription-modal-container" className="modal-body">
        <div className="change-modal">
          <SubscriptionTier tierOptions={tierOptions} tier={subTier.toString()} change={changeTier} help={showTable} />
          {Object.values(component)
            .filter((c) => !c.handle.includes('_addon'))
            .map((comp) => {
              // Create Component Inputs based on the non-zero components in the Chargify subscription
              let item = comp;
              if (pendingComponent[comp.handle]) {
                // Default to pending component values
                item = pendingComponent[comp.handle];
              }
              if (item.type === 'stairstep') {
                const pricehandle = Object.keys(item.prices).find((handle) => handle.includes(subPay.toLowerCase())) || 'none';
                return <StepComponent key={item.id} handle={item.handle} name={item.metric} bought={item.priceId} warn={warn[item.handle] || false} prices={item.prices[pricehandle]} change={changeComponent} />;
              }
              if (item.type === 'per_unit') {
                return <UnitComponent key={item.id} handle={item.handle} name={item.metric} bought={item.purchased} warn={warn[item.handle] || false} change={changeComponent} />;
              }
              if (item.type === 'on_off') {
                return <ToggleComponent key={item.id} handle={item.handle} name={item.metric} bought={item.purchased > 0} warn={warn[item.handle] || false} change={changeComponent} />;
              }
              return null;
            })}
          <h4>
            {i18n('entities.subscription.addOn.title')}
            <br />
            <small>{i18n('entities.subscription.addOn.help')}</small>
          </h4>
          {Object.values(component)
            .filter((c) => c.handle.includes('_addon'))
            .map((comp) => {
              // Create Component Inputs based on the non-zero components in the Chargify subscription
              let item = comp;
              if (pendingComponent[comp.handle]) {
                // Default to pending component values
                item = pendingComponent[comp.handle];
              }
              if (item.type === 'stairstep') {
                const pricehandle = Object.keys(item.prices).find((handle) => handle.includes(subPay.toLowerCase())) || 'none';
                return <StepComponent key={item.id} handle={item.handle} name={item.metric} bought={item.priceId} warn={warn[item.handle] || false} prices={item.prices[pricehandle]} change={changeComponent} />;
              }
              if (item.type === 'per_unit') {
                return <UnitComponent key={item.id} handle={item.handle} name={item.metric} bought={item.purchased} warn={warn[item.handle] || false} change={changeComponent} />;
              }
              if (item.type === 'on_off') {
                return <ToggleComponent key={item.id} handle={item.handle} name={item.metric} bought={item.purchased > 0} warn={warn[item.handle] || false} change={changeComponent} />;
              }
              return null;
            })}
          {allowBillingPeriodChange && <SubscriptionPayment due={subPay} change={changePaymentPeriod} />}
          {newSubscription && <SubscriptionNew payment={label.payment} token={token || subscription?.cardId?.toString()} changeCreditCard={changeCreditCard} removeCreditCard={removeCreditCard} />}
          <SubscriptionTotal billingPeriod={newSubscription ? subPay : subscription?.billingPeriod} payment={label.payment} ending={label.ending} />

          {deferred && !newSubscription && <SubscriptionDeferred billingPeriod={deferred.period} payment={deferred.payment} ending={deferred.ending} />}
          {!newSubscription && due > 0 && (
            <>
              <SubscriptionDue due={label.due} remaining={label.remaining} />
              {subscription?.maskedCardNumber && <SubscriptionCard cardType={subscription?.cardType} maskedCardNumber={subscription?.maskedCardNumber} changeCreditCard={changeCreditCard} />}
            </>
          )}
          {warn.remittance && <p className="warnMessage small">{i18n('entities.subscription.billing.warning')}</p>}
          <br />
          <Row>
            <Col span={24}>
              <Checkbox
                data-cy="subscription-modal-termsAndConditions"
                id="termsAndConditionsSubscription"
                checked={termsAndConditions}
                onChange={(e) => {
                  setTermsAndConditions(e.target.checked);
                }}
              >
                <span className="subscription-span-small">
                  {i18n('legal.checkboxPreamble')}
                  <a target="_blank" href={legalTermsUrl} rel="noreferrer">
                    {i18n('legal.legalTerms')}
                  </a>
                  {i18n('legal.and')}
                  <a target="_blank" href={privacyPolicyUrl} rel="noreferrer">
                    {i18n('legal.privacyPolicy')}
                  </a>
                  {i18n('legal.andAgree')}
                </span>
              </Checkbox>
            </Col>
          </Row>
          <br />
          <Row>
            <Col span={20} offset={2}>
              <span className="subscription-span-small">
                {i18n('legal.bodyPreamble')}
                <a target="_blank" href={i18n('legal.ordersEmailLink')} rel="noreferrer">
                  {i18n('legal.ordersEmail')}
                </a>
                {i18n('legal.orContact')}
                <a href={i18n('legal.contactPhoneLink')}>{i18n('legal.contactPhone')}</a>
                {i18n('legal.extension')}
              </span>
            </Col>
          </Row>
          <div className="subscription-modal-branding">{i18n('legal.branding')}</div>
        </div>
      </div>
    </Modal>
  );
};

ChangeSubscriptionModal.defaultProps = {
  newSubscription: false,
  token: null,
  removeCreditCard: null,
};
