import React, { useEffect, useState } from 'react';
import { DynamicModal } from 'styleguide/Modals/DynamicModal';
import { i18n } from 'i18n';
import { ModalState } from 'types/modal';
import CreditCardForm from 'styleguide/CreditCardForm/CreditCardForm';
import { Button } from 'antd';
import { ContactInfo } from 'styleguide/PricingTable/ContactInfo';
import { Subscription } from 'domain/setting/subscriptionTypes';

interface Props {
  chargifyAuth: {
    publicKey: string;
    securityToken: string;
    siteUrl: string;
  };
  type: string;
  updateBillingCard: (token: string) => void;
  subscription: Subscription | null;
  error?: any | null;
}

export const ChangeCardModal = ({ subscription, chargifyAuth, type, updateBillingCard, error }: Props) => {
  const defaultState = {
    title: i18n(`settings.billing.payment.${type}`),
    content: [],
    visibility: false,
    footer: undefined,
  };

  const [modalState, setModalState] = useState<ModalState>(defaultState);
  const [loading, setLoading] = useState(false);

  const updateState = (newState: ModalState) => {
    setModalState(newState);
  };

  const close = () => {
    setLoading(false);
    setModalState(defaultState);
  };

  useEffect(() => {
    if (error) {
      updateState({
        title: i18n(`errors.chargify.changeCard.message`),
        content: [<p>{i18n(`errors.chargify.changeCard.description`)}</p>],
        visibility: true,
        footer: (
          <Button id="card-cancel" size="large" onClick={close}>
            {i18n('common.close')}
          </Button>
        ),
      });
    }
  }, [error]);

  const updateAndClose = async (token: string) => {
    await updateBillingCard(token);
    setModalState(defaultState);
    setLoading(false);
  };

  // opens the subscription modal from the Standard section's subscribe button
  const handleOpen = () => {
    updateState({
      title: i18n(`settings.billing.payment.${type}Title`),
      content: [<CreditCardForm subscription={subscription} formSignifier={type} chargifyAuth={chargifyAuth} loading={loading} setLoading={setLoading} handleClose={close} onSubmit={updateAndClose} onError={close} />],
      visibility: true,
      footer: <ContactInfo layout="Horizontal" />,
    });
  };
  return <DynamicModal handleOpen={handleOpen} modalState={modalState} updateState={updateState} buttonText={i18n(`settings.billing.payment.${type}`)} primary={type === 'addCard'} extraClass={type} />;
};
