import React from 'react';
import { QuestionCircleOutlined } from '@ant-design/icons';
import { Tooltip } from 'antd';
import { i18n } from 'i18n';
import { SimpleModal } from 'styleguide/Modals/SimpleModal';
import { ContactInfo } from 'styleguide/PricingTable/ContactInfo';
import { legalTermsUrl } from 'view/global/defaults';

interface cancelProps {
  expDate: string;
  handleCancellation: () => void;
}

export const CancelSubscriptionModal = ({ expDate, handleCancellation }: cancelProps) => {
  // text describing when the user's access will end
  const access = (
    <div className="info-text" data-cy="access-message">
      <span>{`${i18n('settings.billing.info.cancel.access')} `}</span>
      <span className="span-bold">{expDate}</span>
    </div>
  );

  // text describing our data retention
  const data = (
    <div className="info-text" data-cy="data-retention-message">
      {`${i18n('settings.billing.info.cancel.data.a')}`}
      <a target="_blank" href={legalTermsUrl} rel="noreferrer">
          {` ${i18n('settings.billing.info.cancel.data.b')} `}
      </a>
    </div>
  );

  // text warning of data deletion
  const deletion = (
    <div className="info-text" data-cy="data-deletion-message">
      {`${i18n('settings.billing.info.cancel.deletion.a')}`}
      <span className="span-warn">{` ${i18n('settings.billing.info.cancel.deletion.b')} `}</span>
      {`${i18n('settings.billing.info.cancel.deletion.c')}`}
    </div>
  );

  // contact information
  const contact = (
    <div className="info-text" data-cy="contact-message">
      <span>{i18n('settings.billing.info.cancel.contact')}</span>
      <ContactInfo />
    </div>
  );

  const body = (
    <div className="cancel-modal-body-text" data-cy="cancel-modal-body">
      {access}
      {data}
      {deletion}
      {contact}
    </div>
  );

  return (
    <SimpleModal
      buttonText={i18n('settings.billing.buttons.cancel')}
      title={i18n('settings.billing.info.cancel.title')}
      body={body}
      okText={i18n('settings.billing.buttons.cancel')}
      cancelText={i18n('common.close')}
      onAccept={handleCancellation}
      extraClass="cancel-modal-btn"
      warning
    />
  );
};
