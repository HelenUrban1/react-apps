import React from 'react';
import { Table } from 'antd';
import currency from 'currency.js';
import moment from 'moment';
import model from 'modules/shared/billing/billingInvoiceModel';
import { i18n } from 'i18n';
import { InvoiceData } from './types';

const { fields } = model;

interface Props {
  rows: InvoiceData[];
  loading: boolean;
  doDownload: (event: any) => void;
}

const Invoices: React.FC<Props> = ({ rows, loading, doDownload }) => {
  const columns = [
    fields.created.forTable({
      title: i18n('settings.billing.invoices.date'),
      sorter: false,
      render: (value: string) => moment(value).format('MM/DD/YYYY'),
    }),
    fields.items.forTable({
      title: i18n('settings.billing.invoices.item'),
      sorter: false,
    }),
    fields.totalAmount.forTable({
      title: i18n('settings.billing.invoices.charge'),
      sorter: false,

      // Chargify API values for this field are strings, and instead of including
      // two decimal places to represent decimal currency amounts, they include one
      // (e.g., '1500.0'). The library call formats the value for proper display,
      // including placing commas where appropriate.
      render: (value: string) => `$${currency(value).format()}`,
    }),
    fields.status.forTable({
      title: i18n('settings.billing.invoices.status'),
      sorter: false,
      render: (value: string) => value.charAt(0).toUpperCase() + value.substr(1).toLowerCase(),
    }),
    fields.id.forTable({
      title: i18n('settings.billing.invoices.invoice'),
      sorter: false,
      render: (value: string) => (
        <button type="button" data-cy="invoice-download" className="link" data-invoice-id={value} onClick={doDownload}>
          PDF
        </button>
      ),
    }),
  ];
  return (
    <section className="settings-subsection">
      <h3 data-cy="invoices">{i18n('settings.billing.invoices.title')}</h3>
      <Table rowKey="uid" loading={loading} className="invoice-table" bordered dataSource={rows} columns={columns} pagination={false} />
    </section>
  );
};

export default Invoices;
