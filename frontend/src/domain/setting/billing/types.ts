export interface UsageStat {
  title: string;
  max: number | string;
  current: number;
  percent: number;
}

export interface InvoiceData {
  issue_date: string;
  product_name: string;
  status: string;
  total_amount: string;
  uid: string;
}

export interface PaymentInfo {
  contact: string;
  contactEmail: string;
  renewDate: string;
  collection: string;
  card: {
    cardId?: number;
    paymentType?: string;
    maskedCardNumber?: string;
    cardType?: string;
    expirationMonth?: number;
    expirationYear?: number;
    customerId?: string;
  } | null;
}
