import React, { useEffect, useState } from 'react';
import { i18n } from 'i18n';
import moment from 'moment';
import { InputType, Select } from 'view/global/inputs';
import { MailOutlined, PhoneOutlined } from '@ant-design/icons';
import { Modal } from 'antd';
import Spinner from 'view/shared/Spinner';
import { IxButton } from 'styleguide';
import { useSelector } from 'react-redux';
import { AppState } from 'modules/state';
import { User } from 'types/user';
import { openEmail } from 'styleguide/PricingTable/ContactInfo';
import { ChangeCardModal } from './modals/ChangeCardModal';

interface Props {
  users: User[];
  setBillingContact: (id: string) => void;
  updateBillingCard: (token: string) => void;
  removeBillingCard: () => void;
}

const Payment = ({ users, setBillingContact, updateBillingCard, removeBillingCard }: Props) => {
  const { data: subscription, auth } = useSelector((reduxState: AppState) => reduxState.subscription);
  const [billingContact, setContact] = useState<User>();
  useEffect(() => {
    if (subscription?.billingId && subscription?.billingId !== billingContact?.id) {
      const billingUser = users.find((use) => use.id === subscription.billingId);
      if (billingUser) {
        setContact(billingUser);
      }
    }
  });

  if (!subscription) {
    return <></>;
  }

  const handleChange = ({ target }: { target: { name: string; value: string } }) => {
    setBillingContact(target.value);
  };
  const billingSelect: InputType = {
    name: 'billing-contact-select',
    value: subscription.billingId,
    change: handleChange,
    group: users
      .filter((user) => user.email && user.email !== '')
      .map((user) => {
        return { name: user.id, label: user.email, value: user.id };
      }),
  };
  const cancelPopup = () => {};
  const confirmRemoveCardMessage = () => {
    Modal.confirm({
      title: i18n('settings.billing.payment.confirmTitle'),
      content: i18n('settings.billing.payment.confirmSubtitle', billingContact?.fullName, subscription.renewalDate),
      okText: i18n('common.destroy'),
      cancelText: i18n('common.cancel'),
      className: 'deleteCard',
      onOk: removeBillingCard,
      onCancel: cancelPopup,
    });
  };

  return (
    <section id="payment" data-cy="payment">
      <h3 data-cy="payment">{i18n('settings.billing.payment.title')}</h3>
      <div className="paymentSection">
        <h4 data-cy="payment-method">{i18n('settings.billing.payment.method')}</h4>
        {subscription.maskedCardNumber && subscription.paymentCollectionMethod === 'automatic' && (
          <div className="card">
            <span className="card-type card-span">
              <img src={`${process.env.PUBLIC_URL}/images/${subscription.cardType}.jpg`} alt="card type logo" />
            </span>
            <span data-cy="masked-number" className="card-span">
              {subscription.maskedCardNumber}
            </span>
            <span data-cy="expiration" className="card-span">
              {subscription.expirationMonth &&
                subscription.expirationYear &&
                `Expires ${moment()
                  .month(subscription.expirationMonth - 1)
                  .year(subscription.expirationYear)
                  .format('MM/YY')}`}
            </span>
            <span className="card-actions card-span">
              {auth && (
                <span className="card-span">
                  <ChangeCardModal subscription={subscription} type="updateCard" chargifyAuth={auth} updateBillingCard={updateBillingCard} />
                </span>
              )}
              <span className="card-span">
                <IxButton onClick={confirmRemoveCardMessage} text={i18n('settings.billing.payment.removeCard')} size="large" className="btn-secondary" dataCy="removeCard" />
              </span>
            </span>
          </div>
        )}
        {!subscription && (
          <div className="card">
            <Spinner />
          </div>
        )}
        {((subscription && !subscription.maskedCardNumber) || subscription.paymentCollectionMethod === 'remittance') && auth && (
          <div>
            <p className="card-span" data-cy="pay-by-invoice">
              {i18n(`settings.billing.payment.${subscription.paymentCollectionMethod}`)}
            </p>
            <p className="card-span" data-cy="renew-message">
              {i18n('settings.billing.payment.renew')}
            </p>
            {auth && <ChangeCardModal subscription={subscription} type="addCard" chargifyAuth={auth} updateBillingCard={updateBillingCard} />}
          </div>
        )}
      </div>
      <div className="paymentSection">
        <h4 data-cy="billing-contact">{i18n('settings.billing.contact')}</h4>
        <Select input={billingSelect} />
        <span className="subtitle card-span">{i18n('settings.billing.contactDescription')}</span>
      </div>
      <div className="paymentSection">
        <h4 data-cy="contact-us">{i18n('settings.billing.contactUs.title')}</h4>
        <p>{i18n('settings.billing.contactUs.contactUs')}</p>
        <p data-cy="contact-phone">
          <PhoneOutlined className="contact-icon" />
          {i18n('settings.billing.contactUs.phone')}
        </p>
        <p data-cy="contact-email">
          <MailOutlined className="contact-icon" />
          <a onClick={() => openEmail(i18n('settings.billing.contactUs.billingHref'))}>{i18n('settings.billing.contactUs.email')}</a>
        </p>
      </div>
    </section>
  );
};

Payment.defaultProps = {
  billing: undefined,
};

export default Payment;
