import React from 'react';
import ContentWrapper from 'view/layout/styles/ContentWrapper';
import ProfileForm from 'view/auth/ProfileForm';
import { i18n } from 'i18n';
import { getHistory } from 'modules/store';

const User = () => {
  return (
    <section id="user" className="settings-section">
      <h2 data-cy="setting-section-title">{i18n('settings.user.title')}</h2>
      <section id="profile" className="settings-subsection">
        <ContentWrapper>
          <ProfileForm onCancel={() => getHistory().push('/')} />
        </ContentWrapper>
      </section>
    </section>
  );
};

export default User;
