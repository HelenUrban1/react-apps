import React from 'react';
import { i18n } from 'i18n';
import { ArrowLeftOutlined, SaveOutlined } from '@ant-design/icons';
import { IxButton } from 'styleguide';
import { SettingState } from '../settingTypes';

interface Props {
  initialValues?: SettingState;
  changed: boolean;
  handleChange: (event: React.ChangeEvent<HTMLInputElement>) => void;
  handleSubmit: () => void;
  handleCancel: () => void;
}

const Options = ({ initialValues, changed, handleChange, handleSubmit, handleCancel }: Props) => {
  const handleClick = (event: React.MouseEvent<HTMLElement, globalThis.MouseEvent>) => {
    event.preventDefault();
    handleSubmit();
  };

  return (
    <section className="settingsForm">
      <form>
        <label htmlFor="promptFont">
          <input type="checkbox" id="promptFont" name="promptFont" checked={initialValues?.promptFont?.value === 'true'} onChange={handleChange} />
          {i18n('settings.user.promptFont')}
        </label>
        <IxButton className="btn-secondary" dataCy="SettingsCancel-btn" leftIcon={<ArrowLeftOutlined />} text={i18n('common.cancel')} onClick={handleCancel} style={{ marginRight: '10px' }} />
        <IxButton className="btn-primary" dataCy="SettingsSubmit-btn" leftIcon={<SaveOutlined />} disabled={!changed} tipText={!changed ? i18n('settings.upToDate') : i18n('settings.saveSettings')} text={i18n('common.save')} onClick={handleClick} />
      </form>
    </section>
  );
};

export default Options;
