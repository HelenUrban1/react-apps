import React, { useEffect, useState } from 'react';
import { i18n } from 'i18n';
import { Checkbox, Tooltip } from 'antd';
import { CheckboxChangeEvent } from 'antd/lib/checkbox';
import { SettingState } from '../settingTypes';

interface Props {
  name: string;
  level: string;
  values: SettingState;
  checked: boolean;
  handleChange: (newSettings: SettingState, key: string) => void;
  disabled?: boolean;
  tooltip?: string;
}

const Options = ({ name, level, values, handleChange, disabled, checked, tooltip }: Props) => {
  const [isChecked, setIsChecked] = useState<boolean>(checked);

  useEffect(() => {
    setIsChecked(checked);
  }, [checked]);

  const onChange = (event: CheckboxChangeEvent) => {
    const old = values[name];
    const { target } = event;

    const newSettings = {
      ...values,
      [name]: old
        ? {
            ...old,
            level,
            value: target.type === 'checkbox' ? target.checked.toString() : target.value,
            name: target.name,
          }
        : {
            level,
            value: target.type === 'checkbox' ? target.checked.toString() : target.value,
            name: target.name,
          },
    };
    handleChange(newSettings, name);
    setIsChecked(event.target.checked);
  };

  return (
    <Tooltip title={tooltip}>
      <Checkbox checked={isChecked} onChange={onChange} name={name} data-testid={name} disabled={disabled} style={{ alignItems: 'start' }}>
        {i18n(`settings.${level}.${name}`)}
      </Checkbox>
    </Tooltip>
  );
};

export default Options;
