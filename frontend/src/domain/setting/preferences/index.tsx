import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router';
import { SaveOutlined } from '@ant-design/icons';
import { i18n } from 'i18n';
import { IxButton } from 'styleguide';
import Permissions from 'security/permissions';
import { AppState } from 'modules/state';
import authSelectors from 'modules/auth/authSelectors';
import log from 'modules/shared/logger';
import I18nSelect from 'view/layout/I18nSelect';
import PermissionChecker from 'modules/auth/permissionChecker';
import { createSettingThunk, updateSettingThunk } from 'modules/session/sessionActions';
import { Settings } from '../settingApi';
import { SettingState } from '../settingTypes';
import Options from './Options';
import '../settingStyles.less';

const invertBooleanValue = (value: boolean) => {
  return !value;
};

const convertStringToBoolean = (value: string | undefined) => {
  return value ? value === 'true' : false;
};

const Preferences = () => {
  const user = useSelector((state: AppState) => authSelectors.selectCurrentUser(state));
  const dispatch = useDispatch();
  const history = useHistory();
  const { userSettings, settings } = useSelector((state: AppState) => state.session);
  const [currentSettings, setCurrentSettings] = useState<SettingState>({ ...userSettings, ...settings });
  const [changed, setChanged] = useState<string[]>([]);
  const permissionValidator = new PermissionChecker(user);

  useEffect(() => {
    setCurrentSettings({ ...currentSettings, ...userSettings });
  }, [userSettings]);

  if (!user) {
    history.push('/');
    return <></>;
  }

  const handleChange = (newSettings: SettingState, key: string) => {
    if (!changed.includes(key)) {
      setChanged([...changed, key]);
    } else {
      setChanged([...changed].filter((k) => k !== key));
    }
    setCurrentSettings(newSettings);
  };

  const enableSetting = (setting: any, userId: string, value: any, id: string) => {
    const data = Settings.createGraphqlObject({ ...setting, userId, value });
    if (id) {
      dispatch(updateSettingThunk(data, id));
    } else {
      dispatch(createSettingThunk(data));
    }
  };

  const handleSubmit = () => {
    if (!userSettings) {
      log.debug('nothing to change');
      return;
    }
    const newChanges = [...changed];
    for (let i = 0; i < changed.length; i++) {
      const key = newChanges.shift();
      if (key) {
        let { value, level } = currentSettings[key];

        if (key === 'promptFont') {
          value = invertBooleanValue(convertStringToBoolean(value)).toString();
        }

        const setting = level === 'user' ? userSettings[key] : settings[key];
        const { id } = setting || {};
        if (value) {
          enableSetting(currentSettings[key], user.id, value, id);
        } else {
          newChanges.push(key);
        }
      }
    }
    setChanged(newChanges);
  };

  // TODO: Should we just have a button to download the font? Seems the only reason someone would re-enable it is to download it,
  // so right now they'd have to re-enable it and re-download a report to get the font, when they could just click a button here instead

  // Line 105, the prompt asks wether the user would like to "disable" the font prompt, and so must show the opposite of the value of promptFront
  return (
    <section id="user" className="settings-section">
      <h2 data-cy="setting-section-title">{i18n('settings.preferences.title')}</h2>
      <section id="language" className="settings-subsection">
        <h4>{i18n('settings.preferences.language')}</h4>
        <I18nSelect />
      </section>
      <form>
        {permissionValidator.match(Permissions.values.partEdit) && (
          <section id="gdt" className="settings-subsection">
            <h4>{i18n('settings.preferences.gdt')}</h4>
            <Options name="promptFont" level="user" values={currentSettings} handleChange={handleChange} checked={!(userSettings.promptFont?.value !== 'false')} />
          </section>
        )}
        <IxButton //
          className="btn-primary"
          dataCy="SettingsSubmit-btn"
          leftIcon={<SaveOutlined />}
          disabled={changed.length === 0}
          tipText={changed.length === 0 ? i18n('settings.upToDate') : i18n('settings.saveSettings')}
          text={i18n('common.save')}
          onClick={handleSubmit}
        />
      </form>
    </section>
  );
};

export default Preferences;
