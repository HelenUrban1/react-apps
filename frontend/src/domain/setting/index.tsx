import React, { useState, useEffect } from 'react';
import { useQuery, useMutation } from '@apollo/client';
import { AppState } from 'modules/state';
import { doUpdateCurrentSubscription } from 'modules/subscription/subscriptionActions';
import { i18n } from 'i18n';
import { Loader } from 'view/global/Loader';
import { useHistory } from 'react-router';
import { notification } from 'antd';
import { RootStateOrAny, useDispatch, useSelector } from 'react-redux';
import OrganizationAPI, { OrganizationList, OrganizationListVars } from 'graphql/organization';
import log from 'modules/shared/logger';
import metroActions from 'modules/metro/metroActions';
import { User as UserType } from 'types/user';
import Analytics from 'modules/shared/analytics/analytics';
import Permissions from 'security/permissions';
import PermissionChecker from 'modules/auth/permissionChecker';
import authActions from 'modules/auth/authActions';
import { Operator } from 'types/operator';
import config from 'config';
import User from './user';
import {
  iamAPI,
  IamChangeOwner,
  IamChangeOwnerInputVars,
  IamEdit,
  IamEditInputVars,
  authCancelInvite,
  authCancelInviteVars,
  authResendInvite,
  authResendInviteVars,
  authInviteUser,
  authInviteUserVars,
  IamListUsers,
  authRequestAccept,
  authRequestAcceptVars,
  authRequestReject,
} from './iamApi';
import SettingsMenu from './menu';
import AccountSettings from './account';
import Lists from './configurations';
import Seats from './seats';
import Operators from './operators';
import Billing from './billing';
import Preferences from './preferences';
import NotificationsList from './notification/list';
import NetInspect from './netInspect';
import { SubscriptionAPI } from './subscriptionApi';
import './settingStyles.less';
import { Security } from './security';

// reads the URL and parses out the last segment to return
// sets what settings tab should be open on initialization
const getKeyFromUrl = (url: string) => {
  const segments = url.split('/');
  return segments[segments.length - 1];
};

const SettingsPage = () => {
  const { user, accountMembership } = useSelector((state: AppState) => state.session);
  const history = useHistory();
  const [key, setKey] = useState(getKeyFromUrl(history.location.pathname));
  const [users, setUsers] = useState<UserType[]>([]);
  const [error, setError] = useState<any>();
  const [openConfigPanels, setOpenConfigPanels] = useState<string[]>([]);

  const permissionValidator = new PermissionChecker(user);
  const userPermissions = {
    hasOwnership: permissionValidator.match(Permissions.values.accountOwner),
    hasManageAccess: permissionValidator.match(Permissions.values.accountManagement),
    hasBillingAccess: permissionValidator.match(Permissions.values.accountBillingAccess),
    hasReviewer: permissionValidator.match(Permissions.values.reviewer),
    hasSphinxAccess: permissionValidator.match(Permissions.values.sphinxApiKeysAccess),
  };
  const dispatch = useDispatch();
  const { data: subscription } = useSelector((reduxState: AppState) => reduxState.subscription);
  const { loading: subscriptionLoading, error: subscriptionError, state } = useSelector((reduxState: RootStateOrAny) => reduxState.subscription);
  const { operators } = useSelector((reduxState: AppState) => reduxState.metro);

  useEffect(() => {
    if (permissionValidator.match(Permissions.values.metro) && user?.activeAccountMemberId) {
      dispatch(metroActions.doInitMetro(user.activeAccountMemberId, { station: '', operator: '' }));
    }
  }, []);

  useEffect(() => {
    if (subscriptionError) {
      setError(subscriptionError);
    }
  }, [subscriptionError]);

  // -------- Seats --------
  // load the list of users for this account, skip this query if the user doesn't have admin access
  const { loading: listLoading, refetch } = useQuery<IamListUsers>(iamAPI.query.list, {
    variables: { filter: { activeAccountId: accountMembership?.accountId || '' } },
    fetchPolicy: 'cache-and-network',
    skip: !userPermissions.hasManageAccess && !userPermissions.hasBillingAccess,
    onCompleted: ({ iamListUsers }) => {
      setUsers(iamListUsers.rows.filter((listUser) => !listUser.disabled && listUser.accountMemberships.find(({ accountId }) => accountMembership?.accountId === accountId)));

      const paidUsersCount = iamListUsers.rows.filter((seat) => !seat.disabled && seat.paid).length;
      if (subscription && paidUsersCount !== subscription?.paidUserSeatsUsed) {
        const updatedSubscription = SubscriptionAPI.createGraphqlObject({
          ...subscription,
          paidUserSeatsUsed: paidUsersCount,
        });
        dispatch(doUpdateCurrentSubscription(subscription.id, updatedSubscription, 'Update_Usage'));
      }
    },
    onError: (err) => {
      log.error('SettingsPage.iamAPI.query.list error', err);
      setError({ message: i18n('errors.query.iamUsers') });
    },
  });

  const refetchUsers = () => {
    if (refetch) {
      refetch({
        variables: { filter: { activeAccountId: accountMembership?.accountId || '' } },
        fetchPolicy: 'no-cache',
      });
    }
  };

  // update the list of users in the state to maintain up to date data
  const updateUsers = (editedUser: UserType) => {
    const newUsers = [...users];
    const oldInd = newUsers.findIndex((arrUser) => arrUser.id === editedUser.id);
    newUsers[oldInd] = editedUser;
    setUsers(newUsers);
  };

  const [resendUserInvite, { loading: resendInviteLoading }] = useMutation<authResendInvite, authResendInviteVars>(iamAPI.mutate.resendInvite, {
    onCompleted: (data) => {
      if (data.authResendInvite) {
        notification.success({
          message: i18n('user.invite.success.title'),
          description: i18n('user.invite.success.description'),
        });
        Analytics.track({
          event: Analytics.events.inviteReSent,
          properties: {},
        });
      } else {
        notification.error({
          message: i18n('user.invite.failure.title'),
          description: i18n('user.invite.failure.description'),
        });
        Analytics.track({
          event: Analytics.events.inviteReSentError,
          properties: {},
        });
      }
      refetchUsers();
    },
    onError: (err) => {
      log.error('SettingsPage.resendUserInvite error', err);
      setError({ message: i18n('errors.edit.iamUsers') });
    },
  });

  const [rejectUserRequest, { loading: rejectRequestLoading }] = useMutation<authRequestReject>(iamAPI.mutate.rejectRequest, {
    onCompleted: (data) => {
      if (data.authRequestReject) {
        notification.success({
          message: i18n('user.invite.reject.title'),
          description: i18n('user.invite.reject.description'),
        });
        Analytics.track({
          event: Analytics.events.accessRequestRejected,
          properties: {},
        });
      } else {
        notification.error({
          message: i18n('errors.reject.title'),
          description: i18n('errors.reject.description'),
        });
        Analytics.track({
          event: Analytics.events.accessRequestRejectedError,
          properties: {},
        });
      }
      refetchUsers();
    },
    onError: (err) => {
      log.error('SettingsPage.rejectRequestLoading error', err);
      setError({ message: i18n('errors.edit.iamUsers') });
    },
  });

  const [cancelUserInvite, { loading: cancelInviteLoading }] = useMutation<authCancelInvite, authCancelInviteVars>(iamAPI.mutate.cancelInvite, {
    onCompleted: (data) => {
      if (data.authCancelInvite) {
        notification.success({
          message: i18n('user.invite.cancel.title'),
          description: i18n('user.invite.cancel.description'),
        });
        Analytics.track({
          event: Analytics.events.inviteEmailCanceled,
          properties: {},
        });
      } else {
        notification.error({
          message: i18n('errors.cancelInvite.title'),
          description: i18n('errors.cancelInvite.description'),
        });
        Analytics.track({
          event: Analytics.events.inviteEmailCanceledError,
          properties: {},
        });
      }
      refetchUsers();
    },
    onError: (err) => {
      log.error('SettingsPage.cancelInviteLoading error', err);
      setError({ message: i18n('errors.edit.iamUsers') });
    },
  });

  const [acceptUserRequest, { loading: acceptUserLoading }] = useMutation<authRequestAccept, authRequestAcceptVars>(iamAPI.mutate.acceptRequest, {
    onCompleted: (data) => {
      if (data.authRequestAccept) {
        notification.success({
          message: i18n('user.invite.accept.title'),
          description: i18n('user.invite.accept.description'),
        });
      } else {
        notification.error({
          message: i18n('errors.acceptFailed.title'),
          description: i18n('errors.acceptFailed.description'),
        });
      }
      refetchUsers();
    },
    onError: (err) => {
      log.error('SettingsPage.acceptUserRequest error', err);
      setError({ message: i18n('errors.edit.iamUsers') });
    },
  });

  const [transferOwner, { loading: transferOwnerLoading }] = useMutation<IamChangeOwner, IamChangeOwnerInputVars>(iamAPI.mutate.transferOwner, {
    onCompleted: ({ iamChangeOwner }) => {
      // after success show the changeing roles page, then redirect and refresh to allow changes to take effect
      history.push('/changing-roles', { newRoles: iamChangeOwner[0].roles });
      Analytics.track({
        event: Analytics.events.ownerTransferredOwnership,
        user: {
          currentUser: user,
          targetUser: iamChangeOwner[1],
        },
        properties: {
          'currentUser.id': user?.id,
          'targetUser.id': iamChangeOwner[1].id,
          'targetUser.roles': iamChangeOwner[1].roles,
          'targetUser.accountMember.status': iamChangeOwner[1].accountMemberships.find((mem) => mem.accountId === iamChangeOwner[1].activeAccountId)?.status,
        },
      });
    },
    onError: (err) => {
      log.error('SettingsPage.transferOwner error', err);
      setError({ message: i18n('errors.edit.iamUsers') });
    },
  });

  const [editIamUser, { loading: editUserLoading }] = useMutation<IamEdit, IamEditInputVars>(iamAPI.mutate.edit, {
    onCompleted: ({ iamEdit }) => {
      updateUsers(iamEdit);
      // current user has admin access, current user is the edited user, current user removed their admin role
      const newMembership = iamEdit.accountMemberships.find((mem) => mem.accountId === iamEdit.activeAccountId);
      // update User if User edited themselves
      if (iamEdit.id === user?.id) {
        if (newMembership?.status === 'Archived') {
          dispatch(authActions.doSignout());
        } else if (iamEdit.roles?.join() !== user?.roles?.join()) {
          // self role was downgraded, after success show the changeing roles page then redirect and refresh to allow changes to take effect
          history.push('/changing-roles', {
            newRoles: iamEdit.roles,
            status: newMembership?.status,
          });
          Analytics.track({
            event: Analytics.events.adminRemovedOwnRole,
            user: {
              currentUser: user,
              targetUser: iamEdit,
            },
            properties: {
              'currentUser.id': user.id,
              'targetUser.id': iamEdit.id,
              'targetUser.roles': iamEdit.roles,
              'targetUser.accountMember.status': newMembership?.status,
              'targetUser.accountMember.access': newMembership?.accessControl,
            },
          });
        }
        const newUser = { ...user };
        newUser.accountMemberships = iamEdit.accountMemberships;
        newUser.roles = iamEdit.roles;
        newUser.paid = iamEdit.paid;
        dispatch(authActions.doRefreshCurrentUser());
      } else {
        // Admin changed other user's status/roles/access
        Analytics.track({
          event: Analytics.events.adminChangedUserProperties,
          user: {
            currentUser: user,
            targetUser: iamEdit,
          },
          properties: {
            'currentUser.id': user?.id,
            'targetUser.id': iamEdit.id,
            'targetUser.roles': iamEdit.roles,
            'targetUser.accountMember.status': newMembership?.status,
            'targetUser.accountMember.access': newMembership?.accessControl,
          },
        });
      }
    },
    onError: (err) => {
      log.error('SettingsPage.editIamUser error', err);
      setError({ message: i18n('errors.edit.iamUsers') });
    },
    refetchQueries: [
      {
        query: iamAPI.query.list,
        variables: { filter: { activeAccountId: accountMembership?.accountId } },
      },
    ],
  });

  const [inviteUser, { loading: inviteUserLoading }] = useMutation<authInviteUser, authInviteUserVars>(iamAPI.mutate.invite);

  // fetch the list of Organizations from the DB to get the current Account's organization
  const { data, loading: orgLoading } = useQuery<OrganizationList, OrganizationListVars>(OrganizationAPI.query.list, {
    fetchPolicy: 'no-cache',
    skip: !userPermissions.hasManageAccess,
    variables: {
      filter: { status: 'Active', type: 'Account' },
    },
  });

  // changes the key for the tabs, active tab needs to be tracked in state because the menu is two separate sets of tabs
  // changes the URL to match the current tab
  const changeKey = (section: string) => {
    setKey(section);
    // history.push(`/settings/${section}`);
    // TODO: figure out way to use React Router to change URL without reloading page
    // params: data, title, url
    window.history.pushState(null, 'InspectionXpert', `/settings/${section}`);
  };

  const addOperator = (operator: Operator) => {
    dispatch(metroActions.doAddOperator({ ...operator, fullName: `${operator.firstName} ${operator.lastName}`, status: 'Active' }));
  };

  const editOperator = (operator: Operator) => {
    dispatch(metroActions.doUpdateOperator({ ...operator, fullName: `${operator.firstName} ${operator.lastName}` }));
  };

  const deactivateOperator = (operator: Operator) => {
    dispatch(metroActions.doUpdateOperator({ ...operator, status: 'Inactive' }));
  };

  const settingSections: { [key: string]: any } = {
    profile: <User />,
    preferences: <Preferences />,
    security: config && config.authStrategy === 'cognito' ? <Security /> : <></>,
    notifications: <NotificationsList />,
    account: <AccountSettings />,
    seats: (
      <Seats
        users={users}
        permissions={userPermissions}
        editIamUser={editIamUser}
        transferOwner={transferOwner}
        inviteUser={inviteUser}
        refetchUsers={refetchUsers}
        resendUserInvite={resendUserInvite}
        cancelUserInvite={cancelUserInvite}
        acceptUserRequest={acceptUserRequest}
        inviteLoading={inviteUserLoading}
        rejectUserRequest={rejectUserRequest}
      />
    ),
    operators: <Operators operators={operators} addOperator={addOperator} editOperator={editOperator} deleteOperator={deactivateOperator} users={users} />,
    billing: <Billing setKey={changeKey} users={users} permissions={userPermissions} />,
    configs: <Lists openConfigPanels={openConfigPanels} setOpenConfigPanels={setOpenConfigPanels} />,
    connection: <NetInspect />,
    // un-comment these lines to view the old connection manager
    // connection: <ConnectionView />,
    // new: <ConnectionNewPage />,
    // edit: <ConnectionEditPage />,
  };

  const closeError = () => {
    setError(undefined);
  };

  return (
    <div className="settings-container">
      <Loader
        loading={undefined}
        queries={[
          //
          orgLoading,
          listLoading,
          subscriptionLoading,
          editUserLoading,
          transferOwnerLoading,
          cancelInviteLoading,
          acceptUserLoading,
          resendInviteLoading,
          rejectRequestLoading,
        ]}
        error={error}
        closeError={closeError}
      />

      <main id="settings" className={state}>
        <h1>{i18n('settings.nav.accountSettings')}</h1>
        <div className="settings-content">
          {user && <SettingsMenu currentUser={user} permissions={userPermissions} currentOrg={data?.self.rows[0]} currentTab={key} setKey={changeKey} openConfigPanels={openConfigPanels} setOpenConfigPanels={setOpenConfigPanels} />}
          {settingSections[key]}
        </div>
      </main>
    </div>
  );
};

export default SettingsPage;
