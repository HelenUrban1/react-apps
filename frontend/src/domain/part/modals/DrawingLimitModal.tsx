import React, { useEffect, useRef, useState } from 'react';
import { WarningOutlined } from '@ant-design/icons';
import { Button, Modal } from 'antd';
import { useMutation } from '@apollo/client';
import log from 'modules/shared/logger';
import { i18n } from 'i18n';
import { SubscriptionCard, SubscriptionDue, SubscriptionTotal, DrawingPrices } from 'styleguide';
import CreditCardForm from 'styleguide/CreditCardForm/CreditCardForm';
import Analytics from 'modules/shared/analytics/analytics';
import { convertCentsToUSD, getComponentMetric } from 'utils/DataCalculation';
import { dateFromNumbersToWords } from 'utils/DateOperations';
import { getCurrentPrice } from 'utils/SubscriptionUtils';
import { SubscriptionAPI } from 'domain/setting/subscriptionApi';
import { useSelector } from 'react-redux';
import { AppState } from 'modules/state';
import StepComponent from 'styleguide/Subscription/StepComponent/StepComponent';
import { ComponentPrice, ComponentInput, ComponentInfo, Info, PricePoint } from 'domain/setting/subscriptionTypes';
import { ContactInfo } from 'styleguide/PricingTable/ContactInfo';

interface Props {
  visibility: boolean;
  handleSubscriptionChange: ({
    //
    price,
    components,
    componentInputs,
    due,
  }: {
    price?: number;
    components?: ComponentInfo;
    componentInputs?: ComponentInput[];
    due?: number;
  }) => void;
  setVisibility: React.Dispatch<React.SetStateAction<boolean>>;
}

export const DrawingLimitModal = ({ visibility, handleSubscriptionChange, setVisibility }: Props) => {
  const { data: subscription, auth, catalog, components } = useSelector((reduxState: AppState) => reduxState.subscription);
  const [due, setDue] = useState<number>(0);
  const [pricePerPeriod, setPricePerPeriod] = useState<number>(subscription?.currentBillingAmountInCents || 0);
  const [label, setLabel] = useState<{ [x: string]: string }>({
    payment: convertCentsToUSD(pricePerPeriod),
    due: convertCentsToUSD(due * 100),
    remaining: '',
    ending: dateFromNumbersToWords(subscription?.currentPeriodEndsAt),
  });
  const componentsChangeData = useRef<{ [key: string]: ComponentInput }>({});
  const [componentData, setComponentData] = useState<Info>();
  const [loading, setLoading] = useState(false);

  const resetModal = () => {
    const drawings = components.find((comp) => comp.unit_name.includes('drawing')) || components.find((comp) => comp.unit_name.includes('part'));
    if (!components || !subscription) {
      log.error('no components or subscription');
      log.debug({ components, subscription, catalog });
      return;
    }

    if (!drawings || drawings.pricing_scheme !== 'stairstep') {
      log.error('component limit not available');
      return;
    }
    let compPrice = 0;
    let pricePointId = 'custom';
    const prices = catalog[subscription?.providerFamilyHandle || 'none']?.components[drawings?.component_handle || 'none']?.prices || [];
    const priceArray = getCurrentPrice(subscription?.billingPeriod || 'Annually', prices);
    const priceMap: { [key: string]: ComponentPrice } = {};
    (priceArray as PricePoint).prices.forEach((price) => {
      priceMap[price.id] = price;
      if (drawings.allocated_quantity >= price.starting_quantity && (!price.ending_quantity || drawings.allocated_quantity <= price.ending_quantity)) {
        compPrice = parseFloat(price.unit_price) * 100;
        pricePointId = price.id.toString(); // Because for some reason price_point_id doesn't match this
      }
    });

    const unit = getComponentMetric(drawings.unit_name);
    setComponentData({
      name: unit,
      unit: drawings.unit_name,
      id: drawings.component_id,
      handle: drawings.component_handle || 'none',
      used: 0,
      purchased: drawings.allocated_quantity,
      priceId: pricePointId,
      price: compPrice,
      type: drawings.pricing_scheme || 'per_unit',
      priceMap,
      prices: priceArray ? (priceArray as PricePoint).prices : [],
    });
    setDue(0);
    setPricePerPeriod(compPrice);
    setLabel({ ...label, due: '$0.00', payment: convertCentsToUSD(compPrice) });
  };

  useEffect(() => {
    if (visibility) {
      resetModal();
    }
  }, [visibility]);

  const closeCardModal = () => {
    Analytics.track({
      event: Analytics.events.subscriptionDrawingsChangeCardCancel,
      subscription,
      properties: {
        'subscription.period': subscription?.billingPeriod,
        'subscription.tier': subscription?.providerProductId,
        'subscription.drawings.new': componentData?.purchased,
      },
    });
    Modal.destroyAll();
  };

  const [updatedBillingToken] = useMutation(SubscriptionAPI.mutate.addPaymentMethod, {
    onCompleted: (data) => {
      if (data?.subscriptionBillingPaymentMethodCreate) {
        closeCardModal();
      } else {
        Modal.error({
          title: i18n('errors.chargify.changeCard.message'),
          content: <p>{i18n('errors.chargify.changeCard.description')}</p>,
          onOk: closeCardModal,
        });
      }
      setLoading(false);
    },
    onError: (error) => {
      log.error('DrawingLimitModal.addPaymentMethod error', error);
      setLoading(false);
      return false;
    },
    // refetchQueries: [{ query: SubscriptionAPI.query.find, variables: { accountId: user.accountMemberships.find((membership: any) => membership.id === user.activeAccountMemberId).accountId } }],
  });

  const changeComponent = (handle: string, compType: string, value: string) => {
    if (!subscription || subscription?.currentBillingAmountInCents === undefined || !componentData) {
      log.error('Cannot calculate due when missing subscription fields');
      return;
    }
    const newComponent = { ...componentData };
    const step = newComponent.priceMap[value];
    if (!step.ending_quantity) {
      // Contact sales for amounts past limit
      // TODO: sales contact
      return;
    }
    Analytics.track({
      event: Analytics.events.subscriptionDrawingsChangeDrawings,
      subscription,
      properties: {
        'subscription.period': subscription?.billingPeriod,
        'subscription.tier': subscription?.providerProductId,
        'subscription.drawings.old': newComponent.purchased,
        'subscription.drawings.new': step.ending_quantity,
      },
    });
    newComponent.priceId = value;
    newComponent.price = parseFloat(step.unit_price) * 100;
    newComponent.purchased = step.ending_quantity;
    const price = parseFloat(step.unit_price) * 100;
    const dueAmount = subscription.currentBillingAmountInCents - price;

    Analytics.track({
      event: Analytics.events.subscriptionDrawingsChangeCalculateDue,
      subscription,
      properties: {
        'subscription.period': subscription?.billingPeriod,
        'subscription.tier': subscription?.providerProductId,
        'subscription.drawings.old': newComponent.purchased,
        'subscription.drawings.new': step.ending_quantity,
        'subscription.current': subscription.currentBillingAmountInCents,
        'subscription.due': dueAmount,
        'subscription.cost': price,
      },
    });
    setPricePerPeriod(price);
    setDue(dueAmount);
    setLabel({
      payment: convertCentsToUSD(price),
      due: convertCentsToUSD(dueAmount),
      remaining: '',
      ending: dateFromNumbersToWords(subscription?.currentPeriodEndsAt),
    });
    componentsChangeData.current[newComponent.handle || 'none'] = {
      component_id: newComponent.id,
      product_family_key: subscription?.providerFamilyHandle || 'none',
      quantity: newComponent.purchased,
      memo: 'User Triggered Component Change via Application',
    };
    setComponentData(newComponent);
  };

  const updateAndClose = async (token: string) => {
    if (!subscription || !token) {
      setLoading(false);
      return;
    }
    Analytics.track({
      event: Analytics.events.subscriptionDrawingsChangeCardConfirm,
      subscription,
      properties: {
        'subscription.period': subscription.billingPeriod,
        'subscription.tier': subscription.providerProductId,
        'subscription.drawings.new': componentData?.purchased,
        billingToken: token,
      },
    });
    await updatedBillingToken({
      variables: {
        id: subscription.id,
        data: {
          accountId: subscription.accountId,
          billingToken: token,
          customerId: subscription.customerId,
        },
      },
    });
  };

  const changeCreditCard = () => {
    if (!subscription) return;
    Analytics.track({
      event: Analytics.events.subscriptionDrawingsChangeCardStart,
      subscription,
      properties: {
        'subscription.period': subscription.billingPeriod,
        'subscription.tier': subscription.providerProductId,
        'subscription.drawings.new': componentData?.purchased,
      },
    });
    Modal.info({
      title: '',
      icon: <></>,
      width: 564,
      className: 'cardModal',
      content: (
        <>
          <CreditCardForm subscription={subscription} formSignifier={subscription.cardType} chargifyAuth={auth} loading={loading} setLoading={setLoading} handleClose={closeCardModal} onSubmit={updateAndClose} onError={closeCardModal} />
          <div className="info-footer contact-us Horizontal">
            <ContactInfo layout="Horizontal" />
          </div>
          <div className="footer-border" />
        </>
      ),
    });
  };

  const handleClose = () => {
    Analytics.track({
      event: Analytics.events.subscriptionDrawingsChangeCancel,
      subscription,
      properties: {
        'subscription.period': subscription?.billingPeriod,
        'subscription.tier': subscription?.providerProductId,
        'subscription.drawings.new': componentData?.purchased,
      },
    });
    setVisibility(false);
  };
  const handleSave = () => {
    if (!subscription || !componentData) return;
    Analytics.track({
      event: Analytics.events.subscriptionDrawingsChangeConfirm,
      subscription,
      properties: {
        'subscription.period': subscription.billingPeriod,
        'subscription.tier': subscription.providerProductId,
        'subscription.drawings.new': componentData?.purchased,
      },
    });
    handleSubscriptionChange({
      price: pricePerPeriod,
      componentInputs: Object.values(componentsChangeData.current),
      due,
    });
    setVisibility(false);
  };

  const modalTitle = (
    <h4 className="limit-modal" data-cy="drawing-limit-modal">
      <WarningOutlined />
      {i18n('entities.subscription.drawings.limit')}
    </h4>
  );

  const modalButtons = (
    <>
      <Button size="large" onClick={handleClose}>
        {i18n('common.cancel')}
      </Button>
      <Button type="primary" size="large" className="primary" onClick={handleSave}>
        {due > 0 ? i18n('entities.subscription.change.pay') : i18n('entities.subscription.change.confirm')}
      </Button>
    </>
  );
  if (!componentData) {
    return <></>;
  }
  return (
    <Modal
      destroyOnClose
      visible={visibility}
      title={modalTitle}
      onCancel={handleClose}
      className="general-modal change-modal-btn"
      okText={due > 0 ? i18n('entities.subscription.change.pay') : i18n('entities.subscription.change.confirm')}
      onOk={handleSave}
      cancelText={i18n('common.cancel')}
      centered
      footer={modalButtons}
    >
      <div data-cy="drawing-limit-modal-container" className="modal-body">
        <p>{i18n('entities.subscription.drawings.limitMessage')}</p>
        <div className="flex">
          <StepComponent key={componentData.id} handle={componentData.handle || 'none'} name={componentData.name} bought={componentData.priceId} warn={false} prices={componentData.prices} change={changeComponent} />
          <DrawingPrices interval={subscription?.billingPeriod || 'Annually'} prices={componentData.prices} />
        </div>
        <SubscriptionTotal billingPeriod={subscription?.billingPeriod?.toString() || ''} payment={label.payment} ending={label.ending} />
        {due > 0 && (
          <>
            <SubscriptionDue due={label.due} remaining={label.remaining} />
            <SubscriptionCard cardType={subscription?.cardType || ''} maskedCardNumber={subscription?.maskedCardNumber || ''} changeCreditCard={changeCreditCard} />
          </>
        )}
      </div>
    </Modal>
  );
};
