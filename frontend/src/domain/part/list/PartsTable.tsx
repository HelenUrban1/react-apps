import React, { useEffect, useState } from 'react';
import { FileAddTwoTone, LockOutlined, ThunderboltTwoTone } from '@ant-design/icons';
import { Popconfirm, Table, Tooltip } from 'antd';
import { i18n } from 'i18n';
import { IxTag } from 'styleguide';
import authSelectors from 'modules/auth/authSelectors';
import { ConfirmDelete } from 'view/global/confirmDelete';
import { useDispatch, useSelector } from 'react-redux';
import PermissionChecker from 'modules/auth/permissionChecker';
import Permissions from 'security/permissions';
import { FilterValue, SorterResult, TablePaginationConfig } from 'antd/lib/table/interface';
import { AppState } from 'modules/state';
import { isBeforeProduction } from 'config';
import moment from 'moment';
import { getAutoBalloonResults } from 'utils/Autoballoon/Autoballoon';
import { PartActions } from 'modules/part/partActions';
import IxcTheme from 'styleguide/styles/IxcTheme';
import { PartStepEnum } from 'modules/navigation/navigationActions';
import { Part, PartTypeEnum, PartReviewStatusEnum, PartAccessControlEnum, PartReviewStatusValues, PartAccessControlValues } from '../partTypes';
import AutoballoonStatus from 'utils/Autoballoon/AutoballoonStatus';

const { DOCS } = PartStepEnum;

interface Props {
  data: Part[];
  pageCount: number;
  handlePartExtract: (record: Part, source: string) => void;
  handlePartDelete: (record: Part, source: string) => void;
  handlePartRevision: (record: Part) => void;
  handleRowClick: (record: Part) => void;
  handleChange: (pagination: TablePaginationConfig, filters: Record<string, FilterValue | null>, sorter: SorterResult<Part> | SorterResult<Part>[], extra: any) => void;
  displayDrawer: boolean;
  loading: boolean;
}

interface Filters {
  text: PartTypeEnum | PartReviewStatusEnum | PartAccessControlEnum;
  value: PartTypeEnum | PartReviewStatusEnum | PartAccessControlEnum;
}
interface Customers {
  text: string;
  value: string;
}

type Dupe = PartTypeEnum | PartReviewStatusEnum | PartAccessControlEnum | string;

export const PartsTable: React.FC<Props> = ({
  data, //
  pageCount,
  handlePartDelete,
  handlePartExtract,
  handlePartRevision,
  handleRowClick,
  handleChange,
  displayDrawer,
  loading,
}) => {
  const dispatch = useDispatch();
  const { settings } = useSelector((state: AppState) => state.session);
  const user = useSelector((state: AppState) => authSelectors.selectCurrentUser(state));
  const { organizations } = useSelector((state: AppState) => state.session);

  const permissionValidator = new PermissionChecker(user);
  const editPartsPermission = permissionValidator.match(Permissions.values.partEdit);

  const restricted = useSelector((state: AppState) => authSelectors.selectIsRestricted(state));
  const { part, tableFilter, tablePage, tableSorter } = useSelector((state: AppState) => state.part);
  // use organization list to fill this out
  const customers: Customers[] = (organizations.all || []).map((item) => {
    return { text: item.name, value: item.id } as Customers;
  });
  // use default values to fill this out
  const statuses: Filters[] = PartReviewStatusValues.map((item: string) => {
    return { text: i18n(`entities.part.enumerators.reviewStatus.${item}`), value: item } as Filters;
  });

  const accessControlStatus: Filters[] = PartAccessControlValues.map((item: string) => {
    if (item === i18n('entities.part.enumerators.accessControl.ITAR_TEXT')) {
      return { text: i18n(`entities.part.enumerators.accessControl.restricted`), value: item } as Filters;
    }
    return { text: i18n(`entities.part.enumerators.accessControl.${item}`), value: item } as Filters;
  });

  const [columns, setColumns] = useState([]);

  const buildColumns = () => {
    const cols: any = [
      {
        width: 90,
        dataIndex: 'key',
        className: 'ix-table-cell actions testing-part-actions-header',
        render: (text: string, record: Part) => {
          if (restricted && record.accessControl && record.accessControl !== 'None') {
            return (
              <span className="part-actions">
                <Tooltip title={i18n('entities.part.restricted') + i18n(`entities.part.enumerators.accessControl.${record.accessControl}`)} placement="top">
                  <LockOutlined />
                </Tooltip>
              </span>
            );
          }
          if (!editPartsPermission) {
            return <></>;
          }

          const canRevise = record.workflowStage >= DOCS;

          return (
            <span className="part-actions">
              <Tooltip title={i18n('entities.part.list.tips.balloon')} placement="top">
                <button type="button" className="icon-button part-extract" onClick={() => handlePartExtract(record, 'list icon')}>
                  <ThunderboltTwoTone twoToneColor={IxcTheme.colors.azure} />
                </button>
              </Tooltip>
              {isBeforeProduction && (
                <Popconfirm okText={i18n('common.yes')} cancelText={i18n('common.no')} trigger="click" title={`${i18n('entities.part.list.tips.revise')}?`} onConfirm={() => handlePartRevision(record)} disabled={!canRevise}>
                  <Tooltip title={canRevise ? i18n('entities.part.list.tips.revise') : i18n('revisions.stage')} placement="top">
                    <button type="button" className="icon-button part-revision" onClick={() => {}} disabled={!canRevise}>
                      <FileAddTwoTone twoToneColor={IxcTheme.colors.sunshine} />
                    </button>
                  </Tooltip>
                </Popconfirm>
              )}
              <ConfirmDelete classes="icon-button part-delete" entity={i18n('entities.part.name')} confirm={() => handlePartDelete(record, 'list icon')} />
            </span>
          );
        },
      },
      {
        title: i18n('entities.part.fields.number'),
        className: 'ix-table-cell testing-part-number-header clickableRow',
        dataIndex: 'number',
        key: 'number',
        sorter: true,
      },
      {
        title: i18n('entities.part.fields.name'),
        className: 'ix-table-cell testing-part-name-header clickableRow',
        dataIndex: 'name',
        key: 'name',
        sorter: true,
      },
      {
        title: 'Revision',
        className: 'ix-table-cell testing-part-revision-header clickableRow',
        dataIndex: 'revision',
        key: 'revision',
        render: (text: string, record: Part) => (
          <Tooltip title={moment(record.createdAt).format('YYYY-MM-DD hh:mm:ss A')}>
            <span className="clickableRow">{record.revision || moment(record.createdAt).format('YYYY-MM-DD')}</span>
          </Tooltip>
        ),
        sorter: true,
      },
      {
        title: i18n('entities.part.fields.customer'),
        className: 'ix-table-cell testing-customer-header clickableRow',
        dataIndex: 'customer',
        key: 'customer',
        filters: customers,
        filteredValue: tableFilter?.customer ? tableFilter.customer : [],
        render: (text: string, record: Part) => <span className="clickableRow">{record.customer ? record.customer.name : ''}</span>,
        sorter: true,
      },
      {
        title: i18n('entities.part.fields.updatedAt'),
        className: 'ix-table-cell testing-edited-header clickableRow',
        dataIndex: 'updatedAt',
        key: 'updatedAt',
        sorter: true,
        render: (date: string) => (
          <Tooltip className="clickableRow" title={moment(date).format('YYYY-MM-DD hh:mm:ss A')}>
            {moment(date).format('YYYY-MM-DD')}
          </Tooltip>
        ),
      },
      {
        title: i18n('entities.part.fields.reviewStatus'),
        className: 'ix-table-cell testing-status-header clickableRow',
        dataIndex: 'reviewStatus',
        key: 'reviewStatus',
        filters: statuses,
        filteredValue: tableFilter?.reviewStatus ? tableFilter.reviewStatus : [],
        render: (text: string) => {
          return (
            <IxTag className="clickableRow" value={text}>
              {text}
            </IxTag>
          );
        },
      },
      {
        title: i18n('entities.part.fields.accessControl'),
        className: 'ix-table-cell testing-access-header clickableRow',
        dataIndex: 'accessControl',
        key: 'accessControl',
        filters: accessControlStatus,
        filteredValue: tableFilter?.accessControl ? tableFilter.accessControl : [],
        render: (text: string) => {
          return (
            <IxTag className="clickableRow" restricted value={text}>
              {text}
            </IxTag>
          );
        },
      },
    ];

    if (settings.autoballoonEnabled?.value === 'true') {
      cols.push({
        title: i18n('entities.part.fields.autoballoon'),
        className: 'ix-table-cell testing-autoballoon-header ix-autoballoon-cell',
        dataIndex: 'autoballoon',
        key: 'autoballoon',
        sorter: false,
        render: (text: string, record: Part) => {
          const autoballoonValues = record?.primaryDrawing?.sheets?.reduce(getAutoBalloonResults, {
            found: 0,
            accepted: 0,
            acceptedTimeout: 0,
            rejected: 0,
            reviewed: 0,
            reviewedTimeout: 0,
            createdAt: record.createdAt,
          });

          return <AutoballoonStatus autoballoonValues={autoballoonValues} part={record} tooltipPlacement="topLeft" />;
        },
      });
    }

    return cols;
  };

  useEffect(() => {
    if (organizations) {
      setColumns(buildColumns());
    }
  }, [organizations, tableFilter, tableSorter, tablePage]);

  useEffect(() => {
    return () => {
      if (!part) {
        dispatch({ type: PartActions.RESET_TABLE });
      }
    };
  }, []);

  return (
    <Table
      className={`main-table${displayDrawer ? ' drawer-open' : ''}`}
      columns={columns}
      dataSource={data}
      rowKey="id"
      rowClassName={(record: Part) => `ix-table-row${part?.originalPart === record.originalPart ? ' ant-table-row-selected' : ''}`}
      onChange={handleChange}
      onRow={(record: Part) => {
        if (restricted && record.accessControl && record.accessControl !== 'None') {
          return {
            className: 'disabled',
          };
        }
        return {
          onClick: (e: React.MouseEvent<HTMLElement>) => {
            const target = e?.target as HTMLElement;
            if (target.tagName === 'TD' || (target.tagName === 'SPAN' && !target.parentElement?.id?.includes('popconfirm') && target?.className && target.className.search('clickableRow') > -1)) {
              handleRowClick(record);
            }
          },
        };
      }}
      pagination={{
        total: pageCount,
        current: tablePage, // from redux
        pageSize: 10, // TODO: Make pagesize configurable
        showSizeChanger: false,
      }}
      loading={loading}
    />
  );
};
