import React, { CSSProperties, ChangeEvent, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router';
import { FileAddOutlined, ReadOutlined, RightOutlined } from '@ant-design/icons';
import { Button, Divider, Drawer, Popconfirm, Select, Tooltip } from 'antd';
import { i18n } from 'i18n';
import log from 'modules/shared/logger';
import { handleFileDownload } from 'utils/files';
import authSelectors from 'modules/auth/authSelectors';
import { createSettingThunk, updateSettingThunk } from 'modules/session/sessionActions';
import { File } from 'types/file';
import { Drawing } from 'graphql/drawing';
import { Settings } from 'domain/setting/settingApi';
import { Setting, SettingInput } from 'domain/setting/settingTypes';
import EditInPlace from 'view/global/EditInPlace';
import CustomerSelect from 'view/global/CustomerSelect';
import PermissionChecker from 'modules/auth/permissionChecker';
import Permissions from 'security/permissions';
import { RevisionHistory } from 'styleguide/Revision/History';
import { ReportMiniList, DocumentMiniList } from 'styleguide/MiniList';
import { ReportPartial } from 'types/reportTemplates';
import { AppState } from 'modules/state';
import { UploadModal } from 'styleguide/Modals/UploadModal';
import { doSetPart, uploadDrawingThunk } from 'modules/part/partActions';
import { Loader } from 'view/global/Loader';
import moment from 'moment';
import { RevisionBanner } from 'styleguide/Revision/RevisionBanner';
import { setPdfOverlayViewState } from 'modules/pdfOverlayView/pdfViewActions';
import { PartEditorActions } from 'modules/partEditor/partEditorActions';
import { Part, PartReviewStatusEnum } from '../partTypes';
import { PartDetailsActions } from './PartDetailsActions';

const { Option } = Select;

interface Props {
  parentSelector: string;
  isOpen: boolean;
  handleDrawerClose: () => void;
  handleReviewStatusChange: (change: PartReviewStatusEnum) => void;
  handleCustomerChange: (value: string) => void;
  handlePartFieldChange: (event: ChangeEvent<HTMLTextAreaElement>) => void;
  handlePartFieldBlur: (event: ChangeEvent<HTMLTextAreaElement>) => void;
  handleChangeRevision: (part: string) => void;
  handlePartRevision: (record: Part) => void;
  handlePartExtract: (part: Part, source: string) => void;
  handlePartDelete: (part: Part, source: string) => void;
  addFile: () => void;
  addAttachment: () => void;
  removeFile: (document: Drawing | File, fileType: 'Drawing' | 'Attachment' | 'Report') => void;
  downloadFile: (publicUrl: string, fileName: string, fileType?: string, partId?: string, drawingId?: string) => void;
  deleteReport: (report: ReportPartial) => void;
  downloadAll: (part: Part) => void;
  saving: boolean;
  limit: number;
  handleDrawerUploadClosed: () => void;
  handleDrawerUploadOpened: () => void;
}

export const PartDetailsDrawer: React.FC<Props> = ({
  parentSelector,
  isOpen, // Whether drawer is visible
  handleDrawerClose, // Closing of drawer
  handlePartFieldChange, // Changes to any EditInPlace text inputs
  handlePartFieldBlur, // Blur on any EditInPlace text inputs
  handleCustomerChange, // Changes to customer select
  handleReviewStatusChange, // Changes to review status select
  handleChangeRevision, // Changes to the selected revision
  handlePartRevision, // Creates a new part revision
  handlePartExtract,
  handlePartDelete,
  addFile,
  addAttachment,
  removeFile,
  downloadFile,
  deleteReport,
  downloadAll,
  saving,
  limit,
  handleDrawerUploadClosed,
  handleDrawerUploadOpened,
}) => {
  const style: CSSProperties = { fontWeight: 'bold', fontSize: '1.1em', resize: 'none' };
  const user = useSelector((state: AppState) => authSelectors.selectCurrentUser(state));
  const { part, revisions, revisionIndex, revisionType } = useSelector((state: AppState) => state.part);
  const { settings, userSettings } = useSelector((state: AppState) => state.session);
  const { currentUser } = useSelector((state: AppState) => state.auth);
  const { state: subState } = useSelector((app: AppState) => app.subscription);
  const { jwt } = useSelector((state: AppState) => state.auth);
  const dispatch = useDispatch();

  const [targetDrawingId, setTargetDrawingId] = useState<string>();
  const [confirmDrawingRevision, setConfirmDrawingRevision] = useState<string>();

  const permissionValidator = new PermissionChecker(user);
  const editPartsPermission = permissionValidator.match(Permissions.values.partEdit);
  const editReportsPermission = permissionValidator.match(Permissions.values.reportEdit);
  const hasSphinxAccess = permissionValidator.match(Permissions.values.sphinxApiKeysAccess);

  const history = useHistory();

  const [historyShow, setHistory] = useState(false);

  const toggleDrawer = () => {
    setHistory((prev) => !prev);
  };

  const handleUpdatePrompt = () => {
    if (!currentUser || !userSettings) {
      log.error(`Missing required info: user? ${!currentUser} settings? ${!userSettings}`);
      return;
    }
    if (userSettings.promptFont) {
      const updatedSetting: Setting = { ...userSettings.promptFont, id: (userSettings.promptFont as Setting).id, value: 'false' };
      dispatch(updateSettingThunk(Settings.createGraphqlObject(updatedSetting), updatedSetting.id));
    } else {
      const newSetting: SettingInput = { name: 'promptFont', userId: currentUser.id, value: 'false' };
      dispatch(createSettingThunk(newSetting, currentUser.id));
    }
  };

  const handleReport = (e?: any, report?: ReportPartial) => {
    if (!part) {
      log.warn('no part');
      return;
    }
    if (e) e.preventDefault();
    dispatch(doSetPart(part, part));
    if (!report) {
      history.push(`/parts/${part.id}/reports/`, { source: `/parts/${part.id}` });
      return;
    }
    history.push(`/parts/${part.id}/reports/${report.id}`, { source: `/parts/${part.id}` });
  };

  const handlePartDrawingUpload = async (files: any[], skipAlignment = false) => {
    const targetDrawing = part?.drawings?.find((drawing) => drawing.id === targetDrawingId);
    const shouldCreatePart = false;
    if (targetDrawing && jwt && part) {
      if (skipAlignment) {
        const redirectToDocsStep = true;
        dispatch(uploadDrawingThunk(files, shouldCreatePart, redirectToDocsStep, targetDrawing, history));
      } else {
        dispatch(setPdfOverlayViewState({ revisionFiles: files }));
        dispatch({ type: PartEditorActions.SET_PART_EDITOR_STATE, payload: { targetDrawing: targetDrawingId } });
        history.push(`/parts/${part.id}`);
      }
    } else {
      dispatch(uploadDrawingThunk(files, shouldCreatePart, skipAlignment, targetDrawing, history));
      setTargetDrawingId(undefined);
      handleDrawerUploadClosed();
    }
  };

  const handleCancelUpload = () => {
    setTargetDrawingId(undefined);
    handleDrawerUploadClosed();
  };

  const handleStartRevision = (drawingId: string) => {
    if (revisionIndex) {
      setConfirmDrawingRevision(drawingId);
    } else {
      setTargetDrawingId(drawingId);
      handleDrawerUploadOpened();
    }
  };

  const getRevisionButtonTooltip = (index: number, number: string | null) => {
    if (!number) return i18n('revisions.stage');
    if (index) return i18n('revisions.oldRevTip');
    return i18n('revisions.create');
  };

  const footer = (
    <section className="list-drawer-footer">
      <Tooltip title="Close Drawer">
        <RightOutlined onClick={handleDrawerClose} />
      </Tooltip>
    </section>
  );

  return (
    <Drawer
      footer={footer}
      footerStyle={{ height: '48px' }}
      getContainer={parentSelector}
      visible={isOpen}
      zIndex={1}
      width={360}
      mask={false}
      placement="right"
      className={`list-page-drawer${Object.keys(revisions)[0] && Object.keys(revisions)[0] !== part?.id && !!part?.revision ? ' old-revision' : ''} ${subState}`}
      closable={false}
    >
      {part && (
        <form className="part-drawer-form">
          <PartDetailsActions
            editPartsPermission={editPartsPermission}
            editReportsPermission={editReportsPermission}
            hasSphinxAccess={editReportsPermission}
            lastReportId={part.lastReportId}
            handleDrawerClose={handleDrawerClose}
            handlePartDelete={handlePartDelete}
            handlePartExtract={handlePartExtract}
            handleReport={handleReport}
            downloadAll={downloadAll}
            saving={saving}
          />
          <section id={`info-input-${part.id}`} className="part-info form-section">
            <EditInPlace label="Part Number" name="number" defaultValue={part.number || ''} style={style} onChange={handlePartFieldChange} onBlur={handlePartFieldBlur} autosize canEdit={editPartsPermission} saving={saving} />
            <EditInPlace label="Part Name" defaultValue={part.name || ''} name="name" style={style} onChange={handlePartFieldChange} onBlur={handlePartFieldBlur} autosize canEdit={editPartsPermission} saving={saving} />
            <section className="revision-select">
              <span className="label-text">Part Revision</span>
              <section className="select-with-button wide preset-row">
                <Select //
                  value={part.id}
                  onChange={(val: string) => handleChangeRevision(val)}
                  size="middle"
                  className="select-save-input"
                  data-cy="rev-select"
                  optionLabelProp="label"
                  dropdownRender={(menu) => (
                    <div className="ix-cascader-dropdown">
                      {menu}
                      <Divider style={{ margin: '4px 0' }} />
                      <button type="button" data-cy="details-rev-history-btn" className="link cascader-btn" onClick={toggleDrawer}>
                        <ReadOutlined />
                        {i18n('toolbar.revisionHistory')}
                      </button>
                    </div>
                  )}
                >
                  {Object.keys(revisions).map((rev, index) => {
                    return (
                      <Select.Option value={revisions[rev].id} key={`rev-${revisions[rev].id}`} label={revisions[rev].revision || moment(revisions[rev].createdAt).format('YYYY-MM-DD')}>
                        {`${revisions[rev].revision || moment(revisions[rev].createdAt).format('YYYY-MM-DD')}${index === 0 ? ' (current)' : ''}`}
                      </Select.Option>
                    );
                  })}
                </Select>
                <Tooltip placement="topLeft" title={getRevisionButtonTooltip(revisionIndex, part.number)}>
                  <Popconfirm
                    okText={i18n('common.yes')}
                    cancelText={i18n('common.no')}
                    trigger="click"
                    title={revisionIndex ? i18n('revisions.oldRevWarn', part?.revision, revisionIndex) : `${i18n('entities.part.list.tips.revise')}?`}
                    onConfirm={() => handlePartRevision(part)}
                    placement="topLeft"
                    disabled={!part.number || Object.keys(revisions).length === 0 || revisionIndex >= 1}
                  >
                    <Button icon={<FileAddOutlined />} className="btn-primary" size="middle" disabled={!part.number || Object.keys(revisions).length === 0 || revisionIndex >= 1} />
                  </Popconfirm>
                </Tooltip>
              </section>
            </section>
            <CustomerSelect value={part.customer && part.customer.id !== part.id ? part.customer.id : ''} onChange={handleCustomerChange} canEdit={editPartsPermission} />
          </section>
          <section className="part-status form-section">
            <label className="form-input">
              {i18n('entities.part.fields.status')} <span className={`indicator ${part.reviewStatus}`} />
              <Select value={part.reviewStatus} onChange={handleReviewStatusChange} disabled={!editPartsPermission}>
                <Option value="Unverified">{i18n('entities.part.enumerators.reviewStatus.Unverified')}</Option>
                <Option value="Ready_for_Review">{i18n('entities.part.enumerators.reviewStatus.Ready_for_Review')}</Option>
                <Option value="Verified">{i18n('entities.part.enumerators.reviewStatus.Verified')}</Option>
              </Select>
            </label>
          </section>
          <section className="part-reports">
            <ReportMiniList
              partEdited={part.updatedAt}
              reports={part.reports}
              download={handleFileDownload}
              prompt={userSettings.promptFont?.value !== 'false'}
              handleUpdatePrompt={handleUpdatePrompt}
              edit={(report) => {
                handleReport(undefined, report);
              }}
              deleteReport={deleteReport}
              create={handleReport}
              canEdit={editReportsPermission}
            />
          </section>
          <section className="part-documents">
            <UploadModal //
              isDisplayed={!!targetDrawingId}
              handleUpload={handlePartDrawingUpload}
              handleCancel={handleCancelUpload}
              fileType="pdf"
              fileExt=".pdf"
              hint="PDF"
              entity={i18n('entities.part.name')}
              limit={1}
              type="Drawing"
              targetDrawing={part.drawings?.find((d) => d.id === targetDrawingId)}
            />
            <Popconfirm
              title={i18n('revisions.oldRevWarn', part?.revision, revisionIndex)}
              onCancel={() => setConfirmDrawingRevision(undefined)}
              onConfirm={() => {
                if (confirmDrawingRevision) {
                  setTargetDrawingId(confirmDrawingRevision);
                  handleDrawerUploadOpened();
                }
                setConfirmDrawingRevision(undefined);
              }}
              visible={!!confirmDrawingRevision}
            >
              <DocumentMiniList
                documents={part.drawings || []}
                primary={part.primaryDrawing.id}
                download={downloadFile}
                deleteDocument={removeFile}
                handleStartRevision={handleStartRevision}
                create={addFile}
                canEdit={editPartsPermission && limit > (part?.drawings?.length || 0)}
                partId={part.id}
                type="Drawing"
              />
            </Popconfirm>
          </section>
          <section className="part-attachments">
            <DocumentMiniList
              documents={part.attachments}
              primary={part.attachments && part.attachments[0] ? part.attachments[0].name : ''}
              download={downloadFile}
              deleteDocument={removeFile}
              create={addAttachment}
              canEdit={editPartsPermission}
              partId={part.id}
              type="Attachment"
            />
          </section>
        </form>
      )}
      {revisionIndex > 0 && revisionType && <RevisionBanner previous={revisionIndex} type={revisionType} short />}
      {isOpen && <Loader queries={[!part]} loading={undefined} error={undefined} closeError={() => {}} />}

      <RevisionHistory parent="main#partsList" historyShow={historyShow} toggleDrawer={toggleDrawer} />
    </Drawer>
  );
};

export default PartDetailsDrawer;
