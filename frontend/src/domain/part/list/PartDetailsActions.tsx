import Icon, { CloseOutlined, DeleteOutlined, DownloadOutlined, EnvironmentOutlined, ExportOutlined, FileTextOutlined, WarningOutlined } from '@ant-design/icons';
import { Button, Dropdown, Menu, Modal, notification, Popconfirm, Radio, Tooltip, Input } from 'antd';
import { ReportPartial } from 'types/reportTemplates';
import { i18n } from 'i18n';
import authSelectors from 'modules/auth/authSelectors';
import log from 'modules/shared/logger';
import { AppState } from 'modules/state';
import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import { NetInspectIcon } from 'styleguide/shared/svg';
import SphinxApi, { UploadResponse, UploadVars } from 'modules/sphinx/sphinxApi';
import { getApiKeyFromConnections } from 'modules/sphinx/connectionModel';
import { useMutation, useQuery } from '@apollo/client';
import { useHistory } from 'react-router-dom';
import Analytics from 'modules/shared/analytics/analytics';
import config from 'config';
import { Part } from '../partTypes';

interface ActionsProps {
  editPartsPermission: boolean;
  editReportsPermission: boolean;
  hasSphinxAccess: boolean;
  lastReportId: string | null;
  handleReport: (e?: any, report?: ReportPartial) => void;
  handleDrawerClose: () => void;
  handlePartExtract: (part: Part, source: string) => void;
  handlePartDelete: (part: Part, source: string) => void;
  downloadAll: (part: Part) => void;
  saving: boolean;
}

const netInspectUrl = 'https://www.net-inspect.com/FirstArticles/';

export const PartDetailsActions = ({ editPartsPermission, editReportsPermission, hasSphinxAccess, lastReportId, handleReport, handlePartExtract, handlePartDelete, handleDrawerClose, downloadAll, saving }: ActionsProps) => {
  const { part, partLoaded } = useSelector((state: AppState) => state.part);
  const { addons, state: trial } = useSelector((store: AppState) => store.subscription);
  const [loading, setLoading] = useState(false);
  const [showNetInspectModal, setShowNetInspectModal] = useState(false);

  const [faiNumber, setFaiNumber] = useState<string | null>(lastReportId);
  const [overwriteFai, setOverwriteFai] = useState(false);

  const user = useSelector((state: AppState) => authSelectors.selectCurrentUser(state));
  const history = useHistory();

  const { accountId, siteId } = user || {};
  const userId = user?.id;
  const findInput = { accountId, siteId, userId };

  const { data: connectionData } = useQuery(SphinxApi.query.connectionFind, {
    variables: { data: findInput },
  });

  const connections = connectionData?.connectionFind || [];
  const apiKey = getApiKeyFromConnections(connections);

  const [uploadFile] = useMutation<UploadResponse, UploadVars>(SphinxApi.mutate.upload, {
    onCompleted: (uploadData) => {
      // Error handling in sphinx is lacking so we can't get much info other than what destination failed
      // Throwing the default error if the FAI number is not valid causing a failure
      const uploadResult = uploadData.upload;
      if (uploadResult.succeeded && uploadResult.succeeded.length > 0) {
        let newFaiNumber;
        let link = null;
        const data = uploadResult.succeeded.find((r) => r.destination === 'Net-Inspect');
        // If the upload succeeded, save and display the FAI report ID
        if (data && data.data) {
          newFaiNumber = data.data;
          link = netInspectUrl + newFaiNumber;
          setFaiNumber(newFaiNumber);
        }
        notification.success({
          message: i18n('files.netInspect.successTitle'),
          description: (
            <span>
              {i18n('files.netInspect.successDescription', part?.name)}
              <br />
              <strong>FAI ID: </strong>
              {newFaiNumber}
              {link && (
                <a href={link} target="_blank" rel="noreferrer" className="upload-link">
                  <br />
                  {i18n('files.netInspect.uploadLink')}
                </a>
              )}
            </span>
          ),
        });

        Analytics.track({
          event: Analytics.events.subscriptionNetInspectUpload,
          part,
          properties: {
            netInspectFaiId: newFaiNumber,
          },
        });
      }
      if (uploadResult.failed && uploadResult.failed.length > 0) {
        notification.error({
          message: i18n('files.netInspect.errorTitle'),
          description: i18n('files.netInspect.errorDescription'),
        });
        Analytics.track({
          event: Analytics.events.subscriptionNetInspectFailure,
          part,
        });
      }
      setLoading(false);
    },
    onError: (err) => {
      log.error(err);
      notification.error({
        message: i18n('files.netInspect.errorTitle'),
        description: i18n('files.netInspect.errorDescription'),
      });

      Analytics.track({
        event: Analytics.events.subscriptionNetInspectFailure,
        part,
      });
      setLoading(false);
    },
  });

  const toggleShowNetInspectModal = () => {
    setShowNetInspectModal(!showNetInspectModal);
  };

  const canShowNetInspectModal = () => {
    if (!part) {
      return;
    }
    if (!apiKey) {
      notification.error({
        message: i18n('files.netInspect.connectionErrorTitle'),
        description: i18n('files.netInspect.connectionErrorDescription'),
        onClose: () => {
          history.push(`/settings/connection`);
        },
        duration: 3,
      });
      return;
    }
    toggleShowNetInspectModal();
  };

  const exportToSphinx = async () => {
    // eslint-disable-next-line no-restricted-globals
    if (overwriteFai && (!faiNumber || isNaN(parseInt(faiNumber, 10)))) {
      notification.warning({
        message: i18n('files.netInspect.errorFaiNumber'),
      });
      return;
    }

    setLoading(true);
    if (overwriteFai && faiNumber) {
      uploadFile({
        variables: {
          data: {
            apiKey,
            partId: part?.id,
            firstArticleId: faiNumber,
          },
        },
      });
    } else {
      uploadFile({
        variables: {
          data: {
            apiKey,
            partId: part?.id,
          },
        },
      });
    }
    setOverwriteFai(false);
    setFaiNumber(null);
    toggleShowNetInspectModal();
  };

  const downloadMenu = (
    <Menu>
      <Menu.Item className="downloadLI">
        <Button size="large" icon={<Icon className="net-inspect-icon" component={NetInspectIcon} />} className="excelDownload btn-dropdown" data-cy="excel-action-download-excel" onClick={canShowNetInspectModal}>
          {i18n('files.netInspect.defaultTitle')}
        </Button>
      </Menu.Item>
      <Menu.Item className="downloadLI">
        <Button
          size="large"
          icon={<DownloadOutlined />}
          className="excelDownload btn-dropdown"
          data-cy="excel-action-download-excel"
          onClick={async (e: any) => {
            if (!e || !part || !partLoaded) {
              log.warn(`Click disabled: Part ${part} Event ${e} Loaded ${partLoaded}`);
              return;
            }
            e.preventDefault();
            downloadAll(part);
          }}
        >
          {i18n('files.localDownload.title')}
        </Button>
      </Menu.Item>
    </Menu>
  );
  // const showExport = hasSphinxAccess && (trial === 'trial' || !!addons.net_inspect);
  const showExport = config && config.sphinx.netinspect.enabled && hasSphinxAccess && (trial === 'trial' || !!addons.net_inspect);
  return (
    <section className={`drawer-actions ${!partLoaded ? 'disabled' : ''}`}>
      <Modal onCancel={toggleShowNetInspectModal} onOk={exportToSphinx} visible={showNetInspectModal} title={i18n('files.netInspect.exportTitle')} className="general-modal part-continue-modal">
        <Radio.Group
          style={{ paddingBottom: '10px' }}
          onChange={(e) => {
            if (e?.target?.value === 'Overwrite Existing FAI') {
              setOverwriteFai(true);
            } else {
              setOverwriteFai(false);
            }
          }}
          // I'm lazy
          value={overwriteFai ? 'Overwrite Existing FAI' : 'New FAI'}
          buttonStyle="solid"
        >
          <Radio.Button value="New FAI">New FAI</Radio.Button>
          <Radio.Button value="Overwrite Existing FAI">Overwrite Existing FAI</Radio.Button>
        </Radio.Group>
        {overwriteFai && (
          <Input
            value={faiNumber || ''}
            onChange={(e) => {
              setFaiNumber(e?.target?.value);
            }}
            placeholder={i18n('files.netInspect.placeholderText')}
          />
        )}
      </Modal>
      {editPartsPermission && (
        <Tooltip title="Balloon Part">
          <Button
            data-cy="detail-extract"
            className="btn-primary square-lg"
            onClick={() => {
              if (!part || !partLoaded) {
                log.warn(`Click disabled: Part ${part} Loaded ${partLoaded}`);
                return;
              }
              handlePartExtract(part, 'drawer icon');
            }}
          >
            <EnvironmentOutlined />
          </Button>
        </Tooltip>
      )}
      {editReportsPermission && (
        <Tooltip title="Part Report">
          <Button
            data-cy="detail-new-report"
            className="btn-primary square-lg"
            onClick={(e: any) => {
              if (!e || !partLoaded) {
                log.warn(`Click disabled: Event ${e} Loaded ${partLoaded}`);
                return;
              }
              handleReport(e);
            }}
          >
            <FileTextOutlined />
          </Button>
        </Tooltip>
      )}

      {showExport && (
        <Tooltip title="Export Part">
          <Dropdown overlay={downloadMenu} overlayClassName="excelActionDownload" trigger={['click']}>
            <Button loading={loading || saving} data-cy="detail-export" className="btn-primary square-lg" onClick={(e: any) => e.preventDefault()}>
              {!(loading || saving) && <ExportOutlined />}
            </Button>
          </Dropdown>
        </Tooltip>
      )}

      {editPartsPermission && (
        <Tooltip title="Delete Part">
          <Popconfirm
            placement="bottomRight"
            title={i18n('common.areYouSure')}
            onConfirm={() => {
              if (part) handlePartDelete(part, 'drawer icon');
            }}
            icon={<WarningOutlined style={{ color: 'red' }} />}
            okText={i18n('common.destroy')}
            cancelText={i18n('common.cancel')}
          >
            <Button
              data-cy="detail-delete"
              className="btn-primary btn-danger square-lg"
              onClick={(e: any) => {
                if (!partLoaded) {
                  log.warn(`Click disabled: Loaded ${partLoaded}`);
                  e.preventDefault();
                }
              }}
            >
              <DeleteOutlined />
            </Button>
          </Popconfirm>
        </Tooltip>
      )}
      <Tooltip title="Close Drawer" placement="topLeft">
        <Button
          data-cy="drawer-close"
          className="btn-secondary square-lg disabled"
          onClick={(e: any) => {
            if (!partLoaded) {
              log.warn(`Click disabled: Part ${part} Event ${e} Loaded ${partLoaded}`);
              return;
            }
            handleDrawerClose();
          }}
        >
          <CloseOutlined />
        </Button>
      </Tooltip>
    </section>
  );
};
