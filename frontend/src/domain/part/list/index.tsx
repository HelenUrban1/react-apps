import React, { useState, useEffect, ChangeEvent, useRef, useCallback } from 'react';
import { useHistory } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { useLazyQuery } from '@apollo/client';
import { io } from 'socket.io-client';
import { debounce } from 'lodash';
import config from 'config';
import { i18n } from 'i18n';
import { saveAs } from 'file-saver';
import { TablePaginationConfig } from 'antd/lib/table/interface';
import { PlusSquareOutlined, SearchOutlined } from '@ant-design/icons';
import { Button, Input, Row, Col, Modal, notification } from 'antd';
import Permissions from 'security/permissions';
import OrganizationAPI from 'graphql/organization';
import { Characteristics } from 'graphql/characteristics';
import { CharacteristicList } from 'types/characteristics';
import { File } from 'types/file';
import { Drawing } from 'graphql/drawing';
import { Loader } from 'view/global/Loader';
import { UploadModal } from 'styleguide/Modals/UploadModal';
import { canAddDrawings } from 'utils/SubscriptionUtils';
import { downloadFile, convertFile, downloadBalloonedFile } from 'utils/files';
import { AppState } from 'modules/state';
import { steps } from 'modules/part/partApi';
import Analytics from 'modules/shared/analytics/analytics';
import { doSetSubscription, doUpdateCurrentSubscription } from 'modules/subscription/subscriptionActions';
import authSelectors from 'modules/auth/authSelectors';
import {
  doAddPartRevisions,
  deleteAttachmentThunk,
  doDeletePart,
  doGetPartRevisions,
  doSetPart,
  doSetTableFilter,
  doSetTablePage,
  doSetTableSorter,
  PartActions,
  updatePartThunk,
  uploadAttachmentsThunk,
  uploadDrawingThunk,
  deleteDrawingThunk,
  doGetPart,
} from 'modules/part/partActions';
import { Parts, PartList } from 'domain/part/partApi';
import { Component, ComponentInput, Subscription } from 'domain/setting/subscriptionTypes';
import { ReportAPI } from 'graphql/report';
import { ReportPartial } from 'types/reportTemplates';
import { SubscriptionAPI } from 'domain/setting/subscriptionApi';
import axios from 'modules/shared/network/axios';
import log from 'modules/shared/logger';
import { FileObject } from 'styleguide/File/Upload';
import { NavigationActions, PartStepEnum } from 'modules/navigation/navigationActions';
import { PartEditorActions } from 'modules/partEditor/partEditorActions';
import { ReportTemplateActions, doDeleteReport } from 'modules/reportTemplate/reportTemplateActions';
import PermissionChecker from 'modules/auth/permissionChecker';
import { CharacteristicActions } from 'modules/characteristic/characteristicActions';
import { PartDetailsDrawer } from './PartDetailsDrawer';
import { PartsTable } from './PartsTable';
import { Part, PartReviewStatusEnum } from '../partTypes';
import { DrawingLimitModal } from '../modals/DrawingLimitModal';
import '../partStyles.less';

const PartsList: React.FC = () => {
  const dispatch = useDispatch();
  const user = useSelector((state: AppState) => authSelectors.selectCurrentUser(state));
  const { part, previousPart, originalPart, newPart, partLoaded, tableSorter, tableFilter, partError, tablePage } = useSelector((state: AppState) => state.part);
  const { accountMembership, settings } = useSelector((state: AppState) => state.session);

  const { jwt } = useSelector((state: AppState) => state.auth);
  const { data: subscription, loading: subscriptionLoading, error: subscriptionError, components, type, catalog } = useSelector((reduxState: AppState) => reduxState.subscription);
  const history = useHistory();
  const shouldCreatePart = useRef(true);

  const [error, setError] = useState<any>();
  const [displayContinue, setDisplayContinue] = useState(false);
  const doRefetch = useRef<any>();

  const permissionValidator = new PermissionChecker(user);
  const createPartsPermission = permissionValidator.match(Permissions.values.partCreate) && (subscription?.status?.toLowerCase() === 'trialing' || subscription?.status?.toLowerCase() === 'active');

  // Track whether the details drawer is displayed or not
  const [displayDrawer, setDisplayDrawer] = useState(false);

  // Track whether the upload modal is displayed or not
  const [displayUpload, setDisplayUpload] = useState<'image' | 'pdf' | 'excel' | 'any' | undefined>();
  const [drawerUploadOpen, setDrawerUploadOpen] = useState(false);

  // Always refetch with the same query params
  // query shit goes here!!!!!
  const [partListQueryParams, setPartListQueryParams] = useState<{ filter: { status: string[]; searchTerm?: string; customer?: string[]; reviewStatus?: string[]; nextRevision: null | string }; orderBy: string; limit: number; offset: number }>({
    filter: { status: ['Active', 'Inactive'], reviewStatus: [], customer: [], nextRevision: null },
    orderBy: 'updatedAt_DESC',
    limit: 10,
    offset: 0,
  });

  const [fetchList, { loading, error: partListError, data, fetchMore, refetch }] = useLazyQuery<PartList>(Parts.query.list, {
    variables: { ...partListQueryParams, filter: { ...partListQueryParams.filter, nextRevision: null } },
  });
  useEffect(() => {
    fetchList();
    if (refetch) {
      doRefetch.current = debounce(refetch, 2000, { trailing: true, leading: false, maxWait: 5000 });
    }
  }, [partListQueryParams]);

  const findParts = async () => {
    let orderByString = '';
    // when the sorter gets cleared only order is undefined
    if (tableSorter?.order) {
      if (tableSorter.columnKey === 'customer') {
        orderByString = 'customer_name';
      } else if (tableSorter.columnKey) {
        orderByString = tableSorter.columnKey;
      } else {
        orderByString = 'updatedAt';
      }
      if (tableSorter.order === 'ascend') {
        orderByString += '_ASC';
      } else {
        orderByString += '_DESC';
      }
    } else {
      orderByString = 'updatedAt_DESC';
    }
    setPartListQueryParams({
      ...partListQueryParams,
      filter: {
        ...partListQueryParams.filter,
        ...tableFilter,
      },
      orderBy: orderByString,
    });
  };

  const partUpdateHandler = () => {
    if (doRefetch.current) {
      doRefetch.current({
        variables: {
          ...partListQueryParams,
        },
        fetchPolicy: 'no-cache',
      });
    }
  };

  useEffect(() => {
    dispatch({ type: ReportTemplateActions.CLEAR });
    dispatch({ type: NavigationActions.RESET });
    dispatch({ type: CharacteristicActions.CLEANUP_STORE });
    if (settings.autoballoonEnabled?.value === 'true' && config.wsUrl) {
      // Update parts list when parts or drawingSheets update so we can see autoballoon progress
      const socket = io(config.wsUrl, { transports: ['websocket'], withCredentials: true, auth: { token: `Bearer ${jwt}` }, path: config.wsPath });
      // May be able to optimize this by using the part info to update redux
      // or only running findParts if the updated part is on the current page, but may still want to handle part.create with a call to findParts
      socket.on('part.update', partUpdateHandler);
      socket.on('drawingSheet.update', partUpdateHandler);
      socket.on('part.autoMarkupCompleted', partUpdateHandler);
      return () => {
        socket.off('part.update', partUpdateHandler);
        socket.off('drawingSheet.update', partUpdateHandler);
        socket.disconnect();
      };
    }
    return () => null;
  }, []);

  useEffect(() => {
    // tableSorter will have {columnKey: string "number" | "name" | "customer" | "updatedAt", order: "ascend" | "descend"
    findParts();
  }, [tableSorter, tableFilter]);

  const [saving, setSaving] = useState(false);

  useEffect(() => {
    if (partError) {
      setError(partError);
    }
  }, [partError]);

  useEffect(() => {
    // closes upload modal if needed
    if (partLoaded) {
      if (displayUpload) setDisplayUpload(undefined);
      if (saving) {
        setSaving(false);
        findParts();
      }
    }
  }, [partLoaded]);

  // Question : Why are we fetching customers and characteristics here, but not doing anything with them
  // To create the report? TODO: Check if that is the reason and move this data elsewhere so review can also access it
  const [getCustomer] = useLazyQuery(OrganizationAPI.query.find);

  const [getCharacteristics] = useLazyQuery<CharacteristicList>(Characteristics.query.list);

  const loadCustomer = () => {
    if (part && part.customer && part.customer.name) {
      getCustomer({
        variables: {
          id: part.customer.id,
        },
      });
    }
  };

  const loadCharacteristics = () => {
    if (part && part.id) {
      getCharacteristics({
        variables: {
          filter: { part: part.id, deletedAtRange: undefined },
          orderBy: 'markerIndex_ASC',
        },
      });
    }
  };

  const openDrawer = () => {
    if (part) {
      dispatch({ type: PartActions.SET_ORIGINAL_PART, payload: part });
      setDisplayDrawer(true);
    }
  };

  const checkSecurity = () => {
    if (part && accountMembership && accountMembership.accessControl && part.accessControl && part.accessControl !== 'None') {
      Analytics.track({
        event: Analytics.events.partRestricted,
        part,
        user,
      });
      history.push('/403');
    }
  };

  useEffect(() => {
    if (part?.id) {
      // Check if user has needed access control
      checkSecurity();
      // Loads customer
      loadCustomer();
      // Loads characteristics
      loadCharacteristics();
      // Open drawer when redirecting from Report Creation
      openDrawer();
    } else {
      setDisplayDrawer(false);
    }
  }, [part?.customer, part?.id]);

  // ------------------ Subscription Changes for Drawing Based Subscriptions ------------------

  const [visibility, setVisibility] = useState<boolean>(false);

  // Increment Used Drawings and prompt upgrade if the used number exceeds the number purchased
  const increaseDrawings = (amount: number) => {
    if (!subscription || type !== 'Freemium') {
      log.warn('Did not change drawings for non-freemium subscription');
      return;
    }
    const newTotal = subscription.drawingsUsed ? subscription.drawingsUsed + amount : amount;
    const cat = catalog && subscription.providerFamilyHandle ? catalog[subscription.providerFamilyHandle]?.components : {};
    if (!canAddDrawings(subscription, components, cat, amount)) {
      setVisibility(true);
      return;
    }
    const updatedSubscription = SubscriptionAPI.createGraphqlObject({
      ...subscription,
      drawingsUsed: newTotal,
    });
    dispatch(doUpdateCurrentSubscription(subscription.id, updatedSubscription, 'Update_Subscription'));
  };

  // Decrement Used Drawings
  const decreaseDrawings = (amount: number) => {
    if (!subscription || type !== 'Freemium') {
      log.warn('Did not change drawings for non-freemium subscription');
      return;
    }
    let newTotal = subscription.drawingsUsed ? subscription.drawingsUsed - amount : 0;
    if (newTotal < 0) {
      log.error('Used drawings should not descend below zero');
      newTotal = 0;
    }
    const updatedSubscription = SubscriptionAPI.createGraphqlObject({
      ...subscription,
      drawingsUsed: newTotal,
    });
    dispatch(doUpdateCurrentSubscription(subscription.id, updatedSubscription, 'Update_Subscription'));
  };

  const changeComponents = (sub: Subscription, componentInputs: ComponentInput[]) => {
    const input = { ...sub };
    const newComponentInputs: ComponentInput[] = [];
    const pending: Component[] = [];
    componentInputs.forEach((comp) => {
      const original = components.find((c) => c.component_id === comp.component_id);
      if (original && original.allocated_quantity > comp.quantity) {
        pending.push({ ...original, allocated_quantity: comp.quantity });
      } else if (original && original.allocated_quantity < comp.quantity) {
        newComponentInputs.push({ ...comp });
      }
    });

    if (pending) {
      input.pendingComponents = JSON.stringify(pending);
    }
    return { input, newComponentInputs };
  };

  const handleSubscriptionChange = ({
    //
    price,
    componentInputs,
    due,
  }: {
    price?: number;
    componentInputs?: ComponentInput[];
    due?: number;
  }) => {
    if (!subscription) {
      return;
    }
    let input = { ...subscription };
    if (price !== undefined) {
      if (input.currentBillingAmountInCents && price < input.currentBillingAmountInCents) {
        input.pendingBillingAmountInCents = price;
      } else {
        input.currentBillingAmountInCents = price;
        input.pendingBillingAmountInCents = null;
      }
    }
    let inputs: ComponentInput[] = [];
    if (componentInputs) {
      const compUpdate = changeComponents(input, componentInputs);
      input = compUpdate.input;
      inputs = compUpdate.newComponentInputs;
    }
    const newData = SubscriptionAPI.createGraphqlObject(input);

    dispatch(doUpdateCurrentSubscription(subscription.id, newData, 'Update_Subscription', inputs));
  };

  // ------------------ Methods used by PartsTable ---------------------
  const fetchParts = (amount: number, offset: number) => {
    if (fetchMore) {
      fetchMore({
        variables: {
          offset,
          limit: amount,
        },
        updateQuery: (prev, { fetchMoreResult }) => {
          if (!fetchMoreResult || !prev.partList) return prev;
          return {
            ...prev,
            partList: {
              count: prev.partList.count,
              rows: [...prev.partList.rows, ...fetchMoreResult.partList.rows],
            },
          };
        },
      });
    }
  };

  // / Pagination of table records
  const handlePagination = (pageNumber: number) => {
    // Set page number to send with report
    dispatch(doSetTablePage(pageNumber));
    if (!data || !data.partList.rows) {
      return;
    }

    // If large part jump, seperate calls to not crash AWS
    const pageDiff = pageNumber - tablePage;
    const partsPerPage = 10;
    const pageLimit = 50;

    if (pageNumber * partsPerPage > data.partList.rows.length && pageDiff > 0) {
      if (pageDiff > pageLimit) {
        for (let x = 0; x * pageLimit < pageDiff; x++) {
          fetchParts(pageLimit * partsPerPage, data.partList.rows.length + x * partsPerPage);
        }
      } else {
        fetchParts(pageNumber * partsPerPage - data.partList.rows.length, data.partList.rows.length);
      }
    }
  };

  // Handle change of anything on the table
  const handleChange = (pagination: TablePaginationConfig, filters: any, sorter: any, extra: any) => {
    if (extra.action === 'paginate') {
      handlePagination(pagination.current || 1);
    } else if (extra.action === 'filter') {
      handlePagination(1);
      dispatch(doSetTableFilter(filters));
    } else if (extra.action === 'sort') {
      handlePagination(1);
      if (!sorter?.order) {
        dispatch(doSetTableSorter(null));
      } else {
        dispatch(doSetTableSorter({ columnKey: sorter.columnKey, order: sorter.order }));
      }
    }
  };

  // Handle Part Revision
  const handlePartRevision = (targetPart: Part) => {
    dispatch(doAddPartRevisions(targetPart, history));
  };

  // Handle click of extract icon's in each row in the table

  const handlePartExtract = (targetPart: Part, source: string) => {
    if (part && !partLoaded) return;
    Analytics.track({
      event: Analytics.events.userClickedExtract,
      part: targetPart,
      properties: {
        source,
      },
    });
    dispatch(doSetPart(targetPart, part));
    history.push(`/parts/${targetPart.id}`);
    dispatch({ type: NavigationActions.SET_CURRENT_PART_STEP, payload: PartStepEnum.EXTRACT });
  };

  // Opens the part details drawer
  const handlePartTableRowClick = (selectedPart: Part | string) => {
    if (part && !partLoaded) return;
    Analytics.track({
      event: Analytics.events.partRowClicked,
      part: selectedPart,
      // properties: {},
    });
    setDisplayUpload(undefined);

    const partId = typeof selectedPart === 'string' ? selectedPart : selectedPart.id;
    const originalPartId = typeof selectedPart === 'string' ? part?.originalPart : selectedPart.originalPart;

    if (previousPart?.id === partId && part?.id !== partId) {
      dispatch(doSetPart(previousPart, originalPart));
      if (originalPartId) dispatch(doGetPartRevisions(originalPartId));
    } else {
      dispatch(doGetPart(partId));
    }
  };

  const handleChangeRevision = (selectedPart: Part | string) => {
    handlePartTableRowClick(selectedPart);
    Analytics.track({
      event: Analytics.events.partRevisionSelected,
      part: selectedPart,
    });
  };

  // ----- Methods used by PartDetailsDrawer ---------------------

  // Handle close event (from inside or outside the drawer)
  const handleDrawerClose = () => {
    if (partLoaded) {
      setDisplayDrawer(false);
      setDisplayUpload(undefined);
      setDrawerUploadOpen(false);
      dispatch(doSetPart(null, null));
    }
  };

  // Handle click of delete icon's in each row in the table
  const handlePartDelete = async (passedPart: Part, source: string) => {
    Analytics.track({
      event: Analytics.events.userClickedDeletePart,
      part: passedPart,
      properties: {
        source,
      },
    });
    // Delete Part
    dispatch(doDeletePart(passedPart, refetch, partListQueryParams));

    // Reload Table
    partUpdateHandler();
    setDisplayDrawer(false);
  };

  // Handle changes on EditInPlace text inputs in the drawer
  const handlePartFieldChange = (event: ChangeEvent<HTMLTextAreaElement>) => {
    Analytics.track({
      event: Analytics.events.partEditedFromDrawer,
      part,
      properties: {
        'field.name': event.target.name,
        'field.from': part![event.target.name],
        'field.to': event.target.value,
      },
    });
    const changes = { [event.target.name]: event.target.value };
    if (part) {
      setSaving(true);
      dispatch(updatePartThunk(changes, part.id));
    }
  };

  // only run update if state is still dirty
  const handleDebouncedPartUpdate = (e: any) => {
    if (e && e.target) {
      handlePartFieldChange(e);
    }
  };

  const debouncedUpdate = useCallback(debounce(handleDebouncedPartUpdate, 1000, { trailing: true }), [part]);

  const onValueChange = (e: any) => {
    e.persist();
    if (partLoaded) dispatch({ type: PartActions.SET_PART_LOADED, payload: false });
    if (!saving) debouncedUpdate(e);
  };

  // Handle blur (when a user leaves an input field) on EditInPlace text inputs in the drawer
  const handlePartFieldBlur = async (event: ChangeEvent<HTMLTextAreaElement>) => {
    if (!part || !originalPart || originalPart[event.target.name] === event.target.value) {
      // no need to edit record if original value is the same as current value
      return;
    }

    Analytics.track({
      event: Analytics.events.partEditedFromDrawer,
      part,
      properties: {
        'field.name': event.target.name,
        'field.from': part![event.target.name],
        'field.to': event.target.value,
      },
    });
    const change = { [event.target.name]: event.target.value };
    dispatch(updatePartThunk(change, part.id));
  };

  // Handle changes of the CustomerSelect field in the drawer
  const handleCustomerChange = async (value: string) => {
    if (!part) {
      log.warn('no part set');
      return;
    }

    Analytics.track({
      event: Analytics.events.partEditedFromDrawer,
      part,
      properties: {
        'field.name': 'customer',
        'field.from': part.customer,
        'field.to': value,
      },
    });

    setSaving(true);
    dispatch(updatePartThunk({ customer: value }, part.id));
  };

  // Handle changes of the Part Status select field in the drawer
  const handleReviewStatusChange = async (newReviewStatus: PartReviewStatusEnum) => {
    if (!part) {
      return;
    }
    Analytics.track({
      event: Analytics.events.partEditedFromDrawer,
      part,
      properties: {
        'field.name': 'reviewStatus',
        'field.from': part.reviewStatus,
        'field.to': newReviewStatus,
      },
    });

    setSaving(true);
    dispatch(updatePartThunk({ reviewStatus: newReviewStatus }, part.id));
  };

  // Handle delete drawing

  const handleDeleteDrawing = (drawing: Drawing) => {
    if (part && drawing.characteristics && drawing.characteristics.length > 0) {
      dispatch({
        type: CharacteristicActions.DELETE_CHARACTERISTICS,
        payload: {
          ids: drawing.characteristics.map((char) => char.id),
          part: part.id,
        },
      });
    }
    dispatch(deleteDrawingThunk(drawing));
    decreaseDrawings(1);
  };

  const handleDeleteAttachment = (attachment: File) => {
    dispatch(deleteAttachmentThunk(attachment));
  };

  const handleDeleteFile = (document: Drawing | File, fileType: 'Drawing' | 'Attachment' | 'Report') => {
    switch (fileType) {
      case 'Drawing':
        handleDeleteDrawing(document as Drawing);
        break;
      case 'Attachment':
        handleDeleteAttachment(document as File);
        break;
      default:
        break;
    }
  };

  const handleDeleteReport = (report: ReportPartial) => {
    const change = ReportAPI.createGraphqlInput({ ...(report as any), part: part || undefined, status: 'Deleted' });
    dispatch(doDeleteReport({ id: report.id, report: change }));
  };

  // Download a file
  const handleFileDownload = async (publicUrl: string, fileName: string, fileType?: string, partId?: string, drawingId?: string) => {
    let file;
    if (fileType) {
      file = await convertFile(publicUrl, fileType);
    } else if (partId && drawingId) {
      file = await downloadBalloonedFile(publicUrl, partId, drawingId);
    } else {
      file = await downloadFile(publicUrl);
    }
    Analytics.track({
      event: Analytics.events.userClickedDownloadFile,
      part,
      properties: {
        'file.name': fileName,
      },
    });
    if (file) {
      saveAs(file, fileName);
    }
  };

  const handleSearchChange = (searchText: string) => {
    // always returning results on page one
    handlePagination(1);
    setPartListQueryParams({
      ...partListQueryParams,
      filter: {
        ...partListQueryParams.filter,
        searchTerm: searchText,
      },
    });
    if (refetch) {
      refetch({
        variables: {
          ...partListQueryParams,
          filter: {
            ...partListQueryParams.filter,
            searchTerm: searchText,
          },
        },
        fetchPolicy: 'no-cache',
      });
    }
  };

  const debouncedHandleSearchChange = debounce(handleSearchChange, 500, {
    trailing: true,
  });
  // ----- Methods for New Part Button ----------------------------

  // Handle drop outside of drop zone by preventing default browser file upload
  const handleDropOutOfBounds = (e: any) => {
    e.preventDefault();
  };

  const canAddDrawing = () => {
    const cat = catalog && subscription?.providerFamilyHandle ? catalog[subscription.providerFamilyHandle]?.components : {};
    if (subscription && !canAddDrawings(subscription, components, cat)) {
      setVisibility(true);
      Analytics.track({
        event: Analytics.events.subscriptionDrawingsChangeOpen,
        properties: {
          'subscription.period': subscription?.billingPeriod,
          'subscription.tier': subscription?.providerProductId,
          'subscription.drawings.used': subscription?.drawingsUsed,
        },
      });
      return false;
    }
    return true;
  };

  // Handle click of New Part button
  const handleNewPart = useCallback(() => {
    if (!canAddDrawing()) return;
    if (!displayUpload && !drawerUploadOpen) {
      setDisplayUpload('pdf');
      shouldCreatePart.current = true;
    }
  }, [subscription, catalog, displayUpload, drawerUploadOpen]);

  const handleClickAddPart = () => {
    Analytics.track({ event: Analytics.events.userClickedAddPart });
    shouldCreatePart.current = true;
    handleNewPart();
  };

  const handleAddFile = () => {
    Analytics.track({ event: Analytics.events.userClickedAddDrawing });
    if (canAddDrawing()) {
      shouldCreatePart.current = false;
      setDisplayUpload('pdf');
    }
  };

  // store reference to current dragover event to make sure correct ref is passed to add/removeEventListener
  const callbackRef = useRef<() => void | undefined>();

  // Handle adding/removing event listeners for drag/drop/upload of files
  useEffect(() => {
    if (!displayUpload && !drawerUploadOpen) {
      callbackRef.current = handleNewPart;
      document.addEventListener('dragover', callbackRef.current);
      document.removeEventListener('drop', handleDropOutOfBounds);
      document.removeEventListener('dragover', handleDropOutOfBounds);
    } else {
      document.addEventListener('drop', handleDropOutOfBounds);
      document.addEventListener('dragover', handleDropOutOfBounds);
    }

    return function cleanup() {
      if (callbackRef.current) document.removeEventListener('dragover', callbackRef.current);
      document.removeEventListener('drop', handleDropOutOfBounds);
      document.removeEventListener('dragover', handleDropOutOfBounds);
    };
  }, [handleNewPart]);

  // Refetches Part List data after leading the part wizard
  useEffect(() => {
    if (doRefetch.current && displayDrawer) partUpdateHandler();
  }, [doRefetch.current]);

  // ----- Methods used by PartUpload drag & drop modal -----------

  const handleViewNewPart = () => {
    if (newPart) {
      setDisplayContinue(false);
      steps.set(0, 0);
      history.push(`/parts/${newPart.id}`);
      dispatch({ type: PartActions.SET_NEW_PART, payload: null });
    }
  };

  const handleNewAttachment = () => {
    Analytics.track({ event: Analytics.events.userClickedAddAttachment });
    setDisplayUpload('any');
  };

  useEffect(() => {
    if (newPart?.id && user) {
      setDisplayUpload(undefined);
      // Reload Subscription Metrics
      dispatch(doSetSubscription(user.accountId));

      if (settings.autoballoonEnabled?.value === 'true' && newPart.ocr) {
        // Show new modal
        setDisplayContinue(true);
      } else if (newPart) {
        handleViewNewPart();
      }

      if (doRefetch.current) {
        doRefetch.current({
          variables: {
            ...partListQueryParams,
          },
          fetchPolicy: 'no-cache',
        });
      }
    }
  }, [newPart?.id]);

  const handleWaitForNewPart = () => {
    setDisplayContinue(false);
    dispatch({ type: PartActions.SET_NEW_PART, payload: null });
  };

  // Handle the upload of a part drawing
  const handlePartDrawingUpload = async (files: FileObject[]) => {
    if (displayUpload === 'any') {
      dispatch(uploadAttachmentsThunk(files));
    } else {
      // Check if the subscription can support a new drawing
      increaseDrawings(files.length);

      if (shouldCreatePart.current) {
        dispatch({ type: PartActions.PART_RESET });
        dispatch({ type: PartEditorActions.RESET_PART_EDITOR });
        dispatch({ type: NavigationActions.RESET });
      }

      // Call the upload drawing thunk to create the drawing and add it to a part or create a new part
      dispatch(uploadDrawingThunk(files, shouldCreatePart.current));
    }
  };

  // Event handler for cancellation of New Part
  const handleCancelPart = () => {
    Analytics.track({
      event: Analytics.events.partDrawingUploadedCanceled,
      part,
      // properties: {},
    });
    setDisplayUpload(undefined);
  };

  const generateZip = async (thisPart: Part) => {
    if (!thisPart) {
      return;
    }
    setSaving(true);
    const drawings = thisPart?.drawings
      ? thisPart.drawings.map((item, index) => {
          return {
            url: (item?.file[0] || item?.drawingFile).privateUrl,
            name: item.name || `Drawing ${index}`,
          };
        })
      : [];
    const reports = thisPart?.reports
      ? thisPart.reports.map((item, index) => {
          return {
            url: (item?.file[0] || item?.drawingFile).privateUrl,
            name: item.title || `Report ${index}`,
          };
        })
      : [];

    const filename = `${thisPart.number}${thisPart.revision}_all-ix-files.zip`;
    axios({
      method: 'post',
      url: `${config.backendUrl}/report`,
      data: {
        partId: thisPart.id,
        filename,
        drawingURL: thisPart.primaryDrawing.file[0].privateUrl,
        drawings,
        reports,
      },
      responseType: 'blob',
      headers: {
        authorization: jwt ? `Bearer ${jwt}` : '',
      },
    })
      .then((response) => {
        saveAs(response.data, filename);
      })
      .then(() => {
        notification.success({
          message: i18n('files.localDownload.success'),
        });
        setSaving(false);
      })
      .catch((err) => {
        log.error('PartsList.generateZip error', err);
        notification.error({
          message: i18n('files.localDownload.error'),
          description: err.message,
        });
        setSaving(false);
      });
  };

  useEffect(() => {
    if (subscriptionError) {
      setError(subscriptionError);
    } else if (partError) {
      setError(partError);
    } else if (partListError) {
      setError(partListError);
    } else {
      setError(undefined);
    }
    setDisplayUpload(undefined);
  }, [subscriptionError, partError]);

  const closeError = () => {
    setError(undefined);
    dispatch({ type: PartActions.SET_PART_ERROR, payload: null });
  };

  const limit = shouldCreatePart.current || displayUpload !== 'pdf' ? 10 : 10 - (part?.drawings?.length || 0);

  return (
    <main id="partsList" className="parts-list">
      <Loader queries={[subscriptionLoading, loading, !partLoaded && !displayDrawer]} loading={undefined} error={error} closeError={closeError} />
      <PartDetailsDrawer
        parentSelector="main#partsList"
        isOpen={displayDrawer}
        handleDrawerClose={handleDrawerClose}
        handlePartFieldChange={onValueChange}
        handlePartFieldBlur={handlePartFieldBlur}
        handleChangeRevision={handleChangeRevision}
        handlePartRevision={handlePartRevision}
        handleCustomerChange={handleCustomerChange}
        handleReviewStatusChange={handleReviewStatusChange}
        handlePartExtract={handlePartExtract}
        handlePartDelete={handlePartDelete}
        addFile={handleAddFile}
        addAttachment={handleNewAttachment}
        removeFile={handleDeleteFile}
        downloadAll={generateZip}
        downloadFile={handleFileDownload}
        deleteReport={handleDeleteReport}
        saving={saving}
        limit={limit}
        handleDrawerUploadOpened={() => setDrawerUploadOpen(true)}
        handleDrawerUploadClosed={() => setDrawerUploadOpen(false)}
      />
      <div className="wrapper">
        {subscription && <DrawingLimitModal visibility={visibility} setVisibility={setVisibility} handleSubscriptionChange={handleSubscriptionChange} />}
        {createPartsPermission && (
          <UploadModal //
            isDisplayed={!!displayUpload}
            handleUpload={handlePartDrawingUpload}
            handleCancel={handleCancelPart}
            fileType={displayUpload || 'pdf'}
            fileExt={displayUpload === 'any' ? displayUpload : '.pdf'}
            hint={displayUpload === 'pdf' ? 'PDF' : 'Attachment (.pdf, .xlsx, .doc, .txt, .csv)'}
            entity={i18n('entities.part.name')}
            limit={limit}
            type={displayUpload === 'pdf' ? 'Drawing' : 'Attachment'}
          />
        )}
        <Modal
          onCancel={handleWaitForNewPart}
          onOk={handleViewNewPart}
          visible={displayContinue}
          title={i18n('entities.part.autoballoonModal.title')}
          className="general-modal part-continue-modal"
          footer={
            // eslint-disable-next-line react/jsx-wrap-multilines
            <div className="custom-modal-footer">
              <Button size="large" onClick={handleViewNewPart}>
                {i18n('entities.part.autoballoonModal.ok')}
              </Button>
              <Button size="large" onClick={handleWaitForNewPart} className="btn-primary">
                {i18n('entities.part.autoballoonModal.cancel')}
              </Button>
            </div>
          }
        >
          {i18n('entities.part.autoballoonModal.body')}
        </Modal>
        <Row>
          <Col span={24}>
            <h1>{i18n('entities.part.list.title')}</h1>
          </Col>
        </Row>
        <Row className={displayDrawer ? 'drawer-open' : ''}>
          {createPartsPermission && (
            <Col span={3}>
              <Button size="large" className="btn-primary" id="NewPart-btn" style={{ marginBottom: '15px' }} onClick={handleClickAddPart}>
                <PlusSquareOutlined style={{ marginRight: '5px' }} />
                {i18n('entities.part.new.title')}
              </Button>
            </Col>
          )}

          <Col span={displayDrawer ? 12 : 8} offset={createPartsPermission ? (displayDrawer ? 9 : 13) : displayDrawer ? 12 : 16}>
            <Input size="large" id="part-search" placeholder={i18n('entities.part.search')} onChange={(e: any) => debouncedHandleSearchChange(e.target.value)} prefix={<SearchOutlined />} allowClear />
          </Col>
        </Row>
        <Row>
          <Col span={24}>
            <PartsTable
              data={data?.partList?.rows || []}
              pageCount={data ? data.partList.count : 0}
              handleChange={handleChange}
              handlePartDelete={handlePartDelete}
              handlePartExtract={handlePartExtract}
              handlePartRevision={handlePartRevision}
              handleRowClick={handlePartTableRowClick}
              displayDrawer={displayDrawer}
              loading={loading || saving}
            />
          </Col>
        </Row>
      </div>
    </main>
  );
};

export default PartsList;

// For performance testing
// PartsList.whyDidYouRender = {
//   logOnDifferentValues: false,
// };
