import { Drawing } from 'graphql/drawing';
import { File } from 'types/file';
import { Characteristic } from 'types/characteristics';
import { Organization } from 'graphql/organization';
import { ReportPartial } from 'types/reportTemplates';

export type PartTypeEnum = 'Part' | 'Assembly';
export type PartStatusEnum = 'Active' | 'Inactive' | 'Deleted';
export type PartAccessControlEnum = 'None' | 'ITAR';

export type PartMeasurementEnum = 'Metric' | 'Imperial';
export type PartReviewStatusEnum = 'Unverified' | 'Ready_for_Review' | 'Verified';
export type PartAutoMarkupStatusEnum = 'In_Progress' | 'Completed' | 'Error';

// is there a better way to do this? I'm blanking
export const PartReviewStatusValues = ['Unverified', 'Ready_for_Review', 'Verified'];
export const PartAccessControlValues = ['None', 'ITAR'];

// TODO: Why isn't this in the ../../types/customer

// The default value of a Part when its first being created in the DB
export const newPartDefaults = {
  // Required fields
  primaryDrawing: '',
  workflowStage: 0,
  type: 'Part', // Part | Assembly
  status: 'Active', // Active | Inactive | Deleted
  reviewStatus: 'Unverified', // Unverified | Ready_to_Verify | Verified
  accessControl: 'None', // None | ITAR
  measurement: 'Imperial', // Imperial | Metric

  // Required fields that should be populated from user or user.site defaults
  defaultMarkerOptions: null, // TODO: allow nulls
  defaultLinearTolerances: null, // TODO: allow nulls
  defaultAngularTolerances: null, // TODO: allow nulls
  presets: null,

  // Details added during stage 1
  name: null,
  number: null,
  revision: null,
  drawingNumber: null,
  drawingRevision: null,
  customer: '', // Required, but shouldn't be
  purchaseOrderCode: null,
  purchaseOrderLink: null,
  notes: null,
  tags: null,

  // Collections populated by Sequelize
  drawings: [],
  characteristics: [],
  reports: [],

  workflowName: null, // TODO: Unused - Remove???

  // Milestone events
  // createdAt: null,
  // updatedAt: null,
  completedAt: null,
  balloonedAt: null,
  reportGeneratedAt: null,

  // Net-Inspect FAI Report ID
  lastReportId: null,

  displayLeaderLines: true,
};

// The schema of Part objects when returned from the DB
export interface Part {
  // Required fields
  id: string;
  primaryDrawing: Drawing;
  workflowStage: number;
  type: PartTypeEnum;
  status: PartStatusEnum;
  reviewStatus: PartReviewStatusEnum;
  autoMarkupStatus: null | PartAutoMarkupStatusEnum;
  accessControl: PartAccessControlEnum;
  measurement: PartMeasurementEnum;

  // Required fields that should be populated from user or user.site defaults
  defaultMarkerOptions: null | string;
  defaultLinearTolerances: null | string;
  defaultAngularTolerances: null | string;
  presets: null | string;

  // Details added during stage 1
  name: null | string;
  number: null | string;
  revision: null | string;
  drawingName: null | string;
  drawingNumber: null | string;
  drawingRevision: null | string;
  customer: null | Organization;
  purchaseOrderCode: null | string;
  purchaseOrderLink: null | string;
  notes: null | string;
  tags: null | string;

  // Collections populated by Sequelize
  drawings?: Drawing[];
  characteristics?: Characteristic[];
  reports?: ReportPartial[];
  attachments?: File[];

  workflowName: null | string; // TODO: Unused - Remove???

  // Milestone events
  createdAt: null | Date;
  updatedAt: null | Date;
  completedAt: null | Date;
  balloonedAt: null | Date;
  reportGeneratedAt: null | Date;

  // Net-Inspect FAI Report ID
  lastReportId: string | null;

  // leader lines display
  displayLeaderLines?: boolean;

  // Revisions
  originalPart: string;
  previousRevision?: string | null;
  nextRevision?: string | null;

  [key: string]: Organization | Characteristic[] | Drawing[] | File[] | Drawing | ReportPartial[] | PartAccessControlEnum | PartMeasurementEnum | PartTypeEnum | Date | string | number | boolean | null | undefined;
}

export interface PartAutocomplete {
  id: string;
  label: string;
}

export interface PartRevision {
  id: string;
  revision?: string | null;
  name?: string | null;
  number?: string | null;
  drawings: {
    [key: string]: {
      id: string;
      name?: string;
      number?: string;
      revision: string;
      previousRevision?: string | null;
      nextRevision?: string | null;
      createdAt: Date | null;
    };
  };
  previousRevision?: string | null;
  nextRevision?: string | null;
  createdAt: Date | null;
}

export interface PartTableFilter {
  [key: string]: Array<string>;
}

export interface PartTableSorter {
  columnKey?: string | undefined;
  order?: 'descend' | 'ascend' | null | undefined;
}

// The PartChange type represents a change to one or more values
// which can be used to construct a valid PartUpdateInput (see below)
// All fields are optional to allow partial updates without implicit assignment of undefined
export interface PartChange {
  // Required fields
  primaryDrawing?: string; // Drawing.id
  workflowStage?: number;
  type?: PartTypeEnum;
  status?: PartStatusEnum;
  reviewStatus?: PartReviewStatusEnum;
  accessControl?: string; // PartAccessControlEnum;
  measurement?: string; // PartMeasurementEnum;

  // Required fields that should be populated from user or user.site defaults
  defaultMarkerOptions?: string;
  defaultLinearTolerances?: string;
  defaultAngularTolerances?: string;
  presets?: string;

  // Details added during stage 1
  name?: string;
  number?: string;
  revision?: string;
  drawingName?: string;
  drawingNumber?: string;
  drawingRevision?: string;
  customer?: string; // Customer.id
  purchaseOrderCode?: string;
  purchaseOrderLink?: string;
  notes?: string;
  tags?: string;

  // Collections populated by Sequalize
  drawings?: string[]; // Drawing.id
  characteristics?: string[]; // Characteristic.id
  reports?: string[]; // ReportPartial.id

  // Milestone events
  completedAt?: Date;
  balloonedAt?: Date;
  reportGeneratedAt?: Date;

  // Net-Inspect FAI Report ID
  lastReportId?: string;

  displayLeaderLines?: boolean;
  [key: string]: string | string[] | boolean | number | PartAccessControlEnum | Date | PartTypeEnum | undefined;
}

// This is the 'PartInput' type which is expected by the backend's update method's
// The backend's 'PartInput' is at backend/src/api/part/types/partInput.js
export interface PartUpdateInput {
  // Required fields
  primaryDrawing: string; // Drawing.id
  workflowStage: number;
  type: PartTypeEnum;
  status: PartStatusEnum;
  reviewStatus: PartReviewStatusEnum;
  accessControl: PartAccessControlEnum;
  measurement: PartMeasurementEnum;

  // Required fields that should be populated from user or user.site defaults
  defaultMarkerOptions: null | string;
  defaultLinearTolerances: null | string;
  defaultAngularTolerances: null | string;
  presets: null | string;

  // Details added during stage 1
  name: null | string;
  number: null | string;
  revision: null | string;
  drawingName: null | string;
  drawingNumber: null | string;
  drawingRevision: null | string;
  customer: null | string; // Customer.id
  purchaseOrderCode?: null | string;
  purchaseOrderLink?: null | string;
  notes: null | string;
  tags: null | string;

  // Collections populated by Sequalize
  drawings: string[]; // Drawing.id
  characteristics: string[]; // Characteristic.id
  reports: string[]; // ReportPartial.id
  attachments: File[] | undefined;

  // Milestone events
  completedAt: null | Date;
  balloonedAt: null | Date;
  reportGeneratedAt: null | Date;

  // Net-Inspect FAI Report ID
  lastReportId: null | string;

  // leader lines display
  displayLeaderLines?: boolean;

  // Revision
  originalPart: string | null;
  previousRevision?: string | null;
  nextRevision?: string | null;

  [key: string]: Date | number | boolean | string | string[] | File[] | PartAccessControlEnum | PartTypeEnum | undefined | null;
}
