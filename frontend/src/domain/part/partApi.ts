import gql from 'graphql-tag';
// import getClient from 'modules/shared/graphql/graphqlClient';
import moment from 'moment';
import log from 'modules/shared/logger';
import { Part, PartAccessControlEnum, PartAutoMarkupStatusEnum, PartAutocomplete, PartMeasurementEnum, PartReviewStatusEnum, PartStatusEnum, PartTypeEnum, PartUpdateInput } from 'domain/part/partTypes';
import { Drawing } from 'graphql/drawing';
import { ReportPartial, ReportData } from 'types/reportTemplates';
import { Characteristic, CharacteristicData } from 'types/characteristics';
import { Organization } from 'graphql/organization';
import { getListNameById, getSingleLevelListValue } from 'utils/Lists';
import { ListObject } from 'domain/setting/listsTypes';
import { DrawingSheet } from 'graphql/drawing_sheet';
import { getCurrentDrawings } from 'modules/characteristic/characteristicActions';

// Type definitions for the returned api data
export interface PartCreate {
  partCreate: Part;
}
export interface PartCreateVars {
  data: PartUpdateInput;
}

export interface PartFind {
  partFind: Part;
  drawingList: { rows: Drawing[] };
  reportList: {
    rows: ReportPartial[];
  };
}
export interface PartRevisionBE {
  partRevision: {
    part: Part;
    drawings: Drawing[];
    sheets: DrawingSheet[];
    characteristics: Characteristic[];
  };
}
export interface PartFindVars {
  id: string;
}

export interface PartListAutocomplete {
  partAutocomplete: PartAutocomplete[];
}
export interface PartList {
  partList: {
    count: number;
    rows: Part[];
  };
}

export interface PartListShallow {
  partListShallow: {
    count: number;
    rows: {
      id: string;
      name?: string;
      number?: string;
      revision?: string;
      characteristics?: boolean;
      createdAt: null | Date;
      updatedAt: null | Date;
    }[];
  };
}

export interface PartListVars {
  filter?: {
    id?: string;
    previousRevision?: string | null;
    nextRevision?: string | null;
    originalPart?: string;
    purchaseOrderCode?: string;
    purchaseOrderLink?: string;
    type?: PartTypeEnum;
    name?: string;
    number?: string;
    revision?: string;
    drawingNumber?: string;
    drawingRevision?: string;
    defaultMarkerOptions?: string;
    defaultLinearTolerances?: string;
    defaultAngularTolerances?: string;
    presets?: string;
    status?: PartStatusEnum[];
    autoMarkupStatus?: PartAutoMarkupStatusEnum[];
    reviewStatus?: PartReviewStatusEnum[];
    workflowName?: string;
    workflowStage?: number;
    completedAtRange?: string[];
    balloonedAtRange?: Date[];
    reportGeneratedAtRange?: Date[];
    notes?: string;
    accessControl?: PartAccessControlEnum[];
    measurement?: PartMeasurementEnum;
    tags?: string;
    site?: string;
    customer?: string[];
    primaryDrawing?: string;
    createdAtRange?: Date[];
    searchTerm?: string;
  };
  limit?: number;
  offset?: number;
  orderBy?:
    | 'id_ASC'
    | 'id_DESC'
    | 'purchaseOrderCode_ASC'
    | 'purchaseOrderCode_DESC'
    | 'purchaseOrderLink_ASC'
    | 'purchaseOrderLink_DESC'
    | 'type_ASC'
    | 'type_DESC'
    | 'name_ASC'
    | 'name_DESC'
    | 'number_ASC'
    | 'number_DESC'
    | 'revision_ASC'
    | 'revision_DESC'
    | 'drawingNumber_ASC'
    | 'drawingNumber_DESC'
    | 'drawingRevision_ASC'
    | 'drawingRevision_DESC'
    | 'defaultAngularUnit_ASC'
    | 'defaultAngularUnit_DESC'
    | 'defaultMarkerOptions_ASC'
    | 'defaultMarkerOptions_DESC'
    | 'defaultLinearTolerances_ASC'
    | 'defaultLinearTolerances_DESC'
    | 'defaultAngularTolerances_ASC'
    | 'defaultAngularTolerances_DESC'
    | 'presets_ASC'
    | 'presets_DESC'
    | 'status_ASC'
    | 'status_DESC'
    | 'workflowStage_ASC'
    | 'workflowStage_DESC'
    | 'reviewStatus_ASC'
    | 'reviewStatus_DESC'
    | 'completedAt_ASC'
    | 'completedAt_DESC'
    | 'balloonedAt_ASC'
    | 'balloonedAt_DESC'
    | 'reportGeneratedAt_ASC'
    | 'reportGeneratedAt_DESC'
    | 'notes_ASC'
    | 'notes_DESC'
    | 'accessControl_ASC'
    | 'accessControl_DESC'
    | 'tags_ASC'
    | 'tags_DESC'
    | 'createdAt_ASC'
    | 'createdAt_DESC'
    | 'updatedAt_ASC'
    | 'updatedAt_DESC'
    | 'customer_name_ASC'
    | 'customer_name_DESC';
}

export interface PartEdit {
  partUpdate: Part;
}
export interface PartEditVars {
  id: string;
  data: PartUpdateInput;
}

// Using the gql strings allows for use of Apollo Client React Hooks
// as well as the option of passing cache updates or query refetch requests to our mutations

// I've made it an object here for more descriptive calls and shorter imports
// e.g. Parts.mutate.create is a mutation action to the Part database that creates something new

export interface PartDrawingFind {
  drawingFind: any; // TODO: Set to Drawing type
}

// TODO: Dedupe this from /frontend/src/graphql/drawing.ts
export const PartDrawing = {
  mutate: {
    edit: gql`
      mutation DRAWING_UPDATE($id: String!, $data: DrawingInput!) {
        drawingUpdate(id: $id, data: $data) {
          id
          part {
            id
            name
          }
          file {
            id
            name
            sizeInBytes
            publicUrl
            privateUrl
          }
          drawingFile {
            id
            sizeInBytes
            name
            publicUrl
            privateUrl
          }
          previousRevision
          nextRevision
          originalDrawing
          documentType
          name
          number
          revision
          linearUnit
          angularUnit
          markerOptions
          gridOptions
          linearTolerances
          tags
          angularTolerances
          sheetCount
          sheets {
            id
            previousRevision
            nextRevision
            originalSheet
            pageIndex
            pageName
            height
            width
            markerSize
            rotation
            grid
            useGrid
            displayLines
            displayLabels
            part {
              id
              name
            }
            drawing {
              id
              name
              number
            }
            characteristics {
              id
              previousRevision
              nextRevision
              originalCharacteristic
              markerLabel
              drawing {
                id
                name
                number
              }
            }
            foundCaptureCount
            acceptedCaptureCount
            acceptedTimeoutCaptureCount
            rejectedCaptureCount
            reviewedCaptureCount
            reviewedTimeoutCaptureCount
            defaultLinearTolerances
            defaultAngularTolerances
            createdAt
            updatedAt
          }
          characteristics {
            id
            previousRevision
            nextRevision
            originalCharacteristic
            markerLabel
            drawing {
              id
              name
              number
            }
          }
          createdAt
          updatedAt
        }
      }
    `,
    delete: gql`
      mutation DRAWING_DESTROY($ids: [String!]!) {
        drawingDestroy(ids: $ids)
      }
    `,
    create: gql`
      mutation DRAWING_CREATE($data: DrawingInput!) {
        drawingCreate(data: $data) {
          id
          file {
            id
            name
          }
        }
      }
    `,
  },
  query: {
    find: gql`
      query DRAWING_FIND($id: String!) {
        drawingFind(id: $id) {
          id
          part {
            id
            name
          }
          file {
            id
            name
            sizeInBytes
            publicUrl
            privateUrl
          }
          drawingFile {
            id
            name
            sizeInBytes
            publicUrl
            privateUrl
          }
          documentType
          name
          number
          revision
          linearUnit
          angularUnit
          markerOptions
          gridOptions
          linearTolerances
          tags
          angularTolerances
          sheetCount
          sheets {
            id
            previousRevision
            nextRevision
            originalSheet
            pageIndex
            pageName
            height
            width
            markerSize
            rotation
            grid
            useGrid
            displayLines
            displayLabels
            part {
              id
              name
            }
            drawing {
              id
              name
              number
            }
            characteristics {
              id
              previousRevision
              nextRevision
              originalCharacteristic
              markerLabel
              drawing {
                id
                name
                number
              }
            }
            foundCaptureCount
            acceptedCaptureCount
            acceptedTimeoutCaptureCount
            rejectedCaptureCount
            reviewedCaptureCount
            reviewedTimeoutCaptureCount
            defaultLinearTolerances
            defaultAngularTolerances
            createdAt
            updatedAt
          }
          characteristics {
            id
            previousRevision
            nextRevision
            originalCharacteristic
            markerLabel
            drawing {
              id
              name
              number
            }
          }
          createdAt
          updatedAt
        }
      }
    `,
    list: gql`
      query DRAWING_LIST($filter: DrawingFilterInput, $orderBy: DrawingOrderByEnum, $limit: Int, $offset: Int) {
        drawingList(filter: $filter, orderBy: $orderBy, limit: $limit, offset: $offset) {
          count
          rows {
            id
            part {
              id
              name
            }
            file {
              id
              name
              sizeInBytes
              publicUrl
              privateUrl
            }
            drawingFile {
              id
              sizeInBytes
              name
              publicUrl
              privateUrl
            }
            documentType
            sheetCount
            name
            number
            revision
            originalDrawing
            previousRevision
            nextRevision
            linearUnit
            angularUnit
            markerOptions
            gridOptions
            linearTolerances
            tags
            angularTolerances
            sheets {
              id
              previousRevision
              nextRevision
              originalSheet
              height
              width
              markerSize
              rotation
              grid
              useGrid
              displayLines
              displayLabels
              pageIndex
              pageName
              foundCaptureCount
              acceptedCaptureCount
              acceptedTimeoutCaptureCount
              rejectedCaptureCount
              reviewedCaptureCount
              reviewedTimeoutCaptureCount
              defaultLinearTolerances
              defaultAngularTolerances
            }
            characteristics {
              id
              previousRevision
              nextRevision
              originalCharacteristic
              markerLabel
              drawing {
                id
                name
                number
              }
            }
            updatedAt
            createdAt
          }
        }
      }
    `,
  },
};

// TODO: Dedupe this from /frontend/src/graphql/drawing_sheet.ts
export const PartDrawingSheet = {
  mutate: {
    edit: gql`
      mutation DRAWINGSHEET_UPDATE($id: String!, $data: DrawingSheetInput!) {
        drawingSheetUpdate(id: $id, data: $data) {
          id
          previousRevision
          nextRevision
          originalSheet
          pageName
          pageIndex
          height
          width
          markerSize
          rotation
          grid
          useGrid
          displayLines
          displayLabels
          part {
            id
            name
          }
          drawing {
            id
            name
            number
          }
          characteristics {
            id
            previousRevision
            nextRevision
            originalCharacteristic
            markerLabel
            drawing {
              id
              name
              number
            }
          }
          foundCaptureCount
          acceptedCaptureCount
          acceptedTimeoutCaptureCount
          rejectedCaptureCount
          reviewedCaptureCount
          reviewedTimeoutCaptureCount
          defaultLinearTolerances
          defaultAngularTolerances
          createdAt
          updatedAt
        }
      }
    `,
    delete: gql`
      mutation DRAWINGSHEET_DESTROY($ids: [String!]!) {
        drawingSheetDestroy(ids: $ids)
      }
    `,
    create: gql`
      mutation DRAWINGSHEET_CREATE($data: DrawingSheetInput!) {
        drawingSheetCreate(data: $data) {
          id
          previousRevision
          nextRevision
          originalSheet
        }
      }
    `,
  },
  query: {
    find: gql`
      query DRAWINGSHEET_FIND($id: String!) {
        drawingSheetFind(id: $id) {
          id
          previousRevision
          nextRevision
          originalSheet
          pageName
          pageIndex
          height
          width
          markerSize
          rotation
          grid
          useGrid
          displayLines
          displayLabels
          part {
            id
            name
          }
          drawing {
            id
            name
            number
          }
          characteristics {
            id
            previousRevision
            nextRevision
            originalCharacteristic
            markerLabel
            drawing {
              id
              name
              number
            }
          }
          foundCaptureCount
          acceptedCaptureCount
          acceptedTimeoutCaptureCount
          rejectedCaptureCount
          reviewedCaptureCount
          reviewedTimeoutCaptureCount
          defaultLinearTolerances
          defaultAngularTolerances
          createdAt
          updatedAt
        }
      }
    `,
    list: gql`
      query DRAWINGSHEET_LIST($filter: DrawingSheetFilterInput, $orderBy: DrawingSheetOrderByEnum, $limit: Int, $offset: Int) {
        drawingSheetList(filter: $filter, orderBy: $orderBy, limit: $limit, offset: $offset) {
          count
          rows {
            id
            previousRevision
            nextRevision
            originalSheet
            pageName
            pageIndex
            height
            width
            markerSize
            rotation
            grid
            useGrid
            displayLines
            displayLabels
            part {
              id
              name
            }
            drawing {
              id
              name
              number
            }
            characteristics {
              id
              previousRevision
              nextRevision
              originalCharacteristic
              markerLabel
              drawing {
                id
                name
                number
              }
            }
            foundCaptureCount
            acceptedCaptureCount
            acceptedTimeoutCaptureCount
            rejectedCaptureCount
            reviewedCaptureCount
            reviewedTimeoutCaptureCount
            defaultLinearTolerances
            defaultAngularTolerances
            updatedAt
            createdAt
          }
        }
      }
    `,
  },
};

// TODO: Delete this when NewPart context is ready
export const validatePartInfo = (input: any) => {
  // TODO: Convert to a Yup style thingy?
  // Main purpose is just to not update the DB when the part is in an invalid DB state
  // But still update the part so that inputs and stuff show correctly
  let valid = true;
  if ((!input.number || input.number === '') && input.status !== 'Deleted' && input.workflowStage !== 0) {
    log.warn('Missing Part Number');
    valid = false;
  }
  if ((!input.customer || input.customer === '') && input.customer !== null) {
    log.warn('Missing Customer ID');
    valid = false;
  }
  if (!input.drawings || input.drawings.length === 0) {
    log.warn('Part Must Have At Least One Drawing');
    valid = false;
  }
  if (!input.primaryDrawing || input.primaryDrawing === '') {
    log.warn('Missing Primary Drawing ID');
    valid = false;
  }
  return valid;
};

export const Parts = {
  mutate: {
    edit: gql`
      mutation PART_UPDATE($id: String!, $data: PartInput!) {
        partUpdate(id: $id, data: $data) {
          id
          customer {
            id
            name
          }
          purchaseOrderCode
          purchaseOrderLink
          type
          name
          number
          revision
          originalPart
          previousRevision
          nextRevision
          drawingNumber
          drawingRevision
          defaultMarkerOptions
          defaultLinearTolerances
          defaultAngularTolerances
          presets
          status
          autoMarkupStatus
          reviewStatus
          workflowStage
          completedAt
          balloonedAt
          reportGeneratedAt
          notes
          accessControl
          measurement
          tags
          displayLeaderLines
          primaryDrawing {
            id
            name
            number
            revision
            previousRevision
            nextRevision
            originalDrawing
            angularTolerances
            angularUnit
            linearTolerances
            linearUnit
            markerOptions
            sheetCount
            status
            tags
            createdAt
            updatedAt
            gridOptions
            file {
              id
              sizeInBytes
              name
              publicUrl
              privateUrl
            }
          }
          drawings {
            id
            name
            number
            revision
            previousRevision
            nextRevision
            originalDrawing
            linearUnit
            angularUnit
            markerOptions
            gridOptions
            linearTolerances
            tags
            angularTolerances
            sheetCount
            createdAt
            updatedAt
            gridOptions
            file {
              id
              sizeInBytes
              name
              publicUrl
              privateUrl
            }
            drawingFile {
              id
              sizeInBytes
              name
              publicUrl
              privateUrl
            }
          }
          reports {
            id
            title
            updatedAt
            file {
              name
              publicUrl
              privateUrl
            }
          }
          characteristics {
            id
            previousRevision
            nextRevision
            originalCharacteristic
            status
            captureMethod
            captureError
            drawingSheetIndex
            notationType
            notationSubtype
            notationClass
            fullSpecification
            quantity
            nominal
            upperSpecLimit
            lowerSpecLimit
            plusTol
            minusTol
            unit
            gdtSymbol
            gdtPrimaryToleranceZone
            gdtSecondaryToleranceZone
            gdtPrimaryDatum
            gdtSecondaryDatum
            gdtTertiaryDatum
            criticality
            inspectionMethod
            notes
            drawingRotation
            drawingScale
            boxLocationY
            boxLocationX
            boxWidth
            boxHeight
            boxRotation
            markerGroup
            markerGroupShared
            markerIndex
            markerSubIndex
            markerLabel
            markerStyle
            markerLocationX
            markerLocationY
            markerFontSize
            markerSize
            markerRotation
            gridCoordinates
            balloonGridCoordinates
            connectionPointGridCoordinates
            operation
            toleranceSource
            verified
            fullSpecificationFromOCR
            confidence
            connectionPointLocationX
            connectionPointLocationY
            connectionPointIsFloating
            displayLeaderLine
            leaderLineDistance
            drawing {
              id
              name
              number
            }
            drawingSheet {
              id
            }
          }
          attachments {
            id
            sizeInBytes
            name
            publicUrl
            privateUrl
          }
          createdAt
          updatedAt
        }
      }
    `,
    delete: gql`
      mutation PART_DELETE($id: String!) {
        partDelete(id: $id)
      }
    `,
    create: gql`
      mutation PART_CREATE($data: PartInput!) {
        partCreate(data: $data) {
          id
          customer {
            id
            name
          }
          purchaseOrderCode
          purchaseOrderLink
          type
          name
          number
          revision
          originalPart
          previousRevision
          nextRevision
          drawingNumber
          drawingRevision
          defaultMarkerOptions
          defaultLinearTolerances
          defaultAngularTolerances
          presets
          status
          autoMarkupStatus
          reviewStatus
          workflowStage
          completedAt
          balloonedAt
          reportGeneratedAt
          notes
          accessControl
          measurement
          tags
          displayLeaderLines
          primaryDrawing {
            id
            name
            number
            revision
            previousRevision
            nextRevision
            originalDrawing
            angularTolerances
            angularUnit
            linearTolerances
            linearUnit
            markerOptions
            sheetCount
            status
            tags
            createdAt
            updatedAt
            gridOptions
            file {
              id
              sizeInBytes
              name
              publicUrl
              privateUrl
            }
          }
          drawings {
            id
            name
            number
            revision
            previousRevision
            nextRevision
            originalDrawing
            linearUnit
            angularUnit
            markerOptions
            gridOptions
            linearTolerances
            tags
            angularTolerances
            sheetCount
            createdAt
            updatedAt
            gridOptions
            file {
              id
              sizeInBytes
              name
              publicUrl
              privateUrl
            }
            drawingFile {
              id
              sizeInBytes
              name
              publicUrl
              privateUrl
            }
          }
          reports {
            id
            title
            updatedAt
            file {
              name
              publicUrl
              privateUrl
            }
          }
          characteristics {
            id
            fullSpecification
            previousRevision
            nextRevision
            originalCharacteristic
            drawing {
              id
              name
              number
            }
          }
          attachments {
            id
            sizeInBytes
            name
            publicUrl
            privateUrl
          }
          createdAt
          updatedAt
        }
      }
    `,
    revise: gql`
      mutation PART_REVISION($id: String!) {
        partRevision(id: $id) {
          part {
            id
            customer {
              id
              name
            }
            purchaseOrderCode
            purchaseOrderLink
            type
            name
            number
            revision
            originalPart
            previousRevision
            nextRevision
            drawingNumber
            drawingRevision
            defaultMarkerOptions
            defaultLinearTolerances
            defaultAngularTolerances
            presets
            status
            autoMarkupStatus
            reviewStatus
            workflowStage
            completedAt
            balloonedAt
            reportGeneratedAt
            notes
            accessControl
            measurement
            tags
            displayLeaderLines
            primaryDrawing {
              id
              name
              number
              revision
              previousRevision
              nextRevision
              originalDrawing
              angularTolerances
              angularUnit
              linearTolerances
              linearUnit
              markerOptions
              sheetCount
              status
              tags
              createdAt
              updatedAt
              gridOptions
              file {
                id
                sizeInBytes
                name
                publicUrl
                privateUrl
              }
            }
            drawings {
              id
              name
              number
              revision
              previousRevision
              nextRevision
              originalDrawing
              linearUnit
              angularUnit
              markerOptions
              gridOptions
              linearTolerances
              tags
              angularTolerances
              sheets {
                id
                previousRevision
                nextRevision
                originalSheet
                pageIndex
                height
                width
                markerSize
                rotation
                grid
                useGrid
                displayLines
                displayLabels
                part {
                  id
                  name
                }
                drawing {
                  id
                  name
                }
                characteristics {
                  id
                  markerLabel
                  drawing {
                    id
                    name
                    number
                  }
                }
                foundCaptureCount
                acceptedCaptureCount
                acceptedTimeoutCaptureCount
                rejectedCaptureCount
                reviewedCaptureCount
                reviewedTimeoutCaptureCount
                createdAt
                updatedAt
              }
              sheetCount
              createdAt
              updatedAt
              gridOptions
              file {
                id
                sizeInBytes
                name
                publicUrl
                privateUrl
              }
              drawingFile {
                id
                sizeInBytes
                name
                publicUrl
                privateUrl
              }
            }
            reports {
              id
              title
              updatedAt
              file {
                name
                publicUrl
                privateUrl
              }
            }
            characteristics {
              id
              fullSpecification
              previousRevision
              nextRevision
              originalCharacteristic
              drawing {
                id
                name
                number
              }
            }
            attachments {
              id
              sizeInBytes
              name
              publicUrl
              privateUrl
            }
            createdAt
            updatedAt
          }
          drawings {
            id
            name
            number
            revision
            previousRevision
            nextRevision
            originalDrawing
            linearUnit
            angularUnit
            markerOptions
            gridOptions
            linearTolerances
            tags
            angularTolerances
            sheetCount
            createdAt
            updatedAt
            gridOptions
            file {
              id
              sizeInBytes
              name
              publicUrl
              privateUrl
            }
            drawingFile {
              id
              sizeInBytes
              name
              publicUrl
              privateUrl
            }
          }
          sheets {
            id
            height
            width
            markerSize
            rotation
            grid
            useGrid
            displayLines
            displayLabels
            previousRevision
            nextRevision
            originalSheet
            pageIndex
            pageName
            foundCaptureCount
            acceptedCaptureCount
            acceptedTimeoutCaptureCount
            rejectedCaptureCount
            reviewedCaptureCount
            reviewedTimeoutCaptureCount
            defaultLinearTolerances
            defaultAngularTolerances
            createdAt
            updatedAt
          }
          characteristics {
            id
            previousRevision
            nextRevision
            originalCharacteristic
            status
            captureMethod
            captureError
            drawingSheetIndex
            notationType
            notationSubtype
            notationClass
            fullSpecification
            quantity
            nominal
            upperSpecLimit
            lowerSpecLimit
            plusTol
            minusTol
            unit
            gdtSymbol
            gdtPrimaryToleranceZone
            gdtSecondaryToleranceZone
            gdtPrimaryDatum
            gdtSecondaryDatum
            gdtTertiaryDatum
            criticality
            inspectionMethod
            notes
            drawingRotation
            drawingScale
            boxLocationY
            boxLocationX
            boxWidth
            boxHeight
            boxRotation
            markerGroup
            markerGroupShared
            markerIndex
            markerSubIndex
            markerLabel
            markerStyle
            markerLocationX
            markerLocationY
            markerFontSize
            markerSize
            markerRotation
            gridCoordinates
            balloonGridCoordinates
            connectionPointGridCoordinates
            operation
            toleranceSource
            verified
            fullSpecificationFromOCR
            confidence
            connectionPointLocationX
            connectionPointLocationY
            connectionPointIsFloating
            displayLeaderLine
            leaderLineDistance
            drawing {
              id
              name
              number
            }
            drawingSheet {
              id
            }
          }
        }
      }
    `,
    import: gql`
      mutation PART_IMPORT($data: PartInput!, $importHash: string) {
        partImport(data: $data, importHash: $importHash)
      }
    `,
  },
  query: {
    step: gql`
      query step @client {
        currentStep
        maxStep
      }
    `,
    find: gql`
      query PART_FIND($id: String!) {
        partFind(id: $id) {
          id
          customer {
            id
            status
            settings
            name
            phone
            email
            street1
            street2
            city
            region
            postalCode
            country
          }
          purchaseOrderCode
          purchaseOrderLink
          type
          name
          number
          revision
          originalPart
          previousRevision
          nextRevision
          drawingNumber
          drawingRevision
          defaultMarkerOptions
          defaultLinearTolerances
          defaultAngularTolerances
          presets
          status
          autoMarkupStatus
          reviewStatus
          workflowStage
          completedAt
          balloonedAt
          reportGeneratedAt
          notes
          accessControl
          measurement
          tags
          displayLeaderLines
          primaryDrawing {
            id
            name
            number
            revision
            previousRevision
            nextRevision
            originalDrawing
            angularTolerances
            angularUnit
            linearTolerances
            linearUnit
            markerOptions
            sheetCount
            createdAt
            updatedAt
            gridOptions
            status
            documentType
            tags
            file {
              id
              sizeInBytes
              name
              publicUrl
              privateUrl
            }
            characteristics {
              id
              previousRevision
              nextRevision
              originalCharacteristic
              drawing {
                id
                name
                number
              }
            }
            part {
              id
            }
            sheets {
              id
              height
              width
              markerSize
              rotation
              grid
              useGrid
              displayLines
              displayLabels
              previousRevision
              nextRevision
              originalSheet
              pageIndex
              pageName
              foundCaptureCount
              acceptedCaptureCount
              acceptedTimeoutCaptureCount
              rejectedCaptureCount
              reviewedCaptureCount
              reviewedTimeoutCaptureCount
              defaultLinearTolerances
              defaultAngularTolerances
              createdAt
              updatedAt
            }
          }
          attachments {
            id
            sizeInBytes
            name
            publicUrl
            privateUrl
          }
          characteristics {
            id
            previousRevision
            nextRevision
            originalCharacteristic
            status
            captureMethod
            captureError
            drawingSheetIndex
            notationType
            notationSubtype
            notationClass
            fullSpecification
            quantity
            nominal
            upperSpecLimit
            lowerSpecLimit
            plusTol
            minusTol
            unit
            gdtSymbol
            gdtPrimaryToleranceZone
            gdtSecondaryToleranceZone
            gdtPrimaryDatum
            gdtSecondaryDatum
            gdtTertiaryDatum
            criticality
            inspectionMethod
            notes
            drawingRotation
            drawingScale
            boxLocationY
            boxLocationX
            boxWidth
            boxHeight
            boxRotation
            markerGroup
            markerGroupShared
            markerIndex
            markerSubIndex
            markerLabel
            markerStyle
            markerLocationX
            markerLocationY
            markerFontSize
            markerSize
            markerRotation
            gridCoordinates
            balloonGridCoordinates
            connectionPointGridCoordinates
            operation
            toleranceSource
            verified
            fullSpecificationFromOCR
            confidence
            connectionPointLocationX
            connectionPointLocationY
            connectionPointIsFloating
            displayLeaderLine
            leaderLineDistance
            drawing {
              id
              name
              number
            }
            drawingSheet {
              id
            }
          }
          createdAt
          updatedAt
        }
        drawingList(filter: { part: $id, status: Active }) {
          rows {
            id
            part {
              id
            }
            status
            documentType
            name
            number
            revision
            linearUnit
            angularUnit
            markerOptions
            linearTolerances
            angularTolerances
            sheetCount
            tags
            createdAt
            updatedAt
            gridOptions
            file {
              id
              sizeInBytes
              name
              publicUrl
              privateUrl
            }
            originalDrawing
            previousRevision
            nextRevision
            drawingFile {
              id
              sizeInBytes
              name
              publicUrl
              privateUrl
            }
            characteristics {
              id
              previousRevision
              nextRevision
              originalCharacteristic
              notationType
              notationSubtype
              notationClass
              fullSpecification
              nominal
              drawingRotation
              drawingScale
              boxLocationY
              boxLocationX
              boxWidth
              boxHeight
              boxRotation
              markerIndex
              markerLabel
              markerStyle
              markerLocationX
              markerLocationY
              markerFontSize
              markerSize
              markerRotation
              drawingSheetIndex
              captureMethod
              captureError
              status
              toleranceSource
              connectionPointLocationX
              connectionPointLocationY
              connectionPointIsFloating
              displayLeaderLine
              leaderLineDistance
              drawing {
                id
                name
                number
              }
              drawingSheet {
                id
              }
            }
            sheets {
              id
              previousRevision
              nextRevision
              originalSheet
              height
              width
              markerSize
              rotation
              grid
              useGrid
              displayLines
              displayLabels
              pageIndex
              pageName
              foundCaptureCount
              acceptedCaptureCount
              acceptedTimeoutCaptureCount
              rejectedCaptureCount
              reviewedCaptureCount
              reviewedTimeoutCaptureCount
              defaultLinearTolerances
              defaultAngularTolerances
            }
          }
        }
        reportList(filter: { part: $id, status: Active }) {
          rows {
            id
            status
            partVersion
            title
            data
            templateUrl
            filters
            status
            createdAt
            updatedAt
            file {
              name
              publicUrl
              privateUrl
            }
          }
        }
      }
    `,
    list: gql`
      query PART_LIST($filter: PartFilterInput, $orderBy: PartOrderByEnum, $limit: Int, $offset: Int) {
        partList(filter: $filter, orderBy: $orderBy, limit: $limit, offset: $offset) {
          count
          rows {
            id
            customer {
              id
              name
            }
            purchaseOrderCode
            purchaseOrderLink
            type
            name
            number
            revision
            originalPart
            previousRevision
            nextRevision
            drawingNumber
            drawingRevision
            defaultMarkerOptions
            defaultLinearTolerances
            defaultAngularTolerances
            presets
            status
            autoMarkupStatus
            reviewStatus
            workflowStage
            completedAt
            balloonedAt
            reportGeneratedAt
            notes
            accessControl
            measurement
            tags
            displayLeaderLines
            primaryDrawing {
              id
              name
              number
              revision
              previousRevision
              nextRevision
              originalDrawing
              angularTolerances
              angularUnit
              linearTolerances
              linearUnit
              markerOptions
              sheetCount
              createdAt
              updatedAt
              gridOptions
              status
              documentType
              tags
              file {
                id
                sizeInBytes
                name
                publicUrl
                privateUrl
              }
              sheets {
                id
                previousRevision
                nextRevision
                originalSheet
                height
                width
                markerSize
                rotation
                grid
                useGrid
                displayLines
                displayLabels
                pageIndex
                pageName
                foundCaptureCount
                acceptedCaptureCount
                acceptedTimeoutCaptureCount
                rejectedCaptureCount
                reviewedCaptureCount
                reviewedTimeoutCaptureCount
                defaultLinearTolerances
                defaultAngularTolerances
                createdAt
                updatedAt
              }
            }
            drawings {
              id
              name
              number
              revision
              previousRevision
              nextRevision
              originalDrawing
              linearUnit
              angularUnit
              markerOptions
              gridOptions
              linearTolerances
              tags
              angularTolerances
              sheetCount
              createdAt
              updatedAt
              gridOptions
              status
              documentType
              file {
                id
                sizeInBytes
                name
                publicUrl
                privateUrl
              }
              drawingFile {
                id
                sizeInBytes
                name
                publicUrl
                privateUrl
              }
            }
            reports {
              id
              status
              partVersion
              title
              data
              templateUrl
              filters
              status
              createdAt
              updatedAt
              file {
                name
                publicUrl
                privateUrl
              }
            }
            characteristics {
              id
              previousRevision
              nextRevision
              originalCharacteristic
              fullSpecification
              drawing {
                id
                name
                number
              }
            }
            attachments {
              id
              sizeInBytes
              name
              publicUrl
              privateUrl
            }
            updatedAt
            createdAt
          }
        }
      }
    `,
    listShallow: gql`
      query PART_LIST_SHALLOW($filter: PartFilterInput, $orderBy: PartOrderByEnum, $limit: Int, $offset: Int) {
        partListShallow(filter: $filter, orderBy: $orderBy, limit: $limit, offset: $offset) {
          count
          rows {
            id
            name
            number
            revision
            characteristics
            updatedAt
            createdAt
          }
        }
      }
    `,
    revisions: gql`
      query PART_REVISIONS($filter: PartFilterInput, $orderBy: PartOrderByEnum, $limit: Int, $offset: Int) {
        partList(filter: $filter, orderBy: $orderBy, limit: $limit, offset: $offset) {
          count
          rows {
            id
            name
            number
            revision
            originalPart
            previousRevision
            nextRevision
            createdAt
            drawings {
              id
              previousRevision
              nextRevision
              originalDrawing
              name
              number
              revision
              createdAt
            }
          }
        }
      }
    `,
    autocomplete: gql`
      query PART_AUTOCOMPLETE($query: String, $limit: Int) {
        partAutocomplete(query: $query, limit: $limit) {
          id
          label
        }
      }
    `,
  },
  step: {
    order: ['Part Information', 'Document Information', 'Default Tolerances', 'Grid Zones', 'Balloon Styles', 'Data Extraction', 'Item Review', 'Reporting'],
    // Step stores the workflow steps of the part in the client cache
    // These functions simplify editing the cached step
    // get: async () => {
    //   const graphqlClient = await getClient();
    //   return graphqlClient.readQuery({
    //     query: Parts.query.step,
    //   });
    // },
    // set: async (current?: number, max?: number) => {
    //   const graphqlClient = await getClient();
    //   graphqlClient.writeFragment({
    //     id: 'partStep',
    //     fragment: gql`
    //       fragment PartSteps on Part {
    //         steps {
    //           currentStep
    //           maxStep
    //         }
    //       }
    //     `,
    //     data: {
    //       currentStep: current || 0,
    //       maxStep: max || 0,
    //     },
    //   });
    //   // graphqlClient.writeData({
    //   //   data: { currentStep: current || 0, maxStep: max || 0 },
    //   // });
    // },
    // current: async (step?: number) => {
    //   const graphqlClient = await getClient();
    //   graphqlClient.writeFragment({
    //     id: 'partStep',
    //     fragment: gql`
    //       fragment PartSteps on Part {
    //         steps {
    //           currentStep
    //           maxStep
    //         }
    //       }
    //     `,
    //     data: {
    //       currentStep: step,
    //     },
    //   });
    //   // graphqlClient.writeData({
    //   //   data: { currentStep: step },
    //   // });
    // },
    // max: async (step?: number) => {
    //   const graphqlClient = await getClient();
    //   graphqlClient.writeFragment({
    //     id: 'partStep',
    //     fragment: gql`
    //       fragment PartSteps on Part {
    //         steps {
    //           currentStep
    //           maxStep
    //         }
    //       }
    //     `,
    //     data: {
    //       maxStep: step,
    //     },
    //   });
    //   // graphqlClient.writeData({
    //   //   data: { maxStep: step },
    //   // });
    // },
  },

  // TODO: Delete this when NewPart context is ready
  /* eslint-disable react/destructuring-assignment */
  createGraphqlInput: (data: Part) => {
    if (!data || !data.drawings) {
      log.error(`No ${data ? 'drawings' : 'data'} passed to create an input`);
      return null;
    }
    const drawings = data.drawings ? data.drawings.map((drawing) => drawing.id) : [];
    const characteristics = data.characteristics ? data.characteristics.map((char) => char.id) : [];
    const reports = data.reports ? data.reports.map((report) => report.id) : [];
    const input: PartUpdateInput = {
      customer: data.customer ? data.customer.id || '' : null,
      type: data.type ? data.type : 'Part',
      originalPart: data.originalPart,
      previousRevision: data.previousRevision,
      nextRevision: data.nextRevision,
      name: data.name,
      number: data.number,
      revision: data.revision,
      drawingName: data.drawingName,
      drawingNumber: data.drawingNumber,
      drawingRevision: data.drawingRevision,
      defaultMarkerOptions: data.defaultMarkerOptions,
      defaultLinearTolerances: data.defaultLinearTolerances,
      defaultAngularTolerances: data.defaultAngularTolerances,
      presets: data.presets,
      status: data.status,
      autoMarkupStatus: data.autoMarkupStatus,
      reviewStatus: data.reviewStatus,
      workflowStage: data.workflowStage,
      completedAt: data.completedAt,
      balloonedAt: data.balloonedAt,
      reportGeneratedAt: data.reportGeneratedAt,
      lastReportId: data.lastReportId,
      notes: data.notes,
      accessControl: data.accessControl,
      measurement: data.measurement,
      displayLeaderLines: data.displayLeaderLines,
      tags: data.tags,
      purchaseOrderCode: data.purchaseOrderCode,
      purchaseOrderLink: data.purchaseOrderLink,
      primaryDrawing: data.primaryDrawing?.id || (drawings.length > 0 && drawings[0] ? drawings[0] : ''),
      attachments: data.attachments,
      drawings,
      characteristics, // characteristics,
      reports,
    };
    if (validatePartInfo(input)) {
      return input;
    }
    log.warn('input was invalid and not saved to the DB', input);
    return null;
  },
  createPartDataObject: (
    currentPart: Part,
    customer: Organization | null,
    characteristics: Characteristic[] = [],
    types: ListObject | null,
    classifications: ListObject | null,
    methods: ListObject | null,
    operations: ListObject | null,
    units: ListObject | null,
  ) => {
    if (!currentPart || !currentPart.drawings) {
      return null;
    }
    const currentDrawings = getCurrentDrawings(currentPart.drawings);
    const mergeData: ReportData = {
      customer: {
        name: customer && customer.name ? customer.name : '',
      },
      part: {
        name: currentPart.name ? currentPart.name : '',
        number: currentPart.number ? currentPart.number : '',
        revision: currentPart.revision ? currentPart.revision : moment(currentPart.createdAt).format('YYYY-MM-DD'),
        control: currentPart.accessControl ? currentPart.accessControl : 'None',
        status: currentPart.status ? currentPart.status : '',
        updated: currentPart.updatedAt ? moment(currentPart.updatedAt).format('dddd, MMMM Do YYYY, h:mm:ss a') : '',
      },
      primary: {
        name: currentPart.primaryDrawing && currentPart.primaryDrawing.name ? currentPart.primaryDrawing.name : '',
        number: currentPart.primaryDrawing && currentPart.primaryDrawing.number ? currentPart.primaryDrawing.number : '',
        revision: currentPart.primaryDrawing && currentPart.primaryDrawing.revision ? currentPart.primaryDrawing.revision : moment(currentPart.primaryDrawing.createdAt).format('YYYY-MM-DD'),
        control: currentPart.accessControl ? currentPart.accessControl : 'None',
      },
      drawings: currentPart.drawings
        ? currentPart.drawings.map((d) => {
            let fullName = '';
            if (d.name) {
              fullName += d.name;
              if (d.number || d.revision) {
                fullName += ' ';
              }
            }
            if (d.number) {
              fullName += d.number;
              if (d.revision) {
                fullName += ' ';
              }
            }
            if (d.revision) {
              fullName += d.revision;
            }
            return fullName;
          })
        : [],
      item: [],
    };
    if (characteristics) {
      const tempCharacters: CharacteristicData[] = [];
      const addedCharacters: string[] = [];
      for (let i = 0; i < characteristics.length; i++) {
        if (characteristics[i].status === 'Active' && !addedCharacters.includes(characteristics[i].id) && currentDrawings.map((d) => d.id).includes(characteristics[i].drawing.id)) {
          const char = characteristics[i];
          const charDrawing = currentPart.drawings?.find((draw) => draw.id === char.drawing.id) || currentPart.primaryDrawing;
          const drawingGridSettings = JSON.parse(charDrawing.gridOptions || '{}');
          const charDrawingSheet = charDrawing.sheets.find((sheet) => sheet.id === char.drawingSheet.id);
          const useGrid = drawingGridSettings.status !== false && charDrawingSheet && charDrawingSheet.useGrid;
          const crit = classifications ? getSingleLevelListValue(classifications, char.criticality) : 'loading...';
          const op = operations ? getSingleLevelListValue(operations, char.operation) : 'loading...';
          const method = methods ? getSingleLevelListValue(methods, char.inspectionMethod) : 'loading...';
          const type = types ? getListNameById({ list: types, value: char.notationType }) : 'loading...';
          const subtype = types
            ? getListNameById({
                list: types,
                value: char.notationSubtype,
                parentId: char.notationType,
              })
            : 'loading...';
          const unit = units ? getSingleLevelListValue(units, char.unit) : 'loading...';
          tempCharacters.push({
            label: char.markerLabel ? char.markerLabel : '',
            location: `${charDrawing?.name || mergeData.primary.name}, pg. ${char.drawingSheetIndex}${useGrid && char.gridCoordinates ? `, Feature: ${char.gridCoordinates}` : ''}${
              useGrid && char.balloonGridCoordinates ? `, Balloon: ${char.balloonGridCoordinates}` : ''
            }${useGrid && char.connectionPointIsFloating && char.connectionPointGridCoordinates ? `, Connection Point: ${char.connectionPointGridCoordinates}` : ''}`,
            zone: useGrid && char.gridCoordinates ? char.gridCoordinates : '',
            balloon_zone: useGrid && char.balloonGridCoordinates ? char.balloonGridCoordinates : '',
            connection_point_zone: useGrid && char.connectionPointGridCoordinates ? char.connectionPointGridCoordinates : '',
            page: char.drawingSheetIndex ? char.drawingSheetIndex : 0,
            type: type || '',
            subtype: subtype || '',
            quantity: char.quantity ? char.quantity : 1,
            unit: unit || '',
            full_spec: char.fullSpecification ? char.fullSpecification : '',
            tol_type: char.notationClass ? char.notationClass : 'Tolerance',
            nominal: char.nominal ? char.nominal : '',
            plus: char.plusTol ? char.plusTol : '',
            minus: char.minusTol ? char.minusTol : '',
            usl: char.upperSpecLimit ? char.upperSpecLimit : '',
            lsl: char.lowerSpecLimit ? char.lowerSpecLimit : '',
            source: char.toleranceSource ? char.toleranceSource : 'Default_Tolerance',
            classification: crit || '',
            operation: op || '',
            method: method || '',
            comment: char.notes ? char.notes : '',
            verification: char.verified ? char.verified : false,
            number: char.markerIndex ? char.markerIndex : 1,
            status: char.status ? char.status : 'Active',
            capturemethod: char.captureMethod ? char.captureMethod : 'Manual',
            captureError: char.captureError ? char.captureError : '',
            gdtsymbol: char.gdtSymbol ? char.gdtSymbol : '',
            gdtprimarytolerancezone: char.gdtPrimaryToleranceZone ? char.gdtPrimaryToleranceZone : '',
            gdtsecondarytolerancezone: char.gdtSecondaryToleranceZone ? char.gdtSecondaryToleranceZone : '',
            gdtprimarydatum: char.gdtPrimaryDatum ? char.gdtPrimaryDatum : '',
            gdtsecondarydatum: char.gdtSecondaryDatum ? char.gdtSecondaryDatum : '',
            gdttertiarydatum: char.gdtTertiaryDatum ? char.gdtTertiaryDatum : '',
            markergroup: char.markerGroup ? char.markerGroup : '',
            markergroupshared: char.markerGroupShared ? char.markerGroupShared : false,
            markersubindex: char.markerSubIndex ? char.markerSubIndex : 0,
            markerstyle: char.markerStyle ? char.markerStyle : '',
            markerfontsize: char.markerFontSize ? char.markerFontSize : 18,
            markerSize: char.markerSize ? char.markerSize : 36,
            ocr: char.fullSpecificationFromOCR ? char.fullSpecificationFromOCR : '',
            confidence: char.confidence ? char.confidence : 0,
          });
          addedCharacters.push(characteristics[i].id);
        }
      }

      mergeData.item = tempCharacters.sort((a, b) => {
        if (parseInt(a.label, 10) > parseInt(b.label, 10)) {
          return 1;
        }
        if (parseInt(a.label, 10) === parseInt(b.label, 10)) {
          if (a.markersubindex > b.markersubindex) {
            return 1;
          }
        }
        return -1;
      });
    }
    return mergeData;
  },
};
