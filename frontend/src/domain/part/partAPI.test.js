import { attachment, drawing, feature, part } from 'modules/part/fixtures';
import log from 'modules/shared/logger';
import { Parts } from './partApi';

describe('Parts API', () => {
  const warnSpy = jest.spyOn(log, 'warn');
  const errorSpy = jest.spyOn(log, 'error');

  describe('Graphql Input Utility', () => {
    it('throws an error if no data is passed', () => {
      const testInput = Parts.createGraphqlInput(null);
      expect(testInput).toBeNull();
      expect(errorSpy).toHaveBeenCalledTimes(1);
      errorSpy.mockReset();
    });
    it('fails to validate if there are no drawings', () => {
      const testInput = Parts.createGraphqlInput({ ...part, drawings: [] });
      expect(testInput).toBeNull();
      expect(warnSpy).toHaveBeenCalledTimes(2);
      warnSpy.mockReset();
    });
    it('uses the drawing list if primary drawing is missing', () => {
      const testInput = Parts.createGraphqlInput({ ...part, drawings: [drawing], primaryDrawing: null });
      expect(testInput).not.toBeNull();
      expect(testInput.primaryDrawing).toBe(drawing.id);
      expect(warnSpy).toHaveBeenCalledTimes(0);
      warnSpy.mockReset();
    });
    it('fails to validate if input has no primary drawing id', () => {
      const testInput = Parts.createGraphqlInput({ ...part, drawings: [{ ...drawing, id: null }], primaryDrawing: null });
      expect(testInput).toBeNull();
      expect(warnSpy).toHaveBeenCalledTimes(2);
      warnSpy.mockReset();
    });
    it('fails to validate if there is no part number and it is an active part past the first wizard step', () => {
      const testInput = Parts.createGraphqlInput({ ...part, number: null, status: 'Active', workflowStage: 2 });
      expect(testInput).toBeNull();
      expect(warnSpy).toHaveBeenCalledTimes(2);
      warnSpy.mockReset();
    });
    it('fails to validate if there is no customer', () => {
      const testInput = Parts.createGraphqlInput({ ...part, customer: {} });
      expect(testInput).toBeNull();
      expect(warnSpy).toHaveBeenCalledTimes(2);
      warnSpy.mockReset();
    });
    it('returns a part input', () => {
      const testInput = Parts.createGraphqlInput(part);
      expect(testInput).not.toBeNull();
      expect(testInput).toMatchObject({
        originalPart: '3e40e49b-7464-4b4e-8169-b6fb2183c0ff',
        previousRevision: null,
        revision: 'Part A',
        accessControl: 'None',
        balloonedAt: null,
        characteristics: [feature.id],
        completedAt: null,
        customer: null,
        defaultAngularTolerances: null,
        defaultLinearTolerances: null,
        defaultMarkerOptions: null,
        displayLeaderLines: true,
        drawingNumber: null,
        drawingRevision: null,
        measurement: 'Imperial',
        name: 'lowerplate',
        notes: null,
        number: '1234',
        presets: null,
        primaryDrawing: drawing.id,
        purchaseOrderCode: null,
        purchaseOrderLink: null,
        reportGeneratedAt: null,
        reviewStatus: 'Unverified',
        status: 'Active',
        tags: null,
        type: 'Part',
        workflowStage: 0,
        drawings: [drawing.id],
        attachments: [attachment],
        reports: [],
      });
    });
  });
});
