import React, { useState, useEffect, useRef } from 'react';
import { useHistory, useLocation } from 'react-router';
import { useDispatch, useSelector } from 'react-redux';
import { Layout } from 'antd';
import Analytics from 'modules/shared/analytics/analytics';
import { doSetPart, updatePartThunk } from 'modules/part/partActions';
import { AppState } from 'modules/state';
import { ReportTemplateActions } from 'modules/reportTemplate/reportTemplateActions';
import { PartStepEnum, updateCurrentPartStep, updateMaxPartStep, updatePartStepThunk } from 'modules/navigation/navigationActions';
import { CharacteristicActions } from 'modules/characteristic/characteristicActions';
import { PartDetailsForm } from './partDetails/PartDetailsForm';
import { DocumentDetailsForm } from './documentDetails/DocumentDetailsForm';
import { DefaultTolerancesForm } from './defaultTolerances/DefaultTolerancesForm';
import { BalloonStylesForm } from './balloonStyles/BalloonStylesForm';
import { GridForm } from './grid/GridForm';
import { ItemPanel } from './characteristics/ItemPanel';
import ReviewDrawer from './review/ReviewDrawer';
import { Parts } from '../partApi';
// import { i18n } from 'i18n';
const PART_EDITOR_STEP_NAMES = Parts.step.order;

const { Sider } = Layout;

const { EXTRACT, REVIEW } = PartStepEnum;

// Exit method references the history object passed to the route
export const PartEditorDrawer: React.FC<{
  exit: () => void;
}> = ({ exit }) => {
  // Prepare to update the left navigation at each stage of part creation
  const navigation = useSelector((state: AppState) => state.navigation);
  const { part } = useSelector((state: AppState) => state.part);
  const { characteristics, compareRevision } = useSelector((state: AppState) => state.characteristics);
  const { settings } = useSelector((state: AppState) => state.session);

  // Prepare to update the part at each stage of part creation

  const dispatch = useDispatch();
  const { state } = useLocation<any>();

  const showAlert = useRef(true);
  const setShowAlert = (val: boolean) => {
    showAlert.current = val;
  };

  // Local state to track whether the drawer is open or closed
  const [collapsed, setCollapsed] = useState(false);
  const history = useHistory();

  // When a new ID is passed in, retrieve update the navigation
  useEffect(() => {
    if (navigation && part) {
      const lastStep = settings.autoballoonEnabled?.value === 'true' ? PartStepEnum.REVIEW : PartStepEnum.EXTRACT;
      if (navigation.maxPartStep > lastStep) {
        // move back into the part wizard from reporting
        dispatch(updateMaxPartStep(lastStep));
      }
      if (navigation.currentPartStep > lastStep) {
        // move back into the part wizard from reporting
        dispatch(updateCurrentPartStep(lastStep));
      }
      if (state?.step !== undefined && navigation.currentPartStep < 0) {
        // change current step when clicking from reporting to the side nav
        dispatch(updatePartStepThunk(state.step));
        // clear state.step so refresh takes you to the highest part stage
        history.replace(history.location.pathname, {});
      } else if (navigation.maxPartStep !== part.workflowStage && navigation.currentPartStep < 0) {
        // set current step to the highest part stage on load
        const step = part.workflowStage;
        dispatch(updatePartStepThunk(step));
      }
    }
  }, [state?.step, part?.id, navigation]);

  // TODO: All error states should tell the user what they need to do to recover
  if (!part) {
    // I removed this because it looks weird with two spinners
    // Open to changing it if there is a use case where the pdf won't be loading as well
    return <div />;
  }

  // Handle click of Next button at each stage
  const next = async () => {
    if (!part || navigation.currentPartStep === navigation.maxTotalSteps) {
      return;
    }
    const nextStep = navigation.currentPartStep >= 0 ? navigation.currentPartStep + 1 : 0;
    Analytics.track({
      event: Analytics.events.partDrawerDrawerMoveNext,
      eventNameSuffix: ` to [${nextStep}] ${PART_EDITOR_STEP_NAMES[nextStep]}`,
      part,
      properties: {
        'editor.step.current': `[${navigation.currentPartStep}] ${PART_EDITOR_STEP_NAMES[navigation.currentPartStep]}`,
        'editor.step.next': `[${nextStep}] ${PART_EDITOR_STEP_NAMES[nextStep]}`,
      },
    });
    await dispatch(updatePartStepThunk(nextStep));
  };

  // Handle click of Back button at each stage
  const back = () => {
    if (navigation.currentPartStep === 0) {
      dispatch(updatePartStepThunk(-1));
      return;
    }
    const nextStep = navigation.currentPartStep !== null ? navigation.currentPartStep - 1 : 0;
    Analytics.track({
      event: Analytics.events.partEditorDrawerMoveBack,
      eventNameSuffix: ` to [${nextStep}] ${PART_EDITOR_STEP_NAMES[nextStep]}`,
      part,
      properties: {
        'editor.step.current': `[${navigation.currentPartStep}] ${PART_EDITOR_STEP_NAMES[navigation.currentPartStep]}`,
        'editor.step.next': `[${nextStep}] ${PART_EDITOR_STEP_NAMES[nextStep]}`,
      },
    });
    dispatch(updatePartStepThunk(nextStep));
  };

  // Handle click of Cancel button at each stage
  const cancel = () => {
    Analytics.track({
      event: Analytics.events.partEditorDrawerCancel,
      part,
      properties: {
        'editor.step.current': `[${navigation.currentPartStep}] ${PART_EDITOR_STEP_NAMES[navigation.currentPartStep]}`,
      },
    });
    if (part) {
      dispatch(updatePartThunk({ status: 'Deleted' }, part.id));
    }
    dispatch(updatePartStepThunk(-1));
    exit();
  };

  const generateReport = async () => {
    Analytics.track({
      event: Analytics.events.reportWizardStarted,
      part,
      // properties: {},
    });
    const newPart = { ...part, characteristics: [...characteristics] };
    dispatch(doSetPart(newPart, part));
    dispatch({ type: ReportTemplateActions.SET_NEW_REPORT });
    dispatch(updatePartStepThunk(part.workflowStage + 1));
    history.push(`/parts/${part.id}/reports/`, { source: `/parts/${part.id}` });
  };

  let panel = [];
  if (settings.autoballoonEnabled?.value === 'true') {
    panel = [
      <PartDetailsForm canCancel={part.workflowStage !== undefined && part.workflowStage === 0} exit={exit} cancel={cancel} next={next} />,
      <DocumentDetailsForm next={next} back={back} />,
      <DefaultTolerancesForm next={next} back={back} />,
      <GridForm next={next} back={back} showAlert={showAlert.current} setShowAlert={setShowAlert} />,
      <BalloonStylesForm next={next} back={back} />,
      <ItemPanel next={next} back={back} collapsed={!!compareRevision || collapsed} currentStep={EXTRACT} />,
      <ItemPanel next={next} back={back} review generateReport={generateReport} currentStep={REVIEW} collapsed={!!compareRevision || collapsed} />,
    ];
  } else {
    panel = [
      <PartDetailsForm canCancel={part.workflowStage !== undefined && part.workflowStage === 0} exit={exit} cancel={cancel} next={next} />,
      <DocumentDetailsForm next={next} back={back} />,
      <DefaultTolerancesForm next={next} back={back} />,
      <GridForm next={next} back={back} showAlert={showAlert.current} setShowAlert={setShowAlert} />,
      <BalloonStylesForm next={next} back={back} />,
      <ItemPanel next={next} back={back} review generateReport={generateReport} currentStep={REVIEW} collapsed={!!compareRevision || collapsed} />,
    ];
  }

  // Show Review Drawer
  if (navigation.currentPartStep && navigation.currentPartStep >= EXTRACT) {
    return (
      <>
        <Sider //
          collapsible={navigation.currentPartStep >= EXTRACT}
          collapsed={navigation.currentPartStep >= EXTRACT ? !!compareRevision || collapsed : false}
          onCollapse={() => setCollapsed(!collapsed)}
          width="350px"
          className="wizard-drawer"
        >
          {panel[navigation.currentPartStep !== null ? navigation.currentPartStep : 0]}
        </Sider>
        <Sider //
          collapsible={false}
          className="review-drawer"
          width={navigation.currentPartStep === panel.length - 1 && characteristics.length !== 0 ? 300 : 0}
          theme="light"
          style={{ padding: 0 }}
        >
          <ReviewDrawer />
        </Sider>
        {compareRevision && (
          <Sider //
            collapsible
            className="review-drawer revision"
            width={navigation.currentPartStep === panel.length - 1 && characteristics.length !== 0 ? 300 : 0}
            theme="light"
            style={{ padding: 0 }}
            onCollapse={() => dispatch({ type: CharacteristicActions.SET_REVISION, payload: { compareRevision: null, compareIndex: 0 } })}
          >
            <ReviewDrawer revision />
          </Sider>
        )}
      </>
    );
  }

  return (
    <Sider //
      collapsible={navigation.currentPartStep >= EXTRACT}
      collapsed={navigation.currentPartStep >= EXTRACT ? !!compareRevision || collapsed : false}
      onCollapse={() => setCollapsed(!collapsed)}
      width="350px"
      className="part-analysis-sider"
    >
      {panel[navigation.currentPartStep >= 0 ? navigation.currentPartStep : 0]}
    </Sider>
  );
};
