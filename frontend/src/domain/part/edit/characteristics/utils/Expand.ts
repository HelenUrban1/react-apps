import { Characteristics } from 'graphql/characteristics';
import { Characteristic, CharacteristicInput } from 'types/characteristics';
import log from 'modules/shared/logger';
import { sortChars } from 'modules/characteristic/characteristicActions';
import { cloneDeep } from 'lodash';
import { getCartesianFromPolar, getPolarFromCartesian } from './coordTransforms';

export const updateIndexNumbers = (list: Characteristic[], newChars?: CharacteristicInput[], updatedChars?: Characteristic[]) => {
  const changed: { [key: string]: Characteristic } = {};
  updatedChars?.forEach((c) => {
    changed[c.id] = { ...c };
  });
  const added: { [key: string]: CharacteristicInput } = {};
  newChars?.forEach((c) => {
    added[c.markerLabel] = { ...c };
  });

  list.sort(sortChars).forEach((char, index) => {
    if (added[char.markerLabel]) {
      added[char.markerLabel].markerIndex = index;
    } else if (char.markerIndex !== index) {
      changed[char.id] = { ...char, ...changed[char.id], markerIndex: index };
    }
  });

  return { changed: Object.values(changed), added: Object.values(added) };
};

export const expandCharacteristic = (feature: Characteristic, list: Characteristic[], shared: boolean) => {
  if (!feature) {
    log.error('No characteristic to expand');
  }
  const { quantity } = feature;
  let index = feature.markerIndex;
  const newChars = [];
  const combinedList = cloneDeep(list);
  // create new characteristics
  for (let j = 2; j <= quantity; j++) {
    index += 1;
    const subFeature = cloneDeep(feature);
    subFeature.id = '';
    subFeature.markerIndex = index;
    subFeature.markerSubIndex = j - 1;
    subFeature.quantity = 1;
    subFeature.markerGroupShared = shared;
    subFeature.markerLabel = `${subFeature.markerGroup}.${j}`;
    if (!shared) {
      const boxCartesian = {
        x: feature.boxLocationX + feature.boxWidth / 2,
        y: feature.boxLocationY + feature.boxHeight / 2,
      };
      const markerCartesian = getCartesianFromPolar(boxCartesian, { x: subFeature.markerLocationX, y: subFeature.markerLocationY }, feature.markerRotation);
      const newY = markerCartesian.y + feature.markerFontSize * 2 * (j - 1);
      const newPolar = getPolarFromCartesian(boxCartesian, { x: markerCartesian.x, y: newY });
      subFeature.markerLocationX = newPolar.x;
      subFeature.markerLocationY = newPolar.y;
    }
    const subFeatureInput = Characteristics.util.input(subFeature).char;
    newChars.push(subFeatureInput);
    combinedList.push(subFeature);
  }

  // update the original feature
  const updatedOriginal = cloneDeep(feature);
  updatedOriginal.markerSubIndex = 0;
  updatedOriginal.markerLabel = `${feature.markerGroup}.1`;
  updatedOriginal.quantity = 1;
  updatedOriginal.markerGroupShared = shared;
  // update the index for the changed items
  const { changed, added } = updateIndexNumbers(combinedList, newChars, [updatedOriginal]);

  return { newChars: added, updatedChars: changed };
};

export const expandFeatureGroup = (group: Characteristic[], list: Characteristic[], shared: boolean) => {
  const newChars = [];
  const combinedList = cloneDeep(list);
  const updatedChars: Characteristic[] = [];

  const { quantity, markerGroup } = group[0];
  let index = group.sort((a, b) => a.markerSubIndex - b.markerSubIndex)[0].markerIndex; // Index of the first of the original features
  const last = index + group.length; // Index of the last feature in the original group, for renumbering subsequent features

  // create new characteristics
  for (let j = 1; j < quantity; j++) {
    index += group.length;
    for (let x = 0; x < group.length; x++) {
      const feature = group[x];
      const subFeature = { ...feature };
      subFeature.markerIndex = index + x;
      subFeature.markerGroup = `${parseInt(markerGroup, 10) + j}`;
      subFeature.markerSubIndex = x;
      subFeature.quantity = 1;
      subFeature.markerLabel = `${subFeature.markerGroup}.${x + 1}`;
      subFeature.markerGroupShared = shared;

      const boxCartesian = {
        x: feature.boxLocationX + feature.boxWidth / 2,
        y: feature.boxLocationY + feature.boxHeight / 2,
      };
      const markerCartesian = getCartesianFromPolar(boxCartesian, { x: subFeature.markerLocationX, y: subFeature.markerLocationY }, feature.markerRotation);

      if (!shared) {
        const newX = markerCartesian.x + feature.markerFontSize * 2 * j;
        const newPolar = getPolarFromCartesian(boxCartesian, { x: newX, y: markerCartesian.y });
        subFeature.markerLocationX = newPolar.x;
        subFeature.markerLocationY = newPolar.y;
      } else {
        const newY = markerCartesian.y + feature.markerFontSize * 2 * j;
        const newPolar = getPolarFromCartesian(boxCartesian, { x: markerCartesian.x, y: newY });
        subFeature.markerLocationX = newPolar.x;
        subFeature.markerLocationY = newPolar.y;
      }

      const subFeatureInput = Characteristics.util.input(subFeature).char;
      newChars.push(subFeatureInput);
      combinedList.push(subFeature);
      // update the original feature
      const updatedOriginal = { ...feature };
      updatedOriginal.quantity = 1;
      updatedChars.push(updatedOriginal);
      combinedList[updatedOriginal.markerIndex] = updatedOriginal;
    }
  }

  // increment the index for the items after the expanded section
  for (let k = last; k < list.length; k++) {
    index += 1;
    const updated = { ...list[k] };
    updated.markerIndex = index;
    updated.markerGroup = `${parseInt(updated.markerGroup, 10) + quantity - 1}`;
    if (updated.markerSubIndex !== null && updated.markerSubIndex >= 0) {
      updated.markerLabel = `${updated.markerGroup}.${updated.markerSubIndex + 1}`;
    } else {
      updated.markerLabel = `${updated.markerGroup}`;
    }
    updatedChars.push(updated);
  }
  const { changed, added } = updateIndexNumbers(list, newChars, updatedChars);

  return { newChars: added, updatedChars: changed };
};
