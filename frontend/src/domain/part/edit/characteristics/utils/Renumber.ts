import { sortChars } from 'modules/characteristic/characteristicActions';
import { Characteristic } from 'types/characteristics';
import { dragMultipleToNewIndex, moveDragToNewIndex } from 'utils/DragUtilities';
import { updateIndexNumbers } from './Expand';

// update the moved Item updateMoved(item, itemList[i-1].markerIndex, newGroup, newGroup);
const updateMoved = (item: Characteristic, newInd: number, newNum: string, newLabel: string) => {
  const newItem = { ...item };
  newItem.markerIndex = newInd;
  newItem.markerGroup = newNum;
  newItem.markerLabel = newLabel;
  newItem.markerSubIndex = -1;
  newItem.markerGroupShared = false;
  return newItem;
};

// A Grouped Item was moved to a lower Index in the list
export const ungroupBefore = (item: Characteristic, newGroup: string, itemList: Characteristic[]) => {
  const newGroupNumber = parseInt(newGroup, 10);
  let changedOriginal = false;
  const itemsToUpdate = [];
  const newList = [];

  for (let i = 0; i < itemList.length; i++) {
    const currentGroupNumber = parseInt(itemList[i].markerGroup, 10);

    // same item as moved, don't change here
    if (itemList[i].id === item.id) continue;

    // item is before the new insertion point, no update, add to newList
    if (currentGroupNumber < newGroupNumber) {
      newList.push(itemList[i]);
      continue;
    }

    if (!changedOriginal && currentGroupNumber === newGroupNumber) {
      // set the moved item's index equal to the index of the item is displacing
      // groups don't matter here, the displaced items will always be bumped up
      changedOriginal = true;
      const newItem = { ...item };
      newItem.markerIndex = itemList[i].markerIndex;
      newItem.markerGroup = newGroup;
      newItem.markerLabel = newGroup;
      newItem.markerSubIndex = -1;
      newItem.markerGroupShared = false;
      newList.push(newItem);
      itemsToUpdate.push(newItem);
    }

    const newItem = { ...itemList[i] };

    if (currentGroupNumber >= newGroupNumber) {
      // item is after the moved item's new spot, its Index and group number increment
      newItem.markerIndex += 1;
      newItem.markerGroup = (currentGroupNumber + 1).toString();
    }

    // this is the group the item was ungrouped from
    if (currentGroupNumber === parseInt(item.markerGroup, 10)) {
      // this is the group we move the item out of
      const movedSubIndex = item.markerSubIndex;
      // const originalGroupNumber = (parseInt(newItem.markerGroup) - 1).toString();
      // current Item was after the moved item inside their group

      //  check if the group should be destroyed
      if (movedSubIndex === 0 && newItem.markerSubIndex === 1 && (!itemList[i + 1] || itemList[i + 1].markerGroup !== currentGroupNumber.toString())) {
        // ungroup lone remaining item
        newItem.markerSubIndex = -1;
        newItem.markerLabel = newItem.markerGroup;
        newItem.markerGroupShared = false;
      } else if (movedSubIndex === 1 && newItem.markerSubIndex === 0 && (!itemList[i + 2] || itemList[i + 2].markerGroup !== currentGroupNumber.toString())) {
        newItem.markerSubIndex = -1;
        newItem.markerGroupShared = false;
        newItem.markerLabel = newItem.markerGroup;
      } else {
        // group still exists, only change items after the original
        if (itemList[i].markerSubIndex > movedSubIndex) {
          // item was later in the group, decrement its sub-index
          newItem.markerSubIndex -= 1;
        }
        newItem.markerLabel = `${newItem.markerGroup}.${newItem.markerSubIndex + 1}`;
      }
    } else {
      // item is not part of the same group
      newItem.markerLabel = newItem.markerLabel.includes('.') ? `${newItem.markerGroup}.${newItem.markerSubIndex + 1}` : newItem.markerGroup;
    }

    newList.push(newItem);
    itemsToUpdate.push(newItem);
  }

  const { changed } = updateIndexNumbers(newList, [], itemsToUpdate);
  // updateDatabase(itemsToUpdate, edit, part);
  return { newList, itemsToUpdate: changed };
};

// A Grouped Item was moved to a higher Index in the list
export const ungroupAfter = (item: Characteristic, newGroup: string, itemList: Characteristic[]) => {
  const originalGroupNumber = parseInt(item.markerGroup, 10);
  const newGroupNumber = parseInt(newGroup, 10);
  let changedOriginal = false;
  const insertMovedItem = false;
  const itemsToUpdate = [];
  const newList = [];

  for (let i = 0; i < itemList.length; i++) {
    const currentGroupNumber = parseInt(itemList[i].markerGroup, 10);

    // same item as moved, don't change here
    if (itemList[i].id === item.id) continue;

    if (currentGroupNumber < parseInt(item.markerGroup, 10)) {
      //  item was before the original point, so no changes needed, add back to newList array
      newList.push(itemList[i]);
      continue;
    }

    if (!changedOriginal && currentGroupNumber === newGroupNumber) {
      // set the moved item's index equal to the index of the item is displacing
      // groups don't matter here, the displaced items will always be bumped up
      changedOriginal = true;
      const newItem = { ...item };
      newItem.markerIndex = itemList[i].markerIndex - 1;
      newItem.markerGroup = newGroup;
      newItem.markerLabel = newGroup;
      newItem.markerSubIndex = -1;
      newItem.markerGroupShared = false;
      itemsToUpdate.push(newItem);
      newList.push(newItem);
    }

    if (!changedOriginal && currentGroupNumber === newGroupNumber) {
      // set the moved item's index equal to the index of the item its displacing
      // groups don't matter here, the displaced items will always be bumped up
      changedOriginal = true;
      const newItem = { ...item };
      newItem.markerIndex = itemList[i].markerIndex - 1;
      newItem.markerGroup = newGroup;
      newItem.markerLabel = newGroup;
      newItem.markerSubIndex = -1;
      newItem.markerGroupShared = false;
      itemsToUpdate.push(newItem);
      newList.push(newItem);
    }

    const newItem = { ...itemList[i] };

    if (currentGroupNumber >= newGroupNumber) {
      // item is after the moved item's new spot, its group # increases
      newItem.markerGroup = (currentGroupNumber + 1).toString();
      newItem.markerLabel = newItem.markerLabel.includes('.') ? `${newItem.markerGroup}.${newItem.markerSubIndex + 1}` : newItem.markerGroup;
    } else if (currentGroupNumber === originalGroupNumber) {
      // this is the group we move the item out of
      const movedSubIndex = item.markerSubIndex;

      //  check if the group should be destroyed
      if (movedSubIndex === 0 && newItem.markerSubIndex === 1 && (!itemList[i + 1] || itemList[i + 1].markerGroup !== currentGroupNumber.toString())) {
        // 2 item group, 1st item was removed, ungroup lone remaining item, decrease its marker index
        newItem.markerIndex -= 1;
        newItem.markerSubIndex = -1;
        newItem.markerLabel = newItem.markerGroup;
        newItem.markerGroupShared = false;
      } else if (movedSubIndex === 1 && newItem.markerSubIndex === 0 && (!itemList[i + 2] || itemList[i + 2].markerGroup !== currentGroupNumber.toString())) {
        // 2 item group, 2nd item was removed, ungroup lone remaining item, leave its index the same
        newItem.markerSubIndex = -1;
        newItem.markerLabel = newItem.markerGroup;
        newItem.markerGroupShared = false;
      } else {
        // group still exists, only change items after the removed item
        if (itemList[i].markerSubIndex > movedSubIndex) {
          // item was later in the group, decrement its sub-index and index
          newItem.markerIndex -= 1;
          newItem.markerSubIndex -= 1;
        }
        newItem.markerLabel = `${newItem.markerGroup}.${newItem.markerSubIndex + 1}`;
      }
    } else {
      // Item is between the original group and the insertion point, decrease its Index
      newItem.markerIndex -= 1;
    }

    newList.push(newItem);
    itemsToUpdate.push(newItem);
  }

  const { changed } = updateIndexNumbers(newList, [], itemsToUpdate);
  // updateDatabase(itemsToUpdate, edit, part);
  return { newList, itemsToUpdate: changed };
};

// A feature was dragged to a new Index in the list
export const dragItem = (newList: Characteristic[]) => {
  const itemsToUpdate = [];
  const adjustedList = [];
  let number = 0;
  for (let i = 0; i < newList.length; i++) {
    const element = { ...newList[i] };
    if (!element.markerSubIndex || element.markerSubIndex === -1) {
      number += 1;
    }
    const orgIndex = element.markerIndex;
    element.markerIndex = i;
    element.markerGroup = number.toString();
    if (element.markerSubIndex !== null && element.markerSubIndex !== undefined && element.markerSubIndex >= 0) {
      // feature was part of a group
      element.markerLabel = `${element.markerGroup}.${element.markerSubIndex + 1}`;
      itemsToUpdate.push(element);
    } else if (element.markerLabel !== number.toString()) {
      // single feature
      element.markerLabel = number.toString();
      itemsToUpdate.push(element);
    } else if (orgIndex !== i) {
      itemsToUpdate.push(element);
    }
    adjustedList.push(element);
  }

  return { adjustedList, itemsToUpdate };
};

// An ungrouped item was moved to a lower Index in the list
export const moveBefore = (item: Characteristic, newGroup: string, itemList: Characteristic[]) => {
  const newGroupNumber = parseInt(newGroup, 10);
  let changedOriginal = false;
  const itemsToUpdate = [];
  const newList = [];

  for (let i = 0; i < itemList.length; i++) {
    // The current item in the array is the item we just moved
    if (itemList[i].id === item.id) {
      // skip
      continue;
    }

    // The current item is not the item we just moved
    const currentGroupNumber = parseInt(itemList[i].markerGroup, 10);

    if (!changedOriginal && currentGroupNumber === newGroupNumber) {
      // found the index we moved our item to, update it
      changedOriginal = true;
      const newItem = { ...item };
      newItem.markerIndex = itemList[i].markerIndex;
      newItem.markerGroup = newGroup;
      newItem.markerLabel = newGroup;
      newItem.markerSubIndex = -1;
      newItem.markerGroupShared = false;
      itemsToUpdate.push(newItem);
      newList.push(newItem);
    }

    if (currentGroupNumber >= newGroupNumber && currentGroupNumber <= parseInt(item.markerGroup, 10)) {
      // the item in the list has a group number greater than or equal to the new group number, and less than the old number
      // increment items in this range
      const newItem = { ...itemList[i] };
      newItem.markerIndex++;
      newItem.markerGroup = (currentGroupNumber + 1).toString();
      newItem.markerLabel = newItem.markerLabel.includes('.') ? `${newItem.markerGroup}.${newItem.markerSubIndex + 1}` : newItem.markerGroup.toString();
      itemsToUpdate.push(newItem);
      newList.push(newItem);
    } else {
      newList.push(itemList[i]);
    }
  }

  const { changed } = updateIndexNumbers(newList, [], itemsToUpdate);
  // updateDatabase(itemsToUpdate, edit, part);
  return { newList, itemsToUpdate: changed };
};

// An ungrouped item was moved to a higher Index in the list
export const moveAfter = (item: Characteristic, newGroup: string, itemList: Characteristic[]) => {
  const newGroupNumber = parseInt(newGroup, 10);
  let changedOriginal = false;
  let insertMovedItem = false;
  const itemsToUpdate: Characteristic[] = [];
  const newList: Characteristic[] = [];

  let didInsert = false;
  for (let i = 0; i < itemList.length; i++) {
    // The current item in the array is the item we just moved
    if (itemList[i].id === item.id) {
      continue;
    }

    // Item is different than the one we just changed, check if its in between the new and old, or the same index
    const currentGroupNumber = parseInt(itemList[i].markerGroup, 10);

    // Need to wait until we've passed the insertion point before adding to the array
    if (insertMovedItem) {
      const updatedItem = updateMoved(item, itemList[i - 1].markerIndex, newGroup, newGroup);
      itemsToUpdate.push(updatedItem);
      newList.push(updatedItem);
      didInsert = true;
      insertMovedItem = false;
    }

    // found the index we moved our item to, mark it as ready to update and wait for the next go around to insert it into the arrays
    // need additional check to make sure if the new group number was previously a group, we're at the end of the group
    if (!changedOriginal && currentGroupNumber === newGroupNumber && (!itemList[i + 1] || itemList[i].markerGroup !== itemList[i + 1].markerGroup)) {
      changedOriginal = true;
      insertMovedItem = true;
    }

    // item is between the original spot of the moved item and its new spot
    if (currentGroupNumber <= newGroupNumber && currentGroupNumber > parseInt(item.markerGroup, 10)) {
      const newItem = { ...itemList[i] };
      newItem.markerIndex -= 1;
      newItem.markerGroup = (currentGroupNumber - 1).toString();
      newItem.markerLabel = newItem.markerLabel.includes('.') ? `${newItem.markerGroup}.${newItem.markerSubIndex + 1}` : newItem.markerGroup.toString();
      itemsToUpdate.push(newItem);
      newList.push(newItem);
    } else {
      newList.push(itemList[i]);
    }
  }

  // If item was moved to end of the list, need to add after loop
  if (!didInsert) {
    const updatedItem = updateMoved(item, itemList[itemList.length - 1].markerIndex, newGroup, newGroup);
    itemsToUpdate.push(updatedItem);
    newList.push(updatedItem);
  }

  const { changed } = updateIndexNumbers(newList, []);
  const keys = changed.map((c) => c.id);
  itemsToUpdate.forEach((c) => {
    if (!keys.includes(c.id)) {
      keys.push(c.id);
      changed.push(c);
    }
  });

  return { newList, itemsToUpdate: changed };
};

export const renumberItem = (item: Characteristic, newNumber: string, characteristicList: Characteristic[]) => {
  let res;

  if (!characteristicList || item.markerLabel === newNumber) return undefined;

  if (item.markerSubIndex !== -1 && item.markerSubIndex !== null) {
    // item was part of a group, needs to be ungrouped
    if (parseInt(item.markerGroup, 10) < parseInt(newNumber, 10)) {
      // item was moved ahead of its current group
      res = ungroupAfter(item, newNumber, characteristicList);
    } else {
      // item was moved behind its current group
      res = ungroupBefore(item, newNumber, characteristicList);
    }
    // item was not part of a group
  } else if (parseInt(item.markerGroup, 10) < parseInt(newNumber, 10)) {
    // item was moved ahead of its previous index
    res = moveAfter(item, newNumber, characteristicList);
  } else {
    // item was moved behind its previous index
    res = moveBefore(item, newNumber, characteristicList);
  }
  return res;
};

// Reduce a non-grouped numbers Index by some amount
const reduceWhole = (item: Characteristic, decByGroup: number, decByInd: number) => {
  const newItem = { ...item };
  newItem.markerIndex -= decByInd;
  newItem.markerLabel = (parseInt(newItem.markerGroup, 10) - decByGroup).toString();
  newItem.markerGroup = newItem.markerLabel;
  return newItem;
};

// Reduce the Index and Sub-Indeces of Grouped Items by some amount, or ungroup a single item
const reduceGroup = (groupItems: Characteristic[], decByGroup: number, decByInd: number, newList: Characteristic[], updateList: Characteristic[]) => {
  const items = [...newList];
  const updates = [...updateList];

  if (groupItems.length === 1) {
    // revert lone grouped item to a whole index
    const newItem = { ...groupItems[0] };
    newItem.markerIndex = newItem.markerIndex - decByInd - groupItems[0].markerSubIndex;
    newItem.markerGroup = (parseInt(newItem.markerGroup, 10) - decByGroup).toString();
    newItem.markerLabel = newItem.markerGroup;
    newItem.markerSubIndex = -1;
    newItem.markerGroupShared = false;
    items.push(newItem);
    updates.push(newItem);
  } else {
    for (let i = 0; i < groupItems.length; i++) {
      // revert group items by index and sub-index
      const newItem = { ...groupItems[i] };
      newItem.markerSubIndex = i;
      const subIndexDiff = groupItems[i].markerSubIndex - i;
      newItem.markerIndex -= decByInd + subIndexDiff;
      newItem.markerGroup = (parseInt(newItem.markerGroup, 10) - decByGroup).toString();
      newItem.markerLabel = `${newItem.markerGroup}.${newItem.markerSubIndex + 1}`;
      items.push(newItem);
      updates.push(newItem);
    }
  }
  const { changed } = updateIndexNumbers(newList, [], updates);

  return { items, updates: changed };
};

// Function for renumbering after any assortment of items was deleted
export const renumberAfterDelete = (deletedIDs: string[], itemList: Characteristic[]) => {
  let decByGroup = 0;
  let decByInd = 0;
  let inGroup = false;
  let groupItems = [];
  let currentGroup = 0;
  let itemsToUpdate: Characteristic[] = [];
  let newList: Characteristic[] = [];
  for (let i = 0; i < itemList.length; i++) {
    // item was before any deletions and isn't in a group and wasn't deleted, nothing to change
    if (decByInd === 0 && (itemList[i].markerSubIndex < 0 || itemList[i].markerSubIndex === null) && !deletedIDs.includes(itemList[i].id)) {
      newList.push(itemList[i]);
      continue;
    }

    // item was deleted and wasn't in a group, increment the reducer value
    if (deletedIDs.includes(itemList[i].id) && (itemList[i].markerSubIndex < 0 || itemList[i].markerSubIndex === null)) {
      decByGroup++;
      decByInd++;
      continue;
    }

    // item is in a group
    if (itemList[i].markerSubIndex >= 0 && itemList[i].markerSubIndex !== null) {
      inGroup = true;
      let deletedCount = 0;
      currentGroup = parseInt(itemList[i].markerGroup, 10);
      if (!deletedIDs.includes(itemList[i].id)) {
        // first item in the group wasn't deleted, add it to the list
        groupItems.push(itemList[i]);
      } else {
        // first item was deleted, decrement the index reducer
        deletedCount++;
      }
      while (inGroup) {
        if (itemList[i + 1] && parseInt(itemList[i + 1].markerGroup, 10) === currentGroup) {
          // still in item group
          if (!deletedIDs.includes(itemList[i + 1].id)) {
            // next item in group wasn't deleted
            groupItems.push(itemList[i + 1]);
          } else {
            // next item is deleted, decrement the index reducer
            deletedCount++;
          }
          i++;
        } else {
          // reached the end of the group
          inGroup = false;
          currentGroup = 0;
        }
      }
      if (groupItems.length === 0) {
        // whole group was deleted, increase the reducer value
        decByGroup += 1;
      } else {
        // reduce group of items
        const returnedLists = reduceGroup(groupItems, decByGroup, decByInd, newList, itemsToUpdate);
        newList = returnedLists.items;
        itemsToUpdate = returnedLists.updates;
      }
      // reset item group array to empty
      decByInd += deletedCount;
      groupItems = [];
    } else {
      // item ins't in a group and wasn't deleted and it needs to be decremented
      const newItem = reduceWhole(itemList[i], decByGroup, decByInd);
      newList.push(newItem);
      itemsToUpdate.push(newItem);
    }
  }
  const { changed } = updateIndexNumbers(newList, [], itemsToUpdate);
  // updateDatabase(itemsToUpdate, edit, part);
  return { newList, itemsToUpdate: changed };
};

export const sortCharacteristics = (characteristics: Characteristic[], startGroup: string, startIndex: number) => {
  const sortedList = characteristics.sort(sortChars);
  const newList: Characteristic[] = [];
  let currentIndex = startIndex;
  let currentGroup = parseInt(startGroup, 10);

  for (let i = 0; i < sortedList.length; i++) {
    let newItem = { ...sortedList[i] };
    const subInd = sortedList[i].markerSubIndex;
    if (subInd !== null && subInd >= 0) {
      if (sortedList[i + 1] && sortedList[i + 1].markerGroup === sortedList[i].markerGroup) {
        // a group was part of the moved items, handle the group
        let inGroup = true;
        let subGroupInd = 0;
        const originalGroupNumber = newItem.markerGroup;
        while (inGroup) {
          newItem.markerGroup = currentGroup.toString();
          newItem.markerSubIndex = subGroupInd++;
          newItem.markerLabel = `${newItem.markerGroup}.${newItem.markerSubIndex + 1}`;
          newItem.markerIndex = currentIndex++;
          newList.push(newItem);
          if (!sortedList[i + 1] || sortedList[i + 1].markerGroup !== originalGroupNumber) {
            inGroup = false;
          } else {
            i++;
            newItem = { ...sortedList[i] };
          }
        }
        currentGroup++;
      } else {
        // this was the only item left in the group, ungroup it
        newItem.markerGroup = currentGroup.toString();
        newItem.markerSubIndex = -1;
        newItem.markerGroupShared = false;
        newItem.markerLabel = currentGroup.toString();
        newItem.markerIndex = currentIndex++;
        currentGroup++;
        newList.push(newItem);
      }
    } else {
      newItem.markerGroup = currentGroup.toString();
      currentGroup++;
      newItem.markerSubIndex = -1;
      newItem.markerGroupShared = false;
      newItem.markerLabel = newItem.markerGroup;
      newItem.markerIndex = currentIndex++;
      newList.push(newItem);
    }
  }

  return { subList: newList, nextGroup: currentGroup, nextIndex: currentIndex };
};

// split list into before insertion, selected items, after selection
// renumber selected and after separately and then concat sections back together
export const bulkReorder = (selectedItems: Characteristic[], newGroup: string, itemList: Characteristic[]) => {
  const inactive: Characteristic[] = [];
  const preList: Characteristic[] = [];
  const postList: Characteristic[] = [];
  let currentGroup = 1;

  // split characteristics between before/after
  for (let i = 0; i < itemList.length; i++) {
    if (selectedItems.some((selected) => selected.id === itemList[i].id)) {
      currentGroup += 1;
      continue;
    }
    if (currentGroup < parseInt(newGroup, 10)) {
      if (itemList[i].markerSubIndex >= 0 && itemList[i].markerSubIndex !== null) {
        // this is a group, don't increment the current group until its done
        let inGroup = true;
        while (inGroup && itemList[i]) {
          if (!selectedItems.some((selected) => selected.id === itemList[i].id)) {
            preList.push(itemList[i]);
          }
          if (itemList[i + 1] && itemList[i + 1].markerGroup !== itemList[i].markerGroup) {
            inGroup = false;
            currentGroup += 1;
          } else {
            i += 1;
          }
        }
      } else {
        preList.push(itemList[i]);
        currentGroup += 1;
      }
    } else {
      postList.push(itemList[i]);
    }
  }

  const pre = sortCharacteristics(preList, '1', 0);
  const moved = sortCharacteristics(selectedItems, pre.nextGroup.toString(), pre.nextIndex);
  const post = sortCharacteristics(postList, moved.nextGroup.toString(), moved.nextIndex);
  const regions = sortCharacteristics(inactive, post.nextGroup.toString(), post.nextIndex);
  const editList = moved.subList.concat(pre.subList.concat(post.subList.concat(regions.subList)));
  const fullList = pre.subList.concat(moved.subList.concat(post.subList.concat(regions.subList)));
  const { changed } = updateIndexNumbers(fullList, [], editList);

  return { fullList, editList: changed };
};

export const checkFullGroup = (group: string, selectedItems: Characteristic[], itemList: Characteristic[]) => {
  // check if we're ungrouping a full group
  if (selectedItems[0].markerSubIndex !== 0) {
    // missing at least the first item from the group
    return false;
  }
  for (let i = 0; i < itemList.length; i++) {
    if (itemList[i].markerGroup === group && !selectedItems.some((char) => char.id === itemList[i].id)) {
      // hit an item in the list that's in the group but not selected
      return false;
    }
    if (parseInt(itemList[i].markerGroup, 10) > parseInt(group, 10)) {
      // got through the group in the item list, no need to look further
      break;
    }
  }

  return true;
};

export const ungroupItems = (selectedItems: Characteristic[], itemList: Characteristic[]) => {
  const sorted = selectedItems.sort((a, b) => (a.markerIndex > b.markerIndex ? 1 : -1));
  const oldGroup = sorted[0].markerGroup;
  const remaining = itemList.filter((a) => a.markerGroup === oldGroup && !sorted.some((b) => b.id === a.id));
  const fullGroup = checkFullGroup(oldGroup, sorted, itemList);

  let newGroup = fullGroup ? parseInt(oldGroup, 10) : parseInt(oldGroup, 10) + 1; // starting group # for the ungrouped items
  const nGroup = newGroup.toString(); // remember new group for bulkReorder call
  const newItems: Characteristic[] = []; // list of selected items after being ungrouped
  remaining.forEach((char, index) => {
    const newItem = { ...char };
    newItem.markerGroupShared = remaining.length === 1;
    newItem.markerSubIndex = remaining.length === 1 ? -1 : index;
    newItem.markerLabel = remaining.length === 1 ? oldGroup : `${oldGroup}.${index + 1}`;
    newItems.push(newItem);
  });
  sorted.forEach((char) => {
    const newItem = { ...char };
    newItem.markerGroup = newGroup.toString();
    newItem.markerGroupShared = false;
    newItem.markerSubIndex = -1;
    newItem.markerLabel = newItem.markerGroup;
    newItems.push(newItem);
    newGroup += 1;
  });

  return bulkReorder(newItems, nGroup, itemList);
};

export const groupItems = (selectedItems: Characteristic[], itemList: Characteristic[], shared: boolean) => {
  const sorted = selectedItems.sort((a, b) => (a.markerIndex > b.markerIndex ? 1 : -1));
  const { quantity } = selectedItems.sort((a, b) => b.quantity - a.quantity)[0];
  const fullGroup = selectedItems[0].markerLabel.includes('.') ? checkFullGroup(sorted[0].markerGroup, sorted, itemList) : true;
  const startGroup = fullGroup ? sorted[0].markerGroup : (parseInt(sorted[0].markerGroup, 10) + 1).toString();

  const groupedList: Characteristic[] = [];
  for (let i = 0; i < sorted.length; i++) {
    const newItem = { ...sorted[i] };
    newItem.markerGroup = startGroup;
    newItem.markerSubIndex = i;
    newItem.markerLabel = `${newItem.markerGroup}.${newItem.markerSubIndex}`;
    newItem.markerGroupShared = shared;
    newItem.quantity = quantity;
    groupedList.push(newItem);
  }

  return bulkReorder(groupedList, startGroup, itemList);
};

export const reorderController = (newNumber: string, list: Characteristic[], selected: Characteristic[]) => {
  if (!list || !newNumber || selected.length <= 0) return { newList: undefined, updates: undefined };
  const keys = selected.map((c) => c.id);
  const selectedGroup = [...selected];
  // get whole group
  selected.forEach((s) => {
    if (s.markerSubIndex !== -1 && s.markerSubIndex !== null) {
      const group = list.filter((feature) => feature.markerGroup === s.markerGroup);
      group.forEach((g) => {
        if (!keys.includes(g.id)) {
          keys.push(g.id);
          selectedGroup.push(g);
        }
      });
    }
  });
  const topItem = selectedGroup.sort(sortChars)[0];
  const result = {
    destination: {
      index: parseInt(newNumber, 10) - 1,
    },
    source: {
      index: topItem.markerIndex,
      draggableId: topItem.id,
    },
  };
  let adjustedList;
  if (selectedGroup && selectedGroup.length > 0) {
    adjustedList = dragMultipleToNewIndex(result, [...list, {}], selectedGroup);
  } else {
    adjustedList = moveDragToNewIndex(result, [...list, {}]);
  }
  if (!adjustedList || adjustedList.length === 0) return { newList: undefined, updates: undefined };

  adjustedList = adjustedList.filter((c: Characteristic) => !!c.id);
  const dragged = dragItem(adjustedList);

  return { newList: dragged.adjustedList, updates: dragged.itemsToUpdate };
};
