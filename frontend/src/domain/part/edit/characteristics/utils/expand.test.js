import { defaultUnitIDs, defaultTypeIDs, defaultSubtypeIDs, defaultMethodIDs } from 'view/global/defaults';
import { expandCharacteristic, expandFeatureGroup } from './Expand';

const chars = [
  {
    id: '1',
    drawingSheetIndex: 2,
    status: 'Inactive',
    captureMethod: 'Automated',
    notationType: defaultTypeIDs.Dimension, // Dimension
    notationSubtype: defaultSubtypeIDs.Length, // Length
    notationClass: 'Basic',
    fullSpecification: '3.24 / 3.21',
    quantity: 3,
    nominal: '12.87',
    upperSpecLimit: '12.87',
    lowerSpecLimit: '12.87',
    plusTol: '0',
    minusTol: '0',
    unit: defaultUnitIDs.millimeter,
    toleranceSource: 'Document_Defined',
    inspectionMethod: defaultMethodIDs.Caliper, // Caliper
    criticality: undefined,
    notes: '',
    drawingRotation: 0.0,
    drawingScale: 1.0,
    boxLocationY: 120.5,
    boxLocationX: 120.5,
    boxWidth: 250,
    boxHeight: 100,
    boxRotation: 0.0,
    markerGroup: '1',
    markerGroupShared: false,
    markerIndex: 0,
    markerSubIndex: -1,
    markerLabel: '1',
    markerStyle: 'f1386f09-9c03-4c59-ada9-acb6a16bfbab',
    markerLocationX: 100.5,
    markerLocationY: 120.5,
    markerFontSize: 18,
    markerSize: 36,
    gridCoordinates: '',
    balloonGridCoordinates: '',
    connectionPointGridCoordinates: '',
    verified: false,
    part: {
      id: 'd6c5bc49-9fb2-4fbb-8a61-77bc8d28036f', // belongsTo
    },
    drawing: {
      id: 'c200ce70-72b8-44f7-8d3f-4b93f6d1b491', // belongsTo
    },
    drawingSheet: {
      id: 'd6c5bc49-9fb2-4fbb-8a61-77bc8d380352',
    },
    createdAt: Date.now(),
    updatedAt: Date.now(),
    deletedAt: Date.now(),
  },
  {
    id: '2',
    drawingSheetIndex: 2,
    status: 'Inactive',
    captureMethod: 'Automated',
    notationType: defaultTypeIDs.Dimension, // Dimension
    notationSubtype: defaultSubtypeIDs.Length, // Length
    notationClass: 'Basic',
    fullSpecification: '3.24 / 3.21',
    quantity: 3,
    nominal: '12.87',
    upperSpecLimit: '12.87',
    lowerSpecLimit: '12.87',
    plusTol: '0',
    minusTol: '0',
    unit: defaultUnitIDs.millimeter,
    toleranceSource: 'Document_Defined',
    inspectionMethod: defaultMethodIDs.Caliper, // Caliper
    criticality: undefined,
    notes: '',
    drawingRotation: 0.0,
    drawingScale: 1.0,
    boxLocationY: 120.5,
    boxLocationX: 120.5,
    boxWidth: 250,
    boxHeight: 100,
    boxRotation: 0.0,
    markerGroup: '2',
    markerGroupShared: false,
    markerIndex: 1,
    markerSubIndex: -1,
    markerLabel: '2',
    markerStyle: 'f1386f09-9c03-4c59-ada9-acb6a16bfbab',
    markerLocationX: 100.5,
    markerLocationY: 120.5,
    markerFontSize: 18,
    markerSize: 36,
    gridCoordinates: '',
    balloonGridCoordinates: '',
    connectionPointGridCoordinates: '',
    verified: false,
    part: {
      id: 'd6c5bc49-9fb2-4fbb-8a61-77bc8d28036f', // belongsTo
    },
    drawing: {
      id: 'c200ce70-72b8-44f7-8d3f-4b93f6d1b491', // belongsTo
    },
    drawingSheet: {
      id: 'd6c5bc49-9fb2-4fbb-8a61-77bc8d380352',
    },
    createdAt: Date.now(),
    updatedAt: Date.now(),
    deletedAt: Date.now(),
  },
];
const group = [
  {
    id: '31',
    drawingSheetIndex: 2,
    status: 'Inactive',
    captureMethod: 'Automated',
    notationType: defaultTypeIDs.Dimension, // Dimension
    notationSubtype: defaultSubtypeIDs.Length, // Length
    notationClass: 'Tolerance',
    fullSpecification: '3X 1.23',
    quantity: 3,
    nominal: '1.23',
    upperSpecLimit: '1.24',
    lowerSpecLimit: '1.22',
    plusTol: '0.01',
    minusTol: '0.01',
    unit: defaultUnitIDs.millimeter,
    toleranceSource: 'Document_Defined',
    inspectionMethod: defaultMethodIDs.Caliper, // Caliper
    criticality: undefined,
    notes: '',
    drawingRotation: 0.0,
    drawingScale: 1.0,
    boxLocationY: 120.5,
    boxLocationX: 120.5,
    boxWidth: 250,
    boxHeight: 100,
    boxRotation: 0.0,
    markerGroup: '3',
    markerGroupShared: false,
    markerIndex: 2,
    markerSubIndex: 0,
    markerLabel: '3.1',
    markerStyle: 'f1386f09-9c03-4c59-ada9-acb6a16bfbab',
    markerLocationX: 100.5,
    markerLocationY: 120.5,
    markerFontSize: 18,
    markerSize: 36,
    gridCoordinates: '',
    balloonGridCoordinates: '',
    connectionPointGridCoordinates: '',
    verified: false,
    part: {
      id: 'd6c5bc49-9fb2-4fbb-8a61-77bc8d28036f', // belongsTo
    },
    drawing: {
      id: 'c200ce70-72b8-44f7-8d3f-4b93f6d1b491', // belongsTo
    },
    drawingSheet: {
      id: 'd6c5bc49-9fb2-4fbb-8a61-77bc8d380352',
    },
    createdAt: Date.now(),
    updatedAt: Date.now(),
    deletedAt: Date.now(),
  },
  {
    id: '32',
    drawingSheetIndex: 2,
    status: 'Inactive',
    captureMethod: 'Automated',
    notationType: defaultTypeIDs.Dimension, // Dimension
    notationSubtype: defaultSubtypeIDs.Length, // Length
    notationClass: 'Tolerance',
    fullSpecification: '.45',
    quantity: 3,
    nominal: '.45',
    upperSpecLimit: '.46',
    lowerSpecLimit: '.44',
    plusTol: '0.01',
    minusTol: '0.01',
    unit: defaultUnitIDs.millimeter,
    toleranceSource: 'Document_Defined',
    inspectionMethod: defaultMethodIDs.Caliper, // Caliper
    criticality: undefined,
    notes: '',
    drawingRotation: 0.0,
    drawingScale: 1.0,
    boxLocationY: 120.5,
    boxLocationX: 120.5,
    boxWidth: 250,
    boxHeight: 100,
    boxRotation: 0.0,
    markerGroup: '3',
    markerGroupShared: false,
    markerIndex: 3,
    markerSubIndex: 1,
    markerLabel: '3.2',
    markerStyle: 'f1386f09-9c03-4c59-ada9-acb6a16bfbab',
    markerLocationX: 100.5,
    markerLocationY: 120.5,
    markerFontSize: 18,
    markerSize: 36,
    gridCoordinates: '',
    balloonGridCoordinates: '',
    connectionPointGridCoordinates: '',
    verified: false,
    part: {
      id: 'd6c5bc49-9fb2-4fbb-8a61-77bc8d28036f', // belongsTo
    },
    drawing: {
      id: 'c200ce70-72b8-44f7-8d3f-4b93f6d1b491', // belongsTo
    },
    drawingSheet: {
      id: 'd6c5bc49-9fb2-4fbb-8a61-77bc8d380352',
    },
    createdAt: Date.now(),
    updatedAt: Date.now(),
    deletedAt: Date.now(),
  },
];

describe('The expand utility', () => {
  describe('when expanding with a shared balloon', () => {
    it('Creates new Character Inputs for expanded characteristic with shared balloon', () => {
      const ret = expandCharacteristic(chars[0], chars, true);
      expect(ret.newChars.length).toBe(2);
      expect(ret.newChars[0].markerGroupShared).toBeTruthy();
      expect(ret.newChars[0].markerGroup).toBe('1');
      expect(ret.newChars[0].markerIndex).toBe(1);
      expect(ret.newChars[0].markerSubIndex).toBe(1);
      expect(ret.newChars[0].markerLabel).toBe('1.2');
      expect(ret.newChars[1].markerGroupShared).toBeTruthy();
      expect(ret.newChars[1].markerGroup).toBe('1');
      expect(ret.newChars[1].markerIndex).toBe(2);
      expect(ret.newChars[1].markerSubIndex).toBe(2);
      expect(ret.newChars[1].markerLabel).toBe('1.3');
    });

    it('Returns an Updated List of Characteristics after expanding with a shared', () => {
      const ret = expandCharacteristic(chars[0], chars, true);
      expect(ret.updatedChars.length).toBe(2);

      expect(ret.updatedChars[0].markerGroupShared).toBeTruthy();
      expect(ret.updatedChars[0].markerGroup).toBe('1');
      expect(ret.updatedChars[0].markerIndex).toBe(0);
      expect(ret.updatedChars[0].markerSubIndex).toBe(0);
      expect(ret.updatedChars[0].markerLabel).toBe('1.1');

      expect(ret.updatedChars[1].markerGroupShared).toBeFalsy();
      expect(ret.updatedChars[1].markerGroup).toBe('2');
      expect(ret.updatedChars[1].markerIndex).toBe(3);
      expect(ret.updatedChars[1].markerSubIndex).toBe(-1);
      expect(ret.updatedChars[1].markerLabel).toBe('2');
    });
  });

  describe('when expanding with sub balloons', () => {
    it('creates new character inputs for the expanded characteristics', () => {
      const ret = expandCharacteristic(chars[0], chars, false);
      expect(ret.newChars.length).toBe(2);
      expect(ret.newChars[0].markerGroupShared).toBeFalsy();
      expect(ret.newChars[0].markerGroup).toBe('1');
      expect(ret.newChars[0].markerIndex).toBe(1);
      expect(ret.newChars[0].markerSubIndex).toBe(1);
      expect(ret.newChars[0].markerLabel).toBe('1.2');
      expect(ret.newChars[1].markerGroupShared).toBeFalsy();
      expect(ret.newChars[1].markerGroup).toBe('1');
      expect(ret.newChars[1].markerIndex).toBe(2);
      expect(ret.newChars[1].markerSubIndex).toBe(2);
      expect(ret.newChars[1].markerLabel).toBe('1.3');
    });

    it('Returns an Updated List of Characteristics after expanding', () => {
      const ret = expandCharacteristic(chars[0], chars, false);
      expect(ret.updatedChars.length).toBe(2);
      expect(ret.updatedChars[0].markerGroupShared).toBeFalsy();
      expect(ret.updatedChars[0].markerGroup).toBe('1');
      expect(ret.updatedChars[0].markerIndex).toBe(0);
      expect(ret.updatedChars[0].markerSubIndex).toBe(0);
      expect(ret.updatedChars[0].markerLabel).toBe('1.1');

      expect(ret.updatedChars[1].markerGroupShared).toBeFalsy();
      expect(ret.updatedChars[1].markerGroup).toBe('2');
      expect(ret.updatedChars[1].markerIndex).toBe(3);
      expect(ret.updatedChars[1].markerSubIndex).toBe(-1);
      expect(ret.updatedChars[1].markerLabel).toBe('2');
    });
  });

  describe('when expanding a group', () => {
    it('creates new character inputs for the expanded characteristics', () => {
      const ret = expandFeatureGroup(group, [...chars, ...group], false);
      expect(ret.newChars.length).toBe(4); // 4.1, 4.2, 5.2, & 5.2
      expect(ret.newChars[0].markerGroupShared).toBeFalsy();
      expect(ret.newChars[0].markerGroup).toBe('4');
      expect(ret.newChars[0].markerIndex).toBe(4);
      expect(ret.newChars[0].markerSubIndex).toBe(0);
      expect(ret.newChars[0].markerLabel).toBe('4.1');
      expect(ret.newChars[0].quantity).toBe(1);
      expect(ret.newChars[1].markerGroupShared).toBeFalsy();
      expect(ret.newChars[1].markerGroup).toBe('4');
      expect(ret.newChars[1].markerIndex).toBe(5);
      expect(ret.newChars[1].markerSubIndex).toBe(1);
      expect(ret.newChars[1].markerLabel).toBe('4.2');
      expect(ret.newChars[1].quantity).toBe(1);
      expect(ret.newChars[2].markerGroupShared).toBeFalsy();
      expect(ret.newChars[2].markerGroup).toBe('5');
      expect(ret.newChars[2].markerIndex).toBe(6);
      expect(ret.newChars[2].markerSubIndex).toBe(0);
      expect(ret.newChars[2].markerLabel).toBe('5.1');
      expect(ret.newChars[2].quantity).toBe(1);
      expect(ret.newChars[3].markerGroupShared).toBeFalsy();
      expect(ret.newChars[3].markerGroup).toBe('5');
      expect(ret.newChars[3].markerIndex).toBe(7);
      expect(ret.newChars[3].markerSubIndex).toBe(1);
      expect(ret.newChars[3].markerLabel).toBe('5.2');
      expect(ret.newChars[3].quantity).toBe(1);
    });

    it('Returns an Updated List of Characteristics after expanding group', () => {
      const ret = expandFeatureGroup(group, [...chars, ...group], false);
      expect(ret.updatedChars.length).toBe(2); // 3.1 & 3.2
      expect(ret.updatedChars[0].markerGroupShared).toBeFalsy();
      expect(ret.updatedChars[0].markerLabel).toBe('3.1');
      expect(ret.updatedChars[0].quantity).toBe(1);
      expect(ret.updatedChars[1].markerGroupShared).toBeFalsy();
      expect(ret.updatedChars[1].markerLabel).toBe('3.2');
      expect(ret.updatedChars[1].quantity).toBe(1);
    });
  });
});
