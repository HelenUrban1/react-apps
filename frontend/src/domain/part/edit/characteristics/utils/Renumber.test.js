import * as Renumber from './Renumber';
import { characteristics, listAfterDelete } from './RenumberTestData';

const mockFunction = () => {};

const setChars = characteristics;

describe('Characteristic List Renumbering when', () => {
  describe('Moving an Ungrouped Feature', () => {
    it('Should move an Feature ahead to the Correct Index and Reduce those in between', () => {
      const newNumber = '5';
      const feature = characteristics[0];
      const newArr = Renumber.moveAfter(feature, newNumber, characteristics).newList;
      expect(newArr.length).toEqual(characteristics.length);
      expect(characteristics[0].id).toEqual(newArr[6].id);
      expect(characteristics[1].id).toEqual(newArr[0].id);
      expect(characteristics[2].id).toEqual(newArr[1].id);
      expect(characteristics[3].id).toEqual(newArr[2].id);
      expect(characteristics[4].id).toEqual(newArr[3].id);
      expect(characteristics[5].id).toEqual(newArr[4].id);
    });

    it('Should move an Feature ahead to the Correct Index at the end of the list and Reduce those in between', () => {
      const newNumber = '13';
      const feature = characteristics[0];
      const newArr = Renumber.moveAfter(feature, newNumber, characteristics).newList;
      expect(newArr.length).toEqual(characteristics.length);
      expect(characteristics[0].id).toEqual(newArr[12].id);
      expect(characteristics[1].id).toEqual(newArr[0].id);
      expect(characteristics[2].id).toEqual(newArr[1].id);
      expect(characteristics[3].id).toEqual(newArr[2].id);
      expect(characteristics[4].id).toEqual(newArr[3].id);
      expect(characteristics[5].id).toEqual(newArr[4].id);
      expect(characteristics[6].id).toEqual(newArr[5].id);
      expect(characteristics[7].id).toEqual(newArr[6].id);
      expect(characteristics[8].id).toEqual(newArr[7].id);
      expect(characteristics[9].id).toEqual(newArr[8].id);
      expect(characteristics[10].id).toEqual(newArr[9].id);
      expect(characteristics[11].id).toEqual(newArr[10].id);
      expect(characteristics[12].id).toEqual(newArr[11].id);
    });

    it('Should Renumber the Features between a Moved Ahead Features Start and End', () => {
      const newNumber = '5';
      const feature = characteristics[0];
      const newArr = Renumber.moveAfter(feature, newNumber, characteristics).newList;
      expect(newArr.length).toEqual(characteristics.length);
      expect(newArr[0].markerLabel).toEqual('1.1');
      expect(newArr[1].markerLabel).toEqual('1.2');
      expect(newArr[2].markerLabel).toEqual('1.3');
      expect(newArr[3].markerLabel).toEqual('2');
      expect(newArr[4].markerLabel).toEqual('3');
    });

    it('Should not Change Features After the Insertion Point when Moving Ahead', () => {
      const newNumber = '5';
      const feature = characteristics[0];
      const newArr = Renumber.moveAfter(feature, newNumber, characteristics).newList;
      expect(newArr.length).toEqual(characteristics.length);
      expect(characteristics[7]).toEqual(newArr[7]);
      expect(characteristics[characteristics.length - 1]).toEqual(newArr[newArr.length - 1]);
    });

    it('Should move the Feature backwards to the Correct Index and Increase those in between', () => {
      const newNumber = '1';
      const feature = characteristics[6];
      const newArr = Renumber.moveBefore(feature, newNumber, characteristics).newList;
      expect(newArr.length).toEqual(characteristics.length);
      expect(characteristics[6].id).toEqual(newArr[0].id);
      expect(characteristics[0].id).toEqual(newArr[1].id);
      expect(characteristics[1].id).toEqual(newArr[2].id);
      expect(characteristics[2].id).toEqual(newArr[3].id);
      expect(characteristics[3].id).toEqual(newArr[4].id);
      expect(characteristics[4].id).toEqual(newArr[5].id);
    });

    it('Should Increase the Feature Numbers for Features Between the old and New indexes of the Moved Feature', () => {
      const newNumber = '1';
      const feature = characteristics[6];
      const newArr = Renumber.moveBefore(feature, newNumber, characteristics).newList;
      expect(newArr.length).toEqual(characteristics.length);
      expect(newArr[1].markerLabel).toEqual('2');
      expect(newArr[2].markerLabel).toEqual('3.1');
      expect(newArr[3].markerLabel).toEqual('3.2');
      expect(newArr[4].markerLabel).toEqual('3.3');
      expect(newArr[5].markerLabel).toEqual('4');
    });

    it('Should not Change the Features before the new index, or after the old', () => {
      const newNumber = '3';
      const feature = characteristics[7];
      const newArr = Renumber.moveBefore(feature, newNumber, characteristics).newList;
      expect(newArr.length).toEqual(characteristics.length);
      expect(characteristics[0]).toEqual(newArr[0]);
      expect(characteristics[3]).toEqual(newArr[3]);
      expect(characteristics[8]).toEqual(newArr[8]);
      expect(characteristics[characteristics.length - 1]).toEqual(newArr[newArr.length - 1]);
    });
  });

  describe('Moving a Grouped Feature', () => {
    it('Should Ungroup the Moved Feature if Ungrouped Before', () => {
      const newNumber = '1';
      const feature = characteristics[2]; // 2.2
      const newArr = Renumber.ungroupBefore(feature, newNumber, characteristics).newList;
      expect(newArr.length).toEqual(characteristics.length);
      expect(characteristics[2].id).toEqual(newArr[0].id);
      expect(newArr[0].markerLabel).toEqual('1');
    });

    it('Should Renumber the Remaining Group Features after Ungrouping Before', () => {
      const newNumber = '1';
      const feature = characteristics[2]; // 2.2
      const newArr = Renumber.ungroupBefore(feature, newNumber, characteristics).newList;
      expect(newArr.length).toEqual(characteristics.length);
      expect(newArr[2].id).toEqual(characteristics[1].id);
      expect(newArr[2].markerSubIndex).toEqual(0);
      expect(newArr[2].markerLabel).toEqual('3.1');
      expect(newArr[3].id).toEqual(characteristics[3].id);
      expect(newArr[3].markerSubIndex).toEqual(1);
      expect(newArr[3].markerLabel).toEqual('3.2');
    });

    it('Should Destroy the Group if only 1 Feature is Left after Ungrouping Before', () => {
      const newNumber = '1';
      const feature = characteristics[2]; // 2.2
      const tempArr = Renumber.ungroupBefore(feature, newNumber, characteristics).newList;
      const secondItem = tempArr[3]; // 3.3
      const newArr = Renumber.ungroupBefore(secondItem, newNumber, tempArr).newList;
      expect(newArr.length).toEqual(characteristics.length);
      expect(newArr[0].id).toEqual(characteristics[3].id);
      expect(newArr[1].id).toEqual(characteristics[2].id);
      expect(newArr[3].id).toEqual(characteristics[1].id);
      expect(newArr[2].id).toEqual(characteristics[0].id);
      expect(newArr[4].id).toEqual(characteristics[4].id);
      expect(newArr[3].markerSubIndex).toEqual(-1);
      expect(newArr[3].markerLabel).toEqual('4');
      expect(newArr[0].markerSubIndex).toEqual(-1);
      expect(newArr[0].markerLabel).toEqual('1');
      expect(newArr[1].markerSubIndex).toEqual(-1);
      expect(newArr[1].markerLabel).toEqual('2');
      expect(newArr[2].markerSubIndex).toEqual(null);
      expect(newArr[2].markerLabel).toEqual('3');
    });

    it('Should Ungroup the Moved Feature if Ungrouped After', () => {
      const newNumber = '4';
      const feature = characteristics[2]; // 2.2
      const newArr = Renumber.ungroupAfter(feature, newNumber, characteristics).newList;
      expect(newArr.length).toEqual(characteristics.length);
      expect(characteristics[2].id).toEqual(newArr[4].id);
      expect(newArr[4].markerLabel).toEqual('4');
      expect(newArr[4].markerSubIndex).toEqual(-1);
    });

    it('Should Renumber the Remaining Group Features after Ungrouping After', () => {
      const newNumber = '4';
      const feature = characteristics[2]; // 2.2
      const newArr = Renumber.ungroupAfter(feature, newNumber, characteristics).newList;
      expect(newArr.length).toEqual(characteristics.length);
      expect(newArr[1].id).toEqual(characteristics[1].id);
      expect(newArr[1].markerSubIndex).toEqual(0);
      expect(newArr[1].markerLabel).toEqual('2.1');
      expect(newArr[2].id).toEqual(characteristics[3].id);
      expect(newArr[2].markerSubIndex).toEqual(1);
      expect(newArr[2].markerLabel).toEqual('2.2');
    });

    it('Should Destroy the Group if only 1 Feature is Left after Ungrouping After', () => {
      const newNumber = '4';
      const feature = characteristics[2]; // 2.2
      const tempArr = Renumber.ungroupAfter(feature, newNumber, characteristics).newList;
      const secondItem = tempArr[1]; // 2.1
      const newArr = Renumber.ungroupAfter(secondItem, newNumber, tempArr).newList;
      expect(newArr.length).toEqual(characteristics.length);
      expect(newArr[0].id).toEqual(characteristics[0].id);
      expect(newArr[1].id).toEqual(characteristics[3].id);
      expect(newArr[2].id).toEqual(characteristics[4].id);
      expect(newArr[3].id).toEqual(characteristics[1].id);
      expect(newArr[4].id).toEqual(characteristics[2].id);
      expect(newArr[3].markerSubIndex).toEqual(-1);
      expect(newArr[3].markerLabel).toEqual('4');
      expect(newArr[0].markerSubIndex).toEqual(null);
      expect(newArr[0].markerLabel).toEqual('1');
      expect(newArr[1].markerSubIndex).toEqual(-1);
      expect(newArr[1].markerLabel).toEqual('2');
      expect(newArr[2].markerSubIndex).toEqual(null);
      expect(newArr[2].markerLabel).toEqual('3');
      expect(newArr[4].markerSubIndex).toEqual(-1);
      expect(newArr[4].markerLabel).toEqual('5');
    });

    it('Should Increase Group Numbers after the Group when Ungrouping an Feature', () => {
      const newNumber = '4';
      const feature = characteristics[2]; // 2.2
      const newArr = Renumber.ungroupAfter(feature, newNumber, characteristics).newList;
      expect(newArr.length).toEqual(characteristics.length);
      expect(newArr[5].markerGroup).toEqual('5');
      expect(newArr[newArr.length - 1].markerGroup).toEqual((parseInt(characteristics[characteristics.length - 1].markerLabel) + 1).toString());
    });
  });

  describe('Deleting an Feature', () => {
    it('Should delete an Feature from the List', () => {
      const feature = characteristics[1];
      const newArr = Renumber.renumberAfterDelete([feature.id], characteristics);
      expect(newArr.newList.length).toEqual(characteristics.length - 1);
      expect(newArr.newList[1]).not.toEqual(feature);
    });

    it('Should Renumber the Features After the Deletion Point when Deleting Ungrouped', () => {
      const feature = characteristics[0];
      const newArr = Renumber.renumberAfterDelete([feature.id], characteristics);
      expect(newArr.newList.length).toEqual(characteristics.length - 1);
      for (let i = 0; i < newArr.length; i++) {
        expect(newArr.newList[i].markerGroup).toEqual((parseInt(characteristics[i + 1].markerGroup) - 1).toString());
        expect(newArr.newList[i].markerIndex).toEqual(characteristics[i + 1].markerIndex - 1);
      }
    });

    it('Should only decrease the index of following features if a group member was deleted', () => {
      const feature = characteristics[1]; // 2.1
      const newArr = Renumber.renumberAfterDelete([feature.id], characteristics);
      expect(newArr.newList.length).toEqual(characteristics.length - 1);
      for (let i = 1; i < newArr.length; i++) {
        expect(newArr.newList[i].markerGroup).toEqual(characteristics[i + 1].markerGroup);
        expect(newArr.newList[i].markerIndex).toEqual(characteristics[i + 1].markerIndex - 1);
      }
    });

    it('Should decrease the sub-index of the following group members', () => {
      const feature = characteristics[1]; // 2.1
      const newArr = Renumber.renumberAfterDelete([feature.id], characteristics);
      expect(newArr.newList.length).toEqual(characteristics.length - 1);
      expect(newArr.newList[1].markerSubIndex).toEqual(0);
      expect(newArr.newList[1].markerLabel).toEqual('2.1');
      expect(newArr.newList[2].markerSubIndex).toEqual(1);
      expect(newArr.newList[2].markerLabel).toEqual('2.2');
    });

    it('Should decrease following features by 1 marker group and 3 indexes when deleting an entire 3 feature group', () => {
      const newArr = Renumber.renumberAfterDelete([characteristics[1].id, characteristics[2].id, characteristics[3].id], characteristics);
      expect(newArr.newList.length).toEqual(10);
      for (let i = 1; i < newArr.length; i++) {
        expect(newArr[i].markerGroup).toEqual((parseInt(characteristics[i + 3].markerGroup) - 1).toString());
        expect(newArr[i].markerIndex).toEqual(characteristics[i + 3].markerIndex - 3);
      }
    });

    it('Should renumber as needed when deleting an arbitrary set of features', () => {
      const newArr = Renumber.renumberAfterDelete([characteristics[1].id, characteristics[7].id, characteristics[8].id, characteristics[11].id], characteristics);
      expect(newArr.newList.length).toEqual(9);
      expect(newArr.newList).toEqual(listAfterDelete);
    });
  });

  describe('The Sort Characteristics Function', () => {
    it('Should sort and renumber an arbitrary set of features', () => {
      const clonedCharacteristics = JSON.parse(JSON.stringify(characteristics));
      const newArr = Renumber.sortCharacteristics([clonedCharacteristics[1], clonedCharacteristics[7], clonedCharacteristics[9], clonedCharacteristics[4]], '1', 0);
      expect(newArr.subList.length).toEqual(4);
      expect(newArr.subList[0].markerGroup).toEqual('1');
      expect(newArr.subList[1].markerGroup).toEqual('2');
      expect(newArr.subList[2].fullSpecification).toEqual(characteristics[7].fullSpecification);
      expect(newArr.subList[3].markerIndex).toEqual(3);
      expect(newArr.nextGroup).toEqual(5);
      expect(newArr.nextIndex).toEqual(4);
    });
  });

  describe('Should renumber multiple features', () => {
    it('when the selected features are not in order', () => {
      const clonedCharacteristics = JSON.parse(JSON.stringify(characteristics));
      const res = Renumber.bulkReorder([clonedCharacteristics[7], clonedCharacteristics[8], clonedCharacteristics[9]], '4', clonedCharacteristics);
      expect(res.fullList[5].id).toEqual(characteristics[7].id);
      expect(res.fullList[6].id).toEqual(characteristics[8].id);
      expect(res.fullList[7].id).toEqual(characteristics[9].id);
      expect(res.fullList[10].id).toEqual(characteristics[10].id);
      expect(res.fullList[11].id).toEqual(characteristics[11].id);
    });

    it('when a group member is part of the selected features', () => {
      const clonedCharacteristics = JSON.parse(JSON.stringify(characteristics));
      const res = Renumber.bulkReorder(
        //
        [clonedCharacteristics[2], clonedCharacteristics[9], clonedCharacteristics[11]],
        '5',
        clonedCharacteristics,
      );
      expect(res.fullList[1].markerLabel).toEqual('2.1');
      expect(res.fullList[2].markerLabel).toEqual('2.2');
      expect(res.fullList[3].markerLabel).toEqual('3');
      expect(res.fullList[4].markerLabel).toEqual('4');
      expect(res.fullList[5].markerLabel).toEqual('5');
      expect(res.fullList[6].fullSpecification).toEqual(characteristics[9].fullSpecification);
      expect(res.fullList[7].id).toEqual(characteristics[11].id);
    });
  });

  describe('Should renumber entire groups', () => {
    it('when they are multi-selected', () => {
      const clonedCharacteristics = JSON.parse(JSON.stringify(characteristics));
      const res = Renumber.reorderController('5', clonedCharacteristics, [clonedCharacteristics[1], clonedCharacteristics[2], clonedCharacteristics[3]]);
      const sorted = res.newList.sort((a, b) => (a.markerIndex < b.markerIndex ? -1 : 1));
      expect(sorted[4].markerLabel).toEqual('5.1');
      expect(sorted[5].markerLabel).toEqual('5.2');
      expect(sorted[6].markerLabel).toEqual('5.3');
    });
  });

  describe('Dragging card to new location', () => {
    const singleMoveList = [
      { id: 2, markerGroup: '2', markerLabel: '2', markerIndex: 1, markerSubIndex: -1 },
      { id: 1, markerGroup: '1', markerLabel: '1', markerIndex: 0, markerSubIndex: -1 },
      { id: 3, markerGroup: '3', markerLabel: '3.1', markerIndex: 2, markerSubIndex: 0 },
      { id: 4, markerGroup: '3', markerLabel: '3.2', markerIndex: 3, markerSubIndex: 1 },
    ];
    const groupMoveList = [
      { id: 1, markerGroup: '1', markerLabel: '1', markerIndex: 0, markerSubIndex: -1 },
      { id: 3, markerGroup: '3', markerLabel: '3.1', markerIndex: 2, markerSubIndex: 0 },
      { id: 4, markerGroup: '3', markerLabel: '3.2', markerIndex: 3, markerSubIndex: 1 },
      { id: 2, markerGroup: '2', markerLabel: '2', markerIndex: 1, markerSubIndex: -1 },
    ];

    it('return full list of adjusted features', () => {
      const { adjustedList } = Renumber.dragItem(singleMoveList);
      expect(adjustedList).toEqual([
        { id: 2, markerGroup: '1', markerLabel: '1', markerIndex: 0, markerSubIndex: -1 },
        { id: 1, markerGroup: '2', markerLabel: '2', markerIndex: 1, markerSubIndex: -1 },
        { id: 3, markerGroup: '3', markerLabel: '3.1', markerIndex: 2, markerSubIndex: 0 },
        { id: 4, markerGroup: '3', markerLabel: '3.2', markerIndex: 3, markerSubIndex: 1 },
      ]);
    });
    it('return list of changed feature that need to be updated', () => {
      const { itemsToUpdate } = Renumber.dragItem(groupMoveList);
      expect(itemsToUpdate).toEqual([
        { id: 3, markerGroup: '2', markerLabel: '2.1', markerIndex: 1, markerSubIndex: 0 },
        { id: 4, markerGroup: '2', markerLabel: '2.2', markerIndex: 2, markerSubIndex: 1 },
        { id: 2, markerGroup: '3', markerLabel: '3', markerIndex: 3, markerSubIndex: -1 },
      ]);
    });
  });

  describe('Controller', () => {
    it('should bulk reorder when multiple features are selected', () => {
      const { newList, updates } = Renumber.reorderController('1', characteristics, [characteristics[5], characteristics[4]]);
      expect(newList.length).toEqual(characteristics.length);
      expect(characteristics[0].id).toEqual(newList[2].id);
      expect(characteristics[1].id).toEqual(newList[3].id);
      expect(characteristics[2].id).toEqual(newList[4].id);
      expect(characteristics[3].id).toEqual(newList[5].id);
      expect(characteristics[4].id).toEqual(newList[0].id);
      expect(characteristics[5].id).toEqual(newList[1].id);

      expect(updates.length).toEqual(6); // only update changed characteristics
      expect(characteristics[0].id).toEqual(updates[2].id);
      expect(characteristics[1].id).toEqual(updates[3].id);
      expect(characteristics[2].id).toEqual(updates[4].id);
      expect(characteristics[3].id).toEqual(updates[5].id);
      expect(characteristics[4].id).toEqual(updates[0].id);
      expect(characteristics[5].id).toEqual(updates[1].id);
    });

    it('should move all features in a group if a sub-feature is moved', () => {
      const { newList, updates } = Renumber.reorderController('1', characteristics, [characteristics[2]]);
      expect(newList.length).toEqual(characteristics.length);
      expect(characteristics[0].id).toEqual(newList[3].id);
      expect(characteristics[1].id).toEqual(newList[0].id);
      expect(characteristics[2].id).toEqual(newList[1].id);
      expect(characteristics[3].id).toEqual(newList[2].id);

      expect(updates.length).toEqual(4); // only update changed characteristics
      expect(characteristics[0].id).toEqual(updates[3].id);
      expect(characteristics[1].id).toEqual(updates[0].id);
      expect(characteristics[2].id).toEqual(updates[1].id);
      expect(characteristics[3].id).toEqual(updates[2].id);
    });

    it('should call the correct function when a single feature is moved forward in the list', () => {
      const { newList, updates } = Renumber.reorderController('2', characteristics, [characteristics[0]]);
      expect(newList.length).toEqual(characteristics.length);
      expect(characteristics[0].id).toEqual(newList[3].id);
      expect(characteristics[1].id).toEqual(newList[0].id);
      expect(characteristics[2].id).toEqual(newList[1].id);
      expect(characteristics[3].id).toEqual(newList[2].id);
      expect(updates.length).toEqual(4);
      expect(characteristics[0].id).toEqual(updates[3].id);
      expect(characteristics[1].id).toEqual(updates[0].id);
      expect(characteristics[2].id).toEqual(updates[1].id);
      expect(characteristics[3].id).toEqual(updates[2].id);
    });

    it('should call the correct function when a single feature is moved backward in the list', () => {
      const { newList, updates } = Renumber.reorderController('1', characteristics, [characteristics[4]]);
      expect(newList.length).toEqual(characteristics.length);
      expect(characteristics[0].id).toEqual(newList[1].id);
      expect(characteristics[1].id).toEqual(newList[2].id);
      expect(characteristics[2].id).toEqual(newList[3].id);
      expect(characteristics[3].id).toEqual(newList[4].id);
      expect(characteristics[4].id).toEqual(newList[0].id);
      expect(updates.length).toEqual(5);
      expect(characteristics[0].id).toEqual(updates[1].id);
      expect(characteristics[1].id).toEqual(updates[2].id);
      expect(characteristics[2].id).toEqual(updates[3].id);
      expect(characteristics[3].id).toEqual(updates[4].id);
      expect(characteristics[4].id).toEqual(updates[0].id);
    });

    it('should return null if missing a parameter', () => {
      const { newList, updates } = Renumber.reorderController('1', [], []);
      expect(newList).toBeUndefined();
      expect(updates).toBeUndefined();
    });
  });
});
