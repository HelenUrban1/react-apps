import { Buffer } from 'buffer';
const { fabric } = require('fabric');
const { createCanvas, loadImage } = require('canvas');

export function getRotatedCoords(left, top, width, height, angle) {
  let newWidth = width;
  let newHeight = height;

  // get the rectangle width and height, adjusted for the angle of the dimension
  if (angle === 90) {
    newHeight = width;
    newWidth = height;
  } else if (angle === 270) {
    newHeight = width;
    newWidth = height;
  }
  // cheat and use fabric to get the coordinates of the other corners
  // given the top left corner, width, height, and combined angle of the pdf and dimension
  const box = new fabric.Rect({
    left,
    top,
    newWidth,
    newHeight,
    angle,
  });
  // calculate the corners without rendering the object to a canvas
  box.setCoords();

  const minX = Math.round(Math.min(box.aCoords.tl.x, box.aCoords.bl.x, box.aCoords.tr.x, box.aCoords.br.x));
  const minY = Math.round(Math.min(box.aCoords.tl.y, box.aCoords.bl.y, box.aCoords.tr.y, box.aCoords.br.y));
  const center = box.getCenterPoint();

  // return lowest x and y, center x and y, and the adjusted width and height of the dimension
  return [minX, minY, Math.round(center.x), Math.round(center.y), Math.round(newWidth), Math.round(newHeight)];
}

export function convertPDFTronCoords(x, y, width, height, pageWidth, pageHeight, rotation) {
  // PDFTron's origin is the bottom left
  let nX = x;
  let nY = y;
  let nX2 = x + width;
  let nY2 = y + height;
  switch (rotation) {
    case 1: // 90 Degrees
      nY = x;
      nX = y;
      nX2 = nX + height;
      nY2 = nY + width;
      break;
    case 2: // 180 Degrees (Only a guess, since I don't have a sample to test against)
      nX = pageWidth - x;
      nX2 = pageWidth - (x + width);
      break;
    case 3: // 270 Degrees (Only a guess, since I don't have a sample to test against)
      nX = pageHeight - y;
      nY = pageWidth - x;
      nX2 = pageHeight - (y + height);
      nY2 = pageWidth - (x + width);
      break;
    default:
      // 0 Degrees
      nY = pageHeight - y;
      nY2 = pageHeight - (y + height);
  }
  return { nX, nY, nX2, nY2 };
}

export function removeLines(imgData, width, height, isGdtOrBasic, onlyExtract, extractionMode, data) {
  if (!imgData) return imgData;

  let blackPixels = [];
  let row;
  let col;
  let index;
  let x;
  const tempData = data || new ImageData(new Uint8ClampedArray(imgData.data), imgData.width, imgData.height);
  let firstRow = -1;

  // Remove horizontal lines
  for (row = 0; row < height; row++) {
    // column
    for (col = 0; col < width; col++) {
      index = row * (width * 4) + col * 4;
      if ((tempData.data[index] < 200 || tempData.data[index + 1] < 200 || tempData.data[index + 2] < 200) && tempData.data[index + 3] > 0) {
        blackPixels.push(index);
      }
    }
    // If 75% of the row is black we assume the row has a line/box in it
    if (blackPixels.length > parseFloat(width * 0.75).toFixed(0)) {
      for (x = 0; x < blackPixels.length; x++) {
        tempData.data[blackPixels[x]] = 255;
        tempData.data[blackPixels[x] + 1] = 255;
        tempData.data[blackPixels[x] + 2] = 255;
        tempData.data[blackPixels[x] + 3] = 255;
      }
      if (isGdtOrBasic === false && !onlyExtract && extractionMode !== 'text') {
        // track the number of lines and make sure that at least 2 lines are a certain number of rows apart
        if (firstRow < 0) {
          firstRow = x;
        } else {
          // if(x > firstRow + 2)

          isGdtOrBasic = true;
        }
      }
    }
    blackPixels = [];
  }
  return { data: tempData, isGdtOrBasic };
}

export async function asyncRemoveLines(imgData, width, height, isGdtOrBasic, onlyExtract, extractionMode, data) {
  if (!imgData) {
    return imgData;
  }
  width = Math.floor(width);
  height = Math.floor(height);
  let blackPixels = [];
  let row = 0;
  let index;
  let x;
  const tempData = data || new ImageData(new Uint8ClampedArray(imgData.data), imgData.width, imgData.height);
  let firstRow = -1;

  if (!tempData.data) {
    return null;
  }

  return await new Promise(async (resolve, reject) => {
    try {
      // Remove horizontal lines
      if (row === height || !height) return resolve({ data: tempData, isGdtOrBasic });
      const rowLoop = async () => {
        row++;
        let col = 0;
        await new Promise(async (resolve, reject) => {
          try {
            if (col === width || !width) return resolve();

            const colLoop = async () => {
              col++;

              index = row * (width * 4) + col * 4;

              if ((tempData.data[index] < 200 || tempData.data[index + 1] < 200 || tempData.data[index + 2] < 200) && tempData.data[index + 3] > 0) {
                blackPixels.push(index);
              }

              if (col === width) resolve();
              else colLoop();
            };

            colLoop();
          } catch (e) {
            reject(e);
          }
        });

        // If 75% of the row is black we assume the row has a line/box in it
        if (blackPixels.length > parseFloat(width * 0.75).toFixed(0)) {
          let x = 0;

          await new Promise(async (resolve, reject) => {
            try {
              if (x === blackPixels.length || !blackPixels.length) return resolve();

              const xLoop = async () => {
                x++;

                tempData.data[blackPixels[x]] = 255;

                tempData.data[blackPixels[x] + 1] = 255;

                tempData.data[blackPixels[x] + 2] = 255;

                tempData.data[blackPixels[x] + 3] = 255;

                if (x === blackPixels.length) resolve();
                else xLoop();
              };

              xLoop();
            } catch (e) {
              reject(e);
            }
          });

          if (isGdtOrBasic === false && !onlyExtract && extractionMode !== 'text') {
            // track the number of lines and make sure that at least 2 lines are a certain number of rows apart

            if (firstRow < 0) {
              firstRow = x;
            } else {
              // if(x > firstRow + 2)

              isGdtOrBasic = true;
            }
          }
        }

        blackPixels = [];
        if (row === height) resolve({ data: tempData, isGdtOrBasic });
        else rowLoop();
      };
      rowLoop();
    } catch (e) {
      reject(e);
    }
  });
}

export function grayscale(pixels) {
  const d = pixels.data;
  for (let i = 0; i < d.length; i += 4) {
    const r = d[i];
    const g = d[i + 1];
    const b = d[i + 2];
    // CIE luminance for the RGB
    const v = 0.2126 * r + 0.7152 * g + 0.0722 * b;
    d[i] = d[i + 1] = d[i + 2] = v;
  }
  return sharpen(pixels);
}

export function sharpen(pixels) {
  const operator = [0, -1, 0, -1.5, 6, -1.5, 0, -1, 0];

  return convolution(pixels, operator);
}

export function convolution(pixels, weights) {
  const side = Math.round(Math.sqrt(weights.length));
  const halfSide = Math.floor(side / 2);
  const src = pixels.data;
  const canvasWidth = pixels.width;
  const canvasHeight = pixels.height;
  const temporaryCanvas = createCanvas(canvasWidth, canvasHeight);
  const temporaryCtx = temporaryCanvas.getContext('2d');
  const outputData = temporaryCtx.createImageData(canvasWidth, canvasHeight);

  for (let y = 0; y < canvasHeight; y++) {
    for (let x = 0; x < canvasWidth; x++) {
      const dstOff = (y * canvasWidth + x) * 4;
      let sumReds = 0;
      let sumGreens = 0;
      let sumBlues = 0;

      for (let kernelY = 0; kernelY < side; kernelY++) {
        for (let kernelX = 0; kernelX < side; kernelX++) {
          const currentKernelY = y + kernelY - halfSide;

          const currentKernelX = x + kernelX - halfSide;

          if (currentKernelY >= 0 && currentKernelY < canvasHeight && currentKernelX >= 0 && currentKernelX < canvasWidth) {
            const offset = (currentKernelY * canvasWidth + currentKernelX) * 4;

            const weight = weights[kernelY * side + kernelX];

            sumReds += src[offset] * weight;

            sumGreens += src[offset + 1] * weight;

            sumBlues += src[offset + 2] * weight;
          }
        }
      }
      outputData.data[dstOff] = sumReds;
      outputData.data[dstOff + 1] = sumGreens;
      outputData.data[dstOff + 2] = sumBlues;
      outputData.data[dstOff + 3] = 255;
    }
  }
  return outputData;
}

export async function createPage({ page, instance, documentUrl, pdf }) {
  if (!instance) {
    throw new Error('Cannot create page from null');
  }
  let doc = pdf;
  if (!pdf || pdf === null) {
    doc = await instance.Core.PDFNet.PDFDoc.createFromURL(documentUrl);
    doc.initSecurityHandler();
    doc.lock();
  }

  const itr = await doc.getPageIterator(page);
  const currentPage = await itr.current();
  const pdfRotation = await currentPage.getRotation();
  const pageHeight = await currentPage.getPageHeight();
  const pageWidth = await currentPage.getPageWidth();
  return { sheet: currentPage, width: pageWidth, height: pageHeight, rotation: pdfRotation };
}

export function readUploadedFileAsText(inputFile) {
  const temporaryFileReader = new FileReader();

  return new Promise((resolve, reject) => {
    temporaryFileReader.onerror = () => {
      temporaryFileReader.abort();
      reject(new DOMException('Problem parsing input file.'));
    };

    temporaryFileReader.onloadend = () => {
      resolve(temporaryFileReader.result);
    };
    temporaryFileReader.readAsDataURL(inputFile);
  });
}

export function getAngledCapture(width, height, rotation, image, dimWidth, dimHeight) {
  // Create the "Captured Image" canvas
  const capturedImageCanvas = createCanvas(width, height);
  const capturedImageContext = capturedImageCanvas.getContext('2d'); // Rotate the Captured Image canvas to match the rotation of the dimension and the pdf
  capturedImageContext.translate(width / 2, height / 2);
  capturedImageContext.rotate((-rotation * Math.PI) / 180);
  capturedImageContext.translate(-width / 2, -height / 2); // Place the rotated image into a holder canvas, so it will be upright and can be grabbed by getImageData
  const holderCanvas = createCanvas(width, height);
  const holderContext = holderCanvas.getContext('2d');
  holderContext.putImageData(image, 0, 0);
  capturedImageContext.drawImage(holderContext.canvas, 0, 0); // Get the size scale between the bitmap and the canvas
  const diagonal = Math.sqrt(dimWidth ** 2 + dimHeight ** 2);
  const newScale = width / diagonal; // crop the extra buffer area we made at the beginning
  const differenceX = Math.floor((width - dimWidth * newScale) / 2);
  const differenceY = Math.floor((height - dimHeight * newScale) / 2); // return just the dimension image
  return capturedImageContext.getImageData(differenceX, differenceY, dimWidth * newScale, dimHeight * newScale);
}

export function rotateVerticalCapture(image) {
  // Create the "Captured Image" canvas
  const rotatedImageCanvas = createCanvas(image.height, image.width);
  const rotatedImageContext = rotatedImageCanvas.getContext('2d');
  // Rotate the Captured Image canvas to match the rotation of the dimension and the pdf
  rotatedImageContext.translate(image.height / 2, image.width / 2);
  rotatedImageContext.rotate((90 * Math.PI) / 180);
  rotatedImageContext.translate(-image.width / 2, -image.height / 2);
  // Place the rotated image into a holder canvas, so it will be upright and can be grabbed by getImageData
  const holderCanvas = createCanvas(image.width, image.height);
  const holderContext = holderCanvas.getContext('2d');
  holderContext.putImageData(image, 0, 0);
  rotatedImageContext.drawImage(holderContext.canvas, 0, 0);

  const img = new Image();
  img.src = rotatedImageCanvas.toDataURL('image/png');
  const dataURL = rotatedImageCanvas.toDataURL('image/png');

  return rotatedImageContext.getImageData(0, 0, image.height, image.width);
}

export function getHorizontalCapture(img, width, height) {
  const canvas = createCanvas(width, height);
  const context = canvas.getContext('2d');
  context.drawImage(img, 0, 0);
  return context.getImageData(0, 0, width, height);
}

export async function getCroppingRectangle({ passedInstance, dim, page, single, test }) {
  let rect = new passedInstance.Core.PDFNet.Rect();

  // if (!!test) {
  //     rect = new passedInstance.Core.PDFNet.Rect();
  // } else {
  //     // always seems to be undefined
  //     rect = await passedInstance.Rect.init(0, 0, 0, 0);
  // }
  if (single) {
    // await rect.set(0, Math.round(page.height), Math.round(page.width), 0);
    rect = new passedInstance.Core.PDFNet.Rect(0, Math.round(page.height), Math.round(page.width), 0);
  } else if (dim.rotation > 0) {
    const { nX, nY, nX2, nY2 } = convertPDFTronCoords(dim.cx, dim.cy, dim.width, dim.height, page.width, page.height, page.rotation);
    const diagonal = Math.sqrt(dim.width ** 2 + dim.height ** 2);
    const blcx = nX - diagonal / 2;
    const blcy = nY - diagonal / 2;
    const trcx = blcx + diagonal;
    const trcy = blcy + diagonal;
    rect = new passedInstance.Core.PDFNet.Rect(blcx, blcy, trcx, trcy);
  } else {
    const { nX, nY, nX2, nY2 } = convertPDFTronCoords(dim.x1, dim.y1, dim.width, dim.height, page.width, page.height, page.rotation);
    rect = new passedInstance.Core.PDFNet.Rect(nX, nY, nX2, nY2);
  }
  return rect;
}

export async function captureImage({ x, y, width, height, rotation, sheet, instance, documentUrl, pdf, single }) {
  if (!instance) {
    throw new Error('PDFTron instance not passed');
  }
  let pdfDraw = await instance.Core.PDFNet.PDFDraw.create(300);
  if (!pdf || pdf === null) {
    pdfDraw = await instance.Core.PDFNet.PDFDraw.create(300);
  } else {
    // this always seems to be undefined?
    // pdfDraw = await instance.PDFDraw.create(300);
  }

  const page = await createPage({ page: sheet, instance, documentUrl, pdf });

  await pdfDraw.setAntiAliasing(false);
  await pdfDraw.setImageSmoothing(true, true);

  // if not !pdf, the base64data variable was still a Uint8Array when trying to pass into loadImage
  // TODO: follow up with Devin or Blaise about the pdf variable - should be right, but causing errors
  let degrees = rotation;
  if (degrees >= 360) degrees -= 360;
  if (degrees < 0) degrees += 360;

  let newImage;
  const [x1, y1, cx, cy, rectangleWidth, rectangleHeight] = getRotatedCoords(x, y, width, height, degrees);
  const rect = await getCroppingRectangle({
    passedInstance: instance,
    dim: { x1, y1, cx, cy, width: rectangleWidth, height: rectangleHeight, rotation: degrees },
    page,
    single,
    test: !pdf,
  });

  await pdfDraw.setClipRect(rect);

  let bitmap = pdfDraw.getBitmap(page.sheet, 0, false);
  const pngBuffer = await pdfDraw.exportStream(page.sheet);
  let base64Data = new Buffer.from(pngBuffer);

  if (!pdf) {
    base64Data = await readUploadedFileAsText(new Blob([pngBuffer], { type: 'image/png' }));
  }

  // if not !pdf, the base64data variable was still a Uint8Array when trying to pass into loadImage
  // TODO: follow up with Devin or Blaise about the pdf variable - should be right, but causing errors
  base64Data = await readUploadedFileAsText(new Blob([pngBuffer], { type: 'image/png' }));

  const img = await loadImage(base64Data);

  const imageCropped = getHorizontalCapture(img, bitmap.data_.width, bitmap.data_.height);

  if (degrees > 1000) {
    newImage = getAngledCapture(bitmap.data_.width, bitmap.data_.height, degrees, imageCropped, rectangleWidth, rectangleHeight);
  } else {
    newImage = imageCropped;
  }

  if (newImage.width * 1.2 < newImage.height && rotation % 90 === 0) {
    newImage = rotateVerticalCapture(newImage);
  }

  bitmap = null;
  return newImage;
  // return grayscale(newImage);
}
