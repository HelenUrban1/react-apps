import { Grid } from '../../grid/Grid';
import { coordRotation, getPolarFromCartesian, getCartesianFromPolar, getSanitizedDimensions, getCaptureCoordinatesInViewer, updateBalloonPosition, gridPosition, getAnnotationTextCoords } from './coordTransforms';
import { testFabRect, testCharacteristic } from './extractUtilTestData';

describe('Coordinate Transform Functions', () => {
  it('Rotates coordinates correctly', () => {
    const startX = 35;
    const startY = 35;
    const width = 400;
    const height = 400;

    let newCoords = coordRotation(startX, startY, 90, width, height);
    expect(newCoords.newY).toEqual(35);
    expect(newCoords.newX).toEqual(365);

    newCoords = coordRotation(startX, startY, 180, width, height);
    expect(newCoords.newY).toEqual(365);
    expect(newCoords.newX).toEqual(365);

    newCoords = coordRotation(startX, startY, 270, width, height);
    expect(newCoords.newY).toEqual(365);
    expect(newCoords.newX).toEqual(35);

    newCoords = coordRotation(startX, startY, 360, width, height);
    expect(newCoords.newY).toEqual(35);
    expect(newCoords.newX).toEqual(35);
  });

  it('Gets the Polar Coordinates', () => {
    const anchor = { x: 20, y: 20 };
    const cartesian = { x: 27, y: 27 };

    const polarCoords = getPolarFromCartesian(anchor, cartesian);
    expect(Math.round(polarCoords.x)).toEqual(10);
    expect(polarCoords.y).toEqual(45);
  });

  it('Gets the Cartesian Coordinates', () => {
    const anchor = { x: 0, y: 0 };
    const polar = { x: 20, y: 90 };

    const cartesianCoords = getCartesianFromPolar(anchor, polar, 0);
    expect(Math.round(cartesianCoords.x)).toEqual(0);
    expect(Math.round(cartesianCoords.y)).toEqual(20);
  });

  describe('Sanitizes the Coordinates', () => {
    it('by returning coordinates to the original scale', () => {
      const { left, top, width, height } = getSanitizedDimensions(testFabRect.left, testFabRect.top, testFabRect.left + testFabRect.width, testFabRect.top + testFabRect.height, 600, 800, 0, 0.65);
      expect(Math.round(left)).toEqual(185);
      expect(Math.round(top)).toEqual(154);
      expect(Math.round(width)).toEqual(77);
      expect(Math.round(height)).toEqual(92);
    });
    // TODO: write more tests for getSanitizedDimensions when rotation is implemented
  });

  describe('getCoordinatesToScrollTo', () => {
    const paddingMultiplier = 0.1;

    it('returns original coordinates minus 10 percent padding with no zoom and no rotation', () => {
      const pageZoom = 1;
      const pageRotation = 0;
      const pageWidth = 90;
      const pageHeight = 110;
      const pageAvg = 100;

      const boxLocationX1 = 10;
      const boxLocationY1 = 10;
      const { scrollX: scrollX1, scrollY: scrollY1 } = getCaptureCoordinatesInViewer(boxLocationX1, boxLocationY1, pageRotation, pageWidth, pageHeight, pageZoom);
      expect(scrollX1).toBe(boxLocationX1 - pageAvg * paddingMultiplier);
      expect(scrollY1).toBe(boxLocationY1 - pageAvg * paddingMultiplier);

      const boxLocationX2 = 20;
      const boxLocationY2 = 20;
      const { scrollX: scrollX2, scrollY: scrollY2 } = getCaptureCoordinatesInViewer(boxLocationX2, boxLocationY2, pageRotation, pageWidth, pageHeight, pageZoom);
      expect(scrollX2).toBe(boxLocationX2 - pageAvg * paddingMultiplier);
      expect(scrollY2).toBe(boxLocationY2 - pageAvg * paddingMultiplier);
    });

    it('returns correct coordinates with padding with no zoom and 90 degree rotation', () => {
      const pageZoom = 1;
      const pageRotation = 90;
      const pageWidth = 90;
      const pageHeight = 110;
      const pageAvg = 100;

      const boxLocationX1 = 10;
      const boxLocationY1 = 10;
      const { scrollX: scrollX1, scrollY: scrollY1 } = getCaptureCoordinatesInViewer(boxLocationX1, boxLocationY1, pageRotation, pageWidth, pageHeight, pageZoom);
      expect(scrollX1).toBe(pageHeight - boxLocationY1 - pageAvg * paddingMultiplier);
      expect(scrollY1).toBe(boxLocationX1 - pageAvg * paddingMultiplier);

      const boxLocationX2 = 20;
      const boxLocationY2 = 20;
      const { scrollX: scrollX2, scrollY: scrollY2 } = getCaptureCoordinatesInViewer(boxLocationX2, boxLocationY2, pageRotation, pageWidth, pageHeight, pageZoom);
      expect(scrollX2).toBe(pageHeight - boxLocationY2 - pageAvg * paddingMultiplier);
      expect(scrollY2).toBe(boxLocationX2 - pageAvg * paddingMultiplier);
    });

    it('returns correct coordinates with padding with no zoom and 180 degree rotation', () => {
      const pageZoom = 1;
      const pageRotation = 180;
      const pageWidth = 90;
      const pageHeight = 110;
      const pageAvg = 100;

      const boxLocationX1 = 10;
      const boxLocationY1 = 10;
      const { scrollX: scrollX1, scrollY: scrollY1 } = getCaptureCoordinatesInViewer(boxLocationX1, boxLocationY1, pageRotation, pageWidth, pageHeight, pageZoom);
      expect(scrollX1).toBe(pageWidth - boxLocationX1 - pageAvg * paddingMultiplier);
      expect(scrollY1).toBe(pageHeight - boxLocationY1 - pageAvg * paddingMultiplier);

      const boxLocationX2 = 20;
      const boxLocationY2 = 20;
      const { scrollX: scrollX2, scrollY: scrollY2 } = getCaptureCoordinatesInViewer(boxLocationX2, boxLocationY2, pageRotation, pageWidth, pageHeight, pageZoom);
      expect(scrollX2).toBe(pageWidth - boxLocationX2 - pageAvg * paddingMultiplier);
      expect(scrollY2).toBe(pageHeight - boxLocationY2 - pageAvg * paddingMultiplier);
    });

    it('returns correct coordinates with padding with no zoom and 270 degree rotation', () => {
      const pageZoom = 1;
      const pageRotation = 270;
      const pageWidth = 90;
      const pageHeight = 110;
      const pageAvg = 100;

      const boxLocationX1 = 10;
      const boxLocationY1 = 10;
      const { scrollX: scrollX1, scrollY: scrollY1 } = getCaptureCoordinatesInViewer(boxLocationX1, boxLocationY1, pageRotation, pageWidth, pageHeight, pageZoom);
      expect(scrollX1).toBe(boxLocationY1 - pageAvg * paddingMultiplier);
      expect(scrollY1).toBe(pageWidth - boxLocationX1 - pageAvg * paddingMultiplier);

      const boxLocationX2 = 20;
      const boxLocationY2 = 20;
      const { scrollX: scrollX2, scrollY: scrollY2 } = getCaptureCoordinatesInViewer(boxLocationX2, boxLocationY2, pageRotation, pageWidth, pageHeight, pageZoom);
      expect(scrollX2).toBe(boxLocationY2 - pageAvg * paddingMultiplier);
      expect(scrollY2).toBe(pageWidth - boxLocationX2 - pageAvg * paddingMultiplier);
    });

    it('returns correct coordinates minus 10 percent padding with 0.5x zoom and no rotation', () => {
      const pageZoom = 0.5;
      const pageRotation = 0;
      const pageWidth = 90;
      const pageHeight = 110;
      const pageAvg = 100;

      const boxLocationX1 = 10;
      const boxLocationY1 = 10;
      const { scrollX: scrollX1, scrollY: scrollY1 } = getCaptureCoordinatesInViewer(boxLocationX1, boxLocationY1, pageRotation, pageWidth, pageHeight, pageZoom);
      expect(scrollX1).toBe(pageZoom * (boxLocationX1 - pageAvg * paddingMultiplier));
      expect(scrollY1).toBe(pageZoom * (boxLocationY1 - pageAvg * paddingMultiplier));

      const boxLocationX2 = 20;
      const boxLocationY2 = 20;
      const { scrollX: scrollX2, scrollY: scrollY2 } = getCaptureCoordinatesInViewer(boxLocationX2, boxLocationY2, pageRotation, pageWidth, pageHeight, pageZoom);
      expect(scrollX2).toBe(pageZoom * (boxLocationX2 - pageAvg * paddingMultiplier));
      expect(scrollY2).toBe(pageZoom * (boxLocationY2 - pageAvg * paddingMultiplier));
    });

    it('returns correct coordinates minus 10 percent padding with 2x zoom and no rotation', () => {
      const pageZoom = 2;
      const pageRotation = 0;
      const pageWidth = 90;
      const pageHeight = 110;
      const pageAvg = 100;

      const boxLocationX1 = 10;
      const boxLocationY1 = 10;
      const { scrollX: scrollX1, scrollY: scrollY1 } = getCaptureCoordinatesInViewer(boxLocationX1, boxLocationY1, pageRotation, pageWidth, pageHeight, pageZoom);
      expect(scrollX1).toBe(pageZoom * (boxLocationX1 - pageAvg * paddingMultiplier));
      expect(scrollY1).toBe(pageZoom * (boxLocationY1 - pageAvg * paddingMultiplier));

      const boxLocationX2 = 20;
      const boxLocationY2 = 20;
      const { scrollX: scrollX2, scrollY: scrollY2 } = getCaptureCoordinatesInViewer(boxLocationX2, boxLocationY2, pageRotation, pageWidth, pageHeight, pageZoom);
      expect(scrollX2).toBe(pageZoom * (boxLocationX2 - pageAvg * paddingMultiplier));
      expect(scrollY2).toBe(pageZoom * (boxLocationY2 - pageAvg * paddingMultiplier));
    });

    it('returns correct coordinates with padding with 0.75x zoom and 90 degree rotation', () => {
      const pageZoom = 0.75;
      const pageRotation = 90;
      const pageWidth = 90;
      const pageHeight = 110;
      const pageAvg = 100;

      const boxLocationX1 = 10;
      const boxLocationY1 = 10;
      const { scrollX: scrollX1, scrollY: scrollY1 } = getCaptureCoordinatesInViewer(boxLocationX1, boxLocationY1, pageRotation, pageWidth, pageHeight, pageZoom);
      expect(scrollX1).toBe(pageZoom * (pageHeight - boxLocationY1 - pageAvg * paddingMultiplier));
      expect(scrollY1).toBe(pageZoom * (boxLocationX1 - pageAvg * paddingMultiplier));

      const boxLocationX2 = 20;
      const boxLocationY2 = 20;
      const { scrollX: scrollX2, scrollY: scrollY2 } = getCaptureCoordinatesInViewer(boxLocationX2, boxLocationY2, pageRotation, pageWidth, pageHeight, pageZoom);
      expect(scrollX2).toBe(pageZoom * (pageHeight - boxLocationY2 - pageAvg * paddingMultiplier));
      expect(scrollY2).toBe(pageZoom * (boxLocationX2 - pageAvg * paddingMultiplier));
    });

    it('returns correct coordinates with padding with 1.5x zoom and 180 degree rotation', () => {
      const pageZoom = 1.5;
      const pageRotation = 180;
      const pageWidth = 90;
      const pageHeight = 110;
      const pageAvg = 100;

      const boxLocationX1 = 10;
      const boxLocationY1 = 10;
      const { scrollX: scrollX1, scrollY: scrollY1 } = getCaptureCoordinatesInViewer(boxLocationX1, boxLocationY1, pageRotation, pageWidth, pageHeight, pageZoom);
      expect(scrollX1).toBe(pageZoom * (pageWidth - boxLocationX1 - pageAvg * paddingMultiplier));
      expect(scrollY1).toBe(pageZoom * (pageHeight - boxLocationY1 - pageAvg * paddingMultiplier));

      const boxLocationX2 = 20;
      const boxLocationY2 = 20;
      const { scrollX: scrollX2, scrollY: scrollY2 } = getCaptureCoordinatesInViewer(boxLocationX2, boxLocationY2, pageRotation, pageWidth, pageHeight, pageZoom);
      expect(scrollX2).toBe(pageZoom * (pageWidth - boxLocationX2 - pageAvg * paddingMultiplier));
      expect(scrollY2).toBe(pageZoom * (pageHeight - boxLocationY2 - pageAvg * paddingMultiplier));
    });

    it('returns correct coordinates with padding with 3x zoom and 270 degree rotation', () => {
      const pageZoom = 3;
      const pageRotation = 270;
      const pageWidth = 90;
      const pageHeight = 110;
      const pageAvg = 100;

      const boxLocationX1 = 10;
      const boxLocationY1 = 10;
      const { scrollX: scrollX1, scrollY: scrollY1 } = getCaptureCoordinatesInViewer(boxLocationX1, boxLocationY1, pageRotation, pageWidth, pageHeight, pageZoom);
      expect(scrollX1).toBe(pageZoom * (boxLocationY1 - pageAvg * paddingMultiplier));
      expect(scrollY1).toBe(pageZoom * (pageWidth - boxLocationX1 - pageAvg * paddingMultiplier));

      const boxLocationX2 = 20;
      const boxLocationY2 = 20;
      const { scrollX: scrollX2, scrollY: scrollY2 } = getCaptureCoordinatesInViewer(boxLocationX2, boxLocationY2, pageRotation, pageWidth, pageHeight, pageZoom);
      expect(scrollX2).toBe(pageZoom * (boxLocationY2 - pageAvg * paddingMultiplier));
      expect(scrollY2).toBe(pageZoom * (pageWidth - boxLocationX2 - pageAvg * paddingMultiplier));
    });
  });

  describe('updateBalloonPosition', () => {
    const newPoint = { x: 10, y: 10 };
    const newItem = {
      ...testCharacteristic,
      boxLocationX: 15,
      boxLocationY: 5,
      boxWidth: 5,
      boxHeight: 5,
      boxRotation: 0,
    };
    const boxCartesian = {
      x: newItem.boxLocationX + newItem.boxWidth / 2,
      y: newItem.boxLocationY + newItem.boxHeight / 2,
    };
    const width = 1280;
    const height = 720;

    it('gets the polar coordinates of the point', () => {
      const balloon = updateBalloonPosition(newItem, newPoint, 0, width, height);
      expect(balloon).toMatchObject({ x: 7.905694150420948, y: 161.565051177078 });
      // confirm that it reverses correctly
      const reverse = getCartesianFromPolar(boxCartesian, balloon, 0);
      reverse.x = Math.round(reverse.x);
      reverse.y = Math.round(reverse.y);
      expect(reverse.x).toBeGreaterThan(9.98);
      expect(reverse.x).toBeLessThan(10.02);
      expect(reverse.y).toBeGreaterThan(9.98);
      expect(reverse.y).toBeLessThan(10.02);
    });

    it('returns the point as if the drawing were upright', () => {
      const newCoords = coordRotation(newPoint.x, newPoint.y, 270, width, height);
      const balloon = updateBalloonPosition(newItem, newPoint, 90, width, height);
      expect(balloon).toMatchObject({ x: 1262.5222770311818, y: 90.34036696351747 });
      // confirm that it reverses correctly
      const reverse = getCartesianFromPolar(boxCartesian, balloon, 0);
      expect(Math.round(reverse.x)).toBe(Math.round(newCoords.newX)); // technically 9.9999999901 is not 10, but it's close enough for us, so round these
      expect(Math.round(reverse.y)).toBe(Math.round(newCoords.newY));
    });
  });

  describe('gridPosition', () => {
    const balloon = { x: 10, y: 10 };
    const newItem = {
      ...testCharacteristic,
      boxLocationX: 15,
      boxLocationY: 5,
      boxWidth: 5,
      boxHeight: 5,
      boxRotation: 0,
      connectionPointLocationX: 17,
      connectionPointLocationY: 7,
    };
    const params = {
      initialCanvasWidth: 1000,
      initialCanvasHeight: 500,
      lineWidth: 1,
      labelColor: '#000000',
      lineColor: '#ffffff',
      columnLeftLabel: '1',
      columnRightLabel: '3',
      rowTopLabel: 'A',
      rowBottomLabel: 'C',
      skipLabels: [],
      showLabels: false,
      rowLabels: ['A', 'B', 'C'],
      rowLineRatios: [0, 0.25, 0.5, 1],
      rowLines: [0, 250, 500, 1000],
      colLabels: ['1', '2', '3'],
      colLineRatios: [0, 0.25, 0.5, 1],
      colLines: [0, 125, 250, 500],
    };
    const grid = new Grid(params);

    it('returns the correct location when upright', () => {
      expect(grid.pageRotation).toBe(0);
      const canvas = new fabric.Canvas(null, { width: 1000, height: 500 });
      const { gridCoordinates, connectionPointGridCoordinates, balloonGridCoordinates } = gridPosition(balloon, { left: newItem.boxLocationX, top: newItem.boxLocationY }, 0, canvas, grid, newItem);
      expect(gridCoordinates).toBe('1A');
      expect(connectionPointGridCoordinates).toBe('1A');
      expect(balloonGridCoordinates).toBe('1A');
    });

    it('returns the correct location when 90', () => {
      grid.rotate(90);
      expect(grid.pageRotation).toBe(90);
      const canvas = new fabric.Canvas(null, { width: 500, height: 1000 });
      const { gridCoordinates, connectionPointGridCoordinates, balloonGridCoordinates } = gridPosition(balloon, { left: newItem.boxLocationX, top: newItem.boxLocationY }, 90, canvas, grid, newItem);
      expect(gridCoordinates).toBe('1A');
      expect(connectionPointGridCoordinates).toBe('1A');
      expect(balloonGridCoordinates).toBe('1A');
    });

    it('returns the correct location when 180', () => {
      grid.rotate(90);
      expect(grid.pageRotation).toBe(180);
      const canvas = new fabric.Canvas(null, { width: 1000, height: 500 });
      const { gridCoordinates, connectionPointGridCoordinates, balloonGridCoordinates } = gridPosition(balloon, { left: newItem.boxLocationX, top: newItem.boxLocationY }, 180, canvas, grid, newItem);
      expect(gridCoordinates).toBe('1A');
      expect(connectionPointGridCoordinates).toBe('1A');
      expect(balloonGridCoordinates).toBe('1A');
    });

    it('returns the correct location when 270', () => {
      grid.rotate(90);
      expect(grid.pageRotation).toBe(270);
      const canvas = new fabric.Canvas(null, { width: 500, height: 1000 });
      const { gridCoordinates, connectionPointGridCoordinates, balloonGridCoordinates } = gridPosition(balloon, { left: newItem.boxLocationX, top: newItem.boxLocationY }, 270, canvas, grid, newItem);
      expect(gridCoordinates).toBe('1A');
      expect(connectionPointGridCoordinates).toBe('1A');
      expect(balloonGridCoordinates).toBe('1A');
    });
  });

  describe('annotationCoords', () => {
    const anchorCoord = { left: 20, top: 20, width: 40, height: 60 };
    it('When the annotation is rotated 0 degrees', () => {
      const newCoords = getAnnotationTextCoords({ anchorCoord, boxRotation: 0, rotation: 0 });
      expect(newCoords.left).toBe(-14);
      expect(newCoords.top).toBe(-24);
    });

    it('When the annotation is rotated 0 90 degrees', () => {
      const newCoords = getAnnotationTextCoords({ anchorCoord, boxRotation: 0, rotation: 90 });
      expect(newCoords.left).toBe(14);
      expect(newCoords.top).toBe(-24);
    });

    it('When the annotation is rotated 0 180 degrees', () => {
      const newCoords = getAnnotationTextCoords({ anchorCoord, boxRotation: 0, rotation: 180 });
      expect(newCoords.left).toBe(14);
      expect(newCoords.top).toBe(24);
    });

    it('When the annotation is rotated 0 270 degrees', () => {
      const newCoords = getAnnotationTextCoords({ anchorCoord, boxRotation: 0, rotation: 270 });
      expect(newCoords.left).toBe(-14);
      expect(newCoords.top).toBe(24);
    });
  });
});
