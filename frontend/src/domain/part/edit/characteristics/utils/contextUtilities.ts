import { Characteristic } from 'types/characteristics';

export const updateAfterEdit = (selected: Characteristic[], edited: Characteristic[], characters: Characteristic[]) => {
  const updatedSelected = [...selected];
  const updatedCharacters = [...characters];
  if (!edited || edited.length <= 0) {
    return { updatedSelected, updatedCharacters };
  }
  edited.forEach((char) => {
    let ind = characters.findIndex((a) => a.id === char.id);
    updatedCharacters[ind] = char;
    ind = selected.findIndex((a) => a.id === char.id);
    if (ind >= 0) {
      updatedSelected[ind] = char;
    }
  });
  return { updatedSelected, updatedCharacters };
};

export const setIndex = (selected: Characteristic[], length: number, current?: number) => {
  if (!selected || selected.length <= 0) {
    return current || 0;
  }
  const firstSelected = selected.sort((a, b) => (a.markerIndex > b.markerIndex ? 1 : -1))[0];
  return firstSelected && firstSelected.markerIndex < length ? firstSelected.markerIndex : 0;
};
