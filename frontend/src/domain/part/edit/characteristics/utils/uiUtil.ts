import { i18n } from 'i18n';
import { Annotation } from 'types/annotation';
import { Characteristic } from 'types/characteristics';

export const deleteTipTitle = (selectedFeatures: Characteristic[], selectedAnnotations: Annotation[]) => {
  const feats = selectedFeatures.length;
  const annots = selectedAnnotations.length;

  if (feats && annots) return i18n('toolbar.deleteBoth', feats, annots);
  if (feats) return i18n('toolbar.deleteFeatures', feats);
  if (annots) return i18n('toolbar.deleteAnnotations', annots);
  return i18n('toolbar.noFeaturesSelected');
};
