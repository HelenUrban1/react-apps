import { ANNOTATION_TEXT_PADDING } from 'types/annotation';
import { Characteristic } from 'types/characteristics';
import { CaptureDimensions } from 'utils/ParseTextController/types';
import { Grid } from '../../grid/Grid';
// Helper function that returns the new x, y coordinates after rotating them within the reference frame
export const coordRotation = (x: number, y: number, rotation: number, canvasWidth: number, canvasHeight: number) => {
  let newX: number;
  let newY: number;
  const newWidth = rotation % 180 === 0 ? canvasWidth : canvasHeight;
  const newHeight = rotation % 180 === 0 ? canvasHeight : canvasWidth;
  switch (rotation) {
    case 90:
      newX = newWidth - y;
      newY = x;
      break;
    case 180:
      newX = newWidth - x;
      newY = newHeight - y;
      break;
    case 270:
      newX = y;
      newY = newHeight - x;
      break;
    default:
      newX = x;
      newY = y;
  }
  return { newX, newY };
};

// Helper method that converts cartesian coordinates to polar
export const getPolarFromCartesian = (anchor: { x: number; y: number }, polar: { x: number; y: number }) => {
  // move to origin
  const deltaX = polar.x - anchor.x;
  const deltaY = polar.y - anchor.y;

  // get radius
  const radius = Math.sqrt(deltaX * deltaX + deltaY * deltaY);

  // get rotation in degrees
  const deg = Math.atan2(deltaY, deltaX) * (180 / Math.PI);

  return { x: radius, y: deg };
};

// Helper method that converts polar coordinates to cartesian
export const getCartesianFromPolar = (anchor: { x: number; y: number }, polar: { x: number; y: number }, markerRotation: number) => {
  const rads = (((360 + polar.y - markerRotation) % 360) * Math.PI) / 180;
  const relativeX = polar.x * Math.cos(rads);
  const relativeY = polar.x * Math.sin(rads);
  const cartX = anchor.x + relativeX;
  const cartY = anchor.y + relativeY;
  return { x: cartX, y: cartY };
};

// Helper function that returns the unscaled and unrotated dimensions of a rectangle
export const getSanitizedDimensions = (x1: number, y1: number, x2: number, y2: number, pageWidth: number, pageHeight: number, pageRotation = 0, pageScale = 1) => {
  const startPoint = coordRotation(x1, y1, 360 - pageRotation, pageWidth, pageHeight);
  const endPoint = coordRotation(x2, y2, 360 - pageRotation, pageWidth, pageHeight);
  const width = Math.abs(endPoint.newX - startPoint.newX) / pageScale;
  const height = Math.abs(endPoint.newY - startPoint.newY) / pageScale;
  const left = Math.min(startPoint.newX, endPoint.newX) / pageScale;
  const top = Math.min(startPoint.newY, endPoint.newY) / pageScale;
  return { left, top, width, height };
};

// Helper function that scales and rotates a capture rectangle. Used when modifying an object and when rendering it to the canvas.
export const getTransformedDimensions = (x: number, y: number, width: number, height: number, canvasWidth: number, canvasHeight: number, scale: number, rotation: number) => {
  // page dimensions with 0 rotation and 1 scale
  const normalizedPageWidth = rotation % 180 === 0 ? canvasWidth / scale : canvasHeight / scale;
  const normalizedPageHeight = rotation % 180 === 0 ? canvasHeight / scale : canvasWidth / scale;

  // rotate the top left and bottom right corners of the rectangle, set the new top left point and dimensions
  const rotatedA = coordRotation(x, y, rotation, normalizedPageWidth, normalizedPageHeight);
  const rotatedB = coordRotation(x + width, y + height, rotation, normalizedPageWidth, normalizedPageHeight);
  const newLeft = Math.min(rotatedA.newX, rotatedB.newX);
  const newTop = Math.min(rotatedA.newY, rotatedB.newY);
  const newWidth = Math.abs(rotatedA.newX - rotatedB.newX);
  const newHeight = Math.abs(rotatedA.newY - rotatedB.newY);

  return { newLeft, newTop, newWidth, newHeight };
};

export const getCaptureCoordinatesInViewer = (boxLocationX: number, boxLocationY: number, rotation: number, pageWidth: number, pageHeight: number, zoom: number) => {
  const { newX, newY } = coordRotation(boxLocationX, boxLocationY, rotation, pageWidth, pageHeight);

  // Taking one tenth of the avg of the page height and width as padding to the top and left of the capture
  const padding = ((pageWidth + pageHeight) / 2) * 0.1 * zoom;
  const scrollX = Math.max(newX * zoom - padding, 0);
  const scrollY = Math.max(newY * zoom - padding, 0);
  return { scrollX, scrollY };
};

export const updateBalloonPosition = (feature: Characteristic, newPoint: { x: number; y: number }, rotation: number, pageWidth: number, pageHeight: number) => {
  const rotatedPoint = coordRotation(newPoint.x, newPoint.y, 360 - rotation, pageWidth, pageHeight);
  const newCartesian = rotatedPoint
    ? {
        x: rotatedPoint.newX,
        y: rotatedPoint.newY,
      }
    : {
        x: feature.markerLocationX,
        y: feature.markerLocationY,
      };
  const boxCartesian = {
    x: feature.boxLocationX + feature.boxWidth / 2,
    y: feature.boxLocationY + feature.boxHeight / 2,
  };
  const newPolar = getPolarFromCartesian(boxCartesian, newCartesian);
  return newPolar;
  // return { x: newPolar.x, y: (newPolar.y + rotation) % 360 };
};

export const gridPosition = (balloon: { x: number; y: number }, anchorCoord: { left: number; top: number }, rotation: number, canvas: fabric.Canvas, grid: Grid, feature: Characteristic) => {
  let { gridCoordinates, connectionPointGridCoordinates, balloonGridCoordinates } = feature;
  const width = canvas.getWidth();
  const zoom = canvas.getZoom();
  const height = canvas.getHeight();
  const widthScaled = width / zoom;
  const heightScaled = height / zoom;
  const halfBoxHeight = feature.boxHeight / 2;
  const halfBoxWidth = feature.boxWidth / 2;

  // rotate the top left and bottom right corners of the rectangle, set the new top left point and dimensions
  if (rotation === 0) {
    gridCoordinates = grid.cellLabelForPoint(feature.boxLocationX + halfBoxWidth, feature.boxLocationY + halfBoxHeight, zoom);
    connectionPointGridCoordinates = grid.cellLabelForPoint(feature.connectionPointLocationX, feature.connectionPointLocationY, zoom);
    balloonGridCoordinates = grid.cellLabelForPoint(balloon.x, balloon.y, zoom);
  } else if (rotation === 90) {
    // swap y and x
    gridCoordinates = grid.cellLabelForPoint(widthScaled - (feature.boxLocationY + halfBoxHeight), feature.boxLocationX + halfBoxWidth, zoom);
    connectionPointGridCoordinates = grid.cellLabelForPoint(widthScaled - feature.connectionPointLocationY, feature.connectionPointLocationX, zoom);
    balloonGridCoordinates = grid.cellLabelForPoint(widthScaled - balloon.y, balloon.x, zoom);
  } else if (rotation === 180) {
    gridCoordinates = grid.cellLabelForPoint(widthScaled - feature.boxLocationX - halfBoxWidth, heightScaled - feature.boxLocationY - halfBoxHeight, zoom);
    connectionPointGridCoordinates = grid.cellLabelForPoint(widthScaled - feature.connectionPointLocationX, heightScaled - feature.connectionPointLocationY, zoom);
    balloonGridCoordinates = grid.cellLabelForPoint(widthScaled - balloon.x, heightScaled - balloon.y, zoom);
  } else if (rotation === 270) {
    // swap y and x
    gridCoordinates = grid.cellLabelForPoint(feature.boxLocationY + halfBoxHeight, heightScaled - (feature.boxLocationX + halfBoxWidth), zoom);
    connectionPointGridCoordinates = grid.cellLabelForPoint(feature.connectionPointLocationY, heightScaled - feature.connectionPointLocationX, zoom);
    balloonGridCoordinates = grid.cellLabelForPoint(balloon.y, heightScaled - balloon.x, zoom);
  }

  return { gridCoordinates, connectionPointGridCoordinates, balloonGridCoordinates };
};

export const getAnnotationTextCoords = ({ anchorCoord, boxRotation, rotation }: { anchorCoord: CaptureDimensions; boxRotation: number; rotation: number }) => {
  // keeps text in right orientation when rotation
  const angle = rotation - boxRotation;
  switch (angle) {
    case 90:
      return {
        left: anchorCoord.width / 2 - ANNOTATION_TEXT_PADDING,
        top: -anchorCoord.height / 2 + ANNOTATION_TEXT_PADDING,
      };
    case 180:
      return {
        left: anchorCoord.width - anchorCoord.width / 2 - ANNOTATION_TEXT_PADDING,
        top: anchorCoord.height - anchorCoord.height / 2 - ANNOTATION_TEXT_PADDING,
      };
    case 270:
      return {
        left: 0 - anchorCoord.width / 2 + ANNOTATION_TEXT_PADDING,
        top: anchorCoord.height - anchorCoord.height / 2 - ANNOTATION_TEXT_PADDING,
      };
    default:
      return {
        left: -anchorCoord.width / 2 + ANNOTATION_TEXT_PADDING,
        top: -anchorCoord.height / 2 + ANNOTATION_TEXT_PADDING,
      };
  }
};
