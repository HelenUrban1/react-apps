import { defaultTypeIDs, defaultSubtypeIDs, defaultMethodIDs, defaultUnitIDs } from 'view/global/defaults';

export const characteristics = [
  {
    id: 'e0a01769-c8ed-4090-bd24-9c1b2241f123',
    status: 'AutoInactive',
    drawingSheetIndex: 0,
    notationType: defaultTypeIDs.Dimension, // Dimension
    notationSubtype: defaultSubtypeIDs.Curvilinear, // Curvilinear
    notationClass: 'Tolerance',
    fullSpecification: '1.25 +/- 0.1',
    quantity: 1,
    nominal: '1.25',
    upperSpecLimit: '1.26',
    lowerSpecLimit: '1.24',
    plusTol: '0.1',
    minusTol: '0.1',
    unit: defaultUnitIDs.millimeter,
    criticality: false,
    inspectionMethod: defaultMethodIDs.Caliper, // Caliper
    notes: '',
    drawingRotation: 0.0,
    drawingScale: 1.0,
    boxLocationY: 20.5,
    boxLocationX: 20.5,
    boxWidth: 250,
    boxHeight: 100,
    boxRotation: 0.0,
    markerGroup: '1',
    markerGroupShared: false,
    markerIndex: 0,
    markerSubIndex: null,
    markerLabel: '1',
    markerStyle: 'aad19683-337a-4039-a910-98b0bdd5ee57',
    markerLocationX: 20,
    markerLocationY: 20,
    markerFontSize: 18,
    markerSize: 36,
    gridCoordinates: null,
    balloonGridCoordinates: null,
    connectionPointGridCoordinates: null,
    createdAt: '2020-03-04 09:06:09.886657+00',
    updatedAt: '2020-03-04 09:06:09.886657+00',
    partId: 'd6c5bc49-9fb2-4fbb-8a61-77bc8d28036f', // belongsTo
    drawingId: 'c200ce70-11b8-44f7-8dff-4b93f6d1b491', // belongsTo
    drawingPageId: 'd6c5bc49-9fb2-4f1b-8a61-7abc8d380352',
  },
  {
    id: 'e0a01769-c8ed-4090-bd24-9c1b2241f124',
    drawingSheetIndex: 0,
    status: 'AutoInactive',
    notationType: defaultTypeIDs.Dimension, // Dimension
    notationSubtype: defaultSubtypeIDs.Angle, // Angle
    notationClass: 'Tolerance',
    fullSpecification: '1.25 +/- 0.2',
    quantity: 5,
    nominal: '1.25',
    upperSpecLimit: '1.27',
    lowerSpecLimit: '1.23',
    plusTol: '0.2',
    minusTol: '0.2',
    unit: defaultUnitIDs.millimeter,
    criticality: false,
    inspectionMethod: defaultMethodIDs.Caliper, // Caliper
    notes: '',
    drawingRotation: 0.0,
    drawingScale: 1.0,
    boxLocationY: 20.5,
    boxLocationX: 20.5,
    boxWidth: 250,
    boxHeight: 100,
    boxRotation: 0.0,
    markerGroup: '2',
    markerGroupShared: false,
    markerIndex: 1,
    markerSubIndex: 0,
    markerLabel: '2.1',
    markerStyle: '8d1a8859-c1bc-4c90-b7c6-2bf1640856ac',
    markerLocationX: 20,
    markerLocationY: 20,
    markerFontSize: 18,
    markerSize: 36,
    gridCoordinates: null,
    balloonGridCoordinates: null,
    connectionPointGridCoordinates: null,
    createdAt: '2020-03-04 09:06:09.886657+00',
    updatedAt: '2020-03-04 09:06:09.886657+00',
    partId: 'd6c5bc49-9fb2-4fbb-8a61-77bc8d28036f', // belongsTo
    drawingId: 'c200ce70-11b8-44f7-8dff-4b93f6d1b491', // belongsTo
    drawingPageId: 'd6c5bc49-9fb2-4f1b-8a61-7abc8d380352',
  },
  {
    id: 'e0a01769-c8ed-4090-bd24-9c1b2241f125',
    drawingSheetIndex: 0,
    status: 'AutoInactive',
    notationType: defaultTypeIDs.Dimension, // Dimension
    notationSubtype: defaultSubtypeIDs.Angle, // Angle
    notationClass: 'Tolerance',
    fullSpecification: '3.86 - 3.92',
    quantity: 1,
    nominal: '3.86 - 3.92',
    upperSpecLimit: '3.92',
    lowerSpecLimit: '3.86',
    plusTol: null,
    minusTol: null,
    unit: defaultUnitIDs.millimeter,
    criticality: false,
    inspectionMethod: defaultMethodIDs.Caliper, // Caliper
    notes: '',
    drawingRotation: 0.0,
    drawingScale: 1.0,
    boxLocationY: 20.5,
    boxLocationX: 20.5,
    boxWidth: 250,
    boxHeight: 100,
    boxRotation: 0.0,
    markerGroup: '2',
    markerGroupShared: false,
    markerIndex: 2,
    markerSubIndex: 1,
    markerLabel: '2.2',
    markerStyle: '8d1a8859-c1bc-4c90-b7c6-2bf1640856ac',
    markerLocationX: 20,
    markerLocationY: 20,
    markerFontSize: 18,
    markerSize: 36,
    gridCoordinates: null,
    balloonGridCoordinates: null,
    connectionPointGridCoordinates: null,
    createdAt: '2020-03-04 09:06:09.886657+00',
    updatedAt: '2020-03-04 09:06:09.886657+00',
    partId: 'd6c5bc49-9fb2-4fbb-8a61-77bc8d28036f', // belongsTo
    drawingId: 'c200ce70-11b8-44f7-8dff-4b93f6d1b491', // belongsTo
    drawingPageId: 'd6c5bc49-9fb2-4f1b-8a61-7abc8d380352',
  },
  {
    id: 'e0a01769-c8ed-4090-bd24-9c1b2241f128',
    drawingSheetIndex: 0,
    status: 'AutoInactive',
    notationType: defaultTypeIDs.Dimension, // Dimension
    notationSubtype: defaultSubtypeIDs.Angle, // Angle
    notationClass: 'Tolerance',
    fullSpecification: '3.92 MAX',
    quantity: 1,
    nominal: '.92',
    upperSpecLimit: '3.92',
    lowerSpecLimit: null,
    plusTol: null,
    minusTol: null,
    unit: defaultUnitIDs.millimeter,
    criticality: false,
    inspectionMethod: defaultMethodIDs.Caliper, // Caliper
    notes: '',
    drawingRotation: 0.0,
    drawingScale: 1.0,
    boxLocationY: 20.5,
    boxLocationX: 20.5,
    boxWidth: 250,
    boxHeight: 100,
    boxRotation: 0.0,
    markerGroup: '2',
    markerGroupShared: false,
    markerIndex: 3,
    markerSubIndex: 2,
    markerLabel: '2.3',
    markerStyle: '8d1a8859-c1bc-4c90-b7c6-2bf1640856ac',
    markerLocationX: 20,
    markerLocationY: 20,
    markerFontSize: 18,
    markerSize: 36,
    gridCoordinates: null,
    balloonGridCoordinates: null,
    connectionPointGridCoordinates: null,
    createdAt: '2020-03-04 09:06:09.886657+00',
    updatedAt: '2020-03-04 09:06:09.886657+00',
    partId: 'd6c5bc49-9fb2-4fbb-8a61-77bc8d28036f', // belongsTo
    drawingId: 'c200ce70-11b8-44f7-8dff-4b93f6d1b491', // belongsTo
    drawingPageId: 'd6c5bc49-9fb2-4f1b-8a61-7abc8d380352',
  },
  {
    id: 'e0a01769-c8ed-4090-bd24-9c1b2221f128',
    drawingSheetIndex: 0,
    status: 'AutoInactive',
    notationType: defaultTypeIDs.Dimension, // Dimension
    notationSubtype: defaultSubtypeIDs.Angle, // Angle
    notationClass: 'Tolerance',
    fullSpecification: '8X 12.87 MIN',
    quantity: 8,
    nominal: '12.87',
    upperSpecLimit: null,
    lowerSpecLimit: '12.87',
    plusTol: null,
    minusTol: null,
    unit: defaultUnitIDs.millimeter,
    criticality: false,
    inspectionMethod: defaultMethodIDs.Caliper, // Caliper
    notes: '',
    drawingRotation: 0.0,
    drawingScale: 1.0,
    boxLocationY: 20.5,
    boxLocationX: 20.5,
    boxWidth: 250,
    boxHeight: 100,
    boxRotation: 0.0,
    markerGroup: '3',
    markerGroupShared: false,
    markerIndex: 4,
    markerSubIndex: null,
    markerLabel: '3',
    markerStyle: 'f1386f09-9c03-4c59-ada9-acb6a16bfbab',
    markerLocationX: 20,
    markerLocationY: 20,
    markerFontSize: 18,
    markerSize: 36,
    gridCoordinates: null,
    balloonGridCoordinates: null,
    connectionPointGridCoordinates: null,
    createdAt: '2020-03-04 09:06:09.886657+00',
    updatedAt: '2020-03-04 09:06:09.886657+00',
    partId: 'd6c5bc49-9fb2-4fbb-8a61-77bc8d28036f', // belongsTo
    drawingId: 'c200ce70-11b8-44f7-8dff-4b93f6d1b491', // belongsTo
    drawingPageId: 'd6c5bc49-9fb2-4f1b-8a61-7abc8d380352',
  },
  {
    id: 'e0a01769-c8ed-4090-bd24-9c1b2e41f130',
    drawingSheetIndex: 0,
    status: 'AutoInactive',
    notationType: defaultTypeIDs.Dimension, // Dimension
    notationSubtype: defaultSubtypeIDs.Angle, // Angle
    notationClass: 'Tolerance',
    fullSpecification: '12.87',
    quantity: 1,
    nominal: '12.87',
    upperSpecLimit: null,
    lowerSpecLimit: '12.87',
    plusTol: null,
    minusTol: null,
    unit: defaultUnitIDs.millimeter,
    criticality: false,
    inspectionMethod: defaultMethodIDs.Caliper, // Caliper
    notes: '',
    drawingRotation: 0.0,
    drawingScale: 1.0,
    boxLocationY: 20.5,
    boxLocationX: 20.5,
    boxWidth: 250,
    boxHeight: 100,
    boxRotation: 0.0,
    markerGroup: '4',
    markerGroupShared: false,
    markerIndex: 5,
    markerSubIndex: null,
    markerLabel: '4',
    markerStyle: 'f1386f09-9c03-4c59-ada9-acb6a16bfbab',
    markerLocationX: 20,
    markerLocationY: 20,
    markerFontSize: 18,
    markerSize: 36,
    gridCoordinates: null,
    balloonGridCoordinates: null,
    connectionPointGridCoordinates: null,
    createdAt: '2020-03-04 09:06:09.886657+00',
    updatedAt: '2020-03-04 09:06:09.886657+00',
    partId: 'd6c5bc49-9fb2-4fbb-8a61-77bc8d28036f', // belongsTo
    drawingId: 'c200ce70-11b8-44f7-8dff-4b93f6d1b491', // belongsTo
    drawingPageId: 'd6c5bc49-9fb2-4f1b-8a61-7abc8d380352',
  },
  {
    id: 'e0a01769-c8ed-409e-bd24-9c1b2241f131',
    drawingSheetIndex: 0,
    status: 'AutoInactive',
    notationType: defaultTypeIDs.Dimension, // Dimension
    notationSubtype: defaultSubtypeIDs.Length, // Length
    notationClass: 'Tolerance',
    fullSpecification: '34 +- 12 THRU',
    quantity: 1,
    nominal: '12.87',
    upperSpecLimit: null,
    lowerSpecLimit: '12.87',
    plusTol: null,
    minusTol: null,
    unit: defaultUnitIDs.millimeter,
    criticality: false,
    inspectionMethod: defaultMethodIDs.Caliper, // Caliper
    notes: '',
    drawingRotation: 0.0,
    drawingScale: 1.0,
    boxLocationY: 20.5,
    boxLocationX: 20.5,
    boxWidth: 250,
    boxHeight: 100,
    boxRotation: 0.0,
    markerGroup: '5',
    markerGroupShared: false,
    markerIndex: 6,
    markerSubIndex: null,
    markerLabel: '5',
    markerStyle: 'f1386f09-9c03-4c59-ada9-acb6a16bfbab',
    markerLocationX: 20,
    markerLocationY: 20,
    markerFontSize: 18,
    markerSize: 36,
    gridCoordinates: null,
    balloonGridCoordinates: null,
    connectionPointGridCoordinates: null,
    createdAt: '2020-03-04 09:06:09.886657+00',
    updatedAt: '2020-03-04 09:06:09.886657+00',
    partId: 'd6c5bc49-9fb2-4fbb-8a61-77bc8d28036f', // belongsTo
    drawingId: 'c200ce70-11b8-44f7-8dff-4b93f6d1b491', // belongsTo
    drawingPageId: 'd6c5bc49-9fb2-4f1b-8a61-7abc8d380352',
  },
  {
    id: 'e0a01769-c8ed-40c0-bd24-9c1b2241f135',
    drawingSheetIndex: 0,
    status: 'AutoInactive',
    notationType: defaultTypeIDs.Note, // Note
    notationSubtype: defaultSubtypeIDs.Note, // Note
    notationClass: 'Tolerance',
    fullSpecification: 'This is a Note We Captured',
    quantity: 1,
    nominal: '12.87',
    upperSpecLimit: null,
    lowerSpecLimit: '12.87',
    plusTol: null,
    minusTol: null,
    unit: defaultUnitIDs.millimeter,
    criticality: false,
    inspectionMethod: defaultMethodIDs.Caliper, // Caliper
    notes: '',
    drawingRotation: 0.0,
    drawingScale: 1.0,
    boxLocationY: 20.5,
    boxLocationX: 20.5,
    boxWidth: 250,
    boxHeight: 100,
    boxRotation: 0.0,
    markerGroup: '6',
    markerGroupShared: false,
    markerIndex: 7,
    markerSubIndex: null,
    markerLabel: '6',
    markerStyle: 'f1386f09-9c03-4c59-ada9-acb6a16bfbab',
    markerLocationX: 20,
    markerLocationY: 20,
    markerFontSize: 18,
    markerSize: 36,
    gridCoordinates: null,
    balloonGridCoordinates: null,
    connectionPointGridCoordinates: null,
    createdAt: '2020-03-04 09:06:09.886657+00',
    updatedAt: '2020-03-04 09:06:09.886657+00',
    partId: 'd6c5bc49-9fb2-4fbb-8a61-77bc8d28036f', // belongsTo
    drawingId: 'c200ce70-11b8-44f7-8dff-4b93f6d1b491', // belongsTo
    drawingPageId: 'd6c5bc49-9fb2-4f1b-8a61-7abc8d380352',
  },
  {
    id: 'e0a01769-c8ed-4090-bd20-9c1b2241f167',
    drawingSheetIndex: 0,
    status: 'AutoInactive',
    notationType: defaultTypeIDs.Dimension, // Dimension
    notationSubtype: defaultSubtypeIDs.Length, // Length
    notationClass: 'Tolerance',
    fullSpecification: '777',
    quantity: 7,
    nominal: '12.87',
    upperSpecLimit: null,
    lowerSpecLimit: '12.87',
    plusTol: null,
    minusTol: null,
    unit: defaultUnitIDs.millimeter,
    criticality: false,
    inspectionMethod: defaultMethodIDs.Caliper, // Caliper
    notes: '',
    drawingRotation: 0.0,
    drawingScale: 1.0,
    boxLocationY: 20.5,
    boxLocationX: 20.5,
    boxWidth: 250,
    boxHeight: 100,
    boxRotation: 0.0,
    markerGroup: '7',
    markerGroupShared: false,
    markerIndex: 8,
    markerSubIndex: null,
    markerLabel: '7',
    markerStyle: 'f1386f09-9c03-4c59-ada9-acb6a16bfbab',
    markerLocationX: 20,
    markerLocationY: 20,
    markerFontSize: 18,
    markerSize: 36,
    gridCoordinates: null,
    balloonGridCoordinates: null,
    connectionPointGridCoordinates: null,
    createdAt: '2020-03-04 09:06:09.886657+00',
    updatedAt: '2020-03-04 09:06:09.886657+00',
    partId: 'd6c5bc49-9fb2-4fbb-8a61-77bc8d28036f', // belongsTo
    drawingId: 'c200ce70-11b8-44f7-8dff-4b93f6d1b491', // belongsTo
    drawingPageId: 'd6c5bc49-9fb2-4f1b-8a61-7abc8d380352',
  },
  {
    id: 'e0a01769-c8ed-4090-bda4-9c1b22412432',
    drawingSheetIndex: 0,
    status: 'AutoInactive',
    notationType: defaultTypeIDs.Dimension, // Dimension
    notationSubtype: defaultSubtypeIDs.Length, // Length
    notationClass: 'Tolerance',
    fullSpecification: '3.24 / 3.21',
    quantity: 1,
    nominal: '12.87',
    upperSpecLimit: null,
    lowerSpecLimit: '12.87',
    plusTol: null,
    minusTol: null,
    unit: defaultUnitIDs.millimeter,
    criticality: false,
    inspectionMethod: defaultMethodIDs.Caliper, // Caliper
    notes: '',
    drawingRotation: 0.0,
    drawingScale: 1.0,
    boxLocationY: 20.5,
    boxLocationX: 20.5,
    boxWidth: 250,
    boxHeight: 100,
    boxRotation: 0.0,
    markerGroup: '8',
    markerGroupShared: false,
    markerIndex: 9,
    markerSubIndex: null,
    markerLabel: '8',
    markerStyle: 'f1386f09-9c03-4c59-ada9-acb6a16bfbab',
    markerLocationX: 20,
    markerLocationY: 20,
    markerFontSize: 18,
    markerSize: 36,
    gridCoordinates: null,
    balloonGridCoordinates: null,
    connectionPointGridCoordinates: null,
    createdAt: '2020-03-04 09:06:09.886657+00',
    updatedAt: '2020-03-04 09:06:09.886657+00',
    partId: 'd6c5bc49-9fb2-4fbb-8a61-77bc8d28036f', // belongsTo
    drawingId: 'c200ce70-11b8-44f7-8dff-4b93f6d1b491', // belongsTo
    drawingPageId: 'd6c5bc49-9fb2-4f1b-8a61-7abc8d380352',
  },
  {
    id: 'e0a01729-c8ed-4090-bda1-9c1b2241f131',
    drawingSheetIndex: 0,
    status: 'AutoInactive',
    notationType: defaultTypeIDs.Dimension, // Dimension
    notationSubtype: defaultSubtypeIDs.Radius, // Radius
    notationClass: 'Tolerance',
    fullSpecification: '2.35 - 2.67 mm',
    quantity: 1,
    nominal: '12.87',
    upperSpecLimit: null,
    lowerSpecLimit: '12.87',
    plusTol: null,
    minusTol: null,
    unit: defaultUnitIDs.millimeter,
    criticality: false,
    inspectionMethod: defaultMethodIDs.Caliper, // Caliper
    notes: '',
    drawingRotation: 0.0,
    drawingScale: 1.0,
    boxLocationY: 20.5,
    boxLocationX: 20.5,
    boxWidth: 250,
    boxHeight: 100,
    boxRotation: 0.0,
    markerGroup: '9',
    markerGroupShared: false,
    markerIndex: 10,
    markerSubIndex: null,
    markerLabel: '9',
    markerStyle: 'f1386f09-9c03-4c59-ada9-acb6a16bfbab',
    markerLocationX: 20,
    markerLocationY: 20,
    markerFontSize: 18,
    markerSize: 36,
    gridCoordinates: null,
    balloonGridCoordinates: null,
    connectionPointGridCoordinates: null,
    createdAt: '2020-03-04 09:06:09.886657+00',
    updatedAt: '2020-03-04 09:06:09.886657+00',
    partId: 'd6c5bc49-9fb2-4fbb-8a61-77bc8d28036f', // belongsTo
    drawingId: 'c200ce70-11b8-44f7-8dff-4b93f6d1b491', // belongsTo
    drawingPageId: 'd6c5bc49-9fb2-4f1b-8a61-7abc8d380352',
  },
  {
    id: 'e0a01769-c8ed-4090-bd8d-9c1b4241f131',
    drawingSheetIndex: 0,
    status: 'AutoInactive',
    notationType: defaultTypeIDs.Dimension, // Dimension
    notationSubtype: defaultSubtypeIDs.Radius, // Radius
    notationClass: 'Tolerance',
    fullSpecification: '2PL 3.14 DIA',
    quantity: 2,
    nominal: '12.87',
    upperSpecLimit: null,
    lowerSpecLimit: '12.87',
    plusTol: null,
    minusTol: null,
    unit: defaultUnitIDs.millimeter,
    criticality: false,
    inspectionMethod: defaultMethodIDs.Caliper, // Caliper
    notes: '',
    drawingRotation: 0.0,
    drawingScale: 1.0,
    boxLocationY: 20.5,
    boxLocationX: 20.5,
    boxWidth: 250,
    boxHeight: 100,
    boxRotation: 0.0,
    markerGroup: '10',
    markerGroupShared: false,
    markerIndex: 11,
    markerSubIndex: null,
    markerLabel: '10',
    markerStyle: 'f1386f09-9c03-4c59-ada9-acb6a16bfbab',
    markerLocationX: 20,
    markerLocationY: 20,
    markerFontSize: 18,
    markerSize: 36,
    gridCoordinates: null,
    balloonGridCoordinates: null,
    connectionPointGridCoordinates: null,
    createdAt: '2020-03-04 09:06:09.886657+00',
    updatedAt: '2020-03-04 09:06:09.886657+00',
    partId: 'd6c5bc49-9fb2-4fbb-8a61-77bc8d28036f', // belongsTo
    drawingId: 'c200ce70-11b8-44f7-8dff-4b93f6d1b491', // belongsTo
    drawingPageId: 'd6c5bc49-9fb2-4f1b-8a61-7abc8d380352',
  },
  {
    id: 'e0a01769-c8ed-4090-bd24-9c1b4df1f131',
    drawingSheetIndex: 0,
    status: 'AutoInactive',
    notationType: defaultTypeIDs.Dimension, // Dimension
    notationSubtype: defaultSubtypeIDs.Radius, // Radius
    notationClass: 'Tolerance',
    fullSpecification: '123456789',
    quantity: 1,
    nominal: '12.87',
    upperSpecLimit: null,
    lowerSpecLimit: '12.87',
    plusTol: null,
    minusTol: null,
    unit: defaultUnitIDs.millimeter,
    criticality: false,
    inspectionMethod: defaultMethodIDs.Caliper, // Caliper
    notes: '',
    drawingRotation: 0.0,
    drawingScale: 1.0,
    boxLocationY: 20.5,
    boxLocationX: 20.5,
    boxWidth: 250,
    boxHeight: 100,
    boxRotation: 0.0,
    markerGroup: '11',
    markerGroupShared: false,
    markerIndex: 12,
    markerSubIndex: null,
    markerLabel: '11',
    markerStyle: 'f1386f09-9c03-4c59-ada9-acb6a16bfbab',
    markerLocationX: 20,
    markerLocationY: 20,
    markerFontSize: 18,
    markerSize: 36,
    gridCoordinates: null,
    balloonGridCoordinates: null,
    connectionPointGridCoordinates: null,
    createdAt: '2020-03-04 09:06:09.886657+00',
    updatedAt: '2020-03-04 09:06:09.886657+00',
    partId: 'd6c5bc49-9fb2-4fbb-8a61-77bc8d28036f', // belongsTo
    drawingId: 'c200ce70-11b8-44f7-8dff-4b93f6d1b491', // belongsTo
    drawingPageId: 'd6c5bc49-9fb2-4f1b-8a61-7abc8d380352',
  },
];

export const listAfterDelete = [
  {
    id: 'e0a01769-c8ed-4090-bd24-9c1b2241f123',
    status: 'AutoInactive',
    drawingSheetIndex: 0,
    notationType: defaultTypeIDs.Dimension, // Dimension
    notationSubtype: defaultSubtypeIDs.Curvilinear, // Curvilinear
    notationClass: 'Tolerance',
    fullSpecification: '1.25 +/- 0.1',
    quantity: 1,
    nominal: '1.25',
    upperSpecLimit: '1.26',
    lowerSpecLimit: '1.24',
    plusTol: '0.1',
    minusTol: '0.1',
    unit: defaultUnitIDs.millimeter,
    criticality: false,
    inspectionMethod: defaultMethodIDs.Caliper, // Caliper
    notes: '',
    drawingRotation: 0,
    drawingScale: 1,
    boxLocationY: 20.5,
    boxLocationX: 20.5,
    boxWidth: 250,
    boxHeight: 100,
    boxRotation: 0,
    markerGroup: '1',
    markerGroupShared: false,
    markerIndex: 0,
    markerSubIndex: null,
    markerLabel: '1',
    markerStyle: 'aad19683-337a-4039-a910-98b0bdd5ee57',
    markerLocationX: 20,
    markerLocationY: 20,
    markerFontSize: 18,
    markerSize: 36,
    gridCoordinates: null,
    balloonGridCoordinates: null,
    connectionPointGridCoordinates: null,
    createdAt: '2020-03-04 09:06:09.886657+00',
    updatedAt: '2020-03-04 09:06:09.886657+00',
    partId: 'd6c5bc49-9fb2-4fbb-8a61-77bc8d28036f',
    drawingId: 'c200ce70-11b8-44f7-8dff-4b93f6d1b491',
    drawingPageId: 'd6c5bc49-9fb2-4f1b-8a61-7abc8d380352',
  },
  {
    id: 'e0a01769-c8ed-4090-bd24-9c1b2241f125',
    drawingSheetIndex: 0,
    status: 'AutoInactive',
    notationType: defaultTypeIDs.Dimension, // Dimension
    notationSubtype: defaultSubtypeIDs.Angle, // Angle
    notationClass: 'Tolerance',
    fullSpecification: '3.86 - 3.92',
    quantity: 1,
    nominal: '3.86 - 3.92',
    upperSpecLimit: '3.92',
    lowerSpecLimit: '3.86',
    plusTol: null,
    minusTol: null,
    unit: defaultUnitIDs.millimeter,
    criticality: false,
    inspectionMethod: defaultMethodIDs.Caliper, // Caliper
    notes: '',
    drawingRotation: 0,
    drawingScale: 1,
    boxLocationY: 20.5,
    boxLocationX: 20.5,
    boxWidth: 250,
    boxHeight: 100,
    boxRotation: 0,
    markerGroup: '2',
    markerGroupShared: false,
    markerIndex: 1,
    markerSubIndex: 0,
    markerLabel: '2.1',
    markerStyle: '8d1a8859-c1bc-4c90-b7c6-2bf1640856ac',
    markerLocationX: 20,
    markerLocationY: 20,
    markerFontSize: 18,
    markerSize: 36,
    gridCoordinates: null,
    balloonGridCoordinates: null,
    connectionPointGridCoordinates: null,
    createdAt: '2020-03-04 09:06:09.886657+00',
    updatedAt: '2020-03-04 09:06:09.886657+00',
    partId: 'd6c5bc49-9fb2-4fbb-8a61-77bc8d28036f',
    drawingId: 'c200ce70-11b8-44f7-8dff-4b93f6d1b491',
    drawingPageId: 'd6c5bc49-9fb2-4f1b-8a61-7abc8d380352',
  },
  {
    id: 'e0a01769-c8ed-4090-bd24-9c1b2241f128',
    drawingSheetIndex: 0,
    status: 'AutoInactive',
    notationType: defaultTypeIDs.Dimension, // Dimension
    notationSubtype: defaultSubtypeIDs.Angle, // Angle
    notationClass: 'Tolerance',
    fullSpecification: '3.92 MAX',
    quantity: 1,
    nominal: '.92',
    upperSpecLimit: '3.92',
    lowerSpecLimit: null,
    plusTol: null,
    minusTol: null,
    unit: defaultUnitIDs.millimeter,
    criticality: false,
    inspectionMethod: defaultMethodIDs.Caliper, // Caliper
    notes: '',
    drawingRotation: 0,
    drawingScale: 1,
    boxLocationY: 20.5,
    boxLocationX: 20.5,
    boxWidth: 250,
    boxHeight: 100,
    boxRotation: 0,
    markerGroup: '2',
    markerGroupShared: false,
    markerIndex: 2,
    markerSubIndex: 1,
    markerLabel: '2.2',
    markerStyle: '8d1a8859-c1bc-4c90-b7c6-2bf1640856ac',
    markerLocationX: 20,
    markerLocationY: 20,
    markerFontSize: 18,
    markerSize: 36,
    gridCoordinates: null,
    balloonGridCoordinates: null,
    connectionPointGridCoordinates: null,
    createdAt: '2020-03-04 09:06:09.886657+00',
    updatedAt: '2020-03-04 09:06:09.886657+00',
    partId: 'd6c5bc49-9fb2-4fbb-8a61-77bc8d28036f',
    drawingId: 'c200ce70-11b8-44f7-8dff-4b93f6d1b491',
    drawingPageId: 'd6c5bc49-9fb2-4f1b-8a61-7abc8d380352',
  },
  {
    id: 'e0a01769-c8ed-4090-bd24-9c1b2221f128',
    drawingSheetIndex: 0,
    status: 'AutoInactive',
    notationType: defaultTypeIDs.Dimension, // Dimension
    notationSubtype: defaultSubtypeIDs.Angle, // Angle
    notationClass: 'Tolerance',
    fullSpecification: '8X 12.87 MIN',
    quantity: 8,
    nominal: '12.87',
    upperSpecLimit: null,
    lowerSpecLimit: '12.87',
    plusTol: null,
    minusTol: null,
    unit: defaultUnitIDs.millimeter,
    criticality: false,
    inspectionMethod: defaultMethodIDs.Caliper, // Caliper
    notes: '',
    drawingRotation: 0,
    drawingScale: 1,
    boxLocationY: 20.5,
    boxLocationX: 20.5,
    boxWidth: 250,
    boxHeight: 100,
    boxRotation: 0,
    markerGroup: '3',
    markerGroupShared: false,
    markerIndex: 3,
    markerSubIndex: null,
    markerLabel: '3',
    markerStyle: 'f1386f09-9c03-4c59-ada9-acb6a16bfbab',
    markerLocationX: 20,
    markerLocationY: 20,
    markerFontSize: 18,
    markerSize: 36,
    gridCoordinates: null,
    balloonGridCoordinates: null,
    connectionPointGridCoordinates: null,
    createdAt: '2020-03-04 09:06:09.886657+00',
    updatedAt: '2020-03-04 09:06:09.886657+00',
    partId: 'd6c5bc49-9fb2-4fbb-8a61-77bc8d28036f',
    drawingId: 'c200ce70-11b8-44f7-8dff-4b93f6d1b491',
    drawingPageId: 'd6c5bc49-9fb2-4f1b-8a61-7abc8d380352',
  },
  {
    id: 'e0a01769-c8ed-4090-bd24-9c1b2e41f130',
    drawingSheetIndex: 0,
    status: 'AutoInactive',
    notationType: defaultTypeIDs.Dimension, // Dimension
    notationSubtype: defaultSubtypeIDs.Angle, // Angle
    notationClass: 'Tolerance',
    fullSpecification: '12.87',
    quantity: 1,
    nominal: '12.87',
    upperSpecLimit: null,
    lowerSpecLimit: '12.87',
    plusTol: null,
    minusTol: null,
    unit: defaultUnitIDs.millimeter,
    criticality: false,
    inspectionMethod: defaultMethodIDs.Caliper, // Caliper
    notes: '',
    drawingRotation: 0,
    drawingScale: 1,
    boxLocationY: 20.5,
    boxLocationX: 20.5,
    boxWidth: 250,
    boxHeight: 100,
    boxRotation: 0,
    markerGroup: '4',
    markerGroupShared: false,
    markerIndex: 4,
    markerSubIndex: null,
    markerLabel: '4',
    markerStyle: 'f1386f09-9c03-4c59-ada9-acb6a16bfbab',
    markerLocationX: 20,
    markerLocationY: 20,
    markerFontSize: 18,
    markerSize: 36,
    gridCoordinates: null,
    balloonGridCoordinates: null,
    connectionPointGridCoordinates: null,
    createdAt: '2020-03-04 09:06:09.886657+00',
    updatedAt: '2020-03-04 09:06:09.886657+00',
    partId: 'd6c5bc49-9fb2-4fbb-8a61-77bc8d28036f',
    drawingId: 'c200ce70-11b8-44f7-8dff-4b93f6d1b491',
    drawingPageId: 'd6c5bc49-9fb2-4f1b-8a61-7abc8d380352',
  },
  {
    id: 'e0a01769-c8ed-409e-bd24-9c1b2241f131',
    drawingSheetIndex: 0,
    status: 'AutoInactive',
    notationType: defaultTypeIDs.Dimension, // Dimension
    notationSubtype: defaultSubtypeIDs.Length, // Length
    notationClass: 'Tolerance',
    fullSpecification: '34 +- 12 THRU',
    quantity: 1,
    nominal: '12.87',
    upperSpecLimit: null,
    lowerSpecLimit: '12.87',
    plusTol: null,
    minusTol: null,
    unit: defaultUnitIDs.millimeter,
    criticality: false,
    inspectionMethod: defaultMethodIDs.Caliper, // Caliper
    notes: '',
    drawingRotation: 0,
    drawingScale: 1,
    boxLocationY: 20.5,
    boxLocationX: 20.5,
    boxWidth: 250,
    boxHeight: 100,
    boxRotation: 0,
    markerGroup: '5',
    markerGroupShared: false,
    markerIndex: 5,
    markerSubIndex: null,
    markerLabel: '5',
    markerStyle: 'f1386f09-9c03-4c59-ada9-acb6a16bfbab',
    markerLocationX: 20,
    markerLocationY: 20,
    markerFontSize: 18,
    markerSize: 36,
    gridCoordinates: null,
    balloonGridCoordinates: null,
    connectionPointGridCoordinates: null,
    createdAt: '2020-03-04 09:06:09.886657+00',
    updatedAt: '2020-03-04 09:06:09.886657+00',
    partId: 'd6c5bc49-9fb2-4fbb-8a61-77bc8d28036f',
    drawingId: 'c200ce70-11b8-44f7-8dff-4b93f6d1b491',
    drawingPageId: 'd6c5bc49-9fb2-4f1b-8a61-7abc8d380352',
  },
  {
    id: 'e0a01769-c8ed-4090-bda4-9c1b22412432',
    drawingSheetIndex: 0,
    status: 'AutoInactive',
    notationType: defaultTypeIDs.Dimension, // Dimension
    notationSubtype: defaultSubtypeIDs.Length, // Length
    notationClass: 'Tolerance',
    fullSpecification: '3.24 / 3.21',
    quantity: 1,
    nominal: '12.87',
    upperSpecLimit: null,
    lowerSpecLimit: '12.87',
    plusTol: null,
    minusTol: null,
    unit: defaultUnitIDs.millimeter,
    criticality: false,
    inspectionMethod: defaultMethodIDs.Caliper, // Caliper
    notes: '',
    drawingRotation: 0,
    drawingScale: 1,
    boxLocationY: 20.5,
    boxLocationX: 20.5,
    boxWidth: 250,
    boxHeight: 100,
    boxRotation: 0,
    markerGroup: '6',
    markerGroupShared: false,
    markerIndex: 6,
    markerSubIndex: null,
    markerLabel: '6',
    markerStyle: 'f1386f09-9c03-4c59-ada9-acb6a16bfbab',
    markerLocationX: 20,
    markerLocationY: 20,
    markerFontSize: 18,
    markerSize: 36,
    gridCoordinates: null,
    balloonGridCoordinates: null,
    connectionPointGridCoordinates: null,
    createdAt: '2020-03-04 09:06:09.886657+00',
    updatedAt: '2020-03-04 09:06:09.886657+00',
    partId: 'd6c5bc49-9fb2-4fbb-8a61-77bc8d28036f',
    drawingId: 'c200ce70-11b8-44f7-8dff-4b93f6d1b491',
    drawingPageId: 'd6c5bc49-9fb2-4f1b-8a61-7abc8d380352',
  },
  {
    id: 'e0a01729-c8ed-4090-bda1-9c1b2241f131',
    drawingSheetIndex: 0,
    status: 'AutoInactive',
    notationType: defaultTypeIDs.Dimension, // Dimension
    notationSubtype: defaultSubtypeIDs.Radius, // Radius
    notationClass: 'Tolerance',
    fullSpecification: '2.35 - 2.67 mm',
    quantity: 1,
    nominal: '12.87',
    upperSpecLimit: null,
    lowerSpecLimit: '12.87',
    plusTol: null,
    minusTol: null,
    unit: defaultUnitIDs.millimeter,
    criticality: false,
    inspectionMethod: defaultMethodIDs.Caliper, // Caliper
    notes: '',
    drawingRotation: 0,
    drawingScale: 1,
    boxLocationY: 20.5,
    boxLocationX: 20.5,
    boxWidth: 250,
    boxHeight: 100,
    boxRotation: 0,
    markerGroup: '7',
    markerGroupShared: false,
    markerIndex: 7,
    markerSubIndex: null,
    markerLabel: '7',
    markerStyle: 'f1386f09-9c03-4c59-ada9-acb6a16bfbab',
    markerLocationX: 20,
    markerLocationY: 20,
    markerFontSize: 18,
    markerSize: 36,
    gridCoordinates: null,
    balloonGridCoordinates: null,
    connectionPointGridCoordinates: null,
    createdAt: '2020-03-04 09:06:09.886657+00',
    updatedAt: '2020-03-04 09:06:09.886657+00',
    partId: 'd6c5bc49-9fb2-4fbb-8a61-77bc8d28036f',
    drawingId: 'c200ce70-11b8-44f7-8dff-4b93f6d1b491',
    drawingPageId: 'd6c5bc49-9fb2-4f1b-8a61-7abc8d380352',
  },
  {
    id: 'e0a01769-c8ed-4090-bd24-9c1b4df1f131',
    drawingSheetIndex: 0,
    status: 'AutoInactive',
    notationType: defaultTypeIDs.Dimension, // Dimension
    notationSubtype: defaultSubtypeIDs.Radius, // Radius
    notationClass: 'Tolerance',
    fullSpecification: '123456789',
    quantity: 1,
    nominal: '12.87',
    upperSpecLimit: null,
    lowerSpecLimit: '12.87',
    plusTol: null,
    minusTol: null,
    unit: defaultUnitIDs.millimeter,
    criticality: false,
    inspectionMethod: defaultMethodIDs.Caliper, // Caliper
    notes: '',
    drawingRotation: 0,
    drawingScale: 1,
    boxLocationY: 20.5,
    boxLocationX: 20.5,
    boxWidth: 250,
    boxHeight: 100,
    boxRotation: 0,
    markerGroup: '8',
    markerGroupShared: false,
    markerIndex: 8,
    markerSubIndex: null,
    markerLabel: '8',
    markerStyle: 'f1386f09-9c03-4c59-ada9-acb6a16bfbab',
    markerLocationX: 20,
    markerLocationY: 20,
    markerFontSize: 18,
    markerSize: 36,
    gridCoordinates: null,
    balloonGridCoordinates: null,
    connectionPointGridCoordinates: null,
    createdAt: '2020-03-04 09:06:09.886657+00',
    updatedAt: '2020-03-04 09:06:09.886657+00',
    partId: 'd6c5bc49-9fb2-4fbb-8a61-77bc8d28036f',
    drawingId: 'c200ce70-11b8-44f7-8dff-4b93f6d1b491',
    drawingPageId: 'd6c5bc49-9fb2-4f1b-8a61-7abc8d380352',
  },
];

export const part = {
  id: 'd6c5bc49-9fb2-4fbb-8a61-77bc8d28036f',
  customerId: '480e0437-e26a-4a03-818d-8494b65a0bec',
  purchaseOrderCode: null,
  purchaseOrderLink: null,
  type: null,
  name: 'Graytech Zextracting',
  number: '12345',
  revision: 'H',
  drawingNumber: null,
  drawingRevision: null,
  defaultMarkerOptions: `{"1":{"style":"f1386f09-9c03-4c59-ada9-acb6a16bfbab","assign":["types","surface finish","structure"]},"2":{"style":"f12c5566-7612-48e9-9534-ef90b276ebaa","assign":["classification","critical"]},"3":{"style":"c858d5fb-5fc2-41f3-b4a6-5aa09ec27307","assign":["classification","major"]},"4":{"style":"d276af65-0938-4e25-aa28-6f12273574ce","assign":["classification","minor"]},"5":{"style":"aad19683-337a-4039-a910-98b0bdd5ee57","assign":["classification","incidental"]},"6":{"style":"8d1a8859-c1bc-4c90-b7c6-2bf1640856ac","assign":["types","dimension","radius"]}}`,
  defaultLinearTolerances: '{}',
  defaultAngularTolerances: '{}',
  status: 'Active',
  reviewStatus: 'Unverified',
  workflowStage: '4',
  completedAt: null,
  balloonedAt: null,
  reportGeneratedAt: null,
  notes: null,
  accessControl: 'None',
  measurement: 'Imperial',
  tags: null,
  siteId: '31ef1d56-1b86-4ffa-b886-507ed8f021c5',
  drawing: { id: 'c200ce70-72b8-44f7-8d3f-4b93f6d1b491' },
  createdAt: '2020-02-28T19:58:03.609Z',
  updatedAt: '2020-02-28T19:58:03.609Z',
};
