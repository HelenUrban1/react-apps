import { defaultTypeIDs, defaultSubtypeIDs, defaultMethodIDs } from 'view/global/defaults';
import { updateAfterEdit, setIndex } from './contextUtilities';
import { defaultUnitIDs } from '../../../../../view/global/defaults';

const mockCharacteristics = [
  {
    id: 'A',
    status: 'Active',
    captureMethod: 'Manual',
    drawingPageIndex: 0,
    notationType: defaultTypeIDs.Dimension, // Dimension
    notationSubtype: defaultSubtypeIDs.Curvilinear, // Curvilinear
    notationClass: 'Tolerance',
    fullSpecification: '1.25 +/- 0.1',
    quantity: 1,
    nominal: '1.25',
    upperSpecLimit: '1.26',
    lowerSpecLimit: '1.24',
    plusTol: '0.1',
    minusTol: '0.1',
    unit: defaultUnitIDs.millimeter,
    criticality: false,
    inspectionMethod: defaultMethodIDs.Caliper, // Caliper
    notes: '',
    drawingRotation: 0.0,
    drawingScale: 1.0,
    boxLocationY: 20.5,
    boxLocationX: 20.5,
    boxWidth: 250,
    boxHeight: 100,
    boxRotation: 0.0,
    markerGroup: '1',
    markerIndex: 0,
    markerSubIndex: null,
    markerLabel: '1',
    markerStyle: 'aad19683-337a-4039-a910-98b0bdd5ee57',
    markerLocationX: 20,
    markerLocationY: 20,
    markerFontSize: 18,
    markerSize: 36,
    gridCoordinates: null,
    balloonGridCoordinates: null,
    connectionPointGridCoordinates: null,
    createdAt: '2020-03-04 09:06:09.886657+00',
    updatedAt: '2020-03-04 09:06:09.886657+00',
    partId: 'd6c5bc49-9fb2-4fbb-8a61-77bc8d28036f',
    drawingId: 'c200ce70-11b8-44f7-8dff-4b93f6d1b491',
    drawingPageId: 'd6c5bc49-9fb2-4f1b-8a61-7abc8d380352',
  },
  {
    id: 'B',
    status: 'Inactive',
    captureMethod: 'Automated',
    drawingPageIndex: 0,
    notationType: defaultTypeIDs.Dimension, // Dimension
    notationSubtype: defaultSubtypeIDs.Curvilinear, // Curvilinear
    notationClass: 'Tolerance',
    fullSpecification: '1.25 +/- 0.1',
    quantity: 1,
    nominal: '1.25',
    upperSpecLimit: '1.26',
    lowerSpecLimit: '1.24',
    plusTol: '0.1',
    minusTol: '0.1',
    unit: defaultUnitIDs.millimeter,
    criticality: false,
    inspectionMethod: defaultMethodIDs.Caliper, // Caliper
    notes: '',
    drawingRotation: 0.0,
    drawingScale: 1.0,
    boxLocationY: 20.5,
    boxLocationX: 20.5,
    boxWidth: 250,
    boxHeight: 100,
    boxRotation: 0.0,
    markerGroup: '2',
    markerIndex: 1,
    markerSubIndex: null,
    markerLabel: '1',
    markerStyle: 'aad19683-337a-4039-a910-98b0bdd5ee57',
    markerLocationX: 20,
    markerLocationY: 20,
    markerFontSize: 18,
    markerSize: 36,
    gridCoordinates: null,
    balloonGridCoordinates: null,
    connectionPointGridCoordinates: null,
    createdAt: '2020-03-04 09:06:09.886657+00',
    updatedAt: '2020-03-04 09:06:09.886657+00',
    partId: 'd6c5bc49-9fb2-4fbb-8a61-77bc8d28036f',
    drawingId: 'c200ce70-11b8-44f7-8dff-4b93f6d1b491',
    drawingPageId: 'd6c5bc49-9fb2-4f1b-8a61-7abc8d380352',
  },
  {
    id: 'C',
    status: 'Inactive',
    captureMethod: 'Automated',
    drawingPageIndex: 0,
    notationType: defaultTypeIDs.Dimension, // Dimension
    notationSubtype: defaultSubtypeIDs.Curvilinear, // Curvilinear
    notationClass: 'Tolerance',
    fullSpecification: '1.25 +/- 0.1',
    quantity: 1,
    nominal: '1.25',
    upperSpecLimit: '1.26',
    lowerSpecLimit: '1.24',
    plusTol: '0.1',
    minusTol: '0.1',
    unit: defaultUnitIDs.millimeter,
    criticality: false,
    inspectionMethod: defaultMethodIDs.Caliper, // Caliper
    notes: '',
    drawingRotation: 0.0,
    drawingScale: 1.0,
    boxLocationY: 20.5,
    boxLocationX: 20.5,
    boxWidth: 250,
    boxHeight: 100,
    boxRotation: 0.0,
    markerGroup: '3',
    markerIndex: 2,
    markerSubIndex: null,
    markerLabel: '1',
    markerStyle: 'aad19683-337a-4039-a910-98b0bdd5ee57',
    markerLocationX: 20,
    markerLocationY: 20,
    markerFontSize: 18,
    markerSize: 36,
    gridCoordinates: null,
    balloonGridCoordinates: null,
    connectionPointGridCoordinates: null,
    createdAt: '2020-03-04 09:06:09.886657+00',
    updatedAt: '2020-03-04 09:06:09.886657+00',
    partId: 'd6c5bc49-9fb2-4fbb-8a61-77bc8d28036f',
    drawingId: 'c200ce70-11b8-44f7-8dff-4b93f6d1b491',
    drawingPageId: 'd6c5bc49-9fb2-4f1b-8a61-7abc8d380352',
  },
];

const editedCharacteristic = {
  id: 'A',
  status: 'Active',
  captureMethod: 'Manual',
  drawingPageIndex: 0,
  notationType: defaultTypeIDs.Dimension, // Dimension
  notationSubtype: defaultSubtypeIDs.Curvilinear, // Curvilinear
  notationClass: 'Tolerance',
  fullSpecification: '1.25 +/- 0.1',
  quantity: 5,
  nominal: '5.25',
  upperSpecLimit: '5.20',
  lowerSpecLimit: '5.30',
  plusTol: '0.5',
  minusTol: '0.5',
  unit: defaultUnitIDs.millimeter,
  criticality: false,
  inspectionMethod: defaultMethodIDs.Caliper, // Caliper
  notes: '',
  drawingRotation: 0.0,
  drawingScale: 1.0,
  boxLocationY: 20.5,
  boxLocationX: 20.5,
  boxWidth: 250,
  boxHeight: 100,
  boxRotation: 0.0,
  markerGroup: '1',
  markerIndex: 0,
  markerSubIndex: null,
  markerLabel: '1',
  markerStyle: 'aad19683-337a-4039-a910-98b0bdd5ee57',
  markerLocationX: 20,
  markerLocationY: 20,
  markerFontSize: 18,
  markerSize: 36,
  gridCoordinates: null,
  balloonGridCoordinates: null,
  connectionPointGridCoordinates: null,
  createdAt: '2020-03-04 09:06:09.886657+00',
  updatedAt: '2020-03-04 09:06:09.886657+00',
  partId: 'd6c5bc49-9fb2-4fbb-8a61-77bc8d28036f',
  drawingId: 'c200ce70-11b8-44f7-8dff-4b93f6d1b491',
  drawingPageId: 'd6c5bc49-9fb2-4f1b-8a61-7abc8d380352',
};

describe('updateAfterEdit', () => {
  it('returns an updated character list', () => {
    const { updatedCharacters } = updateAfterEdit([], [editedCharacteristic], mockCharacteristics);
    expect(updatedCharacters[0]).toBe(editedCharacteristic);
    expect(updatedCharacters).not.toMatchObject(mockCharacteristics);
  });
  it('returns an updated selected list', () => {
    const { updatedSelected } = updateAfterEdit(mockCharacteristics, [editedCharacteristic], mockCharacteristics);
    expect(updatedSelected[0]).toBe(editedCharacteristic);
    expect(updatedSelected).not.toMatchObject(mockCharacteristics);
  });
  it('returns original lists if no edited characteristics are passed', () => {
    const { updatedCharacters, updatedSelected } = updateAfterEdit(mockCharacteristics, [], mockCharacteristics);
    expect(updatedSelected).toMatchObject(mockCharacteristics);
    expect(updatedCharacters).toMatchObject(mockCharacteristics);
  });
});

describe('setIndex', () => {
  it('returns the index of the selected item', () => {
    const index = setIndex([mockCharacteristics[1]], 3);
    expect(index).toBe(1);
  });
  it('returns the lowest index of a group of items', () => {
    const index = setIndex(mockCharacteristics, 3);
    expect(index).toBe(0);
  });
  it('returns zero if the index is beyond the length of items', () => {
    const index = setIndex(mockCharacteristics, 1);
    expect(index).toBe(0);
  });
  it('returns current index if no selected items are provided', () => {
    const index = setIndex([], 0, 3);
    expect(index).toBe(3);
  });
  it('returns zero if no selected items and no current index are provided', () => {
    const index = setIndex([], 0);
    expect(index).toBe(0);
  });
});
