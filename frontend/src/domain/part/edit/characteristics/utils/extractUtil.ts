import { CharacteristicNotationClassEnum, CharacteristicNotationTypeEnum, CharacteristicInput, Characteristic, PartialInput, Classified } from 'types/characteristics';
import { classify } from 'utils/classifier';
import { Tolerancer } from 'utils/ParseTextController/Tolerancer';
import { MarkerStyle } from 'types/marker';
import { Part } from 'domain/part/partTypes';
import { Drawing } from 'graphql/drawing';
import config from 'config';
import log from 'modules/shared/logger';
import { defaultTypeIDs, defaultUnitIDs } from 'view/global/defaults';
import { ListObject } from 'domain/setting/listsTypes';
import { Tolerance } from 'types/tolerances';
import { FeatureUI } from 'utils/ParseTextController/UI';
import { ParseTextController } from 'utils/ParseTextController/ParseTextController';
import { Classifier } from 'utils/ParseTextController/Classifier';
import axios from 'modules/shared/network/axios';
import { Core, WebViewerInstance } from '@pdftron/webviewer';
import { getRefreshedJwt } from 'utils/refreshJwt';
import { captureImage, removeLines } from './captureImage';
import { Grid } from '../../grid/Grid';
import { getCartesianFromPolar, gridPosition } from './coordTransforms';

const { createCanvas } = require('canvas');
const FormData = require('form-data');

// QUESTION: Is this still necessary if the current service returns both Tesseract and Pytorch Models
// Type: 0 = dimension, 1 = GDT, 2 = note
const ocrType = (isGdtOrBasic: boolean, mode: string) => {
  if (isGdtOrBasic) {
    return 1;
  }
  if (mode === 'text') {
    return 2;
  }
  return 0;
};

// Helper function that creates a readable "multipart/form-data" stream to post to the OCR service
export const buildOcrForm = (imageData: Blob, isGdtOrBasic: boolean, mode: string) => {
  const form = new FormData();
  // API Key: 6 = OCR
  form.append('api_key', 6);
  // Thresh: Used to turn image of a character into only black and white pixels.
  // Feed forward networks were trained on images with threshold of 165.
  form.append('thresh', 165);
  // QUESTION: Is this still necessary if the current service returns both Tesseract and Pytorch Models
  // Type: 0 = dimension, 1 = GDT, 2 = note
  form.append('type', ocrType(isGdtOrBasic, mode));
  // Name needs to match filename in image
  form.append('name', 'capture.png');
  form.append('image', imageData, 'capture.png');
  return form;
};

// Helper function that converts the small canvas with the capture to a blob
const getCanvasBlob = (canvas: any): Promise<Blob> => {
  return new Promise((resolve) => {
    canvas.toBlob((blob: Blob) => {
      resolve(blob);
    });
  });
};

// Helper function that creates a small canvas object and populates it with the captured section from the PDF
const getImageDataBlob = async (data: any) => {
  const canvas = createCanvas(data.data.width, data.data.height);
  const context = canvas.getContext('2d');
  context.putImageData(data.data, 0, 0);
  return getCanvasBlob(canvas);
};

// Service adapter that posts the image data to the OCR service
const sendToOCR = async (data: any, mode: string) => {
  try {
    const imageData = await getImageDataBlob(data);
    // QUESTION: Is this still necessary if the current service returns both Tesseract and Pytorch Models
    const form = buildOcrForm(imageData, data.isGdtOrBasic, mode);
    const response = await axios({
      method: 'post',
      url: config.edaUrl,
      data: form,
      withCredentials: false,
    });
    return response.data; // .ocr;
  } catch (error) {
    log.error('extractUtils.sendToOCR', error);
    return { error };
  }
};

const getDownloadedPDF = async (instance: WebViewerInstance, currentPage: number, fileUrl: string, authToken?: string) => {
  // instance might be loaded with a pdf comparison, create a fresh document from the drawing url instead
  let pdf: Core.PDFNet.PDFDoc;

  try {
    pdf = await instance.Core.PDFNet.PDFDoc.createFromURL(`${config.backendUrl}/download?privateUrl=${fileUrl}`, {
      withCredentials: true,
      customHeaders: {
        authorization: authToken ? `Bearer ${authToken}` : '',
      },
    });
  } catch (e) {
    log.error(e);
    const refreshedAuthToken = await getRefreshedJwt();
    pdf = await instance.Core.PDFNet.PDFDoc.createFromURL(`${config.backendUrl}/download?privateUrl=${fileUrl}`, {
      withCredentials: true,
      customHeaders: {
        authorization: refreshedAuthToken ? `Bearer ${refreshedAuthToken}` : '',
      },
    });
  }

  // Need to apply the current rotation to make sure rotated docs extract correctly, zoom can be ignored
  if (currentPage) {
    await pdf.getPage(currentPage).then(async (page) => {
      await page.setRotation(instance.Core.documentViewer.getCompleteRotation(currentPage));
    });
  }
  return pdf;
};

// Service function that extracts an image and converts it to a structured engineering notation
export const parseDimension = async (
  instance: WebViewerInstance,
  fileUrl: string,
  currentPage: number,
  x: number,
  y: number,
  width: number,
  height: number,
  rotation: number,
  dispatch: Function,
  tally: number,
  jwt: string,
  inCompareMode: boolean,
) => {
  // Track how long it takes for events to occur
  const startTime = performance.now();
  const performanceLog: [string, number][] = [];
  let ocrRes;
  let textRes: any;
  let pdf;

  // Get a reference to the PDF
  if (!instance) return {};

  //If in comparison mode download a fresh PDF, otherwise use the pdf in memory
  try {
    pdf = inCompareMode ? await getDownloadedPDF(instance, currentPage, fileUrl, jwt) : await instance.Core.documentViewer.getDocument().getPDFDoc();
  } catch (error) {
    log.error('extractUtils.downloadPDF', error);
  }

  try {
    // Extract the image data from the selected region
    const imageData = await captureImage({
      x,
      y,
      width,
      height,
      rotation,
      sheet: currentPage,
      instance,
      documentUrl: fileUrl,
      pdf,
      single: false,
    });
    performanceLog.push(['captureImage', performance.now() - startTime]);

    // Image pre-processor that optimizes extracted image for OCR???
    // The OCR is trained on unframed characters. Also, the data structure that is
    // returned includes whether or not it found horizontal lines to remove, which
    // is how we decide if it should go to the dimension or GDT OCR in the sendToOCR function.

    const strippedImage = await removeLines(imageData, imageData.width, imageData.height, false, false, 'dimension');

    performanceLog.push(['removeLines', performance.now() - startTime]);
    // updateRectangle('rgb(255,0, 0, 0.5)', x, y, width, height, tally);
    // Send image to OCR service
    if (strippedImage.isGdtOrBasic) {
      // we can skip the text ocr because it is gdt or basic
      ocrRes = await sendToOCR(strippedImage, 'dimension');
    } else {
      [ocrRes, textRes] = await Promise.all([sendToOCR(strippedImage, 'dimension'), sendToOCR(strippedImage, 'text')]); // dimension or text ???
    }

    performanceLog.push(['sendToOCR', performance.now() - startTime]);

    const compareOcrResults = await ParseTextController.compareOCRParseResults(ocrRes, textRes);
    return { parsed: compareOcrResults.parsed, ocrRes: compareOcrResults.ocrRes, captureError: compareOcrResults.captureError, unCompared: ocrRes, textRes };
  } catch (error) {
    log.error('extractUtils.parseDimension', error);
  }
};

/**
 * Extracts a capture from the PDF loaded in the PDFTron web viewer.
 * @param instance PDFTron instance
 * @param fileUrl The PDF file URL
 * @param currentPage
 * @param x X coordinate of capture
 * @param y Y coordinate of capture
 * @param width Width of capture
 * @param height Height of capture
 * @param rotation Rotation of the PDF Viewer
 * @param jwt Users auth token
 * @param inCompareMode Is the Document viewer in compare mode
 * @returns The image data for the capture.
 */
export const captureImageFromCanvas = async (instance: WebViewerInstance, fileUrl: string, currentPage: number, x: number, y: number, width: number, height: number, rotation: number, jwt: string, inCompareMode: boolean) => {
  if (!instance) return undefined;

  //If in comparison mode download a fresh PDF, otherwise use the pdf in memory
  const pdf = inCompareMode ? await getDownloadedPDF(instance, currentPage, fileUrl, jwt) : await instance.Core.documentViewer.getDocument().getPDFDoc();

  try {
    const imageData = await captureImage({
      x,
      y,
      width,
      height,
      rotation,
      sheet: currentPage,
      instance,
      documentUrl: fileUrl,
      pdf,
      single: false,
    });

    const strippedImage: { data: ImageData; isGdtOrBasic: boolean } = await removeLines(imageData, imageData.width, imageData.height, false, false, 'dimension');
    return strippedImage;
  } catch (error) {
    log.error('extractUtils.captureImageFromCanvas', error);
  }
};

const getPageRotation = (page: number, tronInstance: WebViewerInstance) => {
  const rot = tronInstance.Core.documentViewer.getCompleteRotation(page);
  switch (rot) {
    case 0:
      return 0;
    case 1:
      return 90;
    case 2:
      return 180;
    case 3:
      return 270;
    default:
      return 0;
  }
};

export const getMarkerLocationXY = (rotation: number, markerFontSize: number, boxWidth: number, boxHeight: number, boxLocationX: number, boxLocationY: number, pageWidth: number, pageHeight: number) => {
  // boxLocationX is the left coord of the box and boxLocationY is the top of the box given no rotation
  const correctedPageWidth = rotation % 180 === 0 ? pageWidth : pageHeight;
  const correctedBoxWidth = rotation % 180 === 0 ? boxWidth : boxHeight;
  const correctedBoxLocation = rotation % 180 === 0 ? boxLocationX : boxLocationY;

  const balloonPadding = markerFontSize * 2;
  let balloonLocationLeft = correctedBoxLocation - balloonPadding;
  let balloonLocationRight = correctedBoxLocation + correctedBoxWidth + balloonPadding;

  // starts on the left side
  let y = 180;
  // x is the distance from the center of the capture to the balloon
  let x = correctedBoxWidth / 2 + balloonPadding;

  if (rotation === 0 || rotation === 270) {
    // correctedBoxLocation is on the left when rotated 0 or 270 degrees
    // left edge is 0
    if (balloonLocationLeft - markerFontSize < 0) {
      // flip to right
      y = 0;
      if (balloonLocationRight + markerFontSize > correctedPageWidth) {
        // if right is out of bounds too then correct as needed
        const maxDistance = Math.max(correctedBoxLocation - markerFontSize, correctedPageWidth - (correctedBoxLocation + correctedBoxWidth + markerFontSize));
        // find out which side has the most space
        if (maxDistance === correctedBoxLocation - markerFontSize) {
          // left wins so we flip it back left
          x = correctedBoxWidth / 2 + (correctedBoxLocation - markerFontSize) / 2;
          y = 180;
        } else {
          // right wins
          x = correctedBoxWidth / 2 + (correctedPageWidth - (correctedBoxLocation + correctedBoxWidth + markerFontSize)) / 2;
        }
      }
    }
  } else if (rotation === 90 || rotation === 180) {
    // correctedBoxLocation is on the right when rotated 90 or 180 degrees
    // left edge is now pageWidth instead of 0
    balloonLocationLeft = correctedBoxLocation + correctedBoxWidth + balloonPadding;
    balloonLocationRight = correctedBoxLocation - balloonPadding;

    if (balloonLocationLeft - markerFontSize > correctedPageWidth) {
      // flip to right
      y = 0;
      if (balloonLocationRight + markerFontSize < 0) {
        // if right is out of bounds too then correct as needed
        const maxDistance = Math.max(correctedBoxLocation - markerFontSize, correctedPageWidth - (correctedBoxLocation + correctedBoxWidth + markerFontSize));
        // find out which side has the most space
        if (maxDistance === correctedBoxLocation - markerFontSize) {
          // right wins
          x = correctedBoxWidth / 2 + (correctedBoxLocation - markerFontSize) / 2;
        } else {
          // left wins so we flip it back left
          x = correctedBoxWidth / 2 + (correctedPageWidth - (correctedBoxLocation + correctedBoxWidth + markerFontSize)) / 2;
          y = 180;
        }
      }
    }
  }

  return {
    x,
    y,
  };
};

const getPageScale = (page: number, tronInstance: WebViewerInstance) => {
  return tronInstance.Core.documentViewer.getZoomLevel();
};

// This appears to be a helper function that reformats "foo_bar" to "Foo Bar"
// TODO: We shouldn't have to reformat outputs from the classifier service
export const toEnumFormat = (text: string) => {
  if (text === 'extreme' || text === undefined) return 'Note';
  const words = text.split('_');
  for (let i = 0; i < words.length; i++) {
    if (words[i] !== 'a' && words[i] !== 'of') {
      words[i] = words[i].charAt(0).toUpperCase() + words[i].substring(1);
    }
  }
  if (words.length > 1) {
    let enumText = words[0];
    for (let i = 1; i < words.length; i++) {
      enumText += `_${words[i]}`;
    }

    return enumText;
  }
  return words[0];
};

const parseDefaultUnitFromTolerances = (tolerances: any) => {
  if (tolerances && tolerances !== 'none') {
    const thing = JSON.parse(tolerances);
    if (thing && thing.unit) {
      return thing.unit;
    }
  }
  return null;
};

export const getUnit = (type: string, part: any) => {
  if (!type || !part) {
    return null;
  }
  if (type.includes('Angle')) {
    const result = parseDefaultUnitFromTolerances(part.defaultAngularTolerances);
    if (result) {
      return result;
    }
    return defaultUnitIDs.degree; // Default to degrees if can't get from setting
  }

  const result = parseDefaultUnitFromTolerances(part.defaultLinearTolerances);
  if (result) {
    return result;
  }

  if (part.measurement === 'Metric') {
    return defaultUnitIDs.millimeter; // Default to millimeters if can't get from setting
  }
  return defaultUnitIDs.inch; // Default to inches if can't get from setting
};

export const getDimensionInput = (incomplete: PartialInput, result: any, classified: Classified, part: any, featureType: string, tolerances: Tolerance) => {
  const fullInput: CharacteristicInput = {
    ...incomplete,
    fullSpecification: classified.FullSpecification,
    quantity: classified.Quantity,
    nominal: classified.Nominal,
    upperSpecLimit: classified.UpperSpecLimit || '',
    lowerSpecLimit: classified.LowerSpecLimit || '',
    plusTol: classified.PlusTol || '',
    minusTol: classified.MinusTol || '',
    markerGroupShared: false,
    unit: getUnit(toEnumFormat(classified.SubType) as CharacteristicNotationTypeEnum, part),
    criticality: null,
    inspectionMethod: null,
    notes: '',
  };

  const toleranced = Tolerancer.assignFeatureTolerances({ feature: fullInput, tolerances, measurement: fullInput.notationSubtype, isGDT: featureType === defaultTypeIDs['Geometric Tolerance'] });

  if (fullInput.toleranceSource === 'Default_Tolerance') {
    // calculate and assign default tolerances
    fullInput.plusTol = toleranced.plusTol || '';
    fullInput.minusTol = toleranced.minusTol || '';
    fullInput.upperSpecLimit = toleranced.upperSpecLimit || '';
    fullInput.lowerSpecLimit = toleranced.lowerSpecLimit || '';
  }

  // No GDT object on Characteristics in this Branch
  if (featureType === defaultTypeIDs['Geometric Tolerance']) {
    // fullInput.gdt = classified.Gdt;
    fullInput.fullSpecification = classified.FullSpecification;
    fullInput.gdtPrimaryToleranceZone = classified.Gdt?.GdtTol;
    fullInput.gdtSecondaryToleranceZone = classified.Gdt?.GdtTolZone2;
    fullInput.gdtPrimaryDatum = classified.Gdt?.GdtPrimaryDatum;
    fullInput.gdtSecondaryDatum = classified.Gdt?.GdtSecondDatum;
    fullInput.gdtTertiaryDatum = classified.Gdt?.GdtTertiaryDatum;
  }

  return fullInput;
};

const getTrimmed = (str: string) => {
  if (str) {
    return str.trim();
  }
  return '';
};

export const getNoteInput = (incomplete: PartialInput, result: any, classified: Classified) => {
  const fullInput: CharacteristicInput = {
    ...incomplete,
    // need to check properties, but still want empty strings to pass
    fullSpecification: Object.prototype.hasOwnProperty.call(result, 'parsed') && Object.prototype.hasOwnProperty.call(result.parsed, 'input') && result.parsed.input !== null ? getTrimmed(result.parsed.input) : result,
    quantity: classified.Quantity,
    nominal: Object.prototype.hasOwnProperty.call(result, 'parsed') && Object.prototype.hasOwnProperty.call(result.parsed, 'input') && result.parsed.input !== null ? getTrimmed(result.parsed.input) : result,
    upperSpecLimit: classified.UpperSpecLimit,
    lowerSpecLimit: classified.LowerSpecLimit,
    plusTol: classified.PlusTol,
    minusTol: classified.MinusTol,
    markerGroupShared: false,
    unit: '',
    criticality: null,
    inspectionMethod: null,
    notes: '',
  };
  return fullInput;
};

const getOtherInput = (incomplete: PartialInput, classified: Classified, part: any) => {
  const fullInput: CharacteristicInput = {
    ...incomplete,
    fullSpecification: classified.FullSpecification,
    quantity: classified.Quantity,
    nominal: classified.Nominal,
    upperSpecLimit: classified.UpperSpecLimit,
    lowerSpecLimit: classified.LowerSpecLimit,
    plusTol: classified.PlusTol,
    minusTol: classified.MinusTol,
    markerGroupShared: false,
    unit: getUnit(toEnumFormat(classified.SubType) as CharacteristicNotationTypeEnum, part),
    criticality: null,
    inspectionMethod: null,
    notes: '',
  };
  return fullInput;
};

export const classifyCharacteristic = (result: any, tolerances: any) => {
  // Cast the result from the engineering notation parser to a characteristic
  const classified: Classified = classify(result.parsed, tolerances.linear, tolerances.angular);
  const type = classified.Type;
  const subType = classified.SubType;

  let notationClass: CharacteristicNotationClassEnum = 'Tolerance';
  if (classified.IsBasic) {
    classified.FullSpecification = Classifier.updateToBasic(classified.FullSpecification);
    notationClass = 'Basic';
  } else if (classified.IsReference) {
    classified.FullSpecification = Classifier.updateToRef(classified.FullSpecification);
    notationClass = 'Reference';
  }
  return { classified, type, subType, notationClass };
};

// Determines the font size to use in a marker based on
// the font size of the previous item and the height of the capture box
export const getMarkerFontSize = (lastItem: Characteristic | undefined, height: number, width: number, rotation: number) => {
  if (lastItem) {
    return lastItem.markerFontSize || 18;
  }
  return FeatureUI.getFontSize({ height, width, rotation });
};

const prepareCharacteristicInput = (incomplete: PartialInput, result: any, classified: Classified, part: Part, tolerances: Tolerance) => {
  let characteristicInput;
  if (incomplete.notationType === defaultTypeIDs.Dimension || incomplete.notationType === defaultTypeIDs['Geometric Tolerance']) {
    characteristicInput = getDimensionInput(incomplete, result, classified, part, incomplete.notationType, tolerances);
  } else if (incomplete.notationType === defaultTypeIDs.Note) {
    characteristicInput = getNoteInput(incomplete, result, classified);
  } else {
    characteristicInput = getOtherInput(incomplete, classified, part);
  }
  return characteristicInput;
};

export const createNewItem = (
  result: any,
  part: Part,
  partDrawing: Drawing,
  currentPage: number,
  lastItem: Characteristic | undefined,
  tronInstance: WebViewerInstance,
  classified: Classified,
  type: string, // UUID
  subtype: string, // UUID
  notationClass: CharacteristicNotationClassEnum,
  // create: Function,
  assignedStyles: { [key: string]: { style: string; assign: string[] } },
  boxLocationX: number,
  boxLocationY: number,
  boxWidth: number,
  boxHeight: number,
  boxRotation: number,
  gridLoc: string,
  tolerances: Tolerance,
  canvas: fabric.Canvas,
  lastLabel: number | null,
  newIndex: number,
  markerSize: number | null,
) => {
  if (!part.id) return null;

  // Cast the result from the engineering notation parser to a characteristic
  // const { classified, enumType, enumSubType, notationClass } = classifyCharacteristic(result, partEditor.tolerances);

  const markerFontSize = markerSize ? Math.floor(markerSize / 2) : getMarkerFontSize(lastItem, boxHeight, boxWidth, boxRotation);
  const rotation = getPageRotation(currentPage, tronInstance);
  const scale: number = tronInstance ? tronInstance.Core.documentViewer.getZoomLevel() : 1;

  const normalizedPageWidth = rotation % 180 === 0 ? canvas.getWidth() / scale : canvas.getHeight() / scale;
  const normalizedPageHeight = rotation % 180 === 0 ? canvas.getHeight() / scale : canvas.getWidth() / scale;
  const markerLocation = getMarkerLocationXY(rotation, markerFontSize, boxWidth, boxHeight, boxLocationX, boxLocationY, normalizedPageWidth, normalizedPageHeight);
  const incomplete: PartialInput = {
    drawingSheetIndex: currentPage,
    fullSpecificationFromOCR: result.ocrRes,
    confidence: -1, // No confidence is returned from current service
    status: 'Active',
    notationType: type,
    notationSubtype: subtype,
    notationClass,
    drawingRotation: rotation,
    drawingScale: getPageScale(currentPage, tronInstance),
    boxLocationY,
    boxLocationX,
    boxWidth,
    boxHeight,
    boxRotation,
    markerGroup: lastLabel ? (lastLabel + 1).toString() : '1',
    markerGroupShared: false,
    markerIndex: newIndex,
    markerSubIndex: -1,
    markerLabel: lastLabel ? (lastLabel + 1).toString() : '1',
    markerStyle: FeatureUI.getTypeMarkerStyle({ styleOptions: assignedStyles, type, subtype }).style,
    markerLocationX: markerLocation?.x ? markerLocation.x : 0, // radius, half the box width + the balloon radius + a little extra
    markerLocationY: markerLocation?.y ? markerLocation.y : 0, // degrees, places marker to the left if there's room, to the right if not
    markerFontSize,
    markerSize: markerSize || markerFontSize * 2,
    markerRotation: rotation,
    gridCoordinates: gridLoc, // partEditor.partDrawing.sheets[currentPage - 1].grid.
    balloonGridCoordinates: gridLoc,
    connectionPointGridCoordinates: gridLoc,
    part: part.id,
    drawing: partDrawing.id,
    drawingSheet: partDrawing.sheets[currentPage - 1].id,
    captureMethod: 'Manual',
    captureError: result.captureError,
    verified: false,
    toleranceSource: classified.LowerSpecLimit || classified.UpperSpecLimit ? 'Document_Defined' : 'Default_Tolerance',
    connectionPointLocationX: markerLocation?.y === 0 ? boxLocationX + boxWidth / 2 : boxLocationX - boxWidth / 2, // show point on the right if capture is close to edge
    connectionPointLocationY: boxLocationY,
    connectionPointIsFloating: false,
    displayLeaderLine: true,
    leaderLineDistance: 0,
  };
  return prepareCharacteristicInput(incomplete, result, classified, part, tolerances);
};

// Updates the gridCoordinates for all the items on a page, usually after a grid update
export const updateItemCoordinates = (items: Characteristic[], grid: Grid, canvas: fabric.Canvas, scale: number, rotation: number, useGrid = true) => {
  const updates: Characteristic[] = [];
  // iterates over all capturedItems (characteristics) on the current page
  items.forEach((item: Characteristic) => {
    const newItem = { ...item };
    const boxCartesian = {
      x: newItem.boxLocationX + newItem.boxWidth / 2,
      y: newItem.boxLocationY + newItem.boxHeight / 2,
    };
    // get the Characteristic from the fabric object
    const balloon = getCartesianFromPolar(boxCartesian, { x: newItem.markerLocationX, y: newItem.markerLocationY }, newItem.markerRotation);
    const { gridCoordinates, connectionPointGridCoordinates, balloonGridCoordinates } = gridPosition(balloon, { left: newItem.boxLocationX, top: newItem.boxLocationY }, rotation, canvas, grid, newItem);

    const newFeatureLoc = useGrid ? gridCoordinates : '';
    const newBalloonLoc = useGrid ? balloonGridCoordinates : '';
    const newConnectionLoc = useGrid ? connectionPointGridCoordinates : '';
    // only update the Characteristic if the new location is different than the old
    if (newFeatureLoc !== newItem.gridCoordinates || newBalloonLoc !== newItem.balloonGridCoordinates || newConnectionLoc !== newItem.connectionPointGridCoordinates) {
      newItem.gridCoordinates = newFeatureLoc;
      newItem.balloonGridCoordinates = newBalloonLoc;
      newItem.connectionPointGridCoordinates = newConnectionLoc;
      updates.push(newItem);
    }
  });

  // update the CharacteristicsContext and the Characteristics in the DB
  return updates;
};

// Factory class that generates Feature value objects from the output of parseDimension (the OCR service wrapper)
// TODO: Instead of passing partEditor into this function, pass a config object which holds the values for tolerances and balloon styles
export const updateExistingItem = (
  result: any,
  part: Part,
  assignedStyles: { [key: string]: { style: string; assign: string[] } },
  existingItem: Characteristic,
  type: string,
  subtype: string,
  notationClass: CharacteristicNotationClassEnum,
  classified: Classified,
  types: ListObject,
  tolerances: Tolerance,
) => {
  if (!part.id || !existingItem) return null;
  // Cast the result from the engineering notation parser to a characteristic - replaced by UUIDs
  // const { classified, enumType, enumSubType, notationClass } = classifyCharacteristic(result, partEditor.tolerances);

  // Prepare the UI object from the characteristic
  const returnItem = { ...existingItem, plusTol: classified.PlusTol, minusTol: classified.MinusTol, upperSpecLimit: classified.UpperSpecLimit, lowerSpecLimit: classified.LowerSpecLimit };

  returnItem.confidence = -1; // No confidence is returned from current service
  returnItem.fullSpecificationFromOCR = result.ocrRes;
  returnItem.notationType = type;
  returnItem.notationSubtype = subtype;
  returnItem.notationClass = notationClass;
  returnItem.markerStyle = FeatureUI.getMarkerStyle({ styleOptions: assignedStyles, type, subtype, classification: returnItem.criticality, operation: returnItem.operation, inspectionMethod: returnItem.inspectionMethod }).style;
  returnItem.captureMethod = existingItem.captureMethod === 'Custom' ? 'Custom' : 'Manual';
  returnItem.captureError = result.captureError;
  returnItem.verified = false;
  returnItem.toleranceSource = classified.LowerSpecLimit || classified.UpperSpecLimit ? 'Document_Defined' : 'Default_Tolerance';

  if (type === defaultTypeIDs.Note) {
    // need to check properties, but still want empty strings to pass
    if (Object.prototype.hasOwnProperty.call(result, 'parsed') && Object.prototype.hasOwnProperty.call(result.parsed, 'input') && result.parsed.input !== null) {
      returnItem.fullSpecification = getTrimmed(result.parsed.input);
      returnItem.nominal = getTrimmed(result.parsed.input);
    } else {
      returnItem.fullSpecification = result;
      returnItem.nominal = result;
    }
    returnItem.unit = '';
  } else {
    // All items besides Notes
    returnItem.fullSpecification = classified.FullSpecification;
    returnItem.nominal = classified.Nominal;
    returnItem.unit = getUnit(toEnumFormat(classified.SubType) as CharacteristicNotationTypeEnum, part);
  }

  // If the item is a GD&T
  if (type === defaultTypeIDs['Geometric Tolerance']) {
    returnItem.fullSpecification = classified.FullSpecification;
    returnItem.gdtPrimaryToleranceZone = classified.Gdt?.GdtTol;
    returnItem.gdtSecondaryToleranceZone = classified.Gdt?.GdtTolZone2;
    returnItem.gdtPrimaryDatum = classified.Gdt?.GdtPrimaryDatum;
    returnItem.gdtSecondaryDatum = classified.Gdt?.GdtSecondDatum;
    returnItem.gdtTertiaryDatum = classified.Gdt?.GdtTertiaryDatum;
  } else if (type !== defaultTypeIDs.Note) {
    // feature is a dimension, re-calculate the tolerances
    const newTolerances = Tolerancer.assignFeatureTolerances({ feature: returnItem, tolerances, measurement: returnItem.notationSubtype, isGDT: false });
    returnItem.upperSpecLimit = newTolerances.upperSpecLimit || '';
    returnItem.lowerSpecLimit = newTolerances.lowerSpecLimit || '';
    returnItem.plusTol = newTolerances.plusTol || '';
    returnItem.minusTol = newTolerances.minusTol || '';
  }

  // All items INCLUDING Notes
  // These are required fields on the CharacteristicInput type
  returnItem.quantity = classified.Quantity;

  return returnItem;
};

// TODO: Make sure Item Type and Subtype are UUIDs - might need to convert strings to UUIDs
export const revealCharacteristic = (region: fabric.Object, assignedStyles: { [key: string]: MarkerStyle }, markerGroup: string, markerIndex: number, characteristicId: string) => {
  const newItem = { ...region.toObject() };
  newItem.id = characteristicId;
  newItem.status = 'Active';
  newItem.markerGroup = markerGroup;
  newItem.markerIndex = markerIndex;
  newItem.markerSubIndex = -1;
  newItem.markerLabel = markerGroup;
  newItem.markerLocationX = newItem.markerSize * 2; // distance away from the edge of the capture
  newItem.markerLocationY = newItem.boxLocationX - newItem.markerSize * 2 < 0 ? 0 : 180; // degrees, positions marker to the direct left if there's room, to the right if not
  newItem.markerStyle = assignedStyles ? FeatureUI.getTypeMarkerStyle({ styleOptions: assignedStyles, type: newItem.notationType, subtype: newItem.notationSubtype }).style : 'f12c5566-7612-48e9-9534-ef90b276ebaa'; // default red circle
  return newItem;
};

export const getSelectionType = (canvas: fabric.Canvas, e: any, multi: boolean) => {
  if (multi || e.e.ctrlKey) {
    return 'multi';
  }
  const activeObject = canvas.getActiveObject();
  if (activeObject && ['balloon', 'extract', 'region', 'annotation-text', 'annotation'].includes(activeObject.type || '')) {
    return 'single';
  }
  return 'new';
};

export const getCharacteristicMarkerChanges = (list: Characteristic[], styles: { [key: string]: MarkerStyle }) => {
  const updates: Characteristic[] = [];
  list.forEach((c) => {
    const marker = FeatureUI.getTypeMarkerStyle({ styleOptions: styles, type: c.notationType, subtype: c.notationSubtype });
    if (marker.matched && marker.style !== c.markerStyle) {
      updates.push({ ...c, markerStyle: marker.style });
    }
  });
  return updates;
};
