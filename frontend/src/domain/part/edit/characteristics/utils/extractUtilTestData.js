import { fabric } from 'fabric';
import { Grid } from 'domain/part/edit/grid/Grid';
import { defaultTypeIDs, defaultSubtypeIDs, defaultMethodIDs, defaultUnitIDs } from 'view/global/defaults';

export const testPart = {
  id: 'test-id',
  purchaseOrderCode: '1',
  purchaseOrderLink: '1',
  type: 'Part',
  name: 'Test Part',
  number: '123',
  revision: 'A',
  drawingNumber: '12',
  drawingRevision: 'A',
  defaultMarkerOptions: {
    1: {
      style: 'f1386f09-9c03-4c59-ada9-acb6a16bfbab',
      assign: ['types', 'surface finish', 'structure'],
    },
    2: { style: 'f12c5566-7612-48e9-9534-ef90b276ebaa', assign: ['classification', 'critical'] },
    3: { style: 'c858d5fb-5fc2-41f3-b4a6-5aa09ec27307', assign: ['classification', 'major'] },
    4: { style: 'd276af65-0938-4e25-aa28-6f12273574ce', assign: ['classification', 'minor'] },
    5: { style: 'aad19683-337a-4039-a910-98b0bdd5ee57', assign: ['classification', 'incidental'] },
    6: { style: '8d1a8859-c1bc-4c90-b7c6-2bf1640856ac', assign: ['types', 'dimension', 'radius'] },
  },
  defaultLinearTolerances: '{}',
  defaultAngularTolerances: '{}',
  notes: 'notes',
  drawings: [
    {
      linearUnit: defaultUnitIDs.millimeter,
      angularUnit: defaultUnitIDs.degree,
    },
  ],
  accessControl: 'None',
  measurement: 'Imperial',
  site: '31ef1d56-1b86-4ffa-b886-507ed8f021c5',
  createdAt: '',
  updatedAt: '',
};

export const testPartialInput = {
  drawingSheetIndex: 1,
  status: 'Active',
  notationType: defaultTypeIDs.Dimension, // Dimension
  notationSubtype: defaultSubtypeIDs.Length, // Length
  notationClass: 'Tolerance',
  drawingRotation: 0,
  drawingScale: 1,
  boxLocationY: 20,
  boxLocationX: 20,
  boxWidth: 40,
  boxHeight: 20,
  boxRotation: 0,
  markerGroup: '1',
  markerGroupShared: false,
  markerIndex: 0,
  markerSubIndex: -1,
  markerLabel: '1',
  markerStyle: 'f12c5566-7612-48e9-9534-ef90b276ebaa',
  markerLocationX: 20,
  markerLocationY: 20,
  markerFontSize: 18,
  markerSize: 36,
  gridCoordinates: '',
  balloonGridCoordinates: '',
  connectionPointGridCoordinates: '',
  part: 'f12c5566-7612-48e9-9534-ef90b276ebaa',
  drawing: 'f12c5566-7612-48e9-9534-ef90b276ebaa',
  captureMethod: 'Automatic',
  drawingSheet: 'f12c5566-7612-48e9-9534-ef90b276ebaa',
  verified: false,
  toleranceSource: 'Document_Defined',
};

export const testNotePartialInput = {
  drawingSheetIndex: 1,
  status: 'Active',
  notationType: defaultTypeIDs.Note, // Note
  notationSubtype: defaultSubtypeIDs.Note, // Note
  notationClass: 'Tolerance',
  drawingRotation: 0,
  drawingScale: 1,
  boxLocationY: 20,
  boxLocationX: 20,
  boxWidth: 40,
  boxHeight: 20,
  boxRotation: 0,
  markerGroup: '5',
  markerGroupShared: false,
  markerIndex: 9,
  markerSubIndex: 4,
  markerLabel: '5.5',
  markerStyle: 'f12c5566-7612-48e9-9534-ef90b276ebaa',
  markerLocationX: 20,
  markerLocationY: 20,
  markerFontSize: 18,
  markerSize: 36,
  gridCoordinates: '',
  balloonGridCoordinates: '',
  connectionPointGridCoordinates: '',
  part: 'f12c5566-7612-48e9-9534-ef90b276ebaa',
  drawing: 'f12c5566-7612-48e9-9534-ef90b276ebaa',
  captureMethod: 'Automatic',
  drawingSheet: 'f12c5566-7612-48e9-9534-ef90b276ebaa',
  verified: false,
  toleranceSource: 'Document_Defined',
};

export const testNoteBlankTextPartialInput = {
  drawingSheetIndex: 1,
  status: 'Active',
  notationType: defaultTypeIDs.Note, // Note
  notationSubtype: defaultSubtypeIDs.Note, // Note
  notationClass: 'Tolerance',
  drawingRotation: 0,
  drawingScale: 1,
  boxLocationY: 20,
  boxLocationX: 20,
  boxWidth: 40,
  boxHeight: 20,
  boxRotation: 0,
  markerGroup: '5',
  markerGroupShared: false,
  markerIndex: 9,
  markerSubIndex: 4,
  markerLabel: '5.5',
  markerStyle: 'f12c5566-7612-48e9-9534-ef90b276ebaa',
  markerLocationX: 20,
  markerLocationY: 20,
  markerFontSize: 18,
  markerSize: 36,
  gridCoordinates: '',
  balloonGridCoordinates: '',
  connectionPointGridCoordinates: '',
  part: 'f12c5566-7612-48e9-9534-ef90b276ebaa',
  drawing: 'f12c5566-7612-48e9-9534-ef90b276ebaa',
  captureMethod: 'Automatic',
  drawingSheet: 'f12c5566-7612-48e9-9534-ef90b276ebaa',
  verified: false,
  toleranceSource: '',
};

export const testClassified = {
  Type: 'Dimension',
  SubType: 'Length',
  Gdt: {
    GdtSymbol: '',
    GdtTol: '',
    GdtTolZone2: '',
    GdtPrimaryDatum: '',
    GdtSecondDatum: '',
    GdtTertiaryDatum: '',
  },
  Quantity: 1,
  FullSpecification: '1.25',
  Nominal: '1.25',
  PlusTol: '',
  MinusTol: '',
  UpperSpecLimit: '1.25',
  LowerSpecLimit: '1.25',
  IsBasic: false,
  IsReference: false,
};

export const testNoteClassified = {
  Type: 'Note',
  SubType: 'Note',
  Gdt: {
    GdtSymbol: '',
    GdtTol: '',
    GdtTolZone2: '',
    GdtPrimaryDatum: '',
    GdtSecondDatum: '',
    GdtTertiaryDatum: '',
  },
  Quantity: 1,
  FullSpecification: 'This is a Note',
  Nominal: 'This is a Note',
  PlusTol: '',
  MinusTol: '',
  UpperSpecLimit: '',
  LowerSpecLimit: '',
  IsBasic: false,
  IsReference: false,
};

export const testFabRect = new fabric.Rect({
  left: 120,
  top: 100,
  width: 50,
  height: 60,
  angle: 90,
  toObject: () => {
    return {
      status: 'Inactive',
      markerGroup: null,
      markerGroupShared: false,
      markerIndex: null,
      markerSubIndex: null,
      markerLabel: null,
      markerLocationX: 15, // radius
      markerLocationY: 0,
      markerRotation: 0,
      markerFontSize: 18,
      markerSize: 36,
      boxLocationX: 70,
      boxLocationY: 60,
      boxWidth: 50,
      boxHeight: 15,
      boxRotation: 0,
      notationSubtype: defaultSubtypeIDs.Length, // Length
      markerStyle: 'f12c5566-7612-48e9-9534-ef90b276ebaa',
      minusTol: '0',
      nominal: '12.87',
      notationClass: 'Basic',
      notationType: defaultTypeIDs.Dimension, // Dimension
      notes: '',
      part: 'd6c5bc49-9fb2-4fbb-8a61-77bc8d28036f',
      plusTol: '0',
      quantity: 1,
      toleranceSource: 'Document_Defined',
      unit: defaultUnitIDs.millimeter,
      upperSpecLimit: '12.87',
      verified: false,
      captureMethod: 'Automated',
      criticality: undefined,
      drawing: 'c200ce70-72b8-44f7-8d3f-4b93f6d1b491',
      drawingRotation: 0,
      drawingScale: 1,
      drawingSheet: 'c200ce70-72b8-44f7-8d3f-4b93f6d1b491',
      drawingSheetIndex: 2,
      fullSpecification: '3.24 / 3.21',
      gridCoordinates: '',
      balloonGridCoordinates: '',
      connectionPointGridCoordinates: '',
      inspectionMethod: defaultMethodIDs.Caliper, // Caliper
      lowerSpecLimit: '12.87',
    };
  },
});

const fabRect = new fabric.Rect({
  top: 40,
  left: 80,
  width: 40,
  height: 30,
  toObject: () => {
    return {
      status: 'Inactive',
      markerGroup: '1',
      drawingSheetIndex: 1,
      markerGroupShared: false,
      markerIndex: 0,
      markerSubIndex: -1,
      markerLabel: '1',
      markerLocationX: 15, // radius
      markerLocationY: 0,
      markerRotation: 0,
      markerFontSize: 18,
      markerSize: 36,
      boxLocationX: 70,
      boxLocationY: 60,
      boxWidth: 50,
      boxHeight: 15,
      boxRotation: 0,
      notationSubtype: 'edc8ecda-5f88-4b81-871f-6c02f1c50afd',
      markerStyle: 'f12c5566-7612-48e9-9534-ef90b276ebaa',
      minusTol: '0',
      nominal: '12.87',
      notationClass: 'Basic',
      notationType: 'db687ac1-7bf8-4ada-be2b-979f8921e1a0',
      notes: '',
      part: 'd6c5bc49-9fb2-4fbb-8a61-77bc8d28036f',
      plusTol: '0',
      quantity: 1,
      toleranceSource: 'Document_Defined',
      unit: 'b909e87e-0445-4ea4-857e-3c1dd31b530b',
      upperSpecLimit: '12.87',
      verified: false,
      captureMethod: 'Automated',
      criticality: undefined,
      drawing: 'c200ce70-72b8-44f7-8d3f-4b93f6d1b491',
      drawingRotation: 0,
      drawingScale: 1,
      drawingSheet: 'c200ce70-72b8-44f7-8d3f-4b93f6d1b491',
      confidence: -1,
      captureError: null,
      drawingSheetIndex: 2,
      fullSpecification: '3.24 / 3.21',
      gridCoordinates: '',
      balloonGridCoordinates: '',
      connectionPointGridCoordinates: '',
      inspectionMethod: null,
      lowerSpecLimit: '12.87',
    };
  },
});

const fabGroup = new fabric.Group();
fabGroup.add(fabRect);

export const testCapturedItem = {
  itemNumber: '2',
  rectangle: fabRect,
  balloon: fabGroup,
  boxLocationY: 50,
  boxLocationX: 50,
  boxRotation: 0,
  markerRotation: 0,
};

export const grid = new Grid({
  initialCanvasWidth: 1280,
  initialCanvasHeight: 720,
  initialPageRotation: 0,
});

export const canvas = new fabric.Canvas(null, { width: 1280, height: 720 });

export const testParsedObject = {
  notation: {
    type: '1.25',
    nominal: {
      text: '1.25',
      value: '1.25',
      units: 'mm',
    },
    plus: {
      text: '0.1',
      value: '0.1',
      units: '',
    },
    minus: {
      text: '0.1',
      value: '0.1',
      units: '',
    },
    text: '1.25',
  },
  quantity: {
    value: '1',
    units: '',
    text: '1',
  },
  words: '1.25 +/- 0.1',
  text: '1.25 +/- 0.1',
  input: '1.25 +/- 0.1',
};

export const testCharacteristic = {
  id: 'e0a01769-c8ed-4090-bda4-9c1b22412432',
  drawingSheetIndex: 2,
  status: 'Inactive',
  captureMethod: 'Automated',
  notationType: defaultTypeIDs.Dimension, // Dimension
  notationSubtype: defaultSubtypeIDs.Length, // Length
  notationClass: 'Basic',
  fullSpecification: '3.24 / 3.21',
  quantity: 1,
  nominal: '12.87',
  upperSpecLimit: '12.87',
  lowerSpecLimit: '12.87',
  plusTol: '0',
  minusTol: '0',
  unit: defaultUnitIDs.millimeter,
  toleranceSource: 'Document_Defined',
  inspectionMethod: defaultMethodIDs.Caliper, // Caliper
  criticality: undefined,
  notes: '',
  drawingRotation: 0.0,
  drawingScale: 1.0,
  boxLocationY: 120.5,
  boxLocationX: 120.5,
  boxWidth: 250,
  boxHeight: 100,
  boxRotation: 0.0,
  markerGroup: '1',
  markerGroupShared: false,
  markerIndex: 0,
  markerSubIndex: -1,
  markerLabel: '1',
  markerStyle: 'f1386f09-9c03-4c59-ada9-acb6a16bfbab',
  markerLocationX: 100.5,
  markerLocationY: 120.5,
  markerFontSize: 18,
  markerSize: 36,
  gridCoordinates: '',
  balloonGridCoordinates: '',
  connectionPointGridCoordinates: '',
  verified: false,
  part: {
    id: 'd6c5bc49-9fb2-4fbb-8a61-77bc8d28036f', // belongsTo
  },
  drawing: {
    id: 'c200ce70-72b8-44f7-8d3f-4b93f6d1b491', // belongsTo
  },
  drawingSheet: {
    id: 'd6c5bc49-9fb2-4fbb-8a61-77bc8d380352',
  },
  createdAt: Date.now(),
  updatedAt: Date.now(),
  deletedAt: Date.now(),
};

export const testQtyCharacteristic = {
  id: 'e0a01769-c8ed-4090-bda4-9c1b22412432',
  drawingSheetIndex: 2,
  status: 'Inactive',
  captureMethod: 'Automated',
  notationType: defaultTypeIDs.Dimension, // Dimension
  notationSubtype: defaultSubtypeIDs.Length, // Length
  notationClass: 'Basic',
  fullSpecification: '3.24 / 3.21',
  quantity: 3,
  nominal: '12.87',
  upperSpecLimit: '12.87',
  lowerSpecLimit: '12.87',
  plusTol: '0',
  minusTol: '0',
  unit: defaultUnitIDs.millimeter,
  toleranceSource: 'Document_Defined',
  inspectionMethod: defaultMethodIDs.Caliper, // Caliper
  criticality: undefined,
  notes: '',
  drawingRotation: 0.0,
  drawingScale: 1.0,
  boxLocationY: 120.5,
  boxLocationX: 120.5,
  boxWidth: 250,
  boxHeight: 100,
  boxRotation: 0.0,
  markerGroup: '1',
  markerGroupShared: false,
  markerIndex: 0,
  markerSubIndex: -1,
  markerLabel: '1',
  markerStyle: 'f1386f09-9c03-4c59-ada9-acb6a16bfbab',
  markerLocationX: 100.5,
  markerLocationY: 120.5,
  markerFontSize: 18,
  markerSize: 36,
  gridCoordinates: '',
  balloonGridCoordinates: '',
  connectionPointGridCoordinates: '',
  verified: false,
  part: {
    id: 'd6c5bc49-9fb2-4fbb-8a61-77bc8d28036f', // belongsTo
  },
  drawing: {
    id: 'c200ce70-72b8-44f7-8d3f-4b93f6d1b491', // belongsTo
  },
  drawingSheet: {
    id: 'd6c5bc49-9fb2-4fbb-8a61-77bc8d380352',
  },
  createdAt: Date.now(),
  updatedAt: Date.now(),
  deletedAt: Date.now(),
};

export const testRevealedData = {
  drawingSheetIndex: 2,
  captureMethod: 'Automated',
  status: 'Active',
  verified: false,
  notationType: defaultTypeIDs.Dimension, // Dimension
  notationSubtype: defaultSubtypeIDs.Length, // Length
  notationClass: 'Tolerance',
  fullSpecification: '3.24 / 3.21',
  quantity: 1,
  nominal: '12.87',
  upperSpecLimit: '',
  lowerSpecLimit: '12.87',
  plusTol: '',
  minusTol: '',
  unit: defaultUnitIDs.millimeter,
  toleranceSource: 'Document_Defined',
  inspectionMethod: defaultMethodIDs.Caliper, // Caliper
  criticality: undefined,
  notes: '',
  drawingRotation: 0.0,
  drawingScale: 1.0,
  boxLocationY: 250,
  boxLocationX: 250,
  boxWidth: 250,
  boxHeight: 100,
  boxRotation: 0.0,
  markerGroup: '2',
  markerGroupShared: false,
  markerIndex: 2,
  markerSubIndex: -1,
  markerLabel: '2',
  markerStyle: 'f1386f09-9c03-4c59-ada9-acb6a16bfbab',
  markerLocationX: 95,
  markerLocationY: 250,
  markerFontSize: 18,
  markerSize: 36,
  gridCoordinates: '',
  balloonGridCoordinates: '',
  connectionPointGridCoordinates: '',
  part: 'd6c5bc49-9fb2-4fbb-8a61-77bc8d28036f', // belongsTo
  drawing: 'c200ce70-72b8-44f7-8d3f-4b93f6d1b491', // belongsTo
  drawingSheet: 'd6c5bc49-9fb2-4fbb-8a61-77bc8d380352',
};

export const testResult = {
  input: '1.25',
};

export const testNoteResult = {
  parsed: {
    input: 'This is a Note',
  },
};

export const testNoteBlankResult = {
  parsed: {
    input: '',
  },
};

export const testNoteTextResult = {
  parsed: {
    input: 'This is a Note',
  },
};

export const testAssignedStyles = {
  default: { style: 'f12c5566-7612-48e9-9534-ef90b276ebaa', assign: ['Default'] },
  1: {
    style: 'f1386f09-9c03-4c59-ada9-acb6a16bfbab',
    assign: ['Type', '9f78f226-11b1-4e70-9705-440c0ef290bc', '4e6617aa-5f61-4257-8e18-787b80ddece1'],
  }, // surface finish, structure
  2: {
    style: 'f12c5566-7612-48e9-9534-ef90b276ebaa',
    assign: ['Classification', '168eba1e-a8bf-41bb-a0d2-955e8a7b8d66'],
  }, // Critical
  3: {
    style: 'c858d5fb-5fc2-41f3-b4a6-5aa09ec27307',
    assign: ['Classification', '187b5423-dafd-402a-b471-2ed2fe76d2f9'],
  }, // Major
  4: {
    style: 'd276af65-0938-4e25-aa28-6f12273574ce',
    assign: ['Classification', '502a8ba4-82a7-423a-a339-b8f358ba3d3a'],
  }, // Minor
  5: {
    style: 'aad19683-337a-4039-a910-98b0bdd5ee57',
    assign: ['Classification', '975a513d-394c-4123-901e-1244756b488e'],
  }, // Incidental
  6: {
    style: '8d1a8859-c1bc-4c90-b7c6-2bf1640856ac',
    assign: ['Type', 'db687ac1-7bf8-4ada-be2b-979f8921e1a0', '49d45a6c-1073-4c6e-805f-8f4105641bf1'],
  }, // Dim, Radius
};

export const testPartEditor = {
  tolerances: {
    linear: {
      type: 'PrecisionTolerance',
      rangeTols: [
        { rangeHigh: '1', rangeLow: '9', plus: '1', minus: '-1' },
        { rangeHigh: '0.1', rangeLow: '0.9', plus: '0.1', minus: '-0.1' },
        { rangeHigh: '0.01', rangeLow: '0.09', plus: '0.01', minus: '-0.01' },
        { rangeHigh: '0.001', rangeLow: '0.009', plus: '0.001', minus: '-0.001' },
        { rangeHigh: '0.0001', rangeLow: '0.0009', plus: '0.0001', minus: '-0.0001' },
      ],
      precisionTols: [
        { level: 'X', plus: '1', minus: '-1' },
        { level: 'X.X', plus: '0.1', minus: '-0.1' },
        { level: 'X.XX', plus: '0.01', minus: '-0.01' },
        { level: 'X.XXX', plus: '0.001', minus: '-0.001' },
        { level: 'X.XXXX', plus: '0.0001', minus: '-0.0001' },
      ],
      unit: defaultUnitIDs.inch,
    },
    angular: {
      type: 'PrecisionTolerance',
      rangeTols: [
        { rangeHigh: '1', rangeLow: '9', plus: '1', minus: '-1' },
        { rangeHigh: '0.1', rangeLow: '0.9', plus: '0.1', minus: '-0.1' },
        { rangeHigh: '0.01', rangeLow: '0.09', plus: '0.01', minus: '-0.01' },
        { rangeHigh: '0.001', rangeLow: '0.009', plus: '0.001', minus: '-0.001' },
        { rangeHigh: '0.0001', rangeLow: '0.0009', plus: '0.0001', minus: '-0.0001' },
      ],
      precisionTols: [
        { level: 'X', plus: '1', minus: '-1' },
        { level: 'X.X', plus: '0.1', minus: '-0.1' },
        { level: 'X.XX', plus: '0.01', minus: '-0.01' },
        { level: 'X.XXX', plus: '0.001', minus: '-0.001' },
        { level: 'X.XXXX', plus: '0.0001', minus: '-0.0001' },
      ],
      unit: defaultUnitIDs.degree,
    },
  },
};

export const noteClassified = {
  FullSpecification: 'L066',
  Gdt: {
    GdtPrimaryDatum: undefined,
    GdtSecondDatum: undefined,
    GdtSymbol: undefined,
    GdtTertiaryDatum: undefined,
    GdtTol: undefined,
    GdtTolZone2: undefined,
  },
  IsBasic: false,
  IsReference: false,
  LowerSpecLimit: '',
  MinusTol: '',
  Nominal: undefined,
  PlusTol: '',
  Quantity: 1,
  SubType: 'Note',
  Type: 'Note',
  UpperSpecLimit: '',
};

export const dimClassified = {
  FullSpecification: '3.39/3.36',
  Gdt: {
    GdtPrimaryDatum: undefined,
    GdtSecondDatum: undefined,
    GdtSymbol: undefined,
    GdtTertiaryDatum: undefined,
    GdtTol: undefined,
    GdtTolZone2: undefined,
  },
  IsBasic: false,
  IsReference: false,
  LowerSpecLimit: '3.36',
  MinusTol: '',
  Nominal: '3.39/3.36',
  PlusTol: '',
  Quantity: 1,
  SubType: 'length',
  Type: 'Dimension',
  UpperSpecLimit: '3.39',
};

export const gdtClassified = {
  FullSpecification: '',
  Gdt: {
    GdtPrimaryDatum: 'A',
    GdtSecondDatum: 'B',
    GdtSymbol: 'position',
    GdtTertiaryDatum: 'C',
    GdtTol: '.01Ⓢ',
    GdtTolZone2: '',
  },
  IsBasic: false,
  IsReference: false,
  LowerSpecLimit: '',
  MinusTol: '',
  Nominal: '',
  PlusTol: '',
  Quantity: 1,
  SubType: 'perpendicularity',
  Type: 'Geometric_Tolerance',
  UpperSpecLimit: '.01',
};

export const testTypes = {
  '9f78f226-11b1-4e70-9705-440c0ef290bc': {
    children: [
      {
        default: true,
        id: '4e6617aa-5f61-4257-8e18-787b80ddece1',
        index: 3,
        listType: 'Type',
        meta: { measurement: 'Length' },
        status: 'Active',
        value: 'Structure',
      },
    ],
    default: true,
    id: '9f78f226-11b1-4e70-9705-440c0ef290bc',
    index: 2,
    listType: 'Type',
    meta: '',
    status: 'Active',
    value: 'Surface Finish',
  },
  'db687ac1-7bf8-4ada-be2b-979f8921e1a0': {
    children: [
      {
        default: true,
        id: 'edc8ecda-5f88-4b81-871f-6c02f1c50afd',
        index: 0,
        listType: 'Type',
        meta: { measurement: 'Length' },
        status: 'Active',
        value: 'Length',
      },
      {
        default: true,
        id: 'b2e1e0aa-0da4-4844-853a-f20971b013a4',
        index: 1,
        listType: 'Type',
        meta: { measurement: 'Plane Angle' },
        status: 'Active',
        value: 'Angle',
      },
      {
        default: true,
        id: '49d45a6c-1073-4c6e-805f-8f4105641bf1',
        index: 2,
        listType: 'Type',
        meta: { measurement: 'Length' },
        status: 'Active',
        value: 'Radius',
      },
      {
        default: true,
        id: '4396f3c2-a915-4458-8cff-488b6d301ecd',
        index: 3,
        listType: 'Type',
        meta: { measurement: 'Length' },
        status: 'Active',
        value: 'Diameter',
      },
    ],
    default: true,
    id: 'db687ac1-7bf8-4ada-be2b-979f8921e1a0',
    index: 0,
    listType: 'Type',
    meta: '',
    status: 'Active',
    value: 'Dimension',
  },
  'f3c4d8e0-fb79-4c72-a3f8-3f584ea075a7': {
    children: [
      {
        default: true,
        id: '19a7a9a1-76c5-45ef-8cf9-611ebe7cf207',
        index: 1,
        listType: 'Type',
        meta: { measurement: 'Length' },
        status: 'Active',
        value: 'Position',
      },
    ],
    default: true,
    id: 'f3c4d8e0-fb79-4c72-a3f8-3f584ea075a7',
    index: 1,
    listType: 'Type',
    meta: '',
    status: 'Active',
    value: 'Geometric Tolerance',
  },
  'fa3284ed-ad3e-43c9-b051-87bd2e95e927': {
    children: [
      {
        default: true,
        id: '805fd539-8063-4158-8361-509e10a5d371',
        index: 0,
        listType: 'Type',
        meta: '',
        status: 'Active',
        value: 'Note',
      },
      {
        default: true,
        id: 'd3e276d7-18fa-47b7-8fc8-0656017dc5cc',
        index: 1,
        listType: 'Type',
        meta: '',
        status: 'Active',
        value: 'Flag Note',
      },
    ],
    default: true,
    id: 'fa3284ed-ad3e-43c9-b051-87bd2e95e927',
    index: 4,
    listType: 'Type',
    meta: '',
    status: 'Active',
    value: 'Note',
  },
};

export const PDFTronInstanceMock = {
  Core: {
    documentViewer: {
      getDocument: () => {
        return {
          getPDFDoc: () => {
            return new Promise((resolve, reject) => {
              resolve({
                getPageIterator: () => {
                  return new Promise((resolve, reject) => {
                    resolve();
                  });
                },
              });
            });
          },
        };
      },
      getCompleteRotation: () => {},
    },
    PDFNet: {
      PDFDoc: {
        createFromURL: () => {
          return new Promise((resolve, reject) => {
            resolve({
              getPage: () => {
                return new Promise((resolve, reject) => {
                  resolve({
                    setRotation: () => {
                      return new Promise((resolve, reject) => {
                        resolve();
                      });
                    },
                  });
                });
              },
              getPageIterator: () => {
                return new Promise((resolve, reject) => {
                  resolve({
                    current: () => {
                      return new Promise((resolve, reject) => {
                        resolve({
                          getRotation: () => {},
                          getPageWidth: () => {},
                          getPageHeight: () => {},
                        });
                      });
                    },
                  });
                });
              },
              getDownloadedPDF: () => {
                return new Promise((resolve, reject) => {
                  resolve();
                });
              },
            });
          });
        },
      },
      PDFDraw: {
        create: () => {
          return new Promise((resolve, reject) => {
            resolve({
              setAntiAliasing: () => {},
              setImageSmoothing: () => {},
              setClipRect: () => {},
              getBitmap: () => {},
              exportStream: () => {},
            });
          });
        },
      },
    },
  },
};
