import { defaultTypeIDs, defaultSubtypeIDs, defaultUnitIDs, DefaultTolerances } from 'view/global/defaults';
import { interpretNotationText } from 'utils/ParseNotation';
import {
  testPart,
  testResult,
  testNoteResult,
  testNotePartialInput,
  testPartialInput,
  testNoteClassified,
  testClassified,
  testNoteBlankResult,
  testNoteTextResult,
  testCharacteristic,
  testAssignedStyles,
  grid,
  canvas,
  noteClassified,
  gdtClassified,
  dimClassified,
  testTypes,
  PDFTronInstanceMock,
} from './extractUtilTestData';
import { toEnumFormat, getDimensionInput, getNoteInput, updateExistingItem, updateItemCoordinates, getMarkerFontSize, getMarkerLocationXY, captureImageFromCanvas } from './extractUtil';
import * as CaptureImageUtils from './captureImage';

jest.mock('./captureImage');

describe('captureImageFromCanvas function', () => {
  const fileUrlMock = 'testfile.pdf';
  const currentPageMock = 1;
  const xMock = 0;
  const yMock = 0;
  const widthMock = 100;
  const heightMock = 100;
  const rotationMock = 0;
  const jwtMock = 'mockJWT';
  const inCompareModeMock = false;

  afterEach(() => {
    jest.clearAllMocks();
  });

  it('should call captureImage and removeLines with correct parameters', async () => {
    const captureImageMock = jest.spyOn(CaptureImageUtils, 'captureImage').mockResolvedValue({ data: {}, width: 2, height: 2 });
    const removeLinesMock = jest.spyOn(CaptureImageUtils, 'removeLines').mockResolvedValue({ data: { data: {}, width: 2, height: 2 }, isGdtOrBasic: false });

    await captureImageFromCanvas(PDFTronInstanceMock, fileUrlMock, currentPageMock, xMock, yMock, widthMock, heightMock, rotationMock, jwtMock, inCompareModeMock);
    expect(captureImageMock).toHaveBeenCalledWith(
      expect.objectContaining({
        x: xMock,
        y: yMock,
        width: widthMock,
        height: heightMock,
        rotation: rotationMock,
        sheet: currentPageMock,
        instance: PDFTronInstanceMock,
        documentUrl: fileUrlMock,
        pdf: expect.anything(),
        single: false,
      }),
    );
    expect(removeLinesMock).toHaveBeenCalled();
  });

  it('should return the ImageData for the captured image', async () => {
    const mockData = new Uint8ClampedArray(new ArrayBuffer('TestImage'));
    jest.spyOn(CaptureImageUtils, 'captureImage').mockResolvedValue({ data: mockData, width: 2, height: 2 });
    jest.spyOn(CaptureImageUtils, 'removeLines').mockResolvedValue({ data: { data: mockData, width: 2, height: 2 }, isGdtOrBasic: false });

    const base64String = await captureImageFromCanvas(PDFTronInstanceMock, fileUrlMock, currentPageMock, xMock, yMock, widthMock, heightMock, rotationMock, jwtMock, inCompareModeMock);
    expect(base64String).toEqual({ data: { data: mockData, width: 2, height: 2 }, isGdtOrBasic: false });
  });

  it('should set the isGdtOrBasic if the captures feature is gdt', async () => {
    jest.spyOn(CaptureImageUtils, 'captureImage').mockResolvedValue({ data: {}, width: 2, height: 2 });
    jest.spyOn(CaptureImageUtils, 'removeLines').mockResolvedValue({ data: { data: {}, width: 2, height: 2 }, isGdtOrBasic: true });

    const base64String = await captureImageFromCanvas(PDFTronInstanceMock, fileUrlMock, currentPageMock, xMock, yMock, widthMock, heightMock, rotationMock, jwtMock, inCompareModeMock);
    expect(base64String).toEqual({ data: { data: {}, width: 2, height: 2 }, isGdtOrBasic: true });
  });

  it('should return undefined if no PDFTron instance is passed', async () => {
    const result = await captureImageFromCanvas(null, fileUrlMock, currentPageMock, xMock, yMock, widthMock, heightMock, rotationMock, jwtMock, inCompareModeMock);
    expect(result).toBeUndefined();
  });
});

describe('The Extract Utilities', () => {
  it('Returns the Enum format of a String', () => {
    const en = toEnumFormat('geometric_tolerance');
    expect(en).toEqual('Geometric_Tolerance');
  });

  it('Returns the full Characteristic Input for a Dimension', () => {
    const charInput = getDimensionInput(testPartialInput, testResult, testClassified, testPart, testPartialInput.notationType, DefaultTolerances);
    expect(charInput).toEqual({
      boxHeight: 20,
      boxLocationX: 20,
      boxLocationY: 20,
      boxRotation: 0,
      boxWidth: 40,
      captureMethod: 'Automatic',
      criticality: null,
      drawing: 'f12c5566-7612-48e9-9534-ef90b276ebaa',
      drawingRotation: 0,
      drawingScale: 1,
      drawingSheet: 'f12c5566-7612-48e9-9534-ef90b276ebaa',
      drawingSheetIndex: 1,
      fullSpecification: '1.25',
      gridCoordinates: '',
      balloonGridCoordinates: '',
      connectionPointGridCoordinates: '',
      inspectionMethod: null,
      lowerSpecLimit: '1.25',
      markerGroup: '1',
      markerGroupShared: false,
      markerIndex: 0,
      markerLabel: '1',
      markerLocationX: 20,
      markerLocationY: 20,
      markerStyle: 'f12c5566-7612-48e9-9534-ef90b276ebaa',
      markerSubIndex: -1,
      markerFontSize: 18,
      markerSize: 36,
      minusTol: '',
      nominal: '1.25',
      notationClass: 'Tolerance',
      notationSubtype: defaultSubtypeIDs.Length, // Length
      notationType: defaultTypeIDs.Dimension, // Dimension
      notes: '',
      part: 'f12c5566-7612-48e9-9534-ef90b276ebaa',
      plusTol: '',
      quantity: 1,
      status: 'Active',
      toleranceSource: 'Document_Defined',
      unit: defaultUnitIDs.inch,
      upperSpecLimit: '1.25',
      verified: false,
    });
  });

  it('Returns the full input for a Note', () => {
    const noteInput = getDimensionInput(testNotePartialInput, testNoteResult, testNoteClassified, testPart, defaultTypeIDs.Note, DefaultTolerances);
    expect(noteInput).toEqual({
      drawingSheetIndex: 1,
      status: 'Active',
      notationType: defaultTypeIDs.Note, // Note
      notationSubtype: defaultSubtypeIDs.Note, // Note
      notationClass: 'Tolerance',
      drawingRotation: 0,
      drawingScale: 1,
      boxLocationY: 20,
      boxLocationX: 20,
      boxWidth: 40,
      boxHeight: 20,
      boxRotation: 0,
      markerGroup: '5',
      markerGroupShared: false,
      markerIndex: 9,
      markerSubIndex: 4,
      markerLabel: '5.5',
      markerStyle: 'f12c5566-7612-48e9-9534-ef90b276ebaa',
      markerLocationX: 20,
      markerLocationY: 20,
      markerFontSize: 18,
      markerSize: 36,
      gridCoordinates: '',
      balloonGridCoordinates: '',
      connectionPointGridCoordinates: '',
      part: 'f12c5566-7612-48e9-9534-ef90b276ebaa',
      drawing: 'f12c5566-7612-48e9-9534-ef90b276ebaa',
      captureMethod: 'Automatic',
      drawingSheet: 'f12c5566-7612-48e9-9534-ef90b276ebaa',
      verified: false,
      toleranceSource: 'Document_Defined',
      fullSpecification: 'This is a Note',
      quantity: 1,
      nominal: 'This is a Note',
      upperSpecLimit: '',
      lowerSpecLimit: '',
      plusTol: '',
      minusTol: '',
      unit: defaultUnitIDs.inch,
      criticality: null,
      inspectionMethod: null,
      notes: '',
    });
  });

  it('Returns a note for blank value', () => {
    const noteInput = getNoteInput(testNotePartialInput, testNoteBlankResult, testNoteClassified);
    expect(noteInput).toEqual({
      drawingSheetIndex: 1,
      status: 'Active',
      notationType: defaultTypeIDs.Note, // Note
      notationSubtype: defaultSubtypeIDs.Note, // Note
      notationClass: 'Tolerance',
      drawingRotation: 0,
      drawingScale: 1,
      boxLocationY: 20,
      boxLocationX: 20,
      boxWidth: 40,
      boxHeight: 20,
      boxRotation: 0,
      markerGroup: '5',
      markerGroupShared: false,
      markerIndex: 9,
      markerSubIndex: 4,
      markerLabel: '5.5',
      markerStyle: 'f12c5566-7612-48e9-9534-ef90b276ebaa',
      markerLocationX: 20,
      markerLocationY: 20,
      markerFontSize: 18,
      markerSize: 36,
      gridCoordinates: '',
      balloonGridCoordinates: '',
      connectionPointGridCoordinates: '',
      part: 'f12c5566-7612-48e9-9534-ef90b276ebaa',
      drawing: 'f12c5566-7612-48e9-9534-ef90b276ebaa',
      captureMethod: 'Automatic',
      drawingSheet: 'f12c5566-7612-48e9-9534-ef90b276ebaa',
      verified: false,
      toleranceSource: 'Document_Defined',
      fullSpecification: '',
      quantity: 1,
      nominal: '',
      upperSpecLimit: '',
      lowerSpecLimit: '',
      plusTol: '',
      minusTol: '',
      unit: '',
      criticality: null,
      inspectionMethod: null,
      notes: '',
    });
  });

  it('Returns a note for a text value', () => {
    const noteInput = getNoteInput(testNotePartialInput, testNoteTextResult, testNoteClassified);
    expect(noteInput).toEqual({
      drawingSheetIndex: 1,
      status: 'Active',
      notationType: defaultTypeIDs.Note, // Note
      notationSubtype: defaultSubtypeIDs.Note, // Note
      notationClass: 'Tolerance',
      drawingRotation: 0,
      drawingScale: 1,
      boxLocationY: 20,
      boxLocationX: 20,
      boxWidth: 40,
      boxHeight: 20,
      boxRotation: 0,
      markerGroup: '5',
      markerGroupShared: false,
      markerIndex: 9,
      markerSubIndex: 4,
      markerLabel: '5.5',
      markerStyle: 'f12c5566-7612-48e9-9534-ef90b276ebaa',
      markerLocationX: 20,
      markerLocationY: 20,
      markerFontSize: 18,
      markerSize: 36,
      gridCoordinates: '',
      balloonGridCoordinates: '',
      connectionPointGridCoordinates: '',
      part: 'f12c5566-7612-48e9-9534-ef90b276ebaa',
      drawing: 'f12c5566-7612-48e9-9534-ef90b276ebaa',
      captureMethod: 'Automatic',
      drawingSheet: 'f12c5566-7612-48e9-9534-ef90b276ebaa',
      verified: false,
      toleranceSource: 'Document_Defined',
      fullSpecification: 'This is a Note',
      quantity: 1,
      nominal: 'This is a Note',
      upperSpecLimit: '',
      lowerSpecLimit: '',
      plusTol: '',
      minusTol: '',
      unit: '',
      criticality: null,
      inspectionMethod: null,
      notes: '',
    });
  });

  describe('Handles a Failed Call to the Parse', () => {
    it('Received Empty/Undefined Text', async () => {
      // const ret = await interpretNotationText(undefined);
      // expect(ret.error).toBeDefined();
      // expect(ret.error.name).toEqual('InvalidParameter');
      // expect(ret.error.message).toEqual('Text is required but was undefined');
      await expect(interpretNotationText(undefined)).rejects.toThrow('Notation Interpreter requires text as input');
    });
  });

  it('Updates an existing characteristic to split limits', () => {
    const result = {
      parsed: {
        notation: {
          type: 'splitLimits',
          upper_limit: { text: '3.39', value: '3.39', units: '' },
          lower_limit: { text: '3.36', value: '3.36', units: '' },
          modifier: '',
          text: '3.39/3.36',
        },
        quantity: { value: '1', units: '', text: '' },
        words: '',
        text: '3.39/3.36',
        input: '3.39\n3.36',
      },
      ocrRes: '3.39\n3.36',
    };

    const resultItem = updateExistingItem(result, testPart, testAssignedStyles, testCharacteristic, defaultTypeIDs.Dimension, defaultSubtypeIDs.Length, 'Tolerance', dimClassified, testTypes, DefaultTolerances);

    expect(resultItem.nominal).toBe(result.parsed.text);
    expect(resultItem.upperSpecLimit).toBe(result.parsed.notation.upper_limit.text);
    expect(resultItem.lowerSpecLimit).toBe(result.parsed.notation.lower_limit.text);
    expect(resultItem.notationType).toBe(defaultTypeIDs.Dimension);
    expect(resultItem.notationSubtype).toBe(defaultSubtypeIDs.Length);
  });

  it('Updates an existing characteristic to GD&T', () => {
    const result = {
      parsed: {
        notation: {
          type: 'gdtControlFrame',
          subtype: 'Position',
          zones: {
            primary: {
              text: '.01Ⓢ', //
              prefix: '',
              value: '.01',
              modifier: 'Ⓢ',
              postModifierValue: '',
            },
          },
          datums: [
            { id: 'A' }, //
            { id: 'B' },
            { id: 'C' },
          ],
        },
        quantity: { value: '1', units: '', text: '' },
        words: '',
        input: '|⌖|.01Ⓢ|A|B|C|',
      },
      ocrRes: '|⌖|.01Ⓢ|A|B|C|',
    };

    const resultItem = updateExistingItem(result, testPart, testAssignedStyles, testCharacteristic, defaultTypeIDs['Geometric Tolerance'], defaultSubtypeIDs.Position, 'Tolerance', gdtClassified, testTypes, DefaultTolerances);

    expect(resultItem.nominal).toBe(''); // Framed version of |⌖|.01Ⓢ|A|B|C|, view with IX GDT font
    expect(resultItem.upperSpecLimit).toBe('.01');
    expect(resultItem.lowerSpecLimit).toBe('');
    expect(resultItem.gdtPrimaryToleranceZone).toBe('.01Ⓢ');
    expect(resultItem.gdtSecondaryToleranceZone).toBe('');
    expect(resultItem.gdtPrimaryDatum).toBe('A');
    expect(resultItem.gdtSecondaryDatum).toBe('B');
    expect(resultItem.gdtTertiaryDatum).toBe('C');
    expect(resultItem.notationType).toBe(defaultTypeIDs['Geometric Tolerance']);
    expect(resultItem.notationSubtype).toBe(defaultSubtypeIDs.Position);
  });

  it('Updates an existing characteristic to note', () => {
    const result = {
      parsed: {
        type: 'note',
        input: 'BREAK ALL SHARP EDGES',
      },
      ocrRes: 'BREAK ALL SHARP EDGES',
    };

    const resultItem = updateExistingItem(result, testPart, testAssignedStyles, testCharacteristic, defaultTypeIDs.Note, defaultSubtypeIDs.Note, 'Tolerance', noteClassified, testTypes, DefaultTolerances);

    expect(resultItem.nominal).toBe(result.parsed.input);
    expect(resultItem.upperSpecLimit).toBe('');
    expect(resultItem.lowerSpecLimit).toBe('');
    expect(resultItem.notationType).toBe(defaultTypeIDs.Note);
    expect(resultItem.notationSubtype).toBe(defaultSubtypeIDs.Note);
  });

  it('Updates from document defined to default tolerance', () => {
    const result = {
      parsed: {
        notation: {
          nominal: { text: '.020', value: '.020', units: '' },
          type: 'length',
          text: '.020',
        },
        quantity: { value: '1', units: '', text: '' },
        words: '',
        text: '.020',
        input: '.020',
      },
      ocrRes: '.020',
    };

    const newClassified = {
      FullSpecification: '.020',
      Gdt: {
        GdtPrimaryDatum: undefined,
        GdtSecondDatum: undefined,
        GdtSymbol: undefined,
        GdtTertiaryDatum: undefined,
        GdtTol: undefined,
        GdtTolZone2: undefined,
      },
      IsBasic: false,
      IsReference: false,
      LowerSpecLimit: undefined,
      MinusTol: undefined,
      Nominal: '.020',
      PlusTol: undefined,
      Quantity: 1,
      SubType: 'length',
      Type: 'Dimension',
      UpperSpecLimit: undefined,
    };

    const oldFeature = { ...testCharacteristic, toleranceSource: 'Document_Defined' };

    const resultItem = updateExistingItem(result, testPart, testAssignedStyles, oldFeature, defaultTypeIDs.Dimension, defaultSubtypeIDs.Length, 'Tolerance', newClassified, testTypes, DefaultTolerances);

    expect(resultItem.nominal).toBe(result.parsed.notation.nominal.text);
    expect(resultItem.upperSpecLimit).toBe('0.021');
    expect(resultItem.lowerSpecLimit).toBe('0.019');
    expect(resultItem.notationType).toBe(defaultTypeIDs.Dimension);
    expect(resultItem.notationSubtype).toBe(defaultSubtypeIDs.Length);
    expect(resultItem.toleranceSource).toBe('Default_Tolerance');
  });

  it('Updates from default tolerance to document defined', () => {
    const result = {
      parsed: {
        notation: {
          nominal: { text: '.250', value: '.250', units: '' },
          minus: { text: '.020', value: '.020', units: '' },
          plus: { text: '.020', value: '.020', units: '' },
          type: 'nominalWithTolerances',
          text: '.250±.020',
        },
        quantity: { value: '1', units: '', text: '' },
        words: '',
        text: '.250±.020',
        input: '.250±.020',
      },
      ocrRes: '.250±.020',
    };

    const newClassified = {
      FullSpecification: '.250±.020',
      Gdt: {
        GdtPrimaryDatum: undefined,
        GdtSecondDatum: undefined,
        GdtSymbol: undefined,
        GdtTertiaryDatum: undefined,
        GdtTol: undefined,
        GdtTolZone2: undefined,
      },
      IsBasic: false,
      IsReference: false,
      LowerSpecLimit: '0.230',
      MinusTol: '.020',
      Nominal: '.250',
      PlusTol: '.020',
      Quantity: 1,
      SubType: 'length',
      Type: 'Dimension',
      UpperSpecLimit: '0.270',
    };

    const oldFeature = { ...testCharacteristic, toleranceSource: 'Default_Tolerance' };

    const resultItem = updateExistingItem(result, testPart, testAssignedStyles, oldFeature, defaultTypeIDs.Dimension, defaultSubtypeIDs.Length, 'Tolerance', newClassified, testTypes, DefaultTolerances);

    expect(resultItem.nominal).toBe(result.parsed.notation.nominal.text);
    expect(resultItem.upperSpecLimit).toBe('0.270');
    expect(resultItem.lowerSpecLimit).toBe('0.230');
    expect(resultItem.notationType).toBe(defaultTypeIDs.Dimension);
    expect(resultItem.notationSubtype).toBe(defaultSubtypeIDs.Length);
    expect(resultItem.toleranceSource).toBe('Document_Defined');
  });

  it('Updates Item Locations', () => {
    const itemOne = { ...testCharacteristic };
    const itemTwo = { ...testCharacteristic };
    const itemThree = { ...testCharacteristic };
    itemOne.boxLocationX = 200;
    itemOne.boxLocationY = 200;
    itemOne.boxWidth = 20;
    itemOne.boxHeight = 20;
    itemOne.gridCoordinates = '';
    itemOne.balloonGridCoordinates = '';
    itemOne.connectionPointGridCoordinates = '';

    itemTwo.boxLocationX = 1040;
    itemTwo.boxLocationY = 480;
    itemTwo.boxWidth = 20;
    itemTwo.boxHeight = 20;
    itemTwo.gridCoordinates = '';
    itemTwo.balloonGridCoordinates = '';
    itemTwo.connectionPointGridCoordinates = '';

    itemThree.boxLocationX = 0;
    itemThree.boxLocationY = 0;
    itemThree.boxWidth = 20;
    itemThree.boxHeight = 20;
    itemThree.gridCoordinates = '';
    itemThree.balloonGridCoordinates = '';
    itemThree.connectionPointGridCoordinates = '';

    const [updatedOne, updatedTwo, updatedThree] = updateItemCoordinates([itemOne, itemTwo, itemThree], grid, canvas, 1, 0, true);
    expect(updatedOne.gridCoordinates).toBe('4B');
    expect(updatedTwo.gridCoordinates).toBe('1A');
    expect(updatedThree.gridCoordinates).toBe('External');
  });

  describe('getMarkerFontSize', () => {
    it('Uses the font size of the lastItem if it exists', () => {
      const lastItem = {
        markerFontSize: 6,
      };
      const height = 30;
      const width = 200;
      const rotation = 0;
      const result = getMarkerFontSize(lastItem, height, width, rotation);
      expect(result).toBe(6);
    });
    it('Uses the default font size of 18 if the lastItem exists but does not have a font size', () => {
      const lastItem = {};
      const height = 30;
      const width = 200;
      const rotation = 0;
      const result = getMarkerFontSize(lastItem, height, width, rotation);
      expect(result).toBe(18);
    });
    it('Max font size is 40', () => {
      const lastItem = null;
      const height = 100000;
      const width = 2000000;
      const rotation = 0;
      const result = getMarkerFontSize(lastItem, height, width, rotation);
      expect(result).toBe(40);
    });
    it('Min font size is 8', () => {
      const lastItem = null;
      const height = 0;
      const width = 200;
      const rotation = 0;
      const result = getMarkerFontSize(lastItem, height, width, rotation);
      expect(result).toBe(8);
    });
    it('Height of 12 returns a font size of 8', () => {
      const lastItem = null;
      const height = 12;
      const width = 200;
      const rotation = 0;
      const result = getMarkerFontSize(lastItem, height, width, rotation);
      expect(result).toBe(8);
    });
    it('Height of 31 returns a font size of 20', () => {
      const lastItem = null;
      const height = 31;
      const width = 200;
      const rotation = 0;
      const result = getMarkerFontSize(lastItem, height, width, rotation);
      expect(result).toBe(20);
    });
    it('Height of 72 returns a font size of 40', () => {
      const lastItem = null;
      const height = 72;
      const width = 200;
      const rotation = 0;
      const result = getMarkerFontSize(lastItem, height, width, rotation);
      expect(result).toBe(40);
    });

    it('Uses the width to determine if it is less than 1.2x of the height', () => {
      const lastItem = null;
      const height = 200;
      const width = 72;
      const rotation = 0;
      const result = getMarkerFontSize(lastItem, height, width, rotation);
      expect(result).toBe(40);

      const lastItem2 = null;
      const height2 = 200;
      const width2 = 31;
      const rotation2 = 90;
      const result2 = getMarkerFontSize(lastItem2, height2, width2, rotation2);
      expect(result2).toBe(20);
    });
  });

  describe('getMarkerLocationXY', () => {
    it('Rotation of 90 returns 0 deg', () => {
      // boxLocationY + boxHeight + 30 > pageHeight
      const rotation = 90;
      const markerFontSize = 8;
      const boxWidth = 40;
      const boxHeight = 50;
      const boxLocationX = 8;
      const boxLocationY = 650;
      const normalizedPageHeight = 700;
      const normalizedPageWidth = 1100;
      const markerLocation = getMarkerLocationXY(rotation, markerFontSize, boxWidth, boxHeight, boxLocationX, boxLocationY, normalizedPageWidth, normalizedPageHeight);

      expect(markerLocation.y).toBe(0);
    });
    it('Rotation of 90 returns 180 deg', () => {
      // anything else
      const rotation = 90;
      const markerFontSize = 8;
      const boxWidth = 40;
      const boxHeight = 50;
      const boxLocationX = 8;
      const boxLocationY = 30;
      const normalizedPageHeight = 700;
      const normalizedPageWidth = 1100;
      const markerLocation = getMarkerLocationXY(rotation, markerFontSize, boxWidth, boxHeight, boxLocationX, boxLocationY, normalizedPageWidth, normalizedPageHeight);

      expect(markerLocation.y).toBe(180);
    });

    it('Rotation of 270 returns 0 deg', () => {
      // boxLocationY - 30 < 0
      const rotation = 270;
      const markerFontSize = 8;
      const boxWidth = 40;
      const boxHeight = 50;
      const boxLocationX = 90;
      const boxLocationY = 8;
      const normalizedPageHeight = 700;
      const normalizedPageWidth = 1100;
      const markerLocation = getMarkerLocationXY(rotation, markerFontSize, boxWidth, boxHeight, boxLocationX, boxLocationY, normalizedPageWidth, normalizedPageHeight);

      expect(markerLocation.y).toBe(0);
    });
    it('Rotation of 270 returns 180 deg', () => {
      // anything else
      const rotation = 270;
      const markerFontSize = 8;
      const boxWidth = 40;
      const boxHeight = 10;
      const boxLocationX = 8;
      const boxLocationY = 650;
      const normalizedPageHeight = 700;
      const normalizedPageWidth = 1100;
      const markerLocation = getMarkerLocationXY(rotation, markerFontSize, boxWidth, boxHeight, boxLocationX, boxLocationY, normalizedPageWidth, normalizedPageHeight);

      expect(markerLocation.y).toBe(180);
    });

    it('Rotation of 0 returns 0 deg', () => {
      // boxLocationX - 30 < 0
      const rotation = 0;
      const markerFontSize = 8;
      const boxWidth = 40;
      const boxHeight = 50;
      const boxLocationX = 8;
      const boxLocationY = 650;
      const normalizedPageHeight = 700;
      const normalizedPageWidth = 1100;
      const markerLocation = getMarkerLocationXY(rotation, markerFontSize, boxWidth, boxHeight, boxLocationX, boxLocationY, normalizedPageWidth, normalizedPageHeight);

      expect(markerLocation.y).toBe(0);
    });
    it('Rotation of 0 returns 180 deg', () => {
      // anything else
      const rotation = 0;
      const markerFontSize = 8;
      const boxWidth = 40;
      const boxHeight = 50;
      const boxLocationX = 80;
      const boxLocationY = 30;
      const normalizedPageHeight = 700;
      const normalizedPageWidth = 1100;
      const markerLocation = getMarkerLocationXY(rotation, markerFontSize, boxWidth, boxHeight, boxLocationX, boxLocationY, normalizedPageWidth, normalizedPageHeight);

      expect(markerLocation.y).toBe(180);
    });

    it('Rotation of 180 returns 0 deg', () => {
      // boxLocationX + boxWidth + 30 > pageWidth
      const rotation = 180;
      const markerFontSize = 8;
      const boxWidth = 50;
      const boxHeight = 50;
      const boxLocationX = 1050;
      const boxLocationY = 650;
      const normalizedPageHeight = 700;
      const normalizedPageWidth = 1100;
      const markerLocation = getMarkerLocationXY(rotation, markerFontSize, boxWidth, boxHeight, boxLocationX, boxLocationY, normalizedPageWidth, normalizedPageHeight);

      expect(markerLocation.y).toBe(0);
    });
    it('Rotation of 180 returns 180 deg', () => {
      // anything else
      const rotation = 180;
      const markerFontSize = 8;
      const boxWidth = 40;
      const boxHeight = 50;
      const boxLocationX = 50;
      const boxLocationY = 30;
      const normalizedPageHeight = 700;
      const normalizedPageWidth = 1100;
      const markerLocation = getMarkerLocationXY(rotation, markerFontSize, boxWidth, boxHeight, boxLocationX, boxLocationY, normalizedPageWidth, normalizedPageHeight);

      expect(markerLocation.y).toBe(180);
    });
  });
});
