import { deleteTipTitle } from './uiUtil';

describe('UI Util Functions', () => {
  it('Returns the correct title when features are selected', () => {
    const title = deleteTipTitle([{}], []);
    expect(title).toBe('Delete 1 features');
  });

  it('Returns the correct title when multiple features are selected', () => {
    const title = deleteTipTitle([{}, {}, {}], []);
    expect(title).toBe('Delete 3 features');
  });

  it('Returns the correct title when no features are selected', () => {
    const title = deleteTipTitle([], []);
    expect(title).toBe('No features selected');
  });

  it('Returns the correct title when an annotation is selected', () => {
    const title = deleteTipTitle([], [{}]);
    expect(title).toBe('Delete 1 annotations');
  });

  it('Returns the correct title when multiple annotations are selected', () => {
    const title = deleteTipTitle([], [{}, {}, {}]);
    expect(title).toBe('Delete 3 annotations');
  });

  it('Returns the correct title when multiple features and annotations are selected', () => {
    const title = deleteTipTitle([{}, {}, {}], [{}, {}, {}]);
    expect(title).toBe('Delete 3 features and 3 annotations');
  });
});
