import React, { useEffect, useReducer, useRef, useState } from 'react';
import { fabric } from 'fabric';
import { useDispatch, useSelector } from 'react-redux';
import { isEqual, debounce, orderBy } from 'lodash';
import { CapturedItem, Characteristic, CharacteristicInput, CharacteristicStatus } from 'types/characteristics';
import { CharacteristicActions, getCapturedItems } from 'modules/characteristic/characteristicActions';
import log from 'modules/shared/logger';
import { Marker, MarkerStyle } from 'types/marker';
import Analytics from 'modules/shared/analytics/analytics';
import { AppState } from 'modules/state';
import { notification } from 'antd';
import { i18n } from 'i18n';
import { PartStepEnum } from 'modules/navigation/navigationActions';
import { FeatureUI } from 'utils/ParseTextController/UI';
import { ParseTextController } from 'utils/ParseTextController/ParseTextController';
import { getAutoBalloonResults, isAutoballooningDone } from 'utils/Autoballoon/Autoballoon';
import { CaptureCoordinates } from 'utils/ParseTextController/types';
import { WebViewerInstance } from '@pdftron/webviewer';
import { defaultFeature } from 'view/global/defaults';
import { Characteristics } from 'graphql/characteristics';
import { Annotation } from 'types/annotation';
import { Annotations } from 'graphql/annotation';
import { editAnnotationThunk } from 'modules/annotation/annotationActions';
import { getSanitizedDimensions, getTransformedDimensions, coordRotation, getCartesianFromPolar, gridPosition } from './utils/coordTransforms';
import { parseDimension, revealCharacteristic, getSelectionType, captureImageFromCanvas } from './utils/extractUtil';
import { getCapturedItem, getNewRect, modifyObject } from './characteristicsRenderer';
import './characteristicsCanvasStyles.less';
import { updatePartThunk } from 'modules/part/partActions';
import { io, Socket } from 'socket.io-client';
import config from 'config';
import { createCanvas } from 'canvas';

const { EXTRACT, GRID, STYLES } = PartStepEnum;

// This comment is necessary to make bitbucket stop being a trashfire.
/*
Glossary of Terms

capturedItems[sheetNumber][fabricObject]
   A Two dimensional array that holds references to the actual fabric objects (rectangle and balloon) 

characteristics
   DB entries returned from graphql
   NOTE: Characteristics are sometimes referred to as Features and/or Items

markerStyle
  style: The balloon fill color, stroke color, shape, name, and leader line settings
  assign: the casscader array for the feature assignment, e.g. ["Type","db687ac1-7bf8-4ada-be2b-979f8921e1a0","edc8ecda-5f88-4b81-871f-6c02f1c50afd"] ([Type, Dimension, Length])

*/

export interface CharacteristicsCanvasProps {
  hideFeatures: Boolean;
}

/* ************* *
* Debugger Block *
* ************** *
const usePrevious = (value: any, initialValue: any) => {
  const ref = useRef(initialValue);
  useEffect(() => {
    ref.current = value;
  });
  return ref.current;
};

const useEffectDebugger = (effectHook: any, dependencies: any, dependencyNames: string[] = []) => {
  const previousDeps = usePrevious(dependencies, []);

  const changedDeps = dependencies.reduce((accum: any, dependency: any, index: number) => {
    if (dependency !== previousDeps[index]) {
      const keyName = dependencyNames[index] || index;
      return {
        ...accum,
        [keyName]: {
          before: previousDeps[index],
          after: dependency,
        },
      };
    }

    return accum;
  }, {});

  if (Object.keys(changedDeps).length) {
    console.log('[use-effect-debugger] ', changedDeps);
  }

  useEffect(effectHook, dependencies);
};
*/

// React component that manages the Fabric canvas for characteristics

export const CharacteristicsCanvas: React.FC<CharacteristicsCanvasProps> = ({ hideFeatures }) => {
  // Load the relevant Context objects
  const navigation = useSelector((state: AppState) => state.navigation);
  const { partDrawing, partDrawingSheet, gridRenderer, tolerances, assignedStyles, compareMode } = useSelector((state: AppState) => state.partEditor);
  const { jwt } = useSelector((state: AppState) => state.auth);
  const { part } = useSelector((state: AppState) => state.part);
  const { characteristics, annotations, selected, capturedItems, mode, inactiveCharacteristics, markers, defaultMarker } = useSelector((state: AppState) => state.characteristics, isEqual);
  const { settings, types } = useSelector((state: AppState) => state.session);
  const popoverRef = useRef<HTMLDivElement>(null);
  const characteristicsRef = useRef<Characteristic[]>(characteristics);
  const characteristicsQueueRef = useRef<Characteristic[]>([]);
  const processingCharacteristicsQueueRef = useRef<boolean>(false);
  const capturedItemsRef = useRef<CapturedItem[][]>(capturedItems);
  const defaultMarkerRef = useRef<Marker | null>(defaultMarker);
  const [socket, setSocket] = useState<Socket | null>(null);

  const { layerManager, tronInstance, fileUrl, index } = useSelector((state: AppState) => state.pdfView);
  const reduxDispatch = useDispatch();

  const generateCaptureId = (rect: any) => `${rect.top},${rect.left},${rect.width}`;

  // Local reducer for managing canvas state
  const reducer = (state: any, action: any) => {
    if (action.type === 'movingLock') {
      return {
        ...state,
        movingLock: action.payload,
      };
    }
    if (action.type === 'addRectNewCapture') {
      // adding a new rectangle to the list coming from a new capture
      // this needs to go through OCR so addItem is set to true
      // requires a newRect to render and params for the addItem() call
      if (action?.payload?.newRect && action?.payload?.params) {
        const capture = {
          id: action.payload.params?.id ? action.payload.params.id : generateCaptureId(action.payload.newRect),
          rect: action.payload.newRect,
          params: action.payload.params,
          addItem: action.payload.params.addItem,
        };
        const payload = {
          ...state,
          captureTally: state.captureTally + 1,
          captureArray: [...state.captureArray, capture],
        };
        return payload;
      }
    } else if (action.type === 'addRectExistingCapture') {
      // adding a new rectangle to the list coming from an existing capture
      // this does NOT need to go through OCR so addItem is set to false
      // params doesn't need to exist as well
      if (action?.payload.newRect) {
        const payload = {
          ...state,
          captureTally: state.captureTally + 1,
          captureArray: [
            ...state.captureArray,
            {
              id: generateCaptureId(action.payload.newrect),
              rect: action.payload.newRect,
              addItem: false,
            },
          ],
        };

        return payload;
      }
    } else if (action.type === 'updateRect') {
      // requires an id to be passed in
      // updates the color of the rectangle specified if given a color
      // sets addItem to false so it doesn't go through OCR more than once
      if (action?.payload?.id) {
        const captureArray = [
          ...state.captureArray.map((capture: { id: string; rect: fabric.Rect; params: object; addItem: boolean }) => {
            const newCapture = { ...capture };
            if (newCapture.id === action.payload.id) {
              if (action.payload.color) {
                newCapture.rect = newCapture.rect.set({
                  fill: action.payload.color,
                  stroke: action.payload.color,
                });
              }
              newCapture.addItem = false;
            }
            return newCapture;
          }),
        ];

        return {
          ...state,
          captureLock: action.payload.captureLock ? action.payload.captureLock : state.captureLock,
          captureArray,
        };
      }
    } else if (action.type === 'removeRect') {
      // removes the rectangle that has the id passed in.
      if (action?.payload?.id) {
        const captureArray = [...state.captureArray.filter((capture: { id: string; rect: fabric.Rect }) => capture.id !== action.payload.id)];

        return {
          ...state,
          captureLock: false,
          captureArray,
        };
      }
    } else if (action.type === 'showCaptureErrorModal') {
      return {
        ...state,
        captureErrorModelOpen: true,
        captureErrorInfo: action.payload.captureErrorInfo,
      };
    } else if (action.type === 'hideCaptureErrorModal') {
      return {
        ...state,
        captureErrorModelOpen: false,
        captureErrorInfo: {},
      };
    } else if (action.type === 'updateMouseDown') {
      if (action?.payload) {
        return {
          ...state,
          mouseDown: action.payload.mouseDown,
        };
      }
    }
    return { ...state };
  };
  const [state, dispatch] = useReducer(reducer, {
    captureErrorModelOpen: false,
    captureErrorInfo: {},
    captureArray: [],
    captureLock: false,
    movingLock: false,
    extracting: false,
    captureTally: 1, // 0 is falsy so we start at 1. The number doesn't really matter as it is just to keep track of the most recent id
    mouseDown: false,
  });
  // Get reference to layer manager, so that the correct canvas can be moved to the top

  // Variable to track coordinates of first click when drawing box
  const selectStart = useRef<CaptureCoordinates>();

  // variable to track if the item is being dragged
  const dragStart = useRef<CaptureCoordinates>();

  const autoballooningDone = useRef<boolean>(false);

  // Boolean flag to indicate whether canvas is at ballooning stage and in manual ballooning
  useEffect(() => {
    if (layerManager) {
      const canvas: fabric.Canvas | null = layerManager.getCurrentCanvasFor('markers');
      state.extracting = (mode === 'Manual' || mode === 'Custom') && navigation.currentPartStep >= EXTRACT;
      if (canvas) canvas.renderAll();
    }
  }, [mode, navigation.currentPartStep]);

  // Boolean flag to indicate whether multiple items are being selected
  const multi = useRef(false);

  // Delay to rectangle updates to wait for indexing to finish (?I think?)
  const debounceUpdateRectangle = debounce(dispatch, 500, { leading: true });
  const timedDispatch = (type: string, payload: object) => {
    setTimeout(() => {
      dispatch({ type, payload });
    }, 500);
  };

  // Function that applies changes when a feature is modified
  const addItem = async (canvas: fabric.Canvas, { left = 0, top = 0, width = 0, height = 0 }, rotation = 0, { sanLeft = 0, sanTop = 0, sanWidth = 0, sanHeight = 0 }, tally: number, event: any, page: number, id: string) => {
    // currentPage is for pageSheetIndex, min val of 1
    const instance = tronInstance as WebViewerInstance;
    // don't extract, either no canvas, no PDFTron, no part, or rectangle is too small
    if (!canvas || !instance || width < 5 || height < 5 || !part || !partDrawing || !fileUrl) {
      dispatch({ type: 'removeRect', payload: { id } });
      return;
    }
    // TODO: These should come from the PdfView redux
    const currentPage = instance.Core.documentViewer.getCurrentPage();
    const drawingRotation = instance ? instance.Core.documentViewer.getRotation(currentPage) * 90 : 0;
    const drawingScale = instance ? instance.Core.documentViewer.getZoomLevel() : 1;
    // get mediabox details
    const doc = instance ? instance.Core.documentViewer.getDocument() : null;
    const pdfDoc = doc ? await doc.getPDFDoc() : null;
    const pgItr = pdfDoc ? await pdfDoc.getPageIterator(currentPage) : null;
    const mediaBox = pgItr ? await (await pgItr.current()).getMediaBox() : new instance.Core.PDFNet.Rect(0, 0, 0, 0);
    const offsetLeft = left + mediaBox.x1;
    const offsetTop = top - mediaBox.y1;

    // Record start of adding items
    Analytics.track({
      event: Analytics.events.manualCaptureStarted,
      part,
      drawing: partDrawing,
      properties: {
        'view.sheet': currentPage,
        'view.rotation': drawingRotation,
        'view.scale': drawingScale,
        'capture.width': width,
        'capture.height': height,
        'capture.rotation': rotation,
        'capture.method': mode,
        multipleDocuments: part?.drawings && part.drawings.length > 0,
      },
    });

    let styles = { ...assignedStyles };

    const defStyle: MarkerStyle = {
      style: 'f12c5566-7612-48e9-9534-ef90b276ebaa',
      assign: ['Default'],
    };

    if (styles === null || !styles) {
      styles = {
        default: defStyle,
      };
    }
    if (!styles.default) {
      styles.default = defStyle;
    }

    if (mode === 'Custom' && styles) {
      // dont need to run through OCR
      handleAddingCustomFeature(canvas, drawingRotation, drawingScale, rotation, sanTop, sanLeft, sanWidth, sanHeight, offsetLeft, offsetTop, width, styles, id, currentPage);
      return;
    }

    // Run OCR on the select region
    const startTime = performance.now();

    // Cast compare mode to boolean for readability, as both string values should technically evaluate to true
    const inCompareMode = compareMode ? true : false;
    const result = await parseDimension(instance, fileUrl, currentPage, offsetLeft, offsetTop, width, height, rotation, debounceUpdateRectangle, tally, jwt || '', inCompareMode);
    const endTime = performance.now();
    const timeToParse = endTime - startTime; // in ms
    // console.log(`addItem.result (${timeToParse} ms)`, result);

    if (styles) {
      // find the gridCoordinates
      // TODO: I don't think gridRenderer is needed here. Just grid.
      // const gridLoc = gridRenderer.grid.cellLabelForPoint(left + width / 2, top + height / 2);
      let newItem: CharacteristicInput | undefined;
      try {
        if (types && result && result.unCompared && partDrawing && partDrawingSheet && part) {
          const parsed = await ParseTextController.parseFeatureFromFullSpec({
            assignedStyles: styles,
            boxRotation: rotation,
            ocrRes: result.unCompared,
            capture: {
              top: sanTop,
              left: sanLeft,
              width: sanWidth,
              height: sanHeight,
            },
            grid: gridRenderer.grid,
            tolerances,
            types,
            canvas,
            drawingRotation,
            drawingScale,
            lastItem: characteristics.length > 0 ? characteristics[characteristics.length - 1] : undefined,
            drawingSheetMarkerSize: partDrawing.sheets[index] ? partDrawing.sheets[index].markerSize : null,
            drawingSheetRotation: parseInt(partDrawing.sheets[index] ? partDrawing.sheets[index].rotation : '0', 10),
            textRes: result.textRes,
          });

          newItem = parsed?.input as CharacteristicInput;
          newItem = { ...newItem, drawing: partDrawing.id, drawingSheet: partDrawingSheet.id, drawingSheetIndex: partDrawingSheet.pageIndex + 1, part: part.id };
          delete newItem.originalCharacteristic;
          delete newItem.previousRevision;
        }
      } catch (error) {
        log.error('CharacteristicsCanvas.addItem', error);
      }
      if (!newItem) return;
      dispatch({
        type: 'updateRect',
        payload: {
          color: 'transparent',
          id: generateCaptureId({ offsetLeft, offsetTop, width }),
        },
      });
      // Record result of adding items
      Analytics.track({
        event: Analytics.events.manualCaptureReturned,
        part,
        drawing: partDrawing,
        characteristic: newItem,
        properties: {
          'view.sheet': currentPage,
          'view.rotation': newItem?.drawingRotation,
          'view.scale': newItem?.drawingScale,
          'capture.method': newItem?.captureMethod,
          'capture.timeInMs': timeToParse,
          'capture.position.y': newItem?.boxLocationY,
          'capture.position.x': newItem?.boxLocationX,
          'capture.width': newItem?.boxWidth,
          'capture.height': newItem?.boxHeight,
          'capture.rotation': newItem?.boxRotation,
          'capture.ocr.text.length': result && result.ocrRes ? result.ocrRes.length : 0,
          'marker.label': newItem?.markerLabel,
          'marker.position.y': newItem?.markerLocationY,
          'marker.position.x': newItem?.markerLocationX,
          'char.index': characteristics.length + 1,
          multipleDocuments: part?.drawings && part.drawings.length > 0,
        },
      });
      reduxDispatch({
        type: CharacteristicActions.CREATE_CHARACTERISTICS,
        payload: [newItem],
      });
      if (part) reduxDispatch(updatePartThunk({ balloonedAt: new Date() }, part.id));
      timedDispatch('removeRect', { id });
    }
  };

  // Function that applies changes when a feature is modified
  const updateItem = async (canvas: fabric.Canvas, existingItem: Characteristic) => {
    if (existingItem.verified || existingItem.captureMethod === 'Custom') {
      return;
    }

    // Don't extract, either no canvas, no PDFTron, or rectangle is too small
    if (!canvas || !tronInstance || existingItem.boxWidth < 5 || existingItem.boxHeight < 5 || !part || !partDrawing || !fileUrl) return;

    const currentPage = tronInstance.Core.documentViewer.getCurrentPage();
    const drawingRotation = tronInstance ? tronInstance.Core.documentViewer.getRotation(currentPage) * 90 : 0;
    const drawingScale = tronInstance ? tronInstance.Core.documentViewer.getZoomLevel() : 1;
    // get mediabox details
    const doc = tronInstance ? tronInstance.Core.documentViewer.getDocument() : null;
    const pdfDoc = doc ? await doc.getPDFDoc() : null;
    const pgItr = pdfDoc ? await pdfDoc.getPageIterator(currentPage) : null;
    const mediaBox = pgItr ? await (await pgItr.current()).getMediaBox() : new tronInstance.Core.PDFNet.Rect(0, 0, 0, 0);

    // Rotate the top left and bottom right corners of the rectangle, set the new top left point and dimensions
    const { newLeft, newTop, newWidth, newHeight } = getTransformedDimensions(existingItem.boxLocationX, existingItem.boxLocationY, existingItem.boxWidth, existingItem.boxHeight, canvas.getWidth(), canvas.getHeight(), drawingScale, drawingRotation);
    // Add any mediabox offsets
    const offsetLeft = newLeft + mediaBox.x1;
    const offsetTop = newTop - mediaBox.y1;
    // Create a copy of the existing item
    const tempItem = { ...existingItem, captureError: '' };
    const changeCharacteristics = characteristics.map((c) => (c.id === tempItem.id ? tempItem : c));
    // Save the revised characteristic with the resized coordinates before running the OCR so that the UI reflects the change immediately
    // maybe update this to be the dashed line??
    const newSelected = selected.map((c) => (c.id === tempItem.id ? tempItem : c));
    const newCaptures = getCapturedItems(changeCharacteristics, annotations, markers, defaultMarker || markers[0], partDrawing.id);
    reduxDispatch({ type: CharacteristicActions.UPDATE_SELECTED, payload: { characteristics: changeCharacteristics, selected: newSelected, capturedItems: newCaptures } });

    // Record start of adding items
    Analytics.track({
      event: Analytics.events.manualCaptureUpdateStarted,
      part,
      drawing: partDrawing,
      properties: {
        'view.sheet': currentPage,
        'view.rotation': drawingRotation,
        'view.scale': drawingScale,
        'capture.width.old': existingItem.boxWidth,
        'capture.width.new': newWidth,
        'capture.height.old': existingItem.boxHeight,
        'capture.height.new': newHeight,
        multipleDocuments: part?.drawings && part.drawings.length > 0,
      },
    });

    // Run OCR on the selected region of the PDF
    const startTime = performance.now();
    const inCompareMode = compareMode ? true : false;
    const result = await parseDimension(tronInstance, fileUrl, currentPage, offsetLeft, offsetTop, newWidth, newHeight, 0, debounceUpdateRectangle, state.captureTally, jwt || '', inCompareMode); // QUESTION: Does rotation need to be calculated here?
    const endTime = performance.now();
    const timeToParse = endTime - startTime; // in ms
    // log.debug(`updateItem.result (${timeToParse} ms)`, result);

    // Determine which balloon style this capture should make
    if (!assignedStyles || assignedStyles === null) {
      log.error(`assignedStyles was ${assignedStyles}. See code comment for fix instructions.`);
    } else {
      let updatedItem: Characteristic | null | undefined = null;
      if (types && result && result.unCompared) {
        const parsed = await ParseTextController.parseFeatureFromFullSpec({
          assignedStyles,
          oldFeature: existingItem,
          boxRotation: 0,
          ocrRes: result.unCompared,
          capture: {
            top: offsetLeft,
            left: offsetTop,
            width: newWidth,
            height: newHeight,
          },
          grid: gridRenderer.grid,
          tolerances,
          types,
        });
        updatedItem = parsed?.feature as Characteristic;
      }
      // Commented out to direct extract to the same parser class as review changes
      // Devin - 10/26/2021
      // const { classified, type, subType, notationClass } = classifyCharacteristic(result, tolerances);
      // const typeId = getListIdByValue({
      //   defaultValue: 'fa3284ed-ad3e-43c9-b051-87bd2e95e927',
      //   list: types,
      //   value: type,
      // });
      // const subtypeId = getListIdByValue({
      //   defaultValue: '805fd539-8063-4158-8361-509e10a5d371',
      //   list: types ? types[typeId]?.children : {},
      //   value: subType,
      // });
      // const updatedItem = updateExistingItem(result, part, assignedStyles, tempItem, typeId, subtypeId, notationClass, classified, types || {}, tolerances);
      if (!updatedItem) return;
      // Record result of adding items
      Analytics.track({
        event: Analytics.events.manualCaptureUpdateReturned,
        part,
        drawing: partDrawing,
        characteristic: updatedItem,
        properties: {
          'view.sheet': currentPage,
          'view.rotation': updatedItem.drawingRotation,
          'view.scale': updatedItem.drawingScale,
          'capture.method': updatedItem.captureMethod,
          'capture.timeInMs': timeToParse,
          'capture.position.x': updatedItem.boxLocationX,
          'capture.position.y': updatedItem.boxLocationY,
          'capture.width': updatedItem.boxWidth,
          'capture.height': updatedItem.boxHeight,
          'capture.rotation': updatedItem.boxRotation,
          'capture.ocr.text.length': result && result.ocrRes ? result.ocrRes.length : 0,
          'marker.label': updatedItem.markerLabel,
          'marker.position.y': updatedItem.markerLocationY,
          'marker.position.x': updatedItem.markerLocationX,
          'item.index': characteristics.length + 1,
          multipleDocuments: part?.drawings && part.drawings.length > 0,
        },
      });
      reduxDispatch({ type: CharacteristicActions.EDIT_CHARACTERISTICS, payload: { updates: [updatedItem], newSelected } });
      if (part) reduxDispatch(updatePartThunk({ updatedAt: new Date(), status: part.status, balloonedAt: new Date() }, part.id));
    }
  };

  /**
   * Takes an empty markup as a Characteristic from Auto-Markup and preforms OCR & Classification on it.
   * @param canvas The markers canvas object.
   * @param inactiveCharacteristic The inactive characteristic produced by Auto-Markup.
   * @returns An OCR'd and Classified Characteristic.
   */
  const extractOcrFromMarkup = async (canvas: fabric.Canvas, inactiveCharacteristic: Characteristic) => {
    // Don't extract, either no canvas, or no PDFTron
    if (!canvas || !tronInstance || !part || !partDrawing || !fileUrl) return;

    // Get document information
    const currentPage = tronInstance.Core.documentViewer.getCurrentPage();
    const drawingRotation = tronInstance ? tronInstance.Core.documentViewer.getRotation(currentPage) * 90 : 0;
    const drawingScale = tronInstance ? tronInstance.Core.documentViewer.getZoomLevel() : 1;

    // get mediabox details
    const currentDocument = tronInstance ? tronInstance.Core.documentViewer.getDocument() : null;
    const pdfDocument = currentDocument ? await currentDocument.getPDFDoc() : null;
    const pageIterator = pdfDocument ? await pdfDocument.getPageIterator(currentPage) : null;
    const mediaBox = pageIterator ? await (await pageIterator.current()).getMediaBox() : new tronInstance.Core.PDFNet.Rect(0, 0, 0, 0);

    // Rotate the top left and bottom right corners of the rectangle, set the new top left point and dimensions
    const { newLeft, newTop, newWidth, newHeight } = getTransformedDimensions(
      inactiveCharacteristic.boxLocationX,
      inactiveCharacteristic.boxLocationY,
      inactiveCharacteristic.boxWidth,
      inactiveCharacteristic.boxHeight,
      canvas.getWidth(),
      canvas.getHeight(),
      drawingScale,
      drawingRotation,
    );

    // Add any mediabox offsets
    const offsetLeft = newLeft + mediaBox.x1;
    const offsetTop = newTop - mediaBox.y1;

    // Record start of adding items
    Analytics.track({
      event: Analytics.events.manualCaptureUpdateStarted,
      part,
      drawing: partDrawing,
      properties: {
        'view.sheet': currentPage,
        'view.rotation': drawingRotation,
        'view.scale': drawingScale,
        'capture.width.old': inactiveCharacteristic.boxWidth,
        'capture.width.new': newWidth,
        'capture.height.old': inactiveCharacteristic.boxHeight,
        'capture.height.new': newHeight,
        multipleDocuments: part?.drawings && part.drawings.length > 0,
      },
    });

    const inCompareMode = !!compareMode;
    const imageData = await captureImageFromCanvas(tronInstance, fileUrl, currentPage, offsetLeft, offsetTop, newWidth, newHeight, 0, jwt ?? '', inCompareMode);
    if (imageData) {
      const selectedCaptureCanvas = createCanvas(imageData.data.width, imageData.data.height);
      const selectedCaptureContext = selectedCaptureCanvas.getContext('2d');
      selectedCaptureContext?.putImageData(imageData.data, 0, 0);
      const captureImage = selectedCaptureCanvas.toDataURL();

      return { imageData: captureImage, isGdtOrBasic: imageData.isGdtOrBasic };
    }
  };

  // Function that handles moving a canvas object and then updating the corresponding feature
  const handleModifyObject = async (canvas: fabric.Canvas, e: any) => {
    if (!e.target || !canvas) {
      return;
    }
    // get current document dimensions and viewing state
    const instance = tronInstance;
    if (!instance) return;
    const currentPage = instance.Core.documentViewer.getCurrentPage();
    const document = instance.Core.documentViewer?.getDocument();
    // view values from the DocumentView context aren't updating, pulling from PDFTron for now
    const scale = instance && document ? instance.Core.documentViewer.getZoomLevel() : 1;
    const rotation = instance ? instance.Core.documentViewer.getRotation(currentPage) * 90 : 0;
    const itemGroup = capturedItems[currentPage - 1].filter((capture) => capture.id === e.target?.name)[0];

    const width = canvas.getWidth();
    const zoom = canvas.getZoom();
    const height = canvas.getHeight();

    // completely normalizing the width/height messes this up somehow
    const newItem = modifyObject(e, e.target, itemGroup, rotation, scale, width / zoom, height / zoom, canvas);

    // Do nothing, either no canvas, no PDFTron, or rectangle is too small
    if (!canvas || !newItem || newItem.boxWidth < 5 || newItem.boxHeight < 5 || !part) {
      return;
    }

    if (e.target.type === 'extract' || e.target.type === 'balloon' || e.target.type === 'connectionPoint') {
      const boxCartesian = {
        x: newItem.boxLocationX + newItem.boxWidth / 2,
        y: newItem.boxLocationY + newItem.boxHeight / 2,
      };
      const rotatedBalloon = coordRotation(e.target.left, e.target.top, 360 - rotation, width / zoom, height / zoom);
      const balloon = e.target.type === 'balloon' ? { x: rotatedBalloon.newX, y: rotatedBalloon.newY } : getCartesianFromPolar(boxCartesian, { x: newItem.markerLocationX, y: newItem.markerLocationY }, newItem.markerRotation);

      const { gridCoordinates, connectionPointGridCoordinates, balloonGridCoordinates } = gridPosition(balloon, { left: newItem.boxLocationX, top: newItem.boxLocationY }, rotation, canvas, gridRenderer.grid, newItem);
      newItem.gridCoordinates = gridCoordinates;
      newItem.balloonGridCoordinates = balloonGridCoordinates;
      newItem.connectionPointGridCoordinates = connectionPointGridCoordinates;

      if (e.target.type === 'extract' && !newItem.verified && newItem.captureMethod !== 'Custom') {
        // rectangle was changed, re-OCR and interpret
        dispatch({ type: 'hideCaptureErrorModal' });
        await updateItem(canvas, newItem);
      } else {
        const updates = [newItem];
        let newSelected = [...selected];
        if (!newSelected.find((c) => c.id === newItem.id)) {
          // If we move a feature without selecting it, select that feature
          newSelected = [newItem];
          // Optimistically select feature so the user doesn't have to wait for the update
          reduxDispatch({ type: CharacteristicActions.UPDATE_SELECTED, payload: { selected: newSelected, index: newItem.markerIndex } });
        }
        // if the item updated is part of a shared group we need to update them all
        if (newItem.markerGroupShared) {
          for (let i = 0; i < capturedItems[currentPage - 1].length; i++) {
            const currentItem = capturedItems[currentPage - 1][i];
            let itemObj = { ...currentItem.rectangle?.toObject() };
            // filter the other items in the group but not the original.
            if (itemObj.markerGroup === newItem.markerGroup && itemObj.id !== newItem.id) {
              // handles the case when you group two different captures
              if (e.target.type === 'balloon' && (itemObj.boxLocationX !== newItem.boxLocationX || itemObj.boxLocationY !== newItem.boxLocationY)) {
                itemObj = modifyObject(e, e.target, currentItem, rotation, scale, canvas.getWidth() / canvas.getZoom(), canvas.getHeight() / canvas.getZoom(), canvas);
              } else if (e.target.type !== 'connectionPoint') {
                // as long as it isn't a connection point
                // update info as needed
                itemObj.markerLocationX = newItem.markerLocationX;
                itemObj.markerLocationY = newItem.markerLocationY;
                itemObj.markerRotation = newItem.markerRotation;
                if (!itemObj.connectionPointIsFloating) {
                  itemObj.connectionPointLocationX = newItem.connectionPointLocationX;
                  itemObj.connectionPointLocationY = newItem.connectionPointLocationY;
                  itemObj.leaderLineDistance = newItem.leaderLineDistance;
                }
              }
              updates.push(itemObj);
            }
          }
        }
        // balloon was changed, just update characteristic
        reduxDispatch({ type: CharacteristicActions.EDIT_CHARACTERISTICS, payload: { updates, newSelected } });
      }
    }
    if (e.target.type === 'annotation') {
      const group = e.target as fabric.Group;
      const anText = group._objects.find((o) => o.type === 'annotation-text') as fabric.Textbox;
      if (anText && anText.text) {
        newItem.annotation = FeatureUI.encodeAnnotationText(anText);
      }
      const newAnnotation = Annotations.util.input(newItem);
      reduxDispatch(editAnnotationThunk({ variables: { id: newAnnotation.id, data: newAnnotation.annotation } }));
    }
  };

  // Helper method to delay modification of objects for half a second just in case additional changes are made
  // TODO: Buffering 1/5 second is a long time. We should experiment with 250 - 500 ms
  const debouncedHandleModifyObject = debounce(handleModifyObject, 500, {
    leading: true,
    trailing: true,
  });

  const clicked = useRef<string | null>(null);

  const handleEditAnnotation = (canvas: fabric.Canvas, rect: fabric.Rect | undefined, e: fabric.IEvent) => {
    const instance = tronInstance;
    if (instance) {
      const document = instance?.Core.documentViewer?.getDocument();
      const currentPage = instance && document ? instance.Core.documentViewer.getCurrentPage() : 1;
      const pageRotation = instance && document ? instance.Core.documentViewer.getRotation(currentPage) * 90 : 0;
      const anText = e.target as fabric.Textbox;
      const annotation = anText.toObject() as Annotation;
      const newAnnotation = Annotations.util.input({ ...annotation, annotation: anText.text || '' });
      newAnnotation.annotation.boxWidth = pageRotation % 180 === 0 ? rect?.width || newAnnotation.annotation.boxWidth : rect?.height || newAnnotation.annotation.boxHeight;
      newAnnotation.annotation.boxHeight = pageRotation % 180 === 0 ? rect?.height || newAnnotation.annotation.boxHeight : rect?.width || newAnnotation.annotation.boxWidth;

      anText.toObject = () => {
        return { ...annotation, ...newAnnotation.annotation };
      };
    }
  };

  // Function to handle moving the leader line and connection point on the capture if a balloon is moved
  const updateConnectedBalloonLocation = (item: any, itemGroupToUpdate: any, scale: number, rotation: number, canvas: fabric.Canvas) => {
    const x1 = item.left;
    const y1 = item.top;
    const { newLeft, newTop, newWidth, newHeight } = getTransformedDimensions(
      itemGroupToUpdate.rectangle.left,
      itemGroupToUpdate.rectangle.top,
      itemGroupToUpdate.rectangle.width,
      itemGroupToUpdate.rectangle.height,
      canvas.getWidth(),
      canvas.getHeight(),
      scale,
      rotation,
    );
    const newCoords = FeatureUI.shortestDistanceX2Y2({ x1, y1, top: newTop, left: newLeft, width: newWidth, height: newHeight, rotation });
    if (newCoords) {
      itemGroupToUpdate.leaderLine.set({
        x1,
        y1,
        x2: newCoords?.x2,
        y2: newCoords?.y2,
      });
    }

    //Show leader line
    itemGroupToUpdate.leaderLine.set({ opacity: 1 });

    if (itemGroupToUpdate.connectionPoint && x1 && newCoords?.x2 && y1 && newCoords?.y2) {
      // account for stroke width
      itemGroupToUpdate.connectionPoint.set({
        top: newCoords?.y2 + itemGroupToUpdate.leaderLine.strokeWidth / 2,
        left: newCoords?.x2 + itemGroupToUpdate.leaderLine.strokeWidth / 2,
      });
      // have to set it for connection point too
      if (newCoords && item?.width && newCoords.distance >= item.width * 1.5) {
        itemGroupToUpdate.connectionPoint.set({
          opacity: 1,
        });
      } else {
        itemGroupToUpdate.connectionPoint.set({
          opacity: 0,
        });
      }
    }
  };

  // Function to handle moving the leader line and connection point on the capture if a capture is moved
  const updateConnectedCaptureLocation = (item: any, itemGroupToUpdate: any) => {
    // fix rotation
    const newCoords = FeatureUI.shortestDistanceX2Y2({ x1: itemGroupToUpdate.balloon.left, y1: itemGroupToUpdate.balloon.top, top: item.top, left: item.left, width: item.width, height: item.height, rotation: 0 });
    if (newCoords) {
      itemGroupToUpdate.leaderLine.set({
        x2: newCoords?.x2,
        y2: newCoords?.y2,
      });
      // account for stroke width
      itemGroupToUpdate.connectionPoint.set({
        left: newCoords?.x2 + itemGroupToUpdate.leaderLine.strokeWidth / 2,
        top: newCoords?.y2 + itemGroupToUpdate.leaderLine.strokeWidth / 2,
      });
    }
    if (newCoords && itemGroupToUpdate?.balloon.width && newCoords.distance >= itemGroupToUpdate?.balloon.width) {
      itemGroupToUpdate.leaderLine.set({
        opacity: 1,
      });
      itemGroupToUpdate.connectionPoint.set({
        opacity: 1,
      });
    } else {
      itemGroupToUpdate.leaderLine.set({
        opacity: 0,
      });
      itemGroupToUpdate.connectionPoint.set({
        opacity: 0,
      });
    }
  };

  /* eslint-disable no-param-reassign */
  const handleObjectScaling = (e: any, canvas: fabric.Canvas) => {
    const activeObj = canvas.getActiveObject();
    // prevent strokeWidth from scaling
    if (!activeObj || (activeObj.type !== 'extract' && activeObj.type !== 'annotation')) return;
    if (activeObj !== null) {
      activeObj.strokeUniform = true;
    }
    const instance = tronInstance as WebViewerInstance;

    if (activeObj.type === 'annotation' && instance) {
      // Adjust text size so it doesn't scale with the rectangle
      const currentPage = instance.Core.documentViewer.getCurrentPage();
      const drawingRotation = instance ? instance.Core.documentViewer.getRotation(currentPage) * 90 : 0;
      const group = activeObj as fabric.Group;
      const anText = group._objects.find((obj) => obj.type === 'annotation-text') as fabric.Textbox;
      const annot = anText.toObject() as Annotation;
      const isRotated = ((360 + drawingRotation - annot.drawingRotation) % 360) % 180 !== 0;
      if (group.width && group.height && anText && anText.width && anText.height) {
        const currentWidth = group.width * (group.scaleX || 1);
        const currentHeight = group.height * (group.scaleY || 1);
        const newWidth = isRotated ? (group.height || 0) * e.transform.target.scaleY : (group.width || 0) * e.transform.target.scaleX;
        anText.set({
          scaleX: isRotated ? group.height / currentHeight : group.width / currentWidth,
          scaleY: isRotated ? group.width / currentWidth : group.height / currentHeight,
          width: newWidth - 4,
        });
      }
      group.set('dirty', true);
      group.render(canvas.getContext());
    }
  };

  const handleMouseMove = (canvas: fabric.Canvas, e: any) => {
    if (state.extracting && e.target === null) {
      canvas.setCursor('crosshair');
    }

    if (!state.extracting && e.target?.type === 'region') {
      canvas.setCursor('pointer');
    }

    if (state.extracting && e.e && (e.e.ctrlKey || e.e.shiftKey) && e.target === null) {
      canvas.setCursor('default');
    }
  };

  const handleDblClick = (canvas: fabric.Canvas, obj: fabric.Group) => {
    const objects = obj._objects;
    const anText = objects.find((o) => o.type === 'annotation-text') as fabric.Textbox;
    if (anText) {
      // split apart group and select the anText object so user can edit
      obj._restoreObjectsState();
      canvas.remove(obj);
      for (let i = 0; i < objects.length; i++) {
        canvas.add(objects[i]);
        objects[i].dirty = true;
      }
      canvas.renderAll();
      canvas.setActiveObject(anText);
      anText.enterEditing();
      anText.selectAll();
    }
  };

  const handleFinishEditing = (canvas: fabric.Canvas, e: any) => {
    const objs = canvas.getObjects();
    const objToGroup = objs.filter((o) => o.name === e.target.name);
    const anText = objToGroup.find((o) => o.type === 'annotation-text') as fabric.Textbox;
    const rect = objToGroup.find((o) => o.type === 'annotation-bg') as fabric.Rect;
    const instance = tronInstance;
    if (anText && instance) {
      const document = instance?.Core.documentViewer?.getDocument();
      const currentPage = instance && document ? instance.Core.documentViewer.getCurrentPage() : 1;
      const pageRotation = instance && document ? instance.Core.documentViewer.getRotation(currentPage) * 90 : 0;
      const annotation = anText.toObject() as Annotation;
      const encodedText = FeatureUI.encodeAnnotationText(anText);
      const newAnnotation = Annotations.util.input({ ...annotation, annotation: encodedText || '' });
      newAnnotation.annotation.boxWidth = pageRotation % 180 === 0 ? rect?.width || newAnnotation.annotation.boxWidth : rect?.height || newAnnotation.annotation.boxHeight;
      newAnnotation.annotation.boxHeight = pageRotation % 180 === 0 ? rect?.height || newAnnotation.annotation.boxHeight : rect?.width || newAnnotation.annotation.boxWidth;
      anText.toObject = () => {
        return { ...annotation, ...newAnnotation };
      };
      reduxDispatch(editAnnotationThunk({ variables: { id: newAnnotation.id, data: newAnnotation.annotation } }));
    }
  };

  const handleMouseDown = (canvas: fabric.Canvas, e: any) => {
    if (!e.pointer) return;
    if (multi.current) multi.current = false; // clear multi select on new mouse action
    if (e.target && e.target !== null) {
      if (e.target.type !== 'region') {
        // set dragging to block extraction
        canvas.setActiveObject(e.target);
        dragStart.current = { x: e.pointer.x, y: e.pointer.y };
      }
      if (e.target.type === 'annotation') {
        const characteristic = e.target.toObject();
        if (clicked.current === characteristic.id) {
          // object was dbl clicked
          handleDblClick(canvas, e.target);
        } else {
          clicked.current = characteristic.id;
          setTimeout(() => {
            clicked.current = null;
          }, 300);
        }
      }
    } else if (state.extracting && e.pointer) {
      // Clear drag
      dragStart.current = undefined;
      // Record first coordinate of selection box
      selectStart.current = { x: e.pointer.x, y: e.pointer.y };
    }
  };

  const handleMouseUp = async (canvas: fabric.Canvas, e: any) => {
    if (!e.pointer) return;
    const selectionType = getSelectionType(canvas, e, multi.current);
    dispatch({ type: 'hideCaptureErrorModal' });
    if (selectionType === 'multi') {
      // Selecting or deselecting existing items
      let newSelected: (Characteristic | Annotation)[] = e.e && e.e.ctrlKey ? selected.map((clone) => ({ ...clone })) : [];
      canvas.getActiveObjects().forEach((obj) => {
        const char = obj.toObject();
        if (!selected.some((selectedChar) => selectedChar.id === char.id)) {
          if (char.markerGroupShared) {
            // add the rest of the group
            newSelected = newSelected.concat(characteristics.filter((characteristic) => characteristic.markerGroup === char.markerGroup));
          } else {
            newSelected.push(char);
          }
        } else if (!multi.current) {
          // deselect
          if (char.markerGroupShared) {
            // deselect all of the group
            newSelected = newSelected.filter((characteristic) => characteristic.markerGroup !== char.markerGroup);
          } else {
            // deselect only this one
            newSelected = selected.filter((characteristic) => characteristic.id !== char.id);
          }
        }
      });
      reduxDispatch({ type: CharacteristicActions.UPDATE_SELECTED, payload: { selected: newSelected } });
    } else if (selectionType === 'single') {
      if (e.target && e.target.type === 'region') {
        const characteristic = { ...e.target.toObject() };

        const instance = tronInstance;
        const document = instance?.Core.documentViewer?.getDocument();
        const currentPage = instance && document ? instance.Core.documentViewer.getCurrentPage() : 1;

        const newRect = getNewRect(e.target.aCoords.bl.x, e.target.aCoords.tr.y, e.target.width, e.target.height);
        newRect.animate('strokeDashOffset', '-=100', {
          duration: 10000,
          onChange: canvas.renderAll.bind(canvas),
        });
        const currentTally = state.captureTally;
        // this will update and trigger the function to add rect
        // rotation here is the rotation of the capture,
        dispatch({
          type: 'addRectNewCapture',
          payload: {
            newRect,
            params: {
              id: characteristic.id,
              addItem: false,
              canvas,
              dims: { left: e.target.aCoords.bl.x, top: e.target.aCoords.tr.y, width: e.target.width, height: e.target.height },
              rotation: 0,
              sanDims: {
                sanLeft: e.target.aCoords.bl.x,
                sanTop: e.target.aCoords.tr.y,
                sanWidth: e.target.width,
                sanHeight: e.target.height,
              },
              tally: currentTally,
              event: e,
              page: currentPage,
            },
          },
        });
        revealItem(e.target, canvas, characteristic.id);
      }
      const char = canvas.getActiveObject()?.toObject();
      if (char) {
        // Selecting a single item
        if (char.markerGroupShared) {
          // selected item was part of a shared group, select all items in the group
          const group = characteristics.filter((characteristic) => characteristic.markerGroup === char.markerGroup);
          reduxDispatch({ type: CharacteristicActions.UPDATE_SELECTED, payload: { selected: group, index: group[0].markerIndex } });
        } else if (char.annotation) {
          reduxDispatch({ type: CharacteristicActions.UPDATE_SELECTED, payload: { selected: [char], index: char.characteristic ? char.characteristic.markerIndex : selected[0]?.markerIndex || index } });
        } else if (selected.length !== 1 || selected[0].id !== char.id) {
          reduxDispatch({ type: CharacteristicActions.UPDATE_SELECTED, payload: { selected: [char], index: char.markerIndex } });
        }
      }
    } else if (selectionType === 'new') {
      // Creating a new selection box
      // extracting removed conditions: e.target?.type !== 'balloon' && e.target?.type !== 'extract'
      if (selectStart.current && state.extracting && e.pointer && !multi.current) {
        const tlBrObject = FeatureUI.getCaptureCorners({ select: selectStart.current, pointer: e.pointer });
        if (FeatureUI.captureHasOverlap({ select: selectStart.current, pointer: e.pointer, canvas })) {
          // notify user that the capture they tried to make failed
          notification.error({
            message: i18n('entities.feature.errors.titles.captureError'),
            description: i18n('entities.feature.errors.messages.contains'),
          });
          return;
        }
        // Finish extraction
        // TODO: These values should come from the document viewer context
        const instance = tronInstance;
        const document = instance?.Core.documentViewer?.getDocument();
        const currentPage = instance && document ? instance.Core.documentViewer.getCurrentPage() : 1;
        const pageRotation = instance && document ? instance.Core.documentViewer.getRotation(currentPage) * 90 : 0;
        const scale = instance && document ? instance.Core.documentViewer.getZoomLevel() : 1;

        const scaledDims = FeatureUI.scaleCaptureDimensions({ tl: tlBrObject.tl, br: tlBrObject.br, scale });

        // If the bounding box is large enough, create a new item
        if (selectStart.current.x > 5) {
          // Convert selected coordinates to the values when the PDF unrotated and unscaled
          // NOTE: Dont send these coordinates to the OCR or it will extract the wrong image
          const newRect = getNewRect(scaledDims.left, scaledDims.top, scaledDims.width, scaledDims.height);
          newRect.animate('strokeDashOffset', '-=100', {
            duration: 10000,
            onChange: canvas.renderAll.bind(canvas),
          });
          const currentTally = state.captureTally;
          const sanitizedBoxCoordinates = getSanitizedDimensions(tlBrObject.tl.x, tlBrObject.tl.y, tlBrObject.br.x, tlBrObject.br.y, canvas.getWidth() / scale, canvas.getHeight() / scale, pageRotation, 1);
          // this will update and trigger the function to add rect
          // rotation here is the rotation of the capture,
          dispatch({
            type: 'addRectNewCapture',
            payload: {
              newRect,
              params: {
                addItem: true,
                canvas,
                dims: scaledDims,
                rotation: 0,
                sanDims: {
                  sanLeft: sanitizedBoxCoordinates.left,
                  sanTop: sanitizedBoxCoordinates.top,
                  sanWidth: sanitizedBoxCoordinates.width,
                  sanHeight: sanitizedBoxCoordinates.height,
                },
                tally: currentTally,
                event: e,
                page: currentPage,
              },
            },
          });
        }
      }

      if (selected && selected.length > 0) {
        // clears out any selected characteristics
        reduxDispatch({ type: CharacteristicActions.UPDATE_SELECTED, payload: { selected: [] } });
      }
    }
    selectStart.current = undefined;
  };

  // Handle autoballooned extractions
  const revealItem = async (region: fabric.Object, canvas: fabric.Canvas, characteristicId: string) => {
    if (part === null) return;

    const newSelected: Characteristic[] = [];
    const newUpdates: Characteristic[] = [];
    const extractedList: Characteristic[] = characteristics.map((clone) => ({ ...clone }));
    const inactiveList: Characteristic[] = inactiveCharacteristics.map((clone) => ({ ...clone }));

    const prevItem = characteristics[characteristics.length - 1];
    let newMarkerGroup = prevItem ? (parseInt(prevItem.markerGroup, 10) + 1).toString() : '1';
    let newMarkerIndex = prevItem ? prevItem.markerIndex + 1 : 0;

    const newItem = revealCharacteristic(region, assignedStyles!, newMarkerGroup, newMarkerIndex, characteristicId);
    newItem.drawingSheet = partDrawing && partDrawing.sheets.length > 0 ? partDrawing.sheets[newItem.drawingSheetIndex - 1] : newItem.drawingSheet;

    // If newItem has already been selected, return
    if (extractedList.some((char) => char.id === newItem.id)) {
      return;
    }

    // add new item to list of extracted items, add it to active IDs, remove it from inactive items
    newUpdates.push(newItem);
    extractedList.push(newItem);
    const ind = inactiveList.findIndex((char) => char.id === newItem.id);
    inactiveList.splice(ind, 1);
    reduxDispatch({
      type: CharacteristicActions.UPDATE_SELECTED,
      payload: {
        characteristics: extractedList,
        inactiveCharacteristics: inactiveList,
        selected: newSelected,
      },
    });

    reduxDispatch({
      type: CharacteristicActions.EDIT_CHARACTERISTICS,
      payload: {
        updates: newUpdates,
        newSelected,
      },
    });

    if (socket) {
      const extractedMarkup = await extractOcrFromMarkup(canvas, newItem);
      if (extractedMarkup) {
        socket.emit('characteristic.getOcr', { imageData: extractedMarkup.imageData, isGdtOrBasic: extractedMarkup.isGdtOrBasic, characteristicId: newItem.id, tolerances, types });
      }
    }
  };

  const handleSelectObjects = async (canvas: fabric.Canvas, e: any) => {
    if (!e && !e.e) return;
    // Ignore balloon, line, and connectionPoint updates if the mouse is down
    if (!canvas || (state.mouseDown && (e.target.type === 'balloon' || e.target.type === 'line' || e.target.type === 'connectionPoint'))) return;
    // Array of selected Characteristics
    const newSelected: (Characteristic | Annotation)[] = e.e && e.e.ctrlKey ? selected : [];

    // Array of selected Fabric objects
    const items = canvas.getActiveObjects();
    if (!items || items.length <= 0) return;

    // Loop over selected Fabric objects
    items.forEach((item) => {
      if (item.type === 'extract' || item.type === 'balloon' || item.type === 'annotation') {
        // Populate the array of selected characteristics and balloons (without adding duplicates)
        const char = item.toObject();
        // TODO: This is a very expensive lookup, because the entire characteristics list is being looped over inside a loop of the entire characteristics list.
        // Consider tracking these as an ordered hash
        if (selected.length === 0 || !selected.some((selectedChar) => selectedChar.id === char.id)) {
          if (char.markerGroupShared) {
            // add the entire group
            for (let i = char.markerIndex; i < characteristics.length; i++) {
              const groupedChar = characteristics[i];
              if (char.markerGroup !== groupedChar.markerGroup) break; // reached end of group
              // check that grouped char isn't already selected
              if (!selected.some((selectedChar) => selectedChar.id === groupedChar.id)) {
                newSelected.push(groupedChar);
              }
            }
          } else {
            newSelected.push(char);
          }
        }
      }
    });

    // Determine if user has selected multiple items
    if (selected.length > 1 || (e.e && e.e.ctrlKey)) {
      multi.current = true;
    } else {
      multi.current = false;
    }
  };

  /* eslint-disable no-param-reassign */
  // Method to setup mouse events whenever canvas is re-rendered
  const setCanvasEvents = (canvas: fabric.Canvas, newMode: string) => {
    // if the mouse is down we don't want to update the canvas events
    if (!canvas) {
      return;
    }
    // Remove old event handlers
    canvas.off('selection:created');
    canvas.off('selection:updated');
    canvas.off('object:modified');
    canvas.off('mouse:down');
    canvas.off('mouse:down:before');
    canvas.off('mouse:up');
    canvas.off('mouse:up:before');
    canvas.off('mouse:move');
    canvas.off('object:scaling');
    canvas.off('text:changed');
    canvas.off('text:editing:exited');

    canvas.on('mouse:move', (e) => {
      handleMouseMove(canvas, e);
    });

    canvas.on('object:scaling', (e) => {
      handleObjectScaling(e, canvas);
    });

    // Mouse down hooks
    canvas.on('mouse:down:before', () => {
      canvas.selection = true; // keeps the blue highlight on click+drag
      dispatch({
        type: 'updateMouseDown',
        payload: {
          mouseDown: true,
        },
      });
    });

    // Starts extraction if manual newMode, single region add if auto
    canvas.on('mouse:down', (e) => {
      handleMouseDown(canvas, e);
    });

    canvas.on('object:moving', (e) => {
      if (!state.movingLock) {
        dispatch({ type: 'movingLock', payload: true });
      }
      const item = e.target;
      const document = tronInstance?.Core.documentViewer?.getDocument();
      const page: number = tronInstance && document ? tronInstance.Core.documentViewer.getCurrentPage() : 1;
      const rotation = tronInstance && document ? tronInstance.Core.documentViewer.getRotation(page) * 90 : 0;
      const scale: number = tronInstance && document ? tronInstance.Core.documentViewer.getZoomLevel() : 1;
      const itemGroupToUpdate = capturedItems[page - 1] ? capturedItems[page - 1].filter((capture) => capture.id === item?.name)[0] : undefined;

      const normalizedPageWidth = rotation % 180 === 0 ? canvas.getWidth() / scale : canvas.getHeight() / scale;
      const normalizedPageHeight = rotation % 180 === 0 ? canvas.getHeight() / scale : canvas.getWidth() / scale;

      // stop items from moving outside of canvas
      if (item?.width && item?.height && item?.left && item?.top) {
        if (item.left - item.width / 2 <= 0) {
          item.set({
            left: item.width / 2,
          });
        }

        if (item.top - item.height / 2 <= 0) {
          item.set({
            top: item.height / 2,
          });
        }
        if (rotation % 180 === 0) {
          // page rotation needs to be accounted for
          if (item.width + item.left > normalizedPageWidth - item.width / 2) {
            item.set({
              left: normalizedPageWidth - item.width / 2,
            });
          }

          if (item.height / 2 + item.top > normalizedPageHeight) {
            item.set({
              top: normalizedPageHeight - item.height / 2,
            });
          }
        } else {
          if (item.width / 2 + item.left > normalizedPageHeight) {
            item.set({
              left: normalizedPageHeight - item.width / 2,
            });
          }

          if (item.height / 2 + item.top > normalizedPageWidth) {
            item.set({
              top: normalizedPageWidth - item.height / 2,
            });
          }
        }
      }
      // if a balloon is moving
      // need to make sure certain values exist so typescript doesn't yell at us
      if (item?.type === 'balloon' && item?.left && item?.top && itemGroupToUpdate?.leaderLine) {
        const itemFeature = itemGroupToUpdate.leaderLine?.toObject();
        const featuresToUpdate = [itemGroupToUpdate];
        // if it is a part of a shared group we have to update all of them
        if (itemFeature.markerGroupShared) {
          for (let i = 0; i < capturedItems[page - 1].length; i++) {
            const newItem = capturedItems[page - 1][i];
            const itemObj = { ...newItem.rectangle?.toObject() };
            // filter the other items in the group but not the original.
            if (itemObj.markerGroup === itemFeature.markerGroup && itemObj.id !== itemFeature.id) {
              featuresToUpdate.push(newItem);
            }
          }
        }
        // loop through the features and update as needed
        for (let j = 0; j < featuresToUpdate.length; j++) {
          const featureGroup = featuresToUpdate[j];
          const featureObj = { ...featureGroup.rectangle?.toObject() };
          if (featureGroup && featureObj.connectionPointIsFloating) {
            // just update the new balloon position
            featureGroup?.leaderLine?.set({
              x1: item.left,
              y1: item.top,
            });
          } else {
            updateConnectedBalloonLocation(item, featureGroup, scale, rotation, canvas);
          }
        }
      }
      // if a capture is moving
      else if (item?.type === 'extract' && itemGroupToUpdate?.balloon) {
        const itemFeature = itemGroupToUpdate.balloon?.toObject();
        if (itemFeature && !itemFeature.connectionPointIsFloating) {
          updateConnectedCaptureLocation(item, itemGroupToUpdate);
        }
      }
      // if a connection point is being moved
      else if (item?.type === 'connectionPoint' && item?.left && item?.top) {
        const x2 = item.left;
        const y2 = item.top;
        if (itemGroupToUpdate?.leaderLine) {
          // sets leader line end point
          itemGroupToUpdate.leaderLine.set({
            x2,
            y2,
          });

          if (itemGroupToUpdate.connectionPoint && itemGroupToUpdate.leaderLine.strokeWidth) {
            // sets connection point location
            // account for stroke width
            itemGroupToUpdate.connectionPoint.set({
              top: y2 + itemGroupToUpdate.leaderLine.strokeWidth / 2,
              left: x2 + itemGroupToUpdate.leaderLine.strokeWidth / 2,
            });
          }
        }
      }
      canvas.renderAll();
    });

    // Mouse up hooks
    canvas.on('mouse:up:before', (e) => {
      dispatch({
        type: 'updateMouseDown',
        payload: {
          mouseDown: false,
        },
      });
      if ((newMode === 'Manual' || newMode === 'Custom') && e.e && !(e.e as any).ctrlKey) {
        // doing manual extraction, don't select overlapped items
        canvas.selection = false;
      } else {
        canvas.selection = true;
      }
    });

    // Finish extraction if manual newMode
    canvas.on('mouse:up', (e) => {
      // if the item doesn't move, we are just updating what is selected like normal.
      // if the item does move we don't need to select it.
      if (!dragStart.current || (dragStart.current?.x === e.pointer?.x && dragStart.current?.y === e.pointer?.y)) {
        handleMouseUp(canvas, e);
      } else {
        // clear dragging so we can extract
        dragStart.current = undefined;
      }
    });

    // Update object dimensions/position/orientation in the DB
    canvas.on('object:modified', (e) => {
      if (e && e.target && e.target.type === 'annotation-text') return;
      debouncedHandleModifyObject(canvas, e);
    });

    canvas.on('selection:created', (e) => handleSelectObjects(canvas, e));
    canvas.on('selection:updated', (e) => handleSelectObjects(canvas, e));

    canvas.on('text:changed', (e) => {
      const rect = canvas._objects.find((o) => o.name === e.target?.name && o.type === 'annotation-bg');
      const instance = tronInstance as WebViewerInstance;
      if (instance) {
        const currentPage = instance.Core.documentViewer.getCurrentPage();
        const drawingRotation = instance ? instance.Core.documentViewer.getCompleteRotation(currentPage) * 90 : 0;
        if (rect && e.target) FeatureUI.renderTextboxAfterEdit({ rect, anText: e.target as fabric.Textbox, rotation: drawingRotation });
        handleEditAnnotation(canvas, rect, e);
      }
    });

    canvas.on('text:editing:exited', (e) => {
      handleFinishEditing(canvas, e);
    });
  };

  const renderRectangles = (canvas: fabric.Canvas, page: number) => {
    if (state && state.captureArray.length > 0) {
      state.captureArray.forEach((capture: { rect: fabric.Object; params: any; id: string }) => {
        if (capture.rect && capture.params.page === page) {
          capture.rect.setCoords();
          // fabricjs caches objects as images for performance, originally the rect was going to the right position but wasn't rotating
          // apparently it was using the cached image and just changing location, set it to dirty to replace the cached image with the new rect
          capture.rect.set('dirty', true);
          capture.rect.set('evented', false);
          // capture.rect.evented = false;
          canvas.add(capture.rect);
        }
      });
    }
  };

  const updatePopover = (targetObject: any, canvas: any) => {
    if (popoverRef.current !== null) {
      const documentLocation = document.querySelector('#partCanvas')?.getBoundingClientRect();
      const canvasLocation = canvas.getElement().getBoundingClientRect();
      const objectLocation = targetObject.getBoundingRect();
      popoverRef.current.style.top = `${documentLocation?.top + canvasLocation.top + objectLocation.top}px`;
      popoverRef.current.style.left = `${documentLocation?.left + canvasLocation.left + objectLocation.left}px`;
    }
  };

  const renderRectangle = (capture: CapturedItem, rectangle: fabric.Rect, canvas: fabric.Canvas, scale: number, rotation: number, autoballoonComplete: boolean, caution: boolean) => {
    // Unpack the Fabric object
    const characteristic = rectangle.toObject();

    let lastItem;

    // rotate the top left and bottom right corners of the rectangle, set the new top left point and dimensions
    const { newLeft, newTop, newWidth, newHeight } = getTransformedDimensions(characteristic.boxLocationX, characteristic.boxLocationY, characteristic.boxWidth, characteristic.boxHeight, canvas.getWidth(), canvas.getHeight(), scale, rotation);
    const anchorCoord = { left: newLeft, top: newTop, width: newWidth, height: newHeight };
    const charCoord = {
      x: characteristic.markerLocationX,
      y: characteristic.markerLocationY,
    };
    if (capture && capture.rectangle && capture.rectangle.left && capture.rectangle.top) {
      let renderedLine;
      let renderedConnectionPoint;
      // clone the rectangle so we can update it without conflict
      const captureRect = fabric.util.object.clone(capture.rectangle);

      // Disable dragging for auto markup regions
      if (characteristic.status === CharacteristicStatus.Inactive) {
        captureRect.lockMovementX = true;
        captureRect.lockMovementY = true;
      }

      // Add rectangle if its a region and mode is Auto, or if the extracted Item is currently selected
      if (
        (mode === 'Auto' && capture.rectangle.type === 'region' && navigation.currentPartStep >= EXTRACT) ||
        (selected &&
          selected.some((someChar) => {
            return someChar.id === capture.id;
          })) ||
        !!capture.annotation
      ) {
        const newRect = FeatureUI.renderFeatureCapture({ rectangle: captureRect, anchorCoord, boxRotation: characteristic.boxRotation, canvas });
        lastItem = newRect;
        // only showing capture point if it is selected
        // if the connection point is floating it needs to point to it
        // otherwise they both need to be on the box
        if (part?.displayLeaderLines && capture.leaderLine && capture.connectionPoint && characteristic.connectionPointIsFloating && characteristic.displayLeaderLine) {
          const normalizedPageWidth = rotation % 180 === 0 ? canvas.getWidth() / scale : canvas.getHeight() / scale;
          const normalizedPageHeight = rotation % 180 === 0 ? canvas.getHeight() / scale : canvas.getWidth() / scale;

          // rotate the top left and bottom right corners of the connectionPoint, set the new top left point and dimensions
          const rotatedCoord = coordRotation(characteristic.connectionPointLocationX, characteristic.connectionPointLocationY, rotation, normalizedPageWidth, normalizedPageHeight);

          renderedLine = FeatureUI.renderFeatureLineToPoint({ anchorCoord, charCoord, x2: rotatedCoord.newX, y2: rotatedCoord.newY, line: capture.leaderLine, canvas, rotation, markerRotation: characteristic.markerRotation });
          if (renderedLine.strokeWidth) {
            // account for stroke width
            renderedConnectionPoint = FeatureUI.renderConnectionPoint({ x2: rotatedCoord.newX + renderedLine.strokeWidth / 2, y2: rotatedCoord.newY + renderedLine.strokeWidth / 2, connectionPoint: capture.connectionPoint, canvas });
          }
        } else if (part?.displayLeaderLines && capture.leaderLine && !characteristic.connectionPointIsFloating && characteristic.displayLeaderLine) {
          renderedLine = FeatureUI.renderFeatureLineToCapture({ anchorCoord, charCoord, line: capture.leaderLine, canvas, rotation, markerRotation: characteristic.markerRotation });
          if (capture.connectionPoint && renderedLine?.x2 && renderedLine?.y2) {
            if (renderedLine.strokeWidth) {
              // account for stroke width
              renderedConnectionPoint = FeatureUI.renderConnectionPoint({ x2: renderedLine.x2 + renderedLine.strokeWidth / 2, y2: renderedLine.y2 + renderedLine.strokeWidth / 2, connectionPoint: capture.connectionPoint, canvas });
            }
          }
        }
      } else if (characteristic?.captureError && !hideFeatures) {
        // show all features with captureErrors
        FeatureUI.renderFeatureCapture({ rectangle: captureRect, anchorCoord, boxRotation: characteristic.boxRotation, canvas });
      } else if (hideFeatures) {
        if (characteristic.status === CharacteristicStatus.Active) {
          captureRect.set('fill', 'white');
          captureRect.set('stroke', 'white');
          captureRect.set('evented', false);
          FeatureUI.renderFeatureCapture({ rectangle: captureRect, anchorCoord, boxRotation: characteristic.boxRotation, canvas });
        }
      }
      if (capture.leaderLine && capture.connectionPoint && characteristic.connectionPointIsFloating && part?.displayLeaderLines && !renderedLine && characteristic.displayLeaderLine) {
        const normalizedPageWidth = rotation % 180 === 0 ? canvas.getWidth() / scale : canvas.getHeight() / scale;
        const normalizedPageHeight = rotation % 180 === 0 ? canvas.getHeight() / scale : canvas.getWidth() / scale;

        // rotate the top left and bottom right corners of the rectangle, set the new top left point and dimensions
        const rotatedCoord = coordRotation(characteristic.connectionPointLocationX, characteristic.connectionPointLocationY, rotation, normalizedPageWidth, normalizedPageHeight);

        renderedLine = FeatureUI.renderFeatureLineToPoint({ anchorCoord, charCoord, x2: rotatedCoord.newX, y2: rotatedCoord.newY, line: capture.leaderLine, canvas, rotation, markerRotation: characteristic.markerRotation });
      } else if (capture.leaderLine && capture.connectionPoint && !characteristic.connectionPointIsFloating && part?.displayLeaderLines && !renderedLine && characteristic.displayLeaderLine) {
        renderedLine = FeatureUI.renderFeatureLineToCapture({ anchorCoord, charCoord, line: capture.leaderLine, canvas, rotation, markerRotation: characteristic.markerRotation });
      }

      if (capture.balloon?.width && renderedLine?.x1 && renderedLine.x2 && renderedLine.y1 && renderedLine.y2) {
        const distance = FeatureUI.getLengthOfLine({ x1: renderedLine.x1, y1: renderedLine.y1, x2: renderedLine.x2, y2: renderedLine.y2 });

        if (!characteristic.connectionPointIsFloating && distance <= capture.balloon.width * 1.5) {
          if (renderedConnectionPoint) {
            renderedConnectionPoint.set({
              opacity: 0,
            });
          }
        }
      }
      if (renderedLine) {
        // makes sure the line is always behind the balloons
        canvas.sendToBack(renderedLine);
      }
      // Place balloon icons
      if (capture.balloon && !caution) {
        FeatureUI.renderFeatureMarker({ anchorCoord, charCoord, balloon: capture.balloon, canvas, rotation, markerRotation: characteristic.markerRotation });
      }
    }

    return lastItem;
  };

  const renderCaptures = (pageIndex: number, canvas: fabric.Canvas, showPopup: boolean = true, caution: boolean = false) => {
    if (!canvas) return;
    if (!capturedItems[pageIndex]) {
      // when captures is empty there is no capturedItems[pageIndex] for pages past 1, so this allows those pages to be cleared when all features are deleted
      canvas.remove(...canvas.getObjects());
      return;
    }
    // canvas.clear removes the events as well so we just remove the objects
    canvas.remove(...canvas.getObjects());

    let lastItem: undefined | fabric.Object;
    const document = tronInstance?.Core.documentViewer?.getDocument();
    // view values from the DocumentView context aren't updating, pulling from PDFTron for now
    const rotation = tronInstance && document ? tronInstance.Core.documentViewer.getRotation(pageIndex + 1) * 90 : 0;
    const scale = tronInstance && document ? tronInstance.Core.documentViewer.getZoomLevel() : 1;

    const sheets1 = part?.drawings?.map((drawing) => drawing.sheets).flat() ?? [];
    const autoballoonValues = sheets1.reduce(getAutoBalloonResults, {
      found: 0,
      accepted: 0,
      acceptedTimeout: 0,
      rejected: 0,
      reviewed: 0,
      reviewedTimeout: 0,
      createdAt: part?.createdAt,
    });

    const autoballoonComplete = isAutoballooningDone(autoballoonValues, part);

    // Loop over all captured items
    capturedItems[pageIndex].forEach((capture) => {
      if (!capture.rectangle && !capture.annotation) {
        log.warn('renderCapture forEach char was undefined');
        return;
      }

      if (capture.rectangle) {
        const possibleLastItem = renderRectangle(capture, capture.rectangle, canvas, scale, rotation, autoballoonComplete, caution);
        if (possibleLastItem) lastItem = possibleLastItem;
      }

      if (capture.annotation) {
        const possibleLastItem = FeatureUI.renderAnnotation({ capture, annotation: capture.annotation, canvas, scale, rotation, caution, selected });
        if (possibleLastItem) lastItem = possibleLastItem;
      }
    });
    renderRectangles(canvas, pageIndex + 1);

    canvas.off('selection:created');
    if (lastItem) {
      if (!state.mouseDown) {
        canvas.setActiveObject(lastItem);
      }
      const lastItemObj = lastItem.toObject();
      // show if there is a captureError, we are allowed to show the popup (from hide/show), and it is not the first render of the capture
      if (lastItemObj.captureError && showPopup && !lastItemObj?.firstRender) {
        const captureError = JSON.parse(lastItemObj.captureError);
        setTimeout(() => {
          updatePopover(lastItem, canvas);
          dispatch({
            type: 'showCaptureErrorModal',
            payload: {
              captureErrorInfo: captureError,
            },
          });
        }, 750);
      } else {
        // delete so the next time it will show the pop up
        delete lastItemObj.firstRender;
      }
    }
    setCanvasEvents(canvas, mode);
    canvas.renderAll();
  };

  // When a new CharacteristicCanvas is added to PdfTron, redraw the balloons and rectangles
  layerManager.subscribe('CharacteristicCanvas', 'layerCanvasMounted', (data: any) => {
    if (data.layer.name === 'markers') {
      const { canvas } = data.layer;
      if (!canvas) return;
      const document = tronInstance?.Core.documentViewer?.getDocument();
      const page: number = tronInstance && document ? tronInstance.Core.documentViewer.getCurrentPage() - 1 : 0;
      const scale: number = tronInstance && document ? tronInstance.Core.documentViewer.getZoomLevel() : 1;
      canvas.setZoom(scale);
      renderCaptures(page, canvas);

      if (navigation.currentPartStep !== GRID) {
        setCanvasEvents(canvas, mode);
        layerManager.moveLayerToFront('markers');
      }
    }
  });

  const handleAddingCustomFeature = (
    canvas: fabric.Canvas,
    drawingRotation: number,
    drawingScale: number,
    rotation: number,
    sanTop: number,
    sanLeft: number,
    sanWidth: number,
    sanHeight: number,
    left: number,
    top: number,
    width: number,
    styles: {
      [key: string]: MarkerStyle;
    },
    rectId: string,
    currentPage: number,
  ) => {
    const newItem = FeatureUI.createFeature({
      canvas,
      grid: gridRenderer.grid,
      drawingRotation,
      drawingScale,
      boxRotation: rotation,
      parsedFeature: { ...defaultFeature },
      lastItem: characteristics.length > 0 ? characteristics[characteristics.length - 1] : undefined,
      drawingSheetMarkerSize: partDrawing?.sheets[index] ? partDrawing.sheets[index].markerSize : null,
      capture: {
        top: sanTop,
        left: sanLeft,
        width: sanWidth,
        height: sanHeight,
      },
      assignedStyles: styles,
    });
    const customItem = Characteristics.util.inputFromCustom({ ...newItem, drawing: partDrawing, drawingSheet: partDrawingSheet, drawingSheetIndex: partDrawingSheet?.pageIndex || 0 + 1, part });
    dispatch({
      type: 'updateRect',
      payload: {
        color: 'transparent',
        id: generateCaptureId({ left, top, width }),
      },
    });
    // Record result of adding items
    Analytics.track({
      event: Analytics.events.customCaptureReturned,
      part,
      drawing: partDrawing,
      characteristic: customItem,
      properties: {
        'view.sheet': currentPage,
        'view.rotation': newItem?.drawingRotation,
        'view.scale': newItem?.drawingScale,
        'capture.method': newItem?.captureMethod,
        'capture.position.y': newItem?.boxLocationY,
        'capture.position.x': newItem?.boxLocationX,
        'capture.width': newItem?.boxWidth,
        'capture.height': newItem?.boxHeight,
        'capture.rotation': newItem?.boxRotation,
        'marker.label': newItem?.markerLabel,
        'marker.position.y': newItem?.markerLocationY,
        'marker.position.x': newItem?.markerLocationX,
        'char.index': characteristics.length + 1,
        multipleDocuments: part?.drawings && part.drawings.length > 0,
      },
    });
    reduxDispatch({
      type: CharacteristicActions.CREATE_CHARACTERISTICS,
      payload: [customItem],
    });
    timedDispatch('removeRect', { id: rectId });
  };

  useEffect(() => {
    if (state.captureErrorModelOpen && popoverRef.current) {
      popoverRef.current.focus();
    }
  }, [state.captureErrorModelOpen, state.captureErrorInfo]);

  useEffect(() => {
    // Update Fabric Canvas Rendering
    if (navigation.currentPartStep <= 0) return;
    const canvas: fabric.Canvas | null = layerManager.getCurrentCanvasFor('markers');
    if (canvas && layerManager) {
      // why does this get called so much?
      // setCanvasEvents(canvas, mode);
      if (navigation.currentPartStep === GRID) {
        layerManager.moveLayerToFront('grid');
      } else {
        layerManager.moveLayerToFront('markers');
      }
    }
  }, [navigation.currentPartStep]);

  useEffect(() => {
    if (state.movingLock) {
      dispatch({ type: 'movingLock' });
      // return;
    }
    if (!state.mouseDown) {
      const canvas: fabric.Canvas | null = layerManager.getCurrentCanvasFor('markers');
      if (canvas) {
        // set this for updates to balloon styles
        if (navigation.currentPartStep >= STYLES) {
          const document = tronInstance?.Core.documentViewer?.getDocument();
          const page: number = tronInstance && document ? tronInstance.Core.documentViewer.getCurrentPage() - 1 : 0;
          const scale: number = tronInstance && document ? tronInstance.Core.documentViewer.getZoomLevel() : 1;
          canvas.setZoom(scale);
          renderCaptures(page, canvas);
        }
      }
    }
  }, [characteristics, capturedItems, selected]);

  useEffect(() => {
    // Update Fabric Canvas Rendering
    if (navigation.currentPartStep < 0) return;
    const canvas: fabric.Canvas | null = layerManager.getCurrentCanvasFor('markers');
    if (canvas) {
      const document = tronInstance?.Core.documentViewer?.getDocument();
      const page: number = tronInstance && document ? tronInstance.Core.documentViewer.getCurrentPage() : 1;

      renderRectangles(canvas, page);
    }
  }, [state.captureArray]);

  useEffect(() => {
    // using this as a mock queue
    // when a rectangle is added with dispatch('addRect') it triggers this use effect
    // the first item in the captureArray is found and OCR is ran on it if it hasn't already been done.
    // when an item is done with OCR and the rectangle is removed the next item in the array goes through OCR
    if (state.captureLock) return;
    const item = state.captureArray.find((i: any) => i.addItem);
    if (item) {
      const itemParams = item.params;
      dispatch({ type: 'updateRect', payload: { id: generateCaptureId(item.rect), captureLock: true } });
      addItem(itemParams.canvas, itemParams.dims, itemParams.rotation, itemParams.sanDims, itemParams.tally, itemParams.event, itemParams.page, item.id);
    }
  }, [state.captureArray.length]);

  const rerender = () => {
    const canvas: fabric.Canvas | null = layerManager.getCurrentCanvasFor('markers');
    if (canvas) {
      setCanvasEvents(canvas, mode);
      // set this for updates to balloon styles
      if (navigation.currentPartStep >= STYLES) {
        const document = tronInstance?.Core.documentViewer?.getDocument();
        const page: number = tronInstance && document ? tronInstance.Core.documentViewer.getCurrentPage() : 1;
        renderCaptures(page - 1, canvas, false);
      }
    }
  };
  useEffect(() => {
    rerender();
  }, [hideFeatures, part?.displayLeaderLines, mode]);

  useEffect(() => {
    const sheets1 = part?.drawings?.map((drawing) => drawing.sheets).flat() ?? [];
    const autoballoonValues = sheets1.reduce(getAutoBalloonResults, {
      found: 0,
      accepted: 0,
      acceptedTimeout: 0,
      rejected: 0,
      reviewed: 0,
      reviewedTimeout: 0,
      createdAt: part?.createdAt,
    });

    if (!autoballooningDone.current && isAutoballooningDone(autoballoonValues, part)) {
      autoballooningDone.current = true;
      rerender();
    }
  }, [part]);

  /**
   * Processes a queue of OCR'd characteristics by removing the first characteristic from the queue and updating the characteristics Redux state.
   * If the queue is empty or if processing is already ongoing, the function does nothing.
   */
  const processCharacteristicsQueue = () => {
    if (!processingCharacteristicsQueueRef.current && characteristicsQueueRef.current.length > 0) {
      processingCharacteristicsQueueRef.current = true;
      const characteristic = characteristicsQueueRef.current.shift();
      if (characteristic?.id) {
        const characteristics = characteristicsRef.current;
        dispatch({ type: 'removeRect', payload: { id: characteristic.id } });
        const existingCharacteristics = characteristics.filter((c) => c.id !== characteristic.id);
        const characteristicsToUpdate = [...existingCharacteristics, characteristic];
        const orderedCharacteristics = orderBy(characteristicsToUpdate, 'markerIndex');
        characteristicsRef.current = characteristicsToUpdate;

        const newCapturedItem = getCapturedItem(characteristic, defaultMarkerRef.current ? defaultMarkerRef.current : undefined);
        const capturedItemsCopy = capturedItemsRef.current;
        const index = characteristic.drawingSheetIndex - 1;

        capturedItemsCopy[index] = capturedItemsCopy[index].filter((c) => c.id !== characteristic.id).concat(newCapturedItem);
        reduxDispatch({ type: CharacteristicActions.UPDATE_SELECTED, payload: { selected: [characteristic], capturedItems: capturedItemsCopy, characteristics: orderedCharacteristics } });
      }
      processingCharacteristicsQueueRef.current = false;
      processCharacteristicsQueue();
    }
  };

  const characteristicUpdateHandler = (characteristic: Characteristic) => {
    characteristicsQueueRef.current.push(characteristic);
    processCharacteristicsQueue();
  };

  useEffect(() => {
    if (settings.autoballoonEnabled?.value === 'true' && config.wsUrl) {
      // Update parts list when parts or drawingSheets update so we can see autoballoon progress
      const socket = io(config.wsUrl, { transports: ['websocket'], withCredentials: true, auth: { token: `Bearer ${jwt}` }, path: config.wsPath });
      socket.on('characteristic.update', characteristicUpdateHandler);
      setSocket(socket);

      return () => {
        socket.off('characteristic.update', characteristicUpdateHandler);
        socket.disconnect();
      };
    }
    return () => {};
  }, []);

  useEffect(() => {
    characteristicsRef.current = characteristics;
  }, [characteristics]);

  useEffect(() => {
    capturedItemsRef.current = capturedItems;
  }, [capturedItems]);

  useEffect(() => {
    defaultMarkerRef.current = defaultMarker;
  }, [defaultMarker]);

  return <div style={{ position: 'absolute', left: '435px', top: '10px', zIndex: 1000 }} />;
};
