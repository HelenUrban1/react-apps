import { fabric } from 'fabric';
import { BrandColors } from 'view/global/defaults';
import log from 'modules/shared/logger';
import { Marker, MarkerStrokeStyleEnum } from 'types/marker';
import { Characteristic, CapturedItem } from 'types/characteristics';
import { FeatureUI } from 'utils/ParseTextController/UI';
import { Annotation } from 'types/annotation';
import { getCartesianFromPolar, getSanitizedDimensions, coordRotation, getTransformedDimensions, updateBalloonPosition } from './utils/coordTransforms';

export const getStrokeStyle = (style: MarkerStrokeStyleEnum) => {
  let lineArr = [];
  switch (style) {
    case 'Dashed':
      lineArr = [5, 2];
      break;
    case 'Dotted':
      lineArr = [2, 2];
      break;
    default:
      lineArr = [5, 0];
      break;
  }
  return lineArr;
};

// Factory method that returns a point array representing the corners of a polygon
export const regularPolygonPoints = (sideCount: number, radius: number) => {
  const sweep = (Math.PI * 2) / sideCount;
  const cx = radius;
  const cy = radius;
  const points = [];
  for (let i = 0; i < sideCount; i++) {
    const x = cx + radius * Math.cos(i * sweep);
    const y = cy * 1.2 + radius * Math.sin(i * sweep);
    points.push({ x, y });
  }
  return points;
};

// Factory method that returns a Hexagon  as a fabric.Polygon
export const getHexagon = (x = 0, y = 0, fill = BrandColors.Coral, stroke = BrandColors.Coral, strokeWidth = 1.5, radius = 0, strokeDashArray = [5, 0]) => {
  const points = regularPolygonPoints(6, radius);
  const fabHex = new fabric.Polygon(points, {
    left: x,
    top: y,
    fill,
    stroke,
    strokeWidth,
    strokeDashArray,
    originX: 'center',
    originY: 'center',
  });
  fabHex.type = 'hexagon';

  return fabHex;
};

// Factory method that returns a Pentagon  as a fabric.Polygon
export const getPentagon = (x = 0, y = 0, fill = BrandColors.Coral, stroke = BrandColors.Coral, strokeWidth = 1.5, radius = 0, strokeDashArray = [5, 0]) => {
  const points = regularPolygonPoints(5, radius);
  const fabPent = new fabric.Polygon(points, {
    left: x,
    top: y,
    fill,
    stroke,
    strokeWidth,
    strokeDashArray,
    angle: -17.5, // regularPolygonPoints creates a slightly rotated pentagon
    originX: 'center',
    originY: 'center',
  });
  fabPent.type = 'pentagon';

  return fabPent;
};

// Factory method that returns a Square as a fabric.Rect
export const getSquare = (x = 0, y = 0, fill = BrandColors.Coral, stroke = BrandColors.Coral, strokeWidth = 1.5, height = 0, strokeDashArray = [5, 0]) => {
  const fabSquare = new fabric.Rect({
    left: x,
    top: y,
    fill,
    stroke,
    strokeWidth,
    strokeDashArray,
    width: height,
    height,
    originX: 'center',
    originY: 'center',
  });
  fabSquare.type = 'square';

  return fabSquare;
};

// Factory method that returns a Diamond as a fabric.Rect
export const getDiamond = (x = 0, y = 0, fill = BrandColors.Coral, stroke = BrandColors.Coral, strokeWidth = 1.5, height = 0, strokeDashArray = [5, 0]) => {
  const fabDia = new fabric.Rect({
    left: x,
    top: y,
    fill,
    stroke,
    strokeWidth,
    strokeDashArray,
    angle: 45,
    width: Math.sqrt((height * height) / 2),
    height: Math.sqrt((height * height) / 2),
    originX: 'center',
    originY: 'center',
  });
  fabDia.type = 'diamond';

  return fabDia;
};

export const getLine = (x1: number, y1: number, x2: number, y2: number, fill: string | undefined, stroke: string | undefined, strokeWidth: number | undefined, strokeDashArray: number[]) => {
  const fabLine = new fabric.Line([x1, y1, x2, y2], {
    fill,
    stroke,
    strokeWidth,
    strokeDashArray,
    selectable: false,
    evented: false,
    lockMovementX: true,
    lockMovementY: true,
    lockScalingX: true,
    lockScalingY: true,
    lockRotation: true,
    lockScalingFlip: true,
    hasControls: false,
    hasBorders: false,
  });
  fabLine.type = 'line';

  return fabLine;
};

export const getConnectionPoint = (x2: number, y2: number, fill: string | undefined) => {
  const connectionPoint = new fabric.Circle({
    fill,
    top: y2,
    left: x2,
    radius: 5,
    strokeWidth: 25,
    originX: 'center',
    originY: 'center',
    lockScalingX: true,
    lockScalingY: true,
    lockRotation: true,
    lockScalingFlip: true,
    hasControls: false,
    hasBorders: false,
  });
  connectionPoint.type = 'connectionPoint';
  return connectionPoint;
};

// Factory method that returns a Triangle as a fabric.Triangle
export const getTriangle = (x = 0, y = 0, fill = BrandColors.Coral, stroke = BrandColors.Coral, strokeWidth = 1.5, width = 0, height = 0, strokeDashArray = [5, 0]) => {
  const fabTri = new fabric.Triangle({
    left: x,
    top: y - height * 0.2,
    fill,
    stroke,
    strokeWidth,
    strokeDashArray,
    width,
    height,
    originX: 'center',
    originY: 'center',
  });
  fabTri.type = 'triangle';

  return fabTri;
};

// Factory method that returns a Rectangle as a fabric.Rect
export const getRect = (x = 0, y = 0, fill = 'transparent', stroke = BrandColors.SunshineP1, strokeWidth = 1.5, width = 0, height = 0, angle = 0, type: string, strokeDashArray?: Array<number>) => {
  interface IRectWithId extends fabric.IRectOptions {
    isRectangle: boolean;
  }
  const opt = {
    left: x + width / 2,
    top: y + height / 2,
    fill,
    stroke,
    strokeWidth,
    strokeDashArray,
    width,
    height,
    angle,
    type,
    originX: 'center',
    originY: 'center',
    cornerSize: 8,
    isRectangle: type === 'extract',
  } as IRectWithId;
  const fabRect = new fabric.Rect(opt);

  return fabRect;
};

// Factory method that returns a Circle as a fabric.Circle
export const getCircle = (x: number, y: number, fill = BrandColors.Coral, stroke = BrandColors.Coral, strokeWidth = 1.5, radius = 25, strokeDashArray = [5, 0]) => {
  const fabCirc = new fabric.Circle({
    left: x,
    top: y,
    fill,
    stroke,
    strokeWidth,
    strokeDashArray,
    radius,
    originX: 'center',
    originY: 'center',
  });
  fabCirc.type = 'circle';

  return fabCirc;
};

// Factory method that returns a Text object as a fabric.Text
export const getText = (text: string, fontSize: number, top: number, left: number, fill = BrandColors.White, lineHeight: number, textAlign: string) => {
  const fabText = new fabric.Text(text, {
    fontSize,
    top,
    left,
    fill,
    lineHeight,
    textAlign: 'center',
    originX: 'center',
    originY: 'center',
    fontFamily: 'Helvetica, Arial',
  });
  fabText.lockRotation = true;

  return fabText;
};

// Factory method that returns a Region Selection as a fabric.Rect
export const getRegion = (x: number, y: number, width: number, height: number, angle = 0) => {
  const rect = getRect(x, y, 'rgb(27, 98, 233, 0.25)', 'rgb(27, 98, 233, 0.25)', 0, width, height, angle, 'region');
  rect.hasControls = false;
  rect.hasBorders = false;
  rect.originX = 'center';
  rect.originY = 'center';
  return {
    itemNumber: null,
    rectangle: rect,
    balloon: null,
    leaderLine: null,
    connectionPoint: null,
  };
};

export const getAnnotationGroup = (annotation: Annotation) => {
  const rect = getRect(0, 0, annotation.backgroundColor, annotation.borderColor, 1, annotation.boxWidth, annotation.boxHeight, annotation.boxRotation, 'annotation-bg');
  rect.originX = 'center';
  rect.originY = 'center';
  rect.selectable = false;
  rect.hasControls = false;
  rect.objectCaching = false;
  rect.name = annotation.id;

  const text = new fabric.Textbox(annotation.annotation.replace(/<NEWLINE>/g, '\n').replace(/<WRAP>/g, ' '), {
    fontFamily: 'InspectionXpert GDT',
    fontSize: annotation.fontSize,
    fill: annotation.fontColor,
    left: 4,
    top: 4,
    textAlign: 'left',
    objectCaching: false,
    hasBorders: false,
    name: annotation.id,
    type: 'annotation-text',
    width: annotation.drawingRotation % 180 === 0 ? annotation.boxWidth - 4 : annotation.boxHeight - 4,
  });
  text.toObject = () => annotation;

  const annotationGroup = new fabric.Group([rect, text]);
  annotationGroup.originX = 'center';
  annotationGroup.originY = 'center';
  annotationGroup.selectable = true;
  annotationGroup.type = 'annotation';
  annotationGroup.hasControls = true;
  annotationGroup.hasBorders = true;
  annotationGroup.lockRotation = true;
  annotationGroup.objectCaching = false;
  annotationGroup.name = annotation.id;
  text.bringToFront();

  annotationGroup.toObject = () => annotation;

  return annotationGroup;
};

// Factory method that returns a Region Selection as a fabric.Rect
export const getNewRect = (x: number, y: number, width: number, height: number, angle = 0) => {
  const rect = getRect(x, y, 'transparent', BrandColors.Azure, 2, width, height, angle, 'region', [5, 2]);
  rect.hasControls = false;
  rect.hasBorders = false;
  rect.originX = 'center';
  rect.originY = 'center';
  return rect;
};

// Font size scaling based on the length of a balloon label
// Shrink the text 20% for each character over 2
export const getFontSizeModifier = (label: string) => {
  if (!label || label.length <= 2) {
    return 1;
  }
  if (label.length >= 7) {
    return 0.2;
  }
  return 1 - 0.2 * (label.length - 2);
};

export const getBalloon = (shape: string, markerX: number, markerY: number, fill: string, border: string, strokeWidth: number, balloonSize: number, strokeStyle: MarkerStrokeStyleEnum) => {
  // Build the balloon shape
  const borderColor = BrandColors[border] || border;
  const fillColor = BrandColors[fill] || fill;
  const strokeDashArray = getStrokeStyle(strokeStyle);

  switch (shape) {
    case 'Triangle':
      return getTriangle(markerX, markerY, fillColor, borderColor, strokeWidth, balloonSize, balloonSize, strokeDashArray);
    case 'Square':
      return getSquare(markerX, markerY, fillColor, borderColor, strokeWidth, balloonSize, strokeDashArray);
    case 'Diamond':
      return getDiamond(markerX, markerY, fillColor, borderColor, strokeWidth, balloonSize, strokeDashArray);
    case 'Pentagon':
      return getPentagon(markerX, markerY, fillColor, borderColor, strokeWidth, balloonSize / 2, strokeDashArray);
    case 'Hexagon':
      return getHexagon(markerX, markerY, fillColor, borderColor, strokeWidth, balloonSize / 2, strokeDashArray);
    default:
      return getCircle(markerX, markerY, fillColor, borderColor, strokeWidth, balloonSize / 2, strokeDashArray);
  }
};

export const getBalloonGroup = (balloon: fabric.Object, text: fabric.Text, visible: boolean) => {
  const balloonGroup = new fabric.Group([balloon]);
  balloonGroup.addWithUpdate(text);

  // Set Balloon properties and behaviors
  balloonGroup.lockScalingX = true;
  balloonGroup.lockScalingY = true;
  balloonGroup.hasRotatingPoint = false;
  balloonGroup.hasControls = false;
  balloonGroup.selectable = true;
  balloonGroup.lockMovementX = false;
  balloonGroup.lockMovementY = false;
  balloonGroup.set({
    cornerColor: 'black',
    cornerSize: 0,
    transparentCorners: true,
  });
  balloonGroup.type = 'balloon';
  balloonGroup.originX = 'center';
  balloonGroup.originY = 'center';
  balloonGroup.visible = visible;

  // Make sure the text floats above the balloon
  text.bringToFront();

  return balloonGroup;
};

// Factory method that creates amd returns a set of Fabric objects that represent a CapturedItem (temNumber, rectangle, balloon)
export const getExtracted = (
  x: number,
  y: number,
  width: number,
  height: number,
  angle: number,
  shape: string,
  markerX: number,
  markerY: number,
  markerRotation: number,
  markerLabel: string,
  fill: string,
  border: string,
  borderWidth: number,
  stroke: string,
  textColor: string,
  fontSize: number,
  markerSize: number,
  shared: boolean,
  rectangleStrokeWidth: number,
  strokeStyle: MarkerStrokeStyleEnum,
  markerStyle: Marker | undefined,
) => {
  // Calculate the coordinates of the marker relative to the box
  const markerPolar = { x: markerX, y: markerY };
  const boxCenter = { x /* (x + (width / 2)) */, y /* (y + (height / 2)) */ };
  const markerCartesian = getCartesianFromPolar(boxCenter, markerPolar, markerRotation);
  const strokeDashArray = getStrokeStyle(strokeStyle);
  const strokeWidth = fontSize / 10;
  // Build the rectangle
  const rect = getRect(x, y, 'transparent', BrandColors[stroke], rectangleStrokeWidth || strokeWidth, width, height, angle, 'extract');
  rect.hasControls = true;
  rect.hasBorders = true;
  rect.lockScalingX = false;
  rect.lockScalingY = false;
  rect.lockRotation = false;
  rect.selectable = true;

  // Build the balloon shape
  const balloon = getBalloon(shape, markerCartesian.x, markerCartesian.y, BrandColors[fill] || fill, BrandColors[border] || border, borderWidth, markerSize, strokeStyle);

  // Determine if the capture is visible
  let visible = true;
  if (shared && markerLabel.split('.')[1] !== '1') visible = false;

  // Calculate and add the text label to the balloon shape
  const label = shared && visible ? markerLabel.split('.')[0] : markerLabel;
  const fontSizeModifier = getFontSizeModifier(label);

  const text = getText(label, fontSize * fontSizeModifier, markerCartesian.y, markerCartesian.x, BrandColors[textColor] || textColor, markerSize, 'center');
  const balloonGroup = getBalloonGroup(balloon, text, visible);

  // default left
  const leaderLine = getLine(markerCartesian.x + boxCenter.x / 2, markerCartesian.y + boxCenter.y / 2, boxCenter.x, boxCenter.y / 2, markerStyle?.leaderLineColor, markerStyle?.leaderLineColor, markerStyle?.leaderLineWidth, strokeDashArray);
  const connectionPoint = getConnectionPoint(boxCenter.x, boxCenter.y / 2, markerStyle?.leaderLineColor);

  // Return the captured item
  const extracted = {
    itemNumber: markerLabel,
    rectangle: rect,
    balloon: balloonGroup,
    leaderLine,
    connectionPoint,
  };

  return extracted;
};

// Helper function that performs the scaling math when a bounding box is resized using the control points in the center of the top or bottom edges
// Control variable = Middle Right, Middle Left
export const handleScaleX = (startX: number, startY: number, startWidth: number, startHeight: number, scaleX: number, rotation: number, control: string) => {
  let newX = startX;
  let newWidth = startWidth;
  let newY = startY;
  let newHeight = startHeight;

  switch (rotation) {
    case 90:
      newHeight *= scaleX;
      if (control === 'mr') {
        // scaled the right side which is the top
        newY -= newHeight - startHeight;
      }
      break;
    case 180:
      newWidth *= scaleX;
      if (control === 'mr') {
        // scaled the right side which is the left
        newX -= newWidth - startWidth;
      }
      break;
    case 270:
      newHeight *= scaleX;
      if (control === 'ml') {
        // scaled the left side which is the top
        newY -= newHeight - startHeight;
      }
      break;
    default:
      newWidth *= scaleX;
      if (control === 'ml') {
        // scaled the left side which is the left
        newX -= newWidth - startWidth;
      }
      break;
  }

  return { newX, newWidth, newY, newHeight };
};

// Helper function that performs the scaling math when a bounding box is resized using the control points in the center of the left or right edges
// Control variable = Middle Top, Middle Bottom
export const handleScaleY = (startX: number, startY: number, startWidth: number, startHeight: number, scaleY: number, rotation: number, control: string) => {
  let newX = startX;
  let newWidth = startWidth;
  let newY = startY;
  let newHeight = startHeight;

  switch (rotation) {
    case 90:
      newWidth *= scaleY;
      if (control === 'mt') {
        // scaled the top side which is the left
        newX -= newWidth - startWidth;
      }
      break;
    case 180:
      newHeight *= scaleY;
      if (control === 'mb') {
        // scaled the bottom side which is the top
        newY -= newHeight - startHeight;
      }
      break;
    case 270:
      newWidth *= scaleY;
      if (control === 'mb') {
        // scaled the bottom side which is the left
        newX -= newWidth - startWidth;
      }
      break;
    default:
      newHeight *= scaleY;
      if (control === 'mt') {
        // scaled the top side which is the tp
        newY -= newHeight - startHeight;
      }
      break;
  }

  return { newX, newWidth, newY, newHeight };
};

// Helper function that performs the scaling math when a bounding box is resized using the control points on the corners
// Control variable = Top, Right, Bottom, Left
export const handleScaleCorner = (startX: number, startY: number, startWidth: number, startHeight: number, scaleY: number, scaleX: number, rotation: number, control: string) => {
  let newX = startX;
  let newWidth = startWidth;
  let newY = startY;
  let newHeight = startHeight;

  switch (rotation) {
    case 90:
      newWidth *= scaleY;
      newHeight *= scaleX;
      if (control.charAt(0) === 't') {
        // scaled a top corner, adjust the left
        newX -= newWidth - startWidth;
      }
      if (control.charAt(1) === 'r') {
        // scaled a right corner, adjust the top
        newY -= newHeight - startHeight;
      }
      break;
    case 180:
      newWidth *= scaleX;
      newHeight *= scaleY;
      if (control.charAt(0) === 'b') {
        // scaled a bottom corner, adjust the top
        newY -= newHeight - startHeight;
      }
      if (control.charAt(1) === 'r') {
        // scaled a right corner, adjust the left
        newX -= newWidth - startWidth;
      }
      break;
    case 270:
      newWidth *= scaleY;
      newHeight *= scaleX;
      if (control.charAt(0) === 'b') {
        // scaled a bottom corner, adjust the left
        newX -= newWidth - startWidth;
      }
      if (control.charAt(1) === 'l') {
        // scaled a left corner, adjust the top
        newY -= newHeight - startHeight;
      }
      break;
    default:
      newWidth *= scaleX;
      newHeight *= scaleY;
      if (control.charAt(0) === 't') {
        // scaled a top corner, adjust the top
        newY -= newHeight - startHeight;
      }
      if (control.charAt(1) === 'l') {
        // scaled a left corner, adjust the left
        newX -= newWidth - startWidth;
      }
      break;
  }

  return { newX, newWidth, newY, newHeight };
};

// Top level entry point for Canvas's object modified event
export const modifyObject = (e: any, obj: fabric.Object, itemGroup: CapturedItem, rotation: number, scale: number, pageWidth: number, pageHeight: number, canvas?: fabric.Canvas) => {
  // log.debug('MODIFY TARGET', e.target);
  // log.debug('MODIFY TRANSFORM', e.transform);
  // Get the characteristic from the closure that was created during fabric object creation
  if ((!itemGroup?.rectangle || !itemGroup?.balloon) && !itemGroup?.annotation) {
    log.error('Captured item was missing rectangle or balloon or annotation');
    return null;
  }
  let newItem = itemGroup.annotation ? { ...itemGroup.annotation.toObject() } : { ...itemGroup.rectangle?.toObject() };

  if (e.target.type === 'extract' || e.target.type === 'annotation-text' || e.target.type === 'annotation') {
    // A capture box was modified
    // QUESTION: When is the transform action undefined?
    // TODO: Handle error here
    if (!e.transform || !e.transform.action) {
      log.warn(`charactersiticsRenderer.modifyObject > e.transform.action was undefined.`);
      return null;
    }
    // Initialize a new object, starting with the object's current properties (prior to the event)
    let newDims = {
      newX: newItem.boxLocationX,
      newY: newItem.boxLocationY,
      newWidth: newItem.boxWidth,
      newHeight: newItem.boxHeight,
      newRotation: newItem.boxRotation,
    };

    let newCoords;
    let newLeft;
    let newTop;
    let newWidth;
    let newHeight;

    // Perform the action
    switch (e.transform.action) {
      case 'drag':
        // handle drag,account for origin center
        newLeft = e.target.left - e.target.width / 2;
        newTop = e.target.top - e.target.height / 2;
        newWidth = e.target.width;
        newHeight = e.target.height;
        newCoords = getSanitizedDimensions(newLeft, newTop, newLeft + newWidth, newTop + newHeight, pageWidth, pageHeight, rotation, 1);
        newDims = { ...newDims, newX: newCoords.left, newY: newCoords.top };
        break;
      case 'scale':
        // handle scale corner
        newDims = {
          ...newDims,
          ...handleScaleCorner(newItem.boxLocationX, newItem.boxLocationY, newItem.boxWidth, newItem.boxHeight, e.target.scaleX, e.target.scaleY, rotation, e.transform.corner),
        };
        break;
      case 'scaleX':
        // handle scale width
        newDims = {
          ...newDims,
          ...handleScaleX(newItem.boxLocationX, newItem.boxLocationY, newItem.boxWidth, newItem.boxHeight, e.target.scaleX, rotation, e.transform.corner),
        };
        break;
      case 'scaleY':
        // handle scale height
        newDims = {
          ...newDims,
          ...handleScaleY(newItem.boxLocationX, newItem.boxLocationY, newItem.boxWidth, newItem.boxHeight, e.target.scaleY, rotation, e.transform.corner),
        };
        break;
      case 'rotate':
        // handle rotate
        newDims.newRotation = e.target.angle;
        break;
      default:
        break;
    }
    // Update the characteristic with the new properties
    newItem = {
      ...newItem,
      boxLocationX: newDims.newX,
      boxLocationY: newDims.newY,
      boxWidth: newDims.newWidth,
      boxHeight: newDims.newHeight,
      boxRotation: newDims.newRotation,
    };

    // The balloon needs to be kept at the same position
    const newPoint = { x: itemGroup.balloon?.left || 0, y: itemGroup.balloon?.top || 0 };
    const balloon = updateBalloonPosition(newItem, newPoint, rotation, pageWidth, pageHeight);
    newItem.markerLocationX = balloon.x;
    newItem.markerLocationY = (balloon.y + newItem.markerRotation) % 360;
  } else if (e.target.type === 'balloon') {
    // A balloon was moved to a new position
    const newPoint = { x: e.target.left, y: e.target.top };
    const balloon = updateBalloonPosition(newItem, newPoint, rotation, pageWidth, pageHeight);
    newItem.markerLocationX = balloon.x;
    newItem.markerLocationY = (balloon.y + newItem.markerRotation) % 360;

    if (!newItem.connectionPointIsFloating && canvas && itemGroup.rectangle?.left && itemGroup.rectangle?.top && itemGroup.rectangle?.width && itemGroup.rectangle?.height) {
      // set the connectionPoint location to be the closest side. This is to update the balloonedPdf
      // for the distance to be accurately calculated we need to rotate the rect
      const distancex1 = newPoint.x;
      const distancey1 = newPoint.y;
      const { newLeft, newTop, newWidth, newHeight } = getTransformedDimensions(itemGroup.rectangle.left, itemGroup.rectangle.top, itemGroup.rectangle.width, itemGroup.rectangle.height, canvas.getWidth(), canvas.getHeight(), scale, rotation);
      const newCoords = FeatureUI.shortestDistanceX2Y2({ x1: distancex1, y1: distancey1, top: newTop, left: newLeft, width: newWidth, height: newHeight, rotation });

      if (newCoords) {
        newItem.connectionPointLocationX = newCoords?.x2;
        newItem.connectionPointLocationY = newCoords?.y2;
        newItem.leaderLineDistance = newCoords?.distance;
      }
    }
  } else if (e.target.type === 'connectionPoint') {
    // A balloon was moved to a new position
    const newConnectionPoint = { x: e.target.left, y: e.target.top };
    const rotatedPoint = coordRotation(newConnectionPoint.x, newConnectionPoint.y, 360 - rotation, pageWidth, pageHeight);
    newItem.connectionPointLocationX = rotatedPoint.newX;
    newItem.connectionPointLocationY = rotatedPoint.newY;

    let intersects = false;
    // for some reason getting just the active object doesn't work here and you have to loop through the items.
    // NOT IDEAL
    if (canvas) {
      canvas.forEachObject((object) => {
        if (e.target && object === e.target) return;
        if (e.target.name === object.name && object.type === 'extract' && e.target.intersectsWithObject(object)) {
          intersects = true;
        }
      });
    }
    if (intersects) {
      newItem.connectionPointIsFloating = false;
    } else {
      newItem.connectionPointIsFloating = true;
    }
  } else {
    log.warn('Unexpected result in characteristicsRenderer.modifyObject. No if statement resolved to true.');
  }

  log.debug('modifyObject return', newItem.id, newItem.verified);
  return newItem;
};

// Method that collects the Fabric object's associated with the provided characteristic
export const getCapturedItem = (characteristic: Characteristic, markerStyle: undefined | Marker) => {
  // Calculate style properties
  // TODO: This should be pulled from the partEditor context which calculates value from account, site, user, and part preferences
  const fill = markerStyle?.fillColor || 'Coral';
  const border = markerStyle?.borderColor || 'Coral';
  const borderWidth = markerStyle?.borderWidth || 1;
  const borderStyle = markerStyle?.borderStyle || 'Solid';
  const font = markerStyle?.fontColor || 'white';
  const shape = markerStyle?.shape || 'circle';
  let stroke = 'SunshineP1';
  let strokeWidth = 0;
  // Prepare a value object to return that represents the CapturedItem
  const item: CapturedItem = {
    id: characteristic.id,
    itemNumber: characteristic.markerLabel,
    rectangle: null,
    balloon: null,
    leaderLine: null,
    connectionPoint: null,
    annotation: null,
  };

  // Retrieve a reference to the fabric object that corresponds to the characteristic
  let obj;
  if (characteristic.captureMethod === 'Automated' && characteristic.status === 'Inactive') {
    // When the provided characteristic is from auto-ballooning and hasn't been converted by the user
    obj = getRegion(characteristic.boxLocationX, characteristic.boxLocationY, characteristic.boxWidth, characteristic.boxHeight);
  } else {
    if (characteristic.verified) {
      stroke = 'SeafoamP1';
    }
    if (characteristic.captureError) {
      stroke = 'Coral';
      strokeWidth = 5;
    }
    // Everything else
    obj = getExtracted(
      characteristic.boxLocationX, //
      characteristic.boxLocationY,
      characteristic.boxWidth,
      characteristic.boxHeight,
      characteristic.boxRotation,
      shape,
      characteristic.markerLocationX,
      characteristic.markerLocationY,
      characteristic.markerRotation,
      characteristic.markerLabel,
      fill,
      border,
      borderWidth,
      stroke,
      font,
      characteristic.markerFontSize,
      characteristic.markerSize,
      characteristic.markerGroupShared,
      strokeWidth,
      borderStyle,
      markerStyle,
    );
  }

  // Update the value object that is going to be returned with object's properties
  item.rectangle = obj.rectangle;
  item.rectangle.selectable = true;
  item.rectangle.name = characteristic.id;

  // Override Fabric's serialization method to return the characteristic.
  item.rectangle.toObject = () => {
    return characteristic;
  };

  // If the item has a balloon, set the value object's balloon name to the characteristic.id
  item.balloon = obj.balloon;
  if (item.balloon) {
    item.balloon.name = characteristic.id;
    // Override Fabric's serialization method to return the characteristic.
    item.balloon.toObject = () => {
      return characteristic;
    };
  }

  // Update the value object that is going to be returned with object's properties
  item.leaderLine = obj.leaderLine;
  if (item.leaderLine) {
    item.leaderLine.name = characteristic.id;
    // Override Fabric's serialization method to return the characteristic.
    item.leaderLine.toObject = () => {
      return characteristic;
    };
  }

  // Update the value object that is going to be returned with object's properties
  item.connectionPoint = obj.connectionPoint;
  if (item.connectionPoint) {
    item.connectionPoint.name = characteristic.id;
    // Override Fabric's serialization method to return the characteristic.
    item.connectionPoint.toObject = () => {
      return characteristic;
    };
  }

  return item;
};

// Method that collects the Fabric object's associated with the provided characteristic
export const getAnnotationObject = (annotation: Annotation, characteristic?: Characteristic, markerStyle?: Marker) => {
  // Prepare a value object to return that represents the CapturedItem
  const item: CapturedItem = {
    id: annotation.id,
    itemNumber: characteristic?.markerLabel || '',
    rectangle: null,
    balloon: null,
    leaderLine: null,
    connectionPoint: null,
    annotation: null,
  };

  // Update the value object that is going to be returned with object's properties
  item.annotation = getAnnotationGroup(annotation);
  item.annotation.name = annotation.id;

  // Override Fabric's serialization method to return the characteristic.
  item.annotation!.toObject = () => {
    return annotation;
  };

  return item;
};
