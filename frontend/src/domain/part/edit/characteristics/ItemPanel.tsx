import React, { useState, ChangeEvent, useEffect, useRef } from 'react';
import ItemList from 'styleguide/ItemList/ItemList';
import IxButton from 'styleguide/Buttons/IxButton';
import { CheckCircleOutlined, RightCircleOutlined } from '@ant-design/icons';
import { i18n } from 'i18n';
import { Characteristic } from 'types/characteristics';
import { Characteristics } from 'graphql/characteristics';
import { Marker } from 'types/marker';
import { PartStepEnum } from 'modules/navigation/navigationActions';
import { svgMaker } from 'utils/SvgMaker';
import { getFramedText, getUnframedText, isFramed } from 'utils/textOperations';
import { useMutation } from '@apollo/client';
import { Popconfirm } from 'antd';
import { ProgressFooter } from 'styleguide/ProgressionFooter';
import Analytics from 'modules/shared/analytics/analytics';
import { SpecialCharactersModal } from 'styleguide/Modals/SpecialCharactersModal';
import { CaptureDimensions } from 'utils/ParseTextController/types';
import { ParseTextController } from 'utils/ParseTextController/ParseTextController';
import { isEqual } from 'lodash';
import { AppState } from 'modules/state';
import { updatePartThunk } from 'modules/part/partActions';
import { useDispatch, useSelector } from 'react-redux';
import { CharacteristicActions } from 'modules/characteristic/characteristicActions';
import './itemPanelStyles.less';
import { changeDrawingThunk } from 'modules/partEditor/partEditorActions';
import { Annotation } from 'types/annotation';
import { ItemPanelHeader } from 'styleguide/ItemPanelHeader/ItemPanelHeader';
import { deleteAnnotationThunk } from 'modules/annotation/annotationActions';
import { dragItem, reorderController } from './utils/Renumber';
import { expandCharacteristic, expandFeatureGroup } from './utils/Expand';

const { EXTRACT, REVIEW } = PartStepEnum;

interface Props {
  activeIDs?: string[];
  next: () => void;
  back: () => void;
  collapsed?: boolean;
  usedStyles?: Marker[];
  defaultMarker?: string;
  generateReport?: Function;
  review?: boolean;
  currentStep: number;
}

export const ItemPanel: React.FC<Props> = ({
  //
  next,
  back,
  collapsed = false,
  review = false,
  currentStep,
  generateReport,
}) => {
  const [drop, setDrop] = useState<boolean>(false);
  const listRef = useRef<any>();
  // Connect to the document context
  // Part Context
  const { partDrawing, assignedStyles, gridRenderer, tolerances } = useSelector((state: AppState) => state.partEditor);
  const { part } = useSelector((state: AppState) => state.part);
  const { characteristics, markers, selected, index, parsed } = useSelector((state: AppState) => state.characteristics);
  const { types } = useSelector((state: AppState) => state.session);
  const navigation = useSelector((state: AppState) => state.navigation);
  const [verified, setVerified] = useState(true);
  const [editItem] = useMutation<Characteristic>(Characteristics.mutate.edit);
  const { tronInstance } = useSelector((state: AppState) => state.pdfView);
  const [symbolModal, setSymbolModal] = useState<{ visible: boolean; feature: Characteristic | null; cursor: number; value: string | undefined }>({ visible: false, feature: null, cursor: 0, value: undefined });
  const { settings } = useSelector((state: AppState) => state.session);

  const dispatch = useDispatch();

  const [newMarkers, setMarkers] = useState([
    {
      id: 'f12c5566-7612-48e9-9534-ef90b276ebaa',
      fillColor: 'Coral',
      borderColor: 'Coral',
      shape: 'Circle',
      svg: svgMaker('Coral', 'Coral', 1, 'Circle'),
    },
  ]);
  const defaultMarker = useRef('f12c5566-7612-48e9-9534-ef90b276ebaa');
  useEffect(() => {
    if (markers && markers.length > 0) {
      defaultMarker.current = markers[0].id;
      const temp: { id: string; svg: JSX.Element; fillColor: string; borderColor: string; shape: string }[] = [];
      for (let i = 0; i < markers.length; i++) {
        const marker = markers[i];
        if (marker.id === assignedStyles?.default?.style) {
          defaultMarker.current = marker.id;
        }
        const fillColor = marker.fillColor === 'White' ? 'Transparent' : marker.fillColor;
        temp.push({
          id: marker.id,
          fillColor,
          borderColor: 'Coral',
          shape: 'Circle',
          svg: svgMaker(fillColor, marker.borderColor, marker.borderWidth, marker.shape, marker.borderStyle),
        });
      }
      setMarkers(temp);
    }
  }, [markers]);

  const usePrevious = (value: any) => {
    const ref = useRef();
    useEffect(() => {
      ref.current = value;
    });
    return ref.current;
  };

  const prevCharacteristics = usePrevious(characteristics) || [];

  // only runs when the item list length changes
  useEffect(() => {
    // built in react-window function to scroll to an item
    // this will fire when prevCharacteristics and characteristics are equal.
    // this happens only when the component gets fully updated from an extraction. If you delete/group prevCharacteristics is different
    if (listRef.current && prevCharacteristics && prevCharacteristics.length === characteristics.length) {
      listRef.current?.scrollToItem(characteristics.length);
    }
  }, [characteristics.length]);

  // Scroll list when moving through review so card is always in view
  useEffect(() => {
    if (listRef.current && characteristics[index]?.id) {
      listRef.current?.scrollToItem(index);
    }
  }, [index]);

  // used to prevent select from happening when choosing from the number dropdown
  const toggleDrop = () => {
    setDrop(!drop);
  };

  const getLabelWidth = () => {
    if (!characteristics) return 2;

    let max = 2;
    characteristics.forEach((characteristic) => {
      if (characteristic.markerLabel.length > max) max = characteristic.markerLabel.length;
    });
    return max;
  };

  const scrollCaptureIntoView = (char: Characteristic) => {
    if (!char) {
      return;
    }
    dispatch(changeDrawingThunk(char.drawing.id));
    const viewer = tronInstance?.Core.documentViewer;
    if (viewer) {
      viewer.trigger('scrollTo', {
        x: char.boxLocationX,
        y: char.boxLocationY,
        pageNumber: char.drawingSheetIndex,
      });
    }
  };

  const selectItem = (e: any, char: Characteristic) => {
    if (e) {
      e.preventDefault();
    }
    // Select was triggering when changing the Item# dropdown causing the List to get
    // updated with its previous ordering, preventing UI update
    if (drop) {
      return;
    }

    let currentIndex = index;
    let newSelected = [...selected];
    if (e?.ctrlKey) {
      if (
        newSelected.some((sel) => {
          return sel.id === char.id;
        })
      ) {
        const ind = newSelected.findIndex((sel) => sel.id === char.id);
        newSelected.splice(ind, 1);
      } else {
        newSelected.push(char);
      }
    } else if (e?.shiftKey) {
      // handle select range when active list is Characteristics instead of ID
      if (document.getSelection() !== null) {
        document.getSelection()!.removeAllRanges();
      }
      newSelected = [];
      const lastSelected: undefined | Characteristic | Annotation = selected.length > 0 ? selected[selected.length - 1] : undefined;
      if (lastSelected) {
        if (lastSelected === char) {
          return;
        }
        let matched = 0;
        for (let i = 0; i < characteristics.length; i++) {
          if (matched === 2) {
            break;
          }
          if (characteristics[i].id === char.id || characteristics[i].id === lastSelected.id) {
            newSelected.push(characteristics[i]);
            matched += 1;
          } else if (matched === 1) {
            newSelected.push(characteristics[i]);
          }
        }
      } else {
        // select from start to clicked
        for (let i = 0; i < characteristics.length; i++) {
          newSelected.push(characteristics[i]);
          if (characteristics[i].id === char.id) {
            break;
          }
        }
      }
    } else if (e?.target && (e.target as HTMLElement).hasAttribute('title')) {
      // don't do anything, clicked a dropdown which sometimes deselects everything else
    } else {
      if (document.activeElement?.getAttribute('name')?.includes('fullSpecification')) {
        (document.activeElement as HTMLInputElement).blur();
      }
      newSelected = [char];
      currentIndex = char.markerIndex;
      scrollCaptureIntoView(char);
    }
    if (!isEqual(newSelected, selected) || currentIndex !== index) {
      dispatch({ type: CharacteristicActions.UPDATE_SELECTED, payload: { selected: newSelected, index: currentIndex } });
    }
  };

  const onTextBlur = async (e: any) => {
    if (!characteristics || !e.target.item) return;
    const { item } = e.target;
    const val: string = isFramed(e.target.value) ? getFramedText(e.target.value) : e.target.value;
    // update the UI list
    let newItem = { ...characteristics[characteristics.findIndex((obj) => obj.id === item.id)], fullSpecification: val };
    if (types) {
      const styles = assignedStyles || {
        default: {
          style: 'f12c5566-7612-48e9-9534-ef90b276ebaa',
          assign: ['Default'],
        },
      };
      // Re-parse feature
      const capture: CaptureDimensions = {
        left: item.boxLocationX,
        top: item.boxLocationY,
        width: item.boxWidth,
        height: item.boxHeight,
      };
      if (item.captureMethod !== 'Custom') {
        const parse = await ParseTextController.parseFeatureFromFullSpec({
          assignedStyles: styles,
          boxRotation: item.boxRotation,
          capture,
          grid: gridRenderer.grid,
          oldFeature: item,
          fullSpecification: getUnframedText(val),
          tolerances,
          types,
        });
        if (!parse) return;
        const { feature } = parse;
        newItem = { ...newItem, ...feature };
      }
    }
    dispatch({ type: CharacteristicActions.EDIT_CHARACTERISTICS, payload: { updates: [newItem] } });
    if (part) dispatch(updatePartThunk({ updatedAt: new Date(), status: part.status, balloonedAt: new Date() }, part.id));
  };

  const onDropChange = (event: ChangeEvent<HTMLSelectElement> | number | string, item: Characteristic) => {
    const newNumber = event.toString();

    const { updates } = reorderController(newNumber, characteristics, [item]);
    if (updates) {
      dispatch({
        type: CharacteristicActions.EDIT_CHARACTERISTICS,
        payload: {
          updates,
        },
      });
    }
  };

  const onDragFeature = (newList: Characteristic[], select: Characteristic[]) => {
    const { adjustedList, itemsToUpdate } = dragItem(newList);
    dispatch({
      type: CharacteristicActions.EDIT_CHARACTERISTICS,
      payload: {
        updates: itemsToUpdate,
      },
    });
    if (!isEqual(select, selected)) {
      dispatch({ type: CharacteristicActions.UPDATE_SELECTED, payload: { selected: select, characteristics: adjustedList } });
    }
  };

  const handleReport = () => {
    if (!generateReport || !part) {
      return;
    }
    generateReport();
  };

  const checkAllVerified = () => {
    Analytics.track({
      event: Analytics.events.allFeaturesMarkedVerified,
      part,
      drawing: partDrawing,
      // properties: {},
    });
    for (let i = 0; i < characteristics.length; i++) {
      if (!characteristics[i].verified) {
        setVerified(false);
        return;
      }
    }
    handleReport();
  };

  // If values are valid, update max stage and submit
  const updateMaxStage = () => {
    const max = Math.max(part?.workflowStage || 0, navigation.maxPartStep, REVIEW);
    if (part) dispatch(updatePartThunk({ workflowStage: max }, part.id));
    next();
  };

  const getButtons = (len: number | undefined, isCollapsed: boolean) => {
    const disabled = characteristics.length === 0;

    if (navigation.currentPartStep === EXTRACT && settings.autoballoonEnabled?.value === 'true') {
      // Extract Step Buttons
      if (isCollapsed) {
        return <IxButton className="btn-primary square-lg" onClick={updateMaxStage} leftIcon={<CheckCircleOutlined />} tipText={disabled ? 'Balloon your drawing to continue.' : i18n('wizard.review')} size="large" disabled={disabled} />;
      }
      return (
        <ProgressFooter
          id="extraction-footer"
          classes="panel-progression-footer extraction-footer"
          cypress="progress-footer"
          steps={navigation.maxTotalSteps}
          current={currentStep + 1}
          onNext={updateMaxStage}
          canProgress={!disabled}
          progressTip={i18n('wizard.noItems')}
          canBack
          onBack={back}
        />
      );
    }
    // Review Step Buttons
    if (isCollapsed) {
      return (
        <Popconfirm
          title={i18n('wizard.unVerified')}
          onConfirm={handleReport}
          visible={!verified}
          onCancel={() => {
            setVerified(true);
          }}
          okText={i18n('common.yes')}
          cancelText={i18n('common.notYet')}
        >
          <IxButton tipText={disabled ? i18n('wizard.noItems') : i18n('wizard.report')} onClick={checkAllVerified} className="btn-primary square-lg" leftIcon={<RightCircleOutlined />} size="large" disabled={disabled} />
        </Popconfirm>
      );
    }

    return (
      <Popconfirm
        title={i18n('wizard.unVerified')}
        onConfirm={handleReport}
        visible={!verified}
        onCancel={() => {
          setVerified(true);
        }}
        okText={i18n('common.yes')}
        cancelText={i18n('common.notYet')}
      >
        <ProgressFooter
          id="review-footer"
          classes="panel-progression-footer extraction-footer"
          cypress="progress-footer"
          steps={navigation.maxTotalSteps}
          current={currentStep + 1}
          canExit={!disabled}
          exitText={i18n('wizard.report')}
          canProgress={!disabled}
          nextText={i18n('wizard.report')}
          onNext={checkAllVerified}
          onExit={checkAllVerified}
          progressTip={i18n('wizard.noItems')}
          canBack
          onBack={back}
        />
      </Popconfirm>
    );
  };

  const expandItem = (item: Characteristic, shared: boolean) => {
    const { newChars, updatedChars } = expandCharacteristic(item, characteristics, shared);

    if (updatedChars && updatedChars.length > 0) {
      dispatch({
        type: CharacteristicActions.EDIT_CHARACTERISTICS,
        payload: {
          updates: updatedChars,
          newItems: newChars,
        },
      });
    } else if (newChars && newChars.length > 0) {
      dispatch({
        type: CharacteristicActions.CREATE_CHARACTERISTICS,
        payload: newChars,
      });
    }
  };

  const expandGroup = (feature: Characteristic, shared: boolean) => {
    const group = characteristics.filter((item) => item.markerGroup === feature.markerGroup).sort((a, b) => a.markerIndex - b.markerIndex);
    const { newChars, updatedChars } = expandFeatureGroup(group, characteristics, shared);
    if (updatedChars && updatedChars.length > 0) {
      dispatch({
        type: CharacteristicActions.EDIT_CHARACTERISTICS,
        payload: {
          updates: updatedChars,
          newItems: newChars,
        },
      });
    } else if (newChars && newChars.length > 0) {
      dispatch({
        type: CharacteristicActions.CREATE_CHARACTERISTICS,
        payload: newChars,
      });
    }
  };

  const getItemList = (chars: Characteristic[]) => {
    if (chars.length > 0) {
      return [...chars, {}];
    }
    return chars;
  };

  const handleShowModal = (feature: Characteristic, field: string, cursor: number, value: string | undefined) => {
    Analytics.track({
      event: Analytics.events.specialCharactersModalOpened,
      characteristic: feature,
      properties: {
        'field.name': field,
        'field.value': feature.fullSpecification,
      },
    });
    setSymbolModal({ visible: true, feature, cursor, value });
  };

  const handleCloseModal = () => {
    setSymbolModal({ visible: false, feature: null, cursor: 0, value: undefined });
  };

  const deleteActive = () => {
    if (!part || !part.characteristics) return;

    if (!part || !part.characteristics) return;
    const deletedCharIds: string[] = [];
    const deletedAnnotIds: string[] = [];
    selected.forEach((s) => {
      if (s.captureMethod) deletedCharIds.push(s.id);
      if (s.annotation) deletedAnnotIds.push(s.id);
    });
    if (deletedCharIds.length > 0) {
      dispatch({
        type: CharacteristicActions.DELETE_CHARACTERISTICS,
        payload: {
          ids: deletedCharIds,
          part: part.id,
        },
      });
      if (part) dispatch(updatePartThunk({ updatedAt: new Date(), status: part.status, balloonedAt: new Date() }, part.id));
    }
    if (deletedAnnotIds.length > 0) {
      dispatch(deleteAnnotationThunk({ variables: { ids: deletedAnnotIds } }, part));
    }
  };

  return (
    <div className="feature-panel-container">
      <ItemPanelHeader collapsed={collapsed} autoballoonEnabled={settings.autoballoonEnabled?.value === 'true'} deleteActive={deleteActive} />
      <SpecialCharactersModal
        update={onTextBlur}
        item={symbolModal.feature}
        value={symbolModal.value || symbolModal.feature?.fullSpecification || ''}
        cursorPos={symbolModal.cursor}
        field="fullSpecification"
        label={i18n('entities.feature.fields.fullSpecification')}
        framed={symbolModal.feature?.notationClass === 'Basic' || isFramed(symbolModal.value || symbolModal.feature?.fullSpecification || '')}
        show={symbolModal.visible}
        source="item-list"
        handleCloseModal={handleCloseModal}
      />
      <div className="feature-list-container">
        <ItemList
          collapsed={navigation.currentPartStep !== null && navigation.currentPartStep >= EXTRACT ? collapsed : false}
          review={review}
          selected={[...selected]} // {characteristics.active}
          itemList={getItemList(characteristics)}
          onClick={selectItem}
          itemWidth={getLabelWidth()}
          markers={newMarkers}
          expand={expandItem}
          expandGroup={expandGroup}
          defaultMarker={defaultMarker.current}
          editItem={editItem}
          onDropChange={onDropChange}
          onDragFeature={onDragFeature}
          onTextBlur={onTextBlur}
          onDropdownVisibleChange={toggleDrop}
          listRef={listRef}
          handleShowModal={handleShowModal}
          loaded={parsed}
        />
      </div>
      <div
        className={`feature-panel-footer${collapsed ? ' collapsed' : ''}`}
        style={{
          display: 'flex',
          flexDirection: 'row',
          flexWrap: 'nowrap',
          padding: '4px 8px',
        }}
      >
        {getButtons(characteristics.length, collapsed)}
      </div>
    </div>
  );
};
