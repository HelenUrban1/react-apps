import { getExtracted, getRegion, getNewRect, modifyObject, getFontSizeModifier, getStrokeStyle } from './characteristicsRenderer';
import { testFabRect, testCapturedItem } from './utils/extractUtilTestData';
import { setupJestCanvasMock } from 'jest-canvas-mock';

describe('The Fabric Utility functions', () => {
  beforeEach(() => {
    jest.resetAllMocks();
    setupJestCanvasMock();
  });
  it('will return a Captured Item with a circular balloon', () => {
    const capturedItem = getExtracted(50, 50, 200, 40, 0, 'circle', 20, 50, 0, '1', 'red', 'red', 'white');
    expect(capturedItem.itemNumber).toEqual('1');
    expect(capturedItem.balloon._objects[0].type).toEqual('circle');
    expect(capturedItem.balloon._objects[0].group.type).toEqual('balloon');
  });

  it('will return a Captured Item with a diamond balloon', () => {
    const capturedItem = getExtracted(50, 50, 200, 40, 0, 'Diamond', 20, 50, 0, '1', 'red', 'red', 'white');
    expect(capturedItem.itemNumber).toEqual('1');
    expect(capturedItem.rectangle.fill).toEqual('transparent');
    expect(capturedItem.balloon._objects[0].type).toEqual('diamond');
    expect(capturedItem.balloon._objects[0].angle).toEqual(45);
  });

  it('will return a Captured Item with a square balloon', () => {
    const capturedItem = getExtracted(50, 50, 200, 40, 0, 'Square', 20, 50, 0, '1', 'red', 'red', 'white');
    expect(capturedItem.itemNumber).toEqual('1');
    expect(capturedItem.rectangle.fill).toEqual('transparent');
    expect(capturedItem.balloon._objects[0].type).toEqual('square');
    expect(capturedItem.balloon._objects[0].angle).toEqual(0);
  });

  it('will return a Captured Item with a triangle balloon', () => {
    const capturedItem = getExtracted(50, 50, 200, 40, 0, 'Triangle', 20, 50, 0, '1', 'red', 'red', 'white');
    expect(capturedItem.itemNumber).toEqual('1');
    expect(capturedItem.balloon._objects[0].type).toEqual('triangle');
  });

  it('will return a Captured Item with a pentagon balloon', () => {
    const capturedItem = getExtracted(50, 50, 200, 40, 0, 'Pentagon', 20, 50, 0, '1', 'red', 'red', 'white');
    expect(capturedItem.itemNumber).toEqual('1');
    expect(capturedItem.balloon._objects[0].type).toEqual('pentagon');
  });

  it('will return a Captured Item with a hexagon balloon', () => {
    const capturedItem = getExtracted(50, 50, 200, 40, 0, 'Hexagon', 20, 50, 0, '1', 'red', 'red', 'white');
    expect(capturedItem.itemNumber).toEqual('1');
    expect(capturedItem.balloon._objects[0].type).toEqual('hexagon');
  });

  it('will return a region rectangle', () => {
    const region = getRegion(50, 50, 200, 40, 0);
    expect(region.rectangle.type).toEqual('region');
    expect(region.rectangle.fill).toEqual('rgb(27, 98, 233, 0.25)');
    expect(region.balloon).toEqual(null);
    expect(region.itemNumber).toEqual(null);
  });

  it('will return a region rectangle with a set color', () => {
    const rectangle = getNewRect(50, 50, 200, 40, 0);
    expect(rectangle.type).toEqual('region');
    expect(rectangle.fill).toEqual('transparent');
  });

  describe('will return the right dash array', () => {
    it('for dashed lines', () => {
      const arr = getStrokeStyle('Dashed');
      expect(arr).toStrictEqual([5, 2]);
    });
    it('for dotted lines', () => {
      const arr = getStrokeStyle('Dotted');
      expect(arr).toStrictEqual([2, 2]);
    });
    it('for solid lines', () => {
      const arr = getStrokeStyle('Solid');
      expect(arr).toStrictEqual([5, 0]);
    });
    it('for invalid lines', () => {
      const arr = getStrokeStyle('sfdsfsdf');
      expect(arr).toStrictEqual([5, 0]);
    });
  });
});

describe('The Object Modifier', () => {
  it('will scale an extract from the top left', () => {
    const ev = {
      target: {
        type: 'extract',
        scaleX: 1.25,
        scaleY: 1.5,
      },
      transform: {
        action: 'scale',
        corner: 'tl',
      },
    };
    const ret = modifyObject(ev, testFabRect, testCapturedItem, 0, 0, 800, 600);
    expect(ret.boxHeight).toEqual(18.75);
    expect(ret.boxWidth).toEqual(75);
    expect(ret.boxLocationX).toEqual(45);
    expect(ret.boxLocationY).toEqual(56.25);
  });

  it('will rotate an extract', () => {
    const ev = {
      target: {
        type: 'extract',
        scaleX: 1,
        scaleY: 1,
        angle: 45,
      },
      transform: {
        action: 'rotate',
      },
    };
    const ret = modifyObject(ev, testFabRect, testCapturedItem, 0, 0, 800, 600);
    expect(ret.boxHeight).toEqual(15);
    expect(ret.boxWidth).toEqual(50);
    expect(ret.boxLocationX).toEqual(70);
    expect(ret.boxLocationY).toEqual(60);
    expect(ret.boxRotation).toEqual(45);
  });

  it('will translate an extract', () => {
    const ev = {
      target: {
        type: 'extract',
        left: 25,
        top: 40,
        width: 50,
        height: 15,
      },
      transform: {
        action: 'drag',
      },
    };
    const ret = modifyObject(ev, testFabRect, testCapturedItem, 0, 0, 800, 600);
    expect(ret.boxHeight).toEqual(15);
    expect(ret.boxWidth).toEqual(50);
    expect(ret.boxLocationX).toEqual(0);
    expect(ret.boxLocationY).toEqual(32.5);
    expect(ret.boxRotation).toEqual(0);
  });

  it('will not transform a region', () => {
    const ev = {
      target: {
        type: 'region',
        left: 25,
        top: 40,
        scaleX: 1.2,
        scaleY: 1.5,
        angle: 175,
      },
    };
    const ret = modifyObject(ev, testFabRect, testCapturedItem, 0, 0, 800, 600);
    expect(ret.boxHeight).toEqual(15);
    expect(ret.boxWidth).toEqual(50);
    expect(ret.boxLocationX).toEqual(70);
    expect(ret.boxLocationY).toEqual(60);
    expect(ret.boxRotation).toEqual(0);
  });

  it('will modify the polar coordinates of a balloon', () => {
    // thing
    const ev = {
      target: {
        type: 'balloon',
        left: 10,
        top: 20,
        scaleX: 1.2,
        scaleY: 1.5,
        angle: 175,
      },
    };
    const ret = modifyObject(ev, testFabRect, testCapturedItem, 0, 0, 800, 600);
    expect(ret.boxHeight).toEqual(15);
    expect(ret.boxWidth).toEqual(50);
    expect(ret.boxLocationX).toEqual(70);
    expect(ret.boxLocationY).toEqual(60);
    expect(ret.boxRotation).toEqual(0);
    expect(Math.round(ret.markerLocationX)).toEqual(97);
    expect(Math.round(ret.markerLocationY)).toEqual(-151);
  });

  it('will modify the coordinates of a connection point', () => {
    // thing
    const ev = {
      target: {
        type: 'connectionPoint',
        left: 10,
        top: 20,
        scaleX: 1.2,
        scaleY: 1.5,
        angle: 175,
      },
    };
    const ret = modifyObject(ev, testFabRect, testCapturedItem, 0, 0, 800, 600);
    expect(ret.boxHeight).toEqual(15);
    expect(ret.boxWidth).toEqual(50);
    expect(ret.boxLocationX).toEqual(70);
    expect(ret.boxLocationY).toEqual(60);
    expect(ret.boxRotation).toEqual(0);
    expect(Math.round(ret.connectionPointLocationX)).toEqual(10);
    expect(Math.round(ret.connectionPointLocationY)).toEqual(20);
    expect(ret.connectionPointIsFloating).toEqual(true);
  });
});

describe('getFontSizeModifier', () => {
  it('should return return 1 if label text is undefined', () => {
    let label;
    const fontSizeModifier = getFontSizeModifier(label);
    expect(fontSizeModifier).toBe(1);
  });
  it('should return return 1 if label text is empty', () => {
    const label = '';
    const fontSizeModifier = getFontSizeModifier(label);
    expect(fontSizeModifier).toBe(1);
  });
  it('should return return 1 if label text is less than 3 characters', () => {
    const label = '12';
    const fontSizeModifier = getFontSizeModifier(label);
    expect(fontSizeModifier).toBe(1);
  });
  it('should return return .8 if label text is 3 characters', () => {
    const label = '123';
    const fontSizeModifier = getFontSizeModifier(label);
    expect(fontSizeModifier).toBe(0.8);
  });
  it('should return return .6 if label text is 4 characters', () => {
    const label = '1234';
    const fontSizeModifier = getFontSizeModifier(label);
    expect(fontSizeModifier).toBe(0.6);
  });
  it('should return return .4 if label text is 5 characters', () => {
    const label = '12345';
    const fontSizeModifier = getFontSizeModifier(label);
    expect(fontSizeModifier).toBeCloseTo(0.4);
  });
  it('should return return .2 if label text length is 6 or more characters ', () => {
    const label1 = '123456';
    const fontSizeModifier1 = getFontSizeModifier(label1);
    expect(fontSizeModifier1).toBeCloseTo(0.2);
    const label2 = '123456789';
    const fontSizeModifier2 = getFontSizeModifier(label2);
    expect(fontSizeModifier2).toBeCloseTo(0.2);
  });
});
