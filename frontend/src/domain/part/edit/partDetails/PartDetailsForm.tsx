import React, { useEffect, useState, useRef } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Popconfirm } from 'antd';
import { i18n } from 'i18n';
import PanelHeader from 'styleguide/PanelHeader/PanelHeader';
import { ProgressFooter } from 'styleguide/ProgressionFooter';
import { MarkerStyle } from 'types/marker';
import { getCustomerSettingPreset } from 'utils/PresetUtils';
import Analytics from 'modules/shared/analytics/analytics';
import { AppState } from 'modules/state';
import { PartStepEnum } from 'modules/navigation/navigationActions';
import { CharacteristicActions } from 'modules/characteristic/characteristicActions';
import { PartEditorActions } from 'modules/partEditor/partEditorActions';
import { updatePartThunk } from 'modules/part/partActions';
import authSelectors from 'modules/auth/authSelectors';
import { Section } from 'view/global/inputs';
import CustomerSelect from 'view/global/CustomerSelect';
import { Tolerance } from 'types/tolerances';
import { RegExValidators } from 'utils/characterValidationRules';
import { updateDefaultTolerances } from '../defaultTolerances/TolerancesLogic';
import { updateCharacteristicMarkers } from '../balloonStyles/BalloonLogic';
import { PresetOptions } from './PresetOptions';
import './partDetails.less';
import moment from 'moment';

const { PART, STYLES, DOCS } = PartStepEnum;

export interface PartDetailsFormProps {
  canCancel: boolean;
  cancel: () => void;
  exit: () => void;
  next: () => void;
}

interface FormValues {
  id: string;
  name: string;
  customer: string;
  number: string;
  revision: string;
  accessControl: string;
  workflowStage: number;
  // Allows access to values using brackets - object[variable]
  [key: string]: string | number;
}

interface PresetRef {
  index: number;
  matched: { preset: Tolerance | { [key: string]: MarkerStyle } | undefined; presetName: string }[];
}

export const PartDetailsForm: React.FC<PartDetailsFormProps> = ({ canCancel, exit, cancel, next }) => {
  const navigation = useSelector((state: AppState) => state.navigation);
  const { characteristics } = useSelector((state: AppState) => state.characteristics);
  const { settings, metricUnits, organizations, markers } = useSelector((state: AppState) => state.session);
  const restricted = useSelector((state: AppState) => authSelectors.selectIsRestricted(state));
  const [showConfirm, setShowConfirm] = useState(false);
  const matchedStylePresets = useRef<PresetRef>({ index: -1, matched: [] });
  const matchedTolerancePresets = useRef<PresetRef>({ index: -1, matched: [] });
  const newCustomer = useRef('');
  const dispatch = useDispatch();

  const resetPresetRefs = () => {
    matchedTolerancePresets.current = { index: -1, matched: [] };
    matchedStylePresets.current = { index: -1, matched: [] };
  };

  const [nextEnabled, setNextEnabled] = useState(false);

  // Prepare to update the part at each stage of part creation
  const { partDrawing, tolerances, presets, assignedStyles, focus } = useSelector((state: AppState) => state.partEditor);
  const { part } = useSelector((state: AppState) => state.part);

  // State to track changes to record between saves
  const [formValues, setFormValues] = useState<FormValues>({
    id: part?.id || '',
    name: part?.name || '',
    customer: part?.customer && part.customer.id ? part.customer.id : '',
    number: part?.number || '',
    revision: part?.revision || moment(part?.createdAt).format('YYYY-MM-DD'),
    accessControl: part?.accessControl || '',
    workflowStage: part?.workflowStage || 0,
  });

  useEffect(() => {
    setNextEnabled(formValues.number.trim().length > 0);
  }, [formValues.number]);

  useEffect(() => {
    if (part && partDrawing) {
      setFormValues({
        id: part?.id || '',
        name: part?.name || '',
        customer: part?.customer && part.customer.id ? part.customer.id : '',
        number: part?.number || '',
        revision: part?.revision || moment(part?.createdAt).format('YYYY-MM-DD'),
        accessControl: part?.accessControl || '',
        workflowStage: part?.workflowStage || 0,
      });
      if (navigation.currentPartStep === PART && focus) {
        (document.getElementById(focus.split(' ')[1])?.firstChild as HTMLTextAreaElement).focus();
        dispatch({ type: PartEditorActions.SET_PART_EDITOR_FOCUS, payload: null });
      }
    }
  }, [partDrawing?.id]);

  const isMetric = (tols: Tolerance) => {
    const unit = metricUnits ? metricUnits[tols.linear.unit] : undefined;
    return !!unit;
  };

  // Updates form state with a change
  const updateFormState = async (field: string, value: string) => {
    let newFormValues = { ...formValues };
    newFormValues[field] = value;

    if (formValues[field] !== value) {
      Analytics.track({
        event: Analytics.events.partInfoUpdated,
        part,
        drawing: partDrawing,
        properties: {
          'field.name': field,
          'field.value.from': formValues[field],
          'field.value.to': value,
        },
      });
    }

    if (field === 'customer' && (matchedTolerancePresets.current.index >= 0 || matchedStylePresets.current.index >= 0)) {
      const { index: tolInd, matched: tolMatched } = matchedTolerancePresets.current;
      const { index: styleInd, matched: styleMatched } = matchedStylePresets.current;
      const partPresets = { ...presets };
      if (tolInd >= 0) {
        const customerTolerances = tolMatched[tolInd].preset as Tolerance;
        partPresets.tolerances = tolMatched[tolInd].presetName;
        if (isMetric(customerTolerances)) {
          newFormValues.measurement = 'Metric';
        } else {
          newFormValues.measurement = 'Imperial';
        }
        newFormValues = {
          ...newFormValues,
          defaultLinearTolerances: JSON.stringify(customerTolerances.linear),
          defaultAngularTolerances: JSON.stringify(customerTolerances.angular),
          presets: JSON.stringify({ ...partPresets, tolerances: customerTolerances.presetName }),
        };
        const { updatedFeatures } = updateDefaultTolerances(customerTolerances, characteristics, false);
        if (updatedFeatures && updatedFeatures.length > 0) {
          dispatch({
            type: CharacteristicActions.EDIT_CHARACTERISTICS,
            payload: {
              updates: updatedFeatures,
            },
          });
        }
      }
      if (styleInd >= 0) {
        const customerStyles = styleMatched[styleInd].preset as { [key: string]: MarkerStyle };
        partPresets.styles = styleMatched[styleInd].presetName;
        updateCharacteristicMarkers(customerStyles, characteristics, (updates, assignedMarkers) => dispatch({ type: CharacteristicActions.UPDATE_MARKERS, payload: { assignedMarkers, updates, markers } }));
        newFormValues = { ...newFormValues, defaultMarkerOptions: JSON.stringify(customerStyles), presets: JSON.stringify({ ...partPresets, styles: partPresets.styles }) };
      }
      dispatch({
        type: PartEditorActions.SET_PART_EDITOR_STATE,
        payload: {
          presets: partPresets,
          assignedStyles:
            (styleMatched[styleInd]?.preset as {
              [key: string]: MarkerStyle;
            }) || assignedStyles,
          tolerances: (tolMatched[tolInd]?.preset as Tolerance) || tolerances,
        },
      });
    }
    resetPresetRefs();
    setFormValues(newFormValues);
    if (part) dispatch(updatePartThunk({ ...newFormValues }, part.id));
  };

  // Handle onChange events by unpacking saving new value for field in form state
  const handleChange = (event: React.ChangeEvent<HTMLInputElement>): void => {
    const [field, value] = [event.target.name, event.target.value];
    updateFormState(field, value.replace(RegExValidators.matchCommonSpecialChars, ''));
  };

  const changeCustomerAndPresets = () => {
    updateFormState('customer', newCustomer.current);
    setShowConfirm(false);
  };

  const changeCustomerOnly = () => {
    resetPresetRefs();
    updateFormState('customer', newCustomer.current);
    setShowConfirm(false);
  };

  const handleChangeCustomer = (value: string) => {
    const stylePresets = settings.stylePresets ? getCustomerSettingPreset(settings.stylePresets, value) : [];
    const tolerancePresets = settings.tolerancePresets ? getCustomerSettingPreset(settings.tolerancePresets, value) : [];
    if (stylePresets.length > 0) {
      matchedStylePresets.current = { index: 0, matched: stylePresets };
    }
    if (tolerancePresets.length > 0) {
      matchedTolerancePresets.current = { index: 0, matched: tolerancePresets };
    }

    if (part && ((part.workflowStage > PART && tolerancePresets.length > 0) || (part.workflowStage >= STYLES && stylePresets.length > 0) || tolerancePresets.length > 1 || stylePresets.length > 1)) {
      setShowConfirm(true);
      newCustomer.current = value;
    } else {
      newCustomer.current = '';
      updateFormState('customer', value);
    }
  };

  // Handle submit event by committing all pending changes  the record with all fields
  const handleSubmit = (event: React.SyntheticEvent) => {
    event.preventDefault();

    // If values are valid, update max stage and submit
    const max = Math.max(part?.workflowStage || 0, navigation.maxPartStep, DOCS);
    formValues.workflowStage = max;

    // Save unapplied changes to the part record
    if (part) dispatch(updatePartThunk({ ...formValues }, part.id));

    // Move to the next stage
    next();
  };

  const sections: any = [
    [
      {
        name: 'partGroup',
        label: '',
        value: '',
        group: [
          {
            name: 'number',
            label: 'Part Number',
            classes: 'group-flex-5 required',
            value: formValues.number,
            change: handleChange,
            type: 'text',
            required: true,
            validateTo: RegExValidators.matchCommonSpecialChars,
          },
          {
            name: 'revision',
            label: 'Revision',
            classes: 'group-flex-3',
            value: formValues.revision,
            blur: handleChange,
            type: 'text',
          },
        ],
      },
      {
        name: 'name',
        label: 'Part Name',
        value: formValues.name,
        type: 'text',
        blur: handleChange,
        validateTo: RegExValidators.matchCommonSpecialChars,
      },
    ],
  ];

  if (!restricted) {
    sections[0].push({
      name: 'accessControl',
      label: 'Document Control',
      value: formValues.accessControl,
      change: handleChange,
      type: 'select',
      group: [
        {
          name: 'None',
          label: 'None',
          value: 'None',
        },
        {
          name: 'ITAR',
          label: 'ITAR/EAR',
          value: 'ITAR',
        },
      ],
    });
  }

  const handlePresetOptionChange = (value: string, type: 'Style' | 'Tolerance') => {
    if (type === 'Style') {
      matchedStylePresets.current.index = matchedStylePresets.current?.matched.findIndex((preset) => preset.presetName === value) || -1;
    } else {
      matchedTolerancePresets.current.index = matchedTolerancePresets.current?.matched.findIndex((preset) => preset.presetName === value) || -1;
    }
  };

  return (
    <>
      <section id="partAnalysis" className="side-panel-content">
        <PanelHeader header={`${PART + 1}. ${i18n('entities.part.title.info')}`} copy={i18n('entities.part.instructions.info')} dataCy="part_info_" />
        <form className="panel-form" onSubmit={handleSubmit}>
          <Popconfirm
            visible={showConfirm} //
            placement="top"
            title={
              <PresetOptions
                matchedStylePresets={matchedStylePresets.current.matched}
                matchedTolerancePresets={matchedTolerancePresets.current.matched}
                handlePresetOptionChange={handlePresetOptionChange}
                customer={organizations?.all.find((org) => org.id === newCustomer.current)?.name || i18n('common.customer')}
              />
            }
            onConfirm={changeCustomerAndPresets}
            okText={i18n('common.apply')}
            cancelText={i18n('common.skip')}
            onCancel={changeCustomerOnly}
          >
            <CustomerSelect value={formValues.customer} onChange={handleChangeCustomer} />
          </Popconfirm>
          {sections.map((section: any, index: number) => (
            <Section key={`form-section-${index}`} id={index} sections={section} />
          ))}
        </form>
      </section>
      <div className="form-buttons form-section sider-panel-progress-footer">
        <ProgressFooter
          id="part-info-footer"
          classes="panel-progression-footer"
          cypress="progress-footer"
          steps={navigation.maxTotalSteps}
          current={PART + 1}
          onNext={handleSubmit}
          canExit={!canCancel}
          canProgress={nextEnabled}
          progressTip={i18n('wizard.noPartNumber')}
          onExit={exit}
          canCancel={canCancel}
          onCancel={cancel}
          onBack={exit}
          backCy="Back-btn-footer"
          flow={i18n('entities.part.enumerators.type.Part')}
        />
      </div>
    </>
  );
};
