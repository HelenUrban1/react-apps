import React from 'react';
import { Select } from 'antd';
import { i18n } from 'i18n';
import { MarkerStyle } from 'types/marker';
import { Tolerance } from 'types/tolerances';

const { Option } = Select;

interface PresetOptions {
  matchedStylePresets: { preset: Tolerance | { [key: string]: MarkerStyle } | undefined; presetName: string }[] | undefined;
  matchedTolerancePresets: { preset: Tolerance | { [key: string]: MarkerStyle } | undefined; presetName: string }[] | undefined;
  handlePresetOptionChange: (value: string, type: 'Style' | 'Tolerance') => void;
  customer: string;
}

export const PresetOptions = ({ matchedStylePresets, matchedTolerancePresets, handlePresetOptionChange, customer }: PresetOptions) => {
  const getSelectOptions = (matchedPresets: { preset: Tolerance | { [key: string]: MarkerStyle } | undefined; presetName: string }[]) => {
    return matchedPresets.map((preset) => (
      <Option key={preset.presetName} value={preset.presetName}>
        {preset.presetName}
      </Option>
    ));
  };

  return (
    <>
      <section className="matched-preset-header">
        <span>{i18n('wizard.presetModal.applyTo', customer)}</span>
      </section>
      <section className="matched-preset-options">
        {matchedTolerancePresets && matchedTolerancePresets.length > 0 && (
          <section className="matched-tolerance-options">
            <span>{i18n('entities.part.title.tolerances')}</span>
            <Select dropdownClassName="preset-dropdown" defaultValue={matchedTolerancePresets[0].presetName} onChange={(value: string) => handlePresetOptionChange(value, 'Tolerance')}>
              {getSelectOptions(matchedTolerancePresets)}
            </Select>
          </section>
        )}
        <div className="spacer" />
        {matchedStylePresets && matchedStylePresets.length > 0 && (
          <section className="matched-style-options">
            <span>{i18n('wizard.styles')}</span>
            <Select dropdownClassName="preset-dropdown" defaultValue={matchedStylePresets[0].presetName} onChange={(value: string) => handlePresetOptionChange(value, 'Style')}>
              {getSelectOptions(matchedStylePresets)}
            </Select>
          </section>
        )}
      </section>
    </>
  );
};
