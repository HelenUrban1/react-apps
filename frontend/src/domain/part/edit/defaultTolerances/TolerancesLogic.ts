import { Characteristic } from 'types/characteristics';
import log from 'modules/shared/logger';
import { Tolerance, PrecisionTolerance, RangeTolerance } from 'types/tolerances';
import { Tolerancer } from 'utils/ParseTextController/Tolerancer';
import { cloneDeep } from 'lodash';
import { DefaultTolerances } from 'view/global/defaults';
import { parseValidJSONString } from 'utils/textOperations';
import { ListObject } from 'domain/setting/listsTypes';
import { DrawingSheet } from 'graphql/drawing_sheet';

export const isValidNumber = new RegExp(/^-?\d*\.?\d*$/);

export const convertTolerance = (
  OldTolerances: {
    defaultLinearTolerances: string | null;
    defaultAngularTolerances: string | null;
  },
  tolerances: Tolerance,
  section: string,
) => {
  const newTolerances = { ...OldTolerances };
  if (section === 'linear') {
    newTolerances.defaultLinearTolerances = JSON.stringify(tolerances.linear);
  } else if (section === 'angular') {
    newTolerances.defaultAngularTolerances = JSON.stringify(tolerances.angular);
  } else if (section === 'both') {
    newTolerances.defaultLinearTolerances = JSON.stringify(tolerances.linear);
    newTolerances.defaultAngularTolerances = JSON.stringify(tolerances.angular);
  } else {
    log.warn('No known section provided');
    return OldTolerances;
  }
  if (newTolerances.defaultLinearTolerances === OldTolerances.defaultLinearTolerances && newTolerances.defaultAngularTolerances === OldTolerances.defaultAngularTolerances) {
    log.debug('Tolerances not changed');
    return { defaultLinearTolerances: null, defaultAngularTolerances: null };
  }
  return newTolerances;
};

export const validateLevel = (tols: PrecisionTolerance[], level?: string) => {
  const usedLevels: string[] = [];
  for (let index = 0; index < tols.length; index++) {
    const tol = tols[index];
    if (level && tol.level === level) {
      return false;
    }
    if (!usedLevels.includes(tol.level)) {
      usedLevels.push(tol.level);
    } else {
      return false;
    }
  }
  return true;
};

export const validateSpecificLevel = (tols: PrecisionTolerance[], level?: string) => {
  let valid = true;
  const usedLevels: string[] = [];

  tols.forEach((tol: PrecisionTolerance) => {
    if (!usedLevels.includes(tol.level)) {
      usedLevels.push(tol.level);
    } else if (level && tol.level === level) {
      valid = false;
    }
  });
  return valid;
};

export const validateTolerance = (tol: PrecisionTolerance | RangeTolerance) => {
  let valid = true;
  Object.entries(tol).forEach(([key, value]) => {
    if (key !== 'level' && valid) {
      valid = isValidNumber.test(value);
    }
  });
  return valid;
};

export const validateSection = (tolerances: Tolerance, section: string) => {
  let valid = true;
  if (tolerances[section].type === 'PrecisionTolerance') {
    if (validateLevel(tolerances[section].precisionTols)) {
      tolerances[section].precisionTols.forEach((tolerance: PrecisionTolerance) => {
        if (valid) {
          valid = validateTolerance(tolerance);
        }
      });
    } else {
      valid = false;
    }
  } else {
    tolerances[section].rangeTols.forEach((tolerance: RangeTolerance) => {
      if (valid) {
        valid = validateTolerance(tolerance);
      }
    });
  }
  return valid;
};

export const checkUpdate = (tolerances: Tolerance) => {
  let valid = true;
  if (validateSection(tolerances, 'linear')) {
    valid = validateSection(tolerances, 'angular');
  } else {
    valid = false;
  }
  return valid;
};

export const toggleToleranceType = (tolerances: Tolerance, section: string) => {
  const newTolerances = cloneDeep(tolerances);
  newTolerances[section].type = newTolerances[section].type === 'PrecisionTolerance' ? 'RangeTolerance' : 'PrecisionTolerance';
  return newTolerances;
};

export const changePrecisionLevel = (tolerances: Tolerance, index: number, section: string, value: string) => {
  const newTolerances = cloneDeep(tolerances);
  if (newTolerances[section] && newTolerances[section].precisionTols[index]) {
    newTolerances[section].precisionTols[index].level = value;
  } else {
    log.warn('Section or level for precision tolerance unavailable to be set');
  }
  return newTolerances;
};

export const removeTolerance = (tolerances: Tolerance, section: string, index: number) => {
  const newTolerances = cloneDeep(tolerances);
  if (newTolerances[section].type === 'PrecisionTolerance') {
    newTolerances[section].precisionTols.splice(index, 1);
  } else {
    newTolerances[section].rangeTols.splice(index, 1);
  }
  return newTolerances;
};

export const addTolerance = (tolerances: Tolerance, section: string) => {
  const newTolerances = cloneDeep(tolerances);
  if (newTolerances[section].type === 'PrecisionTolerance') {
    newTolerances[section].precisionTols.push({
      level: '',
      plus: '',
      minus: '',
    });
  } else {
    newTolerances[section].rangeTols.push({
      rangeHigh: '',
      rangeLow: '',
      plus: '',
      minus: '',
    });
  }
  return newTolerances;
};

export const changeToleranceInput = (tolerances: Tolerance, index: number, section: string, input: string, value: string) => {
  const newTolerances = cloneDeep(tolerances);
  if (newTolerances[section].type === 'PrecisionTolerance') {
    newTolerances[section].precisionTols[index][input] = value;
  } else {
    newTolerances[section].rangeTols[index][input] = value;
  }
  return newTolerances;
};

export const changeUnit = (tolerances: Tolerance, section: string, value: string) => {
  const newTolerances = cloneDeep(tolerances);
  newTolerances[section].unit = value;
  return newTolerances;
};

export const buildNewTolerances = ({ section, action, value, index, input, tolerances }: { section: string; action: string; value?: string; index?: number; input?: string; tolerances: Tolerance }) => {
  let newTolerances: Tolerance = cloneDeep(tolerances);
  let valid = true;
  let fromValue;

  switch (action) {
    case 'add':
      valid = false;
      newTolerances = addTolerance(newTolerances, section);
      break;
    case 'precision':
      if (index === undefined || value === undefined) {
        valid = false;
        break;
      }
      newTolerances = changePrecisionLevel(newTolerances, index, section, value);
      valid = validateLevel(newTolerances[section].precisionTols);
      break;
    case 'input':
      if (index === undefined || value === undefined || input === undefined) {
        valid = false;
        break;
      }
      newTolerances = changeToleranceInput(newTolerances, index, section, input, value);
      valid = validateSection(newTolerances, section);
      break;
    case 'unit':
      if (value === undefined) {
        break;
      }
      newTolerances = changeUnit(newTolerances, section, value);
      break;
    case 'remove':
      if (index === undefined) {
        break;
      }
      newTolerances = removeTolerance(newTolerances, section, index);
      break;
    case 'toggle':
      newTolerances = toggleToleranceType(newTolerances, section);
      break;
    default:
      log.warn('no action passed to tolerance update');
      return { newTolerances, valid, fromValue };
  }

  return { newTolerances, valid, fromValue };
};

export const isChanged = (a: string | null, b: string | null) => {
  if (a !== b && !(a === null && b === '')) {
    // log.debug('a,b', a, b);
    return true;
  }
  return false;
};

// Updates the tolerances and unit when the part Tolerances are changed
export const updateDefaultTolerances = (tolerances: Tolerance, characteristics: Characteristic[], unitsOnly = true) => {
  const updatedFeatures: Characteristic[] = [];
  const allFeatures: Characteristic[] = [];
  characteristics.forEach((feature) => {
    const newFeature = { ...feature };
    const noTolsFeature = { ...newFeature, upperSpecLimit: '', lowerSpecLimit: '', plusTol: '', minusTol: '' };
    const newTols = Tolerancer.calculateDimensionSpecLimits({ feature: noTolsFeature, tolerances, measurement: noTolsFeature.notationSubtype });
    if (feature.toleranceSource === 'Default_Tolerance' && feature.notationClass === 'Tolerance' && !unitsOnly) {
      newFeature.plusTol = newTols.plusTol || feature.plusTol;
      newFeature.minusTol = newTols.minusTol || feature.minusTol;
      newFeature.upperSpecLimit = newTols.upperSpecLimit || feature.upperSpecLimit;
      newFeature.lowerSpecLimit = newTols.lowerSpecLimit || feature.lowerSpecLimit;
    }
    const newUnit = Tolerancer.getTolerancesForSubtype({ subType: feature.notationSubtype, tolerances }).unit;
    newFeature.unit = newUnit || feature.unit;
    if (isChanged(feature.upperSpecLimit, newFeature.upperSpecLimit) || isChanged(feature.lowerSpecLimit, newFeature.lowerSpecLimit) || isChanged(feature.unit, newFeature.unit)) {
      newFeature.verified = false;
      updatedFeatures.push(newFeature);
    }
    allFeatures.push(newFeature);
  });

  return { allFeatures, updatedFeatures };
};

export const rebuildToleranceSettingObject = ({ currentPreset, linear, angular, settings }: { currentPreset?: string; linear?: string | null; angular?: string | null; settings?: Tolerance }) => {
  const defaults = settings || DefaultTolerances;
  if (currentPreset) {
    const defaultToleranceSetting: Tolerance = {
      linear: linear && linear !== 'none' ? parseValidJSONString(linear) : cloneDeep(defaults.linear),
      angular: angular && angular !== 'none' ? parseValidJSONString(angular) : cloneDeep(defaults.angular),
    };
    return defaultToleranceSetting;
  }
  return {} as Tolerance;
};

export const isMetric = (tols: Tolerance, metricUnits?: ListObject | null) => {
  const unit = metricUnits ? metricUnits[tols.linear.unit] : undefined;
  return !!unit;
};

export const allOtherSheetsAreDefault = (currentIndex: number, sheets?: DrawingSheet[]) => {
  if (currentIndex !== 0 || !sheets) {
    return false;
  }
  const defAng = JSON.stringify(DefaultTolerances.angular);
  const defLin = JSON.stringify(DefaultTolerances.linear);

  for (let index = 0; index < sheets.length; index++) {
    const sheet = sheets[index];
    if (index === 0 && sheet.defaultAngularTolerances === defAng && sheet.defaultLinearTolerances === defLin) {
      return false;
    }
    if (index !== 0 && (sheet.defaultAngularTolerances !== defAng || sheet.defaultLinearTolerances !== defLin)) {
      return false;
    }
  }
  return true;
};
