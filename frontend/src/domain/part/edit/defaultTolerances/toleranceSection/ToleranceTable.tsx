import React from 'react';
import { PrecisionTolerance, RangeTolerance } from 'types/tolerances';
import { ToleranceRow } from './ToleranceRow';

interface ToleranceTableProps {
  title: string;
  presetName: string;
  type: 'PrecisionTolerance' | 'RangeTolerance';
  rangeTols: RangeTolerance[];
  precisionTols: PrecisionTolerance[];
  update: ({ section, action, value, index, input }: { section: string; action: string; value?: string; index?: number; input?: string }) => void;
  change: ({ section, action, value, index, input }: { section: string; action: string; value: string; index: number; input: string }) => void;
}

export const ToleranceTable = ({ title, presetName, type, rangeTols, precisionTols, update, change }: ToleranceTableProps) => {
  return (
    <table id={`${title}-table`} className="tolerances-table">
      <thead>
        <tr>
          <th id={`${title}-${type}`}>{type === 'PrecisionTolerance' ? 'Precision' : 'Range'}</th>
          <th id={`${title}-plus`}>Plus Tol</th>
          <th id={`${title}-minus`}>Minus Tol</th>
          <th />
        </tr>
      </thead>
      <tbody>
        {type === 'PrecisionTolerance' &&
          precisionTols.length > 0 &&
          precisionTols.map((tol, index) => (
            <ToleranceRow //
              key={`prec-${index}-${tol.level}-${presetName}`}
              title={title}
              presetName={presetName}
              type={type}
              index={index}
              tol={tol}
              rangeTols={rangeTols}
              precisionTols={precisionTols}
              update={update}
              change={change}
            />
          ))}
        {type === 'RangeTolerance' &&
          rangeTols.length > 0 &&
          rangeTols.map((tol, index) => (
            <ToleranceRow //
              key={`range-${index}-${tol.rangeLow}-${presetName}`}
              title={title}
              presetName={presetName}
              type={type}
              index={index}
              tol={tol}
              rangeTols={rangeTols}
              precisionTols={precisionTols}
              update={update}
              change={change}
            />
          ))}
      </tbody>
    </table>
  );
};
