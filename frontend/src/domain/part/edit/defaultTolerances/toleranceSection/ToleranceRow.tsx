import React from 'react';
import { message, Select } from 'antd';
import { EditTextInPlace } from 'styleguide/EditInPlace/EditInPlace';
import { validateNumber } from 'utils/DataCalculation';
import { PrecisionTolerance, RangeTolerance } from 'types/tolerances';
import RemoveOptionButton from 'styleguide/Buttons/RemoveOption';
import { validateSpecificLevel } from '../TolerancesLogic';

const { Option } = Select;

const possiblePrecisionLevels = ['X', 'X.X', 'X.XX', 'X.XXX', 'X.XXXX', 'X.XXXXX', 'X.XXXXXX'];

interface ToleranceRowProps {
  title: string;
  presetName: string;
  type: 'PrecisionTolerance' | 'RangeTolerance';
  tol: RangeTolerance | PrecisionTolerance;
  index: number;
  rangeTols: RangeTolerance[];
  precisionTols: PrecisionTolerance[];
  update: ({ section, action, value, index, input }: { section: string; action: string; value?: string; index?: number; input?: string }) => void;
  change: ({ section, action, value, index, input }: { section: string; action: string; value: string; index: number; input: string }) => void;
}

const PrecisionToleranceRow = ({ title, presetName, tol, index, type, rangeTols, precisionTols, update, change }: ToleranceRowProps) => {
  const selectContents = (e: any) => {
    e.target.select();
  };

  return (
    <tr key={`${title}-${index}-${presetName}-${precisionTols.length}`}>
      <th id={`${title}-${index}-precision`} className={`tolerance-${type}`}>
        <Select
          className={`precision-select ${validateSpecificLevel(precisionTols, tol.level) ? '' : 'validation-error'}`}
          dropdownClassName={`precision-select-${index}`}
          value={tol.level}
          onChange={(value: string) => update({ section: title.toLowerCase(), action: 'precision', value, index })}
        >
          {possiblePrecisionLevels.map((level) => (
            <Option key={level} value={level}>
              {level}
            </Option>
          ))}
        </Select>
      </th>
      <th id={`${title}-${index}-plus`}>
        <EditTextInPlace
          id={index === 0 ? `${title}-PrecisionTolerance-first` : undefined}
          value={tol.plus}
          name=""
          label=""
          errorConditional={tol.plus ? !validateNumber(tol.plus) : false}
          errorText="Not a valid number"
          onChange={(e: any) => {
            e.preventDefault();
            change({
              section: title.toLowerCase(),
              action: 'input',
              index,
              value: e.target.value,
              input: 'plus',
            });
          }}
          onBlur={(e: any) => {
            e.preventDefault();
            if (!tol.level) {
              return;
            }
            if (!validateNumber(e.target.value)) {
              message.error('Please check that your tolerance is a valid number');
            }
            update({
              section: title.toLowerCase(),
              action: 'input',
              index,
              value: e.target.value,
              input: 'plus',
            });
          }}
          onFocus={selectContents}
        />
      </th>
      <th id={`${title}-${index}-minus`}>
        <EditTextInPlace
          name=""
          label=""
          value={tol.minus}
          errorConditional={tol.minus ? !validateNumber(tol.minus) : false}
          errorText="Not a valid number"
          onChange={(e: any) => {
            e.preventDefault();
            change({
              section: title.toLowerCase(),
              action: 'input',
              index,
              value: e.target.value,
              input: 'minus',
            });
          }}
          onBlur={(e: any) => {
            e.preventDefault();
            if (!tol.level) {
              return;
            }
            if (!validateNumber(e.target.value)) {
              message.error('Please check that your tolerance is a valid number');
            }
            update({
              section: title.toLowerCase(),
              action: 'input',
              index,
              value: e.target.value,
              input: 'minus',
            });
          }}
          onFocus={selectContents}
        />
      </th>
      <th id={`${title}-${index}-precision-remove`}>
        <RemoveOptionButton
          remove={(e: any) => {
            e.preventDefault();
            update({ section: title.toLowerCase(), action: 'remove', index });
          }}
        />
      </th>
    </tr>
  );
};

const RangeToleranceRow = ({ title, presetName, tol, index, type, rangeTols, precisionTols, update, change }: ToleranceRowProps) => {
  const selectContents = (e: any) => {
    e.target.select();
  };
  return (
    <tr key={`${title}-${index}-${presetName}-${rangeTols.length}`}>
      <th id={`${title}-${index}-range`}>
        <div className={`range-inputs ${tol.rangeLow.length > 5 || tol.rangeHigh.length > 5 ? 'fullWidth' : ''}`}>
          <EditTextInPlace
            name=""
            label=""
            value={tol.rangeLow}
            errorConditional={tol.rangeLow ? !validateNumber(tol.rangeLow) : false}
            errorText="Not a valid number"
            onChange={(e: any) => {
              e.preventDefault();
              change({
                section: title.toLowerCase(),
                action: 'input',
                index,
                value: e.target.value,
                input: 'rangeLow',
              });
            }}
            onBlur={(e: any) => {
              e.preventDefault();
              if (!validateNumber(e.target.value)) {
                message.error('Please check that your tolerance is a valid number');
              }
              update({
                section: title.toLowerCase(),
                action: 'input',
                index,
                value: e.target.value,
                input: 'rangeLow',
              });
            }}
            onFocus={selectContents}
          />
          <span className="range-divider">-</span>
          <EditTextInPlace
            id={index === 0 ? `${title}-RangeTolerance-first` : undefined}
            name=""
            label=""
            value={tol.rangeHigh}
            errorConditional={tol.rangeHigh ? !validateNumber(tol.rangeHigh) : false}
            errorText="Not a valid number"
            onChange={(e: any) => {
              e.preventDefault();
              change({
                section: title.toLowerCase(),
                action: 'input',
                index,
                value: e.target.value,
                input: 'rangeHigh',
              });
            }}
            onBlur={(e: any) => {
              e.preventDefault();
              if (!validateNumber(e.target.value)) {
                message.error('Please check that your tolerance is a valid number');
              }
              update({
                section: title.toLowerCase(),
                action: 'input',
                index,
                value: e.target.value,
                input: 'rangeHigh',
              });
            }}
            onFocus={selectContents}
          />
        </div>
      </th>
      <th id={`${title}-${index}-plus`}>
        <EditTextInPlace
          name=""
          label=""
          value={tol.plus}
          errorConditional={tol.plus ? !validateNumber(tol.plus) : false}
          errorText="Not a valid number"
          onChange={(e: any) => {
            e.preventDefault();
            change({
              section: title.toLowerCase(),
              action: 'input',
              index,
              value: e.target.value,
              input: 'plus',
            });
          }}
          onBlur={(e: any) => {
            e.preventDefault();
            if (!validateNumber(e.target.value)) {
              message.error('Please check that your tolerance is a valid number');
            }
            update({
              section: title.toLowerCase(),
              action: 'input',
              index,
              value: e.target.value,
              input: 'plus',
            });
          }}
          onFocus={selectContents}
        />
      </th>
      <th id={`${title}-${index}-minus`}>
        <EditTextInPlace
          name=""
          label=""
          key=""
          value={tol.minus}
          errorConditional={tol.minus ? !validateNumber(tol.minus) : false}
          errorText="Not a valid number"
          onChange={(e: any) => {
            e.preventDefault();
            change({
              section: title.toLowerCase(),
              action: 'input',
              index,
              value: e.target.value,
              input: 'minus',
            });
          }}
          onBlur={(e: any) => {
            e.preventDefault();
            if (!validateNumber(e.target.value)) {
              message.error('Please check that your tolerance is a valid number');
            }
            update({
              section: title.toLowerCase(),
              action: 'input',
              index,
              value: e.target.value,
              input: 'minus',
            });
          }}
          onFocus={selectContents}
        />
      </th>
      <th id={`${title}-${index}-range-remove`}>
        <RemoveOptionButton
          remove={(e: any) => {
            e.preventDefault();
            update({ section: title.toLowerCase(), action: 'remove', index });
          }}
        />
      </th>
    </tr>
  );
};

export const ToleranceRow = ({ title, presetName, tol, index, type, rangeTols, precisionTols, update, change }: ToleranceRowProps) => {
  return (
    <>
      {(type as string) === 'PrecisionTolerance' ? (
        <PrecisionToleranceRow title={title} presetName={presetName} tol={tol} type={type} index={index} rangeTols={rangeTols} precisionTols={precisionTols} update={update} change={change} />
      ) : (
        <RangeToleranceRow title={title} presetName={presetName} tol={tol} type={type} index={index} rangeTols={rangeTols} precisionTols={precisionTols} update={update} change={change} />
      )}
    </>
  );
};
