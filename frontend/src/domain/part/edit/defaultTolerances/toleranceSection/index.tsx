import React from 'react';
import { PrecisionTolerance, RangeTolerance } from 'types/tolerances';
import RadioGroup from 'antd/lib/radio/group';
import { Select } from 'antd';
import AddOptionButton from 'styleguide/Buttons/AddOption';
import { ToleranceTable } from './ToleranceTable';

const { Option } = Select;

interface ListItem {
  id: string;
  value: string;
}

interface ToleranceSectionProps {
  title: string;
  presetName: string;
  type: 'PrecisionTolerance' | 'RangeTolerance';
  units: {
    linear: ListItem[];
    angular: ListItem[];
  };
  rangeTols: RangeTolerance[];
  precisionTols: PrecisionTolerance[];
  unit: string;
  update: ({ section, action, value, index, input }: { section: string; action: string; value?: string; index?: number; input?: string }) => void;
  change: ({ section, action, value, index, input }: { section: string; action: string; value: string; index: number; input: string }) => void;
}

export const ToleranceSection = ({ title, presetName, type, units, rangeTols, precisionTols, unit, update, change }: ToleranceSectionProps) => {
  return (
    <section id={`${title}-section`} className="tolerance-section">
      <label id={`${title}-type-selector`} className="stretch-label">
        <span>Apply by:</span>
        <RadioGroup
          value={type}
          onChange={(e: any) => {
            e.preventDefault();
            update({ section: title.toLowerCase(), action: 'toggle' });
          }}
          options={[
            { label: 'Precision', value: 'PrecisionTolerance' },
            { label: 'Range', value: 'RangeTolerance' },
          ]}
        />
      </label>
      <ToleranceTable title={title} presetName={presetName} type={type} rangeTols={rangeTols} precisionTols={precisionTols} update={update} change={change} />
      <div className="tolerance-actions">
        <AddOptionButton
          add={(e: any) => {
            e.preventDefault();
            update({ section: title.toLowerCase(), action: 'add' });
          }}
          option="Tolerance"
        />
        <div id={`${title}-unit`} className="unit-row">
          Unit:
          <Select
            className="unit-selector"
            value={unit}
            onChange={(value: string) => {
              update({ section: title.toLowerCase(), action: 'unit', value });
            }}
          >
            {title === 'Angular' &&
              units.angular.map((item: ListItem) => (
                <Option key={item.id} value={item.id}>
                  {item.value}
                </Option>
              ))}
            {title === 'Linear' &&
              units.linear.map((item: ListItem) => (
                <Option key={item.id} value={item.id}>
                  {item.value}
                </Option>
              ))}
          </Select>
        </div>
      </div>
    </section>
  );
};
