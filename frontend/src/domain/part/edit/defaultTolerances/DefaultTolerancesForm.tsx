import React, { useState, useEffect } from 'react';
import { Tolerance } from 'types/tolerances';
import { defaultUnitIDs } from 'view/global/defaults';
import { i18n } from 'i18n';
import { cloneDeep } from 'lodash';
import PanelHeader from 'styleguide/PanelHeader/PanelHeader';
import { ProgressFooter } from 'styleguide/ProgressionFooter';
import Analytics from 'modules/shared/analytics/analytics';
import { useDispatch, useSelector } from 'react-redux';
import { AppState } from 'modules/state';
import config from 'config';
import { PartEditorActions, updateDrawingSheetThunk } from 'modules/partEditor/partEditorActions';
import { updatePartThunk } from 'modules/part/partActions';
import { getPresetSelectOptions, currentSettingMatchesPreset } from 'utils/PresetUtils';
import { Setting } from 'domain/setting/settingTypes';
import { Settings } from 'domain/setting/settingApi';
import { SelectSave } from 'view/global/inputs/SelectSave';
import { IxButton } from 'styleguide';
import { Characteristic } from 'types/characteristics';
import { Collapse, Popconfirm, Radio } from 'antd';
import { CharacteristicActions } from 'modules/characteristic/characteristicActions';
import { updateSettingThunk } from 'modules/session/sessionActions';
import { PartStepEnum } from 'modules/navigation/navigationActions';
import { checkUpdate, changeUnit, convertTolerance, changeToleranceInput, buildNewTolerances, updateDefaultTolerances, rebuildToleranceSettingObject, isMetric, allOtherSheetsAreDefault } from './TolerancesLogic';
import { TolerancePresetModal } from './TolerancePresetModal';
import { ToleranceSection } from './toleranceSection';
import './toleranceStyles.less';

const { Panel } = Collapse;

const { TOLS, GRID } = PartStepEnum;

interface Props {
  back: () => void;
  next: () => void;
}

interface ListItem {
  id: string;
  value: string;
}

export const DefaultTolerancesForm: React.FC<Props> = ({ back, next }) => {
  const navigation = useSelector((state: AppState) => state.navigation);
  const { characteristics } = useSelector((state: AppState) => state.characteristics);
  const { part } = useSelector((state: AppState) => state.part);
  const { presets, partDrawing, partDrawingSheet, tolerances } = useSelector((state: AppState) => state.partEditor);
  const dispatch = useDispatch();

  const { settings, metricUnits, imperialUnits } = useSelector((state: AppState) => state.session);

  const [updatedFeatures, setUpdatedFeatures] = useState<Characteristic[]>([]);
  const [unverified, setUnverified] = useState(0);

  const [showModal, setShowModal] = useState(false);

  const [newChanges, setChanged] = useState(false);

  const [openConfigPanels, setOpenConfigPanels] = useState<string[]>(['linear-tols']);
  const handleTogglePanel = (key: any) => {
    setOpenConfigPanels(key);
  };

  // Update Units
  const [units, setUnits] = useState<{
    linear: ListItem[];
    angular: ListItem[];
  }>({ linear: [], angular: [] });
  useEffect(() => {
    const linear: ListItem[] = [];
    const angular: ListItem[] = [];
    if (part && metricUnits && imperialUnits) {
      let defaultUnits;
      if (part.measurement === 'Metric') {
        defaultUnits = Object.values(metricUnits);
      } else {
        defaultUnits = Object.values(imperialUnits);
      }

      defaultUnits.forEach((unit) => {
        if (unit.meta?.measurement.includes('angle')) {
          angular.push({ id: unit.id, value: unit.meta?.unit || unit.value });
        } else if (unit.meta?.measurement.includes('length')) {
          linear.push({ id: unit.id, value: unit.meta?.unit || unit.value });
        }
      });
      setUnits({ linear, angular });
    }
    // Match default linear unit to part measurement
    if (part?.measurement === 'Metric' && tolerances.linear.unit === defaultUnitIDs.inch) {
      dispatch({ type: PartEditorActions.SET_PART_EDITOR_TOLERANCES, payload: { ...tolerances, linear: { ...tolerances.linear, unit: defaultUnitIDs.millimeter } } });
    } else if (part?.measurement === 'Imperial' && tolerances.linear.unit === defaultUnitIDs.millimeter) {
      dispatch({ type: PartEditorActions.SET_PART_EDITOR_TOLERANCES, payload: { ...tolerances, linear: { ...tolerances.linear, unit: defaultUnitIDs.inch } } });
    }
  }, [part?.measurement, metricUnits, imperialUnits]);

  // Select first tolerance input on load, for easy editing
  useEffect(() => {
    const firstInput = document.getElementById(`Linear-${tolerances.linear.type}-first`) as HTMLInputElement;
    if (!firstInput) {
      return;
    }
    firstInput.focus();
    firstInput.select();
    setChanged(false);
    setUpdatedFeatures([]);
  }, [partDrawingSheet?.id]);

  useEffect(() => {
    if (characteristics && characteristics.length > 0 && updatedFeatures && updatedFeatures.length > 0) {
      let newFeatures = [...updatedFeatures];
      newFeatures = newFeatures.filter((char) => {
        const newChar = characteristics.find((c) => c.id === char.id);
        if (!newChar || (newChar.nominal === char.nominal && newChar.plusTol === char.plusTol && newChar.minusTol === char.minusTol && newChar.lowerSpecLimit === char.lowerSpecLimit && newChar.upperSpecLimit === char.upperSpecLimit)) {
          return false;
        }
        return true;
      });
      setUpdatedFeatures(newFeatures);
    }
  }, [characteristics]);

  // Toggle Preset Save Button
  const [disablePresetButton, setDisablePresetButton] = useState(false);
  useEffect(() => {
    const currentPreset = presets?.tolerances || 'Default';
    const defaultTolerances = rebuildToleranceSettingObject({ currentPreset, linear: partDrawingSheet?.defaultLinearTolerances, angular: partDrawingSheet?.defaultAngularTolerances });
    const doesMatchPreset = currentSettingMatchesPreset(currentPreset, settings.tolerancePresets, defaultTolerances);
    if (doesMatchPreset !== disablePresetButton) {
      setDisablePresetButton(doesMatchPreset);
    }
  }, [partDrawingSheet?.defaultLinearTolerances, partDrawingSheet?.defaultAngularTolerances, presets?.tolerances, settings.tolerancePresets]);

  // Enforce that there will be a part
  if (!part || !partDrawingSheet) {
    return <div>No Part</div>;
  }

  // On Keystroke
  const change = ({ section, value, index, input }: { section: string; action: string; value: string; index: number; input: string }) => {
    // This fires on every keystroke inside an input, so not where we want to fire changed events
    let newTolerances: Tolerance = cloneDeep(tolerances);
    newTolerances = changeToleranceInput(newTolerances, index, section, input, value);
    dispatch({ type: PartEditorActions.SET_PART_EDITOR_TOLERANCES, payload: newTolerances });
  };

  const checkForFeaturesToUpdate = (newTolerances: Tolerance, all = false) => {
    if (characteristics.length === 0) {
      return;
    }
    let { updatedFeatures: changedFeatures } = updateDefaultTolerances(newTolerances, characteristics, false);
    if (changedFeatures.length > 0) {
      if (!all) {
        changedFeatures = changedFeatures.filter((char) => char.drawingSheet.id === partDrawingSheet.id);
      }
      setUpdatedFeatures(changedFeatures);
    } else {
      setUpdatedFeatures([]);
    }
  };

  const handleApplyAll = async () => {
    setChanged(false);
    if (!partDrawing) {
      return;
    }
    await Promise.all(
      partDrawing.sheets.map((sheet) => {
        if (sheet.id === partDrawingSheet.id) {
          return null;
        }
        return dispatch(updateDrawingSheetThunk(sheet.pageIndex, { defaultAngularTolerances: partDrawingSheet.defaultAngularTolerances, defaultLinearTolerances: partDrawingSheet.defaultLinearTolerances }));
      }),
    );
    checkForFeaturesToUpdate(tolerances, true);
  };

  // On Blur
  const update = ({ section, action, value, index, input }: { section: string; action: string; value?: string; index?: number; input?: string }) => {
    const { newTolerances, valid } = buildNewTolerances({ section, action, value, index, input, tolerances });

    // Fire metrics event if there was no value to change or if there was a value and it changed
    Analytics.track({
      event: Analytics.events.partTolerancesUpdated,
      part,
      drawing: partDrawing,
      properties: {
        'form.action': action,
        'form.section': section,
        'form.index': index,
        'form.field': input,
        // 'form.field.from': fromValue,
        'form.field.to': value,
      },
    });

    if (valid && checkUpdate(newTolerances)) {
      const stringedTolerances = convertTolerance(
        {
          defaultLinearTolerances: partDrawingSheet.defaultLinearTolerances,
          defaultAngularTolerances: partDrawingSheet.defaultAngularTolerances,
        },
        newTolerances,
        'both',
      );

      if (stringedTolerances.defaultAngularTolerances && stringedTolerances.defaultLinearTolerances) {
        dispatch(updateDrawingSheetThunk(partDrawingSheet.pageIndex, { defaultAngularTolerances: stringedTolerances.defaultAngularTolerances, defaultLinearTolerances: stringedTolerances.defaultLinearTolerances }));
        checkForFeaturesToUpdate(newTolerances);
        if (!newChanges) {
          setChanged(true);
        }
      }
    } else {
      dispatch({ type: PartEditorActions.SET_PART_EDITOR_TOLERANCES, payload: newTolerances });
    }
  };

  // If values are valid, update max stage and submit
  const updateMaxStage = () => {
    const max = Math.max(part?.workflowStage || 0, navigation.maxPartStep, GRID);
    dispatch(updatePartThunk({ workflowStage: max }, part.id));
  };

  const unsetInvalid = async (nav: 'next' | 'back') => {
    if (!checkUpdate(tolerances)) {
      const settingsDefaultTolerance = settings.tolerancePresets?.metadata[presets?.tolerances || 'Default']?.setting as Tolerance | undefined;
      const newTolerances = rebuildToleranceSettingObject({ currentPreset: 'Default', linear: partDrawingSheet.defaultLinearTolerances, angular: partDrawingSheet.defaultAngularTolerances, settings: settingsDefaultTolerance });
      if (newTolerances.linear.unit === defaultUnitIDs.inch && part?.measurement === 'Metric') {
        // Fix default unit, since linear defaults to Imperial measurement
        newTolerances.linear.unit = defaultUnitIDs.millimeter;
      }
      await dispatch({ type: PartEditorActions.SET_PART_EDITOR_TOLERANCES, payload: newTolerances });
    }
    if (nav === 'next') {
      if (allOtherSheetsAreDefault(partDrawingSheet.pageIndex, partDrawing?.sheets)) {
        // If user only changed first page, assume they didn't know it was per sheet and assign tolerances to all sheets
        await handleApplyAll();
      }
      updateMaxStage();
      next();
    } else {
      back();
    }
  };

  const updatePreset = (newTolerances: any) => {
    if (settings.tolerancePresets && settings.tolerancePresets.id) {
      const settingId = settings.tolerancePresets.id;
      const updatedSetting: Setting = { ...settings.tolerancePresets, id: settingId, metadata: JSON.stringify(newTolerances) };
      dispatch(updateSettingThunk(Settings.createGraphqlObject(updatedSetting), updatedSetting.id));
    }
  };

  const handleChangeMeasurement = (value: string) => {
    const tempTols = { ...tolerances };
    tempTols.linear.unit = value === 'Metric' ? defaultUnitIDs.millimeter : defaultUnitIDs.inch;
    if (characteristics && characteristics.length > 0) {
      const { updatedFeatures: changedFeatures } = updateDefaultTolerances(tempTols, characteristics);
      if (changedFeatures && changedFeatures.length > 0) {
        dispatch({
          type: CharacteristicActions.EDIT_CHARACTERISTICS,
          payload: {
            updates: changedFeatures,
          },
        });
      }
    }
    let { defaultLinearTolerances } = part;
    if (value === 'Metric') {
      defaultLinearTolerances = JSON.stringify(changeUnit(tolerances, 'linear', defaultUnitIDs.millimeter).linear);
    } else if (value === 'Imperial') {
      defaultLinearTolerances = JSON.stringify(changeUnit(tolerances, 'linear', defaultUnitIDs.inch).linear);
    }
    if (part) dispatch(updatePartThunk({ measurement: value, defaultLinearTolerances: defaultLinearTolerances || undefined }, part.id));
  };

  const handleChangeTolerancesPreset = (value: string) => {
    const newPreset = settings.tolerancePresets?.metadata[value];
    if (newPreset && newPreset.setting) {
      const newTolerances = newPreset.setting as Tolerance;
      dispatch({ type: PartEditorActions.SET_PART_EDITOR_STATE, payload: { tolerances: newTolerances, presets: { ...presets, tolerances: value } } });
      dispatch(
        updatePartThunk(
          {
            defaultLinearTolerances: JSON.stringify(newPreset.setting.linear),
            measurement: isMetric(tolerances, metricUnits) ? 'Metric' : 'Imperial',
            defaultAngularTolerances: JSON.stringify(newPreset.setting.angular),
            presets: JSON.stringify({ ...presets, tolerances: value }),
          },
          part.id,
        ),
      );
      checkForFeaturesToUpdate(tolerances);
    }
  };

  const handleCreatePreset = (newPresetName: string) => {
    const newTolerances = settings.tolerancePresets ? { ...settings.tolerancePresets.metadata } : {};
    newTolerances[newPresetName] = { customers: part.customer?.id ? [part.customer.id] : [], setting: tolerances || {} };
    updatePreset(newTolerances);
    setShowModal(false);
    dispatch({ type: PartEditorActions.SET_PART_EDITOR_PRESETS, payload: { ...presets, tolerances: newPresetName } });
    dispatch(updatePartThunk({ presets: JSON.stringify({ ...presets, tolerances: newPresetName }) }, part.id));
    Analytics.track({
      event: Analytics.events.createdStylePresetFromPart,
      preset: {
        name: newPresetName,
        ...newTolerances[newPresetName],
      },
    });
  };

  const handleUpdatePreset = () => {
    const newTolerances = { ...settings.tolerancePresets?.metadata } || {};
    newTolerances[presets?.tolerances || 'Default'].setting = tolerances || {};
    updatePreset(newTolerances);
    Analytics.track({
      event: Analytics.events.updatedTolerancePreset,
      preset: {
        name: presets?.tolerances,
        ...tolerances[presets?.tolerances || ''],
      },
    });
  };

  const handleSavePreset = (e: any) => {
    if (e.key === 'save') {
      setShowModal(true);
    } else {
      handleUpdatePreset();
    }
  };

  const handleUpdateFeatures = async () => {
    dispatch({
      type: CharacteristicActions.EDIT_CHARACTERISTICS,
      payload: {
        updates: updatedFeatures,
      },
    });
  };

  const handleConfirmUpdate = () => {
    setUnverified(0);
    handleUpdateFeatures();
  };

  const handleCancelUpdate = () => {
    setUnverified(0);
  };

  const handleClickedUpdate = () => {
    const verified = updatedFeatures.filter((feat) => {
      const original = characteristics.find((char) => char.id === feat.id);
      if (original && original.verified !== feat.verified) return true;
      return false;
    });
    if (verified.length > 0) {
      // show confirmation by setting state var to #
      setUnverified(verified.length);
    } else {
      handleUpdateFeatures();
    }
  };

  const maxTotalSteps = settings.autoballoonEnabled?.value === 'true' ? config.autoBallooning.maxTotalSteps : config.autoBallooning.maxTotalSteps - 1;

  return (
    <>
      <section id="defaultTolerances" className="side-panel-content">
        <PanelHeader header={`${TOLS + 1}. ${i18n('entities.part.title.tolerances')}`} copy={i18n('entities.part.instructions.tolerances')} info={i18n('entities.part.instructions.tolerancesHint')} dataCy="part_tols_" />
        <TolerancePresetModal showModal={showModal} presets={settings.tolerancePresets} handleCreatePreset={handleCreatePreset} setShowModal={setShowModal} />
        <span className="label-text" data-cy="preset-select-label">
          {`${i18n('settings.custom.presets.tolerances.tolerancePreset')}:`}
        </span>
        <SelectSave
          disabled={disablePresetButton}
          field="tolerances"
          options={getPresetSelectOptions(settings.tolerancePresets, part.customer)}
          value={presets?.tolerances || 'Default'}
          handleSave={handleSavePreset}
          handleOnChange={handleChangeTolerancesPreset}
        />
        <span className="input-label">{i18n('wizard.toleranceForm.system')}</span>
        <Radio.Group
          onChange={(e: any) => {
            handleChangeMeasurement(e.target.value);
          }}
          value={part.measurement}
        >
          <Radio key="Metric" id="radio-metric" value="Metric">
            <span id="radio-metric-label" data-cy="radio-metric-label">
              {i18n('common.metric')}
            </span>
          </Radio>
          <Radio key="Imperial" id="radio-imperial" value="Imperial">
            <span id="radio-imperial-label" data-cy="radio-imperial-label">
              {i18n('common.imperial')}
            </span>
          </Radio>
        </Radio.Group>
        <Collapse accordion className="tols-collapse" data-cy="tols-collapse" activeKey={openConfigPanels} onChange={handleTogglePanel} ghost>
          <Panel className="tols-collapse-panel" header={<span className="panel-header">Linear</span>} key="linear-tols">
            <ToleranceSection
              title={i18n('wizard.toleranceForm.linear')}
              presetName={presets?.tolerances || ''}
              type={tolerances.linear.type}
              rangeTols={tolerances.linear.rangeTols}
              precisionTols={tolerances.linear.precisionTols}
              unit={tolerances.linear.unit}
              units={units}
              update={update}
              change={change}
            />
          </Panel>
          <Panel className="tols-collapse-panel" header={<span className="panel-header">Angular</span>} key="angular-tols">
            <ToleranceSection
              title={i18n('wizard.toleranceForm.angular')}
              presetName={presets?.tolerances || ''}
              type={tolerances.angular.type}
              rangeTols={tolerances.angular.rangeTols}
              precisionTols={tolerances.angular.precisionTols}
              unit={tolerances.angular.unit}
              units={units}
              update={update}
              change={change}
            />
          </Panel>
        </Collapse>
        <div className="form-buttons stack-buttons">
          {partDrawing?.sheets && partDrawing.sheets.length > 1 && (
            <IxButton
              dataCy="apply-all"
              text={i18n('wizard.toleranceForm.applyAll')}
              tipText={!newChanges ? i18n('wizard.toleranceForm.noChange') : undefined}
              onClick={handleApplyAll}
              className="btn"
              disabled={!newChanges}
              style={{ width: '100%' }}
            />
          )}
          <Popconfirm title={i18n('wizard.toleranceForm.updateWarn', updatedFeatures.length)} visible={unverified > 0} onConfirm={handleConfirmUpdate} onCancel={handleCancelUpdate}>
            <IxButton
              dataCy="update-features"
              text={i18n('wizard.toleranceForm.updateFeatures', updatedFeatures.length)}
              tipText={updatedFeatures.length === 0 ? i18n('wizard.toleranceForm.noChange') : undefined}
              onClick={handleClickedUpdate}
              className="btn-primary"
              disabled={updatedFeatures.length === 0}
              style={{ width: '100%' }}
            />
          </Popconfirm>
        </div>
      </section>
      <div className="form-buttons form-section sider-panel-progress-footer">
        <ProgressFooter
          id="default-tolerances-footer"
          classes="panel-progression-footer"
          cypress="progress-footer"
          steps={maxTotalSteps}
          current={TOLS + 1}
          onNext={() => unsetInvalid('next')}
          canProgress
          canBack
          onBack={() => unsetInvalid('back')}
        />
      </div>
    </>
  );
};
