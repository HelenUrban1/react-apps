import { cloneDeep } from 'lodash';
import { defaultTypeIDs, defaultSubtypeIDs, defaultMethodIDs, defaultUnitIDs, DefaultTolerances } from 'view/global/defaults';
import log from 'modules/shared/logger';
import {
  convertTolerance,
  validateLevel,
  validateTolerance,
  validateSection,
  checkUpdate,
  toggleToleranceType,
  changePrecisionLevel,
  removeTolerance,
  addTolerance,
  changeToleranceInput,
  changeUnit,
  updateDefaultTolerances,
  buildNewTolerances,
  isChanged,
  rebuildToleranceSettingObject,
  isMetric,
  allOtherSheetsAreDefault,
} from './TolerancesLogic';

const warningSpy = jest.spyOn(log, 'warn');

describe('Tolerance Utilities', () => {
  beforeEach(() => {
    // Reset Spy Count
    warningSpy.mockReset();
  });
  const initialDefaults = { ...DefaultTolerances };

  const initialFeatures = [
    {
      id: 'e0a01769-c8ed-4090-bd24-9c1b2241f123',
      status: 'AutoInactive',
      drawingPageIndex: 0,
      notationType: defaultTypeIDs.Dimension, // Dimension
      notationSubtype: defaultSubtypeIDs.Curvilinear, // Curvilinear
      notationClass: 'Tolerance',
      toleranceSource: 'Default_Tolerance',
      fullSpecification: '1.25',
      quantity: 1,
      nominal: '1.25',
      upperSpecLimit: '1.26',
      lowerSpecLimit: '1.24',
      plusTol: '0.1',
      minusTol: '0.1',
      unit: defaultUnitIDs.millimeter,
      criticality: false,
      inspectionMethod: defaultMethodIDs.Caliper, // Caliper
      notes: '',
      verified: true,
      drawingRotation: 0.0,
      drawingScale: 1.0,
      boxLocationY: 20.5,
      boxLocationX: 20.5,
      boxWidth: 250,
      boxHeight: 100,
      boxRotation: 0.0,
      markerGroup: '1',
      markerIndex: 0,
      markerSubIndex: null,
      markerLabel: '1',
      markerStyle: 'aad19683-337a-4039-a910-98b0bdd5ee57',
      markerLocationX: 20,
      markerLocationY: 20,
      markerFontSize: 18,
      gridCoordinates: null,
      createdAt: '2020-03-04 09:06:09.886657+00',
      updatedAt: '2020-03-04 09:06:09.886657+00',
      partId: 'd6c5bc49-9fb2-4fbb-8a61-77bc8d28036f', // belongsTo
      drawingId: 'c200ce70-11b8-44f7-8dff-4b93f6d1b491', // belongsTo
      drawingPageId: 'd6c5bc49-9fb2-4f1b-8a61-7abc8d380352',
    },
    {
      id: 'e0a01769-c8ed-4090-bd24-9c1b2241f124',
      drawingPageIndex: 0,
      status: 'AutoInactive',
      notationType: defaultTypeIDs.Dimension, // Dimension
      notationSubtype: defaultSubtypeIDs.Angle, // Angle
      notationClass: 'Tolerance',
      toleranceSource: 'Document_Defined',
      fullSpecification: '1.25 +/- 0.2',
      quantity: 5,
      nominal: '1.25',
      upperSpecLimit: '1.27',
      lowerSpecLimit: '1.23',
      plusTol: '0.2',
      minusTol: '0.2',
      unit: defaultUnitIDs.degree,
      criticality: false,
      verified: true,
      inspectionMethod: defaultMethodIDs.Caliper, // Caliper
      notes: '',
      drawingRotation: 0.0,
      drawingScale: 1.0,
      boxLocationY: 20.5,
      boxLocationX: 20.5,
      boxWidth: 250,
      boxHeight: 100,
      boxRotation: 0.0,
      markerGroup: '2',
      markerIndex: 1,
      markerSubIndex: 0,
      markerLabel: '2.1',
      markerStyle: '8d1a8859-c1bc-4c90-b7c6-2bf1640856ac',
      markerLocationX: 20,
      markerLocationY: 20,
      markerFontSize: 18,
      gridCoordinates: null,
      createdAt: '2020-03-04 09:06:09.886657+00',
      updatedAt: '2020-03-04 09:06:09.886657+00',
      partId: 'd6c5bc49-9fb2-4fbb-8a61-77bc8d28036f', // belongsTo
      drawingId: 'c200ce70-11b8-44f7-8dff-4b93f6d1b491', // belongsTo
      drawingPageId: 'd6c5bc49-9fb2-4f1b-8a61-7abc8d380352',
    },
  ];

  describe('convertTolerance', () => {
    const initialLinear =
      '{"type":"PrecisionTolerance","rangeTols":[{"rangeLow":"1","rangeHigh":"9","plus":"1","minus":"-1"},{"rangeLow":"0.1","rangeHigh":"0.9","plus":"0.1","minus":"-0.1"},{"rangeLow":"0.01","rangeHigh":"0.09","plus":"0.01","minus":"-0.01"},{"rangeLow":"0.001","rangeHigh":"0.009","plus":"0.001","minus":"-0.001"},{"rangeLow":"0.0001","rangeHigh":"0.0009","plus":"0.0001","minus":"-0.0001"}],"precisionTols":[{"level":"X","plus":"1","minus":"-1"},{"level":"X.X","plus":"0.1","minus":"-0.1"},{"level":"X.XX","plus":"0.01","minus":"-0.01"},{"level":"X.XXX","plus":"0.001","minus":"-0.001"},{"level":"X.XXXX","plus":"0.0001","minus":"-0.0001"}],"unit":"b909e87e-0445-4ea4-857e-3c1dd31b530b"}';
    const initialAngular =
      '{"type":"PrecisionTolerance","rangeTols":[{"rangeLow":"1","rangeHigh":"9","plus":"1","minus":"-1"},{"rangeLow":"0.1","rangeHigh":"0.9","plus":"0.1","minus":"-0.1"},{"rangeLow":"0.01","rangeHigh":"0.09","plus":"0.01","minus":"-0.01"},{"rangeLow":"0.001","rangeHigh":"0.009","plus":"0.001","minus":"-0.001"},{"rangeLow":"0.0001","rangeHigh":"0.0009","plus":"0.0001","minus":"-0.0001"}],"precisionTols":[{"level":"X","plus":"1","minus":"-1"},{"level":"X.X","plus":"0.1","minus":"-0.1"},{"level":"X.XX","plus":"0.01","minus":"-0.01"},{"level":"X.XXX","plus":"0.001","minus":"-0.001"},{"level":"X.XXXX","plus":"0.0001","minus":"-0.0001"}],"unit":"e1150ac6-6803-41fa-b525-9b8aba2eccb2"}';
    const oldTolerances = {
      defaultLinearTolerances: initialLinear,
      defaultAngularTolerances: initialAngular,
    };
    const newTolerances = {
      linear: {
        ...initialDefaults.linear,
        precisionTols: [...initialDefaults.linear.precisionTols],
        rangeTols: [...initialDefaults.linear.rangeTols],
      },
      angular: {
        ...initialDefaults.angular,
        precisionTols: [...initialDefaults.angular.precisionTols],
        rangeTols: [...initialDefaults.angular.rangeTols],
      },
    };
    newTolerances.linear.precisionTols[0] = { level: 'X.XXXXX', plus: '123', minus: '-123' };
    newTolerances.angular.precisionTols[0] = { level: 'X.XXXXX', plus: '123', minus: '-123' };
    const expectedLinear =
      '{"type":"PrecisionTolerance","rangeTols":[{"rangeLow":"1","rangeHigh":"9","plus":"1","minus":"-1"},{"rangeLow":"0.1","rangeHigh":"0.9","plus":"0.1","minus":"-0.1"},{"rangeLow":"0.01","rangeHigh":"0.09","plus":"0.01","minus":"-0.01"},{"rangeLow":"0.001","rangeHigh":"0.009","plus":"0.001","minus":"-0.001"},{"rangeLow":"0.0001","rangeHigh":"0.0009","plus":"0.0001","minus":"-0.0001"}],"precisionTols":[{"level":"X.XXXXX","plus":"123","minus":"-123"},{"level":"X.X","plus":"0.1","minus":"-0.1"},{"level":"X.XX","plus":"0.01","minus":"-0.01"},{"level":"X.XXX","plus":"0.001","minus":"-0.001"},{"level":"X.XXXX","plus":"0.0001","minus":"-0.0001"}],"unit":"b909e87e-0445-4ea4-857e-3c1dd31b530b"}';

    const expectedAngular =
      '{"type":"PrecisionTolerance","rangeTols":[{"rangeLow":"1","rangeHigh":"9","plus":"1","minus":"-1"},{"rangeLow":"0.1","rangeHigh":"0.9","plus":"0.1","minus":"-0.1"},{"rangeLow":"0.01","rangeHigh":"0.09","plus":"0.01","minus":"-0.01"},{"rangeLow":"0.001","rangeHigh":"0.009","plus":"0.001","minus":"-0.001"},{"rangeLow":"0.0001","rangeHigh":"0.0009","plus":"0.0001","minus":"-0.0001"}],"precisionTols":[{"level":"X.XXXXX","plus":"123","minus":"-123"},{"level":"X.X","plus":"0.1","minus":"-0.1"},{"level":"X.XX","plus":"0.01","minus":"-0.01"},{"level":"X.XXX","plus":"0.001","minus":"-0.001"},{"level":"X.XXXX","plus":"0.0001","minus":"-0.0001"}],"unit":"e1150ac6-6803-41fa-b525-9b8aba2eccb2"}';

    it('Converts linear tolerances to JSON string', () => {
      const linearString = convertTolerance(oldTolerances, newTolerances, 'linear');
      expect(linearString.defaultLinearTolerances).toBe(expectedLinear);
      expect(linearString.defaultAngularTolerances).toBe(initialAngular);
    });

    it('Converts angular tolerances to JSON string', () => {
      const angularString = convertTolerance(oldTolerances, newTolerances, 'angular');
      expect(angularString.defaultAngularTolerances).toBe(expectedAngular);
      expect(angularString.defaultLinearTolerances).toBe(initialLinear);
    });

    it('Converts both tolerances to JSON string', () => {
      const bothString = convertTolerance(oldTolerances, newTolerances, 'both');
      expect(bothString.defaultLinearTolerances).toBe(expectedLinear);
      expect(bothString.defaultAngularTolerances).toBe(expectedAngular);
    });

    it('returns original JSON string if missing a section', () => {
      const bothString = convertTolerance(oldTolerances, newTolerances, 'fake');
      expect(bothString.defaultLinearTolerances).toBe(initialLinear);
      expect(bothString.defaultAngularTolerances).toBe(initialAngular);
    });

    it('returns null if tolerances are unchanged', () => {
      const bothString = convertTolerance(oldTolerances, oldTolerances, 'both');
      expect(bothString.defaultLinearTolerances).toBe(undefined);
      expect(bothString.defaultAngularTolerances).toBe(undefined);
    });
  });

  describe('validateLevel', () => {
    it('validates true for an unused precision level', () => {
      expect(validateLevel(initialDefaults.linear.precisionTols, 'X.XXXXX')).toBeTruthy();
      expect(validateLevel(initialDefaults.angular.precisionTols, 'X.XXXXX')).toBeTruthy();
    });

    it('validates false for a used precision level', () => {
      expect(validateLevel(initialDefaults.linear.precisionTols, 'X')).toBeFalsy();
      expect(validateLevel(initialDefaults.linear.precisionTols, 'X.X')).toBeFalsy();
      expect(validateLevel(initialDefaults.linear.precisionTols, 'X.XX')).toBeFalsy();
      expect(validateLevel(initialDefaults.linear.precisionTols, 'X.XXX')).toBeFalsy();
      expect(validateLevel(initialDefaults.linear.precisionTols, 'X.XXXX')).toBeFalsy();

      expect(validateLevel(initialDefaults.angular.precisionTols, 'X')).toBeFalsy();
      expect(validateLevel(initialDefaults.angular.precisionTols, 'X.X')).toBeFalsy();
      expect(validateLevel(initialDefaults.angular.precisionTols, 'X.XX')).toBeFalsy();
      expect(validateLevel(initialDefaults.angular.precisionTols, 'X.XXX')).toBeFalsy();
      expect(validateLevel(initialDefaults.angular.precisionTols, 'X.XXXX')).toBeFalsy();
    });
  });

  describe('validateTolerance', () => {
    it('validates true if entire tolerance is valid', () => {
      expect(validateTolerance({ level: 'X.XXXXX', plus: '123', minus: '-123' })).toBeTruthy();
      expect(validateTolerance({ level: 'X.XXXXX', plus: '0.123', minus: '-.123' })).toBeTruthy();
      expect(validateTolerance({ rangeHigh: '1', rangeLow: '9', plus: '1.23', minus: '-0.123' })).toBeTruthy();
      expect(validateTolerance({ rangeHigh: '.1', rangeLow: '0.9', plus: '123.0', minus: '-12.3' })).toBeTruthy();
      expect(validateTolerance(initialDefaults.linear.precisionTols[0])).toBeTruthy();
    });
    it('validates false if any portion of a tolerance is invalid', () => {
      expect(validateTolerance({ level: 'X', plus: '12O3', minus: '-l23' })).toBeFalsy();
      expect(validateTolerance({ level: 'X.XXXXX', plus: '123asdf', minus: '-123' })).toBeFalsy();
      expect(validateTolerance({ level: 'X.XXXXX', plus: '123', minus: 'dfgd' })).toBeFalsy();
      expect(validateTolerance({ rangeHigh: 'O', rangeLow: '9', plus: '123', minus: '-123' })).toBeFalsy();
      expect(validateTolerance({ rangeHigh: '1', rangeLow: '9aas', plus: '123', minus: '-123' })).toBeFalsy();
      expect(validateTolerance({ rangeHigh: 'a', rangeLow: '9', plus: '123', minus: '-123' })).toBeFalsy();
      expect(validateTolerance({ rangeHigh: '1', rangeLow: 'b', plus: '123', minus: '-123' })).toBeFalsy();
      expect(validateTolerance({ rangeHigh: '1', rangeLow: '9', plus: 'awd', minus: 'ass123' })).toBeFalsy();
      expect(validateTolerance({ rangeHigh: '1', rangeLow: '9', plus: '123', minus: '123dwwawd' })).toBeFalsy();
    });
  });

  describe('validateSection', () => {
    it('validates true if entire tolerance section is valid', () => {
      expect(validateSection(initialDefaults, 'linear')).toBeTruthy();
      expect(validateSection(initialDefaults, 'angular')).toBeTruthy();
    });

    it('validates false if any portion of a tolerance section is invalid', () => {
      const newTolerances = cloneDeep(initialDefaults);
      newTolerances.angular.precisionTols[0] = { level: 'ZZZZ', plus: 'AAAA', minus: 'GGGG' };
      newTolerances.linear.precisionTols[0] = { level: 'X', plus: 'l', minus: '|' };
      expect(validateSection(newTolerances, 'linear')).toBeFalsy();
      expect(validateSection(newTolerances, 'angular')).toBeFalsy();
      newTolerances.angular.precisionTols[0] = { level: 'X', plus: '1', minus: '1a' };
      newTolerances.linear.precisionTols[0] = { level: 'X', plus: 'l', minus: '1' };
      expect(validateSection(newTolerances, 'linear')).toBeFalsy();
      expect(validateSection(newTolerances, 'angular')).toBeFalsy();
    });
  });

  describe('checkUpdate', () => {
    it('validates true if all tolerances are valid', () => {
      expect(checkUpdate(initialDefaults)).toBeTruthy();
    });

    it('validates false if any tolerance is invalid', () => {
      const newTolerances = cloneDeep(initialDefaults);
      newTolerances.angular.precisionTols[0] = { level: 'ZZZZ', plus: 'AAAA', minus: 'GGGG' };
      expect(checkUpdate(newTolerances)).toBeFalsy();
      newTolerances.angular.precisionTols[0] = { level: 'X', plus: '1', minus: '1' };
      newTolerances.linear.precisionTols[0] = { level: 'X', plus: 'l', minus: '|' };
      expect(checkUpdate(newTolerances)).toBeFalsy();
    });
  });

  describe('toggleToleranceType', () => {
    it('toggles tolerance type from precision to range', () => {
      expect(initialDefaults.linear.type).toBe('PrecisionTolerance');
      const newTolerances = toggleToleranceType(initialDefaults, 'linear');
      expect(newTolerances.linear.type).toBe('RangeTolerance');
    });

    it('toggles tolerance type from range to precision', () => {
      const immutableTolerance = cloneDeep(initialDefaults);
      immutableTolerance.angular.type = 'RangeTolerance';
      expect(immutableTolerance.angular.type).toBe('RangeTolerance');
      const newTolerances = toggleToleranceType(immutableTolerance, 'angular');
      expect(newTolerances.angular.type).toBe('PrecisionTolerance');
    });
  });

  describe('changePrecisionLevel', () => {
    it('sets new precision level to the appropriate tolerance', () => {
      const newLinearTolerances = changePrecisionLevel(initialDefaults, 0, 'linear', 'X.XXXXX');
      expect(newLinearTolerances.linear.precisionTols[0].level).toBe('X.XXXXX');
      const newAngularTolerances = changePrecisionLevel(initialDefaults, 0, 'angular', 'ZZZZZ');
      expect(newAngularTolerances.angular.precisionTols[0].level).toBe('ZZZZZ');
    });

    it('warns if precision level was not able to be set', () => {
      const newLinearTolerances = changePrecisionLevel(initialDefaults, 12, 'ham', 'X.XXXXX');
      expect(newLinearTolerances).toMatchObject(initialDefaults);
      expect(warningSpy).toHaveBeenCalledTimes(1);
    });
  });

  describe('removeTolerance', () => {
    it('removes a precision tolerance from a section', () => {
      expect(initialDefaults.linear.precisionTols).toHaveLength(5);
      const newLinearTolerances = removeTolerance(initialDefaults, 'linear', 0);
      expect(newLinearTolerances.linear.precisionTols).toHaveLength(4);
      expect(newLinearTolerances.linear.precisionTols[0]).toMatchObject(initialDefaults.linear.precisionTols[1]);
      expect(initialDefaults.angular.precisionTols).toHaveLength(5);
      const newAngularTolerances = removeTolerance(initialDefaults, 'angular', 0);
      expect(newAngularTolerances.angular.precisionTols).toHaveLength(4);
      expect(newAngularTolerances.angular.precisionTols[0]).toMatchObject(initialDefaults.angular.precisionTols[1]);
    });
    it('removes a ranged tolerance from a section', () => {
      const testRanged = { ...initialDefaults, linear: { ...initialDefaults.linear, type: 'RangeTolerance' }, angular: { ...initialDefaults.angular, type: 'RangeTolerance' } };
      expect(testRanged.linear.rangeTols).toHaveLength(5);
      const newLinearTolerances = removeTolerance(testRanged, 'linear', 0);
      expect(newLinearTolerances.linear.rangeTols).toHaveLength(4);
      expect(newLinearTolerances.linear.rangeTols[0]).toMatchObject(testRanged.linear.rangeTols[1]);
      expect(testRanged.angular.rangeTols).toHaveLength(5);
      const newAngularTolerances = removeTolerance(testRanged, 'angular', 0);
      expect(newAngularTolerances.angular.rangeTols).toHaveLength(4);
      expect(newAngularTolerances.angular.rangeTols[0]).toMatchObject(testRanged.angular.rangeTols[1]);
    });
  });

  describe('addTolerance', () => {
    it('adds a precision tolerance to a section', () => {
      expect(initialDefaults.linear.precisionTols).toHaveLength(5);
      const newLinearTolerances = addTolerance(initialDefaults, 'linear', 0);
      expect(newLinearTolerances.linear.precisionTols).toHaveLength(6);
      expect(initialDefaults.angular.precisionTols).toHaveLength(5);
      const newAngularTolerances = addTolerance(initialDefaults, 'angular', 0);
      expect(newAngularTolerances.angular.precisionTols).toHaveLength(6);
    });
    it('adds a ranged tolerance to a section', () => {
      const testRanged = { ...initialDefaults, linear: { ...initialDefaults.linear, type: 'RangeTolerance' }, angular: { ...initialDefaults.angular, type: 'RangeTolerance' } };
      expect(testRanged.linear.rangeTols).toHaveLength(5);
      const newLinearTolerances = addTolerance(testRanged, 'linear', 0);
      expect(newLinearTolerances.linear.rangeTols).toHaveLength(6);
      expect(testRanged.angular.rangeTols).toHaveLength(5);
      const newAngularTolerances = addTolerance(testRanged, 'angular', 0);
      expect(newAngularTolerances.angular.rangeTols).toHaveLength(6);
    });
  });

  describe('changeToleranceInput', () => {
    it('sets a new positive tolerance', () => {
      const newLinearTolerances = changeToleranceInput(initialDefaults, 0, 'linear', 'plus', '12345');
      expect(newLinearTolerances.linear.precisionTols[0].plus).toBe('12345');
      const newAngularTolerances = changeToleranceInput(initialDefaults, 0, 'angular', 'plus', '12345');
      expect(newAngularTolerances.angular.precisionTols[0].plus).toBe('12345');
    });

    it('sets a new negative tolerance', () => {
      const newLinearTolerances = changeToleranceInput(initialDefaults, 0, 'linear', 'minus', '12345');
      expect(newLinearTolerances.linear.precisionTols[0].minus).toBe('12345');
      const newAngularTolerances = changeToleranceInput(initialDefaults, 0, 'angular', 'minus', '12345');
      expect(newAngularTolerances.angular.precisionTols[0].minus).toBe('12345');
    });

    it('sets a new high range tolerance', () => {
      const immutableTolerance = cloneDeep(initialDefaults);
      immutableTolerance.linear.type = 'RangeTolerance';
      immutableTolerance.angular.type = 'RangeTolerance';
      const newLinearTolerances = changeToleranceInput(immutableTolerance, 0, 'linear', 'rangeHigh', '12345');
      expect(newLinearTolerances.linear.rangeTols[0].rangeHigh).toBe('12345');
      const newAngularTolerances = changeToleranceInput(immutableTolerance, 0, 'angular', 'rangeHigh', '12345');
      expect(newAngularTolerances.angular.rangeTols[0].rangeHigh).toBe('12345');
    });

    it('sets a new low range tolerance', () => {
      const immutableTolerance = cloneDeep(initialDefaults);
      immutableTolerance.linear.type = 'RangeTolerance';
      immutableTolerance.angular.type = 'RangeTolerance';
      const newLinearTolerances = changeToleranceInput(immutableTolerance, 0, 'linear', 'rangeLow', '12345');
      expect(newLinearTolerances.linear.rangeTols[0].rangeLow).toBe('12345');
      const newAngularTolerances = changeToleranceInput(immutableTolerance, 0, 'angular', 'rangeLow', '12345');
      expect(newAngularTolerances.angular.rangeTols[0].rangeLow).toBe('12345');
    });
  });

  describe('changeUnit', () => {
    it('sets a new unit to a section', () => {
      const newLinearTolerances = changeUnit(initialDefaults, 'linear', '12345');
      expect(newLinearTolerances.linear.unit).toBe('12345');
      const newAngularTolerances = changeUnit(initialDefaults, 'angular', '12345');
      expect(newAngularTolerances.angular.unit).toBe('12345');
    });
  });

  describe('buildNewTolerances', () => {
    it('adds a new tolerance level', () => {
      const { newTolerances, valid } = buildNewTolerances({ section: 'linear', action: 'add', value: '1.5', index: 0, input: 'plus', tolerances: initialDefaults });
      expect(initialDefaults.linear.precisionTols).toHaveLength(5);
      expect(newTolerances.linear.precisionTols).toHaveLength(6);
      expect(valid).toBeFalsy();
    });
    it('changes the precision level', () => {
      const { newTolerances, valid } = buildNewTolerances({ section: 'linear', action: 'precision', value: '1.5', index: 0, input: 'plus', tolerances: initialDefaults });
      expect(newTolerances.linear.precisionTols[0].level).toBe('1.5');
      expect(valid).toBeTruthy();
    });
    it('changes the tolerance input', () => {
      const { newTolerances, valid } = buildNewTolerances({ section: 'linear', action: 'input', value: '1.5', index: 0, input: 'plus', tolerances: initialDefaults });
      expect(newTolerances.linear.precisionTols[0].plus).toBe('1.5');
      expect(valid).toBeTruthy();
    });
    it('changes the unit', () => {
      const { newTolerances, valid } = buildNewTolerances({ section: 'linear', action: 'unit', value: '1.5', index: 0, input: 'plus', tolerances: initialDefaults });
      expect(newTolerances.linear.unit).toBe('1.5');
      expect(valid).toBeTruthy();
    });
    it('removes a tolerance level', () => {
      const { newTolerances, valid } = buildNewTolerances({ section: 'linear', action: 'remove', value: '1.5', index: 0, input: 'plus', tolerances: initialDefaults });
      expect(initialDefaults.linear.precisionTols).toHaveLength(5);
      expect(newTolerances.linear.precisionTols).toHaveLength(4);
      expect(valid).toBeTruthy();
    });
    it('switches the tolerance type', () => {
      const { newTolerances, valid } = buildNewTolerances({ section: 'linear', action: 'toggle', value: '1.5', index: 0, input: 'plus', tolerances: initialDefaults });
      expect(initialDefaults.linear.type).toBe('PrecisionTolerance');
      expect(newTolerances.linear.type).toBe('RangeTolerance');
      expect(valid).toBeTruthy();
    });
    it('warns the user if unknown action is provided', () => {
      const { newTolerances, valid } = buildNewTolerances({ section: 'linear', action: 'default', value: '1.5', index: 0, input: 'plus', tolerances: initialDefaults });
      expect(newTolerances).toMatchObject(initialDefaults);
      expect(valid).toBeTruthy();
      expect(warningSpy).toHaveBeenCalledTimes(1);
    });
  });

  describe('isChanged', () => {
    it('returns true if empty string is replaced with null', () => {
      const changed = isChanged('', null);
      expect(changed).toBeTruthy();
    });

    it('returns true if the values are different', () => {
      const changed = isChanged(1, 2);
      expect(changed).toBeTruthy();
    });

    it('returns false if the values are the same', () => {
      const changed = isChanged('same', 'same');
      expect(changed).toBeFalsy();
    });

    it('returns false if null is replaced with empty string', () => {
      const changed = isChanged(null, '');
      expect(changed).toBeFalsy();
    });
  });

  describe('updateDefaultTolerances', () => {
    it('only updates features that use default tolerances', () => {
      const updatedFeatures = updateDefaultTolerances(DefaultTolerances, [{ ...initialFeatures[0] }, { ...initialFeatures[1] }], false);
      expect(updatedFeatures.updatedFeatures.length).toBe(1);
      expect(updatedFeatures.allFeatures.length).toBe(2);
    });

    it('only updates units', () => {
      const testTols = cloneDeep(DefaultTolerances);
      testTols.linear.unit = 'testingLin';
      testTols.angular.unit = 'testingAng';
      const updatedFeatures = updateDefaultTolerances(testTols, [{ ...initialFeatures[0] }, { ...initialFeatures[1] }], true);
      expect(updatedFeatures.updatedFeatures.length).toBe(2);
      expect(updatedFeatures.updatedFeatures[0].upperSpecLimit).toBe(initialFeatures[0].upperSpecLimit);
      expect(updatedFeatures.updatedFeatures[0].lowerSpecLimit).toBe(initialFeatures[0].lowerSpecLimit);
      expect(updatedFeatures.updatedFeatures[0].plusTol).toBe(initialFeatures[0].plusTol);
      expect(updatedFeatures.updatedFeatures[0].minusTol).toBe(initialFeatures[0].minusTol);
      expect(updatedFeatures.updatedFeatures[0].unit).toBe('testingLin');
      expect(updatedFeatures.updatedFeatures[1].upperSpecLimit).toBe(initialFeatures[1].upperSpecLimit);
      expect(updatedFeatures.updatedFeatures[1].lowerSpecLimit).toBe(initialFeatures[1].lowerSpecLimit);
      expect(updatedFeatures.updatedFeatures[1].plusTol).toBe(initialFeatures[1].plusTol);
      expect(updatedFeatures.updatedFeatures[1].minusTol).toBe(initialFeatures[1].minusTol);
      expect(updatedFeatures.updatedFeatures[1].unit).toBe('testingAng');
    });

    it('unverifies updated features', () => {
      const updatedFeatures = updateDefaultTolerances(DefaultTolerances, [{ ...initialFeatures[0] }, { ...initialFeatures[1] }], false);
      expect(updatedFeatures.updatedFeatures[0].verified).toBeFalsy();
      expect(updatedFeatures.allFeatures[1].verified).toBeTruthy();
    });
  });

  describe('rebuildToleranceSettingObject', () => {
    const fakeTols = { type: 'type', rangeTols: [], precisionTols: [], unit: 'unit' };
    const fakeJSON = JSON.stringify(fakeTols);
    it('returns an object from linear and angular tolerance stringified defaults ', () => {
      const toleranceObject = rebuildToleranceSettingObject({ currentPreset: 'default', linear: fakeJSON, angular: fakeJSON });
      expect(toleranceObject).toMatchObject({
        linear: fakeTols,
        angular: fakeTols,
      });
    });

    it('returns an object from the global defaults', () => {
      const toleranceObject = rebuildToleranceSettingObject({ currentPreset: 'default' });
      expect(toleranceObject).toMatchObject(initialDefaults);
    });

    it('returns an empty object if no preset is provided', () => {
      const toleranceObject = rebuildToleranceSettingObject({});
      expect(toleranceObject).toMatchObject({});
    });
  });

  describe('isMetric', () => {
    const fakeMetricUnits = { true: { id: 'true', value: 'true', default: true, listType: 'Unit_of_Measurement', index: 0, status: 'Active', deleted: false } };
    it('returns true if the unit is metric', () => {
      const metric = isMetric({ linear: { unit: 'true' } }, fakeMetricUnits);
      expect(metric).toBeTruthy();
    });

    it('returns false if the unit is not metric or not found', () => {
      const metric = isMetric({ linear: { unit: 'false' } }, fakeMetricUnits);
      expect(metric).toBeFalsy();
    });
  });

  describe('allOtherSheetsAreDefault', () => {
    const defaultSheet = { defaultAngularTolerances: JSON.stringify(DefaultTolerances.angular), defaultLinearTolerances: JSON.stringify(DefaultTolerances.linear) };
    const changedSheet = { defaultLinearTolerances: '{}', defaultAngularTolerances: '{}', pageIndex: 0 };
    const sheets = [changedSheet, { ...defaultSheet, pageIndex: 1 }, { ...defaultSheet, pageIndex: 2 }, { ...defaultSheet, pageIndex: 3 }, { ...defaultSheet, pageIndex: 4 }];
    it('returns false if there are no sheets', () => {
      const isDefault = allOtherSheetsAreDefault(1);
      expect(isDefault).toBeFalsy();
    });

    it('returns false if the current sheet is not the first', () => {
      const isDefault = allOtherSheetsAreDefault(1, sheets);
      expect(isDefault).toBeFalsy();
    });

    it('returns false if the first sheet is using default tolerances', () => {
      const defaultAll = [...sheets];
      defaultAll[0] = { ...defaultSheet, pageIndex: 0 };
      const isDefault = allOtherSheetsAreDefault(0, defaultAll);
      expect(isDefault).toBeFalsy();
    });

    it('returns false if any sheet beyond the first is not using default tolerances', () => {
      const changedLater = [...sheets];
      changedLater[2] = { ...changedSheet, pageIndex: 3 };
      const isDefault = allOtherSheetsAreDefault(0, changedLater);
      expect(isDefault).toBeFalsy();
    });

    it('returns true if every sheet except the first is not using default tolerances', () => {
      const isDefault = allOtherSheetsAreDefault(0, sheets);
      expect(isDefault).toBeTruthy();
    });
  });
});
