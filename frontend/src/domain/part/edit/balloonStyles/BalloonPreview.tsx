import React from 'react';
import { Popconfirm, Tooltip } from 'antd';
import { i18n } from 'i18n';
import { QuestionCircleOutlined, DeleteOutlined } from '@ant-design/icons';
import { Marker } from 'types/marker';
import { numberedSvgMaker } from 'utils/SvgMaker';

interface BalloonProps {
  style: Marker;
  canDelete: boolean;
  index: string;
  handleDeleteStyle: (e: any, id: string, index: string) => void;
  handleEditStyle?: (marker: Marker) => void;
  tip?: string;
}

export const BalloonPreview = ({ style, canDelete, index, handleDeleteStyle, handleEditStyle, tip = '' }: BalloonProps) => {
  // prevents selecting the style when choosing no on the delete popconfirm
  const handleCancelDelete = (e: any) => {
    e.preventDefault();
    e.stopPropagation();
  };

  const handleConfirmDelete = (e: any) => {
    e.preventDefault();
    e.stopPropagation();
    handleDeleteStyle(e, style.id, index);
  };

  const handleEdit = () => {
    if (handleEditStyle) handleEditStyle(style);
  };

  return (
    <>
      {canDelete && (
        <div className="style-delete-button">
          <Popconfirm title={i18n('common.areYouSure')} icon={<QuestionCircleOutlined style={{ color: 'red' }} />} onCancel={handleCancelDelete} onConfirm={handleConfirmDelete}>
            <DeleteOutlined
              className="btn-danger-secondary btn-delete-style"
              onClick={(e: any) => {
                e.preventDefault();
                e.stopPropagation();
              }}
            />
          </Popconfirm>
        </div>
      )}
      {handleEditStyle && <div className="edit-style" onClick={handleEdit} />}
      <Tooltip title={tip}>{numberedSvgMaker(style.fillColor, style.borderColor, style.borderWidth, style.fontColor, style.shape, style.borderStyle)}</Tooltip>
    </>
  );
};
