import React, { useEffect, useState } from 'react';
import { Divider, Select } from 'antd';
import { Marker } from 'types/marker';
import AddOptionButton from 'styleguide/Buttons/AddOption';
import { i18n } from 'i18n';
import Analytics from 'modules/shared/analytics/analytics';
import Markers, { MarkerInput } from 'graphql/marker';
import { useMutation } from '@apollo/client';
import Permissions from 'security/permissions';
import PermissionChecker from 'modules/auth/permissionChecker';
import { useDispatch, useSelector } from 'react-redux';
import authSelectors from 'modules/auth/authSelectors';
import { AppState } from 'modules/state';
import { SessionActions } from 'modules/session/sessionActions';
import { BalloonStyleModal } from './modals/BalloonStyleModal';
import { BalloonPreview } from './BalloonPreview';

const { Option, OptGroup } = Select;

interface BalloonSelectProps {
  update: ({ index, id }: { index: string; id: string; newMarkers?: Marker[] }) => void;
  index: string;
  defaultBalloon: string;
  style: string;
  usedStyles: string[];
  id: string;
}

export const BalloonSelect = ({ update, index, defaultBalloon, style, usedStyles, id }: BalloonSelectProps) => {
  const dispatch = useDispatch();
  const { markers } = useSelector((state: AppState) => state.session);
  const { part } = useSelector((state: AppState) => state.part);
  const [defaults, setDefaults] = useState<Marker[]>(markers?.filter((opt) => opt.markerStyleName === 'Default') || []);
  const [customs, setCustoms] = useState<Marker[]>(markers?.filter((opt) => opt.markerStyleName !== 'Default') || []);
  const [showModal, setShowModal] = useState(false);
  const [currentMarker, setCurrentMarker] = useState<Marker>();

  useEffect(() => {
    // make sure to update all selects when adding/deleting styles
    setDefaults(markers?.filter((opt) => opt.markerStyleName === 'Default') || []);
    setCustoms(markers?.filter((opt) => opt.markerStyleName !== 'Default') || []);
  }, [markers]);

  const user = useSelector((state: AppState) => authSelectors.selectCurrentUser(state));
  const permissionValidator = new PermissionChecker(user);
  const hasStyleEditAccess = permissionValidator.match(Permissions.values.accountManagement);

  const [deleteStyle] = useMutation(Markers.mutate.delete, {
    onCompleted: ({ markerDestroy }) => {
      const newCustoms = [...customs];
      markerDestroy.forEach((destroyedId: string) => {
        const deletedCustomInd = newCustoms.findIndex((marker) => marker.id === destroyedId);
        if (deletedCustomInd >= 0) {
          const deletedCustom = { ...newCustoms[deletedCustomInd] };
          deletedCustom.deletedAt = 'now';
          newCustoms[deletedCustomInd] = deletedCustom;
        }
      });
      setCustoms(newCustoms);
      const newMarkers = markers ? [...markers] : [];
      if (markers) {
        markerDestroy.forEach((destroyedId: string) => {
          const deletedCustomInd = newMarkers.findIndex((marker) => marker.id === destroyedId);
          if (deletedCustomInd >= 0) {
            newMarkers.splice(deletedCustomInd, 1);
          }
        });
        dispatch({ type: SessionActions.UPDATE_MARKERS, payload: { markers: newMarkers } });
      }

      if (markerDestroy.includes(style)) {
        let newStyle = 'f12c5566-7612-48e9-9534-ef90b276ebaa';
        // switch style to default or next available default
        if (index !== 'default' || usedStyles.includes('f12c5566-7612-48e9-9534-ef90b276ebaa')) {
          let balloon = 0;
          while (usedStyles.includes(defaults[balloon].id) && balloon <= defaults.length) {
            balloon += 1;
          }

          newStyle = defaults[balloon].id;
        }

        update({ index, id: newStyle, newMarkers });
      }

      Analytics.track({
        event: Analytics.events.featureMarkerStyleDeleted,
        part,
        properties: {
          'marker.id': markerDestroy[0],
        },
      });
    },
    refetchQueries: [
      {
        query: Markers.query.list,
      },
    ],
  });

  const updateMarkers = (marker: Marker) => {
    const newMarkers: Marker[] = markers ? [...markers] : [];
    const customInd = customs.findIndex((mark: Marker) => mark.id === marker.id);
    if (customInd >= 0) {
      const newCustoms: Marker[] = [...customs].splice(customInd, 1);
      newCustoms[customInd] = marker;
      setCustoms(newCustoms);
    } else {
      setCustoms([...customs, marker]);
    }

    if (markers) {
      const markersInd = newMarkers.findIndex((mark: Marker) => mark.id === marker.id);
      if (markersInd >= 0) {
        newMarkers[markersInd] = marker;
        dispatch({ type: SessionActions.UPDATE_MARKERS, payload: { markers: newMarkers } });
      } else {
        newMarkers.push(marker);
        dispatch({ type: SessionActions.UPDATE_MARKERS, payload: { markers: [...newMarkers] } });
      }
    }
    update({ index, id: marker.id, newMarkers });
  };

  const [createStyle] = useMutation(Markers.mutate.create, {
    onCompleted: ({ markerCreate }) => {
      updateMarkers(markerCreate);

      Analytics.track({
        event: Analytics.events.featureMarkerStyleCreated,
        part,
        marker: markerCreate,
      });
    },
    refetchQueries: [
      {
        query: Markers.query.list,
      },
    ],
  });

  const [editStyle] = useMutation(Markers.mutate.edit, {
    onCompleted: ({ markerUpdate }) => {
      updateMarkers(markerUpdate);

      Analytics.track({
        event: Analytics.events.featureMarkerStyleUpdated,
        part,
        marker: markerUpdate,
      });
    },
    refetchQueries: [
      {
        query: Markers.query.list,
      },
    ],
  });

  const handleSubmit = (values: any, marker?: Marker) => {
    setShowModal(false);
    setCurrentMarker(undefined);
    const styleInput: MarkerInput = Markers.createInputFromObject({
      shape: values.shape,
      fillColor: values.fillColor.hex,
      fontColor: values.fontColor.hex,
      fontSize: values.fontSize,
      borderColor: values.borderColor.hex,
      borderWidth: values.borderWidth,
      borderStyle: values.borderStyle,
      hasLeaderLine: values.lineStyle !== 'none',
      leaderLineColor: values.lineColor.hex,
      leaderLineWidth: values.lineWidth,
      leaderLineStyle: values.lineStyle,
    });
    if (marker) {
      // handle edit marker
      editStyle({ variables: { id: marker.id, data: styleInput } });
    } else {
      // handle create marker
      createStyle({ variables: { data: styleInput } });
    }
  };

  const handleCancelModal = () => {
    setShowModal(false);
    setCurrentMarker(undefined);
  };

  const handleDeleteStyle = (e: any, markerId: string) => {
    e.preventDefault();
    e.stopPropagation();
    deleteStyle({ variables: { ids: [markerId] } });
  };

  const handleEditStyle = (marker: Marker) => {
    setCurrentMarker(marker);
  };

  useEffect(() => {
    if (currentMarker) setShowModal(true);
  }, [currentMarker]);

  return (
    <>
      <Select
        size="large"
        onChange={(value: string) => update({ index, id: value })}
        id={`balloon-style-${index}`}
        className={`balloon-select style-select${part ? '-default' : ''}`}
        dropdownClassName="balloon-styles"
        defaultValue={defaultBalloon}
        value={style}
        dropdownMatchSelectWidth={false}
        listHeight={customs.filter((marker) => !marker.deletedAt).length > 0 ? 324 : 284}
        dropdownRender={(menu) => (
          <div>
            {menu}
            {hasStyleEditAccess && (
              <>
                <Divider style={{ margin: '4px 0' }} />
                <div className="add-option">
                  <AddOptionButton
                    add={(e) => {
                      e.preventDefault();
                      setShowModal(true);
                    }}
                    option={i18n('wizard.customStyle')}
                  />
                </div>
              </>
            )}
          </div>
        )}
      >
        {customs.filter((marker) => !marker.deletedAt).length > 0 && (
          <OptGroup label={i18n('wizard.customStyles')}>
            {customs
              .slice()
              .sort((x: any, y: any) => {
                if (x.borderColor < y.borderColor) {
                  return -1;
                }
                if (x.borderColor > y.borderColor) {
                  return 1;
                }
                if (x.shape < y.shape) {
                  return -1;
                }
                return 1;
              })
              .map((marker) => (
                <Option
                  key={marker.id}
                  className={`balloon-style-option option-${marker.borderColor}-${marker.shape}-${marker.fillColor} ${usedStyles.includes(marker.id) ? 'disabled' : ''} ${marker.deletedAt === null ? 'show-option' : 'hide-option'}`}
                  value={marker.id}
                  disabled={!!usedStyles.includes(marker.id)}
                >
                  <BalloonPreview style={marker} canDelete={hasStyleEditAccess} handleDeleteStyle={handleDeleteStyle} handleEditStyle={!!usedStyles.includes(marker.id) && marker.id === style ? handleEditStyle : undefined} index={index} />
                </Option>
              ))}
          </OptGroup>
        )}
        <OptGroup label={i18n('wizard.defaultStyles')}>
          {defaults
            .slice()
            .sort((x: any, y: any) => {
              if (x.borderColor < y.borderColor) {
                return -1;
              }
              if (x.borderColor > y.borderColor) {
                return 1;
              }
              if (x.shape < y.shape) {
                return -1;
              }
              return 1;
            })
            .map((marker) => (
              <Option
                key={marker.id}
                className={`balloon-style-option option-${marker.borderColor}-${marker.shape}-${marker.fillColor} ${usedStyles.includes(marker.id) ? 'disabled' : ''}`}
                value={marker.id}
                disabled={!!usedStyles.includes(marker.id)}
              >
                <BalloonPreview style={marker} canDelete={false} handleDeleteStyle={handleDeleteStyle} index={index} />
              </Option>
            ))}
        </OptGroup>
      </Select>
      <BalloonStyleModal id={id} index={index} visible={showModal} handleCancel={handleCancelModal} handleSubmit={handleSubmit} marker={currentMarker} />
    </>
  );
};
