import React from 'react';
import { CloseCircleTwoTone } from '@ant-design/icons';
import { Cascader } from 'antd';
import { Marker } from 'types/marker';
import { BrandColors } from 'view/global/defaults';
import { i18n } from 'i18n';
import { BaseOptionType } from 'antd/lib/cascader';
import { BalloonSelect } from './BalloonSelect';

// // Only show final assignment selection in dropdown
// export const displayRender = (labels: string[]) => {
//   return <span key={labels.join(' / ')}>{labels[labels.length - 1]}</span>;
// };

// Create SVG of Balloon based on Marker data
// TODO: This should be moved into a utils module
export const svgMaker = (color: string, stroke: string, text: string, shape: string) => {
  let balloon;
  switch (shape) {
    case 'Diamond':
      balloon = <polygon points="14,0 28,14 14,28 0,14" stroke={BrandColors[stroke]} fill={BrandColors[color]} />;
      break;
    case 'Triangle':
      balloon = <polygon points="14,0 28,28 0,28" stroke={BrandColors[stroke]} fill={BrandColors[color]} />;
      break;
    default:
      balloon = <ellipse cx="14" cy="14" rx="13" ry="13" stroke={BrandColors[stroke]} fill={BrandColors[color]} />;
  }
  return (
    <svg width="30" id={`${color}-${shape}-${stroke}`} height="30" viewBox="0 0 28 28" fill="none" xmlns="http://www.w3.org/2000/svg">
      {balloon}
      <text x="10" y={shape === 'Triangle' ? '23' : '19'} fill={BrandColors[text]}>
        7
      </text>
    </svg>
  );
};

export interface BalloonOptionProps {
  cascaderOptions: BaseOptionType[];
  index: string;
  id: string;
  usedStyles: string[];
  defaultBalloon: string;
  displayRender: (label: string[], selectedOptions: string[] | undefined) => any;
  assign?: string[];
  style: string;
  update: ({ index, id, assign, newMarkers }: { index: number | string; id?: string; assign?: string[]; newMarkers?: Marker[] }) => void;
}

// Repeat Component for Balloon Style
export const BalloonOption: React.FC<BalloonOptionProps> = ({ cascaderOptions, index, id, usedStyles, defaultBalloon, displayRender, style, assign, update }) => {
  const filter = (inputValue: string, path: BaseOptionType[]) => {
    return path.some((option: any) => option.label.toLowerCase().indexOf(inputValue.toLowerCase()) > -1);
  };

  const handleCascaderChange = (value: BaseOptionType, selectedOptions?: BaseOptionType[] | undefined) => {
    if (!selectedOptions) return;
    update({ index, assign: selectedOptions.map((opt) => opt.value?.toString() || '') });
  };

  return (
    <section id={`style-${index}`} cy-data="style" className="balloon-assignment">
      {/* rows */}
      <BalloonSelect update={update} index={index} id={id} defaultBalloon={defaultBalloon} style={style} usedStyles={usedStyles} />
      {/* Assignments */}
      {index !== 'default' ? (
        <Cascader
          size="large"
          key={style + assign}
          onChange={handleCascaderChange}
          options={cascaderOptions}
          value={assign}
          displayRender={(label: any) => displayRender(label, assign)}
          className={`assign-picker ${index}-assignment`}
          dropdownClassName={`assign-options-${index}`}
          placeholder="Please select"
          showSearch={{ filter }}
          allowClear={false}
        />
      ) : (
        <div className="default-text">{i18n('common.defaultStyle')}</div>
      )}
      {/* Remove Style */}
      {index !== 'default' && (
        <button
          type="button"
          className="icon-button remove"
          onClick={(e: any) => {
            e.preventDefault();
            update({ index });
          }}
        >
          <CloseCircleTwoTone />
        </button>
      )}
    </section>
  );
};
