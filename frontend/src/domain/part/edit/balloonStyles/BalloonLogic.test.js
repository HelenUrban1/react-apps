import { convertCascader } from 'utils/ConvertCascader';
import { DefaultStyles } from 'view/global/defaults';
import { validateAssignedStyles, checkForPresetStyle } from './BalloonLogic';

describe('Balloon Styles Functions', () => {
  const classifications = [
    { value: 'critical', label: 'Critical' },
    { value: 'major', label: 'Major' },
    { value: 'minor', label: 'Minor' },
    { value: 'incidental', label: 'Incidental' },
  ];

  const customStyles = {
    1: { style: '123', assign: ['critical'] },
    2: { style: null, assign: [] },
    default: { style: '92873984', assign: ['Default'] },
  };

  const presets = {
    custom: {
      customers: ['Customer'],
      setting: customStyles,
    },
    Default: {
      customers: [],
      setting: DefaultStyles,
    },
  };

  const settings = {
    stylePresets: {
      metadata: presets,
    },
  };

  it('converts default methods into a cascader options array', () => {
    const options = convertCascader(classifications, 'classifications', []);
    expect(options).toMatchObject({
      value: 'classifications',
      label: 'Classifications',
      children: [
        {
          value: 'critical',
          label: 'Critical',
          disabled: false,
        },
        {
          value: 'major',
          label: 'Major',
          disabled: false,
        },
        {
          value: 'minor',
          label: 'Minor',
          disabled: false,
        },
        {
          value: 'incidental',
          label: 'Incidental',
          disabled: false,
        },
      ],
    });
  });
  it('disables used cascader options', () => {
    const options = convertCascader(classifications, 'classifications', ['critical']);
    expect(options).toMatchObject({
      value: 'classifications',
      label: 'Classifications',
      children: [
        {
          value: 'critical',
          label: 'Critical',
          disabled: true,
        },
        {
          value: 'major',
          label: 'Major',
          disabled: false,
        },
        {
          value: 'minor',
          label: 'Minor',
          disabled: false,
        },
        {
          value: 'incidental',
          label: 'Incidental',
          disabled: false,
        },
      ],
    });
  });
  it('validates true if all styles are assigned', () => {
    const valid = validateAssignedStyles({
      1: { style: '123', assign: ['critical'] },
      2: { style: '123', assign: ['incidental'] },
      default: { style: '92873984', assign: ['Default'] },
    });
    expect(valid).toBeTruthy();
  });
  it('validates false if any style is missing an element', () => {
    const valid1 = validateAssignedStyles({
      1: { style: '123', assign: ['critical'] },
      2: { style: '', assign: ['incidental'] },
      default: { style: '92873984', assign: ['Default'] },
    });
    const valid2 = validateAssignedStyles({
      1: { style: '123', assign: ['critical'] },
      2: { style: '123', assign: [''] },
      default: { style: '92873984', assign: ['Default'] },
    });
    const valid3 = validateAssignedStyles({
      1: { style: '123', assign: ['critical'] },
      2: { style: null, assign: [] },
      default: { style: '92873984', assign: ['Default'] },
    });
    expect(valid1).toBeFalsy();
    expect(valid2).toBeFalsy();
    expect(valid3).toBeFalsy();
  });
  it('returns the right preset name', () => {
    let preset = checkForPresetStyle(JSON.stringify(DefaultStyles), settings);
    expect(preset).toBe('Default');

    preset = checkForPresetStyle(JSON.stringify(customStyles), settings);
    expect(preset).toBe('custom');

    preset = checkForPresetStyle(JSON.stringify({ fake: 'style' }), settings);
    expect(preset).toBe('None');
  });
});
