import React, { useState } from 'react';
import { Form, Input, Modal } from 'antd';
import { LoadedPresets } from 'domain/setting/settingTypes';
import { i18n } from 'i18n';
import { Marker, MarkerStyle } from 'types/marker';
import { UsedStylesRow } from 'styleguide/ConfigTables/StylesTable/UsedStylesRow/UsedStylesRow';

interface StyleModalProps {
  showModal: boolean;
  presets: LoadedPresets | undefined;
  handleCreatePreset: (value: string) => void;
  setShowModal: (value: boolean) => void;
  usedStyles: { [key: string]: MarkerStyle } | null;
  markers: Marker[] | null;
}

export const StylePresetModal = ({ showModal, presets, handleCreatePreset, setShowModal, usedStyles, markers }: StyleModalProps) => {
  const [value, setValue] = useState('');
  const [status, setStatus] = useState<'' | 'error' | 'success' | 'warning' | 'validating' | undefined>();
  const [message, setMessage] = useState<string | undefined>();

  const handleChangeInput = (e: any) => {
    setValue(e.target.value);
  };

  const isDuplicateValue = () => {
    const presetNames = Object.keys(presets?.metadata || {});
    return presetNames.includes(value);
  };

  const handleOk = () => {
    if (value.replaceAll(' ', '').length === 0) {
      setStatus('error');
      setMessage(`${i18n(`errors.config.empty`)}`);
    } else if (isDuplicateValue()) {
      setStatus('error');
      setMessage(`${i18n(`errors.config.duplicate`)}`);
    } else {
      handleCreatePreset(value);
      setValue('');
      setStatus(undefined);
      setMessage(undefined);
    }
  };

  const handleCancel = () => {
    setShowModal(false);
  };

  return (
    <Modal title={i18n('wizard.presetModal.styleTitle')} visible={showModal} okText={i18n('common.save')} onOk={handleOk} onCancel={handleCancel}>
      <Form layout="vertical" className="list-feature-modal-form" data-cy="list-feature-modal-form">
        <Form.Item label={i18n('wizard.presetModal.presetNameLabel')} hasFeedback validateStatus={status} help={message}>
          <Input className="new-preset-modal-input" data-cy="new-preset-modal-input" value={value} onChange={handleChangeInput} placeholder={i18n('wizard.presetModal.presetNameHint')} />
        </Form.Item>
        {usedStyles && (
          <Form.Item label={i18n('wizard.presetModal.used')}>
            <UsedStylesRow usedStyles={usedStyles} markers={markers} />
          </Form.Item>
        )}
      </Form>
    </Modal>
  );
};
