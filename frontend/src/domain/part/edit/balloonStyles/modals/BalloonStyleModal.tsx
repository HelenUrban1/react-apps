import React, { useCallback, useEffect, useState } from 'react';
import { Modal, Form, Select, Row, Col, Input, Checkbox } from 'antd';
import { ColorPicker } from 'styleguide/ColorPicker';
import { fabric } from 'fabric';
import { BrandColors } from 'view/global/defaults';
import { Marker, MarkerShape, MarkerStrokeStyleEnum } from 'types/marker';
import { i18n } from 'i18n';
import log from 'modules/shared/logger';
import { getBalloon, getBalloonGroup, getFontSizeModifier, getStrokeStyle, getText } from '../../characteristics/characteristicsRenderer';

const { Option } = Select;

interface ModalProps {
  id: string;
  index: string;
  visible: boolean;
  handleSubmit: (values: any, marker?: Marker) => void;
  handleCancel: any;
  marker?: Marker;
}

type formTypes = {
  borderColor: { hex: string };
  borderStyle: MarkerStrokeStyleEnum;
  borderWidth: number;
  fillColor: { hex: string };
  fontSize: number;
  fontColor: { hex: string };
  lineColor: { hex: string };
  lineStyle: MarkerStrokeStyleEnum;
  lineWidth: number;
  shape: MarkerShape;
};

const updateCanvas = (canvas: any, shape: string, lineWidth: number, lineColor: string, lineStyle: number[], fillColor: string, borderColor: string, borderWidth: number, borderStyle: MarkerStrokeStyleEnum, fontColor: string, label: string) => {
  canvas.clear();
  const shapeMid = shape === 'Triangle' ? 65.5 : 56;
  const line = new fabric.Line([32, 32 - lineWidth / 2, 100, 32 - lineWidth / 2], {
    fill: lineColor,
    stroke: lineColor,
    strokeWidth: lineWidth,
    selectable: false,
    strokeDashArray: lineStyle,
  });
  // const size = fontSize * 2;
  const balloon = getBalloon(shape, 56, shapeMid, fillColor, borderColor, borderWidth, 48, borderStyle);
  const fontSizeModifier = getFontSizeModifier(label);
  const text = getText(label, 24 * fontSizeModifier, shapeMid, 56, fontColor, 48, 'center');
  const balloonGroup = getBalloonGroup(balloon, text, true);
  balloonGroup.selectable = false;
  canvas.add(line);
  canvas.add(balloonGroup);
  canvas.renderAll();
};

export const BalloonStyleModal = ({ id, index, visible, handleSubmit, handleCancel, marker }: ModalProps) => {
  const [styleForm] = Form.useForm();
  const [canvas, setCanvas] = useState<fabric.Canvas>();
  const [label, setLabel] = useState('7.7');
  const [match, setMatch] = useState(true);

  const defaultFormValues: formTypes = {
    borderColor: { hex: marker?.borderColor || BrandColors.Coral },
    borderStyle: marker?.borderStyle || 'Solid',
    borderWidth: marker?.borderWidth || 2,
    fillColor: { hex: marker?.fillColor || BrandColors.Coral },
    fontSize: marker?.fontSize || 10,
    fontColor: { hex: marker?.fontColor || '#FFFFFF' },
    lineColor: { hex: marker?.leaderLineColor || BrandColors.Coral },
    lineStyle: marker?.leaderLineStyle || 'Solid',
    lineWidth: marker?.leaderLineWidth || 2,
    shape: marker?.shape || 'Circle',
  };

  useEffect(() => {
    if (marker) {
      const fields = {
        borderColor: { hex: marker.borderColor },
        borderStyle: marker.borderStyle,
        borderWidth: marker.borderWidth,
        fillColor: { hex: marker.fillColor },
        fontSize: marker.fontSize,
        fontColor: { hex: marker.fontColor },
        lineColor: { hex: marker.leaderLineColor },
        lineStyle: marker.leaderLineStyle,
        lineWidth: marker.leaderLineWidth,
        shape: marker.shape,
      };
      styleForm.setFieldsValue(fields);
      if (canvas) {
        updateCanvas(canvas, fields.shape, fields.lineWidth, fields.lineColor.hex, getStrokeStyle(fields.lineStyle), fields.fillColor.hex, fields.borderColor.hex, fields.borderWidth, fields.borderStyle, fields.fontColor.hex, label);
      }
    }
  }, [marker?.id]);

  const ref = useCallback(
    (node) => {
      if (node !== null) {
        try {
          const fCanvas = new fabric.Canvas(node);

          updateCanvas(
            fCanvas, //
            marker?.shape || defaultFormValues.shape,
            marker?.leaderLineWidth || defaultFormValues.lineWidth,
            marker?.leaderLineColor || defaultFormValues.lineColor.hex,
            getStrokeStyle(marker?.leaderLineStyle || defaultFormValues.lineStyle),
            marker?.fillColor || defaultFormValues.fillColor.hex,
            marker?.borderColor || defaultFormValues.borderColor.hex,
            marker?.borderWidth || defaultFormValues.borderWidth,
            marker?.borderStyle || defaultFormValues.borderStyle,
            marker?.fontColor || defaultFormValues.fontColor.hex,
            label,
          );

          setCanvas(fCanvas);
        } catch (err) {
          // prevents error boundary from triggering on getComputedStyles of Null error
          log.error('BallonStyleModal.useCallback', err);
        }
      }
    },
    [marker],
  );

  const submitForm = (values: any) => {
    handleSubmit(values, marker);
  };

  const handleConfirmModal = () => {
    styleForm.submit();
  };

  const handleCancelModal = () => {
    handleCancel();
  };

  const handleCleanup = () => {
    setMatch(true);
    setLabel('7.7');
    styleForm.resetFields();
  };

  // update balloon preview
  const changePreview = (changedFields: any, allFields: any) => {
    const newFields = [...allFields];
    if (match) {
      // match leader line settings to border settings
      newFields[6].value = allFields[3].value;
      newFields[7].value = allFields[4].value;
      newFields[8].value = allFields[5].value;

      styleForm.setFieldsValue({
        lineColor: allFields[3].value,
        lineStyle: allFields[4].value,
        lineWidth: allFields[5].value,
      });
    }
    const fillColor = newFields[0].value.hex || BrandColors.Coral;
    const shape = newFields[1].value;
    const fontColor = newFields[2].value.hex || '#FFFFFF';
    const borderColor = newFields[3].value.hex || BrandColors.Coral;
    const borderStyle = newFields[4].value || 'solid';
    const borderWidth = newFields[5].value || 2;
    const lineColor = newFields[6].value.hex || BrandColors.Coral;
    const lineStyle = getStrokeStyle(newFields[7].value);
    const lineWidth = newFields[8].value || 2;

    if (canvas) {
      updateCanvas(canvas, shape, lineWidth, lineColor, lineStyle, fillColor, borderColor, borderWidth, borderStyle, fontColor, label);
    }
  };

  const handleChangeLabel = (e: any) => {
    const newLabel = e.target.value;
    if (canvas) {
      const balloonGroup = canvas.getObjects()[1] as fabric.Group;
      const text = balloonGroup._objects[1] as fabric.Text;
      if (text) {
        const fontSizeModifier = getFontSizeModifier(newLabel);
        text.set('text', newLabel);
        text.set('fontSize', fontSizeModifier * 24);
        balloonGroup.dirty = true;
        canvas.renderAll();
      }
    }
    setLabel(newLabel);
  };

  const handleChangeMatch = (e: any) => {
    setMatch(e.target.checked);

    if (e.target.checked) {
      styleForm.setFieldsValue({
        lineColor: styleForm.getFieldValue('borderColor') || defaultFormValues.borderColor,
        lineStyle: styleForm.getFieldValue('borderStyle') || defaultFormValues.borderColor,
        lineWidth: styleForm.getFieldValue('borderWidth') || defaultFormValues.borderWidth,
      });
      const allFields = styleForm.getFieldsValue();
      changePreview({}, [
        { value: allFields.fillColor },
        { value: allFields.shape },
        { value: allFields.fontColor },
        { value: allFields.borderColor },
        { value: allFields.borderStyle },
        { value: allFields.borderWidth },
        { value: allFields.lineColor },
        { value: allFields.lineStyle },
        { value: allFields.lineWidth },
      ]);
    }
  };

  const handleChangeColor = (color: string, field: string) => {
    switch (field) {
      case 'fillColor':
        styleForm.setFieldsValue({ fillColor: { hex: color } });
        break;
      case 'fontColor':
        styleForm.setFieldsValue({ fontColor: { hex: color } });
        break;
      case 'borderColor':
        styleForm.setFieldsValue({ borderColor: { hex: color } });
        if (match) {
          styleForm.setFieldsValue({ lineColor: { hex: color } });
        }
        break;
      case 'lineColor':
        styleForm.setFieldsValue({ lineColor: { hex: color } });
        break;
      default:
        break;
    }
    const allFields = styleForm.getFieldsValue();
    changePreview({}, [
      { value: allFields.fillColor },
      { value: allFields.shape },
      { value: allFields.fontColor },
      { value: allFields.borderColor },
      { value: allFields.borderStyle },
      { value: allFields.borderWidth },
      { value: allFields.lineColor },
      { value: allFields.lineStyle },
      { value: allFields.lineWidth },
    ]);
  };

  return (
    <Modal
      title={i18n('wizard.stylesModal.title')}
      visible={visible}
      okText={i18n('common.save')}
      onCancel={handleCancelModal}
      onOk={handleConfirmModal}
      afterClose={handleCleanup}
      width="fit-content"
      maskClosable={false}
      destroyOnClose
      zIndex={1050}
      getContainer={false}
    >
      <div className="balloon-builder-content">
        <div className="balloon-builder-form">
          <Form onFinish={submitForm} form={styleForm} layout="vertical" initialValues={defaultFormValues} onFieldsChange={changePreview}>
            <h3 data-cy="balloon-row-label">{i18n('wizard.stylesModal.balloon')}</h3>
            <Row>
              <Col>
                <Form.Item className="form-color-input" name="fillColor" label={i18n('wizard.stylesModal.fields.balloonColor')}>
                  <ColorPicker value={styleForm.getFieldValue('fillColor')} field="fillColor" handleChangeColor={handleChangeColor} />
                </Form.Item>
              </Col>
              <Col>
                <Form.Item name="shape" label={i18n('wizard.stylesModal.fields.shape')}>
                  <Select className="style-select" data-cy="shape-select" size="large">
                    <Option value="Circle">{i18n('wizard.stylesModal.shapes.circle')}</Option>
                    <Option value="Triangle">{i18n('wizard.stylesModal.shapes.triangle')}</Option>
                    <Option value="Square">{i18n('wizard.stylesModal.shapes.square')}</Option>
                    <Option value="Diamond">{i18n('wizard.stylesModal.shapes.diamond')}</Option>
                    {/* <Option value="Pentagon">{i18n('wizard.stylesModal.shapes.pentagon')}</Option> shape comes out goofy, text off-center */}
                    <Option value="Hexagon">{i18n('wizard.stylesModal.shapes.hexagon')}</Option>
                  </Select>
                </Form.Item>
              </Col>
            </Row>

            <h3 data-cy="font-row-label">{i18n('wizard.stylesModal.font')}</h3>
            <Row>
              <Col>
                <Form.Item className="form-color-input" name="fontColor" label={i18n('wizard.stylesModal.fields.fontColor')}>
                  <ColorPicker value={styleForm.getFieldValue('fontColor')} field="fontColor" handleChangeColor={handleChangeColor} />
                </Form.Item>
              </Col>
            </Row>

            <h3 data-cy="border-row-label">{i18n('wizard.stylesModal.border')}</h3>
            <Row>
              <Col>
                <Form.Item className="form-color-input" name="borderColor" label={i18n('wizard.stylesModal.fields.borderColor')}>
                  <ColorPicker value={styleForm.getFieldValue('borderColor')} field="borderColor" handleChangeColor={handleChangeColor} />
                </Form.Item>
              </Col>
              <Col>
                <Form.Item name="borderStyle" label={i18n('wizard.stylesModal.fields.borderStyle')}>
                  <Select className="style-select" data-cy="border-style-select" size="large">
                    <Option value="Solid">
                      <div className="border-style-containter">
                        <div className="line-style-option solid" data-cy="border-solid" />
                      </div>
                    </Option>
                    <Option value="Dashed">
                      <div className="border-style-containter">
                        <div className="line-style-option dashed" data-cy="border-dashed" />
                      </div>
                    </Option>
                    <Option value="Dotted">
                      <div className="border-style-containter">
                        <div className="line-style-option dotted" data-cy="border-dotted" />
                      </div>
                    </Option>
                  </Select>
                </Form.Item>
              </Col>
              <Col>
                <Form.Item name="borderWidth" label={i18n('wizard.stylesModal.fields.borderWidth')}>
                  <Select className="border-width-select" data-cy="border-width-select" size="large">
                    <Option value={1}>
                      <div className="line-style-containter">
                        <div className="line-style-option border-w1" style={{ height: '1px' }} />
                      </div>
                    </Option>
                    <Option value={2}>
                      <div className="line-style-containter">
                        <div className="line-style-option border-w2" style={{ height: '2px' }} />
                      </div>
                    </Option>
                    <Option value={3}>
                      <div className="line-style-containter">
                        <div className="line-style-option border-w3" style={{ height: '3px' }} />
                      </div>
                    </Option>
                    <Option value={4}>
                      <div className="line-style-containter">
                        <div className="line-style-option border-w4" style={{ height: '4px' }} />
                      </div>
                    </Option>
                  </Select>
                </Form.Item>
              </Col>
            </Row>
            <h3 data-cy="leader-row-label">{i18n('wizard.stylesModal.leader')}</h3>
            <Row>
              <Checkbox className="line-match-checkbox" checked={match} data-cy="line-match-checkbox" onChange={handleChangeMatch}>
                {i18n('wizard.stylesModal.match')}
              </Checkbox>
            </Row>
            <Row>
              <Col>
                <Form.Item className={`form-color-input${match ? ' disabled' : ''}`} name="lineColor" label={i18n('wizard.stylesModal.fields.lineColor')}>
                  <ColorPicker value={styleForm.getFieldValue('lineColor')} field="lineColor" handleChangeColor={handleChangeColor} disabled={match} />
                </Form.Item>
              </Col>
              <Col>
                <Form.Item name="lineStyle" label={i18n('wizard.stylesModal.fields.lineStyle')}>
                  <Select className="style-select" data-cy="line-style-select" size="large" disabled={match}>
                    <Option value="Solid">
                      <div className="border-style-containter">
                        <div className="line-style-option solid" data-cy="line-solid" />
                      </div>
                    </Option>
                    <Option value="Dashed">
                      <div className="border-style-containter">
                        <div className="line-style-option dashed" data-cy="line-dashed" />
                      </div>
                    </Option>
                    <Option value="Dotted">
                      <div className="border-style-containter">
                        <div className="line-style-option dotted" data-cy="line-dotted" />
                      </div>
                    </Option>
                  </Select>
                </Form.Item>
              </Col>
              <Col>
                <Form.Item name="lineWidth" label={i18n('wizard.stylesModal.fields.lineWidth')}>
                  <Select className="line-width-select" data-cy="line-width-select" size="large" disabled={match}>
                    <Option value={1}>
                      <div className="line-style-containter">
                        <div className="line-style-option line-w1" style={{ height: '1px' }} />
                      </div>
                    </Option>
                    <Option value={2}>
                      <div className="line-style-containter">
                        <div className="line-style-option line-w2" style={{ height: '2px' }} />
                      </div>
                    </Option>
                    <Option value={3}>
                      <div className="line-style-containter">
                        <div className="line-style-option line-w3" style={{ height: '3px' }} />
                      </div>
                    </Option>
                    <Option value={4}>
                      <div className="line-style-containter">
                        <div className="line-style-option line-w4" style={{ height: '4px' }} />
                      </div>
                    </Option>
                  </Select>
                </Form.Item>
              </Col>
            </Row>
          </Form>
        </div>
        <div className="balloon-builder-preview" data-cy="balloon-builder-preview">
          <Form layout="vertical">
            <h3 data-cy="preview-row-label">{i18n('wizard.stylesModal.preview')}</h3>
            <Form.Item className="form-label-input" name="label" label={i18n('wizard.stylesModal.fields.label')}>
              <Input className="text-input" data-cy="preview-label-input" size="large" defaultValue={label} onChange={handleChangeLabel} autoComplete="off" />
            </Form.Item>
          </Form>
          <div className="preview-row">
            <canvas ref={ref} id={`p-canvas-${index}-${id}`} width={100} height={64} data-cy="balloon-preview-canvas" />
            <img className="preview-capture" src="/images/examples/review3.png" alt="preview" />
          </div>
        </div>
      </div>
    </Modal>
  );
};
