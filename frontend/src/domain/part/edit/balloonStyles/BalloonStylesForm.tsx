import React, { useState, useEffect } from 'react';
import { message } from 'antd';
import AddOptionButton from 'styleguide/Buttons/AddOption';
import { BaseOptionType } from 'antd/lib/cascader';
import Analytics from 'modules/shared/analytics/analytics';
import { Marker, MarkerStyle } from 'types/marker';
import { i18n } from 'i18n';
import { convertListObjectToCascaderOptions } from 'utils/ConvertCascader';
import './balloonStyles.less';
import PanelHeader from 'styleguide/PanelHeader/PanelHeader';
import { ProgressFooter } from 'styleguide/ProgressionFooter';
import { getCurrentList, getValueOfHiddenType } from 'utils/Lists';
import { Settings } from 'domain/setting/settingApi';
import { Preset, Setting } from 'domain/setting/settingTypes';
import { currentSettingMatchesPreset, getPresetSelectOptions } from 'utils/PresetUtils';
import { SelectSave } from 'view/global/inputs/SelectSave';
import { useDispatch, useSelector } from 'react-redux';
import { AppState } from 'modules/state';
import { PartEditorActions } from 'modules/partEditor/partEditorActions';
import { updatePartThunk } from 'modules/part/partActions';
import Spinner from 'view/shared/Spinner';
import { CharacteristicActions } from 'modules/characteristic/characteristicActions';
import { updateSettingThunk } from 'modules/session/sessionActions';
import { PartStepEnum } from 'modules/navigation/navigationActions';
import { BalloonSelect } from './BalloonSelect';
import { StylePresetModal } from './modals/StylePresetModal';
import { validateAssignedStyles, updateCharacteristicMarkers } from './BalloonLogic';
import { BalloonOption } from './BalloonOption';

const { STYLES, EXTRACT } = PartStepEnum;

export interface BalloonStylesFormProps {
  back: () => void;
  next: () => void;
}

export const BalloonStylesForm: React.FC<BalloonStylesFormProps> = ({ back, next }) => {
  const navigation = useSelector((state: AppState) => state.navigation);
  const { part } = useSelector((state: AppState) => state.part);
  const { assignedStyles, presets } = useSelector((state: AppState) => state.partEditor);
  const { types, classifications, operations, methods, markers, settings } = useSelector((state: AppState) => state.session);

  const { characteristics } = useSelector((state: AppState) => state.characteristics);
  const dispatch = useDispatch();
  const [defaultBalloon, setDefault] = useState<number>(0);
  const [applyOptions, setOptions] = useState<BaseOptionType[]>([]);
  const [usedStyles, setUsed] = useState<string[]>(assignedStyles ? ['f12c5566-7612-48e9-9534-ef90b276ebaa', ...Object.values(assignedStyles).map((item) => item.style)] : ['f12c5566-7612-48e9-9534-ef90b276ebaa']);
  const [showModal, setShowModal] = useState(false);
  const [usedAssignments, setAssigned] = useState<BaseOptionType[]>(assignedStyles ? Object.values(assignedStyles).map((item) => item.assign) : []);

  // Builds the cascader inputs after the lists have been fetched from the DB in redux Session
  useEffect(() => {
    const options: BaseOptionType[] = [];
    if (classifications) {
      const classificationOptions = convertListObjectToCascaderOptions(classifications, i18n('entities.feature.fields.criticality'), usedAssignments);
      if (classificationOptions) {
        options.push(classificationOptions);
      }
    }
    if (types) {
      const typesOptions = convertListObjectToCascaderOptions(types, i18n('entities.feature.fields.notationType'), usedAssignments);
      if (typesOptions) {
        options.push(typesOptions);
      }
    }
    if (operations) {
      const operationsOptions = convertListObjectToCascaderOptions(operations, i18n('entities.feature.fields.operation'), usedAssignments);
      if (operationsOptions) {
        options.push(operationsOptions);
      }
    }
    if (methods) {
      const methodsOptions = convertListObjectToCascaderOptions(methods, i18n('entities.feature.fields.inspectionMethod'), usedAssignments);
      if (methodsOptions) {
        options.push(methodsOptions);
      }
    }
    setOptions(options);
  }, [usedAssignments, types, classifications]);

  if (!part) {
    return <div>No Part</div>;
  }

  const checkValid = () => {
    if (!assignedStyles) return false;
    let warned = false;
    let valid = true;
    Object.entries(assignedStyles).forEach(([index, item]) => {
      if (!item.assign || item.assign.length <= 0) {
        valid = false;
        if (!warned) {
          warned = true;
          message.error('Please assign a condition to all balloon styles before continuing.');
        }
        const inputs = document.getElementsByClassName(`${index}-assignment`);
        if (inputs[0] && inputs[0].firstChild && !inputs[0].firstChild.textContent) {
          inputs[0].className += ' warning';
        }
      }
    });
    return valid;
  };

  // If values are valid, update max stage and submit
  const updateMaxStage = () => {
    const max = Math.max(part.workflowStage, navigation.maxPartStep, EXTRACT);
    dispatch(updatePartThunk({ workflowStage: max }, part.id));
  };

  const advance = (e: any) => {
    e.preventDefault();

    if (!assignedStyles || Object.keys(assignedStyles).length <= 0) {
      updateMaxStage();
      next();
      return;
    }
    if (checkValid()) {
      updateMaxStage();
      next();
    }
  };

  const goBack = (e: any) => {
    e.preventDefault();
    if (!assignedStyles || Object.keys(assignedStyles).length <= 0) {
      back();
      return;
    }

    if (checkValid()) {
      back();
    }
  };

  const add = (e: any) => {
    e.preventDefault();
    if (!markers) {
      return;
    }
    let balloon = 0;
    let index: any = 1;
    const defaults = markers.filter((marker) => marker.markerStyleName === 'Default');

    while (usedStyles.includes(defaults[balloon].id) && balloon < markers.length) {
      balloon += 1;
    }

    if (assignedStyles && Object.keys(assignedStyles).length > 0) {
      index = Object.keys(assignedStyles)[Object.keys(assignedStyles).length - 1];
      if (index === 'default') {
        index = Object.keys(assignedStyles)[Object.keys(assignedStyles).length - 2];
      }
      index = parseInt(index, 10);
      index += 1;
    }
    const used = [...usedStyles, defaults[balloon].id];
    const keys = assignedStyles ? Object.keys(assignedStyles) : ['0'];
    let last = '0';

    if (keys.length > 1 && keys[keys.length - 1] === 'default') {
      last = keys[keys.length - 2];
    }

    if (!last) last = '0'; // probably unnecessary
    const nextInd = parseInt(last, 10) + 1;
    const newAdditional: { [key: string]: MarkerStyle } = {
      ...assignedStyles,
      [nextInd.toString()]: {
        style: defaults[balloon].id,
        assign: [],
      },
    };

    Analytics.track({
      event: Analytics.events.featureMarkerStylesUpdated,
      part,
      // TODO: drawing,
      properties: {
        'add.index': index, // BUG: When adding first custom value, index is NaN
      },
    });

    setUsed(used);
    setDefault(balloon + 1);
    dispatch({ type: PartEditorActions.SET_PART_EDITOR_ASSIGNED_STYLES, payload: newAdditional });
  };

  const update = ({ index, id, assign, newMarkers }: { index: number | string; id?: string; assign?: string[]; newMarkers?: Marker[] }) => {
    let valid = true;
    const newAdditional: { [key: string]: MarkerStyle } = {
      ...assignedStyles,
    };

    let action = 'update';

    if (id) {
      action = 'style';
      if (index === 'default') {
        newAdditional.default = { style: id, assign: ['Default'] };
      } else {
        newAdditional[index.toString()].style = id;
      }
      const used = Object.values(newAdditional).map((item) => item.style);
      if (!newAdditional.default) {
        used.push('f12c5566-7612-48e9-9534-ef90b276ebaa');
      }
      setUsed(used);
    } else if (assign) {
      action = 'associate';
      newAdditional[index.toString()].assign = assign;
      setAssigned(Object.values(newAdditional).map((item) => item.assign));
    } else {
      action = 'delete';
      delete newAdditional[index];
      const used = Object.values(newAdditional).map((item) => item.style);
      if (!newAdditional.default) {
        used.push('f12c5566-7612-48e9-9534-ef90b276ebaa');
      }
      setUsed(used);
      setAssigned(Object.values(newAdditional).map((item) => item.assign));
    }

    // update Characteristics with new Marker Style if their assignment changed
    updateCharacteristicMarkers(newAdditional, characteristics, (updates, assignedMarkers) => dispatch({ type: CharacteristicActions.EDIT_MARKERS, payload: { assignedMarkers, updates, markers: newMarkers || markers } }));

    Analytics.track({
      event: Analytics.events.featureMarkerStylesUpdated,
      part,
      // TODO: drawing,
      properties: {
        'update.action': action,
        'update.index': index,
        'update.id': id,
        'update.assign': assign, // BUG: This picks up the UUIDs rather than the text names
      },
    });

    valid = validateAssignedStyles(newAdditional);
    if (valid) {
      dispatch(updatePartThunk({ defaultMarkerOptions: JSON.stringify(newAdditional) }, part.id));
    }
    dispatch({ type: PartEditorActions.SET_PART_EDITOR_ASSIGNED_STYLES, payload: newAdditional });
  };

  // Only show final assignment selection in dropdown
  const displayRender = (label: string[], selectedOptions: string[] | undefined) => {
    const currentList = getCurrentList(label[0], classifications, null, types, null, null, null);
    const newLabel = [...label];
    let first = label[1] ? label[1] : 'Not found';
    let second = label[2] ? label[2] : 'Not found';
    if (label[0] === 'Type') {
      if (!label[2] && currentList && selectedOptions) {
        const labels = getValueOfHiddenType(currentList, selectedOptions);
        first = labels.type;
        second = labels.subtype;
      }
    } else if (!label[1] && currentList && selectedOptions) {
      const labels = getValueOfHiddenType(currentList, [selectedOptions[1]]);
      first = labels.type;
      second = labels.subtype;
      newLabel[1] = first;
      if (label[0] === 'Type') newLabel[2] = second;
    }
    return <span key={label.join(' / ')}>{newLabel[newLabel.length - 1]}</span>;
  };

  const handleChangePreset = (value: string) => {
    const newPreset: Preset | undefined = settings.stylePresets ? { ...settings.stylePresets.metadata[value] } : undefined;
    if (newPreset && newPreset.setting) {
      const markerStyleSetting = newPreset.setting as { [key: string]: MarkerStyle };
      updateCharacteristicMarkers(markerStyleSetting, characteristics, (updates, assignedMarkers) => dispatch({ type: CharacteristicActions.EDIT_MARKERS, payload: { assignedMarkers, updates, markers } }));

      dispatch({ type: PartEditorActions.SET_PART_EDITOR_STATE, payload: { assignedStyles: markerStyleSetting, presets: { ...presets, styles: value } } });

      dispatch(updatePartThunk({ defaultMarkerOptions: JSON.stringify(newPreset.setting), presets: JSON.stringify({ ...presets, styles: value }) }, part.id));
    }
  };

  const updatePreset = (newAssignedStyles: any) => {
    if (settings.stylePresets && settings.stylePresets.id) {
      const settingId = settings.stylePresets.id;
      const updatedSetting: Setting = { ...settings.stylePresets, id: settingId, metadata: JSON.stringify(newAssignedStyles) };
      dispatch(updateSettingThunk(Settings.createGraphqlObject(updatedSetting), updatedSetting.id));
    }
  };

  const handleCreatePreset = (newPresetName: string) => {
    const assignedStylePresets = settings.stylePresets ? { ...settings.stylePresets.metadata } : {};
    assignedStylePresets[newPresetName] = { customers: [part.customer?.id || ''], setting: assignedStyles || {} };
    updatePreset(assignedStylePresets);
    setShowModal(false);
    dispatch({ type: PartEditorActions.SET_PART_EDITOR_PRESETS, payload: { ...presets, styles: newPresetName } });
    dispatch(updatePartThunk({ presets: JSON.stringify({ ...presets, styles: newPresetName }) }, part.id));
    Analytics.track({
      event: Analytics.events.createdStylePresetFromPart,
      preset: {
        name: newPresetName,
        ...assignedStylePresets[newPresetName],
      },
    });
  };

  const handleUpdatePreset = () => {
    if (checkValid()) {
      const assignedStylePresets = settings.stylePresets ? { ...settings.stylePresets.metadata } : {};
      assignedStylePresets[presets?.styles || 'Default'].setting = assignedStyles || {};
      updatePreset(assignedStylePresets);
      Analytics.track({
        event: Analytics.events.updatedStylePreset,
        preset: {
          name: presets?.styles,
          ...assignedStylePresets[presets?.styles || ''],
        },
      });
    }
  };

  const handleOpenModal = () => {
    if (checkValid()) setShowModal(true);
  };

  const handleSavePreset = (e: any) => {
    if (e.key === 'save') {
      handleOpenModal();
    } else {
      handleUpdatePreset();
    }
  };

  return (
    <>
      <section id="balloonstyles" className="side-panel-content">
        <PanelHeader header={`${STYLES + 1}. ${i18n('entities.part.title.balloons')}`} copy={i18n('entities.part.instructions.balloons')} info={i18n('entities.part.instructions.balloonsHint')} dataCy="part_balloons_" optional />
        <section>
          <span className="label-text" data-cy="preset-select-label">
            {`${i18n('settings.custom.presets.styles.stylePreset')}:`}
          </span>
          <StylePresetModal showModal={showModal} handleCreatePreset={handleCreatePreset} setShowModal={setShowModal} presets={settings.stylePresets} usedStyles={assignedStyles} markers={markers} />
          <SelectSave
            handleSave={handleSavePreset}
            handleOnChange={handleChangePreset}
            value={presets?.styles || 'Default'}
            field="styles"
            options={getPresetSelectOptions(settings.stylePresets, part.customer)}
            disabled={currentSettingMatchesPreset(presets?.styles || 'Default', settings.stylePresets, assignedStyles || {})}
          />
          {markers === null && <Spinner style={{ margin: '0px' }} />}
          {markers && markers.length < 1 && <p>Failed to load marker styles</p>}
          {markers && markers.length > 1 && applyOptions && (
            <section id="style-default" cy-data="style" className="balloon-assignment">
              <BalloonSelect
                update={update}
                index="default"
                style={assignedStyles && Object.keys(assignedStyles).length > 0 && assignedStyles.default ? assignedStyles.default.style : 'f12c5566-7612-48e9-9534-ef90b276ebaa'}
                usedStyles={usedStyles}
                defaultBalloon="f12c5566-7612-48e9-9534-ef90b276ebaa"
                id={part?.id || ''}
              />
              <div className="default-text">{i18n('common.defaultStyle')}</div>
            </section>
          )}
          {/* Additional Balloon Styles */}
          {assignedStyles &&
            markers &&
            applyOptions &&
            Object.keys(assignedStyles).length > 0 &&
            Object.entries(assignedStyles).map(([index, item]) => {
              if (index === 'default') {
                return null;
              }
              return (
                <BalloonOption
                  key={index}
                  index={index}
                  cascaderOptions={applyOptions}
                  usedStyles={usedStyles}
                  defaultBalloon={markers[defaultBalloon].id}
                  update={update}
                  style={item.style}
                  assign={item.assign}
                  displayRender={displayRender}
                  id={part?.id || ''}
                />
              );
            })}
          {/* Add Style Button */}
          <AddOptionButton add={add} option="Balloon Style" />
        </section>
      </section>
      <div className="form-buttons form-section sider-panel-progress-footer">
        <ProgressFooter id="balloon-styles-footer" classes="panel-progression-footer" cypress="progress-footer" steps={navigation.maxTotalSteps} current={STYLES + 1} onNext={advance} canProgress canBack onBack={goBack} />
      </div>
    </>
  );
};
