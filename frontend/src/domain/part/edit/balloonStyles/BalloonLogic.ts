import { MarkerStyle } from 'types/marker';
import { MethodType } from 'view/global/defaults';
import { BaseOptionType } from 'antd/lib/cascader';
import { Characteristic } from 'types/characteristics';
import { sanitizedTextComparison } from 'utils/textOperations';
import { SettingState } from 'domain/setting/settingTypes';

export const convertCascader = (data: MethodType[], name: string, used: BaseOptionType[]) => {
  if (data && data.length > 0) {
    const options: BaseOptionType = {
      value: name.toLowerCase(),
      label: name.charAt(0).toUpperCase() + name.slice(1),
      children: [],
    };
    data.forEach((item) => {
      const level1: BaseOptionType = {
        value: item.value,
        label: item.label ? item.label : item.value.charAt(0).toUpperCase() + item.value.slice(1),
      };
      if (item.children) {
        level1.children = [];
        item.children.forEach((child) => {
          level1.children!.push({
            value: child.value.toLowerCase(),
            label: child.label ? child.label : child.value.charAt(0).toUpperCase() + child.value.slice(1),
            disabled: !!used.some((value) => value.includes(child.value.toLowerCase())),
          });
        });
      } else {
        level1.disabled = !!used.some((value) => value.includes(item.value.toLowerCase()));
      }
      options.children!.push(level1);
    });
    return options;
  }
  return null;
};

export const validateAssignedStyles = (styles: { [key: string]: MarkerStyle }) => {
  let valid = true;
  Object.values(styles).forEach((item) => {
    if (valid) {
      if (!item.style || item.style === '') {
        valid = false;
      }
      if (!item.assign || item.assign.length < 1 || !item.assign[0] || item.assign[0].replace(' ', '') === '') {
        valid = false;
      }
    }
  });
  return valid;
};

// Cycle through the Characteristic List and the Marker Assignments and update any Characteristics that match a new Assignment
// Need to make updates in the DB and in the Contexts, DB updates will happen in the Contexts
export const updateCharacteristicMarkers = (assignments: { [key: string]: MarkerStyle }, characteristics: Characteristic[], dispatch: (updates: Characteristic[], markers: { [key: string]: MarkerStyle }) => void) => {
  const newAssignments = { ...assignments };
  if (!newAssignments.default) {
    newAssignments.default = { style: 'f12c5566-7612-48e9-9534-ef90b276ebaa', assign: ['Default'] };
  }

  const updateList: Characteristic[] = [];
  const styleList = Object.values(newAssignments);
  characteristics.forEach((characteristic) => {
    const newItem = { ...characteristic };
    let matched = false;
    // match on attributes in order of importance
    if (characteristic.criticality) {
      for (let i = 0; i < styleList.length; i++) {
        if (styleList[i].assign[1] === characteristic.criticality) {
          // found the assignment, check if the right marker style is already applied
          if (styleList[i].style !== characteristic.markerStyle) {
            newItem.markerStyle = styleList[i].style;
          }
          matched = true;
          break;
        }
      }
    } else if (characteristic.operation) {
      for (let i = 0; i < styleList.length; i++) {
        if (styleList[i].assign[1] === characteristic.operation) {
          // found the assignment, check if the right marker style is already applied
          if (styleList[i].style !== characteristic.markerStyle) {
            newItem.markerStyle = styleList[i].style;
          }
          matched = true;
          break;
        }
      }
    } else if (characteristic.inspectionMethod) {
      for (let i = 0; i < styleList.length; i++) {
        if (styleList[i].assign[1] === characteristic.inspectionMethod) {
          // found the assignment, check if the right marker style is already applied
          if (styleList[i].style !== characteristic.markerStyle) {
            newItem.markerStyle = styleList[i].style;
          }
          matched = true;
          break;
        }
      }
    }
    // types have 3 layer depth: category > type > subtype
    else if (characteristic.notationSubtype) {
      for (let i = 0; i < styleList.length; i++) {
        if (sanitizedTextComparison(styleList[i].assign[0], 'type')) {
          if (sanitizedTextComparison(styleList[i].assign[2], characteristic.notationSubtype)) {
            if (styleList[i].style !== characteristic.markerStyle) {
              newItem.markerStyle = styleList[i].style;
            }
            matched = true;
            break;
          }
          // classifications have 2 layer depth: category > classification
        }
      }
    }

    // Characteristic didn't match one of the style assignments, check if Default style is different from current and assign if it is
    if (!matched && newItem.markerStyle !== newAssignments.default.style) {
      newItem.markerStyle = newAssignments.default.style;
      updateList.push(newItem);
    } else {
      updateList.push(newItem);
    }
  });

  dispatch(updateList, newAssignments);
};

export const checkForPresetStyle = (assignedStyles: string, settings: SettingState) => {
  const stylesObj = settings.stylePresets?.metadata || {};
  let match = 'None';
  Object.keys(stylesObj).forEach((key) => {
    if (JSON.stringify(stylesObj[key]?.setting || '{}') === assignedStyles) match = key;
  });
  return match;
};
