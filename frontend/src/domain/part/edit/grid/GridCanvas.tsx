import React, { useEffect } from 'react';
import { fabric } from 'fabric';
import { AppState } from 'modules/state';
import { PartStepEnum } from 'modules/navigation/navigationActions';
import { useDispatch, useSelector } from 'react-redux';
import { updateDrawingSheetThunk } from 'modules/partEditor/partEditorActions';

const { GRID } = PartStepEnum;

export interface GridCanvasProps {
  // showGrid: boolean;
  // allowGridEdit: boolean;
}

export const GridCanvas: React.FC<GridCanvasProps> = () => {
  const { gridRenderer, gridCache, partDrawing } = useSelector((state: AppState) => state.partEditor);
  const { capturedItems } = useSelector((state: AppState) => state.characteristics);
  const dispatch = useDispatch();
  const { layerManager, index: curPageIndex, tronInstance } = useSelector((state: AppState) => state.pdfView);
  const navigation = useSelector((state: AppState) => state.navigation);

  const gridOptions = JSON.parse(partDrawing?.gridOptions || '{}');

  // Wrap form updates in viewUpdated callback
  layerManager.subscribe('GridCanvas', 'pageComplete', () => {
    if (navigation.currentPartStep === GRID) {
      if (gridOptions.status === false || !partDrawing?.sheets[curPageIndex].useGrid) {
        gridRenderer.clearCanvas();
      } else {
        gridRenderer.renderToCanvas();
      }
    }
  });

  // When someone is done moving the grid, save the changes to the DB
  const handleGridModified = (): void => {
    if (gridRenderer && gridCache) {
      // log.debug('SAVE GRID TO DB: ', gridRenderer.grid);
      gridCache[curPageIndex] = gridRenderer.grid;
      // updateDrawingSheetThunk updates the captured items, so no need to do it here
      dispatch(updateDrawingSheetThunk(curPageIndex, { grid: gridRenderer.grid.toJsonString() }));
    }
  };

  /* eslint-disable no-param-reassign */
  // Method to setup mouse events whenever canvas is re-rendered
  const setCanvasEvents = (canvas: fabric.Canvas) => {
    if (!canvas) {
      return;
    }
    // Remove old event handlers so the handler doesn't fire repeatedly
    canvas.off('object:modified');
    // Attach events to grid's event handlers
    canvas.on('object:modified', handleGridModified);
  };

  const handleGridRotation = (delta: number) => {
    if (gridRenderer) {
      const grid = gridRenderer.rotate(delta);
      gridRenderer.grid = grid;
      // this should not be called on every page rotation since page rotation is not permanent
      // grid settings should not change on rotation
      // dispatch(updateDrawingSheetThunk(curPageIndex, { grid: grid.toJsonString() }));
    }
  };

  layerManager.subscribe('GridCanvas', 'layerCanvasMounted', (data: any) => {
    if (data.layer.name === 'grid') {
      const { canvas } = data.layer;
      if (!canvas) return;
      const rotation = tronInstance ? tronInstance.docViewer.getCompleteRotation(curPageIndex + 1) * 90 : 0;
      gridRenderer.setCanvas(canvas);
      setCanvasEvents(canvas);

      let changed = false;
      const width = tronInstance ? tronInstance.docViewer.getPageWidth(curPageIndex + 1) : gridRenderer.grid.initialCanvasWidth;
      const height = tronInstance ? tronInstance.docViewer.getPageHeight(curPageIndex + 1) : gridRenderer.grid.initialCanvasHeight;
      if ((gridRenderer.grid.pageRotation === 0 || gridRenderer.grid.pageRotation === 180) && gridRenderer.grid.initialCanvasWidth !== width) {
        [gridRenderer.grid.initialCanvasWidth, gridRenderer.grid.initialCanvasHeight] = [width, height];
        changed = true;
      }

      if (gridRenderer.grid.pageRotation !== rotation) {
        // We re-set the grid to 0 orientation on load, so if the PDF loads in angled, we need to turn the grid
        const delta = rotation - gridRenderer.grid.pageRotation;
        if (delta === 180 || delta === -180) {
          handleGridRotation(delta / 2);
          handleGridRotation(delta / 2);
        } else if (delta !== 0) {
          handleGridRotation(delta);
        }
      } else if (changed) {
        dispatch(updateDrawingSheetThunk(curPageIndex, { grid: gridRenderer.grid.toJsonString() }));
      }
      // This does the same thing as the useEffect below, but is necessary or the
      // grid won't render when browser is refreshed while on the grid stage
      if (navigation.currentPartStep === GRID) {
        // move grid to front
        layerManager.moveLayerToFront('grid');
      } else {
        // clear grid
        if (!canvas) return;
        // gridRenderer.setCanvas(canvas); // Is this necessary
        gridRenderer.clearCanvas();
      }
    }
  });

  // When the drawing is rotated, rotate the grid
  layerManager.subscribe('GridCanvas', 'viewRotated', (data: any) => {
    handleGridRotation(data.delta);
  });

  // Only display grid when on grid editor step
  useEffect(() => {
    if (navigation.currentPartStep === GRID) {
      const canvas = layerManager.getCurrentCanvasFor('grid');
      if (!canvas) return;
      if (gridOptions.status !== false && partDrawing?.sheets[curPageIndex].useGrid) {
        gridRenderer.setCanvas(canvas);
        layerManager.moveLayerToFront('grid');
      } else {
        gridRenderer.clearCanvas();
      }
    } else {
      const canvas = layerManager.getCurrentCanvasFor('grid');
      if (!canvas) return;
      gridRenderer.setCanvas(canvas); // Is this necessary
      gridRenderer.clearCanvas();
      layerManager.moveLayerToFront('markers'); // TODO: Grid canvas shouldn't know about markers canvas
    }
  }, [navigation.currentPartStep]);

  useEffect(() => {
    if (gridRenderer && (gridOptions.status === false || !partDrawing?.sheets[curPageIndex]?.useGrid)) {
      gridRenderer.clearCanvas();
    }
  }, [gridOptions.status, partDrawing?.sheets[curPageIndex]?.useGrid]);
  return null;
};
