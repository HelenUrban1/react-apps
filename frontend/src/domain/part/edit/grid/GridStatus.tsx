import React, { useState, useEffect } from 'react';
import { Alert, Switch } from 'antd';
import { i18n } from 'i18n';
import { DrawingSheet } from 'graphql/drawing_sheet';

interface GridStatusProps {
  showAlert: boolean;
  setShowAlert: (show: boolean) => void;
  gridSettings: any;
  document: string | undefined;
  drawingSheet?: DrawingSheet;
  updateGridSetting: (isGridOn: boolean, target: 'Drawing' | 'Sheet' | 'Both') => void;
}

export const GridStatus = ({ showAlert, setShowAlert, gridSettings, document, drawingSheet, updateGridSetting }: GridStatusProps) => {
  const useGrid = gridSettings.status !== false && drawingSheet && drawingSheet.useGrid;
  const [value, setValue] = useState(drawingSheet ? drawingSheet.useGrid : gridSettings.status !== false);

  useEffect(() => {
    if (gridSettings.status !== false) {
      setValue(drawingSheet ? drawingSheet.useGrid : gridSettings.status !== false);
    } else {
      setValue(false);
    }
  }, [gridSettings.status, drawingSheet?.useGrid]);

  const handleToggle = (checked: boolean) => {
    setValue(checked);
    if (drawingSheet) {
      // update drawing sheet
      if (gridSettings.status === false) {
        updateGridSetting(checked, 'Both');
      } else {
        updateGridSetting(checked, 'Sheet');
      }
    } else {
      updateGridSetting(checked, 'Drawing');
    }
  };

  return (
    <section role="none" className="grid-status-container  group-flex-3">
      <div className="grid-status-row" style={{ height: '40px' }}>
        <span>{i18n(drawingSheet ? 'wizard.useGridPage' : 'wizard.useGrid')}</span>
        <Switch //
          className="grid-status-switch"
          data-cy={drawingSheet ? 'grid-status-switch-page' : 'grid-status-switch-doc'}
          checked={value}
          checkedChildren={<span className="grid-status-label">{i18n('common.on')}</span>}
          unCheckedChildren={<span className="grid-status-label">{i18n('common.off')}</span>}
          onClick={handleToggle}
        />
      </div>
      {showAlert && useGrid && (
        <Alert
          message={i18n('entities.part.instructions.gridHidden')}
          closable
          type="info"
          className="ix-info-alert"
          data-cy="grid-alert"
          onClose={() => {
            setShowAlert(false);
          }}
        />
      )}
    </section>
  );
};
