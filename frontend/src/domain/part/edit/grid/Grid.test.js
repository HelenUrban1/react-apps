import {
  getIndexToIns, //
  invert,
  toPreciseFloat,
  validateSeriesEdges,
  buildSeries,
  indexToLetters,
  lettersToIndex,
  buildNumberSeries,
  buildLetterSeries,
  Grid,
} from './Grid';

describe('getIndexToIns', () => {
  it('should return the index where the provided number would fit into the provided array', () => {
    expect(getIndexToIns([10, 20, 30, 40, 50], 10)).toBe(0); // Edge test
    expect(getIndexToIns([10, 20, 30, 40, 50], 15)).toBe(0);
    expect(getIndexToIns([10, 20, 30, 40, 50], 20)).toBe(0);
    expect(getIndexToIns([10, 20, 30, 40, 50], 25)).toBe(1);
    expect(getIndexToIns([10, 20, 30, 40, 50], 30)).toBe(1);
    expect(getIndexToIns([10, 20, 30, 40, 50], 35)).toBe(2);
    expect(getIndexToIns([10, 20, 30, 40, 50], 40)).toBe(2);
    expect(getIndexToIns([10, 20, 30, 40, 50], 45)).toBe(3);
    expect(getIndexToIns([10, 20, 30, 40, 50], 50)).toBe(3); // Edge test
  });

  it('should return -1 if the value is below the first number in the arry', () => {
    expect(getIndexToIns([], 1)).toBe(-1);
  });

  it('should return -2 if the value is below the first number in the arry', () => {
    expect(getIndexToIns([10, 20, 30, 40, 50], 5)).toBe(-2);
  });

  it('should return -3 if the value is above the last number in the arry', () => {
    expect(getIndexToIns([10, 20, 30, 40, 50], 55)).toBe(-3);
  });
});

describe('invert', () => {
  it('should return an array whose initial values are reversed and then subtracted from the max', () => {
    const arr1 = [0, 1, 2, 4];
    expect(invert(arr1, 4)).toStrictEqual([0, 2, 3, 4]);

    const arr2 = [0, 2, 3, 4];
    expect(invert(arr2, 4)).toStrictEqual([0, 1, 2, 4]);
  });
});

describe('toPreciseFloat', () => {
  it('should return a float trimmed to the specified precision', () => {
    expect(toPreciseFloat(0.1234, 2)).toBe(0.12);
    expect(toPreciseFloat(1, 2)).toBe(1.0);
    expect(toPreciseFloat(1234.5678, 2)).toBe(1234.57);
  });

  it('should round to the nearest decmal', () => {
    expect(toPreciseFloat(1234.5678, 2)).toBe(1234.57);
    expect(toPreciseFloat(1234.5631, 2)).toBe(1234.56);
  });
});

describe('Label Series Calculation', () => {
  describe('validateSeriesEdges', () => {
    it('should return true if starting and ending values are both numbers', () => {
      expect(validateSeriesEdges('1', '5')).toBe(true);
    });
    it('should return true if starting and ending values are both strings', () => {
      expect(validateSeriesEdges('A', 'E')).toBe(true);
    });
    it('should return false if starting and ending values are not both numbers', () => {
      expect(validateSeriesEdges('1', 'E')).toBe(false);
    });
    it('should return false if starting and ending values are not both strings', () => {
      expect(validateSeriesEdges('A', '5')).toBe(false);
    });
  });

  describe('buildSeries', () => {
    it('should return a ascending series that inclusivly spans the starting and ending values', () => {
      expect(buildSeries('1', '5')).toStrictEqual(['1', '2', '3', '4', '5']);
      expect(buildSeries('A', 'E')).toStrictEqual(['A', 'B', 'C', 'D', 'E']);
      expect(buildSeries('AA', 'AE')).toStrictEqual(['AA', 'AB', 'AC', 'AD', 'AE']);
    });
    it('should return a descending series that inclusivly spans the starting and ending values', () => {
      expect(buildSeries('5', '1')).toStrictEqual(['5', '4', '3', '2', '1']);
      expect(buildSeries('E', 'A')).toStrictEqual(['E', 'D', 'C', 'B', 'A']);
      expect(buildSeries('AE', 'AA')).toStrictEqual(['AE', 'AD', 'AC', 'AB', 'AA']);
    });
    it('should return a series that does not include values in the provided list of skip values', () => {
      expect(buildSeries('5', '1', ['2', '3'])).toStrictEqual(['5', '4', '1']);
      expect(buildSeries('1', '5', ['2', '3'])).toStrictEqual(['1', '4', '5']);
      expect(buildSeries('E', 'A', ['B', 'C'])).toStrictEqual(['E', 'D', 'A']);
      expect(buildSeries('A', 'E', ['B', 'C'])).toStrictEqual(['A', 'D', 'E']);
    });
    it('should limit the series size to the designated maxItems', () => {
      expect(buildSeries('1', '1500').length).toBe(100);
      expect(buildSeries('1', '1500', [], 1200).length).toBe(1200);
      expect(buildSeries('A', 'ABCDE').length).toBe(100);
      expect(buildSeries('A', 'ABCDE', [], 1200).length).toBe(1200);
    });
    it('should throw an error if the starting and ending values are not the same type', () => {
      // TODO: Update test to mock exception rather than throw a real exception
      try {
        buildSeries('1', 'E');
      } catch (e) {
        expect(e.message).toBe(`Starting and ending values of a series must be both numbers or both letters but were (1, E)`);
      }
      try {
        buildSeries('A', '5');
      } catch (e) {
        expect(e.message).toBe(`Starting and ending values of a series must be both numbers or both letters but were (A, 5)`);
      }
    });
  });

  describe('buildNumberSeries', () => {
    it('should return a ascending series that inclusivly spans the starting and ending values', () => {
      expect(buildNumberSeries(1, 5)).toStrictEqual(['1', '2', '3', '4', '5']);
    });
    it('should return a descending series that inclusivly spans the starting and ending values', () => {
      expect(buildNumberSeries(5, 1)).toStrictEqual(['5', '4', '3', '2', '1']);
    });
    it('should return a series that does not include values in the provided list of skip values', () => {
      expect(buildNumberSeries(5, 1, ['2', '3'])).toStrictEqual(['5', '4', '1']);
      expect(buildNumberSeries(1, 5, ['2', '3'])).toStrictEqual(['1', '4', '5']);
    });
    it('should limit the series size to the designated maxItems', () => {
      expect(buildNumberSeries(1, 1500).length).toBe(100);
      expect(buildNumberSeries(1, 1500, [], 1200).length).toBe(1200);
    });
  });

  describe('buildLetterSeries', () => {
    it('should return an array of ascending excel style column names that inclusivly spans the starting and ending values', () => {
      expect(buildLetterSeries('A', 'E')).toStrictEqual(['A', 'B', 'C', 'D', 'E']);
      expect(buildLetterSeries('AA', 'AE')).toStrictEqual(['AA', 'AB', 'AC', 'AD', 'AE']);
    });
    it('should return an array of descending excel style column names that inclusivly spans the starting and ending values', () => {
      expect(buildLetterSeries('E', 'A')).toStrictEqual(['E', 'D', 'C', 'B', 'A']);
      expect(buildLetterSeries('AE', 'AA')).toStrictEqual(['AE', 'AD', 'AC', 'AB', 'AA']);
    });
    it('should return a series that does not include values in the provided list of skip values', () => {
      expect(buildLetterSeries('E', 'A', ['B', 'C'])).toStrictEqual(['E', 'D', 'A']);
    });
    it('should limit the max items', () => {
      expect(buildLetterSeries('A', 'ABCDE').length).toBe(100);
      expect(buildLetterSeries('A', 'ABCDE', [], 1200).length).toBe(1200);
    });
  });

  describe('indexToLetters', () => {
    it('should return the excel style letter code for the zero based index provided ', () => {
      expect(indexToLetters(0)).toBe('A');
      expect(indexToLetters(25)).toBe('Z');
      expect(indexToLetters(26)).toBe('AA');
      expect(indexToLetters(730)).toBe('ABC');
      expect(indexToLetters(19009)).toBe('ABCD');
    });
  });

  describe('lettersToIndex', () => {
    it('should return the zero based index for the excel style letter code provided', () => {
      expect(lettersToIndex('A')).toBe(0);
      expect(lettersToIndex('Z')).toBe(25);
      expect(lettersToIndex('AA')).toBe(26);
      expect(lettersToIndex('ABC')).toBe(730);
      expect(lettersToIndex('ABCD')).toBe(19009);
    });
  });
});

describe('Grid', () => {
  describe('#constructor', () => {
    it('should initalize with default values if no inputs are provided', () => {
      const grid = new Grid();
      expect(grid.initialCanvasWidth).toBe(0); // 1280
      expect(grid.initialCanvasHeight).toBe(0); // 720
      expect(grid.lineWidth).toBe(5);
      expect(grid.labelColor).toBe('#963CBD');
      expect(grid.lineColor).toBe('#963CBD');
      expect(grid.columnLeftLabel).toBe('4');
      expect(grid.columnRightLabel).toBe('1');
      expect(grid.rowTopLabel).toBe('B');
      expect(grid.rowBottomLabel).toBe('A');
      expect(grid.skipLabels.length).toBe(0);
      expect(grid.showLabels).toBe(true);
    });

    it('should initalize with initial Canvas Width abd Height values if provided', () => {
      const grid = new Grid({
        initialCanvasWidth: 1280,
        initialCanvasHeight: 720,
      });
      expect(grid.initialCanvasWidth).toBe(1280);
      expect(grid.initialCanvasHeight).toBe(720);
      expect(grid.lineWidth).toBe(5);
      expect(grid.labelColor).toBe('#963CBD');
      expect(grid.lineColor).toBe('#963CBD');
      expect(grid.columnLeftLabel).toBe('4');
      expect(grid.columnRightLabel).toBe('1');
      expect(grid.rowTopLabel).toBe('B');
      expect(grid.rowBottomLabel).toBe('A');
      expect(grid.skipLabels.length).toBe(0);
      expect(grid.showLabels).toBe(true);
    });

    it('should create a new instance built using the provided values', () => {
      const params = {
        initialCanvasWidth: 1000,
        initialCanvasHeight: 500,
        lineWidth: 1,
        labelColor: '#000000',
        lineColor: '#ffffff',
        columnLeftLabel: '2',
        columnRightLabel: '15',
        rowTopLabel: 'E',
        rowBottomLabel: 'Z',
        skipLabels: ['A', 'B'],
        showLabels: false,
      };
      const grid = new Grid(params);
      expect(grid.initialCanvasWidth).toBe(1000);
      expect(grid.initialCanvasHeight).toBe(500);
      expect(grid.lineWidth).toBe(1);
      expect(grid.labelColor).toBe('#000000');
      expect(grid.lineColor).toBe('#ffffff');
      expect(grid.columnLeftLabel).toBe('2');
      expect(grid.columnRightLabel).toBe('15');
      expect(grid.rowTopLabel).toBe('E');
      expect(grid.rowBottomLabel).toBe('Z');
      expect(grid.skipLabels.length).toBe(2);
      expect(grid.showLabels).toBe(false);
    });

    it('should create a new instance without calculating new rows and columns if they are provided', () => {
      const params = {
        initialCanvasWidth: 1280,
        initialCanvasHeight: 720,
        lineWidth: 1,
        labelColor: '#000000',
        lineColor: '#ffffff',
        columnLeftLabel: '4',
        columnRightLabel: '1',
        rowTopLabel: 'B',
        rowBottomLabel: 'A',
        skipLabels: [],
        showLabels: false,
        rowLineRatios: [0, 0.21882, 0.47921, 0.73961, 1],
        rowLines: [38, 238, 476, 714, 952],
        rowLabels: ['B', 'A'],
        colLineRatios: [0, 0.2302, 0.4868, 0.7434, 1],
        colLines: [25, 243, 486, 729, 972],
        colLabels: ['4', '3', '2', '1'],
      };
      const grid = new Grid(params);
      expect(grid.initialCanvasWidth).toBe(1280);
      expect(grid.initialCanvasHeight).toBe(720);
      expect(grid.lineWidth).toBe(1);
      expect(grid.labelColor).toBe('#000000');
      expect(grid.lineColor).toBe('#ffffff');
      expect(grid.columnLeftLabel).toBe('4');
      expect(grid.columnRightLabel).toBe('1');
      expect(grid.rowTopLabel).toBe('B');
      expect(grid.rowBottomLabel).toBe('A');
      expect(grid.skipLabels.length).toBe(0);
      expect(grid.showLabels).toBe(false);
      expect(grid.rowLabels).toStrictEqual(['B', 'A']);
      expect(grid.rowLineRatios).toStrictEqual([0, 0.21882, 0.47921, 0.73961, 1]);
      expect(grid.rowLines).toStrictEqual([38, 238, 476, 714, 952]);
      expect(grid.colLabels).toStrictEqual(['4', '3', '2', '1']);
      expect(grid.colLineRatios).toStrictEqual([0, 0.2302, 0.4868, 0.7434, 1]);
      expect(grid.colLines).toStrictEqual([25, 243, 486, 729, 972]);
    });
  });

  describe('.toJson', () => {
    it('should return JSON object that can be used to reconstitute the grid', () => {
      const grid = new Grid({
        initialCanvasWidth: 1280,
        initialCanvasHeight: 720,
      });
      const gridJson = grid.toJson();
      expect(gridJson.initialCanvasWidth).toBe(1280);
      expect(gridJson.initialCanvasHeight).toBe(720);
      expect(gridJson.lineWidth).toBe(5);
      expect(gridJson.labelColor).toBe('#963CBD');
      expect(gridJson.lineColor).toBe('#963CBD');
      expect(gridJson.columnLeftLabel).toBe('4');
      expect(gridJson.columnRightLabel).toBe('1');
      expect(gridJson.rowTopLabel).toBe('B');
      expect(gridJson.rowBottomLabel).toBe('A');
      expect(gridJson.skipLabels.length).toBe(0);
      expect(gridJson.showLabels).toBe(true);
    });
  });

  describe('.toJsonString', () => {
    it('should return a JSON.stringified instance of the Grid', () => {
      const grid = new Grid({
        initialCanvasWidth: 1280,
        initialCanvasHeight: 720,
        initialPageRotation: 0,
      });
      const gridJsonString = grid.toJsonString();
      // console.log(gridJsonString);
      expect(gridJsonString).toBe(
        '{"initialCanvasWidth":1280,"initialCanvasHeight":720,"pageRotation":0,"lineWidth":5,"labelColor":"#963CBD","lineColor":"#963CBD","columnLeftLabel":"4","columnRightLabel":"1","rowTopLabel":"B","rowBottomLabel":"A","skipLabels":[],"showLabels":true,"rowLineRatios":[0,0.5,1],"rowLines":[18,358,698],"rowLabels":["B","A"],"colLineRatios":[0,0.25,0.5,0.75,1],"colLines":[32,335,638,941,1244],"colLabels":["4","3","2","1"]}',
      );
    });
  });

  describe('.fromJsonString', () => {
    it('should return an instance from the stringified JSON', () => {
      const grid = new Grid({
        initialCanvasWidth: 1280,
        initialCanvasHeight: 720,
      });
      const gridJsonString = grid.toJsonString();
      const derivedGrid = Grid.fromJsonString(gridJsonString);
      expect(derivedGrid.initialCanvasWidth).toBe(1280);
      expect(derivedGrid.initialCanvasHeight).toBe(720);
      expect(derivedGrid.lineWidth).toBe(5);
      expect(derivedGrid.labelColor).toBe('#963CBD');
      expect(derivedGrid.lineColor).toBe('#963CBD');
      expect(derivedGrid.columnLeftLabel).toBe('4');
      expect(derivedGrid.columnRightLabel).toBe('1');
      expect(derivedGrid.rowTopLabel).toBe('B');
      expect(derivedGrid.rowBottomLabel).toBe('A');
      expect(derivedGrid.skipLabels.length).toBe(0);
      expect(derivedGrid.showLabels).toBe(true);
    });
  });

  describe('#setLineStyle', () => {
    it('should set the default line style', () => {
      const grid = new Grid({
        initialCanvasWidth: 1280,
        initialCanvasHeight: 720,
      });
      expect(grid.defaultLineStyle).toStrictEqual({
        padding: 6,
        stroke: '#963CBD',
        strokeWidth: 5,
      });
      const lineWidth = 2;
      const lineColor = '#000000';
      grid.setLineStyle(lineWidth, lineColor);
      expect(grid.defaultLineStyle).toStrictEqual({
        padding: 6,
        stroke: '#000000',
        strokeWidth: 2,
      });
    });
  });

  describe('#setLabelStyle', () => {
    it('should set the default label style', () => {
      const grid = new Grid({
        initialCanvasWidth: 1280,
        initialCanvasHeight: 720,
      });
      expect(grid.defaultLabelStyle).toStrictEqual({
        fill: '#963CBD',
        fontSize: 22,
        fontFamily: 'Arial, Helvetica, sans-serif',
        textBackgroundColor: '#fff',
      });
      const labelColor = '#000000';
      const fontSize = 10;
      const fontFamily = 'Arial Black';
      grid.setLabelStyle(labelColor, fontSize, fontFamily);
      expect(grid.defaultLabelStyle).toStrictEqual({
        fill: labelColor,
        fontSize,
        fontFamily,
        textBackgroundColor: '#fff',
      });
    });
  });

  describe('#setLabels', () => {
    it('should build the row and column label series', () => {
      const grid = new Grid({
        initialCanvasWidth: 1280,
        initialCanvasHeight: 720,
      });
      const columnLeftLabel = '1';
      const columnRightLabel = '5';
      const rowTopLabel = 'A';
      const rowBottomLabel = 'D';
      const skipLabels = ['2', 'C'];
      grid.setLabels(columnLeftLabel, columnRightLabel, rowTopLabel, rowBottomLabel, skipLabels);
      expect(grid.rowLabels).toStrictEqual(['A', 'B', 'D']);
      expect(grid.colLabels).toStrictEqual(['1', '3', '4', '5']);
    });
  });

  describe('#saveGridRatios', () => {
    it('should calculate and save the line positions relative to the canvas size', () => {
      const grid = new Grid({
        initialCanvasWidth: 1000,
        initialCanvasHeight: 1000,
        lineWidth: 1,
        labelColor: '#963CBD',
        lineColor: '#963CBD',
        columnLeftLabel: '4',
        columnRightLabel: '1',
        rowTopLabel: 'D',
        rowBottomLabel: 'A',
        skipLabels: [],
        showLabels: true,
      });
      grid.saveGridRatios();
      expect(grid.rowLineRatios).toStrictEqual([0, 0.25, 0.5, 0.75, 1]);
      expect(grid.rowLines).toStrictEqual([25, 262, 499, 736, 973]);
      expect(grid.colLineRatios).toStrictEqual([0, 0.25, 0.5, 0.75, 1]);
      expect(grid.colLines).toStrictEqual([25, 262, 499, 736, 973]);
    });
  });

  describe('#xCol', () => {
    it('should return the x coordinate of the column line index provided', () => {
      const grid = new Grid({
        initialCanvasWidth: 1280,
        initialCanvasHeight: 720,
      });
      expect(grid.initialCanvasWidth).toBe(1280);
      expect(grid.colLines).toStrictEqual([32, 335, 638, 941, 1244]);
      expect(grid.xCol(0)).toBe(32);
      expect(grid.xCol(1)).toBe(335);
      expect(grid.xCol(2)).toBe(638);
      expect(grid.xCol(3)).toBe(941);
      expect(grid.xCol(4)).toBe(1244);
    });
  });

  describe('#yRow', () => {
    it('should return the y coordinate of the row line index provided', () => {
      const grid = new Grid({
        initialCanvasWidth: 1280,
        initialCanvasHeight: 720,
      });
      expect(grid.initialCanvasHeight).toBe(720);
      expect(grid.rowLines).toStrictEqual([18, 358, 698]);
      expect(grid.yRow(0)).toBe(18);
      expect(grid.yRow(1)).toBe(358);
      expect(grid.yRow(2)).toBe(698);
    });
  });

  describe('#xGridLeftEdge', () => {
    it('should return the x coordinate of the left edge of the grid', () => {
      const grid = new Grid({
        initialCanvasWidth: 1280,
        initialCanvasHeight: 720,
      });
      expect(grid.initialCanvasWidth).toBe(1280);
      expect(grid.colLines).toStrictEqual([32, 335, 638, 941, 1244]);
      expect(grid.xGridLeftEdge()).toBe(32);
    });
  });

  describe('#xGridRightEdge', () => {
    it('should return the x coordinate of the right edge of the grid', () => {
      const grid = new Grid({
        initialCanvasWidth: 1280,
        initialCanvasHeight: 720,
      });
      expect(grid.initialCanvasWidth).toBe(1280);
      expect(grid.colLines).toStrictEqual([32, 335, 638, 941, 1244]);
      expect(grid.xGridRightEdge()).toBe(1244);
    });
  });

  describe('#yGridTopEdge', () => {
    it('should return the y coordinate of the top edge of the grid', () => {
      const grid = new Grid({
        initialCanvasWidth: 1280,
        initialCanvasHeight: 720,
      });
      expect(grid.initialCanvasHeight).toBe(720);
      expect(grid.rowLines).toStrictEqual([18, 358, 698]);
      expect(grid.yGridTopEdge()).toBe(18);
    });
  });

  describe('#yGridBottomEdge', () => {
    it('should return the y coordinate of the bottom edge of the grid', () => {
      const grid = new Grid({
        initialCanvasWidth: 1280,
        initialCanvasHeight: 720,
      });
      expect(grid.initialCanvasHeight).toBe(720);
      expect(grid.rowLines).toStrictEqual([18, 358, 698]);
      expect(grid.yGridBottomEdge()).toBe(698);
    });
  });

  describe('#height', () => {
    it('should return the height of the grid', () => {
      const grid = new Grid({
        initialCanvasWidth: 1280,
        initialCanvasHeight: 720,
      });
      expect(grid.initialCanvasHeight).toBe(720);
      expect(grid.rowLines).toStrictEqual([18, 358, 698]);
      expect(grid.height()).toBe(680);
    });
  });

  describe('#width', () => {
    it('should return the width of the grid', () => {
      const grid = new Grid({
        initialCanvasWidth: 1280,
        initialCanvasHeight: 720,
      });
      expect(grid.initialCanvasWidth).toBe(1280);
      expect(grid.colLines).toStrictEqual([32, 335, 638, 941, 1244]);
      expect(grid.width()).toBe(1212);
    });
  });

  describe('#colGridRatio', () => {
    it('should return the ratio that the col position relative to the grid width', () => {
      const grid = new Grid({
        initialCanvasWidth: 1280,
        initialCanvasHeight: 720,
      });
      expect(grid.initialCanvasWidth).toBe(1280);
      expect(grid.colLines).toStrictEqual([32, 335, 638, 941, 1244]);
      expect(grid.colLineRatios).toStrictEqual([0, 0.25, 0.5, 0.75, 1]);
      expect(grid.colGridRatio(0)).toBe(0);
      expect(grid.colGridRatio(1)).toBe(0.25);
      expect(grid.colGridRatio(2)).toBe(0.5);
      expect(grid.colGridRatio(3)).toBe(0.75);
      expect(grid.colGridRatio(4)).toBe(1);
    });
  });

  describe('#xFromColGridRatio', () => {
    it('should return the x value from the col grid ratio', () => {
      const grid = new Grid({
        initialCanvasWidth: 1280,
        initialCanvasHeight: 720,
      });
      expect(grid.initialCanvasWidth).toBe(1280);
      expect(grid.colLines).toStrictEqual([32, 335, 638, 941, 1244]);
      expect(grid.colLineRatios).toStrictEqual([0, 0.25, 0.5, 0.75, 1]);
      expect(grid.xFromColGridRatio(0)).toBe(32);
      expect(grid.xFromColGridRatio(0.25)).toBe(335);
      expect(grid.xFromColGridRatio(0.5)).toBe(638);
      expect(grid.xFromColGridRatio(0.75)).toBe(941);
      expect(grid.xFromColGridRatio(1)).toBe(1244);
    });
  });

  describe('#rowGridRatio', () => {
    it('should return the ratio that the row position relative to the grid width', () => {
      const grid = new Grid({
        initialCanvasWidth: 1280,
        initialCanvasHeight: 720,
      });
      expect(grid.initialCanvasHeight).toBe(720);
      expect(grid.rowLines).toStrictEqual([18, 358, 698]);
      expect(grid.rowLineRatios).toStrictEqual([0, 0.5, 1]);
      expect(grid.rowGridRatio(0)).toBe(0);
      expect(grid.rowGridRatio(1)).toBe(0.5);
      expect(grid.rowGridRatio(2)).toBe(1);
    });
  });

  describe('#yFromRowGridRatio', () => {
    it('should return the y coordinate of the row relative to the grid height', () => {
      const grid = new Grid({
        initialCanvasWidth: 1280,
        initialCanvasHeight: 720,
      });
      expect(grid.initialCanvasHeight).toBe(720);
      expect(grid.rowLines).toStrictEqual([18, 358, 698]);
      expect(grid.rowLineRatios).toStrictEqual([0, 0.5, 1]);
      expect(grid.yFromRowGridRatio(0)).toBe(18);
      expect(grid.yFromRowGridRatio(0.5)).toBe(358);
      expect(grid.yFromRowGridRatio(1)).toBe(698);
    });
  });

  describe('#labelForCell', () => {
    it('should return the label for the cell address at the given row and column indexes', () => {
      const grid = new Grid({
        initialCanvasWidth: 1280,
        initialCanvasHeight: 720,
      });
      expect(grid.labelForCell(0, 0)).toBe('4B');
      expect(grid.labelForCell(0, 1)).toBe('3B');
      expect(grid.labelForCell(0, 2)).toBe('2B');
      expect(grid.labelForCell(0, 3)).toBe('1B');
      expect(grid.labelForCell(1, 0)).toBe('4A');
      expect(grid.labelForCell(1, 1)).toBe('3A');
      expect(grid.labelForCell(1, 2)).toBe('2A');
      expect(grid.labelForCell(1, 3)).toBe('1A');
    });
  });

  describe('#cellLabelForPoint', () => {
    it('should return the label for the cell address at the given row and column indexes', () => {
      const grid = new Grid({
        initialCanvasWidth: 1280,
        initialCanvasHeight: 720,
      });
      expect(grid.cellLabelForPoint(50, 50)).toBe('4B');
      expect(grid.cellLabelForPoint(500, 500)).toBe('3A');
    });
  });

  describe('#rotate', () => {
    it('should rotate the grid 90 CW', () => {
      const params = {
        initialCanvasWidth: 1000,
        initialCanvasHeight: 500,
        lineWidth: 1,
        labelColor: '#000000',
        lineColor: '#ffffff',
        columnLeftLabel: '1',
        columnRightLabel: '3',
        rowTopLabel: 'A',
        rowBottomLabel: 'C',
        skipLabels: [],
        showLabels: false,
        rowLabels: ['A', 'B', 'C'],
        rowLineRatios: [0, 0.25, 0.5, 1],
        rowLines: [0, 250, 500, 1000],
        colLabels: ['1', '2', '3'],
        colLineRatios: [0, 0.25, 0.5, 1],
        colLines: [0, 125, 250, 500],
      };
      const grid = new Grid(params);

      expect(grid.initialCanvasWidth).toBe(1000);
      expect(grid.initialCanvasHeight).toBe(500);
      expect(grid.columnLeftLabel).toBe('1');
      expect(grid.columnRightLabel).toBe('3');
      expect(grid.rowTopLabel).toBe('A');
      expect(grid.rowBottomLabel).toBe('C');

      expect(grid.rowLabels).toStrictEqual(['A', 'B', 'C']);
      expect(grid.rowLineRatios).toStrictEqual([0, 0.25, 0.5, 1]);
      expect(grid.rowLines).toStrictEqual([0, 250, 500, 1000]);
      expect(grid.colLabels).toStrictEqual(['1', '2', '3']);
      expect(grid.colLineRatios).toStrictEqual([0, 0.25, 0.5, 1]);
      expect(grid.colLines).toStrictEqual([0, 125, 250, 500]);

      grid.rotate(90);

      expect(grid.initialCanvasWidth).toBe(500);
      expect(grid.initialCanvasHeight).toBe(1000);
      expect(grid.columnLeftLabel).toBe('C');
      expect(grid.columnRightLabel).toBe('A');
      expect(grid.rowTopLabel).toBe('1');
      expect(grid.rowBottomLabel).toBe('3');

      expect(grid.rowLabels).toStrictEqual(['1', '2', '3']);
      expect(grid.rowLineRatios).toStrictEqual([0, 0.25, 0.5, 1]);
      expect(grid.rowLines).toStrictEqual([0, 125, 250, 500]);
      expect(grid.colLabels).toStrictEqual(['C', 'B', 'A']);
      expect(grid.colLineRatios).toStrictEqual([0, 0.5, 0.75, 1]);
      expect(grid.colLines).toStrictEqual([-500, 0, 250, 500]);

      grid.rotate(-90);

      expect(grid.initialCanvasWidth).toBe(1000);
      expect(grid.initialCanvasHeight).toBe(500);
      expect(grid.columnLeftLabel).toBe('1');
      expect(grid.columnRightLabel).toBe('3');
      expect(grid.rowTopLabel).toBe('A');
      expect(grid.rowBottomLabel).toBe('C');

      expect(grid.rowLabels).toStrictEqual(['A', 'B', 'C']);
      expect(grid.rowLineRatios).toStrictEqual([0, 0.25, 0.5, 1]);
      expect(grid.rowLines).toStrictEqual([0, 250, 500, 1000]);
      expect(grid.colLabels).toStrictEqual(['1', '2', '3']);
      expect(grid.colLineRatios).toStrictEqual([0, 0.25, 0.5, 1]);
      expect(grid.colLines).toStrictEqual([0, 125, 250, 500]);

      grid.rotate(-90);

      expect(grid.initialCanvasWidth).toBe(500);
      expect(grid.initialCanvasHeight).toBe(1000);
      expect(grid.columnLeftLabel).toBe('A');
      expect(grid.columnRightLabel).toBe('C');
      expect(grid.rowTopLabel).toBe('3');
      expect(grid.rowBottomLabel).toBe('1');

      expect(grid.rowLabels).toStrictEqual(['3', '2', '1']);
      expect(grid.rowLineRatios).toStrictEqual([0, 0.5, 0.75, 1]);
      expect(grid.rowLines).toStrictEqual([500, 750, 875, 1000]);
      expect(grid.colLabels).toStrictEqual(['A', 'B', 'C']);
      expect(grid.colLineRatios).toStrictEqual([0, 0.25, 0.5, 1]);
      expect(grid.colLines).toStrictEqual([0, 250, 500, 1000]);

      grid.rotate(90);

      expect(grid.initialCanvasWidth).toBe(1000);
      expect(grid.initialCanvasHeight).toBe(500);
      expect(grid.columnLeftLabel).toBe('1');
      expect(grid.columnRightLabel).toBe('3');
      expect(grid.rowTopLabel).toBe('A');
      expect(grid.rowBottomLabel).toBe('C');

      expect(grid.rowLabels).toStrictEqual(['A', 'B', 'C']);
      expect(grid.rowLineRatios).toStrictEqual([0, 0.25, 0.5, 1]);
      expect(grid.rowLines).toStrictEqual([0, 250, 500, 1000]);
      expect(grid.colLabels).toStrictEqual(['1', '2', '3']);
      expect(grid.colLineRatios).toStrictEqual([0, 0.25, 0.5, 1]);
      expect(grid.colLines).toStrictEqual([0, 125, 250, 500]);
    });
  });
});
