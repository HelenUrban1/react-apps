import React, { ChangeEvent, FormEvent, useState, useEffect, useRef, useCallback } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { AppState } from 'modules/state';
import { Input, Row, Col, Form, Button, Tooltip } from 'antd';
import { PartStepEnum } from 'modules/navigation/navigationActions';
import log from 'modules/shared/logger';
import PanelHeader from 'styleguide/PanelHeader/PanelHeader';
import { i18n } from 'i18n';
import { ProgressFooter } from 'styleguide/ProgressionFooter';
import Analytics from 'modules/shared/analytics/analytics';
import { ColorPicker } from 'styleguide/ColorPicker';
import { PartEditorActions, updateDrawingSheetsThunk, updateDrawingSheetThunk, updateDrawingThunk } from 'modules/partEditor/partEditorActions';
import { DrawingSheetChange } from 'graphql/drawing_sheet';
import { updatePartThunk } from 'modules/part/partActions';
import { flipAlphaNumeric } from 'utils/textOperations';
import { debounce } from 'lodash';
import { Grid, validateSeriesEdges } from './Grid';
import { GridStatus } from './GridStatus';

const { GRID, STYLES } = PartStepEnum;

// TODO: fix or turn off dependency cycle warnings
export interface GridFormProps {
  next: () => void;
  back: () => void;
  showAlert: boolean;
  setShowAlert: (val: boolean) => void;
}

interface FormValues {
  showLabels: boolean;
  labelColor: string;
  lineColor: string;
  lineWidth: string;
  columnLeftLabel: string;
  columnRightLabel: string;
  rowTopLabel: string;
  rowBottomLabel: string;
  skipLabels: string[];
  max: number;
  // Allows access to values using brackets - object[variable]
  [key: string]: string | string[] | boolean | number;
}

// Helper RegEx to ensure inputs are either numbers or letters and no larger than two characters
const RE_ALPHA_NUMERIC = RegExp(/^[0-9a-zA-Z]{1,2}$/);

export const GridForm: React.FC<GridFormProps> = ({ next, back, showAlert, setShowAlert }) => {
  const navigation = useSelector((state: AppState) => state.navigation);

  // Part details from wizard context
  const { part } = useSelector((state: AppState) => state.part);
  const { gridRenderer, gridCache, partDrawing, partEditorLoaded } = useSelector((state: AppState) => state.partEditor);
  const { index: curPageIndex } = useSelector((state: AppState) => state.pdfView);
  const dispatch = useDispatch();

  const gridSettings = JSON.parse(partDrawing?.gridOptions || '{}');
  if (!Object.prototype.hasOwnProperty.call(gridSettings, 'status')) gridSettings.status = true;

  const defaultValues = useRef<FormValues>({
    lineWidth: gridRenderer.grid.lineWidth.toString(),
    labelColor: gridRenderer.grid.labelColor,
    lineColor: gridRenderer.grid.lineColor,
    columnLeftLabel: gridRenderer.grid.columnLeftLabel,
    columnRightLabel: gridRenderer.grid.columnRightLabel,
    rowTopLabel: gridRenderer.grid.rowTopLabel,
    rowBottomLabel: gridRenderer.grid.rowBottomLabel,
    skipLabels: gridRenderer.grid.skipLabels,
    showLabels: true,
    max: part?.workflowStage ? part.workflowStage : 3,
  });

  // State to track changes to record between saves
  const [formValues, setFormValues] = useState<FormValues>({
    ...defaultValues.current,
  });

  useEffect(() => {
    if (gridCache && partDrawing && partDrawing.sheets && partDrawing.sheets[curPageIndex] && partDrawing.sheets[curPageIndex].grid) {
      let useGrid: Grid;
      if (!gridCache[curPageIndex]) {
        // Only load from the DB once
        useGrid = Grid.fromJsonString(partDrawing.sheets[curPageIndex].grid);
        gridCache[curPageIndex] = useGrid;
      } else {
        useGrid = gridCache[curPageIndex];
      }
      setFormValues({
        lineWidth: useGrid.lineWidth.toString(),
        labelColor: useGrid.labelColor,
        lineColor: useGrid.lineColor,
        columnLeftLabel: useGrid.columnLeftLabel,
        columnRightLabel: useGrid.columnRightLabel,
        rowTopLabel: useGrid.rowTopLabel,
        rowBottomLabel: useGrid.rowBottomLabel,
        skipLabels: useGrid.skipLabels,
        showLabels: useGrid.showLabels,
        max: defaultValues.current.max,
      });
      const gridOptions = JSON.parse(partDrawing?.gridOptions || '{}');
      if (navigation.currentPartStep === GRID && gridOptions.status !== false && partDrawing?.sheets[curPageIndex]?.useGrid) {
        // Redraw the canvas with the new grid
        gridRenderer.renderToCanvas(useGrid);
      }
    }
  }, [partDrawing, curPageIndex]);

  // Debounce calls to save to the database for half a second
  const saveGridChanges = (gridChanged: boolean, pageIndex: number, field: string, cleanValue: string) => {
    if (gridChanged) {
      dispatch(updateDrawingSheetThunk(pageIndex, { grid: gridRenderer.grid.toJsonString() }));
      Analytics.track({
        event: Analytics.events.partGridUpdated,
        part,
        drawing: partDrawing,
        properties: {
          'view.sheet': pageIndex,
          'form.field': field,
          'form.field.to': cleanValue,
          multipleDocuments: part?.drawings && part.drawings.length > 0,
        },
      });
    }
  };
  const debouncedSaveGridChanges = useCallback(debounce(saveGridChanges, 500, { trailing: true }), []);

  const updateFormState = (field: string, value: any) => {
    let cleanValue: any = value;

    if (field === 'skipLabels') {
      // Value comes in as a string, so convert it back to an array
      cleanValue = value.split(',').map((item: string) => {
        let cleanItem = item.trim().toUpperCase();
        if (!RE_ALPHA_NUMERIC.test(cleanItem)) cleanItem = '';
        return cleanItem;
      });
    } else if (field.includes('Label')) {
      // Uppercase all other labels
      cleanValue = value.trim().toUpperCase();
      // If the value isn't a number or letter, don't apply it
      if (!RE_ALPHA_NUMERIC.test(cleanValue)) cleanValue = '';
    }

    // Create copy of form values to ensure state reference triggers render
    const newFormValues = { ...formValues };
    newFormValues[field] = cleanValue;

    // Prepare to update existing characteristics if grid is changed
    let colorsChanged = false;
    let labelChanged = false;
    let gridChanged = false;

    // Update displayed grid
    switch (field) {
      case 'labelColor':
        if (gridRenderer.grid.labelColor !== newFormValues.labelColor) {
          gridRenderer.grid.setLabelStyle(newFormValues.labelColor);
          colorsChanged = true;
        }
        break;
      case 'lineColor':
      case 'lineWidth':
        if (gridRenderer.grid.lineWidth !== parseInt(newFormValues.lineWidth, 10) || gridRenderer.grid.lineColor !== newFormValues.lineColor) {
          gridRenderer.grid.setLineStyle(parseInt(newFormValues.lineWidth, 10), newFormValues.lineColor);
          colorsChanged = true;
        }
        break;
      // For labels check if there's a number/letter mismatch and correct it if so
      case 'columnLeftLabel':
        // Grid changed, so record that existing characteristics should be updated
        labelChanged = true;
        if (!validateSeriesEdges(cleanValue, newFormValues.columnRightLabel)) {
          newFormValues.columnRightLabel = flipAlphaNumeric(newFormValues.columnRightLabel);
        }
        break;
      case 'columnRightLabel':
        labelChanged = true;
        if (!validateSeriesEdges(newFormValues.columnLeftLabel, cleanValue)) {
          newFormValues.columnLeftLabel = flipAlphaNumeric(newFormValues.columnLeftLabel);
        }
        break;
      case 'rowTopLabel':
        labelChanged = true;
        if (!validateSeriesEdges(cleanValue, newFormValues.rowBottomLabel)) {
          newFormValues.rowBottomLabel = flipAlphaNumeric(newFormValues.rowBottomLabel);
        }
        break;
      case 'rowBottomLabel':
        labelChanged = true;
        if (!validateSeriesEdges(newFormValues.rowTopLabel, cleanValue)) {
          newFormValues.rowTopLabel = flipAlphaNumeric(newFormValues.rowTopLabel);
        }
        break;
      case 'skipLabels':
        labelChanged = true;
        gridChanged = true;
        break;
      default:
        break;
    }
    // Don't update if a label is missing
    if (
      newFormValues.columnLeftLabel !== '' && //
      newFormValues.columnRightLabel !== '' &&
      newFormValues.rowTopLabel !== '' &&
      newFormValues.rowBottomLabel !== '' &&
      labelChanged
    ) {
      gridRenderer.grid.setLabels(
        newFormValues.columnLeftLabel,
        newFormValues.columnRightLabel,
        newFormValues.rowTopLabel,
        newFormValues.rowBottomLabel,
        newFormValues.skipLabels, // Creates array from comma separate string
      );
      gridChanged = true;
    }

    // NOTE: Added this check, because color pickers for line and label were
    // calling updateFormState on every load, even when colors didn't change
    setFormValues(newFormValues);
    gridRenderer.renderToCanvas();
    debouncedSaveGridChanges(gridChanged || colorsChanged, curPageIndex, field, cleanValue);
  };

  // Disable / Enable grid completely
  const updateGridSetting = (isGridOn: boolean, target: 'Drawing' | 'Sheet' | 'Both') => {
    Analytics.track({
      event: isGridOn ? Analytics.events.drawingGridOn : Analytics.events.drawingGridOff,
      part,
      drawing: partDrawing,
      drawingSheet: partDrawing?.sheets[curPageIndex],
      properties: {
        multipleDocuments: part?.drawings && part.drawings.length > 0,
      },
    });
    switch (target) {
      case 'Drawing':
        dispatch(updateDrawingThunk({ gridOptions: JSON.stringify({ ...gridSettings, status: isGridOn }) }));
        break;
      case 'Sheet':
        dispatch(updateDrawingSheetThunk(curPageIndex, { useGrid: isGridOn }));
        break;
      case 'Both':
        dispatch(updateDrawingThunk({ gridOptions: JSON.stringify({ ...gridSettings, status: isGridOn }) }, curPageIndex));
        break;
      default:
        break;
    }
  };

  const handleFocus = (e: ChangeEvent<HTMLInputElement>) => {
    const [field, value] = [e.target.name, e.target.value];
    // When someone clicks into a field, clear its contents completely to prepare for a new value
    if (value === formValues[field]) {
      e.target.value = '';
    }
  };

  const handleChange = (e: ChangeEvent<HTMLInputElement>) => {
    const [field, value] = [e.target.name, e.target.value];
    updateFormState(field, value);
  };

  const handleBlur = (e: ChangeEvent<HTMLInputElement>) => {
    const [field, value] = [e.target.name, e.target.value];
    let useValue: any = value.trim();
    if (useValue === '') {
      // TODO: IDE Typescript enforcement isn't letting me use the string as an object attribute accessor
      // useValue = gridRenderer.grid[e.target.name];
      if (e.target.name === 'columnLeftLabel') {
        useValue = gridRenderer.grid.columnLeftLabel;
      } else if (e.target.name === 'columnRightLabel') {
        useValue = gridRenderer.grid.columnRightLabel;
      } else if (e.target.name === 'rowTopLabel') {
        useValue = gridRenderer.grid.rowTopLabel;
      } else if (e.target.name === 'rowBottomLabel') {
        useValue = gridRenderer.grid.rowBottomLabel;
      }
    }
    e.target.value = useValue;
  };

  const handleLineColorChange = (color: string) => {
    updateFormState('lineColor', color);
  };

  const handleLabelColorChange = (color: string) => {
    updateFormState('labelColor', color);
  };

  // If values are valid, update max stage and submit
  const updateMaxStage = () => {
    if (part) {
      const max = Math.max(part?.workflowStage || 0, navigation.maxPartStep, STYLES);
      dispatch(updatePartThunk({ workflowStage: max }, part.id));
    }
  };

  const handleSubmit = async (e: FormEvent<HTMLFormElement>) => {
    log.debug('handleSubmit', formValues);
    e.preventDefault();
    await dispatch(updateDrawingSheetThunk(curPageIndex, { grid: gridRenderer.grid.toJsonString() }));
    updateMaxStage();
    next();
  };

  const applyingToSheets = useRef(false);

  useEffect(() => {
    if (partEditorLoaded) applyingToSheets.current = false;
  }, [partEditorLoaded]);

  const handleApplyGridSettingsToAllSheets = () => {
    if (partDrawing) {
      Analytics.track({
        event: Analytics.events.partGridAppliedToAllSheets,
        part,
        drawing: partDrawing,
        properties: {
          'view.sheet': curPageIndex,
          multipleDocuments: part?.drawings && part.drawings.length > 0,
        },
      });
      if (partDrawing.sheets.length > 1) applyingToSheets.current = true;
      const newGridCache = gridCache ? [...gridCache] : new Array(partDrawing.sheets.length);
      const changes: { drawingSheetChanges: DrawingSheetChange; pageIndex: number }[] = [];
      partDrawing.sheets.forEach((sheet: any) => {
        if (sheet.pageIndex !== curPageIndex) {
          const gridAsJson = gridRenderer.grid.toJsonString();
          newGridCache[sheet.pageIndex] = Grid.fromJsonString(gridAsJson);
          changes.push({ pageIndex: sheet.pageIndex, drawingSheetChanges: { grid: gridAsJson } });
        }
      });
      dispatch(updateDrawingSheetsThunk(changes));
      dispatch({ type: PartEditorActions.SET_PART_EDITOR_GRID_CACHE, payload: newGridCache });
    }
  };

  // Prepare the skip labels value
  let skipLabelsAsString = '';
  try {
    skipLabelsAsString = formValues.skipLabels.join(',');
  } catch (error) {
    log.error('GridForm.skipLabelsAsString', error);
  }

  return (
    <>
      <section id="partAnalysis" className="side-panel-content">
        <PanelHeader header={`${GRID + 1}. ${i18n('entities.part.title.grid')}`} copy={i18n('entities.part.instructions.grid')} info={i18n('entities.part.instructions.gridHint')} dataCy="part_grid_" optional />

        <GridStatus showAlert={showAlert} setShowAlert={setShowAlert} gridSettings={gridSettings} updateGridSetting={updateGridSetting} document={partDrawing?.number} />

        <GridStatus showAlert={showAlert} setShowAlert={setShowAlert} gridSettings={gridSettings} updateGridSetting={updateGridSetting} document={partDrawing?.number} drawingSheet={partDrawing?.sheets[curPageIndex]} />

        <Form layout="vertical" onFinish={handleSubmit}>
          <div style={{ display: `${gridSettings.status !== false && partDrawing?.sheets[curPageIndex]?.useGrid ? 'block' : 'none'}` }}>
            <Row>
              <Col offset={6} span={5}>
                <Form.Item label="Col Left">
                  <input className="ant-input" name="columnLeftLabel" value={formValues.columnLeftLabel} onChange={handleChange} onFocus={handleFocus} onBlur={handleBlur} disabled={!gridSettings.status} />
                </Form.Item>
              </Col>
              <Col offset={5} span={5}>
                <Form.Item label="Col Right">
                  <input className="ant-input" name="columnRightLabel" value={formValues.columnRightLabel} onChange={handleChange} onFocus={handleFocus} onBlur={handleBlur} disabled={!gridSettings.status} />
                </Form.Item>
              </Col>
            </Row>
            <Row>
              <Col span={5}>
                <Row align="top" style={{ height: '122px' }}>
                  <Form.Item label="Row Top">
                    <input className="ant-input" name="rowTopLabel" value={formValues.rowTopLabel} onChange={handleChange} onFocus={handleFocus} onBlur={handleBlur} disabled={!gridSettings.status} />
                  </Form.Item>
                </Row>
                <Row align="bottom">
                  <Form.Item label="Row Bot">
                    <input className="ant-input" name="rowBottomLabel" value={formValues.rowBottomLabel} onChange={handleChange} onFocus={handleFocus} onBlur={handleBlur} disabled={!gridSettings.status} />
                  </Form.Item>
                </Row>
              </Col>
              <Col offset={1} span={18}>
                <div className="grid-preview-container">
                  <span className="grid-label top-left">{`${formValues.columnLeftLabel}${formValues.rowTopLabel}`}</span>
                  <span className="grid-label top-right">{`${formValues.columnRightLabel}${formValues.rowTopLabel}`}</span>
                  <span className="grid-label bottom-left">{`${formValues.columnLeftLabel}${formValues.rowBottomLabel}`}</span>
                  <span className="grid-label bottom-right">{`${formValues.columnRightLabel}${formValues.rowBottomLabel}`}</span>
                  <img width="200" height="200" src="/images/grid-label-ref.svg" alt="Grid Label Reference" />
                </div>
              </Col>
            </Row>
            <Row style={{ marginTop: '5px' }}>
              <Col>
                <Form.Item label={i18n('wizard.skipLabel')}>
                  <Input name="skipLabels" value={skipLabelsAsString} onChange={handleChange} disabled={!gridSettings.status} />
                </Form.Item>
              </Col>
            </Row>
            <Row>
              <Col span={10}>
                <Form.Item className="form-color-input" name="gridColor" label={i18n('wizard.gridColor')}>
                  {formValues && <ColorPicker value={{ hex: formValues.lineColor }} cValue={{ hex: formValues.lineColor }} field="lineColor" handleChangeColor={handleLineColorChange} disabled={!gridSettings.status} />}
                </Form.Item>
              </Col>
              <Col offset={4} span={10}>
                <Form.Item className="form-color-input" name="labelColor" label={i18n('wizard.labelColor')}>
                  {formValues && <ColorPicker value={{ hex: formValues.labelColor }} cValue={{ hex: formValues.labelColor }} field="labelColor" handleChangeColor={handleLabelColorChange} disabled={!gridSettings.status} />}
                </Form.Item>
              </Col>
            </Row>
            <Row style={{ marginTop: '5px' }}>
              <Tooltip title={partDrawing?.sheets && partDrawing.sheets.length <= 1 ? i18n('wizard.singleSheet') : ''}>
                <Button
                  loading={applyingToSheets.current}
                  className="btn-primary btn-wide"
                  size="large"
                  data-cy="Apply_to_All-btn"
                  onClick={handleApplyGridSettingsToAllSheets}
                  disabled={!gridSettings.status || (partDrawing?.sheets && partDrawing.sheets.length <= 1)}
                >
                  {i18n('wizard.applyAll')}
                </Button>
              </Tooltip>
            </Row>
          </div>
        </Form>
      </section>
      <div className="form-buttons form-section sider-panel-progress-footer">
        <ProgressFooter id="grid-settings-footer" classes="panel-progression-footer" cypress="progress-footer" steps={navigation.maxTotalSteps} current={GRID + 1} onNext={handleSubmit} canProgress canBack onBack={back} />
      </div>
    </>
  );
};
