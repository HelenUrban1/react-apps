import { fabric } from 'fabric';
import { TextOptions } from 'fabric/fabric-impl';
import log from 'modules/shared/logger';
import { Grid } from './Grid';

export class GridCanvasRenderer {
  // grid: Grid = new Grid();
  grid: Grid;

  allowGridEdit: boolean;

  canvas: null | fabric.Canvas = null;

  gridScale: number = 1;

  // Assign a Fabric canvas for the grid to draw to
  constructor(grid?: Grid, allowGridEdit: boolean = true) {
    this.allowGridEdit = allowGridEdit;
    if (grid) this.grid = grid;
    else {
      this.grid = new Grid();
    }
  }

  // Removes all items on the grid
  clearCanvas(): void {
    if (!this.canvas) {
      log.debug(`Unable to clear to canvas, because a canvas has not been set.`);
      return;
    }
    this.canvas!.clear();
  }

  // Draws all of the elements of the grid on the canvas
  renderToCanvas(grid?: Grid): void {
    if (grid) this.grid = grid;
    if (!this.grid) {
      log.debug(`Unable to render to canvas, because a grid has not been set.`);
      return;
    }
    if (this.grid && (this.grid.initialCanvasWidth === 0 || this.grid.initialCanvasHeight === 0)) {
      log.debug(`Unable to render to canvas, because provided grid's initialCanvasWidth and/or initialCanvasHeight are zero.`);
      return;
    }
    if (!this.canvas) {
      log.debug(`Unable to render to canvas, because a canvas has not been set.`);
      return;
    }
    this.canvas!.clear();
    this.drawRows();
    this.drawColumns();
    if (this.grid.showLabels) {
      this.drawCellLabels();
    }
  }

  setCanvas(canvas: fabric.Canvas) {
    this.canvas = canvas;
    this.gridScale = canvas.getWidth() / this.grid.initialCanvasWidth!;
    this.grid.gridScale = this.gridScale;
    // Attach events to grid's event handlers
    if (this.allowGridEdit) {
      canvas!.on('object:moving', this.moveLine.bind(this));
    }
    this.renderToCanvas();
    return canvas;
  }

  // Scales the grid values to the provided currentWidth
  setGridScale(currentWidth: number): void {
    if (this.grid.initialCanvasWidth! <= 0) {
      log.error(`Can't scale grid to width: ${currentWidth}`);
    }
    this.gridScale = currentWidth / this.grid.initialCanvasWidth!;
  }

  // Helper function to standardize Row IDs
  rowId(index: number): string {
    return `gridRowLine-${index}`;
  }

  // Helper function to standardize Col IDs
  colId(index: number): string {
    return `gridColLine-${index}`;
  }

  // Helper function to standardize cell label IDs
  cellLabelId(label: string): string {
    return `gridCellLabel-${label}`;
  }

  // Returns fabric object with the given ID
  getFabricObjectById(id: string): any {
    return this.canvas!.getObjects().find((obj: any) => obj.id === id);
  }

  // Return row line object with the index given
  getRow(index: number): fabric.Line {
    return this.getFabricObjectById(this.rowId(index));
  }

  // Returns column line object with the index given
  getCol(index: number): fabric.Line {
    return this.getFabricObjectById(this.colId(index));
  }

  // Return cell label object at the give row and column coordinates
  getCellLabel(rowIndex: number, colIndex: number): fabric.Text {
    return this.getFabricObjectById(this.cellLabelId(this.grid.labelForCell(rowIndex, colIndex)));
  }

  // Helper function to limit offsets due to floating point math
  scaleToCanvas(value: number): number {
    return Math.round(value * this.gridScale);
  }

  // Returns bool that indicates if the grid has been rendered to the current canvas
  isRendered() {
    return this.canvas && this.getFabricObjectById(this.rowId(0)) !== null;
  }

  // Adds all of the row lines
  drawRows(): void {
    for (let i = 0; i < this.grid.rowLines.length; i++) {
      const startX = this.scaleToCanvas(this.grid.xGridLeftEdge());
      let startY = this.scaleToCanvas(this.grid.yRow(i));
      let endX = this.scaleToCanvas(this.grid.xGridRightEdge()) + this.grid.lineWidth;
      let endY = this.scaleToCanvas(this.grid.yRow(i));
      if (endY > this.canvas!.getHeight()) {
        endY = this.canvas!.getHeight() - this.grid.lineWidth;
        startY = this.canvas!.getHeight() - this.grid.lineWidth;
      }
      if (endX > this.canvas!.getWidth()) {
        endX = this.canvas!.getWidth() - this.grid.lineWidth;
      }
      this.drawLine(
        this.rowId(i), //
        startX,
        startY,
        endX,
        endY,
        {
          index: i,
          type: 'row',
        },
        {
          ...this.grid.defaultLineStyle,
          hoverCursor: 'row-resize',
          moveCursor: 'row-resize',
        },
      );
    }
  }

  // Adds all of the col lines
  drawColumns(): void {
    for (let i = 0; i < this.grid.colLines.length; i++) {
      const startX = this.scaleToCanvas(this.grid.xCol(i));
      const startY = this.scaleToCanvas(this.grid.yGridTopEdge());
      let endX = this.scaleToCanvas(this.grid.xCol(i));
      let endY = this.scaleToCanvas(this.grid.yGridBottomEdge()) + this.grid.lineWidth;
      if (endY > this.canvas!.getHeight()) {
        endY = this.canvas!.getHeight() - this.grid.lineWidth;
      }
      if (endX > this.canvas!.getWidth()) {
        endX = this.canvas!.getWidth() - this.grid.lineWidth;
      }
      this.drawLine(
        this.colId(i), //
        startX,
        startY,
        endX,
        endY,
        {
          index: i,
          type: 'col',
        },
        {
          ...this.grid.defaultLineStyle,
          hoverCursor: 'col-resize',
          moveCursor: 'col-resize',
        },
      );
    }
  }

  drawLine(id: string, startX: number, startY: number, endX: number, endY: number, metadata: object = {}, style: object = this.grid.defaultLineStyle): void {
    const options = {
      id,
      selectable: this.allowGridEdit,
      hasControls: false,
      hasRotatingPoint: false,
      lockRotation: true,
      lockMovementX: startY === endY,
      lockMovementY: startX === endX,
      ...metadata,
      ...style,
    } as TextOptions;
    const line = new fabric.Line([startX, startY, endX, endY], options);
    if (!this.allowGridEdit) {
      line.hoverCursor = 'default';
    }
    this.canvas!.add(line);
  }

  // Restyle the already rendered labels
  updateLineStyle(style: object): void {
    if (!this.isRendered()) return;
    let line: fabric.Line;
    let i: number;
    for (i = 0; i < this.grid.rowLines.length; i++) {
      line = this.getRow(i);
      line.set(style);
    }
    for (i = 0; i < this.grid.colLines.length; i++) {
      line = this.getCol(i);
      line.set(style);
    }
    this.canvas!.requestRenderAll();
  }

  // Draw Location Labels
  drawCellLabels(): void {
    const numRows = this.grid.rowLines.length - 1;
    const numCols = this.grid.colLines.length - 1;

    const yRowMidpoints: number[] = [];
    const xColMidpoints: number[] = [];

    // Precompute the row and column midpoints
    for (let rowIndex = 0; rowIndex < numRows; rowIndex++) {
      yRowMidpoints.push(this.grid.yRow(rowIndex) + (this.grid.yRow(rowIndex + 1) - this.grid.yRow(rowIndex)) / 2);
    }

    for (let columnIndex = 0; columnIndex < numCols; columnIndex++) {
      xColMidpoints.push(this.grid.xCol(columnIndex) + (this.grid.xCol(columnIndex + 1) - this.grid.xCol(columnIndex)) / 2);
    }

    for (let rowIndex = 0; rowIndex < numRows; rowIndex++) {
      for (let columnIndex = 0; columnIndex < numCols; columnIndex++) {
        if (rowIndex === numRows - 1 || columnIndex === 0) {
          this.drawCellLabel(xColMidpoints[columnIndex], yRowMidpoints[rowIndex], this.grid.labelForCell(rowIndex, columnIndex));
        }
      }
    }
  }

  // Place the grid label on the canvas
  drawCellLabel(xPos: number, yPos: number, label: string, style: object = this.grid.defaultLabelStyle): void {
    if (!this.canvas) {
      log.warn('NO CANVAS');
      return;
    }
    const id = this.cellLabelId(label);
    const opts = {
      ...style,
      id,
      textAlign: 'center',
      hoverCursor: 'default',
      selectable: false,
      hasControls: false,
      hasRotatingPoint: false,
      lockRotation: true,
      lockMovementX: true,
      lockMovementY: true,
    } as fabric.IObjectOptions;

    const text: fabric.Text = new fabric.Text('');
    text.text = label;
    text.set({
      left: this.scaleToCanvas(xPos) + text.width! / 2,
      top: this.scaleToCanvas(yPos) + text.height! / 2,
    });
    text.setCoords();

    try {
      // For some reason this fails half the time on firefox
      text.setOptions(opts);
    } catch (err) {
      log.error('GridCanvasRenderer.drawCellLabel', 'Encountered an error setting the options of a fabric text object.', err);
      log.debug(!!text.setOptions, text.setOptions, opts);
    }
    this.canvas.add(text);
    // text.set above doesn't place labels in the center of cells reliably, so calling moveLabels here unifies placement
    this.moveLabels();
  }

  // Restyle the labels
  updateCellLabelStyle(style: object): void {
    if (!this.isRendered()) return;
    let cellLabel: fabric.Text;
    let ri: number;
    let ci: number;
    for (ri = 0; ri < this.grid.rowLines.length - 1; ri++) {
      for (ci = 0; ci < this.grid.colLines.length - 1; ci++) {
        cellLabel = this.getCellLabel(ri, ci);
        cellLabel.set(style);
      }
    }
    this.canvas!.requestRenderAll();
  }

  // -------- Grid Modification ---------------------------------------------------

  // Rotate the grid and redraw it on the canvas
  rotate(delta: number) {
    this.grid.rotate(delta);
    this.renderToCanvas();
    return this.grid;
  }

  // handleGridMoving
  moveLine(e: any): void {
    const line: any = e.target;
    if (line.type === 'row') {
      this.moveRow(line);
    } else if (line.type === 'col') {
      this.moveCol(line);
    }
    // Reposition the labels after any move
    this.moveLabels();
  }

  // Move a row
  // moveRow(line:fabric.Line):void {
  moveRow(line: any): void {
    // Keep the row from drifting off the page or below its neighbor
    let yMin: number;
    let yMax: number;
    if (line.index === 0) {
      // Top edge
      yMin = 0;
      yMax = this.scaleToCanvas(this.grid.yRow(-1)) - (this.grid.rowLines.length - 1) * this.grid.lineWidth;
    } else if (line.index === this.grid.rowLines.length - 1) {
      // Bottom edge
      yMin = this.scaleToCanvas(this.grid.yRow(0)) + (this.grid.rowLines.length - 1) * this.grid.lineWidth;
      yMax = this.canvas!.getHeight() - this.grid.lineWidth;
    } else {
      // Prevent middle row from drifting past its neighbors
      yMin = this.scaleToCanvas(this.grid.yRow(line.index - 1)) + this.grid.lineWidth;
      yMax = this.scaleToCanvas(this.grid.yRow(line.index + 1)) - this.grid.lineWidth;
    }
    this.limitYTravel(line, yMin, yMax);

    // Save the new position
    this.grid.rowLines[line.index] = Math.round(line.top / this.gridScale);

    // If the line being moved is an edge, adjust middle rows by their ratios
    if (line.index === 0 || line.index === this.grid.rowLines.length - 1) {
      let i: number;
      // Reposition rows according to their grid ratio
      for (i = 1; i < this.grid.rowLines.length - 1; i++) {
        const yNew = this.grid.yFromRowGridRatio(this.grid.rowLineRatios[i]);
        const rowi = this.getRow(i);
        rowi.set({ top: this.scaleToCanvas(yNew) });
        rowi.setCoords();
        this.grid.rowLines[i] = yNew;
      }

      // Shorten column heights to match top and bottom edges
      for (i = 0; i < this.grid.colLines.length; i++) {
        const coli = this.getCol(i);
        coli.set({
          x1: this.scaleToCanvas(this.grid.xCol(i)),
          y1: this.scaleToCanvas(this.grid.yGridTopEdge()),
          x2: this.scaleToCanvas(this.grid.xCol(i)),
          y2: this.scaleToCanvas(this.grid.yGridBottomEdge()) + this.grid.lineWidth,
        });
        coli.setCoords();
      }
    } else {
      // A middle line was moved, so update its ratio
      this.grid.rowLineRatios[line.index] = this.grid.rowGridRatio(line.index);
    }
  }

  // Move a row
  // moveCol(line:fabric.Line):void {
  moveCol(line: any): void {
    // Keep the row from drifting off the page or below its neighbor
    let xMin: number;
    let xMax: number;
    if (line.index === 0) {
      // Left edge
      xMin = 0;
      xMax = this.scaleToCanvas(this.grid.xCol(-1)) - (this.grid.colLines.length - 1) * this.grid.lineWidth;
    } else if (line.index === this.grid.colLines.length - 1) {
      // Right edge
      xMin = this.scaleToCanvas(this.grid.xCol(0)) + (this.grid.colLines.length - 1) * this.grid.lineWidth;
      xMax = this.canvas!.getWidth() - this.grid.lineWidth;
    } else {
      // Prevent middle column from drifting past its neighbors
      xMin = this.scaleToCanvas(this.grid.xCol(line.index - 1)) + this.grid.lineWidth;
      xMax = this.scaleToCanvas(this.grid.xCol(line.index + 1)) - this.grid.lineWidth;
    }
    this.limitXTravel(line, xMin, xMax);

    // Save the new position
    this.grid.colLines[line.index] = Math.round(line.left / this.gridScale);

    // If the line being moved is an edge, adjust middle columns by their ratios
    if (line.index === 0 || line.index === this.grid.colLines.length - 1) {
      let i: number;
      // Reposition columns according to their grid ratio
      for (i = 1; i < this.grid.colLines.length - 1; i++) {
        const xNew = this.grid.xFromColGridRatio(this.grid.colLineRatios[i]);
        const coli = this.getCol(i);
        coli.set({ left: this.scaleToCanvas(xNew) });
        coli.setCoords();
        this.grid.colLines[i] = xNew;
      }

      // Shorten row widths to match left and right edges
      for (i = 0; i < this.grid.rowLines.length; i++) {
        const rowi = this.getRow(i);
        rowi.set({
          x1: this.scaleToCanvas(this.grid.xGridLeftEdge()),
          y1: this.scaleToCanvas(this.grid.yRow(i)),
          x2: this.scaleToCanvas(this.grid.xGridRightEdge()) + this.grid.lineWidth,
          y2: this.scaleToCanvas(this.grid.yRow(i)),
        });
        rowi.setCoords();
      }
    } else {
      // A middle line was moved, so update its ratio
      this.grid.colLineRatios[line.index] = this.grid.colGridRatio(line.index);
    }
  }

  // Prevent the line X position from being placed below min or above max
  limitXTravel(line: any, xMin: number, xMax: number): void {
    if (line.left > xMax) {
      line.left = xMax;
    } else if (line.left < xMin) {
      line.left = xMin;
    }
  }

  // Prevent the line Y position from being placed below min or above max
  limitYTravel(line: any, yMin: number, yMax: number): void {
    if (line.top > yMax) {
      line.top = yMax;
    } else if (line.top < yMin) {
      line.top = yMin;
    }
  }

  // Reposition Location Labels
  moveLabels(): void {
    let cellLabel: fabric.Text;
    let ri: number;
    let ci: number;
    let yPos: number;
    let xPos: number;
    for (ri = 0; ri < this.grid.rowLines.length - 1; ri++) {
      // Place in center of row
      yPos = this.grid.yRow(ri) + (this.grid.yRow(ri + 1) - this.grid.yRow(ri)) / 2;
      for (ci = 0; ci < this.grid.colLines.length - 1; ci++) {
        // Place in center of column
        xPos = this.grid.xCol(ci) + (this.grid.xCol(ci + 1) - this.grid.xCol(ci)) / 2;
        cellLabel = this.getCellLabel(ri, ci);
        if (!cellLabel) {
          break;
        } else {
          cellLabel.set({
            left: this.scaleToCanvas(xPos) - cellLabel.width! / 2,
            top: this.scaleToCanvas(yPos) - cellLabel.height! / 2,
          });
          cellLabel.setCoords();
        }
      }
    }
  }
}
