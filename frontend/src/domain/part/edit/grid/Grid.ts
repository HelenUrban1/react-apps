/**
 labels
export interface Grid {
  canvasWidth: number,
  canvasHeight: number,
  labels: {
    columnLeft: string,
    columnRight: string,
    rowTop: string,
    rowBottom: string,
    skip: string[]
    style?: { [styleProp:string]:string|number},
    columns?: string[],
    rows?: string[],
  },
  lines?: {
    style?: { [styleProp:string]:string|number},
    rows: number[],
    columns: number[]
  }
}


export interface Grid {
  sheetWidth: number,
  sheetHeight: number,
  lineColor: string,
  lineWidth: number,
  labelColor: string
  showLabels: boolean,
  columnLines: number[],
  columnLeftLabel: string,
  columnRightLabel: string,
  rowLines: number[],
  rowTopLabel: string,
  rowBottomLabel: string,
  skipLabels: string[],
}
*/

import log from '../../../../modules/shared/logger';

// Helper function that returns the index where the provided number should fit into the provided array
export function getIndexToIns(arr: number[], num: number): number {
  if (arr.length === 0) return -1;
  if (num < arr[0]) return -2;
  if (num > arr[arr.length - 1]) return -3;
  if (num === arr[0]) return 0;
  const index = arr.sort((a, b) => a - b).findIndex((currentNum) => num <= currentNum);
  // return index === -1 ? arr.length : index
  return index - 1;
}

// Helper function to invert values of an array relative to a max
// [0,1,2,4] with max 4 [0,2,3,4]
export function invert(arr: number[], max: number): number[] {
  const r: number[] = [];
  const len = arr.length - 1;
  arr.forEach((val, i) => {
    r.push(max - arr[len - i]);
  });
  return r;
}

// Helper function to limit offsets due to floating point math
export function toPreciseFloat(num: number, significantDigits: number = 5): number {
  return Number.parseFloat(num.toFixed(significantDigits));
}

// Validate that the starting and ending values are either both numbers or both non-numbers
export function validateSeriesEdges(startingValue: any, endingValue: any): boolean {
  return (!isNaN(startingValue) && !isNaN(endingValue)) || (isNaN(startingValue) && isNaN(endingValue));
}

// Constructs a series of labels (numeric or letter based) of given length that does not contain labels in the skip array
export function buildSeries(startingValue: any, endingValue: any, skip: string[] = [], maxItems: number = 100): string[] {
  if (!validateSeriesEdges(startingValue, endingValue)) {
    // TODO: Handle error for UI
    log.warn(`Starting and ending values of a series must be both numbers or both letters but were (${startingValue}, ${endingValue})`);
  }
  let series: string[] = [];
  if (isNaN(startingValue)) {
    series = buildLetterSeries(startingValue, endingValue, skip, maxItems);
  } else {
    startingValue = parseInt(startingValue, 10);
    endingValue = parseInt(endingValue, 10);
    series = buildNumberSeries(startingValue, endingValue, skip, maxItems);
  }
  return series;
}

// Constructs a series of numberic labels given length that does not contain labels in the skip array
export function buildNumberSeries(startingValue: number, endingValue: number, skip: string[] = [], maxItems: number = 100): string[] {
  // If the values are decending, swap starting and ending values
  const isDescending: boolean = startingValue > endingValue;
  const increment: number = isDescending ? -1 : 1;
  let label: string;
  const series: string[] = [];
  let i: number = startingValue;
  do {
    label = `${i}`;
    if (!skip.includes(`${i}`)) {
      series.push(label);
    }
    i += increment;
    if (i === maxItems) {
      log.warn(`Letter series has reached max ${maxItems}`);
    }
  } while (series.length < maxItems && i !== endingValue + increment);
  return series;
}

// Returns a string that represents the Letter cordinates of the provided zero based index (0='A', 51='AZ', 52='BA')
export function indexToLetters(index: number): string {
  let result: string = '';
  const a: number = Math.floor(index / 26);
  if (a >= 0) {
    result = indexToLetters(a - 1) + String.fromCharCode(65 + (index % 26));
  }
  return result;
}

// Returns the zero based index address of the provided Letter cordinate ('A'=0, 'AZ'=51, 'BA'=52)
export function lettersToIndex(letters: string): number {
  let result: number = 0;
  const { length } = letters;
  let letter: string;
  let index: number;
  for (let i = 0; i < length; i++) {
    letter = letters[length - 1 - i];
    index = letter.charCodeAt(0) - 64;
    result += index * 26 ** i;
  }
  result -= 1;
  return result;
}

// Constructs a series of excel style letter labels given length that does not contain labels in the skip array
export function buildLetterSeries(startingValue: string, endingValue: string, skip: string[] = [], maxItems: number = 100): string[] {
  const startingIndex: number = lettersToIndex(startingValue);
  const endingIndex: number = lettersToIndex(endingValue);
  const isDescending: boolean = startingIndex > endingIndex;
  const increment: number = isDescending ? -1 : 1;
  let label: string;
  const series: string[] = [];
  let i: number = startingIndex;
  do {
    label = indexToLetters(i);
    if (!skip.includes(label)) {
      series.push(label);
    }
    i += increment;
    if (i === maxItems) {
      log.warn(`Letter series has reached max ${maxItems}`);
    }
  } while (series.length < maxItems && i !== endingIndex + increment);
  return series;
}

// -------- Grid Rendering ---------------------------------------------------

export interface GridProps {
  initialCanvasWidth?: number;
  initialCanvasHeight?: number;
  initialPageRotation?: number;
  pageRotation?: number;
  lineWidth?: number;
  lineColor?: string;
  labelColor?: string;
  columnLeftLabel?: string;
  columnRightLabel?: string;
  rowTopLabel?: string;
  rowBottomLabel?: string;
  skipLabels?: string[];
  showLabels?: boolean;

  rowLineRatios?: number[];
  rowLines?: number[];
  rowLabels?: string[];
  colLineRatios?: number[];
  colLines?: number[];
  colLabels?: string[];
}

export class Grid {
  initialCanvasWidth: number;

  initialCanvasHeight: number;

  initialPageRotation: number;

  pageRotation: number;

  lineWidth: number = 5;

  labelColor: string = '#963CBD';

  lineColor: string = '#963CBD';

  columnLeftLabel: string = '4';

  columnRightLabel: string = '1';

  rowTopLabel: string = 'B';

  rowBottomLabel: string = 'A';

  skipLabels: string[] = [];

  showLabels: boolean = true;

  defaultLineStyle: { [styleProp: string]: any } = {};

  defaultLabelStyle: { [styleProp: string]: any } = {};

  gridScale: number = 1;

  rowLineRatios: number[] = [];

  rowLines: number[] = [];

  rowLabels: string[] = [];

  colLineRatios: number[] = [];

  colLines: number[] = [];

  colLabels: string[] = [];

  // Given an array
  /*
  constructor(params:GridProps = {
    initialCanvasWidth: 1280,
    initialCanvasHeight: 720,
    lineWidth: 5,
    lineColor: '#963CBD',
    labelColor: '#963CBD',
    columnLeftLabel: '4',
    columnRightLabel: '1',
    rowTopLabel: 'B',
    rowBottomLabel: 'A',
    skipLabels: [],
    showLabels:true
  }) {
    */
  constructor(myParams: GridProps = {}) {
    // Save new state
    const params = { ...myParams };

    this.initialCanvasWidth = params.initialCanvasWidth || 0;
    this.initialCanvasHeight = params.initialCanvasHeight || 0;
    this.initialPageRotation = params.initialPageRotation || 0;
    this.pageRotation = params.initialPageRotation || 0;
    if (params.hasOwnProperty('showLabels')) {
      this.showLabels = params.showLabels!;
    }

    // Setup styles
    this.setLineStyle(params.lineWidth || this.lineWidth, params.lineColor || this.lineColor);
    this.setLabelStyle(params.labelColor || this.labelColor);

    // Set and build the label series
    this.setLabels(params.columnLeftLabel || this.columnLeftLabel, params.columnRightLabel || this.columnRightLabel, params.rowTopLabel || this.rowTopLabel, params.rowBottomLabel || this.rowBottomLabel, params.skipLabels || this.skipLabels);

    // If rows and lines were provided, skip inititialization
    if (params.rowLines !== undefined && params.colLines !== undefined) {
      this.rowLineRatios = params.rowLineRatios!;
      this.rowLines = params.rowLines!;
      this.rowLabels = params.rowLabels!;
      this.colLineRatios = params.colLineRatios!;
      this.colLines = params.colLines!;
      this.colLabels = params.colLabels!;
    } else {
      this.initializeLineCollections();
    }

    // ROTATE HERE
    // return to 0 rotation since the pdf loads in at 0
    if (params.pageRotation === 90) {
      this.rotate(-90);
    } else if (params.pageRotation === 180) {
      this.rotate(-90);
      this.rotate(-90);
    } else if (params.pageRotation === 270) {
      this.rotate(90);
    }
    this.pageRotation = 0;
  }

  // Class method for generating a Grid object from a stringified JSON object (that can be stored in DB)
  static fromJsonString(input: string): Grid {
    const params = input && input !== 'none' ? JSON.parse(input) : {};
    return new Grid(params);
  }

  toJsonString(): string {
    return JSON.stringify(this.toJson());
  }

  toJson(): GridProps {
    return {
      // Initial params
      initialCanvasWidth: this.initialCanvasWidth,
      initialCanvasHeight: this.initialCanvasHeight,
      pageRotation: this.pageRotation,
      lineWidth: this.lineWidth,
      labelColor: this.labelColor,
      lineColor: this.lineColor,
      columnLeftLabel: this.columnLeftLabel,
      columnRightLabel: this.columnRightLabel,
      rowTopLabel: this.rowTopLabel,
      rowBottomLabel: this.rowBottomLabel,
      skipLabels: this.skipLabels,
      showLabels: this.showLabels,
      // Internal objects
      rowLineRatios: this.rowLineRatios,
      rowLines: this.rowLines,
      rowLabels: this.rowLabels,
      colLineRatios: this.colLineRatios,
      colLines: this.colLines,
      colLabels: this.colLabels,
    };
  }

  initializeLineCollections(): void {
    // Prepare to layout the correct number of columns and rows

    let xFirst: number;
    let xLast: number;
    let yFirst: number;
    let yLast: number;
    // If this is the first render, place grid edges ~2.5% inside the cavas
    const margin = 0.025;
    let i;

    if (this.colLines.length === 0) {
      // First render
      xFirst = Math.round(this.initialCanvasWidth * margin);
      xLast = Math.round(this.initialCanvasWidth - xFirst - this.lineWidth);
    } else {
      // Updating grid
      xFirst = this.colLines[0];
      xLast = this.colLines[this.colLines.length - 1] - this.lineWidth;
    }

    if (this.rowLines.length === 0) {
      // First render
      yFirst = Math.round(this.initialCanvasHeight * margin);
      yLast = Math.round(this.initialCanvasHeight - yFirst - this.lineWidth);
    } else {
      // Updating grid
      yFirst = this.rowLines[0];
      yLast = this.rowLines[this.rowLines.length - 1] - this.lineWidth;
    }

    // Update columns if the numbers have changed
    const colCount = this.colLabels.length;
    if (colCount !== this.colLines.length - 1) {
      const colWidth = Math.round((xLast - xFirst) / colCount);
      this.colLines = [xFirst];
      // Place remaining lines relative to the starting point
      for (i = 1; i <= colCount; i++) {
        this.colLines.push(xFirst + i * colWidth);
      }
    }

    // Update rows if the numbers have changed
    const rowCount = this.rowLabels.length;
    if (rowCount !== this.rowLines.length - 1) {
      const rowHeight = Math.round((yLast - yFirst) / rowCount);
      this.rowLines = [yFirst];
      // Place remaining lines relative to the starting point
      for (i = 1; i <= rowCount; i++) {
        this.rowLines.push(yFirst + i * rowHeight);
      }
    }
    this.saveGridRatios();
  }

  // Setup style defaults for lines
  setLineStyle(lineWidth: number, lineColor: string) {
    this.lineWidth = lineWidth;
    this.lineColor = lineColor;
    this.defaultLineStyle = {
      padding: 6,
      stroke: lineColor,
      strokeWidth: lineWidth,
    };
    // this.updateLineStyle(this.defaultLineStyle);
  }

  // Setup style defaulst for labels
  setLabelStyle(labelColor: string, fontSize: number = 22, fontFamily: string = 'Arial, Helvetica, sans-serif') {
    this.labelColor = labelColor;
    this.defaultLabelStyle = {
      fill: labelColor,
      fontSize,
      fontFamily,
      textBackgroundColor: '#fff',
      // strokeWidth: 3,
      // stroke: '#fff',
      // paintFirst: 'stroke'
    };
    // this.updateCellLabelStyle(this.defaultLabelStyle);
  }

  // Validate and build the label series for both columns and rows
  setLabels(columnLeftLabel: string | number, columnRightLabel: string | number, rowTopLabel: string | number, rowBottomLabel: string | number, skipLabels: string[]) {
    if (
      // Rebuild the series...
      // If the rowLabels or colLabels haven't been calculated
      this.rowLabels.length === 0 ||
      this.colLabels.length === 0 ||
      // Or if any of the label inputs have changed
      this.skipLabels !== skipLabels ||
      this.rowTopLabel !== rowTopLabel ||
      this.rowBottomLabel !== rowBottomLabel ||
      this.columnLeftLabel !== columnLeftLabel ||
      this.columnRightLabel !== columnRightLabel ||
      // Or if the label series don't match the starting and end labels
      this.rowLabels[0] !== rowTopLabel ||
      this.rowLabels[this.rowLabels.length - 1] !== rowBottomLabel ||
      this.colLabels[0] !== columnLeftLabel ||
      this.colLabels[this.colLabels.length - 1] !== columnRightLabel
    ) {
      this.rowLabels = buildSeries(rowTopLabel, rowBottomLabel, skipLabels);
      this.colLabels = buildSeries(columnLeftLabel, columnRightLabel, skipLabels);
      this.initializeLineCollections();
    }
    // Always set the new values
    this.skipLabels = skipLabels;
    this.rowTopLabel = `${rowTopLabel}`;
    this.rowBottomLabel = `${rowBottomLabel}`;
    this.columnLeftLabel = `${columnLeftLabel}`;
    this.columnRightLabel = `${columnRightLabel}`;
  }

  // Save the grid line positions relative to the grid's width and height
  saveGridRatios(): void {
    let i: number;
    this.rowLineRatios = [];
    for (i = 0; i < this.rowLines.length; i++) {
      this.rowLineRatios[i] = this.rowGridRatio(i);
    }
    this.colLineRatios = [];
    for (i = 0; i < this.colLines.length; i++) {
      this.colLineRatios[i] = this.colGridRatio(i);
    }
  }

  // Return the scaled X position of the column with the given index (index = -1 returns last column)
  xCol(index: number): number {
    const i: number = index === -1 ? this.colLines.length - 1 : index;
    return this.colLines[i];
  }

  // Return the scaled Y position of the row with the given index (index = -1 returns last row)
  yRow(index: number): number {
    const i: number = index === -1 ? this.rowLines.length - 1 : index;
    return this.rowLines[i];
  }

  // X value of left edge of the Grid
  xGridLeftEdge(): number {
    return this.xCol(0);
  }

  // X value of right edge of the Grid
  xGridRightEdge(): number {
    return this.xCol(-1);
  }

  // Y value of top edge of the Grid
  yGridTopEdge(): number {
    return this.yRow(0);
  }

  // Y value of bottom edge of the Grid
  yGridBottomEdge(): number {
    return this.yRow(-1);
  }

  // Grid Height = lastCol.y - firstCol.y
  height(): number {
    return this.yGridBottomEdge() - this.yGridTopEdge();
  }

  // Grid Width = lastRow.x - firstRow.x
  width(): number {
    return this.xGridRightEdge() - this.xGridLeftEdge();
  }

  // Returns the ratio that the col's position is relative to the grid width
  colGridRatio(colIndex: number): number {
    return toPreciseFloat((this.xCol(colIndex) - this.xGridLeftEdge()) / this.width());
  }

  // Returns the ratio that given X value is relative to grid width
  xFromColGridRatio(colGridRatio: number): number {
    return Math.round(this.xGridLeftEdge() + colGridRatio * this.width());
  }

  // Returns the ratio that the row's position is relative to the grid height
  rowGridRatio(rowIndex: number): number {
    return toPreciseFloat((this.yRow(rowIndex) - this.yGridTopEdge()) / this.height());
  }

  // Returns the ratio that given X value is relative to grid width
  yFromRowGridRatio(rowGridRatio: number): number {
    return Math.round(this.yGridTopEdge() + rowGridRatio * this.height());
  }

  // Returns the label for the cell address at the given row and column indexes
  labelForCell(rowIndex: number, colIndex: number): string {
    if (this.pageRotation === 90 || this.pageRotation === 270) {
      // col and row are switched when rotated sideways
      return `${this.rowLabels[rowIndex]}${this.colLabels[colIndex]}`;
    }
    return `${this.colLabels[colIndex]}${this.rowLabels[rowIndex]}`;
  }

  // Return the cell address of the provided coordinates
  cellLabelForPoint(xPos: number, yPos: number, scale = 1): string {
    let result: string;
    // adjust coord position to canvas scale, and then to the grid scale in order to properly conform the two dimension sets
    const ic: number = getIndexToIns(this.colLines, (xPos * scale) / this.gridScale);
    const ir: number = getIndexToIns(this.rowLines, (yPos * scale) / this.gridScale);
    if (ic >= 0 && ir >= 0) {
      result = this.labelForCell(ir, ic);
    } else {
      result = 'External';
    }
    return result;
  }

  // Rotate the grid by the amount specified
  rotate(delta: number) {
    if (delta === 90) {
      // Row values swap with col values
      // Col values swap with rows and invert relative to max
      // Swap lines
      [this.rowLines, this.colLines] = [this.colLines, invert(this.rowLines, this.initialCanvasHeight)];
      // Swap ratios
      [this.rowLineRatios, this.colLineRatios] = [this.colLineRatios, invert(this.rowLineRatios, 1)];
      // Swap labels
      [this.colLabels, this.rowLabels] = [this.rowLabels.reverse(), this.colLabels];
    } else if (delta === -90) {
      // Row values swap with col and invert relative to max
      // Col values swap with col values
      // Swap lines
      [this.rowLines, this.colLines] = [invert(this.colLines, this.initialCanvasWidth), this.rowLines];
      // Swap ratios
      [this.rowLineRatios, this.colLineRatios] = [invert(this.colLineRatios, 1), this.rowLineRatios];
      // Swap labels
      [this.colLabels, this.rowLabels] = [this.rowLabels, this.colLabels.reverse()];
      /*
    // BUG: If you click the rotate buttons REALLY fast, you can get a rotation of 180 here
    } else if (delta === 180 ) {
      // Row values invert relative to max
      this.rowLines = invert(this.rowLines, layerManager.view.height);
      this.rowLineRatios = invert(this.rowLines, 1);
      rowLabels.reverse();
      // Col values invert relative to max
      this.colLines = invert(this.rowLines, layerManager.view.width);
      this.colLineRatios = invert(this.colLineRatios, 1);
      colLabels.reverse();
    */
    } else {
      log.error(`Unable to handle rotation of ${delta}`);
    }

    // Swap height and width
    if (delta === 90 || delta === -90) {
      [this.initialCanvasWidth, this.initialCanvasHeight] = [this.initialCanvasHeight, this.initialCanvasWidth];
    }

    // Update the grid label markers
    this.columnLeftLabel = this.colLabels[0];
    this.columnRightLabel = this.colLabels[this.colLabels.length - 1];
    this.rowTopLabel = this.rowLabels[0];
    this.rowBottomLabel = this.rowLabels[this.rowLabels.length - 1];

    // save rotation so the grid can be returned to a righted state on load
    this.pageRotation += delta;
  }
}
