import React, { useState, useEffect } from 'react';
import { ReadOutlined, SnippetsOutlined } from '@ant-design/icons';
import { Divider, Popconfirm, Select, Tooltip } from 'antd';
import { i18n } from 'i18n';
import { AppState } from 'modules/state';
import { useDispatch, useSelector } from 'react-redux';
import moment from 'moment';
import { useHistory } from 'react-router';
import { doAddPartRevisions } from 'modules/part/partActions';
import { PartStepEnum } from 'modules/navigation/navigationActions';

interface Props {
  toggleDrawer: () => void;
}

export const PartSelect: React.FC<Props> = ({ toggleDrawer }) => {
  const { part, revisions, revisionIndex, revisionType } = useSelector((state: AppState) => state.part);
  const { characteristics } = useSelector((state: AppState) => state.characteristics);
  const { currentPartStep } = useSelector((state: AppState) => state.navigation);

  const [confirm, setConfirm] = useState(false);
  const [value, setValue] = useState(part?.id || '');
  const [options, setOptions] = useState<{}[]>([]);

  const history = useHistory();
  const dispatch = useDispatch();

  useEffect(() => {
    if (part) {
      const revisionOptions = Object.keys(revisions)
        .sort((a, b) => (moment(revisions[a].createdAt).isAfter(moment(revisions[b].createdAt)) ? -1 : 1))
        .map((rev, index) => {
          return (
            <Select.Option
              key={revisions[rev].id}
              value={revisions[rev].id}
              labelrender={revisions[rev].revision || moment(revisions[rev].createdAt).format('YYYY-MM-DD')}
              label={
                <span>
                  <Tooltip placement="left" title={moment(revisions[rev].createdAt).format('YYYY-MM-DD hh:mm:ss A')}>{`${revisions[rev].revision || moment(revisions[rev].createdAt).format('YYYY-MM-DD')}${index === 0 ? ' (current)' : ''}`}</Tooltip>
                </span>
              }
            >
              {`${revisions[rev].revision || moment(revisions[rev].createdAt).format('YYYY-MM-DD')}${index === 0 ? ' (current)' : ''}`}
            </Select.Option>
          );
        });
      setOptions(revisionOptions);
      setValue(part.id);
    }
  }, [part?.id, part?.revision, revisions]);

  if (!part) return <></>;

  const addRevision = () => {
    if (revisionIndex && revisionType === 'part') {
      setConfirm(true);
    } else {
      dispatch(doAddPartRevisions({ ...part, characteristics }, history));
    }
  };

  const handleRevisionAction = (val: string) => {
    switch (val) {
      case 'revision':
        addRevision();
        break;
      case 'history':
        break;
      default:
        history.push(`/parts/${val}`);
        break;
    }
  };

  const dropdownRender = (menus: any) => {
    // TODO: decide if user can create revision from old version
    return (
      <div className="ix-cascader-dropdown">
        {menus}
        <Divider style={{ margin: '4px 0' }} />
        <Tooltip title={revisionType === 'part' ? i18n('revisions.oldRevTip') : ''}>
          <button type="button" data-cy="revise-part-btn" className="link cascader-btn" disabled={revisionType === 'part'} onClick={addRevision}>
            <SnippetsOutlined />
            {i18n('toolbar.createRevision')}
          </button>
        </Tooltip>
        <br />
        <button type="button" data-cy="drawing-rev-history-btn" className="link cascader-btn" onClick={toggleDrawer}>
          <ReadOutlined />
          {i18n('toolbar.revisionHistory')}
        </button>
      </div>
    );
  };

  return (
    <section key="part-select-key" className="drawing-group">
      <section className="drawing-select">
        <span className="tool-label">{i18n('entities.part.name')}</span>
        <section className={`toolbar-part-number ${currentPartStep >= PartStepEnum.EXTRACT ? 'short' : 'wide'}`}>
          <span data-cy="toolbar-part-number" className={`toolbar-select part-number ${currentPartStep >= PartStepEnum.EXTRACT ? 'short' : 'wide'}`}>
            {part.number || '-'}
          </span>
        </section>
      </section>
      <Popconfirm
        title={i18n('revisions.oldRevWarn', part?.revision, revisionIndex)}
        onCancel={() => setConfirm(false)}
        onConfirm={() => {
          setConfirm(false);
          dispatch(doAddPartRevisions({ ...part, characteristics }, history));
        }}
        visible={confirm}
      >
        <section className="drawing-select">
          <span className="tool-label">{i18n('toolbar.revisionLabel')}</span>
          <Select
            data-cy="part-revision-select"
            onChange={handleRevisionAction}
            allowClear={false}
            className="toolbar-select"
            dropdownRender={dropdownRender}
            value={value}
            style={{ maxWidth: '140px' }}
            dropdownMatchSelectWidth={false}
            optionLabelProp="labelrender"
          >
            {options}
          </Select>
        </section>
      </Popconfirm>
    </section>
  );
};
