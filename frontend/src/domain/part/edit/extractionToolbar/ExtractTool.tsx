import React, { useState } from 'react';
import { Extract } from 'styleguide/shared/svg';
import { GatewayOutlined, FlagOutlined, ThunderboltOutlined } from '@ant-design/icons';
import { Tooltip, Button, Popover } from 'antd';
import { useDispatch, useSelector } from 'react-redux';
import { AppState } from 'modules/state';
import { CharacteristicActions } from 'modules/characteristic/characteristicActions';
import { i18n } from 'i18n';
import { getAutoBalloonResults } from 'utils/Autoballoon/Autoballoon';

const buttonID: string = 'toolbar-btn-extract';

const ExtractTool: React.FC = () => {
  const { mode } = useSelector((state: AppState) => state.characteristics);
  const { part } = useSelector((state: AppState) => state.part);
  const [active, setActive] = useState<boolean>(false);
  const { layerManager } = useSelector((state: AppState) => state.pdfView);
  const { settings } = useSelector((state: AppState) => state.session);
  const dispatch = useDispatch();

  let isAutoballooned = part?.autoMarkupStatus != null;
  const sheets1 = part?.drawings?.map((drawing) => drawing.sheets).flat() ?? [];
  const autoballoonValues = sheets1.reduce(getAutoBalloonResults, {
    found: 0,
    accepted: 0,
    acceptedTimeout: 0,
    rejected: 0,
    reviewed: 0,
    reviewedTimeout: 0,
    createdAt: part?.createdAt,
  });

  if (isAutoballooned && autoballoonValues?.found === 0) {
    if (autoballoonValues.createdAt) {
      const dateDiffMins = Math.floor(Math.abs(new Date(autoballoonValues.createdAt).getTime() - new Date().getTime()) / 1000 / 60);
      // If it takes more than 10 mins to get captures, assume its not being autoballooned
      if (dateDiffMins > 10) {
        isAutoballooned = false;
      }
    }
  }

  const selectMode = (newMode: string) => {
    dispatch({ type: CharacteristicActions.UPDATE_EXTRACT_MODE, payload: { mode: newMode } });
    setActive(false);
    if ((newMode === 'Manual' || newMode === 'Custom') && layerManager) {
      layerManager.moveLayerToFront('markers');
    }
  };

  const extractPopoverContent = (
    <div data-cy="extract-tool-content" className="popover-group-content">
      {isAutoballooned && settings.autoballoonEnabled?.value === 'true' ? (
        <button
          type="button"
          id={`${buttonID}-auto`}
          onClick={() => {
            selectMode('Auto');
            setActive(false);
          }}
        >
          <ThunderboltOutlined className="popover-action-icon" />
          <span className="popover-action-label">Auto Balloon</span>
          {/* <span className="hotkey-label">ctrl+e</span> */}
        </button>
      ) : (
        <>
          {settings.autoballoonEnabled?.value === 'true' && part?.autoMarkupStatus != null ? (
            <Tooltip title={i18n('entities.part.tooltips.partNotAutoballooned')} placement="right">
              <button
                disabled
                type="button"
                id={`${buttonID}-auto`}
                onClick={() => {
                  selectMode('Auto');
                  setActive(false);
                }}
              >
                <ThunderboltOutlined className="popover-action-icon" />
                <span className="popover-action-label">Auto Balloon</span>
                {/* <span className="hotkey-label">ctrl+e</span> */}
              </button>
            </Tooltip>
          ) : (
            <></>
          )}
        </>
      )}
      <button
        type="button"
        id={`${buttonID}-manual`}
        onClick={() => {
          selectMode('Manual');
          setActive(false);
        }}
      >
        <GatewayOutlined className="popover-action-icon" />
        <span className="popover-action-label">Manual Balloon</span>
        {/* <span className="hotkey-label">ctrl+shift+e</span> */}
      </button>
      <button
        type="button"
        id={`${buttonID}-custom`}
        onClick={() => {
          selectMode('Custom');
          setActive(false);
        }}
      >
        <FlagOutlined className="popover-action-icon" />
        <span className="popover-action-label">Custom Feature</span>
        {/* <span className="hotkey-label">ctrl+shift+e</span> */}
      </button>
    </div>
  );

  return (
    <Tooltip overlayClassName={active ? 'extract-tooltip-inactive' : 'extract-tooltip-active'} title={i18n(`toolbar.mode.${mode}`)} placement="bottom">
      <Popover placement="bottom" content={extractPopoverContent} title={i18n('toolbar.selectMode')} trigger="click" className="ixc-tool-menu" visible={active} onVisibleChange={() => setActive(!active)}>
        <Button className="toolbar-button" id={buttonID} onClick={() => setActive(true)}>
          <span className="tool-label">{i18n('toolbar.modeLabel')}</span>
          {mode !== 'Custom' ? <Extract color="white" bolt={mode === 'Auto' ? 'yellow' : 'transparent'} /> : <FlagOutlined />}
        </Button>
      </Popover>
    </Tooltip>
  );
};

export default ExtractTool;
