import React from 'react';
import { EyeOutlined, EyeInvisibleOutlined } from '@ant-design/icons';
import { i18n } from 'i18n';
import { useSelector } from 'react-redux';
import { AppState } from 'modules/state';
import { Button, Tooltip } from 'antd';

const buttonID: string = 'toolbar-btn-hide-show';

interface Props {
  toggleHideFeatures: any;
  hideFeatures: boolean;
}

const HideShowTool: React.FC<Props> = ({ toggleHideFeatures, hideFeatures }) => {
  const disabled = useSelector((state: AppState) => state.characteristics.characteristics?.length < 1);

  return (
    <Tooltip title={hideFeatures ? i18n('toolbar.show') : i18n('toolbar.hide')} placement="bottom">
      <Button key="hide-show-button" className="toolbar-button" id={buttonID} onClick={disabled ? () => {} : toggleHideFeatures}>
        <span className="tool-label">{i18n('toolbar.capturesLabel')}</span>
        {hideFeatures ? <EyeInvisibleOutlined /> : <EyeOutlined />}
      </Button>
    </Tooltip>
  );
};

export default HideShowTool;
