import React, { useState, useMemo } from 'react';
import { GroupSVG, UngroupSVG, BalloonSVG } from 'styleguide/shared/svg';
import Icon, { DeploymentUnitOutlined } from '@ant-design/icons';
import { Popover, Button, Tooltip } from 'antd';
import { Characteristic } from 'types/characteristics';
import { i18n } from 'i18n';
import { useDispatch, useSelector } from 'react-redux';
import { AppState } from 'modules/state';
import { CharacteristicActions } from 'modules/characteristic/characteristicActions';
import { ungroupItems, groupItems } from '../characteristics/utils/Renumber';

const buttonID: string = 'toolbar-btn-group';

interface GroupProps {
  selectedFeatures: Characteristic[];
}

const GroupTool = ({ selectedFeatures }: GroupProps) => {
  const { characteristics } = useSelector((state: AppState) => state.characteristics);
  const [active, setActive] = useState<boolean>(false);
  const dispatch = useDispatch();

  const mode = useMemo(() => {
    if (selectedFeatures.length === 0 || (selectedFeatures.length === 1 && !selectedFeatures[0].markerLabel.includes('.'))) return 'disabled';
    const { markerGroup } = selectedFeatures[0];
    if (selectedFeatures.some((char) => char.markerGroup !== markerGroup)) return 'group';
    return 'ungroup';
  }, [selectedFeatures]);

  // TODO figure out why ungrouping items from the middle of a group destroys the group
  const ungroup = () => {
    if (selectedFeatures.length < 1) return;
    const { editList } = ungroupItems(selectedFeatures, characteristics);
    if (editList && editList.length > 0) {
      dispatch({
        type: CharacteristicActions.EDIT_CHARACTERISTICS,
        payload: {
          updates: editList,
        },
      });
    }
  };

  const group = (shared: boolean) => {
    setActive(false);
    if (selectedFeatures.length < 2) return;
    const { editList } = groupItems(selectedFeatures, characteristics, shared);
    if (editList && editList.length > 0) {
      dispatch({
        type: CharacteristicActions.EDIT_CHARACTERISTICS,
        payload: {
          updates: editList,
        },
      });
    }
  };

  const groupPopoverContent = (
    <div data-cy="group-tool-content" className="popover-group-content">
      <button
        type="button"
        onClick={() => {
          group(true);
          setActive(false);
        }}
      >
        <Icon className="popover-action-icon" component={BalloonSVG} />
        <span className="popover-action-label">with Shared Balloon</span>
      </button>
      <button
        type="button"
        onClick={() => {
          group(false);
          setActive(false);
        }}
      >
        <DeploymentUnitOutlined className="popover-action-icon" />
        <span className="popover-action-label">with Sub-Balloons</span>
      </button>
    </div>
  );

  const buttonComponent = useMemo(() => {
    let component = <></>;
    switch (mode) {
      case 'group':
        component = (
          <Tooltip title={i18n('toolbar.group')} placement="bottom">
            <Popover placement="bottom" content={groupPopoverContent} title={i18n('toolbar.groupFeatures', selectedFeatures.length)} trigger="click" className="ixc-tool-menu" visible={active} onVisibleChange={() => setActive(!active)}>
              <Button key="group-button" className="toolbar-button" id={buttonID} onClick={() => setActive(true)}>
                <span className="tool-label">{i18n('toolbar.groupLabel')}</span>
                <Icon className="group-action-icon" component={GroupSVG} />
              </Button>
            </Popover>
          </Tooltip>
        );
        break;
      case 'ungroup':
        component = (
          <Tooltip title={i18n('toolbar.ungroup')} placement="bottom">
            <Button key="ungroup-button" className="toolbar-button" id={buttonID} onClick={ungroup}>
              <span className="tool-label">{i18n('toolbar.ungroupLabel')}</span>
              <Icon className="group-action-icon" component={UngroupSVG} />
            </Button>
          </Tooltip>
        );
        break;
      case 'disabled':
        component = (
          <Tooltip title={i18n('toolbar.selectFeatures')} placement="bottom">
            <Button key="group-button-disabled" className="toolbar-button" id={buttonID} onClick={() => {}}>
              <span className="tool-label">{i18n('toolbar.groupLabel')}</span>
              <Icon className="group-action-icon" component={GroupSVG} />
            </Button>
          </Tooltip>
        );
        break;
      default:
        break;
    }
    return component;
  }, [mode]);

  return buttonComponent;
};

export default GroupTool;
