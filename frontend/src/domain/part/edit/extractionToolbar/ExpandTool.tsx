import React, { useState, useRef } from 'react';
import { DeploymentUnitOutlined } from '@ant-design/icons';
import { Tooltip, Button, Popover } from 'antd';
import { expandCharacteristic, expandFeatureGroup } from 'domain/part/edit/characteristics/utils/Expand';
import ExpandSingle from 'styleguide/Popovers/ExpandSingle';
import { Characteristic } from 'types/characteristics';
import ExpandGroup from 'styleguide/Popovers/ExpandGroup';
import { i18n } from 'i18n';
import { useDispatch, useSelector } from 'react-redux';
import { AppState } from 'modules/state';
import { CharacteristicActions } from 'modules/characteristic/characteristicActions';

const buttonID: string = 'toolbar-btn-expand';

interface ExpandProps {
  selectedFeatures: Characteristic[];
}

// TODO update with selected changes
const ExpandTool = ({ selectedFeatures }: ExpandProps) => {
  const { characteristics } = useSelector((state: AppState) => state.characteristics);
  const [active, setActive] = useState<boolean>(false);
  const popover = useRef<any>();
  const dispatch = useDispatch();

  const expand = (item: Characteristic, shared: boolean) => {
    const { newChars, updatedChars } = expandCharacteristic(item, characteristics, shared);
    if (updatedChars && updatedChars.length > 0) {
      dispatch({
        type: CharacteristicActions.EDIT_CHARACTERISTICS,
        payload: {
          updates: updatedChars,
          newItems: newChars,
        },
      });
    } else if (newChars && newChars.length > 0) {
      dispatch({
        type: CharacteristicActions.CREATE_CHARACTERISTICS,
        payload: newChars,
      });
    }
  };

  const expandGroup = (item: Characteristic, shared: boolean) => {
    const group = characteristics.filter((char) => char.markerGroup === item.markerGroup).sort((a, b) => a.markerIndex - b.markerIndex);
    const { newChars, updatedChars } = expandFeatureGroup(group, characteristics, shared);
    if (updatedChars && updatedChars.length > 0) {
      dispatch({
        type: CharacteristicActions.EDIT_CHARACTERISTICS,
        payload: {
          updates: updatedChars,
          newItems: newChars,
        },
      });
    } else if (newChars && newChars.length > 0) {
      dispatch({
        type: CharacteristicActions.CREATE_CHARACTERISTICS,
        payload: newChars,
      });
    }
  };

  const feature = selectedFeatures[0];
  const grouped = feature && feature?.markerSubIndex && feature?.markerSubIndex >= 0;

  return (
    <Tooltip overlayClassName={active ? 'expand-tooltip-inactive' : 'expand-tooltip-active'} title={feature?.quantity && feature.quantity > 1 ? i18n('toolbar.expand') : i18n('toolbar.selectMultiple')} placement="bottom">
      <Popover //
        overlayClassName="expand-toolbar-popover"
        ref={popover}
        placement="bottom"
        content={grouped ? <ExpandGroup item={feature} expand={expandGroup} /> : <ExpandSingle item={feature} expand={expand} />}
        title={i18n(grouped ? 'entities.feature.expand.groupAction' : 'entities.feature.expand.action', feature?.quantity || 0)}
        trigger="click"
        onVisibleChange={() => setActive(!active)}
      >
        <Button key="expand-button" className="toolbar-button" id={buttonID} onClick={() => setActive(true)}>
          <span className="tool-label">{i18n('toolbar.expandLabel')}</span>
          <DeploymentUnitOutlined className="expand-action-icon" />
        </Button>
      </Popover>
    </Tooltip>
  );
};

export default ExpandTool;
