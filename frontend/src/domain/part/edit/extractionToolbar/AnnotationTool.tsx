import { FormOutlined } from '@ant-design/icons';
import { Button, Tooltip } from 'antd';
import { i18n } from 'i18n';
import { createAnnotationThunk } from 'modules/annotation/annotationActions';
import React from 'react';
import { useDispatch } from 'react-redux';

export const AnnotationTool = () => {
  const dispatch = useDispatch();

  const handleCreateAnnotation = () => {
    dispatch(createAnnotationThunk());
  };

  return (
    <Tooltip title={i18n('toolbar.annotation')} placement="bottom">
      <Button id="toolbar-btn-annotation" key="annotation-button" className="toolbar-button" onClick={handleCreateAnnotation}>
        <span className="tool-label">{i18n('toolbar.annotationLabel')}</span>
        <FormOutlined />
      </Button>
    </Tooltip>
  );
};
