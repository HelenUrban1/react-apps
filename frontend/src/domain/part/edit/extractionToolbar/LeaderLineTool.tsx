import React from 'react';
import { LeaderLinesOff, LeaderLinesOn } from 'styleguide/shared/svg';
import { i18n } from 'i18n';
import { useSelector } from 'react-redux';
import { AppState } from 'modules/state';
import { Button, Tooltip } from 'antd';

const buttonID: string = 'toolbar-btn-leader-line';

interface Props {
  toggleLeaderLines: any;
}

const LeaderLineTool: React.FC<Props> = ({ toggleLeaderLines }) => {
  const disabled = useSelector((state: AppState) => state.characteristics.characteristics?.length < 1);
  const { part } = useSelector((state: AppState) => state.part);

  return (
    <Tooltip title={part?.displayLeaderLines ? i18n('toolbar.hideLeaders') : i18n('toolbar.showLeaders')} placement="bottom">
      <Button key="leader-line-button" className="toolbar-button" id={buttonID} onClick={disabled ? () => {} : toggleLeaderLines}>
        <span className="tool-label">{i18n('toolbar.leadersLabel')}</span>
        {part?.displayLeaderLines ? <LeaderLinesOn color="white" /> : <LeaderLinesOff color="white" />}
      </Button>
    </Tooltip>
  );
};

export default LeaderLineTool;
