import React, { useState, useEffect, useRef } from 'react';
import Icon from '@ant-design/icons';
import { BalloonSVG } from 'styleguide/shared/svg';
import { Button, Popover, Tooltip } from 'antd';
import ResizeBalloon from 'styleguide/Popovers/ResizeBalloon';
import { i18n } from 'i18n';
import debounce from 'lodash/debounce';
import { useDispatch, useSelector } from 'react-redux';
import { AppState } from 'modules/state';
import { CharacteristicActions } from 'modules/characteristic/characteristicActions';
import { Characteristic } from 'types/characteristics';
import { updateDrawingSheetsThunk, updateDrawingSheetThunk } from 'modules/partEditor/partEditorActions';
import { DrawingSheetChange } from 'graphql/drawing_sheet';

const buttonID: string = 'toolbar-btn-resize';

interface ResizeToolProps {
  index: number;
  zoom: number;
}

const BalloonResizeTool = ({ index, zoom }: ResizeToolProps) => {
  const { characteristics } = useSelector((state: AppState) => state.characteristics);
  const { partDrawing, partEditorLoaded, assignedStyles } = useSelector((state: AppState) => state.partEditor);
  const { markers } = useSelector((state: AppState) => state.session);
  const [active, setActive] = useState<boolean>(false);
  const [markerSize, setMarkerSize] = useState<number>(36);
  const popover = useRef<any>();
  const dispatch = useDispatch();

  const updateMarker = (e: any, applyToAllPages = false) => {
    const updates: Characteristic[] = [];
    characteristics
      .filter((item) => partDrawing?.id === item.drawing.id && (applyToAllPages || item.drawingSheetIndex - 1 === index) && item.markerSize !== e)
      .forEach((item) => {
        updates.push({ ...item, markerSize: e, markerFontSize: Math.round(e / 2) });
      });
    dispatch({
      type: CharacteristicActions.EDIT_CHARACTERISTICS,
      payload: {
        updates,
      },
    });
    if (applyToAllPages) {
      const changes: { drawingSheetChanges: DrawingSheetChange; pageIndex: number }[] = [];
      partDrawing?.sheets.forEach((sheet) => {
        changes.push({ pageIndex: sheet.pageIndex, drawingSheetChanges: { markerSize: e } });
      });
      dispatch(updateDrawingSheetsThunk(changes));
    } else {
      dispatch(updateDrawingSheetThunk(index, { markerSize: e }));
    }
  };

  const debouncedUpdateMarker = debounce(updateMarker, 500, {
    trailing: true,
  });

  const getMarkerSize = () => {
    const char = characteristics.find((c) => c.drawingSheet.id === partDrawing?.sheets[index].id);
    if (char) return char.markerSize;
    if (partDrawing?.sheets[index]?.markerSize) return partDrawing.sheets[index].markerSize;
    if (characteristics.length > 0) return characteristics[0].markerSize;
    return 36;
  };

  useEffect(() => {
    if (partDrawing?.sheets && index < partDrawing?.sheets.length) setMarkerSize(getMarkerSize());
  }, [index, partDrawing?.sheets, characteristics]);

  return (
    <Tooltip title={i18n('toolbar.resize')} placement="bottom">
      <Popover //
        overlayClassName="resize-toolbar-popover"
        ref={popover}
        placement="bottomRight"
        // title={i18n('entities.feature.resize.action')}
        content={
          <ResizeBalloon
            active={active}
            markerSize={markerSize || 36}
            updateMarker={debouncedUpdateMarker}
            singleSheet={partDrawing?.sheets.length === 1}
            loading={!partEditorLoaded}
            marker={markers ? markers.find((marker) => marker.id === assignedStyles?.default.style) : undefined}
            zoom={zoom}
          />
        }
        trigger="click"
        onVisibleChange={() => {
          setActive(!active);
        }}
      >
        <Button id="toolbar-btn-resize" key="resize-button" className="toolbar-button" onClick={() => setActive(true)}>
          <span className="tool-label">{i18n('toolbar.resizeLabel')}</span>
          <Icon id={buttonID} component={BalloonSVG} />
        </Button>
      </Popover>
    </Tooltip>
  );
};

export default BalloonResizeTool;
