import React, { useState, useEffect, useRef } from 'react';
import { DiffOutlined, PlusCircleTwoTone, ReadOutlined, SnippetsOutlined } from '@ant-design/icons';
import { Cascader, Divider, Popconfirm, Select, Tooltip } from 'antd';
import { i18n } from 'i18n';
import { AppState } from 'modules/state';
import { useDispatch, useSelector } from 'react-redux';
import moment from 'moment';
import { changeDrawingThunk, startCompareDrawingThunk, startDrawingAlignmentThunk } from 'modules/partEditor/partEditorActions';
import { uploadDrawingThunk } from 'modules/part/partActions';
import { UploadModal } from 'styleguide/Modals/UploadModal';
import { Drawing } from 'graphql/drawing';
import { getCurrentDrawings } from 'modules/characteristic/characteristicActions';
import { PartStepEnum } from 'modules/navigation/navigationActions';
import { BaseOptionType, CascaderRef } from 'antd/lib/cascader';

interface Props {
  toggleDrawer: () => void;
}

export const DrawingSelect: React.FC<Props> = ({ toggleDrawer }) => {
  const { part, revisionIndex } = useSelector((state: AppState) => state.part);
  const { partDrawing, partEditorLoaded, targetDrawing } = useSelector((state: AppState) => state.partEditor);
  const { tronInstance } = useSelector((state: AppState) => state.pdfView);
  const { revisionFiles } = useSelector((state: AppState) => state.pdfOverlayView);
  const { currentPartStep } = useSelector((state: AppState) => state.navigation);

  const dispatch = useDispatch();

  const [confirm, setConfirm] = useState(false);
  const [upload, setUpload] = useState(false);
  const [addDrawingToPart, setAddDrawingToPart] = useState(false);

  const [drawingValue, setDrawingValue] = useState(partDrawing?.id || '');
  const [drawingOptions, setDrawingOptions] = useState<{}[]>([]);

  const [revValue, setRevValue] = useState<string[]>([partDrawing?.id || '', '']);
  const [revOptions, setRevOptions] = useState<BaseOptionType[]>([]);
  const cascaderRef = useRef<CascaderRef>(null);

  useEffect(() => {
    if (partDrawing) {
      const revOpts: BaseOptionType[] = [];

      const drawingRevisions = part?.drawings?.filter((drawing) => drawing.originalDrawing === partDrawing.originalDrawing).sort((a, b) => (moment(a.createdAt).isAfter(moment(b.createdAt)) ? -1 : 1));
      drawingRevisions?.forEach((drawing, index) => {
        revOpts.push({
          key: drawing.id,
          value: drawing.id,
          label: (
            <span>
              <Tooltip placement="left" title={moment(drawing.createdAt).format('YYYY-MM-DD hh:mm:ss A')}>{`${drawing.revision || moment(drawing.createdAt).format('YYYY-MM-DD')}${index === 0 ? ' (current)' : ''}`}</Tooltip>
            </span>
          ),
          children: [],
          disabled: drawing.id === partDrawing.id,
        });
      });
      if (revOpts.length === 0) {
        revOpts.push({
          key: partDrawing.id,
          value: partDrawing.id,
          label: partDrawing.revision || moment(partDrawing.createdAt).format('YYYY-MM-DD'),
          children: [],
          disabled: true,
        });
      }
      if (drawingRevisions && drawingRevisions.length > 1) {
        revOpts.push({
          key: 'compare-drawing-key',
          value: 'compare',
          label: (
            <div data-cy="compare-drawing-btn" className="cascader-btn" style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
              <span className="cascader-btn-label">
                <DiffOutlined />
                {i18n('toolbar.compareTo')}
              </span>
            </div>
          ),
          children: drawingRevisions?.map((drawing) => {
            return {
              key: drawing.id,
              value: drawing.id,
              label: (
                <span>
                  <Tooltip placement="right" title={moment(drawing.createdAt).format('YYYY-MM-DD hh:mm:ss A')}>
                    {drawing.revision || moment(drawing.createdAt).format('YYYY-MM-DD')}
                  </Tooltip>
                </span>
              ),
              children: [],
              disabled: drawing.id === partDrawing.id,
            };
          }),
          disabled: !drawingRevisions || drawingRevisions?.length <= 0,
        });
      }
      setRevOptions(revOpts);
      setRevValue([partDrawing.id]);
    }
  }, [partDrawing?.id, partDrawing?.revision, part?.drawings]);

  useEffect(() => {
    if (partDrawing && part?.drawings) {
      const currentDrawings: Drawing[] = getCurrentDrawings(part.drawings, partDrawing.id);
      const opts =
        currentDrawings.length > 0
          ? currentDrawings.map((draw, index) => {
              const curDraw = draw.id === partDrawing.id ? partDrawing : draw;
              let text = curDraw.number;
              if (!text || text.length <= 0) {
                text = curDraw.name;
              }
              if (!text || text.length <= 0) {
                text = curDraw.file[0].name;
              }
              if (!text || text.length <= 0) {
                text = `Drawing ${index + 1}`;
              }
              return (
                <Select.Option key={curDraw.id} value={curDraw.id} label={curDraw.number && curDraw.number.length > 0 ? curDraw.number : '-'}>
                  {`${text}${curDraw.id === part?.primaryDrawing.id ? ' (primary)' : ''}`}
                </Select.Option>
              );
            })
          : [<></>];
      opts.push(
        <Select.Option key="rev-draw-add-key" value="add">
          <div data-cy="drawing-rev-add-btn" className="cascader-btn">
            <PlusCircleTwoTone twoToneColor="#1890ff" />
            {i18n('toolbar.addDrawing')}
          </div>
        </Select.Option>,
      );
      setDrawingOptions(opts);
      setDrawingValue(partDrawing.id);
    }
  }, [partDrawing?.id, partDrawing?.number, partDrawing?.revision, part?.drawings]);

  useEffect(() => {
    if (tronInstance && partEditorLoaded && revisionFiles && targetDrawing) {
      // Part editor loaded after starting Drawing Rev from Part Details Drawer
      // Start the Drawing Alignment Step
      dispatch(startDrawingAlignmentThunk(revisionFiles, targetDrawing));
    }
  }, [revisionFiles, tronInstance, partEditorLoaded]);

  const hasDragListener = useRef(false);

  // Handle drop outside of drop zone by preventing default browser file upload
  const handleDropOutOfBounds = (e: any) => {
    e.preventDefault();
  };

  // Drag Event Listeners
  useEffect(() => {
    if (!hasDragListener.current && !upload) {
      hasDragListener.current = true;
      document.removeEventListener('drop', handleDropOutOfBounds);
      document.removeEventListener('dragover', handleDropOutOfBounds);
    } else if (hasDragListener.current && upload) {
      // prevent browser opening file if dropped outside of drop zone
      document.addEventListener('dragover', handleDropOutOfBounds);
      document.addEventListener('drop', handleDropOutOfBounds);
      hasDragListener.current = false;
    }
  }, [upload]);

  if (!partDrawing) return <></>;

  const handleAddDrawing = () => {
    // add a new drawing
    setAddDrawingToPart(true);
    setUpload(true);
  };

  const handleCompareDrawing = (val: string) => {
    dispatch(startCompareDrawingThunk(val, 'Compare'));
  };

  const handleChangeDrawing = (val: string) => {
    if (val === 'add') {
      handleAddDrawing();
    } else {
      dispatch(changeDrawingThunk(val));
    }
  };

  const addRevision = () => {
    if (revisionIndex) {
      setConfirm(true);
    } else {
      setUpload(true);
    }
  };

  const handleRevisionAction = (value: any, selectOptions: any) => {
    switch (value[0]) {
      case 'revision':
        addRevision();
        break;
      case 'history':
        break;
      case 'compare':
        handleCompareDrawing(value[1].toString());
        break;
      default:
        setRevValue(value);
        handleChangeDrawing(value[0].toString());
        break;
    }
  };

  const handleAction = (action: Function) => {
    cascaderRef.current?.blur();
    action();
  };

  const getTip = (ind: number, stage: number | undefined) => {
    if (ind >= 1) return i18n('revisions.oldRevTip');
    if ((stage || 0) < 1) return i18n('revisions.stage');
    return '';
  };

  const dropdownRender = (menus: any) => {
    return (
      <div className="ix-cascader-dropdown">
        {menus}
        <Divider style={{ margin: '4px 0' }} />
        <Tooltip title={getTip(revisionIndex, part?.workflowStage)}>
          <button type="button" data-cy="revise-drawing-btn" className="link cascader-btn" disabled={revisionIndex >= 1 || (part?.workflowStage || 0) < 1} onClick={() => handleAction(addRevision)}>
            <SnippetsOutlined />
            {i18n('toolbar.createRevision')}
          </button>
        </Tooltip>
        <br />
        <button type="button" data-cy="drawing-rev-history-btn" className="link cascader-btn" onClick={() => handleAction(toggleDrawer)}>
          <ReadOutlined />
          {i18n('toolbar.revisionHistory')}
        </button>
      </div>
    );
  };

  const handlePartDrawingUpload = async (files: any[], skipAlignment = false) => {
    const shouldCreatePart = false;
    if (addDrawingToPart) {
      // Call the upload drawing thunk to create the drawing and add it to a part or create a new part
      dispatch(uploadDrawingThunk(files, shouldCreatePart, addDrawingToPart));
      setAddDrawingToPart(false);
    } else if (skipAlignment) {
      dispatch(uploadDrawingThunk(files, shouldCreatePart, addDrawingToPart, partDrawing));
    } else {
      // Upload the drawing and then handle a drawing revision
      dispatch(startDrawingAlignmentThunk(files));
    }
    setUpload(false);
  };

  const handleCancelUpload = () => {
    setAddDrawingToPart(false);
    setUpload(false);
  };

  return (
    <section key="drawing-select-key" className="drawing-group">
      <UploadModal //
        isDisplayed={upload}
        handleUpload={handlePartDrawingUpload}
        handleCancel={handleCancelUpload}
        fileType="pdf"
        fileExt=".pdf"
        hint="PDF"
        entity={i18n('entities.part.name')}
        limit={10 - (part?.drawings?.length || 0)}
        type="Drawing"
        targetDrawing={upload && !addDrawingToPart ? partDrawing : undefined}
      />
      <section className="drawing-select">
        <span className="tool-label">{i18n('toolbar.drawingsLabel')}</span>
        <Select data-cy="part-drawing-select" className={`toolbar-select ${currentPartStep >= PartStepEnum.EXTRACT ? 'short' : 'wide'}`} onChange={handleChangeDrawing} value={drawingValue} dropdownMatchSelectWidth={false} optionLabelProp="label">
          {drawingOptions}
        </Select>
      </section>
      <Popconfirm
        title={i18n('revisions.oldRevWarnDraw', part?.revision, partDrawing.revision, revisionIndex)}
        onCancel={() => setConfirm(false)}
        onConfirm={() => {
          setConfirm(false);
          setUpload(true);
        }}
        visible={confirm}
      >
        <section className="drawing-select">
          <span className="tool-label">{i18n('toolbar.revisionLabel')}</span>
          <Cascader
            ref={cascaderRef}
            data-cy="drawing-revision-select"
            onChange={handleRevisionAction}
            allowClear={false}
            className="toolbar-select"
            dropdownRender={dropdownRender}
            value={revValue}
            style={{ maxWidth: '110px' }}
            options={revOptions}
            displayRender={() => {
              return partDrawing.revision;
            }}
          />
        </section>
      </Popconfirm>
    </section>
  );
};
