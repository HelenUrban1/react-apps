import React, { useEffect, useMemo } from 'react';
import { CharacteristicActions } from 'modules/characteristic/characteristicActions';
import { useDispatch, useSelector } from 'react-redux';
import { AppState } from 'modules/state';
import Spinner from 'view/shared/Spinner';
import { PartStepEnum } from 'modules/navigation/navigationActions';
import { Toolbar } from 'styleguide/Toolbar/Toolbar';
import { WebViewerInstance } from '@pdftron/webviewer';
import { Characteristic } from 'types/characteristics';
import { Annotation } from 'types/annotation';
import { ExtractTool, ExpandTool, GroupTool, HideShowTool, LeaderLineTool, BalloonResizeTool, PdfTool } from '.';
import { DrawingSelect } from './DrawingSelect';
import { PartSelect } from './PartSelect';
import { CompareTool } from './CompareTool';
import './extractionToolbarStyles.less';
import { AnnotationTool } from './AnnotationTool';
import { deleteAnnotationThunk } from 'modules/annotation/annotationActions';

export interface Constraints {
  margin: string;
  width: string;
}

interface Props {
  toggleHideFeatures: any;
  hideFeatures: boolean;
  toggleLeaderLines: any;
  toggleDrawer: () => void;
}

const { EXTRACT } = PartStepEnum;

export const ExtractionTools = ({ toggleHideFeatures, hideFeatures, toggleLeaderLines, toggleDrawer }: Props) => {
  const { mode, selected } = useSelector((state: AppState) => state.characteristics);
  const { currentPartStep } = useSelector((state: AppState) => state.navigation);
  const { settings } = useSelector((state: AppState) => state.session);
  const { compareMode } = useSelector((state: AppState) => state.partEditor);
  const { part } = useSelector((state: AppState) => state.part);
  const { tronInstance, zoom, pdfLoaded, index } = useSelector((state: AppState) => state.pdfView);
  const { tronInstance: tronOverlayInstance, zoom: overlayZoom, pdfLoaded: overlayLoaded } = useSelector((state: AppState) => state.pdfOverlayView);
  const dispatch = useDispatch();

  const selectedFeatures: Characteristic[] = useMemo(() => selected.filter((s) => !!s.quantity) as Characteristic[], [selected]);
  const selectedAnnotations: Annotation[] = useMemo(() => selected.filter((s) => !!s.backgroundColor) as Annotation[], [selected]);

  const instance = useMemo(() => (tronOverlayInstance || tronInstance) as WebViewerInstance | null, [tronInstance, tronOverlayInstance]);
  const z = useMemo(() => (tronOverlayInstance ? overlayZoom : zoom), [instance, zoom, overlayZoom]);
  const loaded = useMemo(() => (tronOverlayInstance ? overlayLoaded : pdfLoaded), [instance, overlayLoaded, pdfLoaded]);

  useEffect(() => {
    if (mode !== 'Auto' && mode !== 'Manual' && mode !== 'Custom') {
      const selectedMode = settings.autoballoonEnabled?.value === 'true' && part?.autoMarkupStatus != null ? 'Auto' : 'Manual';
      dispatch({ type: CharacteristicActions.UPDATE_EXTRACT_MODE, payload: { mode: selectedMode } });
    }
  }, [mode]);

  const deleteActive = () => {
    if (!part || !part.characteristics) return;
    const deletedCharIds: string[] = [];
    const deletedAnnotIds: string[] = [];
    selected.forEach((s) => {
      if (s.captureMethod) deletedCharIds.push(s.id);
      if (s.annotation) deletedAnnotIds.push(s.id);
    });
    if (deletedCharIds.length > 0) {
      dispatch({
        type: CharacteristicActions.DELETE_CHARACTERISTICS,
        payload: {
          ids: deletedCharIds,
          part: part.id,
        },
      });
    }
    if (deletedAnnotIds.length > 0) {
      dispatch(deleteAnnotationThunk({ variables: { ids: deletedAnnotIds } }, part));
    }
  };

  const handleZoomIn = () => {
    const viewer = instance?.Core.documentViewer;
    if (viewer) {
      const increment = Math.min(1, Math.max(z * 0.2, 0.06));
      const newZoom = Math.min(4, z + increment);
      viewer.zoomTo(newZoom);
    }
  };

  const handleZoomOut = () => {
    const viewer = instance?.Core.documentViewer;
    if (viewer) {
      const increment = Math.min(1, Math.max(z * 0.2, 0.06));
      const newZoom = Math.max(0.25, z - increment);
      viewer.zoomTo(newZoom);
    }
  };

  const handleRotateClockwise = () => {
    const viewer = instance?.Core.documentViewer;
    if (viewer) {
      viewer.rotateClockwise();
    }
  };

  const handleRotateCounterClockwise = () => {
    const viewer = instance?.Core.documentViewer;
    if (viewer) {
      viewer.rotateCounterClockwise();
    }
  };

  const toggleZoomOverlay = () => {
    if (instance) {
      const { UI } = instance;
      if (UI.isElementOpen('zoomOverlay')) {
        UI.closeElements(['zoomOverlay']);
      } else {
        UI.openElements(['zoomOverlay']);
      }
    }
  };

  const togglePanel = () => {
    if (instance) {
      const { UI } = instance;
      if (UI.isElementOpen('leftPanel')) {
        UI.closeElements(['leftPanel']);
      } else {
        UI.openElements(['leftPanel']);
      }
    }
  };

  const docTools = useMemo(() => {
    if (!loaded) return [];
    return compareMode ? [<CompareTool />] : [<PartSelect key="part-select" toggleDrawer={toggleDrawer} />, <DrawingSelect key="drawing-select" toggleDrawer={toggleDrawer} />];
  }, [loaded, compareMode]);

  const extractionTools = useMemo(() => {
    if (currentPartStep >= EXTRACT && compareMode !== 'Align')
      return [
        <ExtractTool key="extract-tool" />,
        <AnnotationTool key="annotation-tool" />,
        <BalloonResizeTool key="resize-tool" index={index} zoom={z} />,
        <ExpandTool key="expand-tool" selectedFeatures={selectedFeatures} />,
        <GroupTool key="group-tool" selectedFeatures={selectedFeatures} />,
        <HideShowTool key="hide-show-tool" hideFeatures={hideFeatures} toggleHideFeatures={toggleHideFeatures} />,
        <LeaderLineTool key="leader-line-tool" toggleLeaderLines={toggleLeaderLines} />,
      ];
    if (compareMode === 'Align' || compareMode === 'Compare') return [<div className="placeholder" style={{ backgroundColor: 'transparent' }} />];
    return [];
  }, [currentPartStep, compareMode, selectedFeatures, selectedAnnotations, z]);

  const pdfTools = useMemo(() => {
    return loaded
      ? [
          // eslint-disable-next-line react/jsx-indent
          <PdfTool
            key="pdf-tool"
            handleZoomIn={handleZoomIn}
            handleZoomOut={handleZoomOut}
            handleRotateClockwise={handleRotateClockwise}
            handleRotateCounterClockwise={handleRotateCounterClockwise}
            toggleZoomOverlay={toggleZoomOverlay}
            togglePanel={togglePanel}
            currentZoom={Math.trunc(z * 100).toString()}
          />,
        ]
      : [<Spinner key="spinner" style={{ margin: '0', paddingTop: '4px' }} />];
  }, [loaded, z, tronOverlayInstance]);

  return <Toolbar extractTools={extractionTools} documentTools={docTools} pdfTools={pdfTools} deleteActive={deleteActive} />;
};
