import { Button } from 'antd';
import config from 'config';
import { i18n } from 'i18n';
import { uploadDrawingThunk } from 'modules/part/partActions';
import { endDrawingAlignmentThunk } from 'modules/partEditor/partEditorActions';
import { AppState } from 'modules/state';
import moment from 'moment';
import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { ColorPicker } from 'styleguide/ColorPicker';
import { FileObject } from 'styleguide/File/Upload';
import { buildRevisedPdf, loadPdfCompare } from 'styleguide/PdfViewer/PdfViewerUtil';
import { isPDF } from 'utils/files';
import { converRgbToHex, convertHexToRgb } from 'utils/styles';
import { v4 as uuid } from 'uuid';

export const CompareTool = () => {
  const { partDrawing, compareDrawing, compareMode } = useSelector((state: AppState) => state.partEditor);
  const { tronInstance } = useSelector((state: AppState) => state.pdfView);
  const { revisionFile, tronInstance: tronOverlayInstance } = useSelector((state: AppState) => state.pdfOverlayView);
  const { jwt } = useSelector((state: AppState) => state.auth);
  const [colorA, setColorA] = useState([1, 197, 197, 1]); // rgb pdftron blue
  const [colorB, setColorB] = useState([197, 2, 2, 1]); // rgb pdftron red

  useEffect(() => {
    if (tronInstance && partDrawing && compareDrawing && jwt && colorA && colorB) {
      loadPdfCompare(tronInstance, partDrawing, compareDrawing, jwt, colorA, colorB);
    }
  }, [colorA, colorB]);

  const dispatch = useDispatch();

  const handleEndCompare = () => {
    dispatch(endDrawingAlignmentThunk());
  };

  const handleEndAlignment = async () => {
    if (tronInstance && tronOverlayInstance && revisionFile && jwt) {
      const newFile: any = await buildRevisedPdf(revisionFile, tronInstance, tronOverlayInstance);
      const fileObj: FileObject = {
        uuid: uuid(),
        uid: newFile.uid,
        size: newFile.size,
        name: newFile.name,
        fileName: newFile.name,
        status: 'error',
        response: i18n('importer.error.invalidFile'),
        type: newFile.type,
        ocr: false,
        originFileObj: newFile,
      };
      dispatch(uploadDrawingThunk([fileObj], false, true, partDrawing));
    }
  };

  const handleColorChange = (color: string, field: string) => {
    const rgb = convertHexToRgb(color);
    if (rgb) {
      if (field === 'A') setColorA(rgb);
      if (field === 'B') setColorB(rgb);
    }
  };

  return (
    <section key="drawing-revision-key" className="drawing-group">
      {compareMode === 'Align' ? (
        <span style={{ color: 'white' }}>Align the revised drawing with your current drawing</span>
      ) : (
        <section className="drawing-select">
          <span className="tool-label">drawing</span>
          <section className="toolbar-part-number wide">
            <span data-cy="toolbar-rev-first" className="toolbar-select part-number wide">
              {partDrawing?.number || '-'}
            </span>
          </section>
        </section>
      )}
      {compareMode === 'Compare' && (
        <>
          <section className="drawing-select">
            <span className="tool-label">revision</span>
            <section className="toolbar-part-number wide">
              <span data-cy="toolbar-rev-first" className="toolbar-select part-number wide">
                {partDrawing?.revision || moment(partDrawing?.createdAt).format('YYYY-MM-DD')}
              </span>
              <ColorPicker value={{ hex: converRgbToHex(colorA) }} cValue={{ hex: converRgbToHex(colorA) }} field="A" handleChangeColor={handleColorChange} />
            </section>
          </section>
          <section className="drawing-select">
            <span className="tool-label">revision</span>
            <section className="toolbar-part-number wide">
              <span data-cy="toolbar-rev-second" className="toolbar-select part-number wide">
                {compareDrawing?.revision || moment(compareDrawing?.createdAt).format('YYYY-MM-DD')}
              </span>
              <ColorPicker value={{ hex: converRgbToHex(colorB) }} cValue={{ hex: converRgbToHex(colorB) }} field="B" handleChangeColor={handleColorChange} />
            </section>
          </section>
        </>
      )}
      <Button
        className="btn-primary"
        onClick={() => {
          if (compareMode === 'Align') {
            handleEndAlignment();
          } else {
            handleEndCompare();
          }
        }}
      >
        {compareMode === 'Align' ? 'Done' : 'Finish compare'}
      </Button>
      {compareMode === 'Align' && (
        <Button className="btn-secondary" onClick={handleEndCompare}>
          Cancel
        </Button>
      )}
      ,
    </section>
  );
};
