import React from 'react';
import IconButton from 'styleguide/Buttons/IconButton';
import { UndoSVG } from 'styleguide/shared/svg';

const buttonID = 'toolbar-btn-undo';

const UndoTool = () => {
  return <IconButton id={buttonID} custom CustomSvg={UndoSVG} tipText="Undo" tipPlacement="bottom" />;
};

export default UndoTool;
