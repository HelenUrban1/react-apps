import React from 'react';
import { Select } from 'antd';
import { useSelector } from 'react-redux';
import { AppState } from 'modules/state';
import Spinner from 'view/shared/Spinner';
import { i18n } from 'i18n';
import { PartStepEnum } from 'modules/navigation/navigationActions';

const { Option } = Select;

const PageSelector: React.FC = () => {
  const { tronInstance } = useSelector((state: AppState) => state.pdfView);
  const { partDrawing, partDrawingSheet } = useSelector((state: AppState) => state.partEditor);
  const { currentPartStep } = useSelector((state: AppState) => state.navigation);

  if (!partDrawing || !partDrawingSheet) {
    return <Spinner key="spinner" style={{ margin: '0', paddingTop: '4px' }} />;
  }

  const handlePageChange = (value: string) => {
    const newIndex = parseInt(value, 10);
    const viewer = tronInstance?.Core.documentViewer;
    if (viewer && newIndex) {
      viewer.setCurrentPage(newIndex);
    }
  };

  return (
    <section id="page-select-block" key="page-select-key" className="tool-block">
      <span className="tool-label">{i18n('toolbar.pageSelectLabel')}</span>
      <Select //
        dropdownClassName="pdf-sheet-selector"
        className={`toolbar-select ${currentPartStep >= PartStepEnum.EXTRACT ? 'short' : 'wide'}`}
        dropdownMatchSelectWidth={false}
        value={(partDrawingSheet.pageIndex + 1).toString()}
        onChange={handlePageChange}
        optionLabelProp="label"
      >
        {partDrawing.sheets?.map((sheet) => (
          <Option key={sheet.id} value={`${sheet.pageIndex + 1}`} label={sheet.pageName || `${sheet.pageIndex + 1} / ${partDrawing.sheets.length}`} data-cy={`pdf-sheet-${sheet.pageIndex}`} className="show-option">
            {`${sheet.pageIndex + 1}: ${sheet.pageName}`}
          </Option>
        ))}
      </Select>
    </section>
  );
};

export default PageSelector;
