import React from 'react';
import IconButton from 'styleguide/Buttons/IconButton';
import { RedoSVG } from 'styleguide/shared/svg';
import { RedoOutlined } from '@ant-design/icons';

const buttonID = 'toolbar-btn-redo';

const RedoTool = () => {
  return <IconButton id={buttonID} custom CustomSvg={RedoSVG} icon={<RedoOutlined />} tipText="Redo" tipPlacement="bottom" />;
};

export default RedoTool;
