import { DownOutlined, LayoutOutlined, MinusCircleOutlined, PlusCircleOutlined, RotateLeftOutlined, RotateRightOutlined } from '@ant-design/icons';
import { Button, Tooltip } from 'antd';
import { slide as Menu } from 'react-burger-menu';
import { i18n } from 'i18n';
import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import { AppState } from 'modules/state';
import PageSelector from './PageSelector';

interface PdfProps {
  handleZoomIn: () => void;
  handleZoomOut: () => void;
  handleRotateClockwise: () => void;
  handleRotateCounterClockwise: () => void;
  toggleZoomOverlay: () => void;
  togglePanel: () => void;
  currentZoom: string;
}

const plusButtonID: string = 'toolbar-btn-zoom-in';
const minusButtonID: string = 'toolbar-btn-zoom-out';
const zoomOverlayButtonID: string = 'toolbar-btn-zoom-overlay';
const panelButtonID: string = 'toolbar-btn-panel';
const clockwiseID: string = 'toolbar-btn-clockwise';
const counterClockwiseID: string = 'toolbar-btn-counter';

export const PdfTool = ({ handleZoomIn, handleZoomOut, toggleZoomOverlay, handleRotateClockwise, handleRotateCounterClockwise, togglePanel, currentZoom }: PdfProps) => {
  const { compareMode } = useSelector((state: AppState) => state.partEditor);

  const [useBurger, setUseBurger] = useState(false);
  const [burger, setBurger] = useState(false);

  const handleWindowResize = () => {
    if (window.innerWidth < 1300) {
      setUseBurger(true);
    } else {
      setUseBurger(false);
    }
  };

  useEffect(() => {
    window.addEventListener('resize', handleWindowResize);

    return function cleanup() {
      window.removeEventListener('resize', handleWindowResize);
    };
  }, []);

  const handleButtonClick = (action: Function, closeBurger?: boolean) => {
    if (closeBurger) setBurger(false);
    action();
  };

  const tools = [
    <Tooltip key="thumbnails-tip-key" placement="bottom" title={i18n('toolbar.viewThumbnails')}>
      <Button key="thumbnail-button" className="toolbar-button" id={panelButtonID} onClick={() => handleButtonClick(togglePanel, true)}>
        <LayoutOutlined className="expand-action-icon flipH" />
      </Button>
    </Tooltip>,
    <section key="rotate-group-key" className="tool-button-group-tight">
      <Tooltip placement="bottom" title={i18n('toolbar.rotateClockwise')}>
        <Button key="clockwise-button" className="toolbar-button" id={clockwiseID} onClick={() => handleButtonClick(handleRotateClockwise)}>
          <RotateRightOutlined className="expand-action-icon" />
        </Button>
      </Tooltip>
      <Tooltip placement="bottom" title={i18n('toolbar.rotateCounterClockwise')}>
        <Button key="counter-clockwise-button" className="toolbar-button" id={counterClockwiseID} onClick={() => handleButtonClick(handleRotateCounterClockwise)}>
          <RotateLeftOutlined className="expand-action-icon" />
        </Button>
      </Tooltip>
    </section>,
    <section key="zoom-group-key" className="tool-button-group-tight">
      <Tooltip placement="bottom" title={i18n('toolbar.zoomIn')}>
        <Button key="zoom-in-button" className="toolbar-button" id={plusButtonID} onClick={() => handleButtonClick(handleZoomIn)}>
          <PlusCircleOutlined className="expand-action-icon" />
        </Button>
      </Tooltip>
      <Tooltip placement="bottom" title={i18n('toolbar.zoomOut')}>
        <Button key="zoom-out-button" className="toolbar-button" id={minusButtonID} onClick={() => handleButtonClick(handleZoomOut)}>
          <MinusCircleOutlined className="expand-action-icon" />
        </Button>
      </Tooltip>
    </section>,
    <Tooltip key="set-zoom-tip-key" placement="bottom" title={i18n('toolbar.setZoom')}>
      <button key="zoom-overlay-button" type="button" className="toolbar-button" id={zoomOverlayButtonID} onClick={() => handleButtonClick(toggleZoomOverlay, true)}>
        <span className="zoom-overlay-span">
          {`${currentZoom}%`}
          <DownOutlined />
        </span>
      </button>
    </Tooltip>,
  ];

  const alignTools = [
    <section key="rotate-group-key" className="tool-button-group-tight">
      <Tooltip placement="bottom" title={i18n('toolbar.rotateClockwise')}>
        <Button key="clockwise-button" className="toolbar-button" id={clockwiseID} onClick={() => handleButtonClick(handleRotateClockwise)}>
          <RotateRightOutlined className="expand-action-icon" />
        </Button>
      </Tooltip>
      <Tooltip placement="bottom" title={i18n('toolbar.rotateCounterClockwise')}>
        <Button key="counter-clockwise-button" className="toolbar-button" id={counterClockwiseID} onClick={() => handleButtonClick(handleRotateCounterClockwise)}>
          <RotateLeftOutlined className="expand-action-icon" />
        </Button>
      </Tooltip>
    </section>,
  ];

  return (
    <>
      <section id="page-tool-block" key="page-tools-key" className="tool-block">
        <span className="tool-label">{i18n('toolbar.pageControlsLabel')}</span>
        {useBurger ? (
          <Menu onOpen={() => setBurger(true)} onClose={() => setBurger(false)} isOpen={burger} outerContainerId="page-tool-block" right>
            <section key="burger-tools-key" className="tool-button-group burger-tools">
              {compareMode === 'Align' ? alignTools : tools}
            </section>
          </Menu>
        ) : (
          <section key="tool-button-group-key" className="tool-button-group">
            {compareMode === 'Align' ? alignTools : tools}
          </section>
        )}
      </section>
      {!useBurger && !compareMode && <PageSelector key="page-key" />}
    </>
  );
};
