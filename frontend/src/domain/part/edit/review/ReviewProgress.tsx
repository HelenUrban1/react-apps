import React from 'react';
import { Progress, Tooltip } from 'antd';

interface Props {
  total: number;
  verified: number;
}

const ReviewProgress: React.FC<Props> = ({ total, verified }) => {
  return (
    <section id="reviewHeader" className="feature-panel-header">
      <Tooltip title={`${verified}/${total} Features Verified`}>
        <Progress data-cy="review-progress" percent={(verified / total) * 100} showInfo={false} />
      </Tooltip>
    </section>
  );
};

export default ReviewProgress;
