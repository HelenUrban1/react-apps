/* eslint-disable complexity */
import React, { useCallback, useEffect, useState, useRef } from 'react';
import { reorderController } from 'domain/part/edit/characteristics/utils/Renumber';
import { Characteristic } from 'types/characteristics';
import { Button, Checkbox, message, Popconfirm, Tooltip } from 'antd';
import { RightCircleOutlined, WarningOutlined } from '@ant-design/icons';
import { BrandColors } from 'view/global/defaults';
import { i18n } from 'i18n';
import { useMutation } from '@apollo/client';
import { debounce } from 'lodash';
import Analytics from 'modules/shared/analytics/analytics';
import { ListItems, ListItemCreate } from 'domain/setting/listsApi';
import { ListObject, ListTypeEnum } from 'domain/setting/listsTypes';
import { updateLists } from 'utils/Lists';
import { CaptureDimensions, ParsedCharacteristic } from 'utils/ParseTextController/types';
import { ParseTextController } from 'utils/ParseTextController/ParseTextController';
import log from 'modules/shared/logger';
import Spinner from 'view/shared/Spinner';
import { useDispatch, useSelector } from 'react-redux';
import { AppState } from 'modules/state';
import { CharacteristicActions, loadCharacteristicRevisionThunk } from 'modules/characteristic/characteristicActions';
import { getUnframedText, isFramed } from 'utils/textOperations';
import { SpecialCharactersModal } from 'styleguide/Modals/SpecialCharactersModal';
import { SessionActions } from 'modules/session/sessionActions';
import { changeDrawingThunk } from 'modules/partEditor/partEditorActions';
import { RevisionBanner } from 'styleguide/Revision/RevisionBanner';
import ReviewProgress from './ReviewProgress';
import ReviewNavigation from './ReviewNavigation';
import DimensionForm from './forms/DimensionForm';
import GdtForm from './forms/GdtForm';
import NoteForm from './forms/NoteForm';
import InspectionForm from './forms/InspectionForm';
import InfoForm from './forms/InfoForm';
import './Review.less';
import { updatePartThunk } from 'modules/part/partActions';

interface ReviewDrawerProps {
  revision?: boolean;
}

const ReviewDrawer = ({ revision }: ReviewDrawerProps) => {
  // Prepare to access the collection of characteristics
  const { characteristics, selected, index, count, verified, parsed: clean, compareRevision, compareIndex } = useSelector((state: AppState) => state.characteristics);
  const dispatch = useDispatch();
  // item is the characteristic currently in focus
  const [item, setItem] = useState(revision ? compareRevision : characteristics[index || 0]);
  const { types, metricUnits, imperialUnits, sessionLoaded } = useSelector((state: AppState) => state.session);
  // Retrieve the default tolerances setup earlier in the partEditor wizard
  const { part } = useSelector((state: AppState) => state.part);
  const { partDrawing, tolerances, gridRenderer, assignedStyles, compareDrawing, compareMode } = useSelector((state: AppState) => state.partEditor);
  const { tronInstance } = useSelector((state: AppState) => state.pdfView);

  const hasRevision = item && (item.previousRevision || item.nextRevision);

  // State to keep track of the insert symbols modal
  const [symbolModal, setSymbolModal] = useState<{ visible: boolean; feature: Characteristic | null; cursor: number; field: string; value: string | undefined }>({ visible: false, feature: null, cursor: 0, field: '', value: undefined });

  const [animating, setVerifyAnimation] = useState('');
  const [showConfirm, setShowConfirm] = useState(false);

  const oldFeature = useRef<Characteristic | null>();

  useEffect(() => {
    if (animating !== '') {
      setTimeout(() => {
        setVerifyAnimation('');
      }, 1000);
    }
  }, [animating]);

  useEffect(() => {
    if (index >= 0 && characteristics[index]) {
      setItem(characteristics[index]);
      if (item?.id !== characteristics[index].id) {
        if (partDrawing?.id !== characteristics[index].drawing.id) {
          dispatch(changeDrawingThunk(characteristics[index].drawing.id));
        }
      }
    } else if (characteristics[0]) {
      setItem(characteristics[0]);
    } else {
      log.warn('No feature available to set');
    }
  }, [index, characteristics]);

  useEffect(() => {
    if (revision) {
      setItem(compareRevision);
    }
  }, [compareRevision]);

  const [createListItem] = useMutation<ListItemCreate>(ListItems.mutate.create, {
    onCompleted: ({ listItemCreate }) => {
      return listItemCreate;
    },
    refetchQueries: [
      {
        query: ListItems.query.list,
        variables: { status: 'Active', deletedAtRange: undefined },
      },
    ],
  });

  const getInputFieldName = (list: string) => {
    switch (list) {
      case 'Classification':
        return 'criticality';
      case 'Operation':
        return 'operation';
      case 'Inspection_Method':
        return 'inspectionMethod';
      case 'Type':
        return 'notationType';
      default:
        return '';
    }
  };

  const scrollCaptureIntoView = (char: Characteristic) => {
    const viewer = tronInstance?.Core.documentViewer;
    if (!viewer || !char) {
      return;
    }

    if (char.drawing.id !== partDrawing?.id) {
      dispatch(changeDrawingThunk(char.drawing.id));
    } else {
      viewer.trigger('scrollTo', {
        x: char.boxLocationX,
        y: char.boxLocationY,
        pageNumber: char.drawingSheetIndex,
      });
    }
  };

  const styles = assignedStyles || {
    default: {
      style: 'f12c5566-7612-48e9-9534-ef90b276ebaa',
      assign: ['Default'],
    },
  };

  const reparse = async (val: string) => {
    if (!types || !item) return undefined;
    // Re-parse feature
    const capture: CaptureDimensions = {
      left: item.boxLocationX,
      top: item.boxLocationY,
      width: item.boxWidth,
      height: item.boxHeight,
    };
    const parsed = await ParseTextController.parseFeatureFromFullSpec({
      assignedStyles: styles,
      boxRotation: item.boxRotation,
      capture,
      grid: gridRenderer.grid,
      oldFeature: item,
      fullSpecification: getUnframedText(val),
      tolerances,
      types,
    });

    return parsed?.feature;
  };

  const animateReview = (newItem: Characteristic | null, checked: boolean) => {
    if (!newItem) return;
    setItem(newItem); // start toggle animation
    let newIndex = index;
    if (checked && index + 1 !== count) {
      const drawer = document.getElementById('review-drawer');
      if (drawer) {
        drawer.scrollTop = 0;
      }
      newIndex += 1;
    }

    if (index !== newIndex) {
      dispatch({ type: CharacteristicActions.UPDATE_SELECTED, payload: { index: newIndex, selected: [characteristics[newIndex]] } });
    }

    setTimeout(() => {
      if (checked && index + 1 !== count) {
        setVerifyAnimation('forward');
      }
      scrollCaptureIntoView(characteristics[newIndex]);
    }, 250); // delay index and context update so the toggle can animate
  };

  const verify = async (checked: boolean, parse = false) => {
    if (!item) return;
    let newItem = { ...item, verified: checked };
    if (parse) {
      const feature = await reparse(item.fullSpecification);
      if (feature?.notationSubtype !== newItem.notationSubtype || (feature?.upperSpecLimit && feature?.upperSpecLimit !== newItem.upperSpecLimit) || (feature?.lowerSpecLimit && feature?.lowerSpecLimit !== newItem.lowerSpecLimit)) {
        oldFeature.current = newItem;
        newItem = { ...newItem, ...feature, verified: false };
        setShowConfirm(true);
      } else {
        newItem = { ...newItem, ...feature, verified: checked };
      }
    }

    if (checked && newItem.verified) animateReview(newItem, checked);

    Analytics.track({
      event: Analytics.events.featureMarkedVerified,
      part,
      drawing: partDrawing,
      characteristic: item,
    });

    dispatch({
      type: CharacteristicActions.EDIT_CHARACTERISTICS,
      payload: {
        updates: [{ ...newItem, captureError: '' }],
        verify: true,
      },
    });
    dispatch({ type: CharacteristicActions.PARSED, payload: true });
  };

  const handleRejectChanges = () => {
    updateCharacteristic([{ ...oldFeature.current, verified: true }]);
    oldFeature.current = null;
    setShowConfirm(false);
    animateReview(item, true);
  };

  const handleAcceptChanges = () => {
    updateCharacteristic([{ ...item, verified: true }]);

    oldFeature.current = null;
    setShowConfirm(false);
    animateReview(item, true);
  };

  const forward = () => {
    if (!clean) return;
    setVerifyAnimation('forward');
    scrollCaptureIntoView(characteristics[index + 1]);
    dispatch({ type: CharacteristicActions.UPDATE_SELECTED, payload: { index: index + 1, selected: [characteristics[index + 1]] } });
  };

  const backward = () => {
    if (!clean) return;
    setVerifyAnimation('backward');
    scrollCaptureIntoView(characteristics[index - 1]);
    dispatch({ type: CharacteristicActions.UPDATE_SELECTED, payload: { index: index - 1, selected: [characteristics[index - 1]] } });
  };

  const update = async (e: any, parse = true) => {
    if (!types) {
      log.error('Type settings must be loaded to update a feature - or item is missing - or this is the revision panel');
      return;
    }
    if (!item) {
      log.error('No item loaded to edit');
      return;
    }
    if (revision) {
      log.error('Cannot edit item while viewing old revision');
      return;
    }

    if (!selected[0] && characteristics[index]) {
      // reselect feature, may have done an extraction and then started editing while waiting
      dispatch({ type: CharacteristicActions.UPDATE_SELECTED, payload: { selected: [characteristics[index]] } });
    }

    if (!e.target) {
      // Changed type cascader
      const [newType, newSubType] = e;
      const change = { notationType: newType.toString(), notationSubtype: newSubType.toString() };
      if (change.notationType !== item.notationType || change.notationSubtype !== item.notationSubtype) {
        const units = part && part.measurement === 'Imperial' ? imperialUnits || {} : metricUnits || {};

        const parsed = await ParseTextController.updateChangedFeature({ assignedStyles: styles, change, oldFeature: item, tolerances, types, units });
        if (!parsed) {
          return;
        }
        const { feature } = parsed;
        updateCharacteristic([{ ...item, ...feature, captureError: '' }]);
      }
    } else if (e.target.name === 'markerGroup') {
      const { updates } = reorderController(e.target.value, characteristics, [item]);
      if (updates && updates.length > 0) {
        updateCharacteristic(updates);
      }
    } else if (e.target.name === 'fullSpecification') {
      if (parse && item.captureMethod !== 'Custom') {
        const feature = await reparse(e.target.value);
        dispatch({ type: CharacteristicActions.PARSED, payload: true });
        if (!feature) return;
        updateCharacteristic([{ ...item, ...feature }]);
      } else {
        updateCharacteristic([{ ...item, fullSpecification: getUnframedText(e.target.value) }]);
        dispatch({ type: CharacteristicActions.PARSED, payload: false });
      }
    } else if (e.target.value === 'Document_Defined') {
      if (e.target.value !== item.toleranceSource) {
        // Re-parse feature
        const parsed = await ParseTextController.parseNewTolerancesFromFullSpec({
          oldFeature: item,
          fullSpecification: getUnframedText(item.fullSpecification),
          tolerances,
          types,
        });
        if (!parsed) return;
        const { feature } = parsed;
        updateCharacteristic([{ ...item, ...feature }]);
        if (feature.toleranceSource !== 'Document_Defined') {
          message.warning('No Tolerances Defined in Full Specification');
        }
      }
    } else if (e.target.value === 'Tolerance') {
      // Re-parse feature
      const capture: CaptureDimensions = {
        left: item.boxLocationX,
        top: item.boxLocationY,
        width: item.boxWidth,
        height: item.boxHeight,
      };
      const newSpec = isFramed(item.fullSpecification) ? getUnframedText(item.fullSpecification) : item.fullSpecification;
      const parsed = await ParseTextController.parseFeatureFromFullSpec({
        assignedStyles: styles,
        boxRotation: item.boxRotation,
        capture,
        grid: gridRenderer.grid,
        oldFeature: { ...item, fullSpecification: newSpec },
        fullSpecification: newSpec.replaceAll(/\||\(|\)/g, ''), // replace pipes and parenthesis in case change is from Basic/Ref to Dim
        tolerances,
        types,
      });
      if (!parsed) return;
      const { feature } = parsed;
      updateCharacteristic([{ ...item, ...feature }]);
    } else if (e.target.name === 'displayLeaderLine') {
      if (e.target.checked !== item.displayLeaderLine) {
        setItem({ ...item, displayLeaderLine: e.target.checked }); // start toggle animation
        updateCharacteristic([{ ...item, displayLeaderLine: e.target.checked }]);
      }
    } else {
      if (e.target.value === item[e.target.name]) {
        // No change
        return;
      }
      const change: ParsedCharacteristic = { [e.target.name]: e.target.value };
      if (e.target.name === 'quantity' && item.markerSubIndex !== -1 && item.markerSubIndex !== null) {
        // update quantity for groups
        const group = characteristics.filter((feature) => feature.markerGroup === item.markerGroup);
        const updates = await Promise.all(
          group.map(async (feature) => {
            const parsed = await ParseTextController.updateChangedFeature({ assignedStyles: styles, change, oldFeature: feature, tolerances, types });
            return { ...feature, ...parsed.feature };
          }),
        );
        if (!updates) {
          return;
        }
        updateCharacteristic(updates);
      } else {
        // Update fields
        const parsed = await ParseTextController.updateChangedFeature({ assignedStyles: styles, change, oldFeature: item, tolerances, types });
        if (!parsed) {
          return;
        }
        const { feature } = parsed;
        updateCharacteristic([{ ...item, ...feature }]);
      }
    }
  };

  const updateCharacteristic = (updates?: any) => {
    dispatch({
      type: CharacteristicActions.EDIT_CHARACTERISTICS,
      payload: {
        updates,
      },
    });

    if (part) dispatch(updatePartThunk({ updatedAt: new Date(), status: part.status, balloonedAt: new Date() }, part.id));
  };

  const debouncedUpdate = useCallback(
    debounce(
      (e: any, parse?: boolean) => {
        dispatch({ type: 'CHARACTERISTICS_LOADED', payload: false });
        update(e, parse);
      },
      500,
      { trailing: true },
    ),
    [item],
  );

  const onValueChange = (e: any, parse = true) => {
    e.persist();
    debouncedUpdate(e, parse);
  };

  const handleCreateListItem = (name: string, activeListItems: ListObject, activeList: ListTypeEnum, parentId?: string, metadata?: string) => {
    const itemIndex = Object.keys(activeListItems || {}).length;
    createListItem({
      variables: {
        data: ListItems.createGraphqlObject({
          name,
          default: false,
          status: 'Active',
          listType: activeList,
          itemIndex,
          parentId,
          metadata,
        }),
      },
    }).then((listItemCreate) => {
      if (!listItemCreate.data) return;
      dispatch({ type: SessionActions.UPDATE_LISTS, payload: updateLists(activeList, activeListItems, listItemCreate.data?.listItemCreate) });
      const fakeEvent =
        listItemCreate.data.listItemCreate.listType !== 'Type'
          ? {
              target: {
                value: listItemCreate.data.listItemCreate.id,
                name: getInputFieldName(listItemCreate.data.listItemCreate.listType),
              },
            }
          : [listItemCreate.data.listItemCreate.parentId, listItemCreate.data.listItemCreate.id];
      update(fakeEvent);
    });
  };

  const handleShowModal = (feature: Characteristic, field: string, cursor: number, value: string | undefined) => {
    Analytics.track({
      event: Analytics.events.specialCharactersModalOpened,
      characteristic: feature,
      properties: {
        'field.name': field,
        'field.value': feature.fullSpecification,
      },
    });
    setSymbolModal({ visible: true, feature, cursor, field, value });
  };

  const handleCloseModal = () => {
    setSymbolModal({ visible: false, feature: null, cursor: 0, field: '', value: undefined });
  };

  const handleViewRevision = () => {
    if (partDrawing && compareDrawing && item && item.originalCharacteristic && hasRevision) dispatch(loadCharacteristicRevisionThunk(item.id, item.originalCharacteristic, compareDrawing.id));
  };

  const handleCloseViewRevision = () => {
    dispatch({ type: CharacteristicActions.SET_REVISION, payload: { compareRevision: null, compareIndex: 0 } });
  };

  if (!characteristics || characteristics.length < 1 || (!sessionLoaded && !symbolModal.visible)) {
    return <Spinner />;
  }

  if (!item) return <></>;

  return (
    <div id="review-drawer" data-cy="review-drawer" className="review-container">
      {item && (
        <>
          <SpecialCharactersModal
            update={update}
            item={item}
            value={symbolModal.value || item[symbolModal.field]?.toString() || ''}
            cursorPos={symbolModal.cursor}
            field={symbolModal.field}
            label={i18n(`entities.feature.fields.${symbolModal.field}`)}
            framed={(symbolModal.feature?.notationClass === 'Basic' && symbolModal.field === 'fullSpecification') || isFramed(item[symbolModal.field]?.toString() || '')}
            show={symbolModal.visible}
            source="review-drawer"
            handleCloseModal={handleCloseModal}
          />
          {revision ? (
            <section className="rev-banner-container">
              <RevisionBanner previous={compareRevision?.nextRevision ? compareIndex : 0} type="feature" />
            </section>
          ) : (
            <ReviewProgress total={count || 0} verified={verified || 0} />
          )}
          {compareMode === 'Compare' && !revision && (
            <Tooltip title={!hasRevision ? 'Feature has no other version' : ''} placement="top">
              <Button disabled={!!compareRevision || !hasRevision} className="btn-secondary center" style={{ width: 'fit-content' }} onClick={handleViewRevision}>
                View other version
                <RightCircleOutlined />
              </Button>
            </Tooltip>
          )}
          <div id="reviewContent" className={`item-list-container review-forms ${animating ? 'transition-forms' : ''}`} style={{ paddingTop: '4px' }}>
            <div className={`animationContainer ${animating}`}>
              <Popconfirm
                title={
                  <div>
                    <div>{i18n('wizard.reviewCheck')}</div>
                    <div>{i18n('wizard.reviewApply')}</div>
                  </div>
                }
                visible={showConfirm}
                placement="rightTop"
                onCancel={handleRejectChanges}
                onConfirm={handleAcceptChanges}
                okText={i18n('wizard.acceptChanges')}
                cancelText={i18n('wizard.rejectChanges')}
                icon={<WarningOutlined style={{ color: BrandColors.Coral }} />}
              >
                <section data-cy="infoForm">
                  <InfoForm
                    characteristic={item}
                    type={types && types[item.notationType] ? types[item.notationType].value : null}
                    verify={verify}
                    update={update}
                    debouncedUpdate={onValueChange}
                    handleShowModal={handleShowModal}
                    handleCreateListItem={handleCreateListItem}
                    isRevision={revision}
                  />
                </section>
              </Popconfirm>
              <section data-cy="toleranceForm">
                {types && types[item.notationType] && types[item.notationType].value === 'Dimension' && (
                  <DimensionForm
                    disabled={!!(item.toleranceSource === 'Document_Defined' || item.notationClass === 'Basic' || item.notationClass === 'Reference')}
                    characteristic={item}
                    update={update}
                    debouncedUpdate={onValueChange}
                    isRevision={revision}
                  />
                )}
                {types && types[item.notationType] && types[item.notationType].value === 'Geometric Tolerance' && (
                  <GdtForm characteristic={item} update={update} debouncedUpdate={onValueChange} handleShowModal={handleShowModal} isRevision={revision} />
                )}
                {types && types[item.notationType] && types[item.notationType].value !== 'Geometric Tolerance' && types[item.notationType].value !== 'Dimension' && (
                  <NoteForm characteristic={item} update={update} debouncedUpdate={onValueChange} handleShowModal={handleShowModal} isRevision={revision} />
                )}
              </section>
              <section>
                <Checkbox name="displayLeaderLine" checked={item.displayLeaderLine} onChange={update} disabled={revision}>
                  {i18n('wizard.displayLeaderLine')}
                </Checkbox>
              </section>
              <section data-cy="inspectionForm">
                <InspectionForm characteristic={item} update={update} debouncedUpdate={onValueChange} handleShowModal={handleShowModal} handleCreateListItem={handleCreateListItem} isRevision={revision} />
              </section>
            </div>
          </div>
          {!revision && <ReviewNavigation index={index + 1} total={count || 0} forward={forward} backward={backward} />}
        </>
      )}
    </div>
  );
};

export default ReviewDrawer;
