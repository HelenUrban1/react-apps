import { Characteristic, CharacteristicsInformation } from 'types/characteristics';

export const createNewCharacteristicContext = (
  oldContext: CharacteristicsInformation,
  change: {
    characteristic?: { [key: string]: any };
    index?: 'forward' | 'backward';
    selected?: boolean;
  },
): CharacteristicsInformation => {
  let list = oldContext.characteristics;
  let { index } = oldContext;
  let { selected } = oldContext;
  let { verified } = oldContext;
  if (change.characteristic) {
    const oldChar = oldContext.characteristics[oldContext.index];
    const newChar: Characteristic = { ...oldChar, ...change.characteristic };
    let countVerified = 0;
    list = oldContext.characteristics.map((char) => {
      if (char.id === newChar.id) {
        if (newChar.verified) {
          countVerified += 1;
        }
        return newChar;
      }
      if (char.verified) {
        countVerified += 1;
      }
      return char;
    });
    verified = countVerified;
  }
  if (change.index) {
    if (change.index === 'forward') {
      index += 1;
    } else {
      index -= 1;
    }
  }
  if (change.selected) {
    const newSelected = oldContext.selected;
    if (!newSelected.some((char) => char.id === oldContext.characteristics[index].id)) newSelected.push(oldContext.characteristics[index]);
    // selected = newSelected;
    selected = [oldContext.characteristics[index]];
  }
  return { ...oldContext, characteristics: list, index, selected, verified };
};
