import { Part } from 'domain/part/partTypes';
import { ListObject } from 'domain/setting/listsTypes';
import { Characteristic, CharacteristicNotationTypeEnum } from 'types/characteristics';
import { Tolerance } from 'types/tolerances';
import { getListIdByValue } from 'utils/Lists';
import { interpretNotationText } from 'utils/ParseNotation';
import { sanitizeText, toEnumFormat } from 'utils/textOperations';
import { classifyCharacteristic, getUnit } from '../characteristics/utils/extractUtil';

interface DimensionUpdateData {
  feature: Characteristic;
  fullSpec?: string;
  nominal?: string;
  quantity?: number;
  upperSpec?: string;
  lowerSpec?: string;
  plus?: string;
  minus?: string;
}

export const updateDimension = ({ feature, fullSpec, nominal, quantity, upperSpec, lowerSpec, plus, minus }: DimensionUpdateData) => {
  const updatedDimension = { ...feature };
  updatedDimension.fullSpec = fullSpec || updatedDimension.fullSpec;
  updatedDimension.nominal = nominal || updatedDimension.nominal;
  updatedDimension.quantity = quantity || updatedDimension.quantity;
  updatedDimension.upperSpecLimit = upperSpec || updatedDimension.upperSpecLimit;
  updatedDimension.lowerSpecLimit = lowerSpec || updatedDimension.lowerSpecLimit;
  updatedDimension.plusTol = plus || updatedDimension.plusTol;
  updatedDimension.minusTol = minus || updatedDimension.minusTol;
  updatedDimension.verified = false;
  return updatedDimension;
};

interface GDTUpdateData {
  feature: Characteristic;
  fullSpec?: string;
  primaryTol?: string;
  secondaryTol?: string;
  primaryDatum?: string;
  secondaryDatum?: string;
  tertiaryDatum?: string;
}

export const updateGDT = ({ feature, fullSpec, primaryTol, secondaryTol, primaryDatum, secondaryDatum, tertiaryDatum }: GDTUpdateData) => {
  const updatedGDT = { ...feature };
  updatedGDT.fullSpec = fullSpec || updatedGDT.fullSpec;
  updatedGDT.gdtPrimaryToleranceZone = primaryTol || updatedGDT.gdtPrimaryToleranceZone;
  updatedGDT.gdtSecondaryToleranceZone = secondaryTol || updatedGDT.gdtSecondaryToleranceZone;
  updatedGDT.gdtPrimaryDatum = primaryDatum || updatedGDT.gdtPrimaryDatum;
  updatedGDT.gdtSecondaryDatum = secondaryDatum || updatedGDT.gdtSecondaryDatum;
  updatedGDT.gdtTertiaryDatum = tertiaryDatum || updatedGDT.gdtTertiaryDatum;
  updatedGDT.verified = false;
  return updatedGDT;
};

export const reParseFeature = async (feature: Characteristic, tolerances: Tolerance, types: ListObject, part: Part) => {
  const sanitizedFullSpec = sanitizeText(feature.fullSpecification);
  const interpreted = await interpretNotationText(sanitizedFullSpec);
  const { classified, type, subType, notationClass } = classifyCharacteristic(interpreted, tolerances);
  const typeId = getListIdByValue({
    defaultValue: 'fa3284ed-ad3e-43c9-b051-87bd2e95e927',
    list: types,
    value: type,
  });
  const subtypeId = getListIdByValue({
    defaultValue: '805fd539-8063-4158-8361-509e10a5d371',
    list: types ? types[typeId]?.children : {},
    value: subType,
  });
  let returnItem = { ...feature };
  returnItem.notationType = typeId;
  returnItem.notationSubtype = subtypeId;
  returnItem.notationClass = notationClass;
  returnItem.verified = false;

  // get Type name from UUID
  const itemType = type;

  if (itemType === 'Note') {
    // need to check properties, but still want empty strings to pass
    if (interpreted?.parsed?.input !== null) {
      returnItem.fullSpecification = interpreted.parsed.input.trim();
      returnItem.nominal = interpreted.parsed.input.trim();
    } else {
      returnItem.fullSpecification = interpreted;
      returnItem.nominal = interpreted;
    }
    returnItem.unit = '';
  } else {
    // All items besides Notes
    returnItem.fullSpecification = classified.FullSpecification;
    returnItem.nominal = classified.Nominal;
    returnItem.unit = getUnit(toEnumFormat(classified.SubType) as CharacteristicNotationTypeEnum, part);
  }

  // If the item is a GD&T
  if (itemType === 'Geometric Tolerance') {
    returnItem = updateGDT({
      feature: returnItem,
      fullSpec: classified.FullSpecification,
      primaryTol: classified.Gdt?.GdtTol,
      secondaryTol: classified.Gdt?.GdtTolZone2,
      primaryDatum: classified.Gdt?.GdtPrimaryDatum,
      secondaryDatum: classified.Gdt?.GdtSecondDatum,
      tertiaryDatum: classified.Gdt?.GdtTertiaryDatum,
    });
  }

  // All items INCLUDING Notes
  // These are required fields on the CharacteristicInput type

  returnItem = updateDimension({
    feature: returnItem,
    quantity: classified.Quantity,
    upperSpec: classified.UpperSpecLimit,
    lowerSpec: classified.LowerSpecLimit,
    plus: classified.PlusTol,
    minus: classified.MinusTol,
  });

  return returnItem;
};
