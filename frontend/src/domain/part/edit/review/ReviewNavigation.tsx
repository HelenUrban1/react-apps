import React from 'react';
import IconButton from 'styleguide/Buttons/IconButton';
import { LeftCircleOutlined, RightCircleOutlined } from '@ant-design/icons';
import { useSelector } from 'react-redux';
import { AppState } from 'modules/state';

interface Props {
  index: number;
  total: number;
  forward: () => void;
  backward: () => void;
}

const ReviewNavigation: React.FC<Props> = ({ index, total, forward, backward }) => {
  const { loaded } = useSelector((state: AppState) => state.characteristics);

  return (
    <div id="reviewFooter" className="drawer-footer">
      <IconButton id="review-back" icon={<LeftCircleOutlined />} onClick={backward} disabled={index === 1 || !loaded} />
      <span data-cy="feature-indicator">
        <span data-cy="feature-current">{index}</span>
        {` / `}
        <span data-cy="feature-total">{total}</span>
      </span>
      <IconButton id="review-next" icon={<RightCircleOutlined />} onClick={forward} disabled={index === total || !loaded} />
    </div>
  );
};

export default ReviewNavigation;
