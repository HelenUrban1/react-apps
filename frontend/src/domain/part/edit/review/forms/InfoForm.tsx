import React, { useState } from 'react';
import { InputType, Section } from 'view/global/inputs';
import { convertListObjectToCascaderOptions } from 'utils/ConvertCascader';
import { BaseOptionType } from 'antd/lib/cascader';
import { VerifiedStatus } from 'styleguide/VerifiedStatus/VerifiedStatus';
import { validateNumber } from 'utils/DataCalculation';
import { i18n } from 'i18n';
import log from 'modules/shared/logger';
import { isFramed, sanitizedTextComparison } from 'utils/textOperations';
import { SymbolButton } from 'styleguide/Buttons/SymbolButton';
import { Characteristic } from 'types/characteristics';
import { ListObject, ListTypeEnum } from 'domain/setting/listsTypes';
import Permissions from 'security/permissions';
import PermissionChecker from 'modules/auth/permissionChecker';
import { getCurrentList, getMeasurementsSelectOptions } from 'utils/Lists';
import { ListTypeModal } from 'domain/setting/configurations/ListTypeModal';
import { Tooltip } from 'antd';
import { useSelector } from 'react-redux';
import { AppState } from 'modules/state';
import './forms.less';

interface Props {
  characteristic: Characteristic;
  type: string | null;
  update: (e: any, parse?: boolean) => void;
  debouncedUpdate: (e: any, parse?: boolean) => void;
  handleCreateListItem: (name: string, activeListItems: ListObject, activeList: ListTypeEnum, parentId?: string, metadata?: string) => void;
  verify: (checked: boolean) => void;
  handleShowModal: (feature: Characteristic, field: string, cursor: number, value: string | undefined) => void;
  isRevision?: boolean;
}

const InfoForm: React.FC<Props> = ({ characteristic, type, update, debouncedUpdate, handleCreateListItem, verify, handleShowModal, isRevision }) => {
  const { part } = useSelector((state: AppState) => state.part);
  const { partDrawing } = useSelector((state: AppState) => state.partEditor);
  const { index } = useSelector((state: AppState) => state.pdfView);
  const { currentUser: user } = useSelector((state: AppState) => state.auth);
  const { characteristics } = useSelector((state: AppState) => state.characteristics);
  const { types, classifications, methods, operations, metricUnits, imperialUnits } = useSelector((state: AppState) => state.session);
  const permissionValidator = new PermissionChecker(user);
  const hasListEditAccess = permissionValidator.match(Permissions.values.accountManagement);
  const [activeList, setActiveList] = useState<ListTypeEnum>('Classification');
  const [activeListItems, setActiveListItems] = useState<ListObject | null>(null);
  const [displayModal, setDisplayModal] = useState<boolean>(false);
  const isDisabled = characteristic.verified || isRevision;
  const charDrawing = part?.drawings?.find((draw) => draw.id === characteristic.drawing.id) || part?.primaryDrawing;
  const useGrid = JSON.parse(charDrawing?.gridOptions || '{}').status !== false && (partDrawing?.sheets.find((sheet) => sheet.id === characteristic.drawingSheet.id) || partDrawing?.sheets[index])?.useGrid;
  const getNumberOptions = () => {
    const lastItem = characteristics[characteristics.length - 1];
    const lastGroup = lastItem.markerGroup ? parseInt(lastItem.markerGroup, 10) : parseInt(lastItem.markerLabel, 10);

    const options = [];
    for (let i = 1; i < lastGroup + 1; i++) {
      options.push({
        name: i.toString(),
        label: i.toString(),
        value: i.toString(),
      });
    }
    return options;
  };

  const getStatusComponent = () => {
    return <VerifiedStatus verify={verify} verified={characteristic.verified} isRevision={isRevision || false} />;
  };

  // Creates the Cascader options from the Types list
  const getTypesCascader = () => {
    if (!types) return [];
    const options: BaseOptionType[] = [];
    options.push(convertListObjectToCascaderOptions(types, i18n('entities.feature.fields.type'), []));

    // skip over the top level Type since its the only option
    return options[0].children;
  };

  // Gets the Measurement type of the Type > Subtype for the selected characteristic
  const getMeasurementType = () => {
    let returnType = '';
    if (!types) return returnType;
    const typeKeys = Object.keys(types);
    typeKeys.forEach((t) => {
      const sameType = sanitizedTextComparison(types[t].id, characteristic.notationType);
      if (sameType && types[t].children) {
        Object.values(types[t].children!)?.forEach((subtype) => {
          const sameSubtype = sanitizedTextComparison(subtype.id, characteristic.notationSubtype);
          if (sameSubtype && subtype.meta?.measurement) {
            returnType = subtype.meta?.measurement;
          }
        });
      }
    });
    return returnType;
  };

  // Gets the list of units applicable to the characteristic based on measurement type and measurement system
  const getUnits = () => {
    if (!part) {
      log.warn('No Part Available.');
      return [];
    }
    const measurementType = getMeasurementType();
    const unitsList = sanitizedTextComparison(part.measurement, 'Metric') ? metricUnits : imperialUnits;
    if (!unitsList) {
      log.warn('No Units Available.');
      return [];
    }
    const list: { name: string; label: string; value: string }[] = [
      {
        name: `${part.measurement}_none`,
        value: '',
        label: i18n(`common.none`),
      },
    ];
    const usedKeys: string[] = [];
    const unitKeys = Object.keys(unitsList);
    unitKeys.forEach((unit) => {
      const metaData = unitsList[unit].meta;
      if (metaData && sanitizedTextComparison(metaData.measurement, measurementType)) {
        usedKeys.push(unitsList[unit].id);
        list.push({
          name: metaData.unit || unitsList[unit].value,
          label: metaData.unit || unitsList[unit].value,
          value: unitsList[unit].id,
        });
      }
    });
    if (measurementType !== '' && !usedKeys.includes(characteristic.unit)) {
      log.warn('Unit List Does Not Match Characteristic Measurement Type. Trying Other Unit List.');
      const otherUnits = sanitizedTextComparison(part.measurement, 'Metric') ? imperialUnits : metricUnits;
      if (otherUnits) {
        const otherKeys = Object.keys(otherUnits || {});
        otherKeys.forEach((unit) => {
          const metaData = otherUnits[unit].meta;
          if (metaData && sanitizedTextComparison(metaData.measurement, measurementType)) {
            usedKeys.push(otherUnits[unit].id);
            list.push({
              name: metaData.unit || otherUnits[unit].value,
              label: metaData.unit || otherUnits[unit].value,
              value: otherUnits[unit].id,
            });
          }
        });
      }
    }
    if (!usedKeys.includes(characteristic.unit)) {
      log.warn('Unit for Characteristic Not Found.');
      list.push({
        name: 'unknown',
        label: i18n('common.unknown'),
        value: characteristic.unit,
      });
    }
    return list;
  };

  const showModal = (field: string, cursorPos: number) => {
    const value = document.getElementsByName(field) ? (document.getElementsByName(field)[0] as HTMLInputElement).value : characteristic[field];
    handleShowModal(characteristic, field, cursorPos, value?.toString());
  };

  const getSections = (): InputType[] => {
    const sects: InputType[] = [
      {
        name: 'infoGroup',
        value: '',
        type: 'group',
        group: [
          {
            name: 'markerGroup',
            label: 'Feature #',
            classes: 'group-flex-1',
            value: characteristic.markerLabel,
            type: 'select',
            disabled: isDisabled,
            group: getNumberOptions(),
            change: update,
          },
          {
            name: 'verified',
            classes: 'group-flex-3 group-flex-end',
            value: '',
            type: 'component',
            component: getStatusComponent(),
          },
        ],
      },
      {
        name: 'notationType',
        label: i18n('entities.feature.fields.notationType'),
        value: '',
        classes: 'group-flex-1',
        cascaderValue: [characteristic.notationType, characteristic.notationSubtype],
        type: 'cascader',
        disabled: isDisabled,
        group: [],
        cascaderOptions: getTypesCascader(),
        list: types,
        change: update,
        add: hasListEditAccess
          ? () => {
              setActiveList('Type');
              setActiveListItems(getCurrentList('Type', classifications, methods, types, operations, imperialUnits, metricUnits));
              setDisplayModal(true);
            }
          : undefined,
      },
      {
        name: 'measurementGroup',
        value: '',
        type: '',
        group: [
          {
            name: 'quantity',
            label: i18n('entities.feature.fields.quantity'),
            classes: 'group-flex-3',
            errorConditional: characteristic.quantity ? !validateNumber(characteristic.quantity.toString()) : false,
            errorText: 'Not a valid number',
            value: characteristic.quantity ? characteristic.quantity.toString() : '1',
            type: 'number',
            disabled: isDisabled,
            blur: debouncedUpdate,
            change: update,
            limitTo: 'number',
          },
          {
            name: 'unit',
            label: i18n('entities.feature.fields.unit'),
            classes: 'group-flex-3',
            value: characteristic.unit,
            type: 'select',
            disabled: isDisabled,
            group: getUnits(),
            change: update,
            doNotFormat: true,
          },
        ],
      },
      {
        name: 'gridCoordinateGroup',
        value: '',
        type: '',
        group: [
          {
            name: 'gridLocation',
            label: i18n('wizard.gridLocation'),
            classes: 'group-flex-3',
            value: useGrid ? characteristic.gridCoordinates : '-',
            type: 'text',
            disabled: true,
            tooltip: `${characteristic?.drawing?.name}, Page ${characteristic.drawingSheetIndex}${useGrid ? `, ${characteristic.gridCoordinates}` : ''}`,
          },
          {
            name: 'balloonGridLocation',
            label: i18n('wizard.balloonGridLocation'),
            classes: 'group-flex-3',
            value: useGrid ? characteristic.balloonGridCoordinates : '-',
            type: 'text',
            disabled: true,
            tooltip: `${characteristic.drawing.name}, Page ${characteristic.drawingSheetIndex}${useGrid ? `, ${characteristic.balloonGridCoordinates}` : ''}`,
          },
        ],
      },
    ];
    if (characteristic.connectionPointIsFloating) {
      sects.push({
        name: 'connectionPointGridLocation',
        label: i18n('wizard.connectionPointGridLocation'),
        classes: 'group-flex-3',
        value: useGrid ? characteristic.connectionPointGridCoordinates : '-',
        type: 'text',
        disabled: true,
        tooltip: `${characteristic.drawing.name}, Page ${characteristic.drawingSheetIndex}${useGrid ? `, ${characteristic.connectionPointGridCoordinates}` : ''}`,
      });
    }
    if (type === 'Dimension' || type === 'Geometric Tolerance') {
      sects.push({
        name: 'fullSpecification',
        label: i18n('entities.feature.fields.fullSpecification'),
        value: characteristic.fullSpecification,
        framed: characteristic.notationClass === 'Basic' || isFramed(characteristic.fullSpecification),
        type: 'text',
        change: (e: any) => debouncedUpdate(e, false),
        blur: update,
        disabled: isDisabled || type === 'Geometric Tolerance',
        tooltip: type === 'Geometric Tolerance' ? i18n('entities.part.instructions.gdt') : undefined,
        modal: <SymbolButton showModal={showModal} field="fullSpecification" />,
      });
    }

    return sects;
  };

  const createListItem = (name: string, listType: ListTypeEnum, parentId: string | undefined, metadata?: string | undefined) => {
    if (!types) return;
    handleCreateListItem(name, types, 'Type', parentId, metadata);
  };

  // const [section, setSection] = useState<InputType[]>(getSections());

  // useEffect(() => {
  //   setSection(getSections());
  // }, [characteristic]);

  if (!part) {
    return <div>No Part</div>;
  }

  return (
    <>
      <ListTypeModal
        modalTitle={`Add ${activeList.replace('_', ' ')}`}
        defaultType={undefined}
        listItems={activeListItems}
        handleCreateListItem={createListItem}
        visible={displayModal}
        setDisplayModal={setDisplayModal}
        measurementOptions={getMeasurementsSelectOptions(metricUnits, imperialUnits)}
      />

      {isDisabled && (
        <Tooltip title={isRevision ? i18n('entities.feature.reviewStatus.lockedRevision') : i18n('entities.feature.reviewStatus.locked')}>
          <form data-cy="infoForm">
            <Section id={0} sections={getSections()} />
          </form>
        </Tooltip>
      )}
      {!isDisabled && (
        <form data-cy="infoForm">
          <Section id={0} sections={getSections()} />
        </form>
      )}
    </>
  );
};

export default InfoForm;
