import React from 'react';
import { Section } from 'view/global/inputs';
import { Characteristic } from 'types/characteristics';
import { i18n } from 'i18n';
import { SymbolButton } from 'styleguide/Buttons/SymbolButton';
import { Tooltip } from 'antd';

interface Props {
  characteristic: Characteristic;
  update: (e: any, parse?: boolean) => void;
  debouncedUpdate: (e: any, parse?: boolean) => void;
  handleShowModal: (feature: Characteristic, field: string, cursor: number, value: string | undefined) => void;
  isRevision?: boolean;
}

const NoteForm: React.FC<Props> = ({ characteristic, update, debouncedUpdate, handleShowModal, isRevision }) => {
  const isDisabled = characteristic.verified || isRevision;
  const showModal = (field: string, cursorPos: number) => {
    const value = document.getElementsByName(field) ? (document.getElementsByName(field)[0] as HTMLTextAreaElement).value : characteristic[field];
    handleShowModal(characteristic, field, cursorPos, value?.toString());
  };

  const section = [
    {
      name: 'fullSpecification',
      label: i18n('entities.feature.fields.fullSpecification'),
      value: characteristic.fullSpecification,
      type: 'textbox',
      disabled: isDisabled,
      classes: 'resizeable',
      minimumSize: 5,
      change: (e: any) => debouncedUpdate(e, false),
      blur: update,
      modal: <SymbolButton showModal={showModal} field="fullSpecification" />,
    },
  ];
  return (
    <form id="noteForm" data-cy="noteForm">
      {isDisabled && (
        <Tooltip title={isRevision ? i18n('entities.feature.reviewStatus.lockedRevision') : i18n('entities.feature.reviewStatus.locked')}>
          <div>
            <Section key="form-section-0" id={0} sections={section} />
          </div>
        </Tooltip>
      )}
      {!isDisabled && <Section key="form-section-0" id={0} sections={section} />}
    </form>
  );
};

export default NoteForm;
