import React from 'react';
import { Tooltip } from 'antd';
import { i18n } from 'i18n';
import { Section } from 'view/global/inputs';
import { Characteristic } from 'types/characteristics';
import { validateNumber, validateNominal } from 'utils/DataCalculation';
import { getOptionsFromStrings } from 'utils/textOperations';

interface Props {
  characteristic: Characteristic;
  disabled: boolean;
  debouncedUpdate: (e: any) => void;
  update: (e: any) => void;
  isRevision?: boolean;
}

const DimensionForm: React.FC<Props> = ({ characteristic, disabled, update, debouncedUpdate, isRevision }) => {
  const isDisabled = characteristic.verified || isRevision;
  const isNotSplit = (characteristic.plusTol && characteristic.plusTol !== '') || (characteristic.minusTol && characteristic.minusTol !== '');
  const section = [
    {
      name: 'notationClass',
      value: characteristic.notationClass,
      disabled: isDisabled,
      group: getOptionsFromStrings('entities.feature.enumerators', 'notationClass', ['Tolerance', 'Basic', 'Reference']),
      type: 'radio',
      change: update,
    },
    {
      name: 'nominal',
      label: i18n('entities.feature.fields.nominal'),
      value: characteristic.nominal,
      disabled: isDisabled,
      errorConditional: characteristic.nominal ? !validateNominal(characteristic.nominal) : false,
      errorText: 'Not a valid number',
      type: 'text',
      change: debouncedUpdate,
      blur: update,
      limitTo: 'nominal',
    },
    {
      name: 'toleranceGroup',
      value: '',
      type: 'group',
      group: [
        {
          name: 'plusTol',
          label: i18n('entities.feature.fields.plusTol'),
          errorConditional: characteristic.plusTol ? !validateNumber(characteristic.plusTol) : false,
          errorText: 'Not a valid number',
          disabled: isDisabled || disabled,
          value: characteristic.notationClass === 'Basic' || characteristic.notationClass === 'Reference' ? '' : characteristic.plusTol,
          type: 'text',
          change: debouncedUpdate,
          blur: update,
          limitTo: 'number',
        },
        {
          name: 'minusTol',
          label: i18n('entities.feature.fields.minusTol'),
          disabled: isDisabled || disabled,
          errorConditional: characteristic.minusTol ? !validateNumber(characteristic.minusTol) : false,
          errorText: 'Not a valid number',
          value: characteristic.notationClass === 'Basic' || characteristic.notationClass === 'Reference' ? '' : characteristic.minusTol,
          type: 'text',
          change: debouncedUpdate,
          blur: update,
          limitTo: 'number',
        },
      ],
    },
    {
      name: 'limitGroup',
      value: '',
      type: 'group',
      group: [
        {
          name: 'upperSpecLimit',
          label: i18n('entities.feature.fields.upperSpecLimit'),
          disabled: isNotSplit || isDisabled,
          value: characteristic.notationClass === 'Basic' || characteristic.notationClass === 'Reference' ? '' : characteristic.upperSpecLimit,
          type: 'text',
          change: debouncedUpdate,
          blur: update,
          limitTo: 'number',
        },
        {
          name: 'lowerSpecLimit',
          label: i18n('entities.feature.fields.lowerSpecLimit'),
          disabled: isNotSplit || isDisabled,
          value: characteristic.notationClass === 'Basic' || characteristic.notationClass === 'Reference' ? '' : characteristic.lowerSpecLimit,
          type: 'text',
          change: debouncedUpdate,
          blur: update,
          limitTo: 'number',
        },
      ],
    },
    {
      name: 'toleranceSource',
      label: i18n('entities.feature.fields.toleranceSource'),
      value: characteristic.toleranceSource,
      disabled: isDisabled,
      group: getOptionsFromStrings('entities.feature.enumerators', 'toleranceSource', ['Default_Tolerance', 'Document_Defined', 'Manually_Set']),
      type: 'select',
      change: update,
    },
  ];

  return (
    <form id="dimensionForm" data-cy="dimensionForm">
      {isDisabled && (
        <Tooltip title={isRevision ? i18n('entities.feature.reviewStatus.lockedRevision') : i18n('entities.feature.reviewStatus.locked')}>
          <div>
            <Section key="form-section-0" id={0} sections={section} />
          </div>
        </Tooltip>
      )}
      {!isDisabled && <Section key="form-section-0" id={0} sections={section} />}
    </form>
  );
};

export default DimensionForm;
