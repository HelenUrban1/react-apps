/* eslint-disable complexity */
import React, { ChangeEvent, useState } from 'react';
import { Characteristic } from 'types/characteristics';
import { Section } from 'view/global/inputs';
import Permissions from 'security/permissions';
import PermissionChecker from 'modules/auth/permissionChecker';
import { i18n } from 'i18n';
import { ListObject, ListTypeEnum } from 'domain/setting/listsTypes';
import { ListItemModal } from 'domain/setting/configurations/ListItemModal';
import { getSingleLevelListValue, getCurrentList } from 'utils/Lists';
import { SymbolButton } from 'styleguide/Buttons/SymbolButton';
import { isFramed } from 'utils/textOperations';
import { Tooltip } from 'antd';
import './forms.less';
import { useSelector } from 'react-redux';
import { AppState } from 'modules/state';

interface Props {
  characteristic: Characteristic;
  update: (e: any) => void;
  debouncedUpdate: (e: any) => void;
  handleCreateListItem: (name: string, activeListItems: ListObject, activeList: ListTypeEnum, parentId?: string, metadata?: string) => void;
  handleShowModal: (feature: Characteristic, field: string, cursor: number, value: string | undefined) => void;
  isRevision?: boolean;
}

const InspectionForm: React.FC<Props> = ({ characteristic, update, debouncedUpdate, handleCreateListItem, handleShowModal, isRevision }) => {
  const { user, types, classifications, methods, operations, metricUnits, imperialUnits } = useSelector((state: AppState) => state.session);
  const [activeList, setActiveList] = useState<ListTypeEnum>('Classification');
  const [activeListItems, setActiveListItems] = useState<ListObject | null>(null);
  const [displayModal, setDisplayModal] = useState<boolean>(false);
  const permissionValidator = new PermissionChecker(user);
  const hasListEditAccess = permissionValidator.match(Permissions.values.accountManagement);
  const isDisabled = characteristic.verified || isRevision;

  // Gets the select options from a given list and type
  const getSelectOptions = (list: ListObject, type: string) => {
    let options: { name: string; value: string; label: string; deleted: boolean }[] = [];
    const listKeys = Object.keys(list);
    listKeys.forEach((key) => {
      const entry = list[key];
      if (entry.status === 'Active') {
        options.push({
          name: entry.value,
          label: entry.default ? i18n(`entities.feature.enumerators.${type}[${entry.value}]`) : entry.value,
          value: entry.id,
          deleted: entry.deleted,
        });
      }
    });
    // options.sort((a, b) => ((a.value || '') > (b.value || '') ? 1 : -1));
    options = [
      {
        name: `${type}_none`,
        value: '',
        label: i18n(`common.none`),
        deleted: false,
      },
      ...options,
    ];
    if (hasListEditAccess) options.push();
    return options;
  };

  const resetInput = () => {
    setDisplayModal(false);
  };

  const createListItem = (name: string) => {
    resetInput();
    handleCreateListItem(name, activeListItems || {}, activeList);
  };

  const handleChange = (event: ChangeEvent<HTMLSelectElement>) => {
    if ((event.target.value && event.target.value.includes('-add')) || (!characteristic[event.target.name] && !event.target.value) || characteristic[event.target.name] === event.target.value) {
      // selected option was to add a new item or the same as the current option
      return;
    }
    update(event);
  };

  const showModal = (field: string, cursorPos: number) => {
    const value = document.getElementsByName(field) ? (document.getElementsByName(field)[0] as HTMLTextAreaElement).value : characteristic[field];
    handleShowModal(characteristic, field, cursorPos, value?.toString());
  };

  const section = [
    {
      name: 'criticality',
      value: '',
      type: 'group',
      group: [
        {
          name: 'criticality',
          label: 'Classification',
          value: getSingleLevelListValue(classifications, characteristic.criticality || null) || '',
          group: getSelectOptions(classifications || {}, 'criticality'),
          type: 'select',
          disabled: isDisabled,
          change: handleChange,
          add: hasListEditAccess
            ? () => {
                setActiveList('Classification');
                setActiveListItems(getCurrentList('Classification', classifications, methods, types, operations, imperialUnits, metricUnits));
                setDisplayModal(true);
              }
            : undefined,
        },
        {
          name: 'operation',
          label: i18n('entities.feature.fields.operation'),
          value: getSingleLevelListValue(operations, characteristic.operation) || '',
          group: getSelectOptions(operations || {}, 'operations'),
          type: 'select',
          disabled: isDisabled,
          change: handleChange,
          add: hasListEditAccess
            ? () => {
                setActiveList('Operation');
                setActiveListItems(getCurrentList('Operation', classifications, methods, types, operations, imperialUnits, metricUnits));
                setDisplayModal(true);
              }
            : undefined,
        },
      ],
    },
    {
      name: 'inspectionMethod',
      label: i18n('entities.feature.fields.inspectionMethod'),
      value: getSingleLevelListValue(methods, characteristic.inspectionMethod) || '',
      group: getSelectOptions(methods || {}, 'inspectionMethods'),
      type: 'select',
      disabled: isDisabled,
      change: handleChange,
      add: hasListEditAccess
        ? () => {
            setActiveList('Inspection_Method');
            setActiveListItems(getCurrentList('Inspection_Method', classifications, methods, types, operations, imperialUnits, metricUnits));
            setDisplayModal(true);
          }
        : undefined,
    },
    {
      name: 'notes',
      label: 'Comment',
      value: characteristic.notes,
      type: 'textbox',
      disabled: isDisabled,
      classes: 'resizeable',
      framed: isFramed(characteristic.notes || ''),
      minimumSize: 5,
      change: debouncedUpdate,
      blur: update,
      modal: <SymbolButton showModal={showModal} field="notes" />,
    },
  ];

  return (
    <>
      <ListItemModal modalTitle={`Add ${i18n(`settings.custom.lists.${activeList}`)}`} listType={activeList} listItems={activeListItems} handleCreateListItem={createListItem} visible={displayModal} setDisplayModal={setDisplayModal} />

      {isDisabled && (
        <Tooltip title={isRevision ? i18n('entities.feature.reviewStatus.lockedRevision') : i18n('entities.feature.reviewStatus.locked')}>
          <form data-cy="inspectionForm">
            <Section key="form-section-0" id={0} sections={section} />
          </form>
        </Tooltip>
      )}
      {!isDisabled && (
        <form data-cy="inspectionForm">
          <Section key="form-section-0" id={0} sections={section} />
        </form>
      )}
    </>
  );
};

export default InspectionForm;
