import React, { useMemo } from 'react';
import { Section } from 'view/global/inputs';
import { i18n } from 'i18n';
import { Characteristic } from 'types/characteristics';
import { SymbolButton } from 'styleguide/Buttons/SymbolButton';
import { Tooltip } from 'antd';

interface Props {
  characteristic: Characteristic;
  update: (e: any) => void;
  debouncedUpdate: (e: any) => void;
  handleShowModal: (feature: Characteristic, field: string, cursor: number, value: string | undefined) => void;
  isRevision?: boolean;
}

const GdtForm: React.FC<Props> = ({ characteristic, update, debouncedUpdate, handleShowModal, isRevision }) => {
  const showModal = (field: string, cursorPos: number) => {
    const value = document.getElementsByName(field) ? (document.getElementsByName(field)[0] as HTMLInputElement).value : characteristic[field];
    handleShowModal(characteristic, field, cursorPos, value?.toString());
  };

  const isDisabled = useMemo(() => characteristic.verified || isRevision, [characteristic, isRevision]);

  const section = useMemo(
    () => [
      {
        name: 'gdtPrimaryToleranceZone',
        label: i18n('entities.feature.fields.gdtPrimaryToleranceZone'),
        value: characteristic.gdtPrimaryToleranceZone ? characteristic.gdtPrimaryToleranceZone : '',
        type: 'text',
        disabled: isDisabled,
        change: debouncedUpdate,
        blur: update,
        modal: <SymbolButton showModal={showModal} field="gdtPrimaryToleranceZone" />,
      },
      {
        name: 'gdtSecondaryToleranceZone',
        label: i18n('entities.feature.fields.gdtSecondaryToleranceZone'),
        value: characteristic.gdtSecondaryToleranceZone ? characteristic.gdtSecondaryToleranceZone : '',
        type: 'text',
        disabled: isDisabled,
        change: debouncedUpdate,
        blur: update,
        modal: <SymbolButton showModal={showModal} field="gdtSecondaryToleranceZone" />,
      },
      {
        name: 'datumGroup',
        value: '',
        label: 'Datums',
        type: 'group',
        group: [
          {
            name: 'gdtPrimaryDatum',
            label: i18n('entities.feature.enumerators.notationSubtype.Datum'),
            value: characteristic.gdtPrimaryDatum ? characteristic.gdtPrimaryDatum : '',
            type: 'text',
            classes: 'group-flex-end',
            disabled: isDisabled,
            change: debouncedUpdate,
            blur: update,
            limitTo: 'uppercase',
            modal: <SymbolButton showModal={showModal} field="gdtPrimaryDatum" />,
          },
          {
            name: 'gdtSecondaryDatum',
            value: characteristic.gdtSecondaryDatum ? characteristic.gdtSecondaryDatum : '',
            type: 'text',
            classes: 'group-flex-end',
            disabled: isDisabled,
            change: debouncedUpdate,
            blur: update,
            limitTo: 'uppercase',
            modal: <SymbolButton showModal={showModal} field="gdtSecondaryDatum" />,
          },
          {
            name: 'gdtTertiaryDatum',
            value: characteristic.gdtTertiaryDatum ? characteristic.gdtTertiaryDatum : '',
            type: 'text',
            classes: 'group-flex-end',
            disabled: isDisabled,
            change: debouncedUpdate,
            blur: update,
            limitTo: 'uppercase',
            modal: <SymbolButton showModal={showModal} field="gdtTertiaryDatum" />,
          },
        ],
      },
      {
        name: 'limitGroup',
        value: '',
        type: 'group',
        group: [
          {
            name: 'upperSpecLimit',
            label: i18n('entities.feature.fields.upperSpecLimit'),
            value: characteristic.upperSpecLimit,
            type: 'text',
            disabled: isDisabled,
            change: debouncedUpdate,
            blur: update,
          },
          {
            name: 'lowerSpecLimit',
            label: i18n('entities.feature.fields.lowerSpecLimit'),
            value: characteristic.lowerSpecLimit,
            type: 'text',
            disabled: isDisabled,
            change: debouncedUpdate,
            blur: update,
          },
        ],
      },
    ],
    [characteristic, isDisabled],
  );

  return (
    <form id="gdtForm" data-cy="gdtForm">
      {isDisabled && (
        <Tooltip title={isRevision ? i18n('entities.feature.reviewStatus.lockedRevision') : i18n('entities.feature.reviewStatus.locked')}>
          <div>
            <Section key="form-section-0" id={0} sections={section} />
          </div>
        </Tooltip>
      )}
      {!isDisabled && <Section key="form-section-0" id={0} sections={section} />}
    </form>
  );
};

export default GdtForm;
