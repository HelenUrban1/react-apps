import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { i18n } from 'i18n';
import { PartStepEnum } from 'modules/navigation/navigationActions';
import PanelHeader from 'styleguide/PanelHeader/PanelHeader';
import { ProgressFooter } from 'styleguide/ProgressionFooter';
import { AppState } from 'modules/state';
import { updatePartThunk } from 'modules/part/partActions';
import { Section } from 'view/global/inputs';
import { RegExValidators } from 'utils/characterValidationRules';
import log from 'modules/shared/logger';
import './documentDetails.less';
import Analytics from 'modules/shared/analytics/analytics';
import { PartEditorActions, updateDrawingSheetThunk, updateDrawingThunk } from 'modules/partEditor/partEditorActions';
import { DocumentTypeEnum } from 'graphql/drawing';
import Spinner from 'view/shared/Spinner';
import moment from 'moment';

const { DOCS, TOLS } = PartStepEnum;

export interface DocumentDetailsFormProps {
  next: () => void;
  back: () => void;
}

interface FormValues {
  id: string;
  documentType: DocumentTypeEnum;
  name: string;
  number: string;
  revision: string;
  pageNames: string[];
  [key: string]: string | string[] | number;
}

export const DocumentDetailsForm: React.FC<DocumentDetailsFormProps> = ({ back, next }) => {
  const navigation = useSelector((state: AppState) => state.navigation);
  const dispatch = useDispatch();
  const { index: curPageIndex, tronInstance } = useSelector((state: AppState) => state.pdfView);

  // Prepare to update the part at each stage of part creation
  const { partDrawing, focus } = useSelector((state: AppState) => state.partEditor);
  const { part } = useSelector((state: AppState) => state.part);
  const [isPrimary, setPrimary] = useState(partDrawing?.id === part?.primaryDrawing?.id);

  useEffect(() => {
    if (part?.primaryDrawing?.id && partDrawing?.id) {
      const primary = partDrawing.id === part.primaryDrawing.id;
      if (isPrimary !== primary) {
        setPrimary(primary);
      }
    }
  }, [part?.primaryDrawing?.id, partDrawing?.id]);

  const handlePrimary = (value: boolean) => {
    if (part?.primaryDrawing?.id && partDrawing?.id) {
      if (value && partDrawing.id !== part.primaryDrawing.id) {
        // toggle primary on for a drawing
        dispatch(updatePartThunk({ primaryDrawing: partDrawing.id }, part.id));
      } else if (!value && part.drawings && partDrawing.id === part.primaryDrawing.id) {
        // toggle primary off for current primary drawing
        const newId = part.drawings.find((d) => d.id !== partDrawing.id);
        if (!newId) {
          log.error("can't find alternate drawing to set as primary");
          return;
        }
        dispatch(updatePartThunk({ primaryDrawing: newId.id }, part.id));
      }
      setPrimary(value);
    }
  };

  // State to track changes to record between saves
  const [formValues, setFormValues] = useState<FormValues>({
    id: partDrawing?.id || '',
    documentType: partDrawing?.documentType || 'Other',
    name: partDrawing && partDrawing.name ? partDrawing.name : '',
    number: partDrawing && partDrawing.number ? partDrawing.number : '',
    revision: partDrawing && partDrawing.revision ? partDrawing.revision : moment(partDrawing?.createdAt).format('YYYY-MM-DD'),
    pageNames: partDrawing?.sheets ? partDrawing.sheets.map((page) => page.pageName) : [],
  });

  useEffect(() => {
    if (part && partDrawing) {
      setFormValues({
        id: partDrawing?.id || '',
        documentType: partDrawing?.documentType || 'Other',
        name: partDrawing && partDrawing.name ? partDrawing.name : '',
        number: partDrawing && partDrawing.number ? partDrawing.number : '',
        revision: partDrawing && partDrawing.revision ? partDrawing.revision : moment(partDrawing?.createdAt).format('YYYY-MM-DD'),
        pageNames: partDrawing?.sheets ? partDrawing.sheets.map((page) => page.pageName) : [`${curPageIndex + 1}`],
      });
      if (navigation.currentPartStep === DOCS && focus) {
        (document.getElementById(focus.split(' ')[1])?.firstChild as HTMLTextAreaElement).focus();
        dispatch({ type: PartEditorActions.SET_PART_EDITOR_FOCUS, payload: null });
      }
    }
  }, [partDrawing?.id, partDrawing?.sheets.length]);

  // Updates form state with a change
  const updateFormState = async (field: string, value: string) => {
    const newFormValues = { ...formValues };

    if (formValues[field] !== value) {
      Analytics.track({
        event: Analytics.events.docInfoUpdated,
        part,
        drawing: partDrawing,
        drawingSheet: partDrawing?.sheets[curPageIndex],
        properties: {
          'field.name': field,
          'field.value.from': formValues[field],
          'field.value.to': value,
        },
      });
    }

    if (field.includes('pageName')) {
      const nameParts = field.split('-');
      const ind = parseInt(nameParts[1], 10);
      newFormValues.pageNames[ind] = value;
      dispatch(updateDrawingSheetThunk(ind, { pageName: value }));
    } else {
      newFormValues[field] = value;
      dispatch(updateDrawingThunk({ [field]: newFormValues[field] }));
    }

    setFormValues(newFormValues);
  };

  // Handle onChange events by unpacking saving new value for field in form state
  const handleChange = (event: React.ChangeEvent<HTMLInputElement>): void => {
    const [field, value] = [event.target.name, event.target.value];
    updateFormState(field, value.replace(RegExValidators.matchCommonSpecialChars, ''));
  };

  // Handle page change
  const handlePageChange = (value: any) => {
    const viewer = tronInstance?.Core.documentViewer;
    if (!viewer) {
      return;
    }
    viewer.trigger('scrollTo', {
      x: 0,
      y: 0,
      pageNumber: value + 1,
    });
  };

  // If values are valid, update max stage and submit
  const updateMaxStage = () => {
    if (part) {
      const max = Math.max(part?.workflowStage || 0, navigation.maxPartStep, TOLS);
      dispatch(updatePartThunk({ workflowStage: max }, part.id));
    }
  };

  // Handle submit event by committing all pending changes  the record with all fields
  const handleSubmit = (event: React.SyntheticEvent) => {
    log.debug('handleSubmit', formValues);
    event.preventDefault();
    updateMaxStage();

    // Save unapplied changes to the Drawing and DrawingSheet records
    if (partDrawing) {
      if (partDrawing.name !== formValues.name || partDrawing.number !== formValues.number || partDrawing.revision !== formValues.revision) {
        dispatch(updateDrawingThunk({ name: formValues.name, number: formValues.number, revision: formValues.revision }));
      }

      partDrawing.sheets.forEach((sheet) => {
        if (sheet.pageName !== formValues.pageNames[sheet.pageIndex]) {
          dispatch(updateDrawingSheetThunk(sheet.pageIndex, { pageName: formValues.pageNames[sheet.pageIndex] }));
        }
      });
    }

    // handlePageChange(0);

    next();
  };

  const sections: any[] = [
    {
      name: 'drawingSelect',
      label: '',
      value: '',
      group: [
        {
          name: 'documentType',
          label: 'Document Type',
          classes: 'group-flex-5',
          value: formValues.documentType,
          change: handleChange,
          type: 'select',
          group: ['Drawing', 'Notes', 'Materials', 'Specifications', 'Other'].map((type: string) => {
            return { name: type, value: type, label: type };
          }),
        },
        {
          name: 'primary',
          label: 'Primary',
          value: isPrimary,
          classes: 'switch-input group-flex-3',
          change: handlePrimary,
          type: 'switch',
          disabled: !part || !part.drawings || part.drawings.length <= 1,
          tooltip: i18n('entities.part.instructions.primaryHint'),
        },
      ],
    },
    {
      name: 'number',
      label: i18n('entities.drawing.fields.number'),
      value: formValues.number,
      classes: 'group-flex-3',
      blur: handleChange,
      type: 'drawingNumber',
      validateTo: RegExValidators.matchCommonSpecialChars,
      tooltip: i18n('entities.drawing.instructions.partNumberCopy'),
      partNumber: part?.number,
    },
    {
      name: 'revision',
      label: i18n('entities.drawing.fields.revision'),
      classes: 'group-flex-3',
      value: formValues.revision,
      blur: handleChange,
      type: 'text',
    },
    {
      name: 'name',
      label: 'Drawing Name',
      value: formValues.name,
      blur: handleChange,
      type: 'text',
    },
  ];

  if (partDrawing?.sheets && partDrawing.sheets.length > 0) {
    partDrawing.sheets.forEach((page) => {
      sections.push({
        name: `pageGroup-${page.pageIndex}-${partDrawing.id}`,
        label: '',
        value: '',
        group: [
          {
            name: `pageName-${page.pageIndex}`,
            label: page.pageIndex === 0 ? 'Page Names' : undefined,
            classes: 'group-flex-3',
            value: formValues.pageNames[page.pageIndex],
            blur: handleChange,
            focus: () => {
              handlePageChange(page.pageIndex);
            },
            type: 'text',
          },
        ],
      });
    });
  }

  return (
    <>
      <section id="documentAnalysis" className="side-panel-content">
        <div className="doc-selector">
          <PanelHeader header={`${DOCS + 1}. ${i18n('entities.part.title.docs')}`} copy={i18n('entities.part.instructions.docs')} dataCy="document_info_" />
        </div>
        <form className="panel-form" onSubmit={handleSubmit}>
          <Section id={0} sections={sections} />
        </form>
        {(!partDrawing || !partDrawing.sheets || partDrawing.sheets.length === 0) && <Spinner style={{ margin: '0px', height: '48px' }} />}
      </section>
      <div className="form-buttons form-section sider-panel-progress-footer">
        <ProgressFooter id="docs-info-footer" classes="panel-progression-footer" cypress="progress-footer" steps={navigation.maxTotalSteps} current={DOCS + 1} onNext={handleSubmit} canProgress canBack onBack={back} />
      </div>
    </>
  );
};
