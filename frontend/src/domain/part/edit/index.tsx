import React, { useEffect, useState } from 'react';
import { RouteComponentProps, useHistory, useParams } from 'react-router-dom';
import Analytics from 'modules/shared/analytics/analytics';
import log from 'modules/shared/logger';
import { CharacteristicActions, loadCharacteristicsThunk } from 'modules/characteristic/characteristicActions';
import { useDispatch, useSelector } from 'react-redux';
import { AppState } from 'modules/state';
import { initializePartEditorThunk, PartEditorActions } from 'modules/partEditor/partEditorActions';
import { doGetPart, doLoadCurrentPartVersion, updatePartThunk } from 'modules/part/partActions';
import { NavigationActions } from 'modules/navigation/navigationActions';
import { RevisionBanner } from 'styleguide/Revision/RevisionBanner';
import config from 'config';
import { RevisionHistory } from 'styleguide/Revision/History';
import { DrawingViewer } from './DrawingViewer';
import { PartEditorDrawer } from './PartEditorDrawer';
import '../partStyles.less';
import { ExtractionTools } from './extractionToolbar/ExtractionToolbar';

interface Props {
  trial: boolean;
}
interface Params {
  id: string;
}

const PartEditor: React.FC<RouteComponentProps & Props> = ({ trial }) => {
  const dispatch = useDispatch();
  const { id: partId } = useParams<Params>();
  const { part, revisionIndex, revisionType } = useSelector((state: AppState) => state.part);
  const { partEditorLoaded, compareMode, partDrawing, assignedStyles, focus } = useSelector((state: AppState) => state.partEditor);
  const { currentUser } = useSelector((state: AppState) => state.auth);
  const { accountMembership, settings } = useSelector((state: AppState) => state.session);
  const history = useHistory();
  const navigation = useSelector((state: AppState) => state.navigation);

  const [hideFeatures, setHideFeatures] = useState<boolean>(false);

  const [historyShow, setHistory] = useState(false);

  const toggleDrawer = () => {
    setHistory((prev) => !prev);
  };

  const toggleHideLeaderLines = () => {
    if (part) {
      dispatch(updatePartThunk({ displayLeaderLines: !part.displayLeaderLines }, part.id));
    }
  };

  const toggleHideFeatures = () => {
    setHideFeatures(!hideFeatures);
  };

  // This route expects there to be a part ID in the query string
  if (!partId) {
    log.error('No ID Passed');
    Analytics.track({
      event: Analytics.events.partIDError,
      properties: {
        'part.id': partId,
      },
    });
    history.push(`/parts`);
  }

  // If the part exits, they are taken back to the parts list page?
  const exit = (): void => {
    Analytics.track({
      event: Analytics.events.userExitedPartEditor,
      properties: {
        'part.id': partId,
      },
    });
    history.push(`/parts`);
  };

  const checkSecurity = () => {
    if (part && accountMembership && accountMembership.accessControl && part.accessControl && part.accessControl !== 'None') {
      Analytics.track({
        event: Analytics.events.partRestricted,
        part,
        user: currentUser,
      });
      history.push('/403');
    }
  };

  useEffect(() => {
    if (part && partDrawing && partEditorLoaded) {
      if (focus) {
        const step = parseInt(focus.split(' ')[0], 10);
        if (step >= 0) dispatch({ type: NavigationActions.SET_CURRENT_PART_STEP, payload: step });
      }
    }
  }, [partEditorLoaded]);

  useEffect(() => {
    dispatch(loadCharacteristicsThunk(partId, assignedStyles || {}));
  }, [partDrawing?.id]);

  useEffect(() => {
    if (part && part.id === partId) {
      checkSecurity();
      dispatch(initializePartEditorThunk(part.id));
    } else {
      dispatch(doGetPart(partId));
    }
    const maxTotalSteps = settings.autoballoonEnabled?.value === 'true' ? config.autoBallooning.maxTotalSteps : config.autoBallooning.maxTotalSteps - 1;

    dispatch({ type: NavigationActions.SET_MAX_TOTAL_STEPS, payload: maxTotalSteps });
  }, [partId, part?.id]);

  useEffect(() => {
    const maxTotalSteps = settings.autoballoonEnabled?.value === 'true' ? config.autoBallooning.maxTotalSteps : config.autoBallooning.maxTotalSteps - 1;

    dispatch({ type: NavigationActions.SET_MAX_TOTAL_STEPS, payload: maxTotalSteps });
    return () => {
      dispatch({ type: CharacteristicActions.CLEANUP_STORE });
      dispatch({ type: PartEditorActions.RESET_PART_EDITOR });
      const resetPart = false;
      dispatch(doLoadCurrentPartVersion(resetPart));
    };
  }, []);

  // This is the top of the part editor scope, so wrap all children with the contexts that will be needed
  return (
    <>
      <ExtractionTools hideFeatures={hideFeatures} toggleHideFeatures={toggleHideFeatures} toggleDrawer={toggleDrawer} toggleLeaderLines={toggleHideLeaderLines} />
      <main
        id="partCreator"
        className={`part-creator has-drawer${trial ? ' trial' : ''}`}
        style={{
          display: 'flex',
          flexDirection: 'row',
          flexWrap: 'nowrap',
          padding: '0px !important',
        }}
      >
        {compareMode !== 'Align' && <PartEditorDrawer exit={exit} />}
        {navigation.currentPartStep + 1 < navigation.maxTotalSteps && <DrawingViewer hideFeatures={hideFeatures} />}
        <RevisionHistory parent="main#partCreator" historyShow={historyShow} toggleDrawer={toggleDrawer} />
      </main>
      {compareMode !== 'Align' && revisionIndex > 0 && revisionType && <RevisionBanner previous={revisionIndex} type={revisionType} />}
    </>
  );
};

// Export as default so that it can be interpreted by React Router
export default PartEditor;
