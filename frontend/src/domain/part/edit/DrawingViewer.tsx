import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { PdfViewer } from 'styleguide/PdfViewer';
import log from 'modules/shared/logger';
import Spinner from 'view/shared/Spinner';
import { CharacteristicActions } from 'modules/characteristic/characteristicActions';
import { AppState } from 'modules/state';
import { PartStepEnum } from 'modules/navigation/navigationActions';
import { endDrawingAlignmentThunk, PartEditorActions } from 'modules/partEditor/partEditorActions';
import { Tolerance } from 'types/tolerances';
import { defaultUnitIDs } from 'view/global/defaults';
import { OverlayViewer } from 'styleguide/PdfViewer/OverlayViewer';
import { CharacteristicsCanvas } from './characteristics/CharacteristicsCanvas';
import { GridCanvas } from './grid/GridCanvas';
import { getCharacteristicMarkerChanges } from './characteristics/utils/extractUtil';
import { rebuildToleranceSettingObject } from './defaultTolerances/TolerancesLogic';

const { STYLES } = PartStepEnum;

interface DrawingViewerProps {
  hideFeatures: boolean;
}

export const DrawingViewer = ({ hideFeatures }: DrawingViewerProps) => {
  const navigation = useSelector((state: AppState) => state.navigation);
  const { part } = useSelector((state: AppState) => state.part);
  const { assignedStyles, partDrawing, compareMode, partEditorMessage, partEditorError, presets } = useSelector((state: AppState) => state.partEditor);
  const dispatch = useDispatch();
  const { markers, settings } = useSelector((state: AppState) => state.session);
  const { characteristics, index: ind } = useSelector((state: AppState) => state.characteristics);
  const { index, layerManager, tronInstance } = useSelector((state: AppState) => state.pdfView);
  const { revisionFile } = useSelector((state: AppState) => state.pdfOverlayView);

  const [compareLoaded, setCompareLoaded] = useState(0);

  useEffect(() => {
    if (!assignedStyles || !markers || !characteristics) return;
    dispatch({ type: CharacteristicActions.UPDATE_MARKERS, payload: { updates: [], assignedMarkers: assignedStyles, markers } });
  }, [markers, characteristics]);

  // Moved loading of chars up to index 4/21/22
  // useEffect(() => {
  //   if (part?.id && assignedStyles) {
  //     dispatch(loadCharacteristicsThunk(part.id, assignedStyles));
  //   }
  // }, [part?.id, assignedStyles]);

  useEffect(() => {
    // this was firing after CharacteristicActions.EDIT_MARKERS was called from the balloonStylesForm, but with incomplete updates, does anything break if we prevent this from firing while on styles step?
    if (!!assignedStyles && !!markers && navigation.currentPartStep !== STYLES) {
      const updates = getCharacteristicMarkerChanges(characteristics, assignedStyles);
      dispatch({ type: CharacteristicActions.EDIT_MARKERS, payload: { updates, assignedMarkers: assignedStyles, markers } });
    }
  }, [assignedStyles, markers]);

  // TODO: This is firing way more than it needs to.
  // See frontend/src/styleguide/PdfViewer/canvasLayerManager.ts for implementation
  useEffect(() => {
    if (layerManager) layerManager.addLayers(['grid', 'markers']);

    return function cleanup() {
      if (layerManager) layerManager.removeAllLayers();
      if (revisionFile && compareLoaded) {
        dispatch(endDrawingAlignmentThunk());
        setCompareLoaded(0);
      }
    };
  }, []);

  // When a person changes the viewed page, update the sheet being viewed
  useEffect(() => {
    if (partDrawing && partDrawing.sheets && partDrawing.sheets.length > index) {
      const settingsDefaultTolerance = settings.tolerancePresets?.metadata[presets?.tolerances || 'Default']?.setting as Tolerance | undefined;
      const tolerances = rebuildToleranceSettingObject({ currentPreset: 'Default', linear: partDrawing.sheets[index].defaultLinearTolerances, angular: partDrawing.sheets[index].defaultAngularTolerances, settings: settingsDefaultTolerance });
      if (tolerances.linear.unit === defaultUnitIDs.inch && part?.measurement === 'Metric') {
        // Fix default unit, since linear defaults to Imperial measurement
        tolerances.linear.unit = defaultUnitIDs.millimeter;
      }
      dispatch({ type: PartEditorActions.SET_PART_EDITOR_DRAWING_SHEET, payload: { sheet: partDrawing.sheets[index], tolerances } });
    }
  }, [partDrawing, index]);

  // Wrap form updates in viewUpdated callback
  layerManager.subscribe('OverlayViewer', 'compareComplete', () => {
    if (tronInstance && compareMode === 'Align' && compareLoaded < 2) {
      setCompareLoaded(compareLoaded + 1);
    }
  });

  useEffect(() => {
    if (compareMode !== 'Align' && compareLoaded) setCompareLoaded(0);
  }, [compareMode]);

  // On doc load make sure correct page is loaded if a feature is selected
  layerManager.subscribe('DrawingViewer', 'documentLoaded', () => {
    const feature = characteristics ? characteristics[ind] : null;
    if (feature && tronInstance?.Core.documentViewer && tronInstance.Core.documentViewer.getCurrentPage() - 1 !== index) {
      try {
        tronInstance.Core.documentViewer.trigger('scrollTo', {
          x: feature.boxLocationX,
          y: feature.boxLocationY,
          pageNumber: feature.drawingSheetIndex,
        });
      } catch (err) {
        log.error('DrawingViewer.scrollTo', err);
      }
    }
  });

  // Manage loading, lookup, and update errors
  let loadingMsg: any = '';
  if (partEditorError) {
    loadingMsg = `Error: ${partEditorError.message}`;
  } else if (!part) {
    loadingMsg = 'Loading part drawing...';
  } else if (partEditorMessage) {
    loadingMsg = partEditorMessage;
  }

  if (loadingMsg || !partDrawing || !partDrawing.drawingFile) {
    if (loadingMsg.includes('Loading')) {
      return <Spinner />;
    }
    return <p className="loading-message">{loadingMsg}</p>;
  }

  return (
    <main className="part-canvas" id="partCanvas">
      {revisionFile && compareMode === 'Align' && compareLoaded < 2 && <Spinner style={{ position: 'absolute', backgroundColor: 'rgba(0,0,0,0.25)', margin: '0px', zIndex: '1050' }} />}
      {!revisionFile && (
        <>
          <CharacteristicsCanvas hideFeatures={hideFeatures} />
          <GridCanvas />
        </>
      )}
      <PdfViewer filePath={partDrawing.drawingFile.privateUrl || ''} tronInstance={tronInstance} />
      {revisionFile && compareLoaded && <OverlayViewer compareLoaded={compareLoaded} />}
    </main>
  );
};
