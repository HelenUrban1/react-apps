import React from 'react';
import { Layout } from 'antd';
import PropTypes from 'prop-types';
import IxSider from '../IxSider/IxSider';
import NavMenu from '../NavMenu/NavMenu';
import menuItems from '../mocks/NavItems';
const { Header, Content, Footer } = Layout;

class MasterFrame extends React.Component {
  render() {
    const contentTopMargin = this.props.hasHeader ? '80px' : 0;
    const contentBottomMargin = this.props.hasFooter ? '80px' : 0;

    return (
      <Layout hasSider>
        <IxSider menu={<NavMenu menuItems={menuItems} />} />
        <Layout>
          {this.props.hasHeader ? <Header style={{ position: 'sticky', width: '100%', top: 0, padding: 0 }}>{this.props.hasHeader && this.props.header}</Header> : null}
          <Content style={{ overflow: 'auto', margin: `${contentTopMargin}, ${25}, ${contentBottomMargin}, ${25}` }}>{this.props.hasContent && this.props.content}</Content>
          {this.props.hasFooter ? <Footer style={{ position: 'sticky', bottom: 0, padding: 0 }}>{this.props.hasFooter && this.props.footer}</Footer> : null}
        </Layout>
      </Layout>
    );
  }
}

MasterFrame.propTypes = {
  /**
   * Whether or not this frame has a header.
   */
  hasHeader: PropTypes.bool,

  /**
   * Header Component
   */
  header: PropTypes.element,

  /**
   * Whether or not this frame has a footer.
   */
  hasFooter: PropTypes.bool,

  /**
   * Footer Component
   */
  footer: PropTypes.element,

  /**
   * Whether or not this frame has a content area.
   */
  hasContent: PropTypes.bool,

  /**
   * Content Component
   */
  content: PropTypes.element,

  /**
   * Navigation Bar Items
   */
  menuItems: PropTypes.array,
};

MasterFrame.defaultProps = {
  // by Default all 'has' props are false
  hasHeader: false,
  hasContent: false,
  hasFooter: false,
  hasDrawer: false,
};

export default MasterFrame;
