import React from 'react';
import { storiesOf } from '@storybook/react';
import MDX from './MasterFrame.mdx';
import MasterFrame from './MasterFrame';
import NavMenu from '../NavMenu/NavMenu';
import MockHeader from '../mocks/MockHeader';
import MockContent from '../mocks/MockContent';
import MockFooter from '../mocks/MockFooter';
import { BrowserRouter as Router } from 'react-router-dom';
import menuItems from '../mocks/NavItems';

const stories = storiesOf('Design System/Frames', module);

stories
  .add(
    'Master',
    () => {
      const story = (
        <Router>
          <MasterFrame sider={<NavMenu />} hasHeader={true} header={<MockHeader />} hasContent={true} content={<MockContent />} hasFooter={true} footer={<MockFooter />} menuItems={menuItems}></MasterFrame>
        </Router>
      );

      return story;
    },
    {
      docs: { page: MDX },
    },
  )
  .add(
    'No Footer',
    () => {
      const story = (
        <Router>
          <MasterFrame sider={<NavMenu />} hasHeader={true} header={<MockHeader />} hasContent={true} content={<MockContent />} hasFooter={false} menuItems={menuItems}></MasterFrame>
        </Router>
      );

      return story;
    },
    {
      docs: { page: MDX },
    },
  )
  .add(
    'No Footer or Header',
    () => {
      const story = (
        <Router>
          <MasterFrame sider={<NavMenu />} hasHeader={false} hasContent={true} content={<MockContent />} hasFooter={false} menuItems={menuItems}></MasterFrame>
        </Router>
      );
      return story;
    },
    {
      docs: { page: MDX },
    },
  );
