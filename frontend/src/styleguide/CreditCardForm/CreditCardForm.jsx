import React, { useState, useEffect, useRef } from 'react';
import Spinner from 'view/shared/Spinner';
import { Button } from 'antd';
import { WarningOutlined } from '@ant-design/icons';
import { i18n } from 'i18n';
import './CreditCardForm.less';
import Analytics from 'modules/shared/analytics/analytics';

/**
 * Billing form that uses the Chargify.js library, which injects iframes into the page.
 *
 * @see {@link https://developer.chargify.com/content/chargify-js/chargify-js.html}
 */
const CreditCardForm = ({ subscription, chargifyAuth, loading, setLoading, formSignifier, handleClose, onSubmit, onError }) => {
  const { publicKey, securityToken, siteUrl } = chargifyAuth;
  const [errorMessage, setErrorMessage] = useState('');
  const [formLoading, setFormLoading] = useState(true);

  const chargify = useRef();
  const chargifyForm = useRef();
  const cardRef = useRef();

  const handleSubmit = (event) => {
    event.preventDefault();
    setErrorMessage('');
    setLoading(true);

    const successCallback = (token) => {
      Analytics.track({
        event: Analytics.events.accountPaymentMethodUpdated,
        subscription,
        properties: {
          'subscription.cardId': subscription.cardId,
        },
      });
      if (chargify.current) {
        setFormLoading(true);
        chargify.current.unload();
        chargify.current = null;
      }
      onSubmit(token);
    };

    const errorCallback = (error) => {
      setFormLoading(false);
      Analytics.track({
        event: Analytics.events.accountPaymentMethodUpdateError,
        subscription,
        error,
      });
      if (error.errors) setErrorMessage(error.errors);
    };

    chargify.current.token(chargifyForm.current, successCallback, errorCallback);
  };

  const handleCloseModal = () => {
    handleClose();
    if (chargify.current) {
      chargify.current.unload();
      chargify.current = null;
    }
  };

  useEffect(() => {
    if (chargify.current || !securityToken) {
      return;
    }

    chargify.current = new window.Chargify();

    const params = {
      type: 'card',
      publicKey,
      serverHost: siteUrl,
      addressDropdowns: true,
      securityToken,
      selector: formSignifier,
      style: {
        field: {
          margin: 0,
        },
        label: {
          margin: 0,
        },
      },
      fields: {
        number: {
          selector: '#chargifyNumber',
          label: `${i18n('entities.subscription.billing.form.number')}:`,
          message: i18n('entities.subscription.billing.formError.number'),
          style: {
            field: {},
            input: { borderRadius: '2px 2px 2px 2px' },
            label: { fontSize: '14px' },
            message: { paddingTop: '1px', paddingBottom: '1px', fontSize: '14px', color: '#f5222d' },
          },
        },
        month: {
          selector: '#chargifyMonth',
          label: `${i18n('entities.subscription.billing.form.expiration')}:`,
          message: i18n('entities.subscription.billing.formError.month'),
          style: {
            field: {},
            input: { borderRadius: '2px 0px 0px 2px', borderRight: '1px solid transparent' },
            label: { fontSize: '14px' },
            message: { paddingTop: '1px', paddingBottom: '1px', fontSize: '14px', color: '#f5222d' },
          },
        },
        year: {
          selector: '#chargifyYear',
          label: ` `,
          message: i18n('entities.subscription.billing.formError.year'),
          style: {
            field: {},
            input: { borderRadius: '0px 2px 2px 0px' },
            label: { fontSize: '14px', height: '24px' },
            message: { paddingTop: '1px', paddingBottom: '1px', fontSize: '14px', color: '#f5222d' },
          },
        },
        cvv: {
          selector: '#chargifyCvv',
          label: `${i18n('entities.subscription.billing.form.cvv')}:`,
          message: i18n('entities.subscription.billing.formError.cvv'),
          required: true,
          style: {
            field: {},
            input: { borderRadius: '2px 2px 2px 2px' },
            label: { fontSize: '14px' },
            message: { paddingTop: '1px', paddingBottom: '1px', fontSize: '14px', color: '#f5222d' },
          },
        },
        firstName: {
          selector: '#chargifyFirstName',
          label: `${i18n('entities.subscription.billing.form.firstName')}:`,
          message: i18n('entities.subscription.billing.formError.firstName'),
          required: true,
          placeholder: ' ',
          style: {
            field: {},
            input: { borderRadius: '2px 2px 2px 2px' },
            label: { fontSize: '14px' },
            message: { paddingTop: '1px', paddingBottom: '1px', fontSize: '14px', color: '#f5222d' },
          },
        },
        lastName: {
          selector: '#chargifyLastName',
          label: `${i18n('entities.subscription.billing.form.lastName')}:`,
          message: i18n('entities.subscription.billing.formError.lastName'),
          required: true,
          placeholder: ' ',
          style: {
            field: {},
            input: { borderRadius: '2px 2px 2px 2px' },
            label: { fontSize: '14px' },
            message: { paddingTop: '1px', paddingBottom: '1px', fontSize: '14px', color: '#f5222d' },
          },
        },
        address: {
          selector: '#chargifyAddressLine1',
          label: `${i18n('entities.subscription.billing.form.address')}:`,
          message: i18n('entities.subscription.billing.formError.address'),
          required: true,
          style: {
            field: {},
            input: { borderRadius: '2px 2px 2px 2px' },
            label: { fontSize: '14px' },
            message: { paddingTop: '1px', paddingBottom: '1px', fontSize: '14px', color: '#f5222d' },
          },
        },
        address2: {
          selector: '#chargifyAddressLine2',
          label: `${i18n('entities.subscription.billing.form.address2')}:`,
          message: i18n('entities.subscription.billing.formError.address2'),
          style: {
            field: {},
            input: { borderRadius: '2px 2px 2px 2px' },
            label: { fontSize: '14px' },
            message: { paddingTop: '1px', paddingBottom: '1px', fontSize: '14px', color: '#f5222d' },
          },
        },
        city: {
          selector: '#chargifyCity',
          label: `${i18n('entities.subscription.billing.form.city')}:`,
          message: i18n('entities.subscription.billing.formError.city'),
          required: true,
          style: {
            field: {},
            input: { borderRadius: '2px 2px 2px 2px' },
            label: { fontSize: '14px' },
            message: { paddingTop: '1px', paddingBottom: '1px', fontSize: '14px', color: '#f5222d' },
          },
        },
        state: {
          selector: '#chargifyState',
          label: `${i18n('entities.subscription.billing.form.state')}:`,
          message: i18n('entities.subscription.billing.formError.state'),
          required: true,
          placeholder: ' ',
          style: {
            field: {},
            input: { borderRadius: '2px 2px 2px 2px' },
            label: { fontSize: '14px' },
            message: { paddingTop: '1px', paddingBottom: '1px', fontSize: '14px', color: '#f5222d' },
          },
        },
        zip: {
          selector: '#chargifyZip',
          label: `${i18n('entities.subscription.billing.form.zip')}:`,
          message: i18n('entities.subscription.billing.formError.zip'),
          required: true,
          style: {
            field: {},
            input: { borderRadius: '2px 2px 2px 2px' },
            label: { fontSize: '14px' },
            message: { paddingTop: '1px', paddingBottom: '1px', fontSize: '14px', color: '#f5222d' },
          },
        },
        country: {
          selector: '#chargifyCountry',
          label: `${i18n('entities.subscription.billing.form.country')}:`,
          message: i18n('entities.subscription.billing.formError.country'),
          required: true,
          placeholder: ' ',
          style: {
            field: {},
            input: { borderRadius: '2px 2px 2px 2px' },
            label: { fontSize: '14px' },
            message: { paddingTop: '1px', paddingBottom: '1px', fontSize: '14px', color: '#f5222d' },
          },
        },
      },
    };

    chargify.current.load(params).then((result) => {
      setFormLoading(false);
    });
  }, []);

  return (
    <section className="card-form-container">
      {formLoading && (
        <div className="form-spinner">
          <Spinner style={{ height: '100%', margin: '0' }} />
        </div>
      )}
      <div className={`chargify-js-wrapper ${formLoading ? 'chargify-hidden' : ''}`}>
        <div className="chargify-js-content">
          <form id={formSignifier} ref={chargifyForm}>
            <div className="submission-error">
              {errorMessage && (
                <span>
                  <WarningOutlined />
                  {i18n('entities.subscription.billing.formError.card', errorMessage)}
                </span>
              )}
            </div>
            <div className="card-front" ref={cardRef}>
              <div id="chargifyNumber" />
            </div>
            <div id="chargifyCardBack" className="card-back">
              <div id="chargifyExpiration">
                <div id="chargifyMonth" />
                <div id="chargifyYear" />
              </div>
              <div id="chargifyCvv" />
            </div>
            <div className="name">
              <div id="chargifyFirstName" />
              <div id="chargifyLastName" />
            </div>
            <div className="addressCountry">
              <div id="chargifyCountry" />
            </div>
            <div className="address1">
              <div id="chargifyAddressLine1" />
            </div>
            <div className="address2">
              <div id="chargifyAddressLine2" />
            </div>
            <div className="addressCityStateZip">
              <div id="chargifyCity" />
              <div id="chargifyState" />
              <div id="chargifyZip" />
            </div>
            <div className="cardButtons">
              <Button id="card-cancel" size="large" onClick={handleCloseModal}>
                {i18n('common.cancel')}
              </Button>
              <Button id="card-submit" type="primary" size="large" className="btn-primary" loading={loading} onClick={handleSubmit}>
                {i18n('entities.subscription.billing.form.submit')}
              </Button>
            </div>
          </form>
        </div>
      </div>
    </section>
  );
};

export default CreditCardForm;
