const TagStyles: { [key: string]: string } = {
  active: 'green',
  deleted: 'red',
  inactive: 'blue',
  Ready_for_Review: 'geekblue',
  Verified: 'green',
  Unverified: 'red',
  None: 'geekblue',
};

export default TagStyles;
