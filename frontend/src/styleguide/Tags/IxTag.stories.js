import React from 'react';
import { storiesOf } from '@storybook/react';
import IxTag from './IxTag';
import MDX from './IxTag.mdx';
import { specs } from 'storybook-addon-specifications';

const stories = storiesOf('Design System/Tag', module);

stories.add(
  'Status',
  () => {
    const story = (
      <>
        <IxTag type="status" value="active" />
        <IxTag type="status" value="deleted" />
        <IxTag type="status" value="inactive" />
      </>
    );

    return story;
  },
  {
    docs: { page: MDX },
  },
);
