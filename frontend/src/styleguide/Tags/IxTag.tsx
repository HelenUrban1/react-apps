import React from 'react';
import { Tag } from 'antd';
import TagStyles from './TagStyles';
import { i18n } from '../../i18n';

interface Props {
  value: string;
  restricted?: boolean;
  className?: string;
}

const IxTag: React.FC<Props> = ({ value, restricted, className }) => {
  const test = value[0].toUpperCase() + value.slice(1);
  if (restricted) {
    if (value === i18n('entities.part.enumerators.accessControl.ITAR_TEXT')) {
      return (
        <Tag className={className || ''} color={TagStyles[value]}>
          {i18n(`entities.part.enumerators.accessControl.restricted`)}
        </Tag>
      );
    }
    return (
      <Tag className={className || ''} color={TagStyles[value]}>
        {i18n(`entities.part.enumerators.accessControl[${test}]`)}
      </Tag>
    );
  }
  return (
    <Tag className={className || ''} color={TagStyles[value]}>
      {i18n(`entities.part.enumerators.reviewStatus[${test}]`)}
    </Tag>
  );
};

export default IxTag;
