import React from 'react';
import { storiesOf } from '@storybook/react';
import { withKnobs } from '@storybook/addon-knobs';
import Toolbar from './Toolbar';

const stories = storiesOf('Design System/Iconbar', module).addDecorator(withKnobs);

stories.add('Action Buttons', () => {
  const story = <Toolbar pdfTools={[]} documentTools={[]} extractTools={[]} />;

  return story;
});
