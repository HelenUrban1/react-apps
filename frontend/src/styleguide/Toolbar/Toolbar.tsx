import { Popconfirm } from 'antd';
import { deleteTipTitle } from 'domain/part/edit/characteristics/utils/uiUtil';
import { AppState } from 'modules/state';
import React, { ReactNode, useCallback, useEffect, useMemo, useState } from 'react';
import { useSelector } from 'react-redux';
import { Annotation } from 'types/annotation';
import { Characteristic } from 'types/characteristics';
import './Toolbar.less';

interface ToolbarProps {
  extractTools: ReactNode[];
  pdfTools: ReactNode[];
  documentTools: ReactNode[];
  deleteActive: () => void;
}

export const Toolbar = ({ extractTools, pdfTools, documentTools, deleteActive }: ToolbarProps) => {
  const { selected } = useSelector((state: AppState) => state.characteristics);
  const { pdfView, pdfOverlayView } = useSelector((state: AppState) => state);
  const { layerManager } = pdfOverlayView.revisionFile ? pdfOverlayView : pdfView;
  const [showDelete, setShowDelete] = useState(false);

  const title = useMemo(() => {
    const selectedFeatures: Characteristic[] = selected.filter((s) => !!s.quantity) as Characteristic[];
    const selectedAnnotations: Annotation[] = selected.filter((s) => !!s.backgroundColor) as Annotation[];
    return deleteTipTitle(selectedFeatures, selectedAnnotations);
  }, [selected]);

  const handleDeleteKeyPressed = (e: any) => {
    const activeObject = layerManager.layers?.markers?.canvas?.getActiveObject();
    if (selected.length > 0 && e.target.type !== 'text' && e.target.type !== 'textarea' && (!activeObject || (activeObject && activeObject.type !== 'annotation-text'))) {
      setShowDelete(true);
    }
  };

  // TODO: This should be handled in the hotkey story when/if that gets picked up (IXC-478)
  const handleKeyPress = useCallback(
    (e: any) => {
      if ((e.key === 'Backspace' || e.key === 'Delete') && !e.shiftKey && !e.ctrlKey) {
        handleDeleteKeyPressed(e);
      }
    },
    [selected],
  );

  useEffect(() => {
    // Handles keypress when focues outside of the canvas
    document.addEventListener('keyup', handleKeyPress);

    // PDFViewer intercepts events when in focus, need to subscribe here
    if (layerManager) {
      layerManager.subscribe('Toolbar', 'canvasKeypress', (e: any) => {
        if ((e.event.key === 'Backspace' || e.event.key === 'Delete') && !e.event.shiftKey && !e.event.ctrlKey) {
          handleDeleteKeyPressed(e.event);
        }
      });
    }

    return () => {
      document.removeEventListener('keyup', handleKeyPress);
    };
  }, [selected, layerManager]);

  const handleConfirmDelete = () => {
    setShowDelete(false);
    deleteActive();
  };

  const handleCancelDelete = () => {
    setShowDelete(false);
  };

  return (
    <Popconfirm title={title} placement="bottom" visible={showDelete} onConfirm={handleConfirmDelete} onCancel={handleCancelDelete}>
      <div className="ixc-full-toolbar-container" key="extract-toolbar">
        {extractTools.length > 0 && (
          <section className="ixc-toolbar-tools extract-tools" key="extract-tools">
            {extractTools.map((tool) => tool)}
          </section>
        )}
        <section className="ixc-toolbar-tools document-tools" key="document-tools">
          {documentTools.map((tool) => tool)}
        </section>
        <section className="ixc-toolbar-tools pdf-tools" key="pdf-tools">
          {pdfTools.map((tool) => tool)}
        </section>
      </div>
    </Popconfirm>
  );
};
