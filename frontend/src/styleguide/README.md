# The InspectionXpert design system

The InspectionXpert design system is a subset of the full [Ant Design component library](https://ant.design/docs/react/introduce).

## Structure

Components are created within folders that share the same name under the src directory.

Files in the component Folder:

- component.js (component file)
- component.stories.js (component storybook file)
- component.mdx (markup file for storybook documentation)
- component.test.js (tests in jest+cypress)
- component.less (component's styles)

```
-src
    -component
        -component.js
        -component.stories.js
        -component.mdx
        -component.test.js
        -component.less (optional)
```

## Build and Run

- To build Storybook: `npm run build-storybook`
- To build Storybook docs: `npm run build-storybook-docs`
- To build and run: `npm run storybook`

## Testing

To run the full suite of jest and snapshot tests run the command: `npm run storybook:test`

Tests are written in `*.test.js` files within each component's folder. And additinal `*Tests.js` file is created which exports
tests as modules. These modules are imported into the component Stories where they are run with the specifications plugin.
It isn't required that the `*Tests.js` file is created and added to the stories, but it does add an easy to find visual for checking
test results when running Storybook.

Additionally component's Story is run through the Storybook snapshot and image snapshot plugins to check for structural and visual
regressions. The configuration for this is already set and doesn't require any setup when creating components.
