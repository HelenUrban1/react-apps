import React from 'react';
import { List } from 'antd';
import { ReportTemplate } from 'types/reportTemplates';
import './TemplateCard.less';
// import { getInitials } from 'utils/textOperations';
import { ConfirmDelete } from 'view/global/confirmDelete';
import { IxAvatar } from 'styleguide/Avatar/IxAvatar';
import Icon from 'styleguide/Icons/Icon';

interface Props {
  temp: ReportTemplate;
  active?: string;
  defaults?: boolean;
  onClick?: (e: any) => void;
  create?: () => void;
  copy?: () => void;
  edit?: () => void;
  remove?: () => void;
}

const TemplateCard = ({ temp, active, defaults, onClick, create, copy, edit, remove }: Props) => {
  if (temp.id === 'new') {
    return (
      <div className="card-button" onClick={create} data-cy="template_new">
        <List.Item className="template-card">
          <div className="card-icon">
            <div className="add-template">
              <Icon icon="plus" block />
            </div>
          </div>
          <div className="card-content">{temp.title}</div>
        </List.Item>
      </div>
    );
  }
  if (!temp.provider) {
    return <List.Item className="template-card" />;
  }
  const logo = temp.provider.orgLogo && temp.provider.orgLogo.length > 0;

  return (
    <div className={`card-button${active && active === temp.id ? ' active' : ''}${defaults ? ' default' : ''}`} onClick={onClick} data-cy={`template_card_${temp.id}`}>
      <List.Item className="template-card">
        <div className="card-icon" style={!logo ? { backgroundColor: '#685BC7', color: 'white' } : undefined} data-cy="template_card_icon">
          {temp.provider.orgLogo && temp.provider.orgLogo.length > 0 && <img src={temp.provider.orgLogo[0].publicUrl} alt={`${temp.provider.name} Logo`} />}
          {/* {!logo && <span className="initials">{getInitials(temp.provider.name)}</span>} */}
          {!logo && temp.provider && temp.provider.name && <IxAvatar type="company" size="default" data={temp.provider} />}
          {/* TODO: swap out with provider Org object */}
        </div>
        <div className="card-content">
          <strong data-cy="template_card_title">{temp.title}</strong>
          <div className="card-actions">
            {copy && (
              <button type="button" className="icon-button" onClick={copy} data-cy="template_card_copy">
                <Icon icon="copy" block />
              </button>
            )}
            {edit && (
              <button type="button" className="icon-button" onClick={edit} data-cy="template_card_edit">
                <Icon icon="edit" block />
              </button>
            )}
            {remove && <ConfirmDelete data-cy="template_card_delete" classes="icon-button report-delete" confirm={remove || (() => {})} colored={false} />}
          </div>
          {temp.provider.name && (
            <>
              <small data-cy="template_card_provider">{`Provided by ${temp.provider.name}`}</small>
            </>
          )}
        </div>
      </List.Item>
    </div>
  );
};

export default TemplateCard;
