import React, { useState } from 'react';
import { InfoCircleOutlined } from '@ant-design/icons';
import { Select, Tooltip } from 'antd';
import { SelectValue } from 'antd/lib/select';
import { ListObjectItem } from 'domain/setting/listsTypes';
import { i18n } from 'i18n';
import { DataType, getMeasurementUnits, MeasurementOption } from 'utils/Lists';
import { toDisplayFormat } from 'utils/textOperations';

const { Option } = Select;

interface MeasurementSelectProps {
  handleEditMeasurement: (listItem: ListObjectItem, measurement: string) => void;
  data: DataType;
  measurementOptions: MeasurementOption[];
}

export const MeasurementSelect = ({ handleEditMeasurement, data, measurementOptions }: MeasurementSelectProps) => {
  const [measurement, setMeasurement] = useState<string | undefined>(data.obj.measurement || data.obj.meta?.measurement);

  const getTipBody = (selectedMeasurement: string | undefined) => {
    const { metric, imperial, both } = getMeasurementUnits(measurementOptions, selectedMeasurement || data.obj.meta?.measurement);
    if (metric.length + imperial.length + both.length === 0) {
      return (
        <div className="unit-tip-container" key={`${data.id}-tip-key`}>
          <div className="unit-tip-imperial">
            <div>{i18n('common.none')}</div>
          </div>
        </div>
      );
    }
    return (
      <div className="unit-tip-container" data-cy="unit-tip-container" key={`${data.id}-tip-key`}>
        {metric.length > 0 && (
          <div className="unit-tip-metric">
            <div className="unit-list-header">{i18n('common.metric')}</div>
            <ul className="unit-list">
              {metric.map((unit: string) => {
                return (
                  <li className="unit-list-item" key={`${data.id}-${unit}-key`}>
                    {unit}
                  </li>
                );
              })}
            </ul>
          </div>
        )}
        {metric.length > 0 && imperial.length > 0 && <hr className="vertical" />}
        {imperial.length > 0 && (
          <div className="unit-tip-imperial">
            <div className="unit-list-header">{i18n('common.imperial')}</div>
            <ul className="unit-list">
              {imperial.map((unit: string) => {
                return (
                  <li className="unit-list-item" key={`${data.id}-${unit}-key`}>
                    {unit}
                  </li>
                );
              })}
            </ul>
          </div>
        )}
        {(metric.length > 0 || imperial.length > 0) && both.length > 0 && <hr className="vertical" />}
        {both.length > 0 && (
          <div className="unit-tip-both">
            <div className="unit-list-header">Metric/Imperial</div>
            <ul className="unit-list">
              {both.map((unit: string) => {
                return (
                  <li className="unit-list-item" key={`${data.id}-${unit}-key`}>
                    {unit}
                  </li>
                );
              })}
            </ul>
          </div>
        )}
      </div>
    );
  };

  const changeMeasurement = (listItem: ListObjectItem, value: string | undefined) => {
    if (value) {
      setMeasurement(value);
      handleEditMeasurement(listItem, value);
    }
  };

  return (
    <div className="measurement-select-container" data-cy="measurement-select-container">
      <div className="select-div">
        <Select
          size="small"
          className="measurement-select"
          showSearch
          data-cy="measurement-select"
          dropdownMatchSelectWidth={false}
          value={measurement || i18n('common.none')}
          onChange={(value: SelectValue) => changeMeasurement(data.obj, value?.toString())}
        >
          {measurementOptions.map((measurementOption: MeasurementOption) => {
            return (
              <Option className={`${measurementOption.name}-select-option`} data-cy={`${measurementOption.name}-select-option`} key={`${measurementOption.name}-select-key`} value={measurementOption.name || ''}>
                {toDisplayFormat(measurementOption.name)}
              </Option>
            );
          })}
        </Select>
      </div>
      <div className="unit-div">
        <Tooltip arrowPointAtCenter title={getTipBody(measurement)} placement="rightTop">
          {measurement && <InfoCircleOutlined data-cy="measurement-units" />}
        </Tooltip>
      </div>
    </div>
  );
};
