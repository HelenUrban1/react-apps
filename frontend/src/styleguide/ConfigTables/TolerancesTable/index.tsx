import { CaretDownOutlined, CaretRightOutlined, InfoCircleOutlined } from '@ant-design/icons';
import { Radio, RadioChangeEvent, Table, Tooltip } from 'antd';
import { ToleranceSection } from 'domain/part/edit/defaultTolerances/toleranceSection';
import { LoadedPresets, SettingPresets } from 'domain/setting/settingTypes';
import { i18n } from 'i18n';
import { AppState } from 'modules/state';
import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import AddOptionButton from 'styleguide/Buttons/AddOption';
import { Tolerance } from 'types/tolerances';
import { ConfirmDelete } from 'view/global/confirmDelete';
import { CustomerMultiSelect } from 'view/global/CustomerMultiSelect';
import { NameInput } from '../StylesTable/NameInput';
import 'domain/part/edit/defaultTolerances/toleranceStyles.less';
import './tolerancesTable.less';
import Analytics from 'modules/shared/analytics/analytics';
import { TolerancePresetModal } from 'domain/part/edit/defaultTolerances/TolerancePresetModal';
import { DefaultTolerances, defaultUnitIDs } from 'view/global/defaults';
import { buildNewTolerances } from 'domain/part/edit/defaultTolerances/TolerancesLogic';

interface ListItem {
  id: string;
  value: string;
}

interface ToleranceTableProps {
  handleUpdatePreset: (presetObj: SettingPresets, loadedPresets: LoadedPresets | undefined) => void;
  handleUpdatePresetName: (presetName: string, newName: string, loadedPresets: LoadedPresets | undefined) => void;
}

export const TolerancesTable = ({ handleUpdatePreset, handleUpdatePresetName }: ToleranceTableProps) => {
  const sessionData = useSelector((state: AppState) => state.session);
  const { settings, metricUnits, imperialUnits } = sessionData;
  const [data, setData] = useState<any[]>();
  const [showModal, setShowModal] = useState(false);
  const [metUnits, setMetUnits] = useState<{
    linear: ListItem[];
    angular: ListItem[];
  }>({ linear: [], angular: [] });
  const [impUnits, setImpUnits] = useState<{
    linear: ListItem[];
    angular: ListItem[];
  }>({ linear: [], angular: [] });

  const handleSetUnits = (type: 'Metric' | 'Imperial') => {
    const linear: ListItem[] = [];
    const angular: ListItem[] = [];
    const units = type === 'Metric' ? metricUnits : imperialUnits;
    Object.values(units || {}).forEach((unit) => {
      if (unit.meta?.measurement.includes('angle')) {
        angular.push({ id: unit.id, value: unit.meta?.unit || unit.value });
      } else if (unit.meta?.measurement.includes('length')) {
        linear.push({ id: unit.id, value: unit.meta?.unit || unit.value });
      }
    });
    if (type === 'Metric') {
      setMetUnits({ linear, angular });
    } else {
      setImpUnits({ linear, angular });
    }
  };

  useEffect(() => {
    handleSetUnits('Metric');
    handleSetUnits('Imperial');
  }, [metricUnits, imperialUnits]);

  const handleDeleteTolerancePreset = (presetName: any) => {
    const presetObj = settings.tolerancePresets ? { ...settings.tolerancePresets.metadata } : {};
    Analytics.track({
      event: Analytics.events.deletedTolerancePreset,
      preset: {
        name: presetName,
        ...presetObj[presetName],
      },
    });
    delete presetObj[presetName];
    handleUpdatePreset(presetObj, settings.tolerancePresets);
  };

  const handleCreatePreset = (value: string) => {
    const presetObj = settings.tolerancePresets ? { ...settings.tolerancePresets.metadata } : {};
    presetObj[value] = {
      customers: [],
      setting: DefaultTolerances,
    };
    handleUpdatePreset(presetObj, settings.tolerancePresets);
    setShowModal(false);
    Analytics.track({
      event: Analytics.events.createdStylePresetFromConfig,
      preset: {
        name: value,
        ...presetObj[value],
      },
    });
  };

  const handleUpdateTolerancePreset = ({ section, action, value, index, input }: { section: string; action: string; value?: string; index?: number; input?: string }, tols: Tolerance, presetName: string) => {
    const { newTolerances } = buildNewTolerances({ section, action, value, index, input, tolerances: tols });
    // handle update
    const presetObj = settings.tolerancePresets ? { ...settings.tolerancePresets.metadata } : {};
    if (presetObj[presetName]) {
      presetObj[presetName].setting = newTolerances;
      handleUpdatePreset(presetObj, settings.tolerancePresets);
    }
  };

  const handleUpdateName = (presetName: string, newName: string) => {
    handleUpdatePresetName(presetName, newName, settings.tolerancePresets);
  };

  const handleChangeCustomer = (value: string[], presetName: string) => {
    const presetObj = settings.tolerancePresets ? { ...settings.tolerancePresets.metadata } : {};
    presetObj[presetName].customers = value;
    handleUpdatePreset(presetObj, settings.tolerancePresets);
  };

  const handleChangeMeasurement = (e: RadioChangeEvent, tolerances: Tolerance, presetName: string) => {
    // handle
    const newTols = { ...tolerances };
    if (e.target.value === 'Metric') {
      newTols.linear.unit = defaultUnitIDs.millimeter;
    } else {
      newTols.linear.unit = defaultUnitIDs.inch;
    }
    const presetObj = settings.tolerancePresets ? { ...settings.tolerancePresets.metadata } : {};
    if (presetObj[presetName]) {
      presetObj[presetName].setting = newTols;
      handleUpdatePreset(presetObj, settings.tolerancePresets);
    }
  };

  const handleOpenPresetModal = () => {
    setShowModal(true);
  };

  const isMetric = (tolerances: Tolerance) => {
    const unit = metricUnits ? metricUnits[tolerances.linear.unit] : undefined;
    return !!unit;
  };

  const buildTolerancesData = () => {
    const datas: any[] = [];
    const presetObj: SettingPresets = settings.tolerancePresets ? { ...settings.tolerancePresets.metadata } : {};

    Object.keys(presetObj)
      .sort((a: string, b: string) => (a === 'Default' ? -1 : 1))
      .forEach((key) => {
        const tolerances = presetObj[key].setting as Tolerance;
        datas.push({
          key,
          id: `tolerance-preset-row-${key}`,
          name: key === 'Default' ? key : <NameInput name={key} presets={settings.tolerancePresets} handleUpdateName={(val: string) => handleUpdateName(key, val)} />,
          measurement: (
            <Radio.Group defaultValue={isMetric(tolerances) ? 'Metric' : 'Imperial'} onChange={(e: RadioChangeEvent) => handleChangeMeasurement(e, tolerances, key)}>
              <Radio value="Metric">Metric</Radio>
              <Radio value="Imperial">Imperial (US/UK)</Radio>
            </Radio.Group>
          ),
          tolerances: (
            <section className="tolerance-tables">
              <ToleranceSection
                title="Linear"
                type={tolerances.linear.type}
                presetName={key}
                rangeTols={tolerances.linear.rangeTols}
                precisionTols={tolerances.linear.precisionTols}
                unit={tolerances.linear.unit}
                units={isMetric(tolerances) ? { linear: metUnits.linear, angular: metUnits.angular } : { linear: impUnits.linear, angular: impUnits.angular }}
                update={({ section, action, value, index, input }) => handleUpdateTolerancePreset({ section, action, value, index, input }, tolerances, key)}
                change={() => {}}
              />
              <div className="tables-spacer" />
              <ToleranceSection
                title="Angular"
                type={tolerances.angular.type}
                presetName={key}
                rangeTols={tolerances.angular.rangeTols}
                precisionTols={tolerances.angular.precisionTols}
                unit={tolerances.angular.unit}
                units={isMetric(tolerances) ? { linear: metUnits.linear, angular: metUnits.angular } : { linear: impUnits.linear, angular: impUnits.angular }}
                update={({ section, action, value, index, input }) => handleUpdateTolerancePreset({ section, action, value, index, input }, tolerances, key)}
                change={() => {}}
              />
            </section>
          ),
          customers:
            key === 'Default' ? (
              <></>
            ) : (
              <CustomerMultiSelect
                value={presetObj[key].customers || ''}
                onChange={(value: string[]) => {
                  handleChangeCustomer(value, key);
                }}
              />
            ),
        });
      });
    return datas;
  };

  useEffect(() => {
    setData(buildTolerancesData());
  }, [settings.tolerancePresets, metUnits, impUnits]);

  const columns: any[] = [
    {
      width: 40,
      dataIndex: 'key',
      className: 'ix-table-cell actions testing-part-actions-header',
      render: (record: any) => <span className="tolerance-preset-actions">{record !== 'Default' && <ConfirmDelete classes="icon-button part-delete" confirm={() => handleDeleteTolerancePreset(record)} />}</span>,
    },
    {
      title: (
        <span>
          {i18n('settings.custom.presets.styles.name')}
          <Tooltip placement="top" title={i18n('settings.custom.presets.styles.nameTip')}>
            <InfoCircleOutlined className="span-icon" />
          </Tooltip>
        </span>
      ),
      dataIndex: 'name',
      key: 'name',
    },
    {
      title: (
        <span>
          {i18n('settings.custom.presets.tolerances.measurement')}
          <Tooltip placement="top" title={i18n('settings.custom.presets.tolerances.measurementTip')}>
            <InfoCircleOutlined className="span-icon" />
          </Tooltip>
        </span>
      ),
      dataIndex: 'measurement',
      key: 'measurement',
    },
    {
      title: (
        <span>
          {i18n('settings.custom.presets.styles.customers')}
          <Tooltip placement="top" title={i18n('settings.custom.presets.styles.customersTip')}>
            <InfoCircleOutlined className="span-icon" />
          </Tooltip>
        </span>
      ),
      dataIndex: 'customers',
      key: 'customers',
    },
  ];

  const customExpandIcon = (props: any) => {
    if (!props.expandable) return <></>;
    if (props.expanded) {
      return (
        <CaretDownOutlined
          className="expand-icon"
          onClick={(e) => {
            props.onExpand(props.record, e);
          }}
        />
      );
    }
    return (
      <CaretRightOutlined
        className="expand-icon"
        onClick={(e) => {
          props.onExpand(props.record, e);
        }}
      />
    );
  };

  return (
    <div id="tolerances-presets-table" className="tolerances-table-container">
      <TolerancePresetModal showModal={showModal} presets={settings.tolerancePresets} handleCreatePreset={handleCreatePreset} setShowModal={setShowModal} />
      <Table
        rowKey="id"
        className="config-table"
        key="tolerances-config-table"
        data-cy="tolerances-config-table"
        size="middle"
        pagination={false}
        columns={columns}
        expandable={{
          expandedRowRender: (record) => record.tolerances,
          rowExpandable: (record) => true,
        }}
        expandIcon={customExpandIcon}
        dataSource={data}
      />
      <AddOptionButton add={handleOpenPresetModal} option={i18n('settings.custom.presets.tolerances.tolerancePreset')} />
    </div>
  );
};
