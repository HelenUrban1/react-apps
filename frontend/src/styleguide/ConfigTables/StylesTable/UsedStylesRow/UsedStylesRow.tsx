import React, { useEffect, useState } from 'react';
import { Button, Popover, Tooltip } from 'antd';
import { PlusCircleTwoTone } from '@ant-design/icons';
import { BaseOptionType } from 'antd/lib/cascader';
import { BalloonOption } from 'domain/part/edit/balloonStyles/BalloonOption';
import { BalloonPreview } from 'domain/part/edit/balloonStyles/BalloonPreview';
import { i18n } from 'i18n';
import { Marker, MarkerStyle } from 'types/marker';
import { convertListObjectToCascaderOptions } from 'utils/ConvertCascader';
import { getCurrentList, getListNameById, getValueOfHiddenType } from 'utils/Lists';
import { useSelector } from 'react-redux';
import { AppState } from 'modules/state';
import 'domain/part/edit/balloonStyles/balloonStyles.less';
import './usedStylesRow.less';

interface StylesProps {
  usedStyles: { [key: string]: MarkerStyle } | null;
  markers: Marker[] | null;
  update?: ({ index, id, assign }: { index: number | string; id?: string; assign?: string[] }, del?: boolean) => void;
  add?: () => void;
  canDelete?: boolean;
}

export const UsedStylesRow = ({ usedStyles, markers, update, add, canDelete = false }: StylesProps) => {
  const { types, classifications, methods, operations } = useSelector((state: AppState) => state.session);
  const [applyOptions, setOptions] = useState<BaseOptionType[]>([]);
  const [usedAssignments, setAssigned] = useState<BaseOptionType[]>(usedStyles ? Object.values(usedStyles).map((item) => item.assign) : []);
  const [usedIds, setUsedIds] = useState<string[]>(usedStyles ? [...Object.values(usedStyles).map((item) => item.style)] : ['f12c5566-7612-48e9-9534-ef90b276ebaa']);

  useEffect(() => {
    // updated used styles array
    setUsedIds(usedStyles ? [...Object.values(usedStyles).map((item) => item.style)] : ['f12c5566-7612-48e9-9534-ef90b276ebaa']);
  }, [usedStyles]);

  useEffect(() => {
    const options: BaseOptionType[] = [];
    if (classifications) {
      const classificationOptions = convertListObjectToCascaderOptions(classifications, i18n('entities.feature.fields.criticality'), usedAssignments);
      if (classificationOptions) {
        options.push(classificationOptions);
      }
    }
    if (types) {
      const typesOptions = convertListObjectToCascaderOptions(types, i18n('entities.feature.fields.notationType'), usedAssignments);
      if (typesOptions) {
        options.push(typesOptions);
      }
    }
    if (operations) {
      const operationsOptions = convertListObjectToCascaderOptions(operations, i18n('entities.feature.fields.operation'), usedAssignments);
      if (operationsOptions) {
        options.push(operationsOptions);
      }
    }
    if (methods) {
      const methodsOptions = convertListObjectToCascaderOptions(methods, i18n('entities.feature.fields.inspectionMethod'), usedAssignments);
      if (methodsOptions) {
        options.push(methodsOptions);
      }
    }
    setOptions(options);
  }, [usedAssignments, types, classifications]);

  const findMarker = (ind: string) => {
    const used = usedStyles ? usedStyles[ind].style : '';
    return markers?.find((marker) => marker.id === used);
  };

  const getAssignment = (ind: string) => {
    const used = usedStyles ? usedStyles[ind].assign : [''];
    if (!used[0]) return i18n('settings.custom.ballooning.styles.unassigned');
    if (used[0]?.toLocaleLowerCase() === 'default') return 'Default';
    const currentList = getCurrentList(used[0], classifications, methods, types, operations, null, null);
    const id = used[2] ? used[2] : used[1];
    const parentId = used[2] ? used[1] : undefined;
    return getListNameById({ list: currentList, value: id, parentId, defaultValue: '' });
  };

  const handleDeleteFromPreset = (e: any, id: string, index: string) => {
    e.preventDefault();
    if (update) update({ index, id, assign: undefined }, true);
  };

  const updateOption = ({ index, id, assign }: { index: number | string; id?: string; assign?: string[] }) => {
    if (update) update({ index, id, assign });
  };

  const addOption = () => {
    if (add) add();
  };

  // Only show final assignment selection in dropdown
  const displayRender = (label: string[], selectedOptions: string[] | undefined) => {
    const currentList = getCurrentList(label[0], classifications, methods, types, operations, null, null);
    const newLabel = [...label];
    let first = label[1] ? label[1] : 'Not found';
    let second = label[2] ? label[2] : 'Not found';
    if (label[0] === 'Type') {
      if (!label[2] && currentList && selectedOptions) {
        const labels = getValueOfHiddenType(currentList, selectedOptions);
        first = labels.type;
        second = labels.subtype;
      }
    } else if (!label[1] && currentList && selectedOptions) {
      const labels = getValueOfHiddenType(currentList, [selectedOptions[1]]);
      first = labels.type;
      second = labels.subtype;
      newLabel[1] = first;
      if (label[0] === 'Type') newLabel[2] = second;
    }
    return <span key={label.join(' / ')}>{newLabel[newLabel.length - 1]}</span>;
  };

  return (
    <section className="used-styles-preview">
      {Object.keys(usedStyles || {})
        .sort((a: string, b: string) => {
          if (a === 'default') return -1;
          if (b === 'default') return 1;
          return a > b ? 1 : -1;
        })
        .map((item, index) => {
          const style = findMarker(item);
          if (style) {
            return (
              <section key={`style-${style.id}`} className="balloon-preview-container">
                {update ? (
                  <Popover
                    title={
                      <section className="style-popover-title">
                        <p>{i18n('settings.custom.presets.styles.styleAssignment')}</p>
                      </section>
                    } //
                    trigger="click"
                    content={
                      <BalloonOption
                        key={item}
                        index={item}
                        cascaderOptions={applyOptions}
                        usedStyles={usedIds}
                        defaultBalloon={style.id}
                        update={updateOption}
                        style={style.id}
                        assign={usedStyles ? usedStyles[item].assign : []}
                        displayRender={displayRender}
                        id={style.id}
                      />
                    }
                  >
                    <Button className="edit-style-btn">
                      <BalloonPreview style={style} tip={getAssignment(item)} index={item} canDelete={canDelete && item.toLocaleLowerCase() !== 'default'} handleDeleteStyle={handleDeleteFromPreset} />
                    </Button>
                  </Popover>
                ) : (
                  <BalloonPreview style={style} tip={getAssignment(item)} index={item} canDelete={canDelete && item.toLocaleLowerCase() !== 'default'} handleDeleteStyle={handleDeleteFromPreset} />
                )}
              </section>
            );
          }
          return <></>;
        })}
      {update && (
        <Tooltip placement="top" title={i18n('settings.custom.presets.styles.addBalloon')}>
          <PlusCircleTwoTone twoToneColor="#1890ff" onClick={addOption} />
        </Tooltip>
      )}
    </section>
  );
};
