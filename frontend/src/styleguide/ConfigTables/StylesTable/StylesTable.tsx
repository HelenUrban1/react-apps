import React, { useEffect, useState } from 'react';
import { Table, Tooltip } from 'antd';
import { SettingPresets, LoadedPresets } from 'domain/setting/settingTypes';
import { UsedStylesRow } from 'styleguide/ConfigTables/StylesTable/UsedStylesRow/UsedStylesRow';
import { Marker, MarkerStyle } from 'types/marker';
import { Organization } from 'graphql/organization';
import { CustomerMultiSelect } from 'view/global/CustomerMultiSelect';
import { useSelector } from 'react-redux';
import { AppState } from 'modules/state';
import { DefaultStyles, defaultSubtypeIDs, defaultTypeIDs } from 'view/global/defaults';
import { StylePresetModal } from 'domain/part/edit/balloonStyles/modals/StylePresetModal';
import AddOptionButton from 'styleguide/Buttons/AddOption';
import { ConfirmDelete } from 'view/global/confirmDelete';
import { i18n } from 'i18n';
import { InfoCircleOutlined } from '@ant-design/icons';
import Analytics from 'modules/shared/analytics/analytics';
import { NameInput } from './NameInput';
import './stylesTable.less';

interface StylesProps {
  presets: LoadedPresets | undefined;
  markers: Marker[] | null;
  handleUpdatePreset: (presetObj: SettingPresets, loadedPresets: LoadedPresets | undefined) => void;
  handleUpdatePresetName: (presetName: string, newName: string, loadedPresets: LoadedPresets | undefined) => void;
  organizations: { all: Organization[]; self: Organization | undefined };
}

export const StylesTable = ({ presets, markers, organizations, handleUpdatePreset, handleUpdatePresetName }: StylesProps) => {
  const [data, setData] = useState<any[]>();
  const [showModal, setShowModal] = useState(false);
  const { types, settings } = useSelector((state: AppState) => state.session);

  const handleChangeCustomer = (value: string[], presetName: string) => {
    const presetObj = presets ? { ...presets.metadata } : {};
    presetObj[presetName].customers = value;
    handleUpdatePreset(presetObj, presets);
  };

  const handleUpdate = ({ index, id, assign }: { index: number | string; id?: string; assign?: string[] }, presetName: string, del?: boolean) => {
    const presetObj = presets ? { ...presets.metadata } : {};
    const newAssignments = { ...presetObj[presetName].setting };
    if ((!assign && !id) || del) {
      // handle delete assignment
      delete newAssignments[index.toString()];
      presetObj[presetName].setting = newAssignments;
    } else {
      // handle change assignment
      if (id) newAssignments[index.toString()].style = id;
      if (assign) newAssignments[index.toString()].assign = assign;
    }
    handleUpdatePreset(presetObj, presets);
  };

  const handleUpdateName = (presetName: string, newName: string) => {
    handleUpdatePresetName(presetName, newName, presets);
  };

  const getNextMarker = (usedStyles: any) => {
    if (!markers) {
      return null;
    }
    const usedStyleIds: string[] = [];
    Object.keys(usedStyles).forEach((key) => {
      usedStyleIds.push(usedStyles[key].style);
    });
    let balloon = 0;
    const defaults = markers.filter((marker) => marker.markerStyleName === 'Default');

    while (usedStyleIds.includes(defaults[balloon].id) && balloon < markers.length) {
      balloon += 1;
    }

    return defaults[balloon].id;
  };

  const getNextAssignment = (usedStyles: any) => {
    let usedTypeIds: string[] = [];
    Object.keys(usedStyles).forEach((key) => {
      usedTypeIds = [...usedStyles[key].assign, ...usedTypeIds];
    });
    let assignment = ['Type', defaultTypeIDs.Note, defaultSubtypeIDs.Note];
    let matched = false;
    if (types) {
      Object.keys(types).forEach((type) => {
        if (types[type].children && !matched) {
          Object.keys(types[type].children!).forEach((subtype) => {
            if (!usedTypeIds.includes(subtype) && !matched) {
              assignment = ['Type', type, subtype];
              matched = true;
            }
          });
        }
      });
    }
    return assignment;
  };

  const handleAddOption = (presetName: string) => {
    const presetObj = presets ? { ...presets.metadata } : {};
    const assignedStyles = { ...presetObj[presetName].setting };

    let ind = 0;
    const assignedKeys = Object.keys(assignedStyles).map((key) => (Number.isFinite(parseInt(key, 10)) ? parseInt(key, 10) : 0));
    if (assignedKeys.length > 0) ind = Math.max(...assignedKeys) + 1;
    const markerId = getNextMarker(assignedStyles);
    const assignment = getNextAssignment(assignedStyles);

    if (!ind || !markerId || !assignment) return;

    assignedStyles[ind.toString()] = {
      assign: assignment,
      style: markerId,
    };

    presetObj[presetName].setting = assignedStyles;
    handleUpdatePreset(presetObj, presets);
  };

  const handleOpenPresetModal = () => {
    setShowModal(true);
  };

  const handleCreatePreset = (value: string) => {
    const presetObj = presets ? { ...presets.metadata } : {};
    presetObj[value] = {
      customers: [],
      setting: DefaultStyles,
    };
    handleUpdatePreset(presetObj, presets);
    setShowModal(false);
    Analytics.track({
      event: Analytics.events.createdStylePresetFromConfig,
      preset: {
        name: value,
        ...presetObj[value],
      },
    });
  };

  const handleDeletePreset = (presetName: any) => {
    const presetObj = presets ? { ...presets.metadata } : {};
    Analytics.track({
      event: Analytics.events.deletedStylePreset,
      preset: {
        name: presetName,
        ...presetObj[presetName],
      },
    });
    delete presetObj[presetName];
    handleUpdatePreset(presetObj, presets);
  };

  const buildStylesData = () => {
    const datas: any[] = [];
    if (!settings.stylePresets) return datas;
    const presetObj = settings.stylePresets.metadata;
    Object.keys(presetObj)
      .sort((a: string, b: string) => (a === 'Default' ? -1 : 1))
      .forEach((key) => {
        datas.push({
          key,
          id: `style-preset-row-${key}`,
          name: key === 'Default' ? key : <NameInput name={key} presets={presets} handleUpdateName={(val: string) => handleUpdateName(key, val)} />,
          balloons: (
            <UsedStylesRow
              usedStyles={presetObj[key].setting as { [key: string]: MarkerStyle }}
              markers={markers}
              update={({ index, id, assign }, del) => {
                handleUpdate({ index, id, assign }, key, del);
              }}
              add={() => handleAddOption(key)}
              canDelete
            />
          ),
          customers:
            key === 'Default' ? (
              <></>
            ) : (
              <CustomerMultiSelect
                value={presetObj[key].customers || ''}
                onChange={(value: string[]) => {
                  handleChangeCustomer(value, key);
                }}
              />
            ),
        });
      });
    return datas;
  };

  useEffect(() => {
    setData(buildStylesData());
  }, [settings, markers, organizations]);

  const columns: any[] = [
    {
      width: 40,
      dataIndex: 'key',
      className: 'ix-table-cell actions testing-part-actions-header',
      render: (record: any) => <span className="style-preset-actions">{record !== 'Default' && <ConfirmDelete classes="icon-button part-delete" confirm={() => handleDeletePreset(record)} />}</span>,
    },
    {
      title: (
        <span>
          {i18n('settings.custom.presets.styles.name')}
          <Tooltip placement="top" title={i18n('settings.custom.presets.styles.nameTip')}>
            <InfoCircleOutlined className="span-icon" />
          </Tooltip>
        </span>
      ),
      dataIndex: 'name',
      key: 'name',
    },
    {
      title: (
        <span>
          {i18n('settings.custom.presets.styles.balloons')}
          <Tooltip placement="top" title={i18n('settings.custom.presets.styles.balloonsTip')}>
            <InfoCircleOutlined className="span-icon" />
          </Tooltip>
        </span>
      ),
      dataIndex: 'balloons',
      key: 'balloons',
    },
    {
      title: (
        <span>
          {i18n('settings.custom.presets.styles.customers')}
          <Tooltip placement="top" title={i18n('settings.custom.presets.styles.customersTip')}>
            <InfoCircleOutlined className="span-icon" />
          </Tooltip>
        </span>
      ),
      dataIndex: 'customers',
      key: 'customers',
    },
  ];

  return (
    <div id="style-presets-table" className="styles-table-container">
      <StylePresetModal showModal={showModal} setShowModal={setShowModal} presets={settings.stylePresets} handleCreatePreset={handleCreatePreset} usedStyles={null} markers={markers} />
      <Table rowKey="id" className="config-table" key="styles-config-table" data-cy="styles-config-table" pagination={false} dataSource={data} columns={columns} size="middle" />
      <AddOptionButton add={handleOpenPresetModal} option="Style Preset" />
    </div>
  );
};
