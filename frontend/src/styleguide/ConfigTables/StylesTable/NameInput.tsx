import React, { ChangeEvent, useState } from 'react';
import { Input, Tooltip } from 'antd';
import { LoadedPresets } from 'domain/setting/settingTypes';
import { i18n } from 'i18n';

interface NameProps {
  name: string;
  presets: LoadedPresets | undefined;
  handleUpdateName: (value: string) => void;
}

export const NameInput = ({ name, presets, handleUpdateName }: NameProps) => {
  const [value, setValue] = useState<string>(name);
  const [errorText, setErrorText] = useState<string>();

  const reset = () => {
    setValue(name);
  };

  const handleBlur = (event: ChangeEvent<HTMLInputElement>) => {
    if (!event.target.value || event.target.value.length === 0) {
      setErrorText(i18n('errors.config.empty'));
    } else if (presets && event.target.value !== value && Object.keys(presets.metadata).includes(event.target.value)) {
      setErrorText(i18n('errors.config.duplicate'));
    } else if (event.target.value !== value) {
      handleUpdateName(event.target.value);
      setErrorText(undefined);
    } else {
      reset();
    }
  };

  const handleOnChange = (event: ChangeEvent<HTMLInputElement>) => {
    if (!event.target.value || event.target.value.length === 0) {
      setErrorText(i18n('errors.config.empty'));
    }
  };

  return (
    <div key={`name-input-div-${name}`} className="table-item-input" data-cy="table-item-input">
      <Tooltip placement="top" title={errorText || ''} data-cy="table-item-input-error-tip">
        <Input key={`name-input-${name}`} size="small" className={`list-input ${errorText ? ' list-input-error' : ''}`} data-cy="table-item-input-field" defaultValue={value || ''} onChange={handleOnChange} onBlur={handleBlur} />
      </Tooltip>
    </div>
  );
};
