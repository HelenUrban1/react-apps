import React from 'react';
import { SortableHandle } from 'react-sortable-hoc';
import { DragHandleIcon } from 'styleguide/shared/svg';

export const DragHandle = () => {
  const Handle = SortableHandle(() => (
    <div className="token-handle">
      <DragHandleIcon color="black" />
    </div>
  ));

  return <Handle />;
};
