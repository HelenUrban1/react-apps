import React from 'react';
import { ListObject, ListObjectItem, ListTypeEnum } from 'domain/setting/listsTypes';
import { convertListToDataSource, DataType, updateList } from 'utils/Lists';
import { EyeInvisibleOutlined, EyeOutlined } from '@ant-design/icons';
import { Select, Switch, Table, Tooltip } from 'antd';
import { SelectValue } from 'antd/lib/select';
import './configTables.less';
import { i18n } from 'i18n';
import Analytics from 'modules/shared/analytics/analytics';
import { SessionActions } from 'modules/session/sessionActions';
import { useDispatch } from 'react-redux';
import { SimpleTableInput } from './SimpleTableInput';

const { Option } = Select;

interface SimpleTableProps {
  listItems: ListObject | null;
  listType: ListTypeEnum;
  handleEditListItem: (id: string, newItem: ListObjectItem) => void;
  handleDeleteListItem: (listItem: ListObjectItem) => void;
  activeItem: ListObjectItem | undefined;
  showInactive: boolean;
}

export const SimpleTable = ({ listItems, listType, handleEditListItem, handleDeleteListItem, activeItem, showInactive }: SimpleTableProps) => {
  const dispatch = useDispatch();

  const handleToggleActive = (listItem: ListObjectItem) => {
    const newListItem = { ...listItem };
    newListItem.status = newListItem.status === 'Active' ? 'Inactive' : 'Active';
    handleEditListItem(listItem.id, newListItem);
    Analytics.track({
      events: Analytics.events.toggleConfigurationEntry,
      accountConfig: newListItem,
    });
  };

  const handleBulkReorder = (id: string, newIndex: number) => {
    if (!listItems || listItems[id].index === newIndex) return;
    let newList = { ...listItems };
    const ahead = newList[id].index < newIndex;
    const startIndex = ahead ? newList[id].index : newIndex;
    const endIndex = ahead ? newIndex : newList[id].index;
    Object.keys(newList)
      .slice(startIndex, endIndex + 1)
      .forEach((key) => {
        if (newList[key].id === id) {
          // set new index
          newList[key].index = newIndex;
        } else {
          // increment or decrement based on ahead
          newList[key].index = ahead ? newList[key].index - 1 : newList[key].index + 1;
        }
        handleEditListItem(key, newList[key]);
      });
    newList = Object.fromEntries(Object.entries(newList).sort((a: [string, ListObjectItem], b: [string, ListObjectItem]) => a[1].index - b[1].index));

    dispatch({ type: SessionActions.UPDATE_LISTS, payload: updateList(listType, newList) });

    Analytics.track({
      events: Analytics.events.reorderedConfigurationList,
      accountConfig: newList[id],
    });
  };

  const columns = [
    {
      title: i18n('settings.custom.table.order'),
      className: 'list-order',
      key: 'orderKey',
      width: 80,
      render: (data: DataType) => {
        const { id } = data;
        if (!id || !listItems) return <></>;
        return (
          <div key={`${id}-order`} className="item-order-select-container">
            <Select
              key={`${id}-order-select`}
              data-cy="item-order-select"
              size="small"
              value={listItems[id].index}
              style={{ width: 48 }}
              onChange={(value: SelectValue) => {
                if (value) handleBulkReorder(data.id, parseInt(value.toString(), 10));
              }}
            >
              {Object.keys(listItems).map((key, index) => {
                const disabled = !showInactive && listItems[key].status === 'Inactive';
                return (
                  <Option key={`${key}-option-val-${index}`} data-cy="item-order-select-option" value={index} disabled={disabled}>
                    <Tooltip title={disabled ? i18n('settings.custom.table.hidden') : ''}>
                      <div>{index + 1}</div>
                    </Tooltip>
                  </Option>
                );
              })}
            </Select>
          </div>
        );
      },
    },
    {
      title: i18n(`settings.custom.lists.${listType}`),
      className: 'table-value',
      key: 'inputKey',
      render: (data: DataType) => {
        const { id } = data;
        if (!id || !listItems) return <></>;
        return <SimpleTableInput listItem={listItems[id]} listItems={listItems} handleEditListItem={handleEditListItem} handleDeleteListItem={handleDeleteListItem} />;
      },
    },
    {
      title: i18n(`settings.custom.table.active`),
      className: 'table-active-toggle',
      key: 'activeKey',
      width: 80,
      align: 'center' as 'center',
      render: (data: DataType) => {
        const { id } = data;
        if (!id || !listItems) return <></>;
        return (
          <div className="item-visibility-toggle-container" data-cy="item-visibility-toggle-container">
            <Switch
              size="default"
              key={`${listItems[id].value}-active-switch`}
              data-cy="item-visibility-toggle"
              checkedChildren={<EyeOutlined />}
              unCheckedChildren={<EyeInvisibleOutlined />}
              defaultChecked={listItems[id].status === 'Active'}
              onChange={(e: any) => handleToggleActive(listItems[data.id])}
            />
          </div>
        );
      },
    },
  ];

  if (!listItems) return <></>;
  return <Table rowKey="id" className="config-table" key={`${listType}-table`} data-cy="config-table" size="middle" pagination={false} dataSource={convertListToDataSource(listItems, showInactive, '')} columns={columns} scroll={{ y: 400 }} />;
};
