/* eslint-disable complexity */
import React, { useRef, useState } from 'react';
import { ListObject, ListObjectItem, ListTypeEnum } from 'domain/setting/listsTypes';
import { convertTypeListToDataSource, DataType, getMeasurementsSelectOptions, updateList } from 'utils/Lists';
import { SortableContainer, SortableElement } from 'react-sortable-hoc';
import { arrayMoveImmutable as arrayMove } from 'array-move';
import { CaretDownOutlined, CaretRightOutlined, InfoCircleOutlined, PlusCircleTwoTone, SearchOutlined } from '@ant-design/icons';
import { Checkbox, Input, message, Switch, Table, Tooltip } from 'antd';
import { useDispatch, useSelector } from 'react-redux';
import { AppState } from 'modules/state';
import { i18n } from 'i18n';
import { SimpleTableInput } from './SimpleTableInput';
import { MeasurementSelect } from './MeasurementSelect';
import { DragHandle } from './DragHandle';
import './configTables.less';
import SessionActions from 'modules/session/sessionActions';

interface TypesTableProps {
  handleEditListItem: (id: string, newItem: ListObjectItem, update?: boolean) => void;
  handleDeleteListItem: (listItem: ListObjectItem) => void;
  handleOpenModal: (listType: ListTypeEnum, type: string) => void;
  handleToggleActive: (listItem: ListObjectItem) => void;
}
export const TypesTable = ({ handleEditListItem, handleDeleteListItem, handleOpenModal, handleToggleActive }: TypesTableProps) => {
  const dispatch = useDispatch();
  const { types, metricUnits, imperialUnits } = useSelector((state: AppState) => state.session);
  const [showInactive, setShowInactive] = useState<boolean>(true);
  const [search, setSearch] = useState<string>('');
  const dragItem = useRef<ListObjectItem>();

  const dataSource = convertTypeListToDataSource(types, showInactive, search);
  const measurementOptions = getMeasurementsSelectOptions(metricUnits, imperialUnits);

  // adjusts the indices of the children of a top level item after a sibling is renumbered
  const reorderSubtypes = (listItem: ListObjectItem, listItems: ListObject | null, newIndex: number, parentId: string) => {
    if (!listItems || listItem.index === newIndex || !parentId) return listItems;
    const newList = { ...listItems };
    const subtypes = listItems[parentId].children ? { ...listItems[parentId].children! } : {};
    const ahead = listItem.index < newIndex;
    const startIndex = ahead ? listItem.index : newIndex;
    const endIndex = ahead ? newIndex : listItem.index;
    const newTypes: ListObject = {};

    Object.entries(subtypes).forEach(([id, sub]) => {
      if (sub.index > endIndex || sub.index < startIndex) {
        newTypes[id] = sub;
      } else {
        const newSub = { ...sub };
        if (listItem?.id === sub.id) {
          // set new index
          newSub.index = newIndex;
        } else {
          // increment or decrement based on ahead
          newSub.index = ahead ? newSub.index - 1 : newSub.index + 1;
        }
        handleEditListItem(newSub.id, newSub, false);
        newTypes[id] = newSub;
      }
    });
    newList[parentId].children = newTypes;

    return newList;
  };

  // Adjusts the indices of list items around the renumbered item
  const handleBulkReorder = (listItem: ListObjectItem, newIndex: number, parentId: string) => {
    const newList = reorderSubtypes(listItem, types, newIndex, parentId);

    // update the list to keep data current, call Analytics
    if (newList) {
      dispatch({ type: SessionActions.UPDATE_LISTS, payload: updateList(listItem.listType, newList) });
    }
  };

  const handleEditMeasurement = (listItem: ListObjectItem, measurement: string) => {
    const newItem = { ...listItem };
    newItem.meta = measurement === 'None' ? undefined : { measurement };
    handleEditListItem(newItem.id, newItem);
  };

  // first column, always used, title varies
  const valueColumn = {
    title: (
      <span>
        {i18n('settings.custom.table.feature')}
        <Tooltip placement="top" title={i18n('settings.custom.table.header.feature')}>
          <InfoCircleOutlined className="span-icon" />
        </Tooltip>
      </span>
    ),
    className: 'config-value-cell',
    key: 'configKey',
    width: '40%',
    render: (data: any) => {
      if (data.obj && !data.childs) {
        const listItems = types;
        if (!listItems) return <></>;
        let indexes: any[] | undefined = data.obj.parentId ? Object.keys(listItems[data.obj.parentId || ''].children || {}) : Object.keys(listItems);
        if (!indexes) indexes = [];
        return (
          <div className="config-table-leaf">
            <DragHandle />
            <SimpleTableInput listItem={data.obj} listItems={listItems} handleEditListItem={handleEditListItem} handleDeleteListItem={handleDeleteListItem} />
          </div>
        );
      }
      const val = data.obj ? data.obj.value : data.value;
      return (
        <div className="config-cell-container">
          <span className="row-label">{`${val} (${data.childs.length})`}</span>
          {val !== 'Geometric Tolerance' && data.childs && data.childs[0] && !data.childs[0].children && (
            <Tooltip placement="top" title={`Add ${val}`}>
              <PlusCircleTwoTone twoToneColor="#1890ff" onClick={() => handleOpenModal(data.childs[0].obj.listType, data.id)} />
            </Tooltip>
          )}
        </div>
      );
    },
  };

  // measurement column for Types
  const measurementColumn = {
    title: (
      <span>
        {i18n(`Measurement`)}
        <Tooltip placement="top" title={i18n('settings.custom.table.header.measurement')}>
          <InfoCircleOutlined className="span-icon" />
        </Tooltip>
      </span>
    ),
    className: 'table-value',
    key: 'measureKey',
    width: '40%',
    render: (data: DataType) => {
      if (!data.id || !data.obj || data.obj.listType !== 'Type') return <></>;
      if (data.childs) return <></>;
      return <MeasurementSelect handleEditMeasurement={handleEditMeasurement} measurementOptions={measurementOptions} data={data} />;
    },
  };

  const visibleColumn = {
    title: (
      <span>
        {i18n(`settings.custom.table.status`)}
        <Tooltip placement="top" title={i18n('settings.custom.table.header.featureStatus')}>
          <InfoCircleOutlined className="span-icon" />
        </Tooltip>
      </span>
    ),
    className: 'table-active-toggle',
    key: 'activeKey',
    width: '20%',
    align: 'center' as 'center',
    render: (data: DataType) => {
      if (!data.id || !data.obj || !data.obj.parentId) return <></>;
      return (
        <div className="item-visibility-toggle-container" data-cy="item-visibility-toggle-container">
          <Switch key={`${data.obj.value}-active-switch`} data-cy="item-visibility-toggle" checkedChildren="Show" unCheckedChildren="Hide" defaultChecked={data.obj.status === 'Active'} onChange={() => handleToggleActive(data.obj)} />
        </div>
      );
    },
  };

  const columns = [valueColumn, measurementColumn, visibleColumn];

  const customExpandIcon = (props: any) => {
    if (!props.expandable) return <></>;
    if (props.expanded) {
      return (
        <CaretDownOutlined
          className="expand-icon"
          onClick={(e) => {
            props.onExpand(props.record, e);
          }}
        />
      );
    }
    return (
      <CaretRightOutlined
        className="expand-icon"
        onClick={(e) => {
          props.onExpand(props.record, e);
        }}
      />
    );
  };

  const SortableItem = SortableElement((props: any) => {
    const newProps = { ...props, object: JSON.stringify(props.value) };
    return <tr {...newProps} helperclass="dragging-row" />;
  });
  const SortableArea = SortableContainer((props: any) => <tbody {...props} />);

  const onSortEnd = ({ oldIndex, newIndex }: { oldIndex: number; newIndex: number }) => {
    document.body.className = '';
    if (dragItem.current && types && dragItem.current.parentId) {
      // reorder
      const newData = [...dataSource];
      const parentInd = newData.findIndex((el: DataType) => el.id === dragItem.current?.parentId);
      if (parentInd >= 0 && newData[parentInd].childs) {
        dataSource[parentInd].childs = arrayMove(newData[parentInd].childs!, oldIndex, newIndex).filter((el: any) => !!el);
        handleBulkReorder(dragItem.current, newIndex, dragItem.current.parentId!);
      }
    } else {
      // handle failed reorder
      message.error('Failed to reorder');
    }
    dragItem.current = undefined;
  };

  const onSortStart = ({ node }: { node: any }) => {
    document.body.className = 'grabbing';
    const listItem = JSON.parse(node.getAttribute('object'));
    dragItem.current = listItem;
  };

  const DraggableContainer = (props: any) => {
    return <SortableArea useDragHandle helperClass="row-dragging" onSortStart={onSortStart} onSortEnd={onSortEnd} {...props} />;
  };

  const DraggableBodyRow = (restProps: any) => {
    if (restProps.children[0]) {
      const { parentId } = restProps.children[0].props.record.obj;
      const parentIndex = dataSource.findIndex((el: DataType) => el.id === parentId);
      const childs = dataSource[parentIndex].childs || [];
      // function findIndex base on Table rowKey props and should always be a right array index
      const index = childs.findIndex((x) => x.obj && restProps['data-row-key'].includes(x.obj.id));

      return <SortableItem index={index} value={childs[index].obj} {...restProps} />;
    }
    else {
      return <SortableItem index={'0'}/>;
    }
  };

  const expandedRowRender = (record: any) => {
    return (
      <Table
        showHeader={false}
        size="small"
        rowKey="id"
        key="config-table-expanded"
        pagination={false}
        dataSource={record.childs}
        columns={columns}
        components={{ body: { wrapper: DraggableContainer, row: DraggableBodyRow } }}
        // scroll={{ y: 480 }}
      />
    );
  };

  return (
    <div id="feature-types-table" className="list-table-container">
      <section className="config-lists-actions" data-cy="lists-actions">
        <section className="config-list-actions-right" data-cy="list-actions-right">
          <Input size="large" placeholder="search by subtype" onChange={(e: any) => setSearch(e.target.value)} prefix={<SearchOutlined />} allowClear />
          <Checkbox checked={showInactive} onChange={(e: any) => setShowInactive(e.target.checked)} className="inactive-checkbox" data-cy="inactive-checkbox">
            {i18n('common.showHidden')}
          </Checkbox>
        </section>
      </section>
      <Table
        rowKey="id"
        indentSize={24}
        size="middle"
        className="config-table"
        key="types-config-table"
        data-cy="types-config-table"
        pagination={false}
        dataSource={dataSource}
        columns={columns}
        expandable={{
          rowExpandable:record => record.childs && record.childs.length > 0 || false,
          expandIcon: customExpandIcon,
          expandedRowRender: expandedRowRender
        }}
      />
    </div>
  );
};
