import React, { ChangeEvent, ReactNode, useState } from 'react';
import { ListObject, ListObjectItem } from 'domain/setting/listsTypes';
import { DeleteOutlined, ExclamationCircleOutlined } from '@ant-design/icons';
import { useLazyQuery } from '@apollo/client';
import { Input, Popconfirm, Tooltip } from 'antd';
import { i18n } from 'i18n';
import { isDuplicateValue } from 'utils/Lists';
import { Characteristics } from 'graphql/characteristics';
import { CharacteristicList } from 'types/characteristics';

interface SimpleTableInputProps {
  listItem: ListObjectItem;
  listItems: ListObject | null;
  handleEditListItem: (id: string, newItem: ListObjectItem) => void;
  handleDeleteListItem: (listItem: ListObjectItem) => void;
}

export const SimpleTableInput = ({ listItem, listItems, handleEditListItem, handleDeleteListItem }: SimpleTableInputProps) => {
  const [value, setValue] = useState<string>(listItem.value);
  const [tempVal, setTempVal] = useState<string>('');
  const [errorText, setErrorText] = useState<string>();
  const [updateWarn, setUpdateWarn] = useState('');
  const [deleteWarn, setDeleteWarn] = useState<ReactNode>();

  const update = () => {
    const newListItem = { ...listItem };
    newListItem.value = tempVal;
    setValue(tempVal);
    handleEditListItem(listItem.id, newListItem);
    setUpdateWarn('');
    setTempVal('');
  };

  const reset = () => {
    setUpdateWarn('');
    setTempVal('');
    setDeleteWarn(undefined);
    setValue(listItem.value);
  };

  const handleBlur = (event: ChangeEvent<HTMLInputElement>) => {
    if (!event.target.value || event.target.value.length === 0) {
      setErrorText(i18n('errors.config.empty'));
    } else if (listItems && event.target.value !== value && isDuplicateValue(listItems, event.target.value, listItem)) {
      setErrorText(i18n('errors.config.duplicate'));
    } else if (event.target.value !== value) {
      setErrorText(undefined);
      setTempVal(event.target.value);
      setUpdateWarn(`This will update ${value || ''} to ${tempVal} for all parts. Continue?`);
    } else {
      reset();
    }
  };

  const handleOnChange = (event: ChangeEvent<HTMLInputElement>) => {
    if (!event.target.value || event.target.value.length === 0) {
      setErrorText(i18n('errors.config.empty'));
    }
  };

  const getFilter = () => {
    switch (listItem.listType) {
      case 'Type':
        return { notationSubtype: listItem.id };
      case 'Classification':
        return { criticality: listItem.id };
      case 'Operation':
        return { operation: listItem.id };
      case 'Inspection_Method':
        return { inspectionMethod: listItem.id };
      default:
        return {};
    }
  };

  const checkUsage = (usage: number | undefined) => {
    if (usage === undefined) {
      setDeleteWarn(i18n('common.destroy'));
    } else {
      setDeleteWarn(
        <div className="delete-list-item-warning">
          <div>
            <span className="important">{listItem.value}</span>
            <span>{` ${i18n('settings.custom.table.deleteConfirm.usedFor')} `}</span>
            <span className="important">{`${usage}`}</span>
            <span>{` ${i18n('settings.custom.table.deleteConfirm.features')}. `}</span>
          </div>
          <div>
            <span>{`${i18n('settings.custom.table.deleteConfirm.sure')}`}</span>
          </div>
        </div>,
      );
    }
  };

  const [getCount] = useLazyQuery<CharacteristicList>(Characteristics.query.list, {
    fetchPolicy: 'cache-and-network',
    variables: {
      filter: getFilter(),
    },
    onCompleted: ({ characteristicList }) => {
      checkUsage(characteristicList?.count);
    },
    notifyOnNetworkStatusChange: true,
  });

  const handleDelete = () => {
    handleDeleteListItem(listItem);
    setDeleteWarn('');
  };

  return (
    <div className="table-item-input" data-cy="table-item-input">
      {!listItem.default ? (
        <Popconfirm visible={!!updateWarn} title={updateWarn} onConfirm={update} onCancel={reset}>
          <Tooltip placement="top" title={errorText || ''} data-cy="table-item-input-error-tip">
            <Input size="small" className={`list-input ${errorText ? ' list-input-error' : ''}`} data-cy="table-item-input-field" key={`${listItem.id}-input`} defaultValue={value || ''} onChange={handleOnChange} onBlur={handleBlur} />
          </Tooltip>
        </Popconfirm>
      ) : (
        <Tooltip placement="top" title={i18n('errors.config.default')} data-cy="table-item-input-deault-tip">
          <div className="list-input-container" data-cy="list-input-container">
            <Input size="small" className="list-input list-input-disabled" key={`${listItem.id}-input-disabled`} data-cy="table-item-input-field" defaultValue={listItem.value} disabled />
          </div>
        </Tooltip>
      )}
      <section className="item-actions" data-cy="item-actions">
        <Popconfirm icon={<ExclamationCircleOutlined style={{ color: 'red' }} />} visible={!!deleteWarn} title={deleteWarn} onConfirm={handleDelete} onCancel={reset}>
          <Tooltip placement="top" title={i18n('common.destroy')}>
            {!listItem.default && <DeleteOutlined className="item-delete" onClick={() => getCount()} />}
          </Tooltip>
        </Popconfirm>
      </section>
    </div>
  );
};
