import React, { useRef, useState } from 'react';
import { ListObject, ListObjectItem, ListTypeEnum } from 'domain/setting/listsTypes';
import { convertListToDataSource, DataType, updateList } from 'utils/Lists';
import { CaretDownOutlined, CaretRightOutlined, InfoCircleOutlined, PlusCircleTwoTone, SearchOutlined } from '@ant-design/icons';
import { Checkbox, Input, message, Switch, Table, Tooltip } from 'antd';
import { SortableContainer, SortableElement } from 'react-sortable-hoc';
import { useDispatch, useSelector } from 'react-redux';
import { AppState } from 'modules/state';
import { i18n } from 'i18n';
import { arrayMoveImmutable as arrayMove } from 'array-move';
import { SimpleTableInput } from './SimpleTableInput';
import { DragHandle } from './DragHandle';
import './configTables.less';
import { SessionActions } from 'modules/session/sessionActions';

interface InspectionTableProps {
  handleEditListItem: (id: string, newItem: ListObjectItem, update?: boolean) => void;
  handleDeleteListItem: (listItem: ListObjectItem) => void;
  handleOpenModal: (listType: ListTypeEnum, type: string) => void;
  handleToggleActive: (listItem: ListObjectItem) => void;
  getCurrentList: (listType: ListTypeEnum) => ListObject | null;
}

export const InspectionTable = ({ handleEditListItem, handleDeleteListItem, handleOpenModal, handleToggleActive, getCurrentList }: InspectionTableProps) => {
  const dispatch = useDispatch();
  const { classifications, methods, operations } = useSelector((state: AppState) => state.session);
  const [showInactive, setShowInactive] = useState<boolean>(true);
  const [search, setSearch] = useState<string>('');
  const dragItem = useRef<ListObjectItem>();

  const classificationsData = convertListToDataSource(classifications, showInactive, search);
  const methodsData = convertListToDataSource(methods, showInactive, search);
  const operationsData = convertListToDataSource(operations, showInactive, search);

  const dataSource = [
    {
      value: 'Classifications',
      key: 'classifications-list-row',
      id: 'classifications-list-row',
      orderKey: `classifications-list-row-order-key`,
      inputKey: `classifications-list-row-input-key`,
      activeKey: `classifications-list-row-active-key`,
      measureKey: `classifications-list-row-measure-key`,
      subtypeKey: `classifications-list-row-subtype-key`,
      childs: classificationsData,
      obj: null,
    },
    {
      value: 'Methods',
      key: 'methods-list-row',
      id: 'methods-list-row',
      orderKey: `methods-list-row-order-key`,
      inputKey: `methods-list-row-input-key`,
      activeKey: `methods-list-row-active-key`,
      measureKey: `methods-list-row-measure-key`,
      subtypeKey: `methods-list-row-subtype-key`,
      childs: methodsData,
      obj: null,
    },
    {
      value: 'Operations',
      key: 'operations-list-row',
      id: 'operations-list-row',
      orderKey: `operations-list-row-order-key`,
      inputKey: `operations-list-row-input-key`,
      activeKey: `operations-list-row-active-key`,
      measureKey: `operations-list-row-measure-key`,
      subtypeKey: `operations-list-row-subtype-key`,
      childs: operationsData,
      obj: undefined,
    },
  ];

  // adjusts the indices of a top level item after a parent is renumbered
  const reorderInspectionInfo = (id: string, listItems: ListObject | null, newIndex: number) => {
    if (!listItems || listItems[id].index === newIndex) return listItems;
    let newList = { ...listItems };
    const ahead = newList[id].index < newIndex;
    const startIndex = ahead ? newList[id].index : newIndex;
    const endIndex = ahead ? newIndex : newList[id].index;
    Object.keys(newList)
      .slice(startIndex, endIndex + 1)
      .forEach((key) => {
        if (newList[key].id === id) {
          // set new index
          newList[key].index = newIndex;
        } else {
          // increment or decrement based on ahead
          newList[key].index = ahead ? newList[key].index - 1 : newList[key].index + 1;
        }
        handleEditListItem(key, newList[key], false);
      });

    newList = Object.fromEntries(Object.entries(newList).sort((a: [string, ListObjectItem], b: [string, ListObjectItem]) => a[1].index - b[1].index));

    return newList;
  };

  // Adjusts the indices of list items around the renumbered item
  const handleBulkReorder = (listItem: ListObjectItem, newIndex: number, parentId?: string) => {
    const newList = reorderInspectionInfo(listItem.id, getCurrentList(listItem.listType), newIndex);

    // update the list to keep data current, call Analytics
    if (newList) {
      dispatch({ type: SessionActions.UPDATE_LISTS, payload: updateList(listItem.listType, newList) });
    }
  };

  // first column, always used, title varies
  const valueColumn = {
    title: (
      <span>
        {i18n('settings.custom.table.property')}
        <Tooltip placement="top" title={i18n('settings.custom.table.header.inspection')}>
          <InfoCircleOutlined className="span-icon" />
        </Tooltip>
      </span>
    ),
    className: 'config-value-cell',
    key: 'configKey',
    width: '80%',
    render: (data: any) => {
      if (data.obj && !data.childs) {
        const listItems = getCurrentList(data.obj.listType);
        if (!listItems) return <></>;
        return (
          <div className="config-table-leaf">
            <DragHandle />
            <SimpleTableInput listItem={data.obj} listItems={listItems} handleEditListItem={handleEditListItem} handleDeleteListItem={handleDeleteListItem} />
          </div>
        );
      }
      const val = data.obj ? data.obj.value : data.value;
      return (
        <div className="config-cell-container">
          <span className="row-label">{`${val} (${data.childs.length})`}</span>
          {data.childs && data.childs[0] && !data.childs[0].children && (
            <Tooltip placement="top" title={`Add ${val}`}>
              <PlusCircleTwoTone twoToneColor="#1890ff" onClick={() => handleOpenModal(data.childs[0].obj.listType, data.id)} />
            </Tooltip>
          )}
        </div>
      );
    },
  };

  const visibleColumn = {
    title: (
      <span>
        {i18n(`settings.custom.table.status`)}
        <Tooltip placement="top" title={i18n('settings.custom.table.header.inspectionStatus')}>
          <InfoCircleOutlined className="span-icon" />
        </Tooltip>
      </span>
    ),
    className: 'table-active-toggle',
    key: 'activeKey',
    width: '20%',
    align: 'center' as 'center',
    render: (data: DataType) => {
      if (!data.id || !data.obj || (data.obj.children && Object.keys(data.obj.children || {}).length > 0)) return <></>;
      return (
        <div className="item-visibility-toggle-container" data-cy="item-visibility-toggle-container">
          <Switch
            size="default"
            key={`${data.obj.value}-active-switch`}
            data-cy="item-visibility-toggle"
            checkedChildren={i18n('settings.custom.table.show')}
            unCheckedChildren={i18n('settings.custom.table.hide')}
            defaultChecked={data.obj.status === 'Active'}
            onChange={() => handleToggleActive(data.obj)}
          />
        </div>
      );
    },
  };

  const columns = [valueColumn, visibleColumn];

  const customExpandIcon = (props: any) => {
    if (!props.expandable) return <></>;
    if (props.expanded) {
      return (
        <CaretDownOutlined
          className="expand-icon"
          onClick={(e) => {
            props.onExpand(props.record, e);
          }}
        />
      );
    }
    return (
      <CaretRightOutlined
        className="expand-icon"
        onClick={(e) => {
          props.onExpand(props.record, e);
        }}
      />
    );
  };

  const SortableItem = SortableElement((props: any) => {
    const newProps = {
      ...props,
      listtype: props.value.listtype,
      object: JSON.stringify(props.value),
    };
    return <tr {...newProps} />;
  });
  const SortableArea = SortableContainer((props: any) => <tbody {...props} helperClass="dragging-row" />);

  const onSortEnd = ({ oldIndex, newIndex }: { oldIndex: number; newIndex: number }) => {
    document.body.className = '';
    if (dragItem.current) {
      // reorder
      const newData = [...dataSource];
      let ind = 0;
      if (dragItem.current.listType === 'Inspection_Method') {
        ind = 1;
      } else if (dragItem.current.listType === 'Operation') {
        ind = 2;
      }
      dataSource[ind].childs = arrayMove(newData[ind].childs, oldIndex, newIndex).filter((el: any) => !!el);
      handleBulkReorder(dragItem.current, newIndex);
    } else {
      // handle failed reorder
      message.error('Failed to reorder');
    }
    dragItem.current = undefined;
  };

  const onSortStart = ({ node }: { node: Element }) => {
    document.body.className = 'grabbing';
    const listItem = JSON.parse(node.getAttribute('object') || '');
    dragItem.current = listItem;
  };

  const DraggableContainer = (props: any) => {
    return <SortableArea useDragHandle helperClass="row-dragging" onSortStart={onSortStart} onSortEnd={onSortEnd} {...props} />;
  };

  const DraggableBodyRow = (restProps: any) => {
    const listtype = restProps['data-row-key'];
    let data: DataType[] = [];
    if (listtype.includes('Classification')) {
      data = classificationsData;
    } else if (listtype.includes('Operation')) {
      data = operationsData;
    } else if (listtype.includes('Inspection_Method')) {
      data = methodsData;
    }
    // function findIndex base on Table rowKey props and should always be a right array index
    const index = data.findIndex((x) => x.obj && restProps['data-row-key'].includes(x.obj.id));
    return <SortableItem index={index} value={data[index].obj} {...restProps} />;
  };

  const expandedRowRender = (record: any) => {
    return (
      <Table
        showHeader={false}
        rowKey="id"
        key="config-table-expanded"
        pagination={false}
        dataSource={record.childs}
        columns={columns}
        components={{ body: { wrapper: DraggableContainer, row: DraggableBodyRow } }}
        size="small"
        // scroll={{ y: 480 }}
      />
    );
  };

  return (
    <div id="inspection-properties-table" className="list-table-container">
      <section className="config-lists-actions" data-cy="lists-actions">
        <section className="config-list-actions-right" data-cy="list-actions-right">
          <Input size="large" placeholder="search by property" onChange={(e: any) => setSearch(e.target.value)} prefix={<SearchOutlined />} allowClear />
          <Checkbox checked={showInactive} onChange={(e: any) => setShowInactive(e.target.checked)} className="inactive-checkbox" data-cy="inactive-checkbox">
            {i18n('common.showHidden')}
          </Checkbox>
        </section>
      </section>
      <Table
        rowKey="id"
        className="config-table"
        key="inspection-config-table"
        data-cy="inspection-config-table"
        pagination={false}
        dataSource={dataSource}
        columns={columns}
        size="middle"
        expandable={{
          rowExpandable:record => record.childs && record.childs.length > 0 || false,
          expandIcon: customExpandIcon,
          expandedRowRender: expandedRowRender
        }}
      />
    </div>
  );
};
