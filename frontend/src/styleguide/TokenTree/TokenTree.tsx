import React, { useState, useRef, useEffect } from 'react';
import { DownOutlined, PicCenterOutlined, VerticalAlignBottomOutlined, SearchOutlined } from '@ant-design/icons';
import { Tree, Input, Tooltip, message } from 'antd';
import { TokenType } from 'types/reportTemplates';
// import AddOptionButton from 'styleguide/Buttons/AddOption';
import { i18n } from 'i18n';
// import { EventDataNode } from 'antd/lib/tree';
import Token from '../Token/Token';
import { defaultTokens } from '../../view/global/defaults';
import './TokenTree.less';

// const { TreeNode } = Tree;

const checkInUse = (token: string, used: string[]) => {
  if (!used || !token) return false;
  return used.includes(token);
};

interface Props {
  setSelectedToken: (e: any, token: TokenType) => void;
  usedTokens: string[];
  customTokens: TokenType[];
}

const TokenTree = ({ setSelectedToken, usedTokens, customTokens }: Props) => {
  const [expanded, setExpanded] = useState<string[]>(['Part Information', 'Feature Data']);
  const [search, setSearch] = useState<string>('');
  const [autoExpand, setAuto] = useState<boolean>(true);

  // const addCustomToken = () => {
  //   message.success('Add Custom Token');
  // };

  const removeCustomToken = () => {
    message.warning('Remove Custom Token');
  };

  const buildTree = (data: any) => {
    const tree: any = [];
    Object.keys(data).forEach((category) => {
      const tokens: any = [];
      data[category].children.forEach((token: any) => {
        const render = (
          <>
            <Token key={token.token} data-cy={`token-${token.token}`} token={token} handleOnSelect={setSelectedToken} inUse={checkInUse(token.token, usedTokens)} inTree usedTokens={usedTokens} />
          </>
        );
        tokens.push({
          key: token.name,
          name: token.name,
          title: render,
          type: token.type,
          used: checkInUse(token.token, usedTokens),
          tokenObj: token,
          token: render,
        });
      });
      tree.push({
        key: `${data[category].name}`,
        title: data[category].name,
        children: tokens,
        type: 'parent',
      });
    });

    // TODO: re-enable when custom tokens are added
    // const customs: any = []; // get custom tokens here
    // customs.push({
    //   key: 'add-custom-token',
    //   title: i18n('entities.tokens.custom.group'),
    //   type: 'button',
    //   used: false,
    //   token: (
    //     <>
    //       <AddOptionButton add={addCustomToken} option="Custom Token" />
    //     </>
    //   ),
    // });
    // customTokens.forEach((token) => {
    //   customs.push({
    //     key: token.name,
    //     title: token.name,
    //     type: token.type,
    //     used: checkInUse(token.token, usedTokens),
    //     tokenObj: token,
    //     token: (
    //       <>
    //         <Token key={token.token} data-cy={`token-${token.token}`} token={token} handleOnSelect={setSelectedToken} inUse={checkInUse(token.token, usedTokens)} inTree usedTokens={usedTokens} remove={removeCustomToken} />
    //       </>
    //     ),
    //   });
    // });
    // tree.push({
    //   key: `Custom Tokens`,
    //   title: `Custom Tokens`,
    //   children: customs,
    //   type: 'parent',
    // });
    return tree;
  };

  const [treeData, setTreeData] = useState<any[]>(buildTree(defaultTokens));
  const fullTree = useRef(buildTree(defaultTokens));

  useEffect(() => {
    if (!treeData) return;
    const tempTree = [...treeData];
    tempTree.forEach((parent: any) => {
      parent.children.forEach((child: any) => {
        if (child.tokenObj) {
          child.title = (
            <>
              <Token key={child.key} data-cy={`token-${child.tokenObj.token}`} token={child.tokenObj} handleOnSelect={setSelectedToken} inUse={checkInUse(child.tokenObj.token, usedTokens)} inTree usedTokens={usedTokens} remove={removeCustomToken} />
            </>
          );
        }
      });
    });
    setTreeData(tempTree);
    fullTree.current = buildTree(defaultTokens);
  }, [usedTokens.length]);

  const onExpand = (expandedKeys: React.Key[], info: any) => {
    if (info?.node?.type !== 'parent') {
      return;
    }
    setExpanded(expandedKeys.map((key) => key.toString()));
    setAuto(false);
  };

  const onChange = (e: any) => {
    const { value } = e.target;
    const newTreeData: any[] = [];
    const expandedKeys: string[] = [];
    if (value === '') {
      setTreeData(fullTree.current);
      setExpanded(['Part Information', 'Feature Data']);
      setAuto(true);
      setSearch(value);
      return;
    }
    fullTree.current.forEach((parent: any) => {
      if (parent.children) {
        const childs: any[] = [];
        const topLevel = {
          key: parent.key,
          title: parent.title,
          children: childs,
          type: 'parent',
        };
        parent.children.forEach((child: any) => {
          if (child.name.toLowerCase().indexOf(value.toLowerCase()) > -1) {
            topLevel.children.push(child);
          }
        });
        if (topLevel.children.length > 0) {
          newTreeData.push(topLevel);
          if (!expandedKeys.includes(topLevel.key)) expandedKeys.push(topLevel.key);
        }
      }
    });
    setExpanded(expandedKeys);
    setSearch(value);
    setAuto(true);
    setTreeData(newTreeData);
  };

  return (
    <div className="token-tree-container">
      <Input style={{ marginBottom: 8 }} placeholder={i18n('common.search')} onChange={onChange} prefix={<SearchOutlined />} allowClear />
      <div className="token-tips">
        <div className="hint single">
          <div className="tip">
            <Tooltip title={i18n('entities.tokens.legend.singleTip')} placement="bottomLeft" arrowPointAtCenter>
              <PicCenterOutlined />
            </Tooltip>
          </div>
          {i18n('entities.tokens.legend.single')}
        </div>
        <div className="hint dynamic">
          <div className="tip">
            <Tooltip title={i18n('entities.tokens.legend.dynamicTip')} placement="bottomLeft" arrowPointAtCenter>
              <VerticalAlignBottomOutlined />
            </Tooltip>
          </div>
          {i18n('entities.tokens.legend.dynamic')}
        </div>
      </div>
      {/* Commented out to try to remove the deprication warning
      <Tree onExpand={onExpand} expandedKeys={expanded} autoExpandParent={autoExpand} switcherIcon={<DownOutlined className="cypress-token-toggle" />}>
        {treeData.map((data: any) => (
          <TreeNode title={data.title} key={`${data.key}`}>
            {data.children.map((child: any) => (
              <TreeNode className={`${child.type}-${child.used}`} title={child.token} key={`${child.key}`} />
            ))}
          </TreeNode>
        ))}
      </Tree> */}
      <Tree //
        onExpand={onExpand}
        onSelect={onExpand}
        expandedKeys={expanded}
        autoExpandParent={autoExpand}
        switcherIcon={<DownOutlined className="cypress-token-toggle" />}
        treeData={treeData}
      />
    </div>
  );
};

export default TokenTree;
