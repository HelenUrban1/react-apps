/* eslint-disable react/jsx-filename-extension */
import React from 'react';
import { storiesOf } from '@storybook/react';
import TokenTree from './TokenTree';

const fakeTokens = [
  {
    group: 'Custom',
    name: 'Fake Token A',
    token: `{{Fake.Token.A}}`,
    fullToken: `{{Part.Name}} {{Part.Number}}`,
    example: 'IX MULTI TOOL PRT-2422',
    type: 'Single',
  },
  {
    group: 'Custom',
    name: 'Fake Token B',
    token: `{{Fake.Token.B}}`,
    fullToken: `{{Item.Number}} - {{Item.Nominal}}`,
    example: '1 - 3.14',
    type: 'Dynamic',
  },
];

const setSelectedToken = (e, token) => {
  console.log('setting token', e, token);
};

const stories = storiesOf('Design System/Tree', module);

stories.add('TokenTree', () => {
  const story = <TokenTree setSelectedToken={setSelectedToken} usedTokens={[]} customTokens={fakeTokens} />;

  return story;
});
