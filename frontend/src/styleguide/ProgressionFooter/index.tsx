import React, { useState } from 'react';
import { Popconfirm, Progress } from 'antd';
import { LeftCircleOutlined, RightCircleOutlined, QuestionCircleOutlined } from '@ant-design/icons';
import { i18n } from 'i18n';
import IxcTheme from 'styleguide/styles/IxcTheme';
import IxButton from '../Buttons/IxButton';
import './style.less';

type Props = {
  id?: string;
  classes?: string;
  cypress?: string;
  canCancel?: boolean;
  onCancel?: () => void;
  canProgress?: boolean;
  onNext: (e?: any) => void;
  nextTip?: string;
  canBack?: boolean;
  onBack: (e?: any) => void;
  backTip?: string;
  canExit?: boolean;
  onExit?: () => void;
  steps: number;
  current: number;
  exitText?: string;
  nextText?: string;
  cancelText?: string;
  backText?: string;
  progressTip?: string;
  backCy?: string;
  nextCy?: string;
  flow?: string;
};

// TODO: Use step blocks instead of bar after upgrading to Ant Design 4.X
export const ProgressFooter = ({
  id,
  classes,
  cypress,
  canCancel,
  onCancel,
  canProgress,
  onNext,
  canBack,
  onBack,
  canExit,
  onExit,
  steps,
  current,
  exitText,
  progressTip,
  nextCy,
  backCy,
  nextTip,
  backTip,
  flow,
  nextText,
  cancelText,
  backText,
}: Props) => {
  const [showCancelConfirm, setShowCancelConfirm] = useState(false);

  const handleCloseCancel = () => {
    setShowCancelConfirm(false);
  };

  const handleOpenCancel = () => {
    setShowCancelConfirm(true);
  };

  return (
    <section id={id} className={`progress-footer ${classes}`} data-cy={cypress}>
      {canCancel && (
        <Popconfirm
          icon={<QuestionCircleOutlined style={{ color: 'red' }} />}
          title={i18n('common.cancelDelete', flow ? `This ${flow}` : 'This')}
          visible={showCancelConfirm}
          onCancel={handleCloseCancel}
          onConfirm={onCancel}
          okText={i18n('common.yes')}
          cancelText={i18n('common.no')}
        >
          <IxButton onClick={handleOpenCancel} leftIcon={<LeftCircleOutlined />} text={cancelText} size="large" className="btn-secondary" tipText={backTip} dataCy={backCy} />
        </Popconfirm>
      )}
      {!canCancel && current === 1 && <IxButton onClick={onExit} leftIcon={<LeftCircleOutlined />} text={exitText} size="large" className="btn-secondary" tipText={backTip} dataCy={backCy} />}
      {!canCancel && canBack && current !== 1 && (
        <IxButton
          onClick={() => {
            onBack();
            (document.activeElement as HTMLInputElement).blur();
          }}
          leftIcon={<LeftCircleOutlined />}
          text={backText}
          size="large"
          className="btn-secondary"
          tipText={backTip}
          dataCy={backCy}
        />
      )}
      <div className="progress-steps">
        <Progress className={`w${Math.floor(steps / 4)}`} percent={(current / steps) * 100} steps={steps} showInfo={false} strokeColor={IxcTheme.progress.stepperColor} />
        {`${current} / ${steps}`}
      </div>
      {canProgress && current < steps && <IxButton onClick={onNext} leftIcon={<RightCircleOutlined />} text={nextText} size="large" className="btn-primary" dataCy={nextCy} tipText={nextTip} />}
      {!canProgress && current < steps && <IxButton onClick={onNext} leftIcon={<RightCircleOutlined />} text={nextText} size="large" className="btn-primary" dataCy={nextCy} tipText={progressTip} disabled />}
      {canExit && current === steps && <IxButton onClick={onExit} leftIcon={<RightCircleOutlined />} text={exitText} size="large" className="btn-primary" tipText={backTip} dataCy={nextCy} />}
      {!canExit && current === steps && <IxButton onClick={onExit} leftIcon={<RightCircleOutlined />} text={exitText} size="large" className="btn-primary" tipText={backTip} dataCy={nextCy} disabled />}
    </section>
  );
};

ProgressFooter.defaultProps = {
  exitText: i18n('common.exit'),
  nextText: i18n('common.next'),
  cancelText: i18n('common.cancel'),
  backText: i18n('common.back'),
};
