// TODO: Add to UI library
import React, { CSSProperties, ChangeEvent, ReactNode, useState, useRef, useEffect } from 'react';
import { Input, Select, Tooltip } from 'antd';
import { i18n } from 'i18n';
import { getFramedText, isFramed } from 'utils/textOperations';
import SearchSelect from '../Input/SearchSelect';
import './EditInPlace.less';

const { TextArea } = Input;
const { Option, OptGroup } = Select;

interface TextProps {
  label: string;
  name: string;
  defaultValue?: string;
  value?: string;
  style?: CSSProperties;
  disabled?: boolean;
  errorConditional?: boolean;
  errorText?: string;
  id?: string;
  onChange: (event: ChangeEvent<HTMLInputElement>) => void;
  onBlur: (event: ChangeEvent<HTMLInputElement>) => void;
  onFocus?: (event: ChangeEvent<HTMLInputElement>) => void;
  addonAfter?: ReactNode;
  modal?: ReactNode;
}

interface AreaProps {
  label: string;
  name: string;
  defaultValue?: string;
  style?: CSSProperties;
  onChange: (event: ChangeEvent<HTMLTextAreaElement>) => void;
  onBlur: (event: ChangeEvent<HTMLTextAreaElement>) => void;
}

interface DropProps {
  label: string;
  name: string;
  defaultValue?: string;
  style?: CSSProperties;
  onChange: (event: ChangeEvent<HTMLSelectElement>) => void;
  onBlur: (event: ChangeEvent<HTMLSelectElement>) => void;
  onDropdownVisibleChange: Function;
  showArrow: boolean;
  options: number[];
  placeholder: string;
}

interface CustomDropProps {
  id: string;
  value: string;
  options: any;
  onChange: (e: any) => void;
  isOwner: boolean;
  isAdmin: boolean | undefined;
  isActive: boolean | undefined;
  disabled: boolean | undefined;
  container: any;
  billingPermission: boolean;
}

export const EditTextInPlace: React.FC<TextProps> = ({ id, name, defaultValue, value, style, onBlur, onFocus, modal, errorConditional, errorText, disabled = false }) => {
  const defaultStyle: CSSProperties = style || { resize: 'none' };
  if (errorConditional) {
    defaultStyle.borderColor = 'red';
  }
  const inputRef = useRef<HTMLInputElement | null>(null);
  const [holderValue, setValue] = useState(value || defaultValue || '');

  useEffect(() => {
    // Load new values when source changes
    if (value !== holderValue) {
      setValue(value || defaultValue || '');
    }
  }, [value]);

  const handleChangeValue = (e: any) => {
    let val = e.target.value || '';
    if (isFramed(val) && inputRef.current) {
      val = getFramedText(val);
      const cursorPos = inputRef.current.selectionStart;
      // inputRef.current.value = val;
      inputRef.current.selectionStart = cursorPos;
      inputRef.current.selectionEnd = cursorPos;
    }
    setValue(val);
  };

  return (
    <div className="edit-in-place">
      {name.length > 1 && <small>{i18n(`entities.part.fields[${name}]`)}</small>}
      <Tooltip placement="top" title={errorConditional ? errorText : ''}>
        <input id={id} className="ant-input" ref={inputRef} style={defaultStyle} key={defaultValue} name={name} disabled={disabled} value={holderValue} onBlur={onBlur} onChange={handleChangeValue} onFocus={onFocus} />
      </Tooltip>
      {modal}
    </div>
  );
};

export const EditAreaInPlace: React.FC<AreaProps> = ({ name, defaultValue, style, onBlur, onChange }) => {
  return (
    <div className="edit-in-place">
      {name.length > 1 ? <small>{i18n(`entities.part.fields[${name}]`)}</small> : null}
      <TextArea style={style} key={defaultValue} name={name} autoSize value={defaultValue} onBlur={onBlur} onChange={onChange} />
    </div>
  );
};

export const EditDropInPlace: React.FC<DropProps> = ({ name, defaultValue, style, onBlur, onChange, showArrow, options, placeholder, onDropdownVisibleChange }) => {
  return (
    <div className="edit-in-place">
      {name.length > 1 ? <small>{i18n(`entities.part.fields[${name}]`)}</small> : null}
      <SearchSelect
        style={style}
        key={defaultValue}
        name={name}
        autoSize
        defaultValue={defaultValue}
        placeholder={placeholder}
        showArrow={showArrow}
        options={options}
        value={defaultValue}
        onBlur={onBlur}
        onChange={onChange}
        onDropdownVisibleChange={onDropdownVisibleChange}
      />
    </div>
  );
};

export const EditRoleInPlace = ({ id, value, options, onChange, isOwner, isAdmin, isActive, disabled, container, billingPermission }: CustomDropProps) => {
  const getPopupContainer = () => container.current || container;
  return (
    <Select //
      key={`${id}-roleSelect`}
      data-cy={`${id}-roleSelect`}
      className="roles-select"
      value={value}
      onChange={onChange}
      optionLabelProp="value"
      dropdownMatchSelectWidth={false}
      dropdownClassName={`role-select-list ${id}-roleSelect`}
      disabled={disabled}
      getPopupContainer={getPopupContainer}
    >
      <OptGroup key={i18n('user.invite.paidRoles')} data-cy="paidRoles">
        {options.paid
          .filter((role: any) => !role.roles.includes('Owner') || (isOwner && isActive))
          .filter((role: any) => billingPermission || (!billingPermission && role.key !== 'plannerBilling'))
          .map((opt: any) => {
            return (
              <Option value={opt.label} key={opt.label}>
                <div className="custom-select-option" data-cy={`${opt.label.replaceAll(' ', '-')}-val`}>
                  <span className="custom-select-option-label">{opt.label}</span>
                  <span className="custom-select-option-description">{opt.description}</span>
                </div>
              </Option>
            );
          })}
      </OptGroup>
      <OptGroup key={i18n('user.invite.freeRoles')}>
        {options.free
          .filter((role: any) => billingPermission || (!billingPermission && role.key !== 'collaboratorBilling'))
          .map((opt: any) => {
            return (
              <Option value={opt.label} key={opt.label}>
                <div className="custom-select-option" data-cy={`${opt.label.replaceAll(' ', '-')}-val`}>
                  <span className="custom-select-option-label">{opt.label}</span>
                  <span className="custom-select-option-description">{opt.description}</span>
                </div>
              </Option>
            );
          })}
      </OptGroup>
      <OptGroup key={i18n('user.inactive.singular')}>
        <Option value={options.inactive.label} key={options.inactive.label}>
          <div className="custom-select-option" data-cy={`${options.inactive.label}-val`}>
            <span className="custom-select-option-label">{options.inactive.label}</span>
            <span className="custom-select-option-description">{options.inactive.description}</span>
          </div>
        </Option>
      </OptGroup>
    </Select>
  );
};

EditDropInPlace.defaultProps = {
  name: '',
  style: {},
  defaultValue: 'Select...',
  showArrow: false,
  options: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 100, 1000],
  onBlur: () => {},
};
