import { Select, Tooltip } from 'antd';
import React from 'react';
import { i18n } from 'i18n';
import './EditInPlace.less';

const { Option, OptGroup } = Select;

interface RoleOptions {
  paid: {
    label: string;
    description: string;
    roles: string[];
    key: string;
  }[];
  free: {
    label: string;
    description: string;
    roles: string[];
    key: string;
  }[];
  inactive: {
    label: string;
    description: string;
    roles: string[];
    key: string;
  };
}

interface Props {
  current: string;
  options: RoleOptions;
  change: (role: string) => void;
  limited?: boolean;
  container: any;
  billingPermission?: boolean;
  hasOpenSeat: boolean;
}

const Roles: React.FC<Props> = ({ current, options, change, limited = false, billingPermission = false, hasOpenSeat }) => {
  return (
    <Select id="modal-role-title" className="roles-select" data-cy="modal-role-input" defaultValue={current} optionLabelProp="label" dropdownMatchSelectWidth={false} onChange={change}>
      <OptGroup key={i18n('user.invite.paidRoles')}>
        {options.paid
          .filter((role) => limited && role.key !== 'owner')
          .filter((role) => billingPermission || (!billingPermission && role.key !== 'plannerBilling'))
          .map((opt: any) => {
            return (
              <Option value={opt.key} key={opt.key} label={opt.label} disabled={!hasOpenSeat}>
                <Tooltip title={hasOpenSeat ? undefined : i18n('user.invite.insufficientSeats')}>
                  <div className="custom-select-option" data-cy={`${opt.label}-val`}>
                    <span className="custom-select-option-label">{opt.label}</span>
                    <span className="custom-select-option-description">{opt.description}</span>
                  </div>
                </Tooltip>
              </Option>
            );
          })}
      </OptGroup>
      <OptGroup key={i18n('user.invite.freeRoles')}>
        {options.free
          .filter((role) => billingPermission || (!billingPermission && role.key !== 'collaboratorBilling'))
          .map((opt: any) => {
            return (
              <Option value={opt.key} key={opt.key} label={opt.label}>
                <div className="custom-select-option" data-cy={`${opt.label}-val`}>
                  <span className="custom-select-option-label">{opt.label}</span>
                  <span className="custom-select-option-description">{opt.description}</span>
                </div>
              </Option>
            );
          })}
      </OptGroup>
    </Select>
  );
};

Roles.defaultProps = {
  current: 'Planner',
  options: undefined,
  change: (role: string) => null,
};
export default Roles;
