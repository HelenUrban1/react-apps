import React, { Fragment } from 'react';
import { storiesOf } from '@storybook/react';
import { boolean, select, text, withKnobs } from '@storybook/addon-knobs';
import { useState } from '@storybook/addons';
import { EditTextInPlace, EditAreaInPlace, EditDropInPlace } from './EditInPlace';
import { roles } from '../../view/global/defaults';
import Roles from './Roles';

const stories = storiesOf('Design System/EditInPlace', module).addDecorator(withKnobs);

stories
  .add('Text', () => {
    const story = <EditTextInPlace label="" name="" defaultValue="Current Text" />;

    return story;
  })
  .add('TextArea', () => {
    const story = <EditAreaInPlace label="" name="" defaultValue="Current Text" />;

    return story;
  })
  .add('Dropdown', () => {
    const story = <EditDropInPlace label="" name="" defaultValue={1} options={[1, 2, 3, 4, 5, 6]} />;

    return story;
  })
  .add('Roles', () => {
    const [current, setCurrent] = useState('planner');
    const change = (role) => {
      setCurrent(role);
    };

    const story = <Roles current={current} options={roles} change={change} />;

    return story;
  });
