import React from 'react';
import { storiesOf } from '@storybook/react';
import { VerifiedStatus } from './VerifiedStatus';
import { boolean, select, text, withKnobs } from '@storybook/addon-knobs';
import IxcTheme from '../styles/IxcTheme';

const stories = storiesOf('Design System/VerifiedStatus', module).addDecorator(withKnobs);

stories
  .add('Verified', () => {
    const story = <VerifiedStatus verified={true} />;

    return story;
  })
  .add('Unverified', () => {
    const story = <VerifiedStatus verified={false} />;

    return story;
  });
