import React from 'react';
import { Switch } from 'antd';
import { i18n } from 'i18n';
import './VerifiedStatus.less';
import { useSelector } from 'react-redux';
import { AppState } from 'modules/state';

interface Props {
  verified: boolean;
  verify: (checked: boolean, parse?: boolean) => void;
  isRevision: boolean;
}

export const VerifiedStatus: React.FC<Props> = ({ verified, verify, isRevision }) => {
  const { parsed, loaded } = useSelector((state: AppState) => state.characteristics);

  const handleMouseDown = (e: any) => {
    if (!parsed) {
      // prevents blurring Full Spec if input is focused and has been edited
      e.preventDefault();
      e.stopPropagation();
    }
  };

  return (
    <div role="none" className="verified-status-container  group-flex-3" onMouseDown={handleMouseDown}>
      <Switch //
        className="verified"
        data-cy="verification-status"
        checked={verified}
        checkedChildren={<span className="verified-status-label">{!loaded ? `${i18n('common.saving')}...` : i18n('entities.feature.reviewStatus.Verified')}</span>}
        unCheckedChildren={<span className="verified-status-label">{!loaded ? `${i18n('common.saving')}...` : i18n('entities.feature.reviewStatus.Unverified')}</span>}
        onClick={(checked: boolean) => verify(checked, !parsed)}
        disabled={!loaded || isRevision}
      />
    </div>
  );
};

VerifiedStatus.defaultProps = {
  /* Verified Status */
  verified: false,
};
