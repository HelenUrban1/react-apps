import React from 'react';
import { i18n } from 'i18n';
import { Icon } from 'styleguide';
import { ReportPartial } from 'types/reportTemplates';
import { dateIsSameOrBefore } from 'utils/DataCalculation';
import ReportMiniCard from './ReportMiniCard';
import '../MiniList.less';

interface Props {
  reports?: ReportPartial[];
  partEdited: Date | null;
  prompt?: boolean;
  handleUpdatePrompt?: () => void;
  download: ({ publicUrl, fileName, type, partId, font, unframed }: { publicUrl: string; fileName: string; type?: string; partId?: string; font?: boolean; unframed?: boolean }) => void;
  edit: (report: ReportPartial) => void;
  deleteReport: (report: ReportPartial) => void;
  create: () => void;
  canEdit?: boolean;
}

const ReportMiniList = ({ reports, partEdited, download, edit, deleteReport, create, prompt = false, handleUpdatePrompt, canEdit = true }: Props) => {
  return (
    <div className="mini-list" data-cy="reportMiniList">
      <h2>
        <span>
          <Icon icon="copy" block={false} /> {i18n('entities.report.list.mini')}
        </span>
        <small>{i18n('common.total', reports?.length || 0)}</small>
      </h2>
      <div className="list-container">
        {reports &&
          reports.map((report) => {
            const isCurrent = dateIsSameOrBefore(report.updatedAt, partEdited);
            return (
              <ReportMiniCard
                id={report.id}
                key={report.id}
                title={report.title}
                dataStatus={isCurrent}
                prompt={prompt}
                handleUpdatePrompt={handleUpdatePrompt}
                download={(type: string, font?: boolean, unframed?: boolean) => {
                  if (report.file[0].publicUrl) {
                    download({ publicUrl: report.file[0].publicUrl, fileName: report.title, type, font, unframed });
                  }
                }}
                edit={() => {
                  edit(report);
                }}
                deleteReport={() => {
                  deleteReport(report);
                }}
                canEdit={canEdit}
              />
            );
          })}
      </div>
      {canEdit && (
        <button type="button" className="add-button additional" onClick={create} data-cy="list-new-report">
          <Icon icon="plus" block={false} />
          {i18n('entities.report.create.button')}
        </button>
      )}
    </div>
  );
};
export default ReportMiniList;
