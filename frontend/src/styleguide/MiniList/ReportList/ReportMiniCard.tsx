import React, { useState } from 'react';
import { i18n } from 'i18n';
import { DownloadOutlined, EditOutlined, FileExcelOutlined, FileTextOutlined } from '@ant-design/icons';
import { List, Tooltip, Menu, Dropdown, Button } from 'antd';
import { ConfirmDelete } from 'view/global/confirmDelete';
import { GdtFontModal } from 'styleguide/Modals/GdtFontModal';

interface Props {
  id: string;
  title: string;
  dataStatus: boolean;
  prompt: boolean;
  handleUpdatePrompt?: () => void;
  download: (type: string, font?: boolean, unframed?: boolean) => void;
  edit: () => void;
  deleteReport: () => void;
  canEdit?: boolean;
}

const ReportMiniCard = ({ id, title, dataStatus, download, edit, deleteReport, prompt, handleUpdatePrompt, canEdit = true }: Props) => {
  const [show, setShow] = useState<any>();
  const [framed, toggleFramed] = useState(false);
  let hideGDTModal = false;

  const noFont = () => {
    setShow(undefined);
    if (hideGDTModal && handleUpdatePrompt) {
      handleUpdatePrompt();
    }
    download(show, false, framed);
  };

  const withFont = () => {
    setShow(undefined);
    if (hideGDTModal && handleUpdatePrompt) {
      handleUpdatePrompt();
    }
    download(show, true, framed);
  };

  const unFramed = () => {
    toggleFramed((prevState) => !prevState);
  };

  const downloadMenu = (
    <Menu>
      <Menu.Item className="downloadLI">
        <Button
          icon={<FileExcelOutlined />}
          className="excelDownload btn-dropdown"
          data-cy="excel-action-download-excel"
          onClick={() => {
            if (prompt) {
              setShow('xlsx');
            } else {
              download('xlsx', false, framed);
            }
          }}
        >
          {i18n('files.excel')}
        </Button>
      </Menu.Item>
      <Menu.Item className="downloadLI">
        <Button
          icon={<FileTextOutlined />}
          className="excelDownload btn-dropdown"
          data-cy="excel-action-download-csv"
          onClick={() => {
            if (prompt) {
              setShow('csv');
            } else {
              download('csv', false, framed);
            }
          }}
        >
          {i18n('files.csv')}
        </Button>
      </Menu.Item>
      <Menu.Item disabled>
        <label htmlFor="convertFramed" className="checkboxMenu">
          <input data-cy="excel-action-convert" id="convertFramed" name="convertFramed" type="checkbox" onChange={unFramed} />
          {i18n('prompts.font.convert')}
        </label>
      </Menu.Item>
    </Menu>
  );

  return (
    <List.Item className="mini-list-card" key={id} data-cy="reportMiniListCard">
      <GdtFontModal
        show={!!show}
        handleCancel={() => {
          setShow(undefined);
        }}
        handleChange={() => {
          hideGDTModal = !hideGDTModal;
        }}
        handleNoFont={noFont}
        handleWithFont={withFont}
      />
      <Tooltip title={dataStatus ? 'Up to Date' : 'Out of Date'}>
        <div className={`card-status current-${dataStatus}`} data-cy="report-status" />
      </Tooltip>
      <div key={`${title}-${id}`} className="card-content">
        {title}
      </div>
      <div className="card-actions" data-cy="reportCardActions">
        <Dropdown overlay={downloadMenu} overlayClassName="excelActionDownload" trigger={['click']} placement="bottomRight">
          <button type="button" className="icon-button" data-cy="reportCardDownload" onClick={(e) => e.preventDefault()}>
            <DownloadOutlined />
          </button>
        </Dropdown>
        {canEdit && (
          <button type="button" className="icon-button" onClick={edit} data-cy="reportCardEdit">
            <EditOutlined />
          </button>
        )}
        {canEdit && <ConfirmDelete classes="icon-button report-delete" confirm={deleteReport} colored={false} />}
      </div>
    </List.Item>
  );
};
export default ReportMiniCard;
