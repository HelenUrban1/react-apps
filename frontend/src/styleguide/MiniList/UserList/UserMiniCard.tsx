import React, { useEffect, useRef, useState } from 'react';
import moment from 'moment';
import { i18n } from 'i18n';
import { QuestionCircleOutlined } from '@ant-design/icons';
import { Button, List, notification, Popconfirm, Select, Tooltip } from 'antd';
import { User, UserStatusEnum } from 'types/user';
import { IxAvatar } from 'styleguide/Avatar/IxAvatar';
import { roles, documentControls, ControlLevel } from 'view/global/defaults';
import { EditRoleInPlace } from 'styleguide/EditInPlace/EditInPlace';
import { IamEditFields } from 'domain/setting/iamApi';
import { useDispatch, useSelector } from 'react-redux';
import { AppState } from 'modules/state';
import ButtonLink from 'view/shared/styles/ButtonLink';
import actions from 'modules/auth/authActions';

const { Option, OptGroup } = Select;

interface Props {
  id: string;
  user: User;
  resend: (email: string) => void;
  cancel: (email: string) => void;
  acceptRequest: (email: string, userId: string) => void;
  rejectRequest: (email: string) => void;
  currentUser: User;
  handleIamEdit: (user: User, iamEditInput: IamEditFields) => void;
  handleTransferOwner: (id: string) => void;
  isOwner: boolean;
  container: any;
  billingPermission: boolean;
}

const isPending = (user: User) => {
  return user.status === 'Pending';
};
const isRequesting = (user: User) => {
  return user.status === 'Requesting';
};
const isExpired = (user: User) => {
  return !user.emailVerified && user?.emailVerificationTokenExpiresAt && moment(user.emailVerificationTokenExpiresAt).diff(moment()) <= 0;
};
const isInactive = (user: User) => {
  return user.disabled || user.status === 'Archived';
};

const UserMiniCard = ({ id, user, currentUser, handleIamEdit, handleTransferOwner, isOwner, resend, cancel, acceptRequest, rejectRequest, container, billingPermission }: Props) => {
  const [itarConfirmVisible, setItarConfirmVisible] = useState(false);
  const [membership, setAccountMembership] = useState(user.accountMemberships.find((acc) => acc.accountId === user.activeAccountId));
  const [accessValue, setAccessValue] = useState<boolean>(membership?.accessControl || false);
  const [showRoleConfirm, setShowRoleConfirm] = useState<boolean>(false);
  const [roleConfirmMessage, setRoleConfirmMessage] = useState<string>(i18n('settings.seats.removeSelfAdmin'));
  const newUserRoles = useRef<{ status: UserStatusEnum; roles: string[] }>({
    status: 'Active',
    roles: [],
  });
  const { data: subscription } = useSelector((reduxState: AppState) => reduxState.subscription);
  const { mfaEnabled } = useSelector((state: AppState) => state.auth);

  const [mfaReset, setMfaReset] = useState<any>(false);

  const dispatch = useDispatch();

  const handleCancelInvite = () => {
    cancel(user.email);
  };

  const handleResendInvitation = () => {
    resend(user.email);
  };

  const handleAcceptRequest = () => {
    acceptRequest(currentUser.email, user.id);
  };

  const handleRejectRequest = () => {
    rejectRequest(user.emailVerificationToken || '');
  };

  useEffect(() => {
    // updates the active account membership after the user has been edited
    setAccountMembership(user.accountMemberships.find((acc) => acc.accountId === user.activeAccountId));
  }, [user]);

  const handleChangeAccessLevel = (level: string) => {
    if (!membership) return;
    if (level === 'None' && membership.accessControl) {
      // removing access control, show ITAR Popconfirm
      setItarConfirmVisible(true);
    } else if (level === 'ITAR/EAR' && !membership.accessControl) {
      // adding access control, no confirmation needed
      handleIamEdit(user, { accessControl: true });
      setAccessValue(true);
    }
  };

  // switches an accountMember accessControl field for true to false removing restrictions on document access
  const handleRemoveAccessControl = () => {
    if (!membership) return;
    handleIamEdit(user, { accessControl: false });
    setItarConfirmVisible(false);
    setAccessValue(false);
  };

  const getPopupContainer = () => container.current;

  const getAccessField = () => {
    if (isExpired(user) || isPending(user)) {
      return (
        <>
          <Button type="link" onClick={handleResendInvitation} data-cy={`${id}-resend`}>
            {i18n('common.resend')}
          </Button>
          <span>|</span>
          <Popconfirm title={i18n('common.areYouSure')} icon={<QuestionCircleOutlined style={{ color: 'red' }} />} onConfirm={handleCancelInvite}>
            <Button type="link" data-cy={`${id}-cancel`}>
              {i18n('common.cancel')}
            </Button>
          </Popconfirm>
        </>
      );
    }
    if (isRequesting(user)) {
      return (
        <>
          <Button type="link" onClick={handleAcceptRequest} data-cy={`${id}-accept`}>
            {i18n('common.accept')}
          </Button>
          <span>|</span>
          <Popconfirm title={i18n('common.areYouSure')} icon={<QuestionCircleOutlined style={{ color: 'red' }} />} onConfirm={handleRejectRequest}>
            <Button type="link" data-cy={`${id}-reject`}>
              {i18n('common.reject')}
            </Button>
          </Popconfirm>
        </>
      );
    }
    if (!membership || membership.status === 'Archived') {
      return <></>;
    }

    return (
      <Popconfirm
        title={i18n('settings.seats.removeRestriction')}
        icon={<QuestionCircleOutlined style={{ color: 'red' }} />}
        onConfirm={handleRemoveAccessControl}
        onCancel={() => setItarConfirmVisible(false)}
        visible={itarConfirmVisible}
        getPopupContainer={getPopupContainer}
      >
        <Select
          className="access-control-select"
          data-cy={`access-select-${user.email}`}
          value={accessValue ? 'ITAR/EAR' : 'None'}
          key={`${user.id}-access-select-key`}
          onChange={(level: string) => handleChangeAccessLevel(level)}
          defaultValue={membership?.accessControl ? 'ITAR/EAR' : i18n('common.none')}
          dropdownClassName={`${id}-accessSelect`}
          optionLabelProp="key"
          dropdownMatchSelectWidth={250}
          disabled={!currentUser.roles?.includes('Owner')}
          getPopupContainer={getPopupContainer}
        >
          <OptGroup key={i18n('entities.part.fields.accessControl')}>
            {documentControls.map((control: ControlLevel) => {
              return (
                <Option key={control.label} value={control.value} data-cy={`access-${control.name}`}>
                  <span className="doc-control-value custom-select-option" data-cy={`select-option-${control.name.replace('/', '-')}`}>
                    <span className="custom-select-option-label">{control.label}</span>
                    <span className="custom-select-option-description">{control.description}</span>
                  </span>
                </Option>
              );
            })}
          </OptGroup>
        </Select>
      </Popconfirm>
    );
  };

  const getCurrentRole = () => {
    if (isInactive(user)) {
      return i18n('user.inactive.singular');
    }
    if (user.paid) {
      if (user.roles?.includes('Owner')) {
        return i18n('user.owner.singular');
      }
      if (user.roles?.includes('Billing')) {
        return i18n('user.billing.planner');
      }
      return i18n('user.planner.singular');
    }
    if (user.roles?.includes('Billing')) {
      return i18n('user.billing.singular');
    }
    if (user.roles?.includes('Collaborator')) {
      return i18n('user.collaborator.singular');
    }
    return i18n('user.viewer.singular');
  };

  const getNewRoles = (role: string) => {
    const paidRole = roles.paid.find((namedRole) => namedRole.label === role);
    if (paidRole) {
      // changed to a paid role, check for open seat and change if available
      return paidRole.roles;
    }
    // changed to a free role
    const freeRole = roles.free.find((namedRole) => namedRole.label === role);
    if (!freeRole || freeRole.roles === user.roles) {
      return user.roles;
    }
    return freeRole.roles;
  };

  // function called on Popconfirm confirm for removal of self admin role or transfer of ownership
  const handleSpecialRoleChange = () => {
    setShowRoleConfirm(false);
    if (!newUserRoles.current || !currentUser) return;

    if (roleConfirmMessage === i18n('settings.seats.transferOwner')) {
      // transfer ownership
      handleTransferOwner(user.id);
    } else {
      // remove self admin
      handleIamEdit(user, {
        status: newUserRoles.current.status,
        roles: newUserRoles.current.roles,
      });
    }
  };

  const handleRoleChange = (e: any) => {
    const newRoles = getNewRoles(e);
    const status = e === i18n('user.inactive.singular') ? 'Archived' : user.status;

    // user is trying to transfer ownership
    if (newRoles?.includes('Owner')) {
      setRoleConfirmMessage(i18n('settings.seats.transferOwner'));
      setShowRoleConfirm(true);
    } else if (!newRoles?.includes('Admin') && currentUser.id === user.id && newRoles) {
      // user is removing their own Admin role
      setRoleConfirmMessage(i18n('settings.seats.removeSelfAdmin'));
      setShowRoleConfirm(true);
      newUserRoles.current = { status: status || 'Active', roles: newRoles };
    } else if (!newRoles?.includes('Billing') && user.id === subscription?.billingId && newRoles) {
      // user is removing Billing role from billing contact
      notification.error({
        description: i18n('errors.billingRoleDescription'),
        message: i18n('errors.billingRoleTitle'),
        duration: 8,
      });
      newUserRoles.current = { status: status || 'Active', roles: user.roles || newRoles };
    } else {
      if (e === 'Inactive') {
        // handle setting user to inactive
        handleIamEdit(user, { status: 'Archived' });
        return;
      }

      // user picked same role as current
      if (newRoles === user.roles) return;

      // update roles, switch status to Active if not currently active
      handleIamEdit(user, { status: 'Active', roles: newRoles });
    }
  };

  const handleResetMfa = async () => {
    const isReset = await dispatch(actions.doResetMfa(user.email));
    setMfaReset(isReset);
  };

  const getRoleField = () => {
    if (isInactive(user) && isExpired(user)) {
      return <span className="invite-pending">{i18n('user.invite.cancelled')}</span>;
    }
    if (isExpired(user)) {
      return <span className="invite-pending">{i18n('user.invite.expired')}</span>;
    }
    if (isPending(user)) {
      return <span className="invite-pending">{i18n('user.invite.pending')}</span>;
    }
    if (isRequesting(user)) {
      return <span className="invite-pending">{i18n('user.invite.requesting')}</span>;
    }
    return (
      <EditRoleInPlace
        container={container}
        value={getCurrentRole()}
        id={user.id}
        options={roles}
        onChange={handleRoleChange}
        isOwner={isOwner}
        isAdmin={user.roles?.includes('Admin')}
        isActive={user.status === 'Active'}
        disabled={user.roles?.includes('Owner') || (!billingPermission && user.roles?.includes('Billing'))}
        billingPermission={billingPermission}
      />
    );
  };
  return (
    <List.Item className={`mini-list-card user-card-override${isInactive(user) ? ' disabled-user-card' : ''}`} key={id} data-cy="userMiniListCard">
      <IxAvatar type="user" data={user} size="large" />
      <div className="card-content user-content-override" data-cy={user.email}>
        <div className="user-field">
          {!!user.fullName && <span className="name">{user.fullName}</span>}
          <a className={`email${user.fullName ? '' : ' large'}`} data-cy="email-link" href={`mailto:${user.email}`} target="_blank" rel="noreferrer">
            {user.email}
          </a>
        </div>
        <Popconfirm title={roleConfirmMessage} visible={showRoleConfirm} icon={<QuestionCircleOutlined style={{ color: 'red' }} />} onCancel={() => setShowRoleConfirm(false)} onConfirm={handleSpecialRoleChange}>
          <div className={`role-field${isPending(user) ? ' role-pending' : ''}`}>{getRoleField()}</div>
        </Popconfirm>
        <div className="access-field">{getAccessField()}</div>
        {currentUser.roles?.includes('Owner') && (
          <Tooltip title={user.mfaRegistered ? '' : i18n('settings.security.inactive')}>
            <div className="mfa-reset-link">
              {!(isExpired(user) || isPending(user)) && user.id !== currentUser.id && (
                <Popconfirm title={i18n('settings.security.confirmReset')} okText={i18n('common.yes')} cancelText={i18n('common.no')} onConfirm={handleResetMfa}>
                  <ButtonLink disabled={mfaReset === true || !user.mfaRegistered}>{i18n('settings.security.resetLabel')}</ButtonLink>
                </Popconfirm>
              )}
            </div>
          </Tooltip>
        )}
      </div>
    </List.Item>
  );
};
export default UserMiniCard;
