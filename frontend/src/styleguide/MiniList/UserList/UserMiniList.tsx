import React, { ReactNode, useRef } from 'react';
import { i18n } from 'i18n';
import { FileSearchOutlined, SolutionOutlined, TeamOutlined } from '@ant-design/icons';
import { User } from 'types/user';
import { IamEditFields } from 'domain/setting/iamApi';
import UserMiniCard from './UserMiniCard';
import '../MiniList.less';
import ReviewerMiniCard from './ReviewerMiniCard';

interface Props {
  currentUser: User;
  users: User[] | undefined;
  title: string;
  icon: string;
  resend: (email: string) => void;
  cancel: (email: string) => void;
  rejectRequest: (token: string) => void;
  acceptRequest: (email: string, userId: string) => void;
  handleIamEdit: (user: User, iamEditInput: IamEditFields) => void;
  handleTransferOwner: (id: string) => void;
  permissions: {
    hasOwnership: boolean;
    hasManageAccess: boolean;
    hasBillingAccess: boolean;
  };
  settingsContainer: any;
  reviewAccount: boolean;
}

const icons: { [key: string]: ReactNode } = {
  'file-search': <FileSearchOutlined />,
  solution: <SolutionOutlined />,
  team: <TeamOutlined />,
};

const UserMiniList = ({ currentUser, users, title, icon, handleIamEdit, handleTransferOwner, permissions, resend, cancel, rejectRequest, acceptRequest, settingsContainer, reviewAccount }: Props) => {
  const ref = useRef(null);

  return (
    <div className="mini-list" data-cy={`${title}-userMiniList`}>
      <h2>
        <span>
          {icons[icon]}
          {title}
        </span>
        <small>{i18n('common.total', users?.length || 0)}</small>
      </h2>
      <div ref={ref} className="list-container list-container-override">
        {users &&
          users.map((user) => {
            return reviewAccount ? (
              <ReviewerMiniCard
                key={user.id}
                id={user.id}
                user={user}
                currentUser={currentUser}
                handleIamEdit={handleIamEdit}
                isOwner={permissions.hasOwnership}
                resend={resend}
                cancel={cancel}
                acceptRequest={acceptRequest}
                rejectRequest={rejectRequest}
              />
            ) : (
              <UserMiniCard
                key={user.id}
                id={user.id}
                user={user}
                currentUser={currentUser}
                handleIamEdit={handleIamEdit}
                handleTransferOwner={handleTransferOwner}
                isOwner={permissions.hasOwnership}
                resend={resend}
                cancel={cancel}
                acceptRequest={acceptRequest}
                rejectRequest={rejectRequest}
                container={users.length > 4 ? ref : settingsContainer}
                billingPermission={permissions.hasBillingAccess}
              />
            );
          })}
      </div>
    </div>
  );
};
export default UserMiniList;
