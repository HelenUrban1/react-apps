import React from 'react';
import moment from 'moment';
import { i18n } from 'i18n';
import { QuestionCircleOutlined } from '@ant-design/icons';
import { Button, List, Popconfirm, Select } from 'antd';
import { User } from 'types/user';
import { IxAvatar } from 'styleguide/Avatar/IxAvatar';
import { IamEditFields } from 'domain/setting/iamApi';

const { Option, OptGroup } = Select;

interface Props {
  id: string;
  user: User;
  resend: (email: string) => void;
  cancel: (email: string) => void;
  rejectRequest: (token: string) => void;
  acceptRequest: (email: string, userId: string) => void;
  currentUser: User;
  handleIamEdit: (user: User, iamEditInput: IamEditFields) => void;
  isOwner: boolean;
}

const isPending = (user: User) => {
  return user.status === 'Pending';
};
const isRequesting = (user: User) => {
  return user.status === 'Requesting';
};
const isExpired = (user: User) => {
  return !user.emailVerified && user?.emailVerificationTokenExpiresAt && moment(user.emailVerificationTokenExpiresAt).diff(moment()) <= 0;
};
const isInactive = (user: User) => {
  return user.disabled || user.status === 'Archived';
};

const ReviewerMiniCard = ({ id, user, currentUser, handleIamEdit, isOwner, resend, cancel, rejectRequest, acceptRequest }: Props) => {
  const handleCancelInvite = () => {
    cancel(user.email);
  };

  const handleResendInvitation = () => {
    resend(user.email);
  };

  const handleAcceptRequest = () => {
    acceptRequest(currentUser.email, user.id);
  };

  const handleRejectRequest = () => {
    rejectRequest(user.emailVerificationToken || '');
  };

  const getAccessField = () => {
    if (isExpired(user)) {
      return (
        <>
          <Button type="link" onClick={handleResendInvitation} data-cy={`${id}-resend`}>
            {i18n('common.resend')}
          </Button>
        </>
      );
    }
    if (isPending(user)) {
      return (
        <>
          <Button type="link" onClick={handleResendInvitation} data-cy={`${id}-resend`}>
            Resend
          </Button>
          <span>|</span>
          <Popconfirm title={i18n('common.areYouSure')} icon={<QuestionCircleOutlined style={{ color: 'red' }} />} onConfirm={handleCancelInvite}>
            <Button type="link" data-cy={`${id}-cancel`}>
              {i18n('common.cancel')}
            </Button>
          </Popconfirm>
        </>
      );
    }

    if (isRequesting(user)) {
      return (
        <>
          <Button type="link" onClick={handleAcceptRequest} data-cy={`${id}-accept`}>
            {i18n('common.accept')}
          </Button>
          <span>|</span>
          <Popconfirm title={i18n('common.areYouSure')} icon={<QuestionCircleOutlined style={{ color: 'red' }} />} onConfirm={handleRejectRequest}>
            <Button type="link" data-cy={`${id}-reject`}>
              {i18n('common.reject')}
            </Button>
          </Popconfirm>
        </>
      );
    }
    return <></>;
  };

  const getCurrentRole = () => {
    if (isInactive(user)) {
      return i18n('user.inactive.singular');
    }
    if (user.roles?.includes('Owner')) {
      return i18n('user.owner.singular');
    }
    return 'Reviewer';
  };

  const handleChangeStatus = (e: any) => {
    const status = e === i18n('user.inactive.singular') ? 'Archived' : 'Active';

    // handle setting user to inactive
    handleIamEdit(user, { status });
  };

  const getRoleField = () => {
    if (isInactive(user) && isExpired(user)) {
      return <span className="invite-pending">{i18n('user.invite.cancelled')}</span>;
    }
    if (isExpired(user)) {
      return <span className="invite-pending">{i18n('user.invite.expired')}</span>;
    }
    if (isPending(user)) {
      return <span className="invite-pending">Invite Pending</span>;
    }
    if (isRequesting(user)) {
      return <span className="invite-pending">{i18n('user.invite.requesting')}</span>;
    }
    return (
      <Select key={Math.random()} className="roles-select" defaultValue={getCurrentRole()} onChange={handleChangeStatus} optionLabelProp="value" dropdownMatchSelectWidth={false} disabled={user.roles?.includes('Owner')}>
        <OptGroup key="Roles">
          <Option value="Reviewer" key="Reviewer">
            <div className="custom-select-option" data-cy="reviewer-val">
              <span className="custom-select-option-label">Human Reviewer</span>
              <span className="custom-select-option-description">Active Human Reviewer</span>
            </div>
          </Option>
          <Option value="Inactive" key="Inactive">
            <div className="custom-select-option" data-cy="inactive-val">
              <span className="custom-select-option-label">Inactive</span>
              <span className="custom-select-option-description">Inactive Human Reviewer</span>
            </div>
          </Option>
        </OptGroup>
      </Select>
    );
  };

  return (
    <List.Item className={`mini-list-card user-card-override${isInactive(user) ? ' disabled-user-card' : ''}`} key={id} data-cy="userMiniListCard">
      <IxAvatar type="user" data={user} size="large" />
      <div className="card-content user-content-override" data-cy={user.email}>
        <div className="user-field">
          {!!user.fullName && <span className="name">{user.fullName}</span>}
          <a className={`email${user.fullName ? '' : ' large'}`} data-cy="email-link" href={`mailto:${user.email}`} target="_blank" rel="noreferrer">
            {user.email}
          </a>
        </div>
        <div className={`role-field${isPending(user) ? ' role-pending' : ''}`}>{getRoleField()}</div>
        <div className="access-field">{getAccessField()}</div>
      </div>
    </List.Item>
  );
};
export default ReviewerMiniCard;
