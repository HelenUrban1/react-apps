import React, { ReactNode } from 'react';
import { DownloadOutlined, FileExcelOutlined, FileImageOutlined, FileTextOutlined, FilePdfOutlined, FileZipOutlined, FileOutlined, EnvironmentOutlined, FileAddOutlined } from '@ant-design/icons';
import { Dropdown, List, Menu, Tooltip } from 'antd';
import { ConfirmDelete } from 'view/global/confirmDelete';
import { i18n } from 'i18n';
import { useSelector } from 'react-redux';
import { AppState } from 'modules/state';
import { Drawing } from 'graphql/drawing';
import moment from 'moment';

interface Props {
  id: string;
  name: string;
  primary: string;
  dataIcon: string;
  download: (e: any) => void;
  deleteDocument: () => void;
  handleStartRevision?: (drawingId: string) => void;
  canEdit?: boolean;
  versions?: Drawing[];
}

const fileIcons: { [key: string]: ReactNode } = {
  'file-excel': <FileExcelOutlined />,
  'file-image': <FileImageOutlined />,
  'file-text': <FileTextOutlined />,
  'file-pdf': <FilePdfOutlined />,
  'file-zip': <FileZipOutlined />,
  environment: <EnvironmentOutlined />,
  file: <FileOutlined />,
};

const DocumentMiniCard = ({ id, name, dataIcon, primary, download, deleteDocument, handleStartRevision, canEdit = true, versions }: Props) => {
  const { revisionIndex, part } = useSelector((state: AppState) => state.part);

  const getMenuOptions = (vers: Drawing[]) => {
    return vers.map((v) => {
      if (v.characteristics && v.characteristics.length > 0) {
        return (
          <>
            <Menu.Item key={`ballooned ${v.id}`}>
              <div key={`menu-item-ballooned-${v.id}`} className="download-menu-item">
                <Tooltip key={`menu-item-ballooned-${v.id}-tip`} title="Ballooned">
                  <EnvironmentOutlined />
                </Tooltip>
                {`${v.number || v.name} ${v.revision}` || moment(v.createdAt).format('YYYY-MM-DD')}
              </div>
            </Menu.Item>
            <Menu.Item key={`original ${v.id}`}>
              <div key={`menu-item-original-${v.id}`} className="download-menu-item">
                <Tooltip key={`menu-item-original-${v.id}-tip`} title="Original">
                  <FilePdfOutlined />
                </Tooltip>
                {`${v.number || v.name} ${v.revision}` || moment(v.createdAt).format('YYYY-MM-DD')}
              </div>
            </Menu.Item>
          </>
        );
      }
      return (
        <Menu.Item key={`original ${v.id}`}>
          <div key={`menu-item-original-${v.id}`} className="download-menu-item">
            <Tooltip key={`menu-item-original-${v.id}-tip`} title="Original">
              <FilePdfOutlined />
            </Tooltip>
            {`${v.number || v.name} ${v.revision}` || moment(v.createdAt).format('YYYY-MM-DD')}
          </div>
        </Menu.Item>
      );
    });
  };

  return (
    <List.Item className="mini-list-card" key={`${id}-list-item`} data-cy="documentMiniListCard" data-primary={id === primary} data-ballooned={dataIcon === 'environment'}>
      <Tooltip title={dataIcon === 'environment' ? i18n('entities.drawing.list.ballooned') : i18n('entities.drawing.list.original')} key={`${id}-card-tip-key`}>
        {fileIcons[dataIcon]}
      </Tooltip>
      <div className="card-content" key={`${id}-card-content-key`}>
        {name}
      </div>
      <div className="card-actions" key={`${id}-card-actions-key`} data-cy="documentCardActions">
        {handleStartRevision && revisionIndex === 0 && (
          <Tooltip title={(part?.workflowStage || 0) < 1 ? i18n('revisions.stage') : i18n('revisions.createDrawing')} placement="topLeft">
            <button type="button" key={`${id}-card-rev-btn`} className="icon-button" onClick={() => handleStartRevision(id)} data-cy="documentCardRevision" disabled={(part?.workflowStage || 0) < 1}>
              <FileAddOutlined />
            </button>
          </Tooltip>
        )}
        <Tooltip title={i18n('common.download')} placement="topRight">
          {versions ? (
            <Dropdown placement="bottomRight" overlay={<Menu onClick={download}>{getMenuOptions(versions)}</Menu>}>
              <button key={`${id}-card-dl-btn`} type="button" className="icon-button">
                <DownloadOutlined />
              </button>
            </Dropdown>
          ) : (
            <button type="button" key={`${id}-card-dl-btn`} className="icon-button" onClick={download} data-cy="documentCardDownload">
              <DownloadOutlined />
            </button>
          )}
        </Tooltip>
        {id !== primary && canEdit && <ConfirmDelete classes="icon-button document-delete" confirmText="Delete document and any associated items?" confirm={deleteDocument} colored={false} placement="topLeft" />}
      </div>
    </List.Item>
  );
};
export default DocumentMiniCard;
