import React, { useEffect, useState } from 'react';
import { Icon } from 'styleguide';
import { BuildOutlined, PaperClipOutlined } from '@ant-design/icons';
import { getFileExtension } from 'utils/files';
import { File } from 'types/file';
import { Drawing } from 'graphql/drawing';
import { i18n } from 'i18n';
import moment from 'moment';
import { List } from 'antd';
import DocumentMiniCard from './DocumentMiniCard';
import '../MiniList.less';

interface Props {
  documents?: any[];
  primary: string;
  download: (publicUrl: string, fileName: string, fileType?: string, partId?: string, drawingId?: string) => void;
  deleteDocument: (document: Drawing | File, type: 'Drawing' | 'Attachment' | 'Report') => void;
  // TODO: currently the only documents are drawings, but we'll need to fix this when we support other documents
  create: () => void;
  handleStartRevision?: (drawingId: string) => void;
  canEdit?: boolean;
  partId: string;
  type: 'Drawing' | 'Attachment' | 'Report';
}

const getIcon = (url: string) => {
  let icon: string;
  const extension = getFileExtension(url);

  if (!extension) {
    return 'file';
  }

  switch (extension.toLowerCase()) {
    case '.xlsx':
    case '.xls':
      icon = 'file-excel';
      break;
    case '.png':
    case '.jpg':
    case '.gif':
    case '.jpeg':
    case '.bitmap':
      icon = 'file-image';
      break;
    case '.txt':
    case '.doc':
    case '.docx':
    case '.md':
      icon = 'file-text';
      break;
    case '.pdf':
      icon = 'file-pdf';
      break;
    case '.zip':
      icon = 'file-zip';
      break;
    default:
      icon = 'file';
  }
  return icon;
};

const DocumentMiniList = ({ documents, primary, download, deleteDocument, handleStartRevision, create, canEdit = true, partId, type }: Props) => {
  const [currentDrawings, setCurrentDrawings] = useState<Drawing[]>(documents || []);

  // Returns the most recent drawing revisions for the current part
  const getCurrentDrawings = (drawings: Drawing[] | undefined) => {
    if (!drawings) return [];
    const draws = [...drawings];
    const sortedDrawings = draws?.sort((a, b) => (moment(a.createdAt).isAfter(moment(b.createdAt)) ? -1 : 1));
    const currentDraws: Drawing[] = [];
    sortedDrawings?.forEach((drawing) => {
      if (!currentDraws.some((draw) => draw.originalDrawing === drawing.originalDrawing)) {
        currentDraws.push(drawing);
      }
    });
    return currentDraws;
  };

  useEffect(() => {
    if (documents) setCurrentDrawings(getCurrentDrawings(documents));
  }, [documents]);

  const handleDownloadDrawing = (e: any) => {
    const keys = e.key.split(' ');
    const doc = documents?.find((d) => d.id === keys[1]);

    if (doc) {
      if (keys[0] === 'ballooned') {
        // download ballooned
        download(doc.drawingFile.privateUrl, doc.drawingFile.name, undefined, partId, doc.id);
      } else {
        // download original
        download(doc.drawingFile.publicUrl, doc.drawingFile.name);
      }
    }
  };

  return (
    <div key={`${type}-mini-list`} className="mini-list" data-cy="documentMiniList">
      <h2>
        <span>
          {type === 'Drawing' ? <BuildOutlined /> : <PaperClipOutlined />}
          {type === 'Drawing' ? i18n('entities.documents.list.miniPart') : i18n('entities.documents.list.miniAttach')}
        </span>
        <small>{i18n('common.total', type === 'Drawing' ? currentDrawings.length : documents?.length || 0)}</small>
      </h2>
      <List className="list-container">
        {
          // Drawings
          type === 'Drawing' &&
            currentDrawings &&
            currentDrawings.map((document) => {
              const icon = document.characteristics && document.characteristics.length > 0 ? 'environment' : 'file-pdf';
              const fileName = document.number || document.drawingFile?.name;
              return (
                <DocumentMiniCard
                  id={document.id}
                  key={document.id}
                  name={fileName || i18n('common.unnamed')}
                  primary={primary}
                  dataIcon={icon}
                  download={handleDownloadDrawing}
                  deleteDocument={() => {
                    deleteDocument(document, type);
                  }}
                  handleStartRevision={handleStartRevision}
                  canEdit={canEdit}
                  versions={documents?.filter((d) => d.originalDrawing === document.originalDrawing).sort((a, b) => (moment(a.createdAt).isAfter(moment(b.createdAt)) ? -1 : 1))}
                />
              );
            })
        }
        {
          // Regular documents
          type !== 'Drawing' &&
            documents &&
            documents.map((document) => {
              const icon = getIcon(document.drawingFile?.publicUrl || document.publicUrl);
              const fileName = document.publicUrl ? document.name : document.number || document.drawingFile?.name;
              return (
                <DocumentMiniCard
                  id={document.id}
                  key={document.id}
                  name={fileName || i18n('common.unnamed')}
                  primary={primary}
                  dataIcon={icon}
                  download={(e: any) => {
                    e.preventDefault();
                    download(document.drawingFile?.publicUrl || document.publicUrl, document.drawingFile?.name || document.name);
                  }}
                  deleteDocument={() => {
                    deleteDocument(document, type);
                  }}
                  handleStartRevision={handleStartRevision}
                  canEdit={canEdit}
                />
              );
            })
        }
      </List>
      {canEdit && (
        <button type="button" className="add-button additional" onClick={create} data-cy="list-new-document">
          <Icon icon="plus" block={false} />
          {i18n('entities.documents.attach')}
        </button>
      )}
    </div>
  );
};
export default DocumentMiniList;
