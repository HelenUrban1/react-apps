import React, { ReactNode, useRef } from 'react';
import { i18n } from 'i18n';
import { FileSearchOutlined, IdcardOutlined, SolutionOutlined, TeamOutlined } from '@ant-design/icons';
import { Operator } from 'types/operator';
import OperatorMiniCard from './OperatorMiniCard';
import '../MiniList.less';

interface Props {
  operators: Operator[] | undefined;
  title: string;
  icon: string;
  handleEdit: (operator: Operator) => void;
  handleDelete: (operator: Operator) => void;
  settingsContainer: any;
  permissions: {
    canEdit: boolean;
    canDelete: boolean;
  };
}

const icons: { [key: string]: ReactNode } = {
  'file-search': <FileSearchOutlined />,
  solution: <SolutionOutlined />,
  team: <TeamOutlined />,
  idcard: <IdcardOutlined />,
};

const OperatorMiniList = ({ operators, title, icon, handleEdit, handleDelete, permissions, settingsContainer }: Props) => {
  const ref = useRef(null);

  return (
    <div className="mini-list" data-cy={`${title}-userMiniList`}>
      <h2>
        <span>
          {icons[icon]}
          {title}
        </span>
        <small>{i18n('common.total', operators?.length || 0)}</small>
      </h2>
      <div ref={ref} className="list-container list-container-override">
        {operators &&
          operators.map((operator) => (
            <OperatorMiniCard
              key={operator.id}
              id={operator.id}
              operator={operator}
              handleEdit={handleEdit}
              handleDelete={handleDelete}
              canEdit={permissions.canEdit}
              canDelete={permissions.canDelete}
              container={operators.length > 4 ? ref : settingsContainer}
            />
          ))}
      </div>
    </div>
  );
};
export default OperatorMiniList;
