import React, { useEffect, useState } from 'react';
import { i18n } from 'i18n';
import { Form, Input, List, Modal, Select } from 'antd';
import { IxAvatar } from 'styleguide/Avatar/IxAvatar';
import { EditOutlined } from '@ant-design/icons';
import { Operator } from 'types/operator';

const { Option, OptGroup } = Select;

interface Props {
  id: string;
  operator: any;
  handleEdit: (operator: any) => void;
  handleDelete: (operator: any) => void;
  canEdit: boolean;
  canDelete: boolean;
  container: any;
}

const UserMiniCard = ({ id, handleDelete, handleEdit, container, operator }: Props) => {
  const [showEditModal, setShowEditModal] = useState<boolean>(false);
  const [hovering, setHovering] = useState<boolean>(false);
  const [antdIsStupidOperator, setOperator] = useState<Operator | null>(null);

  useEffect(() => {
    setOperator(operator);
  }, [operator]);

  const [form] = Form.useForm();

  const onConfirmEdit = () => {
    form
      .validateFields()
      .then((values) => {
        setOperator({ ...antdIsStupidOperator, ...values });
        form.resetFields();
        setShowEditModal(false);
        handleEdit({ ...operator, ...values });
      })
      .catch((info) => {
        console.error('Validation failed:', info);
      });
  };

  const onCancel = () => {
    setShowEditModal(false);
  };

  const onConfirmDelete = () => {
    handleDelete(operator);
  };

  return (
    <List.Item className={`mini-list-card user-card-override`} key={id} data-cy="userMiniListCard" onMouseOver={() => setHovering(true)} onMouseOut={() => setHovering(false)}>
      {antdIsStupidOperator && (
        <Modal visible={showEditModal} title={i18n('settings.operators.edit')} okText="Submit" cancelText="Cancel" onCancel={onCancel} onOk={onConfirmEdit}>
          <Form form={form} layout="vertical" name="add_operator">
            <Form.Item name="firstName" label="First Name" initialValue={antdIsStupidOperator.firstName} rules={[{ required: true, message: "Please fill in the operator's first name." }]}>
              <Input />
            </Form.Item>
            <Form.Item name="lastName" label="Last Name" initialValue={antdIsStupidOperator.lastName} rules={[{ required: true, message: "Please fill in the operator's last name." }]}>
              <Input />
            </Form.Item>
            <Form.Item name="email" label="Email" initialValue={antdIsStupidOperator.email} rules={[{ type: 'email', message: 'Please enter a valid email' }]}>
              <Input />
            </Form.Item>
            <Form.Item
              name="pin"
              label="PIN"
              initialValue={antdIsStupidOperator.pin}
              rules={[
                { required: form.getFieldValue('accessControl') === 'ITAR', message: 'A PIN is required for ITAR-enabled operators' },
                { max: 8, message: 'Please enter a code between 4 and 8 characters' },
                { min: 4, message: 'Please enter a code between 4 and 8 characters' },
              ]}
            >
              <Input />
            </Form.Item>
          </Form>
        </Modal>
      )}
      <IxAvatar type="user" data={operator} size="large" />
      <div className="card-content user-content-override" data-cy={`${operator.firstName}-${operator.lastName}`}>
        <div className="user-field" style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', width: '100%' }}>
          <div style={{ display: 'flex', alignItems: 'center', width: '20%' }}>
            <div style={{ display: 'flex', flexDirection: 'column' }}>
              <span className="name">{`${operator.firstName} ${operator.lastName}`}</span>
              {operator.email && <a href={`mailto:${operator.email}`}>{operator.email}</a>}
            </div>
            <EditOutlined
              style={{ display: hovering ? 'block' : 'none', marginLeft: '10px', marginTop: '2px' }}
              onClick={() => {
                setShowEditModal(true);
              }}
            />
          </div>
          <Select defaultValue={operator.status} onChange={(value) => handleEdit({ ...operator, status: value })}>
            <Select.Option value="Active">Active</Select.Option>
            <Select.Option value="Inactive">Inactive</Select.Option>
          </Select>
          <Select
            className="access-control-select"
            value={operator.accessControl}
            key={`${operator.id}-access-select-key`}
            onChange={(level: string) => handleEdit({ ...operator, accessControl: level })}
            defaultValue={i18n('common.none')}
            dropdownClassName={`${id}-accessSelect`}
            optionLabelProp="key"
            dropdownMatchSelectWidth={250}
            style={{
              minWidth: '150px',
            }}
          >
            <OptGroup key={i18n('entities.part.fields.accessControl')}>
              <Option key={i18n('entities.part.enumerators.accessControl.None')} value="None" data-cy="access-none">
                <span className="doc-control-value custom-select-option" data-cy={'select-option-none'}>
                  <span className="custom-select-option-label">{i18n('entities.part.enumerators.accessControl.None')}</span>
                  <span className="custom-select-option-description">{i18n('settings.seats.unrestricted')}</span>
                </span>
              </Option>
              <Option key={i18n('entities.part.enumerators.accessControl.ITAR')} value="ITAR" data-cy="access-ITAR/EAR">
                <span className="doc-control-value custom-select-option" data-cy={'select-option-ITAREAR'}>
                  <span className="custom-select-option-label">{i18n('entities.part.enumerators.accessControl.ITAR')}</span>
                  <span className="custom-select-option-description">{i18n('settings.seats.restricted')}</span>
                </span>
              </Option>
            </OptGroup>
          </Select>
        </div>
      </div>
    </List.Item>
  );
};
export default UserMiniCard;
