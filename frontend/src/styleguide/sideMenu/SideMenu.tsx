import React from 'react';
import { Tabs } from 'antd';

const { TabPane } = Tabs;

interface sideMenuProps {
  panes: [{ tab: string; key: string; content: any }];
}

export const SideMenu = ({ panes }: sideMenuProps) => {
  return (
    <Tabs tabPosition="left">
      {panes.map((pane) => {
        return (
          <TabPane tab={pane.tab} key={pane.key}>
            {pane.content}
          </TabPane>
        );
      })}
    </Tabs>
  );
};
