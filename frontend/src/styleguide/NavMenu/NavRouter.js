import React from 'react';
import PropTypes from 'prop-types';
import NavMenu from './NavMenu';
import { withRouter } from 'modules/shared/withRouter/withRouter';

class NavRouter extends React.Component {
  constructor(props) {
    super(props);
    this.state = { activeRoute: this.props.activeRoute, activeStep: this.props.activeStep };
  }

  setActiveKeys = (route, step) => {
    this.setState({ activeRoute: route, activeStep: step });
  };

  componentDidUpdate() {
    const { activeStep, activeRoute } = this.props;
    if (activeRoute !== this.state.activeRoute || activeStep !== this.state.activeStep) {
      this.setState({ activeRoute: this.props.activeRoute, activeStep });
      if (this.props.activeStep === null) {
        this.props.history.push(this.props.activeRoute);
      }
    }
  }

  render() {
    return (
      <NavMenu
        onClick={(item) => {
          if (item.item.props.route) {
            // this.props.restart(item.item.props.route);
            this.props.history.push({ pathname: item.item.props.route });
            this.props.setRoute(item.item.props.route);
            this.setActiveKeys(item.item.props.route, null);
            if (item.item.props.steps) {
              this.setActiveKeys(item.item.props.route, 0);
            } else {
              this.setActiveKeys(item.item.props.route, null);
            }
            // TODO: when switching routes if the route is for section with sub-steps, the current step should be retrieved from the project data
            // for now, just reset it to 0
          } else {
            this.props.jump(parseInt(item.key));
            this.setActiveKeys(this.state.activeRoute, parseInt(item.key));
          }
        }}
        menuItems={this.props.menuItems}
        maxStep={this.props.maxStep}
        activeRoute={this.state.activeRoute}
        currentStep={`${this.state.activeStep}`}
        currentPath={this.props.location.pathname}
      />
    );
  }
}

NavRouter.propTypes = {
  /**
   * The array of menu items to construct the navigation bar.
   */
  menuItems: PropTypes.array,
};

NavRouter.defaultProps = {
  /** Default Empty Nav Array */
  menuItems: [],
};

export default withRouter(NavRouter);
