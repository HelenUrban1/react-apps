/* eslint-disable react/jsx-wrap-multilines */
/* eslint-disable react/jsx-filename-extension */
import React from 'react';
import { Menu } from 'antd';
import NavMenuPopover from './NavMenuPopover';
import './nav.less';

const { SubMenu, ItemGroup } = Menu;

const regex = {
  partsStep: new RegExp(/parts\/./),
};

const ideagenHomeUrl = process.env.REACT_APP_IDEAGEN_HOME_URL;

const buildSubMenu = (menuItem) => {
  const { subitems } = menuItem;
  const menu = [];
  const subMenu = [];
  const subMenuId = menuItem.name.includes('Dev') ? 'User' : menuItem.name.replace(' ', '_');
  for (let j = 0; j < subitems.length; j++) {
    const itemId = subitems[j].name.replace(' ', '_');
    subMenu.push(
      <Menu.Item key={subitems[j].route} id={itemId} data-testid={itemId} route={subitems[j].route}>
        {subitems[j].icon}
        <span className="nav-text">{subitems[j].name}</span>
      </Menu.Item>,
    );
  }
  menu.push(
    <SubMenu
      key={menuItem.name}
      id={subMenuId}
      data-testid={subMenuId}
      title={
        <span>
          {menuItem.icon}
          <span className="nav-text">{menuItem.name}</span>
        </span>
      }
      style={menuItem.bottom ? { marginTop: 'auto' } : {}}
    >
      {subMenu}
    </SubMenu>,
  );
  return menu;
};

const buildStepsMenu = (menuItem, maxStep, currentPath) => {
  const flow = menuItem.steps;
  const itemGroup = [];
  const menu = [];
  for (let j = 0; j < flow.steps.length; j++) {
    const itemId = flow.steps[j].name.replace(' ', '_');
    itemGroup.push(
      <Menu.Item key={j} id={itemId} data-testid={itemId} disabled={flow.steps[j].disabled} className="ixc-step-item" hidden={maxStep < j}>
        {flow.steps[j].icon}
        <span className="nav-text">{flow.steps[j].name}</span>
      </Menu.Item>,
    );
  }
  menu.push(
    <ItemGroup className="ixc-step-group" key={flow.flow} id={flow.flow} hidden={!currentPath.match(regex.partsStep)}>
      {itemGroup}
    </ItemGroup>,
  );
  return menu;
};

const buildMenu = (menuItems, maxStep, currentPath) => {
  const menu = [];

  for (let i = 0; i < menuItems.length; i++) {
    const itemId = menuItems[i].name.replace(' ', '_');
    if (menuItems[i].popover) {
      menu.push(
        <Menu.Item className="nav-menu-item" title={null} key={menuItems[i].route} data-testid={itemId} id={itemId}>
          <NavMenuPopover icon={menuItems[i].icon} name={menuItems[i].name} />
        </Menu.Item>,
      );
    } else if (menuItems[i].subitems.length > 0) {
      menu.push(...buildSubMenu(menuItems[i]));
    } else {
      menu.push(
        <Menu.Item key={menuItems[i].route} id={itemId} data-testid={itemId} route={menuItems[i].route} className={menuItems[i].class ? `${menuItems[i].class}` : ''} steps={menuItems[i].steps} style={menuItems[i].bottom ? { marginTop: 'auto' } : {}}>
          {menuItems[i].icon}
          <span className="nav-text">{menuItems[i].name}</span>
        </Menu.Item>,
      );
    }
    if (menuItems[i].steps) {
      menu.push(...buildStepsMenu(menuItems[i], maxStep, currentPath));
    }
  }

  return menu;
};

const bindAppcues = () => {
  // TODO: When Appcues support explains how to hook into our UI, remove the false flag
  if (false && window.Appcues) {
    // event.preventDefault();
    // window.Appcues.toggleMenu();
    window.Appcues.loadLaunchpad('#Help', {
      // Optionally specify the position of the content relative to the Launchpad icon. Possible values are as followed:
      //	- center (default value, i.e. bottom-center)
      //	- left (i.e. bottom-left)
      //	- right (i.e. bottom-right)
      //	- top (i.e. top-center)
      //	- top-left
      //	- top-right
      position: 'top-right',
      // Optionally add a header or footer. This must be HTML.
      // header: '<h1>Tutorials</h1>',
      // footer: '<p>Your footer here</p>',
      // And if you'd prefer to use a custom icon rather than the default "bell" icon, you can optionally pass
      // in an icon as well. Make sure that src is set to the right resource, whether it's in your site's directory or hosted
      // icon: 'icon.png',
    });
  } else {
    const helpOption = document.getElementById('Help');
    helpOption.addEventListener('click', () => {
      window.open('https://www.inspectionxpert.com/getting-started#tab-2', '_blank');
    });
    const iHomeOption = document.getElementById('Ideagen_Home');
    iHomeOption.addEventListener('click', () => {
      console.log('Ideagen Home Url: ' + ideagenHomeUrl);
      window.open(ideagenHomeUrl, '_blank');
    });
  }
};
class NavMenu extends React.Component {
  componentDidMount() {
    bindAppcues();
  }

  componentWillUnmount() {
    bindAppcues();
  }

  render() {
    const { menuItems, maxStep, currentPath, currentStep } = this.props;
    const cPath = currentPath === undefined ? '/' : currentPath;
    const selected = [`/${cPath.split('/')[1]}`, currentStep];
    const menu = buildMenu(menuItems, maxStep, cPath);
    const { onClick } = this.props;

    return (
      <Menu
        style={{
          height: 'calc(100% - 48px)',
          display: 'flex',
          flexDirection: 'column',
        }}
        mode="inline"
        onClick={onClick}
        selectedKeys={selected}
        className="nav-menu"
      >
        {menu}
      </Menu>
    );
  }
}

NavMenu.defaultProps = {
  /** Menu should be collapsed by default */
  collapsed: true,

  /** Default Empty Nav Array */
  menuItems: [],
};

export default NavMenu;
