import React from 'react';
import NavMenu from './NavMenu';
import menuItems from '../mocks/NavItems';
import { storiesOf } from '@storybook/react';
import MDX from './NavMenu.mdx';
import { Layout } from 'antd';
import IxcTheme from '../styles/IxcTheme';

const { Sider, Content } = Layout;
const stories = storiesOf('Design System/Navigation', module);

stories
  .add(
    'Closed',
    () => {
      const story = (
        <Layout hasSider>
          <Sider
            style={{
              height: '100%',
              position: 'fixed',
              top: 0,
              left: 0,
              overflowX: 'hidden',
              backgroundColor: IxcTheme.colors.primaryColor,
              color: IxcTheme.colors.primaryColor,
            }}
            id="SideNavMenu"
            theme="dark"
            defaultCollapsed={true}
            collapsible
          >
            <div className="logo" />
            <NavMenu menuItems={menuItems} height="100%" />
          </Sider>
          <Content>
            <div style={{ height: '2000px' }}></div>
          </Content>
        </Layout>
      );

      return story;
    },
    {
      docs: { page: MDX },
    },
  )
  .add(
    'Open',
    () => {
      const story = (
        <Layout hasSider>
          <Sider
            style={{
              height: '100%',
              position: 'fixed',
              top: 0,
              left: 0,
              overflowX: 'hidden',
              backgroundColor: IxcTheme.colors.primaryColor,
              color: IxcTheme.colors.primaryColor,
            }}
            id="SideNavMenu"
            theme="dark"
            defaultCollapsed={false}
            collapsible
          >
            <div className="logo" />
            <NavMenu menuItems={menuItems} height="100%" />
          </Sider>
          <Content>
            <div style={{ height: '2000px' }}></div>
          </Content>
        </Layout>
      );

      return story;
    },
    {
      docs: { page: MDX },
    },
  );
