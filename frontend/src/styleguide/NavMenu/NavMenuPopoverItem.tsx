import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { i18n } from 'i18n';
import { Row, Col, Button, List } from 'antd';
import { useHistory } from 'react-router-dom';
import { BuildOutlined } from '@ant-design/icons';
import { NotificationUserStatus } from 'domain/setting/notification/notificationUserStatusTypes';
import { useLazyQuery, useMutation } from '@apollo/client';
import { NotificationUserStatusEdit, NotificationUserStatuses } from 'domain/setting/notification/notificationUserStatusApi';
import { Notifications, NotificationList } from 'domain/setting/notification/notificationApi';
import { doSetUnreadCount, doSetUnreadNotificationPreview } from 'modules/notification/notificationActions';
import authSelectors from 'modules/auth/authSelectors';
import { AppState } from 'modules/state';

interface Props {
  notificationItem: any;
  togglePopover: () => void;
}

const NavMenuPopover: React.FC<Props> = ({ notificationItem, togglePopover }) => {
  const [itemHover, setItemHover] = useState(false);
  const history = useHistory();
  const dispatch = useDispatch();
  const user = useSelector((state: AppState) => authSelectors.selectCurrentUser(state));
  const defaultSort = 'createdAt_DESC';

  const toggleHover = () => {
    setItemHover(!itemHover);
  };

  const [loadNotifications] = useLazyQuery<NotificationList>(Notifications.query.list, {
    variables: {
      filter: { userStatuses: { user: user?.id, archivedAt: false, readAt: false } },
      orderBy: defaultSort,
      limit: 3,
      offset: 0,
    },
    fetchPolicy: 'cache-and-network',
    notifyOnNetworkStatusChange: true, // causes onCompleted to be run during refetch
    onCompleted: (result) => {
      dispatch(doSetUnreadCount(result?.notificationList?.count || 0));
      dispatch(doSetUnreadNotificationPreview(result?.notificationList?.rows || []));
    },
  });

  const [editNotificationUserStatus] = useMutation<NotificationUserStatusEdit>(NotificationUserStatuses.mutate.edit);

  const markAsRead = async (notification: any) => {
    const userStatus = { ...notification?.userStatuses?.[0], notification: notification?.id } as NotificationUserStatus;
    if (userStatus) {
      const newUserStatus = { notification: userStatus.notification, archivedAt: userStatus.archivedAt, readAt: userStatus?.readAt || new Date(), user: userStatus.user };
      await editNotificationUserStatus({
        variables: {
          id: userStatus?.id,
          data: newUserStatus,
        },
      });
      await loadNotifications();
    }
  };

  return (
    <List.Item onMouseEnter={toggleHover} onMouseLeave={toggleHover}>
      <List.Item.Meta
        avatar={
          <span className="icon-size">
            <BuildOutlined />
          </span>
        }
        title={notificationItem.title}
        description={
          <Row className="list-row-padding">
            {notificationItem?.action?.split(',').map((item: string, index: number) => {
              if (item === i18n('notifications.reviewFeatures')) {
                return (
                  <Col span={12} key={`col_${notificationItem.id}_${index}`}>
                    <Button
                      key={`${notificationItem.id}_${index}`}
                      className="btn-primary"
                      size="small"
                      disabled={!itemHover}
                      onClick={async () => {
                        await markAsRead(notificationItem);
                        togglePopover();
                        history.push(`/parts/${notificationItem.relationId}`);
                      }}
                    >
                      {item}
                    </Button>
                  </Col>
                );
              }
              if (item === i18n('notifications.reviewLater')) {
                return (
                  <Col span={12} key={`col_${notificationItem.id}_${index}`}>
                    <Button
                      key={`${notificationItem.id}_${index}`}
                      disabled={!itemHover}
                      className="btn-secondary"
                      size="small"
                      onClick={() => {
                        markAsRead(notificationItem);
                      }}
                    >
                      {item}
                    </Button>
                  </Col>
                );
              }
            })}
          </Row>
        }
      />
    </List.Item>
  );
};

export default NavMenuPopover;
