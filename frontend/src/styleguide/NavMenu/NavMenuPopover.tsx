import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import { AppState } from 'modules/state';
import { i18n } from 'i18n';
import { Row, Col, Popover, Badge, List } from 'antd';
import { useHistory } from 'react-router-dom';
import NavMenuPopoverItem from './NavMenuPopoverItem';

interface Props {
  icon: any;
  name: any;
}

const NavMenuPopover: React.FC<Props> = ({ icon, name }) => {
  const [showPopover, setShowPopover] = useState(false);
  const notification = useSelector((state: AppState) => state.notification);
  const history = useHistory();

  const togglePopover = () => {
    setShowPopover(!showPopover);
  };

  return (
    <Popover
      className="anticon fake-icon"
      placement="rightBottom"
      trigger="click"
      visible={showPopover}
      onVisibleChange={togglePopover}
      overlayStyle={{
        minWidth: '400px',
      }}
      content={
        <List
          itemLayout="vertical"
          dataSource={notification.unreadNotificationPreview}
          header={
            <Row>
              <Col span={16}>{i18n('notifications.unreadNotifications', notification.unreadCount)}</Col>
              <Col span={8}>
                <a
                  style={{ float: 'right' }}
                  onClick={() => {
                    history.push(`/settings/notifications`);
                  }}
                >
                  {i18n('notifications.viewAll')}
                </a>
              </Col>
            </Row>
          }
          renderItem={(notificationItem) => {
            if (notificationItem) {
              return <NavMenuPopoverItem togglePopover={togglePopover} notificationItem={notificationItem} />;
            }
          }}
        />
      }
    >
      <Badge className="notification-qty-badge" size="small" count={notification.unreadCount} />
      {icon}
      <span className="nav-text">{name}</span>
    </Popover>
  );
};

export default NavMenuPopover;
