import { Core, WebViewerInstance } from '@pdftron/webviewer';
import { PdfOverlayViewState, PdfViewState } from 'modules/state';
import { Drawing } from 'graphql/drawing';
import config from 'config';
import { downloadFile } from 'utils/files';
import { CanvasLayerManager, Layer } from './canvasLayerManager';
import log from '../../modules/shared/logger';
import { getCaptureCoordinatesInViewer } from '../../domain/part/edit/characteristics/utils/coordTransforms';

export const triggerMouseEvent = (node: Node, eventType: string) => {
  const clickEvent = document.createEvent('MouseEvents');
  clickEvent.initEvent(eventType, true, true);
  node.dispatchEvent(clickEvent);
};

const deleteObjectProperties = (object: any) => {
  Object.keys(object).forEach((key) => {
    // eslint-disable-next-line no-param-reassign
    delete object[key];
  });
};

export const handlePageNumberUpdated = (
  e: any, //
  canvasesCache: any,
  fabricsCache: any,
  canvasLayerManager: CanvasLayerManager,
  viewer: Core.DocumentViewer,
) => {
  // Clear fabric objects on page change to force new object for a new page
  deleteObjectProperties(canvasesCache);
  deleteObjectProperties(fabricsCache);

  canvasLayerManager.publish('pageNumberUpdated', {
    index: viewer.getCurrentPage() - 1,
    event: e,
  });
};

// Calculates the current view for the layerManager
export const updateLayerManagerView = (viewer: Core.DocumentViewer, layerManager: CanvasLayerManager, pages?: any[]) => {
  const pageNumber: number = viewer.getCurrentPage();
  let atts: any;
  if (pages) {
    atts = { ...pages[pageNumber - 1] };
  } else {
    atts = {
      height: viewer.getPageHeight(1),
      width: viewer.getPageWidth(1),
    };
  }

  const view = {
    ...atts,
    index: pageNumber - 1,
    zoom: viewer.getZoomLevel(),
    rotation: viewer.getCompleteRotation(pageNumber) * 90, // getCompleteRotation
  };

  // Update the layer manager
  layerManager.updateView(view);

  return view;
};

export const handleDocumentLoaded = (viewer: Core.DocumentViewer, pdfView: PdfViewState | PdfOverlayViewState, canvasesCache: any, fabricsCache: any) => {
  const newPdfView: PdfViewState | PdfOverlayViewState = { ...pdfView, pages: [] };

  // Clear the fabric and canvas caches
  deleteObjectProperties(canvasesCache);
  deleteObjectProperties(fabricsCache);

  // If document metadata hasn't been captured, do so now
  if (pdfView.pages?.length === 0) {
    // Collect metadata about the pages in the PDF
    const pageCount: number = viewer.getPageCount();
    for (let pi = 0; pi < pageCount; pi++) {
      newPdfView.pages?.push({
        index: pi,
        height: viewer.getPageHeight(pi + 1),
        width: viewer.getPageWidth(pi + 1),
        rotation: viewer.getCompleteRotation(pi + 1) * 90, // returns 1 i.e. PageRotation.e_90
      });
    }

    // Update the layer
    const pageIndex: number = viewer.getCurrentPage() - 1;
    // eslint-disable-next-line no-param-reassign
    newPdfView.index = pageIndex;
    newPdfView.zoom = viewer.getZoomLevel();
    newPdfView.rotation = viewer.getCompleteRotation(pageIndex + 1) * 90;

    // log.debug('PdfViewer: documentLoaded.updateLayerManagerView', pages);
    updateLayerManagerView(viewer, pdfView.layerManager, newPdfView.pages || []);
  }

  pdfView.layerManager?.publish('documentLoaded', { newPdfView });

  return newPdfView;
};

// Finds the Web Viewer's pageContainer
export const getPageContainer = (viewer: Core.DocumentViewer, instance: WebViewerInstance): HTMLElement | null => {
  const pageContainerId: string = `pageContainer${viewer.getCurrentPage()}`;
  const element: HTMLElement | null = instance.UI.iframeWindow.document.getElementById(pageContainerId);
  if (!element) {
    log.error(`Could not mount canvas layers, because ${pageContainerId} was not found`);
  }
  return element;
};

// Finds the Web Viewer's pageContainer
export const setLayerZIndex = (layer: Layer, instance: WebViewerInstance, layerManager: CanvasLayerManager) => {
  // Get a reference to the layer's element
  const layerCanvasEl = instance.UI.iframeWindow.document.getElementById(layer.id);
  const canvasParent = layerCanvasEl?.parentElement;
  if (!layerCanvasEl || !canvasParent) {
    log.warn(`Could not reorder canvas layer, because element ${layer.id} was not found`);
  } else {
    // Calculate and set the depth of the element
    const zIndex: number = layerManager.zDepthForIndex(layer.index);
    canvasParent.setAttribute('style', `z-index: ${zIndex} ; position: absolute; top: 0; left: 0; height: inherit; width: inherit;`);
  }
};

export const handlePageComplete = (e: any, viewer: Core.DocumentViewer, instance: WebViewerInstance, layerManager: CanvasLayerManager, canvasesCache: any, fabricsCache: any, fabric: any, isOverlay = false) => {
  // log.debug('PdfViewer: pageComplete FIRED');
  // Sets the fabric document to the iframe document so that Fabric attaches
  // event listeners to the iFrame instead of the parent doc. Without it,
  // mouse movements only propagate when the mouse is outside of the iFrame
  // eslint-disable-next-line no-param-reassign
  (fabric as any).document = instance.UI.iframeWindow.document;

  let newView: any = {};

  // Find the Web Viewer's pageContainer so that we can mount additional canvases
  const pageContainer = getPageContainer(viewer, instance);
  if (pageContainer) {
    // Update the layerManager's current view
    // // log.debug('PdfViewer.pageComplete.updateLayerManager');
    newView = updateLayerManagerView(viewer, layerManager);

    // Create and mount a canvas for each layer
    layerManager.layerOrder.forEach((layerName: string) => {
      // Get the layer metadata
      const layer: Layer = layerManager.layers[layerName];

      if (instance.UI.iframeWindow.document.getElementById(layer.id) === null) {
        let newCanvasEl;
        if (canvasesCache[layerName]) {
          // Reference the cached canvas element if it exists
          newCanvasEl = canvasesCache[layerName];
        } else {
          // Create an HTML Canvas element to insert into the DOM
          newCanvasEl = instance.UI.iframeWindow.document.createElement('canvas');
          newCanvasEl.setAttribute('id', layer.id);
          newCanvasEl.setAttribute('class', layer.id);
        }

        // Insert the canvas element into the Web Viewer's pageContainer
        pageContainer.insertAdjacentElement('afterbegin', newCanvasEl);
        setLayerZIndex(layer, instance, layerManager);

        const fabricCanvas =
          fabricsCache[layerName] ||
          new fabric.Canvas(newCanvasEl, {
            // BUG: These throw an error: originX and originY do not exist in type 'ICanvasOptions'
            // originX: 'left',
            // originY: 'top',
            selection: layerName === 'markers',
          });
        if (!fabricsCache[layerName]) {
          // Keeping reference to fabric canvas so it doesn't have to be recreated on each pageComplete call
          // eslint-disable-next-line no-param-reassign
          fabricsCache[layerName] = fabricCanvas;
          // Fabric creates a second canvas and wraps the 2 in a div, we keep a reference to the div to use on next pageComplete call
          // eslint-disable-next-line no-param-reassign
          canvasesCache[layerName] = newCanvasEl.parentElement;
          // Attach a reference to Fabric.canvas to the canvas element so Cypress tests can find it
          newCanvasEl.fabric = fabricCanvas;

          // Styles the canvas we just created
          newCanvasEl.parentElement!.setAttribute('style', 'position: absolute; top: 0; left: 0; width: 100%; height: 100%;');
          layer.canvas = fabricCanvas;
        }
        // Set the height and width of the canvas to match the container's height and width
        if (pageContainer && !isOverlay) {
          fabricCanvas.setHeight(pageContainer.getBoundingClientRect().height);
          fabricCanvas.setWidth(pageContainer.getBoundingClientRect().width);
          // Fire the updateLayerCanvas canvas event
          layerManager.publish('layerCanvasMounted', { layer });
        }
      }
    });
  }
  layerManager.publish('pageComplete', e);

  return newView;
};

const getCoordinatesToScrollTo = (viewer: Core.DocumentViewer, x: number, y: number, pageNumber: number) => {
  // Go to the correct page for the current characteristic
  // setCurrentPage takes 1 based page number, drawingSheetIndex is 1 based
  if (viewer.getCurrentPage() !== pageNumber) {
    viewer.setCurrentPage(pageNumber, true);
  }

  // Get the zoom, rotation, width and height of the current page
  // getPageZoom takes 1-based page index
  const zoom = viewer.getPageZoom(pageNumber);
  // getCompleteRotation takes 1-based page number, result * 90 is degrees of rotation
  const rotation = viewer.getCompleteRotation(pageNumber) * 90;
  // getPageWidth and getPageHeight take 1-based page index
  const pageWidth = viewer.getPageWidth(pageNumber);
  const pageHeight = viewer.getPageHeight(pageNumber);

  return getCaptureCoordinatesInViewer(x, y, rotation, pageWidth, pageHeight, zoom);
};

/**
 * Scrolls the viewer to the location of a characteristic.
 * @param viewer Pdf Document Viewer
 * @param x Characteristic boxLocationX
 * @param y Characteristic boxLocationY
 * @param pageNumber 1-based page number that characteristic is on
 */
export const handleScrollTo = (viewer: Core.DocumentViewer, x: number, y: number, pageNumber: number) => {
  if (!viewer) {
    return;
  }

  const { scrollX, scrollY } = getCoordinatesToScrollTo(viewer, x, y, pageNumber);

  // scroll to the capture box coords
  const scrollViewer = viewer.getScrollViewElement();
  scrollViewer.scroll({ left: scrollX, top: scrollY, behavior: 'smooth' });
};

export const getDocumentFromFile = async (PDFNet: typeof Core.PDFNet, file: File) => {
  const buffer = await file.arrayBuffer();
  return PDFNet.PDFDoc.createFromBuffer(buffer);
};

export const getDocumentFromUrl = async (PDFNet: typeof Core.PDFNet, filePath: string, jwt: string) => {
  return PDFNet.PDFDoc.createFromURL(`${config.backendUrl}/download?privateUrl=${filePath}`, {
    withCredentials: true,
    customHeaders: {
      authorization: jwt ? `Bearer ${jwt}` : '',
    },
  });
};

export const getPageArray = async (document: Core.PDFNet.PDFDoc) => {
  const pages: Core.PDFNet.Page[] = [];
  const itr = await document.getPageIterator(1);

  // eslint-disable-next-line no-await-in-loop
  for (itr; await itr.hasNext(); await itr.next()) {
    // eslint-disable-next-line no-await-in-loop
    const page = await itr.current();
    pages.push(page);
  }

  return pages;
};

export const loadPdfCompareOverlay = async (instance: WebViewerInstance, drawing: Drawing | null, jwt: string, file: File | null, layerManager: any) => {
  const { PDFNet } = instance.Core;

  async function main() {
    let tempDoc1;
    let tempDoc2;

    try {
      // Create docs to compare from
      const [doc1, doc2] = file
        ? await Promise.all([getDocumentFromFile(PDFNet, file), getDocumentFromFile(PDFNet, file)])
        : await Promise.all([getDocumentFromUrl(PDFNet, drawing?.drawingFile.privateUrl || '', jwt), getDocumentFromUrl(PDFNet, drawing?.drawingFile.privateUrl || '', jwt)]);

      tempDoc1 = doc1;
      tempDoc2 = doc2;

      // Get pages
      const [pages1, pages2] = await Promise.all([getPageArray(doc1), getPageArray(doc2)]);

      // Get first page of document and blank page
      const p1 = pages1[0];
      const p2 = pages2[0];

      // Create a new empty Doc to hold our visual diff
      const newDoc = await PDFNet.PDFDoc.create();
      await newDoc.lock();

      // Set color
      const diffOptions = new PDFNet.PDFDoc.DiffOptions();
      diffOptions.setBlendMode(12);
      if (file) {
        diffOptions.setColorA({ A: 1, R: 197, G: 2, B: 2 });
        diffOptions.setColorB({ A: 1, R: 197, G: 2, B: 2 });
      } else {
        diffOptions.setColorA({ A: 1, R: 1, G: 197, B: 197 });
        diffOptions.setColorB({ A: 1, R: 1, G: 197, B: 197 });
      }

      // Add the visual diff to the empty doc
      if (p1 && p2) await newDoc.appendVisualDiff(p1, p2, diffOptions);
      await newDoc.unlock();

      await doc1.destroy();
      await doc2.destroy();

      // Reload with the new doc
      instance.UI.loadDocument(newDoc);
      if (layerManager) layerManager.publish('compareComplete', newDoc);
      return true;
    } catch (err) {
      console.error(err);
      await tempDoc1?.destroy();
      await tempDoc2?.destroy();
      return err;
    }
  }

  return PDFNet.runWithoutCleanup(main, config.pdfTronKey);
};

export const loadPdfCompare = async (instance: WebViewerInstance, partDrawing: Drawing, compareDrawing: Drawing, jwt: string, colorA?: number[], colorB?: number[]) => {
  const { PDFNet } = instance.Core;

  async function main() {
    try {
      const [partDoc, compareDoc] = await Promise.all([getDocumentFromUrl(PDFNet, partDrawing.drawingFile.privateUrl || '', jwt), getDocumentFromUrl(PDFNet, compareDrawing.drawingFile.privateUrl || '', jwt)]);

      const [partDocPages, compareDocPages] = await Promise.all([getPageArray(partDoc), getPageArray(compareDoc)]);

      const newDoc = await PDFNet.PDFDoc.create();
      newDoc.lock();

      const biggestLength = Math.max(partDocPages.length, compareDocPages.length);
      const chain = Promise.resolve();

      const diffOptions = new PDFNet.PDFDoc.DiffOptions();

      if (colorA && colorB) {
        const [rA, gA, bA] = colorA;
        diffOptions.setColorA({ A: 1, R: rA, G: gA, B: bA });

        const [rB, gB, bB] = colorB;
        diffOptions.setColorB({ A: 1, R: rB, G: gB, B: bB });
      }

      for (let i = 0; i < biggestLength; i++) {
        chain.then(async () => {
          let page1 = partDocPages[i];
          let page2 = compareDocPages[i];

          if (!page1) page1 = new PDFNet.Page();
          if (!page2) page2 = new PDFNet.Page();

          // Default diffOptions sets first param to blue and second to red
          return newDoc.appendVisualDiff(page1, page2, diffOptions);
        });
      }

      await chain;
      await newDoc.unlock();

      await partDoc.destroy();
      await compareDoc.destroy();

      return instance.UI.loadDocument(newDoc);
    } catch (err) {
      console.error(err);
      return err;
    }
  }

  return PDFNet.runWithoutCleanup(main, config.pdfTronKey);
};

export const callDocLoad = async (instance: WebViewerInstance, filePath: string, jwt: string | null, retry: boolean, setRetry: any) => {
  if (instance && retry && jwt) {
    log.warn('Failed to load PDF. Initiating retry.');
    try {
      // Loading the document here to pass the auth token in the header
      const drawingFile = await downloadFile(`${config.backendUrl}/download?privateUrl=${filePath}`);
      instance.UI.loadDocument(drawingFile, {
        customHeaders: {
          authorization: jwt ? `Bearer ${jwt}` : '',
        },
      });
      await instance.UI.enableFeatures([instance.UI.Feature.MouseWheelZoom, instance.UI.Feature.PageNavigation]);
    } catch (e) {
      log.error('PdfViewer.runWithCleanup error', e);
    }
    setRetry(false);
  }
};

export const buildRevisedPdf = async (revisionFile: File, currentInstance: WebViewerInstance, revisedInstance: WebViewerInstance) => {
  const { PDFNet } = revisedInstance.Core;
  const { documentViewer: revisedDocViewer } = revisedInstance.Core;

  async function main() {
    try {
      const newDoc = await getDocumentFromFile(revisedInstance.Core.PDFNet, revisionFile);

      const currentZoom = currentInstance.UI.getZoomLevel();
      const revisedZoom = revisedInstance.UI.getZoomLevel();

      const newDocPages = await getPageArray(newDoc);

      // Set the rotation for the new file
      const rot = revisedDocViewer.getCompleteRotation(1);
      const rotChain = Promise.resolve();
      newDocPages.forEach((p) => {
        rotChain.then(() => {
          return p.setRotation(rot);
        });
      });
      await rotChain;

      // Set the dimensions for the new file
      const dimChain = Promise.resolve();
      newDocPages.forEach((p) => {
        dimChain.then(async () => {
          // TODO: I think this needs a more complex calculation for an accurate scale
          const scale = revisedZoom / currentZoom;
          if (Math.abs(1 - scale) < 0.1) {
            // new scale is a rounding error, PDFs are likely the same size already
            return Promise.resolve();
          }
          return p.scale(scale);
        });
      });
      await dimChain;

      const arr = await newDoc.saveMemoryBuffer(PDFNet.SDFDoc.SaveOptions.e_linearized);
      const file = new File([arr], revisionFile.name, { type: 'application/pdf' });

      return file;
    } catch (err) {
      console.error(err);
      return err;
    }
  }

  return PDFNet.runWithCleanup(main, config.pdfTronKey);
};

export const getFileDimensions = async (instance: WebViewerInstance, file: File) => {
  const { PDFNet } = instance.Core;

  async function main() {
    const dims = { w: 0, h: 0 };

    try {
      const newDoc = await getDocumentFromFile(instance.Core.PDFNet, file);
      const newPage = await newDoc.getPage(1);

      dims.w = await newPage.getPageWidth();
      dims.h = await newPage.getPageHeight();

      return dims;
    } catch (err) {
      console.error(err);
      return err;
    }
  }

  return PDFNet.runWithCleanup(main, config.pdfTronKey);
};

export const getDrawingDimensions = async (instance: WebViewerInstance) => {
  const currentDoc = await instance.Core.documentViewer.getDocument().getPDFDoc();
  const currentPage = await currentDoc.getPage(1);

  const dims = { w: 0, h: 0 };

  dims.w = await currentPage.getPageWidth();
  dims.h = await currentPage.getPageHeight();

  return dims;
};

export const hasMatchingDimensionsViewer = async (revisionFile: File, instance: WebViewerInstance) => {
  try {
    const currentDimensions = await getDrawingDimensions(instance);
    const newDimensions = await getFileDimensions(instance, revisionFile);

    if (currentDimensions.w !== newDimensions.w || currentDimensions.h !== newDimensions.h) return false;
  } catch (err) {
    console.error(err);
    return false;
  }
  return true;
};

export const closeCurrentDocument = async (instance: WebViewerInstance | null) => {
  try {
    const doc = instance?.Core.documentViewer.getDocument();
    if (doc) {
      await instance?.UI.closeDocument();
    }
  } catch (err) {
    log.error(err);
  }
};
