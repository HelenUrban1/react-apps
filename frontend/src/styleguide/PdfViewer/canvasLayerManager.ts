import { fabric } from 'fabric';
import log from '../../modules/shared/logger';

export interface Layer {
  name: string;
  id: string;
  index: number;
  canvas: fabric.Canvas | null;
}

// TODO: Move this into string utils class
/*
 * toKebabCase('userId') => "user-id"
 * toKebabCase('waitAMoment') => "wait-a-moment"
 * toKebabCase('TurboPascal') => "turbo-pascal"
 */
function toKebabCase(str: string): string {
  return str
    .replace(/([a-zA-Z])(?=[A-Z])/g, '$1-')
    .replace(/ +/g, '-')
    .toLowerCase();
}

export class CanvasLayerManager {
  // Pub/Sub registry for event propagation between layers
  // subscribers: { [eventName: string]: Function[]} = {};
  subscribers: {
    [eventName: string]: {
      [subscriberId: string]: Function;
    };
  } = {};

  // Array of layer IDs to control sorting
  layerOrder: string[] = [];

  // Dictionary for fast lookup of canvas metadata by name
  layers: { [layerName: string]: Layer } = {};

  // Absolute minimum Z depth that layer's will stack above
  zIndexBase: number = 50;

  // Stores the currently loaded view
  view: { [property: string]: any } = {};

  // Given an array
  constructor(
    params: {
      layers?: string[];
      zIndexBase?: number;
    } = {},
  ) {
    if (params.layers) this.addLayers(params.layers);
    if (params.zIndexBase) this.zIndexBase = params.zIndexBase;
  }

  // Register the list of provided layers to be mounted
  addLayers(layers: string[]): void {
    layers.forEach((layerName) => {
      if (this.layerExists(layerName)) {
        log.warn(`Ignoring request to add ${layerName}, because it already exists.`);
      } else {
        this.addLayer(layerName);
      }
    });
  }

  removeAllLayers(): void {
    this.layerOrder.forEach((layerName) => {
      delete this.layers[layerName];
    });
    this.layerOrder.length = 0;
  }

  // Returns bool reflecting whether layer is already loaded
  layerExists(layer: string): boolean {
    return this.layerOrder.indexOf(layer) >= 0;
  }

  // Register a layer to be mounted
  addLayer(name: string): void {
    this.layerOrder.push(name);
    const index: number = this.layerOrder.length - 1;
    this.layers[name] = {
      name,
      id: CanvasLayerManager.layerId(name),
      index,
      canvas: null,
    };

    // Setup a callback to attach the canvas when it gets mounted by the base
    this.subscribe('CanvasLayerManager', 'layerCanvasMounted', (data: any) => {
      if (data.layer.name === name) {
        this.layers[name].canvas = data.layer.canvas;
      }
    });
  }

  getCurrentCanvasFor(layer: string): fabric.Canvas | null {
    if (!this.layers[layer]) {
      log.warn(`Unable to get canvas for ${layer}, because layer has not been loaded.`);
      return null;
    }
    if (!this.layers[layer].canvas) {
      log.warn(`Unable to get canvas for ${layer}, because canvas was null.`);
      return null;
    }
    return this.layers[layer].canvas!;
  }

  // Helper function to standardize layer IDs
  static layerId(name: string): string {
    return `${toKebabCase(name)}-canvas`;
  }

  // Helper function to return the index position of the given
  indexOfLayer(name: string): number {
    return this.layerOrder.indexOf(name);
  }

  // Helper function to standardize setting of Z depth
  zDepthForIndex(index: number): number {
    return this.zIndexBase + 50 * (this.layerOrder.length - index - 1);
  }

  // Repositions the target layer to the top
  moveLayerToFront(name: string): void {
    // Find the layer we want to place on top
    const topLayer = this.layerOrder.find((layer) => layer === name);
    if (!topLayer) {
      log.warn(`CanvasLayerManager: Could not move layer ${name} to top, because it could not be found.`);
    } else {
      // Remove all other layers
      this.layerOrder = this.layerOrder.filter((layer) => layer !== topLayer);
      // Push the layer we want on top to the top
      this.layerOrder.unshift(topLayer);
      // Update layer metadata
      this.layerOrder.forEach((layerName: string, i: number) => {
        this.layers[layerName].index = i;
      });
      // Fire the layerOrderModified event to let the other layers adapt
      this.publish('layerOrderModified', this.layerOrder);
    }
  }

  // Update the view properties that should be shared across all layers
  updateView(view: { [property: string]: any }): void {
    if (this.view.index === view.index && this.view.rotation !== undefined && this.view.rotation !== view.rotation) {
      let delta: number = view.rotation - this.view.rotation;
      if (delta === -270) {
        delta = 90;
      } else if (delta === 270) {
        delta = -90;
      }
      this.publish('viewRotated', { delta });
    }
    this.view = view;
  }

  // Method for layers to publish events
  publish(event: string, data: any) {
    if (this.subscribers[event]) {
      let subscriberCallback: Function;
      const subs = Object.entries(this.subscribers[event]);
      subs.forEach((sub) => {
        subscriberCallback = sub[1];
        subscriberCallback(data);
      });
      /*
      // TODO: This was causing typescript error because subscriberId was not used. Remove when functionality is confirmed.
      for (const [subscriberId, subscriberCallback] of subs) {
        subscriberCallback=subs[1]; 
        subscriberCallback(data);
      }
      */
    }
  }

  // Method that allows layers to subscribe to events from other layers
  subscribe(subscriberId: string, event: string, callback: Function) {
    if (!this.subscribers[event]) {
      this.subscribers[event] = {}; // NOTE: This removes order of subscribers
    }
    this.subscribers[event][subscriberId] = callback;
  }
}
