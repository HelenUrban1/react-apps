import React, { useEffect, useRef } from 'react';
import WebViewer, { WebViewerInstance } from '@pdftron/webviewer';
import config from 'config';
import log from 'modules/shared/logger';
import { useDispatch, useSelector } from 'react-redux';
import { AppState } from 'modules/state';
import { resetPdfView, setPdfViewFilePath, setPdfViewIndex, setPdfViewRotation, setPdfViewState, setPdfViewStateThunk, setPdfViewTronInstance, setPdfViewZoom } from 'modules/pdfView/pdfViewActions';
import { resetPdfOverlayView, setPdfOverlayViewFile, setPdfOverlayViewIndex, setPdfOverlayViewRotation, setPdfOverlayViewState, setPdfOverlayViewTronInstance, setPdfOverlayViewZoom } from 'modules/pdfOverlayView/pdfViewActions';
import { downloadFile } from 'utils/files';
import { triggerMouseEvent, handleDocumentLoaded, setLayerZIndex, handlePageNumberUpdated, handleScrollTo, loadPdfCompareOverlay, callDocLoad, closeCurrentDocument } from './PdfViewerUtil';
import './viewer.less';

interface Props {
  filePath: string;
  tronInstance: WebViewerInstance | null;
  revisionFile?: File | null;
}

export const PdfViewer = ({ filePath, tronInstance, revisionFile }: Props) => {
  const { pdfView, pdfOverlayView } = useSelector((state: AppState) => state);
  const { partDrawing } = useSelector((state: AppState) => state).partEditor;
  const { jwt } = useSelector((state: AppState) => state.auth);
  const { layerManager } = revisionFile ? pdfOverlayView : pdfView;
  let retry = true;

  const dispatch = useDispatch();

  useEffect(() => {
    // reset PdfView state on unmount
    return () => {
      if (revisionFile) {
        dispatch(resetPdfOverlayView());
      } else {
        dispatch(resetPdfView());
      }
    };
  }, []);

  useEffect(() => {
    async function loadAsync() {
      try {
        // Loading the document here to pass the auth token in the header
        const drawingFile = revisionFile || (await downloadFile(`${config.backendUrl}/download?privateUrl=${filePath}`));
        await closeCurrentDocument(tronInstance);
        tronInstance?.UI.loadDocument(drawingFile, {
          customHeaders: {
            authorization: jwt ? `Bearer ${jwt}` : '',
          },
        });
      } catch (e) {
        log.error('PdfViewer.runWithCleanup error', e);
      }
    }
    if (filePath) {
      dispatch(setPdfViewFilePath(filePath));
      if (tronInstance) loadAsync();
    }
  }, [filePath]);

  useEffect(() => {
    if (revisionFile) {
      setPdfOverlayViewFile(revisionFile);
    }
  }, [revisionFile]);

  const canvasesCache: any = {};
  const fabricsCache: any = {};

  // Grab the HTML doc's root element
  const pdfViewer = useRef<HTMLElement>(null);

  const setRetry = (re: boolean) => {
    retry = re;
  };

  useEffect(() => {
    // Refresh fail callback
    if (tronInstance) {
      const instance = tronInstance;

      instance.UI.iframeWindow.removeEventListener('loaderror', () => callDocLoad(instance, filePath, jwt, retry, setRetry));
      instance.UI.iframeWindow.addEventListener('loaderror', () => callDocLoad(instance, filePath, jwt, retry, setRetry));
    }
  }, [jwt, tronInstance]);

  useEffect(() => {
    // When the PDF component is loaded
    if (pdfViewer.current) {
      WebViewer(
        {
          licenseKey: config.pdfTronKey,
          path: '/pdftronlib', // Path to the static files
          enableAnnotations: false,
          fullAPI: true,
          css: '/css/PdfViewer.css', // Override default CSS styles here
          disabledElements: [
            'selectToolButton',
            'thumbnailsPanelButton',
            'notesPanelButton',
            'outlinesPanelButton',
            'freeHandToolGroupButton',
            'textToolGroupButton',
            'shapeToolGroupButton',
            'signatureToolButton',
            'freeTextToolButton',
            'miscToolGroupButton',
            'searchButton',
            'menuButton',
            'pageNavOverlay',
            'textPopup',
            'header',
            'thumbnailControl',
            'documentControl',
            'panToolButton',
          ],
        },
        pdfViewer.current,
      ).then(async (instance) => {
        dispatch(revisionFile ? setPdfOverlayViewTronInstance(instance) : dispatch(setPdfViewTronInstance(instance)));

        try {
          // Can't do ballooning until this is finished running
          await instance.Core.PDFNet.runWithCleanup(() => {}, config.pdfTronKey);
          // Loading the document here to pass the auth token in the header

          if (revisionFile && jwt) {
            await loadPdfCompareOverlay(instance, null, jwt, revisionFile, pdfView.layerManager);
          } else if (pdfOverlayView.revisionFile && partDrawing && jwt) {
            await loadPdfCompareOverlay(instance, partDrawing, jwt, null, layerManager);
          } else {
            const drawingFile = await downloadFile(`${config.backendUrl}/download?privateUrl=${filePath}`);
            await closeCurrentDocument(instance);
            instance.UI.loadDocument(drawingFile, {
              customHeaders: {
                authorization: jwt ? `Bearer ${jwt}` : '',
              },
            });
          }
        } catch (e) {
          log.error('PdfViewer.runWithCleanup error', e);
        }

        // Disable text selection
        instance.UI.disableFeatures(['TextSelection']);

        instance.UI.setFitMode(instance.UI.FitMode.FitPage);

        // LayoutMode.Single prevents more than 1 page from being visible at a time
        instance.UI.setLayoutMode(instance.UI.LayoutMode.Single);

        // Locking the max zoom to 400% to keep the fabric canvases from getting too large
        // Could be removed if we find a better fix for excessive memory usage of large fabric canvases
        instance.UI.setMinZoomLevel(0.25);
        instance.UI.setMaxZoomLevel(4.0);
        // Disabling marquee zoom tool because it allows zooming past the above limits and doesn't play nice with ballooning
        instance.UI.disableTools(['MarqueeZoomTool']);

        // Setup event handlers
        const viewer = instance.Core.documentViewer;
        const frame = instance.UI.iframeWindow.window.document.body;

        if (!revisionFile) {
          frame.addEventListener('click', () => {
            triggerMouseEvent(document.body, 'mousedown');
          });
        }

        // Initialize the capturedItems array in index.tsx with the # of pages in the document
        viewer.addEventListener('documentLoaded', async () => {
          const newPdfView = handleDocumentLoaded(viewer, revisionFile ? pdfOverlayView : pdfView, canvasesCache, fabricsCache);
          instance.UI.setFitMode(instance.UI.FitMode.FitPage);
          instance.UI.setLayoutMode(instance.UI.LayoutMode.Single);
          if (revisionFile) {
            dispatch(setPdfOverlayViewState({ zoom: newPdfView.zoom, rotation: newPdfView.rotation, pages: newPdfView.pages, index: newPdfView.index, pdfLoaded: true }));
          } else {
            dispatch(setPdfViewState({ zoom: newPdfView.zoom, rotation: newPdfView.rotation, pages: newPdfView.pages, index: newPdfView.index, pdfLoaded: true }));
          }
        });

        try {
          // Handle page complete
          viewer.addEventListener('pageComplete', (e) => {
            if (!revisionFile) dispatch(setPdfViewStateThunk({ e, canvasesCache, fabricsCache }));
          });
          // Handle rotation
          viewer.addEventListener('rotationUpdated', (e) => {
            const degrees: number = viewer.getCompleteRotation(viewer.getCurrentPage()) * 90;
            layerManager.publish('rotationUpdated', { degrees, event: e });
            if (revisionFile) {
              dispatch(setPdfOverlayViewRotation(degrees));
            } else {
              dispatch(setPdfViewRotation(degrees));
            }
          });
          // Handle zoom
          viewer.addEventListener('zoomUpdated', (e) => {
            layerManager.publish('zoomUpdated', { zoom: viewer.getZoomLevel(), event: e });
            if (revisionFile) {
              dispatch(setPdfOverlayViewZoom(viewer.getZoomLevel()));
            } else {
              dispatch(setPdfViewZoom(viewer.getZoomLevel()));
            }
          });
          // Handle page change
          // This event fires at the start of the page change
          viewer.addEventListener('pageNumberUpdated', (e) => {
            handlePageNumberUpdated(e, canvasesCache, fabricsCache, layerManager, viewer);
            if (revisionFile) {
              dispatch(setPdfOverlayViewIndex(viewer.getCurrentPage() - 1));
            } else {
              dispatch(setPdfViewIndex(viewer.getCurrentPage() - 1));
            }
          });
        } catch (err) {
          console.error('pdf tron error');
          console.error(err);
        }

        if (revisionFile) return;

        try {
          // Reset the Z depth of all layers
          if (layerManager)
            layerManager.subscribe('PDFViewer', 'layerOrderModified', (layerNames: string[]) => {
              layerNames.forEach((layerName: string) => {
                // log.debug(`set layer depth: ${layerName}`);
                setLayerZIndex(layerManager.layers[layerName], instance, layerManager);
              });
            });

          viewer.addEventListener('scrollTo', (e: any) => {
            handleScrollTo(viewer, e.x, e.y, e.pageNumber);
          });

          viewer.addEventListener('keyUp', (e) => {
            // bubble up keypress event so it can be used for deleting objects on delete/backspace press
            layerManager.publish('canvasKeypress', { event: e });
          });
        } catch (err) {
          console.error('pdf tron error');
          console.error(err);
        }
      });
    }
  }, [pdfViewer, layerManager]);

  if (!filePath && !revisionFile) {
    return <p>Loading File Viewer</p>;
  }
  return <main id="pdf-viewer" ref={pdfViewer} style={{ width: '100%', height: '100%' }} />;
};

export default PdfViewer;
