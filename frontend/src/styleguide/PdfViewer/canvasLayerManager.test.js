import { CanvasLayerManager, Layer } from './canvasLayerManager';

describe('CanvasLayerManager', () => {
  describe('#constructor', () => {
    it('should create an instance without layers if names are not provided', () => {
      const layerManager = new CanvasLayerManager();
      expect(layerManager.layerOrder.length).toBe(0);
    });

    it('should attach all provided layers', () => {
      const layerNames = ['A', 'B', 'C'];
      const layerManager = new CanvasLayerManager({ layers: layerNames });
      expect(layerManager.layerOrder.length).toBe(3);
      layerNames.forEach((layerName, i) => {
        expect(layerManager.layers[layerName].name).toBe(layerName);
        expect(layerManager.layers[layerName].index).toBe(i);
      });
    });

    it('should override default base zIndex if a base is provided', () => {
      const layerManager = new CanvasLayerManager({ layers: ['A', 'B', 'C'], zIndexBase: 100 });
      expect(layerManager.zDepthForIndex(0)).toBe(200);
      expect(layerManager.zDepthForIndex(1)).toBe(150);
      expect(layerManager.zDepthForIndex(2)).toBe(100);
    });
  });

  describe('#addLayers', () => {
    it('should append all layers provided to the existing layers', () => {
      const layerNames = ['A', 'B', 'C'];
      const layerManager = new CanvasLayerManager();
      layerManager.addLayers(layerNames);
      expect(layerManager.layerOrder.length).toBe(3);
      layerNames.forEach((layerName, i) => {
        expect(layerManager.layers[layerName].name).toBe(layerName);
        expect(layerManager.layers[layerName].index).toBe(i);
      });
    });
  });

  describe('#addLayer', () => {
    it('should append a new layer to the existing layers', () => {
      const layerNames = ['A', 'B'];
      const layerManager = new CanvasLayerManager({ layers: layerNames });
      expect(layerManager.layerOrder.length).toBe(2);
      layerNames.forEach((layerName, i) => {
        expect(layerManager.layers[layerName].name).toBe(layerName);
        expect(layerManager.layers[layerName].index).toBe(i);
      });
      layerManager.addLayer('C');
      expect(layerManager.layers.C.name).toBe('C');
      expect(layerManager.layers.C.index).toBe(2);
    });
  });

  describe('#indexOfLayer', () => {
    it('should return the zero based index of the layer with the name given', () => {
      const layerManager = new CanvasLayerManager({ layers: ['A', 'B', 'C'] });
      expect(layerManager.indexOfLayer('A')).toBe(0);
      expect(layerManager.indexOfLayer('B')).toBe(1);
      expect(layerManager.indexOfLayer('C')).toBe(2);
    });

    it('should return -1 if the layer name is not found', () => {
      const layerManager = new CanvasLayerManager({ layers: ['A', 'B', 'C'] });
      expect(layerManager.indexOfLayer('D')).toBe(-1);
    });
  });

  describe('#layerId', () => {
    it('should return a kebab case standardized ID for the name given', () => {
      expect(CanvasLayerManager.layerId('simple')).toBe('simple-canvas');
      expect(CanvasLayerManager.layerId('This is a fancy Name')).toBe('this-is-a-fancy-name-canvas');
      expect(CanvasLayerManager.layerId('camelCaseName')).toBe('camel-case-name-canvas');
      expect(CanvasLayerManager.layerId('TitleCaseName')).toBe('title-case-name-canvas');
    });
  });

  describe('#zDepthForIndex', () => {
    it('should return a zIndex value relative to the base', () => {
      const layerManager = new CanvasLayerManager({ layers: ['A', 'B', 'C'] });
      expect(layerManager.zDepthForIndex(0)).toBe(150);
      expect(layerManager.zDepthForIndex(1)).toBe(100);
      expect(layerManager.zDepthForIndex(2)).toBe(50);
    });

    it('should have a default zIndex of 50 which can be overriden', () => {
      const layerManager = new CanvasLayerManager({ layers: ['A', 'B', 'C'] });
      layerManager.zIndexBase = 100;
      expect(layerManager.zDepthForIndex(0)).toBe(200);
      expect(layerManager.zDepthForIndex(1)).toBe(150);
      expect(layerManager.zDepthForIndex(2)).toBe(100);
    });
  });

  describe('#moveLayerToFront', () => {
    it('should move the designate layer above all others without changing the rest of the order', () => {
      const layerNames = ['A', 'B', 'C'];
      const layerManager = new CanvasLayerManager({ layers: layerNames });
      expect(layerManager.layerOrder.length).toBe(3);
      layerNames.forEach((layerName, i) => {
        expect(layerManager.layers[layerName].name).toBe(layerName);
        expect(layerManager.layers[layerName].index).toBe(i);
      });

      layerManager.moveLayerToFront('B');
      expect(layerManager.layerOrder.length).toBe(3);

      let layer;
      layer = layerManager.layers.B;
      expect(layer.name).toBe('B');
      expect(layer.index).toBe(0);

      layer = layerManager.layers.A;
      expect(layer.name).toBe('A');
      expect(layer.index).toBe(1);

      layer = layerManager.layers.C;
      expect(layer.name).toBe('C');
      expect(layer.index).toBe(2);
    });
  });

  describe('#subscribe / #publish', () => {
    it('should fire events with data', () => {
      const watcherA = {
        handleSomeEvent(data) {
          return data;
        },
      };
      const watcherB = {
        handleSomeEvent(data) {
          return data;
        },
      };
      const spyA = jest.spyOn(watcherA, 'handleSomeEvent');
      const spyB = jest.spyOn(watcherB, 'handleSomeEvent');

      const layerManager = new CanvasLayerManager({ layers: ['A', 'B', 'C'] });
      layerManager.subscribe('FooCaller', 'someEvent', watcherA.handleSomeEvent);
      layerManager.subscribe('FooCaller', 'someOtherEvent', watcherB.handleSomeEvent);
      layerManager.publish('someEvent', { some: 'value' });
      expect(spyA).toHaveBeenCalledTimes(1);
      expect(spyB).toHaveBeenCalledTimes(0);
    });
  });
});
