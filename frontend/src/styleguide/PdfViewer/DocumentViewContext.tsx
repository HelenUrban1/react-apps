import React, { useState } from 'react';
import { CanvasLayerManager } from './canvasLayerManager';

export interface DocumentPageProps {
  index: number;
  width: number;
  height: number;
  rotation: number;
}

export interface DocumentView {
  update: Function;
  layerManager: CanvasLayerManager;
  fileUrl: string;
  pages: DocumentPageProps[];
  current: {
    index: number;
    width: number;
    height: number;
    rotation: number;
    zoom: number;
  };
}

const defaultDocumentViewContext: DocumentView = {
  update: (documentView: DocumentView) => documentView,
  layerManager: new CanvasLayerManager({ layers: ['grid', 'markers'] }),
  fileUrl: '',
  pages: [],
  current: {
    index: 0,
    rotation: 0,
    width: 0,
    height: 0,
    zoom: 0,
  },
};

// Initial value of DocumentViewContext
export const DocumentViewContext = React.createContext<DocumentView>(defaultDocumentViewContext);

export const DocumentViewContextProvider: React.FC = (props) => {
  const [documentViewState, setDocumentView] = useState<DocumentView>(defaultDocumentViewContext);

  defaultDocumentViewContext.update = (documentView: DocumentView): void => {
    setDocumentView(documentView);
  };

  const { children } = props;
  return <DocumentViewContext.Provider value={documentViewState}>{children}</DocumentViewContext.Provider>;
};
