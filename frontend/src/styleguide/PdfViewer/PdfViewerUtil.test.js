import { fabric } from 'fabric';
import { handlePageNumberUpdated, handleDocumentLoaded, handlePageComplete } from './PdfViewerUtil';

jest.mock('fabric');

describe('PdfViewerUtil', () => {
  describe('handlePageNumberUpdated', () => {
    it('Removes custom properties on canvases cache and fabrics cache', () => {
      const e = {};
      const canvasesCache = {
        canvas1: {},
        canvas2: {},
      };
      const fabricsCache = {
        fabric1: {},
        fabric2: {},
      };
      const layerManager = {
        publish: () => {},
      };
      const viewer = {
        getCurrentPage: () => 1,
      };
      handlePageNumberUpdated(e, canvasesCache, fabricsCache, layerManager, viewer);
      expect(canvasesCache.canvas1).toBe(undefined);
      expect(canvasesCache.canvas1).toBe(undefined);
      expect(fabricsCache.fabric1).toBe(undefined);
      expect(fabricsCache.fabric2).toBe(undefined);
    });
  });

  describe('handleDocumentLoaded', () => {
    it('Removes custom properties on canvases cache and fabrics cache', () => {
      const pdfView = {
        pages: [{}, {}],
      };
      const canvasesCache = {
        canvas1: {},
        canvas2: {},
      };
      const fabricsCache = {
        fabric1: {},
        fabric2: {},
      };
      const viewer = {
        getCurrentPage: () => 1,
      };
      handleDocumentLoaded(viewer, pdfView, canvasesCache, fabricsCache);
      expect(canvasesCache.canvas1).toBe(undefined);
      expect(canvasesCache.canvas1).toBe(undefined);
      expect(fabricsCache.fabric1).toBe(undefined);
      expect(fabricsCache.fabric2).toBe(undefined);
    });
  });

  describe('handlePageComplete', () => {
    it('Adds cached items as custom properties on canvases cache and fabrics cache', () => {
      // Mock the inputs
      const e = {};
      const instance = {
        UI: {
          iframeWindow: {
            document: {
              getElementById: (element) => {
                if (element === 'pageContainer0') {
                  const result = document.createElement('div');
                  result.insertAdjacentElement = () => {};
                  return result;
                }
                return null;
              },
              createElement: (element) => {
                const parent = document.createElement('div');
                const result = document.createElement(element);
                parent.appendChild(result);
                return result;
              },
            },
          },
        },
      };
      const canvasesCache = {};
      const fabricsCache = {};
      const layerManager = {
        publish: () => {},
        updateView: () => {},
        layers: {
          grid: {
            id: 'grid-canvas',
            index: 1,
            name: 'grid',
          },
          markers: {
            id: 'markers-canvas',
            index: 0,
            name: 'markers',
          },
        },
        layerOrder: ['markers', 'grid'],
      };
      const viewer = {
        getCurrentPage: () => 1,
        getPageHeight: () => 100,
        getPageWidth: () => 100,
        getZoom: () => 1,
        getCompleteRotation: () => 0,
      };

      handlePageComplete(e, viewer, instance, layerManager, canvasesCache, fabricsCache, fabric);
      expect(canvasesCache.markers).not.toBeNull();
      expect(canvasesCache.grid).not.toBeNull();
      expect(fabricsCache.markers).not.toBeNull();
      expect(fabricsCache.grid).not.toBeNull();
    });
  });
});
