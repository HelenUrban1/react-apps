import { WebViewerInstance } from '@pdftron/webviewer';
import { AppState } from 'modules/state';
import React, { useRef, useState, useEffect } from 'react';
import { useSelector } from 'react-redux';
import { Position, ResizableDelta, Rnd } from 'react-rnd';
import { PdfViewer } from './PdfViewer';
import { getDrawingDimensions } from './PdfViewerUtil';
import './overlayViewer.less';

export const OverlayViewer = ({ compareLoaded }: { compareLoaded: number }) => {
  const { tronInstance: overlayTronInstance, revisionFile } = useSelector((state: AppState) => state.pdfOverlayView);
  const { tronInstance } = useSelector((state: AppState) => state.pdfView);

  const rndRef = useRef<Rnd>(null);
  const lockDims = useRef(false);

  const [dims, setDims] = useState({ height: 700, width: 700, x: 0, y: 0 });

  useEffect(() => {
    return function cleanup() {
      lockDims.current = false;
    };
  }, []);

  useEffect(() => {
    async function resize() {
      if (rndRef.current && !lockDims.current) {
        lockDims.current = true;
        const instance = tronInstance as WebViewerInstance;
        instance.UI.setFitMode(instance.UI.FitMode.FitPage);
        const currentZoom = instance.Core.documentViewer.getZoomLevel();
        instance.Core.documentViewer.zoomTo(currentZoom / 1.5);

        const pageContainer = instance.UI.iframeWindow.document.getElementById('pageSection1')?.getBoundingClientRect();
        const { w, h } = pageContainer ? { w: pageContainer.width, h: pageContainer.height } : await getDrawingDimensions(tronInstance);
        let { x, y } = dims;
        const canvasContainer = document.getElementById('partCanvas');
        const rndBox = rndRef.current.getSelfElement()?.getBoundingClientRect();

        if (canvasContainer && rndBox) {
          // try to align the draggable box in the center of the pdfViewer
          const pdfViewerBox = canvasContainer.getBoundingClientRect();
          const pdfViewerCenter = { x: pdfViewerBox.width / 2, y: pdfViewerBox.height / 2 };
          x = pdfViewerCenter.x - w / 2;
          y = pdfViewerCenter.y - h / 2;
        }

        setDims({ width: w, height: h, x, y });
      }
    }

    if (rndRef.current && compareLoaded >= 2) resize();
  }, [rndRef.current, compareLoaded]);

  const handleResizeStop = (e: MouseEvent | TouchEvent, direction: any, ref: HTMLElement, delta: ResizableDelta, position: Position) => {
    setDims({ width: parseInt(ref.style.width, 10), height: parseInt(ref.style.height, 10), ...position });
    if (!lockDims.current) lockDims.current = true;
    if (overlayTronInstance) {
      const instance = overlayTronInstance as WebViewerInstance;
      instance.UI.setFitMode(instance.UI.FitMode.FitPage);
    }
  };

  const handleClasses = {
    topLeft: 'resize-handle-wrapper',
    bottomLeft: 'resize-handle-wrapper',
    topRight: 'resize-handle-wrapper',
    bottomRight: 'resize-handle-wrapper',
  };

  return (
    <div className="rnd-container">
      <Rnd
        ref={rndRef}
        size={{ width: dims.width, height: dims.height }}
        position={{ x: dims.x, y: dims.y }}
        onDragStop={(e, d) => {
          setDims({ ...dims, x: d.x, y: d.y });
          if (!lockDims.current) lockDims.current = true;
        }}
        onResizeStop={handleResizeStop}
        resizeHandleClasses={handleClasses}
        enableResizing={{ top: false, right: false, bottom: false, left: false, topRight: true, bottomRight: true, bottomLeft: true, topLeft: true }}
        bounds="parent"
      >
        <div className="pdftron-overlay-drag-handle" />
        <div className="pdftron-overlay">
          <PdfViewer filePath="" tronInstance={overlayTronInstance} revisionFile={revisionFile} />
        </div>
      </Rnd>
    </div>
  );
};
