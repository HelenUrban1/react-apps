import React, { Fragment } from 'react';
import styled, { css } from 'styled-components';

import Icon from './Icon';
import icons from '../shared/icons';

const Meta = styled.div`
  color: #666;
  font-size: 12px;
`;

const Item = styled.li`
  display: inline-flex;
  flex-direction: row;
  align-items: center;
  flex: 0 1 20%;
  min-width: 120px;

  padding: 0px 7.5px 20px;

  svg {
    margin-right: 10px;
    width: 24px;
    height: 24px;
  }

  ${(props) =>
    props.minimal &&
    css`
      flex: none;
      min-width: auto;
      padding: 0;
      background: #fff;
      border: 1px solid #666;

      svg {
        display: block;
        margin-right: 0;
        width: 48px;
        height: 48px;
      }
    `};
`;

const List = styled.ul`
  display: flex;
  flex-flow: row wrap;
  list-style: none;
`;

export default {
  title: 'Design System/Icon',
  component: Icon,
};

export const labels = () => (
  <Fragment>
    There are {Object.keys(icons).length} icons
    <List>
      {Object.keys(icons).map((key) => (
        <Item key={key}>
          <Icon icon={key} aria-hidden style={{ color: '#000000' }} />
          <Meta>{key}</Meta>
        </Item>
      ))}
    </List>
  </Fragment>
);

export const noLabels = () => (
  <List>
    {Object.keys(icons).map((key) => (
      <Item minimal key={key}>
        <Icon icon={key} aria-label={key} />
      </Item>
    ))}
  </List>
);

noLabels.story = {
  name: 'no labels',
};

export const inline = () => (
  <Fragment>
    this is an inline <Icon icon="facehappy" aria-label="Happy face" /> icon (default)
  </Fragment>
);

export const block = () => (
  <Fragment>
    this is a block <Icon icon="facehappy" aria-label="Happy face" block /> icon
  </Fragment>
);

export const logo = () => (
  <Fragment>
    <svg version="1.1" x="0px" y="0px" width="311.7px" height="311.5px" viewBox="0 0 311.7 311.5">
      <path
        d="M13.9959 27.9995C12.9122 27.9995 11.835 27.8755 10.7897 27.6302L10.6088 27.5877C10.3092 27.5174 10.1233 27.2175 10.1936 26.9178L10.2786 26.556C10.3489 26.2564 10.6488 26.0705 10.9485 26.1408L11.1294 26.1833C12.1456 26.422 13.1949 26.5352 14.2514 26.5109C21.1501 26.3727 26.6496 20.6475 26.511 13.7485C26.4442 10.4066 25.0796 7.29114 22.6691 4.97564C20.2582 2.6598 17.0763 1.41501 13.7486 1.48895C10.4066 1.55564 7.29077 2.92019 4.97527 5.33073C2.65977 7.74127 1.42144 10.9094 1.48892 14.2512C1.50888 15.2584 1.6486 16.2569 1.90434 17.2198L1.95198 17.3992C2.03091 17.6967 1.85376 18.0017 1.55629 18.0807L1.197 18.176C0.89953 18.255 0.594456 18.0779 0.515409 17.7805L0.467663 17.6006C0.182096 16.5234 0.0257031 15.4063 0.00290767 14.2808C-0.0722834 10.5423 1.31302 6.99832 3.90354 4.30142C6.49406 1.60452 9.97995 0.0779032 13.7188 0.00282554C17.4506 -0.0712314 21.0015 1.3126 23.6984 3.9038C26.3952 6.49432 27.9219 9.97987 27.9971 13.7187C28.152 21.4368 21.9991 27.842 14.2812 27.9969C14.1861 27.9989 14.091 27.9995 13.9959 27.9995Z M13.1882 9.95499L11.0238 6.74344C10.9668 6.65872 10.8713 6.60791 10.7691 6.60791H7.57318C7.32946 6.60791 7.18282 6.87817 7.31562 7.08253L11.2102 12.7783L13.1882 9.95499Z M15.8319 13.8197L20.5125 7.08241C20.6453 6.87805 20.4986 6.60779 20.2549 6.60779H17.3831C17.0778 6.60779 16.7921 6.75783 16.6187 7.00903L15.4641 8.68195L15.4696 8.68569L5.29131 23.1685C5.25287 23.2202 5.17552 23.2208 5.13639 23.1696L4.06693 21.4348C4.01045 21.3466 3.91292 21.2932 3.80824 21.2932L1.0137 21.3055C0.767712 21.3055 0.621526 21.5802 0.758866 21.7842L3.17349 26.1371C3.23087 26.2196 3.32512 26.2689 3.42571 26.2689L6.69658 26.2658C6.79615 26.2658 6.88949 26.2176 6.9471 26.1364L13.8232 16.661L17.4574 21.9577C17.5147 22.0411 17.6095 22.091 17.7107 22.091H20.8613C21.1048 22.091 21.2514 21.8213 21.1192 21.617L15.8319 13.8197Z"
        fill="#000000"
      />
    </svg>
    <svg width="271" height="271" viewBox="0 0 271 271" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path
        d="M248.015 230.044H196.957V218.739C211.926 209.517 224.691 197.047 234.312 182.153C246.638 163.072 253.154 140.933 253.154 118.13C253.154 52.993 200.16 0 135.022 0C69.884 0 16.8911 52.993 16.8911 118.131C16.8911 140.934 23.4071 163.072 35.7341 182.154C45.3551 197.048 58.121 209.517 73.0891 218.739V230.044H22.0291C10.9831 230.044 2.02905 238.998 2.02905 250.044C2.02905 261.09 10.9831 270.044 22.0291 270.044H93.0891C104.135 270.044 113.089 261.09 113.089 250.044V206.87C113.089 199.138 108.633 192.1 101.645 188.793C74.458 175.925 56.8911 148.189 56.8911 118.131C56.8911 75.05 91.94 40 135.022 40C178.104 40 213.154 75.05 213.154 118.131C213.154 148.188 195.587 175.925 168.4 188.793C161.413 192.101 156.957 199.139 156.957 206.87V250.044C156.957 261.09 165.911 270.044 176.957 270.044H248.015C259.061 270.044 268.015 261.09 268.015 250.044C268.015 238.998 259.061 230.044 248.015 230.044Z"
        fill="black"
      />
    </svg>
  </Fragment>
);
