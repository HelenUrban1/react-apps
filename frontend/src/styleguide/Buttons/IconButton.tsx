import React, { ReactNode, useState } from 'react';
import Icon, { ToolOutlined } from '@ant-design/icons';
import { Button, Tooltip } from 'antd';
import PropTypes from 'prop-types';
import Badge from '../Badge/Badge';
import IxcTheme from '../styles/IxcTheme';

interface IconButtonProps {
  id: string;
  icon?: ReactNode;
  selected?: boolean;
  onClick: (e: any) => void;
  handleDisabledClick?: (e: any) => void;
  size?: 'large' | 'small' | 'middle';
  tipText?: string;
  disabled?: boolean;
  tipPlacement?: 'top' | 'left' | 'right' | 'bottom' | 'topLeft' | 'topRight' | 'bottomLeft' | 'bottomRight' | 'leftTop' | 'leftBottom' | 'rightTop' | 'rightBottom' | undefined;
  custom?: any;
  count?: number;
}

const IconButton = ({ selected, size, onClick, handleDisabledClick, disabled, tipText, tipPlacement, icon, id, count, custom }: IconButtonProps) => {
  const [isHover, setIsHover] = useState(false);
  const click = !disabled ? onClick : handleDisabledClick;

  const handleHover = () => {
    if (!selected) setIsHover(true);
  };

  const handleStopHover = () => {
    if (!selected) setIsHover(false);
  };

  return (
    <Tooltip placement={tipPlacement} title={tipText}>
      {custom ? (
        <Button
          id={id}
          type="link"
          size={size}
          onClick={click}
          style={{
            color: isHover ? IxcTheme.buttons.iconBtnHoverColor : IxcTheme.buttons.iconBtnColor,
          }}
          onMouseOver={handleHover}
          onFocus={handleHover}
          onMouseOut={handleStopHover}
          onBlur={handleStopHover}
        >
          <Icon component={custom} />
          {count && count > 0 && <Badge type="count" count={count} />}
        </Button>
      ) : (
        <Button
          id={id}
          icon={icon}
          type="link"
          size={size}
          onClick={click}
          style={{
            color: isHover ? IxcTheme.buttons.iconBtnHoverColor : IxcTheme.buttons.iconBtnColor,
          }}
          onMouseOver={handleHover}
          onFocus={handleHover}
          onMouseOut={handleStopHover}
          onBlur={handleStopHover}
        >
          {count && count > 0 && <Badge type="count" count={count} />}
        </Button>
      )}
    </Tooltip>
  );
};

IconButton.propTypes = {
  /**
   * Action to perform on Click
   */
  onClick: PropTypes.func,

  /**
   * Action to perform if the button is disabled on click
   */
  handleDisabledClick: PropTypes.func,

  /**
   * Icon of the Button
   */
  icon: PropTypes.element,

  /**
   * Button Size: Large, Small
   */
  size: PropTypes.string,

  /**
   * Direction of the Tooltip (left, top, right, bottom)
   */
  tipPlacement: PropTypes.string,

  /**
   * Text of the Tooltip
   */
  tipText: PropTypes.string,

  /**
   * Whether the button is selected
   */
  selected: PropTypes.bool,
};

IconButton.defaultProps = {
  /**
   * Click default
   */
  onClick: () => {},

  /**
   * Disabled Click Default
   */
  handleDisabledClick: () => {},

  /**
   * Default placeholder Icon
   */
  icon: <ToolOutlined />,

  /**
   * Default size is Large
   */
  size: 'large',

  tipText: undefined,

  tipPlacement: undefined,

  selected: undefined,

  disabled: false,

  custom: false,

  count: undefined,
};

export default IconButton;
