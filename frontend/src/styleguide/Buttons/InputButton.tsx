import React, { ReactNode } from 'react';
import { Button, Tooltip } from 'antd';

interface InputButtonProps {
  field: string;
  tooltip: string;
  onClick: () => void;
  icon: ReactNode;
  btnClass?: string;
  btnLabel?: string;
  btnColor?: string;
}

export const InputButton = ({ field, onClick, tooltip, icon, btnClass, btnLabel, btnColor = '#F6FAF8' }: InputButtonProps) => {
  return (
    <Tooltip title={tooltip} placement="top">
      <Button icon={icon} color={btnColor} className={btnClass || 'insert-modal-opener'} onClick={onClick} />
    </Tooltip>
  );
};
