import React, { ReactNode } from 'react';
import PropTypes from 'prop-types';
import { Button, Tooltip } from 'antd';
// TODO: convert this to functional component
interface btnProps {
  text?: string;
  size: 'large' | 'small' | 'middle' | undefined;
  onClick: (e: any) => void;
  onMouseEnter?: (e: any) => void;
  onMouseLeave?: (e: any) => void;
  className: string;
  handleDisabledClick?: (e: any) => void;
  leftIcon?: ReactNode;
  rightIcon?: ReactNode;
  tipText?: string;
  disabled?: boolean;
  tipPlacement?: 'top' | 'left' | 'right' | 'bottom' | 'topLeft' | 'topRight' | 'bottomLeft' | 'bottomRight' | 'leftTop' | 'leftBottom' | 'rightTop' | 'rightBottom' | undefined;
  dataCy?: string;
  style?: any;
}

const IxButton = ({ text, size, onClick, onMouseEnter, onMouseLeave, className, handleDisabledClick, leftIcon, rightIcon, disabled, tipText, tipPlacement, dataCy, style }: btnProps) => {
  const id = dataCy || text;

  if (tipText) {
    return (
      <Tooltip placement={tipPlacement || 'top'} title={tipText}>
        <Button
          size={size}
          // TODO: This will break all tests that use button ID CSS selectors when langauge is something besides english
          id={id ? `${id.replace(' ', '')}-btn` : 'ix-btn'}
          onClick={disabled ? handleDisabledClick : onClick}
          onMouseEnter={onMouseEnter}
          onMouseLeave={onMouseLeave}
          className={className}
          data-cy={dataCy}
          style={style}
          disabled={disabled}
        >
          {leftIcon && leftIcon}
          {text && text}
          {rightIcon && rightIcon}
        </Button>
      </Tooltip>
    );
  }

  return (
    <Button
      // TODO: This will break all tests that use button ID CSS selectors when language is something besides english
      id={id ? `${id.replace(' ', '')}-btn` : 'ix-btn'}
      size={size}
      onClick={disabled ? handleDisabledClick : onClick}
      onMouseEnter={onMouseEnter}
      onMouseLeave={onMouseLeave}
      className={className}
      data-cy={dataCy}
      style={style}
      disabled={disabled}
    >
      {leftIcon && leftIcon}
      {text && text}
      {rightIcon && rightIcon}
    </Button>
  );
};

IxButton.propTypes = {
  /**
   * Action to perform on Click
   */
  onClick: PropTypes.func,

  /**
   * Button Size: Large, Medium, Small
   */
  size: PropTypes.string,

  /**
   * Label if its a Text Button, don't use if only an Icon Button
   */
  text: PropTypes.string,

  /**
   * Direction of the Tooltip (left, top, right, bottom)
   */
  tipPlacement: PropTypes.string,

  /**
   * Text of the Tooltip
   */
  tipText: PropTypes.string,
};

IxButton.defaultProps = {
  /**
   * Click default
   */
  onClick: () => {
    alert('Button Clicked');
  },

  /**
   * Disabled Click Default
   */
  handleDisabledClick: () => {
    alert('Disabled Button Clicked');
  },

  /**
   * Default placeholder Icon
   */
  leftIcon: undefined,
  rightIcon: undefined,

  /**
   * Default size is Large
   */
  size: 'large',

  dataCy: undefined,

  disabled: false,

  style: undefined,

  text: undefined,

  tipPlacement: undefined,

  tipText: undefined,
};

export default IxButton;
