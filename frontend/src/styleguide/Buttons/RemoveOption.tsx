import React from 'react';
import { CloseCircleTwoTone } from '@ant-design/icons';
import './buttons.less';

interface Props {
  remove: (e: any) => void;
}

const RemoveOptionButton = ({ remove }: Props) => {
  return (
    <button type="button" data-cy="remove-option" className="icon-button remove" onClick={remove}>
      <CloseCircleTwoTone />
    </button>
  );
};

export default RemoveOptionButton;
