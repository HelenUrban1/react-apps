import React, { Fragment } from 'react';
import { storiesOf } from '@storybook/react';
import IxButton from './IxButton';
import IconButton from './IconButton';
import { boolean, select, text, withKnobs } from '@storybook/addon-knobs';
import IxcTheme from '../styles/IxcTheme';
import CustomSvg from '../shared/svgs';
import MDX from './IxButton.mdx';

const stories = storiesOf('Design System/Buttons', module).addDecorator(withKnobs);

let visible = false;

stories
  .add(
    'Action Buttons',
    () => {
      const story = (
        <Fragment>
          <div style={{ display: 'flex', flexDirection: 'row' }}>
            <div style={{ marginTop: '10px', marginLeft: '10px' }}>
              <IxButton />
            </div>
            <div style={{ marginTop: '10px', marginLeft: '10px' }}>
              <IxButton disabled={true} size="large" />
            </div>
            <div style={{ marginTop: '10px', marginLeft: '10px' }}>
              <IxButton selected={true} size="small" />
            </div>
          </div>
          <div>
            <p>With Tooltip (selectable, prints to console on click)</p>
            <div style={{ marginTop: '10px', marginLeft: '10px' }}>
              <IxButton
                tipPlacement="bottom"
                tipText="Tooltip"
                selectable
                onClick={() => {
                  console.log('Testing Click');
                }}
              />
            </div>
            <p>Without Tooltip (not selectable, prints to console on click)</p>
            <div style={{ marginTop: '10px', marginLeft: '10px' }}>
              <IxButton
                color="red"
                onClick={() => {
                  console.log('Not Selectable');
                }}
                disabledClick={() => {
                  console.log('Disabled Click');
                }}
              />
            </div>
            <p>Ghost Button</p>
            <div style={{ marginTop: '10px', marginLeft: '10px' }}>
              <IxButton ghost={true} />
            </div>
          </div>
        </Fragment>
      );

      return story;
    },
    {
      docs: { page: MDX },
    },
  )
  .add(
    'Text Button',
    () => {
      const story = (
        <div>
          <div style={{ display: 'flex', flexDirection: 'row' }}>
            <div style={{ marginTop: '10px', marginLeft: '10px' }}>
              <IxButton text="button" />
            </div>
            <div style={{ marginTop: '10px', marginLeft: '10px' }}>
              <IxButton text="button" disabled={true} size="large" />
            </div>
            <div style={{ marginTop: '10px', marginLeft: '10px' }}>
              <IxButton text="lots of text small button" selected={true} size="small" />
            </div>
          </div>
          <div>
            <p>With Tooltip (selectable, prints to console on click)</p>
            <div style={{ marginTop: '10px', marginLeft: '10px' }}>
              <IxButton
                text="button"
                tipPlacement="bottom"
                tipText="Tooltip"
                selectable
                onClick={() => {
                  console.log('Testing Click');
                }}
              />
            </div>
            <p>Without Tooltip (not selectable, prints to console on click)</p>
            <div style={{ marginTop: '10px', marginLeft: '10px' }}>
              <IxButton
                text="button"
                onClick={() => {
                  console.log('Not Selectable');
                }}
                disabledClick={() => {
                  console.log('Disabled Click');
                }}
              />
            </div>
            <p>Ghost Button</p>
            <div style={{ marginTop: '10px', marginLeft: '10px' }}>
              <IxButton text="button" ghost={true} />
            </div>
          </div>
        </div>
      );

      return story;
    },
    {
      docs: { page: MDX },
    },
  )
  .add(
    'Icon Button',
    () => {
      const story = (
        <div>
          <div style={{ display: 'flex', flexDirection: 'row' }}>
            <div style={{ marginTop: '10px' }}>
              <IconButton count={1} custom={true} CustomSvg={CustomSvg.Group} tipText="Group Items" tipPlacement="bottom" />
            </div>
            <div style={{ marginTop: '10px' }}>
              <IconButton icon="download" />
            </div>
            <div style={{ marginTop: '10px' }}>
              <IconButton tipPlacement="right" tipText="delete" icon="delete" />
            </div>
          </div>
          <div onMouseOver={() => (visible = true)} onMouseLeave={() => (visible = false)} style={{ backgroundColor: 'green', height: '10px', width: '10px' }}>
            {visible && (
              <div style={{ display: 'flex', flexDirection: 'row' }}>
                <div style={{ marginTop: '10px' }}>
                  <IconButton count={1} icon="thunderbolt" />
                </div>
                <div style={{ marginTop: '10px' }}>
                  <IconButton icon="download" />
                </div>
                <div style={{ marginTop: '10px' }}>
                  <IconButton tipPlacement="right" tipText="delete" icon="delete" />
                </div>
              </div>
            )}
          </div>
          <div onMouseOver={() => (visible = true)} onMouseLeave={() => (visible = false)} style={{ backgroundColor: 'green', height: '10px', width: '10px' }}>
            {visible && (
              <div style={{ display: 'flex', flexDirection: 'row' }}>
                <div style={{ marginTop: '10px' }}>
                  <IconButton count={1} custom={true} CustomSvg={CustomSvg.Group} />
                </div>
                <div style={{ marginTop: '10px' }}>
                  <IconButton icon="download" />
                </div>
                <div style={{ marginTop: '10px' }}>
                  <IconButton tipPlacement="right" tipText="delete" icon="delete" />
                </div>
              </div>
            )}
          </div>
        </div>
      );

      return story;
    },
    {
      docs: { page: MDX },
    },
  )
  .add(
    'Button Knobs',
    () => {
      const story = (
        <IxButton
          selectable={boolean('Selectable')}
          disabled={boolean('disabled')}
          selected={boolean('selected')}
          size={select('Size', ['small', 'large'])}
          ghost={boolean('ghost')}
          text={text('Label', 'Hello Storybook')}
          icon={text('Icon', 'tool')}
          color={text('Color', IxcTheme.colors.primaryColor)}
        />
      );

      return story;
    },
    {
      docs: { page: MDX },
    },
  );
