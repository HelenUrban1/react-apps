import React, { useState } from 'react';
import { i18n } from 'i18n';
import { Button, Tooltip } from 'antd';
import { Symbols } from 'styleguide/shared/svg';

interface SymbolButtonProps {
  field: string;
  showModal: (field: string, cursor: number) => void;
  btnClass?: string;
  btnLabel?: string;
  btnColor?: string;
}

export const SymbolButton = ({ field, showModal, btnClass, btnLabel, btnColor = '#F6FAF8' }: SymbolButtonProps) => {
  const [cursorPos, setCursorPos] = useState<number>(0);

  const handleMouseEnter = () => {
    const inp = ['comment', 'correction', 'note'].includes(field) ? (document.activeElement as HTMLTextAreaElement) : (document.activeElement as HTMLInputElement);
    if (inp && inp.selectionStart) {
      setCursorPos(inp.selectionStart);
    }
  };

  const handleClick = () => {
    showModal(field, cursorPos);
  };

  return (
    <Tooltip title={i18n('entities.specialCharacters.modal.insert')} placement="right">
      <Button id="modal-opener" className={btnClass || 'insert-modal-opener'} onClick={handleClick} onMouseEnter={handleMouseEnter} onFocus={() => {}}>
        <Symbols color={btnColor} />
        {btnLabel && btnLabel}
      </Button>
    </Tooltip>
  );
};
