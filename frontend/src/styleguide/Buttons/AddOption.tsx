import React from 'react';
import { PlusCircleTwoTone } from '@ant-design/icons';
import './buttons.less';
import { i18n } from 'i18n';

interface Props {
  add: (e: any) => void;
  option: string;
}

const AddOptionButton = ({ add, option }: Props) => {
  return (
    <section className="add-option">
      <button data-cy={`add-option-${option.replace(' ', '')}`} type="button" id="add-new-option" className="additional" onMouseDown={(e: any) => e.preventDefault()} onClick={add}>
        <PlusCircleTwoTone className="ix-two-tone-icon" />
        {`${i18n('common.add')} ${option}`}
      </button>
    </section>
  );
};

export default AddOptionButton;
