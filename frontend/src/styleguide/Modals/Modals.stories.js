import React, { Fragment } from 'react';
import { storiesOf } from '@storybook/react';
import { boolean, select, text, withKnobs } from '@storybook/addon-knobs';
import { SpecialCharactersModal } from './SpecialCharactersModal';
import IxcTheme from '../styles/IxcTheme';
import MDX from './Modals.mdx';

const stories = storiesOf('Design System/Modals', module).addDecorator(withKnobs);

const visible = false;

stories.add(
  'Special Characters Modal',
  () => {
    const story = (
      <div>
        <div style={{ display: 'flex', flexDirection: 'row' }}>
          <div style={{ marginTop: '10px', marginLeft: '10px' }}>
            <SpecialCharactersModal includeSampleOpenerButton />
          </div>
        </div>
        <div>
          <div style={{ marginTop: '10px', marginLeft: '10px' }}>
            <SpecialCharactersModal />
          </div>
        </div>
      </div>
    );

    return story;
  },
  {
    docs: { page: MDX },
  },
);
