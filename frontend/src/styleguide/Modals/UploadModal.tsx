import React, { useEffect, useState } from 'react';
import { Modal, message } from 'antd';
import { i18n } from 'i18n';
import { Drawing } from 'graphql/drawing';
import { FileObject, FileUploadDragger } from 'styleguide/File/Upload';
import './Modals.less';

interface UploadProps {
  isDisplayed: boolean;
  handleUpload: (files: any[], skipAlignment?: boolean) => void;
  handleCancel: () => void;
  entity: string;
  fileType: 'pdf' | 'excel' | 'image' | 'any';
  fileExt: string;
  hint: string;
  limit: number;
  cancelText?: string;
  submitText?: string;
  type: 'Drawing' | 'Attachment' | 'Report';
  targetDrawing?: Drawing | null | undefined;
}

export const UploadModal = ({ isDisplayed, handleUpload, handleCancel, entity, fileType, fileExt, hint, limit, cancelText, submitText, type, targetDrawing }: UploadProps) => {
  const [files, setFiles] = useState<{ [key: string]: FileObject }>({});
  const [errorMessages, setErrorMessages] = useState<string[] | undefined>();
  const [submitting, setSubmitting] = useState<boolean>(false);
  const [hasMatchingDims, setHasMatchingDims] = useState(false);
  const [skipAlignment, setSkipAlignment] = useState(false);

  useEffect(() => {
    if (errorMessages) {
      errorMessages.forEach((err) => {
        message.error(err, 5);
      });
    }
  }, [errorMessages]);

  useEffect(() => {
    if (!isDisplayed) {
      setFiles({});
      setSubmitting(false);
      setSkipAlignment(false);
      setHasMatchingDims(false);
    }
  }, [isDisplayed]);

  const onOk = async () => {
    if (files && Object.values(files).length > 0 && Object.values(files).length <= limit && (!errorMessages || errorMessages.length === 0)) {
      setSubmitting(true);
      await handleUpload(Object.values(files), skipAlignment);
    } else {
      setSubmitting(false);
      (errorMessages || []).forEach((err) => {
        message.error(err, 3);
      });
    }
  };

  const onCancel = () => {
    setFiles({});
    setSubmitting(false);
    setErrorMessages(undefined);
    handleCancel();
  };

  return (
    <Modal
      className="ix-modal file-upload-modal"
      visible={isDisplayed}
      okText={submitText}
      closable={false}
      onOk={onOk}
      okButtonProps={{ disabled: Object.values(files).length > limit || Object.values(files).length < 1 || (errorMessages && errorMessages.length > 0), loading: submitting }}
      cancelText={cancelText}
      onCancel={onCancel}
    >
      <FileUploadDragger //
        entity={entity}
        fileType={fileType}
        fileExt={fileExt}
        hint={hint}
        limit={limit}
        files={files}
        setFiles={setFiles}
        errorMessages={errorMessages}
        setErrorMessages={setErrorMessages}
        type={type}
        hasMatchingDims={hasMatchingDims}
        setHasMatchingDims={setHasMatchingDims}
        skipAlignment={skipAlignment}
        setSkipAlignment={setSkipAlignment}
        targetDrawing={targetDrawing}
      />
    </Modal>
  );
};

UploadModal.defaultProps = {
  cancelText: i18n('common.cancel'),
  submitText: i18n('common.next'),
};
