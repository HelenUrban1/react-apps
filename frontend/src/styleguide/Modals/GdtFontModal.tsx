import { CloseOutlined, DownloadOutlined, InfoCircleOutlined } from '@ant-design/icons';
import { Modal } from 'antd';
import { i18n } from 'i18n';
import React from 'react';
import { IxButton } from 'styleguide';
import './Modals.less';

interface GdtFontModalProps {
  show?: boolean;
  handleCancel: () => void;
  handleChange: (e: React.ChangeEvent<HTMLInputElement>) => void;
  handleNoFont: () => void;
  handleWithFont: () => void;
}

export const GdtFontModal = ({ show, handleCancel, handleChange, handleNoFont, handleWithFont }: GdtFontModalProps) => {
  return (
    <Modal
      className="gdt-font-modal"
      visible={!!show}
      title={i18n('prompts.font.title')}
      width="720px"
      centered
      onCancel={handleCancel}
      footer={[
        <section key="footer" className="promptFooter">
          <label key="setting" htmlFor="promptFont" className="checkboxInput">
            <input type="checkbox" id="promptFont" name="promptFont" onChange={handleChange} />
            {i18n('prompts.font.dontAsk')}
          </label>
          <IxButton key="cancel" dataCy="SettingsCancel-btn" leftIcon={<CloseOutlined />} className="btn-secondary" text={i18n('prompts.font.cancel')} onClick={handleNoFont} style={{ marginRight: '10px' }} />
          <IxButton key="submit" className="btn-primary" leftIcon={<DownloadOutlined />} text={i18n('prompts.font.download')} onClick={handleWithFont} />
        </section>,
      ]}
    >
      <div>
        <p>{i18n('prompts.font.description1')}</p>
        <p>{i18n('prompts.font.description2')}</p>
        <p>{i18n('prompts.font.description3')}</p>
        <section className="prompt-images">
          <article>
            <p className="label">{i18n('prompts.font.withFont')}</p>
            <img src={`${process.env.PUBLIC_URL}/images/examples/with.png`} alt={i18n('prompt.font.withFont')} />
          </article>
          <article>
            <p className="label">{i18n('prompts.font.noFont')}</p>
            <img src={`${process.env.PUBLIC_URL}/images/examples/without.png`} alt={i18n('prompt.font.noFont')} />
          </article>
        </section>
        <br />
        <div style={{ display: 'flex' }}>
          <InfoCircleOutlined className="span-icon" style={{ fontSize: 20, color: '#1890ff', marginRight: 8 }} />
          <p>{i18n('prompts.font.description4')}</p>
        </div>
      </div>
    </Modal>
  );
};
