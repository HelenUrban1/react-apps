/* eslint import/no-extraneous-dependencies: ["error", {"devDependencies": true}] */
/* eslint no-alert: "off" */
/* eslint no-console: "off" */

import React from 'react';
import { storiesOf } from '@storybook/react';
import { useState, useEffect } from '@storybook/addons';
import { number, select, text, withKnobs } from '@storybook/addon-knobs';
import { UploadModal } from './UploadModal';

const stories = storiesOf('Design System/Modals', module).addDecorator(withKnobs);

stories.add('Upload Modal', () => {
  // // Knobs
  const entity = text('Entity', 'part', 'Upload Settings');
  const fileType = select('File Type', ['pdf', 'excel', 'image'], 'pdf', 'Upload Settings');
  const hint = text('Hint', 'PDF', 'Upload Settings');
  const limit = number('Limit', 10, {}, 'Upload Settings');
  const cancelText = text('Cancel Text', 'Cancel', 'Upload Settings');
  const submitText = text('Submit Text', 'Submit', 'Upload Settings');

  // State
  const [fileExt, setExt] = useState('.pdf');

  useEffect(() => {
    switch (fileType) {
      case 'pdf':
        setExt('.pdf');
        break;
      case 'excel':
        setExt('.xls');
        break;
      case 'image':
        setExt('.png');
        break;
      default:
        setExt('');
        break;
    }
  }, [fileType]);

  // Functions
  const handleUpload = (files) => {
    alert('Uploaded');
    console.log({ files });
  };

  const handleCancel = () => {
    alert('Cancelled');
  };

  return (
    <UploadModal //
      isDisplayed
      handleUpload={handleUpload}
      handleCancel={handleCancel}
      entity={entity}
      fileType={fileType}
      fileExt={fileExt}
      hint={hint}
      limit={limit}
      cancelText={cancelText}
      submitText={submitText}
    />
  );
});
