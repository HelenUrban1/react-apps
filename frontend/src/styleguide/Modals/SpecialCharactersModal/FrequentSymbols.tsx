import React, { useEffect, useState } from 'react';
import { CharacterButton } from './CharacterButton';

interface SymbolsProps {
  frequentSymbols: { [key: string]: number };
  handleInsertSymbol: (event: any, char: string) => void;
}

export const FrequentSymbols = ({ frequentSymbols, handleInsertSymbol }: SymbolsProps) => {
  const [subList, setSubList] = useState<string[]>(
    Object.keys(frequentSymbols)
      .sort((a, b) => (frequentSymbols[a] > frequentSymbols[b] ? -1 : 1))
      .slice(0, 18),
  );

  useEffect(() => {
    setSubList(
      Object.keys(frequentSymbols)
        .sort((a, b) => (frequentSymbols[a] > frequentSymbols[b] ? -1 : 1))
        .slice(0, 18),
    );
  }, [frequentSymbols]);

  return (
    <>
      {subList.map((char) => (
        <CharacterButton
          key={char}
          char={char}
          onClick={() => {
            handleInsertSymbol(undefined, char);
          }}
        />
      ))}
    </>
  );
};
