import { getFramedTip, getInitVal, insertCharacterAtCursor } from './symbolsUtil';

// TODO: Merge these operations with frontend/src/utils/textOperations.test.js

describe('Util functions for symbols', () => {
  it('inserts the symbol at the right spot', () => {
    expect(insertCharacterAtCursor('123', 1, 1, 'A', false)).toEqual('1A23');
    expect(insertCharacterAtCursor('|123|', 2, 2, 'A', true)).toEqual('');
    expect(insertCharacterAtCursor('|123|', 2, 3, 'A', true)).toEqual('');
  });

  it('gets the tip for framed text', () => {
    expect(getFramedTip('fullSpecification', { notationClass: 'Tolerance' }, '3.14', false)).toEqual(undefined);
    expect(getFramedTip('gdtPrimaryToleranceZone', { notationClass: 'Tolerance' }, '3.14', false)).toEqual('Text will be framed in the Full Specification');
    expect(getFramedTip('fullSpecification', { notationClass: 'Basic' }, '3.14', true)).toEqual('Basics automatically frame between Pipe symbols "|"');
    expect(getFramedTip('comment', { notationClass: 'Tolerance' }, '3.14', true)).toEqual('Add Pipe symbols "|" to frame text');
    expect(getFramedTip('comment', { notationClass: 'Tolerance' }, '|3.14|', true)).toEqual(undefined);
  });

  it('gets the initial value', () => {
    expect(getInitVal(false, '1.15')).toEqual('1.15');
    expect(getInitVal(true, '1.15')).toEqual('1.15');
    expect(getInitVal(true, '|1.15|')).toEqual('');
    expect(getInitVal(true, null)).toEqual('');
  });
});
