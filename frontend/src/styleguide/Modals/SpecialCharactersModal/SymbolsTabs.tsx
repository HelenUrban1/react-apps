import React, { useEffect, useState } from 'react';
import { Tabs, Input } from 'antd';
import { i18n } from 'i18n';
import { SearchOutlined } from '@ant-design/icons';
import { Characteristic } from 'types/characteristics';
import { defaultTypeIDs } from 'view/global/defaults';
import { CharacterButton } from './CharacterButton';
import { GetAllCharacters } from './character-list';

const { TabPane } = Tabs;

interface SymbolsTabsProps {
  handleInsertSymbol: (event: any, char: string) => void;
  feature: Characteristic | null;
}

export const SymbolsTabs = ({ handleInsertSymbol, feature }: SymbolsTabsProps) => {
  // Get the list of characters to display in each tab
  const symbols = GetAllCharacters();

  const characterSets = [i18n('entities.specialCharacters.modal.tabs.dimensional'), i18n('entities.specialCharacters.modal.tabs.gdt'), i18n('entities.specialCharacters.modal.tabs.other')];

  const [cadList, setCadList] = useState<{ char: string; name: string }[]>(symbols.CADSymbols.sort((a, b) => a.name.localeCompare(b.name)));
  const [gdtList, setGdtList] = useState<{ char: string; name: string }[]>(symbols.GDTSymbols.sort((a, b) => a.name.localeCompare(b.name)));
  const [commonList, setCommonList] = useState<{ char: string; name: string }[]>(symbols.OtherCharacters.sort((a, b) => a.name.localeCompare(b.name)));
  const [activeTab, setActiveTab] = useState(feature?.notationType === defaultTypeIDs['Geometric Tolerance'] ? '2' : '1');

  const [search, setSearch] = useState<string[]>(['', '', '']);

  // search symbols and filter lists on changes to search input
  useEffect(() => {
    switch (activeTab) {
      case '1':
        setCadList(search[0] ? symbols.CADSymbols.filter((symbol) => symbol.name.toLowerCase().includes(search[0].toLowerCase())).sort((a, b) => a.name.localeCompare(b.name)) : symbols.CADSymbols);
        break;
      case '2':
        setGdtList(search[1] ? symbols.GDTSymbols.filter((symbol) => symbol.name.toLowerCase().includes(search[1].toLowerCase())).sort((a, b) => a.name.localeCompare(b.name)) : symbols.GDTSymbols);
        break;
      case '3':
        setCommonList(search[2] ? symbols.OtherCharacters.filter((symbol) => symbol.name.toLowerCase().includes(search[2].toLowerCase())).sort((a, b) => a.name.localeCompare(b.name)) : symbols.OtherCharacters);
        break;
      default:
        break;
    }
  }, [search]);

  const handleSearch = (e: any, tab: number) => {
    const newSearch = [...search];
    newSearch[tab] = e.target.value || '';
    setSearch(newSearch);
  };

  const handleChangeTab = (e: any) => {
    setActiveTab(e);
  };

  return (
    <Tabs defaultActiveKey={activeTab} activeKey={activeTab} onChange={handleChangeTab}>
      <TabPane tab={characterSets[0]} key="1" className="modal-pane" style={{ height: '200px', overflowY: 'auto' }}>
        <Input data-cy="tab-1-search" placeholder={i18n('entities.specialCharacters.modal.search')} onChange={(e: any) => handleSearch(e, 0)} prefix={<SearchOutlined />} allowClear />
        <div className="character-bank column">
          {cadList.map((char) => (
            <div className="character-row">
              <CharacterButton key={char.name} char={char.char} onClick={handleInsertSymbol} />
              {char.name}
            </div>
          ))}
        </div>
      </TabPane>
      <TabPane tab={characterSets[1]} key="2" className="modal-pane" style={{ height: '200px', overflowY: 'auto' }}>
        <Input data-cy="tab-2-search" placeholder={i18n('entities.specialCharacters.modal.search')} onChange={(e: any) => handleSearch(e, 1)} prefix={<SearchOutlined />} allowClear />
        <div className="character-bank column">
          {gdtList.map((char) => (
            <div className="character-row">
              <CharacterButton key={char.name} char={char.char} onClick={handleInsertSymbol} />
              {char.name}
            </div>
          ))}
        </div>
      </TabPane>
      <TabPane tab={characterSets[2]} key="3" className="modal-pane" style={{ height: '200px', overflowY: 'auto' }}>
        <Input data-cy="tab-3-search" placeholder={i18n('entities.specialCharacters.modal.search')} onChange={(e: any) => handleSearch(e, 2)} prefix={<SearchOutlined />} allowClear />
        <div className="character-bank column">
          {commonList.map((char) => (
            <div className="character-row">
              <CharacterButton key={char.name} char={char.char} onClick={handleInsertSymbol} />
              {char.name}
            </div>
          ))}
        </div>
      </TabPane>
    </Tabs>
  );
};
