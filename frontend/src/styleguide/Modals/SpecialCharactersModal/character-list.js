/**
 * This file was imported from the legacy IX web app prototype codebase.
 */
import { CharacterDictionary } from './character-dict';

const getZeros = (len) => {
  let z = '';
  for (let i = 0; i < len; i++) {
    z += '0';
  }
  return z;
};

function HexDecode(hex) {
  return String.fromCodePoint(hex);
}

// Catch-all for unicode characters of unspecific category
function OtherCharList() {
  const characters = [];
  /* for (let uni = 0x0030; uni <= 0x0039; ++uni)
  {
    characters.push({char: HexDecode(uni));
  }
  // capitol letters
  for (let uni = 0x0041; uni <= 0x005A; ++uni)
  {
    characters.push({char: HexDecode(uni));
  }
  // lower case letters
  for (let uni = 0x0061; uni <= 0x007A; ++uni)
  {
    characters.push({char: HexDecode(uni));
  }
  // punctuation
  for (let uni = 0x0021; uni <= 0x002E; ++uni)
  {
    characters.push({char: HexDecode(uni));
  }
  for (let uni = 0x003A; uni <= 0x0040; ++uni)
  {
    characters.push({char: HexDecode(uni));
  }
  for (let uni = 0x007B; uni <= 0x007E; ++uni)
  {
    characters.push({char: HexDecode(uni));
  } */
  // symbols and special letters
  for (let uni = 0x00a1; uni <= 0x024f; ++uni) {
    const uniString = `0x${getZeros(4 - uni.toString(16).length)}${uni.toString(16)}`;
    if (!CharacterDictionary[uniString].includes('0x')) characters.push({ char: HexDecode(uni), name: CharacterDictionary[uniString] + uniString });
  }
  return characters;
}

function CommonSubset() {
  const characters = [
    { char: HexDecode(0xe177), name: 'Diameter' }, // Diameter
    { char: HexDecode(0xb0), name: 'Degree' }, // Degree
    { char: HexDecode(0xb1), name: 'Plus Minus' }, // Plus-Min
    { char: HexDecode(0x21a7), name: 'Depth' }, // Depth
    { char: HexDecode(0x2335), name: 'Countersink' }, // Countersink
    { char: HexDecode(0x2334), name: 'Counterbore' }, // Counterbore
    { char: HexDecode(0x2313), name: 'Profile of a Surface' }, // Profile of a Surface
    { char: HexDecode(0x2316), name: 'Position' }, // Position
    { char: HexDecode(0x23e5), name: 'Flatness' }, // Flatness
    { char: HexDecode(0x22a5), name: 'Perpendicularity' }, // Perpendicularity
    { char: HexDecode(0x2220), name: 'Angularity' }, // Angularity
    { char: HexDecode(0x25cb), name: 'Circularity' }, // Circularity
    { char: HexDecode(0x2197), name: 'Circular Runout' }, // Circular Runout
    { char: HexDecode(0x2330), name: 'Total Runout' }, // Total Runout
    { char: HexDecode(0x2312), name: 'Profile of a Line' }, // Profile of a Line
    { char: HexDecode(0x233e), name: 'Concentricity' }, // Concentricity
    { char: HexDecode(0x232f), name: 'Symmetry' }, // Symmetry
    { char: HexDecode(0x2225), name: 'Parallelism ISO' }, // Parallelism ISO
    { char: HexDecode(0xe16b), name: 'Parallelism ANSI' }, // Parallelism ANSI
    { char: HexDecode(0x23af), name: 'Straightness' }, // Straightness
    { char: HexDecode(0x232d), name: 'Cylindricity' }, // Cylindricity
    { char: HexDecode(0x24c5), name: 'Projected Tol Zone' }, // Projected Tol Zone
    { char: HexDecode(0x24c2), name: 'Max Material Condition' }, // MMC
    { char: HexDecode(0x24c1), name: 'Least Material Condition' }, // LMC
    { char: HexDecode(0x24bb), name: 'Free State' }, // Free State
    { char: HexDecode(0x24c8), name: 'Uniquely Disposed Profile S' }, // Uniquely Disposed Profile S
    { char: HexDecode(0x24ca), name: 'Uniquely Disposed Profile U' }, // Uniquely Disposed Profile U
    { char: HexDecode(0xe20c), name: '(R)' }, // (R)
  ];

  return characters;
}

function FramedCharacters() {
  const characters = [];
  for (let uni = 0xe020; uni <= 0xe05e; uni++) {
    if (uni === 0xe021) continue;
    if (uni === 0xe022) continue;
    if (uni === 0xe023) continue;
    if (uni === 0xe024) continue;
    if (uni === 0xe026) continue;
    if (uni === 0xe027) continue;
    if (uni === 0xe02a) continue;
    if (uni === 0xe03a) continue;
    if (uni === 0xe03b) continue;
    if (uni === 0xe03c) continue;
    if (uni === 0xe03d) continue;
    if (uni === 0xe03e) continue;
    if (uni === 0xe03f) continue;
    if (uni === 0xe040) continue;
    if (uni === 0xe05c) continue;
    const uniString = `0x${getZeros(4 - uni.toString(16).length)}${uni.toString(16)}`;
    if (!CharacterDictionary[uniString].includes('0x')) characters.push({ char: HexDecode(uni), name: CharacterDictionary[uniString] });
  }
  characters.push({ char: HexDecode(0xe05f), name: 'Underscore' }); // underscore framed (_)
  characters.push({ char: HexDecode(0xe060), name: 'Grave Accent' }); // grave accent framed (`)
  characters.push({ char: HexDecode(0xe07b), name: 'Start Frame' }); // Start Frame
  characters.push({ char: HexDecode(0xe07c), name: 'Pipe' }); // pipe characteristic
  characters.push({ char: HexDecode(0xe07d), name: 'End Frame' }); // End Frame
  characters.push({ char: HexDecode(0xe07e), name: 'Tilde' }); // tilde framed (~)
  characters.push({ char: HexDecode(0xe0b0), name: 'Degree' }); // degree framed (°)
  characters.push({ char: HexDecode(0xe0b1), name: 'Plus Minus' }); // plus/minus framed (±)
  characters.push({ char: HexDecode(0xe0b5), name: 'Micro' }); // mu framed (μ)
  characters.push({ char: HexDecode(0xe361), name: 'Straightness' }); // straightness framed
  characters.push({ char: HexDecode(0xe362), name: 'Flatness' }); // flatness framed
  characters.push({ char: HexDecode(0xe364), name: 'Circularity' }); // circulatiry framed
  characters.push({ char: HexDecode(0xe365), name: 'Cylindricity' }); // cylindricity framed
  characters.push({ char: HexDecode(0xe367), name: 'Profile of a Line' }); // profile of a line framed
  characters.push({ char: HexDecode(0xe368), name: 'Profile of a Surface' }); // profile of a surface framed
  characters.push({ char: HexDecode(0xe369), name: 'Angularity' }); // angularity framed
  characters.push({ char: HexDecode(0xe36a), name: 'Perpendicularity' }); // perpendicularity framed
  characters.push({ char: HexDecode(0xe36b), name: 'Parallelism' }); // parallelism framed
  characters.push({ char: HexDecode(0xe36e), name: 'Position' }); // position framed
  characters.push({ char: HexDecode(0xe36f), name: 'Concentricity' }); // concentricity framed
  characters.push({ char: HexDecode(0xe371), name: 'Symmetry' }); // symmetry framed
  characters.push({ char: HexDecode(0xe375), name: 'Runout' }); // runout framed
  characters.push({ char: HexDecode(0xe376), name: 'Total Runout' }); // total runout framed
  characters.push({ char: HexDecode(0xe36c), name: 'Least Material Condition' }); // lmc/lmb framed (circled L)
  characters.push({ char: HexDecode(0xe36d), name: 'Max Material Condition' }); // mmc/mmb framed (circled M)
  characters.push({ char: HexDecode(0xe374), name: 'Tangent Plane' }); // tangent plane framed (circle T)
  characters.push({ char: HexDecode(0xe370), name: 'Projected Tolerance' }); // projected tolerance framed (circle P)
  characters.push({ char: HexDecode(0xe366), name: 'Free State' }); // free state framed (circle F)
  characters.push({ char: HexDecode(0xe377), name: 'Diameter' }); // radius framed (Ø)
  characters.push({ char: HexDecode(0xe379), name: 'Spherical Radius' }); // spherical radius framed (SR)
  characters.push({ char: HexDecode(0xe37a), name: 'Spherical Diameter' }); // sperifcal diameter framed (SØ)
  characters.push({ char: HexDecode(0xe363), name: 'Controlled Radius' }); // controlled radius framed (CR)
  characters.push({ char: HexDecode(0xe373), name: 'Statistical Tolerance' }); // statistical tolerance framed
  characters.push({ char: HexDecode(0xe3df), name: '' }); // regardless of feature size framed (circle s)
  characters.push({ char: HexDecode(0xe409), name: 'Circle E' }); // (circle E)
  characters.push({ char: HexDecode(0xe40a), name: 'Continuous Features' }); // continuous features framed (CF)
  characters.push({ char: HexDecode(0xe40b), name: 'Independency' }); // independency framed (circle I)
  characters.push({ char: HexDecode(0xe40c), name: 'Circle R' }); // (circle R)
  characters.push({ char: HexDecode(0xe40d), name: 'Unequally Disposed' }); // unequally disposed framed (circle U)
  characters.push({ char: HexDecode(0xe408), name: 'Translation' }); // translation framed
  characters.push({ char: HexDecode(0xe326), name: 'Square' }); // square framed
  characters.push({ char: HexDecode(0xe329), name: 'Between' }); // between framed
  characters.push({ char: ']', name: 'Close Bracket' });
  characters.push({ char: '[', name: 'Open Bracket' });
  return characters;
}

function GDTSymbols() {
  const characters = [];

  // Subtypes
  characters.push({ char: HexDecode(0x2197), name: 'Circular Runout' }); // Circular Runout
  characters.push({ char: HexDecode(0x2225), name: 'Parallelism ISO' }); // Parallelism ISO
  characters.push({ char: HexDecode(0x22a5), name: 'Perpendicularity' }); // Perpendicularity
  characters.push({ char: HexDecode(0x2316), name: 'Position' }); // Position
  characters.push({ char: HexDecode(0xe161), name: CharacterDictionary['0xe161'] }); // Straightness (GDT)
  characters.push({ char: HexDecode(0xe162), name: CharacterDictionary['0xe162'] }); // Flatness (GDT)
  characters.push({ char: HexDecode(0xe164), name: CharacterDictionary['0xe164'] }); // Circularity (GDT)
  characters.push({ char: HexDecode(0xe165), name: CharacterDictionary['0xe165'] }); // Cylindricity (GDT)
  characters.push({ char: HexDecode(0xe167), name: CharacterDictionary['0xe167'] }); // Profile of a Line (GDT)
  characters.push({ char: HexDecode(0xe168), name: CharacterDictionary['0xe168'] }); // Profile of a Surface (GDT)
  characters.push({ char: HexDecode(0xe169), name: CharacterDictionary['0xe169'] }); // Angularity (GDT)
  characters.push({ char: HexDecode(0xe16b), name: 'Parallelism ANSI' }); // Parallelism ANSI
  characters.push({ char: HexDecode(0xe171), name: CharacterDictionary['0xe171'] }); // Symmetry (GDT)
  characters.push({ char: HexDecode(0xe175), name: CharacterDictionary['0xe175'] }); // Runout (GDT)
  characters.push({ char: HexDecode(0xe176), name: CharacterDictionary['0xe176'] }); // Total Runout (GDT)

  // Modifiers
  characters.push({ char: HexDecode(0xe129), name: CharacterDictionary['0xe129'] }); // Between (Both)
  characters.push({ char: HexDecode(0xe13d), name: CharacterDictionary['0xe13d'] }); // Statistical Tolerance (Both)
  characters.push({ char: HexDecode(0xe163), name: CharacterDictionary['0xe163'] }); // Controlled Radius (Both)
  characters.push({ char: HexDecode(0xe166), name: CharacterDictionary['0xe166'] }); // Free State (GDT)
  characters.push({ char: HexDecode(0xe170), name: CharacterDictionary['0xe170'] }); // Projected Tolerance (GDT)
  characters.push({ char: HexDecode(0xe13d), name: CharacterDictionary['0xe13d'] }); // Statistical Tolerance (Both)
  characters.push({ char: HexDecode(0xe174), name: CharacterDictionary['0xe174'] }); // Tangent Plane (GDT)
  characters.push({ char: HexDecode(0xe1df), name: CharacterDictionary['0xe1df'] }); // Regardless of Feature Size (S) (GDT)
  characters.push({ char: HexDecode(0xe208), name: CharacterDictionary['0xe208'] }); // Translation (Both)
  characters.push({ char: HexDecode(0xe209), name: CharacterDictionary['0xe209'] }); // (E) Envelope Requirement (GDT)
  characters.push({ char: HexDecode(0xe20a), name: CharacterDictionary['0xe20a'] }); // Continuous Feature (CF) (GDT)
  characters.push({ char: HexDecode(0xe20b), name: CharacterDictionary['0xe20b'] }); // Independency (I) (GDT)
  characters.push({ char: HexDecode(0xe20c), name: CharacterDictionary['0xe20c'] }); // (R) Not Set (GDT)
  characters.push({ char: HexDecode(0xe20d), name: CharacterDictionary['0xe20d'] }); // Unequally Disposed (U) (GDT)

  characters.push({ char: HexDecode(0x24c2), name: CharacterDictionary['0xe16d'] }); // Max Material Condition
  characters.push({ char: HexDecode(0x24c1), name: CharacterDictionary['0xe16c'] }); // Least Material Condition

  // Structural
  characters.push({ char: '|', name: CharacterDictionary['0x01c0'] }); // Pipe (Both)

  return characters;
}

function CADSymbols() {
  const characters = [];

  characters.push({ char: HexDecode(0xe121), name: CharacterDictionary['0xe121'] }); // Distance Left of Position (CAD)
  characters.push({ char: HexDecode(0xe122), name: CharacterDictionary['0xe122'] }); // Datum Extended Left (CAD)
  characters.push({ char: HexDecode(0xe123), name: CharacterDictionary['0xe123'] }); // Slope (CAD)
  characters.push({ char: HexDecode(0xe124), name: CharacterDictionary['0xe124'] }); // Counterbore (CAD)
  characters.push({ char: HexDecode(0xe125), name: CharacterDictionary['0xe125'] }); // Countersink (CAD)
  characters.push({ char: HexDecode(0xe126), name: CharacterDictionary['0xe126'] }); // Square (CAD)
  characters.push({ char: HexDecode(0xe129), name: CharacterDictionary['0xe129'] }); // Between (Both)
  characters.push({ char: HexDecode(0xe13a), name: CharacterDictionary['0xe13a'] }); // Datum Extended Right (CAD)
  characters.push({ char: HexDecode(0xe13d), name: CharacterDictionary['0xe13d'] }); // Statistical Tolerance (Both)
  characters.push({ char: HexDecode(0xe140), name: CharacterDictionary['0xe140'] }); // Conical Taper (CAD)
  characters.push({ char: HexDecode(0xe151), name: CharacterDictionary['0xe151'] }); // Block (CAD)
  characters.push({ char: HexDecode(0xe163), name: CharacterDictionary['0xe163'] }); // Controlled Radius (Both)
  characters.push({ char: HexDecode(0xe177), name: CharacterDictionary['0xe177'] }); // Diameter (CAD)
  characters.push({ char: HexDecode(0xe179), name: CharacterDictionary['0xe179'] }); // Spherical Radius (CAD)
  characters.push({ char: HexDecode(0xe17a), name: CharacterDictionary['0xe17a'] }); // Spherical Diameter (CAD)
  characters.push({ char: HexDecode(0xe17e), name: CharacterDictionary['0xe17e'] }); // Distance Right of Position (CAD)
  characters.push({ char: HexDecode(0xe200), name: CharacterDictionary['0xe200'] }); // Distance from Position (CAD)
  characters.push({ char: HexDecode(0xe201), name: CharacterDictionary['0xe201'] }); // Up Arrow (CAD)
  characters.push({ char: HexDecode(0xe202), name: CharacterDictionary['0xe202'] }); // Down Arrow (CAD)
  characters.push({ char: HexDecode(0xe203), name: CharacterDictionary['0xe203'] }); // Left Arrow (CAD)
  characters.push({ char: HexDecode(0xe204), name: CharacterDictionary['0xe204'] }); // Right Arrow (CAD)
  characters.push({ char: HexDecode(0xe205), name: CharacterDictionary['0xe205'] }); // Centerline (CAD)
  characters.push({ char: HexDecode(0xe206), name: CharacterDictionary['0xe206'] }); // Property Line (CAD)
  characters.push({ char: HexDecode(0xe207), name: CharacterDictionary['0xe207'] }); // Spotface (CAD)
  characters.push({ char: HexDecode(0xe208), name: CharacterDictionary['0xe208'] }); // Translation (Both)
  characters.push({ char: HexDecode(0xe20e), name: CharacterDictionary['0xe20e'] }); // Surface Finish (CAD)
  characters.push({ char: HexDecode(0xe20f), name: CharacterDictionary['0xe20f'] }); // Surface Finish Removal Process (CAD)
  characters.push({ char: HexDecode(0xe210), name: CharacterDictionary['0xe210'] }); // Surface Finish Removal Not Permitted (CAD)
  characters.push({ char: HexDecode(0xe211), name: CharacterDictionary['0xe211'] }); // Weld (CAD)
  characters.push({ char: HexDecode(0xe15e), name: CharacterDictionary['0xe15e'] }); // Depth (CAD)
  characters.push({ char: HexDecode(0x00b0), name: CharacterDictionary['0x00b0'] }); // Degree (CAD)
  characters.push({ char: HexDecode(0x00b1), name: CharacterDictionary['0x00b1'] }); // Plus Minus (CAD)
  characters.push({ char: '|', name: CharacterDictionary['0x01c0'] }); // Pipe (Both)
  characters.push({ char: HexDecode(0x00b5), name: CharacterDictionary['0x00b5'] }); // Mirco Sign (CAD)

  return characters;
}

export function GetAllCharacters() {
  return {
    OtherCharacters: OtherCharList(),
    FramedCharacters: FramedCharacters(),
    GDTSymbols: GDTSymbols(),
    CADSymbols: CADSymbols(),
    CommonCharacters: CommonSubset(),
  };
}
