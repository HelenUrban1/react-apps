import config from 'config';
import { Characteristic } from 'types/characteristics';
import { i18n } from 'i18n';
import { downloadFile } from 'utils/files';
import { getBetweenPipes, getFramedText } from 'utils/textOperations';

export const getFramedTip = (field: string, item: Characteristic, val: string | undefined, framed: boolean) => {
  if (field.includes('gdt')) {
    return i18n('entities.specialCharacters.modal.frameTips.fullSpec');
  }
  if (item.notationClass === 'Basic' && field === 'fullSpecification') {
    return i18n('entities.specialCharacters.modal.frameTips.basic');
  }
  if (framed && !getBetweenPipes(val || '')) {
    return i18n('entities.specialCharacters.modal.frameTips.noPipes');
  }
  return undefined;
};

export const loadCaptureImage = async (characteristicId: string | undefined) => {
  if (characteristicId) {
    const imageData = await downloadFile(`${config.backendUrl}/download?privateUrl=${characteristicId}.png`);
    const reader = new window.FileReader();
    reader.readAsDataURL(imageData);
    reader.onload = () => {
      const imageDataUrl = reader.result as string;
      const imageCapture = document.getElementById('modal-capture-image');
      imageCapture?.setAttribute('src', imageDataUrl);
    };
  }
};

export const getInitVal = (framed: boolean, value: string) => {
  if (framed) {
    return getFramedText(value || '');
  }

  return value === null ? '' : value;
};

export const insertCharacterAtCursor = (val: string, start: number, end: number, symbol: string, frameText: boolean) => {
  let newVal = val.slice(0, start) + symbol + val.slice(end, Math.max(val.length, 0));
  if (frameText) {
    newVal = getFramedText(newVal);
  }
  return newVal;
};
