import React, { useState, FormEvent, useEffect, useRef, useCallback } from 'react';
import { Button, Modal, Form, Input, Row, Col, Checkbox, Popconfirm } from 'antd';
import { QuestionCircleOutlined } from '@ant-design/icons';
import { i18n } from 'i18n';
import { Characteristic } from 'types/characteristics';
import Analytics from 'modules/shared/analytics/analytics';
import Draggable from 'react-draggable';
import { getFramedText, getUnframedText } from 'utils/textOperations';
import { symbolDefaults } from 'view/global/defaults';
import { updateSettingThunk } from 'modules/session/sessionActions';
import { Settings } from 'domain/setting/settingApi';
import { useDispatch, useSelector } from 'react-redux';
import { AppState } from 'modules/state';
import { FrequentlyUsedSymbols } from 'domain/setting/settingTypes';
import { FrequentSymbols } from './FrequentSymbols';
import { SymbolsTabs } from './SymbolsTabs';
import { getFramedTip, getInitVal, insertCharacterAtCursor, loadCaptureImage } from './symbolsUtil';
import '../Modals.less';

export interface CharModalProps {
  field: string;
  value: string;
  label: string;
  item: Characteristic | null;
  update: (e: any, parse?: boolean) => void;
  cursorPos: number;
  source: string;
  textArea?: boolean;
  framed?: boolean;
  capture?: any;
  characteristicId?: string;
  show?: boolean;
  handleCloseModal?: () => void;
}

export const SpecialCharactersModal: React.FC<CharModalProps> = ({ field, value, label, item, update, cursorPos, source, textArea, framed = false, capture, characteristicId, show, handleCloseModal }) => {
  const [val, setVal] = useState<string>(getInitVal(framed, value));
  const [frameText, setFrameText] = useState(framed);
  const [frameTip, setFrameTip] = useState<string | undefined>(item ? getFramedTip(field, item, item.fullSpecification, framed) : undefined);
  const [showSymbols, setShowSymbols] = useState(false);
  const dispatch = useDispatch();
  const { userSettings } = useSelector((state: AppState) => state.session);
  // frequently used symbols
  const [frequentSymbolsObj, setFrequentSymbols] = useState<FrequentlyUsedSymbols>(userSettings.symbols ? { ...userSettings.symbols.metadata } || {} : symbolDefaults);

  const inputField = useRef<any>();
  const currentCursorPos = useRef<number>();

  // Autofocus input
  const wrapper = useCallback(
    (node) => {
      if (node) {
        if (show && capture) loadCaptureImage(characteristicId);
        inputField.current = node;
        if (!currentCursorPos.current) currentCursorPos.current = cursorPos;
        const input = textArea ? (inputField.current.resizableTextArea.textArea as HTMLTextAreaElement) : (inputField.current.input as HTMLInputElement);
        input.setSelectionRange(currentCursorPos.current, currentCursorPos.current);
        input.focus();
      }
    },
    [show],
  );

  // draggable modal stuff
  const [disabled, setDisabled] = useState(true);
  const [bounds, setBounds] = useState({ left: 0, top: 0, right: 0, bottom: 0 });
  const dragRef = useRef<HTMLDivElement | null>(null);

  const handleDragStart = (event: any, uiData: any) => {
    const { clientWidth, clientHeight } = window?.document?.documentElement;
    const targetRect = dragRef?.current?.getBoundingClientRect() || bounds;
    setBounds({
      left: -targetRect?.left + uiData?.x,
      right: clientWidth - (targetRect?.right - uiData?.x),
      top: -targetRect?.top + uiData?.y,
      bottom: clientHeight - (targetRect?.bottom - uiData?.y),
    });
  };

  // handle framing or unframing text when framed state changes
  useEffect(() => {
    if (frameText && item) {
      setFrameTip(getFramedTip(field, item, val, true));
      setVal(getFramedText(val));
    } else {
      if (item) setVal(getUnframedText(val));
      setFrameTip(undefined);
    }
  }, [frameText]);

  // updates the cursor position in the input when the input value changes
  useEffect(() => {
    if (inputField.current) {
      const input = textArea ? (inputField.current.resizableTextArea.textArea as HTMLTextAreaElement) : (inputField.current.input as HTMLInputElement);
      if (input !== null) {
        input.focus();
        input.selectionStart = currentCursorPos.current || 0;
        input.selectionEnd = currentCursorPos.current || 0;
      }
    }
  }, [val]);

  // updates state of modal if opened for a different feature
  useEffect(() => {
    if (item && show) {
      setFrameText(framed);
      setFrameTip(getFramedTip(field, item, item.fullSpecification, framed));
      setVal(framed ? getFramedText(value) : value);
    }
  }, [show]);

  // updates the value when typing in the input field
  const updateValue = (e: any) => {
    if (!inputField.current) return;
    const input = textArea ? (inputField.current.resizableTextArea.textArea as HTMLTextAreaElement) : (inputField.current.input as HTMLInputElement);
    currentCursorPos.current = input.selectionStart || input.selectionStart === 0 ? input.selectionStart : val.length;
    const newVal = frameText ? getFramedText(e.target.value || '') : e.target.value;
    setVal(newVal);
  };

  // updates the cursor position when clicking in the input field
  const updateCursor = () => {
    if (!inputField.current) return;
    const input = textArea ? (inputField.current.resizableTextArea.textArea as HTMLTextAreaElement) : (inputField.current.input as HTMLInputElement);
    const ind = input.selectionStart;
    const indEnd = input.selectionEnd;
    if (ind !== indEnd || ind === null) return;
    currentCursorPos.current = ind;
  };

  const handleResetSymbols = () => {
    if (userSettings.symbols?.id) {
      dispatch(updateSettingThunk(Settings.createGraphqlObject({ ...userSettings.symbols, id: userSettings.symbols.id, metadata: JSON.stringify(symbolDefaults) }), userSettings.symbols.id));
      setFrequentSymbols(symbolDefaults);
    }
  };

  const updateSymbolsSetting = (symbol: string) => {
    const newFrequentSymbols = { ...frequentSymbolsObj };
    if (newFrequentSymbols[symbol]) {
      newFrequentSymbols[symbol] += 1;
    } else {
      newFrequentSymbols[symbol] = 1;
    }
    if (userSettings.symbols?.id) {
      const newSymbols = { ...userSettings.symbols, id: userSettings.symbols.id, metadata: JSON.stringify(newFrequentSymbols) };
      dispatch(updateSettingThunk(Settings.createGraphqlObject(newSymbols), userSettings.symbols.id));
      setFrequentSymbols(newFrequentSymbols);
    }
  };

  // inserts a symbol when clicking a symbol button
  const handleInsertSymbol = (event: any, symbol: string) => {
    if (!inputField.current) return;
    const input = textArea ? (inputField.current.resizableTextArea.textArea as HTMLTextAreaElement) : (inputField.current.input as HTMLInputElement);
    const start = input && input.selectionStart !== null ? input.selectionStart : 0;
    const end = input && input.selectionEnd !== null ? input.selectionEnd : 0;
    setVal(insertCharacterAtCursor(val, start, end, symbol, frameText));
    currentCursorPos.current = start + 1;

    updateSymbolsSetting(symbol);
    Analytics.track({
      event: Analytics.events.specialCharacterAdded,
      characteristic: item,
      properties: {
        'field.name': field,
        'field.value.from': value,
        'field.value.to': value,
        character: symbol,
      },
    });
  };

  // closes the modal
  const closeModal = () => {
    currentCursorPos.current = undefined;
    if (handleCloseModal) handleCloseModal();
  };

  // handle value submission
  const submit = (ev: FormEvent) => {
    ev.preventDefault();
    const data = {
      target: {
        name: field.replace('-modal', ''),
        value: item ? val : '',
        item,
      },
    };
    update(data, true);
    closeModal();
  };

  const handleToggleFramed = (e: any) => {
    setFrameText(e.target.checked);
  };

  const handleShowSymbols = (showVal: boolean) => {
    setShowSymbols(showVal);
  };

  return (
    <>
      <Modal
        visible={show}
        destroyOnClose
        title={
          <div
            className="draggable-modal-area"
            onMouseOver={() => {
              if (disabled) {
                setDisabled(false);
              }
            }}
            onMouseOut={() => setDisabled(true)}
            onFocus={() => {}}
            onBlur={() => {}}
          >
            {i18n('humanReview.insertChars')}
          </div>
        }
        onCancel={closeModal}
        className="special-character-modal"
        modalRender={(modal: any) => (
          <Draggable onStart={(event, uiData) => handleDragStart(event, uiData)} disabled={disabled} bounds={bounds}>
            <div ref={dragRef} className="draggable-modal-div">
              {modal}
            </div>
          </Draggable>
        )}
        footer={
          <section className="modal-buttons">
            <Button key="cancel" onClick={closeModal}>
              {i18n('common.cancel')}
            </Button>
            <Button type="primary" htmlType="submit" onClick={submit}>
              {i18n('common.submit')}
            </Button>
          </section>
        }
      >
        <Form className="modal-form" onFinish={submit} layout="vertical">
          <Row>
            <Col className={`form-column${!capture ? '-full-width' : ''}`}>
              <Form.Item label={label} className="modal-input">
                {textArea ? (
                  <Input.TextArea id={`${field}-${source}-modal`} ref={wrapper} className="modal-input" key={val} value={val} onChange={updateValue} onClick={updateCursor} />
                ) : (
                  <Input id={`${field}-${source}-modal`} ref={wrapper} className="modal-input" key={val} value={val} onChange={updateValue} onClick={updateCursor} />
                )}
              </Form.Item>
            </Col>
            {capture && (
              <Col>
                <section className="modal-capture">{capture}</section>
              </Col>
            )}
          </Row>
          <Checkbox key={`check-${item?.id || ''}`} onChange={handleToggleFramed} defaultChecked={framed} disabled={(item?.notationClass === 'Basic' && field === 'fullSpecification') || field.includes('gdt')}>
            {`${i18n('entities.specialCharacters.modal.framed')}${frameTip ? ` - ${frameTip}` : ''}`}
          </Checkbox>
        </Form>
        <section className="character-bank-header">
          <h4 className="label">{i18n('entities.specialCharacters.modal.frequent')}</h4>
          <Popconfirm title={i18n('entities.specialCharacters.modal.resetConfirm')} icon={<QuestionCircleOutlined style={{ color: 'red' }} />} onConfirm={handleResetSymbols} trigger="click">
            <Button type="link" data-cy="reset-symbols-btn" className="ix-link-btn">
              {i18n('entities.specialCharacters.modal.resetSymbols')}
            </Button>
          </Popconfirm>
        </section>
        <section className="character-bank">
          <FrequentSymbols frequentSymbols={frequentSymbolsObj} handleInsertSymbol={handleInsertSymbol} />
        </section>
        {showSymbols && <SymbolsTabs handleInsertSymbol={handleInsertSymbol} feature={item} />}
        <section className="character-bank-footer">
          {showSymbols ? (
            <Button type="link" data-cy="hide-symbols-btn" className="ix-link-btn" onClick={() => handleShowSymbols(false)}>
              {i18n('entities.specialCharacters.modal.hideSymbols')}
            </Button>
          ) : (
            <Button type="link" data-cy="show-symbols-btn" className="ix-link-btn" onClick={() => handleShowSymbols(true)}>
              {i18n('entities.specialCharacters.modal.showSymbols')}
            </Button>
          )}
        </section>
      </Modal>
    </>
  );
};

SpecialCharactersModal.defaultProps = {
  value: '1.25 +/- 0.1',
  field: 'fullSpecification',
};

export default SpecialCharactersModal;
