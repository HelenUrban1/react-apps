import { Button } from 'antd';
import React from 'react';

export interface CharButtonProps {
  char: string;
  onClick: Function;
}

export const CharacterButton: React.FC<CharButtonProps> = ({ char, onClick }) => {
  return (
    <Button id={char} key={char} type="default" value={char} onClick={(e: any) => onClick(e, char)} className="gdt">
      {char}
    </Button>
  );
};
