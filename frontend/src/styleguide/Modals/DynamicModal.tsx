import React from 'react';
import { Button, Modal } from 'antd';
import './Modals.less';

type ModalState = {
  title: string;
  visibility: boolean;
  content: any[];
  footer?: any;
};

interface modalProps {
  modalState: ModalState;
  handleOpen: () => void;
  updateState: (newState: ModalState) => void;
  primary: boolean;
  buttonText: string;
  acceptLabel?: string;
  cancelLabel?: string;
  acceptAction?: Function;
  buttonClass?: string;
  extraClass?: string;
}

export const DynamicModal = ({ modalState, handleOpen, updateState, primary = true, buttonText = 'open', acceptLabel, cancelLabel, acceptAction, extraClass, buttonClass }: modalProps) => {
  const handleOk = () => {
    if (acceptAction) acceptAction();
    updateState({ ...modalState, visibility: false });
  };

  const handleClose = () => {
    updateState({ ...modalState, visibility: false });
  };

  return (
    <>
      <Button type={primary ? 'primary' : 'default'} className={primary ? '' : 'btn-secondary'} size="large" onClick={handleOpen} data-cy={extraClass}>
        {buttonText}
      </Button>
      <Modal
        destroyOnClose
        visible={modalState.visibility}
        title={modalState.title}
        onCancel={handleClose}
        className={`general-modal${extraClass ? ` ${extraClass}` : ''}`}
        okText={acceptLabel}
        onOk={handleOk}
        cancelText={cancelLabel}
        centered
        footer={
          modalState.footer || (
            <>
              <Button size="large" onClick={handleClose}>
                {cancelLabel}
              </Button>
              <Button type="primary" size="large" onClick={handleOk}>
                {acceptLabel}
              </Button>
            </>
          )
        }
      >
        <div className="modal-body">
          {modalState.content.map((col, index) => {
            return (
              <section key={`modal-section-${index}`} className="modal-section" style={{ marginRight: `${index + 1 !== modalState.content.length ? '24px' : '0px'}` }}>
                {col}
              </section>
            );
          })}
        </div>
      </Modal>
    </>
  );
};

DynamicModal.defaultProps = {
  acceptLabel: 'Accept',
  cancelLabel: 'Cancel',
  acceptAction: () => {},
  extraClass: 'dynamic-modal-btn',
  buttonClass: undefined,
};
