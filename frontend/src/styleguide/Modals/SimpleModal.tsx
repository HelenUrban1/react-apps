import React, { useState } from 'react';
import { Modal, Button } from 'antd';
import './Modals.less';

interface modalProps {
  buttonText: string;
  title: string;
  body: any;
  okText: string;
  cancelText: string;
  primary?: boolean;
  warning?: boolean;
  onAccept?: () => void;
  onCancel?: () => void;
  extraClass?: string;
  disabled?: boolean;
}

export const SimpleModal = ({ buttonText, title, body, okText, cancelText, primary, warning, onAccept, onCancel, extraClass, disabled }: modalProps) => {
  const [visible, setVisible] = useState(false);

  const handleAccept = () => {
    setVisible(false);

    if (onAccept) onAccept();
  };

  const handleClose = () => {
    setVisible(false);

    if (onCancel) onCancel();
  };

  return (
    <>
      <Button
        className={primary ? `btn-primary${warning ? ' btn-danger' : ''}` : `${warning ? 'btn-primary btn-danger' : 'btn-secondary'}`}
        size="large"
        onClick={() => {
          setVisible(true);
        }}
        data-cy={extraClass}
      >
        {buttonText}
      </Button>
      <Modal
        onCancel={handleClose}
        onOk={handleAccept}
        visible={visible}
        title={title}
        className={`general-modal${extraClass ? ` ${extraClass}` : ''}`}
        footer={
          <div className="custom-modal-footer">
            <Button size="large" onClick={handleClose}>
              {cancelText}
            </Button>
            <Button disabled={disabled} size="large" onClick={handleAccept} className={`btn-primary${warning ? ' btn-danger' : ''}`}>
              {okText}
            </Button>
          </div>
        }
      >
        {body}
      </Modal>
    </>
  );
};
