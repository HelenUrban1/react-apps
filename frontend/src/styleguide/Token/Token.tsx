import React, { useState } from 'react';
import { Tooltip } from 'antd';
import RemoveOptionButton from 'styleguide/Buttons/RemoveOption';
import { TokenType } from 'types/reportTemplates';
import './Token.less';

interface Props {
  token: TokenType;
  inUse: boolean;
  removeable: boolean;
  handleRemove?: (e: any) => void;
  item?: any;
  handleOnSelect?: (e: any, token: TokenType) => void;
  inTree: boolean;
  usedTokens: string[];
  remove?: (e: any) => void;
}

const Token = ({ token, inUse, removeable, handleRemove, item, handleOnSelect, inTree, usedTokens, remove }: Props) => {
  const [drag, setDrag] = useState(false);

  const handleDragStart = (e: any) => {
    setDrag(true);
    const tokenString = JSON.stringify({
      ...token,
    });

    e.dataTransfer.setData('text/plain', tokenString);
  };

  const getStyle = () => {
    if (/* usedTokens.includes(token.token) || */ drag) {
      return { opacity: 0.5, borderBottom: 'none', borderRight: 'none' };
    }
    return {};
  };

  const handleDragEnd = () => {
    setDrag(false);
  };

  return (
    <div className="token-row">
      <Tooltip title={`ex. "${token.example}"`} placement="right" overlayClassName={`${drag || !inTree ? 'hide' : ''}`}>
        <div
          className={`token ${token.type}${drag ? ' dragging' : ''}${inUse ? ' used' : ''}`}
          style={getStyle()}
          draggable
          onDragStart={(e) => handleDragStart(e)}
          onClick={(e) => {
            if (handleOnSelect) {
              handleOnSelect(e, token);
            }
          }}
          onDragEnd={handleDragEnd}
          data-cy={`token-${token.token}`}
        >
          <div className="token-handle">
            <svg viewBox="0 0 24 26">
              <circle cx="2.5" cy="2.5" r="2.5" />
              <circle cx="2.5" cy="12.5" r="2.5" />
              <circle cx="2.5" cy="22.5" r="2.5" />
              <circle cx="12.5" cy="2.5" r="2.5" />
              <circle cx="12.5" cy="12.5" r="2.5" />
              <circle cx="12.5" cy="22.5" r="2.5" />
            </svg>
          </div>
          <span>{token.name}</span>
          {removeable && (
            <span
              className="remove"
              onClick={() => {
                if (!handleRemove || !item) return;
                handleRemove(item);
              }}
            >
              X
            </span>
          )}
        </div>
      </Tooltip>
      {inTree && token.group === 'Custom' && remove && <RemoveOptionButton remove={remove} />}
    </div>
  );
};

Token.defaultProps = {
  name: 'Part Name',
  token: '{{Part.Name}}',
  type: 'Static',
  example: 'IX Multi Tool',
  inUse: false,
  removeable: false,
  handleRemove: () => console.log('remove'),
  inTree: false,
  usedTokens: [],
};

export default Token;
