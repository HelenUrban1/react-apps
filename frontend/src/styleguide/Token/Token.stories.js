/* eslint-disable react/jsx-filename-extension */
import React from 'react';
import { storiesOf } from '@storybook/react';
import Token from './Token';
import './Token.less';

const stories = storiesOf('Design System/Token', module);

stories.add('Data Tokens', () => {
  const story = (
    <>
      <div style={{ padding: '16px' }}>
        Single Value
        <Token name="Part Name" token="{{Part.Name}}" type="Single" example="IX_MULTI_TOOL" inUse={false} removeable={false} inTree usedTokens={[]} />
      </div>
      <div style={{ padding: '16px' }}>
        Single Value in Use
        <Token name="Part Number" token="{{Part.Number}}" type="Single" example="IX_MULTI_TOOL" inUse removeable={false} inTree usedTokens={[]} />
      </div>
      <div style={{ padding: '16px' }}>
        Multi Value
        <Token name="Plus Tol" token="{{Char.Plus}}" type="Dynamic" example="0.01" inUse={false} removeable={false} inTree usedTokens={[]} />
      </div>
      <div style={{ padding: '16px' }}>
        Multi Value in Use
        <Token name="Plus Tol" token="{{Char.Plus}}" type="Dynamic" example="0.01" inUse removeable={false} inTree usedTokens={[]} />
      </div>
    </>
  );

  return story;
});
