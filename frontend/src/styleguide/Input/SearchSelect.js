import React from 'react';
import { Select } from 'antd';

const { Option } = Select;

function onChange(value) {
  return;
}

function onBlur() {
  return;
}

function onFocus() {
  return;
}

function onSearch(val) {
  return;
}

function onDropdownVisibleChange() {
  return;
}

/** Currently Loads slowly when the list is large, AntD 4.0 fixes this issue */

class SearchSelect extends React.Component {
  constructor(props) {
    super(props);
    this.state = { isHover: false };
  }

  render() {
    const { style, defaultValue, showSearch, placeholder, optionFilterProp, onChange, onFocus, onBlur, onSearch, options, onDropdownVisibleChange } = this.props;

    return (
      <Select
        showSearch={showSearch}
        defaultValue={defaultValue}
        placeholder={placeholder}
        optionFilterProp={optionFilterProp}
        onChange={onChange}
        onFocus={onFocus}
        onBlur={onBlur}
        onSearch={onSearch}
        style={style}
        onDropdownVisibleChange={onDropdownVisibleChange}
        filterOption={(input, option) => option.props.children.toString().toLowerCase().indexOf(input.toLowerCase()) >= 0}
        dropdownStyle={{ width: '64px', minWidth: '64px' }}
        dropdownMenuStyle={{ width: '64px', minWidth: '64px' }}
        key={defaultValue}
      >
        {options.map((opt) => {
          return (
            <Option key={opt} value={opt}>
              {opt}
            </Option>
          );
        })}
      </Select>
    );
  }
}

SearchSelect.defaultProps = {
  defaultValue: 1,
  showSearch: true,
  placeholder: 'Item#',
  optionFilterProp: 'children',
  onChange: onChange,
  onFocus: onFocus,
  onBlur: onBlur,
  onSearch: onSearch,
  onDropdownVisibleChange: onDropdownVisibleChange,
  options: [1, 2, 3, 4, 5, 6, 7, 8, 9, 9.5, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 1000],
  style: {},
};

export default SearchSelect;
