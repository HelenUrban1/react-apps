import React, { Fragment } from 'react';
import { storiesOf } from '@storybook/react';
import { boolean, select, text, withKnobs } from '@storybook/addon-knobs';
import SearchSelect from './SearchSelect';

const stories = storiesOf('Design System/Inputs', module).addDecorator(withKnobs);

stories.add('Search Select', () => {
  const story = (
    <Fragment>
      <SearchSelect></SearchSelect>
    </Fragment>
  );

  return story;
});
