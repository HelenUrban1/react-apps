import React from 'react';
import { HomeOutlined } from '@ant-design/icons';
import { Layout, Menu } from 'antd';
import PropTypes from 'prop-types';
import './IxSider.less';

const { Sider } = Layout;

class IxSider extends React.Component {
  render() {
    return (
      <Sider
        style={{
          height: '100vh',
          position: 'sticky',
          top: 0,
        }}
        id="SideNavMenu"
        defaultCollapsed={true}
        collapsible
        onCollapse={this.props.onCollapse}
      >
        {this.props.menu}
        {this.props.hasDrawer && this.props.drawer}
      </Sider>
    );
  }
}

IxSider.propTypes = {
  /**
   * Menu Items for the Navigation Bar
   */
  menuItems: PropTypes.array,

  /**
   * Whether the Sider has an active Drawer component
   */
  hasDrawer: PropTypes.bool,

  /**
   * Drawer Component
   */
  drawer: PropTypes.any,

  /**
   * Menu to put in Sider
   */
  menu: PropTypes.any,

  /**
   * Function on Collapse
   */
  onCollapse: PropTypes.func,
};

IxSider.defaultProps = {
  /**
   * Example Menu
   */
  menu: [
    <Menu>
      <Menu.Item>
        <HomeOutlined />
        <span className="nav-text">InspectionXpert</span>
      </Menu.Item>
    </Menu>,
  ],

  /**
   * No Drawer by Default
   */
  hasDrawer: false,

  /**
   * Function to call when Sider Collapses
   */
  onCollapse: () => {
    console.log('Toggle Collapse');
  },
};

export default IxSider;
