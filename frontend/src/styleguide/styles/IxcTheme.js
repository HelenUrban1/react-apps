const IxcTheme = {
  colors: {
    primaryColor: '#1B838B', // Helix Teal-80
    secondaryColor: '#F6F8FA', // IXC Secondary Color
    tertiaryColor: '#333F48', // IXC Tertiary Color (selected btns)
    linkColor: '#1890ff', // link color
    successColor: '#52c41a', // success state color
    warningColor: '#faad14', // warning state color
    errorColor: '#f5222d', // error state color
    noteColor: '#fffae5', // color for feature notes
    noteColorDark: '#E5B900', // color for darker notes (noteColor darken 50%)
    fontSizeBase: '14px', // major text font size
    headingColor: '#963CBD', // heading text color
    textColor: 'rgba(0, 0, 0, 0.65)', // major text color
    textColorSecondary: 'rgba(0, 0, 0, 0.45)', // secondary text color
    disabledColor: '#A1A1A1', // disable state color
    disabledTextColor: '#606060', // disable text color
    borderRadiusBase: '2px', // major border radius
    borderColorBase: '#606060', // major border color
    boxShadowBase: '0 2px 8px rgba(0, 0, 0, 0.15)', // major shadow for layers
    xpertM2: '#F2E7F7', // Xpert Purple Minus 2
    xpertM1: '#C08AD7', // Xpert Purple minus 1
    xpert: '#963CBD', // Xpert Purple
    xpertP1: '#78229D',
    xpertP2: '#55196F', // Xpert Purple plus 2
    smokeM1: '#F6F8FA', // Smoke minus 1
    smoke: '#DDE5ED', // Smoke
    smokeP1: '#B7C3D5', // Smoke plus 1
    blackberryM1: '#755C9C', // Blackberry minus 1
    blackberry: '#5C4580', // Blackberry
    blackberryP1: '#3B2959', // Blackberry plus 1
    coralM1: '#F5C3BB', // Coral minus 1
    coral: '#E56A54', // Coral
    coralP1: '#E04F39', // Coral plus 1
    sunshineM2: '#FDF7EF', // Sunshine minus 2
    sunshineM1: '#F5D8B1', // Sunshine minus 1
    sunshine: '#EFBE7D', // Sunshine
    sunshineP1: '#E99C4F', // Sunshine plus 1
    sunshineP2: '#E47113', // Sunshine plus 2
    slate: '#333F48', // Slate
    slateM1: '#515F69', // Slate minus 1
    slateM2: '#687783', // Slate minus 2
    slateM3: '#889299', // Slate minus 3
    slateM4: '#BFC7CD', // Slate minus 4
    royalM4: 'E8E6F7', // Royal minus 4
    royalM3: '8B84D7', // Royal minus 3
    royalM2: '685BC7', // Royal minus 2
    royalM1: '2E008B', // Rotyal minus 1
    royal: '201547', // Royal
    azure: '1682ff', // Azure
    blue: '#2647DA', // Blue
  },
  buttons: {
    actionSize: '30px',
    borderRadius: '5px',
    selectedInsetColor: '#0d347c',
    hoverShadow: '0 3px 5px rgba(0,0,0,0.3)',
    innerShadow: 'inset 0px 0px 10px #000',
    iconBtnColor: '#606060',
    iconBtnHoverColor: '#000000',
  },
  progress: {
    stepperColor: '#1B838B', // Helix Teal-80
  },
};

export default IxcTheme;
