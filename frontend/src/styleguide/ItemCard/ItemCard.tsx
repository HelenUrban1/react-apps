import React, { useState, ChangeEvent, MouseEvent, useEffect } from 'react';
import { FlagOutlined, WarningOutlined, Loading3QuartersOutlined } from '@ant-design/icons';
import { Card, Popover, Select, Tooltip } from 'antd';
import { SymbolButton } from 'styleguide/Buttons/SymbolButton';
import { BrandColors, defaultTypeIDs } from 'view/global/defaults';
import { Characteristic } from 'types/characteristics';
import log from 'modules/shared/logger';
import { i18n } from 'i18n';
import ExpandSingle from 'styleguide/Popovers/ExpandSingle';
import ExpandGroup from 'styleguide/Popovers/ExpandGroup';
import { getFramedText } from 'utils/textOperations';
import IxcTheme from 'styleguide/styles/IxcTheme';
import Badge from '../Badge/Badge';
import { EditTextInPlace } from '../EditInPlace/EditInPlace';
import './ItemCard.less';
import { ErrorPopover } from './ErrorPopover';

const { Meta } = Card;

interface CustomEvent {
  target: {
    name: string;
    value: string;
    item: Characteristic;
  };
}

interface Props {
  item: Characteristic;
  collapsed: boolean;
  active: boolean;
  verified: boolean;
  itemWidth: number;
  maxNumber: number;
  expand: (item: Characteristic, shared: boolean) => void;
  expandGroup: (item: Characteristic, shared: boolean) => void;
  onBlur: (event: ChangeEvent<HTMLInputElement>) => void;
  onTextBlur: (e: CustomEvent) => void;
  onDropChange: (event: ChangeEvent<HTMLSelectElement> | number | string, item: Characteristic) => void;
  onDropBlur: (event: ChangeEvent<HTMLSelectElement> | number | string, item: Characteristic) => void;
  onClick: (event: MouseEvent<any>, item: Characteristic) => void;
  label: string;
  name: string;
  editItem: (record: Characteristic) => void;
  marker: any;
  defaultMarker: any;
  onDropdownVisibleChange: Function;
  dismissError: Function;
  deleteFeature: Function;
  handleShowModal: (feature: Characteristic, field: string, cursor: number, value: string | undefined) => void;
  loaded: boolean;
}

// Default Balloon Style
const def = (
  <svg width="24" id="coral-circle-solid" height="24" viewBox="-2 -2 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
    <ellipse cx="13" cy="13" rx="13" ry="13" stroke={BrandColors.Coral} strokeWidth={2} fill={BrandColors.Coral} />
  </svg>
);
const ItemCard: React.FC<Props> = ({ expand, item, collapsed, marker, onClick, onTextBlur, onDropChange, active, maxNumber, label, name, expandGroup, handleShowModal, loaded }) => {
  const [defaultValue, setDefaultValue] = useState<string>(item.notationClass === 'Basic' ? getFramedText(item.fullSpecification) : item.fullSpecification);
  const [errorVisible, setErrorVisible] = useState(false);
  const [qtyVisible, setQtyVisible] = useState(false);
  const className = `${active ? 'ant-card-active' : ''} ${collapsed ? ' ant-card-collapsed' : ''}`;
  const captureError = item?.captureError ? JSON.parse(item.captureError) : null;

  useEffect(() => {
    setDefaultValue(item.notationClass === 'Basic' ? getFramedText(item.fullSpecification) : item.fullSpecification);
  }, [item]);

  const getStyle = (markerObj: SVGElement | undefined, markerLabel: string | undefined) => {
    const arr = [];

    for (let i = 1; i <= maxNumber; i++) {
      arr.push(i);
    }

    return (
      <div className="feature-info">
        {!(collapsed && active) && (!collapsed || item.markerLabel.replace('.', '').length < 5) && (!active || item.markerLabel.replace('.', '').length < 5) && <div className="balloon-style">{!markerObj ? def : markerObj}</div>}
        <div className={`balloon-number${collapsed ? ' balloon-number-collapsed' : ''}`} key={markerLabel}>
          {active ? (
            <Select defaultValue={item?.markerLabel} showArrow={false} onSelect={(e: any) => onDropChange(e, item)} placeholder={item.markerGroup} dropdownMatchSelectWidth={false} showSearch>
              {arr.map((num: number) => (
                <Select.Option title={num.toString()} value={num} key={`${item.id} ${num}`}>
                  {num}
                </Select.Option>
              ))}
            </Select>
          ) : (
            markerLabel
          )}
        </div>
      </div>
    );
  };

  const handleChange = () => {
    // setDefaultValue(item.notationClass === 'Basic' ? getFramedText(e.target.value) : e.target.value);
    // this is causing loss of focus
  };

  const handleClick = (e: any) => {
    if (!loaded && document.activeElement) {
      // user was typing in full spec, need to blur before switching
      (document.activeElement as HTMLTextAreaElement).blur();
    }
    onClick(e, item);
    if (e?.target?.tagName === 'SPAN' && item.notationType === defaultTypeIDs['Geometric Tolerance']) {
      const primary = document.getElementById('gdtPrimaryToleranceZone-input');
      if (!primary) {
        return;
      }
      const inputs = primary?.getElementsByTagName('textarea');
      if (inputs[0]) {
        inputs[0].focus();
      }
    }
  };

  const expandable = item?.quantity && item.quantity > 1 && !collapsed;
  const grouped = item?.markerSubIndex !== null && item.markerSubIndex >= 0;
  const canEdit = !item.verified && active && item?.notationType !== defaultTypeIDs['Geometric Tolerance'];
  let toolTipText = '';
  if (active && item?.notationType === defaultTypeIDs['Geometric Tolerance']) {
    toolTipText = i18n('entities.part.instructions.gdtList');
  }
  if (active && item.verified) {
    toolTipText = i18n('entities.feature.reviewStatus.locked');
  }

  const showModal = (field: string, cursorPos: number) => {
    const value = document.getElementById(`${item.id}-card-input`) ? (document.getElementById(`${item.id}-card-input`) as HTMLInputElement).value : item.fullSpecification;
    handleShowModal(item, field, cursorPos, value?.toString());
  };

  const getClassType = () => {
    if (item.captureError) return '';

    switch (item.notationType) {
      case defaultTypeIDs.Note:
        return 'note';
      case defaultTypeIDs.Dimension:
        return 'dimension';
      case defaultTypeIDs['Geometric Tolerance']:
        return 'gdt';
      case defaultTypeIDs.Other:
      default:
        return 'other';
    }
  };

  const handleQtyVisibility = (visible: boolean) => {
    setQtyVisible(visible);
  };

  const handleErrorVisibility = (visible: boolean) => {
    setErrorVisible(visible);
  };

  return (
    <Card //
      data-cy={`feature-card-${item.markerLabel.replace('.', '-')}`}
      hoverable
      bordered={false}
      className={`${className} ${getClassType()} ${item.captureError ? 'error' : ''}`}
      onClick={handleClick}
    >
      <Meta
        avatar={getStyle(marker, item.markerLabel)}
        key={`spec-${defaultValue}`}
        title={
          !collapsed && (
            <>
              {canEdit ? (
                <div className="full-spec-container" data-cy={`feature-card-spec-${item.markerLabel.replace('.', '-')}`}>
                  <EditTextInPlace
                    label={label}
                    name={name}
                    id={`${item.id}-card-input`}
                    defaultValue={defaultValue}
                    key={item?.fullSpecification}
                    onBlur={(e) => onTextBlur({ target: { name: label, value: e.target.value, item } })} // (item, e, undefined)}
                    onChange={handleChange}
                    modal={<SymbolButton showModal={showModal} field="fullSpecification" />}
                  />
                </div>
              ) : (
                <Tooltip //
                  placement="top"
                  title={toolTipText}
                >
                  <span className="full-spec-static">{defaultValue}</span>
                </Tooltip>
              )}
              <section className="item-card-actions">
                {expandable && !grouped && (
                  <div className={`quantity-tag ${item.quantity > 99 ? 'large' : 'small'}`} data-cy={`feature-card-qty-${item.markerLabel.replace('.', '-')}`}>
                    <Popover placement="right" content={<ExpandSingle item={item} expand={expand} />} title={i18n('entities.feature.expand.action', item.quantity)} trigger="click" onVisibleChange={handleQtyVisibility}>
                      <Badge tip={qtyVisible ? '' : i18n('entities.feature.expand.title')} tipDirection="right" count={item.quantity} mini={collapsed} type="quantity" color="#963CBD">
                        {item.quantity}
                      </Badge>
                    </Popover>
                  </div>
                )}
                {expandable && grouped && (
                  <div className={`quantity-tag ${item.quantity > 99 ? 'large' : 'small'}`} data-cy={`feature-card-qty-${item.markerLabel.replace('.', '-')}`}>
                    <Popover placement="right" content={<ExpandGroup item={item} expand={expandGroup} />} title={i18n('entities.feature.expand.groupAction', item.quantity)} trigger="click" onVisibleChange={handleQtyVisibility}>
                      <Badge tip={qtyVisible ? '' : i18n('entities.feature.expand.group')} tipDirection="right" count={item.quantity} mini={collapsed} type="quantity" color="#963CBD">
                        {item.quantity}
                      </Badge>
                    </Popover>
                  </div>
                )}
                {captureError && !collapsed && (
                  <div className="error-tag">
                    <Tooltip
                      placement="right"
                      title={
                        errorVisible
                          ? undefined
                          : `${captureError.message === 'Saved as a Note' ? i18n('entities.feature.errors.tooltips.verify') : i18n('entities.feature.errors.tooltips.error')} ${
                              captureError.message === 'Saved as a Note' ? '' : i18n(`entities.feature.errors.messages['${captureError?.message || 'generic'}']`)
                            }`
                      }
                    >
                      <Popover
                        overlayStyle={{ maxWidth: '315px' }}
                        title={<span data-cy="error-popover-title">{i18n(`entities.feature.errors.titles['${captureError?.message || 'generic'}']`)}</span>}
                        content={<ErrorPopover message={captureError.message} />}
                        trigger="click"
                        placement="right"
                        onVisibleChange={handleErrorVisibility}
                      >
                        <WarningOutlined style={{ color: BrandColors.Coral }} />
                      </Popover>
                    </Tooltip>
                  </div>
                )}
                {defaultValue?.length === 0 && item?.captureError === null && item.captureMethod !== 'Custom' && !collapsed && (
                  <div className="loadingstate-tag">
                    <Loading3QuartersOutlined spin style={{ color: '#2647DA' }} data-testid="loading-state" />
                  </div>
                )}
              </section>
            </>
          )
        }
      />
      <section className="identifiers">
        {item.captureMethod === 'Custom' && (
          <Tooltip title="Custom Feature">
            <FlagOutlined style={{ color: `#${IxcTheme.colors.azure}` }} className="identifier-icon" />
          </Tooltip>
        )}
      </section>
    </Card>
  );
};

ItemCard.defaultProps = {
  collapsed: false,
  active: false,
  itemWidth: 3,
  maxNumber: 100,
  onTextBlur: (e: CustomEvent) => {
    log.debug(e);
  },
  onDropChange: (event: ChangeEvent<HTMLSelectElement> | number | string, item: Characteristic) => {
    log.debug(event, item);
  },
  onDropBlur: (event: ChangeEvent<HTMLSelectElement> | number | string, item: Characteristic) => {
    log.debug(event, item);
  },
  onClick: (event: MouseEvent<any>, item: Characteristic) => {
    log.debug(event, item);
  },
  label: '',
  name: '',
  marker: (
    <svg width="24" id="coral-circle-solid" height="24" viewBox="-2 -2 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
      <ellipse cx="13" cy="13" rx="13" ry="13" stroke={BrandColors.Coral} strokeWidth={2} fill={BrandColors.Coral} />
    </svg>
  ),
  defaultMarker: (
    <svg width="24" id="coral-circle-solid" height="24" viewBox="-2 -2 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
      <ellipse cx="13" cy="13" rx="13" ry="13" stroke={BrandColors.Coral} strokeWidth={2} fill={BrandColors.Coral} />
    </svg>
  ),
};

export default ItemCard;
