import React, { Fragment } from 'react';
import { defaultTypeIDs, defaultSubtypeIDs, defaultMethodIDs } from 'view/global/defaults';
import { storiesOf } from '@storybook/react';
import { withKnobs } from '@storybook/addon-knobs';
import { defaultUnitIDs } from 'view/global/defaults';
import ItemCard from './ItemCard';

const stories = storiesOf('Design System/ItemCard', module).addDecorator(withKnobs);

const fakeItem = {
  id: 'e0a01769-c8ed-4090-bd24-9c1b2241f123',
  drawingSheetIndex: 0,
  notationType: defaultTypeIDs.Dimension, // Dimension
  notationSubtype: defaultSubtypeIDs.Curvilinear, // Curvilinear
  notationClass: 'Tolerance',
  fullSpecification: '1.25 +/- 0.1',
  quantity: 1,
  nominal: '1.25',
  upperSpecLimit: '1.26',
  lowerSpecLimit: '1.24',
  plusTol: '0.1',
  minusTol: '0.1',
  unit: defaultUnitIDs.millimeter,
  criticality: false,
  inspectionMethod: defaultMethodIDs.Caliper, // Caliper
  notes: '',
  drawingRotation: 0.0,
  drawingScale: 1.0,
  boxLocationY: 20.5,
  boxLocationX: 20.5,
  boxWidth: 250,
  boxHeight: 100,
  boxRotation: 0.0,
  markerGroup: '1',
  markerIndex: 0,
  markerSubIndex: null,
  markerLabel: '1',
  markerStyle: 'aad19683-337a-4039-a910-98b0bdd5ee57',
  markerLocationX: 20,
  markerLocationY: 20,
  markerFontSize: 18,
  markerSize: 36,
  gridCoordinates: null,
  balloonGridCoordinates: null,
  connectionPointGridCoordinates: null,
  createdAt: '',
  updatedAt: '',
  partId: 'd6c5bc49-9fb2-4fbb-8a61-77bc8d28036f', // belongsTo
  drawingId: 'c200ce70-72b8-44f7-8d3f-4b93f6d1b491', // belongsTo
  drawingPageId: 'd6c5bc49-9fb2-4fbb-8a61-77bc8d380352',
};

stories.add('ItemCard', () => {
  const story = (
    <>
      <div style={{ height: '60px', width: '400px', marginTop: '8px' }}>
        <ItemCard active item={fakeItem} />
      </div>
      <div style={{ height: '60px', width: '400px', marginTop: '8px' }}>
        <ItemCard collapsed item={fakeItem} />
      </div>
      <div style={{ height: '60px', width: '400px', marginTop: '8px' }}>
        <ItemCard active collapsed item={fakeItem} />
      </div>
    </>
  );

  return story;
});
