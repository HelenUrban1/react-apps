import React from 'react';
import { render, fireEvent, screen } from '@testing-library/react';
import ItemCard from './ItemCard';
import { Part } from 'domain/part/partTypes';
import { Drawing } from 'graphql/drawing';
import { DrawingSheet } from 'graphql/drawing_sheet';
import { Characteristic } from 'types/characteristics';

const mockItem: Characteristic = {
  id: 'mockId',
  previousRevision: null,
  nextRevision: null,
  originalCharacteristic: null,
  drawingSheetIndex: 0,
  status: 'Active', // Provide appropriate mock values for each property
  captureMethod: 'Automated',
  captureError: 'null',
  notationType: 'mockNotationType',
  notationSubtype: 'mockNotationSubtype',
  notationClass: 'Basic',
  fullSpecification: '',
  quantity: 1,
  nominal: 'mockNominal',
  upperSpecLimit: 'mockUpperSpecLimit',
  lowerSpecLimit: 'mockLowerSpecLimit',
  plusTol: 'mockPlusTol',
  minusTol: 'mockMinusTol',
  unit: 'mm',
  criticality: null,
  inspectionMethod: null,
  notes: '',
  drawingRotation: 0,
  drawingScale: 1,
  boxLocationY: 0,
  boxLocationX: 0,
  boxWidth: 0,
  boxHeight: 0,
  boxRotation: 0,
  markerGroup: 'mockMarkerGroup',
  markerGroupShared: false,
  markerIndex: 0,
  markerSubIndex: 0,
  markerLabel: 'mockMarkerLabel',
  markerStyle: 'mockMarkerStyle',
  markerLocationX: 0,
  markerLocationY: 0,
  markerFontSize: 12,
  markerSize: 12,
  markerRotation: 0,
  gridCoordinates: '0,0',
  balloonGridCoordinates: '0,0',
  connectionPointGridCoordinates: '0,0',
  part: {} as Part, // You may need to provide mock objects for associated types
  drawing: {} as Drawing,
  drawingSheet: {} as DrawingSheet,
  verified: false,
  fullSpecificationFromOCR: '',
  confidence: 0,
  operation: null,
  toleranceSource: 'Default_Tolerance',
  reviewerTaskId: null,
  createdAt: new Date(),
  updatedAt: new Date(),
  deletedAt: new Date(),
  connectionPointLocationX: 0,
  connectionPointLocationY: 0,
  connectionPointIsFloating: false,
  displayLeaderLine: false,
  leaderLineDistance: 0,
};

const defaultProps = {
  item: mockItem,
  collapsed: false,
  active: true,
  loaded: false, // Indicate that the card is not fully loaded
  onTextBlur: jest.fn(),
  onDropChange: jest.fn(),
  onClick: jest.fn(),
  verified: false,
  itemWidth: 3,
  maxNumber: 100,
  expand: jest.fn(),
  expandGroup: jest.fn(),
  onBlur: jest.fn(),
  onDropBlur: jest.fn(),
  label: '',
  name: '',
  editItem: jest.fn(),
  marker: null,
  defaultMarker: null,
  onDropdownVisibleChange: jest.fn(),
  dismissError: jest.fn(),
  deleteFeature: jest.fn(),
  handleShowModal: jest.fn(),
};

describe('ItemCard Component', () => {
  describe('loadingstate-tag', () => {
    it('Should not render loading state when fullSpecification length is larger than 0', () => {
      const mockItemUpdated: Characteristic = { ...mockItem, fullSpecification: '5.2' };
      const defaultPropsUpdated = { ...defaultProps, item: mockItemUpdated };
      const { queryByTestId } = render(<ItemCard {...defaultPropsUpdated} />);
      expect(queryByTestId('loading-state')).not.toBeInTheDocument();
    });
    it('Should not render loading state if captureError is an empty string', () => {
      const mockItemUpdated: Characteristic = { ...mockItem, captureError: '' };
      const defaultPropsUpdated = { ...defaultProps, item: mockItemUpdated };
      const { queryByTestId } = render(<ItemCard {...defaultPropsUpdated} />);
      expect(queryByTestId('loading-state')).not.toBeInTheDocument();
    });
  });
});
