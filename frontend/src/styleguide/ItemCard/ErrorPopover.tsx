import { WarningOutlined } from '@ant-design/icons';
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { i18n } from 'i18n';
import { isEqual } from 'lodash';
import { renumberAfterDelete } from 'domain/part/edit/characteristics/utils/Renumber';
import { CharacteristicActions } from 'modules/characteristic/characteristicActions';
import { AppState } from 'modules/state';
import { IxButton } from 'styleguide';

interface ErrorProps {
  message: string;
}

export const ErrorPopover = ({ message }: ErrorProps) => {
  const { characteristics, selected } = useSelector((state: AppState) => state.characteristics, isEqual);
  const { part } = useSelector((state: AppState) => state.part);
  const dispatch = useDispatch();

  const deleteActive = () => {
    if (!part || !part.characteristics) return;
    const deletedIds: string[] = [];
    selected.forEach((char) => deletedIds.push(char.id));
    if (deletedIds.length <= 0) return;
    dispatch({
      type: CharacteristicActions.DELETE_CHARACTERISTICS,
      payload: {
        ids: deletedIds,
        part: part.id,
      },
    });
  };

  return (
    <div data-cy="error-popover">
      <section className="feature-error-description">
        <WarningOutlined style={{ color: 'red' }} />
        {i18n(`entities.feature.errors.messages['${message || 'generic'}']`)}
      </section>
      <div className="popoverFooter">
        <IxButton
          dataCy="error-popover-delete"
          onClick={() => {
            const updatedItem = { ...selected[0] };
            const res = renumberAfterDelete([updatedItem.id], characteristics);
            deleteActive();
            if (res.itemsToUpdate && res.itemsToUpdate.length > 0) {
              dispatch({
                type: CharacteristicActions.EDIT_CHARACTERISTICS,
                payload: {
                  updates: res.itemsToUpdate,
                },
              });
            }
            dispatch({
              type: CharacteristicActions.UPDATE_SELECTED,
              payload: {
                selected: [],
                count: res.newList.length,
                index: 0,
              },
            });
          }}
          text={i18n('errors.delete')}
          size="small"
          className="btn-secondary popoverButton"
        />
        <IxButton
          dataCy="error-popover-delete"
          onClick={() => {
            // only one will be selected
            const updatedItem = { ...selected[0] };
            updatedItem.captureError = '';
            dispatch({
              type: CharacteristicActions.EDIT_CHARACTERISTICS,
              payload: {
                updates: [updatedItem],
              },
            });
          }}
          text={message === 'Saved as a Note' ? i18n('errors.accept') : i18n('errors.dismiss')}
          size="small"
          className="btn-primary"
        />
      </div>
    </div>
  );
};
