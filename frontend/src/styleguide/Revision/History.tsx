import React from 'react';
import { useSelector } from 'react-redux';
import { Tooltip, Drawer } from 'antd';
import { RightOutlined } from '@ant-design/icons';
import { AppState } from 'modules/state';
import { RevisionCard } from './RevisionCard';
import './RevisionBanner.less';

type Props = {
  parent: string;
  historyShow: boolean;
  toggleDrawer: () => void;
};

const sortRevisions = (a: any, b: any) => {
  if (!a.createdAt && !b.createdAt) {
    return 0;
  }
  if (!a.createdAt) {
    return -1;
  }
  if (!b.createdAt) {
    return 1;
  }
  if (a.createdAt > b.createdAt) {
    return -1;
  }
  return 1;
};

export const RevisionHistory = ({ parent, historyShow, toggleDrawer }: Props) => {
  const { revisions } = useSelector((state: AppState) => state.part);

  const footer = (
    <section className="list-drawer-footer">
      <Tooltip title="Close Drawer">
        <RightOutlined onClick={toggleDrawer} />
      </Tooltip>
    </section>
  );

  return (
    <Drawer //
      data-cy="revision-history"
      footer={footer}
      footerStyle={{ height: '48px' }}
      getContainer={parent}
      visible={historyShow}
      zIndex={5}
      width={300}
      placement="right"
      className="revision-history"
      maskClosable
      onClose={toggleDrawer}
    >
      <h1>Revision History</h1>
      {Object.values(revisions).length > 0 &&
        Object.values(revisions)
          .sort(sortRevisions)
          .map((r) => (
            <div key={`container-${r.id}`}>
              <RevisionCard //
                key={r.id}
                id={r.id}
                type="part"
                name={r.name}
                number={r.number}
                revision={r.revision}
                createdAt={r.createdAt ? new Date(r.createdAt).toDateString() : 'unknown'}
                current={!r.nextRevision}
              />
              {Object.values(r.drawings).length > 0 &&
                Object.values(r.drawings)
                  .sort(sortRevisions)
                  .map((d) => (
                    <RevisionCard //
                      key={d.id}
                      id={d.id}
                      type="drawing"
                      name={d.name}
                      number={d.number}
                      revision={d.revision}
                      createdAt={d.createdAt ? new Date(d.createdAt).toDateString() : 'unknown'}
                      current={!d.nextRevision}
                    />
                  ))}
            </div>
          ))}
    </Drawer>
  );
};
