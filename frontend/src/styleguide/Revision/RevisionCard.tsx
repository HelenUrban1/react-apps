import React from 'react';
import { Tooltip } from 'antd';
import { BuildOutlined, FilePdfOutlined } from '@ant-design/icons';
import { i18n } from 'i18n';

type Props = {
  id: string;
  type: string;
  name?: string | null;
  number?: string | null;
  revision?: string | null;
  createdAt: string;
  current: boolean;
};

export const RevisionCard = ({ id, type, name, number, revision, createdAt, current }: Props) => (
  <div data-cy={id} className="revision-history-card">
    <div className="revision-icon">{type === 'part' ? <BuildOutlined /> : <FilePdfOutlined />}</div>
    <div>
      <h2>{name || number}</h2>
      <h3>{`${i18n('entities.revision.short')} ${revision || 'No Revision Label'}`}</h3>
      <p>
        <Tooltip title={current ? i18n('entities.revision.current') : i18n('entities.revision.previous')}>
          <span className={`card-status current-${current}`} data-cy="report-status" />
        </Tooltip>
        <span>
          {createdAt}
          {current && ` (${i18n('common.current')})`}
        </span>
      </p>
    </div>
  </div>
);

RevisionCard.defaultProps = {
  name: undefined,
  number: 'undefined',
  revision: i18n('entities.revision.none'),
};
