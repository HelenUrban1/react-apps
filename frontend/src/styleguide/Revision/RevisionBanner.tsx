import { NodeExpandOutlined } from '@ant-design/icons';
import { Button, Tooltip } from 'antd';
import { i18n } from 'i18n';
import { doLoadCurrentPartVersion } from 'modules/part/partActions';
import { AppState } from 'modules/state';
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import './RevisionBanner.less';

type Props = {
  previous: number;
  short?: boolean;
  type?: 'part' | 'drawing' | 'feature' | null;
};

export const RevisionBanner = ({ previous, short = false, type }: Props) => {
  const { partLoaded } = useSelector((state: AppState) => state.part);
  const dispatch = useDispatch();

  const getText = (t: any) => {
    if (short) {
      return i18n('revisions.oldShort', previous);
    }
    if (previous === 0) return i18n('revisions.currentFeature');
    if (previous < 0) return i18n('revisions.newFeature', Math.abs(previous));
    return t === 'feature' ? i18n('revisions.oldShort', previous) : i18n('revisions.old', type, previous);
  };

  return (
    <div data-cy="revision-banner" className="revision-banner">
      {getText(type)}
      {type !== 'feature' && (
        <Tooltip title={short ? i18n('revisions.goTo') : undefined} placement="top">
          <Button loading={!partLoaded} icon={<NodeExpandOutlined />} className="btn-secondary" onClick={() => dispatch(doLoadCurrentPartVersion())}>
            {short ? i18n('revisions.goToShort') : i18n('revisions.goTo')}
          </Button>
        </Tooltip>
      )}
    </div>
  );
};

RevisionBanner.defaultProps = {
  type: null,
  short: false,
};
