import React from 'react';
import { storiesOf } from '@storybook/react';
import { specs } from 'storybook-addon-specifications';
import { TitleBlockHeader } from './TitleBlockHeader';

const stories = storiesOf('Design System/Title Block', module);

stories.add('Default', () => {
  const story = (
    <>
      <div>
        <TitleBlockHeader name="Test Part Name" number="Test Part Number" revision="A" collapsed={false} />
      </div>
      <div>
        <TitleBlockHeader name="Test Part Name" number="Test Part Number" revision="A" collapsed />
      </div>
    </>
  );

  return story;
});
