import React from 'react';
import './TitleBlockHeader.less';
import { InfoCircleOutlined } from '@ant-design/icons';
import { Tooltip } from 'antd';
import { styles } from 'styleguide/AntDesignCustomTheme';

interface Props {
  name: string;
  number: string;
  revision: string;
  collapsed: boolean;
}

export const TitleBlockHeader: React.FC<Props> = ({ name, number, revision, collapsed }) => {
  return collapsed ? (
    <Tooltip
      arrowPointAtCenter={true}
      placement="topLeft"
      title={
        <div>
          {name}
          <br />
          {number}
          <br />
          Revision: {revision}
        </div>
      }
    >
      <InfoCircleOutlined style={{ fontSize: 24, margin: '0 auto', color: styles.colors.slate }} />
    </Tooltip>
  ) : (
    <div className="title-block-header">
      <div className="title-block-row">
        <p className="title-left">{name}</p>
        <p className="title-right">Rev.</p>
      </div>
      <div className="title-block-row">
        <p className="title-left">{number}</p>
        <p className="title-right">{revision}</p>
      </div>
    </div>
  );
};

TitleBlockHeader.defaultProps = {
  /* Dummy Part Name */
  name: 'InspectionXpert Multi Tool',

  /* Dummy Part Number */
  number: '1283-THQOPDW-7853',

  /* Dummy Revision */
  revision: 'A1',

  /* Open by Default */
  collapsed: false,
};
