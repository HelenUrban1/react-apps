import { Popover } from 'antd';
import React, { useState } from 'react';
import { IxButton } from 'styleguide';
import { IxFilter } from './IxFilter';

interface IxFilterButtonProps {
  defaultFilters: any;
  handleFilterSubmit: (currentFilters: any) => void;
}

export const IxFilterButton = ({ defaultFilters, handleFilterSubmit }: IxFilterButtonProps) => {
  const [showFilter, setShowFilter] = useState(false);
  const [active, setActive] = useState(false);

  const checkActive = (currentFilters: any) => {
    handleFilterSubmit(currentFilters);
    let mismatched = false;
    Object.keys(currentFilters).forEach((filter) => {
      if (currentFilters[filter] !== defaultFilters[filter]) {
        setActive(true);
        mismatched = true;
      }
    });
    if (!mismatched) setActive(false);
    setShowFilter(false);
  };

  return (
    <Popover placement="bottomRight" title="Seats Filter" trigger="click" visible={showFilter} content={<IxFilter defaultFilters={defaultFilters} handleFilterSubmit={checkActive} />}>
      <IxButton dataCy={`ix-filter-btn-${active ? 'active' : 'inactive'}`} className={active ? 'btn-primary' : 'btn-secondary'} rightIcon="filter" onClick={(e: any) => setShowFilter(true)} />
    </Popover>
  );
};
