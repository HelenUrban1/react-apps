import React from 'react';
import { storiesOf } from '@storybook/react';
import { IxFilterButton } from './IxFilterButton';

const stories = storiesOf('Design System/Filter', module);

const defaultFilters = {
  A: false,
  B: false,
  C: false,
  D: false,
};

const handleFilterSubmit = (currentFilters) => {
  console.log(currentFilters);
};

stories.add('Filter Button', () => {
  const story = <IxFilterButton defaultFilters={defaultFilters} handleFilterSubmit={handleFilterSubmit} />;

  return story;
});
