import { Checkbox } from 'antd';
import React, { useRef } from 'react';
import { IxButton } from 'styleguide';

interface IxFilterProps {
  defaultFilters: any;
  handleFilterSubmit: (currentFilters: any) => void;
}

export const IxFilter = ({ defaultFilters, handleFilterSubmit }: IxFilterProps) => {
  const filters = useRef<any>({ ...defaultFilters });

  return (
    <div style={{ display: 'flex', flexDirection: 'column' }}>
      {Object.keys(filters.current).map((key: string) => {
        if (key !== 'Search') {
          return (
            <Checkbox
              defaultChecked={filters.current[key]}
              onChange={(e: any) => {
                filters.current[key] = e.target.checked;
              }}
              key={`${key}-checkbox`}
            >
              {key}
            </Checkbox>
          );
        }
        return null;
      })}
      <IxButton
        className="btn-primary"
        text="Apply"
        onClick={(e: any) => {
          handleFilterSubmit(filters.current);
        }}
      />
    </div>
  );
};
