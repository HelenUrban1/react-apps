import React, { ChangeEvent, useState } from 'react';
import { i18n } from 'i18n';
import { Select } from 'view/global/inputs';
import { Input, Modal } from 'antd';

type Props = {
  value?: string | null;
  name?: string;
  label?: string;
  orgs: { name: string; label: string; value: string }[];
  disabled?: boolean;
  onChange: (event: ChangeEvent<HTMLTextAreaElement | HTMLSelectElement>) => void;
  handleNewOrg: (org: string) => void;
};

export const Providers = ({ value = null, name = 'customer', label = i18n('entities.customer.singular'), orgs, disabled, onChange, handleNewOrg }: Props) => {
  // Track whether the modal is open or not
  const [displayModal, setDisplayModal] = useState(false);
  // Track the name of the organization being added
  const [newOrganizationName, setNewOrganizationName] = useState('');

  const setNewOrg = () => {
    handleNewOrg(newOrganizationName);
    setNewOrganizationName('');
    setDisplayModal(false);
  };

  const typeNewOrgName = (e: React.ChangeEvent<HTMLInputElement>) => {
    setNewOrganizationName(e.target.value);
  };

  const cancelNewOrg = () => {
    setDisplayModal(false);
    setNewOrganizationName('');
  };

  const triggerAdd = (organization: string) => {
    setNewOrganizationName(organization);
    setDisplayModal(true);
  };

  return (
    <div className="customer-select" data-cy="report-customers">
      <Select
        input={{
          name,
          label,
          value,
          type: 'select',
          group: orgs,
          disabled,
          change: onChange,
          search: true,
          add: triggerAdd,
        }}
      />
      <Modal title="Create Organization" visible={displayModal} onOk={setNewOrg} onCancel={cancelNewOrg}>
        <label htmlFor="newProvider">
          New Organization Name:
          <Input id="newProvider" value={newOrganizationName} onChange={typeNewOrgName} />
        </label>
      </Modal>
    </div>
  );
};

export default Providers;

Providers.defaultProps = {
  value: null,
  name: 'customer',
  label: i18n('entities.customer.singular'),
  disabled: false,
};
