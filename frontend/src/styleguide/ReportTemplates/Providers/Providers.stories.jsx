import React from 'react';
import { storiesOf } from '@storybook/react';
import { useState } from '@storybook/client-api';
import { Providers } from './Providers';

const stories = storiesOf('Reporting/Providers', module);

stories.add('Default', () => {
  const [val, setVal] = useState('a');
  const orgs = [
    {
      name: 'a',
      label: 'A',
      value: 'a',
    },
    {
      name: 'b',
      label: 'B',
      value: 'b',
    },
    {
      name: 'c',
      label: 'C',
      value: 'c',
    },
  ];
  const onChange = (event) => {
    setVal(event.target.value);
  };

  return (
    <>
      <Providers value={val} onChange={onChange} orgs={orgs} />
      <p>
        Your Customer is:
        {` ${val}`}
      </p>
    </>
  );
});
