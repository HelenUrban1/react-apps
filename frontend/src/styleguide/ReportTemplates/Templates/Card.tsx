import React from 'react';
import { i18n } from 'i18n';
import { Button, Card, message, Popconfirm, Tooltip } from 'antd';
import { CopyOutlined, DeleteOutlined, EditOutlined, PictureOutlined, WarningOutlined } from '@ant-design/icons';
import { ReportTemplate } from 'types/reportTemplates';
import { Organization } from 'graphql/organization';
import { FileObject, FileUploadDragger } from 'styleguide/File/Upload';
import './card.less';
import { IxAvatar } from 'styleguide/Avatar/IxAvatar';
import { useSelector } from 'react-redux';
import { AppState } from 'modules/state';

type Props = {
  template?: ReportTemplate;
  image?: string;
  onClick: (template?: any) => void;
  edit?: (template: ReportTemplate) => void;
  copy?: (template: ReportTemplate) => void;
  del?: (template: ReportTemplate) => void;
  enableReport: boolean;
  showReport?: boolean;
  report?: (template: ReportTemplate) => void;
};

type Title = {
  title: string;
  provider: string;
  report: boolean;
  enableReport: boolean;
  showReport?: boolean;
  handleReport: () => void;
};

type Thumbnail = {
  image?: string;
  provider?: Organization;
  title: string;
};

const ThumbnailImage: React.FC<Thumbnail> = ({ image, title, provider }) => (
  <div className="template-card-image" data-cy="template_card_image_container">
    {provider && provider.orgLogo && provider.orgLogo.length > 0 && <img data-cy="avatar" className="ix-logo-img" src={provider.orgLogo[0].publicUrl} alt={`${provider.name} Logo`} />}
    {(provider && !provider.orgLogo) || (provider?.orgLogo?.length === 0 && provider.name && <IxAvatar type="company" size="default" data={provider} />)}
    {image && <img data-cy="template_card_image" alt={`${title} Preview`} src={image} />}
    {!image && provider && <PictureOutlined />}
  </div>
);

const CardHeader: React.FC<Title> = ({ title, provider, report, enableReport, handleReport, showReport }) => (
  <>
    <div className="template-card-header" data-cy="template_card_header">
      <span className={`template-card-title${title.length > 30 ? ' text-shrink' : ''}`} data-cy="template_card_title">
        {title}
      </span>
      <span className="template-card-provider" data-cy="template_card_provider">
        {i18n('entities.reportTemplate.fields.provider')}
        {` ${provider}`}
      </span>
    </div>
    {report && enableReport && (
      <Button block type="primary" data-action="report" data-cy="template_card_report" className="generate-report" onClick={handleReport} disabled={!enableReport}>
        {i18n('entities.report.generate')}
      </Button>
    )}
    {report && !enableReport && showReport && (
      <Tooltip title={i18n('entities.report.needPart')}>
        <Button type="primary" data-action="report" data-cy="template_card_report" className="generate-report" onClick={handleReport} disabled={!enableReport}>
          {i18n('entities.report.generate')}
        </Button>
      </Tooltip>
    )}
  </>
);

export const TemplateCard: React.FC<Props> = ({ template, image, onClick, edit, copy, del, enableReport, report, showReport }) => {
  const { template: selectedTemplate } = useSelector((state: AppState) => state.reportTemplates);
  const handleUpload = (uploads: { [key: string]: FileObject }) => {
    onClick(Object.values(uploads));
  };

  const setErrorMessages = (errors: string[] | undefined) => {
    if (errors) {
      errors.forEach((err) => {
        message.error(err, 5);
      });
    }
  };

  if (!template) {
    return (
      <Card //
        data-cy="template_new"
        className="template-card template-add"
        hoverable={false}
        bordered={false}
        title={i18n('entities.template.manager.new')}
        cover={
          <FileUploadDragger
            fileType="excel"
            fileExt=".xlsx,.csv"
            hint=".xlsx or .csv"
            entity={i18n('entities.report.fields.template')}
            limit={1}
            files={{}}
            setFiles={handleUpload}
            setErrorMessages={setErrorMessages}
            classes="template-add"
            type="Report"
          />
        }
        bodyStyle={{ display: 'none' }}
      />
    );
  }
  const handleClick = (e?: React.MouseEvent) => {
    if (e?.target) {
      const target = e.target as HTMLElement;
      if (target.getAttribute('data-action')) {
        // Prevent onClick from firing for events
        return;
      }
    }
    onClick(template);
  };
  const handleEdit = (e?: React.MouseEvent) => {
    if (e) {
      e.stopPropagation();
    }
    if (!edit) {
      return;
    }
    edit(template);
  };
  const handleCopy = (e?: React.MouseEvent) => {
    if (e) {
      e.stopPropagation();
    }
    if (!copy) {
      return;
    }
    copy(template);
  };
  const handleDelete = (e?: React.MouseEvent) => {
    if (e) {
      e.stopPropagation();
    }
    if (!del) {
      return;
    }
    del(template);
  };
  const handleReport = (e?: React.MouseEvent) => {
    if (e) {
      e.stopPropagation();
    }
    if (!report) {
      return;
    }
    report(template);
  };
  const cancelPopup = (e?: React.MouseEvent) => {
    if (e) {
      e.stopPropagation();
    }
  };

  const getActions = () => {
    const actions = [];
    if (edit) {
      actions.push(
        <Tooltip title={i18n('common.edit')} key="edit">
          <EditOutlined data-action="edit" className="edit-action" data-cy="template_card_edit" onClick={handleEdit} />
        </Tooltip>,
      );
    }
    if (copy) {
      actions.push(
        <Tooltip title={i18n('common.copy')} key="copy">
          <CopyOutlined data-action="copy" className="copy-action" data-cy="template_card_copy" onClick={handleCopy} />
        </Tooltip>,
      );
    }
    if (del) {
      actions.push(
        <Tooltip title={i18n('common.delete')} key="delete">
          <Popconfirm //
            title={i18n('common.areYouSure')}
            onConfirm={handleDelete}
            onCancel={cancelPopup}
            icon={<WarningOutlined style={{ color: 'red' }} />}
            okText={i18n('common.delete')}
            cancelText={i18n('common.cancel')}
          >
            <DeleteOutlined data-action="delete" className="delete-action" data-cy="template_card_delete" />
          </Popconfirm>
        </Tooltip>,
      );
    }
    return actions;
  };

  return (
    <section className="template-card" data-cy={`template_card_${template.id}`}>
      <CardHeader //
        title={template.title}
        provider={template.provider.name}
        report={!!report}
        enableReport={enableReport}
        handleReport={handleReport}
        showReport={showReport}
      />
      <Card //
        className={`${selectedTemplate?.id === template.id ? ' template-card-selected' : ''}`}
        bordered={false}
        bodyStyle={{ display: 'none' }}
        cover={<ThumbnailImage image={image} title={template.title} provider={template.provider} />}
        actions={getActions()}
        hoverable
        onClick={handleClick}
      />
    </section>
  );
};

export default TemplateCard;
