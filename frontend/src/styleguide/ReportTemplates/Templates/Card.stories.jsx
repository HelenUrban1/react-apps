import React from 'react';
import { storiesOf } from '@storybook/react';
import { boolean, files, text, withKnobs } from '@storybook/addon-knobs';
import { TemplateCard } from './Card';

const stories = storiesOf('Reporting/Template Card', module).addDecorator(withKnobs);

const editFunc = (temp) => {
  alert(`Edit: ${temp.title}`);
};
const copyFunc = (temp) => {
  alert(`Copy: ${temp.title}`);
};
const delFunc = (temp) => {
  alert(`Delete: ${temp.title}`);
};
const clickFunc = (temp) => {
  alert(`onClick - Set Template: ${temp.title}`);
};
const uploadFunc = (file) => {
  alert(`Upload: ${file[0].name}`);
};
const reportFunc = (temp) => {
  alert(`Generate Report: ${temp.title}`);
};
stories
  .add('Template', () => {
    // Knobs
    const title = text('Title', 'Some Template Title', 'Template Info');
    const provider = text('Provider', 'Very Real Customer', 'Template Info');
    const hide = boolean('Hide Image', false, 'Template Info');
    const image = files('Image', 'image/*', [`${process.env.PUBLIC_URL}/templates/AS9102B/0.png`], 'Template Info');

    const edit = boolean('Show Edit', true, 'Actions');
    const copy = boolean('Show Copy', true, 'Actions');
    const del = boolean('Show Delete', true, 'Actions');

    const report = boolean('Show Report Button', true, 'Reporting');
    const canReport = boolean('Enable Report Button', true, 'Reporting');

    const emptyTemplate = {
      title,
      id: '',
      provider: {
        id: '',
        status: 'Active',
        type: 'Customer',
        name: provider,
      },
      status: 'Inactive',
      file: [],
      direction: 'Vertical',
    };

    return (
      <TemplateCard //
        template={emptyTemplate}
        image={!hide ? image[0] : undefined}
        onClick={clickFunc}
        edit={edit ? editFunc : undefined}
        copy={copy ? copyFunc : undefined}
        del={del ? delFunc : undefined}
        report={report ? reportFunc : undefined}
        enableReport={canReport}
      />
    );
  })
  .add('Upload', () => {
    return (
      <TemplateCard //
        template={undefined}
        onClick={uploadFunc}
        enableReport={false}
      />
    );
  });
