import React from 'react';
import { storiesOf } from '@storybook/react';
import { boolean, text, withKnobs } from '@storybook/addon-knobs';
import { useState } from '@storybook/addons';
import DataFilter from './DataFilter';

const stories = storiesOf('Reporting/Data Filters', module).addDecorator(withKnobs);

const features = [
  {
    id: 'be60664a-e87e-4be6-9f7c-0bae64b9ff2a',
    captureMethod: 'Automated',
    status: 'Active',
    verified: false,
    drawingSheetIndex: 1,
    notationType: 'db687ac1-7bf8-4ada-be2b-979f8921e1a0', // Dimension
    toleranceSource: 'Document_Defined',
    notationSubtype: 'edc8ecda-5f88-4b81-871f-6c02f1c50afd', // Length
    notationClass: 'Tolerance',
    criticality: '168eba1e-a8bf-41bb-a0d2-955e8a7b8d66',
    inspectionMethod: 'e18329cf-9875-4475-bee9-f40911dd2ada',
    operation: '38d096ec-8339-4ef5-abe3-640303b24bc4',
    fullSpecification: '3.39/3.36',
    quantity: 1,
    nominal: '3.39/3.36',
    upperSpecLimit: '3.39',
    lowerSpecLimit: '3.36',
    unit: '123',
    drawingScale: 0,
    drawingRotation: 0,
    boxLocationY: 143,
    boxLocationX: 377,
    boxWidth: 34,
    boxHeight: 34,
    boxRotation: 0,
    markerLocationY: 180,
    markerLocationX: 40,
    connectionPointLocationX: 377,
    connectionPointLocationY: 160,
    connectionPointIsFloating: true,
    markerStyle: 'f12c5566-7612-48e9-9534-ef90b276ebaa',
    markerGroup: '1',
    markerIndex: 0,
    markerLabel: '1',
    markerFontSize: 18,
    markerSize: 36,
    gridCoordinates: 'A2',
    balloonGridCoordinates: 'A2',
    connectionPointGridCoordinates: 'A2',
    partId: 'c48e082a-ed09-4885-ae15-e45c4f58b628',
    drawingId: 'bf76c82f-4c45-4bf7-be51-34c401c49d85',
    drawingSheetId: '4f53a001-9e39-4c7a-aaf0-d769d3c9a425',
  },
  {
    id: 'e36abada-2a27-40e0-9dd2-e58fea53bf02',
    captureMethod: 'Automated',
    toleranceSource: 'Document_Defined',
    status: 'Active',
    verified: false,
    drawingSheetIndex: 2,
    notationType: 'f3c4d8e0-fb79-4c72-a3f8-3f584ea075a7', // Geometric Tolerance
    notationSubtype: '19a7a9a1-76c5-45ef-8cf9-611ebe7cf207',
    notationClass: 'Tolerance',
    fullSpecification: '', // |<|1.25|A|B|C|
    criticality: '187b5423-dafd-402a-b471-2ed2fe76d2f9',
    inspectionMethod: '1526159c-180d-4d3a-946a-377082d2b6b2',
    operation: 'e7948e9c-c98c-4338-9a95-a4bd707dffe5',
    quantity: 1,
    nominal: '1.25',
    plusTol: '0.1',
    minusTol: '0.1',
    upperSpecLimit: '1.25',
    lowerSpecLimit: '',
    gdtSymbol: '',
    gdtPrimaryToleranceZone: '1.25',
    gdtSecondaryToleranceZone: '',
    gdtPrimaryDatum: 'A',
    gdtSecondaryDatum: 'B',
    gdtTertiaryDatum: 'C',
    unit: '123123123',
    drawingRotation: 0,
    drawingScale: 1,
    boxLocationY: 120,
    boxLocationX: 220,
    boxWidth: 250,
    boxHeight: 100,
    boxRotation: 0,
    markerIndex: 1,
    markerLabel: '2',
    markerGroup: '2',
    markerStyle: 'aad19683-337a-4039-a910-98b0bdd5ee57',
    markerLocationX: 190,
    markerLocationY: 100.5,
    connectionPointLocationX: 220,
    connectionPointLocationY: 170,
    markerFontSize: 18,
    markerSize: 36,
    gridCoordinates: 'B4',
    balloonGridCoordinates: 'B4',
    connectionPointGridCoordinates: 'B4',
    partId: 'c48e082a-ed09-4885-ae15-e45c4f58b628',
    drawingId: 'bf76c82f-4c45-4bf7-be51-34c401c49d85',
    drawingSheetId: '4f53a001-9e39-4c7a-aaf0-d769d3c9a425',
  },
  {
    id: '38c84a73-745a-428b-81f6-e638c01a61fa',
    captureMethod: 'Automated',
    toleranceSource: 'Document_Defined',
    status: 'Active',
    verified: true,
    drawingSheetIndex: 3,
    gridCoordinates: 'D12',
    balloonGridCoordinates: 'D12',
    connectionPointGridCoordinates: 'D12',
    notationType: '9f78f226-11b1-4e70-9705-440c0ef290bc',
    notationSubtype: '4e6617aa-5f61-4257-8e18-787b80ddece1',
    notationClass: 'Tolerance',
    criticality: '502a8ba4-82a7-423a-a339-b8f358ba3d3a', // Minor 502a8ba4-82a7-423a-a339-b8f358ba3d3a
    inspectionMethod: 'd52dffc8-59fa-42b3-8561-8bd9bdb6df73',
    operation: '04756ef3-affc-4b09-b1ce-ab901a75ce69',
    fullSpecification: 'This is a Note We Captured',
    quantity: 1,
    nominal: '',
    upperSpecLimit: '',
    lowerSpecLimit: '',
    plusTol: '',
    minusTol: '',
    unit: '',
    drawingRotation: 0,
    drawingScale: 1,
    boxLocationY: 120,
    boxLocationX: 220,
    boxWidth: 250,
    boxHeight: 100,
    boxRotation: 0,
    markerIndex: 2,
    markerLabel: '3',
    markerGroup: '3',
    markerStyle: 'aad19683-337a-4039-a910-98b0bdd5ee57',
    markerLocationX: 190,
    markerLocationY: 100.5,
    connectionPointLocationX: 220,
    connectionPointLocationY: 170,
    markerFontSize: 18,
    markerSize: 36,
    partId: 'c48e082a-ed09-4885-ae15-e45c4f58b628',
    drawingId: 'bf76c82f-4c45-4bf7-be51-34c401c49d85',
    drawingSheetId: '4f53a001-9e39-4c7a-aaf0-d769d3c9a425',
  },
];

const testTypes = {
  '9f78f226-11b1-4e70-9705-440c0ef290bc': {
    children: {
      '4e6617aa-5f61-4257-8e18-787b80ddece1': {
        default: true,
        id: '4e6617aa-5f61-4257-8e18-787b80ddece1',
        index: 3,
        listType: 'Type',
        meta: { measurement: 'Length' },
        status: 'Active',
        value: 'Structure',
        deleted: false,
      },
    },
    default: true,
    id: '9f78f226-11b1-4e70-9705-440c0ef290bc',
    index: 2,
    listType: 'Type',
    meta: undefined,
    status: 'Active',
    value: 'Surface Finish',
    deleted: false,
  },
  'db687ac1-7bf8-4ada-be2b-979f8921e1a0': {
    children: {
      'edc8ecda-5f88-4b81-871f-6c02f1c50afd': {
        default: true,
        id: 'edc8ecda-5f88-4b81-871f-6c02f1c50afd',
        index: 0,
        listType: 'Type',
        meta: { measurement: 'Length' },
        status: 'Active',
        value: 'Length',
        deleted: false,
      },
      'b2e1e0aa-0da4-4844-853a-f20971b013a4': {
        default: true,
        id: 'b2e1e0aa-0da4-4844-853a-f20971b013a4',
        index: 1,
        listType: 'Type',
        meta: { measurement: 'Plane Angle' },
        status: 'Active',
        value: 'Angle',
        deleted: false,
      },
      '49d45a6c-1073-4c6e-805f-8f4105641bf1': {
        default: true,
        id: '49d45a6c-1073-4c6e-805f-8f4105641bf1',
        index: 2,
        listType: 'Type',
        meta: { measurement: 'Length' },
        status: 'Active',
        value: 'Radius',
        deleted: false,
      },
      '4396f3c2-a915-4458-8cff-488b6d301ecd': {
        default: true,
        id: '4396f3c2-a915-4458-8cff-488b6d301ecd',
        index: 3,
        listType: 'Type',
        meta: { measurement: 'Length' },
        status: 'Active',
        value: 'Diameter',
        deleted: false,
      },
    },
    default: true,
    id: 'db687ac1-7bf8-4ada-be2b-979f8921e1a0',
    index: 0,
    listType: 'Type',
    meta: undefined,
    status: 'Active',
    value: 'Dimension',
    deleted: false,
  },
  'f3c4d8e0-fb79-4c72-a3f8-3f584ea075a7': {
    children: {
      '19a7a9a1-76c5-45ef-8cf9-611ebe7cf207': {
        default: true,
        id: '19a7a9a1-76c5-45ef-8cf9-611ebe7cf207',
        index: 1,
        listType: 'Type',
        meta: { measurement: 'Length' },
        status: 'Active',
        value: 'Position',
        deleted: false,
      },
    },
    default: true,
    id: 'f3c4d8e0-fb79-4c72-a3f8-3f584ea075a7',
    index: 1,
    listType: 'Type',
    meta: undefined,
    status: 'Active',
    value: 'Geometric Tolerance',
    deleted: false,
  },
  'fa3284ed-ad3e-43c9-b051-87bd2e95e927': {
    children: {
      '805fd539-8063-4158-8361-509e10a5d371': {
        default: true,
        id: '805fd539-8063-4158-8361-509e10a5d371',
        index: 0,
        listType: 'Type',
        meta: undefined,
        status: 'Active',
        value: 'Note',
        deleted: false,
      },
      'd3e276d7-18fa-47b7-8fc8-0656017dc5cc': {
        default: true,
        id: 'd3e276d7-18fa-47b7-8fc8-0656017dc5cc',
        index: 1,
        listType: 'Type',
        meta: undefined,
        status: 'Active',
        value: 'Flag Note',
        deleted: false,
      },
    },
    default: true,
    id: 'fa3284ed-ad3e-43c9-b051-87bd2e95e927',
    index: 4,
    listType: 'Type',
    meta: undefined,
    status: 'Active',
    value: 'Note',
    deleted: false,
  },
};

const testClassifications = {
  '975a513d-394c-4123-901e-1244756b488e': {
    children: {},
    default: true,
    id: '975a513d-394c-4123-901e-1244756b488e',
    index: 0,
    listType: 'Classification',
    meta: undefined,
    status: 'Active',
    value: 'Incidental',
    deleted: false,
  },
  '502a8ba4-82a7-423a-a339-b8f358ba3d3a': {
    children: {},
    default: true,
    id: '502a8ba4-82a7-423a-a339-b8f358ba3d3a',
    index: 1,
    listType: 'Classification',
    meta: undefined,
    status: 'Active',
    value: 'Minor',
    deleted: false,
  },
  '187b5423-dafd-402a-b471-2ed2fe76d2f9': {
    children: {},
    default: true,
    id: '187b5423-dafd-402a-b471-2ed2fe76d2f9',
    index: 2,
    listType: 'Classification',
    meta: undefined,
    status: 'Active',
    value: 'Major',
    deleted: false,
  },
  '168eba1e-a8bf-41bb-a0d2-955e8a7b8d66': {
    children: {},
    default: true,
    id: '168eba1e-a8bf-41bb-a0d2-955e8a7b8d66',
    index: 3,
    listType: 'Classification',
    meta: undefined,
    status: 'Active',
    value: 'Critical',
    deleted: false,
  },
};

const testMethods = {
  'd52dffc8-59fa-42b3-8561-8bd9bdb6df73': {
    children: {},
    default: true,
    id: 'd52dffc8-59fa-42b3-8561-8bd9bdb6df73',
    index: 0,
    listType: 'Inspection_Method',
    meta: undefined,
    status: 'Active',
    value: 'Caliper',
    deleted: false,
  },
  '1526159c-180d-4d3a-946a-377082d2b6b2': {
    children: {},
    default: true,
    id: '1526159c-180d-4d3a-946a-377082d2b6b2',
    index: 1,
    listType: 'Inspection_Method',
    meta: undefined,
    status: 'Active',
    value: 'Indicator',
    deleted: false,
  },
  'e18329cf-9875-4475-bee9-f40911dd2ada': {
    children: {},
    default: true,
    id: 'e18329cf-9875-4475-bee9-f40911dd2ada',
    index: 2,
    listType: 'Inspection_Method',
    meta: undefined,
    status: 'Active',
    value: 'CMM',
    deleted: false,
  },
};

const testOperations = {
  '04756ef3-affc-4b09-b1ce-ab901a75ce69': {
    children: {},
    default: true,
    id: '04756ef3-affc-4b09-b1ce-ab901a75ce69',
    index: 0,
    listType: 'Operation',
    meta: undefined,
    status: 'Active',
    value: 'Drilling',
    deleted: false,
  },
  'e7948e9c-c98c-4338-9a95-a4bd707dffe5': {
    children: {},
    default: true,
    id: 'e7948e9c-c98c-4338-9a95-a4bd707dffe5',
    index: 1,
    listType: 'Operation',
    meta: undefined,
    status: 'Active',
    value: 'Punching',
    deleted: false,
  },
  '38d096ec-8339-4ef5-abe3-640303b24bc4': {
    children: {},
    default: true,
    id: '38d096ec-8339-4ef5-abe3-640303b24bc4',
    index: 2,
    listType: 'Operation',
    meta: undefined,
    status: 'Active',
    value: 'Welding',
    deleted: false,
  },
};

stories.add('Default', () => {
  const [filters, setFilters] = useState({ 1: { category: ['Features', 'Number'], value: undefined } });
  const disabled = boolean('Disabled', false, 'Props');
  const options = [
    {
      value: 'Features',
      label: 'Features',
      children: [
        {
          value: 'Notation Type',
          label: 'Type',
        },
        {
          value: 'Notation Sub Type',
          label: 'SubType',
        },
        {
          value: 'Number',
          label: 'Number',
        },
      ],
    },
    {
      value: 'Inspection',
      label: 'Inspection',
      children: [
        {
          value: 'Inspection Method',
          label: 'Method',
        },
        {
          value: 'Criticality',
          label: 'Classification',
        },
        {
          value: 'Operation',
          label: 'Operation',
        },
      ],
    },
    {
      value: 'Location',
      label: 'Location',
      children: [
        {
          value: 'Drawing Sheet Index',
          label: 'Page',
        },
        {
          value: 'Grid Coordinates',
          label: 'Grid',
        },
        {
          value: 'Balloon Grid Coordinates',
          label: 'Balloon',
        },
        {
          value: 'Connection Point Grid Coordinates',
          label: 'Feature',
        },
      ],
    },
  ];
  const lists = {
    notationType: testTypes,
    criticality: testClassifications,
    inspectionMethod: testMethods,
    operation: testOperations,
  };

  const onChange = (val, key) => {
    setFilters({ ...filters, [key]: { ...filters[key], category: val, value: undefined } });
  };

  const setFilter = (val, key) => {
    setFilters({ ...filters, [key]: { ...filters[key], value: val } });
  };

  const addFilter = () => {
    const newKey = Object.keys(filters).length + 1;
    setFilters({ ...filters, [newKey]: { category: undefined, value: undefined } });
  };

  const removeFilter = (key) => {
    const payload = { ...filters };
    delete payload[key];
    if (Object.keys(payload).length === 0) {
      payload['1'] = { category: undefined, value: undefined };
    }
    setFilters(payload);
  };

  return (
    <div>
      <DataFilter title="data" onChange={onChange} addFilter={addFilter} setFilter={setFilter} removeFilter={removeFilter} features={features} lists={lists} filters={filters} options={options} disabled={disabled} />
      <div>
        <br />
        <p>Filters:</p>
        {Object.keys(filters).map((key) => (
          <p key={key}>{`${key}: ${filters[key].category} - ${filters[key].value}`}</p>
        ))}
      </div>
    </div>
  );
});
