import React from 'react';
import { i18n } from 'i18n';
import Cascader, { BaseOptionType } from 'antd/lib/cascader';
import AddOptionButton from 'styleguide/Buttons/AddOption';
import RemoveOptionButton from 'styleguide/Buttons/RemoveOption';
import { Characteristic } from 'types/characteristics';
import { ListObject } from 'domain/setting/listsTypes';
import { FilterObject } from 'types/reportTemplates';
import { Tooltip } from 'antd';
import DataFilterPopup from './DataFilterPopup';
import './DataFilter.less';

type Props = {
  filters: FilterObject;
  title: string;
  options?: BaseOptionType[];
  features: Characteristic[];
  lists: { [key: string]: ListObject };
  disabled?: boolean;
  onChange: (value: string[], key: string) => void;
  setFilter: (value: any, key: string) => void;
  removeFilter: (key: string) => void;
  addFilter: () => void;
};

const DataFilter = React.memo(
  ({
    filters,
    title,
    options = [
      {
        value: 'Features',
        label: 'Features',
        children: [
          {
            value: 'Notation Type',
            label: 'Type',
          },
          {
            value: 'Notation Subtype',
            label: 'SubType',
          },
          {
            value: 'Marker Label',
            label: 'Number',
          },
        ],
      },
      {
        value: 'Inspection',
        label: 'Inspection',
        children: [
          {
            value: 'Inspection Method',
            label: 'Method',
          },
          {
            value: 'Criticality',
            label: 'Classification',
          },
          {
            value: 'Operation',
            label: 'Operation',
          },
        ],
      },
      {
        value: 'Location',
        label: 'Location',
        children: [
          {
            value: 'Drawing',
            label: 'Drawing',
          },
          {
            value: 'Drawing Sheet Index',
            label: 'Page',
          },
          {
            value: 'Grid Coordinates',
            label: 'Grid',
          },
          {
            value: 'Balloon Grid Coordinates',
            label: 'Balloon',
          },
          {
            value: 'Connection Point Grid Coordinates',
            label: 'Feature',
          },
        ],
      },
    ],
    disabled,
    features,
    lists,
    onChange,
    setFilter,
    removeFilter,
    addFilter,
  }: Props) => {
    const displayRender = (label: string[]) => {
      if (!label) {
        return null;
      }

      let display = '';

      label.forEach((val, index) => {
        if (val === 'Geometric Tolerance') {
          display += 'GDT';
        } else {
          display += val;
        }
        if (index !== label.length - 1) {
          display += ' / ';
        }
      });

      return <span key={label.join(' / ')}>{display}</span>;
    };

    const filter = (inputValue: string, path: BaseOptionType[]) => {
      return path.some((option: any) => option.label.toLowerCase().indexOf(inputValue.toLowerCase()) > -1);
    };

    const FilterRow = (key: string) => {
      const change = (val: any) => {
        onChange(val as string[], key);
      };
      const remove = () => {
        removeFilter(key);
      };
      return (
        <div key={key} className="select-container">
          <Cascader
            data-cy={`${title}-cascader`}
            value={filters[key].category}
            placeholder={i18n('common.none')}
            allowClear={false}
            onChange={change}
            disabled={disabled}
            dropdownClassName={`${title}-cascader`}
            className="type-picker"
            displayRender={displayRender}
            options={options}
            showSearch={{ filter }}
          />
          <DataFilterPopup keyId={key} filter={filters[key]} features={features} lists={lists} setFilter={setFilter} />
          {!disabled && <RemoveOptionButton remove={remove} />}
        </div>
      );
    };

    return (
      <div className="filter-select" data-cy={`${title}-filter`}>
        <p>{i18n(`reportTemplates.filters.${title}`)}</p>
        {!disabled && Object.keys(filters).map((key: string) => FilterRow(key))}
        {disabled && <Tooltip title={i18n('reportTemplates.filters.none')}>{Object.keys(filters).map((key: string) => FilterRow(key))}</Tooltip>}
        {!disabled && <AddOptionButton add={addFilter} option={i18n('reportTemplates.filters.filter')} />}
      </div>
    );
  },
);

export default DataFilter;
// DataFilter.whyDidYouRender = true;
