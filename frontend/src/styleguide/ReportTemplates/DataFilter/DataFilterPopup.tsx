import React, { useState } from 'react';
import { FilterFilled } from '@ant-design/icons';
import { Checkbox, InputNumber, Popover, Input } from 'antd';
import { Characteristic } from 'types/characteristics';
import { CheckboxValueType } from 'antd/lib/checkbox/Group';
import { ListObject } from 'domain/setting/listsTypes';
import { Filter } from 'types/reportTemplates';
import { Drawing } from 'graphql/drawing';
import { Part } from 'domain/part/partTypes';
import { determineFilterType } from 'domain/reportTemplates/utilities';
import { getListNameById } from 'utils/Lists';
import './DataFilter.less';

type Props = {
  keyId: string;
  filter: Filter;
  features: Characteristic[];
  lists: { [key: string]: ListObject };
  setFilter: (value: any, key: string) => void;
};

type NumberProps = {
  min?: number;
  max?: number;
  set: (val: any) => void;
};
const NumberPicker: React.FC<NumberProps> = ({ min, max, set }) => {
  const [start, setStart] = useState(min as any);
  const [end, setEnd] = useState(max as any);

  const confirm = () => {
    if (start === undefined) {
      // reset
      return set(undefined);
    }
    return set({ start, end });
  };

  const reset = () => {
    setStart(undefined);
    setEnd(undefined);
  };

  return (
    <div className="number-picker">
      <div className="picker-container">
        <span>Start</span>
        <InputNumber data-cy="start-picker" min={1} value={start} onChange={setStart} />
      </div>
      <div className="picker-container">
        <span>End</span>
        <InputNumber data-cy="end-picker" min={start} value={end} onChange={setEnd} />
      </div>
      <div className="popover-buttons">
        <button className="link" type="button" onClick={confirm}>
          OK
        </button>
        <button className="link" type="button" onClick={reset}>
          Reset
        </button>
      </div>
    </div>
  );
};

type ListProps = {
  values: { label: string; value: string }[];
  choices?: string[];
  set: (val: any) => void;
};
const ListPicker: React.FC<ListProps> = ({ values, choices, set }) => {
  const [selected, setSelected] = useState<CheckboxValueType[]>(choices || []);

  const confirm = () => {
    if (selected.length === 0) {
      // reset
      return set(undefined);
    }
    return set(selected);
  };

  const reset = () => {
    setSelected([]);
  };

  const change = (val: CheckboxValueType[]) => {
    setSelected(val);
  };

  return (
    <div className="list-picker">
      <Checkbox.Group options={values} value={selected} onChange={change} />

      <div className="popover-buttons">
        <button className="link" type="button" onClick={confirm}>
          OK
        </button>
        <button className="link" type="button" onClick={reset}>
          Reset
        </button>
      </div>
    </div>
  );
};

type RangeProps = {
  min?: string;
  max?: string;
  set: (val: any) => void;
};
const NumberRange: React.FC<RangeProps> = ({ min, max, set }) => {
  const [start, setStart] = useState(min?.toString());
  const [end, setEnd] = useState(max?.toString() as any);

  const confirm = () => {
    if (start === undefined) {
      // reset
      return set(undefined);
    }
    return set({ start: parseFloat(start), end: end ? parseFloat(end) : undefined });
  };

  const handleChangeStart = (event: any) => {
    setStart(event.target.value);
  };

  const reset = () => {
    setStart(undefined);
    setEnd(undefined);
  };

  return (
    <div className="number-picker">
      <div className="picker-container">
        <span>Start</span>
        <Input data-cy="start-picker" min={1} value={start} onChange={handleChangeStart} />
      </div>
      <div className="picker-container">
        <span>End</span>
        <InputNumber data-cy="end-picker" min={start} value={end} onChange={setEnd} />
      </div>
      <div className="popover-buttons">
        <button className="link" type="button" onClick={confirm}>
          OK
        </button>
        <button className="link" type="button" onClick={reset}>
          Reset
        </button>
      </div>
    </div>
  );
};

const DataFilterPopup: React.FC<Props> = React.memo(({ keyId, filter, features, lists, setFilter }) => {
  const [showPopup, setShow] = useState(false);
  if (!filter.category) {
    return <></>;
  }
  const { pickerType, cat } = determineFilterType(filter.category);

  const set = (val: any) => {
    setFilter(val, keyId);
    setShow(false);
  };

  const handleVisibleChange = (visible: boolean) => {
    setShow(visible);
  };

  const used: string[] = [];
  const valueOptions: { label: string; value: string }[] = [];

  switch (pickerType) {
    case 'picker':
      return (
        <Popover //
          visible={showPopup}
          onVisibleChange={handleVisibleChange}
          content={<NumberPicker set={set} min={filter.value?.start} max={filter.value?.end} />}
          placement="right"
          overlayClassName="filter-popover"
          trigger="click"
        >
          <button type="button" className={`filter-button ${filter.value && 'active-filter'}`}>
            <FilterFilled />
          </button>
        </Popover>
      );

    case 'list':
      features.forEach((feature) => {
        if (cat && feature[cat]) {
          let featProp = feature[cat];
          let label = featProp;
          if (featProp && Object.prototype.hasOwnProperty.call(featProp, 'id')) {
            label = (featProp as Part | Drawing).name || (featProp as Part | Drawing).number;
            if (!label) {
              label = (featProp as Drawing).file ? (featProp as Drawing).file[0].name : (featProp as Part | Drawing).id;
            }
            featProp = (featProp as Part | Drawing).id;
          }
          featProp = featProp?.toString();
          label = label?.toString();

          if (featProp && !used.includes(featProp)) {
            used.push(featProp);
            if (lists[cat]) {
              valueOptions.push({ label: getListNameById({ list: lists[cat], value: featProp }), value: featProp });
            } else {
              valueOptions.push({ label: label || featProp, value: featProp });
            }
          }
        }
      });
      return (
        <Popover //
          visible={showPopup}
          onVisibleChange={handleVisibleChange}
          content={<ListPicker set={set} values={valueOptions} choices={filter.value} />}
          placement="right"
          overlayClassName="filter-popover"
          trigger="click"
        >
          <button type="button" className={`filter-button ${filter.value && 'active-filter'}`}>
            <FilterFilled />
          </button>
        </Popover>
      );

    case 'range':
      return (
        <Popover //
          visible={showPopup}
          onVisibleChange={handleVisibleChange}
          content={<NumberRange set={set} min={filter.value?.start} max={filter.value?.end} />}
          placement="right"
          overlayClassName="filter-popover"
          trigger="click"
        >
          <button type="button" className={`filter-button ${filter.value && 'active-filter'}`}>
            <FilterFilled />
          </button>
        </Popover>
      );

    default:
      return <></>;
  }
});

export default DataFilterPopup;
