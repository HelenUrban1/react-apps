import React from 'react';

class MockHeader extends React.Component {
  render() {
    return (
      <div style={{ backgroundColor: 'red', width: '100%', height: '100%', textAlign: 'center' }}>
        <p>Canvas Header Text</p>
      </div>
    );
  }
}

export default MockHeader;
