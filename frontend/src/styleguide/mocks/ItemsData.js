const data = [
  {
    key: '1',
    itemNumber: '1',
    itemReq: 'Radius (R1.15 +/- 0.1) mm',
  },
  {
    key: '2',
    itemNumber: '2',
    itemReq: 'Diameter (0.25 - 0.28) mm',
  },
  {
    key: '3',
    itemNumber: '3',
    itemReq: 'Angle (45 +/- 1) deg',
  },
];

for (let i = 4; i < 30; ++i) {
  data.push({
    key: `4.${i}`,
    itemNumber: `${i}`,
    itemReq: 'Radius (R2.15 +/- 0.12) mm',
  });
}

export default data;
