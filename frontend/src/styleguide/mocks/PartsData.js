const data = [
  {
    key: '1',
    partName: 'IX Multi Tool',
    partNumber: 'PRT-232311',
    customer: 'InspectionXpert',
    lastEdited: '11-24-2019',
    status: 'unverified',
  },
  {
    key: '2',
    partName: 'Lower Plate A',
    partNumber: 'DMC-6372-BTRE',
    customer: 'Ideagen',
    lastEdited: '11-25-2019',
    status: 'ready',
  },
  {
    key: '3',
    partName: "Some Part that Has a Really Long Name, the longest name a parts ever had, this customer should look into renaming their parts because this is kind of silly. Aren't the people who use this part complaining about how long the name is?",
    partNumber: 'Some Part that Has a Really Long Number',
    customer: 'Some Part that Has a Really Long Customer',
    lastEdited: '11-26-2019',
    status: 'ready',
  },
];

for (let i = 4; i < 20; ++i) {
  data.push({
    key: `${i}`,
    partName: 'Some Part that Has a Really Long Name',
    partNumber: 'Some Part that Has a Really Long Number',
    customer: 'Some Part that Has a Really Long Customer',
    lastEdited: `11-${i}-2019`,
    status: 'complete',
  });
}

export default data;
