import React from 'react';

class MockFooter extends React.Component {
  render() {
    return (
      <div style={{ width: '100%', height: '50px', backgroundColor: 'orange' }}>
        <button style={{ backgroundColor: '#blue' }}>Upgrade Now</button>
      </div>
    );
  }
}

export default MockFooter;
