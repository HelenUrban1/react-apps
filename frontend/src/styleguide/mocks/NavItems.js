const menuItems = [
  {
    name: 'Home',
    icon: 'home',
    subitems: [],
    route: '/home',
    bottom: false,
  },
  {
    name: 'Parts',
    icon: 'build',
    subitems: [],
    route: '/canvas',
    bottom: false,
  },
  {
    name: 'Help',
    icon: 'question',
    subitems: [],
    route: './help',
    bottom: true,
  },
  {
    name: 'Settings',
    icon: 'setting',
    subitems: [
      {
        name: 'Users',
        icon: 'usergroup-add',
        route: '/users',
      },
      {
        name: 'Lists',
        icon: 'unordered-list',
        route: '/lists',
      },
      {
        name: 'Resources',
        icon: 'download',
        route: '/resources',
      },
    ],
    route: '',
    bottom: false,
  },
  {
    name: 'sample.monkey@fake.com',
    icon: 'user',
    subitems: [
      {
        name: 'My Account',
        icon: 'user',
        route: '/myaccount',
      },
      {
        name: 'My Ideas',
        icon: 'bulb',
        route: '/myideas',
      },
      {
        name: 'Logout',
        icon: 'logout',
        route: '/login',
      },
    ],
    route: '',
    bottom: false,
  },
];

export default menuItems;
