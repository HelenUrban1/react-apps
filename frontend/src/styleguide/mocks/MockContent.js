import React from 'react';
import { Layout } from 'antd';
import { Switch, Route } from 'react-router-dom';

const { Content } = Layout;

class MockContent extends React.Component {
  render() {
    return <div style={{ backgroundColor: 'azure', textAlign: 'center', height: '100%' }}>Mock Content</div>;
  }
}

export default MockContent;
