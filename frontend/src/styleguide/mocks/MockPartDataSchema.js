var schema = {
  id: 'user',
  type: 'object',
  properties: {
    parts: {
      type: 'array',
      minItems: 25,
      maxItems: 200,
      items: {
        type: 'object',
        properties: {
          id: {
            type: 'string',
            faker: 'random.uuid',
          },
          partName: {
            type: 'string',
            faker: 'commerce.productName',
          },
          partNumber: {
            type: Number,
            faker: 'random.number',
          },
          customer: {
            type: 'string',
            faker: 'company.companyName',
          },
          lastEdited: {
            type: Date,
            faker: 'date.recent',
          },
          status: {
            type: 'string',
            pattern: 'ready|unverified|complete',
          },
        },
        required: ['id', 'partName', 'partNumber', 'customer', 'lastEdited', 'status'],
      },
    },
  },
  required: ['parts'],
};

module.exports = schema;
