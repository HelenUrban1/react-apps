import React from 'react';
import { MailOutlined, PhoneOutlined } from '@ant-design/icons';
import { contact } from 'view/global/defaults';
import { i18n } from 'i18n';
import './contactUs.less';

interface ContactInfoProps {
  layout?: 'Horizontal' | 'Vertical';
  email?: string;
  phone?: string;
}

export const openEmail = (link: string) => {
  const windowRef = window.open(link, '_blank');
  if (!windowRef) return;
  windowRef.focus();
};

export const ContactInfo = ({ layout, email, phone }: ContactInfoProps) => {
  return (
    <div key="contact-info-key" className={`contact-methods ${layout}`}>
      {layout === 'Horizontal' && <span className="contact-title">{i18n('settings.billing.contactUs.contactSales')}</span>}
      <span className="contact-row" data-cy="phone-number">
        <PhoneOutlined />
        {phone}
      </span>
      <span className="contact-row">
        <MailOutlined />
        <a data-cy="email-link" target="_blank" rel="noreferrer" href={email === 'support' ? 'https://www.inspectionxpert.com/getting-started#tab-2' : `mailto:${email}`}>
          {email}
        </a>
      </span>
    </div>
  );
};

ContactInfo.defaultProps = {
  layout: 'Vertical',
  email: contact.sales.email,
  phone: contact.sales.phone,
};
