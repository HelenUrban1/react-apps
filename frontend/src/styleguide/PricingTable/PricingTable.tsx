import React, { useState, useEffect } from 'react';
import { CheckOutlined } from '@ant-design/icons';
import { Modal, Tabs } from 'antd';
import { features, pricingCards } from 'view/global/defaults';
import './pricingTable.less';
import { i18n } from 'i18n';
import { ChangeSubscriptionModal } from 'domain/setting/billing/modals/ChangeSubscriptionModal';
import Analytics from 'modules/shared/analytics/analytics';
import { useDispatch, useSelector } from 'react-redux';
import { AppState } from 'modules/state';
import { SubscriptionAPI } from 'domain/setting/subscriptionApi';
import log from 'modules/shared/logger';
import { BillingPeriodEnum, Component, ComponentInfo, ComponentInput, Subscription } from 'domain/setting/subscriptionTypes';
import { doUpdateCurrentSubscription } from 'modules/subscription/subscriptionActions';
import { PricingCard } from './PricingCard';
import { ContactUs } from './ContactUs';

const { TabPane } = Tabs;

type Plan = {
  name: string;
  cost: string;
  features: string[];
};

interface pricingProps {
  plans: Plan[];
  accountId: string;
  type: string | null;
}

export const PricingTable = ({ plans, accountId, type }: pricingProps) => {
  const dispatch = useDispatch();
  const { data: subscription, components } = useSelector((reduxState: AppState) => reduxState.subscription);
  const [creditCardToken, setToken] = useState<string | undefined>();
  const [period, setPeriod] = useState<BillingPeriodEnum>('Annually');
  const [card, setCard] = useState<any>();
  const [visibility, setVisibility] = useState<boolean>(false);

  useEffect(() => {
    setVisibility(!!card);
  }, [card]);

  const showChangeModal = (newCard: any) => {
    if (newCard) setCard(newCard);
  };

  const closeChangeModal = () => {
    setCard(undefined);
  };

  const closeCardModal = () => {
    Analytics.track({
      event: Analytics.events.trialCreateSubscription,
      properties: {
        account: accountId,
        'trial-type': type,
      },
    });
    Modal.destroyAll();
  };

  const saveCreditCard = (token: string) => {
    Analytics.track({
      event: Analytics.events.trialSetCreditCard,
      properties: {
        account: accountId,
        'trial-type': type,
        billingToken: token,
      },
    });
    setToken(token);
    Modal.destroyAll();
  };

  const removeCreditCard = () => {
    Analytics.track({
      event: Analytics.events.trialRemoveCreditCard,
      properties: {
        account: accountId,
        'trial-type': type,
      },
    });
    setToken(undefined);
    Modal.destroyAll();
  };

  const changePeriod = (sub: Subscription, newPeriod: BillingPeriodEnum) => {
    const input = { ...sub };
    input.billingPeriod = newPeriod;
    input.pendingBillingPeriod = null;
    if (input.pendingBillingPeriod === newPeriod) {
      input.pendingBillingPeriod = null;
    }
    return input;
  };

  const changePrice = (sub: Subscription, price: number) => {
    const input = { ...sub };
    if (input.currentBillingAmountInCents && price < input.currentBillingAmountInCents) {
      input.pendingBillingAmountInCents = price;
    } else {
      input.currentBillingAmountInCents = price;
      input.pendingBillingAmountInCents = null;
    }
    return input;
  };

  const changeComponents = (sub: Subscription, componentInputs: ComponentInput[], pendingComponents: Component[] = []) => {
    const input = { ...sub };
    const newComponentInputs: ComponentInput[] = [];
    const pending: Component[] = [...pendingComponents];
    componentInputs.forEach((comp) => {
      const original = components.find((c) => c.component_id === comp.component_id);
      if (original && original.allocated_quantity > comp.quantity) {
        pending.push({ ...original, allocated_quantity: comp.quantity });
      } else if (original && original.allocated_quantity <= comp.quantity) {
        newComponentInputs.push({ ...comp });
      }
    });

    if (pending) {
      input.pendingComponents = JSON.stringify(pending);
    }
    return { input, newComponentInputs };
  };

  const confirmSubscription = ({
    //
    productHandle,
    period,
    price,
    componentInputs,
    handle,
    pendingComponents,
  }: {
    productHandle?: string;
    period?: BillingPeriodEnum;
    price?: number;
    componentInputs?: ComponentInput[];
    handle?: string;
    pendingComponents?: ComponentInfo;
  }) => {
    if (!subscription) {
      log.warn('No Subscription');
      return;
    }
    let input = { ...subscription };

    if (period) {
      input = changePeriod(input, period);
    }
    if (price !== undefined) {
      input = changePrice(input, price);
    }
    if (productHandle) {
      input.providerProductHandle = productHandle;
    }
    let inputs: ComponentInput[] = [];
    const pending: Component[] = [];
    if (pendingComponents) {
      // Include pending changes from a deferred billing change
      Object.values(pendingComponents).forEach((comp) => {
        const original = components.find((c) => c.component_id === comp.id);
        if (!original) {
          return;
        }
        pending.push({ ...original, allocated_quantity: comp.purchased, price_point_id: parseInt(comp.priceId, 10) });
      });
    }
    if (componentInputs) {
      const compUpdate = changeComponents(input, componentInputs, pending);
      input = compUpdate.input;
      inputs = compUpdate.newComponentInputs;
    }
    const data = SubscriptionAPI.createGraphqlObject(input);

    Analytics.track({
      event: Analytics.events.accountSubscriptionStarted,
      properties: {
        'chargify.id': subscription.paymentSystemId,
        'chargify.customer': subscription.customerId,
        'account.id': subscription.accountId,
      },
    });
    dispatch(doUpdateCurrentSubscription(subscription.id, data, 'Trial_To_Subscription', inputs, creditCardToken, handle));
  };

  const setKey = (key: BillingPeriodEnum) => {
    setPeriod(key);
  };

  return (
    <>
      <section className="pricing-options">
        <section className="feature-list">
          <h3>{i18n('entities.subscription.trial.pricing')}</h3>
          {features.map((feature: string, index: number) => {
            return (
              <section key={`feature-row-${index}`} className="pricing-table-feature-row">
                {plans.map((plan: any) => {
                  return (
                    <section key={`feature-check-${index}`} className="plan-feature-check">
                      {plan.features.includes(feature) ? <CheckOutlined /> : ''}
                    </section>
                  );
                })}
                <section key={`feature-info-${index}`} className="pricing-table-feature-cell">
                  {feature}
                </section>
              </section>
            );
          })}
          <ContactUs />
        </section>
        <section className="pricing-cards">
          <section className="pricing-cards-header">
            <span className="disclaimer">{i18n('entities.subscription.trial.disclaimer')}</span>
          </section>
          <section className="pricing-cards-list">
            {pricingCards.map((pricingCard, index) => {
              return (
                <section key={`card-key-${pricingCard.name}-${index}`}>
                  <PricingCard period={period} card={pricingCard} showChangeModal={showChangeModal} />
                </section>
              );
            })}
          </section>
          <ChangeSubscriptionModal
            newSubscription
            token={creditCardToken || subscription?.cardId?.toString()}
            visibility={visibility}
            closeChangeModal={closeChangeModal}
            saveCreditCard={saveCreditCard}
            removeCreditCard={removeCreditCard}
            closeCardModal={closeCardModal}
            handleSubscriptionChange={confirmSubscription}
            startPeriod={period}
            startQty={card?.startQty}
          />
        </section>
      </section>
    </>
  );
};
