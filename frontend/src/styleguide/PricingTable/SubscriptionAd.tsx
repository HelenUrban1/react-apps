import React from 'react';
import { CheckOutlined } from '@ant-design/icons';
import { ContactUs } from './ContactUs';
import './subscriptionAd.less';

export const SubscriptionAd = () => {
  return (
    <div>
      <div className="subscription-advertisement">
        <div className="advertisement-line">
          <CheckOutlined />
          <div>Free support</div>
        </div>
        <div className="advertisement-line">
          <CheckOutlined />
          <div>Free training</div>
        </div>
        <div className="advertisement-line">
          <CheckOutlined />
          <div>30-Day money back guarantee</div>
        </div>
      </div>
      <ContactUs />
    </div>
  );
};
