import React from 'react';
import { i18n } from 'i18n';

interface PricingProps {
  period: string; // annual | monthly
  card: any;
  showChangeModal: (card: any) => void;
}

// TODO: make this global and allow other currencies(?)
const formatter = new Intl.NumberFormat('en-US', {
  style: 'currency',
  currency: 'USD',
  minimumFractionDigits: 0,
  maximumFractionDigits: 0,
});

export const PricingCard = ({ period, card, showChangeModal }: PricingProps) => {
  return (
    <section key={`pricing-card-${card.name}`} className="pricing-card" onClick={() => showChangeModal(card)}>
      <section className="pricing-card-type">
        <span className="pricing-card-type-name">{card.name}</span>
        <span className="pricing-card-type-qty">{card.qty}</span>
      </section>
      <section className="pricing-card-cost">
        <span className="pricing-card-cost-saving">{card.savings > 0 ? formatter.format(card.savings / (period === 'Annually' ? 1 : 12)) : ''}</span>
        <span className="pricing-card-cost-amount">{formatter.format(card.cost / (period === 'Annually' ? 1 : 12))}</span>
        <span className="pricing-card-cost-period">{period === 'Annually' ? i18n('entities.subscription.trial.perYear') : i18n('entities.subscription.trial.perMonth')}</span>
      </section>
    </section>
  );
};
