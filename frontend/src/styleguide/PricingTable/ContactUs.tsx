import React from 'react';
import './contactUs.less';
import { i18n } from 'i18n';
import { ContactInfo } from './ContactInfo';

interface ContactUsProps {
  layout?: 'Vertical' | 'Horizontal';
}

export const ContactUs = ({ layout = 'Vertical' }: ContactUsProps) => {
  return (
    <div className={`contact-us ${layout}`}>
      <span className="question-prompt">{i18n('settings.billing.contactUs.title')}</span>
      {layout === 'Vertical' && <span className="contact-title">{i18n('common.contact')}</span>}
      <ContactInfo layout={layout} />
    </div>
  );
};
