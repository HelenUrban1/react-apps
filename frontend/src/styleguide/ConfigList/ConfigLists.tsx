import React, { useState } from 'react';
import { i18n } from 'i18n';
import { List, notification, Select } from 'antd';
import { PlusOutlined } from '@ant-design/icons';
import { useMutation } from '@apollo/client';
import { ConfigList } from 'styleguide/ConfigList/ConfigList';
import { IxButton } from 'styleguide';
import { SelectValue } from 'antd/lib/select';
import { ListObject, ListObjectItem, ListTypeEnum } from 'domain/setting/listsTypes';
import { ListItems, ListItemCreate, ListItemDelete, ListItemEdit } from 'domain/setting/listsApi';
import { getCurrentList, updateList, updateLists } from 'utils/Lists';
import { ListItemModal } from 'domain/setting/configurations/ListItemModal';
import './configurations.less';
import { SessionActions } from 'modules/session/sessionActions';
import { AppState } from 'modules/state';
import { useDispatch, useSelector } from 'react-redux';

const { Option } = Select;

type ListTypes = 'Type' | 'Classification' | 'Operation' | 'Inspection_Method' | 'Unit_of_Measurement';

const ConfigLists = () => {
  const dispatch = useDispatch();
  const { types, classifications, operations, methods, imperialUnits, metricUnits } = useSelector((state: AppState) => state.session);
  const [activeList, setActiveList] = useState<ListTypes>('Type');
  const [activeItem, setActiveItem] = useState<ListObjectItem>();
  const [displayModal, setDisplayModal] = useState<boolean>(false);

  const [editListItem] = useMutation<ListItemEdit>(ListItems.mutate.edit, {
    onCompleted: ({ listItemUpdate }) => {
      const newClassifications = {
        ...classifications,
      };
      newClassifications[listItemUpdate.id] = {
        children: {},
        value: listItemUpdate.name,
        default: listItemUpdate.default,
        id: listItemUpdate.id,
        status: listItemUpdate.status,
        listType: listItemUpdate.listType,
        index: listItemUpdate.itemIndex,
        deleted: !!listItemUpdate.deletedAt,
      };
      dispatch({ type: SessionActions.UPDATE_CLASSIFICATIONS, payload: { classifications: newClassifications } });
    },
    refetchQueries: [
      {
        query: ListItems.query.list,
        variables: { status: 'Active', deletedAtRange: undefined },
      },
    ],
    onError: (err) => {
      notification.error({
        message: i18n('errors.chargify.invoices.message'),
        description: i18n('errors.chargify.invoices.description'),
        duration: 10,
      });
    },
  });

  const [deleteListItem] = useMutation<ListItemDelete>(ListItems.mutate.delete, {
    refetchQueries: [
      {
        query: ListItems.query.list,
        variables: { status: 'Active', deletedAtRange: undefined },
      },
    ],
    onError: (err) => {
      notification.error({
        message: i18n('errors.chargify.invoices.message'),
        description: i18n('errors.chargify.invoices.description'),
        duration: 10,
      });
    },
  });

  const [createListItem] = useMutation<ListItemCreate>(ListItems.mutate.create, {
    onCompleted: ({ listItemCreate }) => {
      const currentList = getCurrentList(activeList, classifications, methods, types, operations, imperialUnits, metricUnits);
      dispatch({ type: SessionActions.UPDATE_LISTS, payload: updateLists(activeList, currentList, listItemCreate) });
    },
    refetchQueries: [
      {
        query: ListItems.query.list,
        variables: { status: 'Active', deletedAtRange: undefined },
      },
    ],
    onError: (err) => {
      notification.error({
        message: i18n('errors.chargify.invoices.message'),
        description: i18n('errors.chargify.invoices.description'),
        duration: 10,
      });
    },
  });

  const handleCreateListItem = (name: string) => {
    const itemIndex = Object.keys(getCurrentList(activeList, classifications, methods, types, operations, imperialUnits, metricUnits) || {}).length;
    createListItem({
      variables: {
        data: ListItems.createGraphqlObject({
          name,
          default: false,
          status: 'Active',
          listType: activeList,
          itemIndex,
        }),
      },
    });
  };

  const handleEditListItem = (id: string, item: ListObjectItem, update = true) => {
    const newList = {
      ...getCurrentList(activeList, classifications, methods, types, operations, imperialUnits, metricUnits),
    };
    editListItem({
      variables: {
        id,
        data: ListItems.createInputFromObject(item),
      },
    });
    if (update && newList) newList[id] = item;
  };

  const reorderAfterDelete = (list: ListObject, deletedItem: ListObjectItem) => {
    const newList = { ...list };
    const keys = Object.keys(newList);
    for (let i = deletedItem.index; i < keys.length; i++) {
      newList[keys[i]].index -= 1;
      handleEditListItem(keys[i], newList[keys[i]], false);
    }
    dispatch({ type: SessionActions.UPDATE_LISTS, payload: updateList(activeList, newList) });
  };

  const handleDeleteListItem = async (id: string) => {
    const newList = {
      ...getCurrentList(activeList, classifications, methods, types, operations, imperialUnits, metricUnits),
    };
    if (newList) {
      const newItem = { ...newList[id] };
      if (newItem.default) {
        newItem.status = 'Inactive';
        handleEditListItem(id, newItem);
      } else {
        const { data } = await deleteListItem({
          variables: {
            ids: [id],
          },
        });
        if (data?.listItemDestroy) {
          delete newList[id];
          reorderAfterDelete(newList, newItem);
        }
      }
    }
  };

  const handleSwitchList = (list: ListTypes) => {
    setActiveList(list);
    setActiveItem(undefined);
  };

  const getList = (listType: ListTypeEnum) => {
    let listItems;

    switch (listType) {
      case 'Classification':
        listItems = classifications;
        break;
      case 'Type':
        listItems = types;
        break;
      case 'Inspection_Method':
        listItems = methods;
        break;
      case 'Operation':
        listItems = operations;
        break;
      default:
        break;
    }
    if (listItems) {
      if (activeItem?.children && Object.keys(activeItem.children).length > 0) {
        return (
          <>
            <ConfigList listItems={listItems} listType={listType} handleDeleteListItem={handleDeleteListItem} handleEditListItem={handleEditListItem} handleSelectListItem={setActiveItem} activeItem={activeItem} />
            <ConfigList listItems={listItems} listType={listType} handleDeleteListItem={handleDeleteListItem} handleEditListItem={handleEditListItem} handleSelectListItem={() => {}} activeItem={activeItem} secondary />
          </>
        );
      }
      return <ConfigList listItems={listItems} listType={listType} handleDeleteListItem={handleDeleteListItem} handleEditListItem={handleEditListItem} handleSelectListItem={setActiveItem} activeItem={activeItem} />;
    }
    return <></>;
  };

  const lists: ListTypes[] = ['Type', 'Classification', 'Operation', 'Inspection_Method', 'Unit_of_Measurement'];

  return (
    <section id="ConfigurationSettings" className="settings-section">
      <h2 data-cy="setting-section-title">{i18n('settings.custom.title')}</h2>
      <section className="lists-actions" data-cy="lists-actions">
        <ListItemModal
          modalTitle={`Add ${activeList.replace('_', ' ')}`}
          listType={activeList}
          listItems={getCurrentList(activeList, classifications, methods, types, operations, imperialUnits, metricUnits)}
          handleCreateListItem={handleCreateListItem}
          visible={displayModal}
          setDisplayModal={setDisplayModal}
        />
        <IxButton className="btn-primary" text={activeList.replaceAll('_', ' ')} leftIcon={<PlusOutlined />} onClick={(e: any) => setDisplayModal(true)} />
        <Select
          size="large"
          onChange={(value: SelectValue) => {
            if (value) handleSwitchList(value.toString() as ListTypes);
          }}
          dropdownMatchSelectWidth={false}
          value={activeList}
        >
          {lists.map((list) => {
            return (
              <Option value={list} key={list}>
                {list}
              </Option>
            );
          })}
        </Select>
      </section>
      <div className="customization">
        <List
          size="small"
          header={<div>Lists</div>}
          bordered
          dataSource={lists}
          renderItem={(item) => (
            <List.Item key={item} className={`list-item${activeList === item ? ' list-item-active' : ''}`} onClick={(e: any) => handleSwitchList(item)}>
              <List.Item.Meta title={item.replace('_', ' ')} />
            </List.Item>
          )}
        />
        {getList(activeList)}
      </div>
    </section>
  );
};

export default ConfigLists;
