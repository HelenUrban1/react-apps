import { EyeInvisibleOutlined, EyeOutlined, LockOutlined, RightOutlined } from '@ant-design/icons';
import { Input, List, Switch, Tooltip } from 'antd';
import { ListObjectItem } from 'domain/setting/listsTypes';
import React, { ChangeEvent, CSSProperties, useState } from 'react';
import { ConfirmDelete } from 'view/global/confirmDelete';

interface ListCardProps {
  listItem: ListObjectItem;
  handleEditListItem: (id: string, newItem: ListObjectItem) => void;
  handleDeleteListItem: (id: string) => void;
  handleSelectListItem: (listItem: ListObjectItem) => void;
  active: boolean;
}

export const ListCard = ({ listItem, handleEditListItem, handleDeleteListItem, handleSelectListItem, active }: ListCardProps) => {
  const [value, setValue] = useState<string>(listItem.value);
  const [errorText, setErrorText] = useState<string>();

  const defaultStyle: CSSProperties = {};
  if (errorText) {
    defaultStyle.borderColor = 'red';
    defaultStyle.width = '200px';
  }

  const selectListItem = () => {
    handleSelectListItem(listItem);
  };

  const handleBlur = (event: ChangeEvent<HTMLInputElement>) => {
    if (!event.target.value || event.target.value.length === 0) {
      setErrorText('Cannot be empty.');
    } else {
      setErrorText(undefined);
      const newListItem = { ...listItem };
      newListItem.value = event.target.value;
      handleEditListItem(listItem.id, newListItem);
    }
  };

  const handleOnChange = (event: ChangeEvent<HTMLInputElement>) => {
    setValue(event.target.value);
    if (!event.target.value || event.target.value.length === 0) {
      setErrorText('Cannot be empty.');
    }
  };

  const handleToggleActive = () => {
    const newListItem = { ...listItem };
    newListItem.status = newListItem.status === 'Active' ? 'Inactive' : 'Active';
    handleEditListItem(listItem.id, newListItem);
  };

  return (
    <List.Item key={listItem.value} className={`list-item-card${active ? ' list-item-active' : ''}`}>
      <div className="card-toggle">
        <Switch key={`${listItem.value}-active-switch`} checkedChildren={<EyeOutlined />} unCheckedChildren={<EyeInvisibleOutlined />} defaultChecked={listItem.status === 'Active'} onChange={handleToggleActive} />
      </div>
      <div className="card-content">
        {!listItem.default ? (
          <Tooltip placement="top" title={errorText || ''}>
            <Input id={value} key={value} defaultValue={value || ''} onChange={handleOnChange} onBlur={handleBlur} style={defaultStyle} />
          </Tooltip>
        ) : (
          <Input id={value} key={value} defaultValue={listItem.value} style={defaultStyle} disabled />
        )}
      </div>
      <div className="card-actions" data-cy="listItemCardActions">
        {!listItem.default ? (
          <ConfirmDelete classes="icon-button list-item-delete" confirmText="Delete this?" confirm={() => handleDeleteListItem(listItem.id)} colored />
        ) : (
          <div className="list-item-locked">
            <Tooltip placement="top" title="Sorry, you can't edit a default.">
              <LockOutlined />
            </Tooltip>
          </div>
        )}
        {listItem.children && Object.keys(listItem.children || {}).length > 0 && <RightOutlined onClick={selectListItem} />}
      </div>
    </List.Item>
  );
};
