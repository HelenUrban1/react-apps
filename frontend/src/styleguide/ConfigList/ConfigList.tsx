import { List } from 'antd';
import { ListObject, ListObjectItem } from 'domain/setting/listsTypes';
import React from 'react';
import { ListCard } from './ListCard';

interface ConfigListProps {
  listItems: ListObject;
  listType: string;
  handleEditListItem: (id: string, newItem: ListObjectItem) => void;
  handleDeleteListItem: (id: string) => void;
  handleSelectListItem: (listItem: ListObjectItem) => void;
  activeItem: ListObjectItem | undefined;
  secondary?: boolean;
}

export const ConfigList = ({ listItems, listType, handleEditListItem, handleDeleteListItem, handleSelectListItem, activeItem, secondary = false }: ConfigListProps) => {
  return secondary && activeItem?.children ? (
    <List
      key={listType}
      size="small"
      header={listType}
      bordered
      dataSource={Object.values(activeItem.children)}
      renderItem={(item) => <ListCard key={item.id} active={item.id === activeItem?.id} listItem={item} handleDeleteListItem={handleDeleteListItem} handleEditListItem={handleEditListItem} handleSelectListItem={handleSelectListItem} />}
    />
  ) : (
    <List
      key={listType}
      size="small"
      header={listType}
      bordered
      dataSource={Object.keys(listItems)}
      renderItem={(item) => (
        <ListCard key={listItems[item].id} active={listItems[item].id === activeItem?.id} listItem={listItems[item]} handleDeleteListItem={handleDeleteListItem} handleEditListItem={handleEditListItem} handleSelectListItem={handleSelectListItem} />
      )}
    />
  );
};
