export const styles = {
  colors: {
    xpertM2: '#F2E7F7', // Xpert Purple Minus 2
    xpertM1: '#C08AD7', // Xpert Purple minus 1
    xpert: '#963CBD', // Xpert Purple
    xpertP1: '#78229D',
    xpertP2: '#55196F', // Xpert Purple plus 2
    smokeM1: '#F6F8FA', // Smoke minus 1
    smoke: '#DDE5ED', // Smoke
    smokeP1: '#B7C3D5', // Smoke plus 1
    blackberryM1: '#755C9C', // Blackberry minus 1
    blackberry: '#5C4580', // Blackberry
    blackberryP1: '#3B2959', // Blackberry plus 1
    coralM1: '#F5C3BB', // Coral minus 1
    coral: '#E56A54', // Coral
    coralP1: '#E04F39', // Coral plus 1
    sunshineM2: '#FDF7EF', // Sunshine minus 2
    sunshineM1: '#F5D8B1', // Sunshine minus 1
    sunshine: '#EFBE7D', // Sunshine
    sunshineP1: '#E99C4F', // Sunshine plus 1
    sunshineP2: '#E47113', // Sunshine plus 2
    slate: '#333F48', // Slate
    slateM1: '#515F69', // Slate minus 1
    slateM2: '#687783', // Slate minus 2
    slateM3: '#889299', // Slate minus 3
    slateM4: '#BFC7CD', // Slate minus 4
    royalM4: '#E8E6F7', // Royal minus 4
    royalM3: '#8B84D7', // Royal minus 3
    royalM2: '#685BC7', // Royal minus 2
    royalM1: '#2E008B', // Rotyal minus 1
    royal: '#201547', // Royal
  },
};
