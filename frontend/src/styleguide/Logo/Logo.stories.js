import React from 'react';
import { storiesOf } from '@storybook/react';
import { styles } from '../AntDesignCustomTheme';
import { TypeLogo, IconLogo } from '../shared/svg';

const stories = storiesOf('Design System/Logo', module);

stories
  .add('Type', () => {
    const story = (
      <>
        <div>
          <TypeLogo color="black" />
        </div>
        <div>
          <TypeLogo color={styles.colors.xpert} />
        </div>
        <div style={{ backgroundColor: styles.colors.xpertP2, width: 'fit-content' }}>
          <TypeLogo color="white" />
        </div>
      </>
    );

    return story;
  })
  .add('Icon', () => {
    const story = (
      <>
        <div>
          <IconLogo color="black"></IconLogo>
        </div>
        <div>
          <IconLogo color={styles.colors.xpert}></IconLogo>
        </div>
        <div style={{ backgroundColor: styles.colors.xpertP2, width: 'fit-content' }}>
          <IconLogo color="white"></IconLogo>
        </div>
      </>
    );

    return story;
  });
