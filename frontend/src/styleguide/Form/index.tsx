import React, { useState, CSSProperties } from 'react';
import { PlusOutlined, CloseCircleOutlined } from '@ant-design/icons';
import { Input, Select, Radio, Divider } from 'antd';
import './Form.less';
import { IxButton } from '../index';

const { Option } = Select;
const { TextArea } = Input;
// Todo: swap out icons with IX icon
type Props = {
  inputs: Sections;
  submit: (data: any) => void;
  submitText?: string;
  cancel?: () => void;
  cancelText?: string;
  cancelID?: string;
  id?: string;
  classes?: string;
};

interface Sections {
  sections: InputType[][];
}

interface InputType {
  label: string;
  name: string;
  value: string;
  type?: string;
  search?: boolean;
  add?: (e: any) => void;
  group?: InputType[];
  change?: (e: any) => void;
  blur?: (e: any) => void;
  placeholder?: string;
  id?: string;
  classes?: string;
}

const style: CSSProperties = { resize: 'none' };

// TODO: AntD
const SelectInput: React.FC<{ input: InputType }> = ({ input }) => {
  const [searchValue, setValue] = useState('');
  return (
    <label key={input.name} id={input.name} className={`form-input ${input.classes}`}>
      <span className="label-text">{input.label}:</span>
      {input.group && input.search && (
        // Searchable select field
        <Select
          value={input.value}
          showSearch
          optionFilterProp="children"
          filterOption={(text, option) => {
            if (input.add) {
              // Save our search text when adding a new option so it can be passed
              setValue(text);
            }
            // Filter options as a user types, always keeping at least "+ New {whatever this field is}"
            return (
              typeof option?.props.children !== 'string' ||
              (option.props.children && typeof option.props.children === 'string'
                ? option.props.children?.toLowerCase().indexOf(text.toLowerCase()) >= 0 || option.props.children?.toLowerCase().indexOf(` New ${input.name}`.toLowerCase()) >= 0
                : false)
            );
          }}
          onChange={(value: string) => (input.change && value !== 'customer-add' ? input.change({ target: { name: input.name, value } }) : (value = ''))}
        >
          {input.group &&
            input.group.map((group) => (
              <Option key={group.name} value={group.value}>
                {group.label}
              </Option>
            ))}
          {input.add && (
            // Add new option
            <Option key={`${input.name}-add`} value={input.name}>
              <Divider style={{ margin: '4px 0' }} />
              <div
                id={`add-${input.name}`}
                style={{ padding: '4px 8px', cursor: 'pointer' }}
                onMouseDown={(e) => e.preventDefault()}
                onClick={(e) => {
                  e.preventDefault();
                  if (input.add) {
                    input.add(searchValue);
                  }
                  setValue('');
                  return null;
                }}
              >
                <PlusOutlined /> New
                {input.name}
              </div>
            </Option>
          )}
        </Select>
      )}
      {input.group && !input.search && (
        // Non-searchable select
        <Select defaultValue={input.value} onChange={(value: string) => (input.change ? input.change({ target: { name: input.name, value } }) : null)}>
          {input.group &&
            input.group.map((group) => (
              <Option key={group.name} value={group.value}>
                {group.label}
              </Option>
            ))}
          {input.add && (
            // Add new item
            <Option key={`${input.name}-add`} value={input.name}>
              <Divider style={{ margin: '4px 0' }} />
              <div style={{ padding: '4px 8px', cursor: 'pointer' }} onMouseDown={(e) => e.preventDefault()} onClick={input.add}>
                <PlusOutlined /> New
                {input.name}
              </div>
            </Option>
          )}
        </Select>
      )}
    </label>
  );
};

const RadioInput: React.FC<{ input: InputType }> = ({ input }) => {
  return (
    <Radio.Group id={input.name} key={input.name} name={input.name} className={`form-input ${input.classes}`} defaultValue={input.group ? input.group[0].value : ''} onChange={input.change} buttonStyle="solid">
      <label className={`form-input ${input.classes}`}>{input.label}:</label>
      {input.group &&
        input.group.map((group) => (
          <Radio key={group.name} id={`radio-${input.name}-${group.name}`} value={group.value}>
            <span id={`radio-${input.name}-${group.name}-label`}>{group.label}</span>
          </Radio>
        ))}
    </Radio.Group>
  );
};

const Grouped: React.FC<{ input: InputType }> = ({ input }) => {
  return (
    <div key={input.name} id={input.name} className={`form-group ${input.classes}`}>
      {input.group &&
        input.group.map((group) => (
          <label key={group.name} id={group.name} className={`form-input ${group.classes}`}>
            <span className="label-text">{group.label}:</span>
            <TextArea style={style} autoSize defaultValue={group.value} name={group.name} onChange={group.change} onBlur={group.blur} />
          </label>
        ))}
    </div>
  );
};

const Section: React.FC<{ sections: InputType[]; id: number }> = ({ sections, id }) => {
  return (
    <section id={`section-${id}`} className="form-section">
      {sections.map((input) => {
        if (input.group) {
          switch (input.type) {
            case 'radio':
              return <RadioInput key={input.name} input={input} />;
            case 'select':
              return <SelectInput key={input.name} input={input} />;
            default:
              return <Grouped key={input.name} input={input} />;
          }
        }
        return (
          <label key={input.name} id={input.name} className={`form-input ${input.classes}`}>
            <span className="label-text">{input.label}:</span>
            <TextArea style={style} autoSize defaultValue={input.value} name={input.name} onChange={input.change} onBlur={input.blur} />
          </label>
        );
      })}
    </section>
  );
};

const Form: React.FC<Props> = ({ id = '', classes = '', inputs, submit, submitText, cancel, cancelText, cancelID = '' }) => {
  return (
    <form id={id} className={classes} onSubmit={submit}>
      {inputs.sections.map((section, index) => (
        <Section key={`form-section-${index}`} id={index} sections={section} />
      ))}
      <div className="form-buttons">
        {cancel && <IxButton text={cancelText} className="btn-secondary" leftIcon={<CloseCircleOutlined />} onClick={cancel} />}
        <IxButton text={submitText} className="btn-primary" leftIcon={<PlusOutlined />} onClick={submit} />
      </div>
    </form>
  );
};

export default Form;
