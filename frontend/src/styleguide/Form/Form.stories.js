import React from 'react';
import { storiesOf } from '@storybook/react';
import Form from './index';
import MDX from './Form.mdx';

const stories = storiesOf('Design System/Form', module);

stories.add('Closed', () => {
  const story = <Form />;

  return story;
});
