import * as styles from './shared/styles';
import * as global from './shared/global';
import * as animation from './shared/animation';

// react components
export { default as icons } from './shared/icons';
export { default as NavMenu } from './NavMenu/NavMenu';
export { default as NavRouter } from './NavMenu/NavRouter';
export { default as Icon } from './Icons/Icon';
export { default as Avatar } from './Avatar/Avatar';
export { default as MasterFrame } from './MasterFrame/MasterFrame';
export { default as IxSider } from './IxSider/IxSider';
export { default as IxTag } from './Tags/IxTag';
export { default as IxButton } from './Buttons/IxButton';
export { default as Token } from './Token/Token';
export { default as TokenTree } from './TokenTree/TokenTree';

export { styles, global, animation };
