import React, { useState } from 'react';
import Button from '../Button/Button';
import Pill from '../Pill/Pill';
import './MetaDataFilters.less';

interface Option {
  label: string;
  value: string;
  isActive: boolean;
}

interface Props {
  options: Array<Option>;
  title?: string;
  onFilterSelect: ({ isChecked, label }: { isChecked: boolean; label: string }) => void;
  applyButtonText?: string;
  allText?: string;
  onApplyFilters: () => void;
}

const MetaDataFilters = ({ title, options, onFilterSelect, applyButtonText, onApplyFilters, allText }: Props) => {
  const [isAllSelected, setAllSelected] = useState(false);

  const onAllSelect = () => {
    // When "All" is clicked, programatically select or deselect all other options
    options.forEach((option) => {
      onFilterSelect({ isChecked: !isAllSelected, label: option.label });
    });
    setAllSelected(!isAllSelected);
  };

  const onFilterSelectWrapper = ({ label, isChecked }: { isChecked: boolean; label: string }) => {
    // If "All" is already selected, select the option that was clicked while deselecting all others
    if (isAllSelected) {
      setAllSelected(false);
      options.forEach((option) => {
        onFilterSelect({ label: option.label, isChecked: label === option.label });
      });
    } else {
      onFilterSelect({ label, isChecked });
    }
  };

  return (
    <div className="ixc-meta-data-filters-container">
      <div className="ixc-meta-data-filters-title">{title}</div>
      <div className="ixc-meta-data-filters-pills">
        <div className="ixc-meta-data-filters-pill">
          <Pill onSelect={onAllSelect} isActive={isAllSelected} label={allText || '' /* to please TS */} />
        </div>
        {options.map((option: Option) => (
          <div key={option.label} className="ixc-meta-data-filters-pill">
            <Pill onSelect={onFilterSelectWrapper} isActive={!isAllSelected && option.isActive} label={option.label} />
          </div>
        ))}
      </div>
      <div className="ixc-meta-data-filters-buttons">
        <Button onClick={onApplyFilters}>{applyButtonText}</Button>
      </div>
    </div>
  );
};

MetaDataFilters.defaultProps = {
  title: 'Inspection Method',
  applyButtonText: 'APPLY FILTER',
  allText: 'All',
};

export default MetaDataFilters;
