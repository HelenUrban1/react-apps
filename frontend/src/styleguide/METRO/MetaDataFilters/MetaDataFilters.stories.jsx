import React, { useState } from 'react';
import { storiesOf } from '@storybook/react';
import MetaDataFilters from './MetaDataFilters';

const stories = storiesOf('METRO/MetaDataFilters', module);

stories.add('Default', () => {
  const [options, setOptions] = useState([
    { label: 'Calipers', isActive: true },
    { label: 'Air Gage', isActive: false },
    { label: 'Pin Micrometer', isActive: false },
    { label: 'Gage Pins', isActive: false },
    { label: 'Misc', isActive: false },
    { label: 'CMM', isActive: false },
    { label: 'Gage Kit', isActive: true },
    { label: 'Baromatic', isActive: false },
    { label: 'Height Gage', isActive: false },
    { label: 'Thread Ring', isActive: false },
    { label: 'Visual', isActive: false },
  ]);

  const onFilterToggle = ({ label, isChecked }) => {
    const newOptions = [...options];
    const opt = newOptions.find((option) => option.label === label);
    opt.isActive = isChecked;
    setOptions(newOptions);
  };

  const onApply = () => {
    const activeFilters = options.filter((option) => option.isActive).map((option) => option.label);
    alert(`Active filters: ${activeFilters}`);
  };

  const story = (
    <>
      <MetaDataFilters title="Inspection Method" options={options} onFilterSelect={onFilterToggle} onApplyFilters={onApply} />
    </>
  );

  return story;
});
