import React from 'react';
import { CloseCircleOutlined, EditOutlined, FilterFilled, SearchOutlined } from '@ant-design/icons';

import Theme from '../../styles/IxcTheme';
import './MetaDataTile.less';
import { Characteristic } from 'types/characteristics';

enum IconType {
  Edit = 'edit',
  Filter = 'filter',
  Info = 'info-circle',
  Close = 'close',
  Search = 'search',
}

interface Props {
  label: string;
  value: string;
  data?: Characteristic | null;
  icon: IconType;
  active: boolean;
  onClick: () => void;
  footer?: string;
}

const MetaDataTile = ({ label, data, value, icon, active, onClick, footer }: Props) => {
  const getIcon = () => {
    switch (icon) {
      case 'edit':
        return <EditOutlined onClick={onClick} style={active ? { color: Theme.colors.primaryColor } : {}} />;
      case 'filter':
        return <FilterFilled onClick={onClick} style={active ? { color: Theme.colors.primaryColor } : {}} />;
      case 'close':
        return <CloseCircleOutlined onClick={onClick} style={active ? { color: Theme.colors.primaryColor } : {}} />;
      case 'search':
        return <SearchOutlined onClick={onClick} style={active ? { color: Theme.colors.primaryColor } : {}} />;
      default:
        return <></>;
    }
  };

  return (
    <div className="ixc-mdt-outer">
      <div className={`ixc-mdt-inner ${footer && 'ixc-mdt-inner-extended'}`}>
        <div className={`${active && 'ixc-mdt-flex'}`}>
          <div>
            <div className="ixc-mdt-label">{label}</div>
            <div className="ixc-mdt-value">{value}</div>
          </div>
          {!active && footer && <div className="ixc-mdt-footer">{footer}</div>}
        </div>
        <button type="button" className="ixc-mdt-icon">
          {getIcon()}
        </button>
      </div>
    </div>
  );
};

MetaDataTile.IconType = IconType;

export default MetaDataTile;
