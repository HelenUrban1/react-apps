import React from 'react';
import { storiesOf } from '@storybook/react';
import MetaDataTile from './MetaDataTile.tsx';

const stories = storiesOf('METRO/MetaDataTile', module);

stories.add('Default', () => {
  const story = (
    <>
      <MetaDataTile label="Job #" onClick={() => window.alert('Job Clicked')} value="1JB-499JF94-FNEW" icon={MetaDataTile.IconType.Edit} />
      <MetaDataTile label="Part #" onClick={() => window.alert('Part Clicked')} value="PRT-3232" icon={MetaDataTile.IconType.Edit} />
      <MetaDataTile label="Machine" onClick={() => window.alert('Machine Clicked')} value="MCH-342KJ" icon={MetaDataTile.IconType.Filter} />
      <MetaDataTile label="Operation" onClick={() => window.alert('Operation Clicked')} value={10} icon={MetaDataTile.IconType.Filter} active />
    </>
  );

  return story;
});
