import { Characteristic } from 'types/characteristics';
import { Measurement } from 'types/measurement';
import { Sample } from 'types/sample';

export enum SampleTableStatus {
  Unmeasured = 'Unmeasured',
  Fail = 'Fail',
  Pass = 'Pass',
  Scrapped = 'Scrap',
  Selected = 'Selected',
  Empty = 'Empty',
}

export enum DataCellVariants {
  Empty = 'Empty',
  Disabled = 'Disabled',
  Fail = 'Fail',
  Pass = 'Pass',
  Selected = 'Selected',
}

export interface Cell {
  measurements: Measurement[] | undefined;
  sample: Sample;
  feature: Characteristic;
}
