import React, { RefObject } from 'react';
import { SampleTableStatus } from '../metro-types';

import './SampleHeader.less';

interface Props {
  sampleRef: RefObject<HTMLButtonElement>;
  onClick: () => void;
  sampleNumber?: string;
  serialNumber?: string;
  variant: SampleTableStatus;
  status?: SampleTableStatus;
  scrapText?: string;
  featureCoverage?: string;
  isExtra?: boolean;
  criticalOnly?: boolean;
}

const SampleHeader = ({ sampleRef, sampleNumber, serialNumber, onClick, variant, criticalOnly, status, scrapText, featureCoverage, isExtra }: Props) => {
  return (
    <button ref={variant === 'Selected' ? sampleRef : null} type="button" onClick={onClick} className={`ixc-sample-header-container ixc-sample-header-${variant} sample-cell-${status}`} disabled={isExtra || variant === 'Empty'}>
      <div className="ixc-sample-header-status-bar" />
      <div className="ixc-sample-header-content">
        <div className="ixc-sample-header-timer-serial">{serialNumber || '-no serial-'}</div>
        <div className="ixc-sample-header-sample">{variant === SampleTableStatus.Scrapped ? scrapText : sampleNumber}</div>
        <div className="ixc-sample-header-coverage">{featureCoverage || ''}</div>
      </div>
    </button>
  );
};

SampleHeader.defaultProps = {
  scrapText: 'scrap',
  sampleNumber: '',
  serialNumber: '',
  featureCoverage: '',
  status: 'Unmeasured',
  isExtra: false,
  criticalOnly: false,
};

SampleHeader.Variants = SampleTableStatus;

export default SampleHeader;
