import React from 'react';
import { storiesOf } from '@storybook/react';
import SampleHeader from './SampleHeader';

const stories = storiesOf('METRO/Sample Header', module);

stories.add('Default', () => {
  const story = (
    <div style={{ display: 'flex', justifyContent: 'space-around', maxWidth: '800px' }}>
      <div>
        <h2>Unmeasured</h2>
        <SampleHeader onClick={() => alert('Clicked unmeasured')} variant={SampleHeader.Variants.Unmeasured} secondsRemaining={19063} sampleNumber="23" criticalOnly />
      </div>
      <div>
        <h2>Failed</h2>
        <SampleHeader onClick={() => alert('Clicked failed')} variant={SampleHeader.Variants.Failed} serialNumber="233-12343 B 027" sampleNumber="23" criticalOnly />
      </div>
      <div>
        <h2>Passed</h2>
        <SampleHeader onClick={() => alert('Clicked passed')} variant={SampleHeader.Variants.Passed} serialNumber="233-12343 B 027" sampleNumber="23" criticalOnly />
      </div>
      <div>
        <h2>Scrapped</h2>
        <SampleHeader onClick={() => alert('Clicked scrapped')} variant={SampleHeader.Variants.Scrapped} serialNumber="233-12343 B 027" sampleNumber="23" criticalOnly />
      </div>
      <div>
        <h2>Selected</h2>
        <SampleHeader onClick={() => alert('Clicked selected')} variant={SampleHeader.Variants.Selected} serialNumber="233-12343 B 027" sampleNumber="23" criticalOnly />
      </div>
    </div>
  );

  return story;
});
