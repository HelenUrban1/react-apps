import { useEffect, useState, useRef } from 'react';

export default function useKeyboardInput(initialValue: string, validKeys: string[] = []) {
  const [value, setValue] = useState(initialValue);
  const refValue = useRef(initialValue);
  const refValidKeys = useRef(validKeys);
  const [keyDownEvent, setKeyDownEvent] = useState<KeyboardEvent>();

  useEffect(() => {
    setKeyDownEvent(undefined); /** Necessary to avoid computation of useEffect on initalValue change when event is old --> see test of '-' sign */
    setValue(initialValue);
  }, [initialValue]);

  useEffect(() => {
    refValue.current = value;
  }, [value]);

  useEffect(() => {
    refValidKeys.current = validKeys;
  }, [validKeys]);

  function keyDownHandler(event: KeyboardEvent) {
    const { key } = event;
    if (key === 'reset') {
      setValue('');
      return;
    }
    setKeyDownEvent(event);
    if (key === 'Backspace') {
      setValue(refValue.current.slice(0, -1));
    } else if (refValidKeys.current.length > 0) {
      if (refValidKeys.current.includes(key)) {
        setValue(refValue.current + key);
      }
    }
  }

  function virtualInteraction(key: string) {
    keyDownHandler(new KeyboardEvent('keydown', { key }));
  }
  useEffect(() => {
    document.addEventListener('keydown', keyDownHandler);

    return function cleanup() {
      document.removeEventListener('keydown', keyDownHandler);
    };
  }, []);

  return { value, keyDownEvent, virtualInteraction };
}
