import React from 'react';
import { render, fireEvent, cleanup } from '@testing-library/react';
import { characteristics } from 'domain/part/edit/characteristics/utils/RenumberTestData';
import Number from '../Number';

const mockCell = {
  sampleFeature: 1,
  sampleIndex: 1,
  selectedFeature: characteristics[0],
  selectedSample: { id: 'test', serial: 'test', sampleIndex: 0, status: 'Unmeasured' },
};

// TODO: Fix JSX compiling issue or update test to not use testing-library/JSX
describe('numpad keyboard events', () => {
  test('number', async () => {
    expect(true).toBeTruthy();
  });
});

// describe('numpad keyboard events', () => {
//   afterEach(cleanup);

//   test('number', async () => {
//     const { container, getByText, getByRole } = render(
//       <Number
//         onChange={(value) => {}}
//         position="startTopLeft"
//         inline
//         placeholder=""
//         value=""
//         decimal={2}
//         keyValidator={() => true}
//         cancel={() => {}}
//         negative
//         handleTableNav={() => {}}
//         featureIndex={mockCell.featureIndex}
//         sampleIndex={mockCell.sampleIndex}
//         selectedCell={mockCell}
//         gage="test"
//         setGage={() => {}}
//       />
//     );

//     fireEvent.click(getByText('1'));
//     const display = getByRole('display');
//     expect(display.value).toBe('1');

//     fireEvent.keyDown(container, { key: '3' });
//     expect(display.value).toBe('13');

//     fireEvent.keyDown(container, { key: '-' });
//     expect(display.value).toBe('-13');

//     fireEvent.keyDown(container, { key: '2' });
//     expect(display.value).toBe('-132');

//     fireEvent.keyDown(container, { key: '.' });
//     expect(display.value).toBe('-132.');
//   });

//   test('decimal with negative', async () => {
//     const { container, getByRole } = render(
//       <Number
//         onChange={(value) => {}}
//         position="startTopLeft"
//         inline
//         placeholder=""
//         value=""
//         decimal={2}
//         keyValidator={() => true}
//         cancel={() => {}}
//         negative
//         handleTableNav={() => {}}
//         featureIndex={mockCell.featureIndex}
//         sampleIndex={mockCell.sampleIndex}
//         selectedCell={mockCell}
//         gage="test"
//         setGage={() => {}}
//       />
//     );

//     const display = getByRole('display');

//     fireEvent.keyDown(container, { key: '.' });
//     expect(display.value).toBe('0.');

//     fireEvent.keyDown(container, { key: '3' });
//     expect(display.value).toBe('0.3');

//     fireEvent.keyDown(container, { key: '-' });
//     expect(display.value).toBe('-0.3');

//     fireEvent.keyDown(container, { key: '1' });
//     expect(display.value).toBe('-0.31');
//   });

//   test('oddValidator', async () => {
//     const oddValidator = (value) => parseInt(value, 10) % 2 !== 0 && parseFloat(value) % 1 === 0;

//     const { container, getByText, getByRole } = render(
//       <Number
//         onChange={(value) => {}}
//         position="startTopLeft"
//         inline
//         placeholder=""
//         value=""
//         decimal={2}
//         keyValidator={oddValidator}
//         cancel={() => {}}
//         negative={false}
//         handleTableNav={() => {}}
//         featureIndex={mockCell.featureIndex}
//         sampleIndex={mockCell.sampleIndex}
//         selectedCell={mockCell}
//         gage="test"
//         setGage={() => {}}
//       />
//     );

//     fireEvent.click(getByText('1'));
//     const display = getByRole('display');
//     expect(display.value).toBe('1');

//     fireEvent.keyDown(container, { key: '2' });
//     expect(display.value).toBe('1');

//     fireEvent.keyDown(container, { key: '3' });
//     expect(display.value).toBe('13');
//   });

//   test('positive number', async () => {
//     const { container, getByText, getByRole } = render(
//       <Number
//         onChange={(value) => {}}
//         position="startTopLeft"
//         inline
//         placeholder=""
//         value=""
//         decimal={2}
//         keyValidator={() => true}
//         cancel={() => {}}
//         negative
//         handleTableNav={() => {}}
//         featureIndex={mockCell.featureIndex}
//         sampleIndex={mockCell.sampleIndex}
//         selectedCell={mockCell}
//         gage="test"
//         setGage={() => {}}
//       />
//     );

//     const display = getByRole('display');

//     fireEvent.click(getByText('.'));
//     expect(display.value).toBe('0.');

//     fireEvent.keyDown(container, { key: 'Backspace' });
//     expect(display.value).toBe('0');

//     fireEvent.click(getByText('1'));
//     expect(display.value).toBe('1');

//     fireEvent.keyDown(container, { key: '3' });
//     expect(display.value).toBe('13');

//     fireEvent.keyDown(container, { key: '2' });
//     expect(display.value).toBe('132');

//     fireEvent.keyDown(container, { key: '.' });
//     expect(display.value).toBe('132.');

//     fireEvent.keyDown(container, { key: '1' });
//     expect(display.value).toBe('132.1');

//     fireEvent.keyDown(container, { key: '1' });
//     expect(display.value).toBe('132.11');
//   });
// });
