import React, { useRef, useCallback, useState, useEffect } from 'react';
import { ThemeProvider } from 'styled-components';
import styles from './styles';
import { Content } from './elements/Wrapper';
import { COL_WIDTH } from '../DataTable/TableDims';

export interface StaticWrapperProps {
  children: any;
  theme: Object | string;
  position: string;
  locale: string;
  sync: boolean;
  onChange: Function;
  displayRule: Function;
  recordMeasurement: Function;
  featureIndex: number;
  sampleIndex: number;
  measurementIndex: number;
  measurementId: string | null;
}

const StaticWrapper = ({ theme, position, onChange, displayRule, sync = false, children, locale = 'en', recordMeasurement, featureIndex, sampleIndex, measurementIndex, measurementId }: StaticWrapperProps) => {
  const contentRef = useRef();
  const customTheme = typeof theme === 'object' ? theme : styles(theme);
  const [coords, setCoords] = useState({ top: `0px`, left: `0px`, bottom: `0px`, right: `0px` });

  const confirm = useCallback(
    (value, shiftPressed, ctrlPressed) => {
      onChange(displayRule(value));
      recordMeasurement(displayRule(value), shiftPressed, ctrlPressed);
    },
    [featureIndex, sampleIndex, measurementId],
  );

  // Update internal state, if sync is true call the external onChange callback on each change
  const update = useCallback(
    (val) => {
      if (sync) {
        onChange(displayRule(val));
      }
    },
    [sync, featureIndex, sampleIndex, measurementIndex],
  );

  const getCoords = () => {
    const eles = document.getElementsByClassName('ant-table-header');
    const ele = eles[0];
    if (ele) {
      const eleCoords = ele.getBoundingClientRect();
      return { top: `${eleCoords.top}px`, left: `${eleCoords.right - COL_WIDTH}px`, bottom: `${eleCoords.bottom}px`, right: `${eleCoords.right}px` };
    }
    return coords;
  };

  useEffect(() => {
    setCoords(getCoords());
  }, []);

  return (
    <ThemeProvider theme={customTheme?.coords ? customTheme : { ...customTheme, coords }}>
      <Content position={position} coords={coords}>
        {React.cloneElement(children, {
          update,
          confirm,
          locale,
          position,
          ref: contentRef,
        })}
      </Content>
    </ThemeProvider>
  );
};

export default StaticWrapper;
