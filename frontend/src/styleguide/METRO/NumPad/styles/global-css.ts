import { createGlobalStyle } from 'styled-components';

interface GlobalStyleProps {
  display: boolean;
}

const GlobalStyle = createGlobalStyle<GlobalStyleProps>`
html {
  touch-action: manipulation;
}
${(props) =>
  props.display
    ? `
body {
overflow-y: hidden;
-webkit-overflow-scrolling: touch;
 
}
`
    : ``}
`;

export default GlobalStyle;
