import React from 'react';
import { Provider } from 'react-redux';
import { configureStore } from 'modules/store';
import { storiesOf } from '@storybook/react';
import Number from './Number';

const stories = storiesOf('METRO/NumPad', module);

const ReduxProvider = ({ children, reduxStore }) => {
  return <Provider store={reduxStore}>{children}</Provider>;
};

stories.add('Default', () => {
  const store = configureStore();
  const story = (
    <div>
      <ReduxProvider reduxStore={store}>
        <Number onChange={(value) => {}} position="startTopLeft" inline placeholder="" value="" decimal={2} keyValidator={() => true} children={[]} cancel={() => {}} negative />
      </ReduxProvider>
    </div>
  );

  return story;
});
