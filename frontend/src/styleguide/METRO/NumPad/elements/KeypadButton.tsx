import React, { memo } from 'react';

interface ButtonWrapperProps {
  value: string;
  click: Function;
  disabled: boolean;
}

export const KeypadButton = memo<ButtonWrapperProps>(({ value, click, disabled }) => {
  return (
    <button className="ixc-keypad-button" onClick={() => click([value])} disabled={disabled} data-cy={`num-pad-button-${value}`}>
      {value}
    </button>
  );
});
