import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import Color from 'color';
import { media } from '../styles/media-templates';

const BackgroundPanel = styled.div`
  ${(props) =>
    props.theme.coords
      ? ``
      : `
  position: absolute;
  padding: 0;
  margin: 0;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  z-index: 10000;  
`};
`;

interface ContainerProps {
  width?: string;
  position: string;
}

const Container = styled.div<ContainerProps>`
  display: flex;
  width: ${(props) => (props.width ? props.width : '100%')};
  height: 100%;
  align-items: ${(props) => props.position};
  justify-content: center;
  * > ::-webkit-scrollbar {
    width: 4px;
  }

  /* Track */
  * > ::-webkit-scrollbar-track {
    background: #f1f1f1;
  }

  /* Handle */
  * > ::-webkit-scrollbar-thumb {
    background: #546e7a;
  }
`;

interface BackdropProps {
  width?: string;
  position: string;
  coords: any;
}

const Backdrop = styled.div<BackdropProps>`
  width: ${(props) => (props.width ? props.width : '100%')};
  ${(props) =>
    props.coords
      ? `height: 300px;
    `
      : `height: ${props.position === 'center' ? '100vh' : '300px'}
      background-color: ${Color('#000').alpha(0.4).string()};
      `}
`;

interface ContentProps {
  position: string;
  coords: any;
}

const Content = styled.div<ContentProps>`
  * {
    font-family: ${(props) => props.theme.global.fontFamily};
  }
  font-size: 1em;
  z-index: 10000;
  transform: translateX(-100%);
  ${(props) => (props.position === 'center' ? 'height: 100vh' : '')}
  ${(props) =>
    props.coords
      ? `
    position: absolute;
    top: ${props.coords.top};    
    left: ${props.coords.left};
    `
      : `
    position: absolute;
    top: 191px;
    right: -390px;
    display: flex;    
    justify-content: center;
    align-items: ${props.position};
  `};
  ${media.desktop`right: ${(props: any) => (props.coords ? props.coords.right : '150px')};`}
  ${media.mobile`left: 0;`}
  ${media.mobile`right: auto;`}
  ${media.mobile`width: 100wv;`}
`;

interface WrapperProps {
  position: string;
  children: any;
  coords: any;
}

const Wrapper = ({ position, children, coords }: WrapperProps) => {
  return (
    <BackgroundPanel>
      <Container position={position}>
        {position.startsWith('start') || position === 'fullscreen' ? (
          <Content position={position} coords={coords}>
            {children}
          </Content>
        ) : (
          <Backdrop position={position} coords={coords}>
            <Content position={position} coords={coords}>
              {children}
            </Content>
          </Backdrop>
        )}
      </Container>
    </BackgroundPanel>
  );
};

Wrapper.displayName = 'Wrapper';

Wrapper.propTypes = {
  children: PropTypes.oneOfType([PropTypes.object, PropTypes.array]).isRequired,
  position: PropTypes.string.isRequired,
};

export default Wrapper;
export { Container, Content };
