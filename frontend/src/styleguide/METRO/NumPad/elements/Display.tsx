import { i18n } from 'i18n';
import React from 'react';
import useLongPress from '../hooks/useLongPress';

interface DisplayWrapperProps {
  value: string;
  backspace: () => void;
  longPressBackspace: () => void;
  display?: boolean;
}

const DisplayWrapper = ({ value, backspace, longPressBackspace, display }: DisplayWrapperProps) => {
  const backspaceLongPress = useLongPress(longPressBackspace, 1000);

  return (
    <section className="ixc-keypad-input-wrapper" style={{ display: display ? 'flex' : 'none' }}>
      <section className="ixc-keypad-input-display">
        <input role="display" readOnly autoFocus={display} className={`ixc-keypad-input${value?.length !== 0 ? '' : ' ixc-keypad-input-placeholder'}`} value={value} placeholder={i18n('metro.measurements.measurePlaceholder')} />
      </section>
    </section>
  );
};

export default DisplayWrapper;
