import React, { useState, useEffect, forwardRef, useCallback, ChangeEvent } from 'react';
import useOnClickOutside from 'use-onclickoutside';
import { Input } from 'antd';
import { DashboardOutlined, DownOutlined, LeftOutlined, RightOutlined, UpOutlined } from '@ant-design/icons';
import { i18n } from 'i18n';
import { BackspaceIcon } from 'styleguide/shared/svg';
import { SelectedCell } from 'modules/state';
import { KeypadButton } from './KeypadButton';
import Display from './Display';
import useKeyboardInput from '../hooks/useKeyboardInput';

const getContainerStyle = (position: any) => {
  if (position === 'fullscreen') {
    return {
      width: '100vw',
      height: '100vh',
      fontSize: '1.2em',
    };
  }
  return {
    width: '500px',
    height: '300px',
  };
};

export interface KeyPadProps {
  label: string;
  confirm: Function;
  handleTableNav: (type: string) => void;
  cancel: Function;
  displayRule: Function;
  validation: Function;
  keyValid: Function;
  value: number | string;
  sync: boolean;
  update: Function;
  position: string;
  selectedCell: SelectedCell;
  gage: string | null;
  setGage: (newGage: string) => void;
}

const KeyPad = forwardRef(function KeyPad({ displayRule, position, validation, confirm, cancel, keyValid, value, update, sync, handleTableNav, selectedCell, gage, setGage }: KeyPadProps, ref: any) {
  const keypadKeys = ['7', '8', '9', '4', '5', '6', '1', '2', '3', '-', '0', '.'];
  const keyboardKeys = [...Array(10)].map((v, index) => index.toString());
  const [inputValue, setInputValue] = useState(value);
  const [gageEntry, setGageEntry] = useState(false);
  const keyboard = useKeyboardInput(
    inputValue.toString(),
    keyboardKeys.filter((key) => keyValid(inputValue, key)),
  );

  function computeNextKey(newValue: string, key: string) {
    let computedValue;
    if (keyValid(inputValue, key) && !gageEntry) {
      const inputString = inputValue.toString();
      if (key === '-') {
        computedValue = inputString.charAt(0) === '-' ? inputString.substr(1) : `-${inputValue}`;
      } else if (key === '.') {
        const leadingZero = ['', '-'].includes(inputString);
        computedValue = `${inputValue}${leadingZero ? '0' : ''}${key}`;
      } else {
        computedValue = newValue;
      }
      setInputValue(computedValue);
      if (sync) {
        update(computedValue);
      }
    }
  }

  const isButtonElement = (ele: HTMLElement, depth: number): boolean => {
    if (depth >= 5 || !ele) return false;
    if (ele.id?.includes('measurement-button')) return true;
    return ele.parentElement ? isButtonElement(ele.parentElement, depth + 1) : false;
  };

  useOnClickOutside(ref, (e) => {
    const ele = e.target as HTMLElement;
    if (!isButtonElement(ele, 0)) cancel();
  });

  // Reload props.value into the state
  useEffect(() => {
    setInputValue(value);
  }, [value]);

  const handleConfirm = (padValue: string | number) => {
    if (padValue.toString().length > 0) {
      setInputValue('');
      confirm(padValue, keyboard.keyDownEvent?.shiftKey, keyboard.keyDownEvent?.ctrlKey);
    }
  };

  const handleSetGage = (newGage: string) => {
    setGage(newGage);
  };

  const handleOpenGageEntry = () => {
    setGageEntry(true);
    setInputValue('');
    keyboard.virtualInteraction('reset');
  };

  const handleCloseGageEntry = () => {
    setGageEntry(false);
    setInputValue('');
    keyboard.virtualInteraction('reset');
  };

  useEffect(() => {
    if (keyboard.keyDownEvent && !gageEntry) {
      /** useKeyBaordInput set null when initializing the initialValue to avoid this computation before validation  */
      if (['Enter', 'Tab'].includes(keyboard.keyDownEvent.key) && validation(keyboard.value)) {
        if (!gageEntry) {
          handleConfirm(keyboard.value);
        } else {
          handleCloseGageEntry();
        }
      } else if (['Escape'].includes(keyboard.keyDownEvent.key)) {
        cancel();
      } else if (['Backspace'].includes(keyboard.keyDownEvent.key)) {
        if (keyboard.keyDownEvent.ctrlKey || keyboard.keyDownEvent.altKey) {
          setInputValue('');
        } else {
          setInputValue(keyboard.value);
        }
      } else {
        computeNextKey(keyboard.value, keyboard.keyDownEvent.key);
      }
    }
    if (gageEntry && keyboard.keyDownEvent?.key === 'Enter') handleCloseGageEntry();
  }, [keyboard.value, keyboard.keyDownEvent]);

  const onButtonClick = useCallback((clickedKey) => keyboard.virtualInteraction(clickedKey.toString()), []);

  const handleWindowResize = () => {
    if (ref.current) {
      const keypadContainer = ref.current.parentElement as HTMLDivElement;
      keypadContainer.style.left = `${window.innerWidth - 134}px`;
    }
  };

  useEffect(() => {
    window.addEventListener('resize', handleWindowResize);

    return function cleanup() {
      window.removeEventListener('resize', handleWindowResize);
    };
  }, []);

  return (
    <section ref={ref} className="ixc-keypad-container" style={getContainerStyle(position)}>
      <section className="ixc-keypad-keys">
        <button type="button" className={`ixc-keypad-button gage${gage && gage !== '' ? ' set' : ''}`} onClick={handleOpenGageEntry} style={{ display: !gageEntry ? 'flex' : 'none' }}>
          <DashboardOutlined />
          {i18n('metro.measurements.gage')}
        </button>
        <Display display={!gageEntry} value={displayRule(inputValue)} backspace={() => {}} longPressBackspace={() => {}} />
        <Input
          style={{
            display: gageEntry ? 'flex' : 'none',
          }}
          className={`ixc-keypad-gage-input${gage ? '' : ' ixc-keypad-input-placeholder'}`}
          value={gage || undefined}
          placeholder={i18n('metro.measurements.gagePlaceholder')}
          onChange={(e: ChangeEvent<HTMLInputElement>) => handleSetGage(e.target.value)}
        />
        <button type="button" className="ixc-keypad-button save" style={{ display: gageEntry ? 'flex' : 'none' }} onClick={handleCloseGageEntry}>
          {i18n('common.save')}
        </button>
      </section>
      <section className="ixc-keypad-keys">
        <section className="ixc-keypad-keys-column">
          <button type="button" className="ixc-keypad-button pass" data-cy="num-pad-button-pass" onClick={() => handleConfirm('Pass')}>
            {i18n('metro.measurements.pass')}
          </button>
          <button type="button" className="ixc-keypad-button fail" data-cy="num-pad-button-fail" onClick={() => handleConfirm('Fail')}>
            {i18n('metro.measurements.fail')}
          </button>
        </section>
        <section className="ixc-keypad-numbers">
          {keypadKeys.map((key) => (
            <KeypadButton key={`button-${key}`} click={onButtonClick} value={key} disabled={!keyValid(inputValue, key)} />
          ))}
        </section>
        <section className="ixc-keypad-keys-column">
          <button type="button" className="ixc-keypad-button enter" data-cy="num-pad-button-enter" onClick={() => handleConfirm(inputValue)} disabled={!validation(inputValue)}>
            <span>{i18n('metro.measurements.enter')}</span>
          </button>
          <button
            type="button"
            className="ixc-keypad-button backspace"
            data-cy="num-pad-button-backspace"
            // backspaceLongPress={() => setInputValue('')}
            onClick={() => keyboard.virtualInteraction('Backspace')}
          >
            <BackspaceIcon />
          </button>
        </section>
        <section className="ixc-keypad-footer">
          <button type="button" className="ixc-keypad-button footer" data-cy="num-pad-button-close" onClick={() => cancel()}>
            {i18n('metro.measurements.close')}
          </button>
          <section className="ixc-keypad-feature-controls">
            <span className="ixc-keypad-control-label">{`${i18n('metro.measurements.feature')} #${selectedCell.selectedFeature?.markerLabel || selectedCell.featureIndex + 1}`}</span>
            <section className="ixc-keypad-control-buttons">
              <button type="button" className="ixc-keypad-button footer control" data-cy="num-pad-button-prev-feature" onClick={() => handleTableNav('prev_feature')}>
                {/* {i18n('common.back')} */}
                <UpOutlined />
              </button>
              <div className="control-divider" />
              <button type="button" className="ixc-keypad-button footer control" data-cy="num-pad-button-next-feature" onClick={() => handleTableNav('next_feature')}>
                {/* {i18n('common.next')} */}
                <DownOutlined />
              </button>
            </section>
          </section>
          <section className="ixc-keypad-feature-controls">
            <span className="ixc-keypad-control-label">{`${i18n('metro.measurements.sample')} #${selectedCell?.selectedSample?.sampleIndex ? selectedCell.selectedSample.sampleIndex + 1 : ''}`}</span>
            <section className="ixc-keypad-control-buttons">
              <button type="button" className="ixc-keypad-button footer control" data-cy="num-pad-button-prev-sample" onClick={() => handleTableNav('prev_sample')}>
                {/* {i18n('common.back')} */}
                <LeftOutlined />
              </button>
              <div className="control-divider" />
              <button type="button" className="ixc-keypad-button footer control" data-cy="num-pad-button-next-sample" onClick={() => handleTableNav('next_sample')}>
                {/* {i18n('common.next')} */}
                <RightOutlined />
              </button>
            </section>
          </section>
        </section>
      </section>
    </section>
  );
});

export default KeyPad;
