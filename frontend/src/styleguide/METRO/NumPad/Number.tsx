import React from 'react';
import { SelectedCell } from 'modules/state';
import { KeyPad } from './elements';
import { NumPad, NumPadProps } from './NumPad';
import StaticWrapper, { StaticWrapperProps } from './StaticWrapper';
import { KeyPadProps } from './elements/KeyPad';
import './NumPad.less';

const positiveValidation = (value: string | number) => {
  // FIX -0 to be considered positive
  if (value === 0 && Object.is(value, -0)) {
    return false;
  }
  return parseFloat(value.toString()) >= 0;
};

const integerValidation = (value: string | number) => parseFloat(value.toString()) % 1 === 0;

const numberValidator = (decimal: number | boolean, negative: boolean) => (value: string) => {
  if (value === '-' && negative) {
    return true;
  }
  if (Number.isNaN(Number(value))) {
    return false;
  }

  const floatValue = parseFloat(value);

  if (!decimal && !integerValidation(floatValue)) {
    return false;
  }

  if (typeof decimal === 'number' && decimal > 0) {
    if ((floatValue.toString().split('.')[1] || '').length > decimal) {
      return false;
    }
  }

  if (!negative && !positiveValidation(floatValue)) {
    return false;
  }
  return true;
};

interface NumberInputProps {
  inline: boolean;
  children: any;
  keyValidator: Function;
  handleTableNav: (type: string) => void;
  decimal: boolean | number;
  negative: boolean;
  cancel: Function;
  style?: any;
  onChange?: Function;
  recordMeasurement?: Function;
  position?: string;
  placeholder?: string;
  value?: string;
  featureIndex: number;
  sampleIndex: number;
  measurementIndex: number;
  measurementId: string | null;
  selectedCell: SelectedCell;
  gage: string;
  setGage: (newGage: string) => void;
}

const NumberInput = ({ inline = false, children, keyValidator, decimal = true, negative = true, cancel, featureIndex, sampleIndex, measurementIndex, measurementId, handleTableNav, value, gage, setGage, ...props }: NumberInputProps) => {
  const validation = (numberValue: any) => numberValidator(decimal, negative)(numberValue);

  let validator = keyValidator;
  if (!validator) {
    validator = (numberValue: any) => numberValidator(decimal, negative)(numberValue);
  }

  const keyValid =
    (isValid: any) =>
    (keyValue = '', key: any) => {
      if (key === '-') {
        return keyValue.charAt(0) === '-' || isValid(key + value);
      }
      return isValid(key === '.' ? value + key + 1 : value + key);
    };

  const displayRule = (displayValue: any) => displayValue?.replace(/^(-)?0+(0\.|\d)/, '$1$2') || ''; // remove leading zeros

  const staticWrapperProps: StaticWrapperProps = { ...(props as unknown as StaticWrapperProps) };
  const keyPadProps: KeyPadProps = { ...(props as unknown as KeyPadProps) };

  if (inline) {
    return (
      <StaticWrapper {...staticWrapperProps} sampleIndex={sampleIndex} featureIndex={featureIndex} measurementIndex={measurementIndex} measurementId={measurementId} displayRule={displayRule}>
        <KeyPad {...keyPadProps} value={value || ''} validation={validation} keyValid={keyValid(validator)} displayRule={displayRule} cancel={() => cancel(false)} handleTableNav={handleTableNav} gage={gage} setGage={setGage} />
      </StaticWrapper>
    );
  }
  const numPadProps: NumPadProps = { ...(props as unknown as NumPadProps) };
  return (
    <NumPad {...numPadProps} formatInputValue={(inputValue: string | number) => inputValue.toString()} customInput={children} displayRule={displayRule}>
      <KeyPad {...keyPadProps} value={value || ''} validation={validation} keyValid={keyValid(validator)} displayRule={displayRule} handleTableNav={handleTableNav} gage={gage} setGage={setGage} />
    </NumPad>
  );
};

export default NumberInput;
export { numberValidator, positiveValidation, integerValidation };
