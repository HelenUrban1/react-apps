import React, { useState, useLayoutEffect, useCallback, useRef, useEffect, ReactElement } from 'react';
import { Portal } from 'react-portal';
import { ThemeProvider } from 'styled-components';
import Slide from '@mui/material/Slide';
import { InputField, Wrapper } from './elements';

import GlobalStyle from './styles/global-css';
import styles from './styles';

const getTransition = (show: boolean, position: string) => {
  let transition: any = Slide;
  let transitionProps: any = {
    in: show,
    direction: 'up',
    mountOnEnter: true,
    unmountOnExit: true,
  };
  if (position === 'flex-start') {
    transitionProps.direction = 'down';
  }
  if (position !== 'flex-start' && position !== 'flex-end') {
    transition = 'span';
    transitionProps = {};
  }
  return { transition, transitionProps };
};

export interface NumPadProps {
  children: any;
  onChange: Function;
  customInput: ReactElement | ReactElement[];
  placeholder: string;
  position: string;
  label: string;
  disabled: boolean;
  locale: string;
  formatInputValue: Function;
  theme: string | Object;
  value: string | number;
  sync: boolean;
  markers: string[];
  inline: boolean;
  decimal: number;
  displayRule: Function;
}

export const NumPad = ({ children, onChange, customInput, placeholder, position = 'center', formatInputValue, label, theme, locale = 'en', value = '', sync = false, markers = [], disabled = false, displayRule }: NumPadProps) => {
  const [show, setShow] = useState(false);
  const [padValue, setPadValue] = useState(formatInputValue(value));
  const [preValue, setPreValue] = useState<string | number>();
  const [customTheme, setCustomTheme] = useState<any>({});
  const inputRef = useRef<HTMLInputElement>();
  const contentRef = useRef<any>();

  useEffect(() => {
    if (show && padValue !== formatInputValue(value)) {
      setPadValue(formatInputValue(value));
    }
  }, [show]);

  if (value !== preValue) {
    setPreValue(value);
    setPadValue(formatInputValue(value));
  }

  const toggleKeyPad = () => {
    // debugger;
    setShow(!show);
  };

  const confirm = (val: string | number) => {
    if (show) {
      onChange(displayRule(val));
      setPreValue(0);
      setPadValue(formatInputValue(0));
    }
  };

  // Update internal state, if sync is true call the external onChange callback on each change
  const update = useCallback(
    (val: any) => {
      setPadValue(val);
      if (sync) {
        onChange(displayRule(val));
      }
    },
    [sync],
  );

  useLayoutEffect(() => {
    if (contentRef.current && inputRef.current) {
      const { width, height } = contentRef.current.getBoundingClientRect();
      const { top, bottom, left, right } = inputRef.current.getBoundingClientRect();
      const { innerWidth, innerHeight, pageXOffset, pageYOffset } = window;

      const coords: { [key: string]: { top: string; right: string } | { top: string; left: string } | undefined } = {
        startBottomLeft: {
          top: `${Math.min(innerHeight - height, bottom + pageYOffset)}px`,
          left: `${Math.min(innerWidth - width, left + pageXOffset)}px`,
        },
        startBottomRight: {
          top: `${Math.min(innerHeight - height, bottom + pageYOffset)}px`,
          right: `${Math.min(innerWidth - right + pageXOffset, innerWidth - width)}px`,
        },
        startTopLeft: {
          top: `${Math.max(top + pageYOffset - height, pageYOffset)}px`,
          left: `${Math.min(innerWidth - width, left + pageXOffset)}px`,
        },
        startTopRight: {
          top: `${Math.max(top + pageYOffset - height, pageYOffset)}px`,
          right: `${Math.min(innerWidth - right + pageXOffset, innerWidth - width)}px`,
        },
      };

      const newCoords = coords[position];

      setCustomTheme({ ...customTheme, coords: newCoords });
    } else {
      const newTheme = typeof theme === 'object' ? theme : styles(theme);
      setCustomTheme(newTheme);
    }
  }, [show]);

  const display: boolean = position !== 'flex-start' && position !== 'flex-end' ? show : true;
  const { transition, transitionProps } = getTransition(show, position);

  return (
    <section className={`ixc-numpad${display ? ' ixc-numpad-show' : ''}`}>
      <GlobalStyle display={display} />
      <ThemeProvider theme={customTheme}>
        <InputField placeholder={placeholder} showKeyPad={toggleKeyPad} inputValue={value.toString()} label={label} disabled={show || disabled} customInput={customInput} ref={inputRef} />
      </ThemeProvider>
      <Portal>
        {display &&
          React.createElement(
            transition,
            { ...transitionProps },
            <ThemeProvider theme={customTheme}>
              <Wrapper position={position} coords={customTheme.coords}>
                {React.cloneElement(children, {
                  cancel: toggleKeyPad,
                  confirm,
                  update,
                  value,
                  position,
                  label,
                  locale,
                  markers,
                  sync,
                  ref: contentRef,
                })}
              </Wrapper>
            </ThemeProvider>,
          )}
      </Portal>
    </section>
  );
};

export default NumPad;
