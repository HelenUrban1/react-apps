import React from 'react';
import { LeftOutlined, RightOutlined } from '@ant-design/icons';
import { Row, Col } from 'antd';

import './DataTableHeader.less';
import { useSelector } from 'react-redux';
import { AppState } from 'modules/state';
import MetaDataTile from 'styleguide/METRO/MetaDataTile/MetaDataTile';
import { i18n } from 'i18n';
import { SampleTableStatus } from '../../metro-types';
import { DataTableTimer } from './DataTableTimer';

interface Props {
  selectedSampleIndex: number;
  onClickLeft: () => void;
  onClickRight: () => void;
}

const DataTableHeader: React.FC<Props> = ({ selectedSampleIndex, onClickLeft, onClickRight }) => {
  const { samples, job } = useSelector((state: AppState) => state.metro);

  if (!samples) return <></>;

  const onSampleSearchClick = () => {
    // TODO: implement sample search
  };

  return (
    <Row className="ixc-data-table-header-container" gutter={[1, 1]}>
      <Col span={8}>
        <div className="ixc-data-table-job-status">
          {job?.sampling === 'Timed' ? (
            <DataTableTimer job={job} />
          ) : (
            <p>
              <span className="ixc-data-table-count ixc-data-table-total-samples">{samples.filter((sample) => sample.status === SampleTableStatus.Unmeasured).length + 1}</span>
              {i18n('metro.measurements.samplesRemaining').toLowerCase()}
            </p>
          )}
        </div>
      </Col>
      <Col span={8}>
        <div className="ixc-data-table-sample-counts">
          <p>
            <span className="ixc-data-table-count ixc-data-table-total-samples">{samples.length}</span>
            {i18n('metro.measurements.samples').toLowerCase()}
            <span className="ixc-data-table-count ixc-data-table-passed-samples">{samples.filter((sample) => sample.status === SampleTableStatus.Pass).length}</span>
            {i18n('metro.measurements.passed').toLowerCase()}
            <span className="ixc-data-table-count ixc-data-table-scrapped-samples">{samples.filter((sample) => sample.status === SampleTableStatus.Scrapped).length}</span>
            {i18n('metro.measurements.failed').toLowerCase()}
          </p>
        </div>
      </Col>
      <Col span={8}>
        <div className="ixc-data-table-sample-nav">
          <MetaDataTile
            active={false}
            data={null}
            icon={MetaDataTile.IconType.Search}
            label={`${i18n('metro.measurements.sample')} #`}
            value={`${Math.min(selectedSampleIndex + 1, samples.length)} of ${samples.length}`}
            onClick={onSampleSearchClick}
          />
          <button type="button" className="ixc-data-table-left-right-arrow" onClick={onClickLeft}>
            <LeftOutlined />
          </button>
          <button type="button" className="ixc-data-table-left-right-arrow" onClick={onClickRight}>
            <RightOutlined />
          </button>
        </div>
      </Col>
    </Row>
  );
};

export default DataTableHeader;
