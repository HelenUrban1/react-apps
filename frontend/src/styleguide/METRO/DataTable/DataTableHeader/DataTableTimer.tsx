import { message } from 'antd';
import { Job } from 'domain/jobs/jobTypes';
import { i18n } from 'i18n';
import moment from 'moment';
import React, { useEffect, useRef, useState } from 'react';

interface TimerProps {
  job: Job;
}

export const DataTableTimer = ({ job }: TimerProps) => {
  const [seconds, setSeconds] = useState(job.interval);
  const timerOn = useRef(false);

  useEffect(() => {
    if (!timerOn.current) {
      timerOn.current = true;
      setTimeout(() => {
        if (seconds <= 0) {
          // TODO: Trigger new sample event
          message.warning('Start next sample (placeholder)');
          setSeconds(job.interval);
        } else {
          setSeconds(seconds - 1);
        }
        timerOn.current = false;
      }, 1000);
    }
  });

  return (
    <p>
      <span className="ixc-data-table-count ixc-data-table-total-samples">{moment.utc(moment.duration(seconds * 1000).as('milliseconds')).format('HH:mm:ss')}</span>
      {i18n('metro.measurements.timeRemaining').toLowerCase()}
    </p>
  );
};
