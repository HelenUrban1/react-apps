import React, { useState, useEffect, RefObject } from 'react';
import { DownOutlined, UpOutlined } from '@ant-design/icons';
import { Table } from 'antd';
import { FixedSizeList, VariableSizeGrid as Grid, VariableSizeGrid } from 'react-window';
import ResizeObserver from 'rc-resize-observer';
import classNames from 'classnames';
import { Sample } from 'types/sample';
import { Characteristic } from 'types/characteristics';
import { Measurement } from 'types/measurement';
import { useDispatch, useSelector } from 'react-redux';
import { AppState } from 'modules/state';
import actions from 'modules/metro/metroActions';
import { i18n } from 'i18n';
import { FeatureList } from 'styleguide/METRO/DataTable/FeatureList/FeatureList';
import { SampleTableStatus, DataCellVariants } from '../metro-types';
import SampleHeader from '../SampleHeader/SampleHeader';
import DataCell from './DataCell/DataCell';
import SampleCell from './SampleCell/SampleCell';
import DataTableHeader from './DataTableHeader/DataTableHeader';
import MetaDataTile from '../MetaDataTile/MetaDataTile';
import { COL_BUFFER, COL_WIDTH, MIN_LIST_WIDTH, ROW_BUFFER, ROW_HEIGHT, ROW_OFFSET } from './TableDims';
import './DataTable.less';
import './FeatureHeader/FeatureHeader.less';
import SampleHeaderModal from './SampleHeaderModal/SampleHeaderModal';

interface VirtualProps {
  columns: any[];
  dataSource: any[];
  selectedFeature: Characteristic | null;
  selectedSample: Sample | null;
  onCellClick: ({ measurements, feature, sample }: { measurements: Measurement[] | undefined; feature: Characteristic; sample: Sample }, ref: RefObject<HTMLButtonElement>) => void;
  gridRef: RefObject<VariableSizeGrid<any>>;
  measureContainerRef: RefObject<HTMLElement>;
  gridHeight: number;
  isLastColumnSelected: boolean;
}

const VirtualTable = ({ columns, dataSource, selectedFeature, selectedSample, onCellClick, measureContainerRef, gridRef, gridHeight, isLastColumnSelected }: VirtualProps) => {
  const [tableWidth, setTableWidth] = useState(Math.max(document.body.getBoundingClientRect().width * 0.667, document.body.getBoundingClientRect().width - MIN_LIST_WIDTH));

  const resetVirtualGrid = () => {
    gridRef.current?.resetAfterIndices({
      rowIndex: 0,
      columnIndex: 0,
      shouldForceUpdate: true,
    });
  };

  useEffect(() => resetVirtualGrid, [tableWidth]);

  if (!columns) return <></>;

  const mergedColumns = columns.map((col) => {
    if (col.width) return col;

    return { ...col, width: COL_WIDTH };
  });

  const renderVirtualList = (rawData: readonly object[]) => {
    if (!columns) return <></>;

    const cellRender = (record: any, classname: string) => {
      if (!record || record.feature === null) return <DataCell variant={DataCellVariants.Disabled} />;
      const measurements = record.sample?.measurements?.filter((measurement: Measurement) => measurement.characteristicId === record.feature?.id) || [];
      return selectedFeature ? (
        <SampleCell
          key={`${record.key}-cell`}
          measurements={measurements}
          selectedFeature={selectedFeature}
          selectedSample={selectedSample}
          feature={record.feature}
          sample={record.sample}
          onCellClick={onCellClick}
          isSelectedLastCell={classname.includes('last') && isLastColumnSelected}
        />
      ) : (
        <DataCell variant={DataCellVariants.Disabled} />
      );
    };

    return (
      <Grid
        ref={gridRef}
        outerRef={measureContainerRef}
        overscanColumnCount={COL_BUFFER + 2}
        overscanRowCount={ROW_BUFFER + 2}
        initialScrollTop={ROW_OFFSET * ROW_HEIGHT}
        initialScrollLeft={COL_BUFFER * COL_WIDTH - tableWidth}
        className="metro-virtual-grid"
        columnCount={mergedColumns.length}
        columnWidth={() => COL_WIDTH}
        height={gridHeight}
        rowCount={rawData.length}
        rowHeight={() => ROW_HEIGHT}
        width={tableWidth + COL_WIDTH}
      >
        {({ columnIndex, rowIndex, style }) => {
          const classname = classNames('virtual-table-cell', {
            'virtual-table-cell-last': columnIndex === columns.length - 1,
          });

          return (
            <div className={classname} style={style}>
              {cellRender((rawData[rowIndex] as any)[columnIndex], classname)}
            </div>
          );
        }}
      </Grid>
    );
  };

  return (
    <ResizeObserver
      onResize={() => {
        setTableWidth(Math.max(document.body.getBoundingClientRect().width * 0.67, document.body.getBoundingClientRect().width - 300));
      }}
    >
      <Table dataSource={dataSource} className="virtual-table" columns={mergedColumns} pagination={false} components={{ body: renderVirtualList }} scroll={{ y: gridHeight }} />
    </ResizeObserver>
  );
};

interface Props {
  samples: Sample[];
  features: Characteristic[];
  onCellClick: ({ measurements, feature, sample }: { measurements: Measurement[] | undefined; feature: Characteristic; sample: Sample }, ref: RefObject<HTMLButtonElement>) => void;
  measurementMethod: string;
  sampleRef: RefObject<HTMLButtonElement>;
  featureRef: RefObject<HTMLDivElement>;
  featureDetailRef: RefObject<HTMLDivElement>;
  listContainerRef: RefObject<HTMLElement>;
  measureContainerRef: RefObject<HTMLElement>;
  gridRef: RefObject<VariableSizeGrid<any>>;
  listRef: RefObject<FixedSizeList<any>>;
  handleTableNav: (type: 'next_sample' | 'prev_sample' | 'next_feature' | 'prev_feature', skipScrap: boolean) => void;
  animating: string;
  setAnimation: (val: string) => void;
}

const DataTable: React.FC<Props> = ({ samples, features, onCellClick, measurementMethod, sampleRef, featureRef, featureDetailRef, listContainerRef, measureContainerRef, gridRef, listRef, handleTableNav, animating, setAnimation }: Props) => {
  const { initialized, selectedCell } = useSelector((state: AppState) => state.metro);
  const { featureIndex, sampleIndex, selectedFeature, selectedSample } = selectedCell;
  const dispatch = useDispatch();

  const [showSampleHeaderInfo, setShowSampleHeaderInfo] = useState<boolean>(false);
  const [clickedSample, setClickedSample] = useState<any>(null);

  const sampleColumns: any[] = [];
  for (let i = 0; i < COL_BUFFER; i++) sampleColumns.push(null);
  samples.forEach((s: Sample) => sampleColumns.push(s));
  // sampleColumns.push(null); extra column

  useEffect(() => {
    if (initialized && samples && features && !selectedCell.selectedFeature) dispatch(actions.doSelectCell({ ...selectedCell, selectedFeature: features[featureIndex], selectedSample: samples[sampleIndex] }));
  }, [samples, features]);

  useEffect(() => {
    setShowSampleHeaderInfo(true);
  }, [clickedSample]);

  useEffect(() => {
    if (!showSampleHeaderInfo) setClickedSample(null);
  }, [showSampleHeaderInfo]);

  const onClickUp = () => {
    handleTableNav('prev_feature', true);
  };

  const onClickDown = () => {
    handleTableNav('next_feature', true);
  };

  const onClickLeft = () => {
    handleTableNav('prev_sample', false);
  };

  const onClickRight = () => {
    handleTableNav('next_sample', false);
  };

  const keyPress = (event: KeyboardEvent) => {
    const keyMap: any = {
      ArrowLeft: onClickLeft,
      ArrowUp: onClickUp,
      ArrowRight: onClickRight,
      ArrowDown: onClickDown,
    };
    if (keyMap[event.key]) {
      keyMap[event.key]();
    }
  };

  useEffect(() => {
    document.addEventListener('keydown', keyPress, false);

    return () => {
      document.removeEventListener('keydown', keyPress, false);
    };
  });

  const dataSource: { [key: string]: { sample: Sample; feature: Characteristic | null } | string }[] = [];
  const bufferedFeatures: any[] = [null, null, ...features];
  for (let i = 0; i < ROW_BUFFER; i++) bufferedFeatures.push(null);
  bufferedFeatures.forEach((f: Characteristic | null) => {
    const colObj: { [key: string]: { sample: Sample; feature: Characteristic | null } | string } = {};
    sampleColumns.forEach((s: any, index: number) => {
      colObj[`${index}`] = { sample: s, feature: f };
    });
    colObj.id = `f-${f?.id}}`;
    dataSource.push(colObj);
  });

  const getSampleVariant = (sample: Sample | null, index: number): SampleTableStatus => {
    if ((selectedSample && sample?.id === selectedSample?.id) || (sample === null && index === sampleIndex + COL_BUFFER)) return SampleTableStatus.Selected;
    if (sample === null) return SampleTableStatus.Empty;
    switch (sample.status) {
      case 'Unmeasured':
        return SampleTableStatus.Unmeasured;
      case 'Scrap':
        return SampleTableStatus.Scrapped;
      case 'Fail':
        return SampleTableStatus.Fail;
      case 'Pass':
        return SampleTableStatus.Pass;
      default:
        return SampleTableStatus.Unmeasured;
    }
  };

  const handleClickSample = (s: Sample) => {
    dispatch(actions.doSelectCell({ ...selectedCell, sampleIndex: s.sampleIndex, selectedSample: samples[s.sampleIndex] }));
    setClickedSample(s);
  };

  const dataColumns: any[] = [];
  sampleColumns.forEach((s: any, index: number) => {
    dataColumns.push({
      title: (
        <SampleHeader
          key={s?.id}
          sampleRef={sampleRef}
          onClick={() => {
            handleClickSample(s);
          }}
          variant={getSampleVariant(s, index)}
          sampleNumber={s?.sampleIndex + 1 || ''}
          serialNumber={s?.serial || ''}
          criticalOnly={s?.featureCoverage === 'critical'}
          featureCoverage={s?.featureCoverage}
          status={s?.status}
          // isExtra={index === sampleColumns.length - 1}
        />
      ),
      sample: s,
      dataIndex: `${index}`,
      key: `col-key-${index}`,
    });
  });

  const onFeatureSearchClick = () => {
    // TODO: implement feature search
  };

  return (
    <section className="measurement-entry-container" id="sample-header-mount">
      <DataTableHeader onClickLeft={onClickLeft} onClickRight={onClickRight} selectedSampleIndex={sampleIndex} />
      {clickedSample && <SampleHeaderModal sample={clickedSample} setShowSampleHeaderInfo={setShowSampleHeaderInfo} showSampleHeaderInfo={showSampleHeaderInfo} measureContainerRef={measureContainerRef} />}
      <section className="data-table-container">
        <section className="metro-features-container">
          <MetaDataTile
            active={false}
            data={selectedFeature}
            icon={MetaDataTile.IconType.Search}
            label={`${i18n('metro.measurements.feature')} #`}
            value={`${featureIndex + 1} of ${features.length}`}
            footer={`${selectedFeature?.classification ? `${selectedFeature.criticality?.toLowerCase()} • ` : ''} ${measurementMethod}`}
            onClick={onFeatureSearchClick}
          />
          <section className="metro-feature-list-container">
            <section className="metro-feature-list-nav-container">
              <button type="button" className="ixc-data-table-up-down-arrow" onClick={onClickUp}>
                <UpOutlined />
              </button>
              <button type="button" className="ixc-data-table-up-down-arrow" onClick={onClickDown}>
                <DownOutlined />
              </button>
            </section>
            <FeatureList featureRef={featureRef} featureDetailRef={featureDetailRef} features={features} fullWidth animating={animating} setAnimation={setAnimation} listRef={listRef} listContainerRef={listContainerRef} />
          </section>
        </section>
        <VirtualTable
          columns={dataColumns}
          dataSource={dataSource}
          gridHeight={ROW_BUFFER * ROW_HEIGHT}
          onCellClick={onCellClick}
          gridRef={gridRef}
          measureContainerRef={measureContainerRef}
          selectedFeature={selectedFeature}
          selectedSample={selectedSample}
          isLastColumnSelected={sampleIndex + COL_BUFFER === sampleColumns.length - 1}
        />
      </section>
    </section>
  );
};

DataTable.defaultProps = {};

export default DataTable;
