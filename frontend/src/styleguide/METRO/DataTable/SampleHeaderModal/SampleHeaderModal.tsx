import { Button, Form, Input, Modal } from 'antd';
import React, { RefObject } from 'react';
import MetroActions from 'modules/metro/metroActions';
import { useDispatch, useSelector } from 'react-redux';
import { Sample } from 'types/sample';
import { i18n } from 'i18n';
import { AppState } from 'modules/state';

interface Props {
  showSampleHeaderInfo: boolean;
  setShowSampleHeaderInfo: (flag: boolean) => void;
  sample: Sample;
  measureContainerRef: RefObject<HTMLElement>;
}

const SampleHeaderModal: React.FC<Props> = ({ showSampleHeaderInfo, sample, setShowSampleHeaderInfo, measureContainerRef }) => {
  const [form] = Form.useForm();
  const dispatch = useDispatch();
  const MODAL_WIDTH = 300;
  const { operator } = useSelector((state: AppState) => state.metro);

  return (
    <Modal
      className="sample-header-modal metro-modal"
      visible={showSampleHeaderInfo}
      style={{
        position: 'absolute',
        top: `${measureContainerRef.current ? measureContainerRef.current.getBoundingClientRect().top : 281}px`,
        right: `${-4 - MODAL_WIDTH}px`,
      }}
      width={MODAL_WIDTH}
      closable={false}
      maskClosable
      onCancel={() => setShowSampleHeaderInfo(false)}
      getContainer="#sample-header-mount"
      footer={[
        <Button
          key="sample-modal-scrap-btn"
          className={`ixc-metro-btn ${sample.status !== 'Scrap' ? 'red-btn' : 'green-btn'}`}
          style={{
            fontSize: sample.status !== 'Scrap' ? '32px' : '24px',
            marginRight: '1px',
          }}
          onClick={() => {
            dispatch(
              MetroActions.doUpdateSample({
                ...sample,
                status: sample.status !== 'Scrap' ? 'Scrap' : 'Unmeasured',
                updatingOperatorId: operator.id,
              }),
            );
            setShowSampleHeaderInfo(false);
          }}
        >
          {sample.status !== 'Scrap' ? i18n('metro.measurements.scrap') : i18n('metro.measurements.restore')}
        </Button>,
        <Button
          key="sample-modal-save-btn"
          className="ixc-metro-btn"
          style={{
            marginLeft: '1px',
          }}
          onClick={() => {
            const serial = form.getFieldValue('serial');
            form.validateFields().then(() => {
              dispatch(
                MetroActions.doUpdateSample({
                  ...sample,
                  serial,
                  updatingOperatorId: operator.id,
                }),
              );
              setShowSampleHeaderInfo(false);
              form.resetFields();
            });
          }}
        >
          {i18n('common.save')}
        </Button>,
      ]}
    >
      <Form form={form} layout="vertical" name="sample-header-info" initialValues={{ serial: sample.serial }}>
        <Form.Item name="serial" label={i18n('metro.measurements.serialNum')} rules={[{ required: true, message: i18n('metro.measurements.serialReq') }]}>
          <Input />
        </Form.Item>
      </Form>
    </Modal>
  );
};

export default SampleHeaderModal;
