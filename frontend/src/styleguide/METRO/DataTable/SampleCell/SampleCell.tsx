import React, { RefObject, useEffect, useState } from 'react';
import { Sample } from 'types/sample';
import { Characteristic } from 'types/characteristics';
import { Measurement, MeasurementStatusEnum } from 'types/measurement';
import { getCellStatus, getDisplayValue } from 'utils/DataCalculation';
import DataCell from '../DataCell/DataCell';
import { DataCellVariants, SampleTableStatus } from '../../metro-types';

interface Props {
  sample: Sample;
  feature: Characteristic;
  measurements: Measurement[];
  selectedFeature: Characteristic;
  selectedSample: Sample | null;
  onCellClick: ({ measurements, feature, sample }: { measurements: Measurement[] | undefined; feature: Characteristic; sample: Sample }, ref: RefObject<HTMLButtonElement>) => void;
  isSelectedLastCell: boolean;
}

const SampleCell: React.FC<Props> = ({ sample, feature, measurements, selectedFeature, selectedSample, onCellClick, isSelectedLastCell }) => {
  const [measures, setMeasures] = useState<Measurement[]>(measurements);
  useEffect(() => {
    setMeasures(measurements);
  }, []);

  if (!feature || (!sample && !isSelectedLastCell)) {
    return <DataCell variant={DataCellVariants.Disabled} isHeader={false} isHighlighted={false} />;
  }

  let variant;
  let isHighlighted = false;

  if (sample?.status === SampleTableStatus.Scrapped) {
    variant = DataCellVariants.Disabled;
  } else if (measurements.some((m: Measurement) => m.status === MeasurementStatusEnum.Fail)) {
    variant = DataCellVariants.Fail;
  } else if (measurements.length === 0) {
    variant = DataCellVariants.Empty;
  } else {
    variant = DataCellVariants.Pass;
  }

  if (feature.id === selectedFeature.id) {
    if (sample?.id === selectedSample?.id || (sample === null && isSelectedLastCell)) {
      variant = DataCellVariants.Selected;
    } else {
      isHighlighted = true;
    }
  }

  const handleCellClick = (ref: RefObject<HTMLButtonElement>) => {
    onCellClick({ measurements: measures, sample, feature }, ref);
  };

  return (
    <DataCell
      onClick={(ref?: RefObject<HTMLButtonElement>) => {
        if (ref) handleCellClick(ref);
      }}
      variant={variant}
      status={getCellStatus(measures)}
      isHeader={false}
      isHighlighted={isHighlighted}
      // isExtra={isSelectedLastCell} // only needed if end buffer column is used
      isScrap={sample?.status === 'Scrap'}
      id={`${sample?.id}-${feature?.id}`}
    >
      {getDisplayValue(feature, measures)}
    </DataCell>
  );
};

export default SampleCell;
