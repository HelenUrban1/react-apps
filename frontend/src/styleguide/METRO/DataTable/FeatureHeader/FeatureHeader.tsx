import React from 'react';
import { Characteristic } from 'types/characteristics';
import DataCell from '../DataCell/DataCell';
import { DataCellVariants } from '../../metro-types';
import './FeatureHeader.less';

const FeatureHeader: React.FC<{ feature: Characteristic; isSelected: boolean }> = ({ feature, isSelected }) => (
  <DataCell variant={isSelected ? DataCellVariants.Selected : DataCellVariants.Empty} isHeader>
    <div className="ixc-data-table-feature-container">
      <div className="ixc-data-table-feature-number">{feature?.markerLabel}</div>
      <div className="ixc-data-table-feature-label">{feature?.fullSpecification}</div>
    </div>
  </DataCell>
);

export default FeatureHeader;
