export const ROW_HEIGHT = 60; // list/grid row height
export const ROW_BUFFER = 4; // extra rows out of view to render
export const ROW_OFFSET = 1; // 0 to set selected row to top of list/grid
export const COL_WIDTH = 132; // grid column width
export const COL_BUFFER = 14; // extra cols out of view to render
export const MIN_LIST_WIDTH = 300; // minimum width of feature list
