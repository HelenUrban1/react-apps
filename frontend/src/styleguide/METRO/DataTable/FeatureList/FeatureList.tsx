import React, { RefObject, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { CloseCircleOutlined, InfoCircleOutlined } from '@ant-design/icons';
import { List } from 'antd';
import { FixedSizeList, FixedSizeList as VList } from 'react-window';
import { Characteristic } from 'types/characteristics';
import actions from 'modules/metro/metroActions';
import { AppState } from 'modules/state';
import { DataCellVariants } from 'styleguide/METRO/metro-types';
import { ROW_BUFFER, ROW_HEIGHT } from '../TableDims';

interface VirtualProps {
  selectedFeature: Characteristic | null;
  fullWidth: boolean;
  handleFeatureClick: (e: any, feature: Characteristic) => void;
  handleShowDetails: () => void;
  featureRef: RefObject<HTMLDivElement>;
  listRef: RefObject<FixedSizeList<any>>;
  listContainerRef: RefObject<HTMLElement>;
  dataSource: any;
}

const VirtualList = ({ selectedFeature, fullWidth, handleFeatureClick, handleShowDetails, featureRef, listRef, listContainerRef, dataSource }: VirtualProps) => {
  const renderRow = ({ index }: any) => {
    const feature = dataSource[index];
    if (feature.id) {
      const isSelected = feature.id === selectedFeature?.id;
      return (
        <List.Item className="ixc-data-cell-row" key={`feature-list-item-${feature.id}`} onClick={(e: any) => handleFeatureClick(e, feature)}>
          <div ref={feature.id === selectedFeature?.id ? featureRef : null} className={`ixc-data-cell-outer ixc-data-cell-${isSelected ? DataCellVariants.Selected : DataCellVariants.Empty} ${fullWidth && 'ixc-data-cell-full-width'}`}>
            <div className="ixc-data-table-feature-container">
              <div className="ixc-data-table-feature-info">
                <div className="ixc-data-table-feature-number">{feature.markerLabel}</div>
                <div className="ixc-data-table-feature-label">{feature.fullSpecification}</div>
              </div>
              {isSelected && (
                <div className="ixc-data-table-feature-detail">
                  <InfoCircleOutlined onClick={handleShowDetails} />
                </div>
              )}
            </div>
          </div>
        </List.Item>
      );
    }
    return <List.Item className="ixc-data-cell-row-Empty" key={`feature-list-item-empty-${index}`} />;
  };

  return (
    <List id="ixc-metro-feature-list-id" className="ixc-metro-feature-list" itemLayout="vertical" dataSource={dataSource}>
      <VList ref={listRef} outerRef={listContainerRef} className="virtual-feature-list" height={ROW_BUFFER * ROW_HEIGHT} width="100%" itemCount={dataSource.length} itemSize={ROW_HEIGHT} overscanCount={ROW_BUFFER}>
        {({ index, style }) => {
          return (
            <div className="virtual-list-item" style={style}>
              {renderRow({ index })}
            </div>
          );
        }}
      </VList>
    </List>
  );
};

interface FeatureListProps {
  featureRef: RefObject<HTMLDivElement>;
  featureDetailRef: RefObject<HTMLDivElement>;
  listRef: RefObject<FixedSizeList<any>>;
  listContainerRef: RefObject<HTMLElement>;
  features: Characteristic[];
  fullWidth: boolean;
  animating: string;
  setAnimation: (val: string) => void;
}

export const FeatureList = ({ featureRef, featureDetailRef, listRef, listContainerRef, features, fullWidth = true, animating, setAnimation }: FeatureListProps) => {
  const { selectedCell } = useSelector((state: AppState) => state.metro);
  const { classifications, methods, operations } = useSelector((state: AppState) => state.session);
  const { selectedFeature } = selectedCell;
  const dispatch = useDispatch();
  const [showDetails, setShowDetails] = useState(false);

  useEffect(() => {
    if (animating !== '') {
      setTimeout(() => {
        setAnimation('');
      }, 700);
    }
  }, [animating]);

  const feats: any[] = [{ key: 'extra-B' }, { key: 'extra-A' }, ...features, { key: 'extra-1' }, { key: 'extra-2' }, { key: 'extra-3' }];

  const handleFeatureClick = (e: any, feature: Characteristic) => {
    dispatch(actions.doSelectCell({ ...selectedCell, selectedFeature: feature, featureIndex: features.findIndex((a: Characteristic) => a.id === feature.id) }));
  };

  const handleShowDetails = () => {
    setShowDetails(true);
  };

  return (
    <section className="ixc-data-table-features-parent">
      {showDetails && (
        <section ref={showDetails ? featureDetailRef : null} className="ixc-data-table-feature-details">
          <div className={`animationContainer ${animating}`}>
            <div className="ixc-data-table-feature-container">
              <div className="ixc-data-table-feature-info">
                <div className="ixc-data-table-feature-number">{selectedFeature?.markerLabel}</div>
                <div className="ixc-data-table-feature-label">{selectedFeature?.fullSpecification}</div>
              </div>
              <div className="ixc-data-table-feature-detail">
                <CloseCircleOutlined onClick={() => setShowDetails(false)} />
              </div>
            </div>
            <div className="detail-info">
              <div className="ixc-mdt-data-upper">
                <div className="ixc-mdt-data-left">
                  <div className="ixc-mdt-data-item">
                    <div className="ixc-mdt-item-label">Full Specification</div>
                    <div className="ixc-mdt-item-value">{selectedFeature?.fullSpecification}</div>
                  </div>
                  <div className="ixc-mdt-data-item">
                    <div className="ixc-mdt-item-label">Upper Limit</div>
                    {selectedFeature?.upperSpecLimit || '-'}
                  </div>
                  <div className="ixc-mdt-data-item">
                    <div className="ixc-mdt-item-label">Lower Limit</div>
                    {selectedFeature?.lowerSpecLimit || '-'}
                  </div>
                </div>
                <div className="ixc-mdt-data-right">
                  <div className="ixc-mdt-data-item">
                    <div className="ixc-mdt-item-label">Classification</div>
                    {classifications ? classifications[selectedFeature?.classification?.toString() || '']?.value || '-' : '-'}
                  </div>
                  <div className="ixc-mdt-data-item">
                    <div className="ixc-mdt-item-label">Inspection Method</div>
                    {methods ? methods[selectedFeature?.inspectionMethod?.toString() || '']?.value || '-' : ''}
                  </div>
                  <div className="ixc-mdt-data-item">
                    <div className="ixc-mdt-item-label">Operation</div>
                    {operations ? operations[selectedFeature?.operation?.toString() || '']?.value || '-' : ''}
                  </div>
                </div>
              </div>
              <div className="ixc-mdt-data-lower">
                <div className="ixc-mdt-data-item">
                  Comments:
                  {selectedFeature?.comments || '-'}
                </div>
              </div>
            </div>
          </div>
        </section>
      )}
      <VirtualList
        selectedFeature={selectedFeature}
        fullWidth={fullWidth}
        handleFeatureClick={handleFeatureClick}
        handleShowDetails={handleShowDetails}
        dataSource={feats}
        featureRef={featureRef}
        listRef={listRef}
        listContainerRef={listContainerRef}
      />
    </section>
  );
};
