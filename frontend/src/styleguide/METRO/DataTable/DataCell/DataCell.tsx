import React, { RefObject, useRef } from 'react';
import { DataCellVariants } from '../../metro-types';

import './DataCell.less';

interface Props {
  isHeader?: boolean;
  variant?: DataCellVariants;
  status?: string;
  isExtra?: boolean;
  isScrap?: boolean;
  children?: any;
  isHighlighted?: boolean;
  fullWidth?: boolean;
  onClick?: (ref?: RefObject<HTMLButtonElement>) => void;
  id?: string;
}

const DataCell: React.FC<Props> = ({ children, isHeader, variant, isHighlighted, onClick, fullWidth, status, isExtra, isScrap, id }: Props) => {
  const ref = useRef<HTMLButtonElement>(null);
  return (
    <button
      ref={ref}
      id={`${id}-ixc-data-cell-${variant}${isHeader ? '-header' : '-measurement'}`}
      type="button"
      onClick={() => {
        if (onClick && variant !== DataCellVariants.Disabled && !isExtra) onClick(ref);
      }}
      className={`ixc-data-cell-outer ixc-data-cell-${isScrap ? DataCellVariants.Disabled : variant} ${isHeader && 'ixc-data-cell-header'} ${isHighlighted && 'ixc-data-cell-highlighted'}  ${
        fullWidth && 'ixc-data-cell-full-width'
      } cell-status-${status}`}
      disabled={variant === DataCellVariants.Disabled || isExtra || isScrap}
    >
      <div className={`ixc-data-cell-inner `}>{children}</div>
    </button>
  );
};

DataCell.defaultProps = {
  variant: DataCellVariants.Empty,
  children: null,
  status: 'Unmeasured',
  isHeader: false,
  isHighlighted: false,
  fullWidth: false,
  onClick: () => {},
  isExtra: false,
  isScrap: false,
  id: '',
};

export default DataCell;
