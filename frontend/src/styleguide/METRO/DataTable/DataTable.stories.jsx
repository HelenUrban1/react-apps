import React from 'react';
import { storiesOf } from '@storybook/react';
import DataTable from './DataTable';
import { Status } from '../metro-types';

const stories = storiesOf('METRO/Data Table', module);

const samples = [
  { id: '1', serial: '233-12343 B 095', sampleNumber: 1, status: Status.Passed },
  { id: '2', serial: '233-12343 B 096', sampleNumber: 2, status: Status.Failed },
  { id: '3', serial: '233-12343 B 097', sampleNumber: 3, status: Status.Unmeasured, coverage: 'critical' },
  { id: '4', serial: '233-12343 B 098', sampleNumber: 4, status: Status.Scrapped },
  { id: '5', serial: '233-12343 B 099', sampleNumber: 5, status: Status.Unmeasured },
  { id: '6', serial: '233-12343 B 100', sampleNumber: 6, status: Status.Unmeasured },
  { id: '7', serial: '', sampleNumber: 7, status: Status.Unmeasured },
];

const features = [
  { id: '1', fullSpecification: '3.36-3.39 mm' },
  { id: '2', fullSpecification: '2.77 +/- 0.4 mm' },
  { id: '3', fullSpecification: '2.52 mm' },
  { id: '4', fullSpecification: '1.65 mm' },
  { id: '5', fullSpecification: '7.19 mm' },
  { id: '6', fullSpecification: 'R.10 MIN mm' },
];

const measurements = [
  {
    id: '1',
    values: ['2.52', '2.52'],
    status: Status.Passed,
    sample: '1',
    feature: '3',
  },
  {
    id: '2',
    values: ['2.90', '2.71'],
    status: Status.Failed,
    sample: '2',
    feature: '2',
  },
  {
    id: '3',
    values: ['3.37', '3.38'],
    status: Status.Passed,
    sample: '1',
    feature: '1',
  },
];

stories.add('Default', () => {
  const story = (
    <div>
      <DataTable measurementMethod="micrometer" samples={samples} features={features} measurements={measurements} onInfoClick={() => {}} onCellClick={() => {}} secondsRemaining={12345} />
    </div>
  );

  return story;
});
