import React from 'react';
import { LaptopOutlined } from '@ant-design/icons';

export const MasterStationIcons = () => {
  return (
    <table className="master-station-icons" data-cy="master-station-icons">
      <tbody>
        <tr className="master-station-icons-row" key="1" style={{ height: '64px' }}>
          <td key="1-A" style={{ height: '64px' }}>
            <LaptopOutlined className="master-icon" style={{ color: 'blue', fontSize: '64px' }} />
          </td>
          <td key="1-B" style={{ height: '64px' }}>
            <LaptopOutlined className="master-icon" style={{ color: 'green', fontSize: '64px' }} />
          </td>
          <td key="1-C" style={{ height: '64px' }}>
            <LaptopOutlined className="master-icon" style={{ color: 'orange', fontSize: '64px' }} />
          </td>
        </tr>
        <tr className="master-station-icons-row" key="2">
          <td key="2-A">
            <LaptopOutlined className="master-icon" style={{ color: 'purple', fontSize: '64px' }} />
          </td>
          <td key="2-B">
            <LaptopOutlined className="master-icon" style={{ color: 'white', fontSize: '64px' }} />
          </td>
          <td key="2-C">
            <LaptopOutlined className="master-icon" style={{ color: 'yellow', fontSize: '64px' }} />
          </td>
        </tr>
        <tr className="master-station-icons-row" key="3">
          <td key="1-A">
            <LaptopOutlined className="master-icon" style={{ color: 'red', fontSize: '64px' }} />
          </td>
          <td key="2-B">
            <LaptopOutlined className="master-icon" style={{ color: 'goldenrod', fontSize: '64px' }} />
          </td>
          <td key="3-C">
            <LaptopOutlined className="master-icon" style={{ color: 'teal', fontSize: '64px' }} />
          </td>
        </tr>
      </tbody>
    </table>
  );
};
