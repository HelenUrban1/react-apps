import React from 'react';
import { storiesOf } from '@storybook/react';
import Profile from './Profile';

const stories = storiesOf('METRO/Profile', module);

stories.add('Default', () => {
  const story = (
    <>
      <h1>User Variant</h1>
      <div style={{ padding: '50px 0', backgroundColor: 'black', display: 'flex', alignItems: 'flex-start', justifyContent: 'space-around' }}>
        <Profile title="User With Avatar" imageUrl="/images/signin.jpg" variant={Profile.Variants.User} />
        <Profile title="User With Click Handler" onClick={() => alert('Click triggered')} variant={Profile.Variants.User} />
        <Profile title="User Withtheverylongname" variant={Profile.Variants.User} />
      </div>
      <br />
      <h1>Station Variant</h1>
      <div style={{ padding: '50px 0', backgroundColor: 'black', display: 'flex', alignItems: 'flex-start', justifyContent: 'space-around' }}>
        <Profile title="First Station" onClick={() => alert('Click triggered')} variant={Profile.Variants.Station} />
        <Profile title="Second Station" variant={Profile.Variants.Station} />
      </div>
    </>
  );

  return story;
});
