import React, { ReactNode } from 'react';
import { UserOutlined, LaptopOutlined } from '@ant-design/icons';
import { MasterStationIcons } from './MasterStationIcons';
import md5 from 'md5';

import { profileColorMap } from 'view/global/defaults';
import './Profile.less';

enum Variant {
  User,
  Station,
}

interface Props {
  onClick: (id: string) => void;
  // eslint-disable-next-line react/require-default-props
  imageUrl?: string;
  title: string;
  // eslint-disable-next-line react/require-default-props
  id: string;
  variant: Variant;
  isMaster?: boolean;
}

const Icons = {
  [Variant.User]: <UserOutlined />,
  [Variant.Station]: <LaptopOutlined />,
};

const Profile = ({ imageUrl, id, title, onClick, variant, isMaster = false }: Props) => {
  const IconType: ReactNode = isMaster ? <MasterStationIcons /> : Icons[variant];

  // Use a color from the profile color map based on the title or id hash
  const hashMod: number = parseInt(md5(id || title), 16) % profileColorMap.length;
  const color = profileColorMap[hashMod];

  return (
    <button type="button" onClick={() => onClick(id)} className={`ixc-profile-container ${isMaster ? 'ixc-profile-container-master' : ''}`}>
      <div className="ixc-profile" style={{ backgroundColor: isMaster ? 'transparent' : color }}>
        {imageUrl ? <img className="ixc-profile-image" src={imageUrl} alt="Avatar" /> : IconType}
      </div>
      <h2 className="ixc-profile-title">{title}</h2>
    </button>
  );
};

Profile.Variants = Variant;

export default Profile;
