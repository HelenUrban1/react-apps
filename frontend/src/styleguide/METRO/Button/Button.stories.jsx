import React from 'react';
import { storiesOf } from '@storybook/react';
import Button from './Button';

const stories = storiesOf('METRO/Button', module);

stories.add('Default', () => {
  const story = (
    <div style={{ padding: '20px' }}>
      <Button onClick={() => alert('Clicked.')}>Click Me</Button>
    </div>
  );

  return story;
});
