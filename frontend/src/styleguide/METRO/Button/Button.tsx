import React from 'react';
import './Button.less';

interface Props {
  onClick: () => void;
  children: any;
  fullWidth?: boolean;
}

const TouchButton = ({ onClick, children, fullWidth }: Props) => {
  return (
    <button type="button" className={`ixc-metro-btn ixc-touch-button ${fullWidth && 'ixc-touch-button-full-width'}`} onClick={onClick}>
      {children}
    </button>
  );
};

TouchButton.defaultProps = {
  fullWidth: false,
};

export default TouchButton;
