import React, { useState } from 'react';
import { DesktopOutlined, LogoutOutlined, UserOutlined } from '@ant-design/icons';
import { Popover } from 'antd';

import './NavHeader.less';
import { Station } from 'types/station';
import { i18n } from 'i18n';

interface Props {
  station: Station;
  operator: any;
  avatar?: string;
  stations: Station[];
  onLogout: Function;
  onStationSelect: (station: Station) => void;
  onLogoClick: Function;
}

const NavHeader = ({ station, operator, avatar, stations = [], onLogout, onStationSelect, onLogoClick }: Props) => {
  const [showStationPopover, setShowStationPopover] = useState(false);

  const hideStationPopover = () => {
    setShowStationPopover(false);
  };

  const handleShowStation = (visible: boolean) => {
    setShowStationPopover(visible);
  };

  const handleSelectStation = (newStation: Station) => {
    onStationSelect(newStation);
    hideStationPopover();
  };

  const StationPopover = (
    <>
      {stations.map((currentStation: Station) => (
        <button className="nav-menu-option" key={currentStation.name} data-cy={`station-menu-option-${currentStation.name}`} type="button" onClick={() => handleSelectStation(currentStation)}>
          <DesktopOutlined />
          <span>{currentStation.name}</span>
        </button>
      ))}
    </>
  );

  const UserPopover = (
    <button className="nav-menu-option" type="button" onClick={() => onLogout()} data-cy="metro_logout_button">
      <LogoutOutlined />
      <span>Logout</span>
    </button>
  );

  return (
    <header className="ixc-nav-header-container">
      <button className="ixc-nav-header-logo-button" type="button" onClick={() => onLogoClick()}>
        <img className="ixc-nav-header-logo" alt="InspectionXpert Logo" src="/images/Icon Logo.svg" />
      </button>
      <Popover content={StationPopover} placement="bottomLeft" overlayClassName="ixc-popover-container" trigger="click" visible={showStationPopover} onVisibleChange={handleShowStation}>
        <button className="ixc-nav-header-station" type="button">
          <h4 className="ixc-nav-header-label">{i18n('metro.station.station')}</h4>
          <h3 className="ixc-nav-header-value">{station?.name || ''}</h3>
        </button>
      </Popover>
      <div className="ixc-nav-header-spacer" />
      <Popover content={UserPopover} placement="bottomRight" overlayClassName="ixc-popover-container" trigger="click">
        <button type="button" className="ixc-nav-header-user" data-cy="metro_user_dropdown">
          <div>
            <h4 className="ixc-nav-header-label">{i18n('metro.operator.operator')}</h4>
            <h3 className="ixc-nav-header-value">{operator?.fullName || operator?.email}</h3>
          </div>
          {avatar ? <img className="ixc-nav-header-avatar" alt="User Avatar" src={avatar} /> : <UserOutlined className="ixc-nav-header-avatar" />}
        </button>
      </Popover>
    </header>
  );
};

NavHeader.defaultProps = {
  avatar: null,
};

export default NavHeader;
