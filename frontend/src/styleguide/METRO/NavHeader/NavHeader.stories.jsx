import React from 'react';
import { storiesOf } from '@storybook/react';
import NavHeader from './NavHeader';

const stories = storiesOf('METRO/Nav Header', module);

stories.add('Default', () => {
  const story = (
    <>
      <NavHeader
        onLogoClick={() => alert('Logo Clicked')}
        onStationSelect={(station) => alert(`${station} Clicked`)}
        onLogout={() => alert(`Logout Clicked`)}
        station="Master"
        operator="Ryan Mee"
        avatar="images/ryan.png"
        stations={['Alpha', 'Beta', 'Charlie', 'Delta', 'Echo', 'Foxtrot']}
      />
    </>
  );

  return story;
});
