import React from 'react';
import { storiesOf } from '@storybook/react';
import Pill from './Pill';

const stories = storiesOf('METRO/Pill', module);

stories.add('Default', () => {
  const story = (
    <div style={{ padding: '20px' }}>
      <Pill label="Inactive" />
      <Pill isActive label="Active" />
    </div>
  );

  return story;
});
