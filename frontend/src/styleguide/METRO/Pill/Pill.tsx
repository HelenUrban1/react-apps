import React from 'react';
import { Tag } from 'antd';
import './Pill.less';

const { CheckableTag } = Tag;

interface Props {
  label: string;
  onSelect: ({ isChecked, label }: { isChecked: boolean; label: string }) => void;
  isActive: boolean;
}

const Pill = ({ onSelect, isActive, label }: Props) => {
  return (
    <CheckableTag className="ixc-pill-container" checked={isActive} onChange={(checked) => onSelect({ label, isChecked: checked })}>
      {label}
    </CheckableTag>
  );
};

export default Pill;
