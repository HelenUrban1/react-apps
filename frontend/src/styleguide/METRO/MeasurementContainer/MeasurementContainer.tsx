import React, { useState, useEffect, RefObject, useRef } from 'react';
import { Button } from 'antd';
import { i18n } from 'i18n';
import { Measurement } from 'types/measurement';
import './measurementContainer.less';
import { AppState } from 'modules/state';
import { useSelector } from 'react-redux';
import { DeleteOutlined, DownOutlined, UpOutlined } from '@ant-design/icons';

interface MeasurementContainerProps {
  deleteMeasurement: (id: string) => void;
  required: number | undefined;
  measureRef: RefObject<HTMLElement>;
  selectMeasurement: (index: number, measurement: Measurement | null) => void;
  top: number;
}

export const MeasurementContainer = ({ deleteMeasurement, required, measureRef, top, selectMeasurement }: MeasurementContainerProps) => {
  const { selectedCell } = useSelector((state: AppState) => state.metro);
  const ref = useRef<HTMLElement | null>(null);

  const measurements = selectedCell.selectedSample?.measurements?.filter((m: Measurement) => m.characteristicId === selectedCell.selectedFeature?.id);

  const handleClick = (index: number) => {
    selectMeasurement(index, measurements && measurements[index] ? measurements[index] : null);
  };

  const buildButtons = () => {
    const btns = measurements
      ?.filter((measurement: Measurement) => measurement.value.length > 0)
      .map((measurement: Measurement, index: number) => (
        <Button
          id={`measurement-button-${index}`}
          size="large"
          className={`measurement-button ${measurement.status} ${index === selectedCell.measurementIndex ? 'selected' : ''}`}
          data-cy={`measurement-button-${index}`}
          onClick={() => handleClick(index)}
        >
          {measurement.value}
        </Button>
      ));

    const emptyCount = Math.max(1, (required || 0) - (measurements?.length || 0));
    const bufferedButtons = btns ? [...btns] : [];

    for (let i = 0; i < emptyCount; i++) {
      const ind = bufferedButtons.length;
      bufferedButtons.push(
        <Button
          id={`measurement-button-${ind}`}
          size="large"
          className={`measurement-button ${bufferedButtons.length === selectedCell.measurementIndex ? 'selected' : ''}`}
          data-cy={`measurement-button-empty-${ind}`}
          onClick={() => handleClick(ind)}
        />,
      );
    }

    return bufferedButtons;
  };

  const [buttons, setButtons] = useState<any[]>([]);

  useEffect(() => {
    setButtons(buildButtons());
    if (ref.current) {
      const scrollLength = selectedCell.measurementIndex - 4;
      ref.current.scrollTop = Math.max((ref.current.firstElementChild?.getBoundingClientRect().height || 0) * scrollLength, 0);
    }
  }, [ref.current, selectedCell.measurementIndex, selectedCell.selectedMeasurement, selectedCell.featureIndex, selectedCell.sampleIndex]);

  const handlePrevMeasurement = () => {
    if (!measurements) return;
    if (selectedCell.measurementIndex > 0) selectMeasurement(selectedCell.measurementIndex - 1, measurements[selectedCell.measurementIndex - 1]);
  };

  const handleNextMeasurement = () => {
    if (!measurements) return;
    if (selectedCell.measurementIndex < buttons.length - 1) selectMeasurement(selectedCell.measurementIndex + 1, measurements[selectedCell.measurementIndex + 1] || null);
  };

  const handleDeleteMeasurement = () => {
    if (!selectedCell.selectedMeasurement) return;
    deleteMeasurement(selectedCell.selectedMeasurement.id);
  };

  return (
    <section ref={measureRef} className="measurements-container" data-cy="measurements-container" style={{ top }}>
      <span className="measurements-title" data-cy="measurements-title">
        {i18n('metro.measurements.measurements')}
      </span>
      {/* TODO: for when inspection planning is implemented <span className="measurements-required" data-cy="measurements-required">
        {i18n('metro.measurements.required', required)}
      </span> */}
      <section className="measurements-buttons" data-cy="measurements-buttons">
        <section ref={ref} className="measurements-section" data-cy="measurements">
          {buttons}
        </section>
        <section className="measurement-index" data-cy="measurement-index">
          {i18n('metro.measurements.progress', selectedCell.measurementIndex + 1, buttons.length)}
        </section>
        <section className="measurements-nav" data-cy="measurements-nav">
          <button id="measurement-button-up" data-cy="measurement-button-up" type="button" className="measurement-nav-btn ixc-data-table-up-down-arrow" onClick={handlePrevMeasurement}>
            <UpOutlined />
          </button>
          <button id="measurement-button-down" data-cy="measurement-button-down" type="button" className="measurement-nav-btn ixc-data-table-up-down-arrow" onClick={handleNextMeasurement}>
            <DownOutlined />
          </button>
        </section>
        <Button id="measurement-button-delete" data-cy="measurement-button-delete" className="measurement-delete-btn" size="large" icon={<DeleteOutlined />} onClick={handleDeleteMeasurement} disabled={!selectedCell.selectedMeasurement}>
          {i18n('common.delete')}
        </Button>
      </section>
    </section>
  );
};
