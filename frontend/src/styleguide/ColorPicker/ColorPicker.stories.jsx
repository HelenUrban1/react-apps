import React from 'react';
import { useState } from '@storybook/addons';
import { storiesOf } from '@storybook/react';
import { withKnobs } from '@storybook/addon-knobs';
import { SketchPicker } from 'react-color';

const stories = storiesOf('Design System/Color Picker', module).addDecorator(withKnobs);

stories.add('Sketch', () => {
  const [color, setColor] = useState('#ffffff');

  // setState when onChange event triggered
  const handleChange = (newColor) => {
    setColor(newColor.hex);
    console.log(`handleChange ${newColor.hex}`);
  };

  const handleChangeComplete = (newColor) => {
    setColor(newColor.hex);
    console.log(`handleChangeComplete ${newColor.hex}`);
  };

  const story = (
    <div>
      <SketchPicker color={color} onChange={handleChange} onChangeComplete={handleChangeComplete} />
    </div>
  );

  return story;
});
