import { Popover } from 'antd';
import React, { useState } from 'react';
import { SketchPicker } from 'react-color';
import './colorPicker.less';

// Red, Coral, Orange, Sunshine, Yellow, Dark Green, Green, Light Green, Brown, Pinkurple, IX Purple, Azure, Seafoam, Black, Gray, White
const colors = ['#D0021B', '#E56A54', '#F5A623', '#EFBE7D', '#F8E71C', '#417505', '#7ED321', '#B8E986', '#8B572A', '#963CBD', '#BD10E0', '#1682FF', '#00BBB0', '#000000', '#9B9B9B', '#FFFFFF'];

interface ColorProps {
  value: { hex: string }; // use when using useForm controls
  cValue?: { hex: string }; // use when not using useForm controls
  field: string;
  handleChangeColor: (color: string, field: string) => void;
  disabled?: boolean;
}

export const ColorPicker = ({ value, cValue, field, handleChangeColor, disabled = false }: ColorProps) => {
  const [display, setDisplay] = useState(false);

  const handleChange = (color: any) => {
    handleChangeColor(color.hex, field);
  };

  const handleDisplayChange = (e: any) => {
    setDisplay(e);
  };

  return (
    <div data-cy={`${field}-outer`}>
      <Popover
        overlayClassName="color-picker-overlay"
        className="color-picker-popover"
        trigger="click"
        visible={display}
        content={<SketchPicker color={cValue?.hex || value.hex} onChange={handleChange} presetColors={colors} />}
        onVisibleChange={handleDisplayChange}
        placement="rightBottom"
      >
        <div className={`color-picker-swatch${disabled ? ' disabled' : ''}`} onClick={() => {}} data-cy={`${field}-swatch`}>
          <div className="color-picker-color" style={{ backgroundColor: cValue?.hex || value.hex }} data-cy={`${field}-selected-color`} />
        </div>
      </Popover>
    </div>
  );
};

ColorPicker.defaultProps = {
  disabled: false,
};
