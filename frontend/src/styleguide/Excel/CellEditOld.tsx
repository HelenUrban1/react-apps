import Handsontable from 'handsontable';
import { HotTable } from '@handsontable/react';
import log from 'modules/shared/logger';
import { SheetData } from './types';

export default class CustomEditor extends Handsontable.editors.TextEditor {
  setValue(newValue: any) {
    log.debug('setValue', newValue);
    this.TEXTAREA.value = !newValue ? '' : newValue;
  }

  prepare(row: number, col: number, prop: string | number, TD: HTMLTableCellElement, originalValue: any, cellProperties: Handsontable.CellProperties) {
    super.prepare(row, col, prop, TD, originalValue ? originalValue.data : originalValue, cellProperties);
  }

  focus() {
    super.focus();
    this.TEXTAREA.select();
  }
}

export const setGetValueFunction = (table: SheetData[], editor: CustomEditor, currentSheet: number, printRefs: HotTable[] | null[]) => {
  if (!table || !editor) {
    return null;
  }
  if (!editor.col) {
    return null;
  }
  const cell = { ...table[currentSheet].rows[editor.row][editor.col] };
  if ((editor.TEXTAREA.value || cell.data) && cell.data !== editor.TEXTAREA.value) {
    cell.data = editor.TEXTAREA.value;
    editor.instance.setDataAtCell(editor.row, editor.col, cell);
    // Update hidden table for print
    if (printRefs && printRefs[currentSheet]) {
      printRefs[currentSheet]!.hotInstance.setDataAtCell(editor.row, editor.col, cell);
    }
  }
  return cell;
};
