import React, { useRef, useEffect } from 'react';
/* eslint-disable no-param-reassign, react/jsx-one-expression-per-line */
import Handsontable from 'handsontable';
import log from 'modules/shared/logger';
import { useDispatch } from 'react-redux';
import { styleToInline } from 'utils/styles';
import { TokenType, ReportTemplateSettings, ReportTemplate } from 'types/reportTemplates';
import { HotTable, HotColumn } from '@handsontable/react';
import { handleRichCell, removeTemp, removeToken, containsDynamic, addDraggedToken } from 'utils/editorUtil';
import { Layout } from 'antd';
import { ReportTemplateActions } from 'modules/reportTemplate/reportTemplateActions';
import { TemplateInput } from 'domain/reportTemplates/DataEditor';
import { SheetNavigation } from './SheetsNavigation';
import { ExcelHeader } from './ExcelHeader';
import EditorComponent from './CellEdit';
import { setFooter, setVerticalPicker, setHorizontalPicker, setHorizontalFooter } from './footerUtils';
import { SheetData, CellData, SettingsChange } from './types';
import 'handsontable/dist/handsontable.full.css';
import './Excel.less';

export interface CellChanges {
  sheet: string;
  address: string;
  data: CellData;
}

interface Props {
  title?: string;
  table: SheetData[];
  currentSheet: number;
  readOnly: boolean;
  setSheet: React.Dispatch<React.SetStateAction<number>>;
  saveChanges: (changes: Handsontable.CellChange[] | null, source: Handsontable.ChangeSource) => void;
  downloadFile: (type: string, font?: boolean, framed?: boolean) => void;
  refresh?: () => void;
  checkRefresh?: () => void;
  cancel?: () => void;
  confirm?: boolean;
  isDefault?: string;
  mode: string;
  token?: TokenType | null;
  clearToken?: () => void;
  direction?: 'Vertical' | 'Horizontal' | undefined;
  settings?: ReportTemplateSettings | null | undefined;
  edit?: (template?: ReportTemplate, copy?: boolean) => void;
  editFileName?: (title: string) => void;
  clone?: (template?: ReportTemplate, copy?: boolean) => void;
  deleteFile?: () => void;
  canEdit?: boolean;
  saved?: boolean;
  showPrompt: boolean;
  handleUpdatePrompt?: () => void;
  handleUpdateContext?: (change: TemplateInput, updates?: Handsontable.CellChange[]) => void;
  canRefresh?: boolean;
}

const ExcelTable = ({
  handleUpdateContext,
  table,
  currentSheet = 0,
  readOnly,
  setSheet,
  saveChanges,
  isDefault,
  mode,
  direction,
  title,
  showPrompt,
  handleUpdatePrompt,
  token,
  clearToken,
  checkRefresh,
  cancel,
  downloadFile,
  confirm,
  refresh,
  edit,
  editFileName,
  clone,
  deleteFile,
  settings,
  canEdit,
  saved,
  canRefresh,
}: Props) => {
  const handson = useRef<any>();
  const isMounted = useRef<boolean>(false);
  const templateChanges = useRef<SettingsChange[]>();
  const printRefs = useRef<HotTable[] | null[]>([]);
  const printTable = useRef<any>();
  const change = useRef<{ [key: string]: Handsontable.CellChange[] }>({});
  const lastClicked = useRef<CellData>();
  const tokenAddress = useRef({ row: 0, col: 0, address: 'A1' });
  const tempStyles = useRef<any>();
  const isDragging = useRef<{
    drag: boolean;
    token: undefined | TokenType;
    row: number;
    col: number;
    oldValue: string;
  }>({ drag: false, token: undefined, row: 0, col: 0, oldValue: '' });
  const currentDirection = useRef<'Vertical' | 'Horizontal' | undefined>();
  const currentSettings = useRef<ReportTemplateSettings>();
  const isHandlingDragDrop = useRef<boolean>(false);

  const dispatch = useDispatch();

  // unset any footer rows after a direction change
  useEffect(() => {
    if (isMounted.current && currentDirection.current && currentDirection.current !== direction) {
      currentDirection.current = direction;
      tempStyles.current = undefined;
      if (currentSettings.current?.footers) {
        currentSettings.current.footers[currentSheet] = {};
      }
      if (handson.current) {
        handson.current.hotInstance.render();
      }
    } else {
      currentDirection.current = direction;
      isMounted.current = true;
    }
  }, [direction]);

  // checks if rich text cell was dbl-clicked, displays message if it is
  const checkRichCell = (event: MouseEvent, coords: Handsontable.wot.CellCoords, td: HTMLTableCellElement) => {
    if (!table) return;
    handleRichCell(event, coords, td, table, currentSheet, 2);
  };
  useEffect(() => {
    if (!currentSettings.current && settings) {
      currentSettings.current = { ...settings };
    }
  }, [settings]);

  const setStyles = (clickedCell: any) => {
    if (!table || readOnly || !clickedCell || mode === 'report') return false;
    let shouldUpdate: SettingsChange[] | null = null;
    if (token) {
      clickedCell.data = clickedCell.data ? (clickedCell.data += ` ${token.token}`) : ` ${token.token}`;
      if (token.type === 'Dynamic') {
        let footer;
        if (currentSettings.current?.footers && currentSettings.current.footers[currentSheet]) {
          for (let index = 0; index < Object.keys(currentSettings.current.footers[currentSheet]).length; index++) {
            const key = Object.keys(currentSettings.current.footers[currentSheet])[index];
            if (
              currentSettings.current.footers[currentSheet][key] &&
              currentSettings.current.footers[currentSheet][key].footer !== -1 &&
              currentSettings.current.footers[currentSheet][key].footer > (direction === 'Vertical' ? clickedCell.row : clickedCell.col)
            ) {
              footer = currentSettings.current.footers[currentSheet][key].footer;
              break;
            }
          }
          if (footer) {
            const newStyles =
              direction === 'Vertical' ? setFooter(table[currentSheet].rows, footer, clickedCell.col, table[currentSheet].columns.length, clickedCell.row) : setHorizontalFooter(table[currentSheet].rows, footer, clickedCell.row, clickedCell.col);
            if (newStyles) {
              tempStyles.current = newStyles;
              shouldUpdate =
                direction === 'Vertical'
                  ? [
                      {
                        add: true,
                        tok: token.token,
                        address: clickedCell.address,
                        row: clickedCell.row,
                        col: clickedCell.col,
                        sheet: currentSheet,
                        footer,
                      },
                    ]
                  : [
                      {
                        add: true,
                        tok: token.token,
                        address: clickedCell.address,
                        row: clickedCell.row,
                        col: clickedCell.col,
                        sheet: currentSheet,
                        footer,
                      },
                    ];
            }
          } else {
            const newStyles = direction === 'Vertical' ? setVerticalPicker(table[currentSheet].rows, clickedCell.row, clickedCell.col) : setHorizontalPicker(table[currentSheet].rows[clickedCell.row], clickedCell.col, clickedCell.row);
            if (newStyles) {
              tempStyles.current = newStyles;
              shouldUpdate = [
                {
                  add: true,
                  tok: token.token,
                  address: clickedCell.address,
                  row: clickedCell.row,
                  col: clickedCell.col,
                  sheet: currentSheet,
                  footer: -1,
                },
              ];
            }
          }
        } else {
          const newStyles = direction === 'Vertical' ? setVerticalPicker(table[currentSheet].rows, clickedCell.row, clickedCell.col) : setHorizontalPicker(table[currentSheet].rows[clickedCell.row], clickedCell.col, clickedCell.row);
          if (newStyles) {
            tempStyles.current = newStyles;
            shouldUpdate = [
              {
                add: true,
                tok: token.token,
                address: clickedCell.address,
                row: clickedCell.row,
                col: clickedCell.col,
                sheet: currentSheet,
                footer: -1,
              },
            ];
          }
        }
      }

      if (clearToken) clearToken();
      if (handson.current) {
        handson.current.hotInstance.setDataAtCell(clickedCell.row, clickedCell.col, clickedCell);
      }
      tokenAddress.current = {
        row: clickedCell.row,
        col: clickedCell.col,
        address: clickedCell.address,
      };
    } else if (currentSettings.current?.footers && currentSettings.current?.footers[currentSheet] && currentSettings.current.footers[currentSheet][clickedCell.address]) {
      // footer already in settings
      if (currentSettings.current.footers[currentSheet][clickedCell.address].footer === -1) {
        // footer row not set; display footer picker
        const newStyles = direction === 'Vertical' ? setVerticalPicker(table[currentSheet].rows, clickedCell.row, clickedCell.col) : setHorizontalPicker(table[currentSheet].rows[clickedCell.row], clickedCell.col, clickedCell.row);
        if (newStyles) {
          tempStyles.current = newStyles;
        }
      } else {
        // footer row set; just display the footer styles
        const newStyles =
          direction === 'Vertical'
            ? setFooter(table[currentSheet].rows, currentSettings.current.footers[currentSheet][clickedCell.address].footer, clickedCell.col, table[currentSheet].columns.length, currentSettings.current.footers[currentSheet][clickedCell.address].row)
            : setHorizontalFooter(table[currentSheet].rows, currentSettings.current.footers[currentSheet][clickedCell.address].footer, clickedCell.row, currentSettings.current.footers[currentSheet][clickedCell.address].col);
        if (newStyles) {
          tempStyles.current = newStyles;
        }
      }
      tokenAddress.current = {
        row: clickedCell.row,
        col: clickedCell.col,
        address: clickedCell.address,
      };
    } else if (tempStyles.current && tempStyles.current[clickedCell.address]) {
      // Clicked cell is already being styled
      if (tempStyles.current[clickedCell.address].tempData === 'Last Data Row') {
        // set the footer for this section
        const newStyles =
          direction === 'Vertical'
            ? setFooter(table[currentSheet].rows, clickedCell.row, clickedCell.col, table[currentSheet].columns.length, tokenAddress?.current.row)
            : setHorizontalFooter(table[currentSheet].rows, clickedCell.col, clickedCell.row, tokenAddress?.current.col);
        if (newStyles) {
          tempStyles.current = newStyles;
          shouldUpdate =
            direction === 'Vertical'
              ? [
                  {
                    address: tokenAddress.current.address,
                    row: tokenAddress.current.row,
                    col: tokenAddress.current.col,
                    sheet: currentSheet,
                    footer: clickedCell.row,
                  },
                ]
              : [
                  {
                    address: tokenAddress.current.address,
                    row: tokenAddress.current.row,
                    col: tokenAddress.current.col,
                    sheet: currentSheet,
                    footer: clickedCell.col,
                  },
                ];
        }
      } else if (tempStyles.current[clickedCell.address].tempData === 'Remove') {
        // remove the footer for this section
        const newStyles =
          direction === 'Vertical'
            ? setVerticalPicker(table[currentSheet].rows, tokenAddress.current.row, tokenAddress.current.col)
            : setHorizontalPicker(table[currentSheet].rows[tokenAddress.current.row], tokenAddress.current.col, tokenAddress.current.row);
        if (newStyles) {
          tempStyles.current = newStyles;
          shouldUpdate = [
            {
              address: tokenAddress.current.address,
              row: tokenAddress.current.row,
              col: tokenAddress.current.col,
              sheet: currentSheet,
              footer: -1,
            },
          ];
        }
      } else {
        // clear the styles
        tempStyles.current = null;
        tokenAddress.current = {
          row: clickedCell.row,
          col: clickedCell.col,
          address: clickedCell.address,
        };
      }
    } else if (containsDynamic(clickedCell.data)) {
      // dynamic cell not already in settings
      let footer;
      if (currentSettings.current?.footers && currentSettings.current.footers[currentSheet]) {
        for (let index = 0; index < Object.keys(currentSettings.current.footers[currentSheet]).length; index++) {
          const key = Object.keys(currentSettings.current.footers[currentSheet])[index];
          if (currentSettings.current.footers[currentSheet][key] && currentSettings.current.footers[currentSheet][key].footer !== -1) {
            footer = currentSettings.current.footers[currentSheet][key].footer;
            break;
          }
        }
        if (footer) {
          const newStyles =
            direction === 'Vertical' ? setFooter(table[currentSheet].rows, footer, clickedCell.col, table[currentSheet].columns.length, clickedCell.row) : setHorizontalFooter(table[currentSheet].rows, footer, clickedCell.row, clickedCell.col);
          if (newStyles) {
            tempStyles.current = newStyles;
            shouldUpdate =
              direction === 'Vertical'
                ? [
                    {
                      address: clickedCell.address,
                      row: clickedCell.row,
                      col: clickedCell.col,
                      sheet: currentSheet,
                      footer,
                    },
                  ]
                : [
                    {
                      address: clickedCell.address,
                      row: clickedCell.row,
                      col: clickedCell.col,
                      sheet: currentSheet,
                      footer,
                    },
                  ];
          }
        } else {
          const newStyles = direction === 'Vertical' ? setVerticalPicker(table[currentSheet].rows, clickedCell.row, clickedCell.col) : setHorizontalPicker(table[currentSheet].rows[clickedCell.row], clickedCell.col, clickedCell.row);
          if (newStyles) {
            tempStyles.current = newStyles;
            shouldUpdate = [
              {
                address: clickedCell.address,
                row: clickedCell.row,
                col: clickedCell.col,
                sheet: currentSheet,
                footer: -1,
              },
            ];
          }
        }
      } else {
        const newStyles = direction === 'Vertical' ? setVerticalPicker(table[currentSheet].rows, clickedCell.row, clickedCell.col) : setHorizontalPicker(table[currentSheet].rows[clickedCell.row], clickedCell.col, clickedCell.row);
        if (newStyles) {
          tempStyles.current = newStyles;
          shouldUpdate = [
            {
              address: clickedCell.address,
              row: clickedCell.row,
              col: clickedCell.col,
              sheet: currentSheet,
              footer: -1,
            },
          ];
        }
      }
      tokenAddress.current = {
        row: clickedCell.row,
        col: clickedCell.col,
        address: clickedCell.address,
      };
    } else {
      // clear the styles
      tempStyles.current = null;
      tokenAddress.current = {
        row: clickedCell.row,
        col: clickedCell.col,
        address: clickedCell.address,
      };
    }
    return shouldUpdate;
  };

  const triggerTemplateUpdate = (extraChanges?: SettingsChange[]) => {
    if (!templateChanges.current && !extraChanges) {
      return;
    }
    let allChanges: SettingsChange[] = [];
    if (templateChanges.current) {
      allChanges = [...allChanges, ...templateChanges.current];
    }
    if (extraChanges) {
      allChanges = [...allChanges, ...extraChanges];
    }

    if (!allChanges || allChanges.length <= 0) {
      return;
    }
    const tokens = currentSettings.current?.tokens ? [...currentSettings.current.tokens] : [];
    const footers = currentSettings.current?.footers ? { ...currentSettings.current.footers } : {};
    const saved = currentSettings.current?.saved || false;

    let foot = -1;
    let sheet;
    // loop through changes
    for (let index = 0; index < allChanges.length; index++) {
      const changes = allChanges[index];
      if (changes.tok) {
        // handle token
        if (changes.add) {
          tokens.push(changes.tok);
        } else if (currentSettings.current?.tokens) {
          const tokenInd = tokens.findIndex((a) => a === changes.tok);
          if (tokenInd !== -1) tokens.splice(tokenInd, 1);
        }
      }
      if (changes.address !== undefined && changes.footer !== undefined && changes.row !== undefined && changes.col !== undefined && changes.sheet !== undefined) {
        // handle adding footer
        if (!footers[changes.sheet]) {
          footers[changes.sheet] = {};
        } else {
          footers[changes.sheet] = currentSettings.current?.footers && currentSettings.current?.footers[changes.sheet] ? { ...currentSettings.current?.footers[changes.sheet] } : {};
        }
        if (!changes.del) {
          footers[changes.sheet][changes.address] = {
            row: changes.row,
            col: changes.col,
            footer: changes.footer,
          };
          foot = changes.footer;
          sheet = changes.sheet;
        }
      }
      if (changes.sheet !== undefined && changes.sheet !== null && changes.del && changes.address && footers[changes.sheet] && footers[changes.sheet][changes.address]) {
        // handle removing a footer
        delete footers[changes.sheet][changes.address];
      }
    }

    if (sheet !== undefined && foot !== -1) {
      // loop through footers and update any unset ones with the new footer
      const keys = Object.keys(footers[sheet]);
      for (let index = 0; index < keys.length; index++) {
        const cell = footers[sheet][keys[index]];
        if (foot && foot !== -1 && cell.footer === -1) {
          cell.footer = foot;
        }
      }
    }
    currentSettings.current = { saved, footers, tokens };
    handson.current.hotInstance.render();

    if (handleUpdateContext) {
      handleUpdateContext({ settings: currentSettings.current }, change?.current?.edit);
    }
    templateChanges.current = undefined;
  };

  // handles drag and drop a token into a cell
  const handleDropInToken = (
    row: number,
    col: number,
    td: HTMLTableCellElement,
    tokenObj: TokenType,
    remove?: {
      drag: boolean;
      token: TokenType | undefined;
      row: number;
      col: number;
      oldValue: string;
    },
  ) => {
    if (!handson.current) {
      log.warn('no handson current reference');
      isHandlingDragDrop.current = false;
      return;
    }

    isHandlingDragDrop.current = true;

    let changeSheet = { ...table[currentSheet] };
    changeSheet = removeTemp(changeSheet, table, currentSheet);
    tempStyles.current = null;

    let oldCell;
    const { cell: newCell } = addDraggedToken(row, col, td, changeSheet, table, currentSheet, tokenObj.token, direction);
    if (!newCell) {
      log.warn('No New Cell');
      isHandlingDragDrop.current = false;
      return;
    }
    const dataToSet = [[row, col, newCell]];
    handson.current.hotInstance.setDataAtCell(row, col, newCell);

    if (remove?.row && remove?.col) {
      // removes the dragged token data from the original cell
      oldCell = { ...table[currentSheet].rows[remove.row][remove.col] };
      const cellToRemoveFrom = handson.current.hotInstance.getCell(remove.row, remove.col);
      const res = removeToken(remove.row, remove.col, cellToRemoveFrom, changeSheet, table, currentSheet, remove.token, remove.oldValue);

      if (res.updatedCell) {
        if (currentSettings.current?.tokens) {
          const tokenInd = currentSettings.current.tokens.findIndex((a) => a === remove.token?.token);
          if (tokenInd !== -1) currentSettings.current.tokens.splice(tokenInd, 1);
        }
        if (res.removed && currentSettings.current?.footers) {
          // deletes the footer info from the DB model if it was the last dynamic token in the cell
          delete currentSettings.current.footers[currentSheet][oldCell.address];
        }
        // Show updated token data in original cell
        handson.current.hotInstance.setDataAtCell(remove.row, remove.col, res.updatedCell);
      }
    }

    if (newCell && currentSettings.current) {
      if (!currentSettings.current.tokens) {
        currentSettings.current.tokens = [];
      }
      if (!currentSettings.current.tokens.includes(tokenObj.token)) {
        currentSettings.current.tokens.push(tokenObj.token);
      }
    }

    if (newCell && newCell.footerRow && currentSettings.current?.footers) {
      if (!currentSettings.current.footers[currentSheet]) {
        currentSettings.current.footers[currentSheet] = {};
      } else {
        currentSettings.current.footers[currentSheet] = currentSettings.current?.footers && currentSettings.current?.footers[currentSheet] ? { ...currentSettings.current?.footers[currentSheet] } : {};
      }
      currentSettings.current.footers[currentSheet][newCell.address] = {
        row,
        col,
        footer: newCell.footerRow,
      };
      if (currentSheet !== undefined) {
        let foot = newCell.footerRow;
        if (foot === -1) {
          const sheetFooters = currentSettings.current.footers[currentSheet];
          const footKey = Object.keys(sheetFooters).find((key) => sheetFooters[key].footer > -1);
          if (footKey) {
            foot = sheetFooters[footKey].footer;
          }
        }
        // loop through footers and update any unset ones with the new footer
        const keys = Object.keys(currentSettings.current.footers[currentSheet]);
        for (let index = 0; index < keys.length; index++) {
          const cell = currentSettings.current.footers[currentSheet][keys[index]];
          if (cell.footer === -1) {
            cell.footer = foot;
          }
        }
      }
    }

    handson.current.hotInstance.selectCell(row, col);
    const shouldUpdate = setStyles({ ...newCell, row, col });

    lastClicked.current = newCell;
    tokenAddress.current = { row, col, address: newCell.address };
    handson.current.hotInstance.render();

    // update the template settings info for tokens and footers
    if (shouldUpdate) {
      triggerTemplateUpdate([...shouldUpdate]);
    } else if (handleUpdateContext) {
      handleUpdateContext({ settings: currentSettings.current }, change?.current?.edit);
    }

    isHandlingDragDrop.current = false;
    isDragging.current = { drag: false, token: undefined, row: 0, col: 0, oldValue: '' };
  };

  // tracks which token is being dragged and what cell it belonged to originally if applicable
  const setDragging = (dragging: { drag: boolean; token: TokenType | undefined; row: number; col: number; oldValue: string }) => {
    isDragging.current = dragging;
  };

  const setSettingsFooter = (row: number, col: number, sheet: number, address: string, footer: number) => {
    // QUESTION: What is the point of this function?
    return { address, row, col, sheet, footer };
  };

  const updateTemplateSettingsFromEditor = ({
    //
    col,
    row,
    newCell,
    tok,
    add,
    del,
    updatedData,
  }: {
    col?: number;
    row?: number;
    newCell?: any;
    tok?: string;
    add?: boolean;
    del?: boolean;
    updatedData?: SheetData[];
  }) => {
    let newSettings: SettingsChange = {};
    if (tok && add) {
      newSettings.tok = tok;
      newSettings.add = add;
    }
    if (col && row && newCell) {
      newSettings = {
        ...newSettings,
        ...setSettingsFooter(row, col, currentSheet, newCell.address, newCell.footerRow),
      };
      newSettings.del = del;
    }

    if (templateChanges.current) {
      templateChanges.current.push(newSettings);
    } else {
      templateChanges.current = [newSettings];
    }

    if (updatedData) {
      dispatch({ type: ReportTemplateActions.SET_DATA, payload: updatedData });
    }

    if (handson.current) {
      handson.current.hotInstance.render();
    }
  };

  const catchEmptyCell = (changes: Handsontable.CellChange[] | null, source: Handsontable.ChangeSource) => {
    if (!changes) {
      return;
    }
    if (source === 'edit') {
      changes.forEach(([, , oldValue, newValue], index) => {
        if (!newValue) {
          let [row, col, newCell] = changes[index];
          row = parseInt(row.toString(), 10) + 1;
          col = parseInt(col.toString(), 10) + 1;
          newCell = { ...oldValue, data: ' ' };
          updateTemplateSettingsFromEditor({ col, row, newCell, tok: oldValue.data.trim(), add: false });
          if (!containsDynamic(newCell.data)) {
            if (currentSettings.current?.footers && currentSettings.current.footers[currentSheet] && currentSettings.current.footers[currentSheet][newCell.address]) {
              delete currentSettings.current.footers[currentSheet][newCell.address];
            }
            tempStyles.current = null;
            const updatedData = [...table];
            updatedData[currentSheet] = removeTemp(table[currentSheet], table, currentSheet);
            updateTemplateSettingsFromEditor({ col, row, newCell, del: true, updatedData });
          }
          changes[index][3] = newCell;
        }
      });
      triggerTemplateUpdate();
    }
  };

  // adds any necessary temp styles and adds a token when needed
  const handlePreRendering = (event: MouseEvent, coords: Handsontable.wot.CellCoords) => {
    if (!table || readOnly) return;
    if (!table[currentSheet] || !table[currentSheet].rows[coords.row] || !table[currentSheet].rows[coords.row][coords.col]) {
      log.debug('clicked header');
      return;
    }
    const clickedCell = table[currentSheet].rows[coords.row][coords.col];

    if (lastClicked.current && lastClicked.current.address === clickedCell.address && !lastClicked.current.tempStyle) return;
    const shouldUpdate = setStyles({ ...clickedCell, row: coords.row, col: coords.col });

    lastClicked.current = clickedCell;
    if (handson.current) {
      handson.current.hotInstance.render();
    }

    if (shouldUpdate) {
      triggerTemplateUpdate(shouldUpdate);
    }
  };

  // TODO: this function runs against every cell in table each time a cell changes
  // Opportunities for improvement include not added a div to each cell, not attaching
  // events to each cell, adding classes instead of styles to cells
  // Render cell.data in Handsontable
  const customRender = (instance: Handsontable, td: any, row: number, col: number, prop: React.ReactText, value: any, cellProperties: Handsontable.CellProperties) => {
    if (!table || !table[currentSheet] || !table[currentSheet].rows[row] || !table[currentSheet].rows[row][col]) {
      return;
    }
    const cell = table[currentSheet].rows[row][col];
    const propsRef = cellProperties;
    let val = cell.data;
    // Set cell styles
    if (td && cell) {
      const cellRef = td;
      if (tempStyles.current && tempStyles.current[cell.address] && tempStyles.current[cell.address].tempStyle) {
        cellRef.style = styleToInline(tempStyles.current[cell.address].tempStyle);
      } else {
        cellRef.style = styleToInline(cell.style);
      }

      // Formula cells
      if (cell.formula) {
        cellRef.style.backgroundColor = '#dde5ed'; // smoke
        if (mode === 'template') {
          val = `[Formula = ${cell.formula}]`;
        }
      }

      // Return rich text cells
      if (cell.rich) {
        Handsontable.dom.empty(td);
        Handsontable.dom.fastInnerHTML(cellRef, cell.tempData && cell.tempData !== '' ? cell.tempData : cell.data);
        propsRef.readOnly = true;
        if (readOnly) {
          cellRef.style.background = '#f5f5f5';
        }
        return;
      }

      // Set cell data
      if (tempStyles.current && tempStyles.current[cell.address] && tempStyles.current[cell.address].tempData) {
        val = tempStyles.current[cell.address].tempData;
      }

      // Create div element - used as target for drag and drop
      const d = document.createElement('div');
      Handsontable.dom.fastInnerHTML(d, val !== undefined ? val : '');
      d.style.height = '100%';
      const colspan = cell.colspan ? cell.colspan : 1;
      d.style.width = `${instance.getColWidth(col) * colspan}px`;
      d.className = 'cell-drop';

      // Set event listeners for drag and drop
      if (propsRef.readOnly) {
        cellRef.style.backgroundColor = '#f5f5f5';
      } else {
        Handsontable.dom.addEvent(d, 'mouseover', (event: any) => {
          event.preventDefault();
          if (isDragging.current.drag) {
            d.className = 'cell-drop hover';
            const editorRef = (instance.getActiveEditor() as any).editorComponent.mainElementRef.current;
            if (editorRef && editorRef.style.top !== '-500px') {
              editorRef.style.top = '-500px';
            }
          } else {
            d.className = 'cell-drop';
          }
        });
        Handsontable.dom.addEvent(d, 'mouseout', (event: any) => {
          event.preventDefault();
          if (isDragging.current.drag) {
            d.className = 'cell-drop';
          }
        });
        Handsontable.dom.addEvent(d, 'mouseup', (event: any) => {
          const target = event.target as HTMLDivElement;
          if (isDragging.current.drag && isDragging.current.token && (row !== isDragging.current.row || col !== isDragging.current.col)) {
            target.style.backgroundColor = 'rgba(0,0,100,0)';
            return handleDropInToken(row, col, td, isDragging.current.token, isDragging.current);
          }
          isDragging.current = { drag: false, token: undefined, row: 0, col: 0, oldValue: '' };
        });
        Handsontable.dom.addEvent(d, 'drop', (event: any) => {
          event.preventDefault();
          const ev = event as DragEvent;
          const tokenData = ev.dataTransfer!.getData('text/plain');
          if (tokenData) {
            const tokenObj = JSON.parse(tokenData);
            isDragging.current = { drag: true, token: tokenObj, row, col, oldValue: '' };
            handleDropInToken(row, col, td, tokenObj);
          }
        });
        Handsontable.dom.addEvent(d, 'dragenter', (event: any) => {
          if (event && event.target && (event.target as HTMLDivElement).className === 'cell-drop') {
            const target = event.target as HTMLDivElement;
            target.style.backgroundColor = '#E8E6F7';
          }
        });
        Handsontable.dom.addEvent(d, 'dragover', (event: any) => {
          event.preventDefault();
        });
        Handsontable.dom.addEvent(d, 'dragleave', (event: any) => {
          if (event && event.target && (event.target as HTMLDivElement).className === 'cell-drop') {
            const target = event.target as HTMLDivElement;
            target.style.backgroundColor = 'rgba(0,0,100,0.00)';
          }
        });
      }

      // Render
      Handsontable.dom.empty(td);
      td.appendChild(d);
    }
  };

  const updateData = (row: number, col: number, newData: string) => {
    if (!table || !table || currentSheet === undefined || !handson || !handson.current) return false;
    const cell = { ...table[currentSheet].rows[row][col] };
    if (newData.trim() !== cell.data) {
      cell.data = newData.trim();
      if (handson.current) {
        handson.current.hotInstance.setDataAtCell(row, col, cell);
      }
      const shouldUpdate = setStyles({ ...cell, row, col });

      lastClicked.current = cell;
      if (handson.current) {
        handson.current.hotInstance.render();
      }

      if (shouldUpdate) {
        triggerTemplateUpdate(shouldUpdate);
      }
    }
    return true;
  };
  // Render cell.data in Handsontable
  const printRender = (instance: Handsontable, td: any, row: number, col: number, prop: React.ReactText, value: any, cellProperties: Handsontable.CellProperties, index: number) => {
    if (!table || !table[index]) {
      return false;
    }
    const cell = table[index].rows[row][col];
    if (td && cell) {
      td.style = styleToInline(cell.style);
      if (cell.rich) {
        Handsontable.dom.empty(td);
        Handsontable.dom.fastInnerHTML(td, cell.data);
        return td;
      }
      value = cell.data;
      Handsontable.renderers.TextRenderer.apply(instance, [instance, td, row, col, prop, value, cellProperties]);
    }
    return true;
  };

  const printSizing = (sheet: SheetData) => {
    if (!sheet || sheet === null) return [];
    const columns: any[] = [];
    const blanks: number[] = [];
    let allwidth = 0;
    for (let i = 0; i < sheet.columns.length; i++) {
      if (sheet.rows[0][i] && sheet.rows[0][i].data !== ' ') {
        allwidth += sheet.columns[i];
      } else {
        let blank = true;
        for (let k = 0; k < sheet.rows.length; k++) {
          if (sheet.rows[k][i] && sheet.rows[k][i].data !== ' ') {
            allwidth += sheet.columns[i];
            blank = false;
            break;
          }
        }
        if (blank) {
          blanks.push(i);
          allwidth += 10;
        }
      }
    }
    for (let i = 0; i < sheet.columns.length; i++) {
      if (blanks.includes(i)) {
        columns.push((10 / allwidth) * 816);
        // TODO: set this number to match an actual portrait/landscape detected dpi. Right now I'm defaulting to 96 dpi portrait
      } else {
        columns.push((sheet.columns[i] / allwidth) * 816);
      }
    }

    return columns;
  };

  useEffect(() => {
    if (handson.current) {
      handson.current.hotInstance.updateSettings({
        width: 'auto',
      });
      handson.current.hotInstance.render();
    }
  }, [currentSheet, table]);

  useEffect(() => {
    // TODO: Is there a better way to handle this?
    if (handson.current) {
      // Trick handson into showing all rows, since it seems to hang on larger sheets and doesn't fully render
      setTimeout(() => {
        if (handson.current) {
          handson.current.hotInstance.render();
        }
      }, 100);
    }
  }, [settings, table]);

  const handleBlur = (e: any) => {
    if (editFileName && e.target.value !== title) editFileName(e.target.value);
    e.target.scrollLeft = 0;
  };

  const shouldSave = (changes: Handsontable.CellChange[] | null, source: Handsontable.ChangeSource) => {
    if (!changes) {
      return;
    }
    if (source === 'edit') {
      if (isHandlingDragDrop.current) {
        if (source && changes) {
          const key = source as string;
          if (!change.current[key]) {
            change.current[key] = [];
          }
          change.current[key] = [...change.current[source], ...changes];
        }
        return;
      }
      saveChanges(changes, source);
    }
  };

  return (
    <Layout className="excelComponent" data-cy="excel-component" hasSider>
      <section className="excelViewer">
        {table.length <= 3 && (
          <div className="PrintController" style={{ height: `${table.length * 11}in` }}>
            <div ref={printTable} className="PrintContainer">
              {(!readOnly || mode === 'template') &&
                table.map((sheet, index) => {
                  const columns = printSizing(sheet);
                  if (sheet) {
                    return (
                      <div key={`print-${sheet.name}`} className="PrintTables">
                        <HotTable
                          ref={(el) => {
                            printRefs.current[index] = el;
                          }}
                          id="print-table"
                          key="print-table"
                          rowHeaders={false}
                          colHeaders={false}
                          width="8.5in"
                          readOnly
                          className={table[index].showGrid ? 'bordered-excel-pr' : 'borderless-excel-pr'}
                          data={table[index].rows}
                          cells={() => {
                            const cellProperties: any = {};
                            cellProperties.renderer = (instance: Handsontable, td: any, row: number, col: number, prop: React.ReactText, value: any, properties: Handsontable.CellProperties) => {
                              printRender(instance, td, row, col, prop, value, properties, index);
                            };
                            return cellProperties;
                          }}
                          // beforeChange={catchEmptyCell}
                          // afterChange={shouldSave}
                          colWidths={columns}
                          mergeCells={table[index].merges}
                          licenseKey={process.env.REACT_APP_HANDSONTABLE_LICENSE_KEY}
                        />
                      </div>
                    );
                  }
                  return <></>;
                })}
            </div>
          </div>
        )}
        <div className="excelTable" data-cy="excel-table">
          <ExcelHeader //
            canEdit={canEdit}
            saved={saved}
            handleBlur={handleBlur}
            showPrompt={showPrompt}
            downloadFile={downloadFile}
            readOnly={readOnly}
            mode={mode}
            confirm={confirm}
            refresh={refresh}
            cancel={cancel}
            checkRefresh={checkRefresh}
            edit={edit}
            clone={clone}
            printer={printTable}
            deleteFile={deleteFile}
            title={title}
            direction={table[currentSheet].orientation}
            handleUpdatePrompt={handleUpdatePrompt}
            sheets={table.length}
            canRefresh={canRefresh}
          />
          {table[currentSheet] && (
            <HotTable
              id="edit-table"
              key="edit-table"
              ref={handson}
              height="calc(100vh - 72px - 72px)" // full height - padding - header height
              width="100%"
              preventOverflow="horizontal"
              // rowHeaders={!readOnly}
              // colHeaders={!readOnly}
              rowHeaders
              colHeaders
              maxCols={table[currentSheet].columns.length}
              autoColumnSize={false}
              readOnly={readOnly}
              className={table[currentSheet].showGrid ? 'bordered-excel' : 'borderless-excel'}
              data={table[currentSheet].rows}
              cells={() => {
                const cellProperties: any = {};
                cellProperties.renderer = customRender;
                return cellProperties;
              }}
              beforeOnCellMouseDown={handlePreRendering}
              afterOnCellMouseDown={checkRichCell}
              beforeChange={catchEmptyCell}
              afterChange={shouldSave}
              colWidths={table[currentSheet].columns}
              mergeCells={table[currentSheet].merges}
              licenseKey={process.env.REACT_APP_HANDSONTABLE_LICENSE_KEY}
            >
              {table[currentSheet].columns.map((col, index) => {
                return (
                  <HotColumn key={`${currentSheet}-column-${index}`}>
                    <EditorComponent
                      updateData={updateData}
                      triggerTemplateUpdate={triggerTemplateUpdate}
                      updateTemplate={updateTemplateSettingsFromEditor}
                      table={table}
                      rows={table![currentSheet].rows}
                      lastClicked={lastClicked}
                      setDragging={setDragging}
                      mode={mode}
                      currentSheet={currentSheet}
                      curCol={index}
                      hot-editor
                    />
                  </HotColumn>
                );
              })}
            </HotTable>
          )}
        </div>
      </section>

      <SheetNavigation table={table} currentSheet={currentSheet} setSheet={setSheet} isDefault={isDefault} />
    </Layout>
  );
};

export default ExcelTable;

ExcelTable.defaultProps = {
  title: undefined,
  confirm: false,
  direction: undefined,
  canEdit: false,
  saved: false,
  isDefault: undefined,
  token: null,
  settings: null,
  refresh: undefined,
  checkRefresh: undefined,
  cancel: undefined,
  clearToken: undefined,
  edit: undefined,
  editFileName: undefined,
  clone: undefined,
  deleteFile: undefined,
  handleUpdatePrompt: undefined,
  handleUpdateContext: undefined,
  canRefresh: true,
};

// for performance testing
// ExcelTable.whyDidYouRender = {
//   logOnDifferentValues: true,
// };
