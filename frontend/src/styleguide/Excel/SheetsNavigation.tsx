import React, { useState } from 'react';
import { FileOutlined } from '@ant-design/icons';
import { Button, Layout } from 'antd';

import { SheetData } from './types';

const { Sider } = Layout;

type Props = {
  table: SheetData[];
  currentSheet: number;
  setSheet: React.Dispatch<React.SetStateAction<number>>;
  isDefault?: string;
};

export const SheetNavigation: React.FC<Props> = ({ table, currentSheet, setSheet, isDefault }) => {
  const [collapsed, setCollapsed] = useState<boolean>(!isDefault);
  return (
    <Sider className="sheetsNav" data-cy="excel-sheets-navigation" collapsible={!!isDefault} collapsed={collapsed} onCollapse={() => setCollapsed(!collapsed)} reverseArrow>
      <div className={`sheetsContainer${collapsed ? ' collapsed' : ''}`} data-cy="excel-sheets-container">
        {table.map((sheets: SheetData, index: number) => {
          if (sheets && sheets.rows.length > 0) {
            return (
              <Button className={currentSheet === index ? 'sheet active' : 'sheet'} data-cy="excel-sheets-button" key={`change-${index}`} onClick={() => setSheet(index)}>
                {!!isDefault && !collapsed && (
                  <div className="thumbnail">
                    <img src={`${process.env.PUBLIC_URL}/templates/${isDefault}/${index}.png`} />
                  </div>
                )}
                {(!isDefault || collapsed) && <FileOutlined />}
                {sheets.name}
              </Button>
            );
          }
        })}
      </div>
    </Sider>
  );
};
