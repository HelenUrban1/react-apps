export interface SheetData {
  rows: CellData[][];
  columns: number[];
  name: string;
  merges: any;
  orientation: string;
  [key: string]: CellData[] | number[] | string | any;
}

export interface CellData {
  address: string;
  ind: { row: number; col: number };
  style: { [key: string]: any };
  data: string;
  tempData?: string;
  rich: any;
  formula?: any;
  footerRow?: number;
  [key: string]: any;
}

export interface CellChanges {
  sheet: string;
  address: string;
  data: CellData;
}

export interface CellFooter {
  row: number;
  col: number;
  footer: number;
  sheet: number;
}

export interface SettingsChange {
  address?: string;
  row?: number;
  col?: number;
  footer?: number;
  sheet?: number;
  tok?: string;
  add?: boolean;
  del?: boolean;
}
