import React, { useState, useRef, useEffect } from 'react';
import { CloseOutlined } from '@ant-design/icons';
// import { Tooltip, message, Button } from 'antd';
import { DragDropContext, Droppable, Draggable } from 'react-beautiful-dnd';
// import { i18n } from 'i18n';
// import { AddToken } from 'styleguide/shared/svg';
import { v4 as uuid } from 'uuid';
import Handsontable from 'handsontable';
import { defaultTokens } from 'view/global/defaults';
import { removeTemp, removeItem, reorder, initItems, buildString, Item, mergeText, nextText, prevText, hasText, getToken, hasDynamic, addToken } from 'utils/editorUtil';
import { TokenType } from 'types/reportTemplates';

import Token from '../Token/Token';
import { SheetData } from './types';

// TODO: replace with Token Editor Modal
// const success = () => {
//   return message.success('Add Token Modal');
// };

const getItemStyle = (isDragging: any, draggableStyle: any) => ({
  // some basic styles to make the items look a bit nicer
  userSelect: 'none',
  padding: '2px 0px 0px 0px',
  margin: '0px 0px 0px 0px',
  width: 'auto',

  // change background colour if dragging
  boxShadow: isDragging ? '4px 0px 0px rgba(0,0,0,0.25)' : 'none',

  // styles we need to apply on draggables
  ...draggableStyle,
});

const getListStyle = (isDraggingOver: any) => ({
  display: 'flex',
  padding: '0px 0px',
  overflow: 'auto',
  alignItems: 'baseline',
});

type Props = {
  row: number;
  col: number;
  open: boolean;
  value: string;
  setValue: Function;
  handleClose: (e: any) => void;
  resetPosition: () => void;
  setDragging: (dragging: { drag: boolean; token: TokenType | undefined; row: number; col: number; oldValue: string }) => void;
  table: SheetData[];
  currentSheet: number;
  updateTemplate: ({
    //
    col,
    row,
    newCell,
    tok,
    add,
    del,
    updatedData,
  }: {
    col?: number;
    row?: number;
    newCell?: any;
    tok?: string;
    add?: boolean;
    del?: boolean;
    updatedData?: SheetData[];
  }) => void;
  mode: string;
};

// eslint-disable-next-line import/prefer-default-export
export const SelectedCell = ({ row, col, open, value, setValue, handleClose, resetPosition, setDragging, table, currentSheet, updateTemplate, mode }: Props) => {
  const items = useRef<Item[]>(initItems(value, row, col));
  const tempItems = useRef<Item[]>(initItems(value, row, col));
  const [focus, setFocus] = useState({ ind: 0, pos: 0 });
  const [showBtn, setShowBtn] = useState(false);
  const refContainer = useRef<HTMLDivElement>(null);
  const atLeftBound = useRef(false);
  const atRightBound = useRef(false);

  useEffect(() => {
    if (value) {
      items.current = initItems(value, row, col);
      tempItems.current = initItems(value, row, col);
    }
  }, [value, row, col]);

  const hasToken = (str: string) => {
    const reg = /{{[a-zA-Z.]*}}/g;
    const match = str.match(reg);
    if (match !== null) {
      for (let i = 0; i < match.length; i += 1) {
        if (getToken(defaultTokens, match[i])) return true;
      }
    }
    return false;
  };

  useEffect(() => {
    if (open) {
      // autofocus into the text input
      const inp = document.getElementById(`${items.current[items.current.length - 1].cell}-${items.current[items.current.length - 1].id}`);
      if (inp && inp.childNodes && inp.childNodes.length !== 0) {
        const range = document.createRange();
        const sel = window.getSelection();
        range.setStart(inp!.childNodes[0], inp!.innerHTML.length);
        range.collapse(true);
        sel?.removeAllRanges();
        sel?.addRange(range);
      }
      inp?.focus();
    } else {
      document.getElementById(`${items.current[items.current.length - 1].cell}-${items.current[items.current.length - 1].id}`)?.blur();
    }
    if (mode === 'template') setShowBtn(hasText(items.current));
  }, [open]);

  const cursorPosition = () => {
    const sel = document.getSelection();
    if (!sel) return;
    return sel.focusOffset;
  };

  useEffect(() => {
    if (open) {
      // autofocus into the text input
      const inp = document.getElementById(`${items.current[focus.ind].cell}-${items.current[focus.ind].id}`);
      if (inp && inp.childNodes && inp.childNodes.length !== 0) {
        const range = document.createRange();
        const sel = window.getSelection();
        range.setStart(inp!.childNodes[0], focus.pos);
        range.collapse(true);
        sel?.removeAllRanges();
        sel?.addRange(range);
      }
      inp?.focus();
      const pos = cursorPosition();
      atLeftBound.current = pos === 0;
      atRightBound.current = pos === items.current[focus.ind].content.length;
    } else {
      document.getElementById(`${items.current[focus.ind].cell}-${items.current[focus.ind].id}`)?.blur();
    }
  }, [focus.ind, focus.pos]);

  // reorder the list and/or remove a token after dragging and dropping
  const handleDragEnd = (result: any) => {
    if (!result.destination) {
      // const newOrder = reorder(tempItems.current, result.source.index, tempItems.current.length, true);
      // const finalOrder = mergeText(newOrder, row, col);
      // setValue(buildString(finalOrder));
      // items.current = finalOrder;
      // tempItems.current = finalOrder;
      setDragging({ drag: false, token: undefined, row: 0, col: 0, oldValue: '' });
      resetPosition();
      return;
    }

    const newOrder = reorder(tempItems.current, result.source.index, result.destination.index, false);
    const finalOrder = mergeText(newOrder, row, col);
    setValue(buildString(finalOrder));
    items.current = finalOrder;
    tempItems.current = finalOrder;
    setDragging({ drag: false, token: undefined, row: 0, col: 0, oldValue: '' });
  };

  const handleDragStart = (start: any) => {
    const { token } = items.current[start.source.index];
    setDragging({ drag: true, token, row, col, oldValue: buildString(tempItems.current) });
  };

  const removeToken = (item: Item) => {
    const res = removeItem(tempItems.current, item);
    const newList = res.list;
    const mergedList = mergeText(newList, row, col);
    const cell = table[currentSheet].rows[row][col];
    if (cell && res.token) {
      if (res.token.type === 'Dynamic' && !hasDynamic(mergedList)) {
        // removed the last dynamic token from the cell, need to update template settings
        delete cell.footerRow;
        updateTemplate({ col, row, newCell: cell, tok: res.token.token, add: false, del: true });
      } else {
        updateTemplate({ col, row, newCell: cell, tok: res.token.token, add: false });
      }
    }

    setValue(buildString(mergedList));
    items.current = mergedList;
    tempItems.current = mergedList;
  };

  const handleBlur = (e: any, item: Item) => {
    //removed for IXC-2179, was causing data duplication on cell edit, no need to set value when it's already called in handleOnChange
    //item.content = e.target.innerHTML.replace(/&nbsp;/g, ' ');
    //setValue(buildString(tempItems.current));
    item.content = '';
  };

  const handleOnChange = (e: any, cItem: Item) => {
    const newItems: Item[] = [];

    let footer;
    let newToken: TokenType | undefined;
    let inc = 0;
    let newFocus = 0;
    let addedToken = false;
    let lastReturned: Item = cItem;
    items.current.forEach((item) => {
      if (item.cell === cItem.cell) {
        if (hasToken(e.target.innerHTML)) {
          addedToken = true;
          const ret = addToken(e.target.innerHTML, cItem.cell, row, col, table, currentSheet);
          lastReturned = ret.list[ret.list.length - 1];
          ret.list.forEach((it) => {
            newItems.push(it);
          });
          footer = ret.footer;
          newToken = ret.token;
        } else {
          newItems.push({
            id: uuid(),
            cell: `${row}-${col}-input-${(inc += 1)}`,
            content: e.target.innerHTML,
            type: item.type,
            token: item.token,
          });
        }
      } else {
        newItems.push({
          id: uuid(),
          cell: `${row}-${col}-input-${(inc += 1)}`,
          content: item.content,
          type: item.type,
          token: item.token,
        });
      }
    });

    if (addedToken) {
      const mergedList = mergeText(newItems, row, col);
      items.current = mergedList;
      tempItems.current = mergedList;
      newFocus = newItems.findIndex((a) => a.id === lastReturned.id);
      if (newItems[newFocus].type === 'token') newFocus = nextText(newFocus, newItems);
      if (lastReturned.type === 'token') newFocus += 1;
      setFocus({ ind: newFocus, pos: 0 });
      setValue(buildString(mergedList));
      if (footer) {
        updateTemplate({ col, row, newCell: footer, tok: newToken?.token, add: true });
      } else if (newToken) {
        updateTemplate({ tok: newToken?.token, add: true });
      }
    } else {
      tempItems.current = newItems;
      setValue(buildString(newItems));
    }

    // TODO: re-enable when custom tokens implemented
    // if (!showBtn && mode === 'template') setShowBtn(hasText(newItems));
  };

  const getSingleStyle = () => {
    if (items.current.length === 1) {
      return { minWidth: '80px' };
    }
    return {};
  };

  const checkBounds = (ind: number) => {
    const pos = cursorPosition();
    if (pos === 0) {
      atLeftBound.current = true;
    } else if (pos === items.current[ind].content.length - 1) {
      atRightBound.current = true;
    } else {
      atLeftBound.current = false;
      atRightBound.current = false;
    }
  };

  const handleKeyUp = (e: any, cItem: Item) => {
    let ind = items.current.findIndex((a) => a.id === cItem.id);
    let pos = cursorPosition();
    if (!pos) pos = 0;
    let remove = false;
    switch (e.keyCode) {
      case Handsontable.helper.KEY_CODES.DELETE:
        remove = true;
      // eslint-disable-next-line no-fallthrough
      case Handsontable.helper.KEY_CODES.ARROW_RIGHT:
        if (ind !== items.current.length - 1) {
          if (atRightBound.current) {
            if (remove) {
              removeToken(tempItems.current[ind + 1]);
              setFocus({ ind, pos });
            } else {
              // move focus to the start of the next text input
              setFocus({ ind: nextText(ind, tempItems.current), pos: 0 });
              atLeftBound.current = true;
            }
            atRightBound.current = false;
          } else if (pos === tempItems.current[ind].content.length) {
            atRightBound.current = true;
          }
        }
        if (cursorPosition() !== 0) {
          atLeftBound.current = false;
        }
        break;
      case Handsontable.helper.KEY_CODES.BACKSPACE:
        remove = true;
      // eslint-disable-next-line no-fallthrough
      case Handsontable.helper.KEY_CODES.ARROW_LEFT:
        if (ind !== 0) {
          if (atLeftBound.current) {
            if (remove) {
              if (tempItems.current[ind - 2].type === 'text') {
                const newPos = tempItems.current[ind - 2].content.length;
                removeToken(tempItems.current[ind - 1]);
                setFocus({ ind: ind - 2, pos: newPos });
                atLeftBound.current = false;
              } else {
                removeToken(tempItems.current[ind - 1]);
              }
            } else {
              const newInd = prevText(ind, tempItems.current);
              const newPos = tempItems.current[newInd].content.length ? tempItems.current[newInd].content.length : 0;
              setFocus({ ind: newInd, pos: newPos });
              atRightBound.current = true;
            }
            ind -= 2;
            if (ind < 0) ind = 0;
            atLeftBound.current = false;
          } else if (pos === 0) {
            atLeftBound.current = true;
          }
        }
        if (cursorPosition() !== tempItems.current[ind].content.length) {
          atRightBound.current = false;
        }
        break;
      default:
        checkBounds(ind);
        break;
    }
  };

  if (!open) {
    return <></>;
  }

  return (
    <div className="selected-cell" key={items.current.length} ref={refContainer} data-cy="cell-editor">
      <div className="top-container-buttons">
        <CloseOutlined onClick={handleClose} style={{ float: 'right' }} />
      </div>
      <DragDropContext onDragEnd={handleDragEnd} onDragStart={handleDragStart}>
        <Droppable droppableId="droppable" direction="horizontal">
          {(provided, snapshot) => (
            <div ref={provided.innerRef} style={getListStyle(snapshot.isDraggingOver)} className="drop-container" {...provided.droppableProps}>
              {items.current.map((item, index) => (
                <Draggable key={`${item.cell}-${item.id}`} data-cy={`${item.cell}`} draggableId={`${item.cell}-${item.id}`} index={index} isDragDisabled={item.type === 'text'}>
                  {(provided, snapshot) => (
                    <div ref={provided.innerRef} data-cy={`${item.cell}`} {...provided.draggableProps} {...provided.dragHandleProps} style={getItemStyle(snapshot.isDragging, provided.draggableProps.style)}>
                      {item.type === 'token' ? (
                        <Token data-cy={`${item.cell}`} removeable handleRemove={removeToken} token={item.token} name={item.content} item={item} />
                      ) : (
                        <div className="cell-input-container">
                          <span contentEditable={false}>
                            <span
                              id={`${item.cell}-${item.id}`}
                              onKeyUpCapture={(e) => {
                                handleKeyUp(e, item);
                              }}
                              data-cy={`${item.cell}`}
                              role="textbox"
                              className={`cell-input ${item.cell}`}
                              contentEditable
                              suppressContentEditableWarning
                              onInput={(e) => handleOnChange(e, item)}
                              onBlur={(e) => handleBlur(e, item)}
                              style={getSingleStyle()}
                            >
                              {item.content}
                            </span>
                          </span>
                        </div>
                      )}
                    </div>
                  )}
                </Draggable>
              ))}
              {provided.placeholder}
            </div>
          )}
        </Droppable>
      </DragDropContext>
      {/* <div className="bottom-container-buttons">
        {showBtn && (
          <Tooltip title={i18n('entities.reportTemplate.tooltips.createToken')} placement="bottom">
            <Button className="create-tkn-btn" onClick={success} type="link" size="small">
              {i18n('entities.reportTemplate.buttons.createToken')}
              <addDraggedToken />
            </Button>
          </Tooltip>
        )}
      </div> */}
    </div>
  );
};
