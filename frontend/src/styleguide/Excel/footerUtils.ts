import { styles } from 'styleguide/AntDesignCustomTheme';
import { i18n } from 'i18n';
import { CellData } from './types';

const highlightStyle = {
  fontSize: '14px',
  backgroundColor: styles.colors.royalM4,
  border: '1px solid gray',
  fontWeight: 'normal',
};

const footerCellStyle = {
  fontSize: '14px',
  backgroundColor: styles.colors.coralM1,
  border: '1px solid gray',
  fontWeight: 'normal',
  boxShadow: '0px 0px 8px rgba(0,0,0,0.25)',
};

export const setFooter = (rows: CellData[][], footerRow: number, col: number, cols: number, tokenRow?: number) => {
  let cell = null;
  const tempStyles: { [key: string]: any } = {};
  const footer = parseInt(footerRow.toString(), 10);
  let lastRow = rows[footer];
  if (!lastRow) {
    lastRow = tokenRow ? rows[tokenRow] : rows[rows.length - 1];
  }

  if (!cols || !rows || !tokenRow) {
    return null;
  }
  // Loop through current column
  const lastCellColSpan = lastRow[col]?.colspan || 1;
  for (let i = 0; i < rows.length; i++) {
    cell = rows[i][col];
    if (cell) {
      if (!cell.colspan) {
        cell.colspan = 1;
      }
      if (cell.masterCol - 1 === col && (!lastCellColSpan || lastCellColSpan === cell.colspan)) {
        if (i === footer) {
          // style footer cell
          if (!tempStyles[cell.address]) {
            tempStyles[cell.address] = {};
          }
          tempStyles[cell.address].tempStyle = {
            ...cell.style,
            ...footerCellStyle,
          };
          tempStyles[cell.address].tempData = i18n('entities.template.editor.removeFooter');
        } else if (i > footer) {
          // remove the temp data and styles from the cells below the selected footer row
          delete cell.tempData;
          delete cell.tempStyle;
        } else if (i > tokenRow) {
          // remove the add footer text from the column cells above the footer row, leave the tempStyle
          if (!tempStyles[cell.address]) {
            tempStyles[cell.address] = {};
          }
          tempStyles[cell.address].tempStyle = {
            ...cell.style,
            ...highlightStyle,
          };
        } else {
          // remove the temp data and styles from the cells above the dynamic token
          delete cell.tempData;
          delete cell.tempStyle;
        }
      }
    }
  }
  return tempStyles;
};

export const setHorizontalFooter = (rows: CellData[][], footerCol: number, row: number, tokenCol?: number) => {
  let cell = null;
  const curRow = rows[row];
  const tempStyles: { [key: string]: any } = {};

  if (row === undefined || tokenCol === undefined) {
    return null;
  }
  const lastCellRowSpan = curRow[footerCol].rowspan;

  // Loop through current row
  for (let i = 0; i < curRow.length; i++) {
    cell = curRow[i];
    if (cell) {
      if (!cell.rowspan) {
        cell.rowspan = 1;
      }
      if (cell.masterRow - 1 === row && (!lastCellRowSpan || lastCellRowSpan === cell.rowspan)) {
        if (i === footerCol) {
          // style footer cell
          if (!tempStyles[cell.address]) {
            tempStyles[cell.address] = {};
          }
          tempStyles[cell.address].tempStyle = {
            ...cell.style,
            ...footerCellStyle,
          };
          tempStyles[cell.address].tempData = i18n('entities.template.editor.removeFooter');
        } else if (i > footerCol) {
          // remove the temp data and styles from the cells to the right of the selected footer column
          delete cell.tempData;
          delete cell.tempStyle;
        } else if (i > tokenCol) {
          // remove the add footer text from the column cells to the left of the footer column, leave the tempStyle
          if (!tempStyles[cell.address]) {
            tempStyles[cell.address] = {};
          }
          tempStyles[cell.address].tempStyle = {
            ...cell.style,
            ...highlightStyle,
          };
        } else {
          // remove the temp data and styles from the cells to the left of the dynamic token
          delete cell.tempData;
          delete cell.tempStyle;
        }
      }
    }
  }

  /* Column stylings
  // Loop through current column
  for (let i = 0; i < rows.length; i++) {
    cell = rows[i][footerCol];
    if (cell) {
      if (cell.masterCol - 1 === footerCol && (!lastCellColSpan || lastCellColSpan === cell.colspan)) {
        if (i === row) {
          // style footer cell
          if (!tempStyles[cell.address]) {
            tempStyles[cell.address] = {};
          }
          tempStyles[cell.address].tempStyle = {
            ...cell.style,
            ...footerCellStyle,
          };
          tempStyles[cell.address].tempData = i18n('entities.template.editor.removeFooter');
        } else {
          // highlight the rest of the row
          if (!tempStyles[cell.address]) {
            tempStyles[cell.address] = {};
          }
          tempStyles[cell.address].tempStyle = {
            ...cell.style,
            ...highlightStyle,
          };
        }
      } else if (i > row) {
        // stop styling when we reach a merge cell
        return tempStyles;
      }
    }
  }
*/
  return tempStyles;
};

// sets up the footer row picker for the active dynamic cell on a vertical template
export const setVerticalPicker = (rows: CellData[][], rowStart: number, col: number) => {
  const matches = rows[rowStart][col].address.match(/(\D*)/);
  const tokenCellCol = matches ? matches[1] : null;
  const tempStyles: { [key: string]: any } = {};
  let cell = null;
  let lastCellColSpan;

  for (let i = rowStart + 1; i < rows.length; i++) {
    cell = rows[i][col];
    if (tokenCellCol && cell) {
      const match = cell.address.match(/(\D*)/);
      const column = match ? match[1] : null;
      if ((column && column !== tokenCellCol) || cell.masterCol - 1 !== col || (lastCellColSpan && lastCellColSpan !== cell.colspan && !(lastCellColSpan === 1 && !cell.colspan))) {
        // stop styling when we reach a merge cell
        return tempStyles;
      }
      if (!tempStyles[cell.address]) {
        tempStyles[cell.address] = {};
      }
      tempStyles[cell.address].tempStyle = {
        ...cell.style,
        ...highlightStyle,
      };
      tempStyles[cell.address].tempData = i18n('entities.template.editor.addFooter');
      lastCellColSpan = cell.colspan;
    }
  }
  return tempStyles;
};

// sets up the footer row picker for the active dynamic cell on a horizontal template
export const setHorizontalPicker = (col: CellData[], colStart: number, rowStart: number) => {
  const tempStyles: { [key: string]: any } = {};
  let cell = null;
  let lastCellRowSpan;

  for (let i = colStart + 1; i < col.length; i++) {
    cell = col[i];
    if (cell) {
      if (cell.masterRow - 1 !== rowStart || (lastCellRowSpan && lastCellRowSpan !== cell.rowspan)) {
        // stop styling when we reach a merge cell
        return tempStyles;
      }
      if (!tempStyles[cell.address]) {
        tempStyles[cell.address] = {};
      }
      tempStyles[cell.address].tempStyle = {
        ...cell.style,
        ...highlightStyle,
      };
      tempStyles[cell.address].tempData = i18n('entities.template.editor.addFooter');
      lastCellRowSpan = cell.rowspan;
    }
  }
  return tempStyles;
};
