import React from 'react';
import { BaseEditorComponent } from '@handsontable/react';
import NativeListener from 'react-native-listener';
import Handsontable from 'handsontable';
import { SelectedCell } from './SelectedCell';

class EditorComponent extends BaseEditorComponent {
  constructor(props) {
    super(props);

    this.changeValue = this.changeValue.bind(this);
    this.closeSelectedCell = this.closeSelectedCell.bind(this);
    this.resetPosition = this.resetPosition.bind(this);

    this.mainElementRef = React.createRef();
    this.templateUpdates = React.createRef();

    this.containerStyle = {
      display: 'none',
      position: 'absolute',
      left: 0,
      top: 0,
      minWidth: '100px',
      background: '#fff',
      zIndex: 999,
    };
    this.state = {
      value: '',
      init: '',
      open: false,
    };

    this.width = 100;
  }

  setValue(value, callback) {
    this.setState((state, props) => {
      return { value };
    }, callback);
  }

  changeValue(value, close = false) {
    this.setValue(value);
    if (close) this.closeSelectedCell();
  }

  closeSelectedCell() {
    this.setValue(this.state.value, () => {
      this.finishEditing();
    });
  }

  getValue() {
    this.props.updateData(this.row, this.col, this.state.value);
    return this.state.value;
  }

  resetPosition() {
    if (!this.mainElementRef.current || !this.TD) return;
    const tdBounds = this.TD.getBoundingClientRect();

    this.mainElementRef.current.style.left = `${tdBounds.left}px`;
    this.mainElementRef.current.style.top = `${tdBounds.top}px`;
  }

  handleOnScroll() {
    const hotBounds = this.hotInstance.rootElement.getBoundingClientRect();
    const tdBounds = this.TD.getBoundingClientRect();

    if (!this.mainElementRef.current || !hotBounds || !tdBounds) return;

    this.mainElementRef.current.style.left = `${tdBounds.left}px`;
    this.mainElementRef.current.style.top = `${tdBounds.top}px`;

    if (hotBounds.top > tdBounds.top || hotBounds.left > tdBounds.left || hotBounds.left + hotBounds.width < tdBounds.left + tdBounds.width || hotBounds.top + hotBounds.height < tdBounds.top + tdBounds.height) {
      this.mainElementRef.current.style.display = 'none';
    } else {
      this.mainElementRef.current.style.display = 'inline';
    }
  }

  open() {
    if (!this.mainElementRef.current) return;
    this.mainElementRef.current.style.display = 'inline';
    this.mainElementRef.current.className = 'cell-container cell-container-opened';
    this.setState({ open: true });
    this.addHook('afterScrollVertically', () => this.handleOnScroll());
  }

  close() {
    if (!this.mainElementRef || !this.mainElementRef.current || !this.state.open) return;
    this.mainElementRef.current.style.display = 'none';
    this.mainElementRef.current.className = 'cell-container';
    this.setState({ open: false, init: '' });
    this.clearHooks();
    this.props.triggerTemplateUpdate();
  }

  handleDeleteAndBackspace = (event) => {
    if (this && this.mainElementRef.current && this.mainElementRef.current.style.display !== 'none') {
      Handsontable.dom.stopImmediatePropagation(event);
    }
  };

  handleLeftRightArrows = (event) => {
    if (this.state.open) {
      Handsontable.dom.stopImmediatePropagation(event);
    }
  };

  onBeforeKeyDown(event) {
    // prevents default delete/backspace functionality of deleting the cell
    switch (event.keyCode) {
      case Handsontable.helper.KEY_CODES.DELETE:
      case Handsontable.helper.KEY_CODES.BACKSPACE:
        this.handleDeleteAndBackspace(event);
        break;
      case Handsontable.helper.KEY_CODES.ARROW_LEFT:
      case Handsontable.helper.KEY_CODES.ARROW_RIGHT:
        this.handleLeftRightArrows(event);
        break;
      default:
        break;
    }
  }

  static getVal(original) {
    if (typeof original === 'object') {
      return original.data || '';
    }
    if (typeof original === 'string') {
      return original;
    }
    return '';
  }

  prepare(row, col, prop, td, originalValue, cellProperties) {
    // We'll need to call the `prepare` method from
    // the `BaseEditorComponent` class, as it provides
    // the component with the information needed to use the editor
    // (hotInstance, row, col, prop, TD, originalValue, cellProperties)
    if (cellProperties.readOnly) return;
    const val = EditorComponent.getVal(originalValue);
    super.prepare(row, col, prop, td, val, cellProperties);
    const tdPosition = td.getBoundingClientRect();

    // console.log('Preparing', row, col, val, originalValue);

    this.setState({ init: val, value: val, curRow: row, curCol: col });

    // As the `prepare` method is triggered after selecting
    // any cell, we're updating the styles for the editor element,
    // so it shows up in the correct position.
    if (this.mainElementRef.current) {
      this.mainElementRef.current.style.left = `${tdPosition.left}px`;
      this.mainElementRef.current.style.top = `${tdPosition.top}px`;
      this.width = tdPosition.width;
      this.addHook('beforeKeyDown', (e) => this.onBeforeKeyDown(e));
    }
  }

  // prevents the editor from closing when click inside it
  stopMousedownPropagation = (e) => {
    e.stopPropagation();
  };

  render() {
    return (
      <NativeListener onMouseDown={this.stopMousedownPropagation} onMouseUp={this.stopMouseupPropagation}>
        <div style={this.containerStyle} ref={this.mainElementRef} id={`${this.col ? this.col : this.props.curCol}-editor-element`} className="cell-container">
          <SelectedCell
            value={this.state.init}
            setValue={this.changeValue}
            handleClose={this.closeSelectedCell}
            resetPosition={this.resetPosition}
            setDragging={this.props.setDragging}
            open={this.state.open}
            row={this.state.curRow ? this.state.curRow : this.row}
            col={this.state.curCol ? this.state.curCol : this.props.curCol}
            width={this.width}
            lastClicked={this.props.lastClicked}
            table={this.props.table}
            td={this.TD}
            currentSheet={this.props.currentSheet}
            updateTemplate={this.props.updateTemplate}
            mode={this.props.mode}
          />
        </div>
      </NativeListener>
    );
  }
}

export default EditorComponent;
