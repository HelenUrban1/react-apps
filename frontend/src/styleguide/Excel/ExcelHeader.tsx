import React, { useEffect, useRef, useState } from 'react';
import { CopyOutlined, DeleteOutlined, DownloadOutlined, EditOutlined, FileTextOutlined, PrinterOutlined, ReloadOutlined, ShareAltOutlined, FileExcelOutlined } from '@ant-design/icons';
import { Popconfirm, Dropdown, Menu, Button, Tooltip } from 'antd';
import ReactToPrint from 'react-to-print';
import { i18n } from 'i18n';
import { ReportTemplate } from 'types/reportTemplates';
import { GdtFontModal } from 'styleguide/Modals/GdtFontModal';
import { SettingState } from 'domain/setting/settingTypes';
import { AppState } from 'modules/state';
import { useSelector } from 'react-redux';
import authSelectors from 'modules/auth/authSelectors';
import { useHistory } from 'react-router-dom';

type Props = {
  title?: string;
  readOnly: boolean;
  downloadFile: (type: string, font?: boolean, framed?: boolean) => void;
  refresh?: () => void;
  checkRefresh?: () => void;
  cancel?: () => void;
  confirm?: boolean;
  mode: string;
  direction?: string | undefined;
  edit?: (template?: ReportTemplate, copy?: boolean) => void;
  clone?: (template?: ReportTemplate, copy?: boolean) => void;
  handleBlur: (e: any) => void;
  deleteFile?: () => void;
  handleUpdatePrompt?: () => void;
  canEdit?: boolean;
  saved?: boolean;
  printer?: any;
  showPrompt: boolean;
  sheets: number;
  canRefresh?: boolean;
};

export const ExcelHeader = ({
  //
  canEdit,
  saved,
  handleBlur,
  downloadFile,
  readOnly,
  mode,
  direction,
  confirm,
  refresh,
  cancel,
  checkRefresh,
  edit,
  clone,
  printer,
  deleteFile,
  title,
  handleUpdatePrompt,
  showPrompt,
  sheets,
  canRefresh,
}: Props) => {
  const [show, setShow] = useState<any>();
  const [framed, toggleFramed] = useState(false);
  const titleInput = useRef<HTMLInputElement>(null);
  const [titleValue, setTitle] = useState(title || '');
  const { userSettings, settings } = useSelector((state: AppState) => state.session);
  const [currentSettings, setCurrentSettings] = useState<SettingState>({ ...userSettings, ...settings });
  const user = useSelector((state: AppState) => authSelectors.selectCurrentUser(state));
  const history = useHistory();

  let hideGDTModal = false;

  useEffect(() => {
    setCurrentSettings({ ...currentSettings, ...userSettings });
  }, [userSettings]);

  useEffect(() => {
    if (title && title !== titleValue) {
      setTitle(title);
    }
  }, [title]);

  if (!user) {
    history.push('/');
    return <></>;
  }

  const noFont = () => {
    setShow(undefined);
    if (hideGDTModal && handleUpdatePrompt) {
      handleUpdatePrompt();
    }
    downloadFile(show, false, framed);
  };

  const withFont = () => {
    setShow(undefined);
    if (hideGDTModal && handleUpdatePrompt) {
      handleUpdatePrompt();
    }
    downloadFile(show, true, framed);
  };

  const unFramed = () => {
    toggleFramed((prevState) => !prevState);
  };

  const downloadMenu = (
    <Menu>
      <Menu.Item>
        <Button
          icon={<FileExcelOutlined />}
          className="excelDownload btn-dropdown"
          data-cy="excel-action-download-excel"
          onClick={() => {
            if (showPrompt) {
              setShow('xlsx');
            } else {
              downloadFile('xlsx', false, framed);
            }
          }}
        >
          {i18n('files.excel')}
        </Button>
      </Menu.Item>
      <Menu.Item>
        <Button
          icon={<FileTextOutlined />}
          className="excelDownload btn-dropdown"
          data-cy="excel-action-download-csv"
          onClick={() => {
            if (showPrompt) {
              setShow('csv');
            } else {
              downloadFile('csv', false, framed);
            }
          }}
        >
          {i18n('files.csv')}
        </Button>
      </Menu.Item>
      {mode === 'report' && (
        <Menu.Item disabled className="menu-item-checkbox">
          <label htmlFor="convertFramed" className="checkboxMenu">
            <input data-cy="excel-action-convert" id="convertFramed" name="convertFramed" type="checkbox" onChange={unFramed} />
            {i18n('prompts.font.convert')}
          </label>
        </Menu.Item>
      )}
    </Menu>
  );

  const handleTyping = (e: any) => {
    setTitle(e.target.value);
  };

  const focusInput = () => {
    if (titleInput.current) {
      titleInput.current.focus();
      titleInput.current.select();
    }
  };

  const cancelShow = () => setShow(undefined);

  return (
    <section className="excelHeader" data-cy="excel-header">
      <GdtFontModal
        show={!!show}
        handleCancel={cancelShow}
        handleChange={() => {
          hideGDTModal = !hideGDTModal;
        }}
        handleNoFont={noFont}
        handleWithFont={withFont}
      />

      <section className="excel-title-container" data-cy="excel-title-container">
        <div className="excel-title-file" data-cy="excel-file-icon">
          {mode === 'report' && (
            <Tooltip title={i18n('entities.report.title')}>
              <FileTextOutlined />
            </Tooltip>
          )}
          {mode === 'template' && (
            <Tooltip title={i18n('entities.reportTemplate.title')}>
              <FileExcelOutlined />
            </Tooltip>
          )}
        </div>
        {!canEdit ? (
          <div data-cy="excel-title" className="excelTitle">
            {title}
          </div>
        ) : (
          <div className="excelTitle">
            <span className="titleInput">
              <input ref={titleInput} data-cy="excel-title" className="excel-title-input" size={titleValue?.length + 1 || 1} value={titleValue} onChange={handleTyping} onBlur={handleBlur} />
              <EditOutlined className="excel-title-edit" data-cy="excel-title-edit" onClick={focusInput} />
            </span>
            <section id="excelSaveStatus" className="excelStatus" data-cy="excel-status">
              {saved ? i18n('common.saved') : i18n('common.saving')}
            </section>
          </div>
        )}
      </section>
      <section className="excelActions" data-cy="excel-actions">
        {canRefresh && refresh && mode === 'report' && (
          <Popconfirm title="Report already up to date. Refresh data anyway?" visible={confirm} onConfirm={refresh} onCancel={cancel} okText="Refresh" cancelText="Cancel">
            <Button className="btn-secondary" data-cy="excel-action-refresh" onClick={checkRefresh}>
              <ReloadOutlined />
              {` ${i18n('common.refresh')}`}
            </Button>
          </Popconfirm>
        )}
        {!canRefresh && refresh && mode === 'report' && (
          <Tooltip title={i18n('reportTemplates.noRefresh')}>
            <Button disabled data-cy="excel-action-refresh" onClick={checkRefresh}>
              <ReloadOutlined />
              {` ${i18n('common.refresh')}`}
            </Button>
          </Tooltip>
        )}
        {edit && mode === 'template' && readOnly && (
          <Button
            className="btn-secondary"
            data-cy="excel-action-edit"
            onClick={() => {
              if (edit) edit(undefined, false);
            }}
          >
            <EditOutlined />
            {` ${i18n('common.edit')}`}
          </Button>
        )}
        {clone && mode === 'template' && readOnly && (
          <Button
            className="btn-secondary"
            data-cy="excel-action-clone"
            onClick={() => {
              if (clone) clone(undefined, true);
            }}
          >
            <CopyOutlined />
            {` ${i18n('common.clone')}`}
          </Button>
        )}
        {false && (
          <Button className="btn-secondary" data-cy="excel-action-share">
            <ShareAltOutlined />
            {` ${i18n('common.share')}`}
          </Button>
        )}
        <ReactToPrint
          trigger={() => (
            <Tooltip title={sheets > 3 ? i18n('files.printTooLarge') : ''}>
              <Button className={sheets > 3 ? '' : 'btn-secondary'} data-cy="excel-action-print" disabled={sheets > 3}>
                <PrinterOutlined />
                {` ${i18n('common.print')}`}
              </Button>
            </Tooltip>
          )}
          content={() => printer.current}
          onBeforePrint={async () => {
            await new Promise((resolve, reject) => {
              const cypressTest = window.top as any;
              if (cypressTest.Cypress) {
                console.debug('Cypress');
                reject();
              } else {
                resolve(null);
              }
            });
          }}
          pageStyle={`@page { size: 8.5in 11in ${direction}; }`}
          documentTitle={title}
        />
        {deleteFile && (
          <Popconfirm title={i18n('common.areYouSure')} onConfirm={deleteFile} okText={i18n('common.destroy')} cancelText={i18n('common.cancel')}>
            <Button className="btn-secondary" data-cy="excel-action-delete">
              <DeleteOutlined />
              {` ${i18n('common.destroy')}`}
            </Button>
          </Popconfirm>
        )}
        {canEdit && (
          <Dropdown overlay={downloadMenu} overlayClassName="excelActionDownload">
            <Button className="btn-primary" data-cy="excel-action-download" onClick={(e: any) => e.preventDefault()}>
              <DownloadOutlined />
              {` ${i18n('common.download')}`}
            </Button>
          </Dropdown>
        )}
      </section>
    </section>
  );
};

ExcelHeader.defaultProps = {
  title: undefined,
  confirm: false,
  direction: undefined,
  canEdit: false,
  saved: false,
  printer: false,
  refresh: undefined,
  checkRefresh: undefined,
  cancel: undefined,
  edit: undefined,
  clone: undefined,
  deleteFile: undefined,
  handleUpdatePrompt: undefined,
  canRefresh: true, // true for old reporting that doesn't pass this, until flag flips
};
