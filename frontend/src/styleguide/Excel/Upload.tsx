import React, { useState, useRef } from 'react';
import { UploadOutlined } from '@ant-design/icons';
import { Button, Progress, Upload } from 'antd';
import axios from 'modules/shared/network/axios';
import config from 'config';
import log from 'modules/shared/logger';
import { useSelector } from 'react-redux';
import { AppState } from 'modules/state';
import { SheetData } from './types';

type Props = {
  table: React.MutableRefObject<SheetData[] | undefined>;
  handson: React.MutableRefObject<any | undefined>;
  currentSheet: number;
  change: any;
  setSheet: React.Dispatch<React.SetStateAction<number | undefined>>;
  setChange: React.Dispatch<any>;
};

export const ExcelUpload: React.FC<Props> = ({ table, handson, currentSheet, change, setSheet, setChange }) => {
  const [upload, setUpload] = useState<{
    percent: number;
    status?: 'normal' | 'active' | 'success' | 'exception';
  }>({ percent: 0, status: 'normal' });
  const template = useRef<string>();
  const jwt = useSelector((state: AppState) => state.auth);

  // Save template to backend
  const updateTemplate = async () => {
    if (!template) {
      log.warn('no template');
      return;
    }
    if (!change) {
      log.warn('no changes to save');
      return;
    }

    axios({
      method: 'post',
      url: `${config.backendUrl}/template/save`,
      params: {
        path: template.current,
        out: 'src/templates/edited.xlsx', // TODO: let user set this name, and save this file to an S3
      },
      data: { data: JSON.stringify(change) },
      responseType: 'blob',
      headers: {
        authorization: jwt ? `Bearer ${jwt}` : '',
      },
    })
      .then((response) => {
        setChange(null);
        template.current = 'src/templates/edited.xlsx';
      })
      .catch((error) => log.error('ExcelUpload error', error));
    return { template, change };
  };

  return (
    <div style={{ display: 'flex', flexFlow: 'row wrap', padding: '20px', alignItems: 'center' }}>
      <div style={{ display: 'flex', flexFlow: 'row wrap' }}>
        <Upload
          name="file"
          style={{ margin: '0 10px' }}
          showUploadList={false}
          action={`${config.backendUrl}/template`}
          onChange={(info: any) => {
            if (info.file.status === 'uploading') {
              setSheet(undefined);
              table.current = undefined;
              template.current = undefined;
              setUpload({ percent: info.event ? info.event.percent - 10 : 20, status: 'active' });
            } else if (info.file.status === 'done') {
              setUpload({ percent: 100, status: 'success' });
              if (info.file.xhr.response) {
                const response = JSON.parse(info.file.xhr.response);
                table.current = response.data;
                template.current = response.url;
                let sheet = 0;
                for (let i = 0; i < response.data.length; i++) {
                  if (response.data[i]) {
                    sheet = i;
                    break;
                  }
                }
                if (sheet === currentSheet) {
                  handson.current.render();
                } else {
                  setSheet(sheet);
                }
              }
            } else {
              setUpload({ percent: 100, status: 'exception' });
            }
          }}
        >
          <Button>
            <UploadOutlined /> Upload
          </Button>
        </Upload>
        {!!change && change.length > 0 && (
          <Button style={{ margin: '0 10px' }} onClick={updateTemplate}>
            Save
          </Button>
        )}
        {(!change || change.length <= 0) && (
          <Button style={{ margin: '0 10px' }} disabled onClick={updateTemplate}>
            Saved!
          </Button>
        )}
      </div>
      <Progress type="circle" percent={upload.percent} width={80} status={upload.status} />
    </div>
  );
};
