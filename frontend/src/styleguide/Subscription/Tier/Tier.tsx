import { QuestionCircleOutlined } from '@ant-design/icons';
import { i18n } from 'i18n';
import React from 'react';
import { InputType, Select } from 'view/global/inputs';
import { products } from 'view/global/defaults';
import '../Subscription.less';

interface Props {
  tier: string;
  tierOptions: InputType[];
  change: (tier: { target: { name: string; value: string } }) => void;
  help: () => void;
}

const Tier: React.FC<Props> = ({ tier, tierOptions, change, help }) => {
  const input: InputType = {
    label: '',
    value: tier,
    name: 'planTier',
    classes: 'subscriptionInput',
    change,
    group: tierOptions,
  };

  return (
    <div className="change-section">
      <h5 data-cy="modal-tier-title">
        {i18n('entities.subscription.tier.title')}
        <button type="button" className="link help-icon" onClick={help} data-cy="modal-tier-help">
          <QuestionCircleOutlined className="primaryIcon" />
        </button>
      </h5>
      <Select input={input} />
      <p data-cy="modal-tier-description" className="small">
        {i18n(`entities.subscription.tier.${products[tier]?.tier || tier}Desc`)}
      </p>
    </div>
  );
};

Tier.defaultProps = {
  tier: '5233891',
  tierOptions: [
    {
      label: i18n('entities.subscription.tier.entry'),
      value: 'Entry',
      name: 'Entry',
    },
    {
      label: i18n('entities.subscription.tier.ix_standard'),
      value: '5233891',
      name: '5233891',
    },
    {
      label: i18n('entities.subscription.tier.advanced'),
      value: 'Advanced',
      name: 'Advanced',
    },
  ],
  change: (tier: { target: { name: string; value: string } }) => null,
  help: () => null,
};
export default Tier;
