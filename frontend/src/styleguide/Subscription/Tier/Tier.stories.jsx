import React from 'react';
import { storiesOf } from '@storybook/react';
import { i18n } from 'i18n';
import { useState } from '@storybook/addons';
import Tier from './Tier';

const stories = storiesOf('Subscription/Tier', module);

stories.add('Default', () => {
  const [currentTier, setTier] = useState('5233891');

  const onChange = (event) => {
    setTier(event.target.value);
  };

  const testOptions = [
    {
      label: i18n('entities.subscription.tier.entry'),
      value: 'Entry',
      name: 'Entry',
    },
    {
      label: i18n('entities.subscription.tier.ix_standard'),
      value: '5233891',
      name: '5233891',
    },
    {
      label: i18n('entities.subscription.tier.advanced'),
      value: 'Advanced',
      name: 'Advanced',
    },
  ];
  // Note: accessibility warning about aria labels is solved in AntDesign 4.0
  // TODO: Confirm select component meets standards after upgrading antdesign
  return <Tier tier={currentTier} tierOptions={testOptions} change={onChange} />;
});
