import React from 'react';
import { i18n } from 'i18n';
import { QuestionCircleOutlined } from '@ant-design/icons';
import { Tooltip } from 'antd';
import { InputType, Select } from 'view/global/inputs';
import { products } from 'view/global/defaults'; // TODO: replace with catalogue IXC-1219
import '../Subscription.less';
import './Drawings.less';

interface Props {
  currentDrawings: number;
  limit?: boolean;
  warn: boolean;
  tier?: number;
  change: (tier: { target: { name: string; value: string } }) => void;
}

const Drawings: React.FC<Props> = ({ currentDrawings, warn, change, limit = false, tier = 5233891 }) => {
  const prices = products[tier.toString()]?.drawings || [];
  const tierOptions: InputType[] = prices.map((step, index) => {
    return {
      label: `${step.from}-${step.to}`,
      value: index.toString(),
      name: `${step.from}-${step.to}`,
      disabled: currentDrawings > 5 && step.to === 5,
    };
  });
  const input: InputType = {
    label: '',
    value: currentDrawings.toString(),
    name: 'drawing-step',
    classes: 'subscriptionInput',
    change,
    group: tierOptions,
  };
  return (
    <div className="change-section flex-col">
      <h5 data-cy="drawings-modal-title">
        {limit ? i18n('entities.subscription.drawings.increase') : i18n('entities.subscription.drawings.title')}
        <Tooltip title={i18n('entities.subscription.drawings.help')}>
          <QuestionCircleOutlined data-cy="drawings-modal-help" className="primaryIcon help-icon" />
        </Tooltip>
      </h5>
      <Select input={input} />
      {warn && (
        <p data-cy="drawings-modal-description" className="small warnMessage">
          {i18n('entities.subscription.drawings.warning')}
        </p>
      )}
    </div>
  );
};

Drawings.defaultProps = {
  currentDrawings: 1,
  warn: false,
  limit: false,
  change: () => null,
};

export default Drawings;
