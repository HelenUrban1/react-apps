import React from 'react';
import { i18n } from 'i18n';
import '../Subscription.less';
import './Drawings.less';
import { BillingPeriodEnum, ComponentPrice } from 'domain/setting/subscriptionTypes';

interface Props {
  interval: BillingPeriodEnum;
  prices: ComponentPrice[];
}
const DrawingPrices: React.FC<Props> = ({ interval, prices }) => {
  return (
    <div data-cy="drawings-modal-price" className="price-table flex-col">
      <div className="flex-col">
        <p>
          <strong>{i18n('entities.subscription.drawings.drawings')}</strong>
        </p>
        {prices.map((step) => (
          <p key={`${step.starting_quantity}-${step.ending_quantity}`}>{`${step.starting_quantity}-${step.ending_quantity}`}</p>
        ))}
      </div>
      <div className="flex-col">
        <p>
          <strong>{i18n('common.price')}</strong>
        </p>
        {prices.map((step) => {
          if (parseInt(step.unit_price, 10) === 0) {
            return <p key={step.unit_price}>{i18n('settings.billing.trial.trial')}</p>;
          }
          return <p key={step.unit_price}>{`${step.formatted_unit_price}/${interval === 'Monthly' ? i18n('common.month') : i18n('common.year')}`}</p>;
        })}
      </div>
    </div>
  );
};

export default DrawingPrices;
