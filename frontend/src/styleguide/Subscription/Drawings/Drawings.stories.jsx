import React from 'react';
import { storiesOf } from '@storybook/react';
import { useState } from '@storybook/addons';
import Drawings from './Drawings';

// const stories = storiesOf('Subscription/Drawings', module);

const stories = storiesOf('Subscription/Drawing', module);

stories.add('Default', () => {
  const [currentDrawings, setDrawings] = useState(1);
  const [warn, setWarn] = useState(false);

  const plan = {
    tier: 'standard',
    drawings: 2,
  };

  const change = (value) => {
    if (!value) {
      return;
    }
    if (value > plan.drawings) {
      setWarn(true);
    }
    if (warn && value <= plan.drawings) {
      setWarn(false);
    }
    setDrawings(value);
  };

  return <Drawings currentDrawings={currentDrawings} warn={warn} change={change} />;
});
