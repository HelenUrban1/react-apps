import React from 'react';
import { storiesOf } from '@storybook/react';
import Payment from './Payment';
import MDX from './Payment.mdx';

const stories = storiesOf('Subscription/Payment', module);

stories.add(
  'Default',
  () => {
    const story = <Payment />;
    return story;
  },
  {
    docs: { page: MDX },
  },
);
