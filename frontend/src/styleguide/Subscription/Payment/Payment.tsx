import React from 'react';
import { i18n } from 'i18n';
import { Radio } from 'antd';
import '../Subscription.less';

interface Props {
  due: string;
  change: (e: any) => void;
}

const Payment: React.FC<Props> = ({ due, change }) => {
  return (
    <div className="change-section flex-column">
      <h5 data-cy="modal-pay-title">{i18n('entities.subscription.pay.title')}</h5>
      <Radio.Group data-cy="modal-pay-input" className="paymentOptions" onChange={change} value={due}>
        <Radio value="Annually" data-cy="modal-pay-annual">
          {i18n('entities.subscription.pay.annual')}
        </Radio>
        <Radio value="Monthly" data-cy="modal-pay-monthly">
          {i18n('entities.subscription.pay.monthly')}
        </Radio>
      </Radio.Group>
    </div>
  );
};

Payment.defaultProps = {
  due: 'Annually',
  change: () => null,
};

export default Payment;
