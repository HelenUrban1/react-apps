import React from 'react';
import { storiesOf } from '@storybook/react';
import { useState } from '@storybook/addons';
import Seats from './Seats';

// const stories = storiesOf('Subscription/Seats', module);

const stories = storiesOf('Subscription/Seat', module);

stories.add('Default', () => {
  const [currentSeats, setSeats] = useState(1);
  const [warn, setWarn] = useState(false);

  const plan = {
    tier: 'standard',
    seats: 2,
  };

  const change = (value) => {
    if (!value) {
      return;
    }
    if (value > plan.seats) {
      setWarn(true);
    }
    if (warn && value <= plan.seats) {
      setWarn(false);
    }
    setSeats(value);
  };

  return <Seats currentSeats={currentSeats} warn={warn} change={change} />;
});
