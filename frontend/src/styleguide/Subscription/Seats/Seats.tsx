import React from 'react';
import { i18n } from 'i18n';
import { QuestionCircleOutlined } from '@ant-design/icons';
import { InputNumber, Tooltip } from 'antd';
import '../Subscription.less';

interface Props {
  currentSeats: number;
  warn: boolean;
  change: (e: any) => void;
}

const Seats: React.FC<Props> = ({ currentSeats, warn, change }) => {
  return (
    <div className="change-section">
      <h5 data-cy="modal-seats-title">
        {i18n('entities.subscription.seats.title')}
        <Tooltip title={i18n('entities.subscription.seats.help')}>
          <QuestionCircleOutlined data-cy="modal-seats-help" className="primaryIcon help-icon" />
        </Tooltip>
      </h5>
      <label htmlFor="seat-picker">
        <InputNumber id="seat-picker" data-cy="modal-seats-input" className={`seatsPicker${warn ? ' warnMessage' : ''}`} min={1} max={100} value={currentSeats} onChange={change} />
      </label>
      {warn && (
        <p data-cy="modal-seats-description" className="small warnMessage">
          {i18n('entities.subscription.seats.warning')}
        </p>
      )}
    </div>
  );
};

Seats.defaultProps = {
  currentSeats: 1,
  warn: false,
  change: () => null,
};

export default Seats;
