import React from 'react';
import { i18n } from 'i18n';
import { QuestionCircleOutlined } from '@ant-design/icons';
import { InputNumber, Tooltip } from 'antd';
import '../Subscription.less';

interface Props {
  name: string;
  handle: string;
  bought: number;
  warn: boolean;
  change: (handle: string, type: string, value: string) => void;
}

const UnitComponent: React.FC<Props> = ({ handle, name, bought, warn, change }) => {
  const handleChange = (value: string | number | null | undefined) => {
    if (!value) return;
    change(handle, 'per_unit', value.toString());
  };
  return (
    <div className="change-section" data-cy="modal-unit-component">
      <h5 data-cy={`${name}-title`}>
        {i18n(`entities.subscription.${name}.title`)}
        <Tooltip title={i18n(`entities.subscription.${name}.help`)}>
          <QuestionCircleOutlined data-cy={`${name}-help`} className="primaryIcon help-icon" />
        </Tooltip>
      </h5>
      <label htmlFor={`${name}-picker`}>
        <InputNumber //
          type="number"
          id={`${name}-picker`}
          data-cy={`${name}-input`}
          className={`seatsPicker${warn ? ' warnMessage' : ''}`}
          min={1}
          max={100}
          value={bought}
          onChange={handleChange}
        />
      </label>
      {warn && (
        <p data-cy={`${name}-description`} className="small warnMessage">
          {i18n(`entities.subscription.${name}.warning`)}
        </p>
      )}
    </div>
  );
};

export default UnitComponent;
