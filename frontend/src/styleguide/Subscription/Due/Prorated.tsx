import React from 'react';
import { i18n } from 'i18n';
import '../Subscription.less';

interface Props {
  due: string;
  remaining: string;
}

const Prorated: React.FC<Props> = ({ due, remaining }) => {
  return (
    <>
      <p data-cy="subscription-modal-due" style={{ marginBottom: 0 }}>
        {i18n('settings.billing.info.total')}
        <span className="span-bold">{due}</span>
        {i18n('settings.billing.info.remaining')}
        <span className="span-bold">{remaining}</span>
        {i18n('settings.billing.info.period')}
      </p>
      <p data-cy="estimate-disclaimer" className="small">
        <em>{i18n('settings.billing.info.estimate')}</em>
      </p>
    </>
  );
};

export default Prorated;
