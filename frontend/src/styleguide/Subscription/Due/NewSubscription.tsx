import React from 'react';
import { i18n } from 'i18n';
import { CreditCardOutlined } from '@ant-design/icons';
import IxButton from 'styleguide/Buttons/IxButton';
import '../Subscription.less';

interface Props {
  payment: string;
  token?: string;
  changeCreditCard: () => void;
  removeCreditCard?: () => void;
}

const NewSubscription: React.FC<Props> = ({ payment, token, changeCreditCard, removeCreditCard }) => {
  return (
    <div>
      <p data-cy="subscription-modal-due">
        {i18n('entities.subscription.new.total')}
        <span className="span-bold">{payment}</span>
        {i18n('entities.subscription.new.dueToday')}
      </p>
      {token && (
        <p data-cy="subscription-modal-card">
          {i18n('entities.subscription.new.accountIs')}
          <span className="span-bold" data-cy="subscription-modal-billing">
            {i18n('entities.subscription.new.automatic')}
          </span>
          <br />
          <button type="button" data-cy="change-subscription-card-remove" className="link trial-card" onClick={removeCreditCard}>
            {i18n('entities.subscription.new.removeCard')}
          </button>
        </p>
      )}
      {!token && (
        <p data-cy="subscription-modal-card">
          {i18n('entities.subscription.new.accountIs')}
          <span className="span-bold" data-cy="subscription-modal-billing">
            {i18n('entities.subscription.new.remittance')}
          </span>
          <br />
          <IxButton //
            onClick={changeCreditCard}
            data-cy="change-subscription-card-add"
            className="btn-primary"
            leftIcon={<CreditCardOutlined />}
            text={i18n('entities.subscription.new.addCard')}
            tipText={i18n('entities.subscription.new.addCard')}
            tipPlacement="top"
          />
        </p>
      )}
    </div>
  );
};

export default NewSubscription;
