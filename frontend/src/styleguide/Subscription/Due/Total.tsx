import React from 'react';
import { i18n } from 'i18n';
import '../Subscription.less';

interface Props {
  billingPeriod?: string;
  payment?: string;
  ending?: string;
}

const Total: React.FC<Props> = ({ billingPeriod, payment, ending }) => {
  return (
    <p data-cy="subscription-modal-plan">
      {i18n('common.next')}
      {billingPeriod && (
        <span className="span-bold" data-cy="subscription-plan-period">
          {i18n(`settings.billing.info.${billingPeriod.toLocaleLowerCase()}`)}
        </span>
      )}
      {i18n('settings.billing.info.paymentOf')}
      <span className="span-bold" data-cy="subscription-plan-price">
        {payment}
      </span>
      {i18n('settings.billing.info.billedOn')}
      <span className="span-bold">{ending}</span>
    </p>
  );
};

export default Total;
