import React from 'react';
import { i18n } from 'i18n';
import '../Subscription.less';

interface Props {
  cardType?: string;
  maskedCardNumber?: string;
  changeCreditCard: () => void;
}

const CreditCard: React.FC<Props> = ({ cardType, maskedCardNumber, changeCreditCard }) => {
  return (
    <p data-cy="subscription-modal-card">
      {i18n('entities.subscription.change.paidWith')}
      {cardType && (
        <span className="card-type">
          <img src={`${process.env.PUBLIC_URL}/images/${cardType}.jpg`} alt={cardType} />
        </span>
      )}
      {i18n('entities.subscription.change.endingIn')}
      {maskedCardNumber && <span className="span-bold">{`${maskedCardNumber?.substring(maskedCardNumber.length - 4, maskedCardNumber.length)}`}</span>}
      <button type="button" data-cy="change-subscription-card" className="link" onClick={changeCreditCard}>
        {i18n('common.change')}
      </button>
    </p>
  );
};

export default CreditCard;
