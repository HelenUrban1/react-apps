import React from 'react';
import { i18n } from 'i18n';
import '../Subscription.less';

interface Props {
  billingPeriod?: string;
  payment?: string;
  ending?: string;
}

const Deferred: React.FC<Props> = ({ billingPeriod, payment, ending }) => {
  return (
    <p data-cy="subscription-modal-defer">
      {i18n('settings.billing.info.deferred')}
      {billingPeriod && (
        <span className="span-bold" data-cy="subscription-defer-period">
          {i18n(`settings.billing.info.${billingPeriod.toLocaleLowerCase()}`)}
        </span>
      )}
      {i18n('settings.billing.info.paymentOf')}
      <span className="span-bold" data-cy="subscription-defer-price">
        {payment}
      </span>
      {i18n('settings.billing.info.on')}
      <span className="span-bold">{ending}</span>
    </p>
  );
};

export default Deferred;
