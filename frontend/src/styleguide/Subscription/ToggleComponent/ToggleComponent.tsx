import React from 'react';
import { i18n } from 'i18n';
import { QuestionCircleOutlined, CheckOutlined, CloseOutlined } from '@ant-design/icons';
import { Switch, Tooltip } from 'antd';
import '../Subscription.less';

interface Props {
  name: string;
  handle: string;
  bought: boolean;
  warn: boolean;
  change: (handle: string, type: string, value: string) => void;
}

const ToggleComponent: React.FC<Props> = ({ handle, name, bought, warn, change }) => {
  const handleChange = (value: boolean) => {
    change(handle, 'on/off', value ? '1' : '0');
  };
  return (
    <div className="change-section" data-cy="modal-unit-component">
      <h5 className="subscription-component-header" data-cy={`${name}-title`}>
        <Switch className="addon-toggle" data-cy={`${name}-toggle`} checkedChildren={<CheckOutlined />} unCheckedChildren={<CloseOutlined />} checked={bought} onChange={handleChange} />
        {i18n(`entities.subscription.${name}.title`)}
        <Tooltip title={i18n(`entities.subscription.${name}.help`)}>
          <QuestionCircleOutlined data-cy={`${name}-help`} className="primaryIcon help-icon" />
        </Tooltip>
      </h5>
      {warn && (
        <p data-cy={`${name}-description`} className="small warnMessage">
          {i18n(`entities.subscription.${name}.warning`)}
        </p>
      )}
    </div>
  );
};

export default ToggleComponent;
