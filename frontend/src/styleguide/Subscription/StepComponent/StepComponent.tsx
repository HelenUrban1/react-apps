import React, { useEffect } from 'react';
import { i18n } from 'i18n';
import { QuestionCircleOutlined } from '@ant-design/icons';
import { Tooltip } from 'antd';
import { InputType, Select } from 'view/global/inputs';
import { ComponentPrice } from 'domain/setting/subscriptionTypes';
import '../Subscription.less';
import './Step.less';

interface Props {
  name: string;
  handle: string;
  bought: string;
  special?: number;
  prices: ComponentPrice[];
  limit?: boolean;
  warn: boolean;
  disabled?: boolean;
  change: (handle: string, type: string, value: string) => void;
}

const StepComponent: React.FC<Props> = ({ handle, name, bought, prices, warn, change, special, limit = false, disabled = false }) => {
  const handleChange = (event: { target: { name: string; value: string } }) => {
    change(handle, 'stairstep', event.target.value);
  };
  useEffect(() => {
    handleChange({ target: { name: `${name}-step`, value: bought.toString() } });
  }, []);

  const tierOptions: InputType[] = prices.map((price) => {
    let label = `${price.starting_quantity}-${price.ending_quantity}`;
    if (!price.ending_quantity) {
      label = `${price.starting_quantity}+`;
    } else if (price.starting_quantity === price.ending_quantity) {
      label = price.starting_quantity.toString();
    }
    return {
      label,
      value: price.id.toString(),
      name: `${price.starting_quantity}-${price.ending_quantity}`,
      disabled,
    };
  });
  if (special) {
    tierOptions.push({
      label: i18n('entities.subscription.special', special),
      value: 'custom',
      name: 'custom',
      disabled,
    });
  }
  const input: InputType = {
    label: '',
    value: bought.toString(),
    name: `${name}-step`,
    classes: 'subscriptionInput',
    change: handleChange,
    group: tierOptions,
  };
  return (
    <div className="change-section flex-col" data-cy="modal-step-component">
      <h5 data-cy={`${name}-title`}>
        {limit ? i18n(`entities.subscription.${name}.increase`) : i18n(`entities.subscription.${name}.title`)}
        <Tooltip title={i18n(`entities.subscription.${name}.help`)}>
          <QuestionCircleOutlined data-cy={`${name}-help`} className="primaryIcon help-icon" />
        </Tooltip>
      </h5>
      <Select input={input} />
      {warn && (
        <p data-cy={`${name}-description`} className="small warnMessage">
          {i18n(`entities.subscription.${name}.warning`)}
        </p>
      )}
    </div>
  );
};

export default StepComponent;
