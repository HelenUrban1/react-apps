import React from 'react';
import { i18n } from 'i18n';
import '../Subscription.less';
import './Step.less';

interface Props {
  name: string;
  prices: any;
}
const StepPrices: React.FC<Props> = ({ name, prices }) => {
  return (
    <div data-cy={`${name}-modal-price`} className="price-table flex-col">
      <div className="flex-col">
        <p>
          <strong>{i18n(`entities.subscription.${name}.${name}`)}</strong>
        </p>
        {prices.map((step: any) => (
          <p key={step.id}>{`${step.starting_quantity}${step.ending_quantity ? `-${step.ending_quantity}` : '+'}`}</p>
        ))}
      </div>
      <div className="flex-col">
        <p>
          <strong>{i18n('common.price')}</strong>
        </p>
        {prices.map((step: any) => {
          if (step.unit_price === 0) {
            return <p key={step.unit_price}>{i18n('settings.billing.trial.trial')}</p>;
          }
          return <p key={step.unit_price}>{`${step.formatted_unit_price}/${i18n('common.month')}`}</p>;
        })}
      </div>
    </div>
  );
};

export default StepPrices;
