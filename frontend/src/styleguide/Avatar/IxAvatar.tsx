import React from 'react';
import { Avatar } from 'antd';
import { getUserColor, getUserInitials, getCompanyInitials } from 'utils/Avatars';

interface avatarProps {
  type: string;
  data: any;
  size: number | 'small' | 'large' | 'default';
}

export const IxAvatar = ({ type, data, size = 'default' }: avatarProps) => {
  const initials = type === 'user' ? getUserInitials(data) : getCompanyInitials(data);
  const color = getUserColor(data.id);
  return (
    <Avatar data-testid="avatar" shape={type === 'user' ? 'circle' : 'square'} size={size} style={{ color: '#F6F8FA', backgroundColor: color }}>
      {initials}
    </Avatar>
  );
};
