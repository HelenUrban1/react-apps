import React from 'react';
import { v4 as uuid } from 'uuid';
import { Checkbox, message, Tooltip, Upload } from 'antd';
import { UploadFile } from 'antd/lib/upload/interface';
import { i18n } from 'i18n';
import { CloudUploadOutlined, DeleteTwoTone, FileExcelOutlined, FileImageOutlined, FileJpgOutlined, FileOutlined, FilePdfOutlined } from '@ant-design/icons';
import { isPDF, isExcel, isImage, isAttachment } from 'utils/files';
import config from 'config';
import { hasMatchingDimensionsViewer } from 'styleguide/PdfViewer/PdfViewerUtil';
import { hasMatchingDimensionsFromFile } from 'modules/part/partApi';
import { useSelector } from 'react-redux';
import { AppState } from 'modules/state';
import { Drawing } from 'graphql/drawing';
import { CheckboxChangeEvent } from 'antd/lib/checkbox';

const { Dragger } = Upload;

export interface FileObject {
  uuid: string;
  uid: string;
  size: number | undefined;
  name: string;
  fileName: string;
  status: string;
  response: string;
  type: string | undefined;
  ocr: boolean;
  originFileObj: any;
}

type Props = {
  entity: string;
  fileType: 'pdf' | 'excel' | 'image' | 'any';
  fileExt: string;
  hint: string;
  limit: number;
  classes?: string;
  files: { [key: string]: FileObject };
  setFiles: (file: { [key: string]: FileObject }) => void;
  errorMessages?: string[];
  setErrorMessages: (errors?: string[]) => void;
  type: 'Drawing' | 'Attachment' | 'Report';
  hasMatchingDims?: boolean;
  setHasMatchingDims?: React.Dispatch<React.SetStateAction<boolean>>;
  skipAlignment?: boolean;
  setSkipAlignment?: React.Dispatch<React.SetStateAction<boolean>>;
  targetDrawing?: Drawing | null | undefined;
};

export const FileUploadDragger: React.FC<Props> = ({ entity, fileType, fileExt, hint, limit, classes, files, setFiles, errorMessages, setErrorMessages, type, hasMatchingDims, setHasMatchingDims, skipAlignment, setSkipAlignment, targetDrawing }) => {
  const { tronInstance } = useSelector((state: AppState) => state.pdfView);
  const { jwt } = useSelector((state: AppState) => state.auth);

  const beforeUpload = () => {
    return false;
  };

  const checkValidFileType = (fileList: { [key: string]: FileObject }): { newList: { [key: string]: FileObject }; errMessage: string | undefined } => {
    const newFiles: { [key: string]: FileObject } = {};
    let errMessage;

    Object.values(fileList).forEach((file) => {
      const newFile = { ...file };
      switch (fileType) {
        case 'pdf':
          if (!isPDF(file.name)) {
            errMessage = i18n('importer.errors.invalidFilePdf');
            newFile.status = 'error';
            newFile.response = i18n('importer.errors.invalidFilePdf');
          }
          break;
        case 'excel':
          if (!isExcel(file.name)) {
            errMessage = i18n('importer.errors.invalidFileExcel');
            newFile.status = 'error';
            newFile.response = i18n('importer.errors.invalidFileExcel');
          }
          break;
        case 'image':
          if (!isImage(file.name)) {
            errMessage = i18n('importer.errors.invalidFileImage');
            newFile.status = 'error';
            newFile.response = i18n('importer.errors.invalidFileImage');
          }
          break;
        default:
          if (!isAttachment(file.name)) {
            errMessage = i18n('importer.errors.invalidFileAttachment');
            newFile.status = 'error';
            newFile.response = i18n('importer.errors.invalidFileAttachment');
          }
          break;
      }
      newFiles[file.uid] = newFile;
    });

    return { newList: newFiles, errMessage };
  };

  const checkValidTypeOnDrop = (name: string) => {
    switch (fileType) {
      case 'pdf':
        if (!isPDF(name)) return false;
        return true;
      case 'excel':
        if (!isExcel(name)) return false;
        return true;
      case 'image':
        if (!isImage(name)) return false;
        return true;
      default:
        return true;
    }
  };

  // error handling for drag and drop
  const handleDrop = (e: any) => {
    e.preventDefault();
    if (errorMessages && errorMessages.length > 0) {
      errorMessages.forEach((err) => {
        message.error(err, 5);
      });
      return;
    }
    if (e.dataTransfer.items) {
      for (let i = 0; i < e.dataTransfer.items.length; i++) {
        if (e.dataTransfer.items[i].kind === 'file') {
          const file = e.dataTransfer.items[i].getAsFile();
          if (!checkValidTypeOnDrop(file.name)) {
            let newFiles = { ...files };
            const newFile = {
              uuid: uuid(),
              uid: file.uid,
              size: file.size,
              name: file.name,
              fileName: file.name,
              status: 'error',
              response: i18n('importer.error.invalidFile'),
              type: file.type,
              ocr: false,
              originFileObj: file.originFileObj,
            };
            newFiles[file.uid] = newFile;
            const validTypes = checkValidFileType(newFiles);
            const newErrs = [];
            if (validTypes.errMessage) {
              newErrs.push(validTypes.errMessage);
              newFiles = validTypes.newList;
            }
            setErrorMessages(newErrs);
            setFiles(newFiles);
          }
        }
      }
    }
  };

  // get the file type icons for the upload helper text
  const getFileIcons = () => {
    switch (fileType) {
      case 'pdf':
        return <FilePdfOutlined />;
      case 'excel':
        return <FileExcelOutlined />;
      case 'image':
        return (
          <>
            <FileImageOutlined />
            <FileJpgOutlined />
          </>
        );
      default:
        return <FileOutlined />;
    }
  };

  // get the file type icon for the uploaded file
  const getFileIcon = (file: FileObject) => {
    if (isPDF(file.name || '')) {
      return <FilePdfOutlined />;
    }
    if (isExcel(file.name || '')) {
      return <FileExcelOutlined />;
    }
    if (isImage(file.name || '')) {
      return <FileImageOutlined />;
    }
    return <FileOutlined />;
  };

  const handleChange = async ({ fileList }: { fileList: UploadFile[] }) => {
    const newList = { ...files };
    if (fileList.length < 1) {
      setFiles({});
      setErrorMessages(undefined);
      return;
    }

    const overLimit = fileList.length > limit;
    const newErrs = [];
    if (overLimit) {
      newErrs.push(i18n('importer.errors.invalidFileExtra', limit));
    }
    fileList.forEach((file, index) => {
      if (index + 1 > limit) {
        newList[file.uid] = {
          ...newList[file.uid],
          uid: file.uid,
          size: file.size,
          name: file.name,
          fileName: file.name,
          type: file.type,
          status: 'error',
          response: i18n('importer.errors.invalidFileExtra', hint, entity),
          ocr: false,
          originFileObj: file.originFileObj,
        };
      } else {
        const ocr = false;
        const currentOcr = files[file.uid] !== undefined && config.autoBallooning.enabled ? files[file.uid].ocr : ocr;
        newList[file.uid] = {
          ...newList[file.uid],
          uid: file.uid,
          size: file.size,
          name: file.name,
          fileName: file.name,
          type: file.type,
          status: 'success',
          response: '',
          ocr: currentOcr,
          originFileObj: file.originFileObj,
        };
      }
    });

    const validTypes = checkValidFileType(newList);
    if (validTypes.errMessage) {
      newErrs.push(validTypes.errMessage);
    }

    if (targetDrawing) {
      const match = tronInstance ? await hasMatchingDimensionsViewer(newList[fileList[0].uid].originFileObj, tronInstance) : await hasMatchingDimensionsFromFile(targetDrawing, newList[fileList[0].uid], jwt || '');
      if (setHasMatchingDims && match) setHasMatchingDims(true);
    }
    setErrorMessages(newErrs);
    setFiles(validTypes.newList);
  };

  const FileRow = ({ file }: { file: UploadFile }) => {
    const fileObj = files[file.uid];
    const keys = Object.keys(files);
    if (!fileObj) {
      return <></>;
    }
    const setOcr = () => {
      setFiles({
        ...files,
        [file.uid]: {
          ...fileObj,
          ocr: !fileObj.ocr,
        },
      });
    };

    const deleteFile = () => {
      const newFiles = { ...files };
      const overLimit = keys.length > limit;
      delete newFiles[file.uid];
      if (setHasMatchingDims) setHasMatchingDims(false);
      if (overLimit && keys.length - 1 <= limit) {
        // Update Limit
        const newErrs = [];
        Object.entries(newFiles).forEach(([key, val]) => {
          newFiles[key] = {
            ...val,
            status: 'success',
            response: '',
          };
        });
        const validTypes = checkValidFileType(newFiles);
        if (validTypes.errMessage) {
          newErrs.push(validTypes.errMessage);
        }
        setErrorMessages(newErrs);
        setFiles(validTypes.newList);
        return;
      }
      setFiles(newFiles);
    };

    return (
      <div data-cy="file-render-row">
        {keys[0] === file.uid && config.autoBallooning.enabled && <p className="ab-info">{type === 'Drawing' ? 'Select documents to Autoballoon' : 'Attached documents will not be shown during ballooning'}</p>}
        <div className="file-container">
          {type === 'Drawing' && config.autoBallooning.enabled && (
            <div className="set-ocr">
              <Checkbox name="ocr-toggle" type="checkbox" checked={fileObj.ocr} onChange={setOcr} disabled={fileObj.status === 'error' || !isPDF(fileObj.name)} />
            </div>
          )}
          <div className="ant-upload-list-item">
            <div className="icon">{getFileIcon(fileObj)}</div>
            <div className={`file-name${fileObj.status === 'error' ? ' file-upload-error-message' : ''}`}>{file.name}</div>
            <button type="button" className="delete" onClick={deleteFile}>
              <DeleteTwoTone twoToneColor="#f5222d" />
            </button>
          </div>
        </div>
        {targetDrawing && (
          <div className="set-skip-alignment">
            <div className="set-skip-alignment-title">Skip drawing alignment for this revision</div>
            <Tooltip title={hasMatchingDims ? undefined : 'Drawings have different dimensions, alignment required'}>
              <span className="set-skip-alignment-checkbox">
                <Checkbox
                  name="alignment-toggle"
                  type="checkbox"
                  checked={hasMatchingDims ? skipAlignment : false}
                  onChange={(e: CheckboxChangeEvent) => {
                    if (setSkipAlignment) setSkipAlignment(e.target.checked);
                  }}
                  style={{ marginRight: '8px' }}
                  disabled={!hasMatchingDims}
                />
                Skip alignment
              </span>
            </Tooltip>
          </div>
        )}
        {keys[keys.length - 1] === file.uid && keys.length < limit && <p className="upload-info">{i18n('importer.uploaded', keys.length)}</p>}
        {keys[keys.length - 1] === file.uid && keys.length >= limit && <p className="upload-info">{i18n('importer.limit', keys.length)}</p>}
      </div>
    );
  };

  const getAcceptableExtensions = () => {
    switch (fileType) {
      case 'any':
        return '.doc,.docx,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,.pdf,.txt,.csv,.xlsx,.png,.jpeg,.jpg,.gif,.tiff,.svg';
      case 'pdf':
        return '.pdf';
      case 'excel':
        return '.csv,.xlsx';
      case 'image':
        return '.png,.jpeg,.jpg,.gif,.tiff';
      default:
        return `.${fileType}`;
    }
  };

  return (
    <section className="file-upload" onDrop={handleDrop}>
      <Dragger
        listType="picture"
        multiple={limit > 1}
        accept={getAcceptableExtensions()}
        className={`${classes} file-upload-dragger${Object.values(files).length >= limit ? ' file-upload-dragger-disabled' : ''}${Object.values(files).length > 0 ? ' file-upload-dragger-condensed' : ''}`}
        fileList={Object.values(files) as any[]}
        onChange={handleChange}
        beforeUpload={beforeUpload}
        itemRender={(originNode, file) => <FileRow file={file} />}
      >
        <p className="ant-upload-drag-icon">
          <CloudUploadOutlined />
        </p>
        <p className="ant-upload-file-type-icons">{getFileIcons()}</p>
        <p className="ant-upload-text">{`${i18n('importer.form.hint', hint)}`}</p>
      </Dragger>
      {errorMessages && (
        <section className="file-upload-error-message">
          {errorMessages.map((err) => (
            <p key={`msg-key-${err.replaceAll(' ', '')}`}>{err}</p>
          ))}
        </section>
      )}
    </section>
  );
};
