// eslint-disable jsx-props-no-spreading

import React, { useEffect, useState } from 'react';
import { ContainerOutlined } from '@ant-design/icons';
import { ConfigProvider, List } from 'antd';
import PropTypes from 'prop-types';
import { FixedSizeList, areEqual } from 'react-window';
import { DragDropContext, Droppable, Draggable } from 'react-beautiful-dnd';
import { isEqual } from 'lodash';
import { i18n } from 'i18n';
import {
  //
  getDraggingItemHeight,
  moveDragToNewIndex,
  getStyle,
  isSelected,
  getMarker,
  ItemListPropCompare,
  dragMultipleToNewIndex,
  listItemClass,
} from 'utils/DragUtilities';
import ItemCard from '../ItemCard/ItemCard';
import './ItemList.less';

const EmptyItemList = () => {
  return (
    <div className="ant-empty ant-empty-normal ant-empty-small">
      <p>{i18n('entities.feature.list.none')}</p>
      <ContainerOutlined style={{ fontSize: 48 }} />
      <p style={{ marginTop: '18px' }}>{i18n('entities.feature.list.start')}</p>
    </div>
  );
};

// Display Feature li and card

const Item = ({
  //
  provided,
  item,
  items,
  selected,
  markers,
  defaultMarker,
  className,
  style,
  shiftForGroup,
  itemWidth,
  collapsed,
  expand,
  expandGroup,
  onClick,
  maxNumber,
  editItem,
  onTextBlur,
  onDropChange,
  onDropdownVisibleChange,
  handleShowModal,
  loaded,
}) => {
  const active = isSelected(item, selected);
  const draggableStyle = getStyle({ provided, style, shiftForGroup });
  let isGhosting = shiftForGroup.height !== null && active;
  // Multiple Selected Features Dragging
  if (className === 'copyItem' && selected && selected.length > 0) {
    return (
      <div aria-hidden {...provided.draggableProps} {...provided.dragHandleProps} ref={provided.innerRef} style={draggableStyle} className="draggable-item" onMouseDown={(e) => e.currentTarget.focus()}>
        {selected.map((feature) => {
          const subActive = isSelected(feature, selected);
          const marker = getMarker(feature, markers, defaultMarker);
          const itemClass = listItemClass(feature, selected, className);
          return (
            <List.Item key={`grouped-${feature.id}`} className={itemClass}>
              <div className="ix-list-item" style={{ position: 'relative' }}>
                <ItemCard //
                  item={feature}
                  marker={marker}
                  itemWidth={itemWidth}
                  collapsed={collapsed}
                  expand={expand}
                  expandGroup={expandGroup}
                  active={subActive}
                  onClick={onClick}
                  maxNumber={maxNumber}
                  editItem={editItem}
                  onTextBlur={onTextBlur}
                  onDropChange={onDropChange}
                  onDropdownVisibleChange={onDropdownVisibleChange}
                  handleShowModal={handleShowModal}
                  loaded={loaded}
                />
              </div>
            </List.Item>
          );
        })}
      </div>
    );
  }
  // Group of features
  if (item.markerSubIndex === 0) {
    const featureGroup = items.filter((feature) => feature.markerGroup === item.markerGroup);
    const selectIDs = selected.map((a) => a.id);
    const groupIDs = featureGroup.map((b) => b.id);
    isGhosting = shiftForGroup.height !== null && selectIDs.some((id) => groupIDs.indexOf(id) !== -1);
    return (
      <div aria-hidden {...provided.draggableProps} {...provided.dragHandleProps} ref={provided.innerRef} style={draggableStyle} className="draggable-item" onMouseDown={(e) => e.currentTarget.focus()}>
        {featureGroup.map((feature) => {
          const subActive = isSelected(feature, selected);
          const marker = getMarker(feature, markers, defaultMarker);
          const itemClass = listItemClass(feature, selected, isGhosting ? `${className} ghosting` : className);
          return (
            <List.Item key={`grouped-${feature.id}`} className={itemClass}>
              <div className="ix-list-item" style={{ position: 'relative' }}>
                <ItemCard //
                  item={feature}
                  marker={marker}
                  itemWidth={itemWidth}
                  collapsed={collapsed}
                  expand={expand}
                  expandGroup={expandGroup}
                  active={subActive}
                  onClick={onClick}
                  maxNumber={maxNumber}
                  editItem={editItem}
                  onTextBlur={onTextBlur}
                  onDropChange={onDropChange}
                  onDropdownVisibleChange={onDropdownVisibleChange}
                  handleShowModal={handleShowModal}
                  loaded={loaded}
                />
              </div>
            </List.Item>
          );
        })}
      </div>
    );
  }
  // Single feature
  if (item.markerSubIndex === -1 || item.markerSubIndex === null || item.markerSubIndex === undefined) {
    const marker = getMarker(item, markers, defaultMarker);
    const itemClass = listItemClass(item, selected, isGhosting ? `${className} ghosting` : className);
    return (
      <div aria-hidden {...provided.draggableProps} {...provided.dragHandleProps} ref={provided.innerRef} style={draggableStyle} className="draggable-item" onMouseDown={(e) => e.currentTarget.focus()}>
        <List.Item className={itemClass}>
          <div className="ix-list-item" style={{ position: 'relative' }}>
            <ItemCard //
              item={item}
              marker={marker}
              itemWidth={itemWidth}
              collapsed={collapsed}
              expand={expand}
              expandGroup={expandGroup}
              active={active}
              onClick={onClick}
              maxNumber={maxNumber}
              editItem={editItem}
              onTextBlur={onTextBlur}
              onDropChange={onDropChange}
              onDropdownVisibleChange={onDropdownVisibleChange}
              handleShowModal={handleShowModal}
              loaded={loaded}
            />
          </div>
        </List.Item>
      </div>
    );
  }
};

const ItemList = ({
  //
  itemList,
  markers,
  collapsed,
  listRef,
  review,
  defaultMarker,
  selected,
  itemWidth,
  expand,
  expandGroup,
  editItem,
  onClick,
  onDropdownVisibleChange,
  onTextBlur,
  onDropChange,
  onDragFeature,
  handleShowModal,
  loaded,
}) => {
  // Use state for optimistic reordering of cards
  const [items, setItems] = useState(itemList);
  useEffect(() => {
    if (!isEqual(items, itemList)) {
      setItems(itemList);
    }
  }, [itemList]);

  // Use state so we can drag unselected items without re-rendering in between
  const [select, setSelect] = useState(selected);
  useEffect(() => {
    if (!isEqual(select, selected)) {
      setSelect(selected);
    }
  }, [selected]);

  // Save dragged card or group height for style adjustment
  const [shiftForGroup, setShift] = useState({ top: 0, height: null });

  // empty buffer
  const maxNumber = itemList[itemList.length - 2] ? parseInt(itemList[itemList.length - 2].markerGroup, 10) : 0;

  // Memoized row component to add drag handlers
  const Row = React.memo(({ data, index, style }) => {
    const item = data[index];
    // Buffer Item
    if (!item.id) {
      return (
        <List.Item className="end-list-item" style={style} index={index}>
          <div className="ix-list-item ix-list-item-bottom" style={{ position: 'relative' }} />
        </List.Item>
      );
    }
    // Sub Feature
    if (item.markerSubIndex > 0) {
      return null;
    }

    const draggableIndex = parseInt(item.markerGroup, 10) - 1;
    return (
      <Draggable draggableId={item.id} index={draggableIndex} key={item.id}>
        {(provided) => (
          <Item //
            provided={provided}
            item={item}
            items={items}
            markers={markers}
            defaultMarker={defaultMarker}
            selected={select}
            style={style}
            shiftForGroup={shiftForGroup}
            itemWidth={itemWidth}
            collapsed={collapsed}
            expand={expand}
            expandGroup={expandGroup}
            onClick={onClick}
            maxNumber={maxNumber}
            editItem={editItem}
            onTextBlur={onTextBlur}
            onDropChange={onDropChange}
            onDropdownVisibleChange={onDropdownVisibleChange}
            handleShowModal={handleShowModal}
            loaded={loaded}
          />
        )}
      </Draggable>
    );
  }, areEqual);

  const height = document.getElementsByClassName('feature-list-container')[0] ? document.getElementsByClassName('feature-list-container')[0].offsetHeight : 600;
  // Set shiftForGroup to adjust styles when dragging
  const onDragStart = (result) => {
    // Reset selection if dragging unselected feature
    if (select && select.length > 0) {
      const selectIDs = select.map((a) => a.id);
      if (!selectIDs.includes(result.draggableId)) {
        const char = items.find((a) => a.id === result.draggableId);
        if (char.markerSubIndex === 0) {
          const group = items.filter((feature) => feature.markerGroup === char.markerGroup);
          const groupIDs = group.map((b) => b.id);
          if (!selectIDs.some((id) => groupIDs.indexOf(id) !== -1)) {
            setSelect(group);
          }
        } else {
          setSelect([char]);
        }
      }
    }
    const draggingHeight = getDraggingItemHeight(result, items);
    if (!draggingHeight) {
      return;
    }
    setShift(draggingHeight);
  };

  // Splice item list to update UI and send to be renumbered
  const onDragEnd = (result) => {
    setShift({ top: 0, height: null });
    let adjustedList;
    if (select && select.length > 0) {
      adjustedList = dragMultipleToNewIndex(result, items, select);
    } else {
      adjustedList = moveDragToNewIndex(result, items);
    }

    if (!adjustedList) {
      return;
    }
    // trigger optimistic render
    setItems(adjustedList);
    // remove buffer item before renumbering
    const newList = adjustedList.filter((item) => !!item.id);
    onDragFeature(newList, select);
  };

  return (
    <ConfigProvider renderEmpty={EmptyItemList}>
      {itemList.length === 0 ? (
        <EmptyItemList />
      ) : (
        <DragDropContext onDragStart={onDragStart} onDragEnd={onDragEnd}>
          <Droppable //
            droppableId="droppable"
            mode="virtual"
            renderClone={(provided, snapshot, rubric) => {
              // renders the card(s) when dragging
              const item = items.find((feature) => feature.id === rubric.draggableId);
              return (
                <Item //
                  provided={provided}
                  item={item}
                  items={items}
                  className="copyItem"
                  markers={markers}
                  defaultMarker={defaultMarker}
                  selected={select}
                  shiftForGroup={shiftForGroup}
                  itemWidth={itemWidth}
                  collapsed={collapsed}
                  expand={expand}
                  expandGroup={expandGroup}
                  onClick={onClick}
                  maxNumber={maxNumber}
                  editItem={editItem}
                  onTextBlur={onTextBlur}
                  onDropChange={onDropChange}
                  onDropdownVisibleChange={onDropdownVisibleChange}
                  handleShowModal={handleShowModal}
                  loaded={loaded}
                />
              );
            }}
          >
            {(provided) => (
              <List split={false} loading={markers.length === 0}>
                <FixedSizeList //
                  outerRef={provided.innerRef}
                  ref={listRef}
                  height={height}
                  width={collapsed ? 80 : 350}
                  itemCount={items.length}
                  itemSize={48}
                  itemData={items}
                  overscanCount={3}
                  className={`feature-vlist ${review ? 'review-step' : ''}`}
                  style={{ overflow: 'auto' }}
                >
                  {Row}
                </FixedSizeList>
              </List>
            )}
          </Droppable>
        </DragDropContext>
      )}
    </ConfigProvider>
  );
};

const memoize = React.memo(ItemList, ItemListPropCompare);

// Not sure why this file isn't typescript?
ItemList.propTyps = {
  /** List of Part's Items */
  itemList: PropTypes.array,

  /** Array of currently active item IDs */
  selected: PropTypes.array,

  /** Whether the Panel is Collapsed */
  collapsed: PropTypes.bool,

  /** Whether the Wizard is in the Review Step */
  review: PropTypes.bool,

  /** What to do when an Item is clicked */
  onClick: PropTypes.func,

  /** Item Width - width of the Item# field */
  itemWidth: PropTypes.number,
};

ItemList.defaultProps = {
  /** Item List is empty by Default */
  itemList: [],

  /** The first item is selected by Default */
  selected: [],

  /** The List is Open by Default */
  collapsed: false,

  /** Default onClick prints to console */
  onClick: () => {},

  /** Default Item Width is 3 */
  itemWidth: 3,
};

export default memoize;
