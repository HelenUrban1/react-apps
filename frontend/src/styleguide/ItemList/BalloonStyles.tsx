import React from 'react';

export interface Balloon {
  svg: any;
  disabled?: boolean;
  name: string; // A key phrase describing the SVG
  // i.e. solid-color-shape or hollow-color-shape. May need to add stroke when introducing user built
}

export const BalloonStyles: Balloon[] = [
  // Red
  {
    svg: (
      <svg id="solid-red-circle" width="33" height="33" viewBox="0 0 33 33" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M16.5 33C25.6127 33 33 25.6127 33 16.5C33 7.3873 25.6127 0 16.5 0C7.3873 0 0 7.3873 0 16.5C0 25.6127 7.3873 33 16.5 33Z" fill="#B72E1A" />
      </svg>
    ),
    disabled: true,
    name: 'solid-red-circle',
  },
  {
    svg: (
      <svg id="hollow-red-circle" width="33" height="33" viewBox="0 0 33 33" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M32.5 16.5001C32.5 25.3244 25.3486 32.5 16.5 32.5C7.65138 32.5 0.5 25.3244 0.5 16.5001C0.5 7.67561 7.65138 0.5 16.5 0.5C25.3486 0.5 32.5 7.67561 32.5 16.5001Z" stroke="#B72E1A" />
      </svg>
    ),
    disabled: false,
    name: 'hollow-red-circle',
  },
  {
    svg: (
      <svg id="solid-red-triangle" width="33" height="33" viewBox="0 0 33 33" fill="none" xmlns="http://www.w3.org/2000/svg">
        <g clipPath="url(#clip0)">
          <path d="M16.2822 0L32.7822 33H-0.217773L16.2822 0Z" fill="#B72E1A" />
        </g>
        <defs>
          <clipPath id="clip0">
            <rect width="33" height="33" fill="white" />
          </clipPath>
        </defs>
      </svg>
    ),
    disabled: false,
    name: 'solid-red-triangle',
  },
  {
    svg: (
      <svg id="hollow-red-triangle" width="33" height="33" viewBox="0 0 33 33" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M32.191 32.5H0.809017L16.5 1.11803L32.191 32.5Z" stroke="#B72E1A" />
      </svg>
    ),
    disabled: false,
    name: 'hollow-red-triangle',
  },
  {
    svg: (
      <svg id="solid-red-diamond" width="33" height="33" viewBox="0 0 33 33" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M0.707107 16.5L16.5 0.707106L32.2929 16.5L16.5 32.2929L0.707107 16.5Z" stroke="#B72E1A" />
      </svg>
    ),
    disabled: false,
    name: 'solid-red-diamond',
  },
  {
    svg: (
      <svg id="hollow-red-diamond" width="33" height="33" viewBox="0 0 33 33" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M0.707107 16.5L16.5 0.707106L32.2929 16.5L16.5 32.2929L0.707107 16.5Z" stroke="#B72E1A" />
      </svg>
    ),
    disabled: false,
    name: 'hollow-red-diamond',
  },
  // Purple
  {
    svg: (
      <svg id="solid-purple-circle" width="33" height="33" viewBox="0 0 33 33" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M16.5 33C25.6127 33 33 25.6127 33 16.5C33 7.3873 25.6127 0 16.5 0C7.3873 0 0 7.3873 0 16.5C0 25.6127 7.3873 33 16.5 33Z" fill="#78229D" />
      </svg>
    ),
    disabled: false,
    name: 'solid-purple-circle',
  },
  {
    svg: (
      <svg id="hollow-purple-circle" width="33" height="33" viewBox="0 0 33 33" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M32.5 16.5001C32.5 25.3244 25.3486 32.5 16.5 32.5C7.65138 32.5 0.5 25.3244 0.5 16.5001C0.5 7.67561 7.65138 0.5 16.5 0.5C25.3486 0.5 32.5 7.67561 32.5 16.5001Z" stroke="#78229D" />
      </svg>
    ),
    disabled: false,
    name: 'hollow-purple-circle',
  },
  {
    svg: (
      <svg id="solid-purple-triangle" width="33" height="33" viewBox="0 0 33 33" fill="none" xmlns="http://www.w3.org/2000/svg">
        <g clipPath="url(#clip0)">
          <path d="M16.2822 0L32.7822 33H-0.217773L16.2822 0Z" fill="#78229D" />
        </g>
        <defs>
          <clipPath id="clip0">
            <rect width="33" height="33" fill="white" />
          </clipPath>
        </defs>
      </svg>
    ),
    disabled: false,
    name: 'solid-purple-triangle',
  },
  {
    svg: (
      <svg id="hollow-purple-triangle" width="33" height="33" viewBox="0 0 33 33" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M32.191 32.5H0.809017L16.5 1.11803L32.191 32.5Z" stroke="#78229D" />
      </svg>
    ),
    disabled: false,
    name: 'hollow-purple-triangle',
  },
  {
    svg: (
      <svg id="solid-purple-diamond" width="33" height="33" viewBox="0 0 33 33" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M16.5 0L33 16.5L16.5 33L0 16.5L16.5 0Z" fill="#78229D" />
      </svg>
    ),
    disabled: false,
    name: 'solid-purple-diamond',
  },
  {
    svg: (
      <svg id="hollow-purple-diamond" width="33" height="33" viewBox="0 0 33 33" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M0.707107 16.5L16.5 0.707106L32.2929 16.5L16.5 32.2929L0.707107 16.5Z" stroke="#78229D" />
      </svg>
    ),
    disabled: false,
    name: 'hollow-purple-diamond',
  },
  // Orange
  {
    svg: (
      <svg id="solid-orange-circle" width="33" height="33" viewBox="0 0 33 33" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M16.5 33C25.6127 33 33 25.6127 33 16.5C33 7.3873 25.6127 0 16.5 0C7.3873 0 0 7.3873 0 16.5C0 25.6127 7.3873 33 16.5 33Z" fill="#E47113" />
      </svg>
    ),
    disabled: false,
    name: 'solid-orange-circle',
  },
  {
    svg: (
      <svg id="hollow-orange-circle" width="33" height="33" viewBox="0 0 33 33" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M32.5 16.5001C32.5 25.3244 25.3486 32.5 16.5 32.5C7.65138 32.5 0.5 25.3244 0.5 16.5001C0.5 7.67561 7.65138 0.5 16.5 0.5C25.3486 0.5 32.5 7.67561 32.5 16.5001Z" stroke="#E47113" />
      </svg>
    ),
    disabled: false,
    name: 'hollow-orange-circle',
  },
  {
    svg: (
      <svg id="solid-orange-triangle" width="33" height="33" viewBox="0 0 33 33" fill="none" xmlns="http://www.w3.org/2000/svg">
        <g clipPath="url(#clip0)">
          <path d="M16.2822 0L32.7822 33H-0.217773L16.2822 0Z" fill="#E47113" />
        </g>
        <defs>
          <clipPath id="clip0">
            <rect width="33" height="33" fill="white" />
          </clipPath>
        </defs>
      </svg>
    ),
    disabled: false,
    name: 'solid-orange-triangle',
  },
  {
    svg: (
      <svg id="hollow-orange-triangle" width="33" height="33" viewBox="0 0 33 33" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M32.191 32.5H0.809017L16.5 1.11803L32.191 32.5Z" stroke="#E47113" />
      </svg>
    ),
    disabled: false,
    name: 'hollow-orange-triangle',
  },
  {
    svg: (
      <svg id="solid-orange-diamond" width="33" height="33" viewBox="0 0 33 33" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M16.5 0L33 16.5L16.5 33L0 16.5L16.5 0Z" fill="#E47113" />
      </svg>
    ),
    disabled: false,
    name: 'solid-orange-diamond',
  },
  {
    svg: (
      <svg id="hollow-orange-diamond" width="33" height="33" viewBox="0 0 33 33" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M0.707107 16.5L16.5 0.707106L32.2929 16.5L16.5 32.2929L0.707107 16.5Z" stroke="#E47113" />
      </svg>
    ),
    disabled: false,
    name: 'hollow-orange-diamond',
  },
  // Green
  {
    svg: (
      <svg id="solid-green-circle" width="33" height="33" viewBox="0 0 33 33" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M16.5 33C25.6127 33 33 25.6127 33 16.5C33 7.3873 25.6127 0 16.5 0C7.3873 0 0 7.3873 0 16.5C0 25.6127 7.3873 33 16.5 33Z" fill="#017F74" />
      </svg>
    ),
    disabled: false,
    name: 'solid-green-circle',
  },
  {
    svg: (
      <svg id="hollow-green-circle" width="33" height="33" viewBox="0 0 33 33" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M32.5 16.5001C32.5 25.3244 25.3486 32.5 16.5 32.5C7.65138 32.5 0.5 25.3244 0.5 16.5001C0.5 7.67561 7.65138 0.5 16.5 0.5C25.3486 0.5 32.5 7.67561 32.5 16.5001Z" stroke="#017F74" />
      </svg>
    ),
    disabled: false,
    name: 'hollow-green-circle',
  },
  {
    svg: (
      <svg id="solid-green-triangle" width="33" height="33" viewBox="0 0 33 33" fill="none" xmlns="http://www.w3.org/2000/svg">
        <g clipPath="url(#clip0)">
          <path d="M16.2822 0L32.7822 33H-0.217773L16.2822 0Z" fill="#017F74" />
        </g>
        <defs>
          <clipPath id="clip0">
            <rect width="33" height="33" fill="white" />
          </clipPath>
        </defs>
      </svg>
    ),
    disabled: false,
    name: 'solid-green-triangle',
  },
  {
    svg: (
      <svg id="hollow-green-triangle" width="33" height="33" viewBox="0 0 33 33" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M32.191 32.5H0.809017L16.5 1.11803L32.191 32.5Z" stroke="#017F74" />
      </svg>
    ),
    disabled: false,
    name: 'hollow-green-triangle',
  },
  {
    svg: (
      <svg id="solid-green-diamond" width="33" height="33" viewBox="0 0 33 33" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M16.5 0L33 16.5L16.5 33L0 16.5L16.5 0Z" fill="#017F74" />
      </svg>
    ),
    disabled: false,
    name: 'solid-green-diamond',
  },
  {
    svg: (
      <svg id="hollow-green-diamond" width="33" height="33" viewBox="0 0 33 33" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M0.707107 16.5L16.5 0.707106L32.2929 16.5L16.5 32.2929L0.707107 16.5Z" stroke="#017F74" />
      </svg>
    ),
    disabled: false,
    name: 'hollow-green-diamond',
  },
  // Black
  {
    svg: (
      <svg id="solid-black-circle" width="33" height="33" viewBox="0 0 33 33" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M16.5 33C25.6127 33 33 25.6127 33 16.5C33 7.3873 25.6127 0 16.5 0C7.3873 0 0 7.3873 0 16.5C0 25.6127 7.3873 33 16.5 33Z" fill="#333F48" />
      </svg>
    ),
    disabled: false,
    name: 'solid-black-circle',
  },
  {
    svg: (
      <svg id="hollow-black-circle" width="33" height="33" viewBox="0 0 33 33" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M32.5 16.5001C32.5 25.3244 25.3486 32.5 16.5 32.5C7.65138 32.5 0.5 25.3244 0.5 16.5001C0.5 7.67561 7.65138 0.5 16.5 0.5C25.3486 0.5 32.5 7.67561 32.5 16.5001Z" stroke="#333F48" />
      </svg>
    ),
    disabled: false,
    name: 'hollow-black-circle',
  },
  {
    svg: (
      <svg id="solid-black-triangle" width="33" height="33" viewBox="0 0 33 33" fill="none" xmlns="http://www.w3.org/2000/svg">
        <g clipPath="url(#clip0)">
          <path d="M16.2822 0L32.7822 33H-0.217773L16.2822 0Z" fill="#333F48" />
        </g>
        <defs>
          <clipPath id="clip0">
            <rect width="33" height="33" fill="white" />
          </clipPath>
        </defs>
      </svg>
    ),
    disabled: false,
    name: 'solid-black-triangle',
  },
  {
    svg: (
      <svg id="hollow-black-diamond" width="33" height="33" viewBox="0 0 33 33" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M0.707107 16.5L16.5 0.707106L32.2929 16.5L16.5 32.2929L0.707107 16.5Z" stroke="#333F48" />
      </svg>
    ),
    disabled: false,
    name: 'hollow-black-triangle',
  },
  {
    svg: (
      <svg id="solid-black-diamond" width="33" height="33" viewBox="0 0 33 33" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M16.5 0L33 16.5L16.5 33L0 16.5L16.5 0Z" fill="#333F48" />
      </svg>
    ),
    disabled: false,
    name: 'solid-black-diamond',
  },
  {
    svg: (
      <svg id="hollow-black-diamond" width="33" height="33" viewBox="0 0 33 33" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M0.707107 16.5L16.5 0.707106L32.2929 16.5L16.5 32.2929L0.707107 16.5Z" stroke="#333F48" />
      </svg>
    ),
    disabled: false,
    name: 'hollow-black-diamond',
  },
];
