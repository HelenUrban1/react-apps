import React, { Fragment } from 'react';
import { storiesOf } from '@storybook/react';
import ItemList from './ItemList';
import ItemPanel from './ItemPanel';
import { withKnobs } from '@storybook/addon-knobs';
import { BalloonStyles } from './BalloonStyles';
import { fakeItems, fakePart, markers } from './fakeList';

const stories = storiesOf('Design System/ItemList', module).addDecorator(withKnobs);

const getStyleNames = () => {
  let styles = [];
  BalloonStyles.map((style) => {
    styles.push(style.name);
  });
  return styles;
};

const styleNames = getStyleNames();

const getRandomStyle = () => {
  const index = Math.floor(Math.random() * BalloonStyles.length);
  return BalloonStyles[index];
};

const mockItems = [
  {
    id: 0,
    number: 1,
    quantity: 1,
    capturedValue: '2.15 +/- 0.01',
    style: getRandomStyle(),
  },
  {
    id: 1,
    number: 2,
    quantity: 1,
    capturedValue: '34.05 +/- 0.05 THRU',
    style: getRandomStyle(),
  },
  {
    id: 2,
    number: 3,
    quantity: 1,
    capturedValue: '1.50 / 1.25 Really Long Item Capture Value',
    style: getRandomStyle(),
  },
  {
    id: 3,
    number: 4,
    quantity: 3,
    capturedValue: '| (+) | @ 2.5 (m) | A | B | C |',
    style: getRandomStyle(),
  },
  {
    id: 4,
    number: 5,
    quantity: 1,
    capturedValue: 'This is a Note',
    style: getRandomStyle(),
  },
  {
    id: 5,
    number: 6,
    quantity: 100,
    capturedValue: '2.15 +/- 0.01',
    style: getRandomStyle(),
  },
  {
    id: 6,
    number: 7,
    quantity: 1,
    capturedValue: '2.15 +/- 0.01',
    style: getRandomStyle(),
  },
  {
    id: 7,
    number: 8,
    quantity: 1,
    capturedValue: '34.05 +/- 0.05 THRU',
    style: getRandomStyle(),
  },
  {
    id: 8,
    number: 9,
    quantity: 1,
    capturedValue: '1.50 / 1.25',
    style: getRandomStyle(),
  },
  {
    id: 9,
    number: 20,
    quantity: 3,
    capturedValue: '| (+) | @ 2.5 (m) | A | B | C |',
    style: getRandomStyle(),
  },
  {
    id: 10,
    number: 54,
    quantity: 1,
    capturedValue: 'This is a Note',
    style: getRandomStyle(),
  },
  {
    id: 11,
    number: 66,
    quantity: 100,
    capturedValue: '2.15 +/- 0.01',
    style: getRandomStyle(),
  },
  {
    id: 12,
    number: 18,
    quantity: 1,
    capturedValue: '2.15 +/- 0.01',
    style: getRandomStyle(),
  },
  {
    id: 13,
    number: 29,
    quantity: 1,
    capturedValue: '34.05 +/- 0.05 THRU',
    style: getRandomStyle(),
  },
  {
    id: 14,
    number: 31,
    quantity: 1,
    capturedValue: '1.50 / 1.25',
    style: getRandomStyle(),
  },
  {
    id: 15,
    number: 42,
    quantity: 3,
    capturedValue: '| (+) | @ 2.5 (m) | A | B | C |',
    style: getRandomStyle(),
  },
  {
    id: 16,
    number: 999,
    quantity: 1,
    capturedValue: 'This is a Note',
    style: getRandomStyle(),
  },
  {
    id: 17,
    number: 1000,
    quantity: 100,
    capturedValue: '2.15 +/- 0.01',
    style: BalloonStyles[2],
  },
  {
    id: 14,
    number: 31,
    quantity: 1,
    capturedValue: '1.50 / 1.25',
    style: getRandomStyle(),
  },
  {
    id: 15,
    number: 42,
    quantity: 3,
    capturedValue: '| (+) | @ 2.5 (m) | A | B | C |',
    style: getRandomStyle(),
  },
  {
    id: 16,
    number: 999,
    quantity: 1,
    capturedValue: 'This is a Note',
    style: getRandomStyle(),
  },
  {
    id: 17,
    number: 1000,
    quantity: 100,
    capturedValue: '2.15 +/- 0.01',
    style: BalloonStyles[2],
  },
  {
    id: 14,
    number: 31,
    quantity: 1,
    capturedValue: '1.50 / 1.25',
    style: getRandomStyle(),
  },
  {
    id: 15,
    number: 42,
    quantity: 3,
    capturedValue: '| (+) | @ 2.5 (m) | A | B | C |',
    style: getRandomStyle(),
  },
  {
    id: 16,
    number: 999,
    quantity: 1,
    capturedValue: 'This is a Note',
    style: getRandomStyle(),
  },
  {
    id: 17,
    number: 1000,
    quantity: 100,
    capturedValue: '2.15 +/- 0.01',
    style: BalloonStyles[2],
  },
];

const mockActiveIds = [0];

stories
  .add('Item List Open', () => {
    const story = (
      <Fragment>
        <ItemList itemList={fakeItems} editItem={() => alert('Edited Item')} activeIDs={mockActiveIds} markers={markers}></ItemList>
      </Fragment>
    );

    return story;
  })
  .add('Item List Closed', () => {
    const story = (
      <Fragment>
        <ItemList collapsed={true} itemList={fakeItems} editItem={() => alert('Edited Item')} activeIDs={mockActiveIds} markers={markers}></ItemList>
      </Fragment>
    );

    return story;
  })
  .add('Empty List', () => {
    const story = (
      <Fragment>
        <ItemList></ItemList>
      </Fragment>
    );

    return story;
  })
  .add('Item Panel', () => {
    const story = (
      <Fragment>
        <ItemPanel itemList={fakeItems} editItem={() => alert('Edited Item')} activeIDs={mockActiveIds} markers={markers}></ItemPanel>
      </Fragment>
    );

    return story;
  })
  .add('Item Panel Empty', () => {
    const story = (
      <Fragment>
        <ItemPanel itemList={[]} editItem={() => alert('Edited Item')} activeIDs={[]} markers={markers}></ItemPanel>
      </Fragment>
    );

    return story;
  });
