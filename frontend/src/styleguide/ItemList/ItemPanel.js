import React from 'react';
import { InfoCircleOutlined } from '@ant-design/icons';
import { Layout } from 'antd';
import ItemList from './ItemList';
import './ItemPanelOld.less';

const { Sider } = Layout;

class ItemPanel extends React.Component {
  constructor(props) {
    super(props);
    this.onClick = this.onClick.bind(this);
    this.state = { collapsed: false, activeIDs: this.props.activeIDs };
  }

  onCollapse = () => {
    this.setState({ collapsed: !this.state.collapsed });
  };

  onClick = (id) => {
    this.setState({ ...this.state, activeIDs: [id] });
  };

  render() {
    const { itemList, markers, editItem } = this.props;
    const { activeIDs } = this.state;

    return (
      <Sider width={350} collapsible onCollapse={this.onCollapse} theme="light" style={{ height: '100vh', boxShadow: '4px 0px 4px rgb(0, 0, 0, 0.25' }}>
        <div className="feature-panel-container">
          <div className="feature-panel-header">{this.state.collapsed ? <InfoCircleOutlined style={{ fontSize: 36, margin: '0 auto' }} /> : <p>Part Info will Go Here</p>}</div>
          <div className="feature-list-container">
            <ItemList collapsed={this.state.collapsed} activeIDs={activeIDs} itemList={itemList} editItem={editItem} markers={markers} onClick={this.onClick} />
          </div>
          <div className="feature-panel-footer">{this.state.collapsed ? <p>Small Button</p> : <p>Buttons Go Here</p>}</div>
        </div>
      </Sider>
    );
  }
}

export default ItemPanel;
