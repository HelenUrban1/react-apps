import React, { ReactNode } from 'react';
import icons from 'styleguide/shared/icons';

type Props = {
  title: string | null;
  information: string | null;
  cypress?: string;
  icon?: ReactNode;
};

export const SectionHeader = ({ title, information, icon, cypress }: Props) => {
  return (
    <header>
      <h1 data-cy={cypress ? `${cypress}-title` : ''}>{title}</h1>
      <p data-cy={cypress ? `${cypress}-information` : ''}>
        {icon && icons}
        {information}
      </p>
    </header>
  );
};

SectionHeader.defaultProps = {
  cypress: 'section-header',
  icon: undefined,
};
