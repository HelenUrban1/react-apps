import { PlusCircleOutlined, DeleteOutlined, StopOutlined } from '@ant-design/icons';
import { Popconfirm, Tooltip } from 'antd';
import { deleteTipTitle } from 'domain/part/edit/characteristics/utils/uiUtil';
import { i18n } from 'i18n';
import { CharacteristicActions } from 'modules/characteristic/characteristicActions';
import { AppState } from 'modules/state';
import React, { useState, useMemo } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { AutoballoonHeader } from 'styleguide/AutoballoonHeader/AutoballoonHeader';
import { Annotation } from 'types/annotation';
import { Characteristic } from 'types/characteristics';
import './itemPanelHeaderStyles.less';

interface ItemPanelHeaderProps {
  autoballoonEnabled: boolean;
  collapsed: boolean;
  deleteActive: () => void;
}

export const ItemPanelHeader = ({ autoballoonEnabled, collapsed, deleteActive }: ItemPanelHeaderProps) => {
  const { selected, singleCustom, mode } = useSelector((state: AppState) => state.characteristics);
  const [showDelete, setShowDelete] = useState(false);

  const selectedFeatures: Characteristic[] = useMemo(() => selected.filter((s) => !!s.quantity) as Characteristic[], [selected]);
  const selectedAnnotations: Annotation[] = useMemo(() => selected.filter((s) => !!s.backgroundColor) as Annotation[], [selected]);

  const title = useMemo(() => {
    return deleteTipTitle(selectedFeatures, selectedAnnotations);
  }, [selectedFeatures, selectedAnnotations]);

  const dispatch = useDispatch();

  const handleConfirmDelete = () => {
    setShowDelete(false);
    if (selected.length > 0) deleteActive();
  };

  const handleCancelDelete = () => {
    setShowDelete(false);
  };

  const handleClickDelete = () => {
    if (selected.length > 0) setShowDelete(true);
  };

  const handleClickAddCustom = () => {
    dispatch({ type: CharacteristicActions.SINGLE_CUSTOM, payload: { singleCustom: true, mode: 'Custom' } });
  };

  const handleCancelCustom = () => {
    dispatch({ type: CharacteristicActions.SINGLE_CUSTOM, payload: { singleCustom: false, mode: 'Manual' } });
  };

  return (
    <div className={`item-panel-header ${collapsed ? 'collapsed' : ''}`}>
      {autoballoonEnabled && <AutoballoonHeader collapsed={collapsed} />}
      {!collapsed && (
        <div className="item-panel-header-tools">
          <Tooltip title={singleCustom ? i18n('toolbar.cancelCustom') : i18n('toolbar.addCustom')} placement="bottom">
            <button type="button" className="icon-button part-extract large" onClick={singleCustom ? handleCancelCustom : handleClickAddCustom} disabled={!singleCustom && mode === 'Custom'}>
              {singleCustom ? <StopOutlined /> : <PlusCircleOutlined />}
            </button>
          </Tooltip>
          <Popconfirm title={title} placement="bottom" visible={showDelete} onConfirm={handleConfirmDelete} onCancel={handleCancelDelete}>
            <Tooltip title={title} placement="bottom">
              <button type="button" className="icon-button part-delete large" onClick={handleClickDelete} disabled={selectedFeatures.length + selectedAnnotations.length === 0}>
                <DeleteOutlined />
              </button>
            </Tooltip>
          </Popconfirm>
        </div>
      )}
    </div>
  );
};
