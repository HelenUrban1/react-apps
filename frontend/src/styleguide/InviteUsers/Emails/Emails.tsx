import React from 'react';
import { Input } from 'antd';
import { i18n } from 'i18n';
import '../Users.less';

interface Props {
  error?: string;
  change: (emails: string[]) => void;
}

const Emails: React.FC<Props> = ({ error, change }) => {
  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const addresses = event.target.value.split(',').map((item) => {
      return item.trim();
    });
    change(addresses);
  };
  return (
    <div className="invite-section">
      <label htmlFor="modal-email-input" data-cy="modal-email-title">
        {i18n('user.invite.email')}
        <Input id="modal-email-input" data-cy="modal-email-input" className={error ? 'error' : ''} onChange={handleChange} />
      </label>
      {!error && (
        <p data-cy="modal-email-description" className="small">
          {i18n('user.invite.addressDesc')}
        </p>
      )}
      {error && (
        <p data-cy="modal-email-error" className="small error">
          {error}
        </p>
      )}
    </div>
  );
};

Emails.defaultProps = {
  error: undefined,
  change: (emails: string[]) => null,
};
export default Emails;
