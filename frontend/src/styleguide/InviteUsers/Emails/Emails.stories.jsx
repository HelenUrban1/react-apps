import React from 'react';
import { storiesOf } from '@storybook/react';
import { useState } from '@storybook/addons';
import { string, array } from 'yup';
import { i18n } from 'i18n';
import Emails from './Emails';
import MDX from './Emails.mdx';

const stories = storiesOf('Subscription/Emails', module);

stories.add(
  'Default',
  () => {
    const [emails, setEmails] = useState([]);
    const [error, setError] = useState(false);

    const schema = array().of(string().email());

    const change = async (addresses) => {
      const valid = await schema.isValid(addresses);
      if (!valid !== error) {
        setError(i18n('user.invite.addressError'));
      }
      setEmails(addresses);
    };

    return (
      <div>
        <Emails error={error} change={change} />
        <div>
          <p>Emails input:</p>
          <ul>
            {emails.map((mail, index) => (
              <li key={`mail-${index}`}>{mail}</li>
            ))}
          </ul>
        </div>
      </div>
    );
  },
  {
    docs: { page: MDX },
  },
);
