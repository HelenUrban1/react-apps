import React from 'react';
import { storiesOf } from '@storybook/react';
import { useState } from '@storybook/addons';
import { withKnobs, boolean } from '@storybook/addon-knobs';
import Controlled from './Controlled';
import MDX from './Controlled.mdx';

const stories = storiesOf('Subscription/Controlled', module);
stories.addDecorator(withKnobs);

stories.add(
  'Default',
  () => {
    const [control, setControl] = useState(false);

    const change = (e) => {
      setControl(e.target.value);
    };

    return <Controlled control={control} plural={boolean('Multiple Users', false)} change={change} />;
  },
  {
    docs: { page: MDX },
  },
);
