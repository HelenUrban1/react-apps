import React from 'react';
import { i18n } from 'i18n';
import { QuestionCircleOutlined } from '@ant-design/icons';
import { Radio, Tooltip } from 'antd';
import '../Users.less';
import { RadioChangeEvent } from 'antd/lib/radio';

interface Props {
  control: boolean;
  plural: boolean;
  change: (e: RadioChangeEvent) => void;
}

const Controlled: React.FC<Props> = ({ control, plural, change }) => {
  return (
    <div className="invite-section">
      <h5 data-cy="modal-controlled-title">
        {!plural && i18n('user.invite.controlled')}
        {plural && i18n('user.invite.controlledPlural')}
        {/* <button type="button" className="link help-icon" data-cy="modal-tier-help"> */}
        <Tooltip title={i18n('user.invite.controlledHelp')} className="link help-icon" data-cy="modal-tier-help">
          <QuestionCircleOutlined className="primaryIcon" />
        </Tooltip>
        {/* </button> */}
      </h5>
      <Radio.Group data-cy="modal-controlled-input" className="paymentOptions" onChange={change} value={control}>
        <Radio value={false} data-cy="modal-controlled-true">
          {i18n('common.yes')}
        </Radio>
        <Radio value data-cy="modal-controlled-false">
          {i18n('common.no')}
        </Radio>
      </Radio.Group>
    </div>
  );
};

export default Controlled;
