export interface OptionList {
  label: string;
  key: string;
  options: {
    label: string;
    value: string;
    name: string;
  }[];
}
