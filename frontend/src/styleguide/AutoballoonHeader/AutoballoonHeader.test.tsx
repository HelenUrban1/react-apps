import React from 'react';
import { render, fireEvent, screen } from '@testing-library/react';
import { AutoballoonHeader } from './AutoballoonHeader';
import { Provider } from 'react-redux';
// @ts-ignore
import configureMockStore from 'redux-mock-store';

const mockStore = configureMockStore([]);

describe('AutoballoonHeader Component', () => {
  let store: any;
  beforeEach(() => {
    store = mockStore({
      characteristics: {
        mode: 'Auto',
      },
      part: {
        part: {
          autoMarkupStatus: 'In_Progress',
        },
      },
      auth: {
        jwt: 'mock_jwt_token',
      },
      session: {
        settings: {
          autoballoonEnabled: {
            value: 'true',
          },
        },
      },
    });
  });

  it('renders correctly when in progress and not collapsed', () => {
    const { getByTestId } = render(
      <Provider store={store}>
        <AutoballoonHeader collapsed={false} />
      </Provider>,
    );
    expect(getByTestId('automarkup-in-progress')).toBeInTheDocument();
  });

  it('renders correctly when in progress and collapsed', () => {
    const { getByTestId } = render(
      <Provider store={store}>
        <AutoballoonHeader collapsed={true} />
      </Provider>,
    );
    expect(getByTestId('automarkup-in-progress')).toBeInTheDocument();
  });

  it('renders correctly when completed and not collapsed', () => {
    store = mockStore({
      ...store.getState(),
      part: {
        part: {
          autoMarkupStatus: 'Completed',
        },
      },
    });
    const { getByTestId } = render(
      <Provider store={store}>
        <AutoballoonHeader collapsed={false} />
      </Provider>,
    );
    expect(getByTestId('automarkup-complete')).toBeInTheDocument();
  });

  it('renders correctly when completed and collapsed', () => {
    store = mockStore({
      ...store.getState(),
      part: {
        part: {
          autoMarkupStatus: 'Completed',
        },
      },
    });
    const { getByTestId } = render(
      <Provider store={store}>
        <AutoballoonHeader collapsed={true} />
      </Provider>,
    );
    expect(getByTestId('automarkup-complete')).toBeInTheDocument();
  });

  it('switches to manual mode when switchToManualButton is clicked', () => {
    const { getByText } = render(
      <Provider store={store}>
        <AutoballoonHeader collapsed={false} />
      </Provider>,
    );
    fireEvent.click(getByText('Switch to Manual'));
    expect(store.getActions()).toEqual([
      {
        type: 'CHARACTERISTICS_UPDATE_EXTRACT_MODE',
        payload: { mode: 'Manual' },
      },
    ]);
  });

  it('switches to auto mode when switchToAutoButton is clicked', () => {
    store = mockStore({
      ...store.getState(),
      part: {
        part: {
          autoMarkupStatus: 'Completed',
        },
      },
      characteristics: {
        mode: 'Manual',
      },
    });

    const { getByTestId } = render(
      <Provider store={store}>
        <AutoballoonHeader collapsed={false} />
      </Provider>,
    );
    fireEvent.click(getByTestId('switch-to-auto-btn'));
    expect(store.getActions()).toEqual([
      {
        type: 'CHARACTERISTICS_UPDATE_EXTRACT_MODE',
        payload: { mode: 'Auto' },
      },
    ]);
  });
});

