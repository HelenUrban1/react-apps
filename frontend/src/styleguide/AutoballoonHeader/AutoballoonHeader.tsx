import React, { useEffect, useState } from 'react';

import { useDispatch, useSelector } from 'react-redux';
import './AutoballoonHeader.less';
import { SafetyCertificateOutlined, Loading3QuartersOutlined, CheckCircleOutlined } from '@ant-design/icons';
import { Tooltip, Progress, Button } from 'antd';
import { AppState } from 'modules/state';
import { i18n } from 'i18n';
import { CharacteristicActions } from 'modules/characteristic/characteristicActions';
import config from 'config';
import { io } from 'socket.io-client';
import { doGetPart } from 'modules/part/partActions';
import { AutoballoonValues, getAutoBalloonReviewProgress } from 'utils/Autoballoon/Autoballoon';
import debounce from 'lodash/debounce';
import IxcTheme from 'styleguide/styles/IxcTheme';

interface Props {
  collapsed: boolean;
}

export const AutoballoonHeader: React.FC<Props> = ({ collapsed }) => {
  const dispatch = useDispatch();
  const { mode } = useSelector((state: AppState) => state.characteristics);
  const selectMode = (newMode: string) => {
    dispatch({ type: CharacteristicActions.UPDATE_EXTRACT_MODE, payload: { mode: newMode } });
  };
  const { part } = useSelector((state: AppState) => state.part);
  const { jwt } = useSelector((state: AppState) => state.auth);
  const { settings } = useSelector((state: AppState) => state.session);

  const [autoballoonValues, setAutoballoonValues] = useState<AutoballoonValues | null>();
  const [isAutomarkupDone, setIsAutomarkupDone] = useState(false);

  useEffect(() => {
    if (part?.autoMarkupStatus) {
      switch (part.autoMarkupStatus) {
        case 'In_Progress':
          setIsAutomarkupDone(false);
          break;
        case 'Completed':
          setIsAutomarkupDone(true);
          break;
        default:
          setIsAutomarkupDone(false);
          break;
      }
    }
  }, [part]);

  const partUpdateHandler = (a: any) => {
    dispatch(doGetPart(a.id));
  };

  const debouncedPartUpdateHandler = debounce(partUpdateHandler, 2000, { trailing: true, leading: true, maxWait: 5000 });

  useEffect(() => {
    if (settings.autoballoonEnabled?.value === 'true' && config.wsUrl) {
      // Update parts list when parts or drawingSheets update so we can see autoballoon progress
      const socket = io(config.wsUrl, { transports: ['websocket'], withCredentials: true, auth: { token: `Bearer ${jwt}` }, path: config.wsPath });
      // May be able to optimize this by using the part info to update redux
      // or only running findParts if the updated part is on the current page, but may still want to handle part.create with a call to findParts
      socket.on('part.autoMarkupCompleted', debouncedPartUpdateHandler);
      return () => {
        socket.off('part.autoMarkupCompleted', debouncedPartUpdateHandler);
        socket.disconnect();
      };
    }
    return () => {};
  }, []);

  const handleSwitchToManual = () => {
    selectMode('Manual');
  };
  const handleSwitchToAuto = () => {
    selectMode('Auto');
  };

  const switchToManualButton = (
    <Button data-testid="switch-to-manual-btn" onClick={handleSwitchToManual}>
      {i18n('entities.part.autoballoonHeader.switchToManual')}
    </Button>
  );
  const switchToAutoButton = (
    <Button data-testid="switch-to-auto-btn" onClick={handleSwitchToAuto}>
      {i18n('entities.part.autoballoonHeader.switchToAuto')}
    </Button>
  );

  const reviewToolTipMarkup = (
    <Tooltip placement="bottomLeft" title={i18n('entities.part.tooltips.reviewingFeatures')}>
      {autoballoonValues && <Progress data-testid="review-progress" type="circle" percent={getAutoBalloonReviewProgress(autoballoonValues)} width={36} strokeLinecap="square" strokeWidth={10} format={() => <SafetyCertificateOutlined />} />}
    </Tooltip>
  );

  const headerMarkup = {
    inProgress: {
      full: (
        <div className="autoballoon-header">
          <Tooltip placement="bottomLeft" title={i18n('entities.part.tooltips.findingMarkups')}>
            <Loading3QuartersOutlined spin style={{ color: IxcTheme.colors.blue }} data-testid="automarkup-in-progress" />
          </Tooltip>
          {reviewToolTipMarkup}
          {mode === 'Auto' ? switchToManualButton : null}
        </div>
      ),
      collapsed: (
        <div className="autoballoon-header collapsed">
          <Tooltip placement="bottomLeft" title={i18n('entities.part.tooltips.findingMarkups')}>
            <Loading3QuartersOutlined spin style={{ color: IxcTheme.colors.blue }} data-testid="automarkup-in-progress" />
          </Tooltip>
          {reviewToolTipMarkup}
        </div>
      ),
    },
    complete: {
      full: (
        <div className="autoballoon-header">
          <Tooltip placement="bottomLeft" title={i18n('entities.part.tooltips.autoMarkupComplete')}>
            <CheckCircleOutlined style={{ color: '#1B838B' }} data-testid="automarkup-complete" />
          </Tooltip>
          {isAutomarkupDone && mode === 'Auto' ? switchToManualButton : switchToAutoButton}
        </div>
      ),
      collapsed: (
        <div className="autoballoon-header collapsed">
          <Tooltip placement="bottomLeft" title={i18n('entities.part.tooltips.autoMarkupComplete')}>
            <CheckCircleOutlined style={{ color: '#1B838B' }} data-testid="automarkup-complete" />
          </Tooltip>
        </div>
      ),
    },
  };

  if (part?.autoMarkupStatus === 'Completed') {
    return collapsed ? headerMarkup.complete.collapsed : headerMarkup.complete.full;
  }

  if (part?.autoMarkupStatus === 'In_Progress') {
    return collapsed ? headerMarkup.inProgress.collapsed : headerMarkup.inProgress.full;
  }

  return null;
};

AutoballoonHeader.defaultProps = {
  /* Open by Default */
  collapsed: false,
};
