import React from 'react';
import { Characteristic } from 'types/characteristics';
import Icon, { DeploymentUnitOutlined } from '@ant-design/icons';
import { i18n } from 'i18n';
import { BalloonSVG } from 'styleguide/shared/svg';

interface Props {
  item: Characteristic;
  expand: (item: Characteristic, shared: boolean) => void;
}
const ExpandSingle: React.FC<Props> = ({ item, expand }) => {
  return (
    <div data-cy="expand-list-content" className="expand-content">
      <button //
        data-cy="feature-expand-shared"
        type="button"
        onClick={() => expand(item, true)}
        disabled={item?.quantity <= 1 || !item || (item?.markerSubIndex !== null && item.markerSubIndex >= 0)}
        className={`${(item?.quantity <= 1 || !item || (item?.markerSubIndex !== null && item.markerSubIndex >= 0)) && 'disabled'}`}
      >
        <Icon className="expand-action-icon" component={BalloonSVG} />
        <span className="expand-action-label">{i18n('entities.feature.expand.shared')}</span>
      </button>
      <button //
        data-cy="feature-expand-sub"
        type="button"
        onClick={() => expand(item, false)}
        disabled={item?.quantity <= 1 || !item || (item?.markerSubIndex !== null && item.markerSubIndex >= 0)}
        className={`${(item?.quantity <= 1 || !item || (item?.markerSubIndex !== null && item.markerSubIndex >= 0)) && 'disabled'}`}
      >
        <DeploymentUnitOutlined className="expand-action-icon" />
        <span className="expand-action-label">{i18n('entities.feature.expand.sub')}</span>
      </button>
    </div>
  );
};
export default ExpandSingle;
