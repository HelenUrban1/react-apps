import React from 'react';
import { Characteristic } from 'types/characteristics';
import Icon from '@ant-design/icons';
import { i18n } from 'i18n';
import { GroupSVG } from 'styleguide/shared/svg';

interface Props {
  item: Characteristic;
  expand: (item: Characteristic, shared: boolean) => void;
}
const ExpandGroup: React.FC<Props> = ({ item, expand }) => {
  return (
    <div data-cy="expand-list-content" className="expand-content expand-grouped">
      <button //
        data-cy="feature-expand-group"
        type="button"
        disabled={item?.quantity <= 1 || !item}
        className={`${(item?.quantity <= 1 || !item) && 'disabled'}`}
        onClick={() => expand(item, item.markerGroupShared)}
      >
        <Icon className="expand-action-icon" component={GroupSVG} />
        <span className="expand-action-label">{i18n('entities.feature.expand.groupDescription')}</span>
      </button>
    </div>
  );
};
export default ExpandGroup;
