import React, { useCallback, useState, useEffect } from 'react';
import { Slider, Row, Col, Button, Tooltip } from 'antd';
import { i18n } from 'i18n';
import { fabric } from 'fabric';
import { getBalloon, getBalloonGroup, getText } from 'domain/part/edit/characteristics/characteristicsRenderer';
import log from 'modules/shared/logger';
import { BrandColors } from 'view/global/defaults';
import { Marker } from 'types/marker';
import { LoadingOutlined } from '@ant-design/icons';

interface Props {
  active: boolean;
  markerSize: number;
  updateMarker: (e: any, applyToAllPages?: boolean) => void;
  singleSheet: boolean;
  zoom: number;
  loading?: boolean | undefined;
  marker?: Marker;
}
const ResizeBalloon = ({ active, markerSize, updateMarker, singleSheet, zoom, loading, marker }: Props) => {
  const [size, setSize] = useState(markerSize);
  const [applyingToAll, setApplyingToAll] = useState(false);
  const [canvas, setCanvas] = useState<fabric.Canvas>();

  const handleUpdateCanvas = () => {
    if (canvas) {
      canvas.clear();
      const prevSize = zoom * size;
      const offset = 54 + prevSize / 2;
      const balloon = getBalloon(marker?.shape || 'Circle', offset, offset, marker?.fillColor || BrandColors.Coral, marker?.borderColor || BrandColors.Coral, marker?.borderWidth || 2, prevSize, marker?.borderStyle || 'Solid');
      const text = getText('7', Math.round(prevSize / 2), offset, offset, marker?.fontColor || '#FFFFFF', prevSize, 'center');
      const balloonGroup = getBalloonGroup(balloon, text, true);
      balloonGroup.selectable = false;
      canvas.add(balloonGroup);
      canvas.renderAll();
    }
  };

  useEffect(() => {
    if (!loading && applyingToAll) setApplyingToAll(false);
  }, [loading]);

  useEffect(() => {
    setSize(markerSize);
  }, [markerSize]);

  useEffect(() => {
    handleUpdateCanvas();
  }, [canvas, size, zoom]);

  const ref = useCallback(
    (node) => {
      if (node !== null) {
        try {
          const fCanvas = new fabric.Canvas(node);
          setCanvas(fCanvas);
        } catch (err) {
          // prevents error boundary from triggering on getComputedStyles of Null error
          log.error('BallonStyleModal.useCallback', err);
        }
      }
    },
    [marker],
  );

  const handleChangeSlider = (val: number) => {
    setSize(val);
  };

  const handleChangeBalloonSize = (val: number) => {
    updateMarker(val);
  };

  const handleApplyToAllPages = () => {
    setApplyingToAll(true);
    updateMarker(size, true);
  };

  return (
    <Row>
      <Col className="resize-balloon-col" span={24}>
        <section className="resize-balloon-slider-section">
          <Slider
            tipFormatter={(value) => {
              return (
                <>
                  <span>{value}</span>
                  {loading && !applyingToAll ? <LoadingOutlined style={{ marginLeft: '4px' }} /> : ''}
                </>
              );
            }}
            data-cy="resize-balloon-slider"
            step={2}
            min={10}
            max={100}
            value={size}
            onChange={handleChangeSlider}
            onAfterChange={handleChangeBalloonSize}
            tooltipVisible={active}
            tooltipPlacement="top"
          />
          <Tooltip title={singleSheet ? i18n('wizard.singleSheet') : ''} placement="bottom">
            <Button loading={loading && applyingToAll} className="btn-secondary" onClick={handleApplyToAllPages}>
              {i18n('wizard.applyAll')}
            </Button>
          </Tooltip>
        </section>
        <div className="resize-preview">
          <canvas ref={ref} id="resize-canvas" data-cy="balloon-preview-canvas" />
        </div>
      </Col>
    </Row>
  );
};
export default ResizeBalloon;
