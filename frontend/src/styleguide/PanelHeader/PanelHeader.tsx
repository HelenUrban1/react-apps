import React from 'react';
import { InfoCircleOutlined, QuestionCircleOutlined } from '@ant-design/icons';
import { i18n } from 'i18n';
import './PanelHeader.less';
import { Tooltip } from 'antd';

interface Props {
  header: string;
  copy: string;
  info?: string;
  dataCy?: string;
  optional?: boolean;
}

const PanelHeader = ({ header, copy, dataCy, optional, info }: Props) => {
  return (
    <section className="panel-header">
      <header data-cy={`${dataCy || ''}title`}>
        {header}
        {optional && <small>{` (${i18n('common.optional')})`}</small>}
        {info && (
          <Tooltip title={info} placement="rightTop">
            <QuestionCircleOutlined />
          </Tooltip>
        )}
      </header>
      <div className="info-block">
        <InfoCircleOutlined />
        <p data-cy={`${dataCy || ''}information`}>{copy}</p>
      </div>
    </section>
  );
};

PanelHeader.defaultProps = {
  dataCy: undefined,
  optional: false,
};

export default PanelHeader;
