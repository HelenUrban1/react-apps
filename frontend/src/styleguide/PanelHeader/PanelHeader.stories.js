import React from 'react';
import { storiesOf } from '@storybook/react';
import PanelHeader from './PanelHeader';
import MDX from './PanelHeader.mdx';
import IxcTheme from '../styles/IxcTheme';

const stories = storiesOf('Design System/Panel', module);

stories.add('Header', () => {
  const story = <PanelHeader header="Example Header" copy="Here is some example text for this section." />;

  return story;
});
