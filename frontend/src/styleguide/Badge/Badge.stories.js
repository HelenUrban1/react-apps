import React from 'react';
import { storiesOf } from '@storybook/react';
import { withKnobs } from '@storybook/addon-knobs';
import Badge from './Badge';

const stories = storiesOf('Design System/Badge', module).addDecorator(withKnobs);

stories.add('Badges', () => {
  const story = (
    <div>
      <div style={{ position: 'relative', height: '50px', width: '200px', padding: '24px' }}>
        <Badge type="notification" count={7} />
      </div>
      <div style={{ position: 'relative', height: '50px', width: '200px', padding: '24px' }}>
        <Badge type="quantity" count={54} />
      </div>
      <div style={{ position: 'relative', height: '50px', width: '200px', padding: '24px' }}>
        <Badge count={999} color="red" />
      </div>
      <div style={{ position: 'relative', height: '50px', width: '200px', padding: '24px' }}>
        <Badge count={7} mini />
      </div>
    </div>
  );

  return story;
});
