import React from 'react';
import PropTypes from 'prop-types';
import { Tooltip } from 'antd';
import './Badge.less';

class Badge extends React.Component {
  render() {
    const { type, mini, count, onClick, color, tipDirection, tip } = this.props;

    // const getWidth = (mini, count) => {
    //   if (mini) {
    //     return '12px';
    //   }
    //   return `${24 + 4 * (count.toString().length - 1)}px`;
    // };

    const getSizeClass = () => {
      if (count < 10) return 'small';
      if (count < 100) return 'medium';
      return 'large';
    };

    const getBadge = () => {
      return (
        <div
          onClick={onClick}
          className={`${mini ? 'mini ' : ''}${type}-badge ${getSizeClass()}`}
          style={{
            // width: `${getWidth(mini, count)}`,
            height: `${mini ? '12px' : '18px'}`,
            borderRadius: `${mini ? '10px' : '18px'}`,
            backgroundColor: color,
            textAlign: 'center',
            lineHeight: `${mini ? '10px' : '18px'}`,
            color: '#F6F8FA',
            margin: `${mini ? '19px 0px 19px 0px' : '15px 0px 15px 0px'}`,
          }}
        >
          {!mini ? `${type === 'quantity' ? 'x' : ''}${count}` : null}
        </div>
      );
    };

    return type === 'count' ? (
      getBadge()
    ) : (
      <Tooltip placement={tipDirection} title={`${tip}${mini ? ` (${count})` : ''}`}>
        {getBadge()}
      </Tooltip>
    );
  }
}

Badge.propTypes = {
  /* The Type of Badge (quantity, notifications, etc.) */
  type: PropTypes.string,

  /* Whether the Badge should hide the Number */
  mini: PropTypes.bool,

  /* Number for the Badge */
  count: PropTypes.number,

  /* Action to do on click */
  onClick: PropTypes.func,

  /* Color of the Badge */
  color: PropTypes.string,

  /* Direction of the Tooltip relative to the Badge */
  tipDirection: PropTypes.string,

  /* Tooltip Text */
  tip: PropTypes.string,
};

Badge.defaultProps = {
  /* Dummy Item */
  type: 'count',

  /* Show Number by Default */
  mini: false,

  /* Default Number 1 */
  count: 1,

  /* Default click alerts with Clicked Badge */
  onClick: () => null,

  /* Default Color is IX Purple */
  color: '#963CBD',

  /* Default Tooltip placement is to the right */
  tipDirection: 'right',

  /* Default is Default */
  tip: 'Default',
};

export default Badge;
