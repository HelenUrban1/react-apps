/* eslint-disable */
require('dotenv').config();
const sonarqubeScanner = require('sonarqube-scanner');

const projectVersion = `pv${process.env.SEMVER_TAG}`;
// let buildString = [process.env.BUILD_NUMBER, process.env.SCM_TAG, process.env.SCM_BRANCH, process.env.SCM_HASH].filter(Boolean).join('-');
const buildString = `BUILD_NUMBER=${process.env.BUILD_NUMBER}&SCM_TAG=${process.env.SCM_TAG}&SCM_BRANCH=${process.env.SCM_BRANCH}&SCM_HASH=${process.env.SCM_HASH}`;

sonarqubeScanner(
  {
    serverUrl: 'https://sonarqube.ideagenplc.com',
    token: process.env.SONAR_USER_TOKEN,
    options: {
      // https://docs.sonarqube.org/latest/analysis/analysis-parameters/
      // 'sonar.log.level': 'TRACE',
      // 'sonar.verbose': 'true',
      'sonar.projectVersion': projectVersion,
      'sonar.buildString': buildString,
    },
  },
  () => {},
);
