module.exports = {
  printWidth: 250,
  singleQuote: true,
  tabWidth: 2,
  jsxBracketSameLine: false,
  semi: true,
  useTabs: false,
  bracketSpacing: true,
  arrowParens: 'always',
  endOfLine: 'crlf',
  requirePragma: false,
};
