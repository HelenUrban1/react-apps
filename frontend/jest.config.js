module.exports = {
  roots: ['<rootDir>/src'],
  modulePaths: ['<rootDir>/src'],
  moduleDirectories: ['node_modules'],
  moduleFileExtensions: ['ts', 'tsx', 'js', 'jsx', 'json', 'node'],
  moduleNameMapper: {
    '\\.(css|less|scss|sass)$': 'identity-obj-proxy',
  },
  preset: 'ts-jest',
  transform: {
    //'^.+\\.(js|jsx|ts|tsx)$': '<rootDir>/node_modules/babel-jest',
    '^.+\\.(js|jsx|ts|tsx)$': 'ts-jest',
  },
  transformIgnorePatterns: [
    '[/\\\\]node_modules[/\\\\].+\\.(js|jsx|ts|tsx)$', //
    '^.+\\.module\\.(css|sass|scss|less)$',
  ],
  globals: {
    'ts-jest': {
      diagnostics: false,
    },
  },
  testURL: 'http://ixc-local.com',
  setupFilesAfterEnv: ['@testing-library/jest-dom'],
  setupFiles: [
    'jest-canvas-mock', //
    'jest-localstorage-mock',
  ],
  // testMatch: ['**/?(*.)+(spec|test).[jt]s?(x)'],
  testMatch: ['<rootDir>/src/**/*.(spec|test).[jt]s?(x)'],
  testPathIgnorePatterns: [
    '/node_modules/', //
    '/public/',
    '/build/',
    '/coverage/',
  ],
  collectCoverage: false,
  coverageDirectory: 'coverage',
  collectCoverageFrom: [
    'src/**/*.[jt]s?(x)', //
    '!src/**/*.d.ts',
    '!src/index.tsx',
    '!src/serviceWorker.ts',
  ],
  coveragePathIgnorePatterns: [
    './src/*/*.types.{ts,tsx}', //
    './src/index.tsx',
    './src/serviceWorker.ts',
  ],
  coverageReporters: ['text', 'json', 'lcov', 'clover'],
  testResultsProcessor: 'jest-sonar-reporter',
  coverageThreshold: {
    global: {
      statements: 0,
      branches: 0,
      functions: 0,
      lines: 0,
    },
  },
};
