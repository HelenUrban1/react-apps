module.exports = [
  {
    type: 'input',
    name: 'path',
    message: 'What domain does this fall under? (singular)',
  },
];
