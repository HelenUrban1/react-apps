---
to: <%=h.src()%>/domain/<%=path%>/<%=h.inflection.camelize(name, true)%>/index.tsx
---
import React from 'react';

const <%=h.inflection.titleize(name)%>: React.FC = () => {
	return <main id="<%=h.inflection.camelize(name, true)%>"></main>;
};

export default <%=h.inflection.titleize(name)%>;

