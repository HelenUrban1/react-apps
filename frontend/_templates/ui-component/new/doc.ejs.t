---
to: <%=h.src()%>/styleguide/<%=h.inflection.camelize(name)%>/<%=h.inflection.camelize(name)%>.mdx
---
import <%=h.inflection.camelize(name)%> from './<%=h.inflection.camelize(name)%>';
import { Meta, Props } from '@storybook/addon-docs/blocks';

<Meta title="Design System|<%=h.inflection.camelize(name)%>" component={<%=h.inflection.camelize(name)%>} />

# <%=h.inflection.camelize(name)%>

<Props of={<%=h.inflection.camelize(name)%>} />

## Usage