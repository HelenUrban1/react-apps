---
to: <%=h.src()%>/styleguide/<%=h.inflection.camelize(name)%>/<%=h.inflection.camelize(name)%>.tsx
---
import React from 'react';

const <%=h.inflection.camelize(name)%>: React.FC = () => {
	return <div></div>;
};

export default <%=h.inflection.camelize(name)%>;
