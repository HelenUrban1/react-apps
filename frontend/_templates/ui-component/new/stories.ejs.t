---
to: <%=h.src()%>/styleguide/<%=h.inflection.camelize(name)%>/<%=h.inflection.camelize(name)%>.stories.jsx
---
import React from 'react';
import { storiesOf } from '@storybook/react';
import <%=h.inflection.camelize(name)%> from './<%=h.inflection.camelize(name)%>';
import MDX from './<%=h.inflection.camelize(name)%>.mdx';

const stories = storiesOf('Design System/<%=h.inflection.camelize(name)%>', module);

stories.add(
	'Default',
	() => {
		const story = <<%=h.inflection.camelize(name)%> />;

		return story;
	},
	{
		docs: { page: MDX },
	},
);
