---
to: <%=h.src()%>/styleguide/<%=h.inflection.camelize(name)%>/<%=h.inflection.camelize(name)%>.test.js
---
import React from 'react';
import <%=h.inflection.camelize(name)%> from './<%=h.inflection.camelize(name)%>';

describe('<%=h.inflection.camelize(name)%>', () => {
    it('Passes', () => {
        expect(true).toBeTruthy();
    })
})