---
inject: true
to: "<%=h.src()%>domain/<%= h.changeCase.lower(domain) %>/<%= feature ? `${h.changeCase.lower(feature)}/` : null %>index.tsx"
before: "interface"
skip_if: <%= name %>
---
import <%= h.inflection.camelize(name) %> from './<%= h.inflection.camelize(name) %>';