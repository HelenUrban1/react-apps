module.exports = [
  {
    type: 'input',
    name: 'domain',
    message: 'What domain does this fall under? (singular)',
  },
  {
    type: 'input',
    name: 'feature',
    message: 'What feature does this fall under? (default none)',
  },
];
