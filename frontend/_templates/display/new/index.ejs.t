---
to: "<%=h.src()%>domain/<%= h.changeCase.lower(domain) %>/<%= feature ? `${h.changeCase.lower(feature)}/` : null %><%= h.changeCase.camel(name) %>.tsx"
---
import React from 'react';

interface Props {
}

const <%=h.inflection.camelize(name)%> = ({}:Props) => {
	return <section id="<%=h.changeCase.paramCase(name)%>" data-cy="<%=h.changeCase.paramCase(name)%>"></section>;
};

export default <%=h.inflection.camelize(name)%>;

