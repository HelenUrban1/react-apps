---
to: <%=h.src()%>/modules/<%=name.toLowerCase()%>/<%=name.toLowerCase()%>Selectors.ts
---
import { createSelector } from 'reselect';
import { <%=h.inflection.camelize(name)%>State } from '../state.d';

export const selectRaw = (state: { <%=name.toLowerCase()%>: <%=h.inflection.camelize(name)%>State }) => state.<%=name.toLowerCase()%>;

