---
inject: true
to: "<%=h.src()%>modules/reducers.js"
before: "import { combineReducers } from 'redux';"
skip_if: <%=name.toLowerCase()%>Reducers
---
import <%=name.toLowerCase()%> from 'modules/<%=name.toLowerCase()%>/<%=name.toLowerCase()%>Reducers';