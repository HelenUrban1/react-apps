---
inject: true
to: "<%=h.src()%>modules/state.d.ts"
before: "/*"
skip_if: "<%=h.inflection.camelize(name)%>ActionTypes"
---
import { <%=h.inflection.camelize(name)%>ActionTypes } from './<%=name.toLowerCase()%>/<%=name.toLowerCase()%>Actions';