---
to: <%=h.src()%>/modules/<%=name.toLowerCase()%>/<%=name.toLowerCase()%>Actions.ts
---
const prefix = '<%=name.toUpperCase()%>';

export const actions = {
  CLEAR: `${prefix}_CLEAR`,
  LOADING: `${prefix}_LOADING`,
  SUCCESS: `${prefix}_SUCCESS`,
  ERROR: `${prefix}_ERROR`,
};

export type <%=h.inflection.camelize(name)%>ActionTypes = 
  '<%=name.toUpperCase()%>_CLEAR' | 
  `<%=name.toUpperCase()%>_LOADING` |
  `<%=name.toUpperCase()%>_SUCCESS`|
  '<%=name.toUpperCase()%>_ERROR';

export default actions;

