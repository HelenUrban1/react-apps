---
inject: true
to: "<%=h.src()%>modules/state.d.ts"
after: "*/"
skip_if: "<%=name.toLowerCase()%>State"
---
// <%=h.inflection.camelize(name)%>

export interface <%=h.inflection.camelize(name)%>Action {
  type: <%=h.inflection.camelize(name)%>ActionTypes;
  payload: any; // TODO replace this with the payload type
}

export interface <%=h.inflection.camelize(name)%>State {
  loading: boolean;
  error: string | null;
  // TODO replace this with the type model for your state
}