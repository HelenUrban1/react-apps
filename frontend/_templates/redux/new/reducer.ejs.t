---
to: <%=h.src()%>/modules/<%=name.toLowerCase()%>/<%=name.toLowerCase()%>Reducers.ts
---
import './<%=name.toLowerCase()%>Actions';
import { <%=h.inflection.camelize(name)%>State, <%=h.inflection.camelize(name)%>Action } from '../state.d';

const initialData: <%=h.inflection.camelize(name)%>State = {
  loading: false;
  error: null;
};

export default (state = initialData, { type, payload }: <%=h.inflection.camelize(name)%>Action): <%=h.inflection.camelize(name)%>State => {
  switch (type) {
    case '<%=name.toUpperCase()%>_CLEAR':
      return {
        ...initialData,
      };
    case '<%=name.toUpperCase()%>_LOADING':
      return {
        ...state,
        loading: true,
        error: null,
      };
    case '<%=name.toUpperCase()%>_SUCCESS':
      return {
        ...state,
        loading: false,
        <%=name.toLowerCase()%>: payload
      };
    case '<%=name.toUpperCase()%>_ERROR':
      return {
        ...state,
        error: payload,
        loading: false,
        <%=name.toLowerCase()%>: null,
      };
    default:
      return state;
  }
};
