# Hygen Templates

[Hygen](https://www.hygen.io/) is a javascript code generator that lets you set your own templates. We have a couple of templates designed around repeated code patterns, such as UI components, new features, and React display components.

## UI Components

Our UI is handled by [Storybook]() and makes use of the [AntDesign]() system. This template scaffolds up the base, test, styles, story, and documentation for a storybook component.

To create a new component, type `npx hygen ui-component new <name>` in a terminal while in the frontend, where `<name>` is the name of your component. This name will be formatted into camel case.

| File    | Purpose                                                                                                                                                                                                         |
| ------- | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| doc     | Metadata about the component to explain the why, when, and how for other developers. [Read more](https://www.learnstorybook.com/design-systems-for-developers/react/en/document/)                               |
| index   | The base file with the component to be imported to the rest of the project.                                                                                                                                     |
| less    | LESS styling for the component. This file is automatically imported into your index file.                                                                                                                       |
| stories | The Storybook story for the component. This controls the display and knobs available to developers when viewing your component in Storybook. [Read more](https://storybook.js.org/docs/basics/writing-stories/) |
| test    | Jest tests for your component. Automatically imports the component and gives a basic canary test.                                                                                                               |

## Graphql

We use [Apollo](https://www.apollographql.com/docs/react/) on the front end for our Graphql queries. To make calling these queries easier to understand and update, the Graphql tags for a repository are collected into an object alongside the typescript types for the queries and mutations. General CRUD operations are prepopulated by hygen for you.

To create a Graphql Tag file, type `npx hygen graphql new <name>` where `<name>` is the name of your repository. This name will be formatted into camel case.

## Feature

A feature is the top level file for a section of functionality within a given domain. Editing a report, for instance, would be a feature. This template creates a folder with index and test files under the domain given.

To create a new feature, type `npx hygen feature new <name>` where `<name>` is the name of your feature. You will be prompted for a domain `path`, the path from `src/domain` to your domain folder. Generally this is just the name of the domain itself such as `part` or `report`.

| File   | Purpose                                                                                                                                         |
| ------ | ----------------------------------------------------------------------------------------------------------------------------------------------- |
| index  | The base file for your feature. This is where you will call any necessary display components and handle any top level logic.                    |
| prompt | This is a hygen file which allows the template generator to set additional variables. In this case, it sets the `path` with the supplied domain |
| test   | This creates a cypress test file within the domain folder of `cypress/integration/app` and an empty describe block for testing user interaction |

## Display

A display is the subsection of a feature component. If your feature is a `list`, for example, this might be the `table`, `search`, or `detail drawer` within that feature. The files created are very similar to the feature template with a few exceptions:

- a different path for the files to be created in
- a `section` html wrapper instead of the feature's `main` wrapper
- import to the index file is injected in the index file of the provided path

To create a new feature, type `npx hygen display new <name>` where `<name>` is the name of your feature. You will be prompted for a `domain` and `feature` to create a path for the display files to save under. If you want your display component to be in the root of your domain, simply leave the feature blank.

| File   | Purpose                                                                                                                                           |
| ------ | ------------------------------------------------------------------------------------------------------------------------------------------------- |
| index  | The base file for your display. This is where you will call any necessary UI components and handle any local logic.                               |
| prompt | This is a hygen file which allows the template generator to set additional variables. In this case, it sets the `domain` and `feature`            |
| test   | This creates a cypress test file within the `domain` folder of `cypress/integration/app` and an empty describe block for testing user interaction |

## Redux

A redux generator creates a redux store and associated files for actions, reducers, and selectors.

To create a new redux store, type `npx hygen redux new <name>` where `<name>` is the name of your store.

| File      | Purpose                                                                      |
| --------- | ---------------------------------------------------------------------------- |
| actions   | This file creates all the action strings for the redux store                 |
| reducer   | This file creates the redux reducer to assign action payloads to the store   |
| selectors | This file creates a base selector for reading your data from the redux store |
