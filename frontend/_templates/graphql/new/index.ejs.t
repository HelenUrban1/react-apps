---
to: <%=h.src()%>/graphql/<%=name.toLowerCase()%>.ts
---
import gql from 'graphql-tag';

export interface <%=h.inflection.camelize(name)%> {
    id: string
}

export interface <%=h.inflection.camelize(name)%>Input {
    id: string
}

export interface <%=h.inflection.camelize(name)%>Edit {
  <%=name.toLowerCase()%>Update: <%=h.inflection.camelize(name)%>Input;
}

export interface <%=h.inflection.camelize(name)%>Create {
  <%=name.toLowerCase()%>Create: <%=h.inflection.camelize(name)%>;
}

export interface <%=h.inflection.camelize(name)%>Find {
  <%=name.toLowerCase()%>Find: <%=h.inflection.camelize(name)%>;
}

export interface <%=h.inflection.camelize(name)%>List {
  <%=name.toLowerCase()%>List: {
    count: number;
    rows: <%=h.inflection.camelize(name)%>[];
  };
}

const <%=h.inflection.camelize(name)%> = {
  mutate: {
    edit: gql`
        mutation <%=name.toUpperCase()%>_UPDATE($id: String!, $data: <%=h.inflection.camelize(name)%>Input!) {
            <%=name.toLowerCase()%>Update(id: $id, data: $data) {
                id
            }
        }
    `,
    delete: gql`
        mutation <%=name.toUpperCase()%>_DESTROY($id: String!, $data: <%=h.inflection.camelize(name)%>Input!) {
            <%=name.toLowerCase()%>Destroy(id: $id, data: $data) {
                id
            }
        }
    `,
    create: gql`
        mutation <%=name.toUpperCase()%>_CREATE($id: String!, $data: <%=h.inflection.camelize(name)%>Input!) {
            <%=name.toLowerCase()%>Create(id: $id, data: $data) {
                id
            }
        }
    `
  },
  query: {
    find: gql`
        query <%=name.toUpperCase()%>_FIND($id: String!) {
            <%=name.toLowerCase()%>Find(id: $id) {
                id
            }
        }
    `,
    list: gql``
  }
};

export default <%=h.inflection.camelize(name)%>;