import boto3
import botocore
import uuid
import typing
from boto3.dynamodb.conditions import Key


class DynamoDBClient:
    def __init__(self, table):
        self.table = table

    def put_git_commit_id(self, commit_id: str, git_branch: str, env: str, timestamp: str):
        """Write git commit ID hash to DynamoDB"""

        try:
            self.table.put_item(
                Item={'CommitId': commit_id, 'CreatedAt': timestamp,
                      'GitBranch': git_branch, 'Environment': env})
        except botocore.exceptions.ClientError as error:
            raise error

    def get_git_commit_id(self, env: str) -> str:
        """Get latest commit ID hash from DynamoDB"""

        try:
            # Queries in reverse order to get the "latest" entry.
            query = self.table.query(
                IndexName='Environment-CreatedAt-Index',
                KeyConditionExpression=Key('Environment').eq(env),
                Limit=1,
                ScanIndexForward=False,
            )
        except botocore.exceptions.ClientError as error:
            raise error

        return query['Items'][0]['CommitId']

    def put_changed_paths(self, paths: dict, build_number: int, commit_id: str) -> str:
        """Write directories (paths) to DynamoDB"""
        id = str(uuid.uuid4())

        try:
            self.table.put_item(Item={'Id': id, 'Paths': paths,
                                      'BuildNumber': build_number, 'CommitId': commit_id})
        except botocore.exceptions.ClientError as error:
            raise error

        return id

    def update_changed_path(self, id: str, path: str, path_val):
        """Update directory (path) for an existing DynamoDB document

        path_val is one of: None or of bools: True, False
        """

        try:
            self.table.update_item(
                Key={
                    'Id': id
                },
                UpdateExpression="SET Paths.#path = :path_val",
                ExpressionAttributeNames={
                    "#path": path
                },
                ExpressionAttributeValues={
                    ":path_val": path_val
                }
            )
        except botocore.exceptions.ClientError as error:
            raise error

    def get_changed_paths(self, id: str) -> dict:
        """Read directories (paths) from DynamoDB"""
        try:
            response = self.table.get_item(Key={'Id': id})
            item = response['Item']['Paths']
        except botocore.exceptions.ClientError as error:
            raise error

        return item

    def all_changed_paths_true(self, id: str, excluded_paths: list = []) -> bool:
        """Check if all directories (paths) from DynamoDB are done (set to
        True)"""

        try:
            paths = self.get_changed_paths(id)
            if len(paths) < 1:
                raise ValueError(f'expecting one or more paths: {paths}')

            for path in paths:
                if path in excluded_paths:
                    continue
                elif not paths[path]:
                    return False
        except botocore.exceptions.ClientError as error:
            raise error

        return True
