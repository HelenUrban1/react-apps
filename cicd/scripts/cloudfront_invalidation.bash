#!/usr/bin/env bash

set -eu -o pipefail

##
# Run this to invalidate (clear) CloudFront Cache for the static (frontend)
# website.
#
# This script should be ran from the root of the repo to avoid relative path
# issues.
##

if [[ -z "${ENVIRONMENT_NAME:-}" ]]; then
  echo "ERROR: environment variable 'ENVIRONMENT_NAME' must be defined. exiting..."
  exit 1
fi
echo "Environment name: ${ENVIRONMENT_NAME}"

# shellcheck disable=SC2034
cloudfront_distro_dev="E3QQMV2N5YSWHU"
# shellcheck disable=SC2034
cloudfront_distro_qa="EN7AOISJXDS49"
# shellcheck disable=SC2034
cloudfront_distro_stage="E3AJP7X12H5UZY"
# shellcheck disable=SC2034
cloudfront_distro_prod="E1NEU8DEV11QRP"

cloudfront_distro="cloudfront_distro_${ENVIRONMENT_NAME}"
cloudfront_distro_id="${!cloudfront_distro}"
echo "CloudFront distro ID: ${cloudfront_distro_id}"

aws cloudfront create-invalidation \
    --distribution-id "${cloudfront_distro_id}" \
    --paths "/static/*" "/*.html" "/*.png" "/*.js" "/*.css"
