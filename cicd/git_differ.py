#!/usr/bin/env python3

import argparse
import os
import sys
from git import Repo

"""
This will get the list of modified top-level directories between different 
git commits and print to stdout.

For additional context, see:
https://docs.google.com/document/d/1Nf_MxuS4P1irIXdi1ccI95TLsm9H-ySjln3_As37G60/edit?ts=5f174932#heading=h.1277a5ckxt61

TODO:
- logger (stdout/stderr)
- unit tests
"""

DIR_PREFIXES_TO_CONSIDER = [
    'backend',
    'frontend',
    'packages',
    'services/backup-trigger',
    'services/cloudFrontViewerResponse',
    'services/queue-processor',
    'services/webhookConsumer',
    'services/webhookListener',
]

INIT_SUFFIX = 'None'


def diff_paths(repo: Repo, init: bool, from_rev: str, to_rev: str) -> set:
    """Use git to get a diff of paths that were changed from one rev to another.

    Effectively does the following:

    git diff --name-only $FROM_REV $(git merge-base $FROM_REV $TO_REV)

    Returns just the unique directories from diffed/changed paths.
    """
    unique_dirs = set()
    ancestor = repo.merge_base(from_rev, to_rev)[0]
    paths = repo.git.diff(f'{from_rev}..{ancestor}',
                          name_only=True).split('\n')

    for fpath in paths:
        for dir_prefix in DIR_PREFIXES_TO_CONSIDER:
            if not dir_prefix in fpath:
                continue

            if init:
                dir_prefix += f',{INIT_SUFFIX}'

            unique_dirs.add(dir_prefix)

    return unique_dirs


def main():
    parser = argparse.ArgumentParser(
        description='Find git diffed paths')
    parser.add_argument(
        "--init",
        action='store_true',
        default=False,
        help="enable this for initial dynamodb write"
    )
    parser.add_argument(
        "--to-rev",
        default="origin/master",
        help="git to rev, should be the last successfully deployed commit ID"
    )
    parser.add_argument(
        "--from-rev",
        default="HEAD",
        help="git from rev"
    )

    args = parser.parse_args()
    repo = Repo('./')
    paths = diff_paths(repo, args.init, args.from_rev, args.to_rev)
    for dpath in paths:
        print(dpath)


if __name__ == '__main__':
    sys.exit(main())
