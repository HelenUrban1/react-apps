#!/usr/bin/env python3

import argparse
import boto3
import sys
from dynamodb_client import DynamoDBClient

"""
Read git modified paths from DynamoDB.

For additional context, see:
https://docs.google.com/document/d/1Nf_MxuS4P1irIXdi1ccI95TLsm9H-ySjln3_As37G60/edit?ts=5f174932#heading=h.1277a5ckxt61

TODO:
- logger (stdout/stderr)
- unit tests
"""

AWS_REGION = 'us-east-1'
# TODO: remove 'packages' once we have pipelines that report successful deploys
# for them.
EXCLUDED_PATHS_FROM_ALL_TRUE_CHECK = [
    'packages',
    'services/backup-trigger',
    'services/cloudFrontViewerResponse',
    'services/queue-processor',
]


def main():
    parser = argparse.ArgumentParser(
        description='Read git modified paths from DynamoDB')
    parser.add_argument(
        "-d",
        "--dynamodb-doc-id",
        required=True,
        help="dynamodb doc uuid"
    )
    parser.add_argument(
        "-t",
        "--dynamodb-table",
        required=True,
        help="dynamodb table name"
    )
    args = parser.parse_args()

    dynamodb = boto3.resource('dynamodb', region_name=AWS_REGION)
    table = dynamodb.Table(args.dynamodb_table)
    ddb_client = DynamoDBClient(table)

    try:
        all_changed_paths = ddb_client.all_changed_paths_true(
            args.dynamodb_doc_id, EXCLUDED_PATHS_FROM_ALL_TRUE_CHECK)

        # Returning simple string to be parsed by bash.
        if all_changed_paths:
            print("1")
        else:
            print("0")
    except Exception as ex:
        print(f'error reading from dynamodb: {ex}', file=sys.stderr)
        sys.exit(1)


if __name__ == "__main__":
    sys.exit(main())
