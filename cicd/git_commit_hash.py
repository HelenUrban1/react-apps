#!/usr/bin/env python3

import argparse
import boto3
import sys
from dynamodb_client import DynamoDBClient
from datetime import datetime, timezone


"""
Use this to publish the git commit hash ID after a successful deploy so we can
later diff against it in successive builds/deploys.

Usage:
$ ./cicd/git_commit_hash.py --dynamodb-table cicd-last-successful-git-rev-ixc-mono save --git-commit-id d382b27f65e297f0c1b5459a9c4959765348c0dd --git-branch branch develop --environment dev
$ ./cicd/git_commit_hash.py --dynamodb-table cicd-last-successful-git-rev-ixc-mono load --environment dev
d382b27f65e297f0c1b5459a9c4959765348c0dd

NON-HAPPY PATH SPITBALLING:
- we have one successful deploy (out of 3), and that publishes the git commit
  hash from the build to dynamodb.
- the other deploys fail (two out of 3).
- the next build hits codebuild gateway and it does a diff comparing changes
  against the git commit hash from dynamodb.
- this is OK because if the deploy failed, it's assumed that some changes
  would have had to have been made in a new commit which would show a diff
  against the git commit hash in dynamodb.
"""

AWS_REGION = 'us-east-1'


def _get_current_time() -> str:
    """Get an ISO 8601 time string which DynamoDB can use"""
    return datetime.now(timezone.utc).isoformat('T', 'seconds').replace('+00:00', 'Z')


def save(args, ddb_client):
    try:
        ddb_client.put_git_commit_id(
            args.git_commit_id, args.git_branch, args.environment, _get_current_time())
    except Exception as ex:
        print(f'error saving to dynamodb: {ex}', file=sys.stderr)
        sys.exit(1)


def load(args, ddb_client):
    try:
        commit_id = ddb_client.get_git_commit_id(args.environment)
        print(commit_id)
    except Exception as ex:
        print(f'error loading from dynamodb: {ex}', file=sys.stderr)
        sys.exit(1)


def main():
    parser = argparse.ArgumentParser(
        description='Read and record git modified paths from/to DynamoDB')
    subparsers = parser.add_subparsers(title='actions')

    save_parser = subparsers.add_parser('save')
    save_parser.add_argument(
        "-g",
        "--git-commit-id",
        required=True,
        help="git commit id/hash"
    )
    save_parser.add_argument(
        "-b",
        "--git-branch",
        required=True,
        help="git branch name"
    )
    save_parser.set_defaults(func=save)
    load_parser = subparsers.add_parser('load')
    load_parser.set_defaults(func=load)
    parser.add_argument(
        "-t",
        "--dynamodb-table",
        required=True,
        help="dynamodb table name"
    )
    parser.add_argument(
        "-e",
        "--environment",
        required=True,
        help="environment name, ex. dev, stage, qa, prod"
    )
    args = parser.parse_args()

    dynamodb = boto3.resource('dynamodb', region_name=AWS_REGION)
    table = dynamodb.Table(args.dynamodb_table)
    ddb_client = DynamoDBClient(table)
    args.func(args, ddb_client)


if __name__ == "__main__":
    sys.exit(main())
