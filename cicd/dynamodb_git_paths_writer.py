#!/usr/bin/env python3

import argparse
import boto3
import re
import sys
from dynamodb_client import DynamoDBClient

"""
Takes a list of paths and writes them to DynamoDB so we can later see if all
pipelines that were spawned from those paths were finalized.

Usage:
# Initial run with paths from git diff
$ ./cicd/git_differ.py --init | ./cicd/dynamodb_git_paths_writer.py
--dynamodb-table cicd-pipeline-coordination-ixc-mono --git-commit-id
d382b27f65e297f0c1b5459a9c4959765348c0da --build-number 1

# Update just a particular path to indicate that it was successful
echo "backend,True" | ./cicd/dynamodb_git_paths_writer.py --dynamodb-table
cicd-pipeline-coordination-ixc-mono --git-commit-id
d382b27f65e297f0c1b5459a9c4959765348c0da --build-number 1

# Update just a particular path to indicate that it has failed
echo "frontend,False" | ./cicd/dynamodb_git_paths_writer.py --dynamodb-table
cicd-pipeline-coordination-ixc-mono --git-commit-id
d382b27f65e297f0c1b5459a9c4959765348c0da --build-number 1

For additional context, see:
https://docs.google.com/document/d/1Nf_MxuS4P1irIXdi1ccI95TLsm9H-ySjln3_As37G60/edit?ts=5f174932#heading=h.1277a5ckxt61

TODO:
- logger (stdout/stderr)
- unit tests
"""

AWS_REGION = 'us-east-1'


def write_output_file(data: str, output_file_path: str):
    """Write file with dynamodb table ID so we can look it up later in the
    pipeline.
    """
    try:
        with open(output_file_path, 'w') as of:
            of.write(data)
    except Exception:
        print(f'error writing file: {output_file_path}', file=sys.stderr)
        sys.exit(1)


def create(args, paths, ddb_client):
    try:
        id = ddb_client.put_changed_paths(
            paths, args.build_number, args.git_commit_id)
        print(id)
        write_output_file(id, args.output_file)
    except Exception as ex:
        print(f'error writing to dynamodb: {ex}', file=sys.stderr)
        sys.exit(1)


def update(args, paths, ddb_client):
    if len(paths) != 1:
        print(
            f'error updating dynamodb. only pass one path on updates: {paths}', file=sys.stderr)
        sys.exit(1)

    path = list(paths)[0]
    path_val = paths[path]

    try:
        ddb_client.update_changed_path(args.dynamodb_id, path, path_val)
    except Exception as ex:
        print(
            f'error updating doc id ({id}) in dynamodb: {ex}', file=sys.stderr)
        sys.exit(1)


def main():
    parser = argparse.ArgumentParser(
        description='Record git modified paths to DynamoDB')
    parser.add_argument(
        "-t",
        "--dynamodb-table",
        required=True,
        help="dynamodb table name"
    )

    subparsers = parser.add_subparsers(title='actions')
    create_parser = subparsers.add_parser('create')
    create_parser.set_defaults(func=create)
    update_parser = subparsers.add_parser('update')
    update_parser.set_defaults(func=update)

    create_parser.add_argument(
        "-b",
        "--build-number",
        required=True,
        help="build number from CI"
    )
    create_parser.add_argument(
        "-g",
        "--git-commit-id",
        required=True,
        help="git commit id/hash"
    )
    create_parser.add_argument(
        "-o",
        "--output-file",
        default="dynamodb-id.txt",
        help="out file which contains ID for DynamoDB doc"
    )
    update_parser.add_argument(
        "--dynamodb-id",
        required=True,
        help="dynamodb document id"
    )
    args = parser.parse_args()

    # Expect the git diffed paths to be piped in via stdin
    paths = dict()
    for line in sys.stdin:
        stripped_line = line.rstrip()
        if not re.search('^(.+),(None|False|True)$', stripped_line):
            print(
                f'error - input line does not match expected format: {stripped_line}', file=sys.stderr)
            sys.exit(1)

        status = None
        directory, raw_status = stripped_line.split(",")

        # We get the status from a string and convert it to a Python boolean.
        if raw_status == 'True':
            status = True
        elif raw_status == 'False':
            status = False

        paths[directory] = status

    dynamodb = boto3.resource('dynamodb', region_name=AWS_REGION)
    table = dynamodb.Table(args.dynamodb_table)
    ddb_client = DynamoDBClient(table)

    args.func(args, paths, ddb_client)


if __name__ == "__main__":
    sys.exit(main())
