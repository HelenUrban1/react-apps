# cicd

This dir is for shared CICD tools which will be used across all individual services.

A service still may (and should) have its own cicd/ dir.

See the [initial design doc](https://docs.google.com/document/d/1Nf_MxuS4P1irIXdi1ccI95TLsm9H-ySjln3_As37G60/edit?ts=5f174932) for additional context.

## Scripts

Each of these scripts will be called from within the CI/CD pipeline stages and
will most likely not need to be called locally.

### dynamodb_git_paths_reader.py

This script will return a list of "paths" from DynamoDB with the status of the
path.

### dynamodb_git_paths_writer.py

This script will write the "paths" (from a git diff) into DynamoDB with the
status of the path.

### git_commit_hash.py

This script will either load or save the git commit hash from or to DynamoDB.
This git commit hash is saved after a successful deploy of all services that
were modified. New builds compare the current changes to this saved git commit
hash (in a separate script) to determine which services were modified. These services are represented
as "paths" in DynamoDB (handled in separate scripts).

### git_differ.py

This script will compare the current HEAD in git to another git commit hash to
find all directories that were modified in the current HEAD vs. the other git
commit hash.
