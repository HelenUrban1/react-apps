// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//

import { faker } from '@faker-js/faker';
import 'cypress-file-upload';
import authentication from '../integration/locators/authentication.json';
import balloonStyles from '../integration/locators/balloon-styles.json';
import common from '../integration/locators/common.json';
import fileUpload from '../integration/locators/fileUpload.json';
import documentInformation from '../integration/locators/part-docs-info-section.json';
import partGrid from '../integration/locators/part-grid.json';
import partInformation from '../integration/locators/part-information-section.json';
import { default as partList, default as partlist } from '../integration/locators/part-list.json';
import partTolerance from '../integration/locators/part-tolerance.json';
import account from '../integration/locators/settings-account.json';
import billing from '../integration/locators/billing.json';
import { default as sideNav, default as SideNavMenu } from '../integration/locators/side-nav-menu-section.json';
import inviteUserModal from '../integration/locators/Invite-users-modal.json';

const stagePath = 'https://www.ixc-stage.com/auth/signin';
const devPath = 'https://www.ixc-dev.com/auth/signin';

require('@4tw/cypress-drag-drop');

require('@4tw/cypress-drag-drop');

Cypress.Commands.add('resetSession', () => {
  cy.clearCookie('jwt');
  cy.clearCookie('session');
  cy.clearCookie('refresh_token');
});

Cypress.Commands.add('persistSession', () => {
  Cypress.Cookies.defaults({
    preserve: 'refresh_token',
  });
});
Cypress.Commands.add('generateEmail', () => {
  // fill-out form

  function makeid(length) {
    var result = '';
    var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
  }

  console.log(makeid(15));
  cy.get(account.Seats.Email).type(makeid(15) + '@aharo.com', { force: true });
});
Cypress.Commands.add('login', (email, password) => {
  cy.resetSession();
  cy.visit('/');
  cy.get(common.fields.email).type(email);
  cy.get(common.fields.password).type(password, { log: false });
  cy.get(authentication.loginPage.loginBtn).click();
});

Cypress.Commands.add('loginAdmin', () => {
  cy.resetSession();
  cy.visit('/');
  cy.wait(1500);
  cy.url().then(($url) => {
    switch ($url) {
      case stagePath:
        cy.get(common.fields.email).type(Cypress.env('stageAdminUsername'), { log: false });
        cy.get(common.fields.password).type(Cypress.env('stageAdminPassword'), { log: false });
        break;
      case devPath:
        cy.get(common.fields.email).type(Cypress.env('devAdminUsername'), { log: false });
        cy.get(common.fields.password).type(Cypress.env('devAdminPassword'), { log: false });
        break;
      default:
        cy.get(common.fields.email).type(Cypress.env('adminUsername'), { log: false });
        cy.get(common.fields.password).type(Cypress.env('adminPassword'), { log: false });
        break;
    }
    cy.get(authentication.loginPage.loginBtn).click();
    cy.get('h1').should('be.visible').and('contain', 'Parts');
  });
});

// login planner switch between stage and dev
Cypress.Commands.add('loginPlanner', () => {
  cy.resetSession();
  cy.visit('/');
  cy.wait(1500);
  cy.url().then(($url) => {
    switch ($url) {
      case stagePath:
        cy.get(common.fields.email).type(Cypress.env('stagePlannerUsername'), { log: false });
        cy.get(common.fields.password).type(Cypress.env('stagePlannerPassword'), { log: false });
        break;
      case devPath:
        cy.get(common.fields.email).type(Cypress.env('devPlannerUsername'), { log: false });
        cy.get(common.fields.password).type(Cypress.env('devPlannerPassword'), { log: false });
        break;
      default:
        cy.get(common.fields.email).type(Cypress.env('plannerUsername'), { log: false });
        cy.get(common.fields.password).type(Cypress.env('plannerPassword'), { log: false });
        break;
    }
    cy.get(authentication.loginPage.loginBtn).click();
    cy.get('h1').should('be.visible').and('contain', 'Parts');
  });
});

Cypress.Commands.add('loginPlannerWithBilling', () => {
  cy.resetSession();
  cy.visit('/');
  cy.wait(1500);
  cy.url().then(($url) => {
    switch ($url) {
      case stagePath:
        cy.get(common.fields.email).type(Cypress.env('stagePlannerwithbillingUsername'), { log: false });
        cy.get(common.fields.password).type(Cypress.env('stagePlannerwithbillingPassword'), { log: false });
        break;
      case devPath:
        cy.get(common.fields.email).type(Cypress.env('devPlannerwithbillingUsername'), { log: false });
        cy.get(common.fields.password).type(Cypress.env('devPlannerwithbillingPassword'), { log: false });
        break;
      default:
        cy.get(common.fields.email).type(Cypress.env('plannerwithbillingUsername'), { log: false });
        cy.get(common.fields.password).type(Cypress.env('plannerwithbillingPassword'), { log: false });
    }
    cy.get(authentication.loginPage.loginBtn).click();
    cy.get('h1').should('be.visible').and('contain', 'Parts');
  });
});

// login reviewer switch between stage and dev
Cypress.Commands.add('loginReviewer', () => {
  cy.resetSession();
  cy.visit('/');
  cy.wait(1500);
  cy.url().then(($url) => {
    switch ($url) {
      case stagePath:
        cy.get(common.fields.email).type(Cypress.env('stageReviewerUsername'), { log: false });
        cy.get(common.fields.password).type(Cypress.env('stageReviewerPassword'), { log: false });
        break;
      case devPath:
        cy.get(common.fields.email).type(Cypress.env('devReviewerUsername'), { log: false });
        cy.get(common.fields.password).type(Cypress.env('devReviewerPassword'), { log: false });
        break;
      default:
        cy.get(common.fields.email).type(Cypress.env('reviewerUsername'), { log: false });
        cy.get(common.fields.password).type(Cypress.env('reviewerPassword'), { log: false });
    }
    cy.get(authentication.loginPage.loginBtn).click();
    cy.get('h1').should('be.visible').and('contain', 'Parts');
  });
});

// login billing switch between stage and dev
Cypress.Commands.add('loginBilling', () => {
  cy.resetSession();
  cy.visit('/');
  cy.wait(1500);
  cy.url().then(($url) => {
    switch ($url) {
      case stagePath:
        cy.get(common.fields.email).type(Cypress.env('stageBillingUsername'), { log: false });
        cy.get(common.fields.password).type(Cypress.env('stageBillingPassword'), { log: false });
        break;
      case devPath:
        cy.get(common.fields.email).type(Cypress.env('devBillingUsername'), { log: false });
        cy.get(common.fields.password).type(Cypress.env('devBillingPassword'), { log: false });
        break;
      default:
        cy.get(common.fields.email).type(Cypress.env('billingUsername'), { log: false });
        cy.get(common.fields.password).type(Cypress.env('billingPassword'), { log: false });
    }
    cy.get(authentication.loginPage.loginBtn).click();
    cy.get('h1').should('be.visible').and('contain', 'Account Settings');
  });
});

// login inactive switch between stage and dev
Cypress.Commands.add('loginInactive', () => {
  cy.resetSession();
  cy.visit('/');
  cy.wait(1500);
  cy.url().then(($url) => {
    switch ($url) {
      case stagePath:
        cy.get(common.fields.email).type(Cypress.env('stageInactiveUsername'), { log: false });
        cy.get(common.fields.password).type(Cypress.env('stageInactivePassword'), { log: false });
        break;
      case devPath:
        cy.get(common.fields.email).type(Cypress.env('devInactiveUsername'), { log: false });
        cy.get(common.fields.password).type(Cypress.env('devInactivePassword'), { log: false });
        break;
      default:
        cy.get(common.fields.email).type(Cypress.env('inactiveUsername'), { log: false });
        cy.get(common.fields.password).type(Cypress.env('inactivePassword'), { log: false });
    }
    cy.get(authentication.loginPage.loginBtn).click();
    cy.get('h1').should('be.visible').and('contain', '403');
  });
});

// Returns the PDFTron iFrame
Cypress.Commands.add('PDF', () => {
  cy.log(cy.get('#pdf-viewer'));
  return (
    cy
      .get('#pdf-viewer')
      .get('#webviewer-1')
      // Cypress yields jQuery element, which has the real
      // DOM element under property "0".
      // From the real DOM iframe element we can get
      // the "document" element, it is stored in "contentDocument" property
      // Cypress "its" command can access deep properties using dot notation
      // https://on.cypress.io/its
      .its('0.contentDocument.body', { timeout: 30000 })
      .should('be.visible')
      .then(cy.wrap)
  );
});
Cypress.Commands.add('readConfig', () => {
  cy.get('#config')
    .should('have.attr', 'data-json')
    .then((data) => {
      const config = JSON.parse(data);
      cy.wrap(config).as('config');
    });
});

Cypress.Commands.add('logout', () => {
  cy.get(sideNav.logout).click({ force: true }, { timeout: 10000 });
  cy.contains('Logout').click({ force: true }, { timeout: 10000 });
  cy.url().should('include', '/auth/signin');
});

Cypress.Commands.add('forgottenpassword', () => {
  cy.resetSession();
  cy.visit('/');
  cy.get(authentication.loginPage.forgotPasswordLink).click();
  cy.url({ timeout: 5000 }).should('include', '/auth/forgot-password');
});

Cypress.Commands.add('forceVisit', (url) => {
  cy.window().then((win) => {
    return win.open(url, '_self');
  });
});

Cypress.Commands.add('addPartSeeder', () => {
  const fileName = 'test.pdf';
  cy.get(partList.newPartBtn).should('exist').click();
  cy.get(fileUpload.fileUploadArea).should('be.visible');
  cy.get(fileUpload.fileUploadArea).attachFile({ filePath: `/${fileName}`, encoding: 'base64' }, { subjectType: 'drag-n-drop' });
  cy.get(fileUpload.fileUploadArea).should('be.visible');
  cy.get(fileUpload.fileUploadSaveBtn).click({ force: true });
});

Cypress.Commands.add('addPart', () => {
  const fileName = 'test.pdf';
  cy.get(partList.newPartBtn).should('exist').click({ timeout: 10000 });
  cy.get(fileUpload.fileUploadSaveBtn).should('be.visible');
  cy.get(fileUpload.fileUploadArea).attachFile({ filePath: `/${fileName}`, encoding: 'base64' }, { subjectType: 'drag-n-drop' });
  cy.get(fileUpload.fileUploadSaveBtn).should('be.visible').click();
  cy.url().then(($url) => {
    if ($url == devPath) {
      cy.get('.custom-modal-footer > :nth-child(1)').click();

      cy.wait(5000);
    }
  });
  cy.get(partInformation.partTitle).should('exist');
  cy.wait(10000);
});

Cypress.Commands.add('addPartNumber', () => {
  let partNumber = faker.random.numeric(4);
  cy.wrap(partNumber).as('partNumber');
  cy.get('@partNumber').then((partNumber) => {
    cy.get(partInformation.partNumber).type(partNumber);
    cy.get(partInformation.partNumber).should('have.text', partNumber);
  });
});

Cypress.Commands.add('deletePart', () => {
  cy.wait(10000);
  cy.get('@partNumber').then((partNumber) => {
    cy.get('.testing-part-number-header').each(($ele, index) => {
      if ($ele.text().includes(partNumber)) {
        cy.wrap(index).as('index');
      }
    });
    cy.get('@index').then((index) => {
      cy.get('[data-icon=delete]')
        .eq(index - 1)
        .trigger('click');
      cy.get(partList.deletePartContent).should('be.visible');
      cy.get(partList.deletePartConfirm).trigger('click');
      cy.wait(6000);
      cy.get('.testing-part-number-header')
        .eq(index - 1)
        .should('not.contain', partNumber);
    });
  });
});

Cypress.Commands.add('navigateToPartList', () => {
  cy.get(sideNav.partsIcon).click();
  cy.get(sideNav.partsList).should('be.visible').click();
  cy.get('h1').should('be.visible').and('contain', 'Parts');
});

Cypress.Commands.add('navigateToDocInfo', () => {
  cy.get(common.wizard.next).click();
  cy.get(documentInformation.docTitle).should('exist');
});

Cypress.Commands.add('navigateToTolerance', () => {
  cy.get(common.wizard.next).click();
  cy.get(documentInformation.docTitle).should('exist');
  cy.get(common.wizard.next).click();
  cy.get(partTolerance.toleranceTitle).should('exist');
  cy.wait(15000);
});

Cypress.Commands.add('navigateToBalloonStyles', () => {
  cy.get(common.wizard.next).click();
  cy.get(documentInformation.docTitle).should('exist');
  cy.get(common.wizard.next).click();
  cy.get(partTolerance.toleranceTitle).should('exist');
  cy.get(common.wizard.next).click();
  cy.get(partGrid.gridPageTitle).should('exist');
  cy.get(common.wizard.next).click();
  cy.get(balloonStyles.title).should('exist');
});

Cypress.Commands.add('navigateToGridZones', () => {
  cy.get(common.wizard.next).click();
  cy.get(documentInformation.docTitle).should('exist');
  cy.get(common.wizard.next).click();
  cy.get(partTolerance.toleranceTitle).should('exist');
  cy.get(common.wizard.next).click();
  cy.get(partGrid.gridPageTitle).should('exist');
});

Cypress.Commands.add('navigateToSeats', () => {
  cy.get(sideNav.settings).trigger('mouseover');
  cy.get(sideNav.seats).click();
  cy.url().should('include', '/settings/seats');
  cy.wait(8000);
});

Cypress.Commands.add('cancelInvitation', (invite) => {
  //FindBy span = cancel and click on the last one//
  cy.get(invite + ' > .access-field')
    .contains('Cancel')
    .then(($btn) => {
      cy.wrap($btn)
        .eq($btn.length - 1)
        .click({ force: true });
    });
  //FindBy span = ok and click on the last one//
  cy.get('span')
    .contains('OK')
    .then(($btn) => {
      cy.wrap($btn)
        .eq($btn.length - 1)
        .click({ force: true });
    });
  //Assert toast message//
  describe('Assert toast message', () => {
    cy.get(inviteUserModal.inviteNotification).contains('Invitation Cancelled').should('be.visible');
  });
});

Cypress.Commands.add('inviteFreeUser', () => {
  const email = faker.random.alpha(8) + '@gmail.com';
  cy.wrap(email).as('email');
  cy.get('@email').then((email) => {
    cy.get(account.Seats.Email).type(email);
  });
  cy.get(inviteUserModal.roles.modalRoleInput).click();
  cy.get(inviteUserModal.roles.Reviewer).click();
  cy.get(inviteUserModal.confirmInvite).click();
  cy.get(account.Seats.SendInvites).click({ force: true });
});

Cypress.Commands.add('invitePaidUser', () => {
  const email = faker.random.alpha(8) + '@gmail.com';
  cy.wrap(email).as('email');

  cy.get('@email').then((email) => {
    cy.get(account.Seats.Email).type(email);
  });
  cy.get(inviteUserModal.roles.modalRoleInput).click();
  cy.get(inviteUserModal.roles.Planner).click();
  cy.get(inviteUserModal.confirmInvite).click();
  cy.get(account.Seats.SendInvites).click({ force: true });
});

Cypress.Commands.add('CompanyProfileEditAndResetFields', () => {
  cy.get(SideNavMenu.settings).trigger('mouseover');
  cy.get(account.CompanyProfile.companyProfile).click({ force: true });
  cy.get(account.CompanyProfile.Organization).should('be.enabled').clear();
  cy.get(account.CompanyProfile.PrimaryPhone).should('be.enabled').clear();
  cy.get(account.CompanyProfile.Street).should('be.enabled').clear();
  cy.get(account.CompanyProfile.Street2).should('be.enabled').clear();
  cy.get(account.CompanyProfile.City).should('be.enabled').clear();
  cy.get(account.CompanyProfile.Country).should('be.enabled');
  cy.get(account.CompanyProfile.PostalCode).should('be.enabled');
  cy.get(account.CompanyProfile.SaveBtn).click({ force: true });
  cy.get(account.CompanyProfile.ResetBtn).should('be.visible');
  //edit fields//
  cy.get(account.CompanyProfile.PrimaryPhone).should('be.empty');
  cy.get(account.CompanyProfile.City).should('be.empty');
  let phone = faker.random.numeric(10);
  let city = faker.random.alpha(10);
  cy.get(account.CompanyProfile.PrimaryPhone).type(phone, { force: true });
  cy.get(account.CompanyProfile.City).type(city, { force: true });
  cy.get(account.CompanyProfile.SaveBtn).click({ force: true });
  cy.on('window:confirm', (str) => {
    // Confirm
    expect(str).to.contain('Account updated successfully');
    expect(account.CompanyProfile.PrimaryPhone).to.equal(phone);
    expect(account.CompanyProfile.City).to.equal(city);
  });
  //Reset //
  let citynew = faker.random.alpha(10);
  cy.get(account.CompanyProfile.City).clear({ force: true }).type(citynew, { force: true });
  cy.get(account.CompanyProfile.ResetBtn).click({ force: true });
});

Cypress.Commands.add('CompanySecurityPage', () => {
  cy.get(SideNavMenu.settings)
    .trigger('mouseover')
    .get(account.CompanyProfile.companyProfile)
    .click({ force: true })

    //find button with span text = Security//
    .get(account.Security.SecurityTab)
    .contains('Security')
    .should('be.visible')
    .click({ force: true })

    //find button with span text = Require 2FA and assert visable//
    .get(account.Security.mfaButton)
    .contains('Require 2FA')
    .should('be.visible')

    .get(account.Security.CompanyMFAText)
    .contains(
      'Enable this setting to set up 2FA Authentication through a secondary device such as Google Authenticator. Enabling this setting will require all users in this account to use two-factor authentication. Individual users can choose to use 2FA when this setting is disabled.'
    )
    .should('be.visible');
});

Cypress.Commands.add('canNotGetAtCompanySecurityPage', () => {
  cy.get(SideNavMenu.settings).trigger('mouseover').get(account.CompanyProfile.companyProfile).click({ force: true });
  cy.get(account.Security.SecurityTab).should('not.exist').forceVisit('/settings/security');
});

Cypress.Commands.add('CanNotEnterSettingsMenu', () => {
  cy.get('#Settings > .ant-menu-submenu-title').should('not.exist');
});

Cypress.Commands.add('billingSubscriptionEditAllowed', () => {
  cy.get(billing.subscription.changeSubBtn).should('be.enabled').click();
  cy.get(billing.subscription.subcriptionTitle).should('contain', 'Subscription Order Form');
  cy.get(billing.subscription.planTier).should('be.visible');
  cy.get(billing.subscription.paidSeatUser).click();
  cy.get(billing.subscription.paidSeatUserOptions).click();
  cy.get(billing.subscription.tc_checkbox).check();
  cy.get(billing.subscription.confirmBtn).should('be.enabled');
  cy.get(billing.subscription.cancelBtn).click();
});

Cypress.Commands.add('addNewCard', () => {
  cy.get(billing.AddCard.addCardBtn).should('be.enabled').click();
  cy.wait(5000);
  cy.get('iframe');
  cy.contains('Add your credit card');
  cy.get(billing.AddCard.cardNumberIframe)
    .its('0.contentDocument.body')
    .should('be.visible')
    .then((body) => {
      cy.wrap(body).find(billing.AddCard.cardNumber).type('4111111111111111', { force: true });
    });
  cy.get(billing.AddCard.chargifyMonthIframe)
    .its('0.contentDocument.body')
    .should('be.visible')
    .then((body) => {
      cy.wrap(body).find(billing.AddCard.month).type('12', { force: true });
    });
  cy.get(billing.AddCard.chargifyYearIframe)
    .its('0.contentDocument.body')
    .should('be.visible')
    .then((body) => {
      cy.wrap(body).find(billing.AddCard.Year).type('2030', { force: true });
    });
  cy.get(billing.AddCard.charifyCvvIframe)
    .its('0.contentDocument.body')
    .should('be.visible')
    .then((body) => {
      cy.wrap(body).find(billing.AddCard.Cvv).type('111', { force: true });
    });
  cy.get(billing.AddCard.chargifyFirstName)
    .its('0.contentDocument.body')
    .should('be.visible')
    .then((body) => {
      cy.wrap(body).find(billing.AddCard.firstName).type('test', { force: true });
    });
  cy.get(billing.AddCard.chargifyLastName)
    .its('0.contentDocument.body')
    .should('be.visible')
    .then((body) => {
      cy.wrap(body).find(billing.AddCard.lastName).type('test', { force: true });
    });

  cy.get(billing.AddCard.charifyCountryIframe)
    .its('0.contentDocument.body')
    .should('be.visible')
    .then((body) => {
      cy.wrap(body).find(billing.AddCard.country).select(2).should('have.value', 'GB');
    });
  cy.get(billing.AddCard.chargifyAddressLine1)
    .its('0.contentDocument.body')
    .should('be.visible')
    .then((body) => {
      cy.wrap(body).find(billing.AddCard.address).type('Business park', { force: true });
    });
  cy.get(billing.AddCard.chargifyCityIframe)
    .its('0.contentDocument.body')
    .should('be.visible')
    .then((body) => {
      cy.wrap(body).find(billing.AddCard.city).type('London', { force: true });
    });
  cy.get(billing.AddCard.chargifyState)
    .its('0.contentDocument.body')
    .should('be.visible')
    .then((body) => {
      cy.wrap(body).find(billing.AddCard.state).select('London, City of').should('have.value', 'LND');
    });
  cy.get(billing.AddCard.chargifyZip)
    .its('0.contentDocument.body')
    .should('be.visible')
    .then((body) => {
      cy.wrap(body).find(billing.AddCard.Zip).type('LG1', { force: true });
    });
  cy.get(billing.AddCard.cardSubmit).click({ force: true });
  cy.get(billing.AddCard.maskedCardNum).should('exist');
  cy.get(billing.AddCard.expDate).should('exist');
});

Cypress.Commands.add('removeCard', () => {
  cy.get(billing.AddCard.removeCard).click({ force: true });
  cy.on('window:confirm', (str) => {
    expect(str).to.contain('Removing your card will switch you to Pay-By-Invoice');
  });
  cy.get(billing.AddCard.okbtn).click({ force: true });
  cy.get(billing.AddCard.maskedCardNum).should('not.exist');
  cy.get(billing.AddCard.expDate).should('not.exist');

  cy.get(billing.AddCard.charifyCountryIframe)
    .its('0.contentDocument.body')
    .should('be.visible')
    .then((body) => {
      cy.wrap(body).find(billing.AddCard.country).select(2).should('have.value', 'GB');
    });
  cy.get(billing.AddCard.chargifyAddressLine1)
    .its('0.contentDocument.body')
    .should('be.visible')
    .then((body) => {
      cy.wrap(body).find(billing.AddCard.address).type('Business park', { force: true });
    });
  cy.get(billing.AddCard.chargifyCityIframe)
    .its('0.contentDocument.body')
    .should('be.visible')
    .then((body) => {
      cy.wrap(body).find(billing.AddCard.city).type('London', { force: true });
    });
  cy.get(billing.AddCard.chargifyState)
    .its('0.contentDocument.body')
    .should('be.visible')
    .then((body) => {
      cy.wrap(body).find(billing.AddCard.state).select('London, City of').should('have.value', 'LND');
    });
  cy.get(billing.AddCard.chargifyZip)
    .its('0.contentDocument.body')
    .should('be.visible')
    .then((body) => {
      cy.wrap(body).find(billing.AddCard.Zip).type('LG1', { force: true });
    });
  cy.get(billing.AddCard.cardSubmit).click({ force: true });
  cy.get(billing.AddCard.maskedCardNum).should('exist');
  cy.get(billing.AddCard.expDate).should('exist');
});

Cypress.Commands.add('removeCard', () => {
  cy.get(billing.AddCard.removeCard).click({ force: true });
  cy.on('window:confirm', (str) => {
    expect(str).to.contain('Removing your card will switch you to Pay-By-Invoice');
  });
  cy.get(billing.AddCard.okbtn).click({ force: true });
  cy.get(billing.AddCard.maskedCardNum).should('not.exist');
  cy.get(billing.AddCard.expDate).should('not.exist');
});

Cypress.Commands.add('SelectDefaultBalloon', (Balloon) => {
  cy.get(account.Configurations.balloonStylesDefaultBalloon).trigger('mouseover');
  cy.get(account.Configurations.balloonStylesDefaultBalloon).click();
  cy.get(account.Configurations.balloonStylePresetsSelect).click();
  cy.get(Balloon).click();
  cy.get(Balloon).should('be.visible');
});

Cypress.Commands.add('checkCancelDialogBoxFunctions', () => {
  cy.get(common.wizard.cancel).click();
  cy.get(common.CancelModal.cancelModalPopOver).should('be.visible');
  cy.get(common.CancelModal.cancelModalNo).click();
  cy.get(common.CancelModal.cancelModalPopOver).should('not.be.visible');
  cy.get(common.wizard.cancel).click();
  cy.get(common.CancelModal.cancelModalPopOver).should('be.visible');
  cy.get(common.CancelModal.cancelModalYes).click();
  cy.get('h1').should('be.visible').and('contain', 'Parts');
});
