import 'cypress-file-upload'
require('cypress-plugin-tab')


    beforeEach(() => {
        cy.loginPlannerWithBilling()
    })

    it('AddPartsSeeder', () => {
        Cypress._.times(25, () => {
            cy.addPartSeeder()
            cy.get('.custom-modal-footer > .btn-primary > span')
                .should('be.visible')
                .click({ force: true })
        })
    })
