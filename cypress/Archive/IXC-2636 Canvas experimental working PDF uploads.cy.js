import 'cypress-file-upload';
import partList from "../integration/locators/part-list.json"
import fileUpload from "../integration/locators/fileUpload.json"


it.only('File Upload using cypress-file-upload npm package', () => {
    cy.loginAdmin()
    cy.wait(6000)
    cy.get(partList.newPartBtn).should('exist').click()
    const fileName = 'test.PDF'
    mimeType: 'application/pdf',
        cy.get('input[type="file"]')

    cy.get(fileUpload.fileUploadArea).attachFile({ filePath: `/${fileName}`, encoding: "base64" }, { subjectType: 'drag-n-drop' })

    // cy.get('#file-submit').click()
    cy.get(fileUpload.fileUploadSaveBtn).should('be.visible').click();
    // cy.wait(100000)

});