// Tests Written //
// Invite users is clickable and modal is displayed
// Should show an error if no emails are entered (BUG: IXC-2898)
// Invite users is clickable and modal is displayed
// Should show an error if no emails are entered (BUG: IXC-2898)
// Should show an error if the emails entered are invalid
// Should show an error if # of emails entered exceed available seats
// Should show an error if # of emails entered exceed available seats
// Should show an error if duplicate emails are entered
// Should show an error if only existing emails are entered
// Should show a message about existing user if email entered already exist alongside new user
// Should show a message about existing users if some of multiple emails entered already exist (need to confirm rules for this commented out)
// Should not be able to invite multiple new paid users when 1 seat available
// Should not be able to invite new paid user when there are no avaliable seats
// Should be able to invite new free user, user shows as pending, with Resend and cancel invite buttons
// Should be able to invite new paid user, user shows as pending, with Resend and cancel invite buttons
// Should show success message after resending email

// Tests To Do //

//CONTROLLED DOCUMENTS // 
// AMEND - Should be able to invite new free user, user shows as pending, with Resend and cancel invite buttons - To check for no access to controlled documents
// AMEND - Should be able to invite new paid user, user shows as pending, with Resend and cancel invite buttons - To check for no access to controlled documents
// Should be able to invite new free user with access to controlled documents, user shows as pending, and shows access to controlled documents
// Should be able to invite new paid user with access to controlled documents, user shows as pending, and shows access to controlled documents

import { faker } from '@faker-js/faker';
import inviteUserModal from "../../../integration/locators/Invite-users-modal.json";
import account from "../../../integration/locators/settings-account.json";
import Seats from "../../../integration/locators/Seats.json"

describe('Invite Users - Error responses', () => {
    
    it('invite users is clickable and modal is displayed', () => {
        //Test set up
        cy.loginAdmin()
        cy.navigateToSeats()
        cy.get(account.Seats.InviteUsers).should('be.enabled').click()
        cy.get(inviteUserModal.inviteUserModal).should('be.visible')
        //check modal content is dispalyed
        cy.get(inviteUserModal.modalTitle).should('have.text', "Invite a New User")
        cy.get(inviteUserModal.email.modalEmailDescription).should('exist').and('have.text', 'Separate address by commas to invite multiple. Invitations expire after 30 minutes.')
        cy.get(inviteUserModal.email.modalEmailInput).should('exist')
        cy.get(inviteUserModal.roles.modalRoleTitle).should('exist').and('have.text', 'Role')
        cy.get(inviteUserModal.roles.modalRoleDescription).should('exist').and('have.text', 'Can view, edit, and create parts, reports, and jobs.')
        cy.get(inviteUserModal.roles.modalRoleInput).should('exist')
        cy.get(inviteUserModal.modalControlledInput).should('exist').and('have.text', 'YesNo')
        cy.get(inviteUserModal.cancelInvite).should('exist')
        cy.get(inviteUserModal.confirmInvite).should('exist')

    })

    it("Should show an error if when invailid emails are entered", () => {
        //Test set up
        cy.loginAdmin()
        cy.navigateToSeats()
        cy.get(account.Seats.InviteUsers).should('be.enabled').click()
        cy.get(inviteUserModal.inviteUserModal).should('be.visible')
        cy.get(account.Seats.Email).type('plainaddress') //invalid missing @ symbol and domain
        cy.get(inviteUserModal.confirmInvite).click()
        cy.wait(2000)
        cy.get(inviteUserModal.email.modalEmailError).should('exist').and('have.text', 'One or more email address is invalid')

        cy.get(account.Seats.Email).clear().type('@domain.com') //invalid missing address
        cy.get(inviteUserModal.confirmInvite).click()
        cy.wait(2000)
        cy.get(inviteUserModal.email.modalEmailError).should('exist').and('have.text', 'One or more email address is invalid')

        cy.get(account.Seats.Email).clear().type('halo@' + '@aharo.com') //invalid double @
        cy.get(inviteUserModal.confirmInvite).click()
        cy.wait(2000)
        cy.get(inviteUserModal.email.modalEmailError).should('exist').and('have.text', 'One or more email address is invalid')

        cy.get(account.Seats.Email).clear().type('email..email@domain.com') //invalid double dots
        cy.get(inviteUserModal.confirmInvite).click()
        cy.wait(2000)
        cy.get(inviteUserModal.email.modalEmailError).should('exist').and('have.text', 'One or more email address is invalid')

        cy.get(account.Seats.Email).clear().type('#@%^%#$@#$@#.com') //invalid garbage
        cy.get(inviteUserModal.confirmInvite).click()
        cy.wait(2000)
        cy.get(inviteUserModal.email.modalEmailError).should('exist').and('have.text', 'One or more email address is invalid')

        cy.get(account.Seats.Email).clear().type('email@111.222.333.44444') //invalid ip address
        cy.get(inviteUserModal.confirmInvite).click()
        cy.wait(2000)
        cy.get(inviteUserModal.email.modalEmailError).should('exist').and('have.text', 'One or more email address is invalid')

        cy.get(account.Seats.Email).clear().type('email.@domain.com') //invalid trailing dot
        cy.get(inviteUserModal.confirmInvite).click()
        cy.wait(2000)
        cy.get(inviteUserModal.email.modalEmailError).should('exist').and('have.text', 'One or more email address is invalid')

        // Commented out as showing as vaild uni code Being looked into
        // cy.get(account.Seats.Email).clear().type('あいうえお@domain.com') //invalid uni code Being looked into
        // cy.get(inviteUserModal.confirmInvite).click()
        // cy.wait(2000)
        // cy.get(inviteUserModal.email.modalEmailError).should('exist').and('have.text', 'One or more email address is invalid')

    })


    it("Should show an error if no emails are entered", () => {
        //Test set up
        cy.loginAdmin()
        cy.navigateToSeats()
        cy.get(account.Seats.InviteUsers).should('be.enabled').click()
        cy.get(inviteUserModal.inviteUserModal).should('be.visible')
        //Invite user
        cy.get(inviteUserModal.confirmInvite).click()
        cy.get(inviteUserModal.email.modalEmailError).should('exist').and('have.text', 'Please provide at least one email address to invite a user')

        //BUG: IXC-2898 for when a space and not text has been raised. Commented out code for this reason. 
        //Check blank space dispays error
        //cy.get(account.Seats.Email).type(' ')
        // cy.get(inviteUserModal.confirmInvite).should('exist').click()
        // cy.get(inviteUserModal.email.modalEmailError).should('exist').and('have.text', 'Please provide at least one email address to invite a user')
    })

    it("Should show an error if duplicate emails are entered", () => {
        //Test set up
        cy.loginAdmin()
        cy.navigateToSeats()
        cy.get(account.Seats.InviteUsers).should('be.enabled').click()
        cy.get(inviteUserModal.inviteUserModal).should('be.visible')
        //Enter emails
        const duplicate = 'test@test.com'
        cy.get(account.Seats.Email).type(duplicate + ',' + duplicate)
        cy.get(inviteUserModal.roles.modalRoleInput).click()
        cy.get(inviteUserModal.roles.Reviewer).click()
        cy.get(inviteUserModal.confirmInvite).click()
        cy.get(inviteUserModal.email.modalEmailError).should('exist').and('have.text', 'Remove duplicate emails')
    })

    it("Should show an error if # of emails entered exceed available seats ", () => {
        //Test set up
        cy.loginAdmin()
        cy.navigateToSeats()
        cy.get(account.Seats.InviteUsers).should('be.enabled').click()
        cy.get(inviteUserModal.inviteUserModal).should('be.visible')
        //Enter emails
        const email1 = faker.random.alpha(12) + '@test.com'
        const email2 = faker.random.alpha(12) + '@test.com'
        const email3 = faker.random.alpha(12) + '@test.com'
        const email4 = faker.random.alpha(12) + '@test.com'
        cy.wait(8000)
        cy.get(account.Seats.Email).type(email1 + "," + email2 + "," + email3 + "," + email4)
        cy.get(inviteUserModal.confirmInvite).click()
        cy.get(inviteUserModal.email.modalEmailError).should('exist').and('have.text', 'Insufficient seats open, remove emails or purchase more seats')
    })

     it("Should show an error if only existing emails are entered", () => {
        const path = 'https://www.ixc-stage.com/settings/seats'
        //Test set up
        cy.loginAdmin()
        cy.navigateToSeats()
        cy.get(account.Seats.InviteUsers).should('be.enabled').click()
        cy.get(inviteUserModal.inviteUserModal).should('be.visible')
        //Enter exsiting emails
        cy.url().then(($url) => {
            if ($url == path) {
                cy.get(account.Seats.Email).type((Cypress.env('stagePlannerwithbillingUsername')))
            }
            else {
                cy.get(account.Seats.Email).type(Cypress.env('plannerwithbillingUsername'))
            }
        })
        cy.get(inviteUserModal.roles.modalRoleInput).click()
        cy.get(inviteUserModal.roles.Reviewer).click()
        cy.get(inviteUserModal.confirmInvite).click()
        cy.get(inviteUserModal.email.modalEmailError).should('exist').and('have.text', '1 Member(s) already exist')
    })

    // Need to confirm invite users rule as unable to get consistent message returning
    //it("Should show a message about existing user if email entered already exist alongside new user", () => {
    // //Test set up
    // cy.loginAdmin()
    // cy.navigateToSeats()
    // cy.get(account.Seats.InviteUsers).should('be.enabled').click()
    // cy.get(inviteUserModal.inviteUserModal).should('be.visible')
    //     const email1 = faker.random.alpha(12) + '@test.com'
    //     const emails = email1 +',spartan.ideagen+admin@gmail.com' 

    //     //Enter email
    //     cy.get(account.Seats.Email).type(emails)
    //     cy.get(inviteUserModal.roles.modalRoleInput).click()
    //     cy.get(inviteUserModal.roles.Reviewer).click()
    //     cy.get(inviteUserModal.confirmInvite).click()  
    //     cy.get(inviteUserModal.inviteNotification).contains('spartan.ideagen+admin@gmail.com already belongs to your account').should('be.visible')

    //     //cancel invitation
    //     cy.cancelInvitation('[data-cy="' + email1 +'"]')
    //})

    // Need to confirm invite users rule as unable to get consistent message returning
    ////Test set up
    // cy.loginAdmin()
    // cy.navigateToSeats()
    // cy.get(account.Seats.InviteUsers).should('be.enabled').click()
    // cy.get(inviteUserModal.inviteUserModal).should('be.visible')
    //    it("Should show a message about existing users if some of multiple emails entered already exist", () => {
    //     const email1 = faker.random.alpha(12) + '@test.com'
    //     const email2 = faker.random.alpha(12) + '@test.com'
    //     const email3 = faker.random.alpha(12) + '@test.com'
    //     const emails = email1 + ',spartan.ideagen+admin@gmail.com,'
    //     +email2 +',spartan.ideagen+reviewer@gmail.com,' +email3

    //     //Enter email
    //     cy.get(account.Seats.Email).type(emails)
    //     cy.get(inviteUserModal.roles.modalRoleInput).click()
    //     cy.get(inviteUserModal.roles.Reviewer).click()
    //     cy.get(inviteUserModal.confirmInvite).click()

    //     cy.get(inviteUserModal.inviteNotification).contains('One or more users already exist and could not be invited').should('be.visible')

    //     //cancel invitation
    //     cy.cancelInvitation('[data-cy="' + email1 +'"]')
    //     cy.cancelInvitation('[data-cy="' + email2 +'"]')
    //     cy.cancelInvitation('[data-cy="' + email3 +'"]')
    //})


    it('Should not be able to invite multiple new paid users when 1 seat available', () => {
        //Test set up
        cy.loginAdmin()
        cy.navigateToSeats()
        cy.get(account.Seats.InviteUsers).should('be.enabled').click()
        cy.get(inviteUserModal.inviteUserModal).should('be.visible')
        //Invite multiple users
        const email1 = faker.random.alpha(12) + '@test.com'
        const email2 = faker.random.alpha(12) + '@test.com'
        cy.get(account.Seats.Email).type(email1 + "," + email2)
        cy.get(inviteUserModal.roles.modalRoleInput).click()
        cy.get(inviteUserModal.roles.Planner).click()
        cy.get(inviteUserModal.confirmInvite).click()
        cy.get(account.Seats.SendInvites).click({ force: true })
        cy.get(inviteUserModal.email.modalEmailError).should('exist').and('have.text', 'Insufficient seats open, remove emails or purchase more seats')
    })

    it('Should not be able to invite new paid user when there are no avaliable seats', () => {
        //Test set up
        cy.loginAdmin()
        cy.navigateToSeats()
        cy.get(account.Seats.InviteUsers).should('be.enabled').click()
        cy.get(inviteUserModal.inviteUserModal).should('be.visible')
        // Fill avaliable seats
        cy.invitePaidUser()
        cy.wait(3000)
        cy.get('@email').then((email) => {
            const invitedUser = '[data-cy="' + email + '"]'

            //check user invite is pending and Resend / Cancel are displayed//
            cy.get(invitedUser + Seats.seatsList.role).contains('Invite Pending')

            const email1 = faker.random.alpha(12) + '@test.com'
            const email2 = faker.random.alpha(12) + '@test.com'
            cy.get(account.Seats.InviteUsers).click({ force: true })
            cy.wait(3000)
            cy.get(account.Seats.Email).type(email1 + "'" + email2)
            cy.get(inviteUserModal.roles.modalRoleInput).click()

            //Check Paid for selectors are disabled
            cy.get(inviteUserModal.roles.roleDropdown).find('[label="Planner"]').should("have.attr", "class", inviteUserModal.roles.roleDisabledNotSelected)

            cy.get(inviteUserModal.roles.roleDropdown).find('[label="Planner with Billing"]').should("have.attr", "class", inviteUserModal.roles.roleDisabledNotSelected)
            cy.get(inviteUserModal.roles.Reviewer).click()
            cy.get(inviteUserModal.cancelInvite).click()

            //cancel invitation
            cy.get(invitedUser + Seats.seatsList.user).should('exist')
            cy.cancelInvitation('[data-cy="' + email + '"]')
            cy.wait(8000)
            cy.get(invitedUser + Seats.seatsList.user).should('not.exist')
        })
        // Check following delete invitation Planner and Planner with billing are enabled 
        cy.get(account.Seats.InviteUsers).click({ force: true })
        cy.wait(3000)
        cy.get(inviteUserModal.roles.modalRoleInput).click()

        //Check Paid for selectors are enables
        cy.get(inviteUserModal.roles.roleDropdown).find('[label="Planner"]').should("have.attr", "class", inviteUserModal.roles.roleEnabledSelected)

        cy.get(inviteUserModal.roles.roleDropdown).find('[label="Planner with Billing"]').should("have.attr", "class", inviteUserModal.roles.roleEnabledNotSelected)
    })
})

describe('Invited Users in user list', () => {

    it('Should be able to invite new free user, user shows as pending, with Resend and cancel invite buttons', () => {
        //Test set up
        cy.loginAdmin()
        cy.navigateToSeats()
        cy.get(account.Seats.InviteUsers).click({ force: true })
        //invite free user
        cy.inviteFreeUser()
        cy.wait(6000)
        cy.get('@email').then((email) => {
            const invitedUser = '[data-cy="' + email + '"]'

            //check user invite is pending anc Resend / Cancel are displayed//
            cy.get(invitedUser + Seats.seatsList.role).contains('Invite Pending')
            cy.get(invitedUser + Seats.seatsList.access).contains('Resend')
            cy.get(invitedUser + Seats.seatsList.access).contains('Cancel')

            //cancel invitation
            cy.get(invitedUser + Seats.seatsList.user).should('exist')
            cy.cancelInvitation('[data-cy="' + email + '"]')
            cy.wait(8000)
            cy.get(invitedUser + Seats.seatsList.user).should('not.exist')
        })
    })

    it('Should be able to invite new paid user, user shows as pending, with Resend and cancel invite buttons', () => {
        //Test set up
        cy.loginAdmin()
        cy.navigateToSeats()
        cy.get(account.Seats.InviteUsers).click({ force: true })
        //Invite paid user
        cy.invitePaidUser()
        cy.wait(6000)
        cy.get('@email').then((email) => {
            const invitedUser = '[data-cy="' + email + '"]'

            //check user invite is pending anc Resend / Cancel are displayed//
            cy.get(invitedUser + Seats.seatsList.role).contains('Invite Pending')
            cy.get(invitedUser + Seats.seatsList.access).contains('Resend')
            cy.get(invitedUser + Seats.seatsList.access).contains('Cancel')

            //cancel invitation
            cy.get(invitedUser + Seats.seatsList.user).should('exist')
            cy.cancelInvitation('[data-cy="' + email + '"]')
            cy.wait(8000)
            cy.get(invitedUser + Seats.seatsList.user).should('not.exist')
        })
    })

    it('Should show success message after resending email', () => {
        //Test set up
        cy.loginAdmin()
        cy.navigateToSeats()
        cy.get(account.Seats.InviteUsers).click({ force: true })
        //Test resend invite email
        cy.inviteFreeUser()
        cy.wait(8000)
        cy.get('@email').then((email) => {
            const invitedUser = '[data-cy="' + email + '"]'

            //Resend user invite
            cy.get(invitedUser + Seats.seatsList.access).contains('Resend').click()
            cy.wait(3000)
            cy.get(inviteUserModal.inviteNotification).contains('Invitation Sent!').should('be.visible')
            cy.get(inviteUserModal.inviteNotificationDescription).contains('A new invitation has been sent.').should('be.visible')

            //cancel invitation
            cy.get(invitedUser + Seats.seatsList.user).should('exist')
            cy.cancelInvitation('[data-cy="' + email + '"]')
            cy.wait(8000)
            cy.get(invitedUser + Seats.seatsList.user).should('not.exist')
        })
    })
})
