// Tests Written //

// SUBSCRIPTION MODAL //
// shows the user the correct subscription cost based on no. of seats for annual subscription (Pricing is set in evniroment config)
// User is able to add credit card
// Only allows remittance for annual subscriptions
// User can cancel subscription modal
// User can close subscription modal with X
// User can access subscription T&C's
// User can access subscription Privacy Policy
// Confirm / Confirm and Pay button is disabled until T&C's / Privacy Policy checkbox is selected

// Tests To Do //

// SUBSCRIPTION SIGN UP //
// User can successfully purchase an annual subscription and cancel it
// User can successfully purchase an monthly subscription and cancel it

import billing from '../../../integration/locators/billing.json';
import common from '../../../integration/locators/common.json';
import account from '../../../integration/locators/settings-account.json';
import sideNav from '../../../integration/locators/side-nav-menu-section.json';

import 'cypress-file-upload';
require('cypress-plugin-tab');

describe('Subscriptions - Subscription modal', () => {
  it('shows the user the correct subscription cost based on the no. of seats for Annual subscription (Pricing is set in environment config)', () => {
    cy.loginAdmin();
    //Navigate to subscription modal
    cy.get(sideNav.settings).trigger('mouseover');
    cy.get(sideNav.billing).click();

    // Check that the page URL has changed
    cy.url().should('include', '/settings/billing');

    // Check for specific elements on the page
    cy.get('h1').should('contain', 'Account Settings');
    cy.get(account.Billing.billingTab).should('be.visible');
    cy.get(common.Billing.changeSubscriptionBtn).should('have.text', 'Change Subscription');
    cy.get(common.Billing.cancelSubscriptionBtn).should('have.text', 'Cancel Subscription');
    cy.get(common.Billing.addCardBtn).should('have.text', 'Add a Credit Card');
    cy.get(common.Billing.manageSeatsBtn).should('have.text', 'Manage Seats');

    // Check that the page contains specific text
    cy.contains('Billing History & Invoices').should('be.visible');

    //Open subscription modal
    cy.get(common.Billing.changeSubscriptionBtn).click();

    //Check that the subscription cost is correct for 1 seat
    cy.get(billing.subscriptionModal.paidUserSeatStep).click();
    cy.get(billing.subscriptionModal.oneSeat).click();
    cy.get(billing.subscriptionModal.subcriptionPlanPeriod).should('contain', '12 months');
    cy.get(billing.subscriptionModal.DeferPeriod).should('contain', '12 months');
    cy.get(billing.subscriptionModal.ModalDeferPrice).should('contain', '$1,700.00');
    cy.get(billing.subscriptionModal.ModalDeferPrice).should('contain', '$1,700.00');

    //Check that the subscription cost is correct for 2 to 3 seats
    cy.get(billing.subscriptionModal.seatDropdown).click();
    cy.get(billing.subscriptionModal.twoToThreeSeats).click();
    cy.get(billing.subscriptionModal.subcriptionPlanPeriod).should('contain', '12 months');
    cy.get(billing.subscriptionModal.DeferPeriod).should('contain', '12 months');
    cy.get(billing.subscriptionModal.ModalDefer).should('contain', '$3,400.00');
    cy.get(billing.subscriptionModal.ModalDeferPrice).should('contain', '$3,400.00');

    //Check that the subscription cost is correct for 4 to 5 seats
    cy.get(billing.subscriptionModal.seatDropdown).click();
    cy.get(billing.subscriptionModal.fourToFiveSeats).click();
    cy.get(billing.subscriptionModal.ModalPlanPrice).should('contain', '$5,100.00');
    cy.get(billing.subscriptionModal.cancelBtn).click();
    cy.get(common.Billing.changeSubscriptionBtn).click();
  });

  it('User is able to add credit card', () => {
    cy.loginAdmin();
    //Navigate to subscription modal
    cy.wait(10000);
    cy.get(sideNav.settings).trigger('mouseover');
    cy.get(sideNav.billing).click();

    // billing subscription edit allowed //
    cy.billingSubscriptionEditAllowed();

    //Add a new card//
    cy.addNewCard();

    //Remove the card //
    cy.removeCard();
  });

  it('Only allows remittance for annual subscriptions', () => {
    cy.loginAdmin();
    //Navigate to subscription modal
    cy.wait(10000);
    cy.get(sideNav.settings).trigger('mouseover');
    cy.get(sideNav.billing).click();

    // billing subscription edit allowed //
    cy.billingSubscriptionEditAllowed();

    //Open subscription modal
    cy.get(common.Billing.changeSubscriptionBtn).click();

    cy.get(billing.subscriptionModal.subcriptionPlanPeriod).should('contain', '12');

    //Click confirm subscription button
    cy.get(billing.subscriptionModal.confirmBtn).click();
  });

  it('User can cancel subscription modal', () => {
    cy.loginAdmin();
    //Navigate to subscription modal
    cy.wait(10000);
    cy.get(sideNav.settings).trigger('mouseover');
    cy.get(sideNav.billing).click();

    // billing subscription edit allowed //
    cy.billingSubscriptionEditAllowed();

    //Open subscription modal
    cy.get(common.Billing.changeSubscriptionBtn).click();
    //Click cancel subscription button
    cy.get(billing.subscriptionModal.cancelBtn).should('be.visible').click();
  });

  it('User can close subscription modal', () => {
    cy.loginAdmin();
    //Navigate to subscription modal
    cy.wait(10000);
    cy.get(sideNav.settings).trigger('mouseover');
    cy.get(sideNav.billing).click();

    // billing subscription edit allowed //
    cy.billingSubscriptionEditAllowed();

    //Open subscription modal
    cy.get(common.Billing.changeSubscriptionBtn).click();

    //Click cancel subscription button
    cy.get(billing.subscriptionModal.close).should('be.visible').click();
  });

  it('User can access subscription T&Cs', () => {
    cy.loginAdmin();
    //Navigate to subscription modal
    cy.get(sideNav.settings).trigger('mouseover');
    cy.get(sideNav.billing).click();

    // billing subscription edit allowed //
    cy.billingSubscriptionEditAllowed();

    //Open subscription modal
    cy.get(common.Billing.changeSubscriptionBtn).click();

    //View T&Cs
    cy.get(billing.subscriptionModal.termsAndConditions).click();
  });

  it('User can access subscription Privacy Policy', () => {
    cy.loginAdmin();
    //Navigate to subscription modal
    cy.get(sideNav.settings).trigger('mouseover');
    cy.get(sideNav.billing).click();

    // billing subscription edit allowed //
    cy.billingSubscriptionEditAllowed();

    //Open subscription modal
    cy.get(common.Billing.changeSubscriptionBtn).click();

    //View T&Cs
    cy.get(billing.subscriptionModal.privacyPolicy).click();
  });

  it("Confirm / Confirm and Pay button is disabled until T&C's / Privacy Policy checkbox is selected", () => {
    cy.loginAdmin();
    //Navigate to subscription modal
    cy.get(sideNav.settings).trigger('mouseover');
    cy.get(sideNav.billing).click();
    //Open subscription modal
    cy.get(common.Billing.changeSubscriptionBtn).click();
    cy.get(billing.subscriptionModal.termsAndConditionsCheckbox).should('not.be.checked');
    //Click confirm subscription button
    cy.get(billing.subscriptionModal.confirmBtn).should('be.disabled').click({ force: true });
    cy.wait(1000);
    cy.get(billing.subscriptionModal.termsAndConditionsCheckbox).click();
    cy.get(billing.subscriptionModal.termsAndConditionsCheckbox).should('be.checked');
    cy.get(billing.subscriptionModal.confirmBtn).should('be.enabled').click({ force: true });
  });
});
