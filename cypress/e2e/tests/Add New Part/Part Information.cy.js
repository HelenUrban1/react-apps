// Tests Written //
//DEFAULTS//
//Displays appropriate input fields and default values
//Allowed characters for each field: A - Z, a - z, 0 - 9, spaces, special characters (!"#$%&'*+,-./:;<=>?@[]^_`{|}~)" are added and displayed in title bar
//Emoji's are not displayed in fields or in title bar

//PART NAME//
//Part name and Drawing name can be the same

//Part Information toolbar//
//Add a new drawing

//CUSTOMER//
//User is able to add new customer
// Allows search within customer field
// Cancel should work when adding customer
// User is able to select a customer
// Allows characters (!"#$%&'*+,-./:;<=>?@[]^_`{|}~)"
// Cannot duplicate an existing customer

//PART NUMBER//
//Prevents the user from continuing the wizard if there is no Part Number
//Continues the wizard if there is a Part Number
//Alerts the user if Part Number is deleted and does not save blank Part Number
//Part number and drawing number can be the same are displayed in the title bar

//REVISION//
//Revision is displayed in title bar
//Drawing and part revision can be the same
//User can create a new revision -- May already be covered in Document Information
//Revision History is viewable -- May already be covered in Document Information
//New revisions are shown in revision history -- May already be covered in Document Information

const fileName = 'test.pdf';
const today = new Date().toISOString().substring(0, 10);

import common from '../../../integration/locators/common.json';
import partCanvas from '../../../integration/locators/part-canvas-section.json';
import documentInformation from '../../../integration/locators/part-docs-info-section.json';
import partInformation from '../../../integration/locators/part-information-section.json';
import docInformation from '../../../integration/locators/part-docs-info-section.json';
import { default as partList, default as partlist } from '../../../integration/locators/part-list.json';
import fileUpload from '../../../integration/locators/fileUpload.json';

import { faker } from '@faker-js/faker';
import 'cypress-file-upload';
require('cypress-plugin-tab');
const validCharacters = 'AGS boz !#$%<{>=?@[]&*+,-.:;';
const validCharactersConfirmation = 'AGS boz !#$%<{>=?@[]&*+,-.:;';

beforeEach(() => {
  // Login
  cy.loginAdmin();
  cy.visit('/');
  //add part
  cy.addPart();
});

describe('Part Information', () => {
  it('Part Information page displays appropriate input fields and default values', () => {
    cy.get(partInformation.customer)
      .should('exist')
      .and('contain', '')
      .get(partInformation.partNumber)
      .should('exist')
      .and('contain', '')
      .get(partInformation.partRevision)
      .should('exist')
      .and('contain', today)
      .get(partInformation.partName)
      .should('exist')
      .and('contain', fileName.substring(0, 4))
      .get(partInformation.documentControl)
      .should('contain', 'None')
      //Update Part Number
      .get(partInformation.partNumber)
      .clear()
      .addPartNumber();

    //Navigate to parts
    cy.navigateToPartList();
    //Delete part
    cy.deletePart();
  });

  it('Allowed characters are accepted and displayed in title bar', () => {
    cy.get(partInformation.partTitle)
      .should('exist')
      //Enter valid characters into Part Number, Part Name and Revision fields
      .get(partInformation.partNumber)
      .type(validCharacters)
      .get(partInformation.partRevision)
      .clear()
      .type(validCharacters)
      .get(partInformation.partName)
      .clear()
      .type(validCharacters)

      //Check valid characters are displayed in Part Name, Part Number and Revison fields
      .get(partInformation.partNumber)
      .should('have.text', validCharactersConfirmation)
      .get(partInformation.partName)
      .should('have.text', validCharactersConfirmation)
      .get(partInformation.partRevision)
      .should('have.text', validCharactersConfirmation)

      //Validate Part Number and Revison is correct in the title bar
      .get(partCanvas.documentTools.partNumber)
      .should('have.text', validCharactersConfirmation)
      .get(partCanvas.documentTools.partRevisionSelect)
      .should('have.text', validCharactersConfirmation);

    //Update Part Number
    cy.get(partInformation.partNumber).clear();
    cy.addPartNumber();
    cy.wait(1500);

    //Navigate to parts home
    cy.navigateToPartList();
    //Delete part
    cy.deletePart();
  });

  it("Emoji's are not displayed in fields or in title bar", () => {
    const Emoji = '😄';
    cy.addPartNumber();
    cy.get(partInformation.partName).type(Emoji);
    cy.get(partInformation.partName).should('have.text', 'test');
    cy.get(partInformation.partNumber).type(Emoji);
    cy.get('@partNumber').then((partNumber) => {
      cy.get(partInformation.partNumber).should('have.text', partNumber);
      cy.get(partCanvas.documentTools.partNumber).should('have.text', partNumber);
      cy.get(partInformation.partRevision).type(Emoji);
      cy.get(partInformation.partRevision).should('have.text', today + Emoji);
      cy.get(partCanvas.documentTools.partRevisionSelect).should('have.text', today);
      cy.get(common.wizard.next).click();
      cy.get(common.wizard.back).click();
      cy.get(partInformation.partRevision).should('have.text', today);
      //Navigate to parts home
      cy.navigateToPartList();
      //Delete part
      cy.deletePart();
    });
  });

  it('Part name and Drawing name can be the same', () => {
    cy.addPartNumber();

    //Check part name = document file name
    cy.get(partInformation.partName);
    cy.should('exist');
    cy.and('contain', fileName.substring(0, 4));

    //Move to document information
    cy.navigateToDocInfo();
    cy.get(docInformation.drawingName)
      .should('contain', fileName)
      .clear()
      .type(fileName.substring(0, 4))
      .and('contain', fileName.substring(0, 4))

      .log('Part name and Drawing name are the same');
    //Navigate to parts home
    cy.navigateToPartList();
    //Delete part
    cy.deletePart();
  });

  it('Part Number - Prevents the user from continuing the wizard if there is no Part Number', () => {
    //Part Number - Prevents the user from continuing the wizard if there is no Part Number
    cy.get('.btn-primary').trigger('mouseover');
    cy.get(common.wizard.next).should('be.disabled');
    cy.get(common.wizard.toolTip)
      .should('be.visible')
      .should('contain', 'Add Part Number to Continue')
      //Update Part Number
      .get(partInformation.partNumber)
      .clear()
      .addPartNumber();

    //Navigate to parts home
    cy.navigateToPartList();
    //Delete part
    cy.deletePart();
  });

  it('Continues the wizard if there is a Part Number', () => {
    cy.addPartNumber();
    cy.get(common.wizard.next).click();
    cy.get(documentInformation.docTitle).should('exist');

    //Navigate to parts home
    cy.navigateToPartList();
    //Delete part
    cy.deletePart();
  });

  it('Alerts the user if Part Number is deleted and does not save blank Part Number', () => {
    cy.addPartNumber();
    cy.get(common.wizard.next).click();
    cy.get(documentInformation.docTitle).should('exist');
    cy.get(documentInformation.drawingNumber).type(faker.random.alpha(4));
    cy.get(common.wizard.back).click();
    cy.get(partInformation.partNumber).clear();
    cy.get(common.wizard.next).should('be.disabled');
    cy.get('.btn-primary').trigger('mouseover');
    cy.get(common.wizard.toolTip).should('be.visible').should('contain', 'Add Part Number to Continue');
    //Update Part Number
    cy.wait(1500);
    cy.get(partInformation.partNumber).clear().addPartNumber();

    //Navigate to parts home
    cy.navigateToPartList();
    //Delete part
    cy.deletePart();
  });

  it('Part number and drawing number can be the same are displayed in the title bar', () => {
    cy.addPartNumber();
    cy.get('@partNumber').then((partNumber) => {
      cy.get(partCanvas.documentTools.partNumber).should('have.text', partNumber);
      cy.get(common.wizard.next).click();
      cy.wait(500);

      // add drawing number
      cy.get(partInformation.drawingNumber).type(partNumber);
      cy.get(documentInformation.drawingName).click();
      cy.get(partCanvas.documentTools.partDrawingSelect).should('have.text', partNumber);

      //Navigate to parts home
      cy.navigateToPartList();
      //Delete part
      cy.deletePart();
    });

    it('Add a new customer', () => {
      //Add new customer
      cy.get(partInformation.customer).click({ force: true }).get(partInformation.addCustomer).click();
      cy.on('window:confirm', (str) => {
        expect(str).to.contain('Create Customer');
      });

      let newCustomer = faker.random.alpha(8);
      cy.get(partInformation.newCustomerName).type(newCustomer).get(partInformation.createCustomerOkButton).click();

      //convert the first character to Uppercase to match the customer name
      let newCustomer1 = newCustomer.charAt(0).toUpperCase() + newCustomer.slice(1, 3);
      cy.get(partInformation.customer)
        .should('contain', newCustomer1)
        //Update Part Number
        .get(partInformation.partNumber)
        .clear()
        .addPartNumber();

      //Navigate to parts home
      cy.navigateToPartList();
      //Delete part
      cy.deletePart();
    });

    it('Allows search within customer field', () => {
      //Add new customer
      cy.get(partInformation.customer).click({ force: true }).get(partInformation.addCustomer).click();
      cy.on('window:confirm', (str) => {
        expect(str).to.contain('Create Customer');
      });

      let newCustomer = faker.random.alpha(8);
      cy.get(partInformation.newCustomerName).type(newCustomer).get(partInformation.createCustomerOkButton).click();

      //convert the first character to Uppercase to match the customer name
      let newCustomer1 = newCustomer.charAt(0).toUpperCase() + newCustomer.slice(1, 3);
      cy.get(partInformation.customer).should('contain', newCustomer1);
      cy.get(partInformation.customer).click({ force: true }).type(newCustomer1).type('{enter}').should('contain', newCustomer1);
      //Update Part Number
      cy.get(partInformation.partNumber).addPartNumber();

      //Navigate to parts home
      cy.navigateToPartList();
      //Delete part
      cy.deletePart();
    });

    it('Allowed characters are accepted and displayed in customer field', () => {
      // Allows characters (!"#$%&'*+,-./:;<=>?@[]^_`{|}~)"
      cy.get(partInformation.customer).click().type(validCharacters);
      // cy.get(partInformation.customer).type('{enter}');
      cy.get(partInformation.customerListSearch).eq(0).should('have.value', validCharactersConfirmation);
      //Add new customer
      cy.get(partInformation.addCustomer).click();
      cy.on('window:confirm', (str) => {
        expect(str).to.contain('Create Customer');
      });

      let newCustomer = faker.random.alpha(8);
      cy.get(partInformation.newCustomerName).type(newCustomer).get(partInformation.createCustomerOkButton).click();
      //Update Part Number
      cy.get(partInformation.partNumber).clear().addPartNumber();

      //Navigate to parts home
      cy.navigateToPartList();
      //Delete part
      cy.deletePart();
    });

    it('Cancel should work when adding customer', () => {
      // Cancel should work when adding customer
      cy.get(partInformation.customer).click({ force: true }).get(partInformation.addCustomer).click();

      cy.get(partInformation.customer).get(partInformation.addCustomer).click().get(partInformation.newCustomerName).type('Cancel ME').get(partInformation.createCustomerCancelButton).click();

      //Add new customer
      cy.get(partInformation.customer).click({ force: true }).get(partInformation.addCustomer).click();
      cy.on('window:confirm', (str) => {
        expect(str).to.contain('Create Customer');
      });

      let newCustomer = faker.random.alpha(8);
      cy.get(partInformation.newCustomerName).type(newCustomer).get(partInformation.createCustomerOkButton).click();
      //Update Part Number
      cy.get(partInformation.partNumber).clear().addPartNumber();

      //Navigate to parts home
      cy.navigateToPartList();
      //Delete part
      cy.deletePart();
    });
  });

  it('User is able to select a customer', () => {
    // User is able to select a customer
    //Add new customer
    cy.get(partInformation.customer).click({ force: true }).get(partInformation.addCustomer).click();
    cy.on('window:confirm', (str) => {
      expect(str).to.contain('Create Customer');
    });

    let newCustomer = faker.random.alpha(8);
    // add new customer
    cy.get(partInformation.newCustomerName).type(newCustomer).get(partInformation.newCustomerName).should('have.value', newCustomer).get(partInformation.createCustomerOkButton).click();

    //Update Part Number
    cy.get(partInformation.partNumber).clear().addPartNumber();

    //Navigate to parts home
    cy.navigateToPartList();
    //Delete part
    cy.deletePart();
  });

  it('Revision is displayed in title bar', () => {
    cy.addPartNumber();
    cy.get(partInformation.partRevision).should('have.text', today);
    cy.get(partCanvas.documentTools.partRevisionSelect).should('have.text', today);

    //Navigate to parts home
    cy.navigateToPartList();
    //Delete part
    cy.deletePart();
  });

  it('Drawing and part revision can be the same', () => {
    cy.get(partInformation.partNumber).type(today, { delay: 1500 });
    cy.get(partInformation.partNumber).should('have.text', today);
    cy.wrap(today).as('partNumber');

    cy.get(partInformation.partRevision).should('have.text', today);
    cy.get(partCanvas.documentTools.partRevisionSelect).should('have.text', today);

    //Navigate to parts home
    cy.navigateToPartList();
    //Delete part
    cy.wait(5000);
    cy.get('@partNumber').then((partNumber) => {
      cy.get('.testing-part-number-header').each(($ele, index) => {
        if ($ele.text().includes(partNumber)) {
          cy.wrap(index).as('index');
        }
      });
      cy.get('@index').then((index) => {
        cy.get('[data-icon=delete]')
          .eq(index - 1)
          .trigger('click');
        cy.get(partList.deletePartContent).should('be.visible');
        cy.get(partList.deletePartConfirm).trigger('click');
        cy.wait(6000);
        cy.get('.testing-part-number-header')
          .eq(index - 1)
          .should('not.contain', partNumber);
      });
    });
    cy.deletePart();
  });

  it('User can create a new revision', () => {
    cy.addPartNumber();
    //Add new customer
    cy.get(partInformation.customer).click({ force: true }).get(partInformation.addCustomer).click();
    cy.on('window:confirm', (str) => {
      expect(str).to.contain('Create Customer');
    });

    let newCustomer = faker.random.alpha(8);
    cy.get(partInformation.newCustomerName).type(newCustomer).get(partInformation.createCustomerOkButton).click();
    cy.get(partInformation.partRevision).should('have.text', today);
    cy.get(partCanvas.documentTools.partRevisionSelect).should('have.text', today);
    cy.get(common.wizard.next).click();

    cy.get(partCanvas.documentTools.drawingRevisionSelect).click();
    cy.contains('Create new revision').click();
    const fileName = 'test.pdf';
    cy.get(fileUpload.fileUploadSaveBtn).should('be.visible');
    cy.get(fileUpload.fileUploadArea).attachFile({ filePath: `/${fileName}`, encoding: 'base64' }, { subjectType: 'drag-n-drop' });
    cy.get(fileUpload.fileUploadSaveBtn).should('be.visible').click();
    cy.wait(5000);
    cy.get('.btn-primary').click();
    cy.get(docInformation.Header).should('be.visible');
    cy.get(partInformation.partRevision).should('have.text', today);
    cy.get(partInformation.partRevision).type('A');
    cy.get(partInformation.partRevision).should('have.text', 'A' + today);
    //Navigate to parts home
    cy.navigateToPartList();
    //Delete part
    cy.deletePart();
  });

  it('Revision History is viewable', () => {
    cy.addPartNumber();
    cy.get(partInformation.partRevision).should('have.text', today);
    cy.get(partCanvas.documentTools.partRevisionSelect).should('have.text', today);

    cy.get(partInformation.customer).click({ force: true }).get(partInformation.addCustomer).click();
    cy.on('window:confirm', (str) => {
      expect(str).to.contain('Create Customer');
    });

    let newCustomer = faker.random.alpha(8);
    cy.get(partInformation.newCustomerName).type(newCustomer).get(partInformation.createCustomerOkButton).click();
    cy.get(partInformation.partRevision).should('have.text', today);
    cy.get(partCanvas.documentTools.partRevisionSelect).should('have.text', today);
    cy.get(common.wizard.next).click();

    cy.get(partCanvas.documentTools.drawingRevisionSelect).click();
    cy.contains('Create new revision').click();
    const fileName = 'test.pdf';
    cy.get(fileUpload.fileUploadSaveBtn).should('be.visible');
    cy.get(fileUpload.fileUploadArea).attachFile({ filePath: `/${fileName}`, encoding: 'base64' }, { subjectType: 'drag-n-drop' });
    cy.get(fileUpload.fileUploadSaveBtn).should('be.visible').click();
    cy.wait(5000);
    cy.get('.btn-primary').click();
    cy.get(docInformation.Header).should('be.visible');
    cy.get(partInformation.partRevision).should('have.text', today);
    cy.get(partInformation.partRevision).type('B');
    cy.get(partInformation.partRevision).should('have.text', 'B' + today);

    cy.get(partCanvas.documentTools.drawingRevisionSelect).click();
    cy.contains('View revision history').click();
    cy.contains('Revision History').should('be.visible');
    cy.get(common.RevisionDrawer.revisionDrawerClose).click();

    //Navigate to parts home
    cy.navigateToPartList();
    //Delete part
    cy.deletePart();
  });

  it('New revisions are shown in revision history', () => {
    cy.addPartNumber();
    cy.get(partInformation.partRevision).should('have.text', today);
    cy.get(partCanvas.documentTools.partRevisionSelect).should('have.text', today);

    cy.get(partInformation.customer).click({ force: true }).get(partInformation.addCustomer).click();
    cy.on('window:confirm', (str) => {
      expect(str).to.contain('Create Customer');
    });

    let newCustomer = faker.random.alpha(8);
    cy.get(partInformation.newCustomerName).type(newCustomer).get(partInformation.createCustomerOkButton).click();
    cy.get(partInformation.partRevision).should('have.text', today);
    cy.get(partCanvas.documentTools.partRevisionSelect).should('have.text', today);
    cy.get(common.wizard.next).click();

    cy.get(partCanvas.documentTools.drawingRevisionSelect).click();
    cy.contains('Create new revision').click();
    const fileName = 'test.pdf';
    cy.get(fileUpload.fileUploadSaveBtn).should('be.visible');
    cy.get(fileUpload.fileUploadArea).attachFile({ filePath: `/${fileName}`, encoding: 'base64' }, { subjectType: 'drag-n-drop' });
    cy.get(fileUpload.fileUploadSaveBtn).should('be.visible').click();
    cy.wait(5000);
    cy.get('.btn-primary').click();
    cy.get(docInformation.Header).should('be.visible');
    cy.get(partInformation.partRevision).should('have.text', today);
    cy.get(partInformation.partRevision).type('B');
    cy.get(partInformation.partRevision).should('have.text', 'B' + today);
    cy.get(partCanvas.documentTools.drawingRevisionSelect).click();
    cy.contains('View revision history').click();
    cy.contains('Revision History').should('be.visible');
    cy.reload();
    cy.get(partCanvas.documentTools.drawingRevisionSelect).click();
    cy.contains('View revision history').click();
    cy.contains('Revision History').should('be.visible');
    cy.contains('Rev. B' + today).should('be.visible'); //https://ideagen.atlassian.net/browse/IXC-3129
    cy.get(common.RevisionDrawer.revisionDrawerClose).click();
    //Navigate to parts home
    cy.navigateToPartList();
    //Delete part
    cy.deletePart();
  });

  // Cannot duplicate an existing customer
  it('Cannot duplicate an existing customer', () => {
    //Add new customer
    cy.get(partInformation.customer).click({ force: true }).get(partInformation.addCustomer).click();
    cy.on('window:confirm', (str) => {
      expect(str).to.contain('Create Customer');
    });

    let newCustomer = faker.random.alpha(8);
    // add new customer
    cy.get(partInformation.newCustomerName).type(newCustomer).get(partInformation.createCustomerOkButton).click();
    cy.get(partInformation.customer).click({ force: true }).get(partInformation.addCustomer).click();
    // add same new customer
    cy.get(partInformation.newCustomerName).type(newCustomer).get(partInformation.createCustomerOkButton);
    // same new customer being added errors with customer already exists
    cy.get('.ant-form-item-explain-error').should('be.visible').should('contain', 'Customer already exists');
  });
});
