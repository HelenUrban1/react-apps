// Tests Written //
//Add a balloon Style + Delete a balloon Style

// Tests To Do //

//DEFAULTS//
// Renders the default style  -- may already be covered in add balloon style test
// Prevents user from removing the default style

// CUSTOM BALLOON STYLE//
// Default custom balloon style modal fields exist
// User can create and delete a custom balloon style setting the balloon shape and balloon fill colour
// User can create and delete a custom balloon style setting the balloon font colour
// User can create and delete a custom balloon style setting the balloon border colour, border style and border width
// User is unable to edit Leader line when match border is selected
// User can create and delete a custom balloon style setting the balloon leader line colour, Leader line style and Leader line width when match with border is not selected
// Unable to delete a custom balloon syle if in use

//STYLE PRESETS// 
// Style preset save button is disable when no changes are made
// Style preset save button is enabled when a change is made
// User can create a new Style Preset
// User can update default styles preset - this will require return back to the orignal preset for future testing

import 'cypress-file-upload'
import balloonStyles from "../../../integration/locators/balloon-styles.json"

describe('Balloon Styles', () => {

  it('Add a balloon Style + Delete a balloon Style', () => {

    //Login and access balloon styles (Test set up)
    cy.loginAdmin()
    cy.addPart()
    cy.addPartNumber()
    cy.navigateToBalloonStyles()

    //Will be needed if and when auto ballooning gets turned on.
    // cy.get(partList.continueTitle, { timeout: 50000 }).should('contain', 'features').pause();
    // cy.get(partList.getStartedBtn).should('exist').click().pause();
    // cy.get(partInformation.partTitle).should('be.visible');

    //Check default balloon style is visible
    cy.get(balloonStyles.stylesPreset).should('be.visible')
    cy.contains('Default')

    //Check user can add a new balloon style
    cy.get(balloonStyles.addBalloonStyle).click()
    cy.get(balloonStyles.addBalloon).should('be.visible').click()
    cy.contains('Classification').click()
    cy.get(balloonStyles.classification).should('be.visible').click()

    // Delete a balloon Style
    cy.get(balloonStyles.deleteBalloonStyle).click()
    cy.get(balloonStyles.classification).should('not.exist')

    //Navigate to parts home
    cy.navigateToPartList()
    cy.wait(3000)

    //Delete part
    .deletePart()

  })
})
