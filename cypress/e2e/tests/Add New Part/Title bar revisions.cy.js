// Tests Written //
// Add a new drawing 
// View part revision history

// Tests To Do //
// View document revision history
// Create new part revision 
// Create new document revision

const fileName1 = 'test2.pdf'

import fileUpload from "../../../integration/locators/fileUpload.json"
import partCanvas from "../../../integration/locators/part-canvas-section.json"

import 'cypress-file-upload'

var today = new Date().toISOString().slice(0, 10)

describe('New Part title bar', () => {

  it('Add a new drawing', () => {
    //Test set up
    cy.loginAdmin()
    //add part
    cy.addPart()
    cy.addPartNumber()
    cy.wait(3000)

    cy.get(partCanvas.documentTools.partDrawingSelect).invoke('removeAttr', 'unselectable').click()
      .get(partCanvas.documentTools.addDrawingButton).click()
      .get(fileUpload.fileUploadArea).attachFile({ filePath: `/${fileName1}`, encoding: "base64" }, { subjectType: 'drag-n-drop' })
      .wait(5000)
      .get(fileUpload.fileUploadArea).should('be.visible')
      .get(fileUpload.fileUploadSaveBtn).should('be.visible').click({ force: true })
      .wait(5000)

      //check that the new drawing is added//
      .get(partCanvas.documentTools.partDrawingSelect).click()
    cy.get(partCanvas.documentTools.partDrawingDropdown).should('contain', fileName1)

    //Test clean up
    //Navigate to parts home
    cy.navigateToPartList()
    cy.wait(3000)
    //Delete part
    cy.deletePart()
  })

    it('View part revision history', () => {

    //Test set up
    cy.loginAdmin()
    //add part
    cy.addPart()
    cy.wait(2000)
    cy.addPartNumber()
   
    cy.get(partCanvas.documentTools.partRevisionSelect).click()
      .get(partCanvas.documentTools.drawingRevisionHistoryButton).click()
      .get('h1').should('have.text', 'Revision History')
      .get('h2').eq(0).should('have.text', 'test')
      .get('h3').eq(0).should('have.text', 'Rev. ' + today)
      .get('h2').eq(1).should('have.text', 'test.pdf')
      .get('h3').eq(1).should('have.text', 'Rev. ' + today)
      .get(partCanvas.revisionHistory.revisionHistoryCloseDrawer).click()

    //Test clean up
    //Navigate to parts home
    cy.navigateToPartList()
    cy.wait(3000)
    //Delete part
    cy.deletePart()
  })
})