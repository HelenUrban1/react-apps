import { faker } from '@faker-js/faker'
import 'cypress-file-upload'
import partGrid from "../../../integration/locators/part-grid.json"

// Tests Written //
// Grid Oprtions// 
// Grid Zones can be toggled on and off
// Update the column and row numbers in the grid
// Update the grid and label colour

// UPDATE TO EXISITING TESTS//
// Create tests that check the Gridlines are displayed and removed when the untoggled/toggled for document and for page
// Grid and label colours once updated are updated on the grid itself
// Should add rows when the row labels are changed


// Tests To Do //

//DEFAULTS//
// Should show an alert about line visibility that can be dismissed
// Should start with a default grid


//GRID OPTIONS// 
// Should be able to turn off the grid per document when a multipage document is being used
// Should be able to turn off the grid per page when a multipage document is being used
// Apply to all pages when a multipage document is being used

// GRID AJUSTMENTS //
// Should allow sides to adjust grid width and height using mouse movement
// Should allow interior lines to be adjusted using mouse movement
// Should adjust grid when page is rotated 
// Should skip letters and numbers when skip labels are provided

describe('GRID OPTIONS', () => {
 
  it('Grid Zones can be toggled on and off', () => {
     //Login
    cy.loginAdmin()
    //add part
    .addPart()
    .addPartNumber()
    .navigateToGridZones()

    //Untoggle and toggle grid document //MORE TESTS NEEDED WHEN IFRAME SORTED TO CHECK THE GRIDLINES ARE REMOVED / RESTORED
    cy.get(partGrid.gridSwitchDocToggle).should('be.visible').and('have.attr', 'aria-checked', 'true')
    cy.get(partGrid.gridSwitchDocToggle).click({ force: true })
    cy.get(partGrid.gridSwitchDocToggle).should('be.visible').and('have.attr', 'aria-checked', 'false')
    cy.get(partGrid.gridSwitchDocToggle).click({ force: true })
    cy.get(partGrid.gridSwitchDocToggle).should('be.visible').and('have.attr', 'aria-checked', 'true')

    //Toggle grid zones// //MORE TESTS NEEDED WHEN IFRAME SORTED TO CHECK THE GRIDLINES ARE REMOVED / RESTORED on a page basis
    cy.get(partGrid.gridSwitchPageToggle).should('be.visible').and('have.attr', 'aria-checked', 'true')
    cy.get(partGrid.gridSwitchPageToggle).click({ force: true })
    cy.get(partGrid.gridSwitchPageToggle).should('be.visible').and('have.attr', 'aria-checked', 'false')
    cy.get(partGrid.gridSwitchPageToggle).click({ force: true })
    cy.get(partGrid.gridSwitchPageToggle).should('be.visible').and('have.attr', 'aria-checked', 'true')
    cy.wait(3000)

    //Navigate to parts home
    cy.navigateToPartList()
    cy.wait(3000)
    //Delete part
    .deletePart()
  })


  it('Update the column and row numbers in the grid', () => {
     //Login
    cy.loginAdmin()
    //add part
    .addPart()
    .addPartNumber()
    .navigateToGridZones()

    // Check default column and row numbers in the grid are correct
    cy.get(partGrid.colLeft).should('exist').and("have.value", "4")
    cy.get(partGrid.colRight).should('exist').and("have.value", "1")
    cy.get(partGrid.rowTop).should('exist').and("have.value", "B")
    cy.get(partGrid.rowBot).should('exist').and("have.value", "A")
    cy.get(partGrid.ColRowsToSkip).should('be.empty')

    //update column and row numbers

    const colLeft = faker.random.numeric()
    const colRight = faker.random.numeric()

    cy.wait(5000)
    cy.get(partGrid.colLeft).should('exist').click({ timeout: 2000 }).type(colLeft, { delay: 1500 })
    cy.get(partGrid.colRight).should('exist').click({ timeout: 2000 }).type(colRight, { delay: 1500 })
    cy.get(partGrid.rowTop).should('exist').click({ timeout: 2000 }).type("A", { delay: 1500 })
    cy.get(partGrid.rowBot).should('exist').click({ timeout: 2000 }).type("G", { delay: 1500 })
    cy.get(partGrid.ColRowsToSkip).type("B,3")

    //Check values updated
    cy.get(partGrid.colLeft).should("have.value", colLeft)
    cy.get(partGrid.colRight).should("have.value", colRight)
    cy.get(partGrid.rowTop).should("have.value", "A")
    cy.get(partGrid.rowBot).should("have.value", "G")
    cy.get(partGrid.ColRowsToSkip).should("have.value", "B,3")

    //Navigate to parts home
    cy.navigateToPartList()
    cy.wait(3000)
    //Delete part
    .deletePart()
  })

  it('Update the grid and label colour', () => {
    //Login
    cy.loginAdmin()
    //add part
    .addPart()
    .addPartNumber()
    .navigateToGridZones()

    //confirm current label colours and update
    cy.get(partGrid.labelColor).should('have.css', 'background-color', 'rgb(150, 60, 189)').click({ force: true })
    cy.get(partGrid.ColourRed).click({ force: true })
    cy.get(partGrid.labelColor).should('have.css', 'background-color', 'rgb(208, 2, 27)').click({ timeout: 2000 })
    cy.get(partGrid.gridColor).should('have.css', 'background-color', 'rgb(150, 60, 189)').click({ force: true })
    cy.get(partGrid.ColourRed).last().click({ force: true })
    cy.get(partGrid.gridColor).should('have.css', 'background-color', 'rgb(208, 2, 27)')

    //Navigate to parts home
    cy.navigateToPartList()
    cy.wait(3000)

    //Delete part
    .deletePart()
  })

})
