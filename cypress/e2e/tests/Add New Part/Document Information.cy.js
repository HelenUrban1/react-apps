// Tests Written //

//Displays appropriate input fields and correct defaults
//Allowed characters are accepted and displayed in title bar
//Emoji's are not displayed in fields or in title bar

// Tests To Do //

//DOCUMENT TYPE
// Allows user to select different document Type
// Where there are multiple docs user is able to select primary document

//DRAWING NUMBER//
//Drawing number is displayed in the title bar

//DRAWING NAME//
//Drawing name should be pre-populated with document name
//Drawing name is displayed in the title bar

//REVISION//
//Should be pre-populated with todays date
//Revision is displayed in title bar
//Drawing and part revision can be the same
//User can create a new revision -- May already be covered in Document Information
//Revision History is viewable -- May already be covered in Document Information
//New revisions are shown in revision history -- May already be covered in Document Information

//PAGE NAMES//
//Page names cannot be the same
//Number of pages displayed should match number of pages in the document

const fileName = 'test.pdf'
import partCanvas from "../../../integration/locators/part-canvas-section.json"
import docInformation from "../../../integration/locators/part-docs-info-section.json"

import 'cypress-file-upload'
require('cypress-plugin-tab')
var todaysDate = new Date().toISOString().slice(0, 10)

describe('Document Information functionality ', () => {
    
    it('Displays appropriate input fields and correct defaults', () => {
    //Test set up
    cy.loginAdmin()
    cy.addPart()
    cy.addPartNumber()
    cy.navigateToDocInfo()

    //Page contents check
    cy.get(docInformation.documentType).should('not.be.empty').and('contain', 'Drawing')
    cy.get(docInformation.primarySwitch).should('be.visible').and('have.attr', 'aria-checked', 'true')
    cy.get(docInformation.drawingNumber).should('contain', '')
    cy.get(docInformation.drawingRevision).should('contain', todaysDate)
    cy.get(docInformation.drawingName).should('contain', fileName)
    cy.get(docInformation.pageOneInput).should('exist').and('contain', 'Page 1')
    cy.get(docInformation.pageTwoInput).should('exist').and('contain', 'Page 2')
       
    //check the toolbar on the top//
    cy.get('@partNumber').then(partNumber => {
    cy.get(partCanvas.documentTools.partNumber).should('contain', partNumber)})
    cy.get(partCanvas.documentTools.partRevisionSelect).should('contain', todaysDate)
    cy.get(partCanvas.documentTools.drawingRevisionSelect).should('contain', todaysDate)

    // Test Clean up
    cy.navigateToPartList()
    cy.wait(3000)
    cy.deletePart()

    })

    it('Allowed characters are accepted and displayed in title bar', () => {
    const validCharacters = 'AGS boz !#$%<>=?@[]&*+,-.:;"/{{}}\''
    const validCharactersConfirmation = 'AGS boz !#$%<>=?@[]&*+,-.:;"/{}\''

    //Test set up
    cy.loginAdmin()
    cy.addPart()
    cy.addPartNumber()
    cy.navigateToDocInfo()

    //Enter valid charaters into drawing number, drawing revison, drawing name, page input 1 & 2
    cy.get(docInformation.drawingNumber).clear().type(validCharacters)
    cy.get(docInformation.drawingRevision).clear().type(validCharacters)
    cy.get(docInformation.drawingName).clear().type(validCharacters)
    cy.get(docInformation.pageOneInput).clear().type(validCharacters)
    cy.get(docInformation.pageTwoInput).clear().type(validCharacters)

    //Check valid characters are displayed in Part Name, Part Number and Revison fields 
    cy.get(docInformation.drawingNumber).should('have.text',validCharactersConfirmation)
    cy.get(docInformation.drawingRevision).should('have.text',validCharactersConfirmation)
    cy.get(docInformation.drawingName).should('have.text',validCharactersConfirmation)
    cy.get(docInformation.pageOneInput).should('have.text',validCharactersConfirmation)
    cy.get(docInformation.pageTwoInput).should('have.text',validCharactersConfirmation)

    //Validate Drawing number, part and draeing Revison are correct in the title bar
    cy.get('@partNumber').then(partNumber => {
    cy.get(partCanvas.documentTools.partNumber).should('contain', partNumber)})
    cy.get(partCanvas.documentTools.partRevisionSelect).should('contain', todaysDate)
    cy.get(partCanvas.documentTools.partDrawingSelect).should('have.text', validCharactersConfirmation)
    cy.get(partCanvas.documentTools.drawingRevisionSelect).should('have.text', validCharactersConfirmation)

    // Test Clean up
    cy.navigateToPartList()
    cy.wait(3000)
    cy.deletePart()
    })


    it("Emoji's are not displayed in fields or in title bar" , () => {
    //Test set up
    cy.loginAdmin()
    cy.addPart()
    cy.addPartNumber()
    cy.navigateToDocInfo()

    const Emoji = '😄'

    //Enter emoji image into fields
    cy.wait(8000)
    cy.get(docInformation.drawingNumber).type(Emoji,{ delay: 1500 })
    cy.get(docInformation.drawingRevision).clear().type(Emoji,{ delay: 1500 })
    cy.get(docInformation.drawingName).clear().type(Emoji,{ delay: 1500 })
    cy.get(docInformation.pageOneInput).clear().type(Emoji)
    cy.get(docInformation.pageTwoInput).clear().type(Emoji)
    cy.get('#documentAnalysis').click()

    //Check emoji is not displayed
    cy.get(docInformation.drawingNumber).should('have.text','')
    cy.get(docInformation.drawingRevision).should('have.text','')
    cy.get(docInformation.drawingName).should('have.text','')
    cy.get(docInformation.pageOneInput).should('have.text','')
    cy.get(docInformation.pageTwoInput).should('have.text','')

    //Check emoji not displayed in title bar
    cy.get('@partNumber').then(partNumber => {
    cy.get(partCanvas.documentTools.partNumber).should('contain', partNumber)})
    cy.get(partCanvas.documentTools.partRevisionSelect).should('contain', todaysDate)
    cy.get(partCanvas.documentTools.partDrawingSelect).should('have.text', '-')
    cy.get(partCanvas.documentTools.drawingRevisionSelect).should('have.text', '')

    // Test Clean up
    cy.navigateToPartList()
    cy.wait(3000)
    cy.deletePart()
})

})
