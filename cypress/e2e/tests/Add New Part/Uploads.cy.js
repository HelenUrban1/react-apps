// Test Written //
// Upload Part //
// Create a new part and Delete it

// AUTOBALLOON //
// Alert me button keeps user on parts list page - Currently commented out as this feature is disabled

// Tests To Do //

// Modal //
// shows upload modal when clicking new part button
// shows drag and drop on dragove of PDF onto part list
// clicking cancel does not create a part
// clicking ok is disabled without any uploads
// allows up to 10 files
// adding more than 10 files disables Next button and displays error
// adjusts limit when adding more than 10 files and deleting some
// When 10 files are reached the user is not able to add any further files

// AUTOBALLOON // - BLOCKED auto ballooning feature has been removed
// Shows autoballoon wait notice if a file was autoballooned
// continues to part creation if no files were autoballooned 
import 'cypress-file-upload'

describe('Upload New Part', () => {

    it('Create a new part and Delete it', () => {
        cy.loginAdmin()

        //add part
        cy.addPart()
        cy.addPartNumber()
        //Navigate to parts home
        cy.navigateToPartList()
        cy.wait(6000)
        //Delete part
        cy.deletePart()
    })
})
// describe('Autoballoon', () => {

// it('Alert me button keeps user on parts list page', () => {
//     cy.loginAdmin()
//     cy.get('h1').should('contain', 'Parts')
//     cy.get(partList.newPartBtn).should('be.visible').click()
//     cy.get(fileUpload.fileUploadArea).attachFile(fileName, { subjectType: 'drag-n-drop' })
//     cy.get(fileUpload.file).should('be.visible')
//     cy.get(fileUpload.fileUploadSaveBtn).should('be.visible').click()

    // Use full post turning auto balloon on.
    // cy.get(partList.continueTitle).should('contain', 'features')
    // cy.get(partList.alertMeBtn).should('exist').click()
    // cy.get(partList.newPartBtn).should('exist')
// })

// })