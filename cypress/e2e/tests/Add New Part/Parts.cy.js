import partInformation from '../../../integration/locators/part-information-section.json';
import common from '../../../integration/locators/common.json';
import { faker } from '@faker-js/faker';
import 'cypress-file-upload';
import fileUpload from '../../../integration/locators/fileUpload.json';

require('cypress-plugin-tab');
const fileName = 'IX Multi Tool A5 Tahoma 2.pdf';
const fileName2 = 'test1.pdf';

// Tests Written //

// Search the part by part name
// Delete the first part on the page
// Verify fields on side window wrapper
// Attach and delete part document
// Attach and delete attached document
// Create report
// Delete part

describe('Parts Page', () => {
  beforeEach(() => {
    cy.loginAdmin();
  });

  it('search the part by part name', () => {
    //add part
    cy.addPart();
    cy.get(partInformation.partTitle).should('contain', 'Part Information');
    cy.addPartNumber();

    // Check part name = document file name
    cy.get(partInformation.partName).click();
    cy.get(partInformation.partName).clear();
    cy.get(partInformation.partName).type('IX Multi Tool A5 Tahoma 2').should('exist').and('contain', fileName.substring(0, 25));

    //Navigate to parts home
    cy.navigateToPartList();

    //search part by part name
    cy.get(common.Search.searchInput).type('IX Multi Tool A5 Tahoma 2');
    cy.get(common.Search.searchInput).should('have.value', 'IX Multi Tool A5 Tahoma 2');
    cy.get(common.PartTable.partNameTableRow1).should('contain', 'IX Multi Tool A5 Tahoma 2');

    //Delete part
    cy.deletePart();
  });

  it('delete the first part on the page', () => {
    cy.addPart();
    cy.get(partInformation.partTitle).should('contain', 'Part Information');
    cy.addPartNumber();

    // Check part name = document file name
    cy.get(partInformation.partName).click();
    cy.get(partInformation.partName).clear();
    cy.get(partInformation.partName).type('IX Multi Tool A5 Tahoma 2').should('exist').and('contain', fileName.substring(0, 25));

    //Navigate to parts home
    cy.navigateToPartList();
    cy.get(common.Buttons.delete).first().click();
    cy.get(common.Buttons.popupDelete).click({force: true});
  });

  it('verify fields on side window wrapper', () => {
    //add part
    cy.addPart();
    cy.get(partInformation.partTitle).should('contain', 'Part Information');
    cy.addPartNumber();

    // Check part name = document file name
    cy.get(partInformation.partName).click();
    cy.get(partInformation.partName).clear();
    cy.get(partInformation.partName).type('IX Multi Tool A5 Tahoma 2');
    cy.should('exist');
    cy.and('contain', fileName.substring(0, 25));

    //Navigate to parts home
    cy.navigateToPartList();
    cy.get(common.PartTable.partNameTableRow1).click();
    cy.get(common.PartDrawer.form).should('contain', 'Part Number');
    cy.get(common.PartDrawer.form).should('contain', 'Part Name');
    cy.get(common.PartDrawer.form).should('contain', 'Part Revision');
    cy.get(common.PartDrawer.form).should('contain', 'Customer');
    cy.get(common.PartDrawer.form).should('contain', 'Status');
    cy.get(common.PartDrawer.form).should('contain', 'saved reports');
    cy.get(common.PartDrawer.form).should('contain', 'part documents');
    //type part number
    cy.get('.ant-input').eq(1).clear().type('463');
    //type part name
    cy.get('.ant-input').eq(2).clear().type('IX Multi Tool A5 Tahoma 2');
    //click part revision
    cy.get('.ant-select-selection-item').eq(0).click();
    cy.get('.ant-select-item-option-content').eq(0).click();
    cy.wait(2000);
    cy.get('.ant-select-selection-item').eq(1).click();

    //add new customer option
    cy.get(common.PartDrawer.customerSelectField).should('exist').click();
    cy.get('#add-new-option').click();

    cy.get('.ant-input').eq(3);
    cy.on('window:confirm', (str) => {
      expect(str).to.contain('Create Customer');

      cy.deletePart();
    });

    let newCustomer = faker.random.alpha(8);
    cy.get('.ant-input').eq(3).type(newCustomer);
    cy.get('.ant-modal-footer > div > .ant-btn').click();
    cy.get('.part-status > .form-input > .ant-select > .ant-select-selector > .ant-select-selection-item').eq(0).click();
    cy.contains('Unverified');
    cy.contains('Verified');
    cy.contains('Ready for Review');
  });

  it('attach and delete part document', () => {
    //add part
    cy.addPart();
    cy.get(partInformation.partTitle).should('contain', 'Part Information');
    cy.addPartNumber();

    // Check part name = document file name
    cy.get(partInformation.partName).click();
    cy.get(partInformation.partName).clear();
    cy.get(partInformation.partName).type('IX Multi Tool A5 Tahoma 2').should('exist').and('contain', fileName.substring(0, 25));

    //Navigate to parts home
    cy.navigateToPartList();
    // Open part drawer
    cy.get(common.PartTable.partNameTableRow1).first().click();
    cy.wait(2000);
    cy.get(common.PartDrawer.addPartDocument).eq(0).click();
    cy.get(fileUpload.fileUploadSaveBtn).should('be.visible');
    cy.wait(2000);
    cy.get(common.PartDrawer.partDocumentUpload).attachFile({ filePath: `/${fileName2}`, encoding: 'base64' }, { subjectType: 'drag-n-drop' });
    cy.wait(3000);
    cy.get(fileUpload.fileUploadSaveBtn).should('be.visible').click();
    cy.wait(2000);
    cy.get(common.PartDrawer.documentMiniList).should('contain', fileName2);
    cy.get('.icon-button.document-delete').click();
    cy.get('#popconfirm-btn-ok').click({force: true});
  });

  it('attach and delete attached document', () => {
    //add part
    cy.addPart();
    cy.get(partInformation.partTitle).should('contain', 'Part Information');
    cy.addPartNumber();

    // Check part name = document file name
    cy.get(partInformation.partName).click();
    cy.get(partInformation.partName).clear();
    cy.get(partInformation.partName).type('IX Multi Tool A5 Tahoma 2');
    cy.should('exist');
    cy.and('contain', fileName.substring(0, 25));

    //Navigate to parts home
    cy.navigateToPartList();
    // Open part drawer
    cy.get(common.PartTable.partNameTableRow1).first().click();
    cy.wait(2000);
    cy.get(common.PartDrawer.documentMiniListNew).click();
    cy.get(fileUpload.fileUploadSaveBtn).should('be.visible');
    cy.wait(2000);
    cy.get(common.PartDrawer.partDocumentUpload).attachFile({ filePath: `/${fileName2}`, encoding: 'base64' }, { subjectType: 'drag-n-drop' });
    cy.wait(3000);
    cy.get(fileUpload.fileUploadSaveBtn).should('be.visible').click();
    cy.wait(2000);
    cy.get(common.PartDrawer.documentMiniListCard).should('contain', fileName2);
    cy.get(common.PartDrawer.documentDelete).click();
    cy.get(common.Buttons.popupDelete).click({force: true});
  });

  it('create a report', () => {
    //add part
    cy.addPart();
    cy.get(partInformation.partTitle).should('contain', 'Part Information');
    cy.addPartNumber();

    // Check part name = document file name
    cy.get(partInformation.partName).click();
    cy.get(partInformation.partName).clear();
    cy.get(partInformation.partName).type('IX Multi Tool A5 Tahoma 2').should('exist').and('contain', fileName.substring(0, 25));

    //Navigate to parts home
    cy.navigateToPartList();
    cy.get(common.PartTable.partNameTableRow1).click();
    cy.wait(2000);
    //create report
    cy.get(common.PartDrawer.partDrawerButton).eq(1).click();
    cy.get(common.Reports.reportHeader).should('contain', 'AS9102C');
    cy.get(common.Reports.reportButtonAs9102c).click();
    cy.get(common.Reports.reportTitle).should('exist').should('be.visible').should('contain', 'AS9102C');
    //exit report
    cy.get(common.Reports.exitReportButton).click();
    cy.get(common.Reports.reportMiniListCard).should('exist').should('be.visible');
  });

  it('Delete part', () => {
    //add part
    cy.addPart();
    cy.get(partInformation.partTitle).should('contain', 'Part Information');
    cy.addPartNumber();

    // Check part name = document file name
    cy.get(partInformation.partName).click();
    cy.get(partInformation.partName).clear();
    cy.get(partInformation.partName).type('IX Multi Tool A5 Tahoma 2').should('exist').and('contain', fileName.substring(0, 25));

    //Navigate to parts home
    cy.navigateToPartList();

    //Delete part
    cy.get(common.PartTable.partNameTableRow1).first().click();
    cy.get(common.Buttons.delete).first().click();
    cy.get(common.Buttons.popupDelete).click({force: true});
  });
});
