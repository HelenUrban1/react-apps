import 'cypress-file-upload'
require('cypress-plugin-tab')

describe('Cancel Dialog Box - Functions', () => {

    beforeEach(() => {
        cy.loginAdmin()
        
        //add part
        .addPart()
        .addPartNumber()
    })

    it('Cancel Dialog Box - Functions', () => {
        cy.checkCancelDialogBoxFunctions()
    })
})