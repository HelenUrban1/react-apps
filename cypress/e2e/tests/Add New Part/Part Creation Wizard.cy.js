// Tests written //
//Cancel Part creation from wizard - No
//Cancel Part creation from wizard – yes – check part is deleted after yes on part home
//Cancel part creation no longer exists after advancing
//Allows navigation forward and backwards through wizard to the correct pages – retains information

import common from '../../../integration/locators/common.json';
import partInformation from '../../../integration/locators/part-information-section.json';
import partList from '../../../integration/locators/part-list.json';
import 'cypress-file-upload';

describe('Part Creation Wizard', () => {
  it('Cancel Part creation from wizard - No', () => {
    cy.loginAdmin()
      //add part
      .addPart()
      .addPartNumber();
    cy.get(common.wizard.cancel).click();
    cy.get(partInformation.cancelModal).should('contain', 'This Part will be deleted. Continue?');
    cy.get(common.wizard.yes).should('contain', 'Yes');
    cy.get(common.wizard.no).should('contain', 'No').click();
    cy.get(partInformation.partTitle).should('contain', 'Part Information');
    //Navigate to parts
    cy.navigateToPartList();

    //Delete part
    cy.deletePart();
    cy.get(partList.parttable).find(partList.firstRow).should('not.contain', '@partNumber');
  });

  it('Cancel Part creation from wizard - yes - check part is deleted after yes on part home', () => {
    cy.loginAdmin()
      //add part
      .addPart()
      .addPartNumber();
    cy.get(partInformation.partNumber).invoke('text').as('partNumber');
    cy.get(common.wizard.cancel).click();
    cy.get(partInformation.cancelModal).should('contain', 'This Part will be deleted. Continue?');
    cy.get(common.wizard.no).should('contain', 'No');
    cy.get(common.wizard.yes).should('contain', 'Yes').click();

    //does not contain part number
    cy.get('h1').should('contain', 'Parts');
    cy.get(partList.parttable).find(partList.firstRow).should('not.contain', '@partNumber');
  });

  it('Cancel part creation no longer exists after advancing', () => {
    cy.loginAdmin()
      //add part
      .addPart()
      .addPartNumber();
    cy.get(partInformation.partNumber).invoke('text').as('partNumber');
    cy.get(common.wizard.next).click();
    cy.get(common.wizard.back).click();
    cy.get(common.wizard.cancel).click();
    cy.get(common.wizard.yes).should('contain', 'Yes').click();
    cy.get('h1').should('contain', 'Parts');
    cy.get(partList.parttable).find(partList.firstRow).should('not.contain', '@partNumber');
  });

  it('Allows navigation forward and backwards through wizard to the correct pages – retains information', () => {
    cy.loginAdmin()
      //add part
      .addPart()
      .addPartNumber();
    cy.get(partInformation.partNumber).invoke('text').as('partNumber');
    cy.get('@partNumber').then((partNumber) => {
      cy.get(common.wizard.next).click();
      cy.get(partInformation.drawingNumber).type('123').invoke('text').as('drawingNumber');
      cy.get('@drawingNumber').then((drawingNumber) => {
        cy.get(partInformation.documentAnalysis).click();
        cy.get(partInformation.drawingNumber).invoke('text').as('drawingNumber');
        cy.wait(1000);
        cy.get(common.wizard.next).click();
        cy.get(common.wizard.back).click();
        cy.get(partInformation.drawingNumber).should('have.value', drawingNumber);
        cy.get(common.wizard.back).click();
        cy.get(partInformation.partNumber).should('have.value', partNumber);
        cy.get(common.wizard.next).click();
        cy.get(partInformation.drawingNumber).should('have.value', drawingNumber);
        cy.get(common.wizard.back).click();
        cy.get(partInformation.partNumber).should('have.value', partNumber);
      });
    });
  });
});
