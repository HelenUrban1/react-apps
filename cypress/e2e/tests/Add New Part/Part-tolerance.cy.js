import partTolerance from "../../../integration/locators/part-tolerance.json"
import { faker } from '@faker-js/faker'
import 'cypress-file-upload'

require('cypress-plugin-tab');

let lenBefore

describe('IXC-2724 - Part Wizard Tolerance Panel ', () => {
  it('IXC-2724 - Update System of Measurement', () => {

    //Test set up
    cy.loginAdmin()
    cy.addPart()
    cy.addPartNumber()
    cy.navigateToTolerance()

    //Check tolerance wizard for defaults
    cy.get(partTolerance.toleranceTitle).should('have.text', '3. Default Tolerances')
    cy.get(partTolerance.tolerancePreset).should('have.text', 'Default')

    //System of measurement
    cy.get(partTolerance.radioButtonTitle).should('exist').and('have.text', 'System of Measurement')
    cy.get(partTolerance.metricMeasurement).should('exist').and('not.be.checked').and('have.value', 'Metric')
    cy.get(partTolerance.imperialMeasurement).should('exist').and('be.checked').and('have.value', 'Imperial')
    cy.wait(2000)
    //change system measurement
    cy.get(partTolerance.metricMeasurement).click()
    cy.get(partTolerance.metricMeasurement).should('exist').and('be.checked').and('have.value', 'Metric')
    cy.get(partTolerance.imperialMeasurement).should('exist').and('not.be.checked').and('have.value', 'Imperial')

    //Test clean up
    //Navigate to parts home
    cy.navigateToPartList()
    
    //Delete part
    cy.deletePart()
  })
})


describe('IXC-2724 - Linear Table', () => {


  it('IXC-2724 - Default Linear tolerance values are displayed', () => {
    //Test set up
    cy.loginAdmin()
    cy.addPart()
    cy.addPartNumber()
    cy.navigateToTolerance()

    //Check tolerance wizard for defaults
    cy.get(partTolerance.toleranceTitle).should('have.text', '3. Default Tolerances')
    cy.get(partTolerance.tolerancePreset).should('have.text', 'Default')

    //System of measurement
    cy.get(partTolerance.radioButtonTitle).should('exist').and('have.text', 'System of Measurement')
    cy.get(partTolerance.metricMeasurement).should('exist').and('not.be.checked')
    cy.get(partTolerance.metricMeasurement).should('exist').and('have.value', 'Metric')
    cy.get(partTolerance.imperialMeasurement).should('exist').and('be.checked')
    cy.get(partTolerance.imperialMeasurement).should('exist').and('have.value', 'Imperial')

    //Linear section
    cy.get(partTolerance.linear).should('exist').and('have.text', 'Linear').should('have.attr', 'aria-expanded', 'true')

    //apply by
    cy.get(partTolerance.precisionRadio).should('exist').and('be.checked').and('have.value', 'PrecisionTolerance')
    cy.get(partTolerance.rangeRadio).should('exist').and('not.be.checked').and('have.value', 'RangeTolerance')

    //Linear Table//

    //Row 1 default
    cy.get(partTolerance.linearTable.row1.precisionvalue).title('X')
    cy.get(partTolerance.linearTable.row1.plus).should('have.value', '1')
    cy.get(partTolerance.linearTable.row1.minus).should('have.value', '-1')

    //Row 2 default
    cy.get(partTolerance.linearTable.row2.precisionvalue).title('X.X')
    cy.get(partTolerance.linearTable.row2.plus).should('have.value', '0.1')
    cy.get(partTolerance.linearTable.row2.minus).should('have.value', '-0.1')

    //Row 3 default
    cy.get(partTolerance.linearTable.row3.precisionvalue).title('X.XX')
    cy.get(partTolerance.linearTable.row3.plus).should('have.value', '0.01')
    cy.get(partTolerance.linearTable.row3.minus).should('have.value', '-0.01')

    //Row 4 default
    cy.get(partTolerance.linearTable.row4.precisionvalue).title('X.XXX')
    cy.get(partTolerance.linearTable.row4.plus).should('have.value', '0.001')
    cy.get(partTolerance.linearTable.row4.minus).should('have.value', '-0.001')

    //Row 5 default
    cy.get(partTolerance.linearTable.row5.precisionvalue).title('X.XXXX')
    cy.get(partTolerance.linearTable.row5.plus).should('have.value', '0.0001')
    cy.get(partTolerance.linearTable.row5.minus).should('have.value', '-0.0001')


    //User has option to add a tolerance
    cy.get(partTolerance.addTolerance).should('exist')

    //User has unit option
    cy.get(partTolerance.linearTable.unit).should('exist')
    cy.get(partTolerance.linearTable.unitSelector).should('exist')

    //apply all and update x feature
    cy.get(partTolerance.applyAll).should('exist')
    cy.get(partTolerance.updateFeature).should('exist')

    //Test clean up
    //Navigate to parts home
    cy.navigateToPartList()
    
    //Delete part
    cy.deletePart()

  })

  it('IXC-2724 - Update Linear tolerance precision values', () => {
    //Test set up
    cy.loginAdmin()
    cy.addPart()
    cy.addPartNumber()
    cy.navigateToTolerance()

    //Modify 1st row in linear table
    cy.get(partTolerance.linearTable.row1.precisionvalue).click()
    cy.get(partTolerance.precisionDropdown).contains('X.XXXXXX').click()
    cy.get(partTolerance.linearTable.row1.precisionvalue).title('X.XXXXXX')

    //update 1st row plus
    cy.wait(3000)
    let randomplustopcolumn = faker.random.numeric(1)
    cy.get(partTolerance.linearTable.row1.plus).type(randomplustopcolumn)
    cy.get(partTolerance.linearTable.row1.plus).should('have.value', randomplustopcolumn)

    //update 1st row minus
    let randomminusTolcolumn = -faker.random.numeric(1)
    cy.get(partTolerance.linearTable.row1.minus).type(randomminusTolcolumn)

    cy.get(partTolerance.linearTable.row1.minus).should('have.value', randomminusTolcolumn)

    //Test clean up
    //Navigate to parts home
    cy.navigateToPartList()
    
    //Delete part
    cy.deletePart()

  })

  it('IXC-2724 - Modify Linear tolerance unit type', () => {
    //Test set up
    cy.loginAdmin()
    cy.addPart()
    cy.addPartNumber()
    cy.navigateToTolerance()

    //User has unit option
    cy.wait(3000)
    cy.get(partTolerance.linearTable.unit).should('exist')
    cy.get(partTolerance.linearTable.unitSelector).should('exist').should('have.text', 'in').click()

    //amend unit type
    cy.get(partTolerance.linearTable.milUnit).click()
    cy.get(partTolerance.linearTable.unitSelector).should('not.have.text', 'in')
    cy.get(partTolerance.linearTable.unitSelector).should('have.text', 'mil')

    //Test clean up
    //Navigate to parts home
    cy.navigateToPartList()
    
    //Delete part
    cy.deletePart()

  })

  it('IXC-2724 - Duplicate linear tolerance validation error', () => {
    //Test set up
    cy.loginAdmin()
    cy.addPart()
    cy.addPartNumber()
    cy.navigateToTolerance()

    //Modify 1st row in linear table
    cy.get(partTolerance.linearTable.row1.precisionvalue).click()
    cy.wait(2000)
    cy.get(partTolerance.precisionDropdown).should('be.visible').contains('X.XX').click()
    cy.wait(2000)
    cy.get(partTolerance.linearTable.row1.precisionvalue).title('X.XX')
    cy.wait(2000)
    //check validation-error (red outline) is displayed for duplicate precision value
    cy.get(partTolerance.linearTable.row1.precisionvalue).find('div').should('have.class', partTolerance.validationError)
    cy.get(partTolerance.linearTable.row3.precisionvalue).find('div').should('have.class', partTolerance.validationError)

    //Test clean up
    //Navigate to parts home
    cy.navigateToPartList()

    //Delete part
    cy.deletePart()

  })

  it('IXC-2724 - Add and delete linear precision tolerance', () => {
    //Test set up
    cy.loginAdmin()
    cy.addPart()
    cy.addPartNumber()
    cy.navigateToTolerance()

    //Add new tolerance
    let pluscolumn = faker.random.numeric(1)
    let minuscolumn = faker.random.numeric(1)
    cy.get(partTolerance.addTolerance).click()
    cy.wait(8000)

    cy.get(partTolerance.linearTable.precisionTopColumn).last().click({ timeout: 3000 })
    cy.get(partTolerance.precisionDropdown).should('be.visible').contains('X.XXXXX').trigger('mouseover').click()
    cy.get(partTolerance.linearTable.precisionTopColumn).last().title('X.XXXXX')
    cy.wait(3000)
    cy.get(partTolerance.linearTable.plusTolTopColumn).last().click().type(pluscolumn, { delay: 1500 })
    cy.get(partTolerance.linearTable.minusTolColumn).last().click().type(-minuscolumn, { delay: 1500 })
    cy.get(partTolerance.linearTable.plusTolTopColumn).last().find('input').should('have.value', pluscolumn)
    cy.get(partTolerance.linearTable.minusTolColumn).last().find('input').should('have.value', -minuscolumn)

    //Check current number of table rows
    cy.wait(10000)

    cy.get(partTolerance.lineartable).find("tr").its('length').then((len) => {
      lenBefore = len;
      cy.log('Initial table Length is: ' + lenBefore);
      //delete the linear precision tolerance
    })

    cy.get(partTolerance.linearTable.tableRow).last().find(partTolerance.linearTable.deleteRow).trigger('click')

    cy.wait(3000)

    cy.get(partTolerance.lineartable).find("tr").its('length').then((lenAfter) => {
      cy.log('After table Length is: ' + lenAfter);
      expect(lenAfter).to.equal(lenBefore - 1);
    })

    //Test clean up
    //Navigate to parts home
    cy.navigateToPartList()

    //Delete part
    cy.deletePart()
  })

  it('IXC-2724 - Amend linear to range tolerance', () => {

    //Test set up
    cy.loginAdmin()
    cy.addPart()
    cy.addPartNumber()
    cy.navigateToTolerance()

    //Change Linear Tolerance type to range check radio button is selected

    cy.get(partTolerance.LinearOptions.applyBy).should('exist').and('have.text', 'Apply by:')
    cy.get(partTolerance.LinearOptions.precision).should('exist').and('have.text', 'Precision')
    cy.get(partTolerance.LinearOptions.range).eq(1).should('exist').and('have.text', 'Range')
    cy.get(partTolerance.LinearOptions.rangeRadioBtn).should('exist').and('not.be.checked')
    cy.wait(12000)
    cy.get(partTolerance.LinearOptions.range).eq(1).click({ force: true })
    cy.get(partTolerance.LinearOptions.rangeRadioBtn).click().should('be.checked')

    //Check range headers are displayed

    cy.get(partTolerance.linearTable.linearRangeToleranceHeader).should("exist")
    cy.get(partTolerance.linearTable.plusTotheader).should("exist")
    cy.get(partTolerance.linearTable.MinusTotHeader).should("exist")

    //Add new tolerance

    cy.get(partTolerance.addTolerance).click();

    // Fill new tollerance details
    let range1 = faker.finance.amount(0.001, 0.01, 3)
    let range2 = faker.finance.amount(0.00001, 0.001, 5)
    let pluscolumn = faker.finance.amount(0.0001, 0.001, 4)
    let minuscolumn = faker.finance.amount(0.0001, 0.001, 4)
    cy.wait(5000)
    cy.get(partTolerance.linearTable.RangeTolerance1Row5).type(range1, { delay: 2000 })
    cy.get(partTolerance.linearTable.RangeTolerance2Row5).type(range2, { delay: 2000 })
    cy.get(partTolerance.linearTable.RangeTolerance2Row5).type(range2, { delay: 2000 })

    cy.get(partTolerance.linearTable.plusTolTopColumn).last().click().type(pluscolumn, { delay: 2000 })

    cy.get(partTolerance.linearTable.minusTolColumn).last().click().type(-minuscolumn, { delay: 2000 })

    cy.wait(8000)
    // Check filled detials are correct
    cy.get(partTolerance.linearTable.RangeTolerance1Row5).should('have.value', range1)
    cy.get(partTolerance.linearTable.RangeTolerance2Row5).should('have.value', range2)

    cy.get('#Linear-5-plus > .edit-in-place > .ant-input').should('have.value', pluscolumn)
    cy.get('#Linear-5-minus > .edit-in-place > .ant-input').should('have.value', -minuscolumn)

    //Check current number of table rows

    cy.get(partTolerance.lineartable).find("tr").its('length').then((len) => {
      lenBefore = len;
      cy.log('Initial table Length is: ' + lenBefore);

      //delete the linear range tolerance

      cy.wait(5000)
      cy.get(partTolerance.linearTable.tableRow).last().find(partTolerance.linearTable.deleteRow).trigger('mouseover').click()

      cy.wait(5000)

      cy.get(partTolerance.lineartable).find("tr").its('length').then((lenAfter) => {
        cy.log('After table Length is: ' + lenAfter);
        expect(lenAfter).to.equal(lenBefore - 1);
      })

      //Test clean up
      //Navigate to parts home
      cy.navigateToPartList()

      //Delete part
      cy.deletePart()
    })
  })
})

describe('IXC-2724 - Angular Table', () => {


  it('IXC-2724 - Default Angular tolerance values are displayed', () => {
    //Test set up
    cy.loginAdmin()
    cy.addPart()
    cy.addPartNumber()
    cy.navigateToTolerance()

    //Check tolerance wizard for defaults
    cy.get(partTolerance.toleranceTitle).should('have.text', '3. Default Tolerances')
    cy.get(partTolerance.tolerancePreset).should('have.text', 'Default')

    //System of measurement
    cy.get(partTolerance.radioButtonTitle).should('exist').and('have.text', 'System of Measurement')
    cy.get(partTolerance.metricMeasurement).should('exist').and('not.be.checked')
    cy.get(partTolerance.metricMeasurement).should('exist').and('have.value', 'Metric')
    cy.get(partTolerance.imperialMeasurement).should('exist').and('be.checked')
    cy.get(partTolerance.imperialMeasurement).should('exist').and('have.value', 'Imperial')

    //Open Angular section//
    cy.get(partTolerance.angular).should('exist').and('have.text', 'Angular').should('have.attr', 'aria-expanded', 'false')
    cy.get(partTolerance.angularTitle).click()
    cy.get(partTolerance.angular).should('have.attr', 'aria-expanded', 'true')
    cy.get(partTolerance.linear).should('have.attr', 'aria-expanded', 'false')

    //Row 1 default
    cy.get(partTolerance.angularTable.row1.precisionvalue).title('X')
    cy.get(partTolerance.angularTable.row1.plus).should('have.value', '1')
    cy.get(partTolerance.angularTable.row1.minus).should('have.value', '-1')

    //Row 2 default
    cy.get(partTolerance.angularTable.row2.precisionvalue).title('X.X')
    cy.get(partTolerance.angularTable.row2.plus).should('have.value', '0.1')
    cy.get(partTolerance.angularTable.row2.minus).should('have.value', '-0.1')

    //Row 3 default
    cy.get(partTolerance.angularTable.row3.precisionvalue).title('X.XX')
    cy.get(partTolerance.angularTable.row3.plus).should('have.value', '0.01')
    cy.get(partTolerance.angularTable.row3.minus).should('have.value', '-0.01')

    //Row 4 default
    cy.get(partTolerance.angularTable.row4.precisionvalue).title('X.XXX')
    cy.get(partTolerance.angularTable.row4.plus).should('have.value', '0.001')
    cy.get(partTolerance.angularTable.row4.minus).should('have.value', '-0.001')

    //Row 5 default
    cy.get(partTolerance.angularTable.row5.precisionvalue).title('X.XXXX')
    cy.get(partTolerance.angularTable.row5.plus).should('have.value', '0.0001')
    cy.get(partTolerance.angularTable.row5.minus).should('have.value', '-0.0001')


    //User has option to add a tolerance
    cy.get(partTolerance.angularTable.addTolerance).should('exist')

    //User has unit option
    cy.get(partTolerance.angularTable.unit).should('exist')
    cy.get(partTolerance.angularTable.unitSelector).should('exist')

    //apply all and update x feature
    cy.get(partTolerance.applyAll).should('exist')
    cy.get(partTolerance.updateFeature).should('exist')

    //Test clean up
    //Navigate to parts home
    cy.navigateToPartList()
  
    //Delete part
    cy.deletePart()
  })

  it('IXC-2724 - Update Angular tolerance precision values', () => {

    //Test set up
    cy.loginAdmin()
    cy.addPart()
    cy.addPartNumber()
    cy.navigateToTolerance()

    //Angular section//
    cy.get(partTolerance.angular).should('exist').and('have.text', 'Angular').should('have.attr', 'aria-expanded', 'false')
    cy.get(partTolerance.angularTitle).click()
    cy.get(partTolerance.angular).should('have.attr', 'aria-expanded', 'true')
    cy.get(partTolerance.linear).should('have.attr', 'aria-expanded', 'false')

    //update 1st row plus
    let randomplustopcolumn = faker.random.numeric(1)
    cy.get(partTolerance.angularTable.row1.plus).type(randomplustopcolumn)
    cy.get(partTolerance.angularTable.row1.plus).should('have.value', randomplustopcolumn)

    //update 1st row minus
    let randomminusTolcolumn = -faker.random.numeric(1)
    cy.get(partTolerance.angularTable.row1.minus).type(randomminusTolcolumn)
    cy.get(partTolerance.angularTable.row1.minus).should('have.value', randomminusTolcolumn)

    //Test clean up
    //Navigate to parts home
    cy.navigateToPartList()
    
    //Delete part
    cy.deletePart()
  })

  it('IXC-2724 - Modify Angular tolerance unit type', () => {
    //Test set up
    cy.loginAdmin()
    cy.addPart()
    cy.addPartNumber()
    cy.navigateToTolerance()

    //Open Angular section//
    cy.get(partTolerance.angular).should('exist').and('have.text', 'Angular').should('have.attr', 'aria-expanded', 'false')
    cy.get(partTolerance.angularTitle).click()
    cy.wait(4000)
    cy.get(partTolerance.angular).should('have.attr', 'aria-expanded', 'true')
    cy.get(partTolerance.linear).should('have.attr', 'aria-expanded', 'false')

    //User has unit option
    cy.wait(4000)
    cy.get(partTolerance.angularTable.unit).should('exist')
    cy.get(partTolerance.angularTable.unitSelector).should('exist').should('have.text', 'deg').click()


    cy.get(partTolerance.angularTable.circUnit).click();
    cy.get(partTolerance.angularTable.unitSelector).should('not.have.text', 'deg')
    cy.get(partTolerance.angularTable.unitSelector).should('have.text', 'circ')

    //Test clean up
    //Navigate to parts home
    cy.navigateToPartList()
    
    //Delete part
    cy.deletePart()
  })

  it('IXC-2724 - Duplicate Angular tolerance validation error', () => {

    //Test set up
    cy.loginAdmin()
    cy.addPart()
    cy.addPartNumber()
    cy.navigateToTolerance()

    //Angular section//
    cy.get(partTolerance.angular).should('exist').and('have.text', 'Angular').should('have.attr', 'aria-expanded', 'false')
    cy.get(partTolerance.angularTitle).click()
    cy.get(partTolerance.angular).should('have.attr', 'aria-expanded', 'true')
    cy.get(partTolerance.linear).should('have.attr', 'aria-expanded', 'false')

    //Modify 1st row in angular table
    cy.wait(5000)
    cy.get(partTolerance.angularTable.row1.precisionvalue).click({ timeout: 3000 });
    cy.get(partTolerance.precisionDropdown).contains('X.XX').click()
    cy.get(partTolerance.angularTable.row1.precisionvalue).title('X.XX')

    //check validation-error (red outline) is displayed for duplicate precision value
    cy.get(partTolerance.angularTable.row1.precisionvalue).find('div').should('have.class', partTolerance.validationError)
    cy.get(partTolerance.angularTable.row3.precisionvalue).find('div').should('have.class', partTolerance.validationError)

    //Test clean up
    //Navigate to parts home
    cy.navigateToPartList()
    
    //Delete part
    cy.deletePart()
  })

  it('IXC-2724 - Add and delete Angular precision tolerance', () => {
    //Test set up
    cy.loginAdmin()
    cy.addPart()
    cy.addPartNumber()
    cy.navigateToTolerance()

    //Angular section//
    cy.get(partTolerance.angular).should('exist').and('have.text', 'Angular').should('have.attr', 'aria-expanded', 'false')
    cy.get(partTolerance.angularTitle).click()
    cy.get(partTolerance.angular).should('have.attr', 'aria-expanded', 'true')
    cy.get(partTolerance.linear).should('have.attr', 'aria-expanded', 'false')

    //Add new angular tolerance
    
    cy.get(partTolerance.angularSection).find(partTolerance.addTolerance).click()

    cy.wait(8000)
    let pluscolumn = faker.finance.amount(0.0001, 0.001, 4)
    let minuscolumn = faker.finance.amount(0.0001, 0.001, 4)
    cy.get(partTolerance.angularTable.angularTopColumn).last().click({ timeout: 3000 })
    cy.get(partTolerance.precisionDropdown).should('be.visible').contains('X.XXXXX', { timeout: 3000 }).trigger('mouseover').click()
    cy.get(partTolerance.angularTable.angularTopColumn).last().title('X.XXXXX')
    cy.wait(3000)
    cy.get(partTolerance.angularTable.angularplusTolTopColumn).last().click().type(pluscolumn, { delay: 1500 })
    cy.get(partTolerance.angularTable.angularminusTolColumn).last().click().type(-minuscolumn, { delay: 1500 })
    cy.get(partTolerance.angularTable.angularplusTolTopColumn).last().find('input').should('have.value', pluscolumn)
    cy.get(partTolerance.angularTable.angularminusTolColumn).last().find('input').should('have.value', -minuscolumn)

    //Check current number of table rows
    
    cy.get(partTolerance.angularTable.angulartable).find("tr").its('length').then((len) => {
      lenBefore = len;
      cy.log('Initial table Length is: ' + lenBefore);
    })
    cy.wait(5000)

    //delete the angular precision tolerance
    cy.get(partTolerance.angularTable.angluarTableRow).last().find(partTolerance.angularTable.deleteRow).trigger('mouseover').click()
    cy.wait(3000)
    cy.get(partTolerance.angularTable.angulartable).find("tr").its('length').then((lenAfter) => {
      cy.log('After table Length is: ' + lenAfter);
      expect(lenAfter).to.equal(lenBefore - 1);
    })
    //Test clean up
    //Navigate to parts home
    cy.navigateToPartList()
    
    //Delete part
    cy.deletePart()
  })

})

