import { faker } from '@faker-js/faker';
import authentication from '../../../integration/locators/authentication.json';
import common from '../../../integration/locators/common.json';

//list of tests
//cancel forgotten password
//no email address
//forgot password email not recognised
//invalid email address entered
//cancel reset password
//no password entered
//passwords not match
//password does not match requirements
//user can reset password

describe('Forgotten password negative tests', () => {
  beforeEach(() => {
    cy.forgottenpassword();
  });

  it('Cancel forgotten password', () => {
    cy.get(authentication.forgottenPassword.cancelPwdReset).click();
    cy.url().should('include', '/auth/signin');
  });

  it('No email address', () => {
    cy.get(authentication.forgottenPassword.pwdResetbtn).click();
    cy.get(common.messages.error).should('be.visible').and('contain', 'Email is required');
  });

  it('Forgot password email not recognised', () => {
    let email = faker.random.alpha(12) + '@test.com';
    cy.get(common.fields.email).type(email);
    cy.get(authentication.forgottenPassword.pwdResetbtn).click();
    cy.get(common.messages.notficationPopUp, { timeout: 5000 }).should('be.visible');
    cy.get(common.messages.notficationTxt).should('be.visible').and('contain', 'Email not recognized');
  });

  it('Invalid email address entered', () => {
    cy.get(common.fields.email).type('testinvalidemail');
    cy.get(authentication.forgottenPassword.pwdResetbtn).click();
    cy.get(common.messages.error).should('be.visible').and('contain', 'Email testinvalidemail is invalid');
  });
});

describe('Password reset negative tests', () => {
  beforeEach(() => {
    cy.forceVisit(authentication.forgottenPassword.pwdResetUrl);
    cy.url().should('include', '/auth/password-reset');
  });

  it('Cancel reset password', () => {
    cy.get(authentication.resetPassword.cancelPwdReset).click();
    cy.url().should('include', '/auth/signin');
  });

  it('No password entered', () => {
    cy.get(authentication.resetPassword.pwdResetbtn).click();
    cy.get(common.messages.error).eq(0).should('be.visible').and('contain', 'Password is required');
    cy.get(common.messages.error).eq(1).should('be.visible').and('contain', 'Confirm Password is required');
  });

  it('Passwords not match', () => {
    cy.get(common.fields.password).type('TigerTiger10#=');
    cy.get(common.fields.confirmPassword).type('TigerTiger10#');
    cy.get(authentication.resetPassword.pwdResetbtn).click();
    cy.get(common.messages.error).should('be.visible').and('contain', 'Passwords must match');
  });

  it('Password does not match requirements', () => {
    cy.get(common.fields.password).type('TigerTiger10');
    cy.get(common.fields.confirmPassword).type('TigerTiger10');
    cy.get(common.messages.error).should('be.visible').and('contain', 'Password is too weak');
  });
});

describe('Forgotten password functionality', () => {
  beforeEach(() => {
    cy.forgottenpassword();
  });

  it('User can reset password', () => {
    cy.get(common.fields.email).type('  ');
    cy.get(authentication.forgottenPassword.pwdResetbtn).click();
    cy.get(common.messages.error).should('be.visible').and('contain', 'Email    is invalid');
    cy.get(common.fields.email).clear();
    cy.get(common.fields.email).type('cbit999+cw1@gmail.com');
    cy.get(authentication.forgottenPassword.pwdResetbtn).click();
    cy.get(common.messages.notficationPopUp, { timeout: 5000 }).should('be.visible');
    cy.get(common.messages.notficationTxt).should('be.visible').and('contain', 'Please check your email for a password reset link, it will expire in 30 minutes');
  });
});
