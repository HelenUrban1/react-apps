// Written Tests //

// Request account //
// User can request new account
// Resend verification link
// Email not verified sign in with another account

// Invalid request account functionality //
// No email address or name entered
// Sign in with another account

//Verify Email - Negative tests //
// Passwords do not match
// Password does not match requirements
// First name is required
// Last name is required

// Tests To Do //

// Request account //
// invalid email address provided e.g an email missing the om of .com

// Verify email // -- Cannot complete without email address token
// First name not given
// Terms and conditions not selected
// Successfully verify email
// successfully request account, verify email and login

import { faker } from '@faker-js/faker';
import authentication from '../../../integration/locators/authentication.json';
import common from '../../../integration/locators/common.json';

describe('Request account', () => {
  it('User can request new account', () => {
    cy.visit('/');
    cy.get(authentication.newAccount.newAccountlink).click();
    cy.get(authentication.newAccount.SignUp).should('be.disabled');

    //Enter the details for the new account//
    let email = faker.random.alpha(10) + '@test.com';
    cy.get(common.fields.email).type(email);
    cy.get(common.fields.firstName).type(faker.random.alpha(6));
    cy.get(authentication.newAccount.SignUp).click();
    cy.get('h3', { timeout: 10000 }).should('be.visible');
    cy.get('h3', { timeout: 10000 })
      .should('be.visible')
      .and('contain', 'Please confirm your email at ' + email + ' to continue');
  });

  it('Resend verification link', () => {
    cy.visit('/');
    cy.get(authentication.newAccount.newAccountlink).click();
    cy.get(authentication.newAccount.SignUp).should('be.disabled');

    //Enter the details for the new account//
    let email = faker.random.alpha(10) + '@test.com';
    cy.get(common.fields.email).type(email);
    cy.get(common.fields.firstName).type(faker.random.alpha(6));
    cy.get(authentication.newAccount.SignUp).click();
    cy.get('h3', { timeout: 10000 }).should('be.visible');

    //resend verification link
    cy.get(authentication.emailNotVerified.resendVerficationBtn).click();
    cy.get(common.messages.notficationPopUp).should('be.visible');
    cy.get(common.messages.notficationTxt).should('be.visible').and('contain', 'Please check your email for a new activation link, it will expire in 30 minutes');
  });

  it('Email not verified sign in with another account', () => {
    cy.visit('/');
    cy.get(authentication.newAccount.newAccountlink).click();
    cy.get(authentication.newAccount.SignUp).should('be.disabled');

    //Enter the details for the new account//
    let email = faker.random.alpha(10) + '@test.com';
    cy.get(common.fields.email).type(email);
    cy.get(common.fields.firstName).type(faker.random.alpha(6));
    cy.get(authentication.newAccount.SignUp).click();
    cy.get('h3', { timeout: 10000 }).should('be.visible');

    //email not verified sign in with another account
    cy.get('.ix-logo-title').should('be.visible');
    cy.contains('Sign in with another account').should('be.visible').click();
    // cy.get(common.buttons.signInWithOtherAcc).click()
    cy.url().should('include', '/auth/signin');
  });
  it('No email address or name entered', () => {
    cy.visit('/');
    cy.get(authentication.newAccount.newAccountlink).click();
    cy.get(authentication.newAccount.SignUp).should('be.disabled');

    //Check no email or name error
    cy.get(common.fields.email).click();
    cy.get(common.fields.firstName).click();
    cy.wait(3000);
    cy.get(common.messages.error).eq(0).should('be.visible').and('contain', 'Email is required');
    cy.get(common.fields.email).click();
    cy.get(common.messages.error).eq(1).should('be.visible').and('contain', 'Name is required');
    cy.get(authentication.newAccount.SignUp).should('be.disabled');
  });
  it('Sign in with another account', () => {
    cy.visit('/');
    cy.get(authentication.newAccount.newAccountlink).click();
    cy.get(authentication.newAccount.SignUp).should('be.disabled');

    //Check sign in with other acc button
    cy.get(common.buttons.signInWithOtherAcc).click();
    cy.url().should('include', '/auth/signin');
  });
  it('Passwords do not match', () => {
    cy.forceVisit(authentication.verifyEmail.verifyEmailUrl);
    cy.url().should('include', '/auth/verify-email');

    //Type non matching passwords
    cy.get(common.fields.password).type('TigerTiger10#=');
    cy.get(common.fields.confirmPassword).type('TigerTiger10#');
    cy.get(common.fields.firstName).click();
    cy.get(common.messages.error).eq(0).should('be.visible').and('contain', 'Passwords must match');
  });
  it('Password does not match requirements', () => {
    cy.forceVisit(authentication.verifyEmail.verifyEmailUrl);
    cy.url().should('include', '/auth/verify-email');

    //Type passwords that do not match the requirements
    cy.get(common.fields.password).type('TigerTiger10');
    cy.get(common.fields.confirmPassword).type('TigerTiger10');
    cy.get(common.messages.error).should('be.visible').and('contain', 'Password is too weak');
  });
  it('First name is required', () => {
    cy.forceVisit(authentication.verifyEmail.verifyEmailUrl);
    cy.url().should('include', '/auth/verify-email');

    //Check First name is required error response
    cy.get(common.fields.firstName).click();
    cy.get(common.fields.confirmPassword).click();
    cy.get(common.messages.error).eq(0).should('be.visible').and('contain', 'Name is required');
  });

  it('Last name is required', () => {
    cy.forceVisit(authentication.verifyEmail.verifyEmailUrl);
    cy.url().should('include', '/auth/verify-email');

    //Check First name is required error response
    cy.get(common.fields.lastName).click();
    cy.get(common.fields.confirmPassword).click();
    cy.get(common.messages.error).eq(0).should('be.visible').and('contain', 'Name is required');
  });
});
