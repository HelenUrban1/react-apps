import authentication from "../../../integration/locators/authentication.json"
import common from "../../../integration/locators/common.json"

// Tests Written //

// Successful Login and logout functionality //
// login as admin user and logout
// login as planner-paid seat and logout
// login as reviewer-Free seat and logout
// login as planner with Billing - Paid seat and out

// Invalid Login functionality //
// login as Inactive user (Bug IXC-2507)
// Invalid login details
// Invalid email address
// No email address or password provided

// Tests To Do //
// Prevents unauthenticated users from accessing secure pages
// Remember me (BUG- IXC-2655)
describe('Successful Login and logout functionality', () => {

    it('login as admin user and logout', () => {
        cy.loginAdmin()
        cy.logout()
    })

    it('login as planner-paid seat and logout', () => {
        cy.loginPlanner()
        cy.logout()
    })

    it('login as reviewer-Free seat and logout', () => {
        cy.loginReviewer()
        cy.get('h1').should('be.visible').and('contain', 'Parts')
        cy.logout()
    })

    it('login as planner with Billing-Paid seat and out', () => {
        cy.loginPlannerWithBilling()
        cy.logout()
    })

    it('IXC-2453 - login as Billing-Free seat', () => {
        cy.loginBilling()
        cy.logout()
   })

})
describe('Invalid Login functionality', () => {

    it('login as Inactive user (Bug IXC-2650)', () => {
        cy.loginInactive()
        })

    it('Invalid login details', () => {
        cy.login('test@test.com', 'Test10#=');
        cy.get(common.messages.error).should('be.visible').and('contain', "Sorry, we don't recognize your credentials")
        cy.get(common.fields.email).should('be.visible').and('not.contain', 'test@test.com')
        cy.get(common.fields.password).should('be.visible').and('not.contain', 'Test10#=')
    });

    it('Invalid email address', () => {
        cy.login('test', 'Test10#=');
        cy.get(common.messages.error).should('be.visible').and('contain', "Email test is invalid")
        cy.get(common.fields.email).should('be.visible').and('not.contain', 'test')
        cy.get(common.fields.password).should('be.visible').and('not.contain', 'Test10#=')
    })

    it('No email address or password provided', () => {
        cy.visit('/');
        cy.get(authentication.loginPage.loginBtn).click()
        cy.get(common.messages.error).eq(0).should('be.visible').and('contain', "Email is required")
        cy.get(common.messages.error).eq(1).should('be.visible').and('contain', "Password is required")
    })
})