import { faker } from '@faker-js/faker';
import 'cypress-file-upload';
import partList from '../../../integration/locators/part-list.json';
import sideNav from '../../../integration/locators/side-nav-menu-section.json';
import templates from '../../../integration/locators/Templates.json';
import common from '../../../integration/locators/common.json';

const xlsx = 'addTemplate.xlsx';
const csv = 'addTemplate1.csv';
const pdf = 'test.PDF';
const reportTitle = '7582 - 2024-03-19 Part';
let newTitle = faker.random.alpha(10) + ' UI Automation';

//Tests available
//Verify default templates display in the Gallery
//Open AS9102B template and verify fields on review report
//Open template, change title, check report is displayed on part and delete the report
//Open template, change title, add new filter to report and delete filter it
//Add a new template .xlsx file, update the tile and delete template
//Invalid file type csv, user should be displayed an error
//Add a new template invalid file type pdf, user should be displayed an error

describe('IXC-2635 - Templates', () => {
  beforeEach(() => {
    cy.loginAdmin();
  });

  it('Verify default templates display in the Gallery', () => {
    cy.get(sideNav.partsIcon).trigger('mouseover');
    cy.get(templates.submenuPopup).should('be.visible');
    cy.contains('Templates').click();
    cy.get('h2').should('be.visible').and('contain', 'Template Gallery');
    //Verify default templates are displayed
    cy.get(templates.templateList).should('be.visible');
    cy.get(templates.newTemplate).should('exist');
    cy.get(templates.createNewTemplateTile).should('exist').and('have.text', 'Create New Template');
    cy.get(templates.template_fai).should('exist');
    cy.get(templates.templateTitle).eq(0).should('have.text', 'All Data Template');
    cy.get(templates.template_as9102b).should('exist');
    cy.get(templates.templateTitle).eq(1).should('have.text', 'AS9102B Template');
    cy.get(templates.templateTitle).eq(2).should('have.text', 'AS9102C Template');
    cy.get(templates.template_ppap).should('exist');
    cy.get(templates.templateTitle).eq(3).should('have.text', 'PPAP Dimensional Template');
  });

  it('Open AS9102B template and verify fields on review report', () => {
    cy.get(sideNav.partsIcon).trigger('mouseover');
    cy.get(templates.submenuPopup).should('be.visible');
    cy.contains('Templates').click();
    cy.get('h2').should('be.visible').and('contain', 'Template Gallery');
    // Build report button is disabled as no part number is selected
    cy.get(templates.Buildreport).should('not.be.enabled');
    //select a part number
    cy.get(templates.PartNumber).click();
    cy.contains('7582').click({ force: true });

    //Build report button is enabled as no part number is selected
    cy.get(templates.Buildreport).should('be.enabled');

    //select the template AS9102B
    cy.get(templates.template_as9102b).find('button.ant-btn.ant-btn-primary.ant-btn-block.generate-report').click({ force: true });

    //opens the Review report page and checks correct options are displayed -editing the report is out of scope for this automation test
    cy.get(templates.createReport.reportEditiorTitle).should('contain.text', '9. Review Your Report');
    cy.get(templates.createReport.part).should('exist');
    cy.get(templates.createReport.ReportCustomer).should('exist');
    cy.get(templates.createReport.DataFilters).click().invoke('text');
    cy.get(templates.createReport.Features).click().invoke('text');
    cy.get(templates.createReport.Inspection).click().invoke('text');
    cy.get(templates.createReport.Location).click().invoke('text');
    cy.get(templates.AddNewFilter.addFilter).should('exist');
    cy.get(templates.createReport.DownloadBtn).should('exist');
    cy.get(templates.createReport.RefreshBtn).should('exist');
    cy.get(templates.createReport.printBtn).should('exist');
  });

  it('Open template, change title, check report is displayed on part and delete the report', () => {
    cy.get(sideNav.partsIcon).trigger('mouseover');
    cy.get(templates.submenuPopup).should('be.visible');
    cy.contains('Templates').click();
    cy.get('h2').should('be.visible').and('contain', 'Template Gallery');
    // Build report button is disabled as no part number is selected
    cy.get(templates.Buildreport).should('not.be.enabled');
    //select a part number
    cy.wait(10000);
    cy.get(templates.PartNumber).click();
    cy.contains('7582').click({ force: true });

    //Build report button is enabled as no part number is selected
    cy.get(templates.Buildreport).should('be.enabled');

    //select the template AS9102B
    cy.get(templates.template_as9102b).find('button.ant-btn.ant-btn-primary.ant-btn-block.generate-report').click({ force: true });

    //opens the Review report page and updates the title
    cy.get(templates.createReport.reportEditiorTitle).should('contain.text', '9. Review Your Report');
    cy.get(templates.createReport.part).should('exist');
    cy.get(templates.createReport.reportTitle).should('exist').and('have.value', reportTitle);
    cy.get(templates.createReport.reportTitle).click().clear().type(newTitle);
    cy.get(templates.createReport.RefreshBtn).click();
    cy.get(templates.createReport.reportTitle).should('have.value', newTitle);
    cy.get(templates.createReport.nextBtn).click();

    //User is taken to parts page
    cy.get('h1').should('contain', 'Parts');

    //Check part report created for is open
    cy.get(partList.sideWindowWrapper.partNumber).should('be.visible').and('contain', '7582');
    cy.get(partList.sideWindowWrapper.partName).should('be.visible').and('contain', 'IX Multi Tool A5 Tahoma 2');
    //Check part report exsits
    cy.get(partList.sideWindowWrapper.savedReports).should('be.visible').and('contain', newTitle);
    //(clean up)Delete part report from part
    cy.get(partList.sideWindowWrapper.firstSavedReport).should('be.visible').and('contain', newTitle).trigger('mouseover');
    cy.get(partList.sideWindowWrapper.deleteFirstSavedReport).click();
    cy.on('window:confirm', (str) => {
      expect(str).to.contain('Are you sure?');
    });
    cy.get('#popconfirm-btn-ok').click({ force: true });
    cy.get(partList.sideWindowWrapper.savedReports).should('be.visible').and('not.contain', newTitle);
  });

  it('Open template, change title, add new filter to report and delete filter it', () => {
    cy.get(sideNav.partsIcon).trigger('mouseover');
    cy.get(templates.submenuPopup).should('be.visible');
    cy.contains('Templates').click();
    cy.get('h2').should('be.visible').and('contain', 'Template Gallery');
    //open new report template
    cy.get(templates.PartNumber).click();
    cy.contains('7582').click({ force: true });
    cy.get(templates.template_as9102b).find('button.ant-btn.ant-btn-primary.ant-btn-block.generate-report').click({ force: true });
    cy.get(templates.createReport.reportTitle).click().clear().type(newTitle);
    cy.get(templates.createReport.RefreshBtn).click();
    cy.wait(5000);
    //Add new filter to report
    cy.get(templates.createReport.defaultFilter).should('exist');
    cy.get(templates.AddNewFilter.newFilter).should('not.exist');
    cy.get(templates.AddNewFilter.addFilter).click();
    cy.get(templates.createReport.defaultFilter).should('exist');
    cy.get(templates.AddNewFilter.newFilter).should('exist').click();
    cy.get(templates.createReport.Location).click();
    cy.get(templates.createReport.locationDrawing).click();
    cy.get(templates.createReport.filledFilter).should('exist').and('contain', 'Location / Drawing');

    //delete the new filter
    cy.get(templates.AddNewFilter.removeFilter).eq(1).click();
    cy.get(templates.AddNewFilter.removeFilter).last().click();

    //check filter is deleted
    cy.get(templates.filledFilter).should('not.exist');
    cy.get(templates.AddNewFilter.newFilter).should('not.exist');

    //clean up
    cy.get(templates.createReport.nextBtn).click();
    cy.get(partList.sideWindowWrapper.savedReports).should('be.visible').and('contain', newTitle);
    cy.get(partList.sideWindowWrapper.firstSavedReport).should('be.visible').and('contain', newTitle).trigger('mouseover');
    cy.get(partList.sideWindowWrapper.deleteFirstSavedReport).click();
    cy.on('window:confirm', (str) => {
      expect(str).to.contain('Are you sure?');
    });
    cy.get('#popconfirm-btn-ok').click({ force: true });
    cy.get(partList.sideWindowWrapper.savedReports).should('be.visible').and('not.contain', newTitle);
  });

  it('Add a new template .xlsx file, update the tile and delete template', () => {
    cy.get(sideNav.partsIcon).trigger('mouseover');
    cy.get(templates.submenuPopup).should('be.visible');
    cy.contains('Templates').click();
    cy.get('h2').should('be.visible').and('contain', 'Template Gallery');

    cy.wait(10000);
    // Attach new template

    cy.get(templates.addNewTemplate.newTemplateUpload);
    cy.get(common.PartDrawer.partDocumentUpload).attachFile({ filePath: `/${xlsx}`, encoding: 'base64' }, { subjectType: 'drag-n-drop' });
    // the template is opened correctly
    cy.contains('Build Your Template');

    // Update title
    cy.get(templates.createReport.reportTitle).should('exist').and('have.value', 'addTemplate');
    cy.get(templates.createReport.reportTitle).click().clear().type(newTitle);
    cy.get(templates.createReport.reportTitle).should('have.value', newTitle);
    // return to templates page
    cy.get(templates.createReport.backfooterBtn).click({ force: true });

    // Delete Template
    cy.get(templates.deleteTemplate).click({ multiple: true });
    cy.on('window:confirm', (str) => {
      expect(str).to.contain('Are you sure?');
    });
    cy.get('[data-cy="template_card_title"]').eq(4).should('have.text', newTitle);
    cy.get(templates.deleteAddedTemplate).eq(0).click({ force: true });
    cy.get(templates.deleteAddedTemplate).eq(0).click({ force: true });
  });

  it('Invalid file type csv, user should be displayed an error', () => {
    cy.get(sideNav.partsIcon).trigger('mouseover');
    cy.get(templates.submenuPopup).should('be.visible');
    cy.contains('Templates').click();
    cy.get('h2').should('be.visible').and('contain', 'Template Gallery');
    // Attach new template
    cy.wait(10000);
    cy.get("input[type ='file']").attachFile(csv);

    // check user is displayed error response
    cy.get('.ant-message-notice-content').should('be.visible').and('have.text', 'Only excel (.xlsx) files are allowed');
  });

  it('Add a new template invalid file type pdf, user should be displayed an error', () => {
    cy.get(sideNav.partsIcon).trigger('mouseover');
    cy.get(templates.submenuPopup).should('be.visible');
    cy.contains('Templates').click();
    cy.get('h2').should('be.visible').and('contain', 'Template Gallery');
    // Attach new template
    cy.wait(10000);
    cy.get("input[type ='file']").attachFile(pdf);
    // check user is displayed error response
    cy.get('.ant-message-notice-content').should('be.visible').and('have.text', 'Only excel (.xlsx) files are allowed');
  });
});
