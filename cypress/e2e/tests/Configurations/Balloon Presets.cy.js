// Tests Written //

// Configuration should have a section for Balloon Styles
// Should not be able to delete or change name of default row
// Should not be able to delete the Default balloon style
// should update the default style -- Return to previous style
// should add a balloon
// should add a preset, should remove a preset, Should be able to add customer to preset

import 'cypress-file-upload';
import SideNavMenu from '../../../integration/locators/side-nav-menu-section.json';
import account from '../../../integration/locators/settings-account.json';

describe('Balloon Presets', () => {
  beforeEach(() => {
    cy.loginAdmin();
  });

  it('Configuration should have a section for Balloon Styles', () => {
    cy.get(SideNavMenu.settings).trigger('mouseover');
    cy.get(SideNavMenu.configurations).click({ force: true });
    cy.get(account.Configurations.balloonStylePresets).should('be.visible').click();
  });

  it('Should not be able to delete or change name of default row', () => {
    cy.get(SideNavMenu.settings).trigger('mouseover');
    cy.get(SideNavMenu.configurations).click({ force: true });
    cy.get(account.Configurations.balloonStylePresets).should('be.visible').click();
    cy.get(account.Configurations.balloonStylesTableNameLabel).should('be.visible').should('have.text', 'Name');
    cy.get(account.Configurations.balloonStylesTableBalloonLabel).should('be.visible').should('have.text', 'Balloons');
    cy.get(account.Configurations.balloonStylesTableBalloonCustomerLabel).should('be.visible').should('have.text', 'Customers');
    cy.get(account.Configurations.balloonStylesTableDefaultRow).should('be.visible');
    cy.wait(1000);
    cy.get(account.Configurations.balloonStylesTableDefaultText).should('not.be.enabled');
  });

  it('Should not be able to delete the Default balloon style', () => {
    cy.get(SideNavMenu.settings).trigger('mouseover');
    cy.get(SideNavMenu.configurations).click({ force: true });
    cy.get(account.Configurations.balloonStylePresets).should('be.visible').click();
    cy.get(account.Configurations.balloonStylesTableDefaultText).contains('Default').find('.icon-button').should('not.exist');
    cy.get(account.Configurations.balloonStylesDefaultBalloon).trigger('mouseover');
    cy.get('#coral-circle-solid').should('be.visible').trigger('mouseover');
    cy.get(account.Configurations.deleteBalloonIcon).should('not.exist');
  });

  it('should update the default style -- Return to previous style', () => {
    cy.get(SideNavMenu.settings).trigger('mouseover');
    cy.get(SideNavMenu.configurations).click({ force: true });
    cy.get(account.Configurations.balloonStylePresets).should('be.visible').click();
    //Black Balloons
    cy.SelectDefaultBalloon('#black-circle-solid');
    cy.SelectDefaultBalloon('#black-circle-hollow');
    cy.SelectDefaultBalloon('#black-diamond-solid');
    cy.SelectDefaultBalloon('#black-diamond-hollow');
    cy.SelectDefaultBalloon('#black-triangle-solid');
    cy.SelectDefaultBalloon('#black-triangle-hollow');
    //Purple Balloons
    cy.SelectDefaultBalloon('#purple-circle-solid');
    cy.SelectDefaultBalloon('#purple-circle-hollow');
    cy.SelectDefaultBalloon('#purple-diamond-solid');
    cy.SelectDefaultBalloon('#purple-diamond-hollow');
    cy.SelectDefaultBalloon('#purple-triangle-solid');
    cy.SelectDefaultBalloon('#purple-triangle-hollow');
    //Seafoam Balloons
    cy.SelectDefaultBalloon('#seafoam-circle-solid');
    cy.SelectDefaultBalloon('#seafoam-circle-hollow');
    cy.SelectDefaultBalloon('#seafoam-diamond-solid');
    cy.SelectDefaultBalloon('#seafoam-diamond-hollow');
    cy.SelectDefaultBalloon('#seafoam-triangle-solid');
    cy.SelectDefaultBalloon('#seafoam-triangle-hollow');
    //Sunshine Balloons
    cy.SelectDefaultBalloon('#sunshine-circle-solid');
    cy.SelectDefaultBalloon('#sunshine-circle-hollow');
    cy.SelectDefaultBalloon('#sunshine-diamond-solid');
    cy.SelectDefaultBalloon('#sunshine-diamond-hollow');
    cy.SelectDefaultBalloon('#sunshine-triangle-solid');
    cy.SelectDefaultBalloon('#sunshine-triangle-hollow');
    //Coral Balloons
    cy.SelectDefaultBalloon('#coral-circle-hollow');
    cy.SelectDefaultBalloon('#coral-diamond-solid');
    cy.SelectDefaultBalloon('#coral-diamond-hollow');
    cy.SelectDefaultBalloon('#coral-triangle-solid');
    cy.SelectDefaultBalloon('#coral-triangle-hollow');
    cy.SelectDefaultBalloon('#coral-circle-solid');
  });

  it('Add and Remove Preset add customer', () => {
    cy.get(SideNavMenu.settings).trigger('mouseover');
    cy.get(SideNavMenu.configurations).click({ force: true });
    cy.get(account.Configurations.balloonStylePresets).should('be.visible').click();
    cy.get(account.Configurations.balloonStylesAddPreset).should('be.visible').click();
    cy.get(account.Configurations.newPresetsModalCancel).should('be.visible').click();
    cy.get(account.Configurations.balloonStylesAddPreset).should('be.visible').click();
    cy.get(account.Configurations.newPresetsModalClose).should('be.visible').click();
    cy.get(account.Configurations.balloonStylesAddPreset).should('be.visible').click();
    cy.get(account.Configurations.newPresetsModalInput).should('be.visible').type('Test Preset');
    cy.wait(1000);
    cy.get(account.Configurations.newPresetsModalSave).should('be.visible').click();
    cy.get(account.Configurations.presetOneName).should('have.value', 'Test Preset');
    cy.get(account.Configurations.deletePresetOneButton).should('be.visible').click();
    cy.get(account.Configurations.deletePresetOneModalCancelButton).should('be.visible').click();
    cy.get(account.Configurations.presetOneCustomer).should('be.visible').click();
    cy.contains('Ideagen').click();
    cy.get(SideNavMenu.settings).click();
    cy.get(account.Configurations.presetOneCustomer).click();
    cy.wait(1000);
    cy.get(account.Configurations.deletePresetOneButton).should('be.visible').click();
    cy.wait(1000);
    cy.get(account.Configurations.deletePresetOneModalConfirmButton).should('be.visible').click();
    cy.get(account.Configurations.presetOneName).should('not.exist');
  });

  it('should add a balloon', () => {
    cy.get(SideNavMenu.settings).trigger('mouseover');
    cy.get(SideNavMenu.configurations).click({ force: true });
    cy.get(account.Configurations.balloonStylePresets).should('be.visible').click();
    cy.get(account.Configurations.balloonStylesAddPreset).should('be.visible').click();
    cy.get(account.Configurations.newPresetsModalInput).should('be.visible').type('Test Preset');
    cy.wait(1000);
    cy.get(account.Configurations.newPresetsModalSave).should('be.visible').click();
    cy.get(account.Configurations.presetOneName).should('have.value', 'Test Preset');
    cy.get(account.Configurations.addBalloonPresetOne).click();
    cy.get(account.Configurations.addBalloonPresetOne).click();
    cy.get(account.Configurations.addBalloonPresetOne).click();
    cy.get(account.Configurations.addBalloonPresetOne).click();
    cy.get(account.Configurations.deletePresetOneButton).should('be.visible').click();
    cy.get(account.Configurations.deletePresetOneModalConfirmButton).should('be.visible').click();
    cy.get(account.Configurations.presetOneName).should('not.exist');
  });
});
