// Tests Written //

// Should show a Tolerance Presets options
// Should add a tolerance preset
// Should delete a custom tolerance preset
// Should show the tolerances when expanding a row
// should change linear units when changing system of measurement

import { faker } from '@faker-js/faker';
import account from '../../../integration/locators/settings-account.json';
import SideNavMenu from '../../../integration/locators/side-nav-menu-section.json';

beforeEach(() => {
    
   // Login
   cy.loginAdmin()
   cy.visit('/');
  });

describe('Tolerance Presets', () => {

    it('Should show a Tolerance Presets options', () => {
        cy.get(SideNavMenu.settings).trigger('mouseover');
        cy.get(SideNavMenu.configurations).click({ force: true });
        cy.get(account.tolerancePresets.tolerancePresets).should('be.visible').click();
        cy.get(account.tolerancePresets.presetsName).should('be.visible').should('have.text', 'Name');
        cy.get(account.tolerancePresets.systemOfMeasurement).should('be.visible').should('have.text', 'System of Measurement');
        cy.get(account.tolerancePresets.Customers).should('be.visible').should('have.text', 'Customers');
        cy.get(account.tolerancePresets.toleranceExpandIcon).should('be.visible').click();
        cy.get(account.tolerancePresets.defaultName).should('be.visible').should('have.text', 'Default');
        cy.get(account.tolerancePresets.measurementMetric).should('be.visible').should('have.text', 'Metric');
        cy.get(account.tolerancePresets.measurementImperial).should('be.visible').should('have.text', 'Imperial (US/UK)');
        cy.get(account.tolerancePresets.linearApplyBy).should('be.visible').should('have.text', 'Apply by:');
        cy.get(account.tolerancePresets.linearRadioBtnPrecision).should('be.visible').should('have.text', 'Precision');
        cy.get(account.tolerancePresets.linearRadioBtnRange).should('be.visible').should('have.text', 'Range');
        cy.get(account.tolerancePresets.angularApplyBy).should('be.visible').should('have.text', 'Apply by:');
        cy.get(account.tolerancePresets.angularRadioBtnPrecision).should('be.visible').should('have.text', 'Precision');
        cy.get(account.tolerancePresets.angularRadioBtnRange).should('be.visible').should('have.text', 'Range');
        cy.get(account.tolerancePresets.linearPrecision).should('be.visible').should('have.text', 'Precision');
        cy.get(account.tolerancePresets.linearPlusTol).should('be.visible').should('have.text', 'Plus Tol');
        cy.get(account.tolerancePresets.linearMinusTol).should('be.visible').should('have.text', 'Minus Tol');
        cy.get(account.tolerancePresets.angularPrecision).should('be.visible').should('have.text', 'Precision');
        cy.get(account.tolerancePresets.angularPlusTol).should('be.visible').should('have.text', 'Plus Tol');
        cy.get(account.tolerancePresets.angularMinusTol).should('be.visible').should('have.text', 'Minus Tol');
        cy.get(account.tolerancePresets.addLinearTolerance).should('be.visible').should('have.text', 'Add Tolerance');
        cy.get(account.tolerancePresets.addAngularTolerance).should('be.visible').should('have.text', 'Add Tolerance');
        cy.get(account.tolerancePresets.linearZeroPrecision).should('be.visible').should('have.text', 'X');
        cy.get(account.tolerancePresets.linearOnePrecision).should('be.visible').should('have.text', 'X.X');
        cy.get(account.tolerancePresets.linearTwoPrecision).should('be.visible').should('have.text', 'X.XX');
        cy.get(account.tolerancePresets.linearThreePrecision).should('be.visible').should('have.text', 'X.XXX');
        cy.get(account.tolerancePresets.linearFourPrecision).should('be.visible').should('have.text', 'X.XXXX');
        cy.get(account.tolerancePresets.linearPrecisionToleranceFirst).should('be.visible').should('have.value', '1');
        cy.get(account.tolerancePresets.linearOnePlus).should('be.visible').should('have.value', '0.1');
        cy.get(account.tolerancePresets.linearTwpPlus).should('be.visible').should('have.value', '0.01');
        cy.get(account.tolerancePresets.linearThreePlus).should('be.visible').should('have.value', '0.001');
        cy.get(account.tolerancePresets.linearFourPlus).should('be.visible').should('have.value', '0.0001');
        cy.get(account.tolerancePresets.linearZeroMinus).should('be.visible').should('have.value', '-1');
        cy.get(account.tolerancePresets.linearOneMinus).should('be.visible').should('have.value', '-0.1');
        cy.get(account.tolerancePresets.linearTwoMinus).should('be.visible').should('have.value', '-0.01');
        cy.get(account.tolerancePresets.linearThreeMinus).should('be.visible').should('have.value', '-0.001');
        cy.get(account.tolerancePresets.linearFourMinus).should('be.visible').should('have.value', '-0.0001');
        cy.get(account.tolerancePresets.linearUnit).should('be.visible').should('have.text', 'Unit:in');
        cy.get(account.tolerancePresets.angularZeroPrecision).should('be.visible').should('have.text', 'X');
        cy.get(account.tolerancePresets.angularOnePrecision).should('be.visible').should('have.text', 'X.X');
        cy.get(account.tolerancePresets.angularTwoPrecision).should('be.visible').should('have.text', 'X.XX');
        cy.get(account.tolerancePresets.angularThreePrecision).should('be.visible').should('have.text', 'X.XXX');
        cy.get(account.tolerancePresets.angularFourPrecision).should('be.visible').should('have.text', 'X.XXXX');
        cy.get(account.tolerancePresets.angularPrecisionToleranceFirst).should('be.visible').should('have.value', '1');
        cy.get(account.tolerancePresets.angularOnePlus).should('be.visible').should('have.value', '0.1');
        cy.get(account.tolerancePresets.angularTwoPlus).should('be.visible').should('have.value', '0.01');
        cy.get(account.tolerancePresets.angularThreePlus).should('be.visible').should('have.value', '0.001');
        cy.get(account.tolerancePresets.angularFourPlus).should('be.visible').should('have.value', '0.0001');
        cy.get(account.tolerancePresets.angularZeroMinus).should('be.visible').should('have.value', '-1');
        cy.get(account.tolerancePresets.angularOneMinus).should('be.visible').should('have.value', '-0.1');
        cy.get(account.tolerancePresets.angularTwoMinus).should('be.visible').should('have.value', '-0.01');
        cy.get(account.tolerancePresets.angularThreeMinus).should('be.visible').should('have.value', '-0.001');
        cy.get(account.tolerancePresets.angularFourMinus).should('be.visible').should('have.value', '-0.0001');
        cy.get(account.tolerancePresets.angularUnit).should('be.visible').should('have.text', 'Unit:deg');
    });

    it('Should add a tolerance preset', () => {
        cy.get(SideNavMenu.settings).trigger('mouseover');
        cy.get(SideNavMenu.configurations).click({ force: true });
        cy.get(account.tolerancePresets.tolerancePresets).should('be.visible').click();
        cy.wait(1000);
        cy.get(account.tolerancePresets.addOptionalTolerancePreset).should('be.visible').click();
        cy.get(account.tolerancePresets.presetName).should('be.visible').type(faker.name.firstName());
        cy.get(account.tolerancePresets.cancelPreset).should('be.visible').click();
        cy.wait(1000);
        cy.get(account.tolerancePresets.addOptionalTolerancePreset).should('be.visible').click();
        cy.get(account.tolerancePresets.presetName).should('be.visible').type(faker.name.firstName());
        //click save button
        cy.get(account.tolerancePresets.savePreset).should('be.visible').click();
        cy.wait(1000);
        cy.get(account.tolerancePresets.newPrestName).should('be.visible');
    });

    it('Should delete a custom tolerance preset', () => {
        cy.get(SideNavMenu.settings).trigger('mouseover');
        cy.get(SideNavMenu.configurations).click({ force: true });
        cy.get(account.tolerancePresets.tolerancePresets).should('be.visible').click();
        //delete the preset
        cy.get(account.tolerancePresets.deletePresets).click();
        cy.get(account.tolerancePresets.deletePresetsCancel)
        .should('be.visible')
        .click();
        cy.wait(1000);
        cy.get(account.tolerancePresets.deletePresets).click();
        cy.get(account.tolerancePresets.deletePresetsConfirm)
        .should('be.visible')
        .click();
        cy.wait(1000);
        cy.get(account.tolerancePresets.newPrestName).should('not.exist');
    });

    it('Should show the tolerances when expanding a row', () => {
        cy.get(SideNavMenu.settings).trigger('mouseover');
        cy.get(SideNavMenu.configurations).click({ force: true });
        cy.get(account.tolerancePresets.tolerancePresets).should('be.visible').click();
        cy.get(account.tolerancePresets.presetsName).should('be.visible').should('have.text', 'Name');
    });

    it('should change linear units when changing system of measurement', () => {
        cy.get(SideNavMenu.settings).trigger('mouseover');
        cy.get(SideNavMenu.configurations).click({ force: true });
        cy.get(account.tolerancePresets.tolerancePresets).should('be.visible').click();
        cy.get(account.tolerancePresets.presetsName).should('be.visible').should('have.text', 'Name');
        cy.get(account.tolerancePresets.toleranceExpandIcon).should('be.visible').click();
        cy.get(account.tolerancePresets.measurementMetric).click();
        cy.get(account.tolerancePresets.linearUnit).should('be.visible').should('have.text', 'Unit:mm');
        cy.wait(1000);
        cy.get(account.tolerancePresets.measurementImperial).click();
        cy.get(account.tolerancePresets.linearUnit).should('be.visible').should('have.text', 'Unit:in');
    }); 
});






