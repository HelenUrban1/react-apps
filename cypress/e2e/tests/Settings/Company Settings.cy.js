// Tests Written
// Company Profile edit and reset fields
// Seat Management -Invite users form the seats page
// Add Operator - commented out as not in scope currently
//Permissions:
// Admin has access to security settings - MFA
// Non Admin No access to security settings - MFA
import SideNavMenu from '../../../integration/locators/side-nav-menu-section.json';
import seats from '../../../integration/locators/Seats.json';
import account from '../../../integration/locators/settings-account.json';

describe('Company Settings', () => {
  it('Company Profile edit and reset fields', () => {
    //Test set up
    cy.loginAdmin().get('h1').should('contain', 'Parts');
    //Edit and rest fields
    cy.CompanyProfileEditAndResetFields();
  });

  it('Invite users form the seats page', () => {
    //Test set up
    cy.loginAdmin().get('h1').should('contain', 'Parts');
    cy.get(SideNavMenu.settings).trigger('mouseover');
    cy.get(SideNavMenu.seats).click({ force: true });
    cy.wait(6000);
    cy.get(seats.inviteUsers).click();
    //Invite users
    cy.inviteFreeUser().wait(5000);
    cy.get('@email').then((email) => {
      cy.cancelInvitation('[data-cy="' + email + '"]');
    });
  });

  //Test case to check admin has access to security settings - MFA//
  it('Admin has access to security settings - MFA', () => {
    //Test set up
    cy.loginAdmin().get('h1').should('contain', 'Parts');

    //Check for admin security settings
    cy.CompanySecurityPage().navigateToPartList();
    // .logout()
  });

  it('Non Admin No access to security settings - MFA', () => {
    //Permissions
    cy.loginPlanner().canNotGetAtCompanySecurityPage().logout({ timeout: 5000 });
    cy.loginPlannerWithBilling();
    cy.canNotGetAtCompanySecurityPage();
    cy.logout({ timeout: 5000 });
    cy.loginBilling();
    cy.get(account.SecurityTab).should('not.exist').forceVisit('/settings/security');
    cy.logout({ timeout: 5000 });
    cy.loginReviewer();
    cy.CanNotEnterSettingsMenu();
    cy.logout({ timeout: 5000 });
  });

  //Not in scope currently.//
  // it('Add Operator', () => {
  //   cy.get('#rc-tabs-5-tab-operators > .tab-content').click({ force: true })
  //   cy.get(account.Operators.AddOperator).click()
  //   cy.on("window:confirm", (str) => {
  //     expect(str).to.contain("Update Operator");
  //   })
  //   cy.get(account.Operators.firstName).type(faker.random.alpha(8))
  //   cy.get(account.Operators.lastName).type(faker.random.alpha(8))
  //   cy.get(account.Operators.submitBtn).eq(1).click({ force: true })

  // })
});
