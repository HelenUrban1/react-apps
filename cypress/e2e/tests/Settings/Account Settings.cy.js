//Test Written //

//MY PROFILE//
// User is displayed the correct My Settings navigation options
// User is able to update, save and reset my Profile

//PREFERENCES//
// Preferences, user can change language
// Preferences, user can change GDT Font download option and save button is disabled when no changes are made

// Tests To Do //
// Upload Avatar - including remove avatar for future tests

import { faker } from '@faker-js/faker';
import account from '../../../integration/locators/settings-account.json';
import sideNav from '../../../integration/locators/side-nav-menu-section.json';

describe('My Profile', () => {
  it('User is displayed the correct My Settings navigation options', () => {
    //login to admin user and access my profile
    cy.loginAdmin();
    cy.wait(4000);
    cy.get(sideNav.avatar).click();
    cy.get(sideNav.myProfile).click({ force: true });

    //Check My profile is displayed
    cy.url().should('contain', '/settings/profile');
    cy.get('h1').should('have.text', 'Account Settings');
    cy.get('h3').eq(0).should('have.text', 'My Settings');

    //Check the correct tabs are displayed for admin user
    cy.get(account.MyProfile.sideNavTab).eq(0).should('contain', 'My Profile');
    cy.get(account.MyProfile.sideNavTab).eq(1).should('contain', 'Preferences');
    cy.get(account.MyProfile.sideNavTab).eq(2).should('contain', 'Notifications');

    //my profile tab should be active
    cy.get(account.MyProfile.sideNavTab).eq(0).should('have.class', 'ant-tabs-tab ant-tabs-tab-active');
  });

  it('User is able to update, save and reset my Profile', () => {
    //login to admin user and access my profile
    cy.loginAdmin();
    cy.wait(4000);
    cy.get(sideNav.avatar).click();
    cy.get(sideNav.myProfile).click({ force: true });

    //update users first name, last name and phone number
    cy.wait(6000);
    const fname = faker.random.alpha(5);
    const lname = faker.random.alpha(6);
    const number = faker.random.numeric(10);
    cy.get(account.MyProfile.firstName).should('exist').clear().type(fname, { delay: 1500 });
    cy.get(account.MyProfile.lastName).should('exist').clear().type(lname, { force: true });
    cy.get(account.MyProfile.phoneNumber).should('exist').clear().type(number, { force: true });
    cy.get(account.MyProfile.saveBtn).click({ timeout: 10000 });
    cy.on('window:confirm', (str) => {
      expect(str).to.contain('Profile updated successfully');
    });
    cy.wait(4000);

    //verify the data is saved
    cy.get(account.MyProfile.firstName).should('have.value', fname);
    cy.get(account.MyProfile.lastName).should('have.value', lname);
    cy.get(account.MyProfile.phoneNumber).should('have.value', number);

    //Verify the data is reset is working //
    const fname1 = faker.random.alpha(5);
    const lname1 = faker.random.alpha(6);
    const number1 = faker.random.numeric(10);
    cy.get(account.MyProfile.firstName).clear().type(fname1, { force: true });
    cy.get(account.MyProfile.lastName).clear().type(lname1, { force: true });
    cy.get(account.MyProfile.phoneNumber).clear().type(number1, { force: true });

    //verify the data is updated
    cy.get(account.MyProfile.firstName).should('have.value', fname1);
    cy.get(account.MyProfile.lastName).should('have.value', lname1);
    cy.get(account.MyProfile.phoneNumber).should('have.value', number1);
    //cy.wait(4000)
    cy.get(account.MyProfile.resetBtn).click({ force: true });
    cy.on('window:confirm', (str) => {
      expect(str).to.contain('Profile updated successfully');
    });
    cy.get(account.MyProfile.firstName).should('have.value', fname);
    cy.get(account.MyProfile.lastName).should('have.value', lname);
    cy.get(account.MyProfile.phoneNumber).should('have.value', number);
  });
});

describe('Preferences', () => {
  it('Preferences, user can change language', () => {
    //login to admin user and access preferences
    cy.loginAdmin();
    cy.wait(4000);
    cy.get(sideNav.avatar).click();
    cy.get(sideNav.mySettings).click({ force: true });

    // Check correct page is displayed and tab is active
    cy.url().should('contain', '/settings/preferences');
    cy.get('h1').should('have.text', 'Account Settings');
    cy.get('h3').eq(0).should('have.text', 'My Settings');
    cy.get(account.MyProfile.sideNavTab).eq(1).should('have.class', 'ant-tabs-tab ant-tabs-tab-active');

    //Check current language is English
    cy.get(account.Preferences.language).should('exist');
    cy.contains('English');

    //Change language to Português //Language change is disabled for now

    // cy.get(account.Preferences.language).click();
    // cy.contains('Português').click();
    // cy.wait(4000);
    // cy.get(account.Preferences.language).should('be.visible');
    // cy.contains('Português');

    //Change language back to English
    // cy.get(account.Preferences.language).click();
    // cy.contains('English').click();
    // cy.wait(4000);
    // cy.get(account.Preferences.language).should('be.visible');
    // cy.contains('English');
  });

  it('Preferences, user can change GDT Font download option and save button is disabled when no changes are made', () => {
    //login to admin user and access preferences
    cy.loginAdmin();
    cy.wait(4000);
    cy.get(sideNav.avatar).click();
    cy.get(sideNav.mySettings).click({ force: true });

    //Check correct page is displayed and tab is active
    cy.url().should('contain', '/settings/preferences');
    cy.get('h1').should('have.text', 'Account Settings');
    cy.get('h3').eq(0).should('have.text', 'My Settings');
    cy.get(account.MyProfile.sideNavTab).eq(1).should('have.class', 'ant-tabs-tab ant-tabs-tab-active');
    cy.wait(4000);

    //save button is disabled
    cy.get(account.Preferences.saveBtn).should('exist').should('be.disabled');

    //check user can uncheck GDT Font checkbox
    cy.get(account.Preferences.GDTcheckbox).should('exist').uncheck().should('be.not.checked');

    //save button is not disabled
    cy.get(account.Preferences.saveBtn).should('exist').should('not.be.disabled');

    //Move away from page and return check GDT Font checkbox is in original state
    cy.get(account.MyProfile.sideNavTab).eq(0).click();
    cy.get(account.MyProfile.sideNavTab).eq(1).click();
    cy.get(account.Preferences.GDTcheckbox).should('exist').check().should('be.checked');
    cy.get(account.Preferences.saveBtn).should('exist').should('be.disabled');

    //Uncheck GDT Font checkbox, save, move away from page and return check GDT Font checkbox is in updated state

    cy.get(account.Preferences.GDTcheckbox).should('exist').uncheck().should('not.be.checked');
    cy.get(account.Preferences.saveBtn).should('exist').click();
    cy.get(account.MyProfile.sideNavTab).eq(0).click();
    cy.get(account.MyProfile.sideNavTab).eq(1).click();
    cy.get(account.Preferences.GDTcheckbox).should('exist').should('not.be.checked');

    //Check checkbox and save
    cy.get(account.Preferences.GDTcheckbox).should('exist').should('be.not.checked').check().should('be.checked');
    cy.get(account.Preferences.saveBtn).should('exist').should('be.enabled');

    //Move away from page and return check GDT Font checkbox is in updated state
    cy.get(account.MyProfile.sideNavTab).eq(0).click();
    cy.get(account.MyProfile.sideNavTab).eq(1).click();
    cy.get(account.Preferences.GDTcheckbox).should('exist').should('be.not.checked');

    //Check checkbox and save, check tool tip is displayed
    cy.get(account.Preferences.GDTcheckbox).should('exist').should('be.not.checked').check().should('be.checked');
    cy.get(account.Preferences.saveBtn).should('exist').should('be.enabled').click();
    cy.get(account.Preferences.saveBtn).should('exist').should('be.disabled');
    cy.get('.ant-tooltip-inner').should('exist').should('have.text', 'Settings Up To Date');
  });
});
