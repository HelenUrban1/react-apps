// Tests Written //

// Correct settings options are displayed
// Has access to create new part
// Has access to add a new part doc to a part
// Has access to create new report on a part
// Has access to create new template
// Has access to billing

// Tests To Do //
// Add a new job and delete it  - Blocked as currently disabled
// Can access configurations


import 'cypress-file-upload'
import partList from "../../../integration/locators/part-list.json"
import partInformation from "../../../integration/locators/part-information-section.json"
import account from "../../../integration/locators/settings-account.json"
import sideNav from "../../../integration/locators/side-nav-menu-section.json"
import templates from "../../../integration/locators/Templates.json"


describe('Admin Permissions', () => {

    it('Correct Settings options are displayed', () => {
        cy.loginAdmin()

        //My Settings - Preferences
        cy.get(sideNav.avatar).click()
        cy.get(sideNav.mySettings).click({ force: true })

        // Check correct page is displayed and tab is active
        cy.url().should('contain', '/settings/preferences');
        cy.get('h1').should('have.text', 'Account Settings')
        cy.get('h3').eq(0).should('have.text', 'My Settings')
        cy.get(account.MyProfile.sideNavTab).eq(1).should('have.class', 'ant-tabs-tab ant-tabs-tab-active')

        //Check the correct tabs are displayed for Admin user
        cy.get(account.MyProfile.sideNavTab).eq(0).should('contain', 'My Profile')
        cy.get(account.MyProfile.sideNavTab).eq(1).should('contain', 'Preferences')
        cy.get(account.MyProfile.sideNavTab).eq(2).should('contain', 'Notifications')

    })

    it('Has access to create new part', () => {
        cy.loginAdmin()

        cy.get(partList.newPartBtn).should('be.be.visible')
    })

    it('Has access to add a new part doc to a part', () => {
        cy.loginAdmin()
        // add new part
        cy.addPart()
        //add part number
        cy.addPartNumber()
        //Navigate to parts home
        cy.navigateToPartList()

        //access sideWindowWrapper to check add doc access
        cy.get(partList.sideWindowWrapper.partDocumentSection).eq(0).trigger('mouseover')
        cy.get(partList.sideWindowWrapper.attachDocument).should('exist')

        //delete part
        cy.deletePart()
    })

    it('Has access to create new report on a part', () => {
        cy.loginAdmin()
        // add new part
        cy.addPart()
        //add part number
        cy.addPartNumber()
        //Navigate to parts home
        cy.navigateToPartList()
        
        //access sideWindowWrapper to check create report access
        cy.get(partList.sideWindowWrapper.savedReports).eq(0).trigger('mouseover')
        cy.get(partList.sideWindowWrapper.createNewReport).should('exist')
        
         //delete part
        cy.deletePart()
    })

    it('Has access to create report template', () => {

        cy.loginAdmin()
        // Access templates page
        cy.get(sideNav.partsIcon).trigger('mouseover')
        cy.get(templates.submenuPopup).should('be.visible')
        cy.contains('Templates').should('be.visible').click()
        cy.get('h2').should('be.visible').and('contain', 'Template Gallery')

        // Attach new template is visible
        cy.get(templates.createNewTemplateTile).should('be.visible').and('have.text', "Create New Template")
        cy.get(templates.addNewTemplate.templateUploadField).should('be.visible')
    })

    it('Has access to billing ', () => {
        cy.loginAdmin()
        //My Settings - Preferences
        cy.get(sideNav.settings).trigger('mouseover')
        cy.get(sideNav.billing).click()
        cy.url().should('include', '/settings/billing')
        cy.get('h1').should('contain', 'Account Settings')
        cy.get('h2').should('contain', 'Billing')
        cy.get('h3').should('contain', 'Billing History & Invoices')

    })
})