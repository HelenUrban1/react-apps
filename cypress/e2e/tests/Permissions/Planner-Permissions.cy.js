// Functional area     Test case                           Status

// Planner permission  Can  add / edit user settings       Done
//                     Can add parts, create reports       Done
//                     Can create report template          Done
//                     Can view reports, parts             Done

import 'cypress-file-upload'
import partList from "../../../integration/locators/part-list.json"
import account from "../../../integration/locators/settings-account.json"
import sideNav from "../../../integration/locators/side-nav-menu-section.json"
import templates from "../../../integration/locators/Templates.json"

//TO DO:
// Company Settings to be added


describe('IXC-2543 UI Automation - Planner Permissions', () => {

    it('IXC-2543 UI Automation - Planner Permissions - Correct Settings options are displayed', () => {
        cy.loginPlanner()
        //My Settings - Preferences
        cy.get(sideNav.avatar).click()
        cy.get(sideNav.mySettings).click({ force: true })

        // Check correct page is displayed and tab is active
        cy.url().should('contain', '/settings/preferences');
        cy.get('h1').should('have.text', 'Account Settings')
        cy.get('h3').eq(0).should('have.text', 'My Settings')
        cy.get(account.MyProfile.sideNavTab).eq(1).should('have.class', 'ant-tabs-tab ant-tabs-tab-active')

        //Check the correct tabs are displayed for Planner user
        cy.get(account.MyProfile.sideNavTab).eq(0).should('contain', 'My Profile')
        cy.get(account.MyProfile.sideNavTab).eq(1).should('contain', 'Preferences')
        cy.get(account.MyProfile.sideNavTab).eq(2).should('contain', 'Security')
        cy.get(account.MyProfile.sideNavTab).eq(3).should('contain', 'Notifications')

    })

    it('IXC-2543 UI Automation - Planner Permissions - Has access to create new part', () => {
        cy.loginPlanner()
        cy.get(partList.newPartBtn).should('be.be.visible')
    })

    it('IXC-2543 UI Automation - Planner Permissions - Has access to add a new part doc to a part', () => {
        cy.loginPlanner()
        //add new Part
        cy.addPart()
        cy.addPartNumber()

        //Navigate to parts home
        cy.navigateToPartList()

        //access sideWindowWrapper to check add doc access
        cy.get(partList.sideWindowWrapper.partDocumentSection).eq(0).trigger('mouseover')
        cy.get(partList.sideWindowWrapper.attachDocument).should('exist')
        cy.deletePart()
    })

    it('IXC-2543 UI Automation - Planner Permissions - Has access to create new report on a part', () => {
        cy.loginPlanner()
        //add new Part
        cy.addPart()
        cy.addPartNumber()

        //Navigate to parts home
        cy.navigateToPartList()

        //access sideWindowWrapper to check create report access

        cy.get(partList.sideWindowWrapper.savedReports).eq(0).trigger('mouseover')
        cy.get(partList.sideWindowWrapper.createNewReport).should('exist')
        cy.deletePart()
    })

    it('IXC-2543 UI Automation - Planner Permissions - has access to create report template', () => {
        cy.loginPlanner()
        cy.get(partList.newPartBtn).should('exist')

        // Access templates page
        cy.get(sideNav.partsIcon).trigger('mouseover')
        cy.get(templates.submenuPopup).should('be.visible')
        cy.contains('Templates').should('be.visible').click()
        cy.get('h2').should('be.visible').and('contain', 'Template Gallery')

        // Attach new template is visible
        cy.get(templates.createNewTemplateTile).should('be.visible').and('have.text', "Create New Template")
        cy.get(templates.addNewTemplate.templateUploadField).should('be.visible')
    })
})