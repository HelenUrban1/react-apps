/**
 * @file Cypress test for login and logout journey
 * @description This file contains Cypress tests for the login and logout journey.
 * @module LoginAndLogoutJourney
 * @created 19/01/2024
 * @lastUpdated 22/01/2024
 * @ticket https://ideagen.atlassian.net/browse/IXC-2453
 */

import authentication from '../../../integration/locators/authentication.json';
import common from '../../../integration/locators/common.json';

const flag = Cypress.env('flag');

const environments = [
  
  {
    name: 'dev',
    path: 'https://www.ixc-dev.com/auth/signin',
    roles: [
      { role: 'admin', username: 'devAdminUsername', password: 'devAdminPassword' },
      { role: 'planner', username: 'devPlannerUsername', password: 'devPlannerPassword' },
      { role: 'plannerwithbilling', username: 'devPlannerwithbillingUsername', password: 'devPlannerwithbillingPassword' },
      { role: 'reviewer', username: 'devReviewerUsername', password: 'devReviewerPassword' },
      { role: 'billing', username: 'devBillingUsername', password: 'devBillingPassword' },
      { role: 'inactive', username: 'devInactiveUsername', password: 'devInactivePassword' },
    ],
  },
  {
    name: 'qa',
    path: 'https://www.ixc-qa.com/auth/signin',
    roles: [
      { role: 'admin', username: 'adminUsername', password: 'adminPassword' },
      { role: 'planner', username: 'plannerUsername', password: 'plannerPassword' },
      { role: 'plannerwithbilling', username: 'plannerwithbillingUsername', password: 'plannerwithbillingPassword' },
      { role: 'reviewer', username: 'reviewerUsername', password: 'reviewerPassword' },
      { role: 'billing', username: 'billingUsername', password: 'billingPassword' },
      { role: 'inactive', username: 'inactiveUsername', password: 'inactivePassword' },
    ],
  },
  {
    name: 'stage',
    path: 'https://www.ixc-stage.com/auth/signin',
    roles: [
      { role: 'admin', username: 'stageAdminUsername', password: 'stageAdminPassword' },
      { role: 'planner', username: 'stagePlannerUsername', password: 'stagePlannerPassword' },
      { role: 'plannerwithbilling', username: 'stagePlannerwithbillingUsername', password: 'stagePlannerwithbillingPassword' },
      { role: 'reviewer', username: 'stageReviewerUsername', password: 'stageReviewerPassword' },
      { role: 'billing', username: 'stageBillingUsername', password: 'stageBillingPassword' },
      { role: 'inactive', username: 'stageInactiveUsername', password: 'stageInactivePassword' },
    ],
  },
  // Add more environments here if needed
];

describe.only('Login and Logout Tests', () => {
  const environment = environments.find((env) => env.name === flag);
  const { name, path, roles } = environment;
  it(`should not login with invalid password in ${name} environment`, () => {
    environments.forEach((environment) => {
      const { name } = environment;

      // Login
      cy.visit(path);
      cy.get(common.fields.email).type(Cypress.env('adminUsername'), { log: false });
      cy.get(common.fields.password).type('invalidpassword', { log: false });
      cy.get(authentication.loginPage.loginBtn).click();

      // Assert login unsuccessful
      cy.url().should('include', '/auth/signin');
      cy.get(common.fields.email).should('be.visible');
      cy.get(common.fields.password).should('be.visible');
      cy.contains(authentication.errorMessages.passwordInvalid).should('be.visible');
    });
  });

  it(`should not login with invalid email and password in ${name} environment`, () => {
    environments.forEach((environment) => {
      const { name } = environment;

      // Login
      cy.visit(path);
      cy.get(common.fields.email).type('NotAnEmail', { log: false });
      cy.get(common.fields.password).type('invalidpassword', { log: false });
      cy.get(authentication.loginPage.loginBtn).click();

      // Assert login unsuccessful
      cy.url().should('include', '/auth/signin');
      cy.get(common.fields.email).should('be.visible');
      cy.get(common.fields.password).should('be.visible');
      cy.contains(authentication.errorMessages.emailInvalid).should('be.visible');
    });
  });

  it(`should not login with invalid email and blank password in ${name} environment`, () => {
    environments.forEach((environment) => {
      const { name } = environment;

      // Login
      cy.visit(path);
      cy.get(common.fields.email).type('NotAnEmail', { log: false });
      cy.get(common.fields.password).clear();
      cy.get(authentication.loginPage.loginBtn).click();

      // Assert login unsuccessful
      cy.url().should('include', '/auth/signin');
      cy.get(common.fields.email).should('be.visible');
      cy.get(common.fields.password).should('be.visible');
      cy.contains(authentication.errorMessages.emailInvalid).should('be.visible');
    });
  });

  it(`should not login with blank email and invalid password in ${name} environment`, () => {
    environments.forEach((environment) => {
      const { name } = environment;

      // Login
      cy.visit(path);
      cy.get(common.fields.email).clear();
      cy.get(common.fields.password).type('invalidpassword', { log: false });
      cy.get(authentication.loginPage.loginBtn).click();

      // Assert login unsuccessful
      cy.url().should('include', '/auth/signin');
      cy.get(common.fields.email).should('be.visible');
      cy.get(common.fields.password).should('be.visible');
      cy.contains(authentication.errorMessages.emailRequired).should('be.visible');
    });
  });

  it(`should not login with invalid email in ${name} environment`, () => {
    environments.forEach((environment) => {
      const { name } = environment;

      // Login
      cy.visit(path);
      cy.get(common.fields.email).type('NotAnEmail', { log: false });
      cy.get(common.fields.password).type(Cypress.env('adminPassword'), { log: false });
      cy.get(authentication.loginPage.loginBtn).click();

        // Assert login unsuccessful
        cy.url().should('include', '/auth/signin');
        cy.get(common.fields.email).should('be.visible');
        cy.get(common.fields.password).should('be.visible');
        cy.contains(authentication.errorMessages.emailInvalid).should('be.visible');
      });
    });

    it(`not login with blank email and password in ${name} environment`, () => {
      // Login
      cy.visit(path);
      cy.get(common.fields.email).clear();
      cy.get(common.fields.password).clear();
      cy.get(authentication.loginPage.loginBtn).click();

      // Assert login unsuccessful
      cy.url().should('include', '/auth/signin');
      cy.get(common.fields.email).should('be.visible');
      cy.get(common.fields.password).should('be.visible');
      cy.contains(authentication.errorMessages.emailRequired).should('be.visible');
      cy.contains(authentication.errorMessages.passwordRequired).should('be.visible');
    });

    it(`should not login with blank email in ${name} environment`, () => {
      environments.forEach((environment) => {
        const { name } = environment;

        // Login
        cy.visit(path);
        cy.get(common.fields.email).clear();
        cy.get(common.fields.password).type('superSecure'), { log: false };
        cy.get(authentication.loginPage.loginBtn).click();

        // Assert login unsuccessful
        cy.url().should('include', '/auth/signin');
        cy.get(common.fields.email).should('be.visible');
        cy.get(common.fields.password).should('be.visible');
        cy.contains(authentication.errorMessages.emailRequired).should('be.visible');
      });
    });

    roles.forEach((role) => {
      const { role: roleName, username, password } = role;

      it(`login and logout as ${roleName} in ${name} environment`, () => {
        // Login
        cy.visit(path);
        cy.get(common.fields.email).type(Cypress.env(username), { log: false });
        cy.get(common.fields.password).type(Cypress.env(password), { log: false });
        cy.get(authentication.loginPage.loginBtn).click();
        cy.get('h1').should('be.visible');

        // Assert login success
        if (roleName !== 'inactive') {
          cy.url().should('include', '/');
        } else {
          cy.url().should('include', '/403');
        }

        if (roleName !== 'inactive') {
          // Logout
          cy.logout();
        } else {
          stop();
        }

        if (roleName !== 'inactive') {
          // Assert logout success
          cy.url().should('include', '/auth/signin');
          cy.get(common.fields.email).should('be.visible');
        } else {
          stop();
        }
      });

      it(`should not login with blank password as ${roleName} in ${name} environment`, () => {
        environments.forEach((environment) => {
          const { name } = environment;

          // Login
          cy.visit(path);
          cy.get(common.fields.email).type(Cypress.env(username), { log: false });
          cy.get(common.fields.password).clear();
          cy.get(authentication.loginPage.loginBtn).click();

          // Assert login unsuccessful
          cy.url().should('include', '/auth/signin');
          cy.get(common.fields.email).should('be.visible');
          cy.get(common.fields.password).should('be.visible');
          cy.contains(authentication.errorMessages.passwordRequired).should('be.visible');
        });
      });
    });
  });
