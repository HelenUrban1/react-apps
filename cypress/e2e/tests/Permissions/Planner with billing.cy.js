// Tests Written //
// Can view and edit billing Info
// Can view and edit user settings
// Can add parts and create reports
// Can create report templates
// Can not asign admin

// Test To Do //
// Can view reports and parts - Is this not covered in create?? Can this be removed?

import 'cypress-file-upload';
import partList from "../../../integration/locators/part-list.json";
import sideNav from "../../../integration/locators/side-nav-menu-section.json";
import templates from "../../../integration/locators/Templates.json";
import seats from "../../../integration/locators/Seats.json"

const fileName = 'addTemplate.xlsx'

require('cypress-iframe')
describe('Planner with billing', () => {

    beforeEach(() => {
       cy.loginPlannerWithBilling()
    })

    it('Can view reports and parts', () => {
        cy.loginPlannerWithBilling()
        //can view parts and new part button //
        cy.get(partList.newPartBtn).should('exist');
        //bug currently for create new jobs //

    })

    it('Can view and edit billing Info', () => {
        cy.loginPlannerWithBilling()
        cy.get(sideNav.settings).trigger('mouseover')
        cy.get(sideNav.billing).click()
        cy.url().should('include', '/settings/billing')
        cy.get('h1').should('contain', 'Account Settings')
        cy.get('h2').should('contain', 'Billing')
        cy.get('h3').should('contain', 'Billing History & Invoices')

        // billing subscription edit allowed //
        cy.billingSubscriptionEditAllowed()

        //Add a new card//
        cy.addNewCard()

        //Remove the card //
        cy.removeCard()
    })

    it('Can view and edit user settings', () => {
        cy.loginPlannerWithBilling()
        //access company profile
        cy.get(sideNav.settings).trigger('mouseover')
        cy.get(sideNav.companyProfile).click()
        cy.url().should('include', '/settings/account')
        //access seats
        cy.get(sideNav.settings).trigger('mouseover')
        cy.get(sideNav.seats).click()
        cy.url().should('include', '/settings/seats')

        //access operators Commented out as not needed in 1.2.2
        // cy.get(sideNav.settings).trigger('mouseover')
        // cy.get(sideNav.operators).click()
        // cy.url().should('include', '/settings/operators')

        //access billing
        cy.get(sideNav.settings).trigger('mouseover')
        cy.get(sideNav.billing).click()
        cy.url().should('include', '/settings/billing')
        //access config
        cy.get(sideNav.settings).trigger('mouseover')
        cy.get(sideNav.configurations).click()
        cy.url().should('include', '/settings/configs')
    })

    //Create reports
    it('Can add parts and create reports', () => {
        cy.loginPlannerWithBilling()
        cy.addPart()
        cy.navigateToPartList()
        cy.get(partList.parttable).should('exist')
        cy.contains('test').first().click()
        cy.get(partList.sideWindowWrapper.createNewReport).click()
        cy.get(templates.template_fai).click()
        cy.get(templates.createReport.nextBtn).click()
    })

    it('Can create report templates', () => {
        cy.loginPlannerWithBilling()
        cy.get(sideNav.partsIcon).trigger('mouseover')
        cy.get(sideNav.templates)
            .should('be.visible')
            .click()
        //Just validates getting to /templates
        cy.contains('Manage Your Templates')

        cy.get(templates.addNewTemplate.newTemplateUpload).attachFile({ filePath: `/${fileName}`, encoding: "base64" }, { subjectType: 'drag-n-drop' })
        //the template is opened to build it -will not automate this //
    })

    it('Can not asign admin', () => {
        cy.loginPlannerWithBilling()
        //access seats
        cy.get(sideNav.settings).trigger('mouseover')
        cy.get(sideNav.seats).click()
        cy.url().should('include', '/settings/seats')
        const email = 'spartan.ideagen+planner@gmail.com'
        const invitedUser ='[data-cy="' + email +'"]'
        cy.get(invitedUser + seats.seatsList.role).click()
        cy.get('[data-cy="Admin-val"]').should('not.exist')
    })
})

