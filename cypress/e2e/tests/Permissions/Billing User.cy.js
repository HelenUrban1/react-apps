// Tests Written //
// Billing free seat user can only view and edit billing Info
// Cannot access parts
// Cannot access templates
// Cannot access configurations
// Cannot access seats

import billing from '../../../integration/locators/billing.json';
import common from '../../../integration/locators/common.json';

describe('Billing User permissions able to access', () => {
    it('Billing free seat user can only view and edit billing Info', () => {
        cy.loginBilling();
        cy.url().should('include', '/settings/billing');
        cy.get('h1').should('contain', 'Account Settings');
        cy.get('h2').should('contain', 'Billing');
        cy.get('h3').should('contain', 'Billing History & Invoices');

        // billing subscription edit allowed //
        cy.billingSubscriptionEditAllowed();
        //Add a new card//
        cy.addNewCard();

        //Remove the card //
        cy.get(billing.AddCard.removeCard).click({ force: true });
        cy.on('window:confirm', (str) => {
            expect(str).to.contain('Removing your card will switch you to Pay-By-Invoice');
        });
        cy.get(billing.AddCard.okbtn).click({ force: true });
        cy.get(billing.AddCard.maskedCardNum).should('not.exist');
        cy.get(billing.AddCard.expDate).should('not.exist');
    });
});

describe('Billing User permissions unable to access', () => {
    beforeEach(() => {
        cy.loginBilling();
        cy.url().should('include', '/settings/billing');
        cy.get('h1').should('contain', 'Account Settings');
    });

    it('Can not access parts', () => {
        cy.visit('/parts', { failOnStatusCode: false });
        cy.get(common.titles.title).should('contain', '403');
    });

    it('Can not access templates', () => {
        cy.visit('/templates', { failOnStatusCode: false });
        cy.get(common.titles.title).should('contain', '403');
    });

    it('Can not access configurations', () => {
        cy.visit('/settings/configs', { failOnStatusCode: false });
        cy.get(common.titles.title).should('contain', '403');
    });

    it('Can not visit seats', () => {
        cy.visit('/settings/seats', { failOnStatusCode: false });
        cy.get(common.titles.title).should('contain', '403');
    });
});
