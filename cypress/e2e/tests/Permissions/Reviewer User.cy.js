/**
 * @fileoverview This file contains test cases for the permissions of a Reviewer free seat user.
 * The test cases include checking the user's ability to view reports, view billing, add new parts,
 * create new templates, edit company settings, delete parts, edit parts, delete templates, edit templates,
 * and access billing via direct URL.
 * @module Permissions/ReviewerUser
 */

import 'cypress-file-upload';
require('cypress-iframe');
import partList from '../../../integration/locators/part-list.json';
import sideNav from '../../../integration/locators/side-nav-menu-section.json';
import templates from '../../../integration/locators/Templates.json';

describe('Reviewer permissions', () => {
  beforeEach(() => {
    cy.loginReviewer();
  });
  it('Reviewer free seat user can view reports', () => {
    cy.get(partList.newPartBtn).should('not.exist');
  });

  it('Reviewer free seat user cannot view billing', () => {
    cy.get('.anticon .ant-avatar-string').trigger('mouseover');
    cy.get(sideNav.mySettings).should('not.contain', 'Billing');
  });

  it('Reviewer free seat user cannot add new parts', () => {
    cy.get(partList.newPartBtn).should('not.exist');
  });

  it('Reviewer free seat user cannot create new template', () => {
    cy.get(partList.newPartBtn).should('not.exist');
    cy.get(sideNav.partsIcon).click();
    cy.get(sideNav.templates).click();
    cy.get(templates.newTemplate).should('not.exist');
  });

  it('Reviewer free seat user cannot edit company settings', () => {
    cy.contains('Company Profile').should('not.exist');
    // Navigate but not get to /settings/account
    cy.visit('/settings/account', { failOnStatusCode: false }).should('not.eq', '/settings/account');
    cy.get(partList.newPartBtn).should('not.exist');
  });

  it('Reviewer free seat user cannot delete parts', () => {
    cy.get(partList.deletePartIcon).should('not.exist');
  });

  it('Reviewer free seat user cannot edit parts', () => {
    cy.get(partList.iconButtonPartExtract).should('not.exist');
  });

  it('Reviewer free seat user cannot delete templates', () => {
    cy.get(partList.newPartBtn).should('not.exist');
    cy.get(sideNav.partsIcon).click();
    cy.get(sideNav.templates).click();
    cy.get(templates.deleteTemplate).should('not.exist');
  });

  it('Reviewer free seat user cannot edit templates', () => {
    cy.get(sideNav.partsIcon).click();
    cy.wait(1000);
    cy.get(sideNav.templates).click();
    cy.wait(1000);
    cy.get(partList.templateCardEdit).should('not.exist');
  });

  it('Reviewer free seat user cannot access billing via direct URL', () => {
    cy.visit('/billing', { failOnStatusCode: false }).should('not.eq', '/billing');
  });
});
