// Tests Written //

// PART SEARCH //
// Search a part by partName

// SIDE WRAPPER //
// Verify fields on the side Window wrapper
// Able to attach and delete part document from side wrapper
// Able to attach and delete attached document from side wrapper
// Able to create new report from side wrapper

// Tests To Do //
// New part button exists
// Redirects to the signin page unless user is logged in
// able to navigate thorugh pages of parts
// Update create new report to add delete report (sidewrapper)



// FITLERS APPLIED //
// Filter by customer
// Filter by Review status
// Filter By Access control
// Reset filters

// SORT BY //
// Sort by Part Number
// Sort by Part Name
// Sort by Revision
// Sort by Customer
// Sort by Updated at
// Sort by Review status
// Sort by Access control
// Reset sort? 

const fileName = 'test.pdf'
const fileName1 = 'test2.PDF'

import { faker } from '@faker-js/faker'
import 'cypress-file-upload'
import partList from "../../../integration/locators/part-list.json"
import templates from "../../../integration/locators/Templates.json"
import fileUpload from "../../../integration/locators/fileUpload.json"
import 'cypress-file-upload'

describe('Part Search', () => {

  it('Search a part by partName', () => {

    //Test set up
    cy.loginAdmin()

    //Verify table headers //
    cy.get(partList.partTableHeader).get('th').each(elem => {
      let columnHeaders
      columnHeaders = elem.text()
    })
    cy.contains('Part Number')
    cy.contains('Part Name')
    cy.contains('Revision')
    cy.contains('Customer')
    cy.contains('Updated at')
    cy.contains('Review Status')
    cy.contains('Access Control')
    // cy.contains('Autoballoon')

    //Search part using part name//
    cy.get(partList.searchPart).type("IX Multi")

    //assert that the first row contains 'IX Multi'//
    cy.get(partList.parttable).find(partList.firstRow).should('contain', 'IX Multi')
  })

})


describe('Side Wrapper', () => {
  it('Verify fields on the side Window wrapper', () => {
    //Test set up
    cy.loginAdmin()
    //Test side window wrapper
    cy.get(partList.parttable).should('exist')
    cy.contains('test').first().click()

    // check the fields in the side wrapper//
    cy.get(partList.sideWindowWrapper.balloonPart).should('exist')
    cy.get(partList.sideWindowWrapper.partReport).should('exist')

    // cy.get(partList.sideWindowWrapper.exportPart).should('be.visible')
    cy.get(partList.sideWindowWrapper.detailDelete).should('be.visible')
    cy.get(partList.sideWindowWrapper.closeDrawer).should('be.visible')
    cy.get(partList.sideWindowWrapper.partNumber).should('be.visible')
    cy.get(partList.sideWindowWrapper.partName).should('be.visible')
    cy.get(partList.sideWindowWrapper.partNumber).should('be.visible')
    cy.get(partList.sideWindowWrapper.partRevision).should('be.visible')

    //These elments have opacity 0 so not visible//
    cy.get(partList.sideWindowWrapper.createNewReport).should('not.be.visible')
    cy.get(partList.sideWindowWrapper.statusSection).should('be.visible')
    cy.get(partList.sideWindowWrapper.partDocumentSection).should('be.visible')

    //Check the status //
    cy.get(partList.sideWindowWrapper.revisionDropdown).last().click()
    cy.contains('Unverified')
    cy.contains('Ready for Review')
    cy.contains('Verified')
  })
  
  it('Able to attach and delete part document from side wrapper', () => {
    //Test set up
    cy.loginAdmin()

    // Create new part
    cy.addPart()
    cy.addPartNumber()

    //Navigate to parts home
    cy.navigateToPartList()

    //Attach and delete part document from side wrapper
    cy.get(partList.sideWindowWrapper.openPart).eq(1).click()
    cy.wait(3000)
    cy.get(partList.sideWindowWrapper.attachDocument).first().click()
    cy.get(fileUpload.fileUploadArea).should('be.visible')
    cy.get(fileUpload.fileUploadArea).attachFile({ filePath: `/${fileName1}`, encoding: "base64" }, { subjectType: 'drag-n-drop' })
    cy.get(fileUpload.file).should('be.visible')
    cy.get(fileUpload.fileUploadArea).should('be.visible')
    cy.get(fileUpload.fileUploadSaveBtn).click({ force: true })
    cy.get(partList.sideWindowWrapper.cardContent)
    cy.contains('test2.PDF')

    //delete the attached document//
    cy.get(partList.sideWindowWrapper.cardContent).trigger('mouseover')
    cy.get(partList.sideWindowWrapper.documentDelete).click({ force: true })
    cy.on("window:confirm", (str) => {
      expect(str).to.contain("Delete document and any associated items?");
    })
    cy.get(partList.deletePartConfirm).click({ force: true })
    cy.wait(2000)
    cy.get('.card-content').should('not.contain', 'test2.PDF')

    //Clean up
    cy.deletePart()
  })

  it('Able to attach and delete attached document from side wrapper', () => {
    //Test set up
    cy.loginAdmin()

    // Create new part
    cy.addPart()
    cy.addPartNumber()

    //Navigate to parts home
    cy.navigateToPartList()
    cy.wait(6000)
    cy.get(partList.parttable).should('exist')
    cy.contains('test').first().click()

    //Attach and delete attached document from side wrapper
    cy.get(partList.sideWindowWrapper.attachDocument).last().click({ force: true })
    cy.get(fileUpload.fileUploadArea).attachFile({ filePath: `/${fileName1}`, encoding: "base64" }, { subjectType: 'drag-n-drop' })
    cy.get(fileUpload.file).should('be.visible')
    cy.get(fileUpload.fileUploadArea).should('be.visible')
    cy.get(fileUpload.fileUploadSaveBtn).should('be.visible').click()
    cy.get(partList.sideWindowWrapper.cardContent)
    cy.contains('test2.PDF')

    //delete the attached document//
    cy.get(partList.sideWindowWrapper.cardContent).last().trigger('mouseover')
    cy.get(partList.sideWindowWrapper.documentDelete).click({ force: true })
    cy.on("window:confirm", (str) => {
      expect(str).to.contain("Delete document and any associated items?");
    })
    cy.get(partList.deletePartConfirm).click({ force: true })
    cy.get(partList.sideWindowWrapper.cardContent).should('not.contain', 'test2.PDF')

    //Clean up
    cy.deletePart()
  })

  it('Able to create and delete new report from side wrapper', () => {
    //Test set up
    cy.loginAdmin()
    // Create new part
    cy.addPart()
    cy.addPartNumber()

    //Navigate to parts home
    cy.navigateToPartList()
    cy.wait(6000)
    cy.get(partList.parttable).should('exist')
    cy.contains('test').first().click()

    //Create new report
    cy.get(partList.sideWindowWrapper.createNewReport).click()
    cy.get(templates.template_fai).click()
    cy.get(templates.createReport.nextBtn).click()

    //Update report name
    const reportName = faker.random.alphaNumeric(6)
    cy.get(templates.createReport.editReportTitle).click()
    cy.wait(4000)
    cy.get(templates.createReport.reportTitle).type(reportName, {delay:1500});
    cy.get(templates.createReport.nextBtn).click();

    //Check report is in side window
    cy.wait(5000)
    cy.get('@partNumber').then(partNumber => {
    cy.get(partList.firstRow).find('td').eq(2).should('contain', partNumber).click()})
    cy.wait(5000)

    //Delete report

    cy.get(partList.sideWindowWrapper.savedReports).should('be.visible').and('contain', reportName)
    cy.get(partList.sideWindowWrapper.firstSavedReport).should('be.visible').and('contain', reportName).trigger('mouseover')
    cy.get(partList.sideWindowWrapper.deleteFirstSavedReport).click()
    cy.on("window:confirm", (str) => {
      expect(str).to.contain("Are you sure?");
    })
    cy.get(partList.cancelBtn).click({ force: true })
    cy.get(partList.sideWindowWrapper.savedReports).should('be.visible').and('contain', reportName)
    cy.get(partList.sideWindowWrapper.deleteFirstSavedReport).click()
    cy.on("window:confirm", (str) => {
      expect(str).to.contain("Are you sure?");
    })
    cy.get(partList.confirmBtn).click({ force: true })
    cy.get(partList.sideWindowWrapper.savedReports).should('be.visible').and('not.contain', reportName)

    //Clean up
    cy.deletePart()
  })
})