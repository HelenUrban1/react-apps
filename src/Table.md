# Quality Control Professional - Developer help information
This file will be updated as we come across new information in IXOND.

## Developer Table of Contents

- [How to Update EULA](#markdown-header-how-to-update-eula-details)
- [Build Locations](#markdown-header-versions-and-build-locations)
- [Deployment of build artefacts](#markdown-header-deployment-of-build-artifacts)

## How to update EULA details

1. EULA details found in source repository at \Build\common\.
2. The license.htm file is generated using Microsoft Word. 
3. The supplied .docx file from business can be 'saved as' a web page .htm from Word.
4. Any extra folders\files generated can be ignored.
5. Ensure formatting is correct. All heading should be 'Heading1' word format.
6. Once formatted replace the license.htm file at \Build\common\.
7. Compile the main project InspectionXpert_3DOnDemand to apply the new license file.

## Versions and Build Locations

1. This information was originally from document [Uploading Builds](https://ideagenplc.sharepoint.com/:b:/r/sites/InspectionXpert-Development-OnDemand/Shared%20Documents/Dev%20-%20QC-Pro%20(OnDemand)/Uploading%20%26%20Sending%20IXOD%20Builds.pdf?csf=1&web=1&e=jeN9gP).
2. Currently we are only building for Ideagen and Net-Inspect. SolidWorks and Pratt Whitney are no longer used (2023-08-04).
3. Refer to [Jenkins Build Server Set-up](..DeveloperReadme/README.md) for Jenkins server connection details.

| QC Pro Build Name              | Local Build Files C:\JG\IX20_2023\ | S3 Location                                                         | Application Launch Link                                                              
| -------------------------------| ---------------------------------  | ------------------------------------------------------------------- | ------------------------------------------------------------------------------------------- |
| x64 Local Test              | \\#\\x64\\Local\Release            | N\A                                                                 | Copy folder to any PC and then click on the manifest (IXOnDemand.application)        |
| x86 Local Test              | \\#\\x86\\Local\Release            | N\A                                                                 | Copy folder to any PC and then click on the manifest (IXOnDemand.application)        |
| | | | |
| x64 Online Test             | \\#\\x64\\Test\Release             | s3://odstaging.inspectionxpert.com/OnDemand/2.0/Test/x64            | https://odstaging.inspectionxpert.com/OnDemand/2.0/Test/x64/IXOnDemand.application   |
| x86 Online Test             | \\#\\x86\\Test\Release             | s3://odstaging.inspectionxpert.com/OnDemand/2.0/Test/x86            | https://odstaging.inspectionxpert.com/OnDemand/2.0/Test/x86/IXOnDemand.application   |
| x64 Online Prod                | \\#\\x64\\Online\Release           | s3://ondemandtest.inspectionxpert.com/OnDemand/2.0/x64              | http://ondemand.inspectionxpert.com/OnDemand/2.0/x64/ixondemand.application          |
| x86 Online Prod                | \\#\\x86\\Online\Release           | s3://ondemandtest.inspectionxpert.com/OnDemand/2.0/x86              | http://ondemand.inspectionxpert.com/OnDemand/2.0/x86/ixondemand.application          |
| | | | |
| x64 Net&#8209;Inspect Online Test | \\#\\x64\\Net&#8209;Inspect\Release      | s3://odstaging.inspectionxpert.com/OnDemand/Net&#8209;Inspect/x64         | https://odstaging.inspectionxpert.com/OnDemand/Net&#8209;Inspect/x64/IXOnDemand.application |
| x86 Net&#8209;Inspect Online Test | \\#\\x86\\Net&#8209;Inspect\Release      | s3://odstaging.inspectionxpert.com/OnDemand/Net&#8209;Inspect/x86         | https://odstaging.inspectionxpert.com/OnDemand/Net&#8209;Inspect/x86/IXOnDemand.application |
| x64 Net&#8209;Inspect Online Prod    | \\#\\x64\\Net&#8209;Inspect\Release      | s3://ondemandtest.inspectionxpert.com/OnDemand/Net&#8209;Inspect/x64      | https://ondemand.inspectionxpert.com/OnDemand/Net&#8209;Inspect/x64/IXOnDemand.application  |
| x86 Net&#8209;Inspect Online Prod    | \\#\\x86\\Net&#8209;Inspect\Release      | s3://ondemandtest.inspectionxpert.com/OnDemand/Net&#8209;Inspect/x86      | https://ondemand.inspectionxpert.com/OnDemand/Net-Inspect/x86/IXOnDemand.application |

## Deployment of build artifacts

Currently Jenkins Server as 2 build parameters for artifact deployment:<br>
1. DEPLOY_ARTIFACTS - when set to YES, artifacts will be copied to ARTIFACT_BUCKET.<br>
2. ARTIFACT_BUCKET - currently is _s3://qcpro-artifacts_ at https://ideagen.awsapps.com/start#/, account Ideagen-inspectionxpert-legacy.<br>
3. Production and Staging builds can then be deployed manually when scheduled from _s3://qcpro-artifacts_ using script _DeployEnvironment.ps1_.<br>
4. To _DeployEnvironment.ps1_ 2 parameters are required:
   <br>a. Environment value - production or staging. This will either deploy to staging or production s3 buckets from _s3://qcpro-artifacts_. 
   <br>b. Version value - appropriate version e.g. 2023.9.5.38. This will be the from version in _s3://qcpro-artifacts_.