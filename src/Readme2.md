## Table of Contents

- [Current Test/Staging/Production versions ](https://ideagen.atlassian.net/wiki/spaces/IX/pages/39459094544/Qc-pro+Releases)
- [Jenkins Build Server Set-up](#markdown-header-jenkins-build-server-set-up)
- [Developer Machine Set-up](#markdown-header-developer-machine-set-up)
- [Bitbucket Branches](#markdown-header-bitbucket-branches)
- [How to run a Jenkins Build](#markdown-header-how-to-run-a-jenkins-build)
- [All sku's and build Locations](DeveloperReadme/README.md#markdown-header-all-sku's-and-build-locations)
- [Deployment of build artifacts](DeveloperReadme/README.md#markdown-header-deployment-of-build-artifacts)
- [Deployment of qc-pro to Test ](DeveloperReadme/README.md#markdown-header-deployment-to-test)
- [Deployment of qc-pro to Staging ](DeveloperReadme/README.md#markdown-header-deployment-to-staging)
- [Deployment of qc-pro to Production ](DeveloperReadme/README.md#markdown-header-deployment-to-production)
- [How to update the code signing certificate](#markdown-header-how-to-update-the-code-signing-certificate)
- [How to Update EULA](DeveloperReadme/README.md#markdown-header-how-to-update-eula-details)
- [Upgrade of Datakit Libraries](DeveloperReadme/README.md#markdown-header-how-to-upgrade-datakit-libraries)

# Ideagen Quality Control Professional

This repository contains C++, VB.NET and C#.NET source code required for Quality Control Professional software. It includes binaries supplied by third party software.

## Jenkins Build Server Set-up

This will describe the set-up to be performed on a new windows box to generate a Jenkins build of Quality Control Professional.
<br>The below steps were used to rebuild the server hosted on Ideagen VPN _'ec2-16-170-240-247.eu-north-1.compute.amazonaws.com'_. <br>Administrator Logon details can be found in Password Manager in OnDemand space. Password Manager name is _'OnDemand Jenkins Server'_.

**1. Visual Studio 2022 Install**

1. Visual Studio 2022 required for Quality Control Professional built on v141 VS2017 tools set and Windows 10 SDK.
2. Select a custom install and select:
   <br> _'All'_ Visual C++ features are selected.
   <br> _Office Developer Tools for Visual Studio_.
   <br> _ClickOnce Publishing Tools_.
   <br> _.NET Framework 4.8.1 SDK'_ and _.NET Framework 4.8.1 targeting pack_ .
   <br> _MSVC v141 - VS 2017 C++ x64/x86 build tools (v14.16)_.
   <br> _C++ ATL for v141 build tools (x86 & x64)_.
   <br> _C++ MFC for v141 build tools (x86 & x64)_.
   <br> _Windows 10 SDK (10.0.18362.0)_.

**2. Install Git**

1. Install git.
2. Install a git client, it will be easier to resolve issues if you do.

**3. Install Redgate Smart Assembly 8. Used for encrypting .dlls in QC-Pro builds**

1. Download Redgate install from https://www.red-gate.com/Dynamic/account/serials. Credentials can be found in Password Manager as _'red-gate.com'_. The current licence is for Smart Assembly version 8 and for 1 server.
2. Install version SmartAssembly 8 to the default folder C:\Program Files\Red Gate\SmartAssembly 8.
3. Once the installation completes, run Smart Assembly.
4. Click on _'activate'_ and put the serial number as shown at https://www.red-gate.com/Dynamic/account/serials.
5. In order to associate a new server to the licence you need to remove the old server device allocation.

**4. Install Wix Toolset**

1. Download & install from https://github.com/wixtoolset/wix3/releases/. Install version _'3.11.exe'_.
2. Wix installer requires .net Framework 3.5.1 to be enabled. You may need to download this from https://www.microsoft.com/en-us/download/details.aspx?id=22.
3. Download & install the Visual Studio 2022 Wix Extension from
   https://marketplace.visualstudio.com/items?itemName=WixToolset.WixToolsetVisualStudio2022Extension.

**5. Install Jenkins**

1. Go to https://jenkins.io/.
2. Click on download. Select the latest LTS _'.war'_ file for download. (Do not use any other install type).
3. Copy _'jenkins.war'_ to a specific directory.
4. Install the WAR file by following the instructions at https://jenkins.io/doc/book/installing/#war-file.
5. Check what version of Java SDK you require from https://www.jenkins.io/doc/administration/requirements/java/. Version used: https://www.oracle.com/java/technologies/javase/jdk17-archive-downloads.html.
6. Create a .bat file in the same directory as the war. To run use this command instead of just java -jar jenkins.war:
   ```
   java -jar -Xmx1024m jenkins.war
   ```
7. Ensure the bat file runs with Administrator privileges.
8. Create a shortcut of that bat file and place it on desktop.
9. You will now need to run this bat file to run Jenkins manually.
10. Browse to http://localhost:8080 and wait until the Unlock Jenkins page appears.
11. Once unlocked install suggested plugins.
12. Create admin user.
13. Leave the default URL, unless you need to change it if the port is already used.
14. Useful things:
    <br> Install OWASP plugin to have html code in the description field.
    <br> To change the root directory of workspaces (useful for pipelines): Modify _config.xml_ inside C:\Users\Administrator\.jenkins.
    <br> You can set up system variables in the manage Jenkins page without having to create new parameters.

**6. Set-up SSH Credentials for Bitbucket repository cloning**

1. You will need a SSH credential to clone source code from the Bitbucket repository for the Quality Control Professional build.
2. Follow the instructions at https://support.atlassian.com/bitbucket-cloud/docs/set-up-an-ssh-key/ to set up a key pair for Bitbucket.
3. Once the SSH file has been generated go to the left hand panel of Jenkins.
4. Select Jenkins -> Manage Jenkins -> Manage Credentials. http://localhost:8080/manage/credentials/.
5. Add a _'Global Credential'_.
6. Kind is _'SSH Username with Private Key'_.
7. Set the _'ID'_ that you want to recognise that credential.
8. Add a _'description'_.
9. Insert your correct Bitbucket username into _'Username'_.
10. Under _'Private'_ select _'Enter Directly'_. Here you would paste the contents of the Bitbucket private SSH key that generated in step 15. e.g'. Users/administrator/.ssh/id_rsa. You can open this file in notepad to copy out the private key and paste into Jenkins.

**7. Test the SSH credentials are working**

1. Git Clone repository https://bitbucket.org/ideagendevelopment/qc-pro/src/master/ _'git clone git@bitbucket.org:ideagendevelopment/qc-pro.git'_ to a suitable folder, C:\development is currently used on the Jenkins Server.
2. Ensure the repository clones and there are no SSH key errors. If it works here it will work in Jenkins.
3. The clone may take some time as this is a big repository that contains large files.

**8. Code Signing**

1. Once the clone has worked successfully, we need to set-up certificates for code signing. The current process is not ideal and will change in the future.
2. Switch to the development branch e.g '_develop_'. This branch contains the latest Ideagen code signing certificate.
3. Go to the QC-Pro repository and inside _'Build\signing\'_ double click on the file FirstArticle.pfx.
4. Install the ceriticate on the local machine for _'current User'_.
5. Insert the password, you can retrieve this inside the _'Build\CheckFileSigned\CheckFileSigned\CheckFilesSigned.bat'_.
6. Leave as Automatic store.
7. A message should show _'import was successful'_.
8. Re-do everything and install it also for the _'local machine'_.

**9. Create a new Jenkins Job**

1. _'QC-Professional'_ is the main job used to generate QC-Pro builds.
2. Go to Jenkins http://localhost:8080/ and logon. (We need CloudOps to allow inbound traffic in port 8080 to this server).
3. Click on _'new item'_.
4. Specify the name of the job _'QC-Professional'_.
5. Choose _'Free Style Project'_.
6. Click _'Ok'_. Now the job is created.
7. Add a _'Description'_. E.g. Job to use for building _'QC-Professional'_.
8. Go to the _'Source Code Management'_ section of the job.
9. Select _'Git'_ option.
10. Repository url should be set to _'git@bitbucket.org:ideagendevelopment/qc-pro.git_.
11. Specify the SSH credentials that were previously set-up. (Or set-up by clicking _Add_).
12. Specify the branch to build e.g. _'origin/develop'_.

**10. Jenkins additional behaviours recommended:**

1. _Clean before checkout_.
2. '_Clean after checkout_'.
3. '_Timeout_' (in minutes) for clone and fetch operations: 150.
4. '_Fetch Tags_'. Timeout 150. Enable shallow clone with depth of 1.
5. '_Git LFS pull after checkout_'.

**11. Build Triggers** (not yet implemented but recommended)

Poll SCM set to H 10 \* \*
<br> This will check git every ten hours and if new commits are found it will run the job. Modify as you need.

**12. Build Environment**

Check _'Add timestamps to the Console Output'_.

**13. Build Steps**

Here you will add your build commands.

1. Select _'Add Build Step'_.
2. Select _'Execute Windows batch command'_.
3. In command window add:
   ```
   git lfs push
   git lfs pull
   build.bat
   ```

**14. Post build actions** (not yet implemented but recommended)

Send an email to whoever is expecting the build.

**15. How to add a parameter to a Jenkins job**

1. There is a list of parameters passed to _'build.bat'_ which resides in the root of the repository.
2. Go to the Jenkins page for the job _'QC-Professional'_.
3. Go to _'configure'_.
4. Check '_this project is parameterized_' under General.
5. Specify your parameters:
   <br> The parameter name should be uppercase.
   <br> After that, you can use your parameters in the build.bat commands by using %PARAMETER_NAME% where PARAMETER_NAME is your parameter's name.

**16. Parameter List**

As of January 2024 Win32 build will no longer be deployed. All builds going forward will be x64.

| Jenkins Parameter      | Parameter Kind | Default Value                                                                       | Description                                                                                                                                                                                                                               |
| ---------------------- | -------------- | ----------------------------------------------------------------------------------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| DOT_BUILD_NUM          | String         | e.g. 2024.9.7.                                                                      | Version of the release that will be generated. After the final dot, the build process will attach the # of the job instance that's running. Update this to reflect the version that matches latest version in IXApplicationVersions.h,cpp |
| UNDERSCORE_BUILD_NUM   | String         | e.g. 2024\__9_7_                                                                    | Should be the same as above, but with underlines instead of dots                                                                                                                                                                          |
| CURRENT_BUILD_VERSION  | String         | e.g IX20_2024                                                                       | Name of the output folder where the build will be placed when finished. (Inside the HULK_BASE path)                                                                                                                                       |
| HULK_BASE              | String         | C:\JG\                                                                              | Jenkins Generated path of the output folder where the builds will be placed                                                                                                                                                               |
| HULK_BASE_SLASH        | String         | JG                                                                                  |                                                                                                                                                                                                                                           |
| IX_MAGE_EXE_PATH       | String         | C:\Program Files (x86)\Microsoft SDKs\Windows\v10.0A\bin\NETFX 4.6.1 Tools\mage.exe | Path of the mage.exe tool for file signing                                                                                                                                                                                                |
| IXOD                   | Choice         | x64 <br> None                                                                       | Produces RETAIL:<br>Test, Stage, Prod, Local and OnPremise sku's                                                                                                                                                                          |
| NETINSPECT_STAGING     | Choice         | x64 <br> None                                                                       | Produces NET-INSPECT:<br>Test and Staging sku's'                                                                                                                                                                                          |
| NETINSPECT_PRODUCTION  | Choice         | x64 <br> None                                                                       | Produces NET-INSPECT:<br>Production sku                                                                                                                                                                                                   |
| PW_CANADA_STAGING      | Choice         | None <br> x64 <br>                                                                  | Produces PRATT & WHITNEY CANADA:<br>Test and Staging sku's                                                                                                                                                                                |
| PW_CANADA_PRODUCTION   | Choice         | None <br> x64 <br>                                                                  | Produces PRATT & WHITNEY CANADA:<br>Production sku                                                                                                                                                                                        |
| REGRESSION_SUITE_BUILD | Choice         | Yes <br> No                                                                         | This builds only the configuration for Regression Suite, does not run the baseline generation or the file comparison. (Default NO)                                                                                                        |
| ENCRYPTTOOLDIR         | String         | C:\Program Files\Red Gate\SmartAssembly 8\                                          | Path of the SmartAssembly 5 tool used for encryption                                                                                                                                                                                      |
| UNITTEST               | Choice         | Yes <br> None                                                                       | Do unit tests and coverage? <br> Add a "None" option in the job configuration and select it when running a new build to skip unit tests                                                                                                   |
| DEPLOY_ARTIFACTS       | Choice         | Yes <br> No                                                                         | Deploy artifacts to AWS s3 bucket? <br>Choose yes to deploy to s3 ARTIFACT_BUCKET                                                                                                                                                         |
| ARTIFACT_BUCKET        | String         | Path to S3 bucket e.g s3://BucketName                                               | AWS S3 bucket for artifact deployment <br> Used when DEPLOY_ARTIFACTS is Yes                                                                                                                                                              |

**16. Finally**

Click on apply and then save changes. The build should be ready to run:

1. From dashboard click on job _'QC-Professional'_.
2. On left panel select _'Build with parameters'_.
3. Select parameters for build.
4. Select _'Build'_.
5. Build should kick off which could take up to 2 hours depending on parameters chosen.
6. '_Console Output_' displays the build output.
7. Once build has completed build files reside at C:\JG\ as specified by the HULK_BASE parameter. e.g C:\JG\IX20_2025\53\x64.

## Developer Machine Set-up

**1. Visual Studio 2022 Install**

1. Visual Studio 2022 required for Quality Control Professional built on v141 VS2017 tools set and Windows 10 SDK.
2. Install as _'Administrator'_.
3. Select a custom install and select:
   <br> _'All'_ Visual C++ features are selected.
   <br> '_ Office Developer Tools for Visual Studio_'.
   <br> _'ClickOnce Publishing Tools'_.
   <br> _'.NET Framework 4.8.1 SDK'_ and _'.NET Framework 4.8.1 targeting pack'_ .
   <br> _'MSVC v141 - VS 2017 C++ x64/x86 build tools (v14.16)'_.
   <br> _'C++ ATL for v141 build tools (x86 & x64)'_.
   <br> _'C++ MFC for v141 build tools (x86 & x64)'_.
   <br> _'Windows 10 SDK (10.0.18362.0)'_.
4. You will need to run Visual Studio with _'Administrator'_ access to correctly build the application.

**2. Install Git**

1. If you don't yet have git or a git client installed:
   <br> Install git for windows https://git-scm.com/downloads.
   <br> Install a git client.

**3. Install Wix Toolset**

1. Download & install Wix from https://github.com/wixtoolset/wix3/releases. Install version _'3.11.exe'_ as administrator.
2. Wix installer requires .net Framework 3.5.1 to be enabled. You may need to download this from https://www.microsoft.com/en-us/download/details.aspx?id=22.
3. Download & install the Visual Studio 2022 Wix Extension from
   https://marketplace.visualstudio.com/items?itemName=WixToolset.WixToolsetVisualStudio2022Extension.

**4. Install qt.io to alter .ui forms**

1. In order to edit the ui forms you need to Install https://account.qt.io/s/. Details on Password Manager.
2. Download qt-enterprise-windows-x86-5.10.1 and install as administrator.

**5. Set-up SSH Credentials for Bitbucket repository cloning**

1. You will need a SSH credential to clone source code from the repository for the '_Quality Control Professional (QC-pro)_' build.
2. If you do not have an SSH key set-up on Bitbucket then follow the instructions at https://support.atlassian.com/bitbucket-cloud/docs/set-up-an-ssh-key/ to set up a key pair for bitbucket.

**6. Clone the repository**

1. Git Clone repository https://bitbucket.org/ideagendevelopment/qc-pro/src/master/ to a suitable folder.
2. Ensure the repository clones and there are no SSH key errors.
3. The clone may take some time as this is a big repository that contains large files.
4. Switch to branch '_develop_'.
5. From a git cmd run: `git lfs pull`.
6. Ensure the repository is ok by doing a revert to see if any files have changed. I have found oddities with this big repository where some files seem missing and a file revert brings them down off from Bitbucket.

**7. Build Solution from Visual Studio**

1. Open Visual Studio as _'Administrator'_.
2. Open solution '_Build\IX_Cadnostoc_RED.sln_'.
3. Change solution configuration to _'release_debug x64'_ .
4. Main start up project is _'Main Solution\InspectionXpert_3DOnDemand'_.
5. Build the Main Project and it should succeed in building.
6. Compiled files will be in _'Build\x64\Release_debug'_. InspectionXpert OnDemand.exe should run.
7. RedGate Smart Assembly 8 should not be needed for development.
8. Code Signing certificates should not be needed unless there is a change to the certificate. See below to update the code signing certificate.

**8. Build Solution on local machine using build.bat**

Useful to test changes made to the build script: build.bat.

1. Open Visual Studio Code as '_Administrator'_.
2. Open _'qc-pro'_ project folder.
3. Open build script '_build.bat_'.
4. Uncomment lines between
   <br>:: These variables are set via the Jenkins Job usually.
   <br>:: End
5. Set '_HULK_BASE_' to a path on your local machine.
6. Ensure you have set variables to build output you want to produce.
7. ctrl + ~ to open command window. New command prompt.
8. From '_qc-pro_' command prompt directory run `build.bat > output.txt`
9. This will begin the build locally and write output to '_output.txt_'. This is useful for troubleshooting.
10. You will probably need to install '_FirstArticle.pfx_'.

**9. Solution Configurations**

As of January 2024 Win32 build will no longer be deployed. All builds going forward will be x64.

| Configuration                                    | In Production build.bat | Description                                                                                                                                                                                            |
| ------------------------------------------------ | ----------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| <br>**Retail**<br><br>                           |
| Release                                          | ✔                       | Used for creating the sku Retail QC-Pro binaries                                                                                                                                                       |
| Release_Installer                                | ✔                       | Used for creating the clickonce application for Retail QC-Pro build. It requires the output generated from Release compile to produce Test, Staging, Production, Local and OnPremise                   |
| Release_Debug                                    | ✖                       | Used for creating the build with dump information. **This is the best configuration to use for local compiling and debug. Use x64**                                                                    |
| Release_Debug_Installer                          | ✖                       | Used for creating the clickonce application which have dump information creating enabled, it requires the output generated from Release_Debug build                                                    |
| <br>**Net-Inspect**<br><br>                      |
| NetInspect                                       | ✔                       | Used for sku Net-Inspect. Runs for Production. External compiler option of /DNETINSPECT_STAGING is added for test/staging compile in build.bat                                                         |
| NetInspectInstaller                              | ✔                       | Used for creating the clickonce application for Net-Inspect                                                                                                                                            |
| <br>**Pratt & Whitney (no longer used)**<br><br> |
| Pratt_and_Whitney_Canada_Release                 | ✔                       | Used for sku Pratt and Whitney Canada staging\test build                                                                                                                                               |
| PW_Canada_Installer                              | ✔                       | Used for creating the clickonce application for staging\test Pratt and Whitney Canada                                                                                                                  |
| PWD_Canada_Release                               | ✔                       | Used for sku Pratt and Whitney Canada Production                                                                                                                                                       |
| PWD_Canada_Installer                             | ✔                       | Used for creating the clickonce application for Pratt and Whitney Canada Production                                                                                                                    |
| Pratt and Whitney                                | ✖                       | Not used                                                                                                                                                                                               |
| Pratt_and_Whitney_Release                        | ✖                       | Used for creating the Pratt and Whitney USA staging binaries. This is not used                                                                                                                         |
| PW_Installer                                     | ✖                       | Used for creating the clickonce application for PW Usa Staging, it requires the output generated from Pratt_and_Whitney_Release USA. This is not used                                                  |
| PWD_Release                                      | ✖                       | Used for creating the PW USA Production binaries                                                                                                                                                       |
| PWD_Installer                                    | ✖                       | Used for creating the clickonce application for PW USA Production, it requires the output generated from PWD_Release compile.                                                                          |
| <br>**SolidWorks (no longer used)**<br><br>      |
| Release_SWI                                      | ✖                       | Used for creating the SOLIDWORKS Inspection build, to successfully compile this configuration you need to first compile the TrainableOCR.sln                                                           |
| Release_SWI_2020                                 | ✖                       | Used for creating the SOLIDWORKS Inspection 2020, to successfully build this configuration you need to first build the TrainableOCR.sln                                                                |
| ReleaseWith2015_2                                | ✖                       | Used for creating the Datakit 2015 DLL which we have currently removed from the binaries package.(Previously whenever we do the changes in DatakitWrapper code we update the 2015 DLL using 2015 libs) |
| <br>**Other**<br><br>                            |
| UnitTest                                         | ✔                       | Used for performing the Unit Test run                                                                                                                                                                  |
| RegressionSuite_Release                          | ✔                       | Used for creating and executing the regression suite                                                                                                                                                   |
| OnDemandLicensing-UnitTests                      | ✔                       | Was added because the ondemand licensing libs were added to the solution                                                                                                                               |
| Darksite_Release                                 | ✖                       | Used for creating the OnPremise build, this requires the output from the Release build.                                                                                                                |
| Darksite_Release_AutoFAIR                        | ✖                       | Used for creating the clickonce application for Boeing build, this requires the output from the Darksite_Release_AutoFAIR build.                                                                       |
| Darksite_Release_AutoFAIR_Installer              | ✖                       | Used for creating the clickonce application for Boeing build, this requires the output from the Darksite_Release_AutoFAIR build.                                                                       |
| Debug                                            | ✖                       | Not used ATM(We can remove this configuration as we already have configuration which we can use for Debugging (Release_Debug)                                                                          |
| Dev_Config                                       | ✖                       | Not used. (We can remove this configuration as we already have configuration which we can use for Debugging (Release_Debug)                                                                            |

## Bitbucket Branches

| Branch      | Location                                           | Description                                                                        |
| ----------- | -------------------------------------------------- | ---------------------------------------------------------------------------------- |
| Legacy      | origin/sp9.3                                       | Branched at commit 97143db for Ideagen handover                                    |
| Development | origin/develop                                     | Product Development https://bitbucket.org/ideagendevelopment/qc-pro/branch/develop |
| Releases    | origin/release/major.minor e.g. origin/release/9.7 | Production releases with a git tag. Tag e.g '_release/2023.9.7.53_'                |
| Master      | origin/master                                      | Product Production https://bitbucket.org/ideagendevelopment/qc-pro/branch/master   |

**Procedure when Development branch is ready for Release**

1. Create _'release/major.minor'_ branch off develop. e.g release/9.7
2. If any changes required in uat do these on _'release/sp9.7'_. When UAT ok merge _'release/sp9.7'_ to _'develop'_.
3. Git Tag _'release/sp9.7'_ to e.g _'release/2024.9.7.53'_ when released to production.
4. Fast Forward merge _'release/sp9.7'_ to _'master'_.
5. Merge _'master'_ back to _'develop'_ to ensure all changes exist in _'develop'_.

## How to update the code signing certificate

1. Name new signing certificate _'FirstArticle.pfx'_.

2. Copy the new certificate to the folder "Build\signing" overwriting the exiting certificate.

3. If the certificate password has changed this will need to be changed all places in the project where this exists:<br>
   3.1 Do a find on old password in the project files and replace with new password.<br>
   3.2 Ideagen hopes to remove the hard coding of passwords out the project eventually.<br>

4. Update the ClickOnce projects:<br>
   4.1 Open the main solution '_IX_CADNostic_RED.sln_'.<br>
   4.2 Right click the project '_IX_ClickOnce > Properties > Signing_'.<br>
   4.3 Click on _'Select from file'_ and then select the certificate:<br> - The password will be requested here.<br>
   4.4 The certificate's information should update in the property page.<br> - Be sure to check that all the fields have the expected value, specially the "Issued To" and the "Expiration Date".<br>
   4.5 Save the project:<br> - Go on your git client, you will see several changes on the project file.<br> - The only change needed is the one to the "_ManifestCertificateThumbprint_" tag, discard all the other changes.<br>
   4.6 Copy the ManifestCertificateThumbprint tag's value to all the other clickonce projects:<br>
   To do this unload the project (right click > unload project).<br>
   Then right click > edit _'<projectName>.csproj'_.<br> - This will open the XML editor.<br> - Now you can manually change the value for "_ManifestCertificateThumbprint_" tag to match what was saved in the '_IX_ClickOnce_' project.<br> - Save the project file. <br> - Close the project file.<br> - Reload the project file by right clicking on the project > Reload project.<br> - We do this because in the pre-build event we are copying the signing certificate from the folder _'Build\signing'_ to avoid having a copy of the signing certificate on each project.<br>
   4.7 Clear the visual studio's cache files:<br> - Close visual studio.<br> - You can use the git command `git clean -fdx` to clear the whole repository or you can manually delete all the cache files (.suo, .db, .user, etc...).<br>
   4.8 Reopen Visual Studio.<br>
   4.9 Check that the ClickOnce projects are displaying the correct certificate information by '_IX_ClickOnce\_\_\* > Properties > Signing_'.<br>
   4.10 Carry out the build steps to ensure the project builds as described in Build Solution at [Developer Machine Set-up](#markdown-header-developer-machine-set-up).<br>
   4.11 Commit source code branch which is '_develop_'.<br>

5. On the Jenkins Build Server: <br>
   5.1 Connect to the Ideagen VPN.<br>
   5.2 Remote desktop to Jenkins build server '_ec2-16-170-240-247.eu-north-1.compute.amazonaws_'. Details can be found in Password Manager in OnDemand area.<br>
   5.3 On the build server is a folder '_C:\Development_'. This is a git clone of "_qc-pro_" on bitbucket. <br>
   5.4 Ensure branch of code is '_develop_'. Then pull down latest certificate code changes.<br>
   5.5 Continue with instructions for '_Code Signing_' described at [Jenkins Build Server Set-up](#markdown-header-jenkins-build-server-set-up)<br>
   5.6 You can now run a Jenkins Build with the new certificate.<br>

## How to run a Jenkins Build

1. Connect to the Ideagen VPN.
2. Remote desktop to Jenkins build server '_ec2-16-170-240-247.eu-north-1.compute.amazonaws_'. Details can be found in Password Manager in OnDemand area.
3. There are 2 icons on the desktop:<br>
   3.1 RunJenkins.bat: Run this as administrator to launch Jenkins if Jenkins is not running.<br>
   3.2 Dashboard [Jenkins]: This launches the Jenkins web interface.<br>
   3.3 From dashboard click on job _'QC-Professional'_.<br>
   3.4 On left panel of the Jenkins dashboard select _'Build with parameters'_.<br>
   3.5 Select parameters for build.<br>
   3.6 Select _'Build'_.<br>
   3.7 Build should kick off which could take up to 2 hours depending on parameters chosen.<br>
   3.8 '_Console Output_' displays the build output.<br>
   3.9 Once build has completed build files reside at C:\JG\ as specified by the HULK_BASE parameter. e.g C:\JG\IX20_2023\15\x64.<br>

   ***
