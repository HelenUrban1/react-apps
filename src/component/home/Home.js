import React,{useEffect,useContext} from "react"
import { LocalContext } from "../../App";

function Home(props){

    const {patientData, setPatientData} = useContext(LocalContext)
    console.log(patientData)
    
    useEffect(() => {
        console.log("In HOME useeffect")
        
        fetch('/patients', {
        method: "GET"
      })
      .then(response => response.json())
      .then(data => setPatientData(data))      

    },[]);

    
    return(
        <div>
            <h1>Patient List</h1>
            <p>{JSON.stringify(patientData)}</p>
        </div>
        
    )
}

export default Home