import React,{useContext} from "react"
import { LocalContext } from "../../App";
import logo from './PF.png';
import './header.css'
/*<img src={logo} className="App-logo" alt="logo" />*/
function Header(props) {

    const {patientData, setPatientData} = useContext(LocalContext)
    console.log(patientData)

    function doClick()
    {
        setPatientData("Hello State")
        alert("Compenent State Set",patientData)
    }

    return(
        
        <div>
            <div className="header">                
                <p>Patient First Menu</p>
                <button onClick={doClick}>Click Me</button>  
            </div>
        </div>
    )
}

export default Header