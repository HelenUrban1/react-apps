import React,{useEffect,useRef} from "react"
import "./patientregister.css"

function PatientRegister(){

    const inputRef = useRef()
    useEffect(() => {
        inputRef.current.focus()
    }, []);

    return(
        <form action="/patients" method="POST">
        <div>
            <div className="patRegisterForm">                                

                <div className="grid-container">
                    
                <div className="item1" >Patient Registration</div>
                
                <div className="item3">
                    <table border="0">
                        <thead>
                        <tr>
                            <td>
                            <label>First Name</label>
                            <input required type="text" placeholder="First Name" name="firstName" ref={inputRef} />
                            </td>
                        
                            <td>
                            <label>Surname</label>
                            <input type="text" placeholder="Surname" name="surname" />
                            </td>

                            <td>
                            <label>Title</label>
                            <select name="titleList" id="titleList">
                              <option value="option 1">Mr</option>
                              <option value="option 2">Mrs</option>
                            <option value="option 2">Miss</option>
                            <option value="option 2">Master</option>
                            <option value="option 2">Ms</option>
                            <option value="option 2">Doctor</option>
                            </select>
                            </td>
                        
                            <td>
                            <label>Date of Birth</label>
                            <input type="text" placeholder="Date of Birth" name="dateofBirth" />
                            </td>
                    </tr>
                    <tr>
                        <td>
                        <label>NHS Number</label>
                        <input type="text" placeholder="NHS Number" name="nhsNumber" />
                        </td>
                        
                        <td>
                        <label>Gender</label>
                        <select name="genderList" id="genderList">
                          <option value="option 1">Male</option>
                          <option value="option 2">Female</option>
                          <option value="option 2">Unrecorded</option>
                        </select>
                        </td>

                        <td>
                        <label>Occupation</label>
                        <input type="text" placeholder="Occupation" name="occupation" />
                        </td>

                        <td>
                        <label>Employer \ School</label>
                        <input type="text" placeholder="Employer School" name="Employer" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                        <label>Marital Status</label>
                        <input type="text" placeholder="Marital Status" name="maritalstatus" />
                        </td>

                        <td>
                        <label>Ethnic Origin</label>
                        <input type="text" placeholder="Ethnic Origin" name="ethnic" />
                        </td>
                    </tr>
                    </thead>
                   </table> 
                </div>  
                
                <div className="item5"><button id="submit" type="submit">Submit</button></div>
                </div>
            </div>
        </div>
      
        </form>
        
    )
}

export default PatientRegister