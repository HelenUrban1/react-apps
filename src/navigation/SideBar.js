import React from "react"
import { Link, Route, Switch } from 'react-router-dom'
import sidebarData from "./sideBarData"
import { BrowserRouter as Router } from "react-router-dom"
import PatientRegister from "../component/patientregistration/PatientRegister"
import Whiteboard from "../component/whiteboard/Whiteboard"
import Inquiry from "../component/inquiry/Inquiry"
import Admin from "../component/admin/Admin"
import Home from "../component/home/Home"

import "./sideBar.css"

function SideBar() {

  return (
    <div>
      <Router>
        <nav className={"nav-menu"}>
          <ul className="nav-menu-items">
            {sidebarData.map((item) => {
              return (
                <li key={item.id} className="nav-text">
                  <Link
                    to={item.route}
                    className="menu-items"                     
                  >
                    {item.icon}
                    <span>{item.title}</span>
                  </Link>
                </li>
              );
            })}
          </ul>
        </nav>

        <Switch>
            <Route exact path='/patientregister'><PatientRegister /></Route>
            <Route exact path='/whiteboard'><Whiteboard /></Route>
            <Route exact path='/inquiry'><Inquiry /></Route>
            <Route exact path='/admin'><Admin /></Route>
            <Route exact path='/home'><Home /></Route>
        </Switch>

      </Router>
    </div>
  );
}

export default SideBar
