import * as AiIcons from "react-icons/io";

const sidebarData = [
  {
    id: 0,
    title: "Home",
    icon: <AiIcons.IoIosHome />,
    route: "/home",
  },
  {
    id: 1,
    title: "Register",
    icon: <AiIcons.IoIosPaper />,
    route: "/patientregister",
  },
  {
    id: 2,
    title: "Whiteboard",
    icon: <AiIcons.IoIosPulse />,
    route: "/whiteboard",
  },
  {
    id: 3,
    title: "Inquiry",
    icon: <AiIcons.IoMdImage />,
    route: "/inquiry",
  },
  {
    id: 4,
    title: "Admin",
    icon: <AiIcons.IoMdList />,
    route: "/admin",
  },
];

export default sidebarData;
