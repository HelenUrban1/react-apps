import React, {useState, createContext} from 'react'
import './App.css';
import SideBar from './navigation/SideBar';
import Header from './component/header/Header.js'

export const LocalContext = createContext()

function App() {

  const [patientData,setPatientData] = useState(null)    

  return (
    <LocalContext.Provider value= {{setPatientData,patientData}} >
      <div className="App">        
        <Header />        
        <SideBar />
      </div>
    </LocalContext.Provider>
  );
}

export default App;
