console.log("May node be with you")

const express = require('express')
const app = express()
const bodyParser= require('body-parser')
const MongoClient = require('mongodb').MongoClient
//url to local host of mongo db
const url = process.env.MONGODB_URI || `mongodb://localhost:27017/patient-first`;

app.set('view engine', 'ejs')
app.use(express.static('public'))
app.use(bodyParser.json())

MongoClient.connect(url)
    .then (client => {
        console.log('Connected to Database',url)
        const db = client.db('patient-first')
        //collection = table
        const patientCollection = db.collection('patients')
        
        // Make sure you place body-parser before your CRUD handlers!
        app.use(bodyParser.urlencoded({ extended: true }))

        app.listen(3333, function(){
            console.log('Listening on port 3333')
        })

        app.get('/patients', (req,res) => {
            /*let tryFetch = {myString: 'I am working test json fetch'};
            console.log(tryFetch)         
            res.send(JSON.stringify(tryFetch))*/

            db.collection('patients').find().toArray()
                .then(results =>{
                    console.log(results)                    
                    res.send(JSON.stringify(results))                    
                })
                .catch(error => {console.error(error)})
            
        })

        app.post('/patients', (req,res) =>{
            console.log("hello world patients post")
            console.log(req.body)
            //insertOne adds items into collection
            patientCollection.insertOne(req.body)
                .then(result => {
                    console.log(result)
                    res.redirect('/home')
                })
                .catch(error => console.error(error))
        })

        app.put('/patients', (req, res) => {
            console.log("IN PUT")
            console.log('put',req.method)
            console.log('put',req.headers)
            console.log('put',req.body)
            patientCollection.findOneAndUpdate(
            { name: 'Yoda' },
            {
                $set: {
                name: req.body.name,
                quote: req.body.quote
                }
            },
            {
                upsert: true
            }
            )
            .then(result => {console.log(result) 
                res.json('Success')})
            .catch(error => console.error(error))



        })

    })
    .catch(error => console.error(error))
  
  
